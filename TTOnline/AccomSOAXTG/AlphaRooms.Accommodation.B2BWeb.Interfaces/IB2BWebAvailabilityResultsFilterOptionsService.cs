﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityResultsFilterOptionsService
    {
        AccommodationB2BWebAvailabilityResultsFilterOptions CreateEmptyFilterOptions(AccommodationB2BWebAvailabilityRequest availabilityRequest, ChannelInfo channelInfo);
        Task<AccommodationB2BWebAvailabilityResultsFilterOptions> CreateResultsFilterOptionsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, ChannelInfo channelInfo, IList<AccommodationB2BWebAvailabilityResult> results);
    }
}
