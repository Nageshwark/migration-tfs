﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebValuationRequestFactory
    {
        AccommodationValuationRequest CreateValuationRequest(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationB2BWebValuationRequest valuationRequest, ChannelInfo channelInfo, B2BUser agent);
    }
}
