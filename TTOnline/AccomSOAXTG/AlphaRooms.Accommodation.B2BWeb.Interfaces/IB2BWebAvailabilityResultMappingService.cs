﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityResultMappingService
    {
        Task<IList<AccommodationB2BWebAvailabilityResult>> MapFromCoreAvailabilityResultsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> coreResults);
        AccommodationAvailabilityResult[] MapToCoreAvailabilityResult(IList<AccommodationB2BWebAvailabilityResult> availabilityResults);
    }
}
