﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityResultLowerPriceService
    {
        AccommodationB2BWebAvailabilityResult[] ApplyAvailabilityLowerPrice(AccommodationB2BWebAvailabilityResult[] result);
        AccommodationB2BWebAvailabilityResult ApplyAvailabilityLowerPrice(AccommodationB2BWebAvailabilityResult result);

    }
}