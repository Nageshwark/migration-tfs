﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebBookingLogger
    {
        Task LogAsync(AccommodationB2BWebBookingRequest bookingRequest, AccommodationB2BWebRequestStatus requestStatus, AccommodationBookingResponse coreBookingResponse, TimeSpan timeSpan, Exception exception);
    }
}
