﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityService
    {
        Task<Guid> StartAvailabilitySearchAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest);

        Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria);

        Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseByEstablishmentAsync(Guid availabilityId, Guid establishment, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria);

        Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseByRoomsAsync(Guid availabilityId, string[] roomIds);

        Task<AccommodationB2BWebAvailabilityRequest> GetAvailabilityRequestAsync(Guid availabilityId);

        Task<AccommodationB2BWebAvailabilityResultsFilterOptions> GetAvailabilityResultsFilterOptionsAsync(Guid availabilityId);

        Task<AccommodationB2BWebAvailabilityMapResponse> GetAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria);

        Task<AccommodationB2BWebAvailabilityMapResponse> GetAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId);
    }
}
