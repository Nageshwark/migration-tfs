﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebValuationResultMappingService
    {
        Task<AccommodationB2BWebValuationResult> MapFromCoreValuationResultsAsync(AccommodationB2BWebValuationRequest valuationRequest, AccommodationValuationResult coreAvailabilityResponse);
    }
}
