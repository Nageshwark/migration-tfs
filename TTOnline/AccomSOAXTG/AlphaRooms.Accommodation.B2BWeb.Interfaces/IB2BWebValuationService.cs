﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebValuationService
    {
        Task<Guid> StartValuationAsync(AccommodationB2BWebValuationRequest valuationRequest);

        Task<AccommodationB2BWebValuationResponse> GetValuationResponseAsync(Guid valuationId);
    }
}
