﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityProviderIdentifierService
    {
        Task<AccommodationProvider[]> GetActiveAvailabilityProvidersAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest);
    }
}
