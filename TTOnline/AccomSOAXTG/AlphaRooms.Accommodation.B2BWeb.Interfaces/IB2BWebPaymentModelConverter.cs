﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebPaymentModelConverter
    {
        PaymentModel[] ToPaymentModels(PaymentTypeType paymentType);

        PaymentTypeType ToPaymentType(PaymentModel paymentModel);
    }
}
