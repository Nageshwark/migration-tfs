﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebCancellationResultMappingService
    {
        AccommodationB2BWebCancellationResult MapFromCoreCancellationResults(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationCancellationResult coreCancellationResult);
    }
}
