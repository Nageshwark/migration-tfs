﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebBookingResultMappingService
    {
        AccommodationB2BWebBookingResult MapFromCoreBookingResults(AccommodationB2BWebBookingRequest bookingRequest, AccommodationBookingResult result);
    }
}
