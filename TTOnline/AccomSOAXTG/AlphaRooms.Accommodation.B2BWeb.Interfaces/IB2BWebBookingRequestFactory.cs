﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebBookingRequestFactory
    {
        AccommodationBookingRequest CreateBookingRequest(AccommodationB2BWebBookingRequest bookingRequest, ChannelInfo channelInfo, B2BUser agent);
    }
}
