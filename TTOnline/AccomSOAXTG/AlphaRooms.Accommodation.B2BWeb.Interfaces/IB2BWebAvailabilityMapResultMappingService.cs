﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityMapResultMappingService
    {
        IList<AccommodationB2BWebAvailabilityMapResult> MapFromCoreAvailabilityResultsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, IList<AccommodationB2BWebAvailabilityResult> results);
    }
}
