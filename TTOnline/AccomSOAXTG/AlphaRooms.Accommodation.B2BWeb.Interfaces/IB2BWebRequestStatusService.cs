﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebRequestStatusService
    {
        AccommodationB2BWebRequestStatus CreateAvailabilityRequestStatus(AccommodationB2BWebAvailabilityRequest availabilityRequest);

        AccommodationB2BWebRequestStatus CreateValuationRequestStatus(AccommodationB2BWebValuationRequest valuationRequest);

        AccommodationB2BWebRequestStatus CreateBookingRequestStatus(AccommodationB2BWebBookingRequest bookingRequest);

        AccommodationB2BWebRequestStatus CreateCancellationRequestStatus(AccommodationB2BWebCancellationRequest cancellationRequest);

        Task<AccommodationB2BWebRequestStatus> GetRequestStatusOrNullByIdAsync(Guid id);

        Task<AccommodationB2BWebRequestStatus> GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(string availabilityKey);

        DateTime CreateResultsCacheExpireDate();

        bool IsResultsExpireDateExpired(AccommodationB2BWebRequestStatus requestStatus);

        Task SaveRequestStatusAsync(AccommodationB2BWebRequestStatus requestStatus);

        Task UpdateRequestStatus(AccommodationB2BWebRequestStatus requestStatus, Status status, string message);
    }
}
