﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebValuationLogger
    {
        Task LogAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationB2BWebValuationRequest valuationRequest, AccommodationB2BWebRequestStatus requestStatus, AccommodationValuationResponse coreResponse, TimeSpan timeTaken, Exception exception);
    }
}
