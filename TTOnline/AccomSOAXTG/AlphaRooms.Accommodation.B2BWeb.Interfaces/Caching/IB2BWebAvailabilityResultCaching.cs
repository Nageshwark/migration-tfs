﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Cache.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching
{
    public interface IB2BWebAvailabilityResultCaching
    {
        Task<AccommodationB2BWebAvailabilityResults> GetFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, int roomCount, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria, bool debugging);

        Task<AccommodationB2BWebAvailabilityResult> GetByEstablishmentIdAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId);

        Task<AccommodationB2BWebAvailabilityResult> GetByEstablishmentIdFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, Guid establishment, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria, bool debugging);

        Task<AccommodationB2BWebAvailabilityResult[]> GetByRoomIdsAsync(Guid availabilityId, Guid baseDestinationId, string[] roomIds);

        Task SaveRangeAsync(Guid resultsBaseDestinationId, IEnumerable<AccommodationB2BWebAvailabilityResult> results);

        Task DeleteRangeAsync(Guid baseDestinationId, Guid availabilityId);
    }
}
