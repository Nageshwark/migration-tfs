﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching
{
    public interface IB2BWebRequestStatusCaching
    {
        Task<AccommodationB2BWebRequestStatus> GetByIdOrNullAsync(Guid requestId);

        Task<AccommodationB2BWebRequestStatus> GetSuccessfulByAvailabilityKeyOrNullAsync(string availabilityKey);

        Task SaveAsync(AccommodationB2BWebRequestStatus requestStatus);

        DateTime CreateResultsCacheExpireDate();

        bool IsResultsExpireDateExpired(AccommodationB2BWebRequestStatus requestStatus);
    }
}
