﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories
{
    public interface IB2BWebValuationLogRepository
    {
        Task SaveRangeAsync(B2BWebValuationLog[] b2BWebValuationLog);
    }
}
