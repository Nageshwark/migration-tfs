﻿using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories
{
    public interface IB2BWebAvailabilityLogRepository
    {
        Task SaveAsync(B2BWebAvailabilityLog B2BWebAvailabilityLog);        
    }
}
