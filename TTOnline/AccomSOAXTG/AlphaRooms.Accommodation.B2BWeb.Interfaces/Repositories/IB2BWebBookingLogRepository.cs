﻿using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories
{
    public interface IB2BWebBookingLogRepository
    {
        Task SaveRangeAsync(B2BWebBookingLog[] B2BWebBookingLog);
    }
}
