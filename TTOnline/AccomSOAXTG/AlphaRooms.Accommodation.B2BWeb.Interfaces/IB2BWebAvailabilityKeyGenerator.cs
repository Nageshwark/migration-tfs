﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebAvailabilityKeyGenerator
    {
        string CreateAvailabilityKey(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationProvider[] providers);
    }
}
