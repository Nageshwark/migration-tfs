﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Interfaces
{
    public interface IB2BWebCancellationRequestFactory
    {
        AccommodationCancellationRequest CreateCancellationRequest(AccommodationB2BWebCancellationRequest bookingRequest, ChannelInfo channelInfo, B2BUser agent);
    }
}
