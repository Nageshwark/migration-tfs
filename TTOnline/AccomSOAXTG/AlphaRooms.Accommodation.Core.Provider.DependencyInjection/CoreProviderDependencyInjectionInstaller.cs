﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.Repositories;
using AlphaRooms.Accommodation.Core.Provider.Services;
using AlphaRooms.Accommodation.Core.Provider.Services.FilterProcessors;
using AlphaRooms.Accommodation.Provider.TravelGate;
using AlphaRooms.Accommodation.Provider.TravelGate.Factories;
using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using AlphaRooms.Configuration;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities.Ninject;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DependencyInjection
{
    public class CoreProviderDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            // database
            Bind<IDbContextActivator<AccommodationDbContext>>().To<NoProxyDbContextActivator<AccommodationDbContext>>().InSingletonScope();
            Bind<IDbContextActivator<CompetitorPricingDbContext>>().To<NoProxyDbContextActivator<CompetitorPricingDbContext>>().InSingletonScope();

            // business repository
            Bind<ICacheProviderAsync<AccommodationProvider>>().To<CacheProviderAsync<AccommodationProvider>>().InSingletonScope();
            Bind<IAccommodationProviderRepository>().To<AccommodationProviderRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<string>>().To<CacheProviderAsync<string>>().InSingletonScope();
            Bind<ISupplierExcludedOfDepositRepository>().To<SupplierExcludedOfDepositRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<TravellerType>>().To<CacheProviderAsync<TravellerType>>().InSingletonScope();
            Bind<ITravellerTypeRepository>().To<TravellerTypeRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<Facility>>().To<CacheProviderAsync<Facility>>().InSingletonScope();
            Bind<IFacilityRepository>().To<FacilityRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<DestinationSearchMode>>().To<CacheProviderAsync<DestinationSearchMode>>().InSingletonScope();
            Bind<IDestinationSearchModeRepository>().To<DestinationSearchModeRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<Board>>().To<CacheProviderAsync<Board>>().InSingletonScope();
            Bind<IBoardRepository>().To<BoardRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<SupplierAlias>>().To<CacheProviderAsync<SupplierAlias>>().InSingletonScope();
            Bind<ISupplierAliasRepository>().To<SupplierAliasRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<B2BUser>>().To<CacheProviderAsync<B2BUser>>().InSingletonScope();
            Bind<IB2BUserRepository>().To<B2BUserRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<BlockedDestination>>().To<CacheProviderAsync<BlockedDestination>>().InSingletonScope();
            Bind<IBlockedDestinationRepository>().To<BlockedDestinationRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<CancellationPolicy>>().To<CacheProviderAsync<CancellationPolicy>>().InSingletonScope();
            Bind<ICancellationPolicyRepository>().To<CancellationPolicyRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<AccommodationSupplier>>().To<CacheProviderAsync<AccommodationSupplier>>().InSingletonScope();
            Bind<IAccommodationSupplierRepository>().To<AccommodationSupplierRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<Mapping>>().To<CacheProviderAsync<Mapping>>().InSingletonScope();
            Bind<IMappingRepository>().To<MappingRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<Markup>>().To<CacheProviderAsync<Markup>>().InSingletonScope();
            Bind<IMarkupRepository>().To<MarkupRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<PriceAdjustment>>().To<CacheProviderAsync<PriceAdjustment>>().InSingletonScope();
            Bind<IPriceAdjustmentRepository>().To<PriceAdjustmentRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<ProviderCommission>>().To<CacheProviderAsync<ProviderCommission>>().InSingletonScope();
            Bind<IProviderCommissionRepository>().To<ProviderCommissionRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<VolumeDiscount>>().To<CacheProviderAsync<VolumeDiscount>>().InSingletonScope();
            Bind<IVolumeDiscountRepository>().To<VolumeDiscountRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<StarRating>>().To<CacheProviderAsync<StarRating>>().InSingletonScope();
            Bind<IStarRatingRepository>().To<StarRatingRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<PaymentType>>().To<CacheProviderAsync<PaymentType>>().InSingletonScope();
            Bind<IPaymentTypeRepository>().To<PaymentTypeRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<Destination>>().To<CacheProviderAsync<Destination>>().InSingletonScope();
            Bind<IDestinationRepository>().To<DestinationRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<VirtualCreditCardRule>>().To<CacheProviderAsync<VirtualCreditCardRule>>().InSingletonScope();
            Bind<IVirtualCreditCardRuleRepository>().To<VirtualCreditCardRuleRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<RoomDescriptionRemoveWord>>().To<CacheProviderAsync<RoomDescriptionRemoveWord>>().InSingletonScope();
            Bind<IRoomDescriptionRemoveWordRepository>().To<RoomDescriptionRemoveWordRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<RoomDescriptionWordReplacement>>().To<CacheProviderAsync<RoomDescriptionWordReplacement>>().InSingletonScope();
            Bind<IRoomDescriptionReplacementRepository>().To<RoomDescriptionReplacementRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<AccommodationB2BFlashSaleEstablishment>>().To<CacheProviderAsync<AccommodationB2BFlashSaleEstablishment>>().InSingletonScope();
            Bind<IAccommodationFlashSaleRepository>().To<AccommodationFlashSaleRepository>().InSingletonScope();
			Bind<IAccommodationProviderAdminFeeConfigurationRepository>().To<AccommodationProviderAdminFeeConfigurationRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<AccommodationProviderAdminFeeConfiguration>>().To<CacheProviderAsync<AccommodationProviderAdminFeeConfiguration>>().InSingletonScope();


            // catalog repository
            Bind<ICacheProviderAsync<BoardDescriptionMapping>>().To<CacheProviderAsync<BoardDescriptionMapping>>().InSingletonScope();
            Bind<IBoardDescriptionMappingRepository>().To<BoardDescriptionMappingRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<RoomDescriptionMapping>>().To<CacheProviderAsync<RoomDescriptionMapping>>().InSingletonScope();
            Bind<IRoomDescriptionMappingRepository>().To<RoomDescriptionMappingRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<EstablishmentDetails>>().To<CacheProviderAsync<EstablishmentDetails>>().InSingletonScope();
            Bind<IEstablishmentDetailsRepository>().To<EstablishmentDetailsRepository>().InSingletonScope();
            Bind<ICacheProviderAsync<AccommodationProviderEmailTemplate>>().To<CacheProviderAsync<AccommodationProviderEmailTemplate>>().InSingletonScope();
            Bind<IAccommodationProviderEmailTemplateRepository>().To<AccommodationProviderEmailTemplateRepository>().InSingletonScope();

            // shared service
            Bind<IProviderOutputLoggerSettings>().To<AccommodationConfigurationManager>().InSingletonScope();
            Bind<IProviderOutputLogger>().To<ProviderOutputLogger>().InSingletonScope();
            Bind<IProviderParameterUpdater<AccommodationProvider>>().To<SupplierParameterUpdater<AccommodationProvider>>().InSingletonScope();
            Bind<ISmtpServerSettings>().To<AccommodationConfigurationManager>().InSingletonScope();
            Bind<ISmtpClientService>().To<SmtpClientService>().InSingletonScope();
            Bind<IDiParameterContainer>().To<DiParameterContainer>().InSingletonScope();
            Bind<IProviderLoggerService>().To<ProviderLoggerService>();
            Bind<IProviderNonRefundableService>().To<ProviderNonRefundableService>().InSingletonScope();
            Bind<IProviderProxyService>().To<ProviderProxyService>().InSingletonScope();
            Bind<IProviderSpecialRequestService>().To<ProviderSpecialRequestService>().InSingletonScope();

            // availability service
            Bind<IProviderAvailabilityService>().To<ProviderAvailabilityService>().InSingletonScope();
            Bind<IProviderAvailabilityFactory>().ToFactory(() => new UseFirstArgumentAsNameInstanceProvider()).InSingletonScope();
            Bind<IProviderAvailabilityFilterService>().To<ProviderAvailabilityFilterService>().InSingletonScope();
            Bind<IProviderAvailabilityFilterProcessor>().To<RemoveBlockedDestinationProcessor>().InSingletonScope();
            Bind<IProviderAvailabilityFilterProcessor>().To<RemoveNonPayDirectProcessor>().InSingletonScope();

            // valuation service
            Bind<IProviderValuationService>().To<ProviderValuationService>().InSingletonScope();
            Bind<IProviderValuationFactory>().ToFactory(() => new UseFirstArgumentAsNameInstanceProvider()).InSingletonScope();
            Bind<IProviderValuationCancellationPolicyService>().To<ProviderValuationCancellationPolicyService>().InSingletonScope();
            Bind<IProviderValuationVirtualCreditCardRuleService>().To<ProviderValuationVirtualCreditCardRuleService>().InSingletonScope();

            // booking service
            Bind<IProviderBookingService>().To<ProviderBookingService>().InSingletonScope();
            Bind<IProviderBookingFactory>().ToFactory(() => new UseFirstArgumentAsNameInstanceProvider()).InSingletonScope();

            // cancellation service
            Bind<IProviderCancellationService>().To<ProviderCancellationService>().InSingletonScope();
            Bind<IProviderCancellationFactory>().ToFactory(() => new UseFirstArgumentAsNameInstanceProvider()).InSingletonScope();
        }
    }
}
