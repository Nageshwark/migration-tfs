﻿

using System;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.DomainModels
{
    public class PerformanceLog
    {
        public string ProductType { get; set; }
        public string OperationType { get; set; }
        public Channel Channel { get; set; }
        public string RequestIdentifier { get; set; }
        public string DestinationIdentifier { get; set; }
        public DateTime StartTime { get; set; }
        public int TotalTime { get; set; }
        public bool IsSuccess { get; set; }
        public int ResultCount { get; set; } = -1;
        public string HostName { get; set; }
        public string CacheHit { get; set; }
    }
}