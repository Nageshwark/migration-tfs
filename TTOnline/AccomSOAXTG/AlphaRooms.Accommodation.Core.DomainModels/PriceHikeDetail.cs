﻿namespace AlphaRooms.Accommodation.Core.DomainModels
{
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.Accommodation.Core.DomainModels.Enums;

    public class PriceHikeDetail
    {
        public bool priceChange { get; set; }
        public PriceHikeStatus Status { get; set; }
        public Money OriginalCost { get; set; }
        public Money OriginalSale { get; set; }
        public Money PriceHikeCost { get; set; }
        public Money PriceHikeSale { get; set; }
    }
}
