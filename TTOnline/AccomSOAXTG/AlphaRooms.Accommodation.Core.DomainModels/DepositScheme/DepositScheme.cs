﻿namespace AlphaRooms.Accommodation.Core.DomainModels
{
    using System;

    public class DepositScheme
    {
        public int Id { get; set; }
        public bool IsEnabled { get; set; }

        public int DepositMinWeeks { get; set; }
        public bool IsPercentage { get; set; }
        public decimal DepositMinSpend { get; set; }
        public decimal DepositMinHotelSpend { get; set; }

        public decimal? DepositDefaultValue { get; set; }

        public int BalanceDueWeeksBefore { get; set; }
        public decimal BalancePaymentMinSpend { get; set; }

        public DateTime StartDate { get; set; }

        public int? DepositDueWeeksAfter { get; set; }

        public bool? DepositDueIsPercentage { get; set; }

        public decimal? DepositDueMinSpend { get; set; }
    }
}