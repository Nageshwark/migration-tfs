﻿namespace AlphaRooms.Accommodation.Core.DomainModels
{
    using System;

    public interface IDepositSchemeV1Queryable<T>
        where T : DepositSchemeV1
    {
        int Id { get; set; }
        bool IsEnabled { get; set; }

        bool AllowDeposits { get; set; }

        int DepositMinWeeks { get; set; }
        bool IsPercentage { get; set; }
        decimal DepositMinSpend { get; set; }
        decimal DepositMinHotelSpend { get; set; }

        decimal? DepositDefaultValue { get; set; }

        int BalanceDueWeeksBefore { get; set; }
        decimal BalancePaymentMinSpend { get; set; }

        DateTime StartDate { get; set; }
    }
}