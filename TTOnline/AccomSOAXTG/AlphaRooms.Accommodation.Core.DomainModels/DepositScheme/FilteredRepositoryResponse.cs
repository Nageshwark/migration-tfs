﻿namespace AlphaRooms.Accommodation.Core.DomainModels
{
    using System.Collections.Generic;

    public class FilteredRepositoryResponse<TEntity>
    {
        /// <summary>
        /// The results of the query
        /// </summary>
        public IList<TEntity> Results { get; private set; }

        /// <summary>
        /// The count of all records that match the filter criteria
        /// </summary>
        public int TotalCount { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public FilteredRepositoryResponse(IList<TEntity> results, int totalCount)
        {
            this.Results = results;
            this.TotalCount = totalCount;
        }
    }
}