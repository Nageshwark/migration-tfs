﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.DomainModels
{
   public class DepositSchemeV1
    {
        public int Id { get; set; }
        public bool IsEnabled { get; set; }
        public decimal? DepositMinHotelSpend { get; set; }
        public bool IsDepositAdminFeeApplicable { get; set; }
        public decimal? AdminFee { get; set; }
        public bool IsDepositTopUpPaymentEnabled { get; set; }
        public decimal? DepositValue { get; set; }
        public int? DepositTopUpDueDaysAfterBooking { get; set; }
        public bool IsDepositTopUpPercentage { get; set; }
        public bool IsDepositTopUpAdminFee { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime createdDate { get; set; }
        public decimal? BalancePaymentsMinAmt { get; set; }
        public int? MaxInterimPayments { get; set; }
        public string InterimPaymentsSchedule { get; set; }
        public int? DaysBtwInterimPayments { get; set; }
        public bool IsInterimAdminFeeApplicable { get; set; }
        public int? FinalBalanceDueDays { get; set; }

        public decimal? DepositTopUPValue { get; set; }
        public bool IsInterimPaymentsApplicable { get; set; }

        public bool IsInterimPercentage { get; set; }

        public decimal InterimAmount { get; set; }
    }
}
