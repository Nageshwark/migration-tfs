﻿namespace AlphaRooms.Accommodation.Core.DomainModels
{
    public class PriceHikeThreshold
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int? SupplierId { get; set; }
        public decimal Threshold { get; set; }
    }
}
