﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.DomainModels.Enums
{
    public enum PriceHikeStatus
    {
        Acceptable = 1,
        Unnaceptable = 2,
        CurrencyDifference = 3,
        SupplierNotConfigured = 4
    }
}
