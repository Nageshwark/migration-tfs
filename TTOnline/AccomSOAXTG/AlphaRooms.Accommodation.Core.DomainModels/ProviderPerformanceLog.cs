﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.DomainModels
{

    public class ProviderPerformanceLog : PerformanceLog
    {
        public string Provider { get; set; }
        public DateTime? Arrival { get; set; }
        public DateTime? Departure { get; set; }
    }
}
