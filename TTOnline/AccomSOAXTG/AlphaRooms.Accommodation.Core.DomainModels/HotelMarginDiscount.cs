﻿using System;

namespace AlphaRooms.Accommodation.Core.DomainModels
{
    public class HotelMarginDiscount
    {
        public int pkID { get; set; }
        public string fkEdiProvider { get; set; }
        public string strEdiCode { get; set; }
        public DateTime dtmStart { get; set; }
        public DateTime dtmEnd { get; set; }
        public decimal dclDiscount { get; set; }
        public bool blnCommissionable { get; set; }
        public int fkChannelID { get; set; }
        public bool blnDoNotDelete { get; set; }
        public bool blnExcludeFromAdapative { get; set; }
    }
}
