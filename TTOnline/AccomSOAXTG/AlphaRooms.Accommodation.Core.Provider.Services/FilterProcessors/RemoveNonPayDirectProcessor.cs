﻿namespace AlphaRooms.Accommodation.Core.Provider.Services.FilterProcessors
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using System;
    using System.Threading.Tasks;

    public class RemoveNonPayDirectProcessor : IProviderAvailabilityFilterProcessor
    {
        public Task InitializeAsync()
        {
            return Task.FromResult(0);
        }

        public bool CanProcess(AccommodationProviderAvailabilityRequest providerAvailabilityRequest, AccommodationProviderAvailabilityResult result)
        {
            return (result.PaymentModel == PaymentModel.CustomerPayDirect
                || (result.PaymentModel != PaymentModel.CustomerPayDirect
                    && (result.CheckInDate - DateTime.Now).TotalDays > providerAvailabilityRequest.Provider.MinNonPayDirectDays));
        }
    }
}
