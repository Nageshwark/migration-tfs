﻿namespace AlphaRooms.Accommodation.Core.Provider.Services.FilterProcessors
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using System.Threading.Tasks;

    public class RemoveBlockedDestinationProcessor : IProviderAvailabilityFilterProcessor
    {
        private readonly IBlockedDestinationRepository blockedDestinationResposity;
        private BlockedDestination[] blockedDestinations;

        public RemoveBlockedDestinationProcessor(IBlockedDestinationRepository blockedDestinationResposity)
        {
            this.blockedDestinationResposity = blockedDestinationResposity;
        }

        public async Task InitializeAsync()
        {
            this.blockedDestinations = await this.blockedDestinationResposity.GetAllAsync();
        }

        public bool CanProcess(AccommodationProviderAvailabilityRequest providerAvailabilityRequest, AccommodationProviderAvailabilityResult result)
        {
            /* 
             * by the time we get here this.blockedDestinations should have been populated,
             * however this instance is shared between threads, and other threads will be over writting this.blockedDestinations
             * so to keep the below loop thread safe copy it into a local variable
             */
            BlockedDestination[] localBlockedDestinations = this.blockedDestinations;

            bool canProcess = true;

            for (int i = 0; i < localBlockedDestinations.Length; i++)
            {
                canProcess &= !RemoveBlockedDestinationProcessor.IsBlockedProvider(localBlockedDestinations[i], providerAvailabilityRequest);
            }

            return canProcess;
        }

        private static bool IsBlockedProvider(BlockedDestination blockedDestination, AccommodationProviderAvailabilityRequest availabilityRequest)
        {
            return (blockedDestination.Channel == null || blockedDestination.Channel == availabilityRequest.ChannelInfo.Channel)
                && (blockedDestination.ProviderId == null || blockedDestination.ProviderId == availabilityRequest.Provider.Id);
        }
    }
}