﻿using System.CodeDom;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.SOACommon.Contracts;
    using System.Linq;


    public class ProviderValuationVirtualCreditCardRuleService : IProviderValuationVirtualCreditCardRuleService
    {
        private readonly IVirtualCreditCardRuleRepository virtualCreditCardRuleRepository;
        private readonly IAccommodationSupplierRepository accommodationSupplierRepository;

        public ProviderValuationVirtualCreditCardRuleService(IVirtualCreditCardRuleRepository virtualCreditCardRuleRepository, IAccommodationSupplierRepository accommodationSupplierRepository)
        {
            this.virtualCreditCardRuleRepository = virtualCreditCardRuleRepository;
            this.accommodationSupplierRepository = accommodationSupplierRepository;
        }

        public async Task<IList<AccommodationProviderValuationResult>> ApplyVirtualCreditCardRulesAsync(AccommodationProviderValuationRequest valuationRequest, IList<AccommodationProviderValuationResult> results)
        {
            await Async.ParallelForEach(results, async(i) => i.VirtualCreditCardPaymentTerms = await SetVirtualCreditCardTermsAsync(valuationRequest, i));
            return results;
        }

        private async Task<AccommodationProviderValuationResultVccPaymentRule[]> SetVirtualCreditCardTermsAsync(AccommodationProviderValuationRequest valuationRequest
            , AccommodationProviderValuationResult valuationResult)
        {
            if (valuationResult.PaymentModel != PaymentModel.PayWithVirtualCard) return new AccommodationProviderValuationResultVccPaymentRule[0];

            /******* NEW CODE ******/
            DomainModels.VirtualCreditCardRule rule = null;
            int supplierId = 0;
            if (valuationResult.SupplierEdiCode == "A")
            {
                var supplier = await this.accommodationSupplierRepository.GetByNameAsync(valuationResult.ProviderName);
                if (supplier == null) throw new InvalidOperationException($"Unable to recover {valuationResult.ProviderName} supplier info (by name) from cache."); 
                supplierId = supplier.Id;
            }
            else
            {
                var supplier = await this.accommodationSupplierRepository.GetByEdiCodeAsync(valuationResult.SupplierEdiCode);
                if (supplier == null) throw new InvalidOperationException($"Unable to recover {valuationResult.SupplierEdiCode} supplier info (by code) from cache.");
                supplierId = supplier.Id;
            }

            var rules = (await this.virtualCreditCardRuleRepository.GetBySupplierIdAsync(supplierId)).ToList();
            if (rules.Count() > 0)
            {
                // if a room rule exist it has priority, otherwise pick generic rule for supplier
                rule = (rules.Any(x => x.RoomCode != null && x.RoomCode.Equals(valuationResult.RoomCode, StringComparison.OrdinalIgnoreCase)) ? rules.First(x => x.RoomCode != null && x.RoomCode.Equals(valuationResult.RoomCode, StringComparison.OrdinalIgnoreCase)) : rules.First(x => x.RoomCode == null));
            }
            /**********************/

            if (rule == null) rule = await this.virtualCreditCardRuleRepository.GetDefaultRuleAsync();
            var paymentRules = new List<AccommodationProviderValuationResultVccPaymentRule>();
            if (rule.AmountOnBooking != null && rule.AmountOnBooking > 0m)
            {
                paymentRules.Add(new AccommodationProviderValuationResultVccPaymentRule()
                {
                    RoomCode = valuationResult.RoomCode
                    , PaymentDate = DateTime.Today
                    , Price = new Money(Math.Round(valuationResult.CostPrice.Amount / 100 * rule.AmountOnBooking.Value, 2), valuationResult.CostPrice.CurrencyCode)
                });
            }
            if (rule.AmountOnDaysBeforeCheckIn != null && rule.AmountOnDaysBeforeCheckIn > 0m && rule.DaysBeforeCheckIn != null)
            {
                var paymentDate = valuationRequest.SelectedRooms[0].AvailabilityRequest.CheckInDate.Subtract(TimeSpan.FromDays(rule.DaysBeforeCheckIn.Value));
                paymentRules.Add(new AccommodationProviderValuationResultVccPaymentRule()
                {
                    RoomCode = valuationResult.RoomCode
                    , PaymentDate = paymentDate
                    , Price = new Money(Math.Round(valuationResult.CostPrice.Amount / 100 * rule.AmountOnDaysBeforeCheckIn.Value, 2), valuationResult.CostPrice.CurrencyCode)
                });
            }
            if (rule.AmountOnDaysAfterCheckOut != null && rule.AmountOnDaysAfterCheckOut > 0m && rule.DaysAfterCheckOut != null)
            {
                paymentRules.Add(new AccommodationProviderValuationResultVccPaymentRule()
                {
                    RoomCode = valuationResult.RoomCode
                    , PaymentDate = valuationRequest.SelectedRooms[0].AvailabilityRequest.CheckOutDate.AddDays(rule.DaysAfterCheckOut.Value)
                    , Price = new Money(Math.Round(valuationResult.CostPrice.Amount / 100 * rule.AmountOnDaysAfterCheckOut.Value, 2), valuationResult.CostPrice.CurrencyCode)
                });
            }
            return paymentRules.ToArray();
        }
    }
}
