﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderValuationCancellationPolicyService : IProviderValuationCancellationPolicyService
    {
        private readonly ICancellationPolicyRepository cancellationPolicyRepository;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ProviderValuationCancellationPolicyService(ICancellationPolicyRepository cancellationPolicyRepository, IAccommodationConfigurationManager configurationManager)
        {
            this.cancellationPolicyRepository = cancellationPolicyRepository;
            this.configurationManager = configurationManager;
        }
        
        public async Task<string> CreateProviderCancellationPolicyAsync(AccommodationProviderValuationRequest request, byte providerPolicyId)
        {
            var isB2B = request.Agent != null ? true : false;
            var policy = await this.cancellationPolicyRepository.GetByProviderIdAndProviderPolicyIdAsync(request.Provider.Id, providerPolicyId, isB2B);
            return policy.Message;
        }

        public async Task<string> CreateProviderCancellationPolicyAsync(AccommodationProviderValuationRequest request, byte providerPolicyId, params object[] objects)
        {
            var isB2B = request.Agent != null ? true : false;
            var policy = await this.cancellationPolicyRepository.GetByProviderIdAndProviderPolicyIdAsync(request.Provider.Id, providerPolicyId, isB2B);
            objects.ForEach((i, j) =>
            {
                Money m = i as Money;
                if (m == null) return;
                objects[j] = request.ChannelInfo.CurrencySymbol + m.Amount.ToString("0.00");
            });
            return string.Format(policy.Message, objects);
        }
    }
}
