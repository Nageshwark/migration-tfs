﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderNonRefundableService : IProviderNonRefundableService
    {
        private static readonly string[] NonRefundableStrings =
{
            "non refundable", "non-refundable", "nonrefundable",
            "non-ref", "nonref", "no refund", "none refundable",
            "none-refundable", "not refundable", "not-refundable",
            "( non changeable)", "(non changeable)"
        };

		public bool IsDescriptionNonRefundable(string roomDescription)
        {
            return Match(roomDescription);
        }
		
        public bool IsDescriptionNonRefundable(string roomDescription, bool isNonRefundable)
        {
            return Match(roomDescription, isNonRefundable);
        }

        public bool IsDescriptionNonRefundable(string roomDescription, string boardDescription, bool isNonRefundable)
        {
            return (Match(roomDescription, isNonRefundable) || Match(boardDescription, isNonRefundable));
        }

		private static bool Match(string description)
        {
            return NonRefundableStrings.Any(x => description.IndexOf(x, StringComparison.OrdinalIgnoreCase) > -1);
        }
		
        private static bool Match(string description, bool currentValue)
        {
            return currentValue || NonRefundableStrings.Any(x => description.IndexOf(x, StringComparison.OrdinalIgnoreCase) > -1);
        }
        

    }
}
