﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts.Enums;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderLoggerService : IProviderLoggerService
    {
        public readonly IDiParameterContainer diParameterContainer;

        public ProviderLoggerService(IDiParameterContainer diParameterContainer)
        {
            this.diParameterContainer = diParameterContainer;
        }

        public void SetProviderRequest(AccommodationProviderValuationRequest request, object providerRequest)
        {
            SetProviderRequest(request, "default", providerRequest);
        }

        public void SetProviderResponse(AccommodationProviderValuationRequest request, object providerResponse)
        {
            SetProviderResponse(request, "default", providerResponse);
        }

        public void SetProviderRequest(AccommodationProviderValuationRequest request, string key, object providerRequest)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Request, key, providerRequest));
        }

        public void SetProviderResponse(AccommodationProviderValuationRequest request, string key, object providerResponse)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Response, key, providerResponse));
        }

        public void SetProviderRequest(AccommodationProviderBookingRequest request, object providerRequest)
        {
            SetProviderRequest(request, "default", providerRequest);
        }

        public void SetProviderResponse(AccommodationProviderBookingRequest request, object providerResponse)
        {
            SetProviderResponse(request, "default", providerResponse);
        }

        public void SetProviderRequest(AccommodationProviderBookingRequest request, string key, object providerRequest)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Request, key, providerRequest));
        }

        public void SetProviderResponse(AccommodationProviderBookingRequest request, string key, object providerResponse)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Response, key, providerResponse));
        }

        public void SetProviderRequest(AccommodationProviderCancellationRequest request, object providerRequest)
        {
            SetProviderRequest(request, "default", providerRequest);
        }

        public void SetProviderResponse(AccommodationProviderCancellationRequest request, object providerResponse)
        {
            SetProviderResponse(request, "default", providerResponse);
        }

        public void SetProviderRequest(AccommodationProviderCancellationRequest request, string key, object providerRequest)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Request, key, providerRequest));
        }

        public void SetProviderResponse(AccommodationProviderCancellationRequest request, string key, object providerResponse)
        {
            var requests = this.diParameterContainer.GetParameterValue<ConcurrentQueue<AccommodationProviderRequest>>(request);
            requests.Enqueue(new AccommodationProviderRequest(ProviderRequestType.Response, key, providerResponse));
        }
    }
}
