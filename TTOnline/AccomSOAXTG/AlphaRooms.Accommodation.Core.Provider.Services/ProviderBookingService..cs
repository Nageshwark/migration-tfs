﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Services.Exceptions;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderBookingService : IProviderBookingService
    {
        private readonly IProviderBookingFactory providerFactory;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ProviderBookingService(IProviderBookingFactory providerFactory, ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.providerFactory = providerFactory;
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public async Task<AccommodationProviderBookingResponse> GetProviderBookingResponseAsync(AccommodationProviderBookingRequest bookingRequest)
        {
            // returns a result list containing not attempted results if booking is disabled 
            if (!bookingRequest.Provider.IsBookingEnabled) return CreateNotAttemptedProviderBookingResults(bookingRequest);

            // create booking provider
            var bookingProvider = providerFactory.CreateBookingProvider(bookingRequest.Provider.Name);

            // process live search
            IEnumerable<AccommodationProviderBookingResult> results = null;
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, bookingRequest.BookingId.ToString(), string.Format("(ProviderId = {0})", bookingRequest.Provider.Id), bookingRequest.Debugging, logger))
            {
                var providerTask = Task.Run(async() => results = (await bookingProvider.MakeProviderBookingAsync(bookingRequest)).ToArray());
                await Task.WhenAny(providerTask, Task.Delay(configurationManager.AccommodationBookingResultsExpireTimeout));
                if (providerTask.Status == TaskStatus.WaitingForActivation || providerTask.Status == TaskStatus.Running) throw new ProviderProcessTimeoutException("booking", bookingRequest.Provider);
                if (providerTask.Status != TaskStatus.RanToCompletion) throw new ProviderProcessFailedException("booking", bookingRequest.Provider, providerTask);
            }

            if (bookingRequest.Debugging)
                logger.Debug("Provider {0} returned {1} rooms from booking.", bookingRequest.Provider.Id, results.Count());

            return new AccommodationProviderBookingResponse() { Results = results };
        }
        
        private AccommodationProviderBookingResponse CreateNotAttemptedProviderBookingResults(AccommodationProviderBookingRequest providerRequest)
        {
            return new AccommodationProviderBookingResponse()
            {
                Results = providerRequest.ValuatedRooms.Select(i => new AccommodationProviderBookingResult()
                {
                    RoomNumber = i.RoomNumber
                    , BookingStatus = BookingStatus.NotAttempted
                }).ToArray()
            };
        }
    }
}
