﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Services.Exceptions;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderCancellationService : IProviderCancellationService
    {
        private readonly IProviderCancellationFactory providerFactory;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ProviderCancellationService(IProviderCancellationFactory providerFactory, ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.providerFactory = providerFactory;
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public async Task<AccommodationProviderCancellationResponse> GetProviderCancellationResponseAsync(AccommodationProviderCancellationRequest cancellationRequest)
        {
            // create booking provider
            var cancellationProvider = providerFactory.CreateCancellationProvider(cancellationRequest.Provider.Name);

            // process live search
            IEnumerable<AccommodationProviderCancellationResult> results = null;
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, cancellationRequest.CancellationId.ToString(), string.Format("(ProviderId = {0})", cancellationRequest.Provider.Id), cancellationRequest.Debugging, logger))
            {
                var providerTask = Task.Run(async() => results = (await cancellationProvider.CancelProviderBookingAsync(cancellationRequest)).ToArray());
                await Task.WhenAny(providerTask, Task.Delay(configurationManager.AccommodationCancellationResultsExpireTimeout));
                if (providerTask.Status == TaskStatus.WaitingForActivation || providerTask.Status == TaskStatus.Running) throw new ProviderProcessTimeoutException("cancellation", cancellationRequest.Provider);
                if (providerTask.Status != TaskStatus.RanToCompletion) throw new ProviderProcessFailedException("cancellation", cancellationRequest.Provider, providerTask);
            }

            if (cancellationRequest.Debugging)
                logger.Debug("Provider {0} returned {1} rooms from cancellation.", cancellationRequest.Provider.Id, results.Count());

            return new AccommodationProviderCancellationResponse() { Results = results };
        }
    }
}
