﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderProcessTimeoutException : Exception
    {
        private readonly string processName;
        private readonly AccommodationProvider provider;

        public ProviderProcessTimeoutException(string processName, AccommodationProvider provider)
            : base("Provider " + provider.Name + " " + processName + " process timed out(SearchTimeout=" + provider.SearchTimeout + ").")
        {
            this.processName = processName;
            this.provider = provider;
        }

        public string ProcessName
        {
            get { return this.processName; }
        }

        public AccommodationProvider Provider
        {
            get { return this.provider; }
        }
    }
}
