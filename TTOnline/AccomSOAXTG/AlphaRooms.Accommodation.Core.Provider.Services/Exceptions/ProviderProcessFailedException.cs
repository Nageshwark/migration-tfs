﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services.Exceptions
{
    public class ProviderProcessFailedException : Exception 
    {
        private readonly string processName;
        private readonly AccommodationProvider accommodationProvider;
        private readonly Task providerTask;

        public ProviderProcessFailedException(string processName, AccommodationProvider provider, Task providerTask)
            : base("Provider " + provider.Name + " " + processName + " process failed(Status=" + providerTask.Status.ToString() + ").", providerTask.Exception)
        {
            this.processName = processName;
            this.accommodationProvider = provider;
            this.providerTask = providerTask;
        }

        public string ProcessName 
        {
            get { return this.processName; }
        }

        public AccommodationProvider Provider
        {
            get { return this.Provider; }
        }

        public Task ProviderTask
        {
            get { return this.providerTask; }
        }
    }
}
