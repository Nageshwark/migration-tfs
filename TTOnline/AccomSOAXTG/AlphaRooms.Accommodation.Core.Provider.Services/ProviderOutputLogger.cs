﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System.Xml.Linq;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderOutputLogger : IProviderOutputLogger
    {
        private readonly IProviderOutputLoggerSettings settings;
        private readonly ILogger logger;

        public ProviderOutputLogger(IProviderOutputLoggerSettings settings, ILogger logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        public void LogAvailabilityResponse(AccommodationProviderAvailabilityRequest request, ProviderOutputFormat format, object response)
        {
            this.LogAvailabilityResponse(request, null, format, response);
        }

        public void LogAvailabilityResponse(AccommodationProviderAvailabilityRequest request, string processName, ProviderOutputFormat format, object response)
        {
            if (!string.IsNullOrEmpty(settings.ProviderOutputPath))
            {
                try
                {
                    string availabilityPath = Path.Combine(settings.ProviderOutputPath, request.AvailabilityId.ToString());
                    if (!Directory.Exists(availabilityPath)) Directory.CreateDirectory(availabilityPath);
                    string providerPath = Path.Combine(availabilityPath, request.Provider.Name);
                    if (!Directory.Exists(providerPath)) Directory.CreateDirectory(providerPath);
                    if (request.Provider.EdiCode == "TG" && request.MetaToken != null) 
                    {
                        File.WriteAllText(Path.Combine(providerPath, request.Provider.EdiCode + "_1Availability_P"                 
                       + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response, format));
                    }
                    else
                    {
                        File.WriteAllText(Path.Combine(providerPath, request.Provider.EdiCode + "_1Availability_P"
                        + request.ProcessId.ToString()
                        + "_R" + request.Rooms.Select(i => i.RoomNumber).ToString(',')
                        + "_D" + request.DestinationCodes.ToString(',').Replace('|', '_') + "_" + request.CheckInDate.ToString("ddMMMyyyy")
                        + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response, format));
                    }
                    
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier availability output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        public void LogValuationResponse(AccommodationProviderValuationRequest request, ProviderOutputFormat format, object response)
        {
            this.LogValuationResponse(request, null, format, response);
        }

        public void LogValuationResponse(AccommodationProviderValuationRequest request, string processName, ProviderOutputFormat format, object response)
        {
            if (!string.IsNullOrEmpty(settings.ProviderOutputPath))
            {
                try
                {
                    string availabilityPath = Path.Combine(settings.ProviderOutputPath, request.AvailabilityId.ToString());
                    if (!Directory.Exists(availabilityPath)) Directory.CreateDirectory(availabilityPath);
                    string valuationPath = Path.Combine(availabilityPath, request.ValuationId.ToString());
                    if (!Directory.Exists(valuationPath)) Directory.CreateDirectory(valuationPath);
                    File.WriteAllText(Path.Combine(valuationPath, request.Provider.EdiCode + "_2Valuation_P" 
                        + request.ProcessId.ToString() 
                        + "_R" + request.SelectedRooms.Select(i => i.RoomNumber).ToString(',')
                        + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response, format));
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier valuation output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        public void LogBookingResponse(AccommodationProviderBookingRequest request, ProviderOutputFormat format, object response)
        {
            this.LogBookingResponse(request, null, format, response);
        }

        public void LogBookingResponse(AccommodationProviderBookingRequest request, string processName, ProviderOutputFormat format, object response)
        {
            if (!string.IsNullOrEmpty(settings.ProviderOutputPath))
            {
                try
                {
                    string availabilityPath = Path.Combine(settings.ProviderOutputPath, request.AvailabilityId.ToString());
                    if (!Directory.Exists(availabilityPath)) Directory.CreateDirectory(availabilityPath);
                    string valuationPath = Path.Combine(availabilityPath, request.ValuationId.ToString());
                    if (!Directory.Exists(valuationPath)) Directory.CreateDirectory(valuationPath);
                    File.WriteAllText(Path.Combine(valuationPath, request.Provider.EdiCode + "_3Booking_P" 
                        + request.ProcessId.ToString() 
                        + "_R" + request.ValuatedRooms.Select(i => i.RoomNumber).ToString(',')
                        + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response, format));
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier booking output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        public void LogCancellationResponse(AccommodationProviderCancellationRequest request, ProviderOutputFormat format, object response)
        {
            this.LogCancellationResponse(request, null, format, response);
        }

        public void LogCancellationResponse(AccommodationProviderCancellationRequest request, string processName, ProviderOutputFormat format, object bookingResponseList)
        {
            if (!string.IsNullOrEmpty(settings.ProviderOutputPath))
            {
                try
                {
                   // TODO implement cancellation output
                    //string availabilityPath = Path.Combine(settings.ProviderOutputPath, request..ToString());
                    //if (!Directory.Exists(availabilityPath)) Directory.CreateDirectory(availabilityPath);
                    //string valuationPath = Path.Combine(availabilityPath, request.ValuationId.ToString());
                    //if (!Directory.Exists(valuationPath)) Directory.CreateDirectory(valuationPath);
                    //File.WriteAllText(Path.Combine(valuationPath, request.Provider.EdiCode + "_3Booking_P"
                    //    + request.ProcessId.ToString()
                    //    + "_R" + request.ValuatedRooms.Select(i => i.RoomNumber).ToString(',')
                    //    + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response, format));
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier cancellation output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        private string ObjectToString(object response, ProviderOutputFormat format)
        {
            WebScrapeResponse webReponse = response as WebScrapeResponse;
            if (webReponse != null) response = webReponse.Value;
            string responseString = response as string;
            if (responseString != null)
            {
                switch (format)
                {
                    case ProviderOutputFormat.Xml:
                        return responseString.ToIndentedXml();
                    case ProviderOutputFormat.Json:
                        return responseString.ToIndentedJson();
                    case ProviderOutputFormat.Html:
                        return responseString;
                    default:
                        return responseString;
                }
            }
            return response.XmlSerializeFormatted(response.GetType());
        }
    }
}
