﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Net;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderProxyService : IProviderProxyService
    {
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        public void SetProxy<TChannel>(ClientBase<TChannel> client, List<AccommodationProviderParameter> providerParameters) where TChannel : class
        {
            bool useProxy = false;

            if (providerParameters.Exists(a => a.ParameterName == UseProxy))
            {
                useProxy = Convert.ToBoolean(providerParameters.GetParameterValue(UseProxy));
            }

            if (!useProxy)
                return;

            var binding = client.Endpoint.Binding as BasicHttpBinding;

            if (binding == null)
            {
                System.Diagnostics.Debug.WriteLine("Binding of this endpoint is not BasicHttpBinding");
                return;
            }

            string proxyUrl = providerParameters.GetParameterValue(ProxyUrl);
            int proxyPort = Convert.ToInt32(providerParameters.GetParameterValue(ProxyPort));
            string proxyUsername = providerParameters.GetParameterValue(ProxyUsername);
            string proxyPassword = providerParameters.GetParameterValue(ProxyPassword);

            binding.ProxyAddress = new Uri(string.Format("http://{0}:{1}", proxyUrl, proxyPort));
            binding.UseDefaultWebProxy = false;
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.Basic;

            if (client.ClientCredentials == null) return;
            client.ClientCredentials.UserName.UserName = proxyUsername;
            client.ClientCredentials.UserName.Password = proxyPassword;
        }

        public WebProxy SetProxy(List<AccommodationProviderParameter> providerParameters)
        {
            WebProxy proxy = null;

            try
            {
                bool useProxy = false;

                if (providerParameters.Exists(a => a.ParameterName == UseProxy))
                {
                    useProxy = Convert.ToBoolean(providerParameters.GetParameterValue(UseProxy));

                    if (useProxy)
                    {
                        string proxyUrl = providerParameters.GetParameterValue(ProxyUrl);
                        int proxyPort = Convert.ToInt32(providerParameters.GetParameterValue(ProxyPort));
                        string proxyUsername = providerParameters.GetParameterValue(ProxyUsername);
                        string proxyPassword = providerParameters.GetParameterValue(ProxyPassword);


                        proxy = new WebProxy(proxyUrl, proxyPort);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    }

                }

            }
            catch //Suppress any exception if no Proxy Details was not defined (default behaviour)
            {
            }

            return proxy;

        }
    }
}
