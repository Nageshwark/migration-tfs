﻿namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ProviderAvailabilityFilterService : IProviderAvailabilityFilterService
    {
        private readonly IProviderAvailabilityFilterProcessor[] filterProcessors;

        public ProviderAvailabilityFilterService(IEnumerable<IProviderAvailabilityFilterProcessor> filterProcessors)
        {
            this.filterProcessors = filterProcessors.ToArray();
        }

        public async Task FilterAvailabilityAsync(List<AccommodationProviderAvailabilityResult> availabilityResults, AccommodationProviderAvailabilityRequest availabilityRequest)
        {
            // Init the filter, some have DB collections
            await Async.ParallelForEach(this.filterProcessors, async (i) => await i.InitializeAsync());

            // filter results based on the filters, go backwards to remove from the same collection we are iterating
            for (int i = availabilityResults.Count - 1; i >= 0; i--)
            {
                // default to do not remove
                bool remove = false;

                for (int j = 0; j < this.filterProcessors.Length; j++)
                {
                    // sets remove to true if filter want to remvoe row
                    remove |= !this.filterProcessors[j].CanProcess(availabilityRequest, availabilityResults[i]);
                }

                if (remove)
                {
                    availabilityResults.RemoveAt(i);
                }
            }
        }
    }
}