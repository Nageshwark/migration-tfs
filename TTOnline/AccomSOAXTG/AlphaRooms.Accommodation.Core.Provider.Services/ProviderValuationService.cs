﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Services.Exceptions;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderValuationService : IProviderValuationService
    {
        private readonly IProviderValuationFactory providerFactory;
        private readonly ILogger logger;
        private readonly IProviderValuationVirtualCreditCardRuleService virtualCreditCardRuleService;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ProviderValuationService(IProviderValuationFactory providerFactory, ILogger logger, IProviderValuationCancellationPolicyService cancellationPolicyService
            , IProviderValuationVirtualCreditCardRuleService virtualCreditCardRuleService, IAccommodationConfigurationManager configurationManager)
        {
            this.providerFactory = providerFactory;
            this.logger = logger;
            this.virtualCreditCardRuleService = virtualCreditCardRuleService;
            this.configurationManager = configurationManager;
        }

        public async Task<AccommodationProviderValuationResponse> GetProviderValuationResponseAsync(AccommodationProviderValuationRequest valuationRequest)
        {
            // create valuation provider
            var valuationProvider = providerFactory.CreateValuationProvider(valuationRequest.Provider.Name);

            // process live search
            IList<AccommodationProviderValuationResult> results = null;
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Valuation, valuationRequest.ValuationId.ToString(), string.Format("(ProviderId = {0})", valuationRequest.Provider.Id), valuationRequest.Debugging, logger))
            {
                var providerTask = Task.Run(async() => results = (await valuationProvider.GetProviderValuationAsync(valuationRequest)).ToArray());
                await Task.WhenAny(providerTask, Task.Delay(configurationManager.AccommodationValuationResultsExpireTimeout));
                if (providerTask.Status == TaskStatus.WaitingForActivation || providerTask.Status == TaskStatus.Running) throw new ProviderProcessTimeoutException("valuation", valuationRequest.Provider);
                if (providerTask.Status != TaskStatus.RanToCompletion) throw new ProviderProcessFailedException("valuation", valuationRequest.Provider, providerTask);
            }

            if (valuationRequest.Debugging)
                logger.Debug("Provider {0} returned {1} rooms from valuation.", valuationRequest.Provider.Id, results.Count());

            results = await this.virtualCreditCardRuleService.ApplyVirtualCreditCardRulesAsync(valuationRequest, results);

            return new AccommodationProviderValuationResponse() { Results = results };
        }
    }
}
