﻿using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderOutputLoggerSettings : IProviderOutputLoggerSettings
    {
        public string ProviderOutputPath { get; set; }
    }
}
