﻿namespace AlphaRooms.Accommodation.Core.Provider.Services
{
	using AlphaRooms.Accommodation.Core.Provider.Contracts;
	using AlphaRooms.Accommodation.Core.Provider.Interfaces;
	using AlphaRooms.Accommodation.Core.Provider.Services.Exceptions;
	using AlphaRooms.Accommodation.Core.Services;
	using AlphaRooms.SOACommon.DomainModels.Enumerators;
	using AlphaRooms.SOACommon.Services;
	using Core.Contracts;
	using Core.Interfaces;
	using Ninject.Extensions.Logging;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using System.Threading.Tasks;

	public class ProviderAvailabilityService : IProviderAvailabilityService
    {
        private readonly ILogger logger;
        private readonly IProviderAvailabilityFactory providerFactory;
        private readonly IProviderAvailabilityFilterService filterService;
		private readonly IB2CEventLogInfo b2cEventLogInfo;

		public ProviderAvailabilityService(ILogger logger, IProviderAvailabilityFactory providerFactory, IProviderAvailabilityFilterService filterService, IB2CEventLogInfo b2cEventLogInfo)
        {
            this.logger = logger;
            this.providerFactory = providerFactory;
            this.filterService = filterService;
			this.b2cEventLogInfo = b2cEventLogInfo;
		}

        public async Task<AccommodationProviderAvailabilityResponse> GetProviderAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest availabilityRequest)
        {
            // create availability provider
            var availabilityProvider = this.providerFactory.CreateAvailabilityProvider(availabilityRequest.Provider.Name);

            // process live search
            List<AccommodationProviderAvailabilityResult> results = null;
			Stopwatch stopwatch = new Stopwatch();

			using (new PerformanceAnalyser(
                SupplierTypes.Accommodation,
                PerformanceTimerType.SupplierSearching,
                availabilityRequest.AvailabilityId.ToString(),
                $"(ProviderId = {availabilityRequest.Provider.Id})",
                availabilityRequest.Debugging,
                logger))
            {
				stopwatch.Start();
				var providerTask = Task.Run(async () => results = (await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest)).ToList());
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: process live search, SearchID: " + availabilityRequest.AvailabilityId +",ProviderID: " + availabilityRequest.Provider.Id + ", Duration: " + stopwatch.ElapsedMilliseconds);

				await Task.WhenAny(providerTask, Task.Delay(availabilityRequest.Provider.SearchTimeout));

                if (providerTask.Status == TaskStatus.WaitingForActivation || providerTask.Status == TaskStatus.Running)
                {
                    throw new ProviderProcessTimeoutException("availability", availabilityRequest.Provider);
                }

                if (providerTask.Status != TaskStatus.RanToCompletion)
                {
                    throw new ProviderProcessFailedException("availability", availabilityRequest.Provider, providerTask);
                }
            }

            // process filter 
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_ProviderAvailability_PostProcessing_ProcessFilter");
            await this.filterService.FilterAvailabilityAsync(results, availabilityRequest);
            timer.Record();

            if (availabilityRequest.Debugging)
            {
                logger.Debug("Provider {0} returned {1} rooms from a live search.", availabilityRequest.Provider.Id, results.Count);
            }

            return new AccommodationProviderAvailabilityResponse() { Results = results };
        }
	}
}
