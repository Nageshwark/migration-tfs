﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Services
{
    public class ProviderSpecialRequestService : IProviderSpecialRequestService
    {
        public string GetSpecialRequestString(AccommodationProviderBookingRequestRoomSpecialRequest specialRequest)
        {
            StringBuilder sb = new StringBuilder();
            int j = 1;

            if (specialRequest.AdjoiningRooms ||
                specialRequest.CotRequired ||
                specialRequest.DisabledAccess ||
                specialRequest.LateArrival ||
                specialRequest.NonSmoking ||
                specialRequest.SeaViews ||
                !string.IsNullOrWhiteSpace(specialRequest.OtherRequests))
            {
                sb.AppendLine("Guest has requested:");

                if (specialRequest.AdjoiningRooms)
                {
                    sb.AppendLine(j + ": Adjoining Rooms.");
                    j++;
                }

                if (specialRequest.CotRequired)
                {
                    sb.AppendLine(j + ": Infant Cot.");
                    j++;
                }

                if (specialRequest.DisabledAccess)
                {
                    sb.AppendLine(j + ": Disabled Friendly Room.");
                    j++;
                }

                if (specialRequest.LateArrival)
                {
                    sb.AppendLine(j + ": Late Arrival.");
                    j++;
                }

                if (specialRequest.NonSmoking)
                {
                    sb.AppendLine(j + ": Non Smoking Room.");
                    j++;
                }

                if (specialRequest.SeaViews)
                {
                    sb.AppendLine(j + ": Room with a Sea View");
                    j++;
                }

                if (!string.IsNullOrWhiteSpace(specialRequest.OtherRequests))
                {
                    sb.AppendLine(j + ": " + specialRequest.OtherRequests);
                    j++;
                }
            }
            return sb.ToString();
        }
    }
}
