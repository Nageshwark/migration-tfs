﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.DbContexts
{
    public partial class ContentContext : DbContext
    {
        static ContentContext()
        {
            Database.SetInitializer<ContentContext>(null);
        }

        public ContentContext()
            : base("Name=AlphabedsEntities")
        {

        }
    }
}
