﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.DbContexts.Mapping
{
    public class B2CValuationLogMap : EntityTypeConfiguration<B2CValuationLog>
    {
        public B2CValuationLogMap()
        {
            this.ToTable("AccommodationValuationLogs");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.HostName).HasColumnName("HostName").IsRequired();
            this.Property(t => t.AvailabilityId).HasColumnName("SearchId").IsRequired();
            this.Property(t => t.ValuationId).HasColumnName("ValuationId").IsRequired();
            this.Property(t => t.ValuationDate).HasColumnName("ValuationDate").IsRequired();
            this.Property(t => t.Status).HasColumnName("Status").IsOptional();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsOptional();
            this.Property(t => t.ProviderRoomNumbers).HasColumnName("ProviderRoomNumbers").IsOptional();
            this.Property(t => t.RecoveredSelectedRooms).HasColumnName("RecoveredSelectedRooms").IsOptional();            
            this.Property(t => t.ProviderRequests).HasColumnName("ProviderRequests").IsOptional();
            this.Property(t => t.ProviderResponses).HasColumnName("ProviderResponses").IsOptional();
            this.Property(t => t.ProviderStatus).HasColumnName("ProviderStatus").IsOptional();
            this.Property(t => t.ProviderTimeTaken).HasColumnName("ProviderTimeTaken").IsOptional();
            this.Property(t => t.ProviderException).HasColumnName("ProviderException").IsOptional();
            this.Property(t => t.ResultsCount).HasColumnName("ResultsCount").IsOptional();
            this.Property(t => t.TimeTaken).HasColumnName("TimeTaken").IsOptional();
            this.Property(t => t.Exceptions).HasColumnName("Exceptions").IsOptional();
        }
    }
}
