﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using B2BBookingService = AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.B2B.DomainModels;

namespace AlphaRooms.Accommodation.B2B.Services
{
    public class B2BItineraryBookingService : IB2BItineraryBookingService
    {
        private readonly IB2BItineraryBookingFactory itineraryBookingFactory;
        private readonly IB2BItineraryBookingMapping itineraryBookingMapping;
        private readonly IEndpointConfiguration endpointConfiguration;

        public B2BItineraryBookingService(IB2BItineraryBookingFactory itineraryBookingFactory, IB2BItineraryBookingMapping itineraryBookingMapping, IEndpointConfiguration endpointConfiguration)
        {
            this.itineraryBookingFactory = itineraryBookingFactory;
            this.itineraryBookingMapping = itineraryBookingMapping;
            this.endpointConfiguration = endpointConfiguration;
        }
        //Signature changed added valuatedRooms to get SpecialRequests if any - 14th June 2016
        public async Task<Itinerary> CreateItineraryAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms)
        {

            var client = this.GetClientFromEndpointAddress();
            var request = await itineraryBookingFactory.CreateItineraryRequestAsync(agentId, channel, valuationId, valuationResult, customer, valuatedRooms);


            var response = await client.CreateItineraryAsync(request);
            return itineraryBookingMapping.MapToItinerary(response);
        }

        public async Task UpdateItineraryAsync(Itinerary itinerary, AccommodationBookingResult bookingResult)
        {
            var endpoint = this.endpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new B2BBookingService.AlphaBookingServiceClient("*")
                : new B2BBookingService.AlphaBookingServiceClient("*", endpoint);
            foreach (var bookingRoomResult in bookingResult.Rooms)
            {
                var accommodationBookingRoom = itinerary.AccommodationBookingRooms.First(i => i.RoomNumber == bookingRoomResult.RoomNumber);
                var request = itineraryBookingFactory.CreateUpdateBookingItemRequest(itinerary, accommodationBookingRoom, bookingResult, bookingRoomResult);
                var response = await client.UpdateBookingItemAsync(request);
            }
            var updateBookingResponse = await client.UpdateBookingStatusesForItineraryAsync(itinerary.ItineraryReference);
            var updateItineraryRequest = itineraryBookingFactory.CreateDetermineAndUpdateItineraryRequest(itinerary);
            var updateItineraryResponse = await client.DetermineAndUpdateItineraryStatusAsync(updateItineraryRequest);

            var email = await client.SendConfirmationEmailAsync(itinerary.ItineraryReference, default(int));

        }


        private B2BBookingService.AlphaBookingServiceClient GetClientFromEndpointAddress()
        {
            var endpoint = this.endpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new B2BBookingService.AlphaBookingServiceClient("*")
                : new B2BBookingService.AlphaBookingServiceClient("*", endpoint);
            return client;

        }
    }
}
