﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2B.DomainModels;
using AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;

namespace AlphaRooms.Accommodation.B2B.Services
{
    public class B2BItineraryBookingMapping : IB2BItineraryBookingMapping
    {
        public Itinerary MapToItinerary(CreateItineraryResponse response)
        {
            var booking =
                response.ItineraryBookings.Where(b => b.BookingType == BookingType.Accommodation)?.FirstOrDefault();
            if (!booking.HasValue) throw new Exception("No valid Itinerary booking id found");
           
            return new Itinerary()
            {
                ItineraryId = booking.Value.BookingId  // this is misleading, its actually the invoice id being passed to Bev5 for logs - ACS-1100
                , ItineraryReference = response.ItineraryReference
                , AccommodationBookingRooms = response.AccommodationBookingRoomsMap.Select(CreateAccommodationBookingRoom).ToArray()
            };
        }

        private AccommodationBookingRoom CreateAccommodationBookingRoom(CreateItineraryBookingMap accommodationBookingRoom)
        {
            return new AccommodationBookingRoom()
            {
                RoomNumber = accommodationBookingRoom.RoomNumber
                , AccommodationBookingItemId = accommodationBookingRoom.BookingItemId
            };
        }
    }
}
