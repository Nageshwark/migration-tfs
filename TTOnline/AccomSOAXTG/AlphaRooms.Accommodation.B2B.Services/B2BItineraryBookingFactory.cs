﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2BBookingService = AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.B2B.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2B.Services
{
    public class B2BItineraryBookingFactory : IB2BItineraryBookingFactory
    {
        private readonly IBoardRepository boardTypeRepository;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public B2BItineraryBookingFactory(IBoardRepository boardTypeRepository, IEstablishmentDetailsRepository establishmentDetailsRepository)
        {
            this.boardTypeRepository = boardTypeRepository;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<B2BBookingService.CreateItineraryRequest> CreateItineraryRequestAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms)
        {
            var establishmentDetailsDic = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(valuationResult.Rooms[0].BaseDestinationId, channel);
            var establishmentDetails = establishmentDetailsDic[valuationResult.Rooms[0].EstablishmentId];
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            return new B2BBookingService.CreateItineraryRequest()
            {
                AgentId = agentId
                ,
                Channel = ToB2BBookingChannel(channel)
                ,
                Rooms = valuationResult.Rooms.Select((i, index) => CreateItineraryBookingRoom(valuationId, i, establishmentDetails, boardTypes, valuatedRooms.FirstOrDefault(v => v.ValuatedRoomId == i.RoomId))).ToArray()
                ,
                Travellers = new[] { CreateB2BBookingPerson(customer) }
            };
        }

        private B2BBookingService.Channel ToB2BBookingChannel(Channel channel)
        {
            return (B2BBookingService.Channel)channel;
        }

        private B2BBookingService.BookingRoom CreateItineraryBookingRoom(Guid valuationId, AccommodationValuationResultRoom valuationResultRoom
            , EstablishmentDetails establishmentDetails, Dictionary<BoardType, Board> boardTypes, AccommodationBookingRequestRoom accommodationBookingRequestRoom)
        {
            return new B2BBookingService.BookingRoom()
            {
                ValuationId = valuationId
                ,
                Room = CreateItineraryBookingRoomRoom(valuationResultRoom, establishmentDetails, boardTypes),
                Preferences = new B2BBookingService.RoomPreference()
                {
                    AdjoiningRooms = accommodationBookingRequestRoom?.SpecialRequests?.AdjoiningRooms ?? false,
                    LateArrival = accommodationBookingRequestRoom?.SpecialRequests?.LateArrival ?? false,
                    NonSmoking = accommodationBookingRequestRoom?.SpecialRequests?.NonSmoking ?? false,
                    SeaViews = accommodationBookingRequestRoom?.SpecialRequests?.SeaViews ?? false,
                    OtherRequests = accommodationBookingRequestRoom?.SpecialRequests?.OtherRequests ?? string.Empty,
                    Cot = accommodationBookingRequestRoom?.SpecialRequests?.CotRequired ?? false,
                    DisabledFriendly = accommodationBookingRequestRoom?.SpecialRequests?.SeaViews ?? false
                } //FOR SENDING PREFERENCES IN BOOKING SERVICE
            };
        }

        private B2BBookingService.ValuationRoom CreateItineraryBookingRoomRoom(AccommodationValuationResultRoom valuationResultRoom, EstablishmentDetails establishmentDetails
            , Dictionary<BoardType, Board> boards)
        {

            var agentCommissionRate = valuationResultRoom.SpecificData.GetValueOrDefault("AgentCommissionRate");
            var cancellationPolicy = valuationResultRoom.CancellationPolicies.Select(i => "<p>" + i + "</p>").ToString("");
            return new B2BBookingService.ValuationRoom()
            {
                RoomId = valuationResultRoom.RoomNumber
                ,
                RoomId2 = valuationResultRoom.RoomId
                ,
                RoomCode = valuationResultRoom.ProviderResult.RoomCode
                ,
                Supplier = valuationResultRoom.ProviderResult.ProviderName ?? valuationResultRoom.Provider.Name
                ,
                SupplierEdiCode = valuationResultRoom.ProviderResult.ProviderProviderCode ?? valuationResultRoom.Provider.EdiCode
                ,
                DestinationId = valuationResultRoom.DestinationId
                ,
                Destination = establishmentDetails.DestinationName
                ,
                EstablishmentKey = valuationResultRoom.EstablishmentId
                ,
                EstablishmentName = establishmentDetails.EstablishmentName
                ,
                EstablishmentEdiCode = valuationResultRoom.ProviderResult.ProviderEstablishmentCode ?? valuationResultRoom.ProviderResult.EstablishmentEdiCode
                ,
                FromDate = valuationResultRoom.ProviderResult.CheckInDate
                ,
                ToDate = valuationResultRoom.ProviderResult.CheckOutDate
                ,
                NumberOfAdults = valuationResultRoom.AdultsCount
                ,
                NumberOfChildren = valuationResultRoom.ChildrenCount + valuationResultRoom.InfantsCount
                ,
                RoomNumber = valuationResultRoom.RoomNumber
                ,
                RoomDescription = valuationResultRoom.RoomDescription ?? ""
                ,
                BoardCode = valuationResultRoom.BoardType.ToString()
                ,
                BoardType = this.ToBoardType(valuationResultRoom.BoardType)
                ,
                BoardDescription = boards[valuationResultRoom.BoardType].Name

                //, IsSpecialRate = result.IsSpecialRate
                ,
                PriceElements = this.CreateAccommodationPriceElements(valuationResultRoom)
                ,
                PaymentModel = this.ToPaymentModel(valuationResultRoom.ProviderResult.PaymentModel)
                ,
                IsBindingRate = valuationResultRoom.ProviderResult.IsBindingRate
                ,
                IsOpaqueRate = valuationResultRoom.ProviderResult.IsOpaqueRate
                ,
                IsNonRefundable = valuationResultRoom.ProviderResult.IsNonRefundable
                ,
                RateType = this.ToRateType(valuationResultRoom.ProviderResult.RateType)
                ,
                Margin = valuationResultRoom.Margin
                ,
                BookingConditions = this.CreateBookingConditions(cancellationPolicy)
                ,
                VccPaymentTerms = this.CreateVccPaymentTerms(valuationResultRoom.ProviderResult.VirtualCreditCardPaymentTerms)
                ,
                AgentCommissionRate = (!string.IsNullOrEmpty(agentCommissionRate) ? decimal.Parse(agentCommissionRate) : (decimal?)null)
            };
        }

        private B2BBookingService.VccPaymentTerm[] CreateVccPaymentTerms(AccommodationProviderValuationResultVccPaymentRule[] vccPaymentRules)
        {
            if (!vccPaymentRules.Any()) return new B2BBookingService.VccPaymentTerm[0];
            return vccPaymentRules.Select(CreateVccPaymentTerm).ToArray();
        }

        private B2BBookingService.VccPaymentTerm CreateVccPaymentTerm(AccommodationProviderValuationResultVccPaymentRule i)
        {
            return new B2BBookingService.VccPaymentTerm()
            {
                PaymentDate = i.PaymentDate
                ,
                AmountToPay = ToMoney(i.Price)
            };
        }

        private B2BBookingService.AccommodationBookingConditions CreateBookingConditions(string cancellationPolicy)
        {
            if (string.IsNullOrEmpty(cancellationPolicy)) return new B2BBookingService.AccommodationBookingConditions();
            return new B2BBookingService.AccommodationBookingConditions()
            {
                Descriptions = new Dictionary<B2BBookingService.BookingConditionType, string[]>
                { { B2BBookingService.BookingConditionType.Cancellation, new string[] { cancellationPolicy } } }
            };
        }

        private B2BBookingService.PriceElement[] CreateAccommodationPriceElements(AccommodationValuationResultRoom valuationResultRoom)
        {
            return valuationResultRoom.PriceBreakdown.Select(i => CreatePriceElement(valuationResultRoom, i)).ToArray();
        }

        private B2BBookingService.PriceElement CreatePriceElement(AccommodationValuationResultRoom result, AccommodationResultPriceItem priceBreakDown)
        {
            switch (priceBreakDown.Type)
            {
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.SellingPrice:
                    return CreatePriceElement(result, priceBreakDown, "Selling Price", B2BBookingService.BookingElementCategory.Fare);
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.Vat:
                    return CreatePriceElement(result, priceBreakDown, "VAT On Commission", B2BBookingService.BookingElementCategory.VatOnCommission);
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.AgentCommission:
                    return CreatePriceElement(result, priceBreakDown, "Commission", B2BBookingService.BookingElementCategory.AgentCommission);

                default:
                    throw new Exception("Unexcepted price breakdown type " + priceBreakDown.Type.ToString());
            }
        }

        private B2BBookingService.PriceElement CreatePriceElement(AccommodationValuationResultRoom result, AccommodationResultPriceItem priceBreakDown, string elementDescription
            , B2BBookingService.BookingElementCategory elementCategory)
        {
            return new B2BBookingService.PriceElement()
            {
                InternalDescription = elementDescription
                ,
                Category = elementCategory
                ,
                CostFromSupplier = this.ToMoney(priceBreakDown.CostPrice)
                ,
                CostToCustomer = (elementCategory == B2BBookingService.BookingElementCategory.Fare) ? this.ToMoney(priceBreakDown.SalePrice) : this.ToMoney(priceBreakDown.SalePrice, true)
                ,
                PaymentToTake = (elementCategory == B2BBookingService.BookingElementCategory.Fare) ? this.ToMoney(priceBreakDown.SalePrice) : this.ToMoney(priceBreakDown.SalePrice, true)
                ,
                ExchangeRate = result.ExchangeRate
                ,
                IsBindingRate = result.ProviderResult.IsBindingRate
                ,
                IsCardChargeApplicable = (result.ProviderResult.PaymentModel != PaymentModel.CustomerPayDirect)
                ,
                Margin = result.Margin
            };
        }

        private B2BBookingService.Person CreateB2BBookingPerson(AccommodationBookingRequestCustomer customer)
        {
            return new B2BBookingService.Person()
            {
                Id = customer.Id
                ,
                Title = customer.TitleString
                ,
                FirstName = customer.FirstName
                ,
                Surname = customer.Surname
                ,
                ContactNumber = customer.ContactNumber
                ,
                AgeAtTimeOfTravel = customer.Age
            };
        }

        private B2BBookingService.Money ToMoney(Money price, bool isNegative = false)
        {
            return new B2BBookingService.Money() { Value = isNegative ? (-price.Amount) : price.Amount, Currency = price.CurrencyCode };
        }

        private B2BBookingService.BoardType ToBoardType(BoardType boardType)
        {
            return (B2BBookingService.BoardType)boardType;
        }

        private B2BBookingService.PaymentModel ToPaymentModel(PaymentModel paymentModel)
        {
            return (B2BBookingService.PaymentModel)paymentModel;
        }

        private B2BBookingService.RateTypes ToRateType(RateType rateType)
        {
            return (B2BBookingService.RateTypes)rateType;
        }

        public B2BBookingService.UpdateBookingItemRequest CreateUpdateBookingItemRequest(Itinerary itinerary, AccommodationBookingRoom accommodationBookingRoom
            , AccommodationBookingResult bookingResult, AccommodationBookingResultRoom bookingRoomResult)
        {
            var isSuccessful = (bookingRoomResult.ProviderBookingResult.BookingStatus == BookingStatus.Confirmed);
            return new B2BBookingService.UpdateBookingItemRequest()
            {
                AccommodationBookingItemId = accommodationBookingRoom.AccommodationBookingItemId
                ,
                BookedRoom = new B2BBookingService.BookedRoom()
                {
                    IsSuccessful = isSuccessful
                    ,
                    SupplierBookingReference = bookingRoomResult.ProviderBookingResult.ProviderBookingReference
                    ,
                    ErrorType = (isSuccessful ? B2BBookingService.BookingErrorTypes.NoBookingError : B2BBookingService.BookingErrorTypes.SupplierResponse)
                    ,
                    Comment = (isSuccessful ? bookingRoomResult.ProviderBookingResult.Message : null)
                    ,
                    ErrorText = (!isSuccessful ? bookingRoomResult.ProviderBookingResult.Message : null)
                }
            };
        }

        public B2BBookingService.DetermineItineraryStatusRequest CreateDetermineAndUpdateItineraryRequest(Itinerary itinerary)
        {
            return new B2BBookingService.DetermineItineraryStatusRequest()
            {
                ItineraryReference = itinerary.ItineraryReference
                ,
                IsBalancePayment = false
            };
        }
    }
}
