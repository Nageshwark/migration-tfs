﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2B.Services
{
    public class B2BFlashSaleMappingFactory : IB2BFlashSaleMappingFactory
    {
        public Dictionary<int, Guid[]> GetAgentEstablishmentMappings(AccommodationB2BFlashSaleEstablishment[] flashSaleEstablishments, bool includeInActive = false)
        {
            return flashSaleEstablishments
                .Where(x => includeInActive || x.IsActive)
                .GroupBy(k => k.B2BAgentId)
                .ToDictionary(
                    g => g.Key,
                    g => g.Select(t => t.EstablishmentId).ToArray());
        }

        public Dictionary<int, Guid[]> GetActiveAgentEstablishmentMappings(AccommodationB2BFlashSaleEstablishment[] flashSaleEstablishments)
        {
            return GetAgentEstablishmentMappings(flashSaleEstablishments);
        }
    }
}