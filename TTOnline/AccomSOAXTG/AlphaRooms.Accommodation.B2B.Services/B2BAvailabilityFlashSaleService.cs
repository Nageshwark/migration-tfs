﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.B2B.Services
{
    public class B2BAvailabilityFlashSaleService : IAvailabilityFlashSaleService
    {
        private readonly IAccommodationFlashSaleRepository accommodationFlashSaleRepository;
        private readonly IB2BFlashSaleMappingFactory flashSaleMappingFactory;
        private Dictionary<int, Guid[]> agentEstablishmentMappings = null;

        public B2BAvailabilityFlashSaleService(IAccommodationFlashSaleRepository accommodationFlashSaleRepository, IB2BFlashSaleMappingFactory flashSaleMappingFactory)
        {
            this.accommodationFlashSaleRepository = accommodationFlashSaleRepository;
            this.flashSaleMappingFactory = flashSaleMappingFactory;
        }

        public async Task<IList<AccommodationAvailabilityResult>> FilterResultsForFlashSaleAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            // cache the mappings if not already cached
            if (agentEstablishmentMappings == null) await PopulateEstablishmentMappingsAsync();

            // if there are no mappings, return everything
            if (agentEstablishmentMappings == null) return results;

            //get all establishment ids
            var establishmentIds = results.Select(x => x.EstablishmentId).ToArray();

            //get excluded establishments
            var excludedEstablishments = GetExcludedEstablishments(availabilityRequest.Agent.Id);

            //get allowed establishments derived from excluded
            var allowedEstablishments = establishmentIds.Except(excludedEstablishments).ToArray();

            //return only establishments which are allowed
            return results.AsParallel().Where(r => allowedEstablishments.Any(e => e == r.EstablishmentId)).ToList();
        }

        private Guid[] GetExcludedEstablishments(int agentId)
        {
            if (agentEstablishmentMappings.ContainsKey(agentId))
            {
                //establishments for agent
                var agentEstablishments = agentEstablishmentMappings[agentId];

                //return only establishments which are mapped with other agents but not with current agent
                return
                    agentEstablishmentMappings.Where(x => x.Key != agentId)
                        .SelectMany(x => x.Value)
                        .Distinct()
                        .Except(agentEstablishments)
                        .ToArray();
            }
            
            //return all establishments mapped to any agent
            return agentEstablishmentMappings.SelectMany(x => x.Value).Distinct().ToArray();
        }

        private async Task PopulateEstablishmentMappingsAsync()
        {
            agentEstablishmentMappings = new Dictionary<int, Guid[]>();

            //get all flash sale establishment mappings
            var flashSaleEstablishments = await accommodationFlashSaleRepository.GetAllAsync();

            //build agent mappings for all active mappings
            agentEstablishmentMappings = flashSaleMappingFactory.GetActiveAgentEstablishmentMappings(flashSaleEstablishments);
        }
    }
}