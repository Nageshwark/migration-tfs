﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;

    public interface IRoomKeyGenerator
    {
        void ApplyAvailabilityRoomKey(Channel channel, byte[] guestsAges, AccommodationAvailabilityResult result);

        void ApplyValuationRoomKey(ValuationRoomIdAvailabilityResult valuationRequestRoom);
    }
}
