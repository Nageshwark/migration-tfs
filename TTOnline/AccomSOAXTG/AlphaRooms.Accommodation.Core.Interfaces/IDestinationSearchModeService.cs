﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IDestinationSearchModeService
    {
        Task<DestinationSearchMode[]> GetByDestinationIdAsync(Guid destinationId);
        DestinationSearchMode GetDestinationSearchModeOrNull(DestinationSearchMode[] destinationSearchModes, AccommodationProvider provider);
    }
}
