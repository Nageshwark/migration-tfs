﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    public interface IDepositSchemeV1Service
    {
        Task<FilteredRepositoryResponse<DepositSchemeV1>> GetDepositSchemesAsync(
               Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, bool>> whereClause = null,
               Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, object>> orderBy = null,
               SortDirection? sortDirection = null,
               int? skipCount = null,
               int? takeCount = null);

        Task<DepositSchemeV1> GetDepositSchemeAsync(int id);

        Task<DepositSchemeV1> GetCurrentDepositSchemeAsync();

        Task<bool> AddDepositSchemeAsync(DepositSchemeV1 depositScheme);

        Task<bool> UpdateDepositSchemeAsync(DepositSchemeV1 depositScheme);

        Task<bool> DeleteDepositSchemeAsync(int id);

        DepositSchemeV1 GetCurrentDepositScheme();

        bool IsFlexibleDepositEnabled();
    }
}
