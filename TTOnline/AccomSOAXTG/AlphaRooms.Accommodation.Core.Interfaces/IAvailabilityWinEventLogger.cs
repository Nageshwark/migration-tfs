﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using System.Collections.Generic;

    public interface IAvailabilityWinEventLogger
    {
        void Log(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityProcessParamsBase> processParams);
        void LogWarn(string message);
    }
}
