﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IRoomDescriptionGenerator
    {
        void ApplyAvailabilityRoomDescriptionFormat(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityResult result);
        AccommodationValuationResult ApplyValuationRoomDescriptionFormat(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result);
    }
}
