﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using System;

    public interface ICoreGuidService
    {
        Guid GenerateGuid(bool IsMeta);
        bool IsMetaGuid(Guid? guid);
    }
}