﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IBookingService
    {
        Task<AccommodationBookingResponse> ProcessBookingAsync(AccommodationBookingRequest bookingRequest);

        Task<AccommodationBookingResponse> GetBookingResponseAsync(Guid bookingId);
    }
}
