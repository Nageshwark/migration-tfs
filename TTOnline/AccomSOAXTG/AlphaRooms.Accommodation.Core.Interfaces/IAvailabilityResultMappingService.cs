﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAvailabilityResultMappingService
    {
        Task<IList<AccommodationAvailabilityResult>> MapFromProviderAvailabilityResultsAsync(
            AccommodationAvailabilityLiveProcessParams liveProcessParams,
            AccommodationProviderAvailabilityRequest providerRequest,
            AccommodationProviderAvailabilityResult[] providerResults);
        Task<IList<AccommodationAvailabilityResult>> MapMetaProviderAvailabilityResultsAsync(
           AccommodationAvailabilityLiveProcessParams providerRequest,
           AccommodationProviderAvailabilityResult[] providerResults);
    }
}
