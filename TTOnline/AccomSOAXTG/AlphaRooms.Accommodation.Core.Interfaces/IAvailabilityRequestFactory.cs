﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityRequestFactory
    {
        AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationAvailabilityRequest availabilityRequest, AccommodationProvider provider
           , byte processId, string[] destinationCodes, string[] establishmentCodes, AvailabilityRoomAvailabilityKey[] availabilityRoomAvailabilityKey);

        AccommodationProviderAvailabilityRequest CreateMetaProviderAvailabilityRequest(AccommodationAvailabilityRequest availabilityRequest, AccommodationProvider provider, byte processId);
    }
}
