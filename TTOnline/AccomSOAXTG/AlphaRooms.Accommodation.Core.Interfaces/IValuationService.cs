﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IValuationService
    {
        Task<AccommodationValuationResponse> ProcessValuationAsync(AccommodationValuationRequest valuationRequest, AccommodationAvailabilityResult[] searchResults);

        Task<AccommodationValuationResponse> GetValuationResponseAsync(Guid valuationId);
    }
}
