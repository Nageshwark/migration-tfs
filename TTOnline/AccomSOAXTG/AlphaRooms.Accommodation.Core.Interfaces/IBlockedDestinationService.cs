﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IBlockedDestinationService
    {
        Task<BlockedDestination[]> GetByChannelAndDestinationIdHierarchicalAsync(Channel channel, Guid destinationId);
        bool IsDestinationBlocked(BlockedDestination[] blockedDestinations, Channel channel, AccommodationProvider provider, Guid destinationId);
    }
}
