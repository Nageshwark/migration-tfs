﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IRoomDescriptionComparerFactory
    {
        /// <summary>
        /// Responsible for creating a CachedRoomDescriptionComparer, as it is not threadsafe create a new one on every request.
        /// </summary>
        /// <returns>a new CachedRoomDescriptionComparer</returns>
        IRoomDescriptionComparer Create();
    }
}
