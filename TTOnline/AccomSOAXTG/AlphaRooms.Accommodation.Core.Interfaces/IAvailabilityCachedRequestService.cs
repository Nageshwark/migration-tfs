﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAvailabilityCachedRequestService
    {
        AccommodationAvailabilityCachedRequest[] CreateCacheRequests(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityLiveProcessParams[] successfulLiveProcesses);
        Task<AccommodationAvailabilityCachedRequest[]> GetCachedRequestsOrNullAsync(string[] availabilityKeys, AccommodationProvider[] providers, bool isMeta = false);
        Task SaveRangeAsync(IList<AccommodationAvailabilityCachedRequest> cachedRequests, bool isMeta = false);
    }
}
