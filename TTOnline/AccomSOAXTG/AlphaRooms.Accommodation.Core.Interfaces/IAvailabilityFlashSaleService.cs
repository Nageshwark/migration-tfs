﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityFlashSaleService
    {
        Task<IList<AccommodationAvailabilityResult>> FilterResultsForFlashSaleAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results);
    }
}