﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityProcessParamService
    {
        Task<AccommodationAvailabilityLiveProcessParams[]> CreateAvailabilityLiveProcessParamsAsync(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, AccommodationAvailabilityCachedRequest[] cachedRequests, AccommodationProvider[] providers);

        Task<AccommodationAvailabilityCachedProcessParams> CreateAvailabilityCachedProcessParamsOrNullAsync(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, AccommodationAvailabilityCachedRequest[] cachedRequests);
    }
}
