﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using System.Threading.Tasks;

    public interface IRoomDescriptionComparer
    {
        Task InitializeAsync();
        void ApplyAvailabilityRoomDescriptionKey(AccommodationAvailabilityResult availabilityResult);
        bool EqualsRoomDescription(AccommodationAvailabilityResult result1, AccommodationAvailabilityResult result2);
    }
}
