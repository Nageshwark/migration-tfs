﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface ICancellationResultMappingService
    {
        IList<AccommodationCancellationResultRoom> MapFromProviderCancellationResults(AccommodationCancellationLiveProcessParams liveProcessParams, AccommodationProviderCancellationRequest providerRequest
            , AccommodationProviderCancellationResult[] providerResults);
    }
}
