﻿namespace AlphaRooms.Accommodation.Core.Interfaces.Caching
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAvailabilityCachedRequestCaching
    {
        Task<AccommodationAvailabilityCachedRequest[]> GetByAvailabilityKeyOrNullAsync(string[] availabilityKeys, AccommodationProvider[] providerEdiCodes, bool isMeta = false);

        Task SaveRangeAsync(IList<AccommodationAvailabilityCachedRequest> cachedRequests, bool isMeta = false);
    }
}
