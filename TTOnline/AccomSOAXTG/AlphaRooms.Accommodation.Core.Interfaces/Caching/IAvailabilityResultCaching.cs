﻿namespace AlphaRooms.Accommodation.Core.Interfaces.Caching
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAvailabilityResultCaching
    {
        Task<AccommodationAvailabilityResult[]> GetByAvailabilityRequestIdsAsync(Guid baseDestinationId, Guid[] availabilityRequestIds, AccommodationProvider[] accommodationProvider, bool isMeta = false);

        Task<AccommodationAvailabilityResult[]> GetByRoomIdsAsync(Guid baseDestinationId, Guid[] roomIds);

        Task SaveRangeAsync(Guid baseDestinationId, IList<AccommodationAvailabilityResult> nonCachedResults, bool isMeta = false);

        DateTime GetCacheExpireDateThreshold();
    }
}
