﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces.Caching
{
    public interface IValuationResultCaching
    {
        Task<AccommodationValuationResult> GetByValuationIdOrNullAsync(Guid valuationId);

        Task SaveAsync(AccommodationValuationResult valuationResponse);
    }
}
