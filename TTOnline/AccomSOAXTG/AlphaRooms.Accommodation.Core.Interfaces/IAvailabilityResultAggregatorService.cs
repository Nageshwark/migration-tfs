﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using System.Collections.Generic;

    public interface IAvailabilityResultAggregatorService
    {
        IList<AccommodationAvailabilityResult> AggregateRooms(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, IList<AccommodationAvailabilityResult> results);

        IList<AccommodationAvailabilityResult> AssignRoomNumbers(AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, IList<AccommodationAvailabilityResult> results);
    }
}
