﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityKeyGenerator
    {
        string CreateAvailabilityKey(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityRequestRoom room, AccommodationProvider provider);
    }
}
