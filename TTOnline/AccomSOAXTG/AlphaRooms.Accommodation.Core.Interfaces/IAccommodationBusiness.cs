﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAccommodationBusiness
    {
        void FilterBySupplier(string[] suppliersToSearch, B2BUser agent, AccommodationAvailabilityResponse coreAvailabilityResponse);
        Task<IList<AccommodationAvailabilityResult>> PostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results);
        IList<AccommodationAvailabilityResult> TravelGatePostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results);
    }
}