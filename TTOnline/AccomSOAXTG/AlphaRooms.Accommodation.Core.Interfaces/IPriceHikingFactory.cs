namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Contracts;

    public interface IPriceHikingFactory
    {
        PriceHikeThreshold CreateFakeThreshold();
        PriceHikeDetail CreateEmptyResult();
        PriceHikeDetail CreateEmptyResult(PriceHikeStatus status);
        PriceHikeDetail CreateResult(AccommodationAvailabilityResult availRoom, AccommodationValuationResultRoom valRoom, bool isValid);
    }
}