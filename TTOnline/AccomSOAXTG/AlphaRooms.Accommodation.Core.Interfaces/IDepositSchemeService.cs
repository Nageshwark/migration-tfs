﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;

    public interface IDepositSchemeService
    {
        Task<FilteredRepositoryResponse<DepositScheme>> GetDepositSchemesAsync(
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, bool>> whereClause = null,
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, object>> orderBy = null,
            SortDirection? sortDirection = null,
            int? skipCount = null,
            int? takeCount = null);

        Task<DepositScheme> GetDepositSchemeAsync(int id);

        Task<DepositScheme> GetCurrentDepositSchemeAsync();

        Task<bool> AddDepositSchemeAsync(DepositScheme depositScheme);

        Task<bool> UpdateDepositSchemeAsync(DepositScheme depositScheme);

        Task<bool> DeleteDepositSchemeAsync(int id);

        DepositScheme GetCurrentDepositScheme();

        bool IsFlexibleDepositEnabled();
    }
}