﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityResultRecoveryService
    {
        //Task<AccommodationAvailabilityResult[]> ProcessAvailabilityRecoveryAsync(AccommodationAvailabilityRequest coreAvailabilityRequest, string[] roomIds);
        Task ProcessValuationRecoveryAsync(AccommodationValuationRequest valuationRequest, ValuationRoomIdAvailabilityResult[] valuationRooms);
    }
}
