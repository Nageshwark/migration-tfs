﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces.Repositories
{
    public interface IHotelMarginDiscountRepository
    {
        Task<IEnumerable<HotelMarginDiscount>> GetHotelMarginDiscounts(Guid destinationId, int channel, string ediProvider);
    }
}
