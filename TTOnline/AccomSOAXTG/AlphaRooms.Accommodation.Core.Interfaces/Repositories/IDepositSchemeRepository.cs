﻿namespace AlphaRooms.Accommodation.Core.Interfaces.Repositories
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.DomainModels;

    public interface IDepositSchemeRepository
    {
        Task<FilteredRepositoryResponse<DepositScheme>> GetDepositSchemesAsync<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeQueryable<DepositScheme>;

        Task<DepositScheme> GetCurrentDepositSchemeAsync();

        Task<DepositScheme> GetDepositScheme(int id);

        Task<bool> AddDepositSchemeAsync(DepositScheme depositScheme);

        Task<bool> UpdateDepositSchemeAsync(DepositScheme depositScheme);

        Task<bool> DeleteDepositSchemeAsync(int id);


        FilteredRepositoryResponse<DepositScheme> GetDepositSchemes<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeQueryable<DepositScheme>;

        DepositScheme GetCurrentDepositScheme();
    }
}