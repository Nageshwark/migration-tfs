﻿

namespace AlphaRooms.Accommodation.Core.Interfaces.Repositories
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.DomainModels;
    public interface IDepositSchemeV1Repository
    {
        Task<FilteredRepositoryResponse<DepositSchemeV1>> GetDepositSchemesAsync<TQueryable>(
              Expression<Func<TQueryable, bool>> whereClause,
              Expression<Func<TQueryable, object>> orderBy,
              bool? sortDescending,
              int? skipCount,
              int? takeCount)
              where TQueryable : IDepositSchemeV1Queryable<DepositSchemeV1>;

        Task<DepositSchemeV1> GetCurrentDepositSchemeAsync();

        Task<DepositSchemeV1> GetDepositScheme(int id);

        Task<bool> AddDepositSchemeAsync(DepositSchemeV1 depositScheme);

        Task<bool> UpdateDepositSchemeAsync(DepositSchemeV1 depositScheme);

        Task<bool> DeleteDepositSchemeAsync(int id);


        FilteredRepositoryResponse<DepositSchemeV1> GetDepositSchemes<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeV1Queryable<DepositSchemeV1>;

        DepositSchemeV1 GetCurrentDepositScheme();
    }
}
