﻿namespace AlphaRooms.Accommodation.Core.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.DomainModels;

    public interface IPriceHikeRepository
    {
        Task<List<PriceHikeThreshold>> GetPriceHikeThresholds();
    }
}
