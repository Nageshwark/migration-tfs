﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
	public interface IB2CEventLogInfo
	{
		void LogTrace(string logInfo);
	}
}
