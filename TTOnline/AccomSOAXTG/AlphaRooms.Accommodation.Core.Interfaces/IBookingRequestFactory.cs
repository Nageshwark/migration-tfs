﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IBookingRequestFactory
    {
        AccommodationProviderBookingRequest CreateProvideBookingRequest(AccommodationBookingRequest bookingRequest, AccommodationProvider provider, byte processId, BookingRoomValuationResult[] bookingRequestRooms);
    }
}
