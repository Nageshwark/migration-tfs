﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.Contracts;

    public interface IPriceHikingService
    {
        IList<AccommodationAvailabilityResult> ApplyAvailabilityPriceHiking(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results);

        Task<AccommodationValuationResult> ApplyValuationPriceHiking(ValuationRoomIdAvailabilityResult[] valuationRequestRooms, AccommodationValuationResult result, AccommodationValuationRequest valuationRequest);
    }
}