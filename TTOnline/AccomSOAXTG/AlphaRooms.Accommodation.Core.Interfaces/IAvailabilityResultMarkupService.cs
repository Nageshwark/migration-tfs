﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityResultMarkupService
    {
        Task<IList<AccommodationAvailabilityResult>> ApplyAvailabilityMarkupAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results);
    }
}
