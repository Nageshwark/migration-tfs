﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface ICancellationRequestFactory
    {
        AccommodationProviderCancellationRequest CreateProvideCancellationRequest(AccommodationCancellationRequest cancellationRequest, AccommodationProvider provider, byte processId, CancellationRoomBookingResult[] providerRequestRooms);
    }
}
