﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAgeRuleService
    {
        void ApplyAvailabilityGuestTypeRule(AccommodationProvider provider, AccommodationProviderAvailabilityRequestGuest providerRoomGuest);
        void ApplyBookingGuestTypeRule(AccommodationProvider provider, AccommodationProviderBookingRequestRoomGuest providerGuest);
        void ApplyBookingCustomerTypeRule(AccommodationProvider provider, AccommodationProviderBookingRequestCustomer providerCustomer);
        ProviderAge CalculateProviderAges(AccommodationProvider provider, byte[] guestAges);
    }
}
