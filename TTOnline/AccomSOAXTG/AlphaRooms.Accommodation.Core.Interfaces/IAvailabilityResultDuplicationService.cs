﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IAvailabilityResultDuplicationService
    {
        IList<AccommodationAvailabilityResult> CopyRoomResultsToRoomNumber(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results
            , AccommodationAvailabilityRequestRoom[] toRequestRooms);
        IList<AccommodationAvailabilityResult> CopyRoomResultsToRoomNumber(AccommodationAvailabilityResult availabilityResult, byte[] toRequestRooms);
    }
}
