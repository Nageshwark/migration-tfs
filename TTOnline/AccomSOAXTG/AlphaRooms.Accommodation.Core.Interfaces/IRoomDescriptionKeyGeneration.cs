﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using System.Threading.Tasks;

    public interface IRoomDescriptionKeyGeneration
    {
        string GeneratKey(string roomDescription);
        Task InitializeAsync();
    }
}
