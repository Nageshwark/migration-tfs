﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Interfaces
{
    public interface IValuationRequestFactory
    {
        AccommodationProviderValuationRequest CreateProvideValuationRequest(AccommodationValuationRequest valuationRequest, AccommodationProvider provider, byte processId
            , ValuationRoomIdAvailabilityResult[] valuationRequestRooms);
    }
}
