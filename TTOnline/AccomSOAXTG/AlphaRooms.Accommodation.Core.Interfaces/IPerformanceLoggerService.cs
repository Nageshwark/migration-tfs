﻿
using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.Core.Interfaces
{

    public interface IPerformanceLoggerService<in T> where T : PerformanceLog
    {

        Task ReportAsync(T providerPerformanceLog);

        void Report(T providerPerformanceLog);
        
    }
}