﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using Contracts;

    /// <summary>
    /// Responsible for setting the priority of the providers
    /// </summary>
    public interface IAvailabilityResultProviderPriorityService
    {
        /// <summary>
        /// Provides an int that represents the Priority of the provider that created this room, the higher the int the more priority
        /// </summary>
        /// <param name="room">room to find the provider priority for</param>
        /// <returns>the higher result the more priority</returns>
        int GetPriority(AccommodationAvailabilityResult room);
    }
}
