﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using System.Collections.Generic;

    public interface IAvailabilityResultLowerPriceService
    {
        IList<AccommodationAvailabilityResult> ApplyAvailabilityLowerPrice(IList<AccommodationAvailabilityResult> results);
    }
}
