﻿namespace AlphaRooms.Accommodation.Core.Interfaces
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;

    public interface IAvailabilityResultRoomIdGenerator
    {
        void ApplyAvailabilityRoomId(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] availabilityRequestRooms, AccommodationAvailabilityResult result);
        string CreateRoomId(Channel channel, byte[] guests, AccommodationAvailabilityResult result);
        AccommodationAvailabilityResultRoomIdWrapper Parse(string i);
    }
}
