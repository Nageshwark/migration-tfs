﻿namespace AlphaRooms.Accommodation.B2C.WcfInterface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.Text;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.WcfInterface.Interface;
    using AlphaRooms.Accommodation.B2C.Interfaces;

    public class AccommodationServices : IAccommodationAvailabilityService, IAccommodationValuationService, IAccommodationBookingService
    {
        private readonly IB2CAvailabilityService availabilityService;
        private readonly IB2CValuationService valuationService;
        private readonly IB2CBookingService bookingService;
        private readonly IB2CCancellationService cancellationService;

        public AccommodationServices(IB2CAvailabilityService availabilityService, IB2CValuationService valuationService, IB2CBookingService bookingService, IB2CCancellationService cancellationService)
        {
            this.availabilityService = availabilityService;
            this.valuationService = valuationService;
            this.bookingService = bookingService;
            this.cancellationService = cancellationService;

            SetSecurityProtocolType();
        }

        private void SetSecurityProtocolType()
        {
            ServicePointManager.SecurityProtocol = 0;
            foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
            {
                switch (protocol)
                {
                    case SecurityProtocolType.Ssl3:
                    case SecurityProtocolType.Tls:
                    case SecurityProtocolType.Tls11:
                        break;
                    case SecurityProtocolType.Tls12:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                }
            }
        }

        public async Task<Guid> StartAccommodationAvailabilitySearchAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            return await availabilityService.StartAvailabilitySearchAsync(availabilityRequest);
        }

        public async Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool getFilterDetails)
        {
            return await availabilityService.GetAvailabilityResponseAsync(availabilityId, filterCriteria, sortCriteria, getFilterDetails);
        }

        public async Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityByEstablishmentResponseAsync(Guid availabilityId, Guid establishment, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria)
        {
            return await availabilityService.GetAvailabilityResponseByEstablishmentAsync(availabilityId, establishment, filterCriteria, sortCriteria);
        }

        public async Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityByRoomsResponseAsync(Guid availabilityId, uint[] roomIds)
        {
            return await availabilityService.GetAvailabilityResponseByRoomsAsync(availabilityId, roomIds);
        }

        public async Task<AccommodationB2CAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria)
        {
            return await availabilityService.GetAvailabilityMapResponseAsync(availabilityId, filterCriteria);
        }

        public async Task<AccommodationB2CAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId)
        {
            return await availabilityService.GetAvailabilityMapResponseByEstablishmentAsync(availabilityId, establishmentId);
        }

        public async Task<AccommodationB2CAvailabilityRequest> GetAccommodationAvailabilityRequestAsync(Guid availabilityId)
        {
            return await availabilityService.GetAvailabilityRequestAsync(availabilityId);
        }

        public async Task<AccommodationB2CAvailabilityResultsFilterOptions> GetAccommodationAvailabilityResultsFilterOptionsAsync(Guid availabilityId)
        {
            return await availabilityService.GetAvailabilityResultsFilterOptionsAsync(availabilityId);
        }

        public async Task<Guid> StartAccommodationValuationProcessAsync(AccommodationB2CValuationRequest valuationRequest)
        {
            return await valuationService.StartValuationAsync(valuationRequest);
        }

        public async Task<AccommodationB2CValuationResponse> GetAccommodationValuationResponseAsync(Guid valuationId)
        {
            return await valuationService.GetValuationResponseAsync(valuationId);
        }

        public async Task<Guid> StartAccommodationBookingProcessAsync(AccommodationB2CBookingRequest bookingRequest)
        {
            return await bookingService.StartBookingAsync(bookingRequest);
        }

        public async Task<AccommodationB2CBookingResponse> GetAccommodationBookingResponseAsync(Guid bookingId)
        {
            return await bookingService.GetBookingResponseAsync(bookingId);
        }

        public async Task<Guid> StartAccommodationCancellationProcess(AccommodationB2CCancellationRequest cancellationRequest)
        {
            return await cancellationService.StartCancellationProcessAsync(cancellationRequest);
        }

        public async Task<AccommodationB2CCancellationResponse> GetAccommodationCancellationResponse(Guid cancellationId)
        {
            return await cancellationService.GetCancellationResponseAsync(cancellationId);
        }

        public async Task<AccommodationB2CAvailabilityResponse> GetMetaAccommodationAvailablityResponseAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            return await availabilityService.GetMetaAvailablityResponseAsync(availabilityRequest);
        }
    }
}
