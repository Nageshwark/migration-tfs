using Alpharooms.Accommodation.B2BXml.AuthenticationService;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.Implementation;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.Infrastructure;
using Alpharooms.Accommodation.B2BXml.TicketService.Implementations;
using Alpharooms.Accommodation.B2BXml.TicketService.Infrastructure;
using AlphaRooms.Accommodation.B2B.Services;
using AlphaRooms.Accommodation.B2BWeb.Services;
using AlphaRooms.Accommodation.B2BXml.DbContexts;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Accommodation.B2BXml.Repositories;
using AlphaRooms.Accommodation.B2BXml.Services;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Services;
using AlphaRooms.Utilities.EntityFramework;
using log4net.Config;
using Ninject.Modules;
using AlphaRooms.Configuration;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.B2BXml.DependencyInjection
{


    public class B2BXmlDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            XmlConfigurator.Configure();

            // databases
            Bind<IDbContextActivator<B2BXmlLogDbContext>>().To<NoProxyDbContextActivator<B2BXmlLogDbContext>>().InSingletonScope();

            // repositories
            Bind<IB2BXmlAvailabilityLogRepository>().To<B2BXmlAvailabilityLogRepository>().InSingletonScope();
            Bind<IB2BXmlValuationLogRepository>().To<B2BXmlValuationLogRepository>().InSingletonScope();
            Bind<IB2BXmlBookingLogRepository>().To<B2BXmlBookingLogRepository>().InSingletonScope();

            // shared service
            Bind<IB2BXmlLoginService>().To<B2BXmlLoginService>().InSingletonScope();
            Bind<IB2BXmlErrorService>().To<B2BXmlErrorService>().InSingletonScope();

            Bind<IEndpointConfiguration>().To<EndpointConfiguration>().WhenInjectedInto<B2BItineraryBookingService>().InSingletonScope().WithConstructorArgument("service", "BookingService");
            Bind<IEndpointConfiguration>().To<EndpointConfiguration>().WhenInjectedInto<AuthenticateUserService>().InSingletonScope().WithConstructorArgument("service", "AuthenticationService");
            Bind<IEndpointConfiguration>().To<EndpointConfiguration>().WhenInjectedInto<CancellationTicketService>().InSingletonScope().WithConstructorArgument("service", "TicketAdminService");
            Bind<IAccommodationBusiness>().To<B2BXmlAccommodationBusiness>().InSingletonScope();

            // availability services
            Bind<IB2BXmlAvailabilityRequestFactory>().To<B2BXmlAvailabilityRequestFactory>().InSingletonScope();
            Bind<IB2BXmlAvailabilityRequestValidator>().To<B2BXmlAvailabilityRequestValidator>().InSingletonScope();
            Bind<IB2BXmlAvailabilityResultMappingService>().To<B2BXmlAvailabilityResultMappingService>().InSingletonScope();
            Bind<IB2BXmlAvailabilityService>().To<B2BXmlAvailabilityService>().InSingletonScope();
            Bind<IB2BXmlAvailabilityLogger>().To<B2BXmlAvailabilityLogger>().InSingletonScope();
            Bind<IAvailabilityResultConsolidationService>().To<B2BXmlAvailabilityConsolidationService>().InSingletonScope();
            Bind<IB2BXmlAvailabilityResultFilterService>().To<B2BXmlAvailabilityResultFilterService>().InSingletonScope();
            Bind<IAvailabilityResultLowerPriceService>().To<AvailabilityResultLowerPriceService>().InSingletonScope();

            // valuation services
            Bind<IB2BXmlValuationRequestFactory>().To<B2BXmlValuationRequestFactory>().InSingletonScope();
            Bind<IB2BXmlValuationRequestValidator>().To<B2BXmlValuationRequestValidator>().InSingletonScope();
            Bind<IB2BXmlValuationResultMappingService>().To<B2BXmlValuationResultMappingService>().InSingletonScope();
            Bind<IB2BXmlValuationService>().To<B2BXmlValuationService>().InSingletonScope();
            Bind<IB2BXmlValuationLogger>().To<B2BXmlValuationLogger>().InSingletonScope();

            // booking services
            Bind<IB2BXmlBookingRequestFactory>().To<B2BXmlBookingRequestFactory>().InSingletonScope();
            Bind<IB2BXmlBookingRequestValidator>().To<B2BXmlBookingRequestValidator>().InSingletonScope();
            Bind<IB2BXmlBookingResultMappingService>().To<B2BXmlBookingResultMappingService>().InSingletonScope();
            Bind<IB2BXmlBookingService>().To<B2BXmlBookingService>().InSingletonScope();
            Bind<IB2BXmlBookingLogger>().To<B2BXmlBookingLogger>().InSingletonScope();

            // cancellation services
            Bind<IB2BXmlCancellationRequestFactory>().To<B2BXmlCancellationRequestFactory>().InSingletonScope();
            Bind<IB2BXmlCancellationRequestValidator>().To<B2BXmlCancellationRequestValidator>().InSingletonScope();
            Bind<IB2BXmlCancellationResultMappingService>().To<B2BXmlCancellationResultMappingService>().InSingletonScope();
            Bind<IB2BXmlCancellationService>().To<B2BXmlCancellationService>().InSingletonScope();
            Bind<IB2BXmlCancellationLogger>().To<B2BXmlCancellationLogger>().InSingletonScope();

            //New cancellation service for B2BXML
            Bind<ICancellationService>().To<B2BXmlCancellationTicketService>().InSingletonScope().Named("B2BXML");
            Bind<ICancellationTicketService>().To<CancellationTicketService>().InSingletonScope();

            //authentication service
            Bind<IAuthenticateUserService>().To<AuthenticateUserService>().InSingletonScope();
            Bind<IAuthenticateUserResponse>().To<AuthenticateUserResponse>().InSingletonScope();

            //Encryption module
            Bind<IEncryptDecryptService>().To<B2BXmlEncryptDecryptService>().InSingletonScope();

            //Agent Configuration Service
            Bind<IB2BXmlApplyAgentConfigurationService>().To<B2BXmlApplyAgentConfigurationService>().InSingletonScope();


        }
    }
}

