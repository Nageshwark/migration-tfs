﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BAgentVatService
    {
        Money CalculateAgentVat(B2BCompany company, Money commission);
    }
}