﻿using System;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BFlashSaleMappingFactory
    {
        Dictionary<int, Guid[]> GetAgentEstablishmentMappings(AccommodationB2BFlashSaleEstablishment[] flashSaleEstablishments, bool includeInActive = false);

        Dictionary<int, Guid[]> GetActiveAgentEstablishmentMappings(AccommodationB2BFlashSaleEstablishment[] flashSaleEstablishments);
    }
}