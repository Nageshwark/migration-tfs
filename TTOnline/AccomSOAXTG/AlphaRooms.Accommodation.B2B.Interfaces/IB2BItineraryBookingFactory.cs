﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2BBookingService = AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.B2B.DomainModels;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BItineraryBookingFactory
    {
        Task<B2BBookingService.CreateItineraryRequest> CreateItineraryRequestAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms);
        B2BBookingService.UpdateBookingItemRequest CreateUpdateBookingItemRequest(Itinerary itinerary, AccommodationBookingRoom accommodationBookingRoom, AccommodationBookingResult bookingResult, AccommodationBookingResultRoom bookingRoomResult);
        B2BBookingService.DetermineItineraryStatusRequest CreateDetermineAndUpdateItineraryRequest(Itinerary itinerary);
    }
}
