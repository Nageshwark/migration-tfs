﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Contracts;
using B2BBookingService = AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.B2B.DomainModels;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BItineraryBookingService
    {
        Task<Itinerary> CreateItineraryAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms);
        Task UpdateItineraryAsync(Itinerary itinerary, AccommodationBookingResult bookingResult);
    }
}
