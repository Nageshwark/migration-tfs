﻿using AlphaRooms.Accommodation.B2B.DomainModels;
using B2BBookingService = AlphaRooms.Accommodation.B2B.Interfaces.B2BBookingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BItineraryBookingMapping
    {
        Itinerary MapToItinerary(B2BBookingService.CreateItineraryResponse response);
    }
}
