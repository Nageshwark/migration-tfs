﻿using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2B.Interfaces
{
    public interface IB2BAgentCommissionService
    {
       Money CalculateAgentCommission(decimal? agentCommissionRate, Money cost);
       decimal? GetAgentCommission(B2BUser agent);
    }
}