﻿namespace AlphaRooms.Accommodation.Core.Caching
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces.Caching;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AvailabilityCachedRequestCaching : IAvailabilityCachedRequestCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationAvailabilityCachedRequest> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationAvailabilityCachedRequest> cacheWriter;

        public AvailabilityCachedRequestCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationAvailabilityCachedRequest> cacheQuery
            , ICacheWriterExpireAsync<AccommodationAvailabilityCachedRequest> cacheWriter)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
        }

        public async Task<AccommodationAvailabilityCachedRequest[]> GetByAvailabilityKeyOrNullAsync(string[] availabilityKeys, AccommodationProvider[] providers, bool isMeta = false)
        {
            var results = (await this.cacheQuery.GetItemsAsync(i => availabilityKeys.Contains(i.AvailabilityKey) 
                && i.CacheExpireDate > this.cacheQuery.GetCacheExpireDateThreshold() + configurationManager.AccommodationCachedRequestExpireThreshold
                , new CacheSort<AccommodationAvailabilityCachedRequest>(i => i.CacheExpireDate, CacheSortOrder.Desc), isMeta)).ToArray();
            var providersDic = providers.ToDictionary(i => i.Id, i => i);
            results.ForEach(result => result.Provider = providersDic[result.ProviderId]);
            return results;
        }

        public async Task SaveRangeAsync(IList<AccommodationAvailabilityCachedRequest> cachedRequests, bool isMeta = false)
        {
            await Async.ParallelForEach(cachedRequests, async(cachedRequest) =>
            {
                await this.cacheWriter.AddOrUpdateAsync(i => i.AvailabilityKey == cachedRequest.AvailabilityKey, cachedRequest, isMeta);
            });
        }
    }
}
