﻿using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.Core.Caching
{
    public class CancellationResultCaching : ICancellationResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationCancellationResult> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationCancellationResult> cacheWriter;
        private readonly IAccommodationProviderRepository providerRepository;

        public CancellationResultCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationCancellationResult> cacheQuery
            , ICacheWriterExpireAsync<AccommodationCancellationResult> cacheWriter, IAccommodationProviderRepository providerRepository)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.providerRepository = providerRepository;
        }

        public Task<AccommodationCancellationResult> GetByCancellationIdOrNullAsync(Guid cancellationId)
        {
            throw new NotImplementedException();
        }

        public async Task SaveAsync(AccommodationCancellationResult cancellationResponse)
        {
            await this.cacheWriter.AddAsync(cancellationResponse, this.configurationManager.AccommodationCancellationResultsExpireTimeout);
        }
    }
}
