﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Caching
{
    public class BookingResultCaching : IBookingResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationBookingResult> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationBookingResult> cacheWriter;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;

        public BookingResultCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationBookingResult> cacheQuery
            , ICacheWriterExpireAsync<AccommodationBookingResult> cacheWriter
            , IAccommodationProviderRepository providerRepository, IAccommodationSupplierRepository supplierRepository)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.providerRepository = providerRepository;
            this.supplierRepository = supplierRepository;
        }

        public async Task<AccommodationBookingResult> GetByBookingIdOrNullAsync(Guid bookingId)
        {
            var result = await this.cacheQuery.GetItemOrNullAsync(i => i.BookingId == bookingId);
            if (result == null) return result;
            var providers = await providerRepository.GetAllDictionaryGroupByIdAsync();
            var suppliers = await supplierRepository.GetAllDictionaryGroupByIdAsync();
            SetNonCacheProperties(result, providers, suppliers);
            return result;
        }

        private void SetNonCacheProperties(AccommodationBookingResult result, Dictionary<int, AccommodationProvider> providers, Dictionary<int, AccommodationSupplier> suppliers)
        {
            result.Rooms.ParallelForEach(room =>
            {
                var provider = providers[room.ProviderId];
                var supplier = suppliers[room.SupplierId];
                room.Provider = provider;
                room.Supplier = supplier;
                room.ValuationResultRoom.Provider = provider;
                room.ValuationResultRoom.Supplier = supplier;
            });
        }

        public async Task SaveAsync(AccommodationBookingResult bookingResult)
        {
            await this.cacheWriter.AddAsync(bookingResult, this.configurationManager.AccommodationBookingResultsExpireTimeout);
        }
    }
}
