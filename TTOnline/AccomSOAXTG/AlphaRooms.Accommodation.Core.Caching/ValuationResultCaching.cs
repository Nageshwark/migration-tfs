﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Caching
{
    public class ValuationResultCaching : IValuationResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationValuationResult> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationValuationResult> cacheWriter;
        private readonly IChannelInfoRepository channelInfoRepository;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;

        public ValuationResultCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationValuationResult> cacheQuery
            , ICacheWriterExpireAsync<AccommodationValuationResult> cacheWriter, IAccommodationProviderRepository providerRepository
            , IAccommodationSupplierRepository supplierRepository, IChannelInfoRepository channelInfoRepository)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.providerRepository = providerRepository;
            this.supplierRepository = supplierRepository;
            this.channelInfoRepository = channelInfoRepository;
        }

        public async Task<AccommodationValuationResult> GetByValuationIdOrNullAsync(Guid valuationId)
        {
            var result = await this.cacheQuery.GetItemOrNullAsync(i => i.ValuationId == valuationId);
            if (result == null) return result;
            var providers = await providerRepository.GetAllDictionaryGroupByIdAsync();
            var suppliers = await supplierRepository.GetAllDictionaryGroupByIdAsync();
            ChannelInfo channelInfo = null;
            SetNonCacheProperties(result, providers, suppliers, channelInfo);
            return result;
        }

        private void SetNonCacheProperties(AccommodationValuationResult result, Dictionary<int, AccommodationProvider> providers, Dictionary<int, AccommodationSupplier> suppliers
            , ChannelInfo channelInfo)
        {
            result.Rooms.ForEach(room =>
            {
                room.Provider = providers[room.ProviderId];
                room.Supplier = suppliers[room.SupplierId];
            });
        }

        public async Task SaveAsync(AccommodationValuationResult valuationResponse)
        {
            await this.cacheWriter.AddAsync(valuationResponse, this.configurationManager.AccommodationValuationResultsExpireTimeout);
        }
    }
}
