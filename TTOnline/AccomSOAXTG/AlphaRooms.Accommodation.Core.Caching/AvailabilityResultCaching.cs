﻿namespace AlphaRooms.Accommodation.Core.Caching
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces.Caching;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;


    public class AvailabilityResultCaching : IAvailabilityResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheMultiDbQueryExpireAsync<AccommodationAvailabilityResult, Guid> cacheQuery;
        private readonly ICacheMultiDbWriterExpireAsync<AccommodationAvailabilityResult, Guid> cacheWriter;
        private readonly IChannelInfoRepository channelInfoRepository;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;

        public AvailabilityResultCaching(IAccommodationConfigurationManager configurationManager, ICacheMultiDbQueryExpireAsync<AccommodationAvailabilityResult, Guid> cacheQuery
            , ICacheMultiDbWriterExpireAsync<AccommodationAvailabilityResult, Guid> cacheWriter, IAccommodationProviderRepository providerRepository
            , IAccommodationSupplierRepository supplierRepository, IChannelInfoRepository channelInfoRepository)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.providerRepository = providerRepository;
            this.supplierRepository = supplierRepository;
            this.channelInfoRepository = channelInfoRepository;
        }

        public async Task<AccommodationAvailabilityResult[]> GetByAvailabilityRequestIdsAsync(Guid baseDestinationId, Guid[] availabilityRequestIds, AccommodationProvider[] providers, bool isMeta = false)
        {
            var results = (await this.cacheQuery.GetItemsAsync(baseDestinationId, i => availabilityRequestIds.Contains(i.AvailabilityRequestId), isMeta)).ToArray();
            if (results.IsEmpty()) return results;
            var providersDic = providers.ToDictionary(i => i.Id, i => i);
            var suppliers = await supplierRepository.GetAllDictionaryGroupByIdAsync();
            SetNonCacheProperties(results, providersDic, suppliers);
            return results;
        }

        public async Task<AccommodationAvailabilityResult[]> GetByRoomIdsAsync(Guid baseDestinationId, Guid[] roomIds)
        {
            var results = (await this.cacheQuery.GetItemsAsync(baseDestinationId, i => roomIds.Contains(i.Id))).ToArray();
            if (results.IsEmpty()) return results;
            var providers = await providerRepository.GetAllDictionaryGroupByIdAsync();
            var suppliers = await supplierRepository.GetAllDictionaryGroupByIdAsync();
            SetNonCacheProperties(results, providers, suppliers);
            return results;
        }

        private void SetNonCacheProperties(AccommodationAvailabilityResult[] results, Dictionary<int, AccommodationProvider> providers, Dictionary<int, AccommodationSupplier> suppliers)
        {
            results.ForEach(result =>
            {
                result.Provider = providers[result.ProviderId];
                result.Supplier = suppliers[result.SupplierId];
            });
        }

        public async Task SaveRangeAsync(Guid baseDestinationId, IList<AccommodationAvailabilityResult> results, bool isMeta = false)
        {
            results.ForEach(result => { result.Source = SearchResultSource.Cache; });
            await this.cacheWriter.AddRangeAsync(baseDestinationId, results, isMeta);
        }

        public DateTime GetCacheExpireDateThreshold()
        {
            return this.cacheQuery.GetCacheExpireDateThreshold();
        }
    }
}
