﻿namespace AlphaRooms.Accommodation.B2C.UnitTests.AlphaRooms.Accommodation.B2C.ServicesTests
{
    using Core.Contracts;
    using Core.Interfaces;
    using Core.Provider.Interfaces.Repositories;
    using Core.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Newtonsoft.Json;
    using Services;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Reflection;
    using System.Text;

    [TestClass]
    public class B2CConsolidationSpeedTests
    {
        private static IReadOnlyList<AccommodationAvailabilityResult> CachedTestRooms = B2CConsolidationSpeedTests.GetTestRooms();

        // cached because reading the files was slow
        public static List<AccommodationAvailabilityResult> TestRooms
        {
            get
            {
                var testRoomsCopy = new List<AccommodationAvailabilityResult>(B2CConsolidationSpeedTests.CachedTestRooms.Count);

                for (int i = 0; i < B2CConsolidationSpeedTests.CachedTestRooms.Count; i++)
                {
                    testRoomsCopy.Add((AccommodationAvailabilityResult)B2CConsolidationSpeedTests.CachedTestRooms[i].Clone());
                }

                return testRoomsCopy;
            }
        }

        [Ignore]
        [TestMethod]
        public void B2CConsolidationSpeedTest()
        {
            const int Iterations = 250;

            List<AccommodationAvailabilityResult> rooms = B2CConsolidationSpeedTests.TestRooms;

            // warm up 
            for (int i = 0; i < Iterations / 10; i++)
            {
                B2CConsolidationSpeedTests.Consolidate(rooms, false);
            }

            // test
            Stopwatch timer = Stopwatch.StartNew();
            for (int i = 0; i < Iterations; i++)
            {
                B2CConsolidationSpeedTests.Consolidate(rooms, false);
            }
            timer.Stop();

            TimeSpan expected = TimeSpan.FromSeconds(20d);
            TimeSpan actual = timer.Elapsed;

            Console.WriteLine("Total Time Taken: " + actual);
            var perIt = TimeSpan.FromMilliseconds(actual.TotalMilliseconds / Iterations);
            Console.WriteLine("Time Taken per Iteration: " + perIt);

            Assert.IsTrue(actual < expected, string.Format("Consolidation took to long to complete. Target: {0} Actual: {1}", expected, actual));
        }

        public static IList<AccommodationAvailabilityResult> Consolidate(List<AccommodationAvailabilityResult> rooms, bool setUpPriorityService = true)
        {
            Mock<IRoomDescriptionRemoveWordRepository> removeWordRepository = new Mock<IRoomDescriptionRemoveWordRepository>();
            Mock<IRoomDescriptionReplacementRepository> replacementRepository = new Mock<IRoomDescriptionReplacementRepository>();

            RoomDescriptionKeyGeneration roomDescriptionKeyGeneration = new RoomDescriptionKeyGeneration(
                removeWordRepository.Object,
                replacementRepository.Object);

            IRoomDescriptionComparer deduplication = new RoomDescriptionComparer(roomDescriptionKeyGeneration);

            Mock<IAvailabilityResultProviderPriorityService> priorityService = new Mock<IAvailabilityResultProviderPriorityService>();

            if (setUpPriorityService)
            {
                // this makes it really slow because mock is slow
                priorityService
                    .Setup(x => x.GetPriority(It.Is<AccommodationAvailabilityResult>(y => y.ProviderResult.SupplierEdiCode == "A")))
                    .Returns(int.MaxValue);
            }

            B2CAvailabilityConsolidationService consolidation = new B2CAvailabilityConsolidationService(deduplication, priorityService.Object);

            AccommodationAvailabilityRequest availabilityRequest = new AccommodationAvailabilityRequest();

            var results = consolidation.ConsolidateAvailabilityResults(availabilityRequest, rooms);

            IAvailabilityResultLowerPriceService lowerPriceService = new B2CAvailabilityResultLowerPriceService();
            results = lowerPriceService.ApplyAvailabilityLowerPrice(results);

            Assert.IsNotNull(results);

            return results;
        }

        private static List<AccommodationAvailabilityResult> GetTestRooms()
        {
            List<AccommodationAvailabilityResult> rooms;

            // read in data from CSV
            var assembly = Assembly.GetExecutingAssembly();
            using (Stream fileStream = assembly.GetManifestResourceStream("AlphaRooms.Accommodation.B2C.UnitTests.MallorcaAvailabilityResults.zip"))
            {
                string jsonString = Unzip(fileStream);
                rooms = JsonConvert.DeserializeObject<List<AccommodationAvailabilityResult>>(jsonString);

                foreach (var item in rooms)
                {
                    item.IsDuplicateRate = false;
                }

                string sl = JsonConvert.SerializeObject(rooms);
                File.WriteAllText("somefile.json", sl);
            }

            return rooms;
        }

        private static string Unzip(Stream bytes)
        {
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(bytes, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        private static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
    }
}
