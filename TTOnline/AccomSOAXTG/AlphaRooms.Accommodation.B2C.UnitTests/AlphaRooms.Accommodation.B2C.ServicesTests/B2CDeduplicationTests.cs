﻿namespace AlphaRooms.Accommodation.B2C.UnitTests.AlphaRooms.Accommodation.B2C.ServicesTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Core.Provider.DomainModels.Enums;
    using SOACommon.Contracts;
    using Core.Contracts;
    using Core.Provider.Contracts;

    [TestClass]
    public class B2CDeduplicationTests
    {
        [TestMethod]
        public void DeduplicationSetsHighestPrice()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                .ToList();

            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);
            var consolidationHighest = consolidationGroup.OrderBy(x => x.Price).Last();

            var duplicatingGroup = consolidationGroup
                // Deduplication group
                .Where(x => string.Equals(roomMarkedAsCheapest.RoomDescriptionKey, x.RoomDescriptionKey, StringComparison.OrdinalIgnoreCase))
                .OrderBy(x => x.SalePrice)
                .ToList();

            var highestPricedDuplicate = duplicatingGroup.Last();

            // double check that this tst is valid and Duplicate max is not also the consolation max
            Assert.AreNotEqual(consolidationHighest, highestPricedDuplicate);

            Assert.AreEqual(roomMarkedAsCheapest.HighestPrice, highestPricedDuplicate.Price);
        }

        [TestMethod]
        public void DeduplicationSetsHighestPriceMulti()
        {
            // there can be multipul Deduplication groups per consolidation groups make sure me identify them all
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .OrderBy(x => x.Price)
                .ToList();

            var cheapest = deduplicateGroup.First();
            var highest = deduplicateGroup.Last();

            Assert.AreEqual(cheapest.HighestPrice, highest.Price);
        }

        [TestMethod]
        public void DeduplicationSetsIsDeduplicate()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                .ToList();

            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);

            var duplicateGroup = consolidationGroup
                // Deduplication group
                .Where(x => string.Equals(roomMarkedAsCheapest.RoomDescriptionKey, x.RoomDescriptionKey, StringComparison.OrdinalIgnoreCase))
                .OrderBy(x => x.SalePrice)
                .ToList();

            // it has seven rooms
            Assert.IsTrue(duplicateGroup.Count > 1);
            Assert.IsFalse(duplicateGroup[0].IsDuplicateRate);

            for (int i = 1; i < duplicateGroup.Count; i++)
            {
                Assert.IsTrue(duplicateGroup[i].IsDuplicateRate);
            }
        }

        [TestMethod]
        public void DeduplicationSetsIsDeduplicateMulti()
        {
            // there can be multipul Deduplication groups per consolidation groups make sure me identify them all
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .OrderBy(x => x.Price)
                .ToList();

            Assert.IsFalse(deduplicateGroup[0].IsDuplicateRate);

            for (int i = 1; i < deduplicateGroup.Count; i++)
            {
                Assert.IsTrue(deduplicateGroup[i].IsDuplicateRate);
            }
        }

        [TestMethod]
        public void DeduplicationIncludesCost()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("14118344-f91e-40c8-ab5d-d330637ba828");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price but make the cost price cheaper so more profit
            expected.CostPrice = new Money(expected.CostPrice.Amount - 1, expected.CostPrice.CurrencyCode);
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .ToList();

            var nonDuplicate = deduplicateGroup.Single(x => !x.IsDuplicateRate);
            Assert.AreEqual(expected, nonDuplicate);
        }

        [TestMethod]
        public void DeduplicationIncludesRoomNumber()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("14118344-f91e-40c8-ab5d-d330637ba828");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price but make it a different room number
            expected.RoomNumber += 1;
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .ToList();

            var nonDuplicates = deduplicateGroup.Where(x => !x.IsDuplicateRate);
            // should be two nonDuplicate rooms as this was a multiroom search
            Assert.IsTrue(nonDuplicates.Count() == 2);
        }

        [TestMethod]
        public void DeduplicationIncludesSupplierPriorityScores()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("14118344-f91e-40c8-ab5d-d330637ba828");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            expected.ProviderResult = (AccommodationProviderAvailabilityResult)expected.ProviderResult.Clone();
            // it is the same price & cost price & but A code as the hight
            expected.ProviderResult.SupplierEdiCode = "A";
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .ToList();

            var nonDuplicate = deduplicateGroup.Single(x => !x.IsDuplicateRate);
            Assert.AreEqual(expected, nonDuplicate);
        }

        [TestMethod]
        public void DeduplicationIncludesRoomDescriptionLength()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("14118344-f91e-40c8-ab5d-d330637ba828");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price & cost price & suppiler, just shorter name
            expected.RoomDescription = expected.RoomDescription.Substring(1);
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var deduplicateGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("13c767bd-05e7-4a11-9c3b-24606350f03e"))
                .Where(x => x.BoardType == BoardType.HalfBoard)
                // this group does not containt the cheapest consolidated room
                .Where(x => string.Equals(x.RoomDescriptionKey, "room Suite ", StringComparison.OrdinalIgnoreCase))
                .ToList();

            var nonDuplicate = deduplicateGroup.Single(x => !x.IsDuplicateRate);
            Assert.AreEqual(expected, nonDuplicate);
        }

        [TestMethod]
        public void DeduplicationDoesNotRemoveRooms()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("14118344-f91e-40c8-ab5d-d330637ba828");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            int expected = testRooms
                .Where(x => x.EstablishmentId == cheapestAtStart.EstablishmentId
                && x.BoardType == cheapestAtStart.BoardType
                && string.Equals(x.RoomDescriptionKey, cheapestAtStart.RoomDescriptionKey))
                .Count();

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            int actual = consolidatedRooms
                .Where(x => x.EstablishmentId == cheapestAtStart.EstablishmentId
                && x.BoardType == cheapestAtStart.BoardType
                && string.Equals(x.RoomDescriptionKey, cheapestAtStart.RoomDescriptionKey))
                .Count();

            Assert.AreEqual(expected, actual);
        }
    }
}
