﻿namespace AlphaRooms.Accommodation.B2C.UnitTests
{
    using Core.Contracts;
    using Core.Provider.Contracts;
    using Core.Provider.DomainModels;
    using Core.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SOACommon.DomainModels;
    using SOACommon.DomainModels.Enumerators;
    using System;
    using System.Collections.Generic;

    [TestClass]
    public class BookingResultBuilderServiceTests
    {
        [TestInitialize]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        [TestCleanup]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        [TestMethod]
        public void BookingResultAggregationTest()
        {
            var service = new BookingResultBuilderService();

            //Simulate 3 room booking request
            var bookingRequest = new AccommodationBookingRequest
            {
                BookingId = new Guid(),
                ChannelInfo = new ChannelInfo { Channel = Channel.BetaBedsUK, Site = Sites.BetaBeds },
                ItineraryId = 11111111,
                ItineraryReference = "",
                ValuatedRooms = new AccommodationBookingRequestRoom[3]
            };

            var bookingLiveProcessParams = new List<AccommodationBookingLiveProcessParams>();
            //Simulate 3 rooms from 2 suppliers
            var room1 = new AccommodationBookingResultRoom() { RoomNumber = 1, ProviderBookingResult = new AccommodationProviderBookingResult { BookingStatus = BookingStatus.Confirmed } };
            var provider1room1 = new AccommodationBookingLiveProcessParams
            {
                BookingRequest = bookingRequest,
                BookingRequestRooms = new BookingRoomValuationResult[1],
                Provider = new AccommodationProvider { EdiCode = "B" },
                ProcessId = 1,
                Response = new BookingProcessResponse { Results = new List<AccommodationBookingResultRoom>() { room1 } }
            };

            var room2 = new AccommodationBookingResultRoom() { RoomNumber = 2, ProviderBookingResult = new AccommodationProviderBookingResult { BookingStatus = BookingStatus.Confirmed } };
            var room3 = new AccommodationBookingResultRoom() { RoomNumber = 3, ProviderBookingResult = new AccommodationProviderBookingResult { BookingStatus = BookingStatus.Confirmed } };
            var provider2room23 = new AccommodationBookingLiveProcessParams
            {
                BookingRequest = bookingRequest,
                BookingRequestRooms = new BookingRoomValuationResult[2],
                Provider = new AccommodationProvider { EdiCode = "CS" },
                ProcessId = 1,
                Response = new BookingProcessResponse { Results = new List<AccommodationBookingResultRoom>() { room2, room3 } }
            };

            bookingLiveProcessParams.Add(provider1room1);
            bookingLiveProcessParams.Add(provider2room23);

            var result = service.CreateBookingResult(bookingRequest, bookingLiveProcessParams);

            Assert.IsTrue(result.Rooms.Length == 3);
        }
    }
}
