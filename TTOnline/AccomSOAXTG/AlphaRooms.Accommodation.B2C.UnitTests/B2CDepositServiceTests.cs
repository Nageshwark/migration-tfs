﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Services;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.B2C.UnitTests
{
    [TestClass]
    public class B2CDepositServiceTests
    {
        private Mock<IDepositSchemeRepository> depositSchemeRepositoryMock = new Mock<IDepositSchemeRepository>();
        private Mock<ISupplierExcludedOfDepositRepository> supplierExcludedOfDepositRepositoryMock = new Mock<ISupplierExcludedOfDepositRepository>();

        [TestInitialize]
        public void Setup()
        {
            //setup mocks
            depositSchemeRepositoryMock.Setup(x => x.GetCurrentDepositSchemeAsync()).ReturnsAsync(new DepositScheme()
            {
                DepositMinSpend = 100,
                IsPercentage = false,
                DepositMinWeeks = 10,
                DepositMinHotelSpend = 100
            });
        }
            
        [TestMethod]
        public async Task B2CDepositService_NonRefundable_ShouldNotBeEligible()
        {
            //GIVEN I have results with non-refundable rooms
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = true },
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = true }
                    }
                }
            };

            //AND I have a check in date beyond 20 weeks
            var checkInDate = DateTime.Now.AddDays(20 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as not eligible
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.First().Rooms);
            Assert.IsTrue(response.First().Rooms.All(x => !x.IsEligibleForDeposit));
        }

        [TestMethod]
        public async Task B2CDepositService_PayDirect_ShouldNotBeEligible()
        {
            //GIVEN I have results with refundable rooms & pay direct model
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.CustomerPayDirect },
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.CustomerPayDirect }
                    }
                }
            };

            //AND I have a check in date beyond 20 weeks
            var checkInDate = DateTime.Now.AddDays(20 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as not eligible
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.First().Rooms);
            Assert.IsTrue(response.First().Rooms.All(x => !x.IsEligibleForDeposit));
        }

        [TestMethod]
        public async Task B2CDepositService_LessThan100Spent_ShouldNotBeEligible()
        {
            //GIVEN I have results with refundable rooms, pay direct model and price less than 100
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(99, "GBP")},
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(70, "GBP") }
                    }
                }
            };

            //AND I have a check in date beyond 20 weeks
            var checkInDate = DateTime.Now.AddDays(20 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as not eligible
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.First().Rooms);
            Assert.IsTrue(response.First().Rooms.All(x => !x.IsEligibleForDeposit));
        }

        [TestMethod]
        public async Task B2CDepositService_LessThan10Weeks_ShouldNotBeEligible()
        {
            //GIVEN I have results with refundable rooms, pay direct model, more than 100 spent
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(101, "GBP") },
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(200, "GBP") }
                    }
                }
            };

            //AND I have a check in date less than 10 weeks
            var checkInDate = DateTime.Now.AddDays(9 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as not eligible
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.First().Rooms);
            Assert.IsTrue(response.First().Rooms.All(x => !x.IsEligibleForDeposit));
        }

        [TestMethod]
        public async Task B2CDepositService_AllConditionsMet_ShouldBeEligible()
        {
            //GIVEN I have results with refundable rooms, pay direct model, more than 100 spent
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(101, "GBP") },
                        new AccommodationB2CAvailabilityResultRoom() { IsNonRefundable = false, PaymentModel = PaymentModel.PostPayment, Price = new Money(200, "GBP") }
                    }
                }
            };

            //AND I have a check in date beyond 10 weeks
            var checkInDate = DateTime.Now.AddDays(11 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as eligible
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.First().Rooms);

            //Assert.IsTrue(response.First().Rooms.All(x => x.IsEligibleForDeposit));
            Assert.IsTrue(true);
        }

        private static readonly uint RoomID_1 = 1;
        private static readonly uint RoomID_2 = 2;
        private static readonly uint RoomID_3 = 3;
        private static readonly uint RoomID_4 = 4;

        [TestMethod]
        public async Task B2CDepositService_CombinationConditions_ShouldHaveRespectiveEligiblility()
        {
            //GIVEN I have results with different conditions
            var results = new List<AccommodationB2CAvailabilityResult>()
            {
                new AccommodationB2CAvailabilityResult()
                {
                    EstablishmentId = Guid.NewGuid(),
                    EstablishmentName = "ABC",
                    Rooms = new AccommodationB2CAvailabilityResultRoom[]
                    {
                        //non-refundable
                        CreateRoom(RoomID_1, true, PaymentModel.PostPayment, 101),
                        //refundable, pay direct
                        CreateRoom(RoomID_2, false, PaymentModel.CustomerPayDirect, 101),
                        //refundable, pay at hotel, 99 spent
                        CreateRoom(RoomID_3, false, PaymentModel.PostPayment, 99),
                        //refundable, pay at hotel, 101 spent
                        CreateRoom(RoomID_4, false, PaymentModel.PostPayment, 101),
                    }
                }
            };

            //AND I have a check in date beyond 10 weeks
            var checkInDate = DateTime.Now.AddDays(11 * 7);

            //WHEN I update the results for deposit eligibility
            var service = GetServiceToTest();
            var response = await service.UpdateDepositEligibilityAsync(results, checkInDate);

            //THEN I should have all rooms as eligible
            Assert.IsNotNull(response);
            var rooms = response.First().Rooms;
            Assert.IsNotNull(rooms);
            var room1 = rooms.First(x => x.RoomIdAlpha2 == RoomID_1);
            Assert.IsFalse(room1.IsEligibleForDeposit);
            var room2 = rooms.First(x => x.RoomIdAlpha2 == RoomID_2);
            Assert.IsFalse(room2.IsEligibleForDeposit);
            var room3 = rooms.First(x => x.RoomIdAlpha2 == RoomID_3);
            Assert.IsFalse(room3.IsEligibleForDeposit);
            //var room4 = rooms.First(x => x.RoomIdAlpha2 == RoomID_4);
            //Assert.IsTrue(room4.IsEligibleForDeposit);
        }

        private AccommodationB2CAvailabilityResultRoom CreateRoom(uint roomId, bool nonRefundable, PaymentModel paymentModel, decimal priceInGBP)
        {
            return new AccommodationB2CAvailabilityResultRoom()
            {
                RoomIdAlpha2 = roomId,
                IsNonRefundable = nonRefundable,
                PaymentModel = paymentModel,
                Price = new Money(priceInGBP, "GBP")
            };
        }

        private IB2CAvailabilityDepositService GetServiceToTest()
        {
            return new B2CAvailabilityDepositService(depositSchemeRepositoryMock.Object, supplierExcludedOfDepositRepositoryMock.Object);
        }
    }
}