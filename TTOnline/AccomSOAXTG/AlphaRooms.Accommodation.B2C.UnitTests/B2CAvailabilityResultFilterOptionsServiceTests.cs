﻿using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Services;
using AlphaRooms.SOACommon.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AlphaRooms.Accommodation.B2C.UnitTests
{
    [TestClass]
    public class B2CAvailabilityResultFilterOptionsServiceTests
    {
        [TestMethod]
        public void B2CFilterOptionsService_Range_CreatesAllSegments()
        {
            //GIVEN I have 10 prices
            var prices = CreateDecimalList("10,20,30,40,50,60,70,80,90,100");

            //WHEN I get the range
            var service = GetB2CAvailabilityResultsFilterOptionsService();
            var result = service.CreateRange(prices);
            Assert.IsNotNull(result);

            //Then I should have min=10, quarter=30, half=55, three quarter=75 & max=100
            AssertSegments(result, 10, 30, 55, 75, 100);
        }

        [TestMethod]
        public void B2CFilterOptionsService_Range_FourItems_CreateSegments()
        {
            //GIVEN I have 10 prices
            var prices = CreateDecimalList("10,20,30,40");

            //WHEN I get the range
            var service = GetB2CAvailabilityResultsFilterOptionsService();
            var result = service.CreateRange(prices);
            Assert.IsNotNull(result);

            //Then I should have min=10, quarter=20, half=25, three quarter=35 & max=40
            AssertSegments(result, 10, 20, 25, 35, 40);
        }

        [TestMethod]
        public void B2CFilterOptionsService_Range_Combinations_CreateSegments()
        {
            //GIVEN I have prices
            var prices = CreateDecimalList("10,20,30");

            //WHEN I get the range
            var service = GetB2CAvailabilityResultsFilterOptionsService();
            var result = service.CreateRange(prices);
            Assert.IsNotNull(result);

            //Then I should have min=10, quarter=15, half=20, three quarter=25 & max=30
            AssertSegments(result, 10, 15, 20, 25, 30);
        }

        [TestMethod]
        public void B2CFilterOptionsService_Range_HigherPrices_CreateSegments()
        {
            //GIVEN I have prices
            var prices = CreateDecimalList("10,30,60,80,80,80,80,100");

            //WHEN I get the range
            var service = GetB2CAvailabilityResultsFilterOptionsService();
            var result = service.CreateRange(prices);
            Assert.IsNotNull(result);

            //Then I should have min=10, quarter=60, half=80, three quarter=none & max=100
            AssertSegments(result, 10, 60, 80, null, 100);
        }

        [TestMethod]
        public void B2CFilterOptionsService_Range_Only2unique_CreateSegments()
        {
            //GIVEN I have prices
            var prices = CreateDecimalList("10,10,10,10,10,50,50,50");

            //WHEN I get the range
            var service = GetB2CAvailabilityResultsFilterOptionsService();
            var result = service.CreateRange(prices);
            Assert.IsNotNull(result);

            //Then I should have min=10, quarter=none, half=none, three quarter=none & max=50
            AssertSegments(result, 10, null, null, null, 50);
        }

        private void AssertSegments(B2CResultsRangeFilterOptions result, decimal? expectedMin, decimal? expectedQuarter, decimal? expectedHalf, decimal? expectedThreeQuarter, decimal? expectedMax)
        {
            //assert min
            Assert.AreEqual(expectedMin, result.RangeMin,"min does not match");

            //assert 25%
            Assert.AreEqual(expectedQuarter, result.QuarterSegment, "quarter does not match");

            //assert 50%
            Assert.AreEqual(expectedHalf, result.HalfSegment, "half does not match");

            //assert 75%
            Assert.AreEqual(expectedThreeQuarter, result.ThreeQuarterSegment, "three quarter does not match");

            //assert max
            Assert.AreEqual(expectedMax, result.RangeMax, "max does not match");
        }

        private List<Money> CreateMoneyList(string monies)
        {
            return monies.Split(',').Select(x => new Money(decimal.Parse(x))).ToList();
        }

        private decimal[] CreateDecimalList(string monies)
        {
            return monies.Split(',').Select(x => decimal.Parse(x)).ToArray();
        }

        private IB2CAvailabilityResultFilterOptionsService GetB2CAvailabilityResultsFilterOptionsService()
        {
            return new B2CAvailabilityResultFilterOptionsService(null, null, null, null);
        }
    }
}