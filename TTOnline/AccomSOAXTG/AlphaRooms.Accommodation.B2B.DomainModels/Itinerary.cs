﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2B.DomainModels
{
    public class Itinerary
    {
        public int ItineraryId { get; set; }
        public string ItineraryReference { get; set; }
        public AccommodationBookingRoom[] AccommodationBookingRooms { get; set; }
    }
}
