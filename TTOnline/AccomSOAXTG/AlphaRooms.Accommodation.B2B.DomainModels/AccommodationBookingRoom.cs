﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2B.DomainModels
{
    public class AccommodationBookingRoom
    {
        public int AccommodationBookingItemId { get; set; }
        public int RoomNumber { get; set; }
    }
}
