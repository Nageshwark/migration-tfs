﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class BookingRoomValuationResult
    {
        public byte RoomNumber { get; set; }
        public byte ProviderRoomNumber { get; set; }
        public AccommodationBookingRequestRoom ValuatedRoom { get; set; }
        public AccommodationAvailabilityResultRoomIdWrapper ValuatedRoomDetails { get; set; }
        public AccommodationValuationResult ValuationResult { get; set; }
        public AccommodationValuationResultRoom ValuationResultRoom { get; set; }
    }
}
