﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingRequest
    {
        public Guid BookingId { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public B2BUser Agent { get; set; }
        public Guid ValuationId { get; set; }
        public int ItineraryId { get; set; }
        public string ItineraryReference { get; set; }
        public AccommodationBookingRequestCustomer Customer { get; set; } 
        public AccommodationBookingRequestRoom[] ValuatedRooms { get; set; }
        public bool Debugging { get; set; }
    }
}
