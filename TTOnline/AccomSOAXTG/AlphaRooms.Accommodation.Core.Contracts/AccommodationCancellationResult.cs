﻿using AlphaRooms.Cache.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationCancellationResult : ICacheExpire
    {
        public const string RoomsFieldName = "Rms";
        public const string CacheExpireDateFieldName = "CED";

        [BsonId]
        public Guid CancellationId { get; set; }

        [BsonElement(RoomsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationCancellationResultRoom[] Rooms { get; set; }

        [BsonElement(CacheExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CacheExpireDate { get; set; }
    }
}
