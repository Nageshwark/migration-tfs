﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class BookingProcessResponse
    {
        public IList<AccommodationBookingResultRoom> Results { get; set; }
        public Exception Exception { get; set; }
        public TimeSpan TimeTaken { get; set; }
        public AccommodationProviderRequest[] ProviderRequests { get; set; }
    }
}
