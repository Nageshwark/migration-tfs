﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class TTAccommodationBookingRequestCustomer : AccommodationBookingRequestCustomer
    {
        public int Site { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public int? UserId { get; set; }
        public SOACommon.DomainModels.Enumerators.Channel Channel { get; set; }
        public int LanguageId { get; set; }
    }
}
