﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationCancellationRequest
    {
        public Guid CancellationId { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public B2BUser Agent { get; set; }
        public string BookingReferenceId { get; set; }
    }
}
