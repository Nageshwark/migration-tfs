﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityResponse
    {
        public IList<AccommodationAvailabilityResult> Results { get; set; }
        public DateTime ResultsCacheExpireDate { get; set; }
        public AccommodationAvailabilityResponseProviderDetails[] ProcessResponseDetails { get; set; }
        public DateTime PostProcessStartDate { get; set; }
    }
}
