﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AvailabilityRoomAvailabilityKey
    {
        public AccommodationProvider Provider { get; set; }
        public byte ProviderRoomNumber { get; set; }
        public AccommodationAvailabilityRequestRoom Room { get; set; }
        public AvailabilityRoomAvailabilityKey DuplicatedRequestRoom { get; set; }
        public string AvailabilityKey { get; set; }
        public AccommodationAvailabilityCachedRequest CachedRequest { get; set; }
        public Guid AvailabilityRequestId { get; set; }
        public byte AdultsCount { get; set; }
        public byte ChildrenCount { get; set; }
        public byte InfantsCount { get; set; }
    }
}
