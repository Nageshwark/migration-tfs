﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class TTAccommodationBookingRequestRoomPaymentDetails : AccommodationBookingRequestRoomPaymentDetails
    {
        public string MerchantRefNum { get; set; }
        public string PaySafeVPSAuthCode { get; set; }
    }
}
