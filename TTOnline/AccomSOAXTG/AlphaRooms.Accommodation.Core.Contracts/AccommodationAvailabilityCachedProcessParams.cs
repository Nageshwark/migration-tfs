﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityCachedProcessParams : AccommodationAvailabilityProcessParamsBase
    {
        public AccommodationProvider[] Providers { get; set; }
        public AccommodationAvailabilityCachedRequest[] CachedRequests { get; set; }
    }
}
