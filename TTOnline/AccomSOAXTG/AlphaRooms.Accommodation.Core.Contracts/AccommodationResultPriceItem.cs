﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationResultPriceItem
    {
        public const string TypeFieldName = "Tpe";
        public const string SaleFieldName = "SlP";
        public const string CostFieldName = "Cst";

        [BsonElement(TypeFieldName)]
        public AccommodationResultPriceItemType Type { get; set; }

        [BsonElement(SaleFieldName)]
        public Money SalePrice { get; set; }
        
        [BsonElement(CostFieldName)]
        public Money CostPrice { get; set; }        
    }
}
