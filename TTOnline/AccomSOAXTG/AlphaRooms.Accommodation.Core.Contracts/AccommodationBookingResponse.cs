﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingResponse
    {
        public AccommodationBookingResponseProviderDetails[] ProcessResponseDetails { get; set; }
        public AccommodationBookingResult Result { get; set; }
    }
}
