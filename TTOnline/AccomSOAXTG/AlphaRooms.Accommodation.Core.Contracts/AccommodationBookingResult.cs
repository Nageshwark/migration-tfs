﻿using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationBookingResult : ICacheExpire
    {
        public const string RoomsFieldName = "Rms";
        public const string BookingStatusFieldName = "BSt";
        public const string CacheExpireDateFieldName = "CED";

        [BsonId]
        public Guid BookingId { get; set; }

        [BsonElement(BookingStatusFieldName)]
        [BsonIgnoreIfDefault]
        public BookingStatus BookingStatus { get; set; }

        [BsonElement(RoomsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationBookingResultRoom[] Rooms { get; set; }

        [BsonElement(CacheExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CacheExpireDate { get; set; }
    }
}
