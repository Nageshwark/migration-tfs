﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AvailabilityRequestRoom
    {
        public AccommodationAvailabilityRequestRoom Room { get; set; }

        public KeyValuePair<AccommodationProvider, string> AvailabilityKey { get; set; }
    }
}
