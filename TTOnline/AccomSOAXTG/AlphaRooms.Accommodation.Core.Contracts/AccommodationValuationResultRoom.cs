﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationValuationResultRoom : IAccommodationResultRoom
    {
        public const string RoomIdFieldName = "RId";
        public const string ProviderResultFieldName = "PRs";
        public const string ProviderIdFieldName = "PvI";
        public const string SupplierIdFieldName = "SpI";
        public const string SupplierAliasFieldName = "SpA";
        public const string BaseDestinationIdFieldName = "BDI";
        public const string DestinationIdFieldName = "DsI";
        public const string EstablishmentIdFieldName = "EsI";
        public const string EstablishmentStarRatingFieldName = "ESR";
        public const string EstablishmentChainIdFieldName = "ECI";
        public const string RoomNumberFieldName = "RmN";
        public const string RoomDescriptionFieldName = "RmD";
        public const string BoardTypeFieldName = "BTp";
        public const string BoardDescriptionFieldName = "BDs";
        public const string PriceBreakdownFieldName = "PBD";
        public const string PriceFieldName = "Prc";
        public const string PricePerNightFieldName = "PRN";
        public const string PricePerPersonFieldName = "PRP";
        public const string PaymentToTakeFieldName = "PTT";
        public const string CostPriceFieldName = "CTP";
        public const string SalePriceFieldName = "SLP";
        public const string DiscountFieldName = "DCT";
        public const string MarginFieldName = "MRG";
        public const string ExchangeRateFieldName = "ERt";
        public const string CancellationPoliciesFieldName = "CPs";
        public const string AvailabilityRequestIdFieldName = "ARI";
        public const string AdultsFieldName = "Adt";
        public const string ChildrenFieldName = "Chd";
        public const string InfantsFieldName = "Inf";
        public const string SpecificDataFieldName = "Sdt";
        public const string CountryCodeFieldName = "CC";
        public const string PriceHikeFieldName = "PRH";
        private const string IsEligibleForDepositFieldName = "EDP";
		public const string IsActiveAccommodationAdminFeeFieldName = "AAFN";
        public const string AccommodationAdminFeeFieldName = "AAF";

        [BsonId]
        public Guid Id { get; set; }

        [BsonElement(RoomIdFieldName)]
        public string RoomId { get; set; }

        [BsonElement(ProviderIdFieldName)]
        public int ProviderId { get; set; }

        [BsonIgnore]
        public AccommodationProvider Provider { get; set; }

        [BsonElement(SupplierIdFieldName)]
        public int SupplierId { get; set; }

        [BsonElement(SupplierAliasFieldName)]
        public string SupplierAlias { get; set; }
        

        [BsonIgnore]
        public AccommodationSupplier Supplier { get; set; }
        
        [BsonElement(ProviderResultFieldName)]
        public AccommodationProviderValuationResult ProviderResult { get; set; }

        [BsonIgnore]
        IAccommodationProviderResultRoom IAccommodationResultRoom.ProviderResult { get { return this.ProviderResult; } }

        [BsonElement(AdultsFieldName)]
        public byte AdultsCount { get; set; }

        [BsonElement(ChildrenFieldName)]
        public byte ChildrenCount { get; set; }

        [BsonElement(InfantsFieldName)]
        public byte InfantsCount { get; set; }

        [BsonElement(AvailabilityRequestIdFieldName)]
        public Guid AvailabilityRequestId { get; set; }

        [BsonElement(BaseDestinationIdFieldName)]
        public Guid BaseDestinationId { get; set; }

        [BsonElement(DestinationIdFieldName)]
        public Guid DestinationId { get; set; }
                
        [BsonElement(EstablishmentIdFieldName)]
        public Guid EstablishmentId { get; set; }

        [BsonElement(EstablishmentStarRatingFieldName)]
        public StarRatingType EstablishmentStarRating { get; set; }

        [BsonElement(EstablishmentChainIdFieldName)]
        public int? EstablishmentChainId { get; set; }

        [BsonElement(RoomNumberFieldName)]
        public byte RoomNumber { get; set; }

        [BsonElement(RoomDescriptionFieldName)]
        public string RoomDescription { get; set; }

        [BsonElement(BoardTypeFieldName)]
        public BoardType BoardType { get; set; }
                
        [BsonElement(PaymentToTakeFieldName)]
        public Money PaymentToTake { get; set; }

        [BsonElement(MarginFieldName)]
        public decimal Margin { get; set; }

        [BsonElement(ExchangeRateFieldName)]
        public decimal ExchangeRate { get; set; }

        [BsonElement(CostPriceFieldName)]
        public Money CostPrice { get; set; }

        [BsonElement(SalePriceFieldName)]
        public Money SalePrice { get; set; }

        [BsonElement(PriceBreakdownFieldName)]
        public AccommodationResultPriceItem[] PriceBreakdown { get; set; }

        [BsonElement(PriceFieldName)]
        public Money Price { get; set; }

        [BsonElement(PricePerNightFieldName)]
        public Money PricePerNight { get; set; }

        [BsonElement(PricePerPersonFieldName)]
        public Money PricePerPerson { get; set; }

        [BsonElement(PriceHikeFieldName)]
        public PriceHikeDetail PriceHikeDetail { get; set; }

        [BsonElement(CancellationPoliciesFieldName)]
        public string[] CancellationPolicies { get; set; }

        [BsonElement(SpecificDataFieldName)]
        public Dictionary<string,string> SpecificData { get; set; }

        [BsonElement(CountryCodeFieldName)]
        public string CountryCode { get; set; }

        public PromotionalDiscountResult DiscountPrice { get; set; }

        [BsonElement(IsEligibleForDepositFieldName)]
        public bool IsEligibleForDeposit { get; set; }
		
		[BsonElement(IsActiveAccommodationAdminFeeFieldName)]
        public bool IsActiveAccommodationAdminFee { get; set; }

        [BsonElement(AccommodationAdminFeeFieldName)]
        public Money AccommodationAdminFee { get; set; }
    }
}
