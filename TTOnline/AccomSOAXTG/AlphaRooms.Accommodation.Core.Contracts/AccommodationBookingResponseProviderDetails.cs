﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingResponseProviderDetails
    {
        public AccommodationProvider Provider { get; set; }
        public BookingRoomValuationResult[] BookingRequestRooms { get; set; }
        public AccommodationProviderRequest[] ProviderRequests { get; set; }
        public IList<AccommodationBookingResultRoom> Results { get; set; }
        public int? ResultsCount { get; set; }
        public TimeSpan? TimeTaken { get; set; }
        public Exception Exception { get; set; }
    }
}
