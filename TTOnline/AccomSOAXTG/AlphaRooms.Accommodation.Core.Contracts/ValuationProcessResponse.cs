using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class ValuationProcessResponse
    {
        public IList<AccommodationValuationResultRoom> Results { get; set; }
        public AccommodationProviderRequest[] ProviderRequests { get; set; }
        public Exception Exception { get; set; }
        public TimeSpan TimeTaken { get; set; }
    }
}
