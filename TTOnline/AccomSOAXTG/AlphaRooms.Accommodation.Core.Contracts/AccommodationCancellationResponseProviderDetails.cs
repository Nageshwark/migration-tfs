﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationCancellationResponseProviderDetails
    {
        public CancellationRoomBookingResult[] CancellationRequestRooms { get; set; }
        public Exception Exception { get; set; }
        public AccommodationProvider Provider { get; set; }
        public int? ResultsCount { get; set; }
        public TimeSpan? TimeTaken { get; set; }
    }
}
