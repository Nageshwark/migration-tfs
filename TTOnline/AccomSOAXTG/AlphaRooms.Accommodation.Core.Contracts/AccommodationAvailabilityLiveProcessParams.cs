﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityLiveProcessParams : AccommodationAvailabilityProcessParamsBase
    {
        public AccommodationProvider Provider { get; set; }
        public Dictionary<string, AccommodationSupplier> Suppliers { get; set; }
        public AvailabilitySearchMode SearchMode { get; set; }
        public Dictionary<string, EstablishmentMapping> EstablishmentMappings { get; set; }
        public string[] ProviderDestinationCodes { get; set; }
        public string[] ProviderEstablishmentCodes { get; set; }
        public byte ProcessId { get; set; }
        public string MetaToken { get; set; }
    }
}
