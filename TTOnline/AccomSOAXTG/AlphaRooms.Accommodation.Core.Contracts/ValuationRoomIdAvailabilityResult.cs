﻿namespace AlphaRooms.Accommodation.Core.Contracts
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;

    public class ValuationRoomIdAvailabilityResult
    {
        public byte RoomNumber { get; set; }
        public byte ProviderRoomNumber { get; set; }
        public string RoomId { get; set; }
        public string RoomKey { get; set; }
        public AccommodationAvailabilityResultRoomIdWrapper SelectedRoomDetails { get; set; }
        public AccommodationAvailabilityResult AvailabilityResult { get; set; }
        public AccommodationProvider Provider { get; set; }
        public AccommodationSupplier Supplier { get; set; }
        public bool IsSelectedRoomRecovered { get; set; }
    }
}
