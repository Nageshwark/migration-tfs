﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationCancellationResponse
    {
        public AccommodationCancellationResponseProviderDetails[] ProcessResponseDetails { get; set; }
        public AccommodationCancellationResult Result { get; set; }
    }
}
