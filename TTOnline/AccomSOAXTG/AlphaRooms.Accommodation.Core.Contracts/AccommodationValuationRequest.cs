﻿namespace AlphaRooms.Accommodation.Core.Contracts
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.SOACommon.DomainModels;
    using System;

    public class AccommodationValuationRequest
    {
        public Guid AvailabilityId { get; set; }
        public Guid ValuationId { get; set; }
        public B2BUser Agent { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public string[] SelectedRoomIds { get; set; }
        public bool Debugging { get; set; }
        public string PromotionalCode { get; set; }
    }
}