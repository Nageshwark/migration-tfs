﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class ProviderAge
    {
        public AccommodationProvider Provider { get; set; }
        public byte Adults { get; set; }
        public byte Children { get; set; }
        public byte Infants { get; set; }
    }
}
