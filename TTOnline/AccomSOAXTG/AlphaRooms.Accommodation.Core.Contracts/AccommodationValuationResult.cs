﻿using AlphaRooms.Cache.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationValuationResult : ICacheExpire
    {
        public const string AvailabilityIdFieldName = "AvI";
        public const string RoomsFieldName = "Rms";
        public const string CacheExpireDateFieldName = "CED";

        [BsonId]
        public Guid ValuationId { get; set; }

        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [BsonElement(RoomsFieldName)]
        public AccommodationValuationResultRoom[] Rooms { get; set; }

        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }
    }
}
