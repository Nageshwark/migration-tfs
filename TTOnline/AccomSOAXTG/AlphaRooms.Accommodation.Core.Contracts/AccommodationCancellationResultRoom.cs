﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationCancellationResultRoom
    {
        public AccommodationProvider Provider { get; set; }
        public AccommodationProviderCancellationResult ProviderCancellationResult { get; set; }
        public byte RoomNumber { get; set; }
        public string ProviderEdiCode { get; set; }
    }
}
