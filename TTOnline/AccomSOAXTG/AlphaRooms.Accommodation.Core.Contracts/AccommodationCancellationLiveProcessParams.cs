﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationCancellationLiveProcessParams
    {
        public AccommodationCancellationRequest CancellationRequest { get; set; }
        public CancellationRoomBookingResult[] CancellationRequestRooms { get; set; }
        public byte ProcessId { get; set; }
        public AccommodationProvider Provider { get; set; }
        public CancellationProcessResponse Response { get; set; }
    }
}
