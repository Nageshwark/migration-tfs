﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class TTAccommodationBookingRequest : AccommodationBookingRequest
    {
        public TTAccommodationBookingRequestRoomPaymentDetails paymentDetails { get; set; }
    }
}
