﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityRequestRoom
    {
        public byte RoomNumber { get; set; }
        public AccommodationAvailabilityRequestRoomGuest[] Guests { get; set; }
    }
}
