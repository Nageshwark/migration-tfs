﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationValuationResponse
    {
        public AccommodationValuationResult Result { get; set; }
        public AccommodationValuationResponseProviderDetails[] ProcessResponseDetails { get; set; }
    }
}
