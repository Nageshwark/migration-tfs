﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Cache.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]    
    public class AccommodationAvailabilityCachedRequest : ICacheExpire
    {
        public const string ProviderIdFieldName = "PId";
        public const string AvailabilityRequestIdFieldName = "ARI";
        public const string CacheExpireDateFieldName = "CED";

        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityRequestIdFieldName)]
        public Guid Id { get; set; }

        [BsonIgnore]
        public AccommodationProvider Provider { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderIdFieldName)]
        public int ProviderId { get; set; }
        
        [BsonId]
        public string AvailabilityKey { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }
    }
}
