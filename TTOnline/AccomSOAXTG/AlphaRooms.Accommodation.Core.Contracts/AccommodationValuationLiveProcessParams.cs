﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationValuationLiveProcessParams
    {
        public AccommodationValuationRequest ValuationRequest { get; set; }
        public AccommodationProvider Provider { get; set; }
        public ValuationRoomIdAvailabilityResult[] ValuationRequestRooms { get; set; }
        public ValuationProcessResponse Response { get; set; }
        public byte ProcessId { get; set; }
    }
}
