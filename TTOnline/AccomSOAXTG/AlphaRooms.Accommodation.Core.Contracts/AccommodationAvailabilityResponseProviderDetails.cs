﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityResponseProviderDetails
    {
        public AccommodationProvider[] Providers { get; set; }

        public byte[] RoomNumbers { get; set; }

        public bool IsLiveResults { get; set; }

        public int? ResultsCount { get; set; }

        public TimeSpan? TimeTaken { get; set; }

        public Exception Exception { get; set; }
    }
}
