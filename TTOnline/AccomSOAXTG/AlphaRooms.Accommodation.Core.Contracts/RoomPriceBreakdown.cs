﻿using System;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [Serializable]
    public class RoomPriceBreakdown
    {
        public string Description { get; set; }
        public Money Amount { get; set; }
        public PriceType  PriceType { get; set; }
    }
}