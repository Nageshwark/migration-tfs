﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityRequest
    {
        public Guid AvailabilityId { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public B2BUser Agent { get; set; }
        public AccommodationProvider[] Providers { get; set; }
        public SearchType SearchType { get; set; }
        public Guid? DestinationId { get; set; }
        public Guid? EstablishmentId { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public AccommodationAvailabilityRequestRoom[] Rooms { get; set; }
        public bool IgnoreConsolidation { get; set; }
        public string Voucher { get; set; }
        public bool Debugging { get; set; }
        public bool ProviderCacheEnabled { get; set; }
        public string MetaToken { get; set; }
}
}
