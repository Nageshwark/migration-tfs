﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingRequestRoomGuest
    {
        public int Id { get; set; }
        public string TitleString { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
    }
}
