﻿namespace AlphaRooms.Accommodation.Core.Contracts
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using MongoDB.Bson.Serialization.Attributes;
    using System;

    [BsonIgnoreExtraElements]
    public class AccommodationAvailabilityResult : ICacheExpire, IAccommodationResultRoom, ICloneable
    {
        private const string RoomKeyFieldName = "RKy";
        private const string AvailabilityRequestIdFieldName = "ARI";
        private const string ProviderIdFieldName = "PvI";
        private const string SupplierIdFieldName = "SpI";
        private const string SourceFieldName = "Src";
        private const string BaseDestinationIdFieldName = "BDI";
        private const string DestinationIdFieldName = "DsI";
        private const string EstablishmentIdFieldName = "EsI";
        private const string EstablishmentStarRatingFieldName = "ESR";
        private const string EstablishmentChainIdFieldName = "ECI";
        private const string RoomDescriptionFieldName = "RmD";
        private const string RoomDescriptionKeyFieldName = "RDK";
        private const string BoardTypeFieldName = "BTp";
        private const string ProviderResultFieldName = "PRs";
        private const string SessionExpireDateFieldName = "SED";
        private const string CacheExpireDateFieldName = "CED";
        private const string IsDuplicateRateFieldName = "IDR";
        private const string CountryCodeFieldName = "CC";
        private const string DiscountPriceFieldName = "Dis";
        private const string CostPriceFieldName = "CP";
        private const string SalePriceFieldName = "SP";
        private const string PriceFieldName = "Pr";
        private const string IsEligibleForDepositFieldName = "EDP";
		private const string IsActiveAccommodationAdminFeeFieldName = "AAFN";
        private const string AccommodationAdminFeeFieldName = "AAF";

        [BsonId]
        public Guid Id { get; set; }

        [BsonIgnore]
        public string RoomId { get; set; }

        [BsonElement(RoomKeyFieldName)]
        [BsonIgnoreIfDefault]
        public string RoomKey { get; set; }

        [BsonElement(AvailabilityRequestIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid AvailabilityRequestId { get; set; }

        [BsonElement(ProviderIdFieldName)]
        [BsonIgnoreIfDefault]
        public int ProviderId { get; set; }

        [BsonIgnore]
        public AccommodationProvider Provider { get; set; }

        [BsonElement(SupplierIdFieldName)]
        [BsonIgnoreIfDefault]
        public int SupplierId { get; set; }

        [BsonIgnore]
        public AccommodationSupplier Supplier { get; set; }

        [BsonElement(SourceFieldName)]
        [BsonIgnoreIfDefault]
        public SearchResultSource Source { get; set; }

        [BsonElement(BaseDestinationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid BaseDestinationId { get; set; }

        [BsonElement(DestinationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid DestinationId { get; set; }

        [BsonElement(EstablishmentIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid EstablishmentId { get; set; }

        [BsonElement(EstablishmentStarRatingFieldName)]
        [BsonIgnoreIfDefault]
        public StarRatingType EstablishmentStarRating { get; set; }

        [BsonElement(EstablishmentChainIdFieldName)]
        [BsonIgnoreIfDefault]
        public int? EstablishmentChainId { get; set; }

        [BsonIgnore]
        public byte RoomNumber { get; set; }

        [BsonElement(RoomDescriptionFieldName)]
        [BsonIgnoreIfDefault]
        public string RoomDescription { get; set; }

        [BsonElement(RoomDescriptionKeyFieldName)]
        [BsonIgnoreIfDefault]
        public string RoomDescriptionKey { get; set; }

        [BsonElement(BoardTypeFieldName)]
        [BsonIgnoreIfDefault]
        public BoardType BoardType { get; set; }

        [BsonIgnore]
        public Money PaymentToTake { get; set; }

        [BsonElement(ProviderResultFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationProviderAvailabilityResult ProviderResult { get; set; }

        [BsonIgnore]
        IAccommodationProviderResultRoom IAccommodationResultRoom.ProviderResult { get { return this.ProviderResult; } }

        [BsonElement(SessionExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime? SessionExpireDate { get; set; }

        [BsonElement(CacheExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CacheExpireDate { get; set; }

        [BsonIgnore]
        public byte AdultsCount { get; set; }

        [BsonIgnore]
        public byte ChildrenCount { get; set; }

        [BsonIgnore]
        public byte InfantsCount { get; set; }

        [BsonIgnore]
        public decimal ExchangeRate { get; set; }

        [BsonIgnore]
        public decimal Margin { get; set; }

        [BsonElement(CostPriceFieldName)]
        [BsonIgnoreIfDefault]
        public Money CostPrice { get; set; }

        [BsonElement(SalePriceFieldName)]
        [BsonIgnoreIfDefault]
        public Money SalePrice { get; set; }

        [BsonIgnore]
        public AccommodationResultPriceItem[] PriceBreakdown { get; set; }

        [BsonElement(DiscountPriceFieldName)]
        [BsonIgnoreIfDefault]
        public PromotionalDiscountResult DiscountPrice { get; set; }

        [BsonIgnore]
        public Money Price { get; set; }

        [BsonIgnore]
        public Money PricePerNight { get; set; }

        [BsonIgnore]
        public bool IsLowestPrice { get; set; }

        [BsonIgnore]
        public Money HighestPrice { get; set; }

        [BsonElement(IsDuplicateRateFieldName)]
        [BsonIgnoreIfDefault]
        public bool IsDuplicateRate { get; set; }

        [BsonElement(CountryCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string CountryCode { get; set; }
		
		[BsonIgnoreIfDefault]
		[BsonElement(IsActiveAccommodationAdminFeeFieldName)]
        public bool IsActiveAccommodationAdminFee { get; set; }

        [BsonElement(AccommodationAdminFeeFieldName)]
        public Money AccommodationAdminFee { get; set; }

        [BsonElement(IsEligibleForDepositFieldName)]
        [BsonIgnoreIfDefault]
        public bool IsEligibleForDeposit { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return this.RoomDescription + " " + this.Price;
        }
    }
}