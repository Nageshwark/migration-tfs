﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationValuationResponseProviderDetails
    {
        public AccommodationProvider Provider { get; set; }
        public ValuationRoomIdAvailabilityResult[] ValuationRequestRooms { get; set; }
        public IList<AccommodationValuationResultRoom> Results { get; set; }
        public int? ResultsCount { get; set; }
        public AccommodationProviderRequest[] ProviderRequests { get; set; }
        public TimeSpan? TimeTaken { get; set; }
        public Exception Exception { get; set; }
    }
}
