﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public interface IAccommodationResultRoom
    {
        AccommodationProvider Provider { get; set; }

        AccommodationSupplier Supplier { get; set; }

        byte AdultsCount { get; set; }

        byte ChildrenCount { get; set; }

        byte InfantsCount { get; set; }

        Guid BaseDestinationId { get; set; }

        Guid DestinationId { get; set; }

        Guid EstablishmentId { get; set; }

        int? EstablishmentChainId { get; set; }

        StarRatingType EstablishmentStarRating { get; set; }

        byte RoomNumber { get; set; }

        string RoomDescription { get; set; }

        BoardType BoardType { get; set; }

        Money PaymentToTake { get; set; }

        decimal Margin { get; set; }

        decimal ExchangeRate { get; set; }

        Money CostPrice { get; set; }

        Money SalePrice { get; set; }

        AccommodationResultPriceItem[] PriceBreakdown { get; set; }

        Money Price { get; set; }

        Money PricePerNight { get; set; }
		
        IAccommodationProviderResultRoom ProviderResult { get; }
		
        string CountryCode { get; set; }

        PromotionalDiscountResult DiscountPrice { get; set; }
		
		bool IsActiveAccommodationAdminFee { get; set; }
		
        Money AccommodationAdminFee { get; set; }
    }
}
