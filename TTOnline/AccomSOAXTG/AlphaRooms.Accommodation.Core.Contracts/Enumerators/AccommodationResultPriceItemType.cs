﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts.Enumerators
{
    public enum AccommodationResultPriceItemType
    {
        BasePrice
        , AlpharoomsCommission
        , VolumeDiscount
        , Markup
        , PriceAdjustment
        , Other
        , BaseAndAdditionPrice
        , AgentCommission
        , Vat
        , TotalPrice
        , SellingPrice
        , PromotionalDiscount
        , AccommodationAdminFee
    }
}
