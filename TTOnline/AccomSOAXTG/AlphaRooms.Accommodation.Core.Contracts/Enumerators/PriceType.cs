﻿namespace AlphaRooms.Accommodation.Core.Contracts.Enumerators
{
    public enum PriceType
    {
        Net=1,
        Vat=2,
        Commission=3,
    }
}