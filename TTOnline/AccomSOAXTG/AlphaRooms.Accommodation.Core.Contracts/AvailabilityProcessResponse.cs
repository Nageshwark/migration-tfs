﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AvailabilityProcessResponse
    {
        public IList<AccommodationAvailabilityResult> Results { get; set; }
        public bool IsLiveResults { get; set; }        
        public Exception Exception { get; set; }
        public TimeSpan TimeTaken { get; set; }
    }
}
