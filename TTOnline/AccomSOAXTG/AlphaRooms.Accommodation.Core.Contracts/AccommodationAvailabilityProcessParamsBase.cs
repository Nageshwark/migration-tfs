﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationAvailabilityProcessParamsBase
    {
        public AccommodationAvailabilityRequest AvailabilityRequest { get; set; }
        public AvailabilityRoomAvailabilityKey[] AvailabilityRequestRooms { get; set; }
        public Guid BaseDestinationId { get; set; }
        public AvailabilityProcessResponse Response { get; set; }
    }
}
