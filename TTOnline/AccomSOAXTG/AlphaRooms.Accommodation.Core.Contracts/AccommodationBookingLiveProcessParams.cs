﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingLiveProcessParams
    {
        public AccommodationBookingRequest BookingRequest { get; set; }
        public BookingRoomValuationResult[] BookingRequestRooms { get; set; }
        public byte ProcessId { get; set; }
        public AccommodationProvider Provider { get; set; }
        public BookingProcessResponse Response { get; set; }
    }
}
