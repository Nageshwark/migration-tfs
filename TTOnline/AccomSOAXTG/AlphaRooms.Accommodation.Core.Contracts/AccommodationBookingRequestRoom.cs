﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingRequestRoom
    {
        public string ValuatedRoomId { get; set; }
        public AccommodationBookingRequestRoomGuest[] Guests { get; set; }
        public AccommodationBookingRequestRoomSpecialRequest SpecialRequests { get; set; }
        public AccommodationBookingRequestRoomPaymentDetails PaymentDetails { get; set; }
    }
}
