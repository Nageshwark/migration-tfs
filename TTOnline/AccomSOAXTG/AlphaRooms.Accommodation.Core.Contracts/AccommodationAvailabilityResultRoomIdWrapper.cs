﻿namespace AlphaRooms.Accommodation.Core.Contracts
{
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using System;

    public class AccommodationAvailabilityResultRoomIdWrapper
    {
        private readonly Channel channel;
        private readonly Guid establishmentId;
        private readonly int providerId;
        private readonly int supplierId;
        private readonly DateTime checkInDate;
        private readonly DateTime checkOutDate;
        private readonly byte roomNumber;
        private readonly byte[] guestAges;
        private readonly string providerRoomCode;
        private readonly string providerRoomDescription;
        private readonly string providerBoardCode;
        private readonly string providerBoardDescription;
        private readonly bool providerIsBindingRate;
        private readonly bool providerIsNonRefundable;
        private readonly bool providerIsOpaqueRate;
        private readonly RateType providerRateType;
        private readonly PaymentModel providerPaymentModel;
        private readonly Money providerCostPrice;
        private readonly Guid roomId;

        public AccommodationAvailabilityResultRoomIdWrapper(
            int providerId,
            int supplierId,
            Channel channel,
            Guid establishmentId,
            DateTime checkInDate,
            DateTime checkOutDate,
            byte roomNumber,
            byte[] guestAges,
            string providerRoomCode,
            string providerRoomDescription,
            string providerBoardCode,
            string providerBoardDescription,
            bool providerIsBindingRate,
            bool providerIsNonRefundable,
            bool providerIsOpaqueRate,
            RateType providerRateType,
            PaymentModel providerPaymentModel,
            Money providerCostPrice,
            Guid roomId)
        {
            this.providerId = providerId;
            this.supplierId = supplierId;
            this.channel = channel;
            this.establishmentId = establishmentId;
            this.checkInDate = checkInDate;
            this.checkOutDate = checkOutDate;
            this.roomNumber = roomNumber;
            this.guestAges = guestAges;
            this.providerRoomCode = providerRoomCode;
            this.providerRoomDescription = providerRoomDescription;
            this.providerBoardCode = providerBoardCode;
            this.providerBoardDescription = providerBoardDescription;
            this.providerIsBindingRate = providerIsBindingRate;
            this.providerIsNonRefundable = providerIsNonRefundable;
            this.providerIsOpaqueRate = providerIsOpaqueRate;
            this.providerRateType = providerRateType;
            this.providerPaymentModel = providerPaymentModel;
            this.providerCostPrice = providerCostPrice;
            this.roomId = roomId;
        }

        public Channel Channel { get { return this.channel; } }
        public Guid EstablishmentId { get { return this.establishmentId; } }
        public int ProviderId { get { return this.providerId; } }
        public int SupplierId { get { return this.supplierId; } }
        public DateTime CheckInDate { get { return this.checkInDate; } }
        public DateTime CheckOutDate { get { return this.checkOutDate; } }
        public byte RoomNumber { get { return this.roomNumber; } }
        public byte[] GuestAges { get { return this.guestAges; } }
        public string ProviderRoomCode { get { return this.providerRoomCode; } }
        public string ProviderRoomDescription { get { return this.providerRoomDescription; } }
        public string ProviderBoardCode { get { return this.providerBoardCode; } }
        public string ProviderBoardDescription { get { return this.providerBoardDescription; } }
        public bool ProviderIsBindingRate { get { return this.providerIsBindingRate; } }
        public bool ProviderIsNonRefundable { get { return this.providerIsNonRefundable; } }
        public bool ProviderIsOpaqueRate { get { return this.providerIsOpaqueRate; } }
        public RateType ProviderRateType { get { return this.providerRateType; } }
        public PaymentModel ProviderPaymentModel { get { return this.providerPaymentModel; } }
        public Money ProviderCostPrice { get { return this.providerCostPrice; } }
        public Guid RoomId { get { return this.roomId; } }
    }
}