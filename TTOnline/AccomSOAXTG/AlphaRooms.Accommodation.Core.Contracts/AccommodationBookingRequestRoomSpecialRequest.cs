﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    public class AccommodationBookingRequestRoomSpecialRequest
    {
        public bool LateArrival { get; set; }
        public bool CotRequired { get; set; }
        public bool SeaViews { get; set; }
        public bool AdjoiningRooms { get; set; }
        public bool NonSmoking { get; set; }
        public bool DisabledAccess { get; set; }
        public string OtherRequests { get; set; }
    }
}
