﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using MongoDB.Bson.Serialization.Attributes;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationBookingResultRoom
    {
        public const string RoomNumberFieldName = "RmN";
        public const string ProviderIdFieldName = "PCd";
        public const string SupplierIdFieldName = "SpI";
        public const string ProviderBookingResultFieldName = "PBR";
        public const string ProviderBookingReferenceFieldName = "PBF";
        public const string MessageFieldName = "Msg";
        public const string ValuationResultRoomFieldName = "VRR";

        [BsonElement(RoomNumberFieldName)]
        [BsonIgnoreIfDefault]
        public Byte RoomNumber { get; set; }

        [BsonElement(ProviderIdFieldName)]
        [BsonIgnoreIfDefault]
        public int ProviderId { get; set; }

        [BsonIgnore()]
        public AccommodationProvider Provider { get; set; }

        [BsonElement(SupplierIdFieldName)]
        [BsonIgnoreIfDefault]
        public int SupplierId { get; set; }

        [BsonIgnore()]
        public AccommodationSupplier Supplier { get; set; }
        
        [BsonElement(ProviderBookingResultFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationProviderBookingResult ProviderBookingResult { get; set; }

        [BsonElement(ValuationResultRoomFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationValuationResultRoom ValuationResultRoom { get; set; }
    }
}
