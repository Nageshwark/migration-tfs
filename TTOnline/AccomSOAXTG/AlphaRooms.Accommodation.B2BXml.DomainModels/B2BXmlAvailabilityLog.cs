﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.DomainModels
{
    public class B2BXmlAvailabilityLog
    {
        public string HostName { get; set; }

        public Guid AvailabilityId { get; set; }

        public DateTime AvailabilityDate { get; set; }

        public SearchType AvailabilityType { get; set; }

        public Status? Status { get; set; }

        public string Request { get; set; }

        public int? ResultsCount { get; set; }

        public int? EstablishmentResultsCount { get; set; }

        public int? PostProcessTimeTaken { get; set; }

        public int TimeTaken { get; set; }

        public string Exceptions { get; set; }

        public string Providers { get; set; }

        public string ProviderSearchTypes { get; set; }

        public string ProviderTimeTaken { get; set; }

        public string ProviderResultsCount { get; set; }

        public string ProviderEstablishmentResultsCount { get; set; }

        public string ProviderExceptions { get; set; }
    }
}
