﻿namespace AlphaRooms.Accommodation.Core.DependencyInjection
{
    using AlphaRooms.Accommodation.Core.Caching;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Interfaces.Caching;
    using AlphaRooms.Accommodation.Core.Services;
    using AlphaRooms.Cache.Mongo;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.Configuration;
    using AlphaRooms.SOACommon.DbContexts;
    using AlphaRooms.SOACommon.DomainModels;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.SOACommon.Repositories;
    using AlphaRooms.SOACommon.Services;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.EntityFramework;
    using Ninject;
    using Ninject.Modules;
    using System;
    using AlphaRooms.Accommodation.Core.DbContexts;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
    using AlphaRooms.Accommodation.Core.Repositories;
    using AlphaRooms.Cache.Mongo.Interfaces;
    using Ninject.Extensions.Factory;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.SOACommon.Interfaces.Repositories;
    using AlphaRooms.SOACommon.Services.Factories;
    using B2C.Interfaces;
    using B2C.Services;
    using System.Configuration;

    public class CoreDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            bool IsNewDepositSchemaEnabled = ConfigurationManager.AppSettings["DepositSchemaVersion"] != null ? string.Compare(ConfigurationManager.AppSettings["DepositSchemaVersion"], "V1", true) == 0 : false;
            // soacommon
            Bind<IDbContextActivator<SOACommonContext>>().To<NoProxyDbContextActivator<SOACommonContext>>().InSingletonScope();
            Bind<IDbContextActivator<SOACommonLogsContext>>().To<NoProxyDbContextActivator<SOACommonLogsContext>>().InSingletonScope();
            Bind<ICacheProvider<Parameter>>().ToConstant(new CacheProvider<Parameter>(TimeSpan.Parse("00:01:00")));
            Bind<IParameterRepository>().To<ParameterRepository>().InSingletonScope();
            Bind<IAccommodationConfigurationManager>().To<AccommodationConfigurationManager>().InSingletonScope();
            var configurationManager = Kernel.Get<AccommodationConfigurationManager>();
            Bind<ICacheProviderSettings>().ToConstant(new CacheProviderSettings(null, configurationManager.AccommodationRepositoryIsCachingEnabled
                , configurationManager.AccommodationRepositoryCacheExpireTime
                , configurationManager.AccommodationRepositoryCachingTimeout
                , configurationManager.AccommodationRepositoryCommandTimeout)).InSingletonScope();
            Rebind<ICacheProvider<Parameter>>().To<CacheProvider<Parameter>>().InSingletonScope();
            Bind<IMongoDbConnection>().To<MongoDbConnection>().InSingletonScope();
            Bind<IMongoDbSettings>().ToConstant(configurationManager).InSingletonScope();

            Bind<ICacheProviderAsync<ChannelInfo>>().To<CacheProviderAsync<ChannelInfo>>().InSingletonScope();
            Bind<IChannelInfoRepository>().To<ChannelInfoRepository>().InSingletonScope();
            Bind<IChannelInfoService>().To<ChannelInfoService>().InSingletonScope();
            Bind<ICacheProviderAsync<ExchangeRate>>().To<CacheProviderAsync<ExchangeRate>>().InSingletonScope();
            Bind<IExchangeRateRepository>().To<ExchangeRateRepository>().InSingletonScope();
            Bind<IExchangeRateService>().To<ExchangeRateService>().InSingletonScope();
            Bind<ICacheProviderAsync<PriceHikeThreshold>>().To<CacheProviderAsync<PriceHikeThreshold>>().InSingletonScope();

            // database
            Bind<IDbContextActivator<MarkupServiceDbContext>>().To<NoProxyDbContextActivator<MarkupServiceDbContext>>().InSingletonScope();
           
          
            Bind<IDbContextActivator<PriceHikeDbContext>>().To<NoProxyDbContextActivator<PriceHikeDbContext>>().InSingletonScope();
            //Repositories
            Bind<IHotelMarginDiscountRepository>().To<HotelMarginDiscountRepository>().InSingletonScope();
          
          
            Bind<ICacheProviderAsync<HotelMarginDiscount>>().To<CacheProviderAsync<HotelMarginDiscount>>().InSingletonScope();
            Bind<IPriceHikeRepository>().To<PriceHikeRepository>().InSingletonScope();
            Bind<IPromotionalDiscountRepository>().To<PromotionalDiscountRepository>().InSingletonScope();

            // caching
            Bind<ICacheMultiDatabaseController<AccommodationAvailabilityResult, Guid>>().To<AvailabilityResultDatabaseController>().InSingletonScope();
            Bind<IMongoDbMultiDbCollection<AccommodationAvailabilityResult, Guid>>().ToConstant(new MongoDbMultiDbCollection<AccommodationAvailabilityResult, Guid>(
                new MongoDbMultiDbCollectionSettings(configurationManager.AccommodationAvailabilityResultsDatabase
                    , configurationManager.AccommodationAvailabilityResultsCollection
                    , configurationManager.AccommodationAvailabilityResultsMultiDbCount
                    , configurationManager.AccommodationAvailabilityResultsMultiDbIsEnabled)
                    , Kernel.Get<IMongoDbConnection>(), Kernel.Get<ICacheMultiDatabaseController<AccommodationAvailabilityResult, Guid>>())).InSingletonScope();
            Bind<ICacheMultiDbQueryExpireAsync<AccommodationAvailabilityResult, Guid>>().To<MongoDbMultiDbQuery<AccommodationAvailabilityResult, Guid>>().InSingletonScope();
            Bind<ICacheMultiDbWriterExpireAsync<AccommodationAvailabilityResult, Guid>>().To<MongoDbMultiDbWriter<AccommodationAvailabilityResult, Guid>>().InSingletonScope();
            Bind<IAvailabilityResultCaching>().To<AvailabilityResultCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationAvailabilityCachedRequest>>().ToConstant(new MongoDbCollection<AccommodationAvailabilityCachedRequest>(
                new MongoDbCollectionSettings(configurationManager.AccommodationCachedRequestDatabase
                    , configurationManager.AccommodationCachedRequestCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationAvailabilityCachedRequest>>().To<MongoDbQuery<AccommodationAvailabilityCachedRequest>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationAvailabilityCachedRequest>>().To<MongoDbWriter<AccommodationAvailabilityCachedRequest>>().InSingletonScope();
            Bind<IAvailabilityCachedRequestCaching>().To<AvailabilityCachedRequestCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationValuationResult>>().ToConstant(new MongoDbCollection<AccommodationValuationResult>(
                new MongoDbCollectionSettings(configurationManager.AccommodationValuationResultsDatabase
                    , configurationManager.AccommodationValuationResultsCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationValuationResult>>().To<MongoDbQuery<AccommodationValuationResult>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationValuationResult>>().To<MongoDbWriter<AccommodationValuationResult>>().InSingletonScope();
            Bind<IValuationResultCaching>().To<ValuationResultCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationBookingResult>>().ToConstant(new MongoDbCollection<AccommodationBookingResult>(
                new MongoDbCollectionSettings(configurationManager.AccommodationBookingResultsDatabase
                    , configurationManager.AccommodationBookingResultsCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationBookingResult>>().To<MongoDbQuery<AccommodationBookingResult>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationBookingResult>>().To<MongoDbWriter<AccommodationBookingResult>>().InSingletonScope();
            Bind<IBookingResultCaching>().To<BookingResultCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationCancellationResult>>().ToConstant(new MongoDbCollection<AccommodationCancellationResult>(
                new MongoDbCollectionSettings(configurationManager.AccommodationCancellationResultsDatabase
                    , configurationManager.AccommodationCancellationResultsCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationCancellationResult>>().To<MongoDbQuery<AccommodationCancellationResult>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationCancellationResult>>().To<MongoDbWriter<AccommodationCancellationResult>>().InSingletonScope();
            Bind<ICancellationResultCaching>().To<CancellationResultCaching>().InSingletonScope();

            // shared services
            Bind<IAgeRuleService>().To<AgeRuleService>().InSingletonScope();
            Bind<IPriceCalculatorService>().To<PriceCalculatorService>().InSingletonScope();
            Bind<IBlockedDestinationService>().To<BlockedDestinationService>().InSingletonScope();
            Bind<IDestinationSearchModeService>().To<DestinationSearchModeService>().InSingletonScope();
            Bind<IRoomKeyGenerator>().To<RoomKeyGenerator>().InSingletonScope();

            Bind<IRoomDescriptionComparer>().To<RoomDescriptionComparer>().InSingletonScope();
            Bind<IRoomDescriptionKeyGeneration>().To<RoomDescriptionKeyGeneration>().InSingletonScope();
            Bind<IRoomDescriptionComparerFactory>().To<RoomDescriptionComparerFactory>().InSingletonScope();
            Bind<IRoomDescriptionGenerator>().To<RoomDescriptionGenerator>().InSingletonScope();
            Bind<IPriceHikingService>().To<PriceHikingService>().InSingletonScope();
            Bind<IPriceHikingFactory>().To<PriceHikingFactory>().InSingletonScope();            
            Bind<IRoomSwitchingService>().To<RoomSwitchingService>().InSingletonScope();
            Bind<IPerformanceLoggerService<PerformanceLog>>().To<PerformanceLoggerService>();
            Bind<IPerformanceLoggerService<ProviderPerformanceLog>>().To<ProviderPerformanceLoggerService>();
            Bind<IPromotionalDiscountFactory>().To<PromotionalDiscountFactory>().InSingletonScope();
            Bind<AccommodationPromotionalDiscountService>().ToSelf().InSingletonScope();
            Bind<IPromotionalDiscountService>().To<PromotionalDiscountService>().InSingletonScope();

            // availability services
            Bind<IAvailabilityService>().To<AvailabilityService>();
            Bind<IAvailabilityServiceFactory>().ToFactory().InSingletonScope();
            Bind<IAvailabilityLiveProcessService>().To<AvailabilityService>().InSingletonScope();
            Bind<IAvailabilityCachedRequestService>().To<AvailabilityCachedRequestService>().InSingletonScope();
            Bind<IAvailabilityKeyGenerator>().To<AvailabilityKeyGenerator>().InSingletonScope();
            Bind<IAvailabilityRequestFactory>().To<AvailabilityRequestFactory>().InSingletonScope();
            Bind<IAvailabilityProcessParamService>().To<AvailabilityProcessParamService>().InSingletonScope();
            Bind<IAvailabilityResultMappingService>().To<AvailabilityResultMappingService>().InSingletonScope();
            Bind<IAvailabilityResultAggregatorService>().To<AvailabilityResultAggregatorService>().InSingletonScope();
            Bind<IAvailabilityWinEventLogger>().To<AvailabilityWinEventLogger>().InSingletonScope();
            Bind<IAvailabilityResultRecoveryService>().To<AvailabilityResultRecoveryService>().InSingletonScope();
            Bind<IAvailabilityRequestRoomService>().To<AvailabilityRequestRoomService>().InSingletonScope();
            Bind<IAvailabilityResultDuplicationService>().To<AvailabilityResultDuplicationService>().InSingletonScope();
            Bind<IAvailabilityResultHighestPriceService>().To<AvailabilityResultHighestPriceService>().InSingletonScope();
            Bind<IAvailabilityResultRoomIdGenerator>().To<AvailabilityResultRoomIdGenerator>().InSingletonScope();
            Bind<IAvailabilityAlternativeSearchService>().To<AvailabilityAlternativeSearchService>();
            Bind<IAvailabilityResultProviderPriorityService>().To<AvailabilityResultProviderPriorityService>().InSingletonScope();
			Bind<IB2CEventLogInfo>().To<B2CEventLogInfo>().InSingletonScope();

			// valuation services
			Bind<IValuationService>().To<ValuationService>();
            Bind<IValuationLiveProcessParamService>().To<ValuationLiveProcessParamService>().InSingletonScope();
            Bind<IValuationRequestFactory>().To<ValuationRequestFactory>().InSingletonScope();
            Bind<IValuationResultMappingService>().To<ValuationResultMappingService>().InSingletonScope();
            Bind<IValuationResultBuilderService>().To<ValuationResultBuilderService>().InSingletonScope();
            Bind<IValuationRequestRoomService>().To<ValuationRequestRoomService>().InSingletonScope();
            //New DepositSchema Based on settings
            if (!IsNewDepositSchemaEnabled){
                Bind<IDepositSchemeService>().To<DepositSchemeService>().InSingletonScope();
                Bind<IValuationCancellationPolicyService>().To<ValuationCancellationPolicyService>().InSingletonScope();
                Bind<IDbContextActivator<DepositSchemeDbContext>>().To<NoProxyDbContextActivator<DepositSchemeDbContext>>().InSingletonScope();
                Bind<IDepositSchemeRepository>().To<DepositSchemeRepository>().InSingletonScope();
            } else{
                Bind<IDepositSchemeV1Service>().To<DepositSchemeV1Service>().InSingletonScope();
                Bind<IValuationCancellationPolicyService>().To<ValuationCancellationPolicyServiceV1>().InSingletonScope();
                Bind<IDbContextActivator<DepositSchemeV1DbContext>>().To<NoProxyDbContextActivator<DepositSchemeV1DbContext>>().InSingletonScope();
                Bind<IDepositSchemeV1Repository>().To<DepositSchemeV1Repository>().InSingletonScope();
            }

            // booking services
            Bind<IBookingService>().To<BookingService>().InSingletonScope();
            Bind<IBookingLiveProcessParamService>().To<BookingLiveProcessParamService>().InSingletonScope();
            Bind<IBookingRequestRoomService>().To<BookingRequestRoomService>().InSingletonScope();
            Bind<IBookingRequestFactory>().To<BookingRequestFactory>().InSingletonScope();
            Bind<IBookingResultMappingService>().To<BookingResultMappingService>().InSingletonScope();
            Bind<IBookingResultBuilderService>().To<BookingResultBuilderService>().InSingletonScope();

            // cancellation services
            Bind<ICancellationService>().To<CancellationService>().InSingletonScope();
            Bind<ICancellationLiveProcessParamService>().To<CancellationLiveProcessParamService>().InSingletonScope();
            Bind<ICancellationRequestFactory>().To<CancellationRequestFactory>().InSingletonScope();
            Bind<ICancellationResultMappingService>().To<CancellationResultMappingService>().InSingletonScope();
            Bind<ICancellationResultBuilderService>().To<CancellationResultBuilderService>().InSingletonScope();

            // guidSrevice   
            Bind<ICoreGuidService>().To<CoreGuidService>().InSingletonScope();
		}
    }
}
