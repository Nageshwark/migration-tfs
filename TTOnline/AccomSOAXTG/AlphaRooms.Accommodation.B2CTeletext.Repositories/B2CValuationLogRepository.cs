﻿using AlphaRooms.Accommodation.B2CTeletext.DbContexts;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Repositories
{
    public class B2CValuationLogRepository : IB2CValuationLogRepository
    {
        private readonly IDbContextActivator<B2CLogDbContext> dbLogsContextActivator;

        public B2CValuationLogRepository(IDbContextActivator<B2CLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2CValuationLog[] b2CValuationLogs)
        {
            using (B2CLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2CValuationLogs.AddRange(b2CValuationLogs);
                await context.SaveChangesAsync();
            }
        }
    }
}
