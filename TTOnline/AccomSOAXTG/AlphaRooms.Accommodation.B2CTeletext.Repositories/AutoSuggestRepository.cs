﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using AlphaRooms.Accommodation.B2CTeletext.DbContexts;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.Accommodation.B2CTeletext.Repositories
{
    public class AutoSuggestRepository : IAutoSuggestRepository
    {
        private readonly IDbContextActivator<ContentContext> dbContextActivator;

        public AutoSuggestRepository(IDbContextActivator<ContentContext> dbContextActivator)
        {
            this.dbContextActivator = dbContextActivator;
        }

        public List<AutoSuggest> GetAutoSuggestionsForHotelsAndDestinations(string term, int localeId)
        {
            var contentContext = this.dbContextActivator.GetDbContext();
            var destinationAndHotelsQuery = GetAutoSuggestions(contentContext, term, localeId, AutoSuggestCategory.Destinations, AutoSuggestCategory.Hotels);

            return destinationAndHotelsQuery.ToList();
        }

        private IEnumerable<AutoSuggest> GetAutoSuggestions(ContentContext contentContext, string term, int localeId, AutoSuggestCategory category1, AutoSuggestCategory? category2 = null)
        {
            var parameters = new List<SqlParameter>
                             {
                                 new SqlParameter("@localeId", localeId),
                                 new SqlParameter("@term", term),
                                 new SqlParameter("@category", (int)category1)
                             };

            if (category2.HasValue)
            {
                parameters.Add(new SqlParameter("@category2", (int)category2.Value));
            }

            var paramsSql = string.Join(",", parameters.Select(x => x.ParameterName));

            var query = ((IObjectContextAdapter)contentContext).ObjectContext.ExecuteStoreQuery<AutoSuggest>("TTOnlineAutoSuggest_Lookup " + paramsSql, parameters.ToArray<object>());

            return query.AsEnumerable();
        }
    }
}
