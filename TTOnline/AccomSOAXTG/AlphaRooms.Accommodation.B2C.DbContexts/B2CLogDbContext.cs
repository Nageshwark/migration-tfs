﻿using AlphaRooms.Accommodation.B2C.DbContexts.Mapping;
using AlphaRooms.Accommodation.B2C.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.DbContexts
{
    public class B2CLogDbContext : DbContext
    {
        static B2CLogDbContext()
        {
            Database.SetInitializer<B2CLogDbContext>(null);
        }

        public B2CLogDbContext() : base("Name=AlphabedsLogsEntities")
        {
        }

        public DbSet<B2CValuationLog> B2CValuationLogs { get; set; }
        public DbSet<B2CBookingLog> B2CBookingLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new B2CValuationLogMap());
            modelBuilder.Configurations.Add(new B2CBookingLogMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task B2CAvailabilityLogInsertAsync(string hostName, Guid searchId, DateTime searchDate, byte searchType, byte? status, string request, int? resultsCount
            , int? establishmentResultsCount, string providers, string providerSearchTypes, string providerResultsCount, string providerEstablishmentResultsCount, string providerTimeTaken
            , string providerExceptions, int? postProcessTimeTaken, int? timeTaken, string exception)
        {
            if (hostName == null) throw new ArgumentNullException("hostName");

            await this.ExecuteStoredProcedureAsync("AccommodationAvailabilityLogsInsert"
                , new SqlParameter("hostName", DataModule.DbNullIfNull(hostName))
                , new SqlParameter("searchId", DataModule.DbNullIfNull(searchId))
                , new SqlParameter("StartDate", DataModule.DbNullIfNull(searchDate))
                , new SqlParameter("searchType", DataModule.DbNullIfNull(searchType))
                , new SqlParameter("status", DataModule.DbNullIfNull(status))
                , new SqlParameter("request", DataModule.DbNullIfNull(request))
                , new SqlParameter("providers", DataModule.DbNullIfNull(providers))
                , new SqlParameter("providerSearchTypes", DataModule.DbNullIfNull(providerSearchTypes))
                , new SqlParameter("providerResultsCount", DataModule.DbNullIfNull(providerResultsCount))
                , new SqlParameter("providerEstablishmentResultsCount", DataModule.DbNullIfNull(providerEstablishmentResultsCount))
                , new SqlParameter("providerTimeTaken", DataModule.DbNullIfNull(providerTimeTaken))
                , new SqlParameter("providerExceptions", DataModule.DbNullIfNull(providerExceptions))
                , new SqlParameter("resultsCount", DataModule.DbNullIfNull(resultsCount))
                , new SqlParameter("establishmentResultsCount", DataModule.DbNullIfNull(establishmentResultsCount))
                , new SqlParameter("postProcessTimeTaken", DataModule.DbNullIfNull(postProcessTimeTaken))
                , new SqlParameter("timeTaken", DataModule.DbNullIfNull(timeTaken))
                , new SqlParameter("exception", DataModule.DbNullIfNull(exception))
                );
        }
        
        public async Task B2CValuationLogInsertAsync(string hostName, Guid availabilityId, Guid valuationId, DateTime valuationDate, byte? status, string request, int timeTaken
            , string providers, string providerRoomNumbers, string providerStatuses, string providerTimeTaken, string providerResultsCount, string providerExceptions
            , string exception)
        {
            if (hostName == null) throw new ArgumentNullException("hostName");

            await this.ExecuteStoredProcedureAsync("AccommodationValuationLogsInsert"
                , new SqlParameter("hostName", DataModule.DbNullIfNull(hostName))
                , new SqlParameter("searchId", DataModule.DbNullIfNull(availabilityId))
                , new SqlParameter("valuationId", DataModule.DbNullIfNull(valuationId))
                , new SqlParameter("valuationDate", DataModule.DbNullIfNull(valuationDate))
                , new SqlParameter("status", DataModule.DbNullIfNull(status))
                , new SqlParameter("request", DataModule.DbNullIfNull(request))
                , new SqlParameter("providers", DataModule.DbNullIfNull(providers))
                , new SqlParameter("providerRoomNumbers", DataModule.DbNullIfNull(providerRoomNumbers))
                , new SqlParameter("providerStatuses", DataModule.DbNullIfNull(providerStatuses))
                , new SqlParameter("providerResultsCount", DataModule.DbNullIfNull(providerResultsCount))
                , new SqlParameter("providerTimeTaken", DataModule.DbNullIfNull(providerTimeTaken))
                , new SqlParameter("providerExceptions", DataModule.DbNullIfNull(providerExceptions))
                , new SqlParameter("timeTaken", DataModule.DbNullIfNull(timeTaken))
                , new SqlParameter("exception", DataModule.DbNullIfNull(exception))
                );
        }

        public async Task B2CBookingLogInsertAsync(string hostName, Guid valuationId, int itineraryId, Guid bookingId, DateTime valuationDate, byte? status, string request, int timeTaken
            , string providers, string providerRoomNumbers, string providerStatuses, string providerTimeTaken, string providerResultsCount, string providerExceptions
            , string exception)
        {
            if (hostName == null) throw new ArgumentNullException("hostName");

            await this.ExecuteStoredProcedureAsync("AccommodationBookingLogsInsert"
                , new SqlParameter("hostName", DataModule.DbNullIfNull(hostName))
                , new SqlParameter("valuationId", DataModule.DbNullIfNull(valuationId))
                , new SqlParameter("itineraryId", DataModule.DbNullIfNull(itineraryId))
                , new SqlParameter("bookingId", DataModule.DbNullIfNull(bookingId))
                , new SqlParameter("bookingDate", DataModule.DbNullIfNull(valuationDate))
                , new SqlParameter("status", DataModule.DbNullIfNull(status))
                , new SqlParameter("request", DataModule.DbNullIfNull(request))
                , new SqlParameter("providers", DataModule.DbNullIfNull(providers))
                , new SqlParameter("providerRoomNumbers", DataModule.DbNullIfNull(providerRoomNumbers))
                , new SqlParameter("providerStatuses", DataModule.DbNullIfNull(providerStatuses))
                , new SqlParameter("providerResultsCount", DataModule.DbNullIfNull(providerResultsCount))
                , new SqlParameter("providerTimeTaken", DataModule.DbNullIfNull(providerTimeTaken))
                , new SqlParameter("providerExceptions", DataModule.DbNullIfNull(providerExceptions))
                , new SqlParameter("timeTaken", DataModule.DbNullIfNull(timeTaken))
                , new SqlParameter("exception", DataModule.DbNullIfNull(exception))
                );
        }
    }
}
