﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.WcfInterface.Interface;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface
{
    public class AccommodationB2BWebServices : IAccommodationB2BWebAvailabilityService, IAccommodationB2BWebValuationService, IAccommodationB2BWebBookingService// , IAccommodationCancellationService
    {
        private readonly IB2BWebAvailabilityService availabilityService;
        private readonly IB2BWebValuationService valuationService;
        private readonly IB2BWebBookingService bookingService;
        private readonly IB2BWebCancellationService cancellationService;

        public AccommodationB2BWebServices(IB2BWebAvailabilityService availabilityService, IB2BWebValuationService valuationService, IB2BWebBookingService bookingService
            , IB2BWebCancellationService cancellationService)
        {
            this.availabilityService = availabilityService;
            this.valuationService = valuationService;
            this.bookingService = bookingService;
            this.cancellationService = cancellationService;

            SetSecurityProtocolType();
        }

        private void SetSecurityProtocolType()
        {
            ServicePointManager.SecurityProtocol = 0;
            foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
            {
                switch (protocol)
                {
                    case SecurityProtocolType.Ssl3:
                    case SecurityProtocolType.Tls:
                    case SecurityProtocolType.Tls11:
                        break;
                    case SecurityProtocolType.Tls12:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                }
            }
        }

        public async Task<Guid> StartAccommodationAvailabilitySearchAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest)
        {
            return await availabilityService.StartAvailabilitySearchAsync(availabilityRequest);
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria)
        {
            return await availabilityService.GetAvailabilityResponseAsync(availabilityId, filterCriteria, sortCriteria);
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityByEstablishmentResponseAsync(Guid availabilityId, Guid establishment, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria)
        {
            return await availabilityService.GetAvailabilityResponseByEstablishmentAsync(availabilityId, establishment, filterCriteria, sortCriteria);
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityByRoomsResponseAsync(Guid availabilityId, string[] roomIds)
        {
            return await availabilityService.GetAvailabilityResponseByRoomsAsync(availabilityId, roomIds);
        }

        public async Task<AccommodationB2BWebAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria)
        {
            return await availabilityService.GetAvailabilityMapResponseAsync(availabilityId, filterCriteria);
        }

        public async Task<AccommodationB2BWebAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId)
        {
            return await availabilityService.GetAvailabilityMapResponseByEstablishmentAsync(availabilityId, establishmentId);
        }

        public async Task<AccommodationB2BWebAvailabilityRequest> GetAccommodationAvailabilityRequestAsync(Guid availabilityId)
        {
            return await availabilityService.GetAvailabilityRequestAsync(availabilityId);
        }

        public async Task<AccommodationB2BWebAvailabilityResultsFilterOptions> GetAccommodationAvailabilityResultsFilterOptionsAsync(Guid availabilityId)
        {
            return await availabilityService.GetAvailabilityResultsFilterOptionsAsync(availabilityId);
        }

        public async Task<Guid> StartAccommodationValuationProcessAsync(AccommodationB2BWebValuationRequest valuationRequest)
        {
            return await valuationService.StartValuationAsync(valuationRequest);
        }

        public async Task<AccommodationB2BWebValuationResponse> GetAccommodationValuationResponseAsync(Guid valuationId)
        {
            return await valuationService.GetValuationResponseAsync(valuationId);
        }
        
        public async Task<Guid> StartAccommodationBookingProcessAsync(AccommodationB2BWebBookingRequest bookingRequest)
        {
            return await bookingService.StartBookingAsync(bookingRequest);
        }

        public async Task<AccommodationB2BWebBookingResponse> GetAccommodationBookingResponseAsync(Guid bookingId)
        {
            return await bookingService.GetBookingResponseAsync(bookingId);
        }

        public async Task<Guid> StartAccommodationCancellationProcess(AccommodationB2BWebCancellationRequest cancellationRequest)
        {
            return await cancellationService.StartCancellationProcessAsync(cancellationRequest);
        }

        public async Task<AccommodationB2BWebCancellationResponse> GetAccommodationCancellationResponse(Guid cancellationId)
        {
            return await cancellationService.GetCancellationResponseAsync(cancellationId);
        }
    }
}
