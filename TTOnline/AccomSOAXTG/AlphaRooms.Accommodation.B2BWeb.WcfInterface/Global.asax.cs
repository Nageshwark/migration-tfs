﻿using AlphaRooms.Accommodation.B2B.DependencyInjection;
using AlphaRooms.Accommodation.B2BWeb.DependencyInjection;
using AlphaRooms.Accommodation.Core.DependencyInjection;
using AlphaRooms.Accommodation.Core.Provider.DependencyInjection;
using AlphaRooms.Accommodation.Provider.DependencyInjection;
using AlphaRooms.Cache.Mongo;
using MongoDB.Bson.Serialization;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface
{
    public class Global : NinjectHttpApplication
    {

        protected override IKernel CreateKernel()
        {
            // Register a custom date time serialiser for Mongo which will prevent mongo from saving dates in UTC format.
            // The custom serialiser will force mongo to saved dates and times exactly as they are received, without any changes.
            BsonSerializer.RegisterSerializer(typeof(DateTime), new LocalTimeMongoSerializer());

            // Install ninject modules to register the services
            return new StandardKernel(new CoreDependencyInjectionInstaller(), new CoreProviderDependencyInjectionInstaller(), new ProviderDependencyInjectionInstaller(), new B2BDependencyInjectionInstaller()
                , new B2BWebDependencyInjectionInstaller());
        }

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }        
    }
}