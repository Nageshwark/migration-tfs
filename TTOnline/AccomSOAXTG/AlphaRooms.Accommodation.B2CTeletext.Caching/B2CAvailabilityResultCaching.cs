﻿namespace AlphaRooms.Accommodation.B2CTeletext.Caching
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;
    using AlphaRooms.Accommodation.B2CTeletext.Contracts.Enums;
    using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
    using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Caching;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.SOACommon.Services;
    using AlphaRooms.Utilities;
    using Ninject.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class B2CAvailabilityResultCaching : IB2CAvailabilityResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheMultiDbQueryExpireAsync<AccommodationB2CAvailabilityResult, Guid> cacheQuery;
        private readonly ICacheMultiDbWriterExpireAsync<AccommodationB2CAvailabilityResult, Guid> cacheWriter;
        private readonly ILogger logger;
        private readonly IB2CPaymentModelConverter paymentModelConverter;

        public B2CAvailabilityResultCaching(IAccommodationConfigurationManager configurationManager, ICacheMultiDbQueryExpireAsync<AccommodationB2CAvailabilityResult, Guid> cacheQuery
            , ICacheMultiDbWriterExpireAsync<AccommodationB2CAvailabilityResult, Guid> cacheWriter, ILogger logger
            , IB2CPaymentModelConverter paymentModelConverter)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.logger = logger;
            this.paymentModelConverter = paymentModelConverter;
        }

        public async Task<AccommodationB2CAvailabilityResults> GetFilteredAndSortedAsync(
            Guid availabilityId,
            Guid baseDestinationId,
            int roomCount,
            AccommodationB2CAvailabilityFilter filterCriteria,
            AccommodationB2CAvailabilitySort sortCriteria,
            bool debugging,
            bool isMeta = false)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.SupplierCacheRetrieval, availabilityId.ToString(), "Internal filtered availability", debugging, logger))
            {
                var query = LinqExpression.Create<AccommodationB2CAvailabilityResult>(i => i.AvailabilityId == availabilityId);
                query = this.ApplyQueryFilter(query, roomCount, filterCriteria);

                var sort = CreateQuerySort(sortCriteria);
                var results = await GetLimitedResultsAsync(baseDestinationId, query, roomCount, true, filterCriteria, sort, isMeta);

                results.Results.ForEach(i => i.Rooms.ForEach(j => j.EstablishmentResult = i));
                Parallel.ForEach(results.Results, result => UpdateRoomPriceAndFilter(result, filterCriteria));

                if (sortCriteria != null && sortCriteria.SortField != null)
                {
                    Func<AccommodationB2CAvailabilityResult, object> sortExpression = B2CAvailabilityResultCaching.GetSortExpression(sortCriteria);

                    // order the result
                    if (sortCriteria.SortOrder == AccommodationB2CAvailabilitySortOrder.Ascendant)
                    {
                        results.Results = results.Results
                            .OrderBy(sortExpression)
                            .ToArray();
                    }
                    else
                    {
                        results.Results = results.Results
                            .OrderByDescending(sortExpression)
                            .ToArray();
                    }
                }
                return results;
            }
        }

        private void UpdateRoomPriceAndFilter(AccommodationB2CAvailabilityResult result, AccommodationB2CAvailabilityFilter filterCriteria)
        {
            result.RoomsCheapestPriceSort = result.Rooms.Min(i => i.Price);
            Parallel.ForEach(result.Rooms, LowestPriceToFalse);
            var cheapestRooms = result.Rooms.GroupBy(i => new { i.BoardType, i.RoomNumber }).Select(j => j.OrderBy(k => k.Price)).Select(l => l.First()).ToArray();
            Parallel.ForEach(cheapestRooms, UpdateCheapestBoardType);

            // getting the lowest price
            if (filterCriteria.OnlyLowerPriceRoomResults)
            {
                GetLowestPriceRoom(result);
            }
        }

        private void UpdateCheapestBoardType(AccommodationB2CAvailabilityResultRoom room)
        {
            room.IsLowestPrice = true;
            room.IsDuplicateRate = false;
        }

        private void LowestPriceToFalse(AccommodationB2CAvailabilityResultRoom room)
        {
            room.IsLowestPrice = false;
            room.IsDuplicateRate = true;
        }

        private void GetLowestPriceRoom(AccommodationB2CAvailabilityResult result)
        {
            result.Rooms = result.Rooms.Where(i => i.IsLowestPrice == true).ToArray();

        }
        public async Task<AccommodationB2CAvailabilityResult> GetByEstablishmentIdAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId, bool isMeta = false)
        {
            return await cacheQuery.GetItemAsync(baseDestinationId, i => i.AvailabilityId == availabilityId && i.EstablishmentId == establishmentId, isMeta);
        }

        public async Task<AccommodationB2CAvailabilityResult> GetByEstablishmentIdFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId
            , AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool debugging, bool isMeta = false)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.SupplierCacheRetrieval, availabilityId.ToString(), "Internal establishment filtered availability", debugging, logger))
            {
                var query = LinqExpression.Create<AccommodationB2CAvailabilityResult>(i => i.AvailabilityId == availabilityId && i.EstablishmentId == establishmentId);
                query = ApplyQueryFilter(query, -1, filterCriteria);
                var sort = CreateQuerySort(sortCriteria);
                var results = await GetLimitedResultsAsync(baseDestinationId, query, -1, false, filterCriteria, sort, isMeta);
                results.Results.ForEach(i => i.Rooms.ForEach(j => j.EstablishmentResult = i));
                // getting the lowest price
                if (filterCriteria.OnlyLowerPriceRoomResults)
                {
                    Parallel.ForEach(results.Results, result => GetLowestPriceRoom(result));
                }
                return (results.ResultsTotalCount == 1 ? results.Results[0] : null);
            }
        }

        public async Task<AccommodationB2CAvailabilityResult[]> GetByRoomIdsAsync(Guid availabilityId, Guid baseDestinationId, uint[] roomIds, bool isMeta = false)
        {
            var results = (await this.cacheQuery.GetItemsAsync(baseDestinationId, i => i.AvailabilityId == availabilityId && i.Rooms.Any(j => roomIds.Contains(j.RoomIdAlpha2)), isMeta)).ToArray();
            results.ForEach(i =>
            {
                i.Rooms = i.Rooms.Where(j => roomIds.Contains(j.RoomIdAlpha2)).ToArray();
                i.Rooms.ForEach(j => j.EstablishmentResult = i);
            });
            return results;
        }

		public async Task<AccommodationB2CAvailabilityResult[]> GetByTokenKeyAsync(Guid availabilityId, Guid baseDestinationId, bool isMeta = false)
		{
			var results = (await this.cacheQuery.GetItemsAsync(baseDestinationId, i => i.AvailabilityId == availabilityId , isMeta)).ToArray();
			return results;
		}

		private Expression<Func<AccommodationB2CAvailabilityResult, bool>> ApplyQueryFilter(
            Expression<Func<AccommodationB2CAvailabilityResult, bool>> query,
            int roomCount,
            AccommodationB2CAvailabilityFilter filterCriteria)
        {
            if (!string.IsNullOrEmpty(filterCriteria.EstablishmentName))
            {
                query = query.And(i => i.EstablishmentName.Contains(filterCriteria.EstablishmentName));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.StarRatings))
            {
                query = query.And(i => filterCriteria.StarRatings.Contains(i.EstablishmentStarRating));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.Districts))
            {
                query = query.And(i => filterCriteria.Districts.Contains(i.DestinationReference));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.FacilityIds))
            {
                filterCriteria.FacilityIds.ForEach(i => query = query.And(j => j.EstablishmentFacilities.Any(k => k.Reference == i)));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.TravellerTypeIds))
            {
                query = query.And(i => i.EstablishmentTravellerTypes.Any(j => filterCriteria.TravellerTypeIds.Contains(j.Id)));
            }

            if (filterCriteria.MinimumTripAdvisorRating.HasValue)
            {
                query = query.And(i => i.EstablishmentTripAdvisorAverageRating >= filterCriteria.MinimumTripAdvisorRating.Value);
            }

            for (int i = 1; i <= roomCount; i++)
            {
                query = query.AndAny(j => j.Rooms, this.ApplyRoomQueryFilter(i, filterCriteria));
            }
            return query;
        }

        private Expression<Func<AccommodationB2CAvailabilityResultRoom, bool>> ApplyRoomQueryFilter(int roomNumber, AccommodationB2CAvailabilityFilter filterCriteria)
        {
            var query = LinqExpression.Create<AccommodationB2CAvailabilityResultRoom>(i => i.RoomNumber == roomNumber);

            if (filterCriteria.PricePerNightMin != null)
            {
                query = query.And(i => i.PricePerNight.Amount >= filterCriteria.PricePerNightMin.Amount);
            }

            if (filterCriteria.PricePerNightMax != null)
            {
                query = query.And(i => i.PricePerNight.Amount <= filterCriteria.PricePerNightMax.Amount);
            }

            if (filterCriteria.PricePerPersonMin != null)
            {
                query = query.And(i => i.PricePerPerson.Amount >= filterCriteria.PricePerPersonMin.Amount);
            }

            if (filterCriteria.PricePerPersontMax != null)
            {
                query = query.And(i => i.PricePerPerson.Amount <= filterCriteria.PricePerPersontMax.Amount);
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.BoardTypes))
            {
                query = query.And(i => filterCriteria.BoardTypes.Contains(i.BoardType));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.ProviderEdiCodes))
            {
                query = query.And(i => filterCriteria.ProviderEdiCodes.Contains(i.ProviderEdiCode));
            }

            if (!ArrayModule.IsNullOrEmpty(filterCriteria.PaymentTypes))
            {
                var paymentModels = filterCriteria.PaymentTypes.SelectMany(this.paymentModelConverter.ToPaymentModels).ToArray();
                var paymentModelQuery = LinqExpression.CreateFalse<AccommodationB2CAvailabilityResultRoom>();
                paymentModelQuery = paymentModelQuery.Or(i => paymentModels.Contains(i.PaymentModel));

                if (filterCriteria.IsEligibleForDeposit.HasValue)
                {
                    paymentModelQuery = paymentModelQuery.Or(i => i.IsEligibleForDeposit);
                }
                query = query.And(paymentModelQuery);
            }
            else if (filterCriteria.IsEligibleForDeposit.HasValue)
            {
                query = query.And(i => i.IsEligibleForDeposit);
            }

            return query;
        }

        private AccommodationB2CAvailabilityResult[] FilterRooms(IEnumerable<AccommodationB2CAvailabilityResult> results, int roomCount, bool removeEstablishmentWithoutRooms
            , AccommodationB2CAvailabilityFilter filterCriteria)
        {
            var query = LinqExpression.CreateTrue<AccommodationB2CAvailabilityResultRoom>();
            if (filterCriteria.PricePerNightMin != null)
            {
                query = query.And(i => i.PricePerNight.Amount >= filterCriteria.PricePerNightMin.Amount);
            }
            if (filterCriteria.PricePerNightMax != null)
            {
                query = query.And(i => i.PricePerNight.Amount <= filterCriteria.PricePerNightMax.Amount);
            }
            if (filterCriteria.PricePerPersonMin != null)
            {
                query = query.And(i => i.PricePerPerson.Amount >= filterCriteria.PricePerPersonMin.Amount);
            }
            if (filterCriteria.PricePerPersontMax != null)
            {
                query = query.And(i => i.PricePerPerson.Amount <= filterCriteria.PricePerPersontMax.Amount);
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.BoardTypes))
            {
                query = query.And(i => filterCriteria.BoardTypes.Contains(i.BoardType));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.ProviderEdiCodes))
            {
                query = query.And(i => filterCriteria.ProviderEdiCodes.Contains(i.ProviderEdiCode));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.PaymentTypes))
            {
                var paymentModels = filterCriteria.PaymentTypes.SelectMany(i => paymentModelConverter.ToPaymentModels(i)).ToArray();
                var paymentModelQuery = LinqExpression.CreateFalse<AccommodationB2CAvailabilityResultRoom>();
                paymentModelQuery = paymentModelQuery.Or(i => paymentModels.Contains(i.PaymentModel));
                if (filterCriteria.IsEligibleForDeposit.HasValue)
                {
                    paymentModelQuery = paymentModelQuery.Or(i => i.IsEligibleForDeposit);
                }
                query = query.And(paymentModelQuery);
            }
            else if (filterCriteria.IsEligibleForDeposit.HasValue)
            {
                query = query.And(i => i.IsEligibleForDeposit);
            }
            //if (filterCriteria.OnlyLowerPriceRoomResults) query = query.And(i => i.IsLowestPrice);
            var rooms = (roomCount > 1 ? Enumerable.Range(1, roomCount).ToArray() : null);
            results.ParallelForEach(result =>
            {
                result.IsEligibleForOnePoundDeposit = result.Rooms.Any(i => i.IsEligibleForDeposit);
                if (filterCriteria.NumberOfRoomResults != null)
                {
                    result.Rooms = result.Rooms.AsQueryable().Where(query).GroupBy(i => i.RoomNumber)
                      .SelectMany(i => i.OrderBy(j => j.Price).Take(filterCriteria.NumberOfRoomResults.Value)).ToArray();
                }
                else
                {
                    result.Rooms = result.Rooms.AsQueryable().Where(query).OrderBy(i => i.Price).ToArray();
                }
                if (rooms != null && !rooms.All(i => result.Rooms.Any(j => j.RoomNumber == i)))
                {
                    result.Rooms = new AccommodationB2CAvailabilityResultRoom[0];
                }
            });
            return (removeEstablishmentWithoutRooms ? results.Where(i => i.Rooms.Any()).ToArray() : results.ToArray());
        }

        private async Task<AccommodationB2CAvailabilityResults> GetLimitedResultsAsync(
            Guid baseDestinationId,
            Expression<Func<AccommodationB2CAvailabilityResult, bool>> query,
            int roomCount,
            bool removeEstablishmentWithoutRooms,
            AccommodationB2CAvailabilityFilter filterCriteria,
            ICacheSort<AccommodationB2CAvailabilityResult> sort,
            bool isMeta = false)
        {
            int? resultsStart = null;
            int? resultsCount = null;
            int resultsTotalCount = -1;
            if (filterCriteria.NumberOfResults != null)
            {
                resultsStart = 0;
                resultsCount = filterCriteria.NumberOfResults.Value;
            }
            else if (filterCriteria.PageNumber != null && filterCriteria.PageSize != null)
            {
                resultsStart = (filterCriteria.PageNumber.Value - 1) * filterCriteria.PageSize.Value;
                resultsCount = filterCriteria.PageSize.Value;
            }
            IEnumerable<AccommodationB2CAvailabilityResult> results;
            if (resultsStart != null && resultsCount != null)
            {
                LimitedResults<AccommodationB2CAvailabilityResult> limitedResults;
                if (sort == null)
                {
                    limitedResults = await this.cacheQuery.GetLimitedItemsAsync(baseDestinationId, query, resultsStart.Value, resultsCount.Value, isMeta);
                }
                else
                {
                    limitedResults = await this.cacheQuery.GetLimitedItemsAsync(baseDestinationId, query, resultsStart.Value, resultsCount.Value, sort, isMeta);
                }
                results = limitedResults.Results;
                resultsTotalCount = limitedResults.TotalResultsCount;
            }
            else
            {
                if (sort == null)
                {
                    results = await this.cacheQuery.GetItemsAsync(baseDestinationId, query, isMeta);
                }
                else
                {
                    results = await this.cacheQuery.GetItemsAsync(baseDestinationId, query, sort, isMeta);
                }
                resultsTotalCount = results.Count();
                if (resultsStart != null)
                {
                    results = results.Skip(resultsStart.Value);
                }
                if (resultsCount != null)
                {
                    results = results.Take(resultsCount.Value);
                }
            }
            return new AccommodationB2CAvailabilityResults() { Results = FilterRooms(results, roomCount, removeEstablishmentWithoutRooms, filterCriteria), ResultsTotalCount = resultsTotalCount };
        }

        public async Task SaveRangeAsync(Guid baseDestinationId, IEnumerable<AccommodationB2CAvailabilityResult> results, bool isMeta = false)
        {
            await this.cacheWriter.AddRangeAsync(baseDestinationId, results, isMeta);
        }

        public async Task DeleteRangeAsync(Guid baseDestinationId, Guid availabilityId, bool isMeta = false)
        {
            await this.cacheWriter.DeleteAsync(baseDestinationId, i => i.AvailabilityId == availabilityId, isMeta);
        }

        private static Func<AccommodationB2CAvailabilityResult, object> GetSortExpression(AccommodationB2CAvailabilitySort sortCriteria)
        {
            Func<AccommodationB2CAvailabilityResult, object> sortExpression;

            switch (sortCriteria.SortField)
            {
                case AccommodationB2CAvailabilitySortField.CheapestPrice:
                    return sortExpression = (i => i.RoomsCheapestPriceSort);
                case AccommodationB2CAvailabilitySortField.MostPopular:
                    return sortExpression = (i => i.EstablishmentPopularity);
                default:
                    throw new InvalidOperationException("Sort option is not supported: " + sortCriteria.SortField);
            }
        }

        private static ICacheSort<AccommodationB2CAvailabilityResult> CreateQuerySort(AccommodationB2CAvailabilitySort sortCriteria)
        {
            if (sortCriteria == null || sortCriteria.SortField == null)
            {
                return null;
            }

            CacheSortOrder sortOrder;

            if (sortCriteria.SortOrder == AccommodationB2CAvailabilitySortOrder.Ascendant)
            {
                sortOrder = CacheSortOrder.Asc;
            }
            else
            {
                sortOrder = CacheSortOrder.Desc;
            }

            switch (sortCriteria.SortField)
            {
                case AccommodationB2CAvailabilitySortField.CheapestPrice:
                    return new CacheSort<AccommodationB2CAvailabilityResult>(i => i.RoomsCheapestPrice, sortOrder);
                case AccommodationB2CAvailabilitySortField.MostPopular:
                    return new CacheSort<AccommodationB2CAvailabilityResult>(i => i.EstablishmentPopularity, sortOrder);
                default:
                    throw new InvalidOperationException("Sort option is not supported: " + sortCriteria.SortField);
            }
        }
    }
}
