﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationB2BWebBookingService
    {
        [OperationContract]
        Task<Guid> StartAccommodationBookingProcessAsync(AccommodationB2BWebBookingRequest availabilityRequest);
        [OperationContract]
        Task<AccommodationB2BWebBookingResponse> GetAccommodationBookingResponseAsync(Guid bookingId);
    }
}
