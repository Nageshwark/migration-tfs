﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationB2BWebCancellationService
    {
        [OperationContract]
        Guid StartAccommodationCancellationProcess(AccommodationB2BWebCancellationRequest cancellationRequest);
        [OperationContract]
        Task<AccommodationB2BWebCancellationResponse> GetAccommodationCancellationResponse(Guid cancellationId);
    }
}
