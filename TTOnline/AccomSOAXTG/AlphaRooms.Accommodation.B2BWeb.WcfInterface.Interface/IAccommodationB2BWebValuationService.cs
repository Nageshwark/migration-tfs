﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationB2BWebValuationService
    {
        [OperationContract]
        Task<Guid> StartAccommodationValuationProcessAsync(AccommodationB2BWebValuationRequest valuationRequest);
        [OperationContract]
        Task<AccommodationB2BWebValuationResponse> GetAccommodationValuationResponseAsync(Guid valuationId);
    }
}
