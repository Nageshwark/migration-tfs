﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationB2BWebAvailabilityService
    {
        [OperationContract]
        Task<Guid> StartAccommodationAvailabilitySearchAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityByEstablishmentResponseAsync(Guid availabilityId, Guid establishment, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityResponse> GetAccommodationAvailabilityByRoomsResponseAsync(Guid availabilityId, string[] roomIds);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityRequest> GetAccommodationAvailabilityRequestAsync(Guid availabilityId);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityResultsFilterOptions> GetAccommodationAvailabilityResultsFilterOptionsAsync(Guid availabilityId);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria);
        [OperationContract]
        Task<AccommodationB2BWebAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId);
    }
}
