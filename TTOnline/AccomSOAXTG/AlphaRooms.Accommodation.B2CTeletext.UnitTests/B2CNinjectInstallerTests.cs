﻿namespace AlphaRooms.Accommodation.B2C.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class B2CNinjectInstallerTests
    {
        [TestMethod]
        public void NinjectInitTest()
        {
            NinjectInstaller.Start();
            NinjectInstaller.Stop();
        }
    }
}
