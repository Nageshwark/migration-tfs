﻿namespace AlphaRooms.Accommodation.B2C.UnitTests.AlphaRooms.Accommodation.B2C.ServicesTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Core.Provider.DomainModels.Enums;
    using Core.Contracts;
    using SOACommon.Contracts;
    using Core.Provider.Contracts;

    [TestClass]
    public class B2CConsolidationTests
    {
        [TestMethod]
        public void ConsolidationSetsLowestPrice()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var minPrice = consolidationGroup.Min(x => x.Price.Amount);

            var cheapestRoom = consolidationGroup.Single(x => x.Price.Amount == minPrice);
            // make sure there is only one IsLowestPrice
            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);

            Assert.AreEqual(cheapestRoom, roomMarkedAsCheapest);
        }

        [TestMethod]
        public void ConsolidationSetsOnlyOneLowestPrice()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;
            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroups = consolidatedRooms
                .GroupBy(x => new { x.EstablishmentId, x.BoardType })
                .ToList();

            // check all groups
            foreach (var consolidationGroup in consolidationGroups)
            {
                var lowestPrice = consolidationGroup.SingleOrDefault(x => x.IsLowestPrice);
                Assert.IsNotNull(lowestPrice);
            }
        }

        [TestMethod]
        public void ConsolidationSetsLowestPriceIncludesCost()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("4f2a7157-edab-45ef-b75d-c8723c8bc3cd");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price but make the cost price cheaper so more profit
            expected.CostPrice = new Money(expected.CostPrice.Amount - 1, expected.CostPrice.CurrencyCode);
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);

            Assert.AreEqual(expected, roomMarkedAsCheapest, "Expected cost Price: " + expected.CostPrice + " actual: " + roomMarkedAsCheapest.CostPrice);
        }

        [TestMethod]
        public void ConsolidationUsesRoomNumber()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("4f2a7157-edab-45ef-b75d-c8723c8bc3cd");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price but make it a different room number
            expected.RoomNumber += 1;
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var roomsMarkedAsCheapest = consolidationGroup.Where(x => x.IsLowestPrice);

            // should be two cheapest rooms as this was a multiroom search
            Assert.IsTrue(roomsMarkedAsCheapest.Count() == 2);
        }

        [TestMethod]
        public void ConsolidationDoesNotUseNonRefunble()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("4f2a7157-edab-45ef-b75d-c8723c8bc3cd");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // room is the same, just differnent IsNonRefundable status
            expected.ProviderResult.IsNonRefundable = !expected.ProviderResult.IsNonRefundable;
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var roomsMarkedAsCheapest = consolidationGroup.Where(x => x.IsLowestPrice);

            // should be two cheapest rooms as this was a multiroom search
            Assert.IsTrue(roomsMarkedAsCheapest.Count() == 1);
        }

        [TestMethod]
        public void ConsolidationSupplierPriorityScores()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("4f2a7157-edab-45ef-b75d-c8723c8bc3cd");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            expected.ProviderResult = (AccommodationProviderAvailabilityResult)expected.ProviderResult.Clone();
            // it is the same price & cost price & but A code as the hight
            expected.ProviderResult.SupplierEdiCode = "A";
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);

            Assert.AreEqual(expected, roomMarkedAsCheapest);
        }

        [TestMethod]
        public void ConsolidationUsesRoomDescriptionLength()
        {
            var testRooms = B2CConsolidationSpeedTests.TestRooms;

            Guid cheapestId = new Guid("4f2a7157-edab-45ef-b75d-c8723c8bc3cd");
            var cheapestAtStart = testRooms.Single(x => x.Id == cheapestId);

            var expected = (AccommodationAvailabilityResult)cheapestAtStart.Clone();
            // it is the same price & cost price & suppiler, just shorter name
            expected.RoomDescription = expected.RoomDescription.Substring(1);
            testRooms.Add(expected);

            var consolidatedRooms = B2CConsolidationSpeedTests.Consolidate(testRooms);

            var consolidationGroup = consolidatedRooms
                .Where(x => x.EstablishmentId == Guid.Parse("1bf87457-bb30-4845-965a-886c47765e1c"))
                .Where(x => x.BoardType == BoardType.Breakfast)
                .ToList();

            var roomMarkedAsCheapest = consolidationGroup.Single(x => x.IsLowestPrice);

            Assert.AreEqual(expected, roomMarkedAsCheapest);
        }
    }
}
