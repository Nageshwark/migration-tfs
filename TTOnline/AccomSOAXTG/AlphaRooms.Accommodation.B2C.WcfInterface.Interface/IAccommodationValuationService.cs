﻿using AlphaRooms.Accommodation.B2C.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationValuationService
    {
        [OperationContract]
        Task<Guid> StartAccommodationValuationProcessAsync(AccommodationB2CValuationRequest valuationRequest);
        [OperationContract]
        Task<AccommodationB2CValuationResponse> GetAccommodationValuationResponseAsync(Guid valuationId);
    }
}
