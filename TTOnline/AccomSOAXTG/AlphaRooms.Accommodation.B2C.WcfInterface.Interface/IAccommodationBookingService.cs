﻿using AlphaRooms.Accommodation.B2C.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationBookingService
    {
        [OperationContract]
        Task<Guid> StartAccommodationBookingProcessAsync(AccommodationB2CBookingRequest availabilityRequest);
        [OperationContract]
        Task<AccommodationB2CBookingResponse> GetAccommodationBookingResponseAsync(Guid bookingId);
    }
}
