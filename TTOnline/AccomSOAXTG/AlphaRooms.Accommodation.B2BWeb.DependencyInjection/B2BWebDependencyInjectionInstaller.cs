﻿using AlphaRooms.Accommodation.B2BWeb.Caching;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.DbContexts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories;
using AlphaRooms.Accommodation.B2BWeb.Repositories;
using AlphaRooms.Accommodation.B2BWeb.Services;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Services;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.Cache.Mongo;
using AlphaRooms.Cache.Mongo.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;
using log4net.Config;
using Ninject;
using Ninject.Modules;
using System;
using AlphaRooms.SOACommon.Interfaces.Repositories;
using AlphaRooms.SOACommon.Repositories;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.SOACommon.Services.Factories;

namespace AlphaRooms.Accommodation.B2BWeb.DependencyInjection
{
    public class B2BWebDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            XmlConfigurator.Configure();

            // databases
            Bind<IDbContextActivator<B2BWebLogDbContext>>().To<NoProxyDbContextActivator<B2BWebLogDbContext>>().InSingletonScope();

            // caching
            var configurationManager = Kernel.Get<IAccommodationConfigurationManager>();
            Bind<ICacheMultiDatabaseController<AccommodationB2BWebAvailabilityResult, Guid>>().To<B2BWebAvailabilityResultDatabaseController>().InSingletonScope();
            Bind<IMongoDbMultiDbCollection<AccommodationB2BWebAvailabilityResult, Guid>>().ToConstant(new MongoDbMultiDbCollection<AccommodationB2BWebAvailabilityResult, Guid>(
                new MongoDbMultiDbCollectionSettings(configurationManager.AccommodationB2BWebAvailabilityResultsDatabase
                    , configurationManager.AccommodationB2BWebAvailabilityResultsCollection
                    , configurationManager.AccommodationB2BWebAvailabilityResultsMultiDbCount
                    , configurationManager.AccommodationB2BWebAvailabilityResultsMultiDbIsEnabled)
                    , Kernel.Get<IMongoDbConnection>(), Kernel.Get<ICacheMultiDatabaseController<AccommodationB2BWebAvailabilityResult, Guid>>())).InSingletonScope();
            Bind<ICacheMultiDbQueryExpireAsync<AccommodationB2BWebAvailabilityResult, Guid>>().To<MongoDbMultiDbQuery<AccommodationB2BWebAvailabilityResult, Guid>>().InSingletonScope();
            Bind<ICacheMultiDbWriterExpireAsync<AccommodationB2BWebAvailabilityResult, Guid>>().To<MongoDbMultiDbWriter<AccommodationB2BWebAvailabilityResult, Guid>>().InSingletonScope();
            Bind<IB2BWebAvailabilityResultCaching>().To<B2BWebAvailabilityResultCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationB2BWebRequestStatus>>().ToConstant(new MongoDbCollection<AccommodationB2BWebRequestStatus>(
                new MongoDbCollectionSettings(configurationManager.AccommodationB2BWebRequestStatusDatabase
                    , configurationManager.AccommodationB2BWebRequestStatusCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationB2BWebRequestStatus>>().To<MongoDbQuery<AccommodationB2BWebRequestStatus>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationB2BWebRequestStatus>>().To<MongoDbWriter<AccommodationB2BWebRequestStatus>>().InSingletonScope();
            Bind<IB2BWebRequestStatusCaching>().To<B2BWebRequestStatusCaching>().InSingletonScope();

            // repositories
            Bind<IB2BWebAvailabilityLogRepository>().To<B2BWebAvailabilityLogRepository>().InSingletonScope();
            Bind<IB2BWebValuationLogRepository>().To<B2BWebValuationLogRepository>().InSingletonScope();
            Bind<IB2BWebBookingLogRepository>().To<B2BWebBookingLogRepository>().InSingletonScope();

            // shared services
            Bind<IB2BWebPaymentModelConverter>().To<B2BWebPaymentModelConverter>().InSingletonScope();
            Bind<IB2BWebRequestStatusService>().To<B2BWebRequestStatusService>().InSingletonScope();
            Bind<IB2BWebRoomIdService>().To<B2BWebRoomIdService>().InSingletonScope();
            Bind<IAccommodationBusiness>().To<B2BWebAccommodationBusiness>().InSingletonScope();

            // availability services
            Bind<IB2BWebAvailabilityKeyGenerator>().To<B2BWebAvailabilityKeyGenerator>().InSingletonScope();
            Bind<IB2BWebAvailabilityLogger>().To<B2BWebAvailabilityLogger>().InSingletonScope();
            Bind<IB2BWebAvailabilityRequestFactory>().To<B2BWebAvailabilityRequestFactory>().InSingletonScope();
            Bind<IB2BWebAvailabilityRequestValidator>().To<B2BWebAvailabilityRequestValidator>().InSingletonScope();
            Bind<IB2BWebAvailabilityResultsFilterOptionsService>().To<B2BWebAvailabilityResultsFilterOptionsService>().InSingletonScope();
            Bind<IB2BWebAvailabilityResultMappingService>().To<B2BWebAvailabilityResultMappingService>().InSingletonScope();
            Bind<IB2BWebAvailabilityService>().To<B2BWebAvailabilityService>().InSingletonScope();
            Bind<IB2BWebAvailabilityProviderIdentifierService>().To<B2BWebAvailabilityProviderIdentifierService>().InSingletonScope();
            Bind<IB2BWebAvailabilityMapResultMappingService>().To<B2BWebAvailabilityMapResultMappingService>().InSingletonScope();
            Bind<IB2BWebAvailabilityResultRecoveryService>().To<B2BWebAvailabilityResultRecoveryService>();
            Bind<IAvailabilityResultConsolidationService>().To<B2BWebAvailabilityConsolidationService>().InSingletonScope();
            Bind<IB2BWebAvailabilityResultLowerPriceService>().To<B2BWebAvailabilityResultLowerPriceService>().InSingletonScope();
            Bind<IAvailabilityResultLowerPriceService>().To<AvailabilityResultLowerPriceService>().InSingletonScope();

            // valuation services
            Bind<IB2BWebValuationService>().To<B2BWebValuationService>().InSingletonScope();
            Bind<IB2BWebValuationLogger>().To<B2BWebValuationLogger>().InSingletonScope();
            Bind<IB2BWebValuationRequestFactory>().To<B2BWebValuationRequestFactory>().InSingletonScope();
            Bind<IB2BWebValuationRequestValidator>().To<B2BWebValuationRequestValidator>().InSingletonScope();
            Bind<IB2BWebValuationResultMappingService>().To<B2BWebValuationResultMappingService>().InSingletonScope();

            // booking services
            Bind<IB2BWebBookingService>().To<B2BWebBookingService>().InSingletonScope();
            Bind<IB2BWebBookingLogger>().To<B2BWebBookingLogger>().InSingletonScope();
            Bind<IB2BWebBookingRequestFactory>().To<B2BWebBookingRequestFactory>().InSingletonScope();
            Bind<IB2BWebBookingRequestValidator>().To<B2BWebBookingRequestValidator>().InSingletonScope();
            Bind<IB2BWebBookingResultMappingService>().To<B2BWebBookingResultMappingService>().InSingletonScope();

            // cancellation services
            Bind<IB2BWebCancellationService>().To<B2BWebCancellationService>().InSingletonScope();
            Bind<IB2BWebCancellationLogger>().To<B2BWebCancellationLogger>().InSingletonScope();
            Bind<IB2BWebCancellationRequestFactory>().To<B2BWebCancellationRequestFactory>().InSingletonScope();
            Bind<IB2BWebCancellationRequestValidator>().To<B2BWebCancellationRequestValidator>().InSingletonScope();
            Bind<IB2BWebCancellationResultMappingService>().To<B2BWebCancellationResultMappingService>().InSingletonScope();

        }
    }
}
