﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationCancellationService
    {
        [OperationContract]
        Guid StartAccommodationCancellationProcess(AccommodationB2CCancellationRequest cancellationRequest);
        [OperationContract]
        Task<AccommodationB2CCancellationResponse> GetAccommodationCancellationResponse(Guid cancellationId);
    }
}
