﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.WcfInterface.Interface
{
    [ServiceContract]
    public interface IAccommodationAvailabilityService
    {
        [OperationContract]
        Task<Guid> StartAccommodationAvailabilitySearchAsync(AccommodationB2CAvailabilityRequest availabilityRequest);
        [OperationContract]
        Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool getFilterDetails);
        [OperationContract]
        Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityByEstablishmentResponseAsync(Guid availabilityId, Guid establishment, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria);
        [OperationContract]
        Task<AccommodationB2CAvailabilityResponse> GetAccommodationAvailabilityByRoomsResponseAsync(Guid availabilityId, uint[] roomIds);
        [OperationContract]
        Task<AccommodationB2CAvailabilityRequest> GetAccommodationAvailabilityRequestAsync(Guid availabilityId);
        [OperationContract]
        Task<AccommodationB2CAvailabilityResultsFilterOptions> GetAccommodationAvailabilityResultsFilterOptionsAsync(Guid availabilityId);
        [OperationContract]
        Task<AccommodationB2CAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria);
        [OperationContract]
        Task<AccommodationB2CAvailabilityMapResponse> GetAccommodationAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId);
		[OperationContract]
		Task<AccommodationB2CAvailabilityResponse> GetMetaAccommodationAvailablityResponseAsync(AccommodationB2CAvailabilityRequest availabilityRequest);
        [OperationContract]
        Task<AccommodationB2CAvailabilityResponse> StartAndGetAccommodationAvailabilityResponseAsync(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool getFilterDetails);
    }
}
