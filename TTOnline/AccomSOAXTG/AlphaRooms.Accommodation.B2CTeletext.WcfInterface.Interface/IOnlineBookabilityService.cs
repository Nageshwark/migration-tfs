﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.WcfInterface.Interface
{
    [ServiceContract]
    public interface IOnlineBookabilityService
    {
        [OperationContract]
        //Dictionary<AutoSuggestCategory, List<DestinationAutoSuggest>> GetAutoCompleteHotelAndDestinationNames(string searchTerm, Channel channel = Channel.TeletextHolidaysUK);
        DestinationHotelAutoSuggest GetAutoCompleteHotelAndDestinationNames(string searchTerm, Channel channel = Channel.TeletextHolidaysUK);
        //string GetAutoCompleteHotelAndDestinationNames(string searchTerm, Channel channel = Channel.TeletextHolidaysUK);
    }
}
