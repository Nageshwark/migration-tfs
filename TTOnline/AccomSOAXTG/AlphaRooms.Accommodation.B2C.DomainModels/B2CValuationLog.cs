﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.DomainModels
{
    public class B2CValuationLog
    {
        public int Id { get; set; }

        public string HostName { get; set; }

        public Guid ValuationId { get; set; }

        public Guid AvailabilityId { get; set; }

        public DateTime ValuationDate { get; set; }

        public Status? Status { get; set; }
        
        public int TimeTaken { get; set; }

        public string Exceptions { get; set; }

        public int? ProviderId { get; set; }

        public string ProviderRoomNumbers { get; set; }

        public string RecoveredSelectedRooms { get; set; }

        public string ProviderRequests { get; set; }

        public string ProviderResponses { get; set; }

        public Status? ProviderStatus { get; set; }

        public int? ProviderTimeTaken { get; set; }

        public int? ResultsCount { get; set; }

        public string ProviderException { get; set; }
    }
}
