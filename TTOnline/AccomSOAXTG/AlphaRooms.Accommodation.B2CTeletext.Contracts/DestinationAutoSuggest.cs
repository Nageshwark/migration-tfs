﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class DestinationAutoSuggest
    {
        [DataMember]
        public Guid? LocationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public string DestinationId { get; set; }
    }
}
