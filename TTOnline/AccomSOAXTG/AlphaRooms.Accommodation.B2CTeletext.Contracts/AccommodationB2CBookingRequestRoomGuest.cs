﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequestRoomGuest
    {
        public const string IdFieldName  = "Id";
        public const string TitleFieldName = "Ttl";
        public const string FirstNameFieldName = "FNm";
        public const string SurnameFieldName = "SNm";
        public const string AgeFieldName = "Age";

        [DataMember]
        [BsonElement(IdFieldName)]
        [BsonIgnoreIfDefault]
        public int Id { get; set; }

        [DataMember]
        [BsonElement(TitleFieldName)]
        [BsonIgnoreIfDefault]
        public string TitleString { get; set; }

        [DataMember]
        [BsonElement(FirstNameFieldName)]
        [BsonIgnoreIfDefault]
        public string FirstName { get; set; }

        [DataMember]
        [BsonElement(SurnameFieldName)]
        [BsonIgnoreIfDefault]
        public string Surname { get; set; }

        [DataMember]
        [BsonElement(AgeFieldName)]
        [BsonIgnoreIfDefault]
        public byte? Age { get; set; }
    }
}
