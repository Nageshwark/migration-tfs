﻿using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    public class B2CResultsRangeSegment
    {
        public const string PercentageFieldName = "Pcn";
        public const string ValueFieldName = "Val";

        [BsonElement(PercentageFieldName)]
        public decimal Percentage { get; set; }

        [BsonElement(ValueFieldName)]
        public decimal Value { get; set; }
    }
}