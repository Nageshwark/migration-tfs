﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilitySort
    {
        [DataMember]
        public AccommodationB2CAvailabilitySortField? SortField { get; set; }

        [DataMember]
        public AccommodationB2CAvailabilitySortOrder? SortOrder { get; set; }
    }
}
