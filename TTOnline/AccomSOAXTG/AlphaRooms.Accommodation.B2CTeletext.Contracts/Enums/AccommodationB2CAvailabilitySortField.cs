﻿namespace AlphaRooms.Accommodation.B2CTeletext.Contracts.Enums
{
    public enum AccommodationB2CAvailabilitySortField
    {
        MostPopular = 0,
        CheapestPrice = 1
    }
}
