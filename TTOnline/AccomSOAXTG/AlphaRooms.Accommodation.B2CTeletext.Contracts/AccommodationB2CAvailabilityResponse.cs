﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilityResponse
    {
        [DataMember]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        public Status AvailabilityStatus { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public AccommodationB2CAvailabilityResult[] AvailabilityResults { get; set; }

        [DataMember]
        public int AvailabilityTotalResultsCount { get; set; }

        [DataMember]
        public AccommodationB2CAvailabilityFilterOptionDetails AvailabilityFilterDetails { get; set; }

        [DataMember]
        public string PromotionalCode { get; set; }
    }
}
