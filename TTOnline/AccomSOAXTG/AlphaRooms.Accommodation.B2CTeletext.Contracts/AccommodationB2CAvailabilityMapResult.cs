﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilityMapResult
    {
        [DataMember]
        public string DestinationSeoFriendlyName { get; set; }

        [DataMember]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        public string EstablishmentName { get; set; }

        [DataMember]
        public StarRatingType EstablishmentStarRating { get; set; }
                
        [DataMember]
        public string EstablishmentSeoFriendlyName { get; set; }

        [DataMember]
        public double EstablishmentLongitude { get; set; }

        [DataMember]
        public double EstablishmentLatitude { get; set; }

        [DataMember]
        public Money RoomsCheapestPrice { get; set; }

        [DataMember]
        public Money RoomsCheapestPricePerNight { get; set; }
    }
}
