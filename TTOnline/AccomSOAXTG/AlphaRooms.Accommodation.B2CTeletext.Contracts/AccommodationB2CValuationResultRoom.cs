﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.DomainModels;
using System;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class AccommodationB2CValuationResultRoom
    {
        [DataMember]
        public string RoomId { get; set; }

        [DataMember]
        public byte RoomNumber { get; set; }

        [DataMember]
        public string ProviderEdiCode { get; set; }

        [DataMember]
        public string ProviderName { get; set; }

        [DataMember]
        public string ProviderAliasName { get; set; }

        [DataMember]
        public Guid DestinationId { get; set; }

        [DataMember]
        public string DestinationName { get; set; }

        [DataMember]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        public string EstablishmentName { get; set; }

        [DataMember]
        public string ProviderEstablishmentCode { get; set; }

        [DataMember]
        public DateTime CheckInDate { get; set; }

        [DataMember]
        public DateTime CheckOutDate { get; set; }

        [DataMember]
        public byte AdultsCount { get; set; }

        [DataMember]
        public byte ChildrenAndInfantsCount { get; set; }

        [DataMember]
        public string RoomDescription { get; set; }

        [DataMember]
        public string ProviderRoomCode { get; set; }

        [DataMember]
        public BoardType BoardType { get; set; }

        [DataMember]
        public string BoardDescription { get; set; }

        [DataMember]
        public bool IsSpecialRate { get; set; }

        [DataMember]
        public bool IsOpaqueRate { get; set; }

        [DataMember]
        public bool IsNonRefundable { get; set; }

        [DataMember]
        public bool IsBindingRate { get; set; }

        [DataMember]
        public PaymentModel PaymentModel { get; set; }

        [DataMember]
        public Money ProviderPrice { get; set; }

        [DataMember]
        public Money Price { get; set; }

        [DataMember]
        public Money PricePerNight { get; set; }

        [DataMember]
        public Money PaymentToTake { get; set; }

        [DataMember]
        public PromotionalDiscountResult Discount { get; set; }

        [DataMember]
        public string CancellationPolicy { get; set; } 
                
        [DataMember]
        public decimal ExchangeRate { get; set; }

        [DataMember]
        public bool IsCardChargeApplicable { get; set; }

        [DataMember]
        public decimal Margin { get; set; }

        [DataMember]
        public RateType RateType { get; set; }

        [DataMember]
        public AccommodationB2CValuationResultVccPaymentRule[] VccPaymentRules { get; set; }

        [DataMember]
        public DateTime CacheExpireDate { get; set; }

        [DataMember]
        public PriceHikeDetail PriceHikeDetail { get; set; }

        [DataMember]
        public bool IsEligibleForDeposit { get; set; }
		
		[DataMember]
        public bool IsActiveAccommodationAdminFee { get; set; }

        [DataMember]
        public Money AccommodationAdminFee { get; set; }
    }
}
