﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class B2CResultsRangeFilterOptions
    {
        public const string RangeMinFieldName = "Min";
        public const string RangeMaxFieldName = "Max";
        public const string QuarterSegmentFieldName = "QSg";
        public const string HalfSegmentFieldName = "HSg";
        public const string ThreeQuarterSegmentFieldName = "TSg";

        [DataMember]
        [BsonElement(RangeMinFieldName)]
        public decimal RangeMin { get; set; }

        [DataMember]
        [BsonElement(RangeMaxFieldName)]
        public decimal RangeMax { get; set; }

        [DataMember]
        [BsonElement(QuarterSegmentFieldName)]
        [BsonIgnoreIfNull]
        public decimal? QuarterSegment { get; set; }

        [DataMember]
        [BsonElement(HalfSegmentFieldName)]
        [BsonIgnoreIfNull]
        public decimal? HalfSegment { get; set; }

        [DataMember]
        [BsonElement(ThreeQuarterSegmentFieldName)]
        [BsonIgnoreIfNull]
        public decimal? ThreeQuarterSegment { get; set; }

    }
}