﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequestCustomer
    {
        public const string IdFieldName = "Id";
        public const string FirstNameFieldName = "FNm";
        public const string SurnameFieldName = "SNm";
        public const string EmailAddressFieldName = "Eml";
        public const string ContactNumberFieldName = "CtN";
        public const string SiteFieldName = "Sfn";
        public const string TitleFieldName = "Tte";
        public const string AddressLine1FieldName = "Al1";
        public const string AddressLine2FieldName = "Al2";
        public const string TownFieldName = "Twn";
        public const string CountryFieldName = "Ctry";
        public const string PostCodeFieldName = "Pc";
        public const string CountyFieldName = "Cty";
        public const string UserIdFieldName = "Uid";
        public const string ChannelFieldName = "Cnl";
        public const string LanguageIdFieldName = "Lng";
        public const string FullNameFieldName = "Fnm";

        [DataMember]
        [BsonElement(IdFieldName)]
        [BsonIgnoreIfDefault]
        public int Id { get; set; }

        [DataMember]
        [BsonElement(FirstNameFieldName)]
        [BsonIgnoreIfDefault]
        public string FirstName { get; set; }

        [DataMember]
        [BsonElement(SurnameFieldName)]
        [BsonIgnoreIfDefault]
        public string Surname { get; set; }

        [DataMember]
        [BsonElement(EmailAddressFieldName)]
        [BsonIgnoreIfDefault]
        public string EmailAddress { get; set; }

        [DataMember]
        [BsonElement(ContactNumberFieldName)]
        [BsonIgnoreIfDefault]
        public string ContactNumber { get; set; }

        [DataMember]
        [BsonElement(SiteFieldName)]
        [BsonIgnoreIfDefault]
        public int Site { get; set; }
        [DataMember]
        [BsonElement(TitleFieldName)]
        [BsonIgnoreIfDefault]
        public string Title { get; set; }
        [DataMember]
        [BsonElement(AddressLine1FieldName)]
        [BsonIgnoreIfDefault]
        public string AddressLine1 { get; set; }
        [DataMember]
        [BsonElement(AddressLine2FieldName)]
        [BsonIgnoreIfDefault]
        public string AddressLine2 { get; set; }
        [DataMember]
        [BsonElement(TownFieldName)]
        [BsonIgnoreIfDefault]
        public string Town { get; set; }
        [DataMember]
        [BsonElement(CountryFieldName)]
        [BsonIgnoreIfDefault]
        public string Country { get; set; }
        [DataMember]
        [BsonElement(PostCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string PostCode { get; set; }
        [DataMember]
        [BsonElement(CountyFieldName)]
        [BsonIgnoreIfDefault]
        public string County { get; set; }
        [DataMember]
        [BsonElement(UserIdFieldName)]
        [BsonIgnoreIfDefault]
        public int? UserId { get; set; }
        [DataMember]
        [BsonElement(ChannelFieldName)]
        [BsonIgnoreIfDefault]
        public SOACommon.DomainModels.Enumerators.Channel? Channel { get; set; }
        [DataMember]
        [BsonElement(LanguageIdFieldName)]
        [BsonIgnoreIfDefault]
        public int LanguageId { get; set; }
    }
}
