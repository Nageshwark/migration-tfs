﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    public class AccommodationB2CCancellationResponse
    {
        public Guid CancellationId { get; set; }
        public object CancellationResult { get; set; }
        public Status CancellationStatus { get; set; }
    }
}
