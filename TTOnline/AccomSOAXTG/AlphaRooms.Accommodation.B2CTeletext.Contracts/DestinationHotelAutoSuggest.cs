﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class DestinationHotelAutoSuggest
    {
        [DataMember]
        public DestinationAutoSuggest[] DestinationAutoSuggest { get; set; }
        [DataMember]
        public HotelAutoSuggest[] HotelAutoSuggest { get; set; }
    }
}
