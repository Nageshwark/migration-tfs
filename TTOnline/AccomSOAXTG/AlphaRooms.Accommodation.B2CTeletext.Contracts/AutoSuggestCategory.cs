﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public enum AutoSuggestCategory
    {
        Destinations = 1,
        Countries = 2,
        Hotels = 3,
        Airports = 4,
        DepartureAirports = 5,
        AirportsWithParking = 6
    }
}
