﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequestRoom
    {
        public const string ValuatedRoomIdFieldName = "VId";
        public const string GuestsFieldName = "Gts";
        public const string SpecialRequestsFieldName = "SRq";
        public const string CardDetailsFieldName = "CDt";

        [DataMember]
        [BsonElement(ValuatedRoomIdFieldName)]
        [BsonIgnoreIfDefault]
        public string ValuatedRoomId { get; set; }

        [DataMember]
        [BsonElement(GuestsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestRoomGuest[] Guests { get; set; }

        [DataMember]
        [BsonElement(SpecialRequestsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestRoomSpecialRequest SpecialRequests { get; set; }

        [DataMember]
        [BsonElement(CardDetailsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestRoomPaymentDetails PaymentDetails { get; set; }
    }
}
