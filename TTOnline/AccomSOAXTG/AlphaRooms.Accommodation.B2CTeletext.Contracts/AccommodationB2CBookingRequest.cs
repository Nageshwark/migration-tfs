﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequest
    {
        public const string BookingIdFieldName = "BId";
        public const string ChannelFieldName = "Chn";
        public const string AvailabilityIdFieldName = "AId";
        public const string ValuationIdFieldName = "VId";
        public const string ItineraryIdFieldName = "IId";
        public const string ItineraryReferenceFieldName = "IRf";
        public const string CustomerFieldName = "Cst";
        public const string ValuatedRoomsFieldName = "VRm";
        public const string DebuggingFieldName = "Dbg";
        public const string PaymentsFieldName = "Pts";

        [DataMember]
        [BsonElement(BookingIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid? BookingId { get; set; }

        [DataMember]
        [BsonElement(ChannelFieldName)]
        [BsonIgnoreIfDefault]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonElement(AvailabilityIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonElement(ValuationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid ValuationId { get; set; }

        [DataMember]
        [BsonElement(ItineraryIdFieldName)]
        [BsonIgnoreIfDefault]
        public int ItineraryId { get; set; }

        [DataMember]
        [BsonElement(ItineraryReferenceFieldName)]
        [BsonIgnoreIfDefault]
        public string ItineraryReference { get; set; }

        [DataMember]
        [BsonElement(CustomerFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestCustomer Customer { get; set; } 

        [DataMember]
        [BsonElement(PaymentsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestRoomPaymentDetails paymentDetails { get; set; }

        [DataMember]
        [BsonElement(ValuatedRoomsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequestRoom[] ValuatedRooms { get; set; }

        [DataMember]
        [BsonElement(DebuggingFieldName)]
        [BsonIgnoreIfDefault]
        public bool Debugging { get; set; }
    }
}
