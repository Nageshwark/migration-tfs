﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    public class AccommodationB2CAvailabilityResults
    {
        public AccommodationB2CAvailabilityResult[] Results { get; set; }

        public int ResultsTotalCount { get; set; }
    }
}
