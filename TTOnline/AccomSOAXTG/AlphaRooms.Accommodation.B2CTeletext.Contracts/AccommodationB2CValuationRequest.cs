﻿namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CValuationRequest
    {
        public const string ValuationIdFieldName = "VId";
        public const string ChannelFieldName = "Chn";
        public const string AvailabilityIdFieldName = "AId";
        public const string SelectedRoomIdsFieldName = "SRI";
        public const string DebuggingFieldName = "Dbg";
        public const string PromotionalCodeFieldName = "Prm";

        [DataMember]
        [BsonElement(ValuationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid? ValuationId { get; set; }

        [DataMember]
        [BsonElement(ChannelFieldName)]
        [BsonIgnoreIfDefault]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonElement(AvailabilityIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonElement(SelectedRoomIdsFieldName)]
        [BsonIgnoreIfDefault]
        public uint[] SelectedRoomIds { get; set; }

        [DataMember]
        [BsonElement(DebuggingFieldName)]
        [BsonIgnoreIfDefault]
        public bool Debugging { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PromotionalCodeFieldName)]
        public string PromotionalCode { get; set; }
    }
}
