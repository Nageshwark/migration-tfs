﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequestRoomSpecialRequest
    {
        private const string LateArrivalFieldName = "LAr";
        private const string CotRequiredFieldName = "Cot";
        private const string SeaViewsFieldName = "SVw";
        private const string AdjoiningRoomsFieldName = "AdR";
        private const string NonSmokingFieldName = "NSk";
        private const string DisabledAccessFieldName = "DAc";
        private const string OtherRequestsFieldName = "ORq";

        [DataMember]
        [BsonElement(LateArrivalFieldName)]
        [BsonIgnoreIfDefault]
        public bool LateArrival { get; set; }

        [DataMember]
        [BsonElement(CotRequiredFieldName)]
        [BsonIgnoreIfDefault]
        public bool CotRequired { get; set; }

        [DataMember]
        [BsonElement(SeaViewsFieldName)]
        [BsonIgnoreIfDefault]
        public bool SeaViews { get; set; }

        [DataMember]
        [BsonElement(AdjoiningRoomsFieldName)]
        [BsonIgnoreIfDefault]
        public bool AdjoiningRooms { get; set; }

        [DataMember]
        [BsonElement(NonSmokingFieldName)]
        [BsonIgnoreIfDefault]
        public bool NonSmoking { get; set; }

        [DataMember]
        [BsonElement(DisabledAccessFieldName)]
        [BsonIgnoreIfDefault]
        public bool DisabledAccess { get; set; }

        [DataMember]
        [BsonElement(OtherRequestsFieldName)]
        [BsonIgnoreIfDefault]
        public string OtherRequests { get; set; }
    }
}
