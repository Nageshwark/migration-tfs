﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilityFilterOptionDetails
    {
        [DataMember]
        public AccommodationB2CAvailabilityFilterOptionDetailsItem[] FilterDetails { get; set; }
    }
}
