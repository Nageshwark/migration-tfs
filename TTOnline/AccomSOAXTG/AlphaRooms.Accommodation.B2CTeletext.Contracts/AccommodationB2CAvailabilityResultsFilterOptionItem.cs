﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResultsFilterOptionItem
    {
        public const string NameFieldName = "Nam";

        [DataMember]
        [BsonId]
        public int Id { get; set; }

        [DataMember]
        [BsonElement(NameFieldName)]
        [BsonIgnoreIfDefault]
        public string Name { get; set; }
    }
}
