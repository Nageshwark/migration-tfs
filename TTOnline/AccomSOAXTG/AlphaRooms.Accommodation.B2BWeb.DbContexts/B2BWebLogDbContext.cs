﻿using AlphaRooms.Accommodation.B2BWeb.DbContexts.Mapping;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.DbContexts
{
    public class B2BWebLogDbContext : DbContext
    {
        static B2BWebLogDbContext()
        {
            Database.SetInitializer<B2BWebLogDbContext>(null);
        }

        public B2BWebLogDbContext() : base("Name=AlphabedsLogsEntities")
        {
        }
        
        public DbSet<B2BWebValuationLog> B2BWebValuationLogs { get; set; }
        public DbSet<B2BWebBookingLog> B2BWebBookingLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new B2BWebValuationLogMap());
            modelBuilder.Configurations.Add(new B2BWebBookingLogMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task B2BWebAvailabilityLogInsertAsync(string hostName, Guid searchId, DateTime searchDate, byte searchType, byte? status, string request, int? resultsCount
            , int? establishmentResultsCount, string providers, string providerSearchTypes, string providerResultsCount, string providerEstablishmentResultsCount, string providerTimeTaken
            , string providerExceptions, int? postProcessTimeTaken, int? timeTaken, string exception)
        {
            if (hostName == null) throw new ArgumentNullException("hostName");
            await this.ExecuteStoredProcedureAsync("AccommodationAvailabilityLogsInsert"
                , new SqlParameter("hostName", DataModule.DbNullIfNull(hostName))
                , new SqlParameter("searchId", DataModule.DbNullIfNull(searchId))
                , new SqlParameter("StartDate", DataModule.DbNullIfNull(searchDate))
                , new SqlParameter("searchType", DataModule.DbNullIfNull(searchType))
                , new SqlParameter("status", DataModule.DbNullIfNull(status))
                , new SqlParameter("request", DataModule.DbNullIfNull(request))
                , new SqlParameter("providers", DataModule.DbNullIfNull(providers))
                , new SqlParameter("providerSearchTypes", DataModule.DbNullIfNull(providerSearchTypes))
                , new SqlParameter("providerResultsCount", DataModule.DbNullIfNull(providerResultsCount))
                , new SqlParameter("providerEstablishmentResultsCount", DataModule.DbNullIfNull(providerEstablishmentResultsCount))
                , new SqlParameter("providerTimeTaken", DataModule.DbNullIfNull(providerTimeTaken))
                , new SqlParameter("providerExceptions", DataModule.DbNullIfNull(providerExceptions))
                , new SqlParameter("resultsCount", DataModule.DbNullIfNull(resultsCount))
                , new SqlParameter("establishmentResultsCount", DataModule.DbNullIfNull(establishmentResultsCount))
                , new SqlParameter("postProcessTimeTaken", DataModule.DbNullIfNull(postProcessTimeTaken))
                , new SqlParameter("timeTaken", DataModule.DbNullIfNull(timeTaken))
                , new SqlParameter("exception", DataModule.DbNullIfNull(exception))
                );
        }
    }
}
