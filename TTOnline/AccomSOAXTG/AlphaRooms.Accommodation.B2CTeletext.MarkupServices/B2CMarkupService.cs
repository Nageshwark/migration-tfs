﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2CTeletext.MarkupServices
{
    public class B2CMarkupService : IAvailabilityResultMarkupService, IValuationResultMarkupService
    {
        private readonly IMarkupRepository markupRepository;
        private readonly IPriceAdjustmentRepository priceAdjustmentRepository;
        private readonly IProviderCommissionRepository providerCommissionRepository;
        private readonly IExchangeRateRepository exchangeRateRepository;
        private readonly IVolumeDiscountRepository volumeDiscountRepository;
        private readonly IAccommodationConfigurationManager configurationManager;
		private readonly IAccommodationProviderAdminFeeConfigurationRepository providerAdminFeeConfigurationsRepository;

        public B2CMarkupService(IMarkupRepository markupRepository, IPriceAdjustmentRepository priceAdjustmentRepository, IProviderCommissionRepository providerCommissionRepository
            , IExchangeRateRepository exchangeRateRepository, IVolumeDiscountRepository volumeDiscountRepository, IAccommodationConfigurationManager configurationManager, IAccommodationProviderAdminFeeConfigurationRepository providerAdminFeeConfigurationsRepository)
        {
            this.markupRepository = markupRepository;
            this.priceAdjustmentRepository = priceAdjustmentRepository;
            this.providerCommissionRepository = providerCommissionRepository;
            this.exchangeRateRepository = exchangeRateRepository;
            this.volumeDiscountRepository = volumeDiscountRepository;
            this.configurationManager = configurationManager;
			this.providerAdminFeeConfigurationsRepository = providerAdminFeeConfigurationsRepository;
        }

        public async Task<IList<AccommodationAvailabilityResult>> ApplyAvailabilityMarkupAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            if (results.Count == 0) return results;
            //results = new AccommodationAvailabilityResult[] { results.Where(i => i.EstablishmentId == Guid.Parse("0FD316BE-7001-4540-9D8C-C1E990289ABD") && i.ProviderResult.CostPrice.Amount == 335.20M).ToArray()[0] };
            var baseDestinationId = results[0].BaseDestinationId;
            var destinationId = results[0].DestinationId;
            var markups = await this.markupRepository.GetActiveFilteredHierarchicalAsync(availabilityRequest.ChannelInfo.Channel, (availabilityRequest.Agent != null ? (int?)availabilityRequest.Agent.Id : null));
            markups = ApplyInitialFilter(markups, results.First(), DateTime.Now);

			// Getting all provider admin fee config details
			var providerAdminConfigPrices = await this.providerAdminFeeConfigurationsRepository.GetAllDictionaryAsync();
			
            var commissions = await this.providerCommissionRepository.GetAllDictionaryAsync();
            var priceAdjustments = await this.priceAdjustmentRepository.GetByDestinationIdDictionaryAsync(baseDestinationId);
            var exchangeRates = await this.exchangeRateRepository.GetExchangeRateAsync(results.Select(i => i.ProviderResult.CostPrice.CurrencyCode).Distinct().ToArray()
                , availabilityRequest.ChannelInfo.CurrencyCode);
            var volumeDiscounts = await this.volumeDiscountRepository.GetAllAsync();
            results.ParallelForEach(result => ApplyPriceMarkup(availabilityRequest.ChannelInfo, (availabilityRequest.Agent != null ? (int?)availabilityRequest.Agent.Id : null)
                , result, markups, commissions, priceAdjustments, exchangeRates, volumeDiscounts, providerAdminConfigPrices));

            return results;
        }
        
        public async Task<AccommodationValuationResult> ApplyValuationMarkupAsync(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            if (result.Rooms.Length == 0)
            {
                return result;
            }
            var baseDestinationId = result.Rooms[0].BaseDestinationId;
            var destinationId = result.Rooms[0].DestinationId;
            var markups = await this.markupRepository.GetActiveFilteredHierarchicalAsync(valuationRequest.ChannelInfo.Channel, (valuationRequest.Agent != null ? (int?)valuationRequest.Agent.Id : null));
			markups = ApplyInitialFilter(markups, result.Rooms[0], DateTime.Now);

			// Getting all provider admin fee config details
			var providerAdminConfigPrices = await this.providerAdminFeeConfigurationsRepository.GetAllDictionaryAsync();
			
            var commissions = await this.providerCommissionRepository.GetAllDictionaryAsync();
            var priceAdjustments = await this.priceAdjustmentRepository.GetByDestinationIdDictionaryAsync(baseDestinationId);
            var exchangeRates = await this.exchangeRateRepository.GetExchangeRateAsync(result.Rooms.Select(i => i.ProviderResult.CostPrice.CurrencyCode).Distinct().ToArray()
                , valuationRequest.ChannelInfo.CurrencyCode);
            var volumeDiscounts = await this.volumeDiscountRepository.GetAllAsync();
            result.Rooms.ParallelForEach(roomResult => ApplyPriceMarkup(valuationRequest.ChannelInfo, (valuationRequest.Agent != null ? (int?)valuationRequest.Agent.Id : null)
                , roomResult, markups, commissions, priceAdjustments, exchangeRates, volumeDiscounts, providerAdminConfigPrices));
            return result;
        }

        private void ApplyPriceMarkup(ChannelInfo channelInfo, int? agentId, IAccommodationResultRoom result, Markup[] allMarkups, Dictionary<string, ProviderCommission> allCommissions
            , Dictionary<Guid, PriceAdjustment[]> allPriceAdjustments, Dictionary<string, ExchangeRate> exchangeRates, VolumeDiscount[] volumeDiscounts, Dictionary<int, AccommodationProviderAdminFeeConfiguration> providerAdminFeeConfiguration)
        {
            // TODO apply HMD if there is a PriceHike
            var commission = allCommissions.GetValueOrDefault(result.Provider.EdiCode + (result.Supplier != null ? result.Supplier.EdiCode : ""));
            var exchangeRate = exchangeRates[result.ProviderResult.CostPrice.CurrencyCode];
            var priceAdjustments = allPriceAdjustments.GetValueOrDefault(result.EstablishmentId);
			var markups = GetFilteredMarkups(allMarkups, result, DateTime.Now);
			var bookingDate = DateTime.Now;
            var applyPrice = !(result.ProviderResult.IsBindingRate || result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect || this.configurationManager.ExcludeSuppliersToApplyPriceMarkup.Contains(result.Provider.EdiCode)); //Exclude Suppliers To Apply Price Markup
			var applyMarkup = (commission != null ? commission.IncludeInHMD : true);
            var costPrice = new Money(result.ProviderResult.CostPrice.Amount, result.ProviderResult.CostPrice.CurrencyCode);
            if (applyPrice && commission != null) costPrice.Amount -= costPrice.Amount * (commission.Commission / 100);
            var salePrice = (IsSaleAndCostPriceDifferentForSupplier(result.Provider.EdiCode, result.ProviderResult.IsBindingRate) ? new Money(result.ProviderResult.SalePrice.Amount, result.ProviderResult.SalePrice.CurrencyCode)
                : (applyMarkup ? costPrice : new Money(result.ProviderResult.CostPrice.Amount, result.ProviderResult.CostPrice.CurrencyCode)));
            if (result.Provider.Id != this.configurationManager.TravelGateProvider[0])  // Exclude applying ExchangeRate to Travelgate results.
                salePrice = new Money(salePrice.Amount * (decimal)exchangeRate.Rate, channelInfo.CurrencyCode);
            Money additionalPrice = new Money(0, salePrice.CurrencyCode);
			Money accommodationAdminFee = new Money(0, channelInfo.CurrencyCode);
            var accommodationAdminFeeDetails = providerAdminFeeConfiguration.Values.Where(x => x.ProviderId.Equals(result.Provider.Id)).Where(x=>x.SupplierId.Equals(result.Supplier.Id)).Where(x => x.IsActiveAdminFee.Equals(true));			
			
            if (applyPrice)
            {
                if (result.ProviderResult.PaymentModel == PaymentModel.PostPayment && volumeDiscounts.Any() && salePrice.Amount > volumeDiscounts.Min(i => i.Volume) && markups.Any())
                {
                    var volumeDiscount = volumeDiscounts.Where(i => i.Volume <= salePrice.Amount).OrderByDescending(i => i.Volume).FirstOrDefault();
                    additionalPrice.Amount -= salePrice.Amount * (volumeDiscount.Discount / 100);
                }
                if (applyMarkup)
                {
                    bool includePriceAdjustments = true;
                    decimal markupSalePrice = 0, markupCostPrice = 0;
                    if (!ArrayModule.IsNullOrEmpty(markups))
                    {
                        foreach (var markup in markups)
                        {
                            if (markup.MarkupApplyType == MarkupApplyType.Value || markup.MarkupApplyType == MarkupApplyType.Override)
                            {
                                markupSalePrice = 0;
                                markupCostPrice = 0;
                            }
                            if (IsCommissionable(result.ProviderResult.RateType))
                            {
                                markupCostPrice += CalculateMarkup(costPrice, result.ProviderResult.CheckInDate, result.ProviderResult.CheckOutDate, markup);
                            }
                            else
                            {
                                markupSalePrice += CalculateMarkup(salePrice, result.ProviderResult.CheckInDate, result.ProviderResult.CheckOutDate, markup);
                            }
                            if (markup.MarkupApplyType == MarkupApplyType.Override)
                            {
                                includePriceAdjustments = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        markupSalePrice += (salePrice.Amount * (this.configurationManager.AccommodationBaseDiscount / 100));
                    }
                    if (includePriceAdjustments && !ArrayModule.IsNullOrEmpty(priceAdjustments))
                    {
                        markupSalePrice += priceAdjustments.Sum(i => CalculatePriceAdjustments(salePrice, result.ProviderResult.CheckInDate, result.ProviderResult.CheckOutDate, i));
                    }
                    if (markupSalePrice != 0)
                    {
                        additionalPrice.Amount += markupSalePrice;
                    }
                    if (markupCostPrice != 0)
                    {
                        costPrice.Amount -= markupCostPrice;
                    }
                }
            }
            costPrice.Amount = Math.Round(costPrice.Amount, 2);
            result.CostPrice = costPrice;
            salePrice.Amount = Math.Round(salePrice.Amount, 2);
            result.SalePrice = salePrice;
            additionalPrice.Amount = Math.Round(additionalPrice.Amount, 2);
            
			if (accommodationAdminFeeDetails.Any() && result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect)
            {
                accommodationAdminFee = new Money(accommodationAdminFeeDetails.FirstOrDefault().AdminFee, channelInfo.CurrencyCode);
                result.PriceBreakdown = new[]
                {
                    new AccommodationResultPriceItem() { Type = AccommodationResultPriceItemType.BasePrice, SalePrice = salePrice }
                    , new AccommodationResultPriceItem() { Type = AccommodationResultPriceItemType.Other, SalePrice = additionalPrice }
                    , new AccommodationResultPriceItem() { Type = AccommodationResultPriceItemType.AccommodationAdminFee, SalePrice = accommodationAdminFee }
                };

                result.IsActiveAccommodationAdminFee = true;
                result.AccommodationAdminFee = accommodationAdminFee;
            }
            else
            {
                result.PriceBreakdown = new[]
                {
                    new AccommodationResultPriceItem() { Type = AccommodationResultPriceItemType.BasePrice, SalePrice = salePrice }
                    , new AccommodationResultPriceItem() { Type = AccommodationResultPriceItemType.Other, SalePrice = additionalPrice }
                };

                result.IsActiveAccommodationAdminFee = false;
                result.AccommodationAdminFee = accommodationAdminFee;
            }
			
            result.Margin = 0;
            result.ExchangeRate = (decimal)exchangeRate.Rate;
        }

        private bool IsCommissionable(RateType rateType)
        {
            return (rateType == RateType.GrossPayDirect || rateType == RateType.GrossStandard);
        }

        private decimal CalculatePriceAdjustments(Money salePrice, DateTime checkInDate, DateTime checkOutDate, PriceAdjustment priceAdjustment)
        {
            var adjustment = priceAdjustment.ChangeToApply ?? 0;
            return CalculateMarkup(salePrice, checkInDate, checkOutDate, priceAdjustment.StartDate, priceAdjustment.EndDate, (decimal)adjustment , null, null);
        }

        private decimal CalculateMarkup(Money costPrice, DateTime checkInDate, DateTime checkOutDate, Markup markup)
        {
            return CalculateMarkup(costPrice, checkInDate, checkOutDate, markup.TravelFromDate, markup.TravelToDate, markup.Value, markup.BookingFromDate, markup.BookingToDate);
        }

        private decimal CalculateMarkup(Money price, DateTime checkInDate, DateTime checkOutDate, DateTime? fromDate, DateTime? toDate, decimal percent, DateTime? bookingStartDate, DateTime? bookingEndDate)
        {
            bool applyMarkup = true;
            if (bookingStartDate != null || bookingEndDate != null)
                applyMarkup = ((bookingStartDate == null || DateTime.Now.Date >= bookingStartDate.GetValueOrDefault()) && (bookingEndDate ==null || DateTime.Now.Date <= bookingEndDate.GetValueOrDefault()));

            var duration = Math.Max(1, (int)(checkOutDate - checkInDate).TotalDays);
            var applyDays = applyMarkup ? GetMarkupApplyDays(checkInDate, checkOutDate, fromDate ?? DateTime.MinValue, toDate ?? DateTime.MaxValue):0;
            return price.Amount * (percent * ((decimal)applyDays / (decimal)duration) / 100M);
        }

        private int GetMarkupApplyDays(DateTime checkInDate, DateTime checkOutDate, DateTime startDate, DateTime endDate)
        {
            DateTime maxStartDate = (checkInDate > startDate ? checkInDate : startDate);
            DateTime minEndDate = (checkOutDate < endDate ? checkOutDate : endDate);
            return Math.Max(0, (int)(minEndDate - maxStartDate).TotalDays);
        }

        private Markup[] GetFilteredMarkups(Markup[] markups, IAccommodationResultRoom result, DateTime bookingDate)
        {
            // This was divided into common elements (ApplyInitialFilter) that can be done once, and multiple elements that need doing every time.
            return markups.Where(i =>
                   (i.DestinationId == null || (i.DestinationId == result.DestinationId || i.DestinationId == result.BaseDestinationId))
                && (i.ProviderId == null || i.ProviderId == result.Provider.Id)
                && (i.SupplierId == null || (result.Supplier != null && i.SupplierId == result.Supplier.Id))
                && (i.ChainId == null || i.ChainId == result.EstablishmentChainId)
                && (i.EstablishmentId == null || i.EstablishmentId == result.EstablishmentId)
                && (i.StarRating == null || i.StarRating == result.EstablishmentStarRating)
                && (i.BoardType == null || i.BoardType == result.BoardType)).OrderBy(i => i.Order).ToArray();
        }

        private Markup[] ApplyInitialFilter(Markup[] markups, IAccommodationResultRoom result, DateTime bookingDate)
        {
            // Common elements to fetch markups. Order shouldn't be important here as we will order where fewer elements appear.
            int duration = (int)(result.ProviderResult.CheckOutDate - result.ProviderResult.CheckInDate).TotalDays;
            return markups.Where(i =>
                   (i.ParentDestinationID == null || i.ParentDestinationID == result.BaseDestinationId)
                && (i.CountryCode == null || i.CountryCode == result.CountryCode)
                && (i.BookingMinDuration == null || i.BookingMinDuration <= duration)
                && (i.BookingMaxDuration == null || i.BookingMaxDuration >= duration)
                && (i.TravelFromDate == null || i.TravelFromDate <= result.ProviderResult.CheckInDate)
                && (i.TravelToDate == null || i.TravelToDate >= result.ProviderResult.CheckOutDate)
                && (i.BookingFromDate == null || i.BookingFromDate <= bookingDate)
                && (i.BookingToDate == null || i.BookingToDate >= bookingDate)).ToArray();
        }

        private decimal GetMarkupPercent(Markup[] filteredMarkups)
        {
            decimal markupPercent = 0;
            foreach (var markup in filteredMarkups)
            {
                if (markup.MarkupApplyType == MarkupApplyType.Value) markupPercent = markup.Value;
                else if (markup.MarkupApplyType == MarkupApplyType.Adjustment) markupPercent += markup.Value;
                else if (markup.MarkupApplyType == MarkupApplyType.Override)
                {
                    markupPercent = markup.Value;
                    break;
                }
                else throw new Exception("Unexpected MarkupApplyType " + markup.MarkupApplyType.ToString() + ".");
            }
            return markupPercent;
        }

        private bool IsSaleAndCostPriceDifferentForSupplier(string providerEdiCode, bool isBindingRate)
        {
            if (isBindingRate) return true;
            if (providerEdiCode == "F" && providerEdiCode == "I" || providerEdiCode == "TG") return true; // EdiTransfers EdiMinotel || return true for Travelgate provider
            return false;
        }
    }
}
