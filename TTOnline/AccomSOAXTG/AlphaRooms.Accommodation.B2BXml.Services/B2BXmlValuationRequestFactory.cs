﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlValuationRequestFactory : IB2BXmlValuationRequestFactory
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IEncryptDecryptService _encryptDecryptService;

        public B2BXmlValuationRequestFactory(IAccommodationConfigurationManager configurationManager,IEncryptDecryptService encryptDecryptService)
        {
            this.configurationManager = configurationManager;
            this._encryptDecryptService = encryptDecryptService;
        }

        public AccommodationValuationRequest CreateValuationRequest(OTA_HotelValRQ otaHotelValRQ, Guid valuationId, ChannelInfo channelInfo, B2BUser agent)
        {
            var valRequestSegment = otaHotelValRQ.ValRequestSegments.ValRequestSegment[0];
            return new AccommodationValuationRequest()
            {
                ValuationId = valuationId
                , ChannelInfo = channelInfo
                , Agent = agent
                , SelectedRoomIds = valRequestSegment.RoomStayCandidates.Select(i => _encryptDecryptService.Decrypt(i.RoomID)).ToArray()
            };
        }
    }
}
