﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityConsolidationService : IAvailabilityResultConsolidationService
    {
        private readonly IRoomDescriptionComparer roomDescriptionComparer;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IAvailabilityResultHighestPriceService resultHighestPriceService;
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BXmlAvailabilityConsolidationService(IRoomDescriptionComparer roomDescriptionComparer, IPriceCalculatorService priceCalculatorService
            , IAvailabilityResultHighestPriceService resultHighestPriceService, IAccommodationConfigurationManager configurationManager)
        {
            this.roomDescriptionComparer = roomDescriptionComparer;
            this.priceCalculatorService = priceCalculatorService;
            this.resultHighestPriceService = resultHighestPriceService;
            this.configurationManager = configurationManager;
        }

        public IList<AccommodationAvailabilityResult> ConsolidateAvailabilityResults(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            
            if (availabilityRequest.IgnoreConsolidation) return results;
            var comparer = CreateConsolidationComparer(configurationManager.AccommodationB2BWebConsolidationIncludePaymentModel);
            return results.GroupBy(i => i, comparer).AsParallel()
                .Select(i => ProcessConsolidation(availabilityRequest, i.OrderBy(j => priceCalculatorService.CalculateRoomPrice(j)).ThenBy(j => j.CostPrice.Amount).ToArray())).ToArray();
        }

      

        private IEqualityComparer<AccommodationAvailabilityResult> CreateConsolidationComparer(bool includePaymentModel)
        {
            if (includePaymentModel)
            {
                return new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                i.EstablishmentId == j.EstablishmentId && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType
                && i.ProviderResult.IsNonRefundable == j.ProviderResult.IsNonRefundable && i.ProviderResult.PaymentModel == j.ProviderResult.PaymentModel
                && roomDescriptionComparer.EqualsRoomDescription(i, j));
            }
            return new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                i.EstablishmentId == j.EstablishmentId && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType
                && i.ProviderResult.IsNonRefundable == j.ProviderResult.IsNonRefundable
                && roomDescriptionComparer.EqualsRoomDescription(i, j));
        }

        private AccommodationAvailabilityResult ProcessConsolidation(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityResult[] accommodationAvailabilityResult)
        {
            resultHighestPriceService.ApplyAvailabilityRoomHighestPrice(availabilityRequest, accommodationAvailabilityResult);
            return accommodationAvailabilityResult[0];
        }
    }
}
