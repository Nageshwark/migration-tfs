﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpharooms.Accommodation.B2BXml.TicketService.Infrastructure;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationTicketService : ICancellationService
    {
        private readonly ICancellationTicketService _cancellationTicketService;

        public B2BXmlCancellationTicketService(ICancellationTicketService cancellationTicketService)
        {
            _cancellationTicketService = cancellationTicketService;
        }
        public async Task<AccommodationCancellationResponse> ProcessCancellationAsync(AccommodationCancellationRequest coreCancellationRequest)
        {
            var itineraryId = coreCancellationRequest.BookingReferenceId;
            var emailaddress = coreCancellationRequest.Agent.Customers.FirstOrDefault()?.EmailAddress ?? coreCancellationRequest.Agent.Company.EmailAddress;
            var channelId = coreCancellationRequest.ChannelInfo.ChannelId;

            var cancellationTicketResponse = await _cancellationTicketService.GetCancellationResponseAsync(itineraryId, emailaddress, channelId);
            var ticketId = cancellationTicketResponse.Id.ToString();
            var cancellationId = new Guid(ticketId.PadLeft(32, '0'));

            var cancellationResult = new AccommodationCancellationResult()
            {
                CancellationId = cancellationId
            };
            //Send Response
            return new AccommodationCancellationResponse()
            {
                Result = cancellationResult

            };

        }

        public Task<AccommodationCancellationResponse> GetCancellationResponseAsync(Guid cancellationId)
        {
            throw new NotImplementedException();
        }
    }
}
