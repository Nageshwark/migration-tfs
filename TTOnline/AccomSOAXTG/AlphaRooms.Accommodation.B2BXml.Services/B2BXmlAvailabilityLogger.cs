﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityLogger : IB2BXmlAvailabilityLogger
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BXmlAvailabilityLogRepository repository;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2BXmlAvailabilityLogger(ILogger logger, IAccommodationConfigurationManager configurationManager, IB2BXmlAvailabilityLogRepository repository, IPerformanceLoggerService<PerformanceLog> performanceLoggerService, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService)
        {
            this.logger = logger;
            this.repository = repository;
            this.performanceLoggerService = performanceLoggerService;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(AccommodationAvailabilityRequest coreAvailabilityRequest, Guid availabilityId, DateTime availabilityStartDate, Status availabilityStatus, string availabilityRequest, AccommodationProvider[] providers
            , AccommodationAvailabilityResponse coreResponse, TimeSpan? postProcessTimeTaken, TimeSpan timeTaken, Exception exception)
        {

            if (coreAvailabilityRequest != null && coreResponse != null)
            {
                CreatePerformanceLogs(coreAvailabilityRequest, coreResponse, availabilityStatus, (int)timeTaken.TotalSeconds, exception == null);
            }
            
            if (!this.configurationManager.AccommodationB2BXmlAvailabilityLoggerIsEnabled) return;
            try
            {
                await this.repository.SaveAsync(new B2BXmlAvailabilityLog()
                {
                    HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                    , AvailabilityId = availabilityId
                    , AvailabilityType = SearchType.HotelOnly
                    , AvailabilityDate = availabilityStartDate
                    , Status = availabilityStatus
                    , Request = availabilityRequest
                    , PostProcessTimeTaken = (postProcessTimeTaken != null ? (int?)postProcessTimeTaken.Value.TotalMilliseconds : null)
                    , TimeTaken = (int)timeTaken.TotalMilliseconds
                    , ResultsCount = coreResponse?.Results?.Count
                    , EstablishmentResultsCount = (coreResponse?.Results?.GroupBy(i => i.EstablishmentId).Count())
                    , Exceptions = (exception?.GetDetailedMessageWithInnerExceptions())
                    , Providers = CreateProviderNames(providers, coreResponse)
                    , ProviderSearchTypes = CreateProviderSearchTypes(providers, coreResponse)
                    , ProviderTimeTaken = CreateProviderTimeTaken(providers, coreResponse)
                    , ProviderResultsCount = CreateProviderResults(providers, coreResponse)
                    , ProviderEstablishmentResultsCount = CreateProviderEstablishmentResults(providers, coreResponse)
                    , ProviderExceptions = CreateProviderExceptions(providers, coreResponse)
                });
            }
            catch (Exception ex)
            {
                logger.Error("Unable to log accommodation availability with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
            }
        }

        private void CreatePerformanceLogs(AccommodationAvailabilityRequest coreAvailabilityRequest, AccommodationAvailabilityResponse coreResponse, Status requestStatus, int timeTaken, bool fromUncontrolledException)
        {

            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();

            var perfLog = new PerformanceLog
            {
                OperationType = "Search",
                Channel = coreAvailabilityRequest.ChannelInfo.Channel,
                ProductType = SupplierTypes.Accommodation.ToString(),
                RequestIdentifier = coreAvailabilityRequest.AvailabilityId.ToString(),
                DestinationIdentifier = coreAvailabilityRequest.DestinationId.ToString(),
                TotalTime = timeTaken,
                IsSuccess = requestStatus == Status.Successful,
                ResultCount = coreResponse?.Results.Count ?? 0,
                HostName = this.configurationManager.AccommodationHostNameTag,
                CacheHit = coreResponse == null ? (fromUncontrolledException ? "F" : "T") : "F"
            };

            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                var providerPerfLogs =
                   coreResponseDetail.Providers.Where(p => p.Id != 5).Select(x =>
                   {
                       var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                       m.Provider = x.EdiCode;
                       m.Arrival = coreAvailabilityRequest.CheckInDate;
                       m.Departure = coreAvailabilityRequest.CheckOutDate;
                       m.TotalTime = coreResponseDetail.TimeTaken != null ? (int)coreResponseDetail.TimeTaken.Value.TotalSeconds : 0;
                       m.ResultCount = coreResponseDetail.ResultsCount ?? 0;
                       m.IsSuccess = coreResponseDetail.Exception == null;
                       return m;
                   });

                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }

            performanceLoggerService.Report(perfLog);
        }

        private string CreateProviderNames(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    builder.Append(provider.Id.ToString());
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }

        private string CreateProviderSearchTypes(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    builder.Append(coreResponseDetail.IsLiveResults ? "L" : "C");
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }

        private string CreateProviderResults(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    builder.Append(coreResponseDetail.ResultsCount != null ? coreResponseDetail.ResultsCount.ToString() : "NULL");
                    if (provider.Id == 5)
                    {
                        var supplierResults = coreResponse.Results.Where(i => i.Provider == provider).GroupBy(i => i.ProviderResult.SupplierEdiCode).ToArray();
                        if (supplierResults.Any())
                        {
                            builder.Append("(");
                            supplierResults.ForEach(j =>
                            {
                                builder.Append(j.Key.Replace("(", "").Replace(")", ""));
                                builder.Append("=");
                                builder.Append(j.Count());
                                builder.Append(",");
                            });
                            builder.Length -= 1;
                            builder.Append(")");
                        }
                    }
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }

        private string CreateProviderEstablishmentResults(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    var providerResults = coreResponse.Results.Where(i => i.Provider == provider).ToArray();
                    builder.Append(coreResponseDetail.ResultsCount != null ? providerResults.GroupBy(i => i.EstablishmentId).Count().ToString() : "NULL");
                    if (provider.Id == 5)
                    {
                        var supplierResults = providerResults.GroupBy(i => i.ProviderResult.SupplierEdiCode).ToArray();
                        if (supplierResults.Any())
                        {
                            builder.Append("(");
                            supplierResults.ForEach(j =>
                            {
                                builder.Append(j.Key.Replace("(", "").Replace(")", ""));
                                builder.Append("=");
                                builder.Append(j.GroupBy(k => k.EstablishmentId).Count());
                                builder.Append(",");
                            });
                            builder.Length -= 1;
                            builder.Append(")");
                        }
                    }
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }

        private string CreateProviderTimeTaken(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    builder.Append(coreResponseDetail.TimeTaken != null ? ((int)coreResponseDetail.TimeTaken.Value.TotalMilliseconds).ToString() : "NULL");
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }

        private string CreateProviderExceptions(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    builder.Append(coreResponseDetail.Exception != null ? coreResponseDetail.Exception.GetDetailedMessageWithInnerExceptions() : "NULL");
                    builder.Append(";");
                }
            }
            if (builder.Length > 0) builder.Length -= 1;
            return builder.ToString();
        }
    }
}
