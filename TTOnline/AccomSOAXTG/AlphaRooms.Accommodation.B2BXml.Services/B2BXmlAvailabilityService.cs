﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityService : IB2BXmlAvailabilityService
    {
        private readonly IB2BXmlAvailabilityRequestValidator requestValidator;
        private readonly IB2BXmlAvailabilityRequestFactory requestFactory;
        private readonly IAvailabilityService coreAvailabilityService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BXmlAvailabilityResultMappingService resultMappingService;
        private readonly IB2BXmlLoginService loginService;
        private readonly IB2BXmlErrorService errorService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IB2BXmlAvailabilityLogger availabilityLogger;
        private readonly IB2BXmlAvailabilityResultFilterService resultFilterService;
        private readonly IAccommodationBusiness accommodationBusiness;

        public B2BXmlAvailabilityService(IB2BXmlAvailabilityRequestValidator requestValidator, IB2BXmlAvailabilityRequestFactory requestFactory, IAvailabilityService coreAvailabilityService
            , IChannelInfoService channelInfoService, IB2BXmlAvailabilityResultMappingService resultMappingService, IB2BXmlLoginService loginService, IB2BXmlErrorService errorService
            , IAccommodationConfigurationManager configurationManager, IAccommodationProviderRepository providerRepository, IB2BXmlAvailabilityLogger availabilityLogger
            , IB2BXmlAvailabilityResultFilterService resultFilterService, IAccommodationBusiness accommodationBusiness)
        {
            this.requestValidator = requestValidator;
            this.requestFactory = requestFactory;
            this.channelInfoService = channelInfoService;
            this.coreAvailabilityService = coreAvailabilityService;
            this.resultMappingService = resultMappingService;
            this.loginService = loginService;
            this.errorService = errorService;
            this.configurationManager = configurationManager;
            this.providerRepository = providerRepository;
            this.availabilityLogger = availabilityLogger;
            this.resultFilterService = resultFilterService;
            this.accommodationBusiness = accommodationBusiness;
        }

        public async Task<string> GetAvailabilityResponseAsync(string request)
        {
            AccommodationAvailabilityResponse coreAvailabilityResponse = null;
            DateTime? postProcessStartDate = null;
            Exception exception = null;
            string response;
            AccommodationProvider[] providers = null;
            var processStartDate = DateTime.Now;
            var processStatus = Status.NotStarted;
            var availabilityId = Guid.NewGuid();
            AccommodationAvailabilityRequest coreAvailabilityRequest = null;
            try
            {
                // create request object
                var otaHotelAvailRQ = request.XmlDeSerialize<OTA_HotelAvailRQ>();

                // Validate Request
                requestValidator.ValidateAvailabilityRequest(otaHotelAvailRQ);

                // validate user and load user profile
                var clientProfile = await loginService.LoginAsync(otaHotelAvailRQ);

                // get client availability reference
                var availabilityReference = otaHotelAvailRQ.EchoToken;

                //get customerChannel from agent
                var customerChannel = clientProfile.Customers.FirstOrDefault()?.Channel;
                if (null == customerChannel) throw new Exception("Customer does not have an associated channel");

                // get channelinfo from request channel
                var channelInfo = await channelInfoService.GetChannelInfo(customerChannel.Value);

                // get a list of providers to process
                providers = (ArrayModule.IsNullOrEmpty(configurationManager.AccommodationProvidersToSearch) ? clientProfile.Providers.ToArray() : await providerRepository.GetByIdsAsync(configurationManager.AccommodationProvidersToSearch));

                // create a core availability request
                coreAvailabilityRequest = requestFactory.CreateAvailabilityRequest(otaHotelAvailRQ, availabilityId, channelInfo, clientProfile, providers);

                // perform a availability search
                coreAvailabilityResponse = await coreAvailabilityService.GetAvailabilityResponseAsync(coreAvailabilityRequest);

                //filter by suppliers, if the agent has them specified
                var suppliers = clientProfile.Suppliers?.Select(x => x.EdiCode).ToArray();
                accommodationBusiness.FilterBySupplier(suppliers, clientProfile, coreAvailabilityResponse);

                // set post process start date flag
                postProcessStartDate = DateTime.Now;
				
				if (coreAvailabilityResponse.Results != null && coreAvailabilityResponse.Results.Count > 0)
                {
                    // execute post process for the results
                    var coreResults = await accommodationBusiness.PostProcessAvailabilityResultsAsync(coreAvailabilityRequest, coreAvailabilityResponse.Results);

                    // apply filter
                    var filteredResults = resultFilterService.FilterResults(otaHotelAvailRQ, coreResults);

                    // map the core results
                    var otaHotelAvailRS = await resultMappingService.MapFromCoreAvailabilityResultsAsync(coreAvailabilityRequest, filteredResults, availabilityReference);

                    response = otaHotelAvailRS.XmlSerialize();
                    processStatus = Status.Successful;
                }
			   else
                {
                    response = new OTA_HotelAvailRS()
                    {
                        Items = new object[] { new ErrorsType() { Error = new[] { new ErrorType() { ShortText = "No results found" } } } }
                    }.XmlSerialize();
                   processStatus = Status.NotFound;
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                processStatus = Status.Failed;
                response = this.errorService.CreateAvailabilityErrorXmlResponse(ex);
            }
            await availabilityLogger.LogAsync(coreAvailabilityRequest, availabilityId, processStartDate, processStatus, request, providers, coreAvailabilityResponse
                , (postProcessStartDate != null ? (TimeSpan?)(DateTime.Now - postProcessStartDate) : null), DateTime.Now - processStartDate, exception);
            return response;
        }
    }
}