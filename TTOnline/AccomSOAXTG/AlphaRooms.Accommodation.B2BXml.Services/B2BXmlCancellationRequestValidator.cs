﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationRequestValidator : IB2BXmlCancellationRequestValidator
    {
        public void ValidateCancellationRequest(OTA_CancelRQ otaCancelRQ)
        {
            // TODO validate ota request expected parameters
            if (ArrayModule.IsNullOrEmpty(otaCancelRQ.POS) || otaCancelRQ.POS[0].RequestorID == null)
                throw new Exception("Login credentials are missing.");
            if (!(new[] { 1m }).Contains(otaCancelRQ.Version))
                throw new Exception("Request version not supported");
        }
    }
}
