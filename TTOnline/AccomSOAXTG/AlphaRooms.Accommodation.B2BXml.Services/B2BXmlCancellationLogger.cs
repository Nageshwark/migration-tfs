﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationLogger : IB2BXmlCancellationLogger
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2BXmlCancellationLogger(IAccommodationConfigurationManager configurationManager, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService, IPerformanceLoggerService<PerformanceLog> performanceLoggerService)
        {
            this.configurationManager = configurationManager;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.performanceLoggerService = performanceLoggerService;
        }

        public Task LogAsync(Guid cancellationId, DateTime processStartDate, string request, Status processStatus, AccommodationCancellationRequest coreCancellationRequest, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeSpan, Exception exception)
        {
            if (coreCancellationRequest != null && coreCancellationResponse != null)
            {
                CreatePerformanceLogs(coreCancellationRequest, processStatus, coreCancellationResponse, timeSpan);
            }
            return null ;
        }

        private void CreatePerformanceLogs(AccommodationCancellationRequest cancellationRequest, Status requestStatus, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeTaken)
        {
            var resultCount = coreCancellationResponse?.Result?.Rooms.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Cancellation",
                RequestIdentifier = cancellationRequest.CancellationId.ToString(),
                Channel = cancellationRequest.ChannelInfo.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();
            var providerPerfLogs = coreCancellationResponse?.ProcessResponseDetails?.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.IsSuccess = d.Exception == null;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }
    }
}