﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlBookingService : IB2BXmlBookingService
    {
        private readonly IB2BXmlBookingRequestValidator requestValidator;
        private readonly IB2BXmlBookingRequestFactory requestFactory;
        private readonly IBookingService coreBookingService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BXmlBookingResultMappingService resultMappingService;
        private readonly IB2BXmlLoginService loginService;
        private readonly IB2BXmlErrorService errorService;
        private readonly IValuationResultCaching valuationResultCaching;
        private readonly IB2BXmlBookingLogger bookingLogger;
        private readonly IB2BItineraryBookingService itineraryBookingService;

        public B2BXmlBookingService(IB2BXmlBookingRequestValidator requestValidator, IB2BXmlBookingRequestFactory requestFactory, IBookingService coreBookingService
            , IChannelInfoService channelInfoService, IB2BXmlBookingResultMappingService resultMappingService, IB2BXmlLoginService loginService, IB2BXmlErrorService errorService
            , IValuationResultCaching valuationResultCaching, IB2BXmlBookingLogger bookingLogger, IB2BItineraryBookingService itineraryBookingService)
        {
            this.requestValidator = requestValidator;
            this.requestFactory = requestFactory;
            this.channelInfoService = channelInfoService;
            this.coreBookingService = coreBookingService;
            this.resultMappingService = resultMappingService;
            this.loginService = loginService;
            this.errorService = errorService;
            this.valuationResultCaching = valuationResultCaching;
            this.bookingLogger = bookingLogger;
            this.itineraryBookingService = itineraryBookingService;
        }

        public async Task<string> GetBookingResponseAsync(string request)
        {
            AccommodationBookingRequest coreBookingRequest = null;
            AccommodationBookingResponse coreBookingResponse = null;
            Exception exception = null;
            string response;
            var processStartDate = DateTime.Now;
            var processStatus = Status.NotStarted;
            var bookingId = Guid.NewGuid();
            try
            {
                // create request object
                var otaHotelResRQ = request.XmlDeSerialize<OTA_HotelResRQ>();

                // Validate Request
                requestValidator.ValidateBookingRequest(otaHotelResRQ);

                // get client booking reference
                var bookingReference = otaHotelResRQ.EchoToken;

                // validate user and load user profile
                var clientProfile = await loginService.LoginAsync(otaHotelResRQ);

                //get customerChannel from agent
                var customerChannel = clientProfile.Customers.FirstOrDefault()?.Channel;
                if (null == customerChannel) throw new Exception("Customer does not have an associated channel");

                // get channelinfo from request channel
                var channelInfo = await channelInfoService.GetChannelInfo(customerChannel.Value);

                // get valuation id
                var valuationId = Guid.Parse(otaHotelResRQ.UniqueID[0].ID);

                // get valuated results
                var valuationResult = await valuationResultCaching.GetByValuationIdOrNullAsync(valuationId);

                // create a core booking request
                coreBookingRequest = requestFactory.CreateBookingRequest(otaHotelResRQ, bookingId, channelInfo, clientProfile, valuationId, valuationResult);

                // create itinerary
                var itinerary = await itineraryBookingService.CreateItineraryAsync(clientProfile.Id, channelInfo.Channel, valuationId, valuationResult, coreBookingRequest.Customer, coreBookingRequest.ValuatedRooms);

                // set itinerary parameters
                coreBookingRequest.ItineraryId = itinerary.ItineraryId;
                coreBookingRequest.ItineraryReference = itinerary.ItineraryReference;

                // perform a booking search
                coreBookingResponse = await coreBookingService.ProcessBookingAsync(coreBookingRequest);

                // update itinerary
                await itineraryBookingService.UpdateItineraryAsync(itinerary, coreBookingResponse.Result);

                // map the core results
                var otaHotelAvailRS = resultMappingService.MapFromCoreBookingResults(coreBookingRequest, coreBookingResponse.Result, valuationResult, bookingReference);

                response = otaHotelAvailRS.XmlSerialize();

                // check if valuation failed
                if (coreBookingResponse.ProcessResponseDetails.All(i => i.Exception == null))
                {
                    // update Mongo Status
                    processStatus = Status.Successful;
                }
                else
                {
                    processStatus = Status.Failed;
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                processStatus = Status.Failed;
                response = errorService.CreateBookingErrorXmlResponse(ex);
            }
            await bookingLogger.LogAsync(bookingId, processStartDate, request, processStatus, coreBookingRequest, coreBookingResponse, DateTime.Now - processStartDate, exception);
            return response;
        }
    }
}
