﻿using System;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.B2BXml.Interfaces;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    /// <summary>
    ///     Simple encrypt / decrypt service doing XOR against a predetermined key
    /// </summary>
    public class B2BXmlEncryptDecryptService : IEncryptDecryptService
    {
        private const string XorKey = "SeriousAboutHolidays";

        public string Encrypt(string toEncrypt)
        {
            var encrypted = __Encrypt(toEncrypt);
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(encrypted.ToString()));
        }

        private static StringBuilder __Encrypt(string toEncrypt)
        {
            var chars = toEncrypt.ToCharArray();
            var stringBuilder = new StringBuilder(chars.Length);
            var index = 0;
            var encrypted = chars.Aggregate(stringBuilder, (s, c) => s.Append((char)(c ^ GetCharInPassword(ref index))));
            return encrypted;
        }

        private static char GetCharInPassword(ref int index)
        {
            index = index == XorKey.Length ? 0 : index;
            var resut = XorKey[index];
            index++;
            return resut;
        }

        public string Decrypt(string toDecrypt)
        {
            var bytes = Convert.FromBase64String(toDecrypt);
            return __Encrypt(Encoding.UTF8.GetString(bytes)).ToString();
        }
    }
}