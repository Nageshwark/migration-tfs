﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlValuationRequestValidator : IB2BXmlValuationRequestValidator
    {
        public void ValidateValuationRequest(OTA_HotelValRQ otaHotelValRQ)
        {
            if (ArrayModule.IsNullOrEmpty(otaHotelValRQ.POS) || otaHotelValRQ.POS[0].RequestorID == null)
                throw new Exception("Login credentials are missing.");
            if (otaHotelValRQ.ValRequestSegments == null ||
                ArrayModule.IsNullOrEmpty(otaHotelValRQ.ValRequestSegments.ValRequestSegment))
                throw new Exception("Search parameters are missing.");
            if (ArrayModule.IsNullOrEmpty(otaHotelValRQ.ValRequestSegments.ValRequestSegment[0].RoomStayCandidates))
                throw new Exception("Room Ids are missing.");
            if (
                otaHotelValRQ.ValRequestSegments.ValRequestSegment[0].RoomStayCandidates.Any(
                    i => string.IsNullOrEmpty(i.RoomID)))
                throw new Exception("Room Ids are missing.");
            if (!(new[] { 1m }).Contains(otaHotelValRQ.Version))
                throw new Exception("Request version not supported");
            if (!ValidateRoomStayCandidatesRPH(otaHotelValRQ.ValRequestSegments.ValRequestSegment[0].RoomStayCandidates))
                throw new Exception("RoomStayCandiate RPH mismatch");

        }

        private static bool ValidateRoomStayCandidatesRPH(
            Contracts.OTAHotelVal.AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate[] stayCandidates)
        {
            return (stayCandidates.Count() ==
                   stayCandidates.Select(i => int.Parse(string.IsNullOrEmpty(i.RPH) ? 1.ToString() : i.RPH)).Max());
        }
    }

}

