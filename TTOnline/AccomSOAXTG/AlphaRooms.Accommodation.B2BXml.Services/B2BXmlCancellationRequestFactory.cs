﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationRequestFactory : IB2BXmlCancellationRequestFactory
    {
        public AccommodationCancellationRequest CreateCancellationRequest(OTA_CancelRQ otaCancelRQ, Guid cancellationId, ChannelInfo channelInfo, B2BUser agent)
        {
            return new AccommodationCancellationRequest()
            {
                CancellationId = cancellationId
                , ChannelInfo = channelInfo
                , Agent = agent
                , BookingReferenceId = otaCancelRQ.UniqueID[0]?.ID
            };
        }
    }
}
