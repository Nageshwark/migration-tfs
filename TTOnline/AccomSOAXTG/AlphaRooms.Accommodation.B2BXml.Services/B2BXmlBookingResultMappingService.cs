﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlBookingResultMappingService : IB2BXmlBookingResultMappingService
    {
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BXmlBookingResultMappingService(IAccommodationConfigurationManager configurationManager, IEstablishmentDetailsRepository establishmentDetailsRepository)
        {
            this.configurationManager = configurationManager;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public OTA_HotelResRS MapFromCoreBookingResults(AccommodationBookingRequest coreBookingRequest, AccommodationBookingResult coreResult, AccommodationValuationResult ValuationResult, string bookingReference)
        {
            return new OTA_HotelResRS()
            {
                TimeStamp = DateTime.Now
                ,
                EchoToken = bookingReference
                ,
                Version = 1M
                ,
                Items = new object[]
                {
                    new SuccessType()
                    , new HotelReservationsType()
                    {
                        HotelReservation = coreResult.Rooms.Select(i => CreateOTAHotelReservation(coreBookingRequest, i)).ToArray()
                    }
                },
                //Added to include Fee info of Valuation in Booking Response
                fees = GetFeeType(ValuationResult.Rooms.FirstOrDefault())
            };
        }

        private HotelReservationsTypeHotelReservation CreateOTAHotelReservation(AccommodationBookingRequest coreBookingRequest, AccommodationBookingResultRoom bookingResultRoom)
        {
            return new HotelReservationsTypeHotelReservation()
            {
                ResStatus = bookingResultRoom.ProviderBookingResult.BookingStatus.ToString()
                , UniqueID = new[] { new UniqueID_Type
                {
                    ID = coreBookingRequest.ValuationId.ToString()
                }}
                , ResGlobalInfo = new HotelReservationTypeResGlobalInfo()
                {
                    HotelReservationIDs = new[] { new HotelReservationIDsTypeHotelReservationID()
                    {
                        ResID_Value = coreBookingRequest.ItineraryReference //.BookingId.ToString()
                    }}
                }
            };
        }

        private FeeType[] GetFeeType(AccommodationValuationResultRoom valuationResultRoom)
        {
            var commission =
                decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == Core.Contracts.Enumerators.AccommodationResultPriceItemType.AgentCommission)?
                     .SalePrice.Amount ?? 0m, 2);
            var vat =
               decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == Core.Contracts.Enumerators.AccommodationResultPriceItemType.Vat)?
                    .SalePrice.Amount ?? 0m, 2);
            var basePrice =
                decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == Core.Contracts.Enumerators.AccommodationResultPriceItemType.SellingPrice)?
                    .SalePrice.Amount - commission ?? 0m, 2);
            return new FeeType[]
            {
                   new FeeType { Code = "Base", AmountSpecified = true, Amount = basePrice, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
                        ,  new FeeType { Code = "Commission", AmountSpecified = true, Amount = commission, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
                        , new FeeType { Code = "VAT", AmountSpecified = true, Amount = vat, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
            };
        }
    }
}
