﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlValuationService : IB2BXmlValuationService
    {
        private readonly IB2BXmlValuationRequestValidator requestValidator;
        private readonly IB2BXmlValuationRequestFactory requestFactory;
        private readonly IValuationService coreValuationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BXmlValuationResultMappingService resultMappingService;
        private readonly IB2BXmlLoginService loginService;
        private readonly IB2BXmlErrorService errorService;
        private readonly IB2BXmlValuationLogger valuationLogger;


        public B2BXmlValuationService(IB2BXmlValuationRequestValidator requestValidator, IB2BXmlValuationRequestFactory requestFactory, IValuationService coreValuationService
            , IChannelInfoService channelInfoService, IB2BXmlValuationResultMappingService resultMappingService, IB2BXmlLoginService loginService, IB2BXmlErrorService errorService
            , IB2BXmlValuationLogger valuationLogger)
        {
            this.requestValidator = requestValidator;
            this.requestFactory = requestFactory;
            this.channelInfoService = channelInfoService;
            this.coreValuationService = coreValuationService;
            this.resultMappingService = resultMappingService;
            this.loginService = loginService;
            this.errorService = errorService;
            this.valuationLogger = valuationLogger;
        }

        public async Task<string> GetValuationResponseAsync(string request)
        {
            AccommodationValuationRequest coreValuationRequest = null;
            AccommodationValuationResponse coreValuationResponse = null;
            Exception exception = null;
            string response;
            var processStartDate = DateTime.Now;
            var processStatus = Status.NotStarted;
            var valuationId = Guid.NewGuid();
            try
            {
                // create request object
                var otaHotelValRQ = request.XmlDeSerialize<OTA_HotelValRQ>();

                // Validate Request
                requestValidator.ValidateValuationRequest(otaHotelValRQ);

                // validate user and load user profile
                var clientProfile = await loginService.LoginAsync(otaHotelValRQ);

                // get client valuation reference
                var valuationReference = otaHotelValRQ.EchoToken;

                //get customerChannel from agent
                var customerChannel = clientProfile.Customers.FirstOrDefault()?.Channel;
                if (null == customerChannel) throw new Exception("Customer does not have an associated channel");

                // get channelinfo from request channel
                var channelInfo = await channelInfoService.GetChannelInfo(customerChannel.Value);

                // create a core valuation request
                coreValuationRequest = requestFactory.CreateValuationRequest(otaHotelValRQ, valuationId, channelInfo, clientProfile);

                // perform a valuation search
                coreValuationResponse = await coreValuationService.ProcessValuationAsync(coreValuationRequest, null);

                // map the core results
                var otaHotelValRS = await resultMappingService.MapFromCoreValuationResultsAsync(coreValuationRequest, coreValuationResponse.Result, valuationReference);

                response = otaHotelValRS.XmlSerialize();

                // check if valuation failed
                if (coreValuationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                {
                    // update Mongo Status
                    processStatus = Status.Successful;
                }
                else
                {
                    processStatus = Status.Failed;
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                processStatus = Status.Failed;
                response = errorService.CreateValuationErrorXmlResponse(ex);
            }
            await valuationLogger.LogAsync(valuationId, processStartDate, request, processStatus, coreValuationRequest, coreValuationResponse, DateTime.Now - processStartDate, exception);
            return response;
        }
    }
}
