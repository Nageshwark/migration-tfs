﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlBookingRequestFactory : IB2BXmlBookingRequestFactory
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;

        public B2BXmlBookingRequestFactory(IAccommodationConfigurationManager configurationManager, IAvailabilityResultRoomIdGenerator roomIdGenerator)
        {
            this.configurationManager = configurationManager;
            this.roomIdGenerator = roomIdGenerator;
        }

        public AccommodationBookingRequest CreateBookingRequest(OTA_HotelResRQ otaHotelResRQ, Guid bookingId, ChannelInfo channelInfo, B2BUser agent, Guid valuationId
            , AccommodationValuationResult valuationResult)
        {
            return new AccommodationBookingRequest()
            {
                BookingId = bookingId
                ,
                ValuationId = valuationId
                ,
                Customer = CreateBookingRequestCustomer(otaHotelResRQ.HotelReservations.HotelReservation[0].ResGuests[0])
                ,
                ValuatedRooms = otaHotelResRQ.HotelReservations.HotelReservation.Select((i, j) => CreateBookingRequestRoom(i, valuationResult.Rooms[j])).ToArray()
                ,
                ChannelInfo = channelInfo
                ,
                Agent = agent

            };
        }

        private AccommodationBookingRequestCustomer CreateBookingRequestCustomer(ResGuestType resGuestType)
        {
            return new AccommodationBookingRequestCustomer()
            {
                TitleString = resGuestType.Profiles[0].Profile.Customer.PersonName[0].NameTitle[0]
                ,
                FirstName = resGuestType.Profiles[0].Profile.Customer.PersonName[0].GivenName[0]
                ,
                Surname = resGuestType.Profiles[0].Profile.Customer.PersonName[0].Surname
                ,
                ContactNumber = configurationManager.AccommodationB2BWebBookingCustomerContactNumber
                ,
                EmailAddress = configurationManager.AccommodationB2BWebBookingCustomerEmailAddress
                ,
                Age = configurationManager.AccommodationAdultAge
            };
        }


        private AccommodationBookingRequestRoom CreateBookingRequestRoom(HotelReservationsTypeHotelReservation hotelReservation, AccommodationValuationResultRoom valuationResultRoom)
        {
            return new AccommodationBookingRequestRoom()
            {
                ValuatedRoomId = valuationResultRoom.RoomId
                ,
                SpecialRequests = CreateBookingRoomSpecialRequest(hotelReservation.Services)
                ,
                Guests = this.roomIdGenerator.Parse(valuationResultRoom.RoomId).GuestAges.Select(i => CreateBookingRequestRoomGuest(i)).ToArray()
            };
        }

        private AccommodationBookingRequestRoomSpecialRequest CreateBookingRoomSpecialRequest(ServicesType services)
        {
            //if no Service tag in incoming XML <services/>
            var otherRequests = services?.Service?.FirstOrDefault(i => i.Type == "OtherRequests") ??
                                new ServiceType() { Value = string.Empty };
            return (new AccommodationBookingRequestRoomSpecialRequest()
            {
                AdjoiningRooms = GetSpecialRequestValue(services, "AdjoiningRooms"),
                CotRequired = GetSpecialRequestValue(services, "CotRequired"),
                DisabledAccess = GetSpecialRequestValue(services, "DisabledAccess"),
                LateArrival = GetSpecialRequestValue(services, "LateArrival"),
                NonSmoking = GetSpecialRequestValue(services, "NonSmoking"),
                SeaViews = GetSpecialRequestValue(services, "SeaViews"),
                OtherRequests = otherRequests.Value
            });
        }

        private bool GetSpecialRequestValue(ServicesType services, string requestName)
        {
            var serviceRequest = services?.Service.FirstOrDefault(i => string.Equals(i.Type, requestName, StringComparison.CurrentCultureIgnoreCase));
            return (serviceRequest != null && string.Equals(serviceRequest.Value, "y", StringComparison.CurrentCultureIgnoreCase));
        }

        private AccommodationBookingRequestRoomGuest CreateBookingRequestRoomGuest(byte guestAge)
        {
            return new AccommodationBookingRequestRoomGuest()
            {
                Age = guestAge
            };
        }
    }
}
