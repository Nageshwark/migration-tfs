﻿using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlApplyAgentConfigurationService : IB2BXmlApplyAgentConfigurationService
    {
       public IList<AccommodationAvailabilityResult> ApplyAgentConfiguration(AccommodationAvailabilityRequest request, IList<AccommodationAvailabilityResult> results)
       {
            //ACS-878 Filter out paydirect results 
            //we have to filter gross rates as per suppliers in agent configuration => ACS-1103
            var agentConfiguration = request.Agent.AgentConfiguration;

            var paydirectFilter =
                results.Where(i => i.ProviderResult.PaymentModel != PaymentModel.CustomerPayDirect).AsQueryable();

            var grossRatesfilter =
                paydirectFilter.Where(r => r.ProviderResult.RateType != RateType.GrossStandard);

            // all results - without Gross Rates
            if (agentConfiguration?.Any() ?? false)
            {
                grossRatesfilter = agentConfiguration.Where(agentConfig => agentConfig.GrossRatesEnabled)
                    .Aggregate(grossRatesfilter,
                        (current, agentConfig) =>
                            current.Union(paydirectFilter.Where(r =>
                                (agentConfig.SupplierId == null
                                    ? r.Provider.Id  == agentConfig.ProviderId
                                    : r.Supplier.Id == agentConfig.SupplierId)
                                && r.ProviderResult.RateType == RateType.GrossStandard)));
            }

            results = grossRatesfilter.ToArray();
            return results;
        }
    }
}
