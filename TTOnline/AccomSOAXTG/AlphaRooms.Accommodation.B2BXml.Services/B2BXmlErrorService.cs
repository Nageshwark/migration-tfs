﻿using OTAHotelAvail = AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using OTAHotelVal = AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using OTAHotelRes = AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using OTACancel = AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlErrorService : IB2BXmlErrorService
    {
        private readonly ILogger _errorLogger;

        public B2BXmlErrorService(ILogger errorLogger)
        {
            _errorLogger = errorLogger;
        }

        public string CreateAvailabilityErrorXmlResponse(Exception ex)
        {
            _errorLogger.Error(ex.GetDetailedMessageWithInnerExceptions());

            return new OTAHotelAvail.OTA_HotelAvailRS()
            {
                Items = new object[] { new OTAHotelAvail.ErrorsType() { Error = new[] { new OTAHotelAvail.ErrorType()
                {
                    ShortText = ex.Message
                    , Code = ex.HResult.ToString()

                }}}}
            }.XmlSerialize();
        }

        public string CreateValuationErrorXmlResponse(Exception ex)
        {
            _errorLogger.Error(ex.GetDetailedMessageWithInnerExceptions());

            return new OTAHotelVal.OTA_HotelValRS()
            {
                Items = new object[] { new OTAHotelVal.ErrorsType() { Error = new[] { new OTAHotelVal.ErrorType()
                {
                    ShortText = ex.Message
                    , Code = ex.HResult.ToString()

                }}}}
            }.XmlSerialize();
        }

        public string CreateBookingErrorXmlResponse(Exception ex)
        {
            _errorLogger.Error(ex.GetDetailedMessageWithInnerExceptions());
            return new OTAHotelRes.OTA_HotelResRS()
            {
                Items = new object[] { new OTAHotelRes.ErrorsType() { Error = new[] { new OTAHotelRes.ErrorType()
                {
                    ShortText = ex.Message
                    , Code = ex.HResult.ToString()

                }}}}
            }.XmlSerialize();
        }

        public string CreateCancellationErrorXmlResponse(Exception ex)
        {
            _errorLogger.Error(ex.GetDetailedMessageWithInnerExceptions());
            return new OTACancel.OTA_CancelRS()
            {
                Items = new object[] { new OTACancel.ErrorsType() { Error = new[] { new OTACancel.ErrorType()
                {
                    ShortText = ex.Message
                    , Code = ex.HResult.ToString()

                }}}}
            }.XmlSerialize();
        }
    }
}
