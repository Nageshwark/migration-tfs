﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.B2BXml.Contracts;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityResultMappingService : IB2BXmlAvailabilityResultMappingService
    {
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IFacilityRepository facilityRepository;
        private readonly IEncryptDecryptService encryptDecryptService;
        private readonly bool EmitRoomCodes = false;
        private const string EmitRoomCodesKey = "B2BXmlIncludeRoomCodes";

        public B2BXmlAvailabilityResultMappingService(IAccommodationConfigurationManager configurationManager, IEstablishmentDetailsRepository establishmentDetailsRepository
            , IFacilityRepository facilityRepository, IEncryptDecryptService encryptDecryptService)
        {
            this.configurationManager = configurationManager;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
            this.facilityRepository = facilityRepository;
            this.encryptDecryptService = encryptDecryptService;
            this.EmitRoomCodes = bool.Parse(this.configurationManager[EmitRoomCodesKey, "false"]);
        }

        public async Task<OTA_HotelAvailRS> MapFromCoreAvailabilityResultsAsync(AccommodationAvailabilityRequest coreAvailabilityRequest, IList<AccommodationAvailabilityResult> coreResults, string availabilityReference)
        {
            var establishmentDetails = (coreResults.Any() ? await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreResults[0].BaseDestinationId, coreAvailabilityRequest.ChannelInfo.Channel) : null);
            var guestCounts = coreAvailabilityRequest.Rooms.ToDictionary(i => i.RoomNumber, i => GetOTAGuestCounts(i));
            var facilities = await facilityRepository.GetAllDictionaryAsync();
            return new OTA_HotelAvailRS()
            {
                TimeStamp = DateTime.Now
                ,
                EchoToken = availabilityReference
                ,
                Version = 1M
                ,
                Items = new object[]
                {
                    new SuccessType()
                    , new OTA_HotelAvailRSRoomStays()
                    {
                        RoomStay = coreResults.GroupBy(i => new { i.EstablishmentId, i.RoomNumber }).AsParallel().Select(i => CreateOTAHotelAvailRSRoomStaysRoomStay(i.Key.RoomNumber, i.Key.EstablishmentId, i.ToArray(), guestCounts, establishmentDetails[i.Key.EstablishmentId], facilities)).ToArray()
                    }
                }
            };
        }

        private OTA_HotelAvailRSRoomStaysRoomStay CreateOTAHotelAvailRSRoomStaysRoomStay(byte roomNumber, Guid establishmentId, AccommodationAvailabilityResult[] availabilityResults
            , Dictionary<byte, GuestCountType> guestCounts, EstablishmentDetails establishmentDetails, Dictionary<Guid, Facility> facilities)
        {
            return new OTA_HotelAvailRSRoomStaysRoomStay()
            {
                RoomStayCandidateRPH = roomNumber.ToString()
                ,
                TimeSpan = CreateOTADateTimeSpanType(availabilityResults[0])
                ,
                GuestCounts = guestCounts[roomNumber]
                ,
                BasicPropertyInfo = CreateOTABasicPropertyInfo(establishmentDetails, facilities)
                ,
                RoomRates = new RoomStayTypeRoomRates
                {
                    RoomRate = availabilityResults.Select(i => CreateOTARoomStayTypeRoomRatesRoomRate(i)).ToArray()
                }
            };
        }

        private DateTimeSpanType CreateOTADateTimeSpanType(AccommodationAvailabilityResult availabilityResult)
        {
            return new DateTimeSpanType()
            {
                Start = availabilityResult.ProviderResult.CheckInDate.ToString("yyyy-MM-dd")
                ,
                End = availabilityResult.ProviderResult.CheckOutDate.ToString("yyyy-MM-dd")
            };
        }

        private GuestCountType GetOTAGuestCounts(AccommodationAvailabilityRequestRoom requestRoom)
        {
            return new GuestCountType()
            {
                GuestCount = requestRoom.Guests.GroupBy(i => i.Age).Select(i => CreateOTAGuestCount(requestRoom, i.Key, i.ToArray())).ToArray()
            };
        }

        private GuestCountTypeGuestCount CreateOTAGuestCount(AccommodationAvailabilityRequestRoom requestRoom, int age, AccommodationAvailabilityRequestRoomGuest[] requestRoomGuest)
        {
            return new GuestCountTypeGuestCount()
            {
                AgeQualifyingCode = (age > configurationManager.AccommodationMaxChildAge ? "10" : "8")
                ,
                Age = age.ToString()
                ,
                Count = requestRoomGuest.Length.ToString()
            };
        }

        private RoomStayTypeBasicPropertyInfo CreateOTABasicPropertyInfo(EstablishmentDetails establishmentDetails, Dictionary<Guid, Facility> facilities)
        {
            return new RoomStayTypeBasicPropertyInfo()
            {
                AreaID = establishmentDetails.DestinationId.ToString()
                ,
                HotelCode = establishmentDetails.EstablishmentId.ToString()
                ,
                HotelName = establishmentDetails.EstablishmentName
                ,
                HotelCityCode = establishmentDetails.CountrySeoFriendlyName
                ,
                HotelAmenity = establishmentDetails.FacilityIds.Select(i => CreateOTABasicPropertyInfoTypeHotelAmenity(facilities[i])).ToArray()
            };
        }

        private BasicPropertyInfoTypeHotelAmenity CreateOTABasicPropertyInfoTypeHotelAmenity(Facility facility)
        {
            return new BasicPropertyInfoTypeHotelAmenity()
            {
                Code = facility.Name
            };
        }

        private RoomStayTypeRoomRatesRoomRate CreateOTARoomStayTypeRoomRatesRoomRate(AccommodationAvailabilityResult result)
        {
            var commission =
               decimal.Round(result.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.AgentCommission)?
                    .SalePrice.Amount ?? 0m, 2);
            var vat =
               decimal.Round(result.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.Vat)?
                    .SalePrice.Amount ?? 0m, 2);
            var basePrice =
               decimal.Round(result.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.SellingPrice)?
                    .SalePrice.Amount ?? 0m, 2);
            var roomstay = new RoomStayTypeRoomRatesRoomRate()
            {
                RoomID = encryptDecryptService.Encrypt(result.RoomId) 
                ,
                RoomRateDescription = new[] { CreateOTAParagraphType(result.RoomDescription) }
                ,
                RoomTypeCode = result.BoardType.ToString()
                ,
                NumberOfUnits = result.ProviderResult.NumberOfAvailableRooms?.ToString() ?? "unknown"
                ,
                Rates = new[] { new RateTypeRate() { Fees = new[]
                {
                    new FeeType { Code = "Base", AmountSpecified = true, Amount = basePrice , CurrencyCode = result.Price.CurrencyCode }
                    ,  new FeeType { Code = "Commission", AmountSpecified = true, Amount = commission , CurrencyCode = result.Price.CurrencyCode }
                    , new FeeType { Code = "VAT", AmountSpecified = true, Amount = vat , CurrencyCode = result.Price.CurrencyCode }
                }}}
                ,
                Total = new TotalType()
                {
                    AmountAfterTaxSpecified = true
                    ,
                    AmountAfterTax = basePrice + vat 
                    ,
                    CurrencyCode = result.Price.CurrencyCode
                    ,
                    TPA_Extensions = CreateTPAExtensions(result)
                }
            };
            if (EmitRoomCodes)
            {
                roomstay.RatePlanCode = result.ProviderResult.RoomCode + "##" + result.ProviderResult.SupplierEdiCode +
                                        "##" + result.ProviderResult.CostPrice + "##" + result.ProviderResult.RateType + "##"+ result.ProviderResult.PaymentModel;
            }
            return roomstay;
        }

        private TPA_ExtensionsType CreateTPAExtensions(AccommodationAvailabilityResult result)
        {
            return new TPA_ExtensionsType()
            {
                Any = new[] { new OTAHotelAvailRSRoomRatesRoomRateTPA_Extension()
                {
                    IsBidingRate = (result.ProviderResult.IsBindingRate ? "Y" : "N")
                    , IsRefundableRate = (result.ProviderResult.IsNonRefundable ? "N" : "Y")
                    , IsDirectPayment = (result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect ? "Y" : "N")
                    , IsOpaqueRate = (result.ProviderResult.IsOpaqueRate ? "Y" : "N")
                    , IsSpecialRate = "N"
                }}
            };
        }

        private ParagraphType CreateOTAParagraphType(string text)
        {
            return new ParagraphType()
            {
                ItemsElementName = new[] { ItemsChoiceType.Text }
                ,
                Items = new[] { new FormattedTextTextType { Value = text } }
            };
        }
    }
}
