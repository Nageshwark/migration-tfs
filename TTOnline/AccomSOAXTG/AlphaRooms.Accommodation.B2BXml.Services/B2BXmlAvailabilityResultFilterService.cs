﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.B2BXml.Contracts;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityResultFilterService : IB2BXmlAvailabilityResultFilterService
    {
        public IList<AccommodationAvailabilityResult> FilterResults(OTA_HotelAvailRQ otaHotelAvailRQ, IList<AccommodationAvailabilityResult> results)
        {

            var criterion =
                otaHotelAvailRQ.AvailRequestSegments?.AvailRequestSegment?[0].HotelSearchCriteria?.Criterion?[0];
            var filterParameters = criterion?.TPA_Extensions?.Any?[0] as OTAHotelAvailRSRoomRatesRoomRateTPA_Extension;
            var query = LinqExpression.CreateTrue<AccommodationAvailabilityResult>();
            var applyFilter = false;
            //if (string.Equals(filterParameters?.IsDirectPayment, "y", StringComparison.CurrentCultureIgnoreCase)) ACS-878
            //{
            //    var tempPaymentModel = PaymentModel.CustomerPayDirect;
            //    query = query.And(i => i.ProviderResult.PaymentModel == tempPaymentModel);
            //    applyFilter = true;
            //}
            if (string.Equals(filterParameters?.IsOpaqueRate, "n", StringComparison.CurrentCultureIgnoreCase))
            {
                query = query.And(i => i.ProviderResult.IsOpaqueRate != true);
                applyFilter = true;
            }

            //Added filter for Establishments - ACS-771
            var c = criterion?.HotelRef.Where(i => i.HotelCode != null).Select(i => Guid.Parse(i.HotelCode)).ToArray() ?? new Guid[] { };
            if (c.Any())
            {
                /*Subject to more RnD - not working
                query = c.Aggregate(query,
                   (current, hotelcode) => current.And(e=>e.EstablishmentId == hotelcode));
                applyFilter = true;*/

                var finalResults = new List<AccommodationAvailabilityResult>();
                results = c.Aggregate(finalResults,
                    (current, hotelCode) =>
                    {
                        current.AddRange(results.Where(i => i.EstablishmentId == hotelCode));
                        return current;
                    });
            }
            return (applyFilter ? results.AsQueryable().Where(query).ToArray() : results);

        }

    }
}