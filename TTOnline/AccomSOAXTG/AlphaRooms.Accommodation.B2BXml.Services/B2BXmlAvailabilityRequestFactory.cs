﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAvailabilityRequestFactory : IB2BXmlAvailabilityRequestFactory
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BXmlAvailabilityRequestFactory(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public AccommodationAvailabilityRequest CreateAvailabilityRequest(OTA_HotelAvailRQ otaHotelAvailRQ, Guid availabilityId, ChannelInfo channelInfo, B2BUser agent
            , AccommodationProvider[] providers)
        {
            var availRequestSegment = otaHotelAvailRQ.AvailRequestSegments.AvailRequestSegment[0];
            var criterion = availRequestSegment.HotelSearchCriteria.Criterion[0];
            return new AccommodationAvailabilityRequest()
            {
                AvailabilityId = availabilityId
                , ChannelInfo = channelInfo
                , Agent = agent
                , Providers = providers
                , CheckInDate = DateTime.Parse(criterion.StayDateRange.Start)
                , CheckOutDate = DateTime.Parse(criterion.StayDateRange.End)
                , DestinationId = (!string.IsNullOrEmpty(criterion.HotelRef[0].AreaID) ? (Guid?)Guid.Parse(criterion.HotelRef[0].AreaID) : null)
                , EstablishmentId = (!string.IsNullOrEmpty(criterion.HotelRef[0].HotelCode) ? (Guid?)Guid.Parse(criterion.HotelRef[0].HotelCode) : null)
                , Rooms = CreateAvailabilityRequestRooms(criterion)
                , ProviderCacheEnabled = configurationManager.AccommodationB2BXmlEnableProviderCache
        };
        }

        private AccommodationAvailabilityRequestRoom[] CreateAvailabilityRequestRooms(HotelSearchCriteriaTypeCriterion criterion)
        {
            return criterion.RoomStayCandidates.OrderBy(i => (!string.IsNullOrEmpty(i.RPH) ? int.Parse(i.RPH) : 0))
                .SelectMany(i => Enumerable.Repeat(CreateAvailabilityRequestRoom(i), (!string.IsNullOrEmpty(i.Quantity) ? int.Parse(i.Quantity) : 1))).ToArray();
        }

        private AccommodationAvailabilityRequestRoom CreateAvailabilityRequestRoom(RoomStayCandidateType roomStayCandidate)
        {
            return new AccommodationAvailabilityRequestRoom()
            {
                RoomNumber = byte.Parse(roomStayCandidate.RPH)
                , Guests = roomStayCandidate.GuestCounts.GuestCount.SelectMany(i => Enumerable.Repeat(CreateAvailabilityRequestRoomGuest(i)
                    , (!string.IsNullOrEmpty(i.Count) ? int.Parse(i.Count) : 1))).ToArray()
            };
        }

        private AccommodationAvailabilityRequestRoomGuest CreateAvailabilityRequestRoomGuest(GuestCountTypeGuestCount guestCount)
        {
            return new AccommodationAvailabilityRequestRoomGuest()
            {
                Age = (!string.IsNullOrEmpty(guestCount.Age) ? byte.Parse(guestCount.Age) : (guestCount.AgeQualifyingCode == "10" ? (byte)configurationManager.AccommodationAdultAge : (byte)255))
            };
        }
    }
}
