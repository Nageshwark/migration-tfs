﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Services;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlAccommodationBusiness : IAccommodationBusiness
    {
        private readonly IAvailabilityResultLowerPriceService resultLowerPriceService;
        private readonly IAvailabilityResultConsolidationService resultConsolidationService;
        private readonly IAvailabilityResultMarkupService resultMarkupService;
        private readonly IAvailabilityFlashSaleService flashSaleService;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IB2BXmlApplyAgentConfigurationService agentConfigurationService;

        public B2BXmlAccommodationBusiness(IPriceCalculatorService priceCalculatorService, AccommodationPromotionalDiscountService promotionalDiscountService, IAvailabilityFlashSaleService flashSaleService, IAvailabilityResultMarkupService resultMarkupService, IAvailabilityResultConsolidationService resultConsolidationService, IAvailabilityResultLowerPriceService resultLowerPriceService, IB2BXmlApplyAgentConfigurationService agentConfigurationService)
        {
            this.priceCalculatorService = priceCalculatorService;
            this.promotionalDiscountService = promotionalDiscountService;
            this.flashSaleService = flashSaleService;
            this.resultMarkupService = resultMarkupService;
            this.resultConsolidationService = resultConsolidationService;
            this.resultLowerPriceService = resultLowerPriceService;
            this.agentConfigurationService = agentConfigurationService;
        }

        public void FilterBySupplier(string[] suppliersToSearch, B2BUser agent, AccommodationAvailabilityResponse coreAvailabilityResponse)
        {
            // If a provider is mapped against an agent, by default it takes all suppliers associated to it.
            // In the other hand if you want to restrict by supplier you have to map it by agent (especially for Aria Contracts).
            if (suppliersToSearch == null || !suppliersToSearch.Any())
            {
                return;
            }

            // Filter results by supplier
            var results = coreAvailabilityResponse.Results;
            var supplierResults = results.Where(r => r.Supplier.EdiCode != null && suppliersToSearch.Contains(r.Supplier.EdiCode)).ToArray();

            var providers = agent.Providers.Select(x => x.EdiCode).Distinct().ToArray();
            var ediProvidersFromSuppliers =
                agent.Providers.Where(x => agent.Suppliers.Select(y => y.ProviderId).Contains(x.Id))
                    .Select(z => z.EdiCode)
                    .Distinct()
                    .ToArray();

            var providersToSearch = providers.Except(ediProvidersFromSuppliers.Intersect(providers)).ToArray();

            var providerResults = results.Where(r => providersToSearch.Contains(r.Provider.EdiCode));

            coreAvailabilityResponse.Results = supplierResults.Union(providerResults).ToArray();
        }

        public async Task<IList<AccommodationAvailabilityResult>> PostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            // filter payDirect nonRefundable - 
            //TODO: THIS IS A HACK. NEED TO REMOVE FROM HERE AND MOVE TO PROVIDERS
            results = results.Where(i => i.ProviderResult.PaymentModel != SOACommon.Contracts.Enumerators.PaymentModel.CustomerPayDirect
                                                    || i.ProviderResult.IsNonRefundable == false).ToArray();

            //filter results according to 
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_FilterByAgentConfiguration");
            results = agentConfigurationService.ApplyAgentConfiguration(availabilityRequest, results);
            timer.Record();

            // filter results for flash sale
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_FilterFlashSale");
            results = await flashSaleService.FilterResultsForFlashSaleAsync(availabilityRequest, results);
            timer.Record();

            // apply markup 
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Markups");
            results = await resultMarkupService.ApplyAvailabilityMarkupAsync(availabilityRequest, results);
            timer.Record();

            // process room switching
            //results = roomSwitchingService.ApplyAvailabilityRoomSwitching(availabilityRequest, results);

            // process price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Pricing");
            results = priceCalculatorService.ApplyAvailabilityPriceCalculation(availabilityRequest, results);
            timer.Record();

            //Apply promotional discounts
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Promotions");
            promotionalDiscountService.ProcessDiscounts(results, availabilityRequest.Voucher, availabilityRequest.ChannelInfo.Channel);
            timer.Record();

            // consolidate the accommodation results and highest price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Consolidate");
            results = resultConsolidationService.ConsolidateAvailabilityResults(availabilityRequest, results);
            timer.Record();

            // identify the lower price rooms
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_LowerPrice");
            results = resultLowerPriceService.ApplyAvailabilityLowerPrice(results);
            timer.Record();

            //Remove establisments which do not have all the rooms
            var roomsCount = Enumerable.Range(1, results.Max(i => i.RoomNumber)).Count();
            if (roomsCount > 1)
            {
                results =
                    results.GroupBy(i => i.EstablishmentId).AsParallel()
                        .Where(i => i.Select(r => r.RoomNumber).Distinct().Count() == roomsCount)
                        .SelectMany(i => i)
                        .ToArray();
            }
            return results;
        }

		public IList<AccommodationAvailabilityResult> TravelGatePostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
		{
			throw new NotImplementedException();
		}
	}
}
