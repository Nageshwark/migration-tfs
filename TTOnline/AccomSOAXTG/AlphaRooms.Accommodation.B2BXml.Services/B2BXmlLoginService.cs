﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.Infrastructure;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlLoginService : IB2BXmlLoginService
    {
        private readonly IB2BUserRepository accommodationAgentRepository;
        private readonly IAuthenticateUserService authenticationService;

        public B2BXmlLoginService(IB2BUserRepository accommodationAgentRepository, IAuthenticateUserService authenticationService)
        {
            this.accommodationAgentRepository = accommodationAgentRepository;
            this.authenticationService = authenticationService;
        }

        public async Task<B2BUser> LoginAsync(OTA_HotelAvailRQ otaHotelAvailRQ)
        {
            return await LoginAsync(otaHotelAvailRQ.POS[0].RequestorID.ID, otaHotelAvailRQ.POS[0].RequestorID.MessagePassword, otaHotelAvailRQ.POS[0]?.ERSP_UserID);
        }

        public async Task<B2BUser> LoginAsync(OTA_HotelValRQ otaHotelValRQ)
        {
            return await LoginAsync(otaHotelValRQ.POS[0].RequestorID.ID, otaHotelValRQ.POS[0].RequestorID.MessagePassword, otaHotelValRQ.POS[0]?.ERSP_UserID);
        }

        public async Task<B2BUser> LoginAsync(OTA_HotelResRQ otaHotelResRQ)
        {
            return await LoginAsync(otaHotelResRQ.POS[0].RequestorID.ID, otaHotelResRQ.POS[0].RequestorID.MessagePassword, otaHotelResRQ.POS[0]?.ERSP_UserID);
        }

        public async Task<B2BUser> LoginAsync(OTA_CancelRQ otaCancelRQ)
        {
            return await LoginAsync(otaCancelRQ.POS[0].RequestorID.ID, otaCancelRQ.POS[0].RequestorID.MessagePassword, otaCancelRQ.POS[0]?.ERSP_UserID);
        }

        private async Task<B2BUser> LoginAsync(string userName, string password, string agentIdentifier = "")
        {
            // ACS - 854
            IAuthenticateUserResponse authentication;
            if (!string.IsNullOrWhiteSpace(agentIdentifier))
            {
                authentication = await authenticationService.XmlAuthenticateAgentAsync(userName, password, agentIdentifier);
            }
            else
            {
                authentication = await authenticationService.AuthenticateAgentAsync(userName, password);
            }

            if (!authentication.IsAuthenticated) throw new Exception("Invalid credentials");
            return await accommodationAgentRepository.GetByIdAsync(authentication.AuthenticatedUserId.GetValueOrDefault());
        }
    }
}
