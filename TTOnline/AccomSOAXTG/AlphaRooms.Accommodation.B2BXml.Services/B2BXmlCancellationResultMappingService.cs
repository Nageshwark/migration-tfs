﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationResultMappingService : IB2BXmlCancellationResultMappingService
    {
        public OTA_CancelRS MapFromCoreCancellationResults(AccommodationCancellationRequest coreCancellationRequest, AccommodationCancellationResult result, string bookingReference)
        {
            return new OTA_CancelRS()
            {
                TimeStamp = DateTime.Now
                , EchoToken = bookingReference
                , Version = 1M
                , Items = new object[] 
                {
                    new SuccessType()
                    , new CancelInfoRSType()
                    { 
                        UniqueID = new UniqueID_Type()
                        {
                            ID = result.CancellationId.ToString()
                        }
                    }
                }
            };
        }
    }
}
