﻿namespace AlphaRooms.Accommodation.B2BXml.Services
{
    using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
    using AlphaRooms.Accommodation.B2BXml.Interfaces;
    using AlphaRooms.Utilities;
    using System;
    using System.Linq;
    using AlphaRooms.SOACommon.Interfaces;

    public class B2BXmlAvailabilityRequestValidator : IB2BXmlAvailabilityRequestValidator
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BXmlAvailabilityRequestValidator(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public void ValidateAvailabilityRequest(OTA_HotelAvailRQ otaHotelAvailRQ)
        {
            if (ArrayModule.IsNullOrEmpty(otaHotelAvailRQ.POS) || otaHotelAvailRQ.POS[0].RequestorID == null) //ACS-854 || otaHotelAvailRQ.POS[0].ERSP_UserID == null)
            { 
                throw new Exception("Login credentials are missing.");
            }
            if (otaHotelAvailRQ.AvailRequestSegments == null || ArrayModule.IsNullOrEmpty(otaHotelAvailRQ.AvailRequestSegments.AvailRequestSegment))
            { 
                throw new Exception("Search parameters are missing.");
            }
            if (otaHotelAvailRQ.AvailRequestSegments.AvailRequestSegment[0].HotelSearchCriteria == null)
            { 
                throw new Exception("Location parameters are missing.");
            }
            if (ArrayModule.IsNullOrEmpty(otaHotelAvailRQ.AvailRequestSegments.AvailRequestSegment[0].HotelSearchCriteria.Criterion))
            { 
                throw new Exception("Search criterion is missing.");
            }
            if (!(new[] { 1m }).Contains(otaHotelAvailRQ.Version))
            { 
                throw new Exception("Request version not supported");
            }
            foreach (var criterion in otaHotelAvailRQ.AvailRequestSegments.AvailRequestSegment[0].HotelSearchCriteria.Criterion)
            {
                if (string.IsNullOrEmpty(criterion.StayDateRange?.Start) || string.IsNullOrEmpty(criterion.StayDateRange.End))
                { 
                    throw new Exception("Date range parameters are missing.");
                }
                if (DateTime.Parse(criterion.StayDateRange?.Start) < DateTime.Now)
                { 
                    throw  new Exception("Invalid Start Date");
                }
                if (DateTime.Parse(criterion.StayDateRange?.Start) >= DateTime.Parse(criterion.StayDateRange?.End))
                { 
                    throw  new Exception("Invalid StayDateRange");
                }
                if ((DateTime.Parse(criterion.StayDateRange?.Start) - DateTime.Today).TotalDays < this.configurationManager.AccommodationB2BXMLGlobalLeadTime)
                {
                    throw new Exception("Invalid LeadTime");
                }
                if (ArrayModule.IsNullOrEmpty(criterion.RoomStayCandidates))
                { 
                    throw new Exception("Room parameters are missing.");
                }
                if (criterion.RoomStayCandidates.Any(i => i.GuestCounts == null || ArrayModule.IsNullOrEmpty(i.GuestCounts.GuestCount)))
                { 
                    throw new Exception("Guest parameters are missing.");
                }
                if (criterion.RoomStayCandidates.Any(i => i.GuestCounts.GuestCount.Any(j => j.AgeQualifyingCode != "10" && string.IsNullOrEmpty(j.Age))))
                { 
                    throw new Exception("Guest with missing age.");
                }
                if (criterion.RoomStayCandidates.Count() !=
                    criterion.RoomStayCandidates.Select(i => int.Parse( string.IsNullOrEmpty(i.RPH) ? 1.ToString() : i.RPH )).Max())
                {
                    throw new Exception("RoomStayCandidate RPH mismatch");
                }
            }

        }
    }
}
