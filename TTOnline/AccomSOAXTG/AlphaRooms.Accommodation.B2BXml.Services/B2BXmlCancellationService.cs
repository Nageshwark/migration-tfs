﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlCancellationService : IB2BXmlCancellationService
    {
        private readonly IB2BXmlCancellationRequestValidator requestValidator;
        private readonly IB2BXmlCancellationRequestFactory requestFactory;
        private readonly ICancellationService coreCancellationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BXmlCancellationResultMappingService resultMappingService;
        private readonly IB2BXmlLoginService loginService;
        private readonly IB2BXmlErrorService errorService;
        private readonly IB2BXmlCancellationLogger cancellationLogger;

        public B2BXmlCancellationService(IB2BXmlCancellationRequestValidator requestValidator, IB2BXmlCancellationRequestFactory requestFactory, [Named("B2BXML")]ICancellationService coreCancellationService
            , IChannelInfoService channelInfoService, IB2BXmlCancellationResultMappingService resultMappingService, IB2BXmlLoginService loginService, IB2BXmlErrorService errorService
            , IB2BXmlCancellationLogger cancellationLogger)
        {
            this.requestValidator = requestValidator;
            this.requestFactory = requestFactory;
            this.channelInfoService = channelInfoService;
            this.coreCancellationService = coreCancellationService;
            this.resultMappingService = resultMappingService;
            this.loginService = loginService;
            this.errorService = errorService;
            this.cancellationLogger = cancellationLogger;
        }

        public async Task<string> GetCancellationResponseAsync(string request)
        {
            AccommodationCancellationRequest coreCancellationRequest = null;
            AccommodationCancellationResponse coreCancellationResponse = null;
            Exception exception = null;
            string response;
            var processStartDate = DateTime.Now;
            var processStatus = Status.NotStarted;
            var cancellationId = Guid.NewGuid();
            try
            {
                // create request object
                var otaCancelRQ = request.XmlDeSerialize<OTA_CancelRQ>();

                // Validate Request
                requestValidator.ValidateCancellationRequest(otaCancelRQ);

                // get client booking reference
                var bookingReference = otaCancelRQ.EchoToken;

                // validate user and load user profile
                var clientProfile = await loginService.LoginAsync(otaCancelRQ);

                //get customerChannel from agent
                var customerChannel = clientProfile.Customers.FirstOrDefault()?.Channel;
                if (null == customerChannel) throw new Exception("Customer does not have an associated channel");

                // get channelinfo from request channel
                var channelInfo = await channelInfoService.GetChannelInfo(customerChannel.Value);

                // create a core booking request
                coreCancellationRequest = requestFactory.CreateCancellationRequest(otaCancelRQ, cancellationId, channelInfo, clientProfile);

                // perform a booking search
                coreCancellationResponse = await coreCancellationService.ProcessCancellationAsync(coreCancellationRequest);

                // map the core results
                var otaCancelRS = resultMappingService.MapFromCoreCancellationResults(coreCancellationRequest, coreCancellationResponse.Result, bookingReference);

                response = otaCancelRS.XmlSerialize();

                if (coreCancellationResponse.ProcessResponseDetails != null)
                {
                    // check if valuation failed
                    if (coreCancellationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        processStatus = Status.Successful;
                    }
                    else
                    {
                        processStatus = Status.Failed;
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                processStatus = Status.Failed;
                response = errorService.CreateCancellationErrorXmlResponse(ex);
            }
            if (coreCancellationResponse?.ProcessResponseDetails != null)
                await cancellationLogger.LogAsync(cancellationId, processStartDate, request, processStatus, coreCancellationRequest, coreCancellationResponse, DateTime.Now - processStartDate, exception);

            return response;
        }
    }
}