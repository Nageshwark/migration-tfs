﻿using AlphaRooms.Accommodation.B2BXml.Contracts;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlValuationResultMappingService : IB2BXmlValuationResultMappingService
    {
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IFacilityRepository facilityRepository;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IEncryptDecryptService encryptDecryptService;

        public B2BXmlValuationResultMappingService(IAccommodationConfigurationManager configurationManager, IEstablishmentDetailsRepository establishmentDetailsRepository
            , IFacilityRepository facilityRepository, IAvailabilityResultRoomIdGenerator roomIdGenerator, IEncryptDecryptService encryptDecryptService)
        {
            this.configurationManager = configurationManager;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
            this.facilityRepository = facilityRepository;
            this.roomIdGenerator = roomIdGenerator;
            this.encryptDecryptService = encryptDecryptService;
        }

        public async Task<OTA_HotelValRS> MapFromCoreValuationResultsAsync(AccommodationValuationRequest coreValuationRequest, AccommodationValuationResult coreResult, string valuationReference)
        {
            var groupEstablishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreResult.Rooms[0].BaseDestinationId, coreValuationRequest.ChannelInfo.Channel);
            var establishmentDetails = groupEstablishmentDetails[coreResult.Rooms[0].EstablishmentId];
            var facilities = await facilityRepository.GetAllDictionaryAsync();
            var roomDetails = coreValuationRequest.SelectedRoomIds.Select(i => roomIdGenerator.Parse(i)).ToArray();
            return new OTA_HotelValRS()
            {
                TimeStamp = DateTime.Now
                ,
                EchoToken = valuationReference
                ,
                Version = 1M
                ,
                Items = new object[]
                {
                   new SuccessType()
                    , new OTA_HotelValRSRoomStays()
                    {
                        RoomStay = coreResult.Rooms.Select((i, j) => CreateOTAHotelValRSRoomStaysRoomStay(i.RoomNumber, i, establishmentDetails, facilities, roomDetails[j])).ToArray()
                    }
                    , new OTAHotelValTPA_Extensions()
                    {
                        UniqueID = coreValuationRequest.ValuationId.ToString()
                    }
                }                 
            };
        }

        private OTA_HotelValRSRoomStaysRoomStay CreateOTAHotelValRSRoomStaysRoomStay(byte roomNumber, AccommodationValuationResultRoom valuationResultRoom
            , EstablishmentDetails establishmentDetails, Dictionary<Guid, Facility> facilities, AccommodationAvailabilityResultRoomIdWrapper roomDetails)
        {
            return new OTA_HotelValRSRoomStaysRoomStay()
            {
                RoomStayCandidateRPH = roomNumber.ToString()
                ,
                TimeSpan = CreateOTADateTimeSpanType(valuationResultRoom)
                ,
                GuestCounts = GetOTAGuestCounts(roomDetails)
                ,
                BasicPropertyInfo = CreateOTABasicPropertyInfo(establishmentDetails, facilities)
                ,
                RoomRates = new RoomStayTypeRoomRates
                {
                    RoomRate = new[] { CreateOTARoomStayTypeRoomRatesRoomRate(valuationResultRoom) }
                }
                ,
                CancelPenalties = CreateOTACancelPenalty(valuationResultRoom)
            };
        }

        private DateTimeSpanType CreateOTADateTimeSpanType(AccommodationValuationResultRoom valuationResultRoom)
        {
            return new DateTimeSpanType()
            {
                Start = valuationResultRoom.ProviderResult.CheckInDate.ToString("yyyy-MM-dd")
                ,
                End = valuationResultRoom.ProviderResult.CheckOutDate.ToString("yyyy-MM-dd")
            };
        }

        private GuestCountType GetOTAGuestCounts(AccommodationAvailabilityResultRoomIdWrapper roomDetails)
        {
            return new GuestCountType()
            {
                GuestCount = roomDetails.GuestAges.GroupBy(i => i).Select(i => CreateOTAGuestCount(i.Key, i.Count())).ToArray()
            };
        }

        private GuestCountTypeGuestCount CreateOTAGuestCount(int age, int count)
        {
            return new GuestCountTypeGuestCount()
            {
                AgeQualifyingCode = (age > configurationManager.AccommodationMaxChildAge ? "10" : "8")
                ,
                Age = age.ToString()
                ,
                Count = count.ToString()
            };
        }

        private RoomStayTypeBasicPropertyInfo CreateOTABasicPropertyInfo(EstablishmentDetails establishmentDetails, Dictionary<Guid, Facility> facilities)
        {
            return new RoomStayTypeBasicPropertyInfo()
            {
                AreaID = establishmentDetails.DestinationId.ToString()
                ,
                HotelCode = establishmentDetails.EstablishmentId.ToString()
                ,
                HotelName = establishmentDetails.EstablishmentName
                ,
                HotelCityCode = establishmentDetails.CountrySeoFriendlyName
                ,
                HotelAmenity = establishmentDetails.FacilityIds.Select(i => CreateOTABasicPropertyInfoTypeHotelAmenity(facilities[i])).ToArray()
            };
        }

        private BasicPropertyInfoTypeHotelAmenity CreateOTABasicPropertyInfoTypeHotelAmenity(Facility facility)
        {
            return new BasicPropertyInfoTypeHotelAmenity()
            {
                Code = facility.Name
            };
        }

        private RoomStayTypeRoomRatesRoomRate CreateOTARoomStayTypeRoomRatesRoomRate(AccommodationValuationResultRoom valuationResultRoom)
        {
            var commission =
                decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.AgentCommission)?
                     .SalePrice.Amount ?? 0m,2);
            var vat =
               decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.Vat)?
                    .SalePrice.Amount ?? 0m,2);
            var basePrice =
                decimal.Round(valuationResultRoom.PriceBreakdown?.FirstOrDefault(i => i.Type == AccommodationResultPriceItemType.SellingPrice)?
                    .SalePrice.Amount - commission ?? 0m,2);
            return new RoomStayTypeRoomRatesRoomRate()
            {
                RoomID =  encryptDecryptService.Encrypt(valuationResultRoom.RoomId)
                , RoomRateDescription = new[] { CreateOTAParagraphType(valuationResultRoom.RoomDescription) }
                , RoomTypeCode = valuationResultRoom.BoardType.ToString()
                , NumberOfUnits = (valuationResultRoom.ProviderResult.NumberOfAvailableRooms != null ? valuationResultRoom.ProviderResult.NumberOfAvailableRooms.ToString() : "unknown")
                , Rates = new[] { new RateTypeRate() { Fees = new[]
                {
                    new FeeType { Code = "Base", AmountSpecified = true, Amount = basePrice, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
                    ,  new FeeType { Code = "Commission", AmountSpecified = true, Amount = commission, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
                    , new FeeType { Code = "VAT", AmountSpecified = true, Amount = vat, CurrencyCode = valuationResultRoom.Price.CurrencyCode }
                }}}
                ,
                Total = new TotalType()
                {
                    AmountAfterTaxSpecified = true
                    ,
                    AmountAfterTax = commission + basePrice //ACS-931
                    ,
                    CurrencyCode = valuationResultRoom.Price.CurrencyCode
                    ,
                    TPA_Extensions = CreateTPAExtensions(valuationResultRoom)
                }
            };
        }

        private TPA_ExtensionsType CreateTPAExtensions(AccommodationValuationResultRoom valuationResultRoom)
        {
            return new TPA_ExtensionsType()
            {
                Any = new[] { new OTAHotelValRSRoomRatesRoomRateTPA_Extension()
                {
                    IsBidingRate = (valuationResultRoom.ProviderResult.IsBindingRate ? "Y" : "N")
                    , IsRefundableRate = (valuationResultRoom.ProviderResult.IsNonRefundable ? "N" : "Y")
                    , IsDirectPayment = (valuationResultRoom.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect ? "Y" : "N")
                    , IsOpaqueRate = (valuationResultRoom.ProviderResult.IsOpaqueRate ? "Y" : "N")
                    , IsSpecialRate = (valuationResultRoom.ProviderResult.IsSpecialRate ? "Y" : "N")
                }}
            };
        }

        private CancelPenaltiesType CreateOTACancelPenalty(AccommodationValuationResultRoom valuationResultRoom)
        {
            return new CancelPenaltiesType()
            {
                CancelPenalty = new[] { new CancelPenaltyType()
                {
                    PenaltyDescription = valuationResultRoom.CancellationPolicies.Select(i => CreateOTAParagraphType(i)).ToArray()
                }}
            };
        }

        private ParagraphType CreateOTAParagraphType(string text)
        {
            return new ParagraphType()
            {
                ItemsElementName = new[] { ItemsChoiceType.Text }
                ,
                Items = new[] { new FormattedTextTextType { Value = text } }
            };
        }
    }
}
