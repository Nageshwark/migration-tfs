﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts.Enums;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlValuationLogger : IB2BXmlValuationLogger
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BXmlValuationLogRepository repository;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2BXmlValuationLogger(ILogger logger, IAccommodationConfigurationManager configurationManager, IB2BXmlValuationLogRepository repository, IPerformanceLoggerService<PerformanceLog> performanceLoggerService, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService)
        {
            this.logger = logger;
            this.repository = repository;
            this.performanceLoggerService = performanceLoggerService;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(Guid valuationId, DateTime valuationStartDate, string request, Status processStatus, AccommodationValuationRequest coreValuationRequest
            , AccommodationValuationResponse coreResponse, TimeSpan timeTaken, Exception exception)
        {
            if (coreValuationRequest != null && coreResponse != null)
            {
                CreatePerformanceLogs(coreValuationRequest, coreResponse, processStatus, timeTaken);
            }
            if (!this.configurationManager.AccommodationB2BXmlValuationLoggerIsEnabled) return;
            try
            {
                B2BXmlValuationLog[] valuationLogs;
                if (coreResponse == null)
                {
                    valuationLogs = new[] { new B2BXmlValuationLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        , ValuationId = valuationId
                        , AvailabilityId = Guid.Empty
                        , ValuationDate = valuationStartDate
                        , Status = processStatus
                        , TimeTaken = (int)timeTaken.TotalMilliseconds
                        , Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        , ProviderRoomNumbers = Enumerable.Range(1, coreValuationRequest?.SelectedRoomIds?.Length ?? 0).ToString(',')
                    }};
                }
                else
                {
                    valuationLogs = coreResponse.ProcessResponseDetails.Select(i => new B2BXmlValuationLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        , ValuationId = valuationId
                        , AvailabilityId = Guid.Empty
                        , ValuationDate = valuationStartDate
                        , Status = processStatus
                        , TimeTaken = (int)timeTaken.TotalMilliseconds
                        , Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        , ProviderId = i.Provider.Id
                        , ProviderRoomNumbers = i.ValuationRequestRooms.Select(j => j.RoomNumber).ToString(',')
                        , RecoveredSelectedRooms = i.ValuationRequestRooms.Select(j => (j.IsSelectedRoomRecovered ? 'Y' : 'N')).ToString(',')
                        , ProviderRequests = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Request)
                        , ProviderResponses = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Response)
                        , ProviderStatus = (i.Exception != null ? Status.Failed : Status.Successful)
                        , ProviderTimeTaken = (i.TimeTaken != null ? (int?)i.TimeTaken.Value.TotalMilliseconds : null)
                        , ResultsCount = i.ResultsCount
                        , ProviderException = (i.Exception != null ? i.Exception.GetDetailedMessageWithInnerExceptions() : null)
                    }).ToArray();
                }
                await this.repository.SaveRangeAsync(valuationLogs);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to log accommodation availability with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
            }
        }

        private void CreatePerformanceLogs(AccommodationValuationRequest valuationRequest, AccommodationValuationResponse coreResponse,
                                          Status requestStatus, TimeSpan timeTaken)
        {
            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();

            var resultCount = coreResponse?.Result?.Rooms?.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Valuation",
                RequestIdentifier = valuationRequest.AvailabilityId.ToString(),
                DestinationIdentifier = "",
                Channel = valuationRequest.ChannelInfo.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            var providerPerfLogs = coreResponse?.ProcessResponseDetails.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.IsSuccess = d.Exception == null;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }

        private string CreateProviderRequests(AccommodationProviderRequest[] providerRequests, ProviderRequestType type)
        {
            if (providerRequests == null) return null;
            StringBuilder builder = new StringBuilder();
            int nullCount = 0;
            foreach (var request in providerRequests.Where(i => i.Type == type))
            {
                if (request.Key != "default" && providerRequests.Length > 1)
                {
                    builder.Append(request.Key);
                    builder.Append(": ");
                }
                if (request.Object == null)
                {
                    builder.Append("<<NULL>>");
                    nullCount++;
                }
                else if (request.Object is string)
                {
                    builder.Append(request.Object);
                }
                else if (request.Object is WebScrapeResponse)
                {
                    builder.Append(((WebScrapeResponse)request.Object).Value);
                }
                else
                {
                    try
                    {
                        builder.Append(request.Object.XmlSerialize(request.Object.GetType()));
                    }
                    catch (Exception ex)
                    {
                        builder.Append("Unable to serialize: " + ex.GetDetailedMessageWithInnerExceptions());
                    }
                }
                builder.Append(";\r\n");
            }
            if (nullCount == providerRequests.Length) return null;
            if (builder.Length > 0) builder.Length -= 3;
            return builder.ToString();
        }
    }
}
