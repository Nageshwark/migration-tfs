﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Services
{
    public class B2BXmlBookingRequestValidator : IB2BXmlBookingRequestValidator
    {
        public void ValidateBookingRequest(OTA_HotelResRQ otaHotelResRQ)
        {
            // TODO validate ota request expected parameters
            if (ArrayModule.IsNullOrEmpty(otaHotelResRQ.POS) || otaHotelResRQ.POS[0].RequestorID == null)
                throw new Exception("Login credentials are missing.");
            if (!(new[] {1m}).Contains(otaHotelResRQ.Version))
                throw new Exception("Request version not supported");
        }
    }
}
