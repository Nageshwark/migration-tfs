﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.B2B.MarkupServices;
using AlphaRooms.Accommodation.B2B.Services;
using AlphaRooms.Accommodation.B2C.Caching;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Interfaces.Caching;
using AlphaRooms.Accommodation.B2C.Services;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.Cache.Mongo;
using AlphaRooms.Cache.Mongo.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2B.DependencyInjection
{
    public class B2BDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            // shared services
            Bind<IB2BAgentCommissionService>().To<B2BAgentCommissionService>().InSingletonScope();
            Bind<IB2BAgentVatService>().To<B2BAgentVatService>().InSingletonScope();
            Bind<IB2BItineraryBookingFactory>().To<B2BItineraryBookingFactory>().InSingletonScope();
            Bind<IB2BItineraryBookingMapping>().To<B2BItineraryBookingMapping>().InSingletonScope();
            Bind<IB2BItineraryBookingService>().To<B2BItineraryBookingService>().InSingletonScope();
            Bind<IAvailabilityFlashSaleService>().To<B2BAvailabilityFlashSaleService>().InSingletonScope();
            Bind<IB2BFlashSaleMappingFactory>().To<B2BFlashSaleMappingFactory>().InSingletonScope();
			Bind<IB2CRequestStatusService>().To<B2CRequestStatusService>().InSingletonScope();

			// caching
			var configurationManager = Kernel.Get<IAccommodationConfigurationManager>();
			Bind<IMongoDbCollection<AccommodationB2CRequestStatus>>().ToConstant(new MongoDbCollection<AccommodationB2CRequestStatus>(
			   new MongoDbCollectionSettings(configurationManager.AccommodationB2CRequestStatusDatabase
				   , configurationManager.AccommodationB2CRequestStatusCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
			Bind<ICacheQueryExpireAsync<AccommodationB2CRequestStatus>>().To<MongoDbQuery<AccommodationB2CRequestStatus>>().InSingletonScope();
			Bind<ICacheWriterExpireAsync<AccommodationB2CRequestStatus>>().To<MongoDbWriter<AccommodationB2CRequestStatus>>().InSingletonScope();
			Bind<IB2CRequestStatusCaching>().To<B2CRequestStatusCaching>().InSingletonScope();

			//guidGeneration
			Bind<IB2CGuidService>().To<B2CGuidService>().InSingletonScope();

			// markup service
			if (Kernel.Get<IAccommodationConfigurationManager>().AccommodationB2BWebUseHmdForMarkup)
            {
                Bind<IAvailabilityResultMarkupService>().To<HmdService>().InSingletonScope();
                Bind<IValuationResultMarkupService>().To<HmdService>().InSingletonScope();
            }
            else
            {
                Bind<IAvailabilityResultMarkupService>().To<B2BMarkupService>().InSingletonScope();
                Bind<IValuationResultMarkupService>().To<B2BMarkupService>().InSingletonScope();
            }
        }
    }
}
