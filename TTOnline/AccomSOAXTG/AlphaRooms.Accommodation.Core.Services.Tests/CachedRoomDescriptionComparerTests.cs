﻿namespace AlphaRooms.Accommodation.Core.Services.Tests
{
    using Contracts;
    using Interfaces;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;

    [TestClass]
    public class CachedRoomDescriptionComparerTests
    {
        [TestMethod]
        public void GeneratesSameKeysAsNonCached()
        {
            var searchResults1 = RoomDescriptionComparerSpeedTests.LoadRecordedSearch();
            var searchResults2 = RoomDescriptionComparerSpeedTests.LoadRecordedSearch();

            for (int i = 0; i < searchResults1.Length; i++)
            {
                // null the key to make sure it is set
                searchResults1[i].RoomDescriptionKey = null;
            }

            CachedRoomDescriptionComparerTests.GenerationKeysNonCached(searchResults1);
            CachedRoomDescriptionComparerTests.GenerationKeysCached(searchResults2);

            for (int i = 0; i < searchResults1.Length; i++)
            {
                // has the cached version and non cached version proceduced the same results
                Assert.AreEqual(searchResults1[i].RoomDescriptionKey, searchResults2[i].RoomDescriptionKey, true);
            }
        }

        [TestMethod]
        public void CachedGenerationIsQuicker()
        {
            var searchResults = RoomDescriptionComparerSpeedTests.LoadRecordedSearch();

            // warm up - for jit and things
            CachedRoomDescriptionComparerTests.GenerationKeysNonCached(searchResults, 2);
            CachedRoomDescriptionComparerTests.GenerationKeysCached(searchResults, 2);

            Stopwatch timer = Stopwatch.StartNew();
            CachedRoomDescriptionComparerTests.GenerationKeysNonCached(searchResults, 100);
            timer.Stop();
            var nonCachedTime = timer.Elapsed;

            timer.Restart();
            CachedRoomDescriptionComparerTests.GenerationKeysCached(searchResults, 100);
            timer.Stop();
            var cachedTime = timer.Elapsed;

            // cahced time is about 50% faster & alot less in CPU
            Assert.IsTrue(cachedTime < nonCachedTime, "Cached time was slower than non cached time");
        }

        private static void GenerationKeysNonCached(AccommodationAvailabilityResult[] searchResults, int iterations = 1)
        {
            RoomDescriptionKeyGeneration roomDescriptionKeyGeneration = new RoomDescriptionKeyGeneration(
                RoomDescriptionComparerTests.GetRemoveWords(),
                RoomDescriptionComparerTests.GetReplacements());

            // is thread safe
            RoomDescriptionComparer roomDescriptionComparer = new RoomDescriptionComparer(roomDescriptionKeyGeneration);

            roomDescriptionComparer.InitializeAsync().Wait();

            for (int i = 0; i < iterations; i++)
            {
                Parallel.For(0, searchResults.Length, j =>
                {
                    roomDescriptionComparer.ApplyAvailabilityRoomDescriptionKey(searchResults[j]);
                });
            }
        }

        private static void GenerationKeysCached(AccommodationAvailabilityResult[] searchResults, int iterations = 1)
        {
            RoomDescriptionKeyGeneration roomDescriptionKeyGeneration = new RoomDescriptionKeyGeneration(
                RoomDescriptionComparerTests.GetRemoveWords(),
                RoomDescriptionComparerTests.GetReplacements());

            // this creates CachedRoomDescriptionComparer as it is not thread safe
            RoomDescriptionComparerFactory factory = new RoomDescriptionComparerFactory(roomDescriptionKeyGeneration);

            for (int i = 0; i < iterations; i++)
            {
                // this is a CachedRoomDescriptionComparer
                IRoomDescriptionComparer roomDescriptionComparer = factory.Create();

                roomDescriptionComparer.InitializeAsync().Wait();

                // is not thread safe
                for (int j = 0; j < searchResults.Length; j++)
                {
                    roomDescriptionComparer.ApplyAvailabilityRoomDescriptionKey(searchResults[j]);
                }
            }
        }
    }
}
