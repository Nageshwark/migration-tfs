﻿namespace AlphaRooms.Accommodation.Core.Services.Tests
{
    using Contracts;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Provider.DomainModels;
    using Provider.Interfaces.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [TestClass]
    public class RoomDescriptionComparerTests
    {
        [TestMethod]
        public void ExtraRateIsRemoved()
        {
            AreEqual("Double / Twin Room", "Double Extra Rate");
        }

        [TestMethod]
        public void StandardDoubleTwinAreEqual()
        {
            AreEqual("Standard Room Pool View", "Double / Twin Room - Pool View");
        }

        [TestMethod]
        public void NoiseWordsRemoved()
        {
            AreEqual("Superior Double / Twin Room", "Superior Room");
        }

        [TestMethod]
        public void SuperiorIsNotStandard()
        {
            AreNotEqual("Standard Room", "Superior Room");
        }

        [TestMethod]
        public void PoolIsNotRemoved()
        {
            AreNotEqual("Double / Twin Room", "Double / Twin Room - Pool View");
        }

        [TestMethod]
        public void PromoIsRemoved()
        {
            AreEqual("Habitación Promo Clásica Alojamiento Y Desayuno ( No Reembolsable )", "Habitación Clásica Alojamiento Y Desayuno ( No Reembolsable )");
        }

        [TestMethod]
        public void NonChangeableIsRemoved()
        {
            AreEqual("Double / Twin Room", "Twin Room Non Changeable (Hb)");
        }

        [TestMethod]
        public void ClassicEqualsNormal()
        {
            AreEqual("Twin Room Non-Refundable (Bb)", "Classic Room - Non Refundable");
        }

        [TestMethod]
        public void SingalCharsAreRemoved()
        {
            AreEqual("Twin Room 2", "Twin Room 3");
        }

        [TestMethod]
        public void BalconyTerraceIsRemoved()
        {
            AreEqual("Double / Twin Room - Balcony / Terrace", "Twin Room 2");
        }

        [TestMethod]
        public void PremiumEqualsSuperior()
        {
            AreEqual("Double / Twin Premium", "Double Superior");
        }

        [TestMethod]
        public void NonRefundableIsRemoved()
        {
            AreEqual("Melia Room", "Melia Room Non Refundable");
        }

        [TestMethod]
        public void PunctuationIsRemoved()
        {
            AreEqual("Room()-..,", "Room");
        }

        [TestMethod]
        public void SuiteDoesNotEqualRoom()
        {
            AreNotEqual("Suite", "Room");
        }

        [TestMethod]
        public void OccupationIsRemoved()
        {
            AreEqual("2 people Room", "Room");
        }

        [TestMethod]
        public void SpainishToEnglish()
        {
            AreEqual("Family Room", "Cama Doble Familial");
        }

        [TestMethod]
        public void RepeatAliasesReplaced()
        {
            // there was an issue in dev when one the first occurrence was replaced not all occurrences
            AreEqual("one bedroom one bedroom", "one bedroom");
        }

        [TestMethod]
        public void EmptyDescription()
        {
            AreEqual(string.Empty, string.Empty);
        }

        [TestMethod]
        public void OnlyNoiseWords()
        {
            AreEqual("shower", "wifi");
        }

        [TestMethod]
        public void ReplacementOverlap()
        {
            /*
             * the aliases replacment will match apart first and also apartment
             * because apart is first it will chose apart to replace as apt
             */
            AreEqual("apartment", "aptment");
        }

        internal static IRoomDescriptionRemoveWordRepository GetRemoveWords()
        {
            List<string> noiseWords = new List<string> { "cama", "wifi", "tv", "todo", "shower", "satellite", "pers", "open", "media", "massage", "incluido", "includ", "hydro", "front", "estancia", "corta", "completa", "bar", "bedded", "breakf", "pensión", "pax", "adults", "adult,", "people", "children", "child", "ad", "ch", "ai", "air", "all", "and", "ap1", "balcony", "bb", "b&b", "big", "board", "best", "breakfast", "brunch", "cancellation", "catering", "classic", "condition", "conditioning", "deal", "direct", "drinks", "economy", "estandar", "ex", "extra", "facilities", "fb", "free", "full", "fullboard", "half", "halfboard", "hb", "included", "inclusive", "kitchenette", "land", "long", "max", "med", "mp", "of", "offer", "only", "or", "parks", "pay", "pc", "plus", "private", "promo", "promotion", "promotional", "rate", "ren", "resort", "ro", "room", "room_standard", "self", "short", "sleeps", "small", "smaller", "special", "standard", "stay", "terrace", "ti", "to", "uai", "ultra", "up", "view", "views", "vista", "w", "with", "without", "non", "refundable", "changeable" };

            Mock<IRoomDescriptionRemoveWordRepository> mock = new Mock<IRoomDescriptionRemoveWordRepository>();

            mock.Setup(x => x.GetAllAsync()).ReturnsAsync(noiseWords.ToDictionary(x => x, x => 0, StringComparer.OrdinalIgnoreCase));
            return mock.Object;
        }

        internal static IRoomDescriptionReplacementRepository GetReplacements()
        {
            string[] toRplace = { "1 dormitorio", "1 habitación", "one bed", "1 bed", "one bedroom", "1 bedroom", "appt", "apart", "apto", "aptos", "appartment", "apartamento", "apartamentos", "apartmet", "apartment", "apartments", "two bed", "2 bed", "2 bedrm", "two bedroom", "2 bedroom", "three bed", "3 bed", "3 bedrm", "three bedroom", "3 bedroom", "premium", "habitación", "junior", "twn", "dbl", "rooms", "doble", "twin", "double", "poolside", "swimming", "piscina", "views", "vista", "partial", "lateral", "internal", "indoor", "inside", "inner", "outer", "outside", "external", "seaview", "familial" };
            string[] replaceWith = { "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "apt", "2-bedroom", "2-bedroom", "2-bedroom", "2-bedroom", "2-bedroom", "3-bedroom", "3-bedroom", "3-bedroom", "3-bedroom", "3-bedroom", "superior", "room", "room", "room", "room", "room", "room", "room", "room", "pool", "pool", "pool", "view", "view", "side", "side", "interior", "interior", "interior", "interior", "exterior", "exterior", "exterior", "sea", "family" };

            Alias[] replacements = new Alias[toRplace.Length];
            for (int i = 0; i < toRplace.Length; i++)
            {
                replacements[i] = new Alias(toRplace[i], replaceWith[i]);
            }

            Mock<IRoomDescriptionReplacementRepository> mock = new Mock<IRoomDescriptionReplacementRepository>();

            mock.Setup(x => x.GetAllAliasAsync()).ReturnsAsync(replacements);
            return mock.Object;
        }

        private static void AreEqual(string value1, string value2)
        {
            Compare(value1, value2, Assert.IsTrue, "Should be equal value1: \"{0}\" value2: \"{0}\" expected: \"{2}\" actual: \"{3}\"");
        }

        private static void AreNotEqual(string value1, string value2)
        {
            Compare(value1, value2, Assert.IsFalse, "Should be not be equal value1: \"{0}\" value2: \"{0}\" expected: \"{2}\" actual: \"{3}\"");
        }

        private static void Compare(string value1, string value2, Action<bool, string> assert, string message)
        {
            RoomDescriptionKeyGeneration roomDescriptionKeyGeneration = new RoomDescriptionKeyGeneration(
                GetRemoveWords(),
                GetReplacements());

            RoomDescriptionComparer roomDescriptionComparer = new RoomDescriptionComparer(roomDescriptionKeyGeneration);

            Task init = roomDescriptionComparer.InitializeAsync();
            Task.WaitAll(init);

            AccommodationAvailabilityResult expected = new AccommodationAvailabilityResult
            {
                RoomDescription = value1,
            };

            AccommodationAvailabilityResult actual = new AccommodationAvailabilityResult
            {
                RoomDescription = value2,
            };

            roomDescriptionComparer.ApplyAvailabilityRoomDescriptionKey(expected);
            roomDescriptionComparer.ApplyAvailabilityRoomDescriptionKey(actual);

            assert(
                roomDescriptionComparer.EqualsRoomDescription(expected, actual),
                string.Format(message, value1, value2, actual.RoomDescriptionKey, expected.RoomDescriptionKey));
        }
    }
}
