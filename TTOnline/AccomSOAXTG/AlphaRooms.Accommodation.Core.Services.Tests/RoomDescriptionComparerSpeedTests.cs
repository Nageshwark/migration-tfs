﻿namespace AlphaRooms.Accommodation.Core.Services.Tests
{
    using Contracts;
    using CsvHelper;
    using CsvHelper.Configuration;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    [TestClass]
    public class RoomDescriptionComparerSpeedTests
    {
        [Ignore]
        [TestMethod]
        public void PerformanceTest()
        {
            const int Iterations = 300;

            AccommodationAvailabilityResult[] rooms = LoadRecordedSearch();

            // warm up 
            for (int i = 0; i < Iterations / 10; i++)
            {
                RoomDescriptionComparerSpeedTests.Deduplicate(rooms);
            }

            // test
            Stopwatch timer = Stopwatch.StartNew();
            for (int i = 0; i < Iterations; i++)
            {
                RoomDescriptionComparerSpeedTests.Deduplicate(rooms);
            }
            timer.Stop();

            TimeSpan expected = TimeSpan.FromSeconds(60d);
            TimeSpan actual = timer.Elapsed;

            Assert.IsTrue(actual < expected, string.Format("Deduplication took to long to complete. Target: {0} Actual: {1}", expected, actual));
        }

        internal static AccommodationAvailabilityResult[] LoadRecordedSearch()
        {
            AccommodationAvailabilityResult[] rooms;

            // read in data from CSV
            var assembly = Assembly.GetExecutingAssembly();
            using (TextReader tr = new StreamReader(assembly.GetManifestResourceStream("AlphaRooms.Accommodation.Core.Services.Tests.MallorcaDeduplication.csv")))
            {
                var csv = new CsvReader(tr);
                csv.Configuration.RegisterClassMap<RoomAvailabilityMap>();
                rooms = csv.GetRecords<AccommodationAvailabilityResult>().ToArray();
            }

            return rooms;
        }

        private static void Deduplicate(AccommodationAvailabilityResult[] rooms)
        {
            RoomDescriptionKeyGeneration roomDescriptionKeyGeneration = new RoomDescriptionKeyGeneration(
                RoomDescriptionComparerTests.GetRemoveWords(),
                RoomDescriptionComparerTests.GetReplacements());

            RoomDescriptionComparer roomDeduplication = new RoomDescriptionComparer(roomDescriptionKeyGeneration);

            Task init = roomDeduplication.InitializeAsync();
            Task.WaitAll(init);

            for (int j = 0; j < rooms.Length; j++)
            {
                roomDeduplication.ApplyAvailabilityRoomDescriptionKey(rooms[j]);
            }
        }

        private sealed class RoomAvailabilityMap : CsvClassMap<AccommodationAvailabilityResult>
        {
            public RoomAvailabilityMap()
            {
                Map(m => m.EstablishmentId).Index(0);
                Map(m => m.BoardType).Index(1);
                Map(m => m.RoomDescription).Index(2);
                //Map(m => m.RoomDescription).Index(3);
                //Map(m => m.SalePrice).Index(4);
                //Map(m => m.TotalDiscountAmount).Index(5);
            }
        }
    }
}