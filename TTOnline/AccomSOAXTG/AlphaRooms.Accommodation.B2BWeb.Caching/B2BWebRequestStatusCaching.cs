﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Caching
{
    public class B2BWebRequestStatusCaching : IB2BWebRequestStatusCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationB2BWebRequestStatus> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationB2BWebRequestStatus> cacheWriter;

        public B2BWebRequestStatusCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationB2BWebRequestStatus> cacheQuery
            , ICacheWriterExpireAsync<AccommodationB2BWebRequestStatus> cacheWriter)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
        }

        public async Task<AccommodationB2BWebRequestStatus> GetByIdOrNullAsync(Guid requestId)
        {
            return await this.cacheQuery.GetItemOrNullAsync(i => i.Id == requestId);
        }

        public async Task<AccommodationB2BWebRequestStatus> GetSuccessfulByAvailabilityKeyOrNullAsync(string availabilityKey)
        {
            return await this.cacheQuery.GetFirstItemOrNullAsync(i => i.AvailabilityKey == availabilityKey && i.Status == Status.Successful
                && i.ResultsExpireDate > this.cacheQuery.GetCacheExpireDateThreshold() + configurationManager.AccommodationB2BWebRequestStatusExpireThreshold
                , new CacheSort<AccommodationB2BWebRequestStatus>(i => i.ResultsExpireDate, CacheSortOrder.Desc));
        }

        public DateTime CreateResultsCacheExpireDate()
        {
            return this.cacheQuery.GetCacheExpireDateThreshold();
        }

        public bool IsResultsExpireDateExpired(AccommodationB2BWebRequestStatus cachedRequestStatus)
        {
            return this.cacheQuery.IsCacheExpireDateExpired(cachedRequestStatus.ResultsExpireDate.Value);
        }

        public async Task SaveAsync(AccommodationB2BWebRequestStatus requestStatus)
        {
            await this.cacheWriter.AddOrUpdateAsync(i => i.Id == requestStatus.Id, requestStatus, this.configurationManager.AccommodationB2BWebRequestStatusExpireTimeout);
        }
    }
}
