﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Contracts.Enums;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Caching
{
    public class B2BWebAvailabilityResultCaching : IB2BWebAvailabilityResultCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheMultiDbQueryExpireAsync<AccommodationB2BWebAvailabilityResult, Guid> cacheQuery;
        private readonly ICacheMultiDbWriterExpireAsync<AccommodationB2BWebAvailabilityResult, Guid> cacheWriter;
        private readonly ILogger logger;
        private readonly IB2BWebPaymentModelConverter paymentModelConverter;

        public B2BWebAvailabilityResultCaching(IAccommodationConfigurationManager configurationManager, ICacheMultiDbQueryExpireAsync<AccommodationB2BWebAvailabilityResult, Guid> cacheQuery
            , ICacheMultiDbWriterExpireAsync<AccommodationB2BWebAvailabilityResult, Guid> cacheWriter, ILogger logger, IB2BWebPaymentModelConverter paymentModelConverter)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.logger = logger;
            this.paymentModelConverter = paymentModelConverter;
        }

        public async Task<AccommodationB2BWebAvailabilityResults> GetFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, int roomCount
            , AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria, bool debugging)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.SupplierCacheRetrieval, availabilityId.ToString(), "Internal filtered availability", debugging, logger))
            {
                var query = LinqExpression.Create<AccommodationB2BWebAvailabilityResult>(i => i.AvailabilityId == availabilityId);
                query = ApplyQueryFilter(query, roomCount, filterCriteria);
                var sort = CreateQuerySort(sortCriteria);
                return await GetLimitedResultsAsync(baseDestinationId, query, roomCount, true, filterCriteria, sortCriteria, sort);
            }
        }

        public async Task<AccommodationB2BWebAvailabilityResult> GetByEstablishmentIdAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId)
        {
            return await cacheQuery.GetItemAsync(baseDestinationId, i => i.AvailabilityId == availabilityId && i.EstablishmentId == establishmentId);
        }

        public async Task<AccommodationB2BWebAvailabilityResult> GetByEstablishmentIdFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId
            , AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria, bool debugging)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.SupplierCacheRetrieval, availabilityId.ToString(), "Internal establishment filtered availability", debugging, logger))
            {
                var query = LinqExpression.Create<AccommodationB2BWebAvailabilityResult>(i => i.AvailabilityId == availabilityId && i.EstablishmentId == establishmentId);
                query = ApplyQueryFilter(query, -1, filterCriteria);
                var sort = CreateQuerySort(sortCriteria);
                var results = await GetLimitedResultsAsync(baseDestinationId, query, -1, false, filterCriteria, sortCriteria, sort);
                return (results.ResultsTotalCount == 1 ? results.Results[0] : null);
            }
        }

        public async Task<AccommodationB2BWebAvailabilityResult[]> GetByRoomIdsAsync(Guid availabilityId, Guid baseDestinationId, string[] roomIds)
        {
            var results = (await this.cacheQuery.GetItemsAsync(baseDestinationId, i => i.AvailabilityId == availabilityId && i.Rooms.Any(j => roomIds.Contains(j.RoomId)))).ToArray();
            results.ParallelForEach(i => i.Rooms = i.Rooms.Where(j => roomIds.Contains(j.RoomId)).ToArray());
            return results;
        }

        private Expression<Func<AccommodationB2BWebAvailabilityResult, bool>> ApplyQueryFilter(Expression<Func<AccommodationB2BWebAvailabilityResult, bool>> query
            , int roomCount, AccommodationB2BWebAvailabilityFilter filterCriteria)
        {
            if (!string.IsNullOrEmpty(filterCriteria.EstablishmentName)) query = query.And(i => i.EstablishmentName.Contains(filterCriteria.EstablishmentName));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.StarRatings)) query = query.And(i => filterCriteria.StarRatings.Contains(i.EstablishmentStarRating));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.Districts)) query = query.And(i => filterCriteria.Districts.Contains(i.DestinationReference));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.FacilityIds)) filterCriteria.FacilityIds.ForEach(i => query = query.And(j => j.EstablishmentFacilities.Any(k => k.Reference == i)));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.TravellerTypeIds)) query = query.And(i => i.EstablishmentTravellerTypes.Any(j => filterCriteria.TravellerTypeIds.Contains(j.Id)));
            for (int i = 1; i <= roomCount; i++) query = query.AndAny(j => j.Rooms, ApplyRoomQueryFilter(i, filterCriteria));
            return query;
        }

        private Expression<Func<AccommodationB2BWebAvailabilityResultRoom, bool>> ApplyRoomQueryFilter(int roomNumber, AccommodationB2BWebAvailabilityFilter filterCriteria)
        {
            var query = LinqExpression.Create<AccommodationB2BWebAvailabilityResultRoom>(i => i.RoomNumber == roomNumber);
            if (filterCriteria.PriceMin != null) query = query.And(i => i.Price.Amount >= filterCriteria.PriceMin.Amount);
            if (filterCriteria.PriceMax != null) query = query.And(i => i.Price.Amount <= filterCriteria.PriceMax.Amount);
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.BoardTypes)) query = query.And(i => filterCriteria.BoardTypes.Contains(i.BoardType));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.ProviderEdiCodes)) query = query.And(i => filterCriteria.ProviderEdiCodes.Contains(i.ProviderEdiCode));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.PaymentTypes))
            {
                var paymentModels = filterCriteria.PaymentTypes.SelectMany(i => paymentModelConverter.ToPaymentModels(i)).ToArray();
                query = query.And(i => paymentModels.Contains(i.PaymentModel));
            }
            if (filterCriteria.OnlyLowerPriceRoomResults) query = query.And(i => i.IsLowestPrice);
            return query;
        }

        private AccommodationB2BWebAvailabilityResult[] FilterRooms(IEnumerable<AccommodationB2BWebAvailabilityResult> results, int roomCount, bool removeEstablishmentWithoutRooms
            , AccommodationB2BWebAvailabilityFilter filterCriteria)
        {
            var query = LinqExpression.CreateTrue<AccommodationB2BWebAvailabilityResultRoom>();
            if (filterCriteria.PriceMin != null) query = query.And(i => i.Price.Amount >= filterCriteria.PriceMin.Amount);
            if (filterCriteria.PriceMax != null) query = query.And(i => i.Price.Amount <= filterCriteria.PriceMax.Amount);
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.BoardTypes)) query = query.And(i => filterCriteria.BoardTypes.Contains(i.BoardType));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.ProviderEdiCodes)) query = query.And(i => filterCriteria.ProviderEdiCodes.Contains(i.ProviderEdiCode));
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.PaymentTypes)) 
            {
                var paymentModels = filterCriteria.PaymentTypes.SelectMany(i => paymentModelConverter.ToPaymentModels(i)).ToArray();
                query = query.And(i => paymentModels.Contains(i.PaymentModel));
            }
            if (filterCriteria.OnlyLowerPriceRoomResults) query = query.And(i => i.IsLowestPrice);
            var rooms = (roomCount > 1 ? Enumerable.Range(1, roomCount).ToArray() : null);
            results.ParallelForEach(result =>
            {
                if (filterCriteria.NumberOfRoomResults != null) result.Rooms = result.Rooms.AsQueryable().Where(query).GroupBy(i => i.RoomNumber)
                    .SelectMany(i => i.OrderBy(j => j.Price).Take(filterCriteria.NumberOfRoomResults.Value)).ToArray();
                else result.Rooms = result.Rooms.AsQueryable().Where(query).OrderBy(i => i.Price).ToArray();
                if (rooms != null && !rooms.All(i => result.Rooms.Any(j => j.RoomNumber == i))) result.Rooms = new AccommodationB2BWebAvailabilityResultRoom[0];
            });
            return (removeEstablishmentWithoutRooms ? results.Where(i => i.Rooms.Any()).ToArray() : results.ToArray());
        }

        private ICacheSort<AccommodationB2BWebAvailabilityResult> CreateQuerySort(AccommodationB2BWebAvailabilitySort sortCriteria)
        {
            if (sortCriteria == null || sortCriteria.SortField == null) return null;
            var order = (sortCriteria.SortOrder == AccommodationB2BWebAvailabilitySortOrder.Ascendant ? CacheSortOrder.Asc : CacheSortOrder.Desc);
            switch (sortCriteria.SortField)
            {
                case AccommodationB2BWebAvailabilitySortField.CheapestPrice:
                    if (sortCriteria.SortOrder == AccommodationB2BWebAvailabilitySortOrder.Ascendant) return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.RoomsCheapestPrice, order);
                    else return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.RoomsHighestPrice, order);
                case AccommodationB2BWebAvailabilitySortField.EstablishmentName:
                    return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.EstablishmentName, order);
                case AccommodationB2BWebAvailabilitySortField.CustomerRating:
                    return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.EstablishmentReviewAverageScore, order);
                case AccommodationB2BWebAvailabilitySortField.StarRating:
                    return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.EstablishmentStarRating, order);
                case AccommodationB2BWebAvailabilitySortField.TripAdvisorRating:
                    return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.EstablishmentTripAdvisorAverageScore, order);
                case AccommodationB2BWebAvailabilitySortField.MostPopular:
                    return new CacheSort<AccommodationB2BWebAvailabilityResult>(i => i.EstablishmentPopularity, order);
                default:
                    return null;
            }
        }

        private async Task<AccommodationB2BWebAvailabilityResults> GetLimitedResultsAsync(Guid baseDestinationId, Expression<Func<AccommodationB2BWebAvailabilityResult, bool>> query
            , int roomCount, bool removeEstablishmentWithoutRooms, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria
            , ICacheSort<AccommodationB2BWebAvailabilityResult> sort)
        {
            int? resultsStart = null;
            int? resultsCount = null;
            int resultsTotalCount = -1;
            if (filterCriteria.NumberOfResults != null)
            {
                resultsStart = 0;
                resultsCount = filterCriteria.NumberOfResults.Value;
            }
            else if (filterCriteria.PageNumber != null && filterCriteria.PageSize != null)
            {
                resultsStart = (filterCriteria.PageNumber.Value - 1) * filterCriteria.PageSize.Value;
                resultsCount = filterCriteria.PageSize.Value;
            }
            IEnumerable<AccommodationB2BWebAvailabilityResult> results;
            if (resultsStart != null && resultsCount != null)
            {
                LimitedResults<AccommodationB2BWebAvailabilityResult> limitedResults;
                if (sort == null) limitedResults = await this.cacheQuery.GetLimitedItemsAsync(baseDestinationId, query, resultsStart.Value, resultsCount.Value);
                else limitedResults = await this.cacheQuery.GetLimitedItemsAsync(baseDestinationId, query, resultsStart.Value, resultsCount.Value, sort);
                results = limitedResults.Results;
                resultsTotalCount = limitedResults.TotalResultsCount;
            }
            else
            {
                if (sort == null) results = await this.cacheQuery.GetItemsAsync(baseDestinationId, query);
                else results = await this.cacheQuery.GetItemsAsync(baseDestinationId, query, sort);
                resultsTotalCount = results.Count();
                if (resultsStart != null) results = results.Skip(resultsStart.Value);
                if (resultsCount != null) results = results.Take(resultsCount.Value);
            }
            return new AccommodationB2BWebAvailabilityResults() { Results = FilterRooms(results, roomCount, removeEstablishmentWithoutRooms, filterCriteria), ResultsTotalCount = resultsTotalCount };
        }

        public async Task SaveRangeAsync(Guid baseDestinationId, IEnumerable<AccommodationB2BWebAvailabilityResult> results)
        {
            await this.cacheWriter.AddRangeAsync(baseDestinationId, results);
        }

        public async Task DeleteRangeAsync(Guid baseDestinationId, Guid availabilityId)
        {
            await this.cacheWriter.DeleteAsync(baseDestinationId, i => i.AvailabilityId == availabilityId);
        }
    }
}
