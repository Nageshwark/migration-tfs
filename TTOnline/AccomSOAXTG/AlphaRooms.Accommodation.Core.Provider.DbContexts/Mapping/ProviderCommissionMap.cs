﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class ProviderCommissionMap : EntityTypeConfiguration<ProviderCommission>
    {
        public ProviderCommissionMap()
        {
            this.ToTable("suppliercommission");
            this.HasKey(i => i.Id);
            this.Property(t => t.Id).HasColumnName("pkId").IsRequired();
            this.Property(t => t.ProviderEdiCode).HasColumnName("fkEdiProvider").IsRequired();
            this.Property(t => t.SupplierEdiCode).HasColumnName("Supplier").IsOptional();
            this.Property(t => t.FromArrivalDate).HasColumnName("FromArrivalDate").IsOptional();
            this.Property(t => t.ToArrivalDate).HasColumnName("ToArrivalDate").IsOptional();
            this.Property(t => t.FromBookingDate).HasColumnName("FromBookDate").IsOptional();
            this.Property(t => t.ToBookingDate).HasColumnName("ToBookDate").IsOptional();
            this.Property(t => t.Commission).HasColumnName("Commission").IsRequired();
            this.Property(t => t.IncludeInHMD).HasColumnName("blnIncludeInHMD").IsRequired();
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate").IsRequired();
        }
    }
}
