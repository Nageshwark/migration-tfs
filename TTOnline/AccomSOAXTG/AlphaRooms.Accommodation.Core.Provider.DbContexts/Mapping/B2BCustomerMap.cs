﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class B2BCustomerMap : EntityTypeConfiguration<B2BCustomer>
    {
        public B2BCustomerMap()
        {
            this.ToTable("Customers", "schmBooking");
            //primary key
            this.HasKey(t => t.Id);
            // Table & Column Mappings
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.FirstName).HasColumnName("FirstName").IsOptional();
            this.Property(t => t.Surname).HasColumnName("Surname").IsOptional();
            this.Property(t => t.Title).HasColumnName("Title").IsOptional();
            this.Property(t => t.ContactNumber).HasColumnName("ContactNumber").IsOptional();
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress").IsOptional();
            this.Property(t => t.AddressLine1).HasColumnName("AddressLine1").IsOptional();
            this.Property(t => t.AddressLine2).HasColumnName("AddressLine2").IsOptional();
            this.Property(t => t.Town).HasColumnName("Town").IsOptional();
            this.Property(t => t.Country).HasColumnName("Country").IsOptional();
            this.Property(t => t.PostCode).HasColumnName("PostCode").IsOptional();
            this.Property(t => t.County).HasColumnName("County").IsOptional();
            this.Property(t => t.UserId).HasColumnName("UserId").IsOptional();
            this.Property(t => t.Channel).HasColumnName("Channel").IsOptional();
            this.Property(t => t.LanguageId).HasColumnName("LanguageId").IsRequired();
            this.Property(t => t.B2BUserId).HasColumnName("B2BUserId").IsOptional();
            this.HasOptional(t => t.B2BUser).WithMany(i => i.Customers).HasForeignKey(x => x.B2BUserId);
        }
    }
}