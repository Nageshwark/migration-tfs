﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class VirtualCreditCardRuleMap : EntityTypeConfiguration<VirtualCreditCardRule>
    {
        public VirtualCreditCardRuleMap()
        {
            this.ToTable("AccommodationSupplierVirtualCreditCardRules");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.SupplierId).HasColumnName("SupplierId").IsOptional();
            this.HasOptional(t => t.Supplier).WithMany().HasForeignKey(i => i.SupplierId);
            this.Property(t => t.RoomCode).HasColumnName("RoomCode").IsOptional().IsUnicode(false);
            this.Property(t => t.AmountOnBooking).HasColumnName("AmountOnBooking").IsOptional();
            this.Property(t => t.DaysBeforeCheckIn).HasColumnName("DaysBeforeCheckIn").IsOptional();
            this.Property(t => t.AmountOnDaysBeforeCheckIn).HasColumnName("AmountOnDaysBeforeCheckIn").IsOptional();
            this.Property(t => t.DaysAfterCheckOut).HasColumnName("DaysAfterCheckOut").IsOptional();
            this.Property(t => t.AmountOnDaysAfterCheckOut).HasColumnName("AmountOnDaysAfterCheckOut").IsOptional();
        }
    }
}
