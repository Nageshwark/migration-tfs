﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class BoardDescriptionMappingMap : EntityTypeConfiguration<BoardDescriptionMapping>
    {
        public BoardDescriptionMappingMap()
        {
            this.HasKey(t => t.BoardDescription);
            this.Property(t => t.BoardDescription).HasColumnName("BoardDescription").IsRequired();
            this.Property(t => t.BoardType).HasColumnName("BoardTypeId").IsOptional();
            this.Property(t => t.ProviderBoardDescription).HasColumnName("ProviderBoardDescription").IsRequired();
        }
    }
}
