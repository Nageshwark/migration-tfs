﻿using System.Data.Entity.ModelConfiguration;
using System.Threading;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationB2BFlashSaleEstablishmentMap : EntityTypeConfiguration<AccommodationB2BFlashSaleEstablishment>
    {
        public AccommodationB2BFlashSaleEstablishmentMap()
        {
            this.ToTable("AccommodationB2BFlashSaleEstablishments");
            this.HasKey(t => new { t.B2BAgentId, t.EstablishmentId });
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsRequired();
            this.Property(t => t.B2BAgentId).HasColumnName("B2BAgentId").IsRequired();
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}