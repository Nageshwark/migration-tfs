﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationProviderParameterMap : EntityTypeConfiguration<AccommodationProviderParameter>
    {
        public AccommodationProviderParameterMap()
        {
            this.ToTable("AccommodationProviderParameters");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsRequired();
            this.Property(t => t.ParameterName).HasColumnName("ParameterName").IsRequired().IsUnicode(false);
            this.Property(t => t.ParameterValue).HasColumnName("ParameterValue").IsRequired().IsUnicode(false);
        }
    }
}
