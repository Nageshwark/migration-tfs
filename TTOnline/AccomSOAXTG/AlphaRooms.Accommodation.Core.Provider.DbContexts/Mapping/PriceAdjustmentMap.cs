﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class PriceAdjustmentMap : EntityTypeConfiguration<PriceAdjustment>
    {
        public PriceAdjustmentMap()
        {
            this.ToTable("CP_Price_Adjustments");
            this.HasKey(i => i.EstablishmentId);
            this.Property(t => t.EstablishmentId).HasColumnName("fkEstablishmentID").IsRequired();
            this.Property(t => t.CheapestSupplierCode).HasColumnName("Cheapest_Supplier").IsOptional();
            this.Property(t => t.Period).HasColumnName("Period").IsRequired();
            this.Property(t => t.SearchDayType).HasColumnName("Search_Day_Type").IsOptional();
            this.Property(t => t.MinMarkup).HasColumnName("Min_Markup").IsOptional();
            this.Property(t => t.StartDate).HasColumnName("DtmStart").IsOptional();
            this.Property(t => t.EndDate).HasColumnName("DtmEnd").IsOptional();
            this.Property(t => t.CurrentMarkup).HasColumnName("Current_Markup").IsOptional();
            this.Property(t => t.ChangeToApply).HasColumnName("Change_To_Apply").IsOptional();
            this.Property(t => t.HasTR).HasColumnName("HasTR").IsOptional();
        }
    }
}
