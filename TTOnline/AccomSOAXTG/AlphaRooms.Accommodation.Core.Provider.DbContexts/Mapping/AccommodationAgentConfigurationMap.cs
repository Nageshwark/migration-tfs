﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationAgentConfigurationMap : EntityTypeConfiguration<AccommodationAgentConfiguration>
    {
        public AccommodationAgentConfigurationMap()
        {
            this.ToTable("AccommodationAgentConfiguration");
            this.HasKey(t => t.Id);
            this.Property(t => t.AgentId).IsRequired();
            this.Property(t => t.GrossRatesEnabled).IsRequired();
            this.Property(t => t.ProviderId).IsRequired();
            this.Property(t => t.SupplierId).IsOptional();
          //  this.HasOptional(t => t.Supplier).WithMany().HasForeignKey(t => t.SupplierId);
           // this.HasRequired(t => t.Provider).WithMany().HasForeignKey(t => t.ProviderId);
        }
    }
}
