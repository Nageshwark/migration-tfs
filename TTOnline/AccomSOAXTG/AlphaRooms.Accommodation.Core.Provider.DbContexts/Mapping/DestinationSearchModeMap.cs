﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class DestinationSearchModeMap : EntityTypeConfiguration<DestinationSearchMode>
    {
        public DestinationSearchModeMap()
        {
            this.ToTable("AccommodationDestinationSearchModes");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsOptional();
            this.Property(t => t.PreferredSearchMode).HasColumnName("PreferredSearchModeId").IsRequired();
            this.Property(t => t.IsEnforcedSearchMode).HasColumnName("IsEnforcedSearchMode").IsRequired();
        }
    }
}
