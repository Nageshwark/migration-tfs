﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class EstablishmentFacilityMap : EntityTypeConfiguration<EstablishmentFacility>
    {
        public EstablishmentFacilityMap()
        {
            this.HasEntitySetName("EstablishmentFacility");
            this.HasKey(t => t.EstablishmentId);
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsRequired();
            this.Property(t => t.FacilityId).HasColumnName("FacilityId").IsRequired();
        }
    }
}
