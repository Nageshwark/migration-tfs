﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Data.Entity.ModelConfiguration;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class SuppliersExcludedOfDepositMap : EntityTypeConfiguration<SuppliersExcludedOfDeposit>
    {
        public SuppliersExcludedOfDepositMap()
        {
            this.HasKey(t => new { t.AccommodationProviderID, t.AccommodationSupplierID, t.PaymentModelID });
            this.ToTable("AccommodationSuppliersDepositConfiguration");

            this.Property(t => t.AccommodationProviderID).HasColumnName("AccommodationProviderID");
            this.Property(t => t.AccommodationSupplierID).HasColumnName("AccommodationSupplierID");
            this.Property(t => t.PaymentModelID).HasColumnName("PaymentModelID");
            this.Property(t => t.IsExcludedDeposit).HasColumnName("IsExcludedDeposit");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
        }
    }
}
