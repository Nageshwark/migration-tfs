﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class EstablishmentMappingMap : EntityTypeConfiguration<EstablishmentMapping>
    {
        public EstablishmentMappingMap()
        {
            this.HasEntitySetName("EstablishmentMapping");
            this.HasKey(t => t.DestinationId);
			this.Property(t => t.ResortDestinationId).HasColumnName("ResortDestinationId").IsRequired();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsRequired();
            this.Property(t => t.ProviderEdiCode).HasColumnName("ProviderEdiCode").IsRequired();
            this.Property(t => t.ProviderEstablishmentCode).HasColumnName("ProviderEstablishmentCode").IsRequired();
        }
    }
}
