﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class CancellationPolicyMap : EntityTypeConfiguration<CancellationPolicy>
    {
        public CancellationPolicyMap()
        {
            this.ToTable("AccommodationCancellationPolicies");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.IsB2B).HasColumnName("IsB2B").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsOptional();
            this.Property(t => t.ProviderPolicyId).HasColumnName("ProviderPolicyId").IsOptional();
            this.Property(t => t.Message).HasColumnName("Message").IsRequired().IsUnicode(false);
        }
    }
}
