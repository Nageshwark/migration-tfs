﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class DestinationMap : EntityTypeConfiguration<Destination>
    {
        public DestinationMap()
        {
            this.HasEntitySetName("Destination");
            this.Property(t => t.ParentDestinationId).HasColumnName("ParentDestinationId").IsRequired();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsOptional();
        }
    }
}
