﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class RoomTypeMap : EntityTypeConfiguration<RoomType>
    {
        public RoomTypeMap()
        {
            this.ToTable("AccommodationRoomTypes");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.Name).HasColumnName("Name").IsRequired();
        }
    }
}
