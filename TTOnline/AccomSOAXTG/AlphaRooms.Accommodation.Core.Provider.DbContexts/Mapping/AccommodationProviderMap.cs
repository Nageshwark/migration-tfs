﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationProviderMap : EntityTypeConfiguration<AccommodationProvider>
    {
        public AccommodationProviderMap()
        {
            this.ToTable("AccommodationProviders");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.IsActive).HasColumnName("IsActive").IsRequired();
            this.Property(t => t.EdiCode).HasColumnName("EdiCode").IsRequired();
            this.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false);
            this.Property(t => t.Phone).HasColumnName("Phone").IsRequired().IsUnicode(false);
            this.Property(t => t.Address).HasColumnName("Address").IsRequired().IsUnicode(false);
            this.Property(t => t.SearchTimeout).HasColumnName("SearchTimeout").IsRequired();
            this.Property(t => t.CacheExpireTimeout).HasColumnName("CacheExpireTimeout").IsRequired();
            this.Property(t => t.SessionExpireTimeout).HasColumnName("SessionExpireTimeout").IsRequired();
            this.Property(t => t.MaxInfantAge).HasColumnName("MaxInfantAge").IsRequired();
            this.Property(t => t.MaxChildAge).HasColumnName("MaxChildAge").IsRequired();
            this.Property(t => t.IsBookingEnabled).HasColumnName("IsBookingEnabled").IsRequired();
            this.Property(t => t.IsCoreCacheActive).HasColumnName("IsCoreCacheActive").IsRequired();
            this.Property(t => t.IsSearchAgeSpecific).HasColumnName("IsSearchAgeSpecific").IsRequired();
            this.Property(t => t.IsMappingEnabled).HasColumnName("IsMappingEnabled").IsRequired();
            this.Property(t => t.ContractDependencyMode).HasColumnName("ContractDependencyMode").IsRequired();
            this.Property(t => t.MaxRoomSearch).HasColumnName("MaxRoomSearch").IsRequired();
            this.Property(t => t.MaxDestinationSearch).HasColumnName("MaxDestinationSearch").IsRequired();
            this.Property(t => t.MaxEstablishmentSearch).HasColumnName("MaxEstablishmentSearch").IsRequired();
            this.Property(t => t.PreferredSearchMode).HasColumnName("PreferredSearchModeId").IsRequired();
            this.Property(t => t.MaxRoomValuation).HasColumnName("MaxRoomValuation").IsOptional();
            this.Property(t => t.MaxRoomBooking).HasColumnName("MaxRoomBooking").IsOptional();
            this.Property(t => t.MinNonPayDirectDays).HasColumnName("MinNonPayDirectDays").IsOptional();
            this.HasMany(t => t.Parameters).WithOptional().HasForeignKey(i => i.ProviderId);
        }
    }
}
