﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class B2BUserMap : EntityTypeConfiguration<B2BUser>
    {
        public B2BUserMap()
        {
            this.ToTable("B2BUsers");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled").IsRequired();
            this.Property(t => t.UserName).HasColumnName("UserName").IsRequired();
            this.Property(t => t.Password).HasColumnName("PasswordHash").IsRequired().IsUnicode(false);
            this.Property(t => t.TypeId).HasColumnName("TypeId").IsRequired();
            this.Property(t => t.SecurityStamp).HasColumnName("SecurityStamp").IsRequired().IsUnicode(false);
            this.Property(t => t.Commission).HasColumnName("Commission").IsOptional();
            this.Property(t => t.LoginIdent).HasColumnName("LoginIdent").IsOptional().IsUnicode(false);
            this.Property(t => t.CompanyId).HasColumnName("B2BCompanyId");
            this.Property(t => t.BillToCompanyId).HasColumnName("BillToCompanyId");
            this.HasMany(t => t.Providers).WithMany().Map(i =>
            {
                i.MapLeftKey("AccommodationAgentId");
                i.MapRightKey("AccommodationProviderId");
                i.ToTable("AccommodationAgentProviders");
            });
            this.HasMany(t => t.Suppliers).WithMany().Map(i =>
            {
                i.MapLeftKey("AccommodationAgentId");
                i.MapRightKey("AccommodationSupplierId");
                i.ToTable("AccommodationAgentSuppliers");
            });
            this.HasMany(t => t.Customers).WithOptional(i => i.B2BUser).HasForeignKey(x => x.B2BUserId);
            this.HasRequired(t => t.Company).WithMany(t => t.B2BUsers).HasForeignKey(d => d.CompanyId);
            this.HasOptional(t => t.BillToCompany).WithMany(t => t.B2BBillingUsers).HasForeignKey(d => d.BillToCompanyId);
            this.HasMany(t => t.AgentConfiguration).WithOptional().HasForeignKey(x => x.AgentId);

        }
    }
}