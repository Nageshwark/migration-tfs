﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class TravellerTypeMap : EntityTypeConfiguration<TravellerType>
    {
        public TravellerTypeMap()
        {
            this.ToTable("tblTravellerType");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("pkId").IsRequired();
            this.Property(t => t.Name).HasColumnName("strTravellerType").IsRequired();
        }
    }
}
