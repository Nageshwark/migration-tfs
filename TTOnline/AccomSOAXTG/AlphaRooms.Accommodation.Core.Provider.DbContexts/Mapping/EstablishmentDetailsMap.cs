﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class EstablishmentDetailsMap : EntityTypeConfiguration<EstablishmentDetails>
    {
        public EstablishmentDetailsMap()
        {
            this.HasEntitySetName("EstablishmentDetails");
            this.HasKey(t => t.EstablishmentId);
            this.Property(t => t.ParentDestinationId).HasColumnName("ParentDestinationId").IsOptional();
            this.Property(t => t.CountrySeoFriendlyName).HasColumnName("CountrySeoFriendlyName").IsRequired();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
            this.Property(t => t.DestinationName).HasColumnName("DestinationName").IsRequired();
            this.Property(t => t.DestinationReference).HasColumnName("DestinationReference").IsRequired();
            this.Property(t => t.DestinationSeoFriendlyName).HasColumnName("DestinationSeoFriendlyName").IsRequired();
            this.Property(t => t.EstablishmentAddressCity).HasColumnName("EstablishmentAddressCity").IsRequired();
            this.Property(t => t.EstablishmentAddressLine1).HasColumnName("EstablishmentAddressLine1").IsRequired();
            this.Property(t => t.EstablishmentAddressLine2).HasColumnName("EstablishmentAddressLine2").IsRequired();
            this.Property(t => t.EstablishmentAddressPostCode).HasColumnName("EstablishmentAddressPostCode").IsRequired();
            this.Property(t => t.EstablishmentChainId).HasColumnName("EstablishmentChainId").IsRequired();
            this.Property(t => t.EstablishmentDescription).HasColumnName("EstablishmentDescription").IsRequired();
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsRequired();
            this.Property(t => t.EstablishmentLatitude).HasColumnName("EstablishmentLatitude").IsRequired();
            this.Property(t => t.EstablishmentLongitude).HasColumnName("EstablishmentLongitude").IsRequired();
            this.Property(t => t.EstablishmentName).HasColumnName("EstablishmentName").IsRequired();
            this.Property(t => t.EstablishmentPopularity).HasColumnName("EstablishmentPopularity").IsRequired();
            this.Property(t => t.EstablishmentReviewAverageScore).HasColumnName("EstablishmentReviewAverageScore").IsRequired();
            this.Property(t => t.EstablishmentReviewCount).HasColumnName("EstablishmentReviewCount").IsRequired();
            this.Property(t => t.EstablishmentSeoFriendlyName).HasColumnName("EstablishmentSeoFriendlyName").IsRequired();
            this.Property(t => t.EstablishmentStarRating).HasColumnName("EstablishmentStarRating").IsRequired();
            this.Property(t => t.EstablishmentTopImageUrl).HasColumnName("EstablishmentTopImageUrl").IsRequired();
            this.Property(t => t.EstablishmentTripAdvisorAverageScore).HasColumnName("EstablishmentTripAdvisorAverageScore").IsRequired();
            this.Property(t => t.EstablishmentTripAdvisorReviewCount).HasColumnName("EstablishmentTripAdvisorReviewCount").IsRequired();
            this.Property(t => t.EstablishmentTripAdvisorAverageRating).HasColumnName("EstablishmentTripAdvisorAverageRating").IsRequired();
            this.Property(t => t.EstablishmentType).HasColumnName("EstablishmentType").IsRequired();
            this.Property(t => t.IsDestinationDistrict).HasColumnName("IsDestinationDistrict").IsRequired();
            this.Property(t => t.CountryISOCode).HasColumnName("CountryISOCode").IsRequired();
        }
    }
}
