﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationProviderAdminFeeConfigurationMap : EntityTypeConfiguration<AccommodationProviderAdminFeeConfiguration>
    {
        public AccommodationProviderAdminFeeConfigurationMap()
        {
            this.ToTable("AccommodationProviderAdminFeeConfiguration");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsRequired();
            this.Property(t => t.SupplierId).HasColumnName("SupplierId").IsRequired();
            this.Property(t => t.PaymentModelId).HasColumnName("PaymentModelId").IsRequired();
            this.Property(t => t.IsActiveAdminFee).HasColumnName("IsActiveAdminFee").IsRequired();
            this.Property(t => t.AdminFee).HasColumnName("AdminFee").IsRequired();         
        }
    }
}
