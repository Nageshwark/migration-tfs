﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class RoomDescriptionRemoveWordMap : EntityTypeConfiguration<RoomDescriptionRemoveWord>
    {
        public RoomDescriptionRemoveWordMap()
        {
            this.ToTable("NoiseWords", "DeDuplication");
            this.HasKey(t => t.Word);
            this.Property(t => t.Word).HasColumnName("ToRemove").IsRequired();
        }
    }
}
