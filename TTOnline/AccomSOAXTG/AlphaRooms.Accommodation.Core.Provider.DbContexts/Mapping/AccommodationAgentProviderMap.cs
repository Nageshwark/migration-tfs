﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationAgentProviderMap : EntityTypeConfiguration<AccommodationAgentProvider>
    {
        public AccommodationAgentProviderMap()
        {
            this.ToTable("AccommodationAgentProviders");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.AgentId).HasColumnName("AgentId").IsRequired();
            this.HasRequired(t => t.Provider).WithMany().HasForeignKey(i => i.ProviderId);
        }
    }
}