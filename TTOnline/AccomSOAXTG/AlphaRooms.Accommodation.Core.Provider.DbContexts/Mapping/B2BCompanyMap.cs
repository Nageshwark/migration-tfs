﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class B2BCompanyMap : EntityTypeConfiguration<B2BCompany>
    {

        public B2BCompanyMap()
        {
            this.ToTable("B2BCompanies");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.Name).HasColumnName("Name").IsRequired();
            this.Property(t => t.LegalName).HasColumnName("LegalName").IsRequired();
            this.Property(t => t.TypeId).HasColumnName("TypeId").IsRequired();
            this.Property(t => t.ContactName).HasColumnName("ContactName").IsOptional();
            this.Property(t => t.ContactNumber).HasColumnName("ContactNumber").IsOptional();
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress").IsOptional();
            this.Property(t => t.Fax).HasColumnName("Fax").IsOptional();
            this.Property(t => t.AddressLine1).HasColumnName("AddressLine1").IsOptional();
            this.Property(t => t.AddressLine2).HasColumnName("AddressLine2").IsOptional();
            this.Property(t => t.Town).HasColumnName("Town").IsOptional();
            this.Property(t => t.County).HasColumnName("County").IsOptional();
            this.Property(t => t.PostCode).HasColumnName("PostCode").IsOptional();
            this.Property(t => t.CountryIsoCode).HasColumnName("CountryIsoCode").IsOptional();
            this.Property(t => t.Commission).HasColumnName("Commission").IsOptional();
            this.Property(t => t.ABTANumber).HasColumnName("ABTANumber").IsOptional();
            this.Property(t => t.VATNumber).HasColumnName("VATNumber").IsOptional();
            this.Property(t => t.NavisionId).HasColumnName("NavisionId").IsOptional();
            this.Property(t => t.IsSelfBilling).HasColumnName("IsSelfBilling").IsOptional();
            this.Property(t => t.ParentCompanyId).HasColumnName("ParentId").IsOptional();
            this.HasOptional(t => t.ParentCompany).WithMany().HasForeignKey(i => i.ParentCompanyId);
            this.HasRequired(t => t.B2BUsers).WithMany().HasForeignKey(i => i.Id);        
        }
    }
}