﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class PriceHikeMap : EntityTypeConfiguration<PriceHike>
    {
        public PriceHikeMap()
        {
            this.ToTable("AccommodationPriceHikes");
            this.HasKey(i => i.Id);
            this.Property(t => t.ClientIpAddress).HasColumnName("ClientIpAddress").IsRequired();
            this.Property(t => t.HikePercent).HasColumnName("HikePercent").IsRequired();
        }
    }
}
