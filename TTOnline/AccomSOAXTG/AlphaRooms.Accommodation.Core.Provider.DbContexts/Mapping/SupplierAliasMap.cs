﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class SupplierAliasMap : EntityTypeConfiguration<SupplierAlias>
    {
        public SupplierAliasMap()
        {
            this.ToTable("AccommodationSupplierAlias");
            this.HasKey(t => t.Id);
            this.Property(t => t.EdiCode).HasColumnName("EdiCode").IsRequired();
            this.Property(t => t.Name).HasColumnName("Name").IsOptional();
            this.Property(t => t.Alias).HasColumnName("Alias").IsOptional();

        }
    }
}
