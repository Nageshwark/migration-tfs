﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationProviderEmailTemplatesMap : EntityTypeConfiguration<AccommodationProviderEmailTemplate>
    {
        public AccommodationProviderEmailTemplatesMap()
        {
            this.ToTable("AccommodationProviderEmailTemplates");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsRequired();
            this.Property(t => t.EmailTemplateTypeId).HasColumnName("EmailTemplateTypeId").IsRequired();
            this.Property(t => t.Body).HasColumnName("Body").IsRequired();
            this.Property(t => t.RoomBody).HasColumnName("RoomBody").IsOptional();
        }
    }
}
