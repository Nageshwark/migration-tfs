﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class AccommodationSupplierMap : EntityTypeConfiguration<AccommodationSupplier>
    {
        public AccommodationSupplierMap()
        {
            this.ToTable("AccommodationSuppliers");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsRequired();
            this.Property(t => t.EdiCode).HasColumnName("EdiCode").IsRequired().IsUnicode(false);
            this.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false);
            this.Property(t => t.IsSupplierCancellationPolicy).HasColumnName("IsSupplierCancellationPolicy").IsRequired();
            this.Property(t => t.IsUniqueContractRequired).HasColumnName("IsUniqueContractRequired").IsRequired();
        }
    }
}
