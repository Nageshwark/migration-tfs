﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class EstablishmentTravellerTypeMap : EntityTypeConfiguration<EstablishmentTravellerType>
    {
        public EstablishmentTravellerTypeMap()
        {
            this.HasEntitySetName("EstablishmentTravellerType");
            this.HasKey(t => t.EstablishmentId);
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsRequired();
            this.Property(t => t.TravellerTypeId).HasColumnName("TravellerTypeId").IsRequired();
        }
    }
}
