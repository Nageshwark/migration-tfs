﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class VolumeDiscountMap : EntityTypeConfiguration<VolumeDiscount>
    {
        public VolumeDiscountMap()
        {
            this.ToTable("tblVolumeDiscount");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("pkId").IsRequired();
            this.Property(t => t.Volume).HasColumnName("dclVolume").IsRequired();
            this.Property(t => t.Discount).HasColumnName("dclDiscount").IsRequired();
            this.Property(t => t.InsertDate).HasColumnName("dtmInserted").IsRequired();
            this.Property(t => t.UpdateDate).HasColumnName("dtmUpdated").IsRequired();
        }
    }
}
