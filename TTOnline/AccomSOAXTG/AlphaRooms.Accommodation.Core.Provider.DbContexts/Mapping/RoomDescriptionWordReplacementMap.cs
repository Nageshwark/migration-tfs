﻿namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Data.Entity.ModelConfiguration;

    public class RoomDescriptionWordReplacementMap : EntityTypeConfiguration<RoomDescriptionWordReplacement>
    {
        public RoomDescriptionWordReplacementMap()
        {
            this.ToTable("Alias", "DeDuplication");
            this.HasKey(t => t.Word);
            this.Property(t => t.Word).HasColumnName("ToReplace").IsRequired();
            this.Property(t => t.Replacement).HasColumnName("ReplaceWith").IsRequired();
            this.Property(t => t.Order).HasColumnName("Order").IsRequired();
        }
    }
}
