﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class MarkupMap : EntityTypeConfiguration<Markup>
    {
        public MarkupMap()
        {
            this.ToTable("AccommodationMarkups");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
	        this.Property(t => t.Channel).HasColumnName("ChannelId").IsOptional();
	        this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsOptional();
	        this.Property(t => t.SupplierId).HasColumnName("SupplierId").IsOptional();
	        this.Property(t => t.CountryCode).HasColumnName("CountryCode").IsOptional();
            this.Property(t => t.DestinationId).HasColumnName("ParentDestinationID").IsOptional();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsOptional(); 
            this.Property(t => t.EstablishmentId).HasColumnName("EstablishmentId").IsOptional();
	        this.Property(t => t.StarRating).HasColumnName("StarRating").IsOptional();
	        this.Property(t => t.RoomTypeId).HasColumnName("RoomTypeId").IsOptional();
	        this.Property(t => t.BoardType).HasColumnName("BoardTypeId").IsOptional();
	        this.Property(t => t.BookingMinDuration).HasColumnName("BookingMinDuration").IsOptional();
	        this.Property(t => t.BookingMaxDuration).HasColumnName("BookingMaxDuration").IsOptional();
	        this.Property(t => t.BookingFromDate).HasColumnName("BookingFromDate").IsOptional();
	        this.Property(t => t.BookingToDate).HasColumnName("BookingToDate").IsOptional();
	        this.Property(t => t.TravelFromDate).HasColumnName("TravelFromDate").IsOptional();
	        this.Property(t => t.TravelToDate).HasColumnName("TravelToDate").IsOptional();
            this.Property(t => t.Order).HasColumnName("Order").IsRequired();
            this.Property(t => t.IsB2B).HasColumnName("IsB2B").IsRequired();
	        this.Property(t => t.B2BAgentId).HasColumnName("B2BAgentId").IsRequired();
	        this.Property(t => t.MarkupApplyType).HasColumnName("MarkupApplyType").IsRequired();
	        this.Property(t => t.Description).HasColumnName("Description").IsRequired().IsUnicode(false); ;
	        this.Property(t => t.Value).HasColumnName("Value").IsRequired();
        }
    }
}