﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class RoomDescriptionMappingMap : EntityTypeConfiguration<RoomDescriptionMapping>
    {
        public RoomDescriptionMappingMap()
        {
            this.HasKey(t => t.RoomDescription);
            this.Property(t => t.RoomDescription).HasColumnName("RoomDescription").IsRequired();
            this.Property(t => t.ProviderRoomDescription).HasColumnName("ProviderRoomDescription").IsRequired();
        }
    }
}
