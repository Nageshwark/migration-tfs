﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class BlockedDestinationMap : EntityTypeConfiguration<BlockedDestination>
    {
        public BlockedDestinationMap()
        {
            this.ToTable("AccommodationBlockedDestinations");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id").IsRequired();
            this.Property(t => t.Channel).HasColumnName("ChannelId").IsOptional();
            this.Property(t => t.ProviderId).HasColumnName("ProviderId").IsOptional();
            this.Property(t => t.SupplierId).HasColumnName("SupplierId").IsOptional();
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
        }
    }
}
