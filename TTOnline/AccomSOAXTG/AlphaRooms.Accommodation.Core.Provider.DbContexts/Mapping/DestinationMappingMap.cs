﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping
{
    public class DestinationMappingMap : EntityTypeConfiguration<DestinationMapping>
    {
        public DestinationMappingMap()
        {
            this.HasEntitySetName("DestinationMapping");
            this.HasKey(t => t.DestinationId);
            this.Property(t => t.DestinationId).HasColumnName("DestinationId").IsRequired();
            this.Property(t => t.ProviderEdiCode).HasColumnName("ProviderEdiCode").IsRequired();
            this.Property(t => t.ProviderDestinationCode).HasColumnName("ProviderDestinationCode").IsRequired();
        }
    }
}
