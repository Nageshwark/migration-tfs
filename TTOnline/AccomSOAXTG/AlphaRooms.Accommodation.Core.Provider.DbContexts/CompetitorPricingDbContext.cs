﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts
{
    public class CompetitorPricingDbContext : DbContext
    {
        static CompetitorPricingDbContext()
        {
            Database.SetInitializer<AccommodationDbContext>(null);
        }

        public CompetitorPricingDbContext() : base("Name=CompetitorPricingEntities")
        {
        }

        public DbSet<PriceAdjustment> PriceAdjustments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PriceAdjustmentMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task<IEnumerable<PriceAdjustment>> GetPriceAdjustmentsByDestinationIdAndDatesAsync(Guid destinationId, DateTime checkInDate, DateTime checkOutDate)
        {
            return await this.ExecuteStoredProcedureAsync<PriceAdjustment>("PriceAdjustmentSelectByDestinationIdAndDates"
                , new SqlParameter("destinationId", destinationId)
                , new SqlParameter("checkInDate", checkInDate)
                , new SqlParameter("checkOutDate", checkOutDate)
                );
        }
        public async Task<IEnumerable<PriceAdjustment>> GetPriceAdjustmentsByDestinationIdAsync(Guid destinationId)
        {
            return await this.ExecuteStoredProcedureAsync<PriceAdjustment>("PriceAdjustmentSelectByDestinationId"
                , new SqlParameter("destinationId", destinationId));
        }
    }
}