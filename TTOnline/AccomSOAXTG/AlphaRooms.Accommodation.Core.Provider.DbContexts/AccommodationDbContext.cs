﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Provider.DbContexts
{
    public class AccommodationDbContext : DbContext
    {
        static AccommodationDbContext()
        {
            Database.SetInitializer<AccommodationDbContext>(null);
        }

        public AccommodationDbContext() : base("Name=AlphabedsEntities")
        {
        }

        //public DbSet<AccommodationAgentProvider> AccommodationAgentProviders { get; set; }        
        public DbSet<AccommodationProvider> AccommodationProviders { get; set; }
        public DbSet<AccommodationProviderEmailTemplate> AccommodationProviderEmailTemplates { get; set; }
        public DbSet<TravellerType> TravellerTypes { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<DestinationSearchMode> DestinationSearchModes { get; set; }
        public DbSet<Board> Boards { get; set; }
        public DbSet<BlockedDestination> BlockedDestinations { get; set; }
        public DbSet<CancellationPolicy> CancellationPolicies { get; set; }
        public DbSet<Markup> Markups { get; set; }
        public DbSet<AccommodationSupplier> AccommodationSuppliers { get; set; }
        public DbSet<SupplierAlias> SupplierAlias { get; set; }
        public DbSet<AccommodationProviderAdminFeeConfiguration> AccommodationProviderAdminFeeConfiguration { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<ProviderCommission> ProviderCommissions { get; set; }
        public DbSet<VolumeDiscount> VolumeDiscounts { get; set; }
        public DbSet<StarRating> StarRatings { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<VirtualCreditCardRule> VirtualCreditCardRules { get; set; }
        public DbSet<RoomDescriptionWordReplacement> RoomDescriptionWordReplacements { get; set; }
        public DbSet<RoomDescriptionRemoveWord> RoomDescriptionRemoveWords { get; set; }
        public DbSet<PriceHike> PriceHikes { get; set; }
        public DbSet<B2BCustomer> Customers { get; set; }
        public DbSet<B2BCompany> B2BCompanies { get; set; }
        public DbSet<B2BUser> B2BUsers { get; set; }
        public DbSet<AccommodationAgentConfiguration> AccommodationAgentConfigurations { get; set; }
        public DbSet<AccommodationB2BFlashSaleEstablishment> FlashSaleEstablishments { get; set; }

        public DbSet<SuppliersExcludedOfDeposit> SuppliersExcludedOfDeposit { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccommodationProviderMap());
            modelBuilder.Configurations.Add(new AccommodationProviderParameterMap());
            modelBuilder.Configurations.Add(new AccommodationProviderEmailTemplatesMap());
			modelBuilder.Configurations.Add(new AccommodationProviderAdminFeeConfigurationMap());
            modelBuilder.Configurations.Add(new TravellerTypeMap());
            modelBuilder.Configurations.Add(new FacilityMap());
            modelBuilder.Configurations.Add(new DestinationSearchModeMap());
            modelBuilder.Configurations.Add(new BoardMap());
            modelBuilder.Configurations.Add(new BlockedDestinationMap());
            modelBuilder.Configurations.Add(new CancellationPolicyMap());
            modelBuilder.Configurations.Add(new MarkupMap());
            modelBuilder.Configurations.Add(new AccommodationSupplierMap());
            modelBuilder.Configurations.Add(new SupplierAliasMap());
            modelBuilder.Configurations.Add(new RoomTypeMap());
            modelBuilder.Configurations.Add(new ProviderCommissionMap());
            modelBuilder.Configurations.Add(new VolumeDiscountMap());
            modelBuilder.Configurations.Add(new StarRatingMap());
            modelBuilder.Configurations.Add(new PaymentTypeMap());
            modelBuilder.Configurations.Add(new DestinationMap());
            modelBuilder.Configurations.Add(new VirtualCreditCardRuleMap());
            modelBuilder.Configurations.Add(new RoomDescriptionWordReplacementMap());
            modelBuilder.Configurations.Add(new RoomDescriptionRemoveWordMap());
            modelBuilder.Configurations.Add(new PriceHikeMap());
            modelBuilder.Configurations.Add(new DestinationMappingMap());
            modelBuilder.Configurations.Add(new EstablishmentMappingMap());
            modelBuilder.Configurations.Add(new EstablishmentDetailsMap());
            modelBuilder.Configurations.Add(new EstablishmentFacilityMap());
            modelBuilder.Configurations.Add(new EstablishmentTravellerTypeMap());
            modelBuilder.Configurations.Add(new B2BCustomerMap());
            modelBuilder.Configurations.Add(new B2BCompanyMap());
            modelBuilder.Configurations.Add(new B2BUserMap());
            modelBuilder.Configurations.Add(new AccommodationAgentConfigurationMap());
            modelBuilder.Configurations.Add(new AccommodationB2BFlashSaleEstablishmentMap());
            modelBuilder.Configurations.Add(new SuppliersExcludedOfDepositMap());

            base.OnModelCreating(modelBuilder);
        }

        public async Task<IEnumerable<Destination>> GetDestinationsAsync()
        {
            return await this.ExecuteStoredProcedureAsync<Destination>("DestinationSelectAll");
        }

        public async Task<MultiResultsReader> GetAccommodationMappingByBaseDestinationIdAsync(Guid destinationId)
        {
            return await this.ExecuteStoredProcedureMultiResultsAsync("AccommodationMappingByMasterDestinationId", new SqlParameter("destinationId", destinationId));
        }

        public async Task<MultiResultsReader> GetEstablishmentDetailsByDestinationIdAsync(Guid destinationId, Channel channel)
        {
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("destinationId", destinationId);
            parameters[1] = new SqlParameter("channelID", (int)channel);
            return await this.ExecuteStoredProcedureMultiResultsAsync("EstablishmentDetailsByDestinationId", parameters);
        }

        public async Task<IEnumerable<BoardDescriptionMapping>> GetBoardDescriptionMappingByLocaleIdAsync(int localeId)
        {
            return await this.ExecuteStoredProcedureAsync<BoardDescriptionMapping>("BoardDescriptionMappingSelectByLocaleId", new SqlParameter("localeId", localeId));
        }

        public async Task<IEnumerable<RoomDescriptionMapping>> GetRoomDescriptionMappingByLocaleIdAsync(int localeId)
        {
            return await this.ExecuteStoredProcedureAsync<RoomDescriptionMapping>("RoomDescriptionMappingSelectByLocaleId", new SqlParameter("localeId", localeId));
        }

        public async Task<IEnumerable<Markup>> GetMarkupsAllAsync()
        {
            return await this.ExecuteStoredProcedureAsync<Markup>("AccommodationMarkupSelectAll");
        }

        public async Task<string[]> GetDepositExcludedListOFProvidersAsync()
        {
            var providerEdiCode = await (from accommProv in AccommodationProviders
                                         join supplierExOfDep in SuppliersExcludedOfDeposit
                                         on accommProv.Id equals supplierExOfDep.AccommodationProviderID
                                         where supplierExOfDep.AccommodationProviderID.HasValue && supplierExOfDep.PaymentModelID == 1 && supplierExOfDep.IsExcludedDeposit == 1
                                         select accommProv.EdiCode).Distinct().ToArrayAsync();

            var supplierEdiCode = await (from accommsuppl in AccommodationSuppliers
                                         join supplierExOfDep in SuppliersExcludedOfDeposit
                                         on accommsuppl.Id equals supplierExOfDep.AccommodationSupplierID
                                         where supplierExOfDep.AccommodationSupplierID.HasValue && supplierExOfDep.PaymentModelID == 1 && supplierExOfDep.IsExcludedDeposit == 1
                                         select accommsuppl.EdiCode).Distinct().ToArrayAsync();

            var finalListOfProvSuppEdiCodes = providerEdiCode.Concat(supplierEdiCode).Distinct().ToArray();
            return finalListOfProvSuppEdiCodes;
        }
		
        public bool CheckIfSupplierExistsInExcludedList(string[] excludedSupplierList, string ediCode)
        {
            return excludedSupplierList.Any(x => x == ediCode);
        }

    }
}
