﻿namespace AlphaRooms.Accommodation.B2C.Caching
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Interfaces.Caching;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using System;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.B2C.Interfaces;

    public class B2CRequestStatusCaching : IB2CRequestStatusCaching
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICacheQueryExpireAsync<AccommodationB2CRequestStatus> cacheQuery;
        private readonly ICacheWriterExpireAsync<AccommodationB2CRequestStatus> cacheWriter;
        private readonly IB2CGuidService guidService;

        public B2CRequestStatusCaching(IAccommodationConfigurationManager configurationManager, ICacheQueryExpireAsync<AccommodationB2CRequestStatus> cacheQuery
            , ICacheWriterExpireAsync<AccommodationB2CRequestStatus> cacheWriter, IB2CGuidService guidService)
        {
            this.configurationManager = configurationManager;
            this.cacheQuery = cacheQuery;
            this.cacheWriter = cacheWriter;
            this.guidService = guidService;
        }

        public async Task<AccommodationB2CRequestStatus> GetByIdOrNullAsync(Guid requestId)
        {
            return await this.cacheQuery.GetItemOrNullAsync(i => i.Id == requestId, this.guidService.IsMetaGuid(requestId));
        }

        public async Task<AccommodationB2CRequestStatus> GetSuccessfulByAvailabilityKeyOrNullAsync(string availabilityKey, Guid? id)
        {
            return await this.cacheQuery.GetFirstItemOrNullAsync(i => i.AvailabilityKey == availabilityKey && i.Status == Status.Successful
                && i.ResultsExpireDate > this.cacheQuery.GetCacheExpireDateThreshold() + configurationManager.AccommodationB2CRequestStatusExpireThreshold
                , new CacheSort<AccommodationB2CRequestStatus>(i => i.StartDate, CacheSortOrder.Asc), this.guidService.IsMetaGuid(id));
        }

        public DateTime CreateResultsCacheExpireDate()
        {
            return this.cacheQuery.GetCacheExpireDateThreshold();
        }

        public bool IsResultsExpireDateExpired(AccommodationB2CRequestStatus cachedRequestStatus)
        {
            return this.cacheQuery.IsCacheExpireDateExpired(cachedRequestStatus.ResultsExpireDate.Value);
        }

        public async Task SaveAsync(AccommodationB2CRequestStatus requestStatus)
        {
            await this.cacheWriter.AddOrUpdateAsync(i => i.Id == requestStatus.Id, requestStatus, this.configurationManager.AccommodationB2CRequestStatusExpireTimeout, this.guidService.IsMetaGuid(requestStatus.Id));
        }

		public async Task<AccommodationB2CRequestStatus> GetSuccessfulByMetaTokenKeyOrNullAsync(string metaTokenKey)
		{
			return await this.cacheQuery.GetFirstItemOrNullAsync(i => i.AvailabilityKey == metaTokenKey && i.Status == Status.Successful
				&& i.ResultsExpireDate > this.cacheQuery.GetCacheExpireDateThreshold() + configurationManager.AccommodationB2CRequestStatusExpireThreshold
				, new CacheSort<AccommodationB2CRequestStatus>(i => i.StartDate, CacheSortOrder.Asc), true);
		}
	}
}
