﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Cache.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Caching
{
    public class B2CAvailabilityResultDatabaseController : ICacheMultiDatabaseController<AccommodationB2CAvailabilityResult, Guid>
    {
        public int GetDatabaseIndexByKey(int multiDatabasesCount, Guid databaseKey)
        {
            var c = databaseKey.ToString()[0];
            var v = (c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10);
            return v * multiDatabasesCount / 16;
        }
    }
}
