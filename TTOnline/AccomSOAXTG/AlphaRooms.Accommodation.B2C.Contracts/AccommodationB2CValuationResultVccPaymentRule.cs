﻿using AlphaRooms.SOACommon.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CValuationResultVccPaymentRule
    {
        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public Money Price { get; set; }

        [DataMember]
        public string RoomCode { get; set; }
    }
}
