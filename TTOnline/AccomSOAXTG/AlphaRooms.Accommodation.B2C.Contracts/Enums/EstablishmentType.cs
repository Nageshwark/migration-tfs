﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts.Enums
{
    public enum EstablishmentType
    {
        Hotel = 0
        , Apartment = 1
        , Other = 2
    }
}
