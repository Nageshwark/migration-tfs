﻿namespace AlphaRooms.Accommodation.B2C.Contracts.Enums
{
    public enum AccommodationB2CAvailabilitySortField
    {
        MostPopular = 0,
        CheapestPrice = 1
    }
}
