﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.B2C.Contracts.Enums
{
    public enum AccommodationB2CAvailabilitySortOrder
    {
        Ascendant = 0
        , Descendent = 1
    }
}
