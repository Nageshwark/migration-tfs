﻿using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    public class AccommodationB2CRequestStatus : ICacheExpire
    {
        public const string RequestTypeFieldName = "RTy";
        public const string StartDateFieldName = "SDt";
        public const string DurationFieldName = "SDr";
        public const string StatusFieldName = "SSt";
        public const string MessageFieldName = "Msg";
        public const string CacheExpireDateFieldName = "CED";
        public const string AvailabilityRequestFieldName = "ARq";
        public const string AvailabilityKeyFieldName = "Avk";
        public const string ResultsBaseDestinationIdFieldName = "ABD";
        public const string ResultsExpireDateFieldName = "RED";
        public const string ResultsAvailabilityIdFieldName = "RAv";
        public const string ResultsFilterOptionsFieldName = "RFt";
        public const string ValuationRequestFieldName = "VRq";
        public const string BookingRequestFieldName = "BRq";
        public const string CancellationRequestFieldName = "Crq";
        public const string HasTC4OriginFieldName = "TCo";

        [BsonId]
        public Guid Id { get; set; }

        [BsonElement(RequestTypeFieldName)]
        [BsonIgnoreIfDefault]
        public RequestType RequestType { get; set; }

        [BsonElement(StartDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime StartDate { get; set; }

        [BsonElement(DurationFieldName)]
        [BsonIgnoreIfDefault]
        public int Duration { get; set; }

        [BsonElement(StatusFieldName)]
        [BsonIgnoreIfDefault]
        public Status Status { get; set; }

        [BsonElement(MessageFieldName)]
        [BsonIgnoreIfDefault]
        public string Message { get; set; }

        [BsonElement(CacheExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CacheExpireDate { get; set; }

        // availability
        [BsonElement(AvailabilityRequestFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityRequest AvailabilityRequest { get; set; }

        [BsonElement(AvailabilityKeyFieldName)]
        [BsonIgnoreIfDefault]
        public string AvailabilityKey { get; set; }

        [BsonElement(ResultsExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime? ResultsExpireDate { get; set; }

        [BsonElement(ResultsBaseDestinationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid? ResultsBaseDestinationId { get; set; }

        [BsonElement(ResultsAvailabilityIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid? ResultsAvailabilityId { get; set; }

        [BsonElement(ResultsFilterOptionsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptions ResultsFilterOptions { get; set; }

        // valuation
        [BsonElement(ValuationRequestFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CValuationRequest ValuationRequest { get; set; }

        // booking
        [BsonElement(BookingRequestFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CBookingRequest BookingRequest { get; set; }

        // cancellation
        [BsonElement(CancellationRequestFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CCancellationRequest CancellationRequest { get; set; }
        
        [BsonElement(HasTC4OriginFieldName)]
        [BsonIgnoreIfDefault]
        public bool HasTC4Origin { get; set; }
    }
}
