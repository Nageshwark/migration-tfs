﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CCancellationRequestCustomer
    {
        public const string IdFieldName = "Id";
        public const string FirstNameFieldName = "FNm";
        public const string SurnameFieldName = "SNm";
        public const string EmailAddressFieldName = "Eml";
        public const string ContactNumberFieldName = "CtN";
        
        [DataMember]
        [BsonElement(IdFieldName)]
        [BsonIgnoreIfDefault]
        public int Id { get; set; }

        [DataMember]
        [BsonElement(FirstNameFieldName)]
        [BsonIgnoreIfDefault]
        public string FirstName { get; set; }

        [DataMember]
        [BsonElement(SurnameFieldName)]
        [BsonIgnoreIfDefault]
        public string Surname { get; set; }

        [DataMember]
        [BsonElement(EmailAddressFieldName)]
        [BsonIgnoreIfDefault]
        public string EmailAddress { get; set; }

        [DataMember]
        [BsonElement(ContactNumberFieldName)]
        [BsonIgnoreIfDefault]
        public string ContactNumber { get; set; }
    }
}
