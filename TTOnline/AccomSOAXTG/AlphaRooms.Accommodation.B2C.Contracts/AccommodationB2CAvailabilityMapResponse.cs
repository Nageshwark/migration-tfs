﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilityMapResponse
    {
        [DataMember]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        public Status AvailabilityStatus { get; set; }

        [DataMember]
        public AccommodationB2CAvailabilityMapResult[] AvailabilityMapResults { get; set; }

        [DataMember]
        public int AvailabilityTotalResultsCount { get; set; }
    }
}
