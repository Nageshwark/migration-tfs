﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CBookingResult
    {
        [DataMember]
        public Guid BookingId { get; set; }

        [DataMember]
        public AccommodationB2CBookingResultRoom[] Rooms { get; set; }
    }
}
