﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CAvailabilityFilterOptionDetailsItem
    {
        [DataMember]
        public string Section { get; set; }

        [DataMember]
        public string FilterId { get; set; }

        [DataMember]
        public int ResultsCount { get; set; }
    }
}
