﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResultsFilterOptionPaymentType
    {
        public const string NameFieldName = "Nam";

        [DataMember]
        [BsonId]
        public PaymentTypeType PaymentType { get; set; }

        [DataMember]
        [BsonElement(NameFieldName)]
        [BsonIgnoreIfDefault]
        public string Name { get; set; }
    }
}