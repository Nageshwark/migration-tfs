﻿namespace AlphaRooms.Accommodation.B2C.Contracts
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResultRoom
    {
        private const string RoomIdAlpha2FieldName = "RIA";
        private const string ProviderEdiCodeFieldName = "PCd";
        private const string ProviderNameFieldName = "PNm";
        private const string ProviderAliasNameFieldName = "PANm";
        private const string ProviderEstablishmentCodeFieldName = "PEC";
        private const string RoomNumberFieldName = "RmN";
        private const string ProviderRoomCodeFieldName = "PRC";
        private const string ProviderBoardCodeFieldName = "PBC";
        private const string ProviderBoardDescriptionFieldName = "PBD";
        private const string RoomDescriptionFieldName = "RDp";
        private const string BoardDescriptionFieldName = "BdD";
        private const string BoardTypeFieldName = "BdT";
        private const string PaymentModelFieldName = "PMd";
        private const string ProviderPriceFieldName = "PPr";
        private const string PriceFieldName = "Prc";
        private const string PricePerNightFieldName = "PPN";
        private const string PricePerPersonFieldName = "PPP";
        private const string PaymentToTakeFieldName = "PTT";
        private const string ExchangeRateFieldName = "ExR";
        private const string MarginFieldName = "Mrg";
        private const string IsLowestPriceFieldName = "LPr";
        private const string RateTypeFieldName = "RTy";
        private const string HighestPriceFieldName = "HPr";
        private const string IsEligibleForDepositFieldName = "EDP";
        private const string DiscountPriceFieldName = "Dis";
        private const string ProviderSpecificDataFieldName = "PSD";
        private const string SupplierEdiCodeFieldName = "s";
        private const string ProviderDestinationCodeFieldName = "dst";
        private const string ProviderContractCodeFieldName = "PC";
        private const string ProviderProviderCodeFieldName = "PPc";
        private const string ProviderRoomDescriptionFieldName = "PRD";
        private const string FlagsFieldName = "F";
		private const string IsActiveAccommodationAdminFeeFieldName = "AAFN";
        private const string AccommodationAdminFeeFieldName = "AAF";

        [BsonId]
        public Guid Id { get; set; }

        [DataMember]
        [BsonElement(RoomIdAlpha2FieldName)]
        [BsonIgnoreIfDefault]
        public uint RoomIdAlpha2 { get; set; }

        [DataMember]
        [BsonElement(RoomNumberFieldName)]
        [BsonIgnoreIfDefault]
        public int RoomNumber { get; set; }

        [DataMember]
        [BsonElement(ProviderEdiCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderEdiCode { get; set; }

        [DataMember]
        [BsonElement(ProviderNameFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderName { get; set; }

        [DataMember]
        [BsonElement(ProviderAliasNameFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderAliasName { get; set; }

        [BsonIgnore]
        public string ProviderFilterName { get; set; }

        [BsonIgnore]
        public AccommodationB2CAvailabilityResult EstablishmentResult { get; set; }

        [DataMember]
        [BsonElement(ProviderEstablishmentCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderEstablishmentCode { get; set; }

        [DataMember]
        [BsonElement(ProviderRoomCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderRoomCode { get; set; }

        [BsonElement(ProviderBoardCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderBoardCode { get; set; }

        [BsonElement(ProviderBoardDescriptionFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderBoardDescription { get; set; }

        [DataMember]
        [BsonElement(RoomDescriptionFieldName)]
        [BsonIgnoreIfDefault]
        public string RoomDescription { get; set; }

        [DataMember]
        [BsonElement(BoardTypeFieldName)]
        [BsonIgnoreIfDefault]
        public BoardType BoardType { get; set; }

        // this looks like a static list should not save this
        [DataMember]
        [BsonElement(BoardDescriptionFieldName)]
        [BsonIgnoreIfDefault]
        public string BoardDescription { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsOpaqueRate { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsNonRefundable { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsBindingRate { get; set; }

        [DataMember]
        [BsonElement(PaymentModelFieldName)]
        [BsonIgnoreIfDefault]
        public PaymentModel PaymentModel { get; set; }

        [DataMember]
        [BsonElement(ProviderPriceFieldName)]
        [BsonIgnoreIfDefault]
        public Money ProviderPrice { get; set; }

        [DataMember]
        [BsonElement(PriceFieldName)]
        [BsonIgnoreIfDefault]
        public Money Price { get; set; }

        [BsonElement(PricePerNightFieldName)]
        [BsonIgnoreIfDefault]
        public Money PricePerNight { get; set; }

        [BsonElement(PricePerPersonFieldName)]
        public Money PricePerPerson { get; set; }

        [DataMember]
        [BsonElement(PaymentToTakeFieldName)]
        [BsonIgnoreIfDefault]
        public Money PaymentToTake { get; set; }

        [DataMember]
        [BsonElement(ExchangeRateFieldName)]
        [BsonIgnoreIfDefault]
        public decimal ExchangeRate { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsCardChargeApplicable
        {
            get
            {
                return this.PaymentModel != PaymentModel.CustomerPayDirect;
            }
            private set
            {
                // need we a set to make the DataMember field work correctly
                throw new InvalidOperationException($"{nameof(IsCardChargeApplicable)} can not be set it is Calutlated filed");
            }
        }

        [DataMember]
        [BsonElement(MarginFieldName)]
        [BsonIgnoreIfDefault]
        public decimal Margin { get; set; }

        // is filtered on
        [DataMember]
        [BsonElement(IsLowestPriceFieldName)]
        [BsonIgnoreIfDefault]
        public bool IsLowestPrice { get; set; }

        // the depubliced price, could just in price no need for currency
        [DataMember]
        [BsonElement(HighestPriceFieldName)]
        [BsonIgnoreIfDefault]
        public Money HighestPrice { get; set; }

        [DataMember]
        [BsonElement(DiscountPriceFieldName)]
        [BsonIgnoreIfDefault]
        public PromotionalDiscountResult Discount { get; set; }

        [DataMember]
        [BsonElement(RateTypeFieldName)]
        [BsonIgnoreIfDefault]
        public RateType RateType { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsDuplicateRate { get; set; }

        // filtered on
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsEligibleForDepositFieldName)]
        public bool IsEligibleForDeposit { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderSpecificDataFieldName)]
        public Dictionary<string, string> ProviderSpecificData { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(SupplierEdiCodeFieldName)]
        public string SupplierEdiCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderDestinationCodeFieldName)]
        public string ProviderDestinationCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderContractCodeFieldName)]
        public string ProviderContract { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderProviderCodeFieldName)]
        public string ProviderProviderCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderRoomDescriptionFieldName)]
        public string ProviderRoomDescription { get; set; }
		
		[DataMember]
		[BsonIgnoreIfDefault]
        [BsonElement(IsActiveAccommodationAdminFeeFieldName)]
        public bool IsActiveAccommodationAdminFee { get; set; }

        [DataMember]
        [BsonElement(AccommodationAdminFeeFieldName)]
        public Money AccommodationAdminFee { get; set; }

        /// <summary>
        /// This contains multiple bool fields condensed into one field.
        /// This is to has save space when storing this into mongo.
        /// </summary>
        [BsonIgnoreIfDefault]
        [BsonElement(FlagsFieldName)]
        private RoomDetailsFlags Flags
        {
            get
            {
                RoomDetailsFlags flags = 0;

                if (this.IsNonRefundable)
                {
                    flags |= RoomDetailsFlags.IsNonRefundable;
                }

                if (this.IsDuplicateRate)
                {
                    flags |= RoomDetailsFlags.IsDuplicateRate;
                }

                if (this.IsBindingRate)
                {
                    flags |= RoomDetailsFlags.IsBindingRate;
                }

                if (this.IsOpaqueRate)
                {
                    flags |= RoomDetailsFlags.IsOpaqueRate;
                }

                return flags;
            }
            set
            {
                this.IsNonRefundable = value.HasFlag(RoomDetailsFlags.IsNonRefundable);
                this.IsDuplicateRate = value.HasFlag(RoomDetailsFlags.IsDuplicateRate);
                this.IsBindingRate = value.HasFlag(RoomDetailsFlags.IsBindingRate);
                this.IsOpaqueRate = value.HasFlag(RoomDetailsFlags.IsOpaqueRate);
            }
        }
    }
}