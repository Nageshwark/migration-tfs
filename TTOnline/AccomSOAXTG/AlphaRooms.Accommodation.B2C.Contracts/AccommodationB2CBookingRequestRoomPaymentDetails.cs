﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CBookingRequestRoomPaymentDetails
    {

        public const string CardTypeFieldName = "CdT";
        public const string CardHolderNameFieldName = "CHN";
        public const string CardNumberFieldName = "CNm";
        public const string CardExpireDateFieldName = "CdE";
        public const string CardSecurityCodeFieldName = "CSC";
        public const string AddressLine1FieldName = "Bld";
        public const string AddressLine2FieldName = "Str";
        public const string CityFieldName = "Twn";
        public const string PostcodeFieldName = "Psc";
        public const string CountryFieldName = "Ctr";
        public const string CountyFieldName = "Cnt";


        [DataMember]
        [BsonElement(CardTypeFieldName)]
        [BsonIgnoreIfDefault]
        public CardType CardType { get; set; }

        [DataMember]
        [BsonElement(CardHolderNameFieldName)]
        [BsonIgnoreIfDefault]
        public string CardHolderName { get; set; }

        [DataMember]
        [BsonElement(CardNumberFieldName)]
        [BsonIgnoreIfDefault]
        public string CardNumber { get; set; }

        [DataMember]
        [BsonElement(CardExpireDateFieldName)]
        [BsonIgnoreIfDefault]
        public MonthYear CardExpireDate { get; set; }

        [DataMember]
        [BsonElement(CardSecurityCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string CardSecurityCode { get; set; }

        [DataMember]
        [BsonElement(AddressLine1FieldName)]
        [BsonIgnoreIfDefault]
        public string AddressLine1 { get; set; }

        [DataMember]
        [BsonElement(AddressLine2FieldName)]
        [BsonIgnoreIfDefault]
        public string AddressLine2 { get; set; }

        [DataMember]
        [BsonElement(CityFieldName)]
        [BsonIgnoreIfDefault]
        public string City { get; set; }

        [DataMember]
        [BsonElement(CountryFieldName)]
        [BsonIgnoreIfDefault]
        public string Country { get; set; }

        [DataMember]
        [BsonElement(PostcodeFieldName)]
        [BsonIgnoreIfDefault]
        public string Postcode { get; set; }

        [DataMember]
        [BsonElement(CountyFieldName)]
        [BsonIgnoreIfDefault]
        public string County { get; set; }
    }
}