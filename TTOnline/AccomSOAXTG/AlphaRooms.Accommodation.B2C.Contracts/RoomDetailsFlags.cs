﻿namespace AlphaRooms.Accommodation.B2C.Contracts
{
    using System;

    /// <summary>
    /// This flag are to save space when storing our mongo results.
    /// large mongo dataset was a problem that caused slow search speed by taking ages to insert into mongo
    /// </summary>
    [Flags]
    internal enum RoomDetailsFlags
    {
        IsNonRefundable = 1 << 0,
        IsDuplicateRate = 1 << 1,
        IsBindingRate = 1 << 2,
        IsOpaqueRate = 1 << 3,
    }
}
