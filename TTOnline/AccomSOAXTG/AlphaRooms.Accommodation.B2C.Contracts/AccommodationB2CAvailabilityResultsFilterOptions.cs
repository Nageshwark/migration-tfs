﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResultsFilterOptions
    {


        public const string PricePerNightMinFieldName = "PNMi";
        public const string PricePerNightMaxFieldName = "PNMa";
        public const string PricePerPersonMinFieldName = "PPMi";
        public const string PricePerPersonMaxFieldName = "PPMa";
        public const string PaymentTypesFieldName = "PyT";
        public const string StarRatingsFieldName = "SRt";
        public const string BoardsFieldName = "Brd";
        public const string FacilitiesFieldName = "Fct";
        public const string TravellerTypesFieldName = "TTs";
        public const string ProvidersFieldName = "Pvd";
        public const string EstablishmentNamesFieldName = "EtN";
        public const string DistrictsFieldName = "Dtt";
        public const string HasDepositOptionFieldName = "Dpo";
        public const string PricePerNightRangeFieldName = "PNRg";
        public const string PricePerPersonRangeFieldName = "PPRg";

        [DataMember]
        [BsonElement(PricePerNightMinFieldName)]
        [BsonIgnoreIfDefault]
        public Money PricePerNightMin { get; set; }

        [DataMember]
        [BsonElement(PricePerNightMaxFieldName)]
        [BsonIgnoreIfDefault]
        public Money PricePerNightMax { get; set; }

		[DataMember]
        [BsonElement(PricePerPersonMinFieldName)]
        public Money PricePerPersonMin { get; set; }

        [DataMember]
        [BsonElement(PricePerPersonMaxFieldName)]
        public Money PricePerPersonMax { get; set; }

        [DataMember]
        [BsonElement(PaymentTypesFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionPaymentType[] PaymentTypes { get; set; }

        [DataMember]
        [BsonElement(StarRatingsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionStarRating[] StarRatings { get; set; }

        [DataMember]
        [BsonElement(BoardsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionBoardType[] Boards { get; set; }

        [DataMember]
        [BsonElement(FacilitiesFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionItem[] Facilities { get; set; }

        [DataMember]
        [BsonElement(TravellerTypesFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionItem[] TravellerTypes { get; set; }

        [DataMember]
        [BsonElement(ProvidersFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionProvider[] Providers { get; set; }

        [DataMember]
        [BsonElement(EstablishmentNamesFieldName)]
        [BsonIgnoreIfDefault]
        public string[] EstablishmentNames { get; set; }

        [DataMember]
        [BsonElement(DistrictsFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CAvailabilityResultsFilterOptionItem[] Districts { get; set; }

        [DataMember]
        [BsonElement(HasDepositOptionFieldName)]
        [BsonIgnoreIfDefault]
        public bool HasDepositOption { get; set; }

        [DataMember]
        [BsonElement(PricePerNightRangeFieldName)]
        public B2CResultsRangeFilterOptions PricePerNightRange { get; set; }

        [DataMember]
        [BsonElement(PricePerPersonRangeFieldName)]
        public B2CResultsRangeFilterOptions PricePerPersonRange { get; set; }
    }
}
