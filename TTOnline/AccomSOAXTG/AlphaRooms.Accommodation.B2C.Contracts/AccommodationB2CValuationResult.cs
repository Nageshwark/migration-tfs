﻿using AlphaRooms.Cache.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CValuationResult
    {
        [DataMember]
        public Guid ValuationId { get; set; }

        [DataMember]
        public AccommodationB2CValuationResultRoom[] Rooms { get; set; }
    }
}
