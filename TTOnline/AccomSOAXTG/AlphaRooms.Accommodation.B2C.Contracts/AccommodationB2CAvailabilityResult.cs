﻿namespace AlphaRooms.Accommodation.B2C.Contracts
{
    using AlphaRooms.Accommodation.B2C.Contracts.Enums;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.SOACommon.Contracts;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResult : ICacheExpire
    {
        private const string AvailabilityIdFieldName = "AvI";
        private const string BaseDestinationIdFieldName = "BDI";
        private const string DestinationIdFieldName = "DsI";
        private const string DestinationNameFieldName = "DsN";
        private const string EstablishmentIdFieldName = "EtI";
        private const string EstablishmentNameFieldName = "EtN";
        private const string EstablishmentStarRatingFieldName = "STr";
        private const string EstablishmentFacilitiesFieldName = "Fcs";
        private const string EstablishmentTravellerTypesFieldName = "TTs";
        private const string RoomsFieldName = "Rms";
        private const string RoomsCountFieldName = "RmC";
        private const string RoomsCheapestPriceFieldName = "CPr";
        private const string RoomsCheapestPricePerNightFieldName = "CPN";

        private const string DestinationSeoFriendlyNameFieldName = "DSN";
        private const string EstablishmentSeoFriendlyNameFieldName = "ESN";
        private const string EstablishmentDescriptionFieldName = "EDs";
        private const string EstablishmentTypeFieldName = "ETp";
        private const string EstablishmentTopImageUrlFieldName = "ETI";
        private const string EstablishmentReviewCountFieldName = "ERC";
        private const string EstablishmentReviewAverageScoreFieldName = "ERA";
        private const string EstablishmentTripAdvisorReviewCountFieldName = "TAC";
        private const string EstablishmentTripAdvisorAverageScoreFieldName = "TAA";
        private const string EstablishmentTripAdvisorAverageRatingFieldName = "TAR";
        private const string EstablishmentPopularityFieldName = "EPo";
        private const string EstablishmentLongitudeFieldName = "ELg";
        private const string EstablishmentLatitudeCountFieldName = "ELt";
        private const string CacheExpireDateFieldName = "CED";
        private const string DestinationReferenceFieldName = "DtR";
        private const string IsDestinationDistrictFieldName = "IDD";
        private const string CheckInDateFieldName = "CIn";
        private const string CheckOutDateFieldName = "COt";
        private const string RoomOccupanciesFieldName = "RmsO";

        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(BaseDestinationIdFieldName)]
        public Guid BaseDestinationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationIdFieldName)]
        public Guid DestinationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentIdFieldName)]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentNameFieldName)]
        public string EstablishmentName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentStarRatingFieldName)]
        public StarRatingType EstablishmentStarRating { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentFacilitiesFieldName)]
        public Facility[] EstablishmentFacilities { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTravellerTypesFieldName)]
        public TravellerType[] EstablishmentTravellerTypes { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsFieldName)]
        public AccommodationB2CAvailabilityResultRoom[] Rooms { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCountFieldName)]
        public int RoomsCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCheapestPriceFieldName)]
        public Money RoomsCheapestPrice { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCheapestPricePerNightFieldName)]
        public Money RoomsCheapestPricePerNight { get; set; }

        // only for Sorting the result.
        [BsonIgnore]
        public Money RoomsCheapestPriceSort { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }

        // Details
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationNameFieldName)]
        public string DestinationName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationSeoFriendlyNameFieldName)]
        public string DestinationSeoFriendlyName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentSeoFriendlyNameFieldName)]
        public string EstablishmentSeoFriendlyName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentDescriptionFieldName)]
        public string EstablishmentDescription { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTypeFieldName)]
        public EstablishmentType EstablishmentType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTopImageUrlFieldName)]
        public string EstablishmentTopImageUrl { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentReviewCountFieldName)]
        public int EstablishmentReviewCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentReviewAverageScoreFieldName)]
        public int EstablishmentReviewAverageScore { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTripAdvisorAverageScoreFieldName)]
        public decimal? EstablishmentTripAdvisorAverageScore { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTripAdvisorReviewCountFieldName)]
        public int? EstablishmentTripAdvisorReviewCount { get; set; }

        [BsonElement(EstablishmentTripAdvisorAverageRatingFieldName)]
        [BsonIgnoreIfNull]
        public decimal? EstablishmentTripAdvisorAverageRating { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentPopularityFieldName)]
        public int EstablishmentPopularity { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentLongitudeFieldName)]
        public double? EstablishmentLongitude { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentLatitudeCountFieldName)]
        public double? EstablishmentLatitude { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(DestinationReferenceFieldName)]
        public int DestinationReference { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(IsDestinationDistrictFieldName)]
        public bool IsDestinationDistrict { get; set; }

        [DataMember]
        [BsonIgnore]
        public bool IsEligibleForOnePoundDeposit { get; set; }

        // should not be here should be got from the request
        [DataMember]
        [BsonElement(CheckInDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CheckInDate { get; set; }

        // should not be here should be got from the request
        [DataMember]
        [BsonElement(CheckOutDateFieldName)]
        [BsonIgnoreIfDefault]
        public DateTime CheckOutDate { get; set; }

        [DataMember]
        [BsonElement(RoomOccupanciesFieldName)]
        [BsonIgnoreIfDefault]
        public AccommodationB2CRoomOccupancy[] RoomOccupancies { get; set; }
    }
}