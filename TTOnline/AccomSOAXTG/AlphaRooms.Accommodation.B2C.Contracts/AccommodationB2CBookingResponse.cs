﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System.Runtime.Serialization;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    public class AccommodationB2CBookingResponse
    {
        [DataMember]
        public Guid BookingId { get; set; }

        [DataMember]
        public Status BookingStatus { get; set; }

        [DataMember]
        public AccommodationB2CBookingResult BookingResult { get; set; }
    }
}
