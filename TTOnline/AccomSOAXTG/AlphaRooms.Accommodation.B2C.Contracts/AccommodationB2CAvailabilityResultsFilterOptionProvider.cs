﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CAvailabilityResultsFilterOptionProvider
    {
        public const string ProvideEdiCodeFieldName = "PrC";
        public const string ProviderNameFieldName = "PrN";

        [DataMember]
        [BsonElement(ProvideEdiCodeFieldName)]
        [BsonIgnoreIfDefault]
        public string ProvideEdiCode { get; set; }

        [DataMember]
        [BsonElement(ProviderNameFieldName)]
        [BsonIgnoreIfDefault]
        public string ProviderName { get; set; } 
    }
}
