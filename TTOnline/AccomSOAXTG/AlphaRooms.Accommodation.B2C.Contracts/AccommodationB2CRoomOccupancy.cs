﻿
using System;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CRoomOccupancy
    {
        public const string AdultsCountFieldName = "Ads";
        public const string ChildrenAndInfantsCountFieldName = "CAI";
        public const string RoomNumberFieldName = "RO";

        [DataMember]
        [BsonElement(RoomNumberFieldName)]
        [BsonIgnoreIfDefault]
        public byte RoomNumber { get; set; }

        [DataMember]
        [BsonElement(AdultsCountFieldName)]
        [BsonIgnoreIfDefault]
        public byte AdultsCount { get; set; }

        [DataMember]
        [BsonElement(ChildrenAndInfantsCountFieldName)]
        [BsonIgnoreIfDefault]
        public byte ChildrenAndInfantsCount { get; set; }
    }
}
