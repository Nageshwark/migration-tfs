﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2CCancellationRequest
    {
        public const string CancellationIdFieldName = "CId";
        public const string ChannelFieldName = "Chn";
        public const string ItineraryIdFieldName = "IId";
        public const string CustomerFieldName = "Cst";
        public const string DebuggingFieldName = "Dbg";

        [DataMember]
        [BsonElement(CancellationIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid? CancellationId { get; set; }

        [DataMember]
        [BsonElement(ChannelFieldName)]
        [BsonIgnoreIfDefault]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonElement(ItineraryIdFieldName)]
        [BsonIgnoreIfDefault]
        public Guid ItineraryId { get; set; }

        [DataMember]
        [BsonElement(DebuggingFieldName)]
        [BsonIgnoreIfDefault]
        public bool Debugging { get; set; }
    }
}
