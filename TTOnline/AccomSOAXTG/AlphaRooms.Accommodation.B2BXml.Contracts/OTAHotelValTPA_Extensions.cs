﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Contracts
{
    [Serializable]
    public class OTAHotelValTPA_Extensions
    {
        public string UniqueID { get; set; }
    }
}
