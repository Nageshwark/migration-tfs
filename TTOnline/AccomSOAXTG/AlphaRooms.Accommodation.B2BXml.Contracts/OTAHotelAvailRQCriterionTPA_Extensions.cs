﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Contracts
{
    [Serializable]
    public class OTAHotelAvailRQCriterionTPA_Extensions
    {
        public string ShowDirectPayment { get; set; }
        public string ShowPostPayment { get; set; }
    }
}
