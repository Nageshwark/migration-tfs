﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Contracts
{
    [Serializable]
    public class OTAHotelAvailRSRoomRatesRoomRateTPA_Extension
    {
        public string IsBidingRate { get; set; }
        public string IsDirectPayment { get; set; }
        public string IsOpaqueRate { get; set; }
        public string IsRefundableRate { get; set; }
        public string IsSpecialRate { get; set; }
    }
}