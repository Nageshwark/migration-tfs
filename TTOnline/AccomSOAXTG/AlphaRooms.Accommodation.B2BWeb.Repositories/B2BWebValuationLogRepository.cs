﻿using AlphaRooms.Accommodation.B2BWeb.DbContexts;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Repositories
{
    public class B2BWebValuationLogRepository : IB2BWebValuationLogRepository
    {
        private readonly IDbContextActivator<B2BWebLogDbContext> dbLogsContextActivator;

        public B2BWebValuationLogRepository(IDbContextActivator<B2BWebLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2BWebValuationLog[] b2BWebValuationLog)
        {
            using (B2BWebLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2BWebValuationLogs.AddRange(b2BWebValuationLog);
                await context.SaveChangesAsync();
            }
        }
    }
}
