﻿using AlphaRooms.Accommodation.B2BWeb.DbContexts;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Repositories
{
    public class B2BWebAvailabilityLogRepository : IB2BWebAvailabilityLogRepository
    {
        private readonly IDbContextActivator<B2BWebLogDbContext> dbLogsContextActivator;

        public B2BWebAvailabilityLogRepository(IDbContextActivator<B2BWebLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveAsync(B2BWebAvailabilityLog b2BWebAvailabilityLog)
        {
            using (B2BWebLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                await context.B2BWebAvailabilityLogInsertAsync(
                    b2BWebAvailabilityLog.HostName, b2BWebAvailabilityLog.AvailabilityId, b2BWebAvailabilityLog.AvailabilityDate
                    , (byte)b2BWebAvailabilityLog.AvailabilityType, (byte?)b2BWebAvailabilityLog.Status, b2BWebAvailabilityLog.Request, b2BWebAvailabilityLog.ResultsCount
                    , b2BWebAvailabilityLog.EstablishmentResultsCount, b2BWebAvailabilityLog.Providers, b2BWebAvailabilityLog.ProviderSearchTypes, b2BWebAvailabilityLog.ProviderResultsCount
                    , b2BWebAvailabilityLog.ProviderEstablishmentResultsCount, b2BWebAvailabilityLog.ProviderTimeTaken, b2BWebAvailabilityLog.ProviderExceptions
                    , b2BWebAvailabilityLog.PostProcessTimeTaken, b2BWebAvailabilityLog.TimeTaken, b2BWebAvailabilityLog.Exceptions);
            }
        }
    }
}
