﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using AlphaRooms.Accommodation.B2BWeb.DbContexts;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.B2BWeb.Repositories
{
    public class B2BWebBookingLogRepository : IB2BWebBookingLogRepository
    {
        private readonly IDbContextActivator<B2BWebLogDbContext> dbLogsContextActivator;

        public B2BWebBookingLogRepository(IDbContextActivator<B2BWebLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2BWebBookingLog[] b2BWebBookingLog)
        {
            using (B2BWebLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2BWebBookingLogs.AddRange(b2BWebBookingLog);
                await context.SaveChangesAsync();
            }
        }
    }
}
