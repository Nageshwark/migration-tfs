﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BlockedDestinationService : IBlockedDestinationService
    {
        private readonly IBlockedDestinationRepository blockedDestinationRepository;

        public BlockedDestinationService(IBlockedDestinationRepository blockedDestinationRepository)
        {
            this.blockedDestinationRepository = blockedDestinationRepository;
        }

        public async Task<BlockedDestination[]> GetByChannelAndDestinationIdHierarchicalAsync(Channel channel, Guid destinationId)
        {
            return await this.blockedDestinationRepository.GetByChannelAndDestinationIdHierarchicalAsync(channel, destinationId);
        }

        public bool IsDestinationBlocked(BlockedDestination[] blockedDestinations, Channel channel, AccommodationProvider provider, Guid destinationId)
        {
            return blockedDestinations.Any(i => (i.Channel == null || i.Channel == channel) && (i.ProviderId == null || i.ProviderId == provider.Id)
                && i.DestinationId == destinationId);
        }
    }
}
