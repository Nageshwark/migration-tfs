﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class RoomSwitchingService : IRoomSwitchingService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IRoomDescriptionComparer roomDescriptionComparer;
        private readonly IPriceCalculatorService priceCalculatorService;

        public RoomSwitchingService(IAccommodationConfigurationManager configurationManager, IRoomDescriptionComparer roomDescriptionComparer, IPriceCalculatorService priceCalculatorService)
        {
            this.configurationManager = configurationManager;
            this.roomDescriptionComparer = roomDescriptionComparer;
            this.priceCalculatorService = priceCalculatorService;
        }

        public IList<AccommodationAvailabilityResult> ApplyAvailabilityRoomSwitching(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            if (!this.configurationManager.AccommodationRoomSwitchingIsEnabled) return results;
            results.Where(i => i.ProviderResult.PaymentModel == PaymentModel.PostPayment).GroupBy(i => i, new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                i.EstablishmentId == j.EstablishmentId && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType && roomDescriptionComparer.EqualsRoomDescription(i, j)))
                .AsParallel().Where(i => i.Count() > 1).Select(i => i.OrderBy(j => priceCalculatorService.CalculateRoomPrice(j)).ToArray()).ForAll(i => ProcessRoomSwitching(i));
            return results;
        }

        private void ProcessRoomSwitching(AccommodationAvailabilityResult[] groupedResults)
        {
            var cheapestOpaqueResult = groupedResults.FirstOrDefault(i => i.ProviderResult.IsOpaqueRate);
            if (cheapestOpaqueResult == null) return;

            // TODO room switching
        }
    }
}
