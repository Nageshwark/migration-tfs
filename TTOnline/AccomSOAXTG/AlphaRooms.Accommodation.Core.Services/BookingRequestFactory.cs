﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingRequestFactory : IBookingRequestFactory
    {
        static BookingRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationBookingRequest, AccommodationProviderBookingRequest>()
                    .ForMember(i => i.ValuatedRooms, i => i.Ignore())
                    .ForMember(i => i.Customer, i => i.Ignore());
                Mapper.CreateMap<AccommodationBookingRequestCustomer, AccommodationProviderBookingRequestCustomer>()
                    .ForMember(i => i.Type, i => i.Ignore());
                Mapper.CreateMap<AccommodationBookingRequestRoom, AccommodationProviderBookingRequestRoom>()
                    .ForMember(i => i.Guests, i => i.Ignore());
                Mapper.CreateMap<AccommodationBookingRequestRoomPaymentDetails, AccommodationProviderBookingRequestRoomPaymentDetails>();
                Mapper.CreateMap<AccommodationBookingRequestRoomGuest, AccommodationProviderBookingRequestRoomGuest>()
                    .ForMember(i => i.Type, i => i.Ignore())
                    .ForMember(i => i.Title, i => i.Ignore());
                Mapper.CreateMap<AccommodationBookingRequestRoomSpecialRequest, AccommodationProviderBookingRequestRoomSpecialRequest>();
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAgeRuleService ageRuleService;

        public BookingRequestFactory(IAccommodationConfigurationManager configurationManager, IAgeRuleService ageRuleService)
        {
            this.configurationManager = configurationManager;
            this.ageRuleService = ageRuleService;
        }

        public AccommodationProviderBookingRequest CreateProvideBookingRequest(AccommodationBookingRequest bookingRequest, AccommodationProvider provider, byte processId
            , BookingRoomValuationResult[] bookingRequestRooms)
        {
            var request = Mapper.Map<AccommodationBookingRequest, AccommodationProviderBookingRequest>(bookingRequest);
            request.Provider = provider;
            request.ProcessId = processId;
            request.AvailabilityId = bookingRequestRooms[0].ValuationResult.AvailabilityId;
            request.Customer = CreateProviderCustomer(bookingRequest.Customer, provider);
            request.ValuatedRooms = bookingRequestRooms.Select(i => CreateAccommodationProviderBookingRequestRoom(bookingRequest, i, processId,i.ValuatedRoom)).ToArray();
            return request;
        }

        private AccommodationProviderBookingRequestCustomer CreateProviderCustomer(AccommodationBookingRequestCustomer customer, AccommodationProvider provider)
        {
            var providerCustomer = Mapper.Map<AccommodationBookingRequestCustomer, AccommodationProviderBookingRequestCustomer>(customer);
            providerCustomer.Title = customer.TitleString.ToEnumOrDefault<GuestTitle>(GuestTitle.Unknown);
            ageRuleService.ApplyBookingCustomerTypeRule(provider, providerCustomer);
            return providerCustomer;
        }

        private AccommodationProviderBookingRequestRoom CreateAccommodationProviderBookingRequestRoom(AccommodationBookingRequest bookingRequest
            , BookingRoomValuationResult bookingRequestRoom, byte processId,AccommodationBookingRequestRoom accommodationBookingRequestRoom)
        {
            var guests = new AccommodationProviderAvailabilityRequestGuestCollection(bookingRequestRoom.ValuatedRoomDetails.GuestAges
                .Select(i => CreateProviderAvailabilityRequestGuest(bookingRequestRoom.ValuationResultRoom.Provider, i)).ToArray());
            bookingRequestRoom.ValuationResultRoom.ProviderResult.RoomNumber = bookingRequestRoom.ProviderRoomNumber;
            var providerRequestRoom = Mapper.Map<AccommodationBookingRequestRoom, AccommodationProviderBookingRequestRoom>(bookingRequestRoom.ValuatedRoom);
            providerRequestRoom.SpecialRequests.AdjoiningRooms =
                accommodationBookingRequestRoom?.SpecialRequests?.AdjoiningRooms ?? false;
            providerRequestRoom.SpecialRequests.CotRequired =
                accommodationBookingRequestRoom?.SpecialRequests?.CotRequired ?? false;
            providerRequestRoom.SpecialRequests.DisabledAccess =
                accommodationBookingRequestRoom?.SpecialRequests?.DisabledAccess ?? false;
            providerRequestRoom.SpecialRequests.LateArrival =
                accommodationBookingRequestRoom?.SpecialRequests?.LateArrival ?? false;
            providerRequestRoom.SpecialRequests.NonSmoking =
                accommodationBookingRequestRoom?.SpecialRequests?.NonSmoking ?? false;
            providerRequestRoom.SpecialRequests.OtherRequests =
                accommodationBookingRequestRoom?.SpecialRequests?.OtherRequests ?? String.Empty;
            providerRequestRoom.SpecialRequests.SeaViews = accommodationBookingRequestRoom?.SpecialRequests?.SeaViews ??
                                                           false;
            providerRequestRoom.RoomNumber = bookingRequestRoom.RoomNumber; //bookingRequestRoom.ProviderRoomNumber;
            providerRequestRoom.AvailabilityRequest = CreateProviderAvailabilityRequest(bookingRequest, bookingRequestRoom, processId, guests);
            providerRequestRoom.ValuationResult = bookingRequestRoom.ValuationResultRoom.ProviderResult;
            providerRequestRoom.Guests = new AccommodationProviderBookingRequestRoomGuestCollection(bookingRequestRoom.ValuatedRoom.Guests
                .Select(i => CreateAccommodationProviderBookingRequestRoomGuest(i, bookingRequestRoom.ValuationResultRoom.Provider)).ToArray());
            return providerRequestRoom;
        }
        
        private AccommodationProviderBookingRequestRoomGuest CreateAccommodationProviderBookingRequestRoomGuest(AccommodationBookingRequestRoomGuest guest, AccommodationProvider provider)
        {
            var providerGuest = Mapper.Map<AccommodationBookingRequestRoomGuest, AccommodationProviderBookingRequestRoomGuest>(guest);
            providerGuest.Title = providerGuest.TitleString.ToEnumOrDefault<GuestTitle>(GuestTitle.Unknown);
            ageRuleService.ApplyBookingGuestTypeRule(provider, providerGuest);
            return providerGuest;
        }

        private AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationBookingRequest bookingRequest, BookingRoomValuationResult bookingRequestRoom
            , byte processId, AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return new AccommodationProviderAvailabilityRequest()
            {
                Channel = bookingRequest.ChannelInfo.Channel
                , AvailabilityId = bookingRequestRoom.ValuationResult.AvailabilityId
                , ChannelInfo = bookingRequest.ChannelInfo
                , CheckInDate = bookingRequestRoom.ValuationResultRoom.ProviderResult.CheckInDate
                , CheckOutDate = bookingRequestRoom.ValuationResultRoom.ProviderResult.CheckOutDate
                , Provider = bookingRequestRoom.ValuationResultRoom.Provider
                , SearchType = SearchType.HotelOnly
                , ProcessId = processId
                , DestinationCodes = new[] { bookingRequestRoom.ValuationResultRoom.ProviderResult.DestinationEdiCode }
                , EstablishmentCodes = new[] { bookingRequestRoom.ValuationResultRoom.ProviderResult.EstablishmentEdiCode }
                , Rooms = new[] { CreateAccommodationProviderAvailabilityRequestRoom(bookingRequestRoom, guests) }
            };
        }

        private AccommodationProviderAvailabilityRequestRoom CreateAccommodationProviderAvailabilityRequestRoom(BookingRoomValuationResult bookingRequestRoom
            , AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = bookingRequestRoom.ValuationResultRoom.AvailabilityRequestId
                , RoomNumber = bookingRequestRoom.ProviderRoomNumber
                , Guests = guests
            };
        }

        private AccommodationProviderAvailabilityRequestGuest CreateProviderAvailabilityRequestGuest(AccommodationProvider provider, byte age)
        {
            var providerGuest = new AccommodationProviderAvailabilityRequestGuest() { Age = age };
            ageRuleService.ApplyAvailabilityGuestTypeRule(provider, providerGuest);
            return providerGuest;
        }
    }
}
