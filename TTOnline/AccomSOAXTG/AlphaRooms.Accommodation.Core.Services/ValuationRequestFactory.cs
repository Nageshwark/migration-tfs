﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationRequestFactory : IValuationRequestFactory
    {
        static ValuationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationValuationRequest, AccommodationProviderValuationRequest>()
                    .ForMember(i => i.SelectedRooms, i => i.Ignore());
            }
        }

        private readonly IAgeRuleService ageRuleService;

        public ValuationRequestFactory(IAgeRuleService ageRuleService)
        {
            this.ageRuleService = ageRuleService;
        }

        public AccommodationProviderValuationRequest CreateProvideValuationRequest(AccommodationValuationRequest valuationRequest, AccommodationProvider provider, byte processId
            , ValuationRoomIdAvailabilityResult[] valuationRequestRooms)
        {
            var request = Mapper.Map<AccommodationValuationRequest, AccommodationProviderValuationRequest>(valuationRequest);
            request.Provider = provider;
            request.ProcessId = processId;
            request.SelectedRooms = valuationRequestRooms.Select(i => CreateAccommodationProviderValuationRequestRoom(valuationRequest, i, processId)).ToArray();
            return request;
        }

        private AccommodationProviderValuationRequestRoom CreateAccommodationProviderValuationRequestRoom(AccommodationValuationRequest valuationRequest
            , ValuationRoomIdAvailabilityResult valuationRequestRoom, byte processId)
        {
            var guests = new AccommodationProviderAvailabilityRequestGuestCollection(valuationRequestRoom.SelectedRoomDetails.GuestAges
                .Select(i => CreateProviderAvailabilityRequestGuest(valuationRequestRoom.Provider, i)).ToArray());
            valuationRequestRoom.AvailabilityResult.ProviderResult.RoomNumber = valuationRequestRoom.ProviderRoomNumber;
            return new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = CreateProviderAvailabilityRequest(valuationRequest, valuationRequestRoom, processId, guests)
                , RoomNumber = valuationRequestRoom.ProviderRoomNumber
                , Guests = guests
                , AvailabilityResult = valuationRequestRoom.AvailabilityResult.ProviderResult
            };
        }
        
        private AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationValuationRequest valuationRequest, ValuationRoomIdAvailabilityResult valuationRequestRoom
            , byte processId, AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return new AccommodationProviderAvailabilityRequest()
            {
                Channel = valuationRequest.ChannelInfo.Channel
                , AvailabilityId = valuationRequest.AvailabilityId
                , ChannelInfo = valuationRequest.ChannelInfo
                , CheckInDate = valuationRequestRoom.AvailabilityResult.ProviderResult.CheckInDate
                , CheckOutDate = valuationRequestRoom.AvailabilityResult.ProviderResult.CheckOutDate
                , Provider = valuationRequestRoom.AvailabilityResult.Provider
                , SearchType = SearchType.HotelOnly
                , ProcessId = processId
                , DestinationCodes = new[] { valuationRequestRoom.AvailabilityResult.ProviderResult.DestinationEdiCode }
                , EstablishmentCodes = new[] { valuationRequestRoom.AvailabilityResult.ProviderResult.EstablishmentEdiCode }
                , Rooms = new[] { CreateAccommodationProviderAvailabilityRequestRoom(valuationRequestRoom, guests) }
            };
        }
        
        private AccommodationProviderAvailabilityRequestRoom CreateAccommodationProviderAvailabilityRequestRoom(ValuationRoomIdAvailabilityResult availabilityRequestRoom
            , AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = availabilityRequestRoom.AvailabilityResult.AvailabilityRequestId
                , RoomNumber = availabilityRequestRoom.ProviderRoomNumber
                , Guests = guests
            };
        }

        private AccommodationProviderAvailabilityRequestGuest CreateProviderAvailabilityRequestGuest(AccommodationProvider provider, byte age)
        {
            var providerGuest = new AccommodationProviderAvailabilityRequestGuest() { Age = age };
            ageRuleService.ApplyAvailabilityGuestTypeRule(provider, providerGuest);
            return providerGuest;
        }
    }
}
