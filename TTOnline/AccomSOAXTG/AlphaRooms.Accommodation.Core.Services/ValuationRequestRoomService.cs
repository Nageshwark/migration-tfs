﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.Utilities;
    using System.Linq;
    using System.Threading.Tasks;

    public class ValuationRequestRoomService : IValuationRequestRoomService
    {
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IRoomKeyGenerator roomKeyGenerator;
        private readonly IAgeRuleService ageRuleService;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;
        private readonly IAvailabilityResultDuplicationService duplicationService;

        public ValuationRequestRoomService(
            IAvailabilityResultRoomIdGenerator roomIdGenerator,
            IRoomKeyGenerator roomKeyGenerator,
            IAgeRuleService ageRuleService,
            IAccommodationProviderRepository providerRepository,
            IAccommodationSupplierRepository supplierRepository,
            IAvailabilityResultDuplicationService duplicationService)
        {
            this.roomIdGenerator = roomIdGenerator;
            this.roomKeyGenerator = roomKeyGenerator;
            this.ageRuleService = ageRuleService;
            this.providerRepository = providerRepository;
            this.supplierRepository = supplierRepository;
            this.duplicationService = duplicationService;
        }

        public async Task<ValuationRoomIdAvailabilityResult[]> CreateValuationRequestRoomsAsync(AccommodationValuationRequest valuationRequest)
        {
            var valuationRequestRooms = valuationRequest.SelectedRoomIds
                .Select(this.CreateValuationRoomIdAvailabilityResult)
                .OrderBy(i => i.SelectedRoomDetails.RoomNumber)
                .ToArray();

            var providers = await providerRepository.GetByIdsAsync(valuationRequestRooms.Select(i => i.SelectedRoomDetails.ProviderId)
                .Distinct()
                .ToArray());

            var suppliers = await supplierRepository.GetByIdsAsync(valuationRequestRooms.Select(i => i.SelectedRoomDetails.SupplierId)
                .Distinct()
                .ToArray());

            Parallel.ForEach(valuationRequestRooms, i =>
            {
                i.RoomNumber = i.SelectedRoomDetails.RoomNumber;
                i.Provider = providers.First(j => j.Id == i.SelectedRoomDetails.ProviderId);
                i.Supplier = suppliers.First(j => j.Id == i.SelectedRoomDetails.SupplierId);
                roomKeyGenerator.ApplyValuationRoomKey(i);
            });

            return valuationRequestRooms;
        }

        private ValuationRoomIdAvailabilityResult CreateValuationRoomIdAvailabilityResult(string roomId)
        {
            return new ValuationRoomIdAvailabilityResult()
            {
                RoomId = roomId,
                SelectedRoomDetails = this.roomIdGenerator.Parse(roomId)
            };
        }

        public void SetAvailabilityResults(ValuationRoomIdAvailabilityResult[] valuationRequestRooms, AccommodationAvailabilityResult[] availabilityResults)
        {
            var valuationRooms = valuationRequestRooms.GroupBy(i => i.SelectedRoomDetails.RoomId);

            Parallel.ForEach(valuationRooms, i =>
            {
                this.GetAvailabilityResult(i.ToArray(), availabilityResults);
            });
        }

        private void GetAvailabilityResult(ValuationRoomIdAvailabilityResult[] valuationRequestRooms, AccommodationAvailabilityResult[] availabilityResults)
        {
            var firstValuationRequestRooms = valuationRequestRooms[0];
            var availabilityResult = availabilityResults.FirstOrDefault(j => j.Id == firstValuationRequestRooms.SelectedRoomDetails.RoomId);

            if (availabilityResult == null)
            {
                return;
            }

            firstValuationRequestRooms.AvailabilityResult = availabilityResult;
            firstValuationRequestRooms.AvailabilityResult.RoomNumber = firstValuationRequestRooms.RoomNumber;

            if (valuationRequestRooms.Length == 1)
            {
                return;
            }

            var duplicatedValuationRequests = valuationRequestRooms
                .Skip(1)
                .Where(i => i.Provider.ContractDependencyMode != ContractDependencyMode.UniqueContracts && !i.Supplier.IsUniqueContractRequired)
                .ToArray();

            var clonedAvailabilityResults = this.duplicationService.CopyRoomResultsToRoomNumber(
                availabilityResult,
                duplicatedValuationRequests.Select(i => i.RoomNumber).ToArray());

            foreach (var valuationRequest in duplicatedValuationRequests)
            {
                valuationRequest.AvailabilityResult = clonedAvailabilityResults.FirstOrDefault(j => j.RoomNumber == valuationRequest.RoomNumber);
            }
        }
    }
}
