﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Globalization;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationCancellationPolicyServiceV1 : IValuationCancellationPolicyService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICancellationPolicyRepository cancellationPolicyRepository;
        private IDepositSchemeV1Service depositSchemeService;


        public ValuationCancellationPolicyServiceV1(IAccommodationConfigurationManager configurationManager, ICancellationPolicyRepository cancellationPolicyRepository, IDepositSchemeV1Service depositSchemeService)
        {
            this.configurationManager = configurationManager;
            this.cancellationPolicyRepository = cancellationPolicyRepository;
            this.depositSchemeService = depositSchemeService;
        }

        public async Task<AccommodationValuationResult> ApplyCancellationPolicyAsync(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            if (valuationRequest.Agent != null)
            {
                var cancellationTermsB2B = await CreateDefaultCancellationTermsAsync(valuationRequest.ChannelInfo, true);
                var nonRefundableTermsB2B = await CreateNonRefundableCancellationTermsAsync(true);
                result.Rooms.ParallelForEach(i => i.CancellationPolicies = CreateCancellationPolicyB2B(valuationRequest.ChannelInfo, cancellationTermsB2B, nonRefundableTermsB2B, i));
                return result;
            }
            var cancellationTerms = await CreateDefaultCancellationTermsAsync(valuationRequest.ChannelInfo, false);
            var nonRefundableTerms = await CreateNonRefundableCancellationTermsAsync(false);
            var travelGatePolicy = await this.cancellationPolicyRepository.GetByProviderIdAndProviderPolicyIdAsync(configurationManager.TravelGateProvider[0], 1, false);
            result.Rooms.ParallelForEach(i => i.CancellationPolicies = CreateCancellationPolicyB2C(valuationRequest.ChannelInfo, cancellationTerms, travelGatePolicy.Message, nonRefundableTerms, i));
            return result;
        }

        private async Task<string[]> CreateDefaultCancellationTermsAsync(ChannelInfo channelInfo, bool isB2B)
        {
            var cancellationFee = GetCancellationFee(channelInfo);
            var noRefundCancellationDaysBeforeDeparture = configurationManager.AccommodationNoRefundCancellationDaysBeforeDeparture;
            var defaultPolicy = await this.cancellationPolicyRepository.GetDefaultCancellationPolicyAsync(isB2B);
            return new[] { string.Format(defaultPolicy.Message, noRefundCancellationDaysBeforeDeparture, channelInfo.CurrencySymbol + cancellationFee.Amount.ToString("0.00"), channelInfo.CurrencySymbol) };
        }

        private async Task<string[]> CreateNonRefundableCancellationTermsAsync(bool isB2B)
        {
            var nonRefundablePolicy = await this.cancellationPolicyRepository.GetNonRefundableCancellationPolicyAsync(isB2B);
            return new[] { nonRefundablePolicy.Message };
        }

        private Money GetCancellationFee(ChannelInfo channelInfo)
        {
            decimal amount;
            switch (channelInfo.Channel)
            {
                case Channel.AlphaRoomsUK:
                case Channel.BetaBedsUK:
                case Channel.TeletextHolidaysUK:
                    amount = configurationManager.AccommodationCancellationFee_UK;
                    break;
                case Channel.AlphaRoomsIE:
                case Channel.BetaBedsIE:
                case Channel.TeletextIE:
                    amount = configurationManager.AccommodationCancellationFee_IE;
                    break;
                case Channel.AlphaRoomsUS:
                case Channel.BetaBedsUS:
                    amount = configurationManager.AccommodationCancellationFee_US;
                    break;
                default:
                    throw new Exception("Unexpected channel " + channelInfo.Channel);
            }
            return new Money(amount, channelInfo.CurrencyCode);
        }

        private string[] CreateCancellationPolicyB2C(ChannelInfo channelInfo, string[] defaultPolicy, string travelGatePolicy, string[] nonRefundableTerms, AccommodationValuationResultRoom result)
        {
            if (result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect && !result.ProviderResult.CancellationPolicy.All(string.IsNullOrEmpty))
            { return result.ProviderResult.CancellationPolicy; }

            if (result.ProviderResult.IsNonRefundable)
            {
                if (result.ProviderId == configurationManager.TravelGateProvider[0])
                {
                    return nonRefundableTerms;
                }
                return (!ArrayModule.IsNullOrEmpty(result.ProviderResult.CancellationPolicy) ? result.ProviderResult.CancellationPolicy : nonRefundableTerms);
            }

            if (result.Supplier.IsSupplierCancellationPolicy && !result.ProviderResult.CancellationPolicy.All(string.IsNullOrEmpty))
            {
                string buildPolicy = null;
                if (result.ProviderId == configurationManager.TravelGateProvider[0])
                {
                    foreach (string policy in result.ProviderResult.CancellationPolicy)
                    {
                        buildPolicy = buildPolicy + policy + "<br>";
                    }
                    return new[] { string.Format(travelGatePolicy, channelInfo.CurrencySymbol, buildPolicy, channelInfo.CurrencySymbol) };
                }

                return result.ProviderResult.CancellationPolicy;
            }

            if (!this.depositSchemeService.IsFlexibleDepositEnabled())
            {
                //Show supplier terms if configured and there are any, otherwise, show ours.
                if (result.Supplier.IsSupplierCancellationPolicy && !result.ProviderResult.CancellationPolicy.All(string.IsNullOrEmpty))
                { return result.ProviderResult.CancellationPolicy; }
            }

            return defaultPolicy;
        }

        private string[] CreateCancellationPolicyB2B(ChannelInfo channelInfo, string[] defaultPolicy, string[] nonRefundableTerms, AccommodationValuationResultRoom result)
        {
            if (result.Supplier.IsSupplierCancellationPolicy) return result.ProviderResult.CancellationPolicy;
            if (result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect) return result.ProviderResult.CancellationPolicy;
            if (result.ProviderResult.IsNonRefundable) return (!ArrayModule.IsNullOrEmpty(result.ProviderResult.CancellationPolicy) ? result.ProviderResult.CancellationPolicy : nonRefundableTerms);
            return defaultPolicy;
        }

        private string[] CreateNonRefundableCancellationPolicy(CancellationPolicy nonRefundablePolicy)
        {
            return new[] { nonRefundablePolicy.Message };
        }
    }
}
