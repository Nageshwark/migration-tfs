﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Services.Exceptions;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Utilities;
using System.Collections.Concurrent;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingService : IBookingService
    {
        private readonly IValuationResultCaching valuationResultCaching;
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly IBookingLiveProcessParamService liveProcessService;
        private readonly IBookingRequestFactory requestFactory;
        private readonly IProviderBookingService providerBookingService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IBookingResultCaching resultCaching;
        private readonly IBookingResultMappingService resultMappingService;
        private readonly IBookingResultBuilderService resultBuilderService;
        private readonly IBookingRequestRoomService bookingRequestRoomService;
        private readonly IDiParameterContainer diParameterContainer;

        public BookingService(IValuationResultCaching valuationResultCaching, IAccommodationProviderRepository accommodationProviderRepository
            , IBookingLiveProcessParamService liveProcessService, IBookingRequestFactory requestFactory, IProviderBookingService providerBookingService
            , IAccommodationConfigurationManager configurationManager, IBookingResultCaching resultCaching, IBookingResultMappingService resultMappingService
            , IBookingResultBuilderService resultBuilderService, IBookingRequestRoomService bookingRequestRoomService, IDiParameterContainer diParameterContainer)
        {
            this.valuationResultCaching = valuationResultCaching;
            this.accommodationProviderRepository = accommodationProviderRepository;
            this.liveProcessService = liveProcessService;
            this.requestFactory = requestFactory;
            this.providerBookingService = providerBookingService;
            this.configurationManager = configurationManager;
            this.resultCaching = resultCaching;
            this.resultMappingService = resultMappingService;
            this.resultBuilderService = resultBuilderService;
            this.bookingRequestRoomService = bookingRequestRoomService;
            this.diParameterContainer = diParameterContainer;
        }

        public async Task<AccommodationBookingResponse> GetBookingResponseAsync(Guid bookingId)
        {
            return new AccommodationBookingResponse() { Result = await this.resultCaching.GetByBookingIdOrNullAsync(bookingId) };
        }

        public async Task<AccommodationBookingResponse> ProcessBookingAsync(AccommodationBookingRequest bookingRequest)
        {
            // create a list containing the valuated room details
            var bookingRequestRooms = bookingRequestRoomService.CreateBookingRequestRooms(bookingRequest);

            //  get valuated room results
            var valuationResult = await valuationResultCaching.GetByValuationIdOrNullAsync(bookingRequest.ValuationId);

            // identify valuation results for this room
            bookingRequestRoomService.SetValuationResults(bookingRequestRooms, valuationResult);

            //  create the necessary live process parameters based on the selected rooms and their providers
            var liveProcessParams = liveProcessService.CreateBookingLiveProcessParams(bookingRequest, bookingRequestRooms);

            // create tasks to perform booking
            var tasks = liveProcessParams.Select(i => Task.Run(async() => await ProcessBookingLiveProviderAsync(i))).ToArray();

            // process the tasks, wait for them to complete or the process to timeout
            var searchTask = Task.Run(async() => await Task.WhenAll(tasks));
            await Task.WhenAny(searchTask, Task.Delay(this.configurationManager.AccommodationValuationTimeout));
            
            // throw an exception if the process timed out
            if (searchTask.Status != TaskStatus.RanToCompletion && liveProcessParams.All(i => i.Response == null)) throw new CoreBookingTimeoutException(bookingRequest, this.configurationManager.AccommodationValuationTimeout);

            // verify booking results and generate a booking package
            var result = resultBuilderService.CreateBookingResult(bookingRequest, liveProcessParams);

            // save noncached results to the db
            await resultCaching.SaveAsync(result);

            return new AccommodationBookingResponse()
            {
                Result = result
                , ProcessResponseDetails = liveProcessParams.Select(i => new AccommodationBookingResponseProviderDetails()
                {
                    Provider = i.Provider
                    , BookingRequestRooms = i.BookingRequestRooms
                    , ProviderRequests = (i.Response != null ? i.Response.ProviderRequests : null)
                    , Results = (i.Response != null ? i.Response.Results : null)
                    , ResultsCount = (i.Response != null ? (int?)i.Response.Results.Count : null)
                    , TimeTaken = (i.Response != null ? (TimeSpan?)i.Response.TimeTaken : null)
                    , Exception = (i.Response != null ? i.Response.Exception : null)
                }).ToArray()
            };
        }

        private async Task ProcessBookingLiveProviderAsync(AccommodationBookingLiveProcessParams liveProcessParams)
        {
            AccommodationProviderBookingRequest providerRequest = null;
            var providerRequests = new ConcurrentQueue<AccommodationProviderRequest>();
            AccommodationProviderBookingResponse providerResponse = null;
            IList<AccommodationBookingResultRoom> mappedResults = null;
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            try
            {
                // create provider request
                providerRequest = requestFactory.CreateProvideBookingRequest(liveProcessParams.BookingRequest, liveProcessParams.Provider, liveProcessParams.ProcessId, liveProcessParams.BookingRequestRooms);

                // register provider logger parameter in the di container
                this.diParameterContainer.Register(providerRequest, providerRequests);

                // perform provider live search
                providerResponse = await providerBookingService.GetProviderBookingResponseAsync(providerRequest);

                // map results to core availability result 
                mappedResults = resultMappingService.MapFromProviderBookingResults(liveProcessParams, providerRequest, providerResponse.Results.ToArray());
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // unregister provider logger parameter in the di container
            if (providerRequest != null) this.diParameterContainer.Unregister(providerRequest);

            // return provider specific results list 
            liveProcessParams.Response = new BookingProcessResponse() 
            {
                Results = mappedResults ?? new AccommodationBookingResultRoom[0]
                , ProviderRequests = providerRequests.ToArray()
                , Exception = exception
                , TimeTaken = DateTime.Now - startDate
            };
        }
    }
}
