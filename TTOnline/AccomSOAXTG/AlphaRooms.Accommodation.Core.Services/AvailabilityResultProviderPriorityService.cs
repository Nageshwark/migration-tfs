﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using Contracts;
    using Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Responsible for setting the priority of the providers
    /// </summary>
    public class AvailabilityResultProviderPriorityService : IAvailabilityResultProviderPriorityService
    {
        private readonly IReadOnlyDictionary<string, int> providerPriorities;

        public AvailabilityResultProviderPriorityService()
        {
            // this is a hack this should be read from the database, but I am holding up the project so leave this dept to get the Consolidation working sooner
            this.providerPriorities = new Dictionary<string, int>
            {
                {"A", 2147483647},
                { "7", 500},
                { "12", 495},
                { "RU", 490},
                { "GP", 485},
                { "SG", 480},
                { "PX", 475},
                { "PF", 475},
                { "PR", 470},
                { "PM", 465},
                { "PO", 460},
                { "PD", 455},
                { "PA", 450},
                { "PJ", 445},
                { "PB", 440},
                { "PC", 435},
                { "PH", 430},
                { "PN", 425},
                { "PP", 420},
                { "PU", 415},
                { "HB", 410},
                { "CR", 405},
                { "HC", 400},
                { "MP", 395},
                { "CN", 390}
            };
        }

        /// <summary>
        /// Provides an int that represents the Priority of the provider that created this room, the higher the int the more priority
        /// </summary>
        /// <param name="room">room to find the provider priority for</param>
        /// <returns>the higher result the more priority</returns>
        public int GetPriority(AccommodationAvailabilityResult room)
        {
            if (room == null)
            {
                throw new ArgumentNullException(nameof(room));
            }

            if (room.ProviderResult == null)
            {
                throw new ArgumentException("ProviderResult is null", nameof(room));
            }

            string providerEdiCode = room.ProviderResult.SupplierEdiCode;

            int priority;
            this.providerPriorities.TryGetValue(providerEdiCode, out priority);
            return priority;
        }
    }
}