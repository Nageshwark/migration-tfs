﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Repositories;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.SOACommon.Services.Factories;
using AlphaRooms.Utilities;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AccommodationPromotionalDiscountService : PromotionalDiscountService, IAccommodationPromotionalDiscountService<IAccommodationResultRoom>
    {

        public AccommodationPromotionalDiscountService(IPromotionalDiscountRepository repository, IPromotionalDiscountFactory factory, ILogger logger)
            : base(repository, factory, logger)
        {

        }

        public void ProcessDiscounts(IEnumerable<IAccommodationResultRoom> results, string code, Channel channel)
        {
            PromotionalDiscount promo = null;

            if (ShouldApplyPromotions(code, channel, ref promo))
            {

                // Rule: Do not apply "per booking" promotions on availability
                if (results is IEnumerable<AccommodationAvailabilityResult> && promo.AccommodationPromotion?.ApplicationType == DiscountApplicationType.PerBooking) return;

                var rooms = results.ToArray();

                var applicableRooms = this.GetApplicableRooms(rooms, promo);

                if (applicableRooms == null || !applicableRooms.Any()) return;

                if (promo.AccommodationPromotion?.ApplicationType == DiscountApplicationType.PerBooking)
                {
                    var bookingAmount = applicableRooms.Sum(r => r.Price.Amount);

                    if (bookingAmount > (promo.Threshold ?? 0))
                    {
                        var percentagePerRoom = new decimal[applicableRooms.Length];

                        for (int i = 0; i < applicableRooms.Length; i++)
                        {
                            var room = applicableRooms[i];
                            var amountPct = (room.Price.Amount * 100 / bookingAmount) / 100;
                            var roomAmount = promo.Amount * amountPct;

                            percentagePerRoom[i] = Math.Round(roomAmount, 2);
                        }

                        var precisionLoss = promo.Amount - percentagePerRoom.Sum();

                        for (int i = 0; i < applicableRooms.Length; i++)
                        {
                            var room = applicableRooms[i];

                            var roomAmount = percentagePerRoom[i];
                            if (i == applicableRooms.Length - 1)
                            {
                                roomAmount += precisionLoss;
                            }

                            var discountResult = GetDiscount(new Money(bookingAmount, room.Price.CurrencyCode), roomAmount, promo);

                            ApplyDiscount(room, discountResult);
                        }
                    }

                }
                else
                {
                    applicableRooms.ParallelForEach(room =>
                    {
                        if (!IsAboveMinThreshold(promo, room.Price)) return;

                        var discountResult = GetDiscount(room.Price, promo.Amount, promo);

                        // Apply discount only if the Room price is greater than the discount. This is to avoid any -ve or 0 price complications.
                        if (room.Price.Amount > Math.Abs((discountResult.Discount.Amount)))
                        {
                            ApplyDiscount(room, discountResult);
                        }
                    });
                }
            }
        }

        private void ApplyDiscount(IAccommodationResultRoom room, PromotionalDiscountResult discountResult)
        {
            // Those three fields need to be updated always
            room.Price = new Money(Math.Round(room.Price.Amount + discountResult.Discount.Amount, 2), room.Price.CurrencyCode);
            room.PricePerNight = new Money(Math.Round(room.Price.Amount / ((int)(room.ProviderResult.CheckOutDate - room.ProviderResult.CheckInDate).TotalDays), 2), room.Price.CurrencyCode);
            room.PaymentToTake = room.Price;
            //// Round discount amount after applying to avoid rounding issues
            //discountResult.Discount = new Money(Math.Round(discountResult.Discount.Amount, 2), discountResult.Discount.CurrencyCode);
            room.DiscountPrice = discountResult;
        }

        private bool ShouldApplyPromotions(string code, Channel channel, ref PromotionalDiscount promo)
        {
            if (string.IsNullOrEmpty(code)) return false;

            var promoResponse = this.GetPromoCode(code, channel);

            if (promoResponse.Status != PromoCodeResponseStatus.Valid) return false;

            promo = promoResponse.PromotionalDiscount;

            return true;
        }

        private IAccommodationResultRoom[] GetApplicableRooms(IAccommodationResultRoom[] rooms, PromotionalDiscount promotionalDiscount)
        {

            if (rooms.Any())
            {
                var destination = rooms.First().DestinationId;
                var checkIn = rooms.First().ProviderResult.CheckInDate;
                var checkOut = rooms.First().ProviderResult.CheckOutDate;

                // Rule: Apply for specified destination only
                if (promotionalDiscount.Destination.HasValue && promotionalDiscount.Destination.Value != destination)
                {
                    return null;
                }

                // Rule: Apply for specified travel dates
                if (promotionalDiscount.CheckInDate.HasValue && promotionalDiscount.CheckOutDate.HasValue)
                {
                    if (!DateExtensions.DateRangeContainsDateRange(promotionalDiscount.CheckInDate.Value, promotionalDiscount.CheckOutDate.Value, checkIn, checkOut)) return null;
                }

                // Get the rooms from the establishments to which the promo code applies
                if (promotionalDiscount.AccommodationPromotion.Establishment != null)
                {
                    rooms = rooms.Where(r => r.EstablishmentId == promotionalDiscount.AccommodationPromotion.Establishment.Value).ToArray();
                }

                // Rule: Do not apply to binding rates
                // Rule: Do not apply to payment model = PayDirect
                rooms = rooms.Where(r => !r.ProviderResult.IsBindingRate && (r.ProviderResult.PaymentModel == PaymentModel.PostPayment || r.ProviderResult.PaymentModel == PaymentModel.PayWithVirtualCard)).ToArray();
            }

            return rooms;
        }
    }
}
