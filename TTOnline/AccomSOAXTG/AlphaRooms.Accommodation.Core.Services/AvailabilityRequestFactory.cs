﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityRequestFactory : IAvailabilityRequestFactory
    {
        static AvailabilityRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationAvailabilityRequest, AccommodationProviderAvailabilityRequest>()
                    .ForMember(i => i.Rooms, i => i.Ignore());
                Mapper.CreateMap<AccommodationAvailabilityRequestRoom, AccommodationProviderAvailabilityRequestRoom>()
                    .ForMember(i => i.Guests, i => i.Ignore());
                Mapper.CreateMap<AccommodationAvailabilityRequestRoomGuest, AccommodationProviderAvailabilityRequestGuest>()
                    .ForMember(i => i.Type, i => i.Ignore());
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAgeRuleService ageRuleService;

        public AvailabilityRequestFactory(IAccommodationConfigurationManager configurationManager, IAgeRuleService ageRuleService)
        {
            this.configurationManager = configurationManager;
            this.ageRuleService = ageRuleService;
        }

        public AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationAvailabilityRequest availabilityRequest, AccommodationProvider provider, byte processId
            , string[] destinationCodes, string[] establishmentCodes, AvailabilityRoomAvailabilityKey[] availabilityRoomAvailabilityKey)
        {
            var request = Mapper.Map<AccommodationAvailabilityRequest, AccommodationProviderAvailabilityRequest>(availabilityRequest);
            request.Channel = availabilityRequest.ChannelInfo.Channel;
            request.ProcessId = processId;
            request.Provider = provider;
            request.DestinationCodes = destinationCodes;
            request.EstablishmentCodes = establishmentCodes;
            request.Rooms = availabilityRoomAvailabilityKey.Select((i, j) => CreateProviderAvailabilityRequestRoom(provider, i, j)).ToArray();
            return request;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderAvailabilityRequestRoom(AccommodationProvider provider, AvailabilityRoomAvailabilityKey availabilityRoomAvailabilityKey, int index)
        {
            var providerRequestRoom = Mapper.Map<AccommodationAvailabilityRequestRoom, AccommodationProviderAvailabilityRequestRoom>(availabilityRoomAvailabilityKey.Room);
            providerRequestRoom.AvailabilityRequestId = availabilityRoomAvailabilityKey.AvailabilityRequestId;
            providerRequestRoom.RoomNumber = availabilityRoomAvailabilityKey.ProviderRoomNumber;
            providerRequestRoom.Guests = new AccommodationProviderAvailabilityRequestGuestCollection(availabilityRoomAvailabilityKey.Room.Guests.Select(i => CreateRoomSearchOccupancyGuest(provider, i)).ToArray());
            return providerRequestRoom;
        }

        private AccommodationProviderAvailabilityRequestGuest CreateRoomSearchOccupancyGuest(AccommodationProvider provider, AccommodationAvailabilityRequestRoomGuest guest)
        {
            var providerRoomGuest = Mapper.Map<AccommodationAvailabilityRequestRoomGuest, AccommodationProviderAvailabilityRequestGuest>(guest);
            ageRuleService.ApplyAvailabilityGuestTypeRule(provider, providerRoomGuest);
            return providerRoomGuest;
        }

        public AccommodationProviderAvailabilityRequest CreateMetaProviderAvailabilityRequest(AccommodationAvailabilityRequest availabilityRequest, AccommodationProvider provider, byte processId)
        {
            var request = Mapper.Map<AccommodationAvailabilityRequest, AccommodationProviderAvailabilityRequest>(availabilityRequest);
            request.Channel = availabilityRequest.ChannelInfo.Channel;
            request.Provider = provider;
            request.ProcessId = processId;
            return request;
        }
    }
}
