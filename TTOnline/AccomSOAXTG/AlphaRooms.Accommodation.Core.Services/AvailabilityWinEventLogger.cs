﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Utilities;
    using Ninject.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AvailabilityWinEventLogger : IAvailabilityWinEventLogger
    {
        private readonly ILogger logger;

        public AvailabilityWinEventLogger(ILogger logger)
        {
            this.logger = logger;
        }

        public void Log(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityProcessParamsBase> processParams)
        {
            var liveProviderResponses = processParams.Where(i => i is AccommodationAvailabilityLiveProcessParams && (i.Response == null || i.Response.Exception != null))
                .Cast<AccommodationAvailabilityLiveProcessParams>().GroupBy(i => i.Provider.Name);
            foreach (var providerResponses in liveProviderResponses)
            {
                StringBuilder builder = new StringBuilder("Accommodation availability live provider error occurred: ");
                builder.Append(providerResponses.Key);
                builder.Append("\r\n");

                foreach (var response in providerResponses)
                {
                    if (response.Response != null && response.Response.Exception != null)
                    {
                        builder.Append(response.Response.Exception.GetDetailedMessageWithInnerExceptions());
                    }
                    else
                    {
                        builder.Append("Live process didn't finish running");
                    }
                    builder.Append("\r\n");
                }
                this.logger.Error(builder.ToString());
            }

            var cachedProviderResponses = processParams.Where(i => i is AccommodationAvailabilityCachedProcessParams && (i.Response == null || i.Response.Exception != null))
                .Cast<AccommodationAvailabilityCachedProcessParams>();
            foreach (var providerResponses in cachedProviderResponses)
            {
                StringBuilder builder = new StringBuilder("Accommodation availability cached error occurred: ");
                builder.Append(providerResponses.Providers.ToString(','));
                builder.Append("\r\n");

                if (providerResponses.Response != null && providerResponses.Response.Exception != null)
                {
                    builder.Append(providerResponses.Response.Exception.GetDetailedMessageWithInnerExceptions());
                }
                else
                {
                    builder.Append("Cache process didn't finish running");
                }
                builder.Append("\r\n");

                this.logger.Error(builder.ToString());
            }
        }

        public void LogWarn(string message)
        {
            this.logger.Warn(message);
        }
    }
}
