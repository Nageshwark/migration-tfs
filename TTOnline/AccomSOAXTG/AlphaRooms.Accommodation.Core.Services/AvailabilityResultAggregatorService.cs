﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Linq;

    public class AvailabilityResultAggregatorService : IAvailabilityResultAggregatorService
    {
        private readonly IAvailabilityResultDuplicationService resultDuplicationService;

        public AvailabilityResultAggregatorService(IAvailabilityResultDuplicationService resultDuplicationService)
        {
            this.resultDuplicationService = resultDuplicationService;
        }

        public IList<AccommodationAvailabilityResult> AggregateRooms(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, IList<AccommodationAvailabilityResult> results)
        {
            results.ParallelForEach(i =>
            {
                var roomAvailabilityKey = roomAvailabilityKeys.First(j => j.AvailabilityRequestId == i.AvailabilityRequestId);
                i.RoomNumber = roomAvailabilityKey.Room.RoomNumber;
                i.AdultsCount = roomAvailabilityKey.AdultsCount;
                i.ChildrenCount = roomAvailabilityKey.ChildrenCount;
                i.InfantsCount = roomAvailabilityKey.InfantsCount;
            });
            if (roomAvailabilityKeys.Any(i => i.DuplicatedRequestRoom != null))
            {
                var duplicatedRooms = roomAvailabilityKeys.Where(i => i.DuplicatedRequestRoom != null).GroupBy(i => i.DuplicatedRequestRoom);
                var clonedResults = duplicatedRooms.GroupJoin(results, i => new { i.Key.Provider, i.Key.Room.RoomNumber }, i => new { i.Provider, i.RoomNumber }, (i, j) => 
                {
                    var toRooms = i.Select(k => k.Room).ToArray();
                    return this.resultDuplicationService.CopyRoomResultsToRoomNumber(availabilityRequest
                        , j.Where(k => k.ProviderResult.NumberOfAvailableRooms == null || k.ProviderResult.NumberOfAvailableRooms >= toRooms.Length + 1).ToArray(), toRooms);
                }).SelectMany(i => i).ToArray();
                results = results.Concat(clonedResults).ToArray();
            }
            return results;
        }

        public IList<AccommodationAvailabilityResult> AssignRoomNumbers(AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, IList<AccommodationAvailabilityResult> results)
        {
            results.ParallelForEach(i =>
            {
                var roomAvailabilityKey = roomAvailabilityKeys.First(j => j.AvailabilityRequestId == i.AvailabilityRequestId);
                i.RoomNumber = roomAvailabilityKey.Room.RoomNumber;
            });
            return results;
        }
    }
}
