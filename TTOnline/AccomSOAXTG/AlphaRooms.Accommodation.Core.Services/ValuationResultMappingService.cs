﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationResultMappingService : IValuationResultMappingService
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
      

        public ValuationResultMappingService(ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;            
        }

        public IList<AccommodationValuationResultRoom> MapFromProviderValuationResults(AccommodationValuationLiveProcessParams liveProcessParams
            , AccommodationProviderValuationRequest providerRequest, AccommodationProviderValuationResult[] providerResults)
        {           
            return providerResults.AsParallel().Select((providerResult) =>
            {
                try
                {
                    var result = new AccommodationValuationResultRoom();
                    result.Id = Guid.NewGuid();
                    var roomRequest = liveProcessParams.ValuationRequestRooms[(liveProcessParams.ValuationRequestRooms.Length == 1 ? 0 : providerResult.RoomNumber - 1)];
                    result.ProviderResult = providerResult;
                    result.Provider = liveProcessParams.Provider;
                    result.ProviderId = result.Provider.Id;
                    result.Supplier = roomRequest.AvailabilityResult.Supplier;
                    result.SupplierId = result.Supplier.Id;
                    result.SupplierAlias = roomRequest.AvailabilityResult.ProviderResult.ProviderAliasName;
                    result.RoomNumber = roomRequest.RoomNumber;
                    result.AvailabilityRequestId = roomRequest.AvailabilityResult.AvailabilityRequestId;
                    result.DestinationId = roomRequest.AvailabilityResult.DestinationId;
                    result.BaseDestinationId = roomRequest.AvailabilityResult.BaseDestinationId;
                    result.EstablishmentId = roomRequest.AvailabilityResult.EstablishmentId;
                    result.EstablishmentStarRating = roomRequest.AvailabilityResult.EstablishmentStarRating;
                    result.EstablishmentChainId = roomRequest.AvailabilityResult.EstablishmentChainId;
                    result.RoomDescription = providerResult.RoomDescription.ToPascalCase();
                    result.BoardType = roomRequest.AvailabilityResult.BoardType;
                    result.RoomId = roomRequest.RoomId;
                    result.AdultsCount = roomRequest.SelectedRoomDetails.GuestAges.CountByte(i => i > configurationManager.AccommodationMaxChildAge);
                    result.ChildrenCount = roomRequest.SelectedRoomDetails.GuestAges.CountByte(i => i > configurationManager.AccommodationMaxInfantAge && i <= configurationManager.AccommodationMaxChildAge);
                    result.InfantsCount = roomRequest.SelectedRoomDetails.GuestAges.CountByte(i => i <= configurationManager.AccommodationMaxInfantAge);
                    //result.HikePrice = roomRequest.SelectedRoomDetails.HikePrice;
                    result.IsEligibleForDeposit = roomRequest.AvailabilityResult.IsEligibleForDeposit;
                    result.Price = providerResult.SalePrice;
                    result.SalePrice = providerResult.SalePrice;
                    result.CostPrice = providerResult.CostPrice;
                    if (providerResult.ProviderSpecificData != null && providerResult.ProviderSpecificData.ContainsKey("exchangeRate"))
                    {
                        result.ExchangeRate = Convert.ToDecimal(providerResult.ProviderSpecificData["exchangeRate"]);//Binding exchange rate here for xtg meta
                    }
                    result.AccommodationAdminFee = roomRequest.AvailabilityResult.AccommodationAdminFee;
                    result.IsActiveAccommodationAdminFee = roomRequest.AvailabilityResult.IsActiveAccommodationAdminFee;
                    result.CountryCode = roomRequest.AvailabilityResult.CountryCode;
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromProviderValuationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, providerResult.ToIndentedJson());
                    return null;
                }
            }).Where(i => i != null).ToArray();
        }
    }
}
