﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityResultDuplicationService : IAvailabilityResultDuplicationService
    {
        static AvailabilityResultDuplicationService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationAvailabilityResult, AccommodationAvailabilityResult>();
                Mapper.CreateMap<AccommodationProviderAvailabilityResult, AccommodationProviderAvailabilityResult>();
                Mapper.CreateMap<AccommodationProviderAvailabilityRequestRoom, AccommodationProviderAvailabilityRequestRoom>();
            }
        }

        public IList<AccommodationAvailabilityResult> CopyRoomResultsToRoomNumber(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results
            , AccommodationAvailabilityRequestRoom[] toRequestRooms)
        {
            return results.AsParallel().SelectMany(i => toRequestRooms.Select(j => CreateCopyAvailabilityResult(j.RoomNumber, i))).ToArray();
        }

        public IList<AccommodationAvailabilityResult> CopyRoomResultsToRoomNumber(AccommodationAvailabilityResult availabilityResult, byte[] toRequestRooms)
        {
            return toRequestRooms.Select(i => CreateCopyAvailabilityResult(i, availabilityResult)).ToArray();
        }

        private AccommodationAvailabilityResult CreateCopyAvailabilityResult(byte roomNumber, AccommodationAvailabilityResult result)
        {
            var clone = Mapper.Map<AccommodationAvailabilityResult, AccommodationAvailabilityResult>(result);
            clone.RoomNumber = roomNumber;
            return clone;
        }
    }
}
