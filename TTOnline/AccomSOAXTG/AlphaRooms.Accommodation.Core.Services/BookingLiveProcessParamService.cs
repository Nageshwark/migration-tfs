﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingLiveProcessParamService : IBookingLiveProcessParamService
    {
        public AccommodationBookingLiveProcessParams[] CreateBookingLiveProcessParams(AccommodationBookingRequest bookingRequest, BookingRoomValuationResult[] bookingRequestRooms)
        {
            return bookingRequestRooms.GroupBy(i => i.ValuationResultRoom.Provider).AsParallel().SelectMany(providerRooms => 
            {
                if (providerRooms.Key.ContractDependencyMode == ContractDependencyMode.Dependent)
                {
                    return providerRooms.GroupBy(i => i.ValuationResultRoom.ProviderResult.Contract)
                        .SelectMany(i => i.SplitToArrays(providerRooms.Key.MaxRoomBooking != null ? providerRooms.Key.MaxRoomBooking.Value : providerRooms.Count())
                        .Select((j, k) => CreateAccommodationBookingLiveProcessParams(bookingRequest, providerRooms.Key, k, j)));
                }
                return providerRooms.SplitToArrays((providerRooms.Key.MaxRoomBooking != null ? providerRooms.Key.MaxRoomBooking.Value : providerRooms.Count()))
                    .Select((i, j) => CreateAccommodationBookingLiveProcessParams(bookingRequest, providerRooms.Key, j, i));
            }).ToArray();
        }

        private AccommodationBookingLiveProcessParams CreateAccommodationBookingLiveProcessParams(AccommodationBookingRequest bookingRequest, AccommodationProvider provider, int index
            , BookingRoomValuationResult[] providerRooms)
        {
            providerRooms.ForEach((i, j) => i.ProviderRoomNumber = (byte)(j + 1));
            return new AccommodationBookingLiveProcessParams()
            {
                Provider = provider
                , ProcessId = (byte)(index + 1)
                , BookingRequest = bookingRequest
                , BookingRequestRooms = providerRooms
            };
        }
    }
}
