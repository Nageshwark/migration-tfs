﻿namespace AlphaRooms.Accommodation.Core.Services
{
    internal class StringContainsResult
    {
        private readonly int startIndex;
        private readonly int endIndex;
        private readonly string replaceWith;

        public StringContainsResult(int startIndex, int endIndex, string replaceWith)
        {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.replaceWith = replaceWith;
        }

        public int StartIndex
        {
            get
            {
                return startIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return endIndex;
            }
        }

        public string ReplaceWith
        {
            get
            {
                return this.replaceWith;
            }
        }

        public bool IsOverlapping(int startIndex, int endIndex)
        {
            return !(this.startIndex > endIndex
                || this.endIndex < startIndex);
        }

        public override string ToString()
        {
            return "Start: " + this.startIndex.ToString() + " End: " + this.endIndex.ToString() + " Replacment: " + this.replaceWith.ToString();
        }
    }
}