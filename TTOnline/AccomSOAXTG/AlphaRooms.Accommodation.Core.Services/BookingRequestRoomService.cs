﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingRequestRoomService : IBookingRequestRoomService
    {
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IAgeRuleService ageRuleService;

        public BookingRequestRoomService(IAvailabilityResultRoomIdGenerator roomIdGenerator, IAgeRuleService ageRuleService)
        {
            this.roomIdGenerator = roomIdGenerator;
            this.ageRuleService = ageRuleService;
        }

        public BookingRoomValuationResult[] CreateBookingRequestRooms(AccommodationBookingRequest bookingRequest)
        {
            var bookingRequestRooms = bookingRequest.ValuatedRooms.Select(i => new BookingRoomValuationResult() { ValuatedRoom = i, ValuatedRoomDetails = roomIdGenerator.Parse(i.ValuatedRoomId) })
                .OrderBy(i => i.ValuatedRoomDetails.RoomNumber).ToArray();
            bookingRequestRooms.ForEach(i => i.RoomNumber = i.ValuatedRoomDetails.RoomNumber);
            return bookingRequestRooms;
        }

        public void SetValuationResults(BookingRoomValuationResult[] bookingRequestRooms, AccommodationValuationResult valuationResult)
        {
            bookingRequestRooms.ForEach(i =>
            {
                i.ValuationResult = valuationResult;
                i.ValuationResultRoom = valuationResult.Rooms.First(j => j.RoomNumber == i.RoomNumber);
            });
        }
    }
}
