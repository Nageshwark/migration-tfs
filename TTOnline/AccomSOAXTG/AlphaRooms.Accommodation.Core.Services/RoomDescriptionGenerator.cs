﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class RoomDescriptionGenerator : IRoomDescriptionGenerator
    {
        private static readonly Regex CaptureWords = new Regex(@"([+-]*)[A-Za-z0-9]([A-Za-z0-9]|[-'\.\,@]{1,2}(?=[A-Za-z0-9]))*", RegexOptions.Compiled);
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IProviderNonRefundableService nonRefundableService;

        private readonly string nonRefundableLabel;

        public RoomDescriptionGenerator(IAccommodationConfigurationManager configurationManager, IProviderNonRefundableService nonRefundableService)
        {
            this.configurationManager = configurationManager;
            this.nonRefundableService = nonRefundableService;
            this.nonRefundableLabel = this.configurationManager.AccommodationRoomDescriptionNonRefundableLabel;
        }

        public void ApplyAvailabilityRoomDescriptionFormat(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityResult result)
        {
            ApplyRoomDescriptionFormat(result.ProviderResult.IsNonRefundable, result);
        }

        public AccommodationValuationResult ApplyValuationRoomDescriptionFormat(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            result.Rooms.ParallelForEach(i => ApplyRoomDescriptionFormat(i.ProviderResult.IsNonRefundable, i));
            return result;
        }

         private void ApplyRoomDescriptionFormat(bool isNonRefundable, IAccommodationResultRoom result)
        {
            if (!isNonRefundable) return;
            if (isNonRefundable)
            {
                if (!nonRefundableService.IsDescriptionNonRefundable(result.RoomDescription))
                    result.RoomDescription += nonRefundableLabel;
            }

            if (!nonRefundableService.IsDescriptionNonRefundable(result.RoomDescription, isNonRefundable)) result.RoomDescription += nonRefundableLabel;
        }
    }
}
