﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityResultHighestPriceService : IAvailabilityResultHighestPriceService
    {
        public AvailabilityResultHighestPriceService()
        {
        }

        public void ApplyAvailabilityRoomHighestPrice(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityResult[] result)
        {
            if (result.Length == 1) return;
            result[0].HighestPrice = result[result.Length - 1].Price;
        }
    }
}
