﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class PriceHikingFactory : IPriceHikingFactory
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public PriceHikingFactory(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public PriceHikeThreshold CreateFakeThreshold()
        {
            return new PriceHikeThreshold() {Threshold = configurationManager.SupplierValuationThresholdDefault };
        }

        public PriceHikeDetail CreateEmptyResult()
        {
            return new PriceHikeDetail();
        }

        public PriceHikeDetail CreateEmptyResult(PriceHikeStatus status)
        {
            return new PriceHikeDetail
            {
                Status = status,
                priceChange = false
            };
        }

        /// <summary>
        /// Sale amounts are post processed and can include HMD and other discounts.
        /// </summary>
        /// <param name="availRoom">Availability room result</param>
        /// <param name="valRoom">Valuation room result</param>
        /// <param name="isValid"></param>
        /// <returns></returns>
        public PriceHikeDetail CreateResult(AccommodationAvailabilityResult availRoom, AccommodationValuationResultRoom valRoom, bool isValid)
        {
            return new PriceHikeDetail
            {
                Status = isValid ? PriceHikeStatus.Acceptable : PriceHikeStatus.Unnaceptable,
                priceChange = availRoom.ProviderResult.CostPrice.Amount != valRoom.CostPrice.Amount,
                OriginalCost = availRoom.ProviderResult.CostPrice,
                OriginalSale = null,
                PriceHikeCost = valRoom.CostPrice,
                PriceHikeSale = valRoom.Price
            };
        }
    }
}
