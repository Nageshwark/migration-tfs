﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Linq;

    public class AvailabilityResultLowerPriceService : IAvailabilityResultLowerPriceService
    {
        public IList<AccommodationAvailabilityResult> ApplyAvailabilityLowerPrice(IList<AccommodationAvailabilityResult> results)
        {
            results.GroupBy(i => i, new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) => i.EstablishmentId == j.EstablishmentId
                && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType)).AsParallel().Select(i => i.OrderBy(j => j.Price).First()).ForAll(i => i.IsLowestPrice = true);
            return results;
        }
    }
}
