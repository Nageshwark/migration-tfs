﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.Accommodation.Core.Interfaces.Repositories;

    public class DepositSchemeService : IDepositSchemeService
    {
        private readonly IDepositSchemeRepository depositSchemeRepository;

        public DepositSchemeService(IDepositSchemeRepository depositSchemeRepository)
        {
            if (depositSchemeRepository == null)
            {
                throw new ArgumentNullException();
            }

            this.depositSchemeRepository = depositSchemeRepository;
        }

        public async Task<FilteredRepositoryResponse<DepositScheme>> GetDepositSchemesAsync(
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, bool>> whereClause = null,
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, object>> orderBy = null,
            SortDirection? sortDirection = null,
            int? skipCount = null,
            int? takeCount = null)
        {
            return await this.depositSchemeRepository.GetDepositSchemesAsync(whereClause, orderBy, sortDirection.GetValueOrDefault() == SortDirection.Descending, skipCount, takeCount);
        }

        public Task<DepositScheme> GetDepositSchemeAsync(int id)
        {
            return this.depositSchemeRepository.GetDepositScheme(id);
        }

        public Task<DepositScheme> GetCurrentDepositSchemeAsync()
        {
            return this.depositSchemeRepository.GetCurrentDepositSchemeAsync();
        }

        public Task<bool> AddDepositSchemeAsync(DepositScheme depositSchemeDetail)
        {
            return this.depositSchemeRepository.AddDepositSchemeAsync(depositSchemeDetail);
        }

        public Task<bool> UpdateDepositSchemeAsync(DepositScheme depositSchemeDetail)
        {
            return this.depositSchemeRepository.UpdateDepositSchemeAsync(depositSchemeDetail);
        }

        public Task<bool> DeleteDepositSchemeAsync(int id)
        {
            return this.depositSchemeRepository.DeleteDepositSchemeAsync(id);
        }

        public DepositScheme GetCurrentDepositScheme()
        {
            return this.depositSchemeRepository.GetCurrentDepositScheme();
        }

        public bool IsFlexibleDepositEnabled()
        {
            var currentScheme = this.depositSchemeRepository.GetCurrentDepositScheme();
            return currentScheme != null && currentScheme.DepositDueWeeksAfter.HasValue && currentScheme.DepositDueMinSpend.HasValue;
        }
    }
}