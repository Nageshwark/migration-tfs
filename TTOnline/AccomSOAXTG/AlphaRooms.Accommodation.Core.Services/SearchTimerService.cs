﻿using System;

namespace AlphaRooms.Accommodation.Core.Services
{
    /// <summary>
    /// Records approximate timings for searches using log4net
    /// </summary>
    public class SearchTimerService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger("SearchTimings");

        // Don’t use stopwatch we do not need detail (ms) timings approximate is fine.
        private readonly DateTime startTime;
        private readonly Guid requestGuid;
        private readonly string description;

        public SearchTimerService(Guid requestGuid, string description)
            : this(requestGuid, description, DateTime.UtcNow)
        {
        }

        protected SearchTimerService(Guid requestGuid, string description, DateTime startTime)
        {
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentException("Can not be null or empty.", "description");
            }

            if (startTime.Kind != DateTimeKind.Utc)
            {
                // use Utc as far less overhead than just Now, also no clock skew when clocks go back
                throw new ArgumentException("must be UTC", "startTime");
            }

            this.requestGuid = requestGuid;
            this.description = description;
            this.startTime = startTime;
        }

        public void Record()
        {
            Log.Debug(this);
        }

        public override string ToString()
        {
            return this.requestGuid + "," + this.startTime.TimeOfDay + "," + (DateTime.UtcNow - this.startTime).TotalSeconds.ToString("F") + "," + this.description;
        }
    }
}
