﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class CancellationResultBuilderService : ICancellationResultBuilderService
    {
        public AccommodationCancellationResult CreateCancellationResult(AccommodationCancellationRequest cancellationRequest, IList<AccommodationCancellationResultRoom> cancellationRoomResults)
        {
            // TODO handle room cancellation failure and cancellation process summary
            return new AccommodationCancellationResult()
            {
                CancellationId = cancellationRequest.CancellationId
                , Rooms = cancellationRoomResults.OrderBy(i => i.RoomNumber).ToArray()
            };
        }
    }
}
