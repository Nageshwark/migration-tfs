﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using Interfaces;
    using Provider.DomainModels;
    using Provider.Interfaces.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /*
      * Warning 
      * 
      * This file is performance critical (speed). All changes must be profiled!
      */

    /// <summary>
    /// Responsible for creating and comparing RoomDescriptionKey.
    /// This key is used to it find duplicate rooms.
    /// A duplicate is a room where the room description is considered to be the same has another with a equal board type and board description, with the duplicate being more expensive.
    /// </summary>
    public class RoomDescriptionKeyGeneration : IRoomDescriptionKeyGeneration
    {
        // these will be replaced with spaces
        private static readonly char[] Punctuation = new char[] { '-', '/', '+', '(', ')', '*', '.', ',' };

        // split the description into word on spaces
        private static readonly char[] SplitOn = new char[] { ' ' };

        private readonly IRoomDescriptionRemoveWordRepository roomDescriptionRemoveWordRepository;
        private readonly IRoomDescriptionReplacementRepository roomDescriptionReplacementRepository;

        private IReadOnlyDictionary<string, int> removeWords;
        private IReadOnlyList<Alias> aliases;

        public RoomDescriptionKeyGeneration(IRoomDescriptionRemoveWordRepository roomDescriptionRemoveWordRepository, IRoomDescriptionReplacementRepository roomDescriptionReplacementRepository)
        {
            this.roomDescriptionRemoveWordRepository = roomDescriptionRemoveWordRepository;
            this.roomDescriptionReplacementRepository = roomDescriptionReplacementRepository;
        }

        public async Task InitializeAsync()
        {
            this.removeWords = await this.roomDescriptionRemoveWordRepository.GetAllAsync();
            this.aliases = await this.roomDescriptionReplacementRepository.GetAllAliasAsync();
        }

        public string GeneratKey(string roomDescription)
        {
            if (roomDescription == null)
            {
                throw new ArgumentNullException(nameof(roomDescription));
            }

            string tmp = ReplacePunctuation(roomDescription);

            tmp = this.ReplaceAliases(tmp);

            string[] descriptionWords = SplitToWords(tmp);

            // sort the split words into alphabetical order
            Array.Sort(descriptionWords, StringComparer.OrdinalIgnoreCase);

            StringBuilder duplicationKey = new StringBuilder();

            for (int i = 0; i < descriptionWords.Length; i++)
            {
                string word = descriptionWords[i];

                if (IsNotSingalChar(word)
                    && !this.removeWords.ContainsKey(word)
                    && IsNotDuplicate(descriptionWords, i))
                {
                    duplicationKey.Append(word);
                    duplicationKey.Append(" ");
                }
            }

            return duplicationKey.ToString();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsNotDuplicate(string[] descriptionWords, int currentIndex)
        {

            return
                // can not be a duplicate its the first one
                currentIndex == 0
                // the descriptionWords are sorted, it is a duplicate in the array, the next prevouse index will be the same
                || !string.Equals(descriptionWords[currentIndex], descriptionWords[currentIndex - 1], StringComparison.OrdinalIgnoreCase);
        }

        private string ReplaceAliases(string roomDescription)
        {
            /*
             * to avoid a problem where one replacement would match alter replacement, we cannot do replacements in place. 
             * Instead find all matches and replace the ones that do not overlap using the order of the replacements for priority.
             */

            // keeps track of current replacments
            List<StringContainsResult> replacments = new List<StringContainsResult>();

            for (int i = 0; i < this.aliases.Count; i++)
            {
                string wordToReplace = this.aliases[i].ToReplace;

                int index = 0;

                // wordToReplace may appear more than once, so do not just stop on the first instance 
                while (-1 != (index = roomDescription.IndexOf(wordToReplace, index, StringComparison.OrdinalIgnoreCase)))
                {
                    int endIndex = index + (wordToReplace.Length - 1);

                    // find if this match overlaps with another, we are using the order of deduplication words to set the priority
                    bool isOverlap = false;
                    for (int j = 0; j < replacments.Count; j++)
                    {
                        if (replacments[j].IsOverlapping(index, endIndex))
                        {
                            isOverlap = true;
                            break;
                        }
                    }

                    if (!isOverlap)
                    {
                        replacments.Add(new StringContainsResult(
                            index,
                            endIndex,
                            this.aliases[i].ReplaceWith));
                    }

                    index += (endIndex + 1);

                    if (index >= roomDescription.Length)
                    {
                        // do not index passed the end of the string
                        break;
                    }
                }
            }

            roomDescription = Repalce(replacments, roomDescription);

            return roomDescription;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsNotSingalChar(string word)
        {
            return word.Length > 1;
        }

        private static string ReplacePunctuation(string roomDescription)
        {
            char[] value = roomDescription.ToCharArray();

            for (int i = 0; i < value.Length; i++)
            {
                for (int j = 0; j < Punctuation.Length; j++)
                {
                    if (value[i] == Punctuation[j])
                    {
                        // replace the Punctuation with a space char
                        value[i] = ' ';
                        break;
                    }
                }
            }

            string retVal = new string(value);
            return retVal;
        }

        private static string[] SplitToWords(string roomDescription)
        {
            string[] splitRoomDescription = roomDescription.Split(SplitOn, StringSplitOptions.RemoveEmptyEntries);
            return splitRoomDescription;
        }

        private static string Repalce(List<StringContainsResult> replacements, string replaceIn)
        {
            StringBuilder newValue = new StringBuilder();

            int index = 0;

            foreach (var replacement in replacements.OrderBy(x => x.StartIndex))
            {
                if (index != replacement.StartIndex)
                {
                    newValue.Append(
                        replaceIn.Substring(index, replacement.StartIndex - index));
                }

                newValue.Append(replacement.ReplaceWith);
                index = replacement.EndIndex + 1;
            }

            newValue.Append(
                replaceIn.Substring(index, replaceIn.Length - index));

            return newValue.ToString();
        }
    }
}