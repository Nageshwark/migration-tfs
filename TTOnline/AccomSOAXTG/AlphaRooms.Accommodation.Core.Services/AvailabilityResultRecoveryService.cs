﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityResultRecoveryService : IAvailabilityResultRecoveryService
    {
        private readonly IProviderAvailabilityService providerAvailabilityService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAvailabilityKeyGenerator availabilityKeyGenerator;
        private readonly IAvailabilityProcessParamService processParamService;
        private readonly IAvailabilityRequestRoomService requestRoomService;
        private readonly IAvailabilityLiveProcessService liveProcessService;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IAvailabilityResultMarkupService resultMarkupService;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IRoomDescriptionGenerator resultRoomDescriptionService;

        public AvailabilityResultRecoveryService(IProviderAvailabilityService providerAvailabilityService, IChannelInfoService channelInfoService, IAccommodationProviderRepository providerRepository
            , IAvailabilityKeyGenerator availabilityKeyGenerator, IAvailabilityProcessParamService processParamService, IAvailabilityRequestRoomService requestRoomService
            , IAvailabilityLiveProcessService liveProcessService, IAvailabilityResultRoomIdGenerator roomIdGenerator, IAvailabilityResultMarkupService resultMarkupService
            , IPriceCalculatorService priceCalculatorService, IAvailabilityService availabilityService, IAccommodationConfigurationManager configurationManager
            , IAvailabilityResultDuplicationService resultDuplicationService, IRoomDescriptionGenerator resultRoomDescriptionService)
        {
            this.providerAvailabilityService = providerAvailabilityService;
            this.channelInfoService = channelInfoService;
            this.providerRepository = providerRepository;
            this.availabilityKeyGenerator = availabilityKeyGenerator;
            this.processParamService = processParamService;
            this.requestRoomService = requestRoomService;
            this.liveProcessService = liveProcessService;
            this.roomIdGenerator = roomIdGenerator;
            this.resultMarkupService = resultMarkupService;
            this.priceCalculatorService = priceCalculatorService;
            this.resultRoomDescriptionService = resultRoomDescriptionService;
        }
        
        public async Task ProcessValuationRecoveryAsync(AccommodationValuationRequest valuationRequest, ValuationRoomIdAvailabilityResult[] valuationRooms)
        {
            await Async.ParallelForEach(valuationRooms, async(valuationRoom) =>
            {
                if (valuationRoom.AvailabilityResult == null)
                {
                    // Perform cached expired recovery service
                    valuationRoom.AvailabilityResult = await RecoverAvailabilityRoomAsync(valuationRequest, valuationRoom);
                    valuationRoom.IsSelectedRoomRecovered = true;
                }
                else if (valuationRoom.AvailabilityResult.SessionExpireDate != null && valuationRoom.AvailabilityResult.SessionExpireDate < DateTime.Now)
                {
                    // Perform session expired recovery service
                    valuationRoom.AvailabilityResult = await RecoverAvailabilityRoomAsync(valuationRequest, valuationRoom);
                    valuationRoom.IsSelectedRoomRecovered = true;
                }
            });
        }
        
        private async Task<AccommodationAvailabilityResult> RecoverAvailabilityRoomAsync(AccommodationValuationRequest valuationRequest, ValuationRoomIdAvailabilityResult valuationRoom)
        {
            var availabilityRequest = await CreateAccommodationAvailabilityRequest(new Guid(), valuationRequest.Agent, valuationRoom.SelectedRoomDetails);
            var availabilityRequestRooms = requestRoomService.CreateAvailabilityRequestRooms(availabilityRequest);
            var liveProcessParams = await processParamService.CreateAvailabilityLiveProcessParamsAsync(availabilityRequest, availabilityRequestRooms
                , new AccommodationAvailabilityCachedRequest[0], availabilityRequest.Providers);
            await this.liveProcessService.ProcessAvailabilityLiveProviderAsync(liveProcessParams[0]);
            if (liveProcessParams[0].Response == null || liveProcessParams[0].Response.Exception != null) throw new Exception("Recover availability process failed.");
            var results = liveProcessParams[0].Response.Results;
            var recoveredResult = FindExactAvailabilityResultOrNull(results, valuationRoom.SelectedRoomDetails);
            if (recoveredResult == null) throw new Exception("Unable to recover availability result.");
            var processedRecoveredResult = await PostProcessAvailabilityResultsAsync(availabilityRequest, availabilityRequestRooms, new[] { recoveredResult });
            var recoveredRoom = processedRecoveredResult[0];
            recoveredRoom.RoomId = valuationRoom.RoomId;
            recoveredRoom.RoomNumber = valuationRoom.RoomNumber;
            recoveredRoom.AvailabilityRequestId = valuationRequest.AvailabilityId;
            return recoveredRoom;
        }

        private async Task<AccommodationAvailabilityRequest> CreateAccommodationAvailabilityRequest(Guid availabilityId, B2BUser agent, AccommodationAvailabilityResultRoomIdWrapper roomDetails)
        {
            return new AccommodationAvailabilityRequest()
            {
                ChannelInfo = await this.channelInfoService.GetChannelInfo(roomDetails.Channel)
                , Agent = agent
                , AvailabilityId = availabilityId
                , SearchType = SearchType.HotelOnly
                , DestinationId = null
                , EstablishmentId = roomDetails.EstablishmentId
                , CheckInDate = roomDetails.CheckInDate
                , CheckOutDate = roomDetails.CheckOutDate
                , Providers = new AccommodationProvider[] { await this.providerRepository.GetByIdAsync(roomDetails.ProviderId) }
                , Rooms = new AccommodationAvailabilityRequestRoom[]
                {
                    new AccommodationAvailabilityRequestRoom()
                    {
                        RoomNumber = 1
                        , Guests = roomDetails.GuestAges.Select(i => new AccommodationAvailabilityRequestRoomGuest() { Age = i }).ToArray()
                    }
                }
            };
        }

        private async Task<IList<AccommodationAvailabilityResult>> PostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest
            , AvailabilityRoomAvailabilityKey[] availabilityRequestRooms, IList<AccommodationAvailabilityResult> results)
        {
            results = await resultMarkupService.ApplyAvailabilityMarkupAsync(availabilityRequest, results);
            results = priceCalculatorService.ApplyAvailabilityPriceCalculation(availabilityRequest, results);
            results.ParallelForEach(result =>
            {
                resultRoomDescriptionService.ApplyAvailabilityRoomDescriptionFormat(availabilityRequest, result);
                roomIdGenerator.ApplyAvailabilityRoomId(availabilityRequest, availabilityRequestRooms, result);
            });
            return results;
        }

        private AccommodationAvailabilityResult FindExactAvailabilityResultOrNull(IList<AccommodationAvailabilityResult> results, AccommodationAvailabilityResultRoomIdWrapper roomDetails)
        {
            return results.FirstOrDefault(i => i.EstablishmentId == roomDetails.EstablishmentId
                && StringModule.EqualsIgnoreEmpty(i.ProviderResult.RoomCode, roomDetails.ProviderRoomCode)
                && StringModule.EqualsIgnoreEmpty(i.ProviderResult.RoomDescription, roomDetails.ProviderRoomDescription)
                && StringModule.EqualsIgnoreEmpty(i.ProviderResult.BoardCode, roomDetails.ProviderBoardCode)
                && StringModule.EqualsIgnoreEmpty(i.ProviderResult.BoardDescription, roomDetails.ProviderBoardDescription)
                && i.ProviderResult.IsBindingRate == roomDetails.ProviderIsBindingRate
                && i.ProviderResult.IsNonRefundable == roomDetails.ProviderIsNonRefundable
                && i.ProviderResult.IsOpaqueRate == roomDetails.ProviderIsOpaqueRate
                && i.ProviderResult.PaymentModel == roomDetails.ProviderPaymentModel
                && i.ProviderResult.RateType == roomDetails.ProviderRateType
                && i.ProviderResult.CostPrice.Amount == roomDetails.ProviderCostPrice.Amount);
        }
    }
}