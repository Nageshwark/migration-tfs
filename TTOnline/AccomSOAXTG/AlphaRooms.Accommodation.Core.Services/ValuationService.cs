﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Services.Exceptions;
using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationService : IValuationService
    {
        private readonly IAvailabilityResultCaching availabilityResultsCaching;
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly IValuationLiveProcessParamService liveProcessService;
        private readonly IValuationRequestFactory requestFactory;
        private readonly IProviderValuationService providerValuationService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IValuationResultCaching resultCaching;
        private readonly IAvailabilityResultRecoveryService recoveryService;
        private readonly IValuationResultMappingService resultMappingService;
        private readonly IValuationResultBuilderService resultBuilderService;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IValuationRequestRoomService valuationRequestRoomService;
        private readonly IValuationCancellationPolicyService cancellationPolicyService;
        private readonly IDestinationRepository destinationRepository;
        private readonly IValuationResultMarkupService markupService;
        private readonly IRoomDescriptionGenerator roomDescriptionGenerator;
        private readonly IDiParameterContainer diParameterContainer;
        private readonly IPriceHikingService priceHikingService;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;

        public ValuationService(IAvailabilityResultCaching availabilityResultsCaching, IAccommodationProviderRepository accommodationProviderRepository
            , IValuationLiveProcessParamService liveProcessService, IValuationRequestFactory requestFactory, IProviderValuationService providerValuationService
            , IValuationResultMappingService resultsMappingService, IAccommodationConfigurationManager configurationManager, IValuationResultCaching resultCaching
            , IAvailabilityResultRecoveryService recoveryService, IValuationResultMappingService resultMappingService, IValuationResultBuilderService resultBuilderService
            , IPriceCalculatorService priceCalculatorService, IValuationRequestRoomService valuationRequestRoomService, IValuationCancellationPolicyService cancellationPolicyService
            , IDestinationRepository destinationRepository, IValuationResultMarkupService markupService, IRoomDescriptionGenerator roomDescriptionGenerator
            , IDiParameterContainer diParameterContainer, IPriceHikingService priceHikingService, AccommodationPromotionalDiscountService promotionalDiscountService)
        {
            this.availabilityResultsCaching = availabilityResultsCaching;
            this.accommodationProviderRepository = accommodationProviderRepository;
            this.liveProcessService = liveProcessService;
            this.requestFactory = requestFactory;
            this.providerValuationService = providerValuationService;
            this.configurationManager = configurationManager;
            this.resultCaching = resultCaching;
            this.recoveryService = recoveryService;
            this.resultMappingService = resultMappingService;
            this.resultBuilderService = resultBuilderService;
            this.priceCalculatorService = priceCalculatorService;
            this.valuationRequestRoomService = valuationRequestRoomService;
            this.cancellationPolicyService = cancellationPolicyService;
            this.destinationRepository = destinationRepository;
            this.markupService = markupService;
            this.roomDescriptionGenerator = roomDescriptionGenerator;
            this.diParameterContainer = diParameterContainer;
            this.priceHikingService = priceHikingService;
            this.promotionalDiscountService = promotionalDiscountService;
        }

        public async Task<AccommodationValuationResponse> GetValuationResponseAsync(Guid valuationId)
        {
            return new AccommodationValuationResponse() { Result = await this.resultCaching.GetByValuationIdOrNullAsync(valuationId) };
        }

        public async Task<AccommodationValuationResponse> ProcessValuationAsync(AccommodationValuationRequest valuationRequest, AccommodationAvailabilityResult[] availabilityResults)
        {
            // create a list containing the selected room details
            var valuationRequestRooms = await ProcessValuationRequestRoomsAsync(valuationRequest, availabilityResults);
            
            //  create the necessary live process parameters based on the selected rooms and their providers
            var liveProcessParams = liveProcessService.CreateValuationLiveProcessParams(valuationRequest, valuationRequestRooms);

            // create tasks to perform valuation
            var tasks = liveProcessParams.Select(i => Task.Run(async () => await ProcessValuationLiveProviderAsync(i))).ToArray();

            // process the tasks, wait for them to complete or the process to timeout
            var searchTask = Task.Run(async () => await Task.WhenAll(tasks));
            await Task.WhenAny(searchTask, Task.Delay(this.configurationManager.AccommodationValuationTimeout));

            // throw an exception if the process timed out
            if (searchTask.Status != TaskStatus.RanToCompletion && liveProcessParams.All(i => i.Response == null))
            {
                throw new CoreValuationTimeoutException(valuationRequest, this.configurationManager.AccommodationValuationTimeout);
            }

            // verify valuation results and generate a valuation package
            var result = resultBuilderService.CreateValuationResult(valuationRequest, liveProcessParams);

            // execute post process for the results
            if (result.Rooms.Any()) result = await PostProcessValuationResultsAsync(valuationRequestRooms, valuationRequest, result);

            // save noncached results to the db
            await resultCaching.SaveAsync(result);

            return new AccommodationValuationResponse()
            {
                Result = result,
                ProcessResponseDetails = liveProcessParams.Select(i => new AccommodationValuationResponseProviderDetails()
                {
                    Provider = i.Provider,
                    ValuationRequestRooms = i.ValuationRequestRooms,
                    ProviderRequests = i.Response?.ProviderRequests,
                    Results = i.Response?.Results,
                    ResultsCount = i.Response?.Results.Count,
                    TimeTaken = i.Response?.TimeTaken,
                    Exception = i.Response?.Exception
                }).ToArray()
            };
        }

        private async Task<AccommodationValuationResult> PostProcessValuationResultsAsync(ValuationRoomIdAvailabilityResult[] valuationRequestRooms, AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            // apply markups
            result = await markupService.ApplyValuationMarkupAsync(valuationRequest, result);
            // process price
            result = priceCalculatorService.ApplyValuationPriceCalculation(valuationRequest, result);

            // apply promotion discounts
            promotionalDiscountService.ProcessDiscounts(result.Rooms, valuationRequest.PromotionalCode, valuationRequest.ChannelInfo.Channel);

            // set room description
            result = roomDescriptionGenerator.ApplyValuationRoomDescriptionFormat(valuationRequest, result);

            // create cancellation policies
            result = await cancellationPolicyService.ApplyCancellationPolicyAsync(valuationRequest, result);

            // price hike
            result = await priceHikingService.ApplyValuationPriceHiking(valuationRequestRooms, result, valuationRequest);

            return result;
        }

        private async Task ProcessValuationLiveProviderAsync(AccommodationValuationLiveProcessParams liveProcessParams)
        {
            AccommodationProviderValuationRequest providerRequest = null;
            var providerRequests = new ConcurrentQueue<AccommodationProviderRequest>();
            AccommodationProviderValuationResponse providerResponse = null;
            IList<AccommodationValuationResultRoom> mappedResults = null;
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            try
            {
                // create provider request
                providerRequest = requestFactory.CreateProvideValuationRequest(liveProcessParams.ValuationRequest, liveProcessParams.Provider, liveProcessParams.ProcessId, liveProcessParams.ValuationRequestRooms);

                // register provider logger parameter in the di container
                this.diParameterContainer.Register(providerRequest, providerRequests);

                // perform provider live search
                providerResponse = await providerValuationService.GetProviderValuationResponseAsync(providerRequest);

                // map results to core availability result 
                mappedResults = resultMappingService.MapFromProviderValuationResults(liveProcessParams, providerRequest, providerResponse.Results.ToArray());
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // unregister provider logger parameter in the di container
            if (providerRequest != null) this.diParameterContainer.Unregister(providerRequest);

            // return provider specific results list 
            liveProcessParams.Response = new ValuationProcessResponse()
            {
                Results = mappedResults ?? new AccommodationValuationResultRoom[0],
                ProviderRequests = providerRequests.ToArray(),
                Exception = exception,
                TimeTaken = DateTime.Now - startDate,
            };
        }

        private async Task<ValuationRoomIdAvailabilityResult[]> ProcessValuationRequestRoomsAsync(AccommodationValuationRequest valuationRequest, AccommodationAvailabilityResult[] availabilityResults)
        {
            // create a list containing the selected room details
            var valuationRequestRooms = await valuationRequestRoomService.CreateValuationRequestRoomsAsync(valuationRequest);

            if (availabilityResults != null && availabilityResults.Any())
            {
                /* Default: B2C and B2BWeb */
                // set the availability results in the correct valuation rooms
                valuationRequestRoomService.SetAvailabilityResults(valuationRequestRooms, availabilityResults);
            }
            else
            {
                /* B2BXml */
                // get the destination related to this hotel
                var destination = await destinationRepository.GetByEstablishmentIdAsync(valuationRequestRooms[0].SelectedRoomDetails.EstablishmentId);

                // get the base destination for this hotel
                var baseDestinationId = destination.ParentDestinationId ?? destination.DestinationId;

                // get selected room availability results
                availabilityResults = await availabilityResultsCaching.GetByRoomIdsAsync(baseDestinationId, valuationRequestRooms.Select(i => i.SelectedRoomDetails.RoomId).ToArray());

                //set the availability results in the correct valuation rooms
                valuationRequestRoomService.SetAvailabilityResults(valuationRequestRooms, availabilityResults);

                // verify and process availability recover if required
                await recoveryService.ProcessValuationRecoveryAsync(valuationRequest, valuationRequestRooms);
            }

            return valuationRequestRooms;
        }
    }
}
