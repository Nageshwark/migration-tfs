﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class DestinationSearchModeService : IDestinationSearchModeService
    {
        private readonly IDestinationSearchModeRepository destinationSearchModeRepository;

        public DestinationSearchModeService(IDestinationSearchModeRepository destinationSearchModeRepository)
        {
            this.destinationSearchModeRepository = destinationSearchModeRepository;
        }

        public async Task<DestinationSearchMode[]> GetByDestinationIdAsync(Guid destinationId)
        {
            return await this.destinationSearchModeRepository.GetByDestinationIdAsync(destinationId);
        }

        public DestinationSearchMode GetDestinationSearchModeOrNull(DestinationSearchMode[] destinationSearchModes, AccommodationProvider provider)
        {
            return destinationSearchModes.Where(i => i.ProviderId == null || i.ProviderId == provider.Id).OrderBy(i => i.ProviderId).FirstOrDefault();
        }
    }
}
