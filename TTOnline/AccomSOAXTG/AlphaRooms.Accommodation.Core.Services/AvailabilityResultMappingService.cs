﻿using AlphaRooms.Cache.Mongo.Interfaces;

namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.ExtensionMethods;
    using Ninject.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.Services;
    using B2C.Interfaces;
    using B2C.Contracts;
    using AutoMapper;
    using SOACommon.Contracts;
    using SOACommon.Contracts.Enumerators;

    public class AvailabilityResultMappingService : IAvailabilityResultMappingService
    {
        private readonly ILogger logger;
        private readonly IBoardRepository boardRepository;
        private readonly IRoomKeyGenerator roomKeyGenerator;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IRoomDescriptionComparerFactory roomDescriptionComparerFactory;
        private readonly IMongoDbSettings settings;
        private readonly IDestinationRepository destinationRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;
        //private readonly IB2CRequestStatusService requestStatusService;
        private readonly IAvailabilityResultAggregatorService roomAggregatorService;
        private readonly IAvailabilityRequestRoomService availabilityRequestRoomService;
        private readonly IAccommodationProviderAdminFeeConfigurationRepository providerAdminFeeConfigurationsRepository;
        private readonly IAccommodationConfigurationManager configurationManager;

        public AvailabilityResultMappingService(
            ILogger logger,
            IBoardRepository boardRepository,
            IAccommodationConfigurationManager configurationManager,
            IRoomKeyGenerator roomKeyGenerator,
            IEstablishmentDetailsRepository establishmentDetailsRepository,
            IRoomDescriptionComparerFactory roomDescriptionComparerFactory,
            IMongoDbSettings settings,
            IDestinationRepository destinationRepository,
            IAccommodationSupplierRepository supplierRepository,
            //IB2CRequestStatusService requestStatusService,
            IAvailabilityRequestRoomService availabilityRequestRoomService,
            IAvailabilityResultAggregatorService roomAggregatorService,
            IAccommodationProviderAdminFeeConfigurationRepository providerAdminFeeConfigurationsRepository)
        {
            this.logger = logger;
            this.boardRepository = boardRepository;
            this.roomKeyGenerator = roomKeyGenerator;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
            this.roomDescriptionComparerFactory = roomDescriptionComparerFactory;
            this.settings = settings;
            this.destinationRepository = destinationRepository;
            this.supplierRepository = supplierRepository;
            //this.requestStatusService = requestStatusService;
            this.availabilityRequestRoomService = availabilityRequestRoomService;
            this.roomAggregatorService = roomAggregatorService;
            this.providerAdminFeeConfigurationsRepository = providerAdminFeeConfigurationsRepository;
            this.configurationManager = configurationManager;
        }

        public async Task<IList<AccommodationAvailabilityResult>> MapFromProviderAvailabilityResultsAsync(
            AccommodationAvailabilityLiveProcessParams liveProcessParams,
            AccommodationProviderAvailabilityRequest providerRequest,
            AccommodationProviderAvailabilityResult[] providerResults)
        {
            var mappingTimer = new SearchTimerService(liveProcessParams.AvailabilityRequest.AvailabilityId, "Search_ProviderAvailability_PostProcessing_Mapping");

            var boardTypesTask = this.boardRepository.GetAllDictionaryAsync();
            var establishmentDetailsTask = this.establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(liveProcessParams.BaseDestinationId, liveProcessParams.AvailabilityRequest.ChannelInfo.Channel);

            // do both at the same time
            await Task.WhenAll(boardTypesTask, establishmentDetailsTask);

            var boardTypes = boardTypesTask.Result;
            var establishmentDetails = establishmentDetailsTask.Result;

            AccommodationAvailabilityResult[] availabilityResult;
            Money accommodationAdminFee = new Money(0, providerRequest.ChannelInfo.CurrencyCode);

            DateTime expirationTime = this.GetCacheExpireDate(liveProcessParams.Provider.CacheExpireTimeout);

            availabilityResult = providerResults.AsParallel().Select(providerResult =>
            {
                try
                {
                    var availabilityRequestRoom = liveProcessParams.AvailabilityRequestRooms[(liveProcessParams.AvailabilityRequestRooms.Length == 1 ? 0 : providerResult.RoomNumber - 1)];
                    var result = new AccommodationAvailabilityResult();
                    result.Id = Guid.NewGuid();
                    result.RoomNumber = 0;
                    result.AvailabilityRequestId = availabilityRequestRoom.AvailabilityRequestId;
                    result.BaseDestinationId = liveProcessParams.BaseDestinationId;
                    result.Provider = liveProcessParams.Provider;
                    result.ProviderId = result.Provider.Id;
                    result.Supplier = GetSupplier(liveProcessParams.Suppliers, providerResult.SupplierEdiCode);
                    result.SupplierId = result.Supplier.Id;
                    result.ProviderResult = providerResult;
                    result.Source = SearchResultSource.Live;
                    result.CacheExpireDate = expirationTime;
                    if (result.ProviderId == configurationManager.TravelGateProvider[0])
                    {
                        providerResult.ProviderName = result.Supplier.Name;
                        providerResult.ProviderFilterName = result.Supplier.Name + " (" + result.Provider.EdiCode + ")";
                        result.SalePrice = providerResult.SalePrice;
                        result.CostPrice = providerResult.CostPrice;
                        result.ExchangeRate = Convert.ToDecimal(providerResult.ProviderSpecificData["exchangeRate"]);//Binding exchange rate here
                        result.AccommodationAdminFee = accommodationAdminFee;
                    }

                    if (liveProcessParams.Provider.IsMappingEnabled)
                    {
                        EstablishmentMapping establishmentMapping = null;
                        if (!liveProcessParams.EstablishmentMappings.TryGetValue(providerResult.EstablishmentEdiCode, out establishmentMapping)) return null;
                        result.DestinationId = establishmentMapping.ResortDestinationId;
                        result.EstablishmentId = establishmentMapping.EstablishmentId;
                        result.BoardType = GetBoardType(boardTypes, providerResult.BoardDescription);
                    }
                    else
                    {
                        result.DestinationId = Guid.Parse(providerResult.DestinationEdiCode);
                        result.EstablishmentId = Guid.Parse(providerResult.EstablishmentEdiCode);
                        result.BoardType = (BoardType)Enum.Parse(typeof(BoardType), providerResult.BoardCode);
                    }

                    result.RoomDescription = providerResult.RoomDescription.ToPascalCase();

                    roomKeyGenerator.ApplyAvailabilityRoomKey(
                        liveProcessParams.AvailabilityRequest.ChannelInfo.Channel,
                        availabilityRequestRoom.Room.Guests.Select(x => x.Age).ToArray(),
                        result);

                    if (providerRequest.Provider.SessionExpireTimeout != null) result.SessionExpireDate = DateTime.Now + providerRequest.Provider.SessionExpireTimeout.Value;
                    EstablishmentDetails establishmentDetailsItem;
                    if (!establishmentDetails.TryGetValue(result.EstablishmentId, out establishmentDetailsItem)) return null;
                    result.EstablishmentChainId = establishmentDetailsItem.EstablishmentChainId;
                    result.EstablishmentStarRating = establishmentDetailsItem.EstablishmentStarRating;
                    result.CountryCode = establishmentDetailsItem.CountryISOCode;
                    //Guests Mapping
                    result.AdultsCount = providerResult.Adults;
                    result.ChildrenCount = providerResult.Children;
                    result.InfantsCount = providerResult.Infants;

                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromProviderAvailabilityResults Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, providerResult.ToIndentedJson());
                    return null;
                }
            })
            .Where(i => i != null)
            .ToArray();

            mappingTimer.Record();

            var timer = new SearchTimerService(liveProcessParams.AvailabilityRequest.AvailabilityId, "Search_ProviderAvailability_PostProcessing_GenerateRoomDescriptions");
            await this.GenerateRoomDescriptions(availabilityResult);
            timer.Record();

            return availabilityResult;
        }

        private async Task GenerateRoomDescriptions(AccommodationAvailabilityResult[] availabilityResult)
        {
            // need to create a new one as it is not threadsafe
            var roomDescriptionComparer = this.roomDescriptionComparerFactory.Create();

            await roomDescriptionComparer.InitializeAsync();

            for (int i = 0; i < availabilityResult.Length; i++)
            {
                roomDescriptionComparer.ApplyAvailabilityRoomDescriptionKey(availabilityResult[i]);
            }
        }

        private AccommodationSupplier GetSupplier(Dictionary<string, AccommodationSupplier> suppliers, string supplierEdiCode)
        {
            if (string.IsNullOrEmpty(supplierEdiCode)) return suppliers["default"];
            return suppliers.GetValueOrDefault(supplierEdiCode, suppliers["default"]);
        }

        private BoardType GetBoardType(Dictionary<BoardType, Board> boardTypes, string boardDecription)
        {
            if (string.IsNullOrEmpty(boardDecription)) return BoardType.RoomOnly;
            var board = boardTypes.Values.FirstOrDefault(i => boardDecription.Contains(i.Keyword, StringComparison.CurrentCultureIgnoreCase));
            return (board != null ? (BoardType)board.Id : BoardType.RoomOnly);
        }

        private DateTime GetCacheExpireDate(TimeSpan cacheExpireTimeout)
        {
            return ((this.settings.CacheServerIsDateUTC ? DateTime.UtcNow : DateTime.Now) + cacheExpireTimeout);
        }
    

        public async Task<IList<AccommodationAvailabilityResult>> MapMetaProviderAvailabilityResultsAsync(AccommodationAvailabilityLiveProcessParams providerRequest, AccommodationProviderAvailabilityResult[] providerResults)
        {
            Destination destination = null;
            try
            {
                if (providerResults[0].DestinationEdiCode != null) destination = await destinationRepository.GetByDestinationIdAsync(providerRequest.AvailabilityRequest.DestinationId.Value);
                else destination = await destinationRepository.GetByEstablishmentIntIdAsync(Int32.Parse(providerResults[0].EstablishmentEdiCode));
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromProviderAvailabilityResults Error: result [{0}] {1}", "(unknown)", ex.Message, ex.StackTrace);
                return null;
            }

            providerRequest.AvailabilityRequest.DestinationId = destination.DestinationId;
            providerRequest.AvailabilityRequest.EstablishmentId = destination.EstablishmentId;
            providerRequest.AvailabilityRequest.CheckInDate = providerResults[0].CheckInDate;
            providerRequest.AvailabilityRequest.CheckOutDate = providerResults[0].CheckOutDate;

            var noOfNights = (providerRequest.AvailabilityRequest.CheckOutDate - providerRequest.AvailabilityRequest.CheckInDate).TotalDays.ToString();

            var BaseDestinationId = (destination.ParentDestinationId ?? destination.DestinationId);

            // Getting all provider admin fee config details
            var providerAdminConfigPrices = await this.providerAdminFeeConfigurationsRepository.GetAllDictionaryAsync();
            Money accommodationAdminFee = new Money(0, providerRequest.AvailabilityRequest.ChannelInfo.CurrencyCode);
            var accommodationAdminFeeDetails = providerAdminConfigPrices.Values.Where(x => x.ProviderId.Equals(providerRequest.Provider.Id)).Where(x => x.SupplierId.Equals(GetSupplier(providerRequest.Suppliers, providerResults[0].SupplierEdiCode))).Where(x => x.IsActiveAdminFee.Equals(true));

            var boardTypesTask = this.boardRepository.GetAllDictionaryAsync();

            var establishmentDetailsTask = this.establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(BaseDestinationId, providerRequest.AvailabilityRequest.ChannelInfo.Channel);

            // do both at the same time
            await Task.WhenAll(boardTypesTask, establishmentDetailsTask);

            var boardTypes = boardTypesTask.Result;
            var establishmentDetails = establishmentDetailsTask.Result;

            AccommodationAvailabilityResult[] availabilityResult;

            DateTime expirationTime = this.GetCacheExpireDate(providerRequest.Provider.CacheExpireTimeout);

            // Parsing user requested rooms and ages
            List<AccommodationAvailabilityRequestRoom> RequestedRooms = new List<AccommodationAvailabilityRequestRoom>();
            int requestedRoomNumber = 1;
            List<byte> ages = new List<byte>();

            providerResults[0].RoomGuests.ForEach(i => {
                AccommodationAvailabilityRequestRoom requestedRoom = new AccommodationAvailabilityRequestRoom();
                requestedRoom.RoomNumber = (byte)requestedRoomNumber;
                List<AccommodationAvailabilityRequestRoomGuest> roomAges = new List<AccommodationAvailabilityRequestRoomGuest>();
                i.Age.ForEach(j => {
                    AccommodationAvailabilityRequestRoomGuest Guest = new AccommodationAvailabilityRequestRoomGuest();
                    Guest.Age = j;
                    roomAges.Add(Guest);
                });

                requestedRoom.Guests = roomAges.ToArray();
                RequestedRooms.Add(requestedRoom);
                ++requestedRoomNumber;
            });

            providerRequest.AvailabilityRequest.Rooms = RequestedRooms.ToArray();

            var availabilityRequestedRooms = availabilityRequestRoomService.CreateAvailabilityRequestRooms(providerRequest.AvailabilityRequest);
            availabilityRequestedRooms.ParallelForEach(i => i.AvailabilityRequestId = Guid.NewGuid());

            availabilityResult = providerResults.AsParallel().Select(providerResult =>
            {
                try
                {

                    var availabilityRequestRoom = availabilityRequestedRooms[(availabilityRequestedRooms.Length == 1 ? 0 : providerResult.RoomNumber - 1)];
                    providerRequest.AvailabilityRequest.CheckInDate = providerResult.CheckInDate;
                    providerRequest.AvailabilityRequest.CheckOutDate = providerResult.CheckOutDate;

                    var result = new AccommodationAvailabilityResult();
                    result.Id = Guid.NewGuid();
                    result.RoomNumber = providerResult.RoomNumber;
                    result.AvailabilityRequestId = availabilityRequestRoom.AvailabilityRequestId;
                    result.BaseDestinationId = BaseDestinationId;
                    result.Provider = providerRequest.Provider;
                    result.ProviderId = providerRequest.Provider.Id;
                    result.Supplier = GetSupplier(providerRequest.Suppliers, providerResult.SupplierEdiCode);
                    result.SupplierId = result.Supplier.Id;
                    result.ProviderResult = providerResult;
                    result.Source = SearchResultSource.Live;
                    result.CacheExpireDate = expirationTime;
                    result.Price = providerResult.SalePrice;
                    result.SalePrice = providerResult.SalePrice;
                    result.CostPrice = providerResult.CostPrice;
                    if (providerResult.ProviderSpecificData != null)
                    {
                        result.ExchangeRate = Convert.ToDecimal(providerResult.ProviderSpecificData["exchangeRate"]);//Binding exchange rate here
                    }
                    providerResult.ProviderName = result.Supplier.Name;


                    if (accommodationAdminFeeDetails.Any() && result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect)
                    {
                        accommodationAdminFee = new Money(accommodationAdminFeeDetails.FirstOrDefault().AdminFee, providerRequest.AvailabilityRequest.ChannelInfo.CurrencyCode);
                        result.IsActiveAccommodationAdminFee = true;
                        result.AccommodationAdminFee = accommodationAdminFee;
                    }
                    else
                    {
                        result.IsActiveAccommodationAdminFee = false;
                        result.AccommodationAdminFee = accommodationAdminFee;
                    }

                    result.PricePerNight = new Money(Math.Round(providerResult.SalePrice.Amount / (Int32.Parse(noOfNights)), 2), providerResult.SalePrice.CurrencyCode);
                    result.DestinationId = destination.DestinationId;
                    result.EstablishmentId = (Guid)destination.EstablishmentId;
                    result.BoardType = GetBoardType(boardTypes, providerResult.BoardDescription);
                    result.RoomDescription = providerResult.RoomDescription.ToPascalCase();

                    roomKeyGenerator.ApplyAvailabilityRoomKey(
                        providerRequest.AvailabilityRequest.ChannelInfo.Channel,
                        availabilityRequestRoom.Room.Guests.Select(x => x.Age).ToArray(),
                        result);

                    if (providerRequest.Provider.SessionExpireTimeout != null) result.SessionExpireDate = DateTime.Now + providerRequest.Provider.SessionExpireTimeout.Value;
                    EstablishmentDetails establishmentDetailsItem;
                    if (!establishmentDetails.TryGetValue(result.EstablishmentId, out establishmentDetailsItem)) return null;
                    result.EstablishmentChainId = establishmentDetailsItem.EstablishmentChainId;
                    result.EstablishmentStarRating = establishmentDetailsItem.EstablishmentStarRating;
                    result.CountryCode = establishmentDetailsItem.CountryISOCode;

                    //Guests Mapping
                    result.AdultsCount = providerResult.Adults;
                    result.ChildrenCount = providerResult.Children;
                    result.InfantsCount = providerResult.Infants;
                    result.ProviderResult = providerResult;

                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromProviderAvailabilityResults Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, providerResult.ToIndentedJson());
                    return null;
                }
            })
            .Where(i => i != null)
            .ToArray();

            await this.GenerateRoomDescriptions(availabilityResult);

            var availabilityResults = this.roomAggregatorService.AssignRoomNumbers(availabilityRequestedRooms, availabilityResult);

            List<AccommodationB2CAvailabilityRequestRoom> requestRooms = new List<AccommodationB2CAvailabilityRequestRoom>();

            return availabilityResults;
        }
    }
}
