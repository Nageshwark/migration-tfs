﻿
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class PriceHikingService : IPriceHikingService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IPriceHikeRepository priceHikeRepository;
        private readonly IPriceHikingFactory priceHikingFactory;
        private readonly ILogger logger;

        public PriceHikingService(IAccommodationConfigurationManager configurationManager, IPriceCalculatorService priceCalculatorService, IPriceHikeRepository priceHikeRepository, ILogger logger, IPriceHikingFactory priceHikingFactory)
        {
            this.configurationManager = configurationManager;
            this.priceCalculatorService = priceCalculatorService;
            this.priceHikeRepository = priceHikeRepository;
            this.logger = logger;
            this.priceHikingFactory = priceHikingFactory;
        }

        public IList<AccommodationAvailabilityResult> ApplyAvailabilityPriceHiking(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            if (!this.configurationManager.AccommodationPriceHikingIsEnabled) return results;
            var providerIds = this.configurationManager.AccommodationPriceHikingProviderIds;
            var maxHikePercent = this.configurationManager.AccommodationPriceHikingMaxHikingPercent;
            results.Where(i => !i.ProviderResult.IsOpaqueRate).GroupBy(i => i, new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                i.EstablishmentId == j.EstablishmentId && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType))
                .AsParallel().Where(i => i.Count() > 1).Select(i => i.OrderBy(j => priceCalculatorService.CalculateRoomPrice(j)).ToArray())
                .ForAll(i => ProcessRoomPriceHiking(i, providerIds, maxHikePercent));
            return results;
        }

        public async Task<AccommodationValuationResult> ApplyValuationPriceHiking(ValuationRoomIdAvailabilityResult[] valuationRequestRooms,
            AccommodationValuationResult result, AccommodationValuationRequest valuationRequest)
        {

            List<PriceHikeThreshold> priceHikeThresholds = (await priceHikeRepository.GetPriceHikeThresholds());

            foreach (var room in result.Rooms)
            {
                ValuationRoomIdAvailabilityResult var = valuationRequestRooms.SingleOrDefault(r => r.RoomId == room.RoomId);
                if (var != null)
                {
                    AccommodationAvailabilityResult ar = valuationRequestRooms.SingleOrDefault(r => r.RoomId == room.RoomId)?.AvailabilityResult;
                    PriceHikeThreshold pht = null;

                    if (ar != null)
                    {
                        if (priceHikeThresholds.Any(s => s.ProviderId == room.Supplier.ProviderId && s.SupplierId == room.Supplier.Id))
                        {
                            pht = priceHikeThresholds.SingleOrDefault(s => s.ProviderId == room.Supplier.ProviderId && s.SupplierId == room.Supplier.Id);
                        }
                        else if (priceHikeThresholds.Any(s => s.ProviderId == room.Supplier.ProviderId && s.SupplierId == null)) // this needs checking how the null comes
                        {
                            pht = priceHikeThresholds.SingleOrDefault(s => s.ProviderId == room.Supplier.ProviderId && s.SupplierId == null);
                        }
                        room.PriceHikeDetail = ProcessValuationRoomPriceHike(pht, ar, room, var, valuationRequest.AvailabilityId);
                    }
                }
            }

            return result;
        }

        private void ProcessRoomPriceHiking(AccommodationAvailabilityResult[] groupedResults, int[] providerIds, decimal maxHikePercent)
        {
            var selectedRoom = groupedResults[0];
            if (!providerIds.Any(i => selectedRoom.Provider.Id == i))
            {
                return;
            }
            var hikePriceRoom = groupedResults.Where(i => i != selectedRoom).FirstOrDefault(i => !providerIds.Any(j => i.Provider.Id == j));
            if (hikePriceRoom == null)
            {
                return;
            }
            var selectedRoomPrice = priceCalculatorService.CalculateRoomPrice(selectedRoom);
            var hikePriceRoomPrice = priceCalculatorService.CalculateRoomPrice(hikePriceRoom);
            var hikePrice = hikePriceRoomPrice - selectedRoomPrice;
            if (hikePrice <= 0)
            {
                return;
            }
        }

        private PriceHikeDetail ProcessValuationRoomPriceHike(PriceHikeThreshold pht, AccommodationAvailabilityResult availRoom,
            AccommodationValuationResultRoom valRoom, ValuationRoomIdAvailabilityResult var, System.Guid availabilityID)
        {
            if (pht == null)
            {
                pht = priceHikingFactory.CreateFakeThreshold();
                this.logger.Debug($"No threshold configured for Provider={valRoom.Supplier.ProviderId} Supplier={valRoom.Supplier.Id}");
            }

            if (availRoom.ProviderResult.CostPrice.CurrencyCode != valRoom.CostPrice.CurrencyCode)
            {
                return priceHikingFactory.CreateEmptyResult(PriceHikeStatus.CurrencyDifference);
            }

            var toleranceCost = availRoom.ProviderResult.CostPrice.Amount * (1 + (pht.Threshold / 100));
            bool isOk = valRoom.CostPrice.Amount <= toleranceCost;

            if (!isOk)
            {
                this.logger.Error(string.Format("Price difference encountered on provider {0}. \n"
                        + "Search identifier: {7} \n"
                        + "Destination identifier: {8} \n"
                        + "Check In: {10} \n"
                        + "Check Out: {11} \n"
                        + "Guest Ages: {12} \n"
                        + "Availability cost: {1}{2} \n"
                        + "Valuation cost: {3}{4} \n"
                        + "EstablishmentID: {9} Room: {5} ({6})",
                     valRoom.ProviderResult.ProviderName, // 0
                     availRoom.ProviderResult.CostPrice.Amount,
                     availRoom.ProviderResult.CostPrice.CurrencyCode,
                     valRoom.CostPrice.Amount,
                     valRoom.CostPrice.CurrencyCode,
                     valRoom.RoomDescription, //5
                     valRoom.RoomId,
                     availabilityID,
                     valRoom.DestinationId,
                     valRoom.EstablishmentId,
                     var.SelectedRoomDetails.CheckInDate, // 10
                     var.SelectedRoomDetails.CheckOutDate,
                     System.String.Join(",", var.SelectedRoomDetails.GuestAges)));
            }

            return priceHikingFactory.CreateResult(availRoom, valRoom, isOk);
        }
    }
}
