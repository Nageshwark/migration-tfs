﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingResultBuilderService : IBookingResultBuilderService
    {
        public AccommodationBookingResult CreateBookingResult(AccommodationBookingRequest bookingRequest, IList<AccommodationBookingLiveProcessParams> bookingLiveProcessParams)
        {
            /* 
               BB-46: 
               Now rooms are identified with subsequent room number despite of number of providers requested to avoid malfunction
               with B5 connector. So this method has been simplified to fit in this new behaviour.
            */
            var bookingRoomResults = new List<AccommodationBookingResultRoom>();

            foreach (var liveProcessParams in bookingLiveProcessParams.Where(liveProcessParams => liveProcessParams.Response?.Results != null))
            {
                bookingRoomResults.AddRange(liveProcessParams.Response.Results);
            }

            return new AccommodationBookingResult()
            {
                BookingId = bookingRequest.BookingId
                ,
                BookingStatus = (bookingRoomResults.All(i => i.ProviderBookingResult.BookingStatus == BookingStatus.Confirmed) ? BookingStatus.Confirmed : BookingStatus.NotConfirmed)
                ,
                Rooms = bookingRoomResults.ToArray()
            };
        }

        private AccommodationBookingResultRoom CreateFailedBookingRoomResult(AccommodationBookingLiveProcessParams liveProcessParams, BookingRoomValuationResult bookingRoomValuationResult)
        {
            return new AccommodationBookingResultRoom
            {
                RoomNumber = bookingRoomValuationResult.RoomNumber
                ,
                Provider = liveProcessParams.Provider
                ,
                ProviderId = liveProcessParams.Provider.Id
                ,
                Supplier = bookingRoomValuationResult.ValuationResultRoom.Supplier
                ,
                SupplierId = bookingRoomValuationResult.ValuationResultRoom.Supplier.Id
                ,
                ValuationResultRoom = bookingRoomValuationResult.ValuationResultRoom
                ,
                ProviderBookingResult = new AccommodationProviderBookingResult()
                {
                    RoomNumber = bookingRoomValuationResult.RoomNumber
                    ,
                    BookingStatus = BookingStatus.NotAttempted
                }
            };
        }
    }
}
