﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
    public class DepositSchemeV1Service : IDepositSchemeV1Service
    {
        private readonly IDepositSchemeV1Repository depositSchemeRepository;

        public DepositSchemeV1Service(IDepositSchemeV1Repository depositSchemeRepository)
        {
            if (depositSchemeRepository == null)
            {
                throw new ArgumentNullException();
            }

            this.depositSchemeRepository = depositSchemeRepository;
        }


        public Task<bool> AddDepositSchemeAsync(DepositSchemeV1 depositScheme)
        {
            return this.depositSchemeRepository.AddDepositSchemeAsync(depositScheme);
        }

        public Task<bool> DeleteDepositSchemeAsync(int id)
        {
            return this.depositSchemeRepository.DeleteDepositSchemeAsync(id);
        }

        public DepositSchemeV1 GetCurrentDepositScheme()
        {
            return this.depositSchemeRepository.GetCurrentDepositScheme();
        }

        public Task<DepositSchemeV1> GetCurrentDepositSchemeAsync()
        {
            return this.depositSchemeRepository.GetCurrentDepositSchemeAsync();
        }

        public Task<DepositSchemeV1> GetDepositSchemeAsync(int id)
        {
            return this.depositSchemeRepository.GetDepositScheme(id);
        }

        public async Task<FilteredRepositoryResponse<DepositSchemeV1>> GetDepositSchemesAsync(Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, bool>> whereClause = null, Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, object>> orderBy = null, SortDirection? sortDirection = default(SortDirection?), int? skipCount = default(int?), int? takeCount = default(int?))
        {
            return await this.depositSchemeRepository.GetDepositSchemesAsync(whereClause, orderBy, sortDirection.GetValueOrDefault() == SortDirection.Descending, skipCount, takeCount);
        }

        public bool IsFlexibleDepositEnabled()
        {
            var currentScheme = this.depositSchemeRepository.GetCurrentDepositScheme();
            return currentScheme != null && currentScheme.DepositTopUpDueDaysAfterBooking.HasValue && currentScheme.DepositValue.HasValue;
        }

        public Task<bool> UpdateDepositSchemeAsync(DepositSchemeV1 depositScheme)
        {
            return this.depositSchemeRepository.UpdateDepositSchemeAsync(depositScheme);
        }
    }
}
