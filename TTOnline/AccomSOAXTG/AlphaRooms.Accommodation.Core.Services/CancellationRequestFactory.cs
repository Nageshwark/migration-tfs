﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AutoMapper;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class CancellationRequestFactory : ICancellationRequestFactory
    {
        static CancellationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationCancellationRequest, AccommodationProviderCancellationRequest>();
            }
        }

        public AccommodationProviderCancellationRequest CreateProvideCancellationRequest(AccommodationCancellationRequest cancellationRequest, AccommodationProvider provider, byte processId, CancellationRoomBookingResult[] providerRequestRooms)
        {
            var request = Mapper.Map<AccommodationCancellationRequest, AccommodationProviderCancellationRequest>(cancellationRequest);
            request.Provider = provider;
            return request;
        }
    }
}
