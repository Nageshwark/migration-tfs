﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityProcessParamService : IAvailabilityProcessParamService
    {
        private readonly ILogger logger;
        private readonly IAvailabilityRequestFactory requestFactory;
        private readonly IMappingRepository mappingRepository;
        private readonly IDestinationSearchModeService destinationSearchModeService;
        private readonly IBlockedDestinationService blockedDestinationService;
        private readonly IAccommodationSupplierRepository supplierRepository;
        private readonly IDestinationRepository destinationRepository;

        public AvailabilityProcessParamService(IAvailabilityRequestFactory requestFactory, IMappingRepository mappingRepository, ILogger logger
            , IDestinationSearchModeService destinationSearchModeService, IBlockedDestinationService blockedDestinationService, IAccommodationSupplierRepository supplierRepository
            , IDestinationRepository destinationRepository)
        {
            this.requestFactory = requestFactory;
            this.mappingRepository = mappingRepository;
            this.destinationSearchModeService = destinationSearchModeService;
            this.blockedDestinationService = blockedDestinationService;
            this.supplierRepository = supplierRepository;
            this.destinationRepository = destinationRepository;
            this.logger = logger;
        }

        public async Task<AccommodationAvailabilityLiveProcessParams[]> CreateAvailabilityLiveProcessParamsAsync(AccommodationAvailabilityRequest availabilityRequest
            , AvailabilityRoomAvailabilityKey[] availabilityRequestRooms, AccommodationAvailabilityCachedRequest[] cachedRequests, AccommodationProvider[] providers)
        {
            var requestDestination = await GetRequestDestinationIdAsync(availabilityRequest);
            var allSuppliers = await supplierRepository.GetAllDictionaryGroupByProviderIdGroupByEdiCodeAsync();
            var destinationSearchModes = await destinationSearchModeService.GetByDestinationIdAsync(requestDestination.BaseDestinationId);
            var blockedDestinations = await blockedDestinationService.GetByChannelAndDestinationIdHierarchicalAsync(availabilityRequest.ChannelInfo.Channel, requestDestination.BaseDestinationId);
            var requestMappings = await GetMappingsAsync(availabilityRequest, requestDestination);
            var liveParams = providers.AsParallel().SelectMany(provider =>
            {
                if (blockedDestinationService.IsDestinationBlocked(blockedDestinations, availabilityRequest.ChannelInfo.Channel, provider, requestDestination.BaseDestinationId)) return new AccommodationAvailabilityLiveProcessParams[0];
                var suppliers = allSuppliers[provider.Id];
                var searchMode = GetProviderSearchMode(availabilityRequest, provider, destinationSearchModes);
                if (searchMode == null) return new AccommodationAvailabilityLiveProcessParams[0];
                var validRequestRooms = availabilityRequestRooms.Where(i => i.Provider == provider && i.CachedRequest == null).ToArray();
                if (validRequestRooms.IsEmpty()) return new AccommodationAvailabilityLiveProcessParams[0];
                validRequestRooms = validRequestRooms.OrderBy(i => i.Room.RoomNumber).ToArray(); //ACS-649:Removed disctinct grouping by AvailabilityKey
                string[] providerDestinationCodes = null;
                string[] providerEstablishmentCodes = null;
                Dictionary<string, EstablishmentMapping> establishmentMappings = null;
                if (provider.IsMappingEnabled)
                {
                    ProviderMapping providerMapping;
                    if (requestMappings == null || !requestMappings.TryGetValue(provider.EdiCode, out providerMapping)) return new AccommodationAvailabilityLiveProcessParams[0];
                    providerDestinationCodes = providerMapping.DestinationCodes;
                    establishmentMappings = providerMapping.EstablishmentMappings.SelectMany(i => i.Value).ToDictionaryIgnoreDuplicated(i => i.ProviderEstablishmentCode, i => i);
                    providerEstablishmentCodes = establishmentMappings.Select(i => i.Key).ToArray();
                }
                else
                {
                    providerDestinationCodes = (availabilityRequest.DestinationId != null ? new[] { availabilityRequest.DestinationId.ToString() } : new string[0]);
                    providerEstablishmentCodes = (availabilityRequest.EstablishmentId != null ? new[] { availabilityRequest.EstablishmentId.ToString() } : new string[0]);
                }
                if (searchMode == AvailabilitySearchMode.Destination)
                {
                    return providerDestinationCodes.SplitToArrays(provider.MaxDestinationSearch ?? providerDestinationCodes.Length)
                        .SelectMany(i => ProcessMultiRoomSearchMode(availabilityRequest, requestDestination.BaseDestinationId, provider, suppliers, searchMode.Value, validRequestRooms, establishmentMappings, i, new string[0]));
                }
                return providerEstablishmentCodes.SplitToArrays(provider.MaxEstablishmentSearch ?? providerEstablishmentCodes.Length)
                    .SelectMany(i => ProcessMultiRoomSearchMode(availabilityRequest, requestDestination.BaseDestinationId, provider, suppliers, searchMode.Value, validRequestRooms, establishmentMappings, new string[0], i));
            }).ToArray();
            liveParams.ForEach((i, j) => i.ProcessId = (byte)(j + 1));
            return liveParams;
        }

        private async Task<Dictionary<string, ProviderMapping>> GetMappingsAsync(AccommodationAvailabilityRequest availabilityRequest, RequestDestination requestDestination)
        {
            if (availabilityRequest.DestinationId == requestDestination.BaseDestinationId)
            {
                return await mappingRepository.GetByBaseDestinationIdAsync(requestDestination.BaseDestinationId);
            }
            else if (availabilityRequest.DestinationId != null)
            {
                var mappingResults = await mappingRepository.GetByDestinationIdAsync(requestDestination.BaseDestinationId, availabilityRequest.DestinationId.Value);
                if (mappingResults == null)
                {
                    logger.Warn($"Cannot retrieve mappings from GetByDestinationIdAsync(BaseDestinationId={requestDestination.BaseDestinationId}, DestinationId={availabilityRequest.DestinationId.Value})");
                }
                return mappingResults;
            }
            return await mappingRepository.GetByEstablishmentIdAsync(requestDestination.BaseDestinationId, requestDestination.DestinationId, availabilityRequest.EstablishmentId.Value);
        }

        private async Task<RequestDestination> GetRequestDestinationIdAsync(AccommodationAvailabilityRequest availabilityRequest)
        {
            Destination destination = null;
            if (availabilityRequest.DestinationId != null) destination = await destinationRepository.GetByDestinationIdAsync(availabilityRequest.DestinationId.Value);
            else destination = await destinationRepository.GetByEstablishmentIdAsync(availabilityRequest.EstablishmentId.Value);
            return new RequestDestination { BaseDestinationId = (destination.ParentDestinationId ?? destination.DestinationId), DestinationId = destination.DestinationId };
        }

        private AvailabilitySearchMode? GetProviderSearchMode(AccommodationAvailabilityRequest availabilityRequest, AccommodationProvider provider, DestinationSearchMode[] destinationSearchModes)
        {
            var searchMode = provider.PreferredSearchMode;
            var destinationSearchMode = destinationSearchModeService.GetDestinationSearchModeOrNull(destinationSearchModes, provider);
            if (destinationSearchMode != null)
            {
                if (destinationSearchMode.IsEnforcedSearchMode && ((destinationSearchMode.PreferredSearchMode == AvailabilitySearchMode.Destination && provider.MaxDestinationSearch == 0)
                    || (destinationSearchMode.PreferredSearchMode == AvailabilitySearchMode.Establishment && provider.MaxEstablishmentSearch == 0))) return null;
                searchMode = destinationSearchMode.PreferredSearchMode;
            }
            if (provider.MaxEstablishmentSearch > 0 && availabilityRequest.EstablishmentId != null) searchMode = AvailabilitySearchMode.Establishment;
            if (searchMode == AvailabilitySearchMode.Destination && provider.MaxDestinationSearch == 0) searchMode = AvailabilitySearchMode.Establishment;
            if (searchMode == AvailabilitySearchMode.Establishment && provider.MaxEstablishmentSearch == 0) searchMode = AvailabilitySearchMode.Destination;
            return searchMode;
        }

        private IEnumerable<AccommodationAvailabilityLiveProcessParams> ProcessMultiRoomSearchMode(AccommodationAvailabilityRequest availabilityRequest, Guid baseDestinationId
            , AccommodationProvider provider, Dictionary<string, AccommodationSupplier> suppliers, AvailabilitySearchMode searchMode, AvailabilityRoomAvailabilityKey[] availabilityRequestRooms
            , Dictionary<string, EstablishmentMapping> establishmentMappings, string[] providerDestinationCodes, string[] ProviderEstablishmentCodes)
        {
            switch (provider.MultiRoomSearchType)
            {
                case AvailabilityMultiRoomSearchType.OnlySameRoomType:
                    return availabilityRequestRooms.GroupBy(i => i.AvailabilityKey).SelectMany(i => 
                        ProcessAccommodationAvaliabilityLiveProcessParams(availabilityRequest, baseDestinationId, provider, suppliers, searchMode, i.ToArray(), establishmentMappings, providerDestinationCodes
                        , ProviderEstablishmentCodes));

                case AvailabilityMultiRoomSearchType.OnlyDifferentRoomType:
                    return availabilityRequestRooms.GroupDifferentBy(i => i.AvailabilityKey).SelectMany(i =>
                        ProcessAccommodationAvaliabilityLiveProcessParams(availabilityRequest, baseDestinationId, provider, suppliers, searchMode, i.ToArray(), establishmentMappings, providerDestinationCodes
                        , ProviderEstablishmentCodes));

                case AvailabilityMultiRoomSearchType.Normal:
                    return ProcessAccommodationAvaliabilityLiveProcessParams(availabilityRequest, baseDestinationId, provider, suppliers, searchMode, availabilityRequestRooms, establishmentMappings
                        , providerDestinationCodes, ProviderEstablishmentCodes);

                default:
                    throw new Exception("Unexpected multi room search type " + provider.MultiRoomSearchType.ToString());
            }
        }

        private IEnumerable<AccommodationAvailabilityLiveProcessParams> ProcessAccommodationAvaliabilityLiveProcessParams(AccommodationAvailabilityRequest availabilityRequest, Guid baseDestinationId
            , AccommodationProvider provider, Dictionary<string, AccommodationSupplier> suppliers, AvailabilitySearchMode searchMode, AvailabilityRoomAvailabilityKey[] availabilityRequestRooms
            , Dictionary<string, EstablishmentMapping> establishmentMappings, string[] providerDestinationCodes, string[] ProviderEstablishmentCodes)
        {
            return availabilityRequestRooms.SplitToArrays(provider.MaxRoomSearch ?? availabilityRequestRooms.Length)
                .Select(i => CreateAccommodationAvaliabilityLiveProcessParams(availabilityRequest, baseDestinationId, provider, suppliers, searchMode
                    , establishmentMappings, providerDestinationCodes, ProviderEstablishmentCodes, i));
        }

        private AccommodationAvailabilityLiveProcessParams CreateAccommodationAvaliabilityLiveProcessParams(AccommodationAvailabilityRequest availabilityRequest, Guid baseDestinationId
            , AccommodationProvider provider, Dictionary<string, AccommodationSupplier> suppliers, AvailabilitySearchMode searchMode
            , Dictionary<string, EstablishmentMapping> establishmentMappings, string[] providerDestinationCodes, string[] ProviderEstablishmentCodes
            , AvailabilityRoomAvailabilityKey[] availabilityRequestRooms)
        {
            availabilityRequestRooms.ForEach((i, j) => i.ProviderRoomNumber = (byte)(j + 1));
            return new AccommodationAvailabilityLiveProcessParams() 
            {
                AvailabilityRequest = availabilityRequest
                , BaseDestinationId = baseDestinationId
                , SearchMode = searchMode
                , Provider = provider
                , Suppliers = suppliers
                , EstablishmentMappings = establishmentMappings
                , AvailabilityRequestRooms = availabilityRequestRooms
                , ProviderDestinationCodes = providerDestinationCodes
                , ProviderEstablishmentCodes = ProviderEstablishmentCodes
            };
        }
        
        public async Task<AccommodationAvailabilityCachedProcessParams> CreateAvailabilityCachedProcessParamsOrNullAsync(AccommodationAvailabilityRequest availabilityRequest, 
            AvailabilityRoomAvailabilityKey[] roomAvailabilityKeys, AccommodationAvailabilityCachedRequest[] cachedRequests)
        {
            if (cachedRequests.IsEmpty()) return null;
            var requestDestination = await GetRequestDestinationIdAsync(availabilityRequest);
            return new AccommodationAvailabilityCachedProcessParams()
            {
                AvailabilityRequest = availabilityRequest
                , AvailabilityRequestRooms = roomAvailabilityKeys.Where(i => i.CachedRequest != null).ToArray()
                , CachedRequests = cachedRequests 
                , Providers = cachedRequests.Select(i => i.Provider).DistinctBy(i => i.Id).ToArray()
                , BaseDestinationId = requestDestination.BaseDestinationId
            };
        }
    }

    internal class RequestDestination
    {
        public Guid BaseDestinationId { get; set; }
        public Guid DestinationId { get; set; }
    }
}
