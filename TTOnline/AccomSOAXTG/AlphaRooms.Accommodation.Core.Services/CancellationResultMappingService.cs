﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class CancellationResultMappingService : ICancellationResultMappingService
    {
        private readonly ILogger logger;

        public CancellationResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public IList<AccommodationCancellationResultRoom> MapFromProviderCancellationResults(AccommodationCancellationLiveProcessParams liveProcessParams
            , AccommodationProviderCancellationRequest providerRequest, AccommodationProviderCancellationResult[] providerResults)
        {
            return providerResults.AsParallel().Select((providerResult, index) =>
            {
                try
                {
                    var roomRequest = liveProcessParams.CancellationRequestRooms[(liveProcessParams.CancellationRequestRooms.Length == 1 ? 0 : providerResult.RoomNumber - 1)];
                    var result = new AccommodationCancellationResultRoom();
                    result.ProviderCancellationResult = providerResult;
                    result.Provider = liveProcessParams.Provider;
                    result.ProviderEdiCode = liveProcessParams.Provider.EdiCode;
                    result.RoomNumber = roomRequest.RoomNumber;
                    //result.ValuationResultRoom = roomRequest.ValuationResultRoom;
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromProviderCancellationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, providerResult.ToIndentedJson());
                    return null;
                }
            }).Where(i => i != null).ToArray();
        }
    }
}
