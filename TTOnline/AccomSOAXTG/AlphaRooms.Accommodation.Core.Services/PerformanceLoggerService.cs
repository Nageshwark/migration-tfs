﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class PerformanceLoggerService : IPerformanceLoggerService<PerformanceLog>
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(PerformanceLog).Name);
        
        private PerformanceLog Parameters { get; set; }
        private bool IsInvalid { get; set; }


        public async Task ReportAsync(PerformanceLog providerPerformanceLog)
        {
            await Task.Run(() =>
            {
                Report(providerPerformanceLog);
            });
        }

        public void Report(PerformanceLog performanceLog)
        {
            this.Parameters = performanceLog;
            Log.Debug(this);
        }

        public override string ToString()
        {
            return this.Parameters.ProductType + ","
                   + this.Parameters.OperationType + ","
                   + (IsInvalid ? "I" : (Parameters.IsSuccess ? "T" : "F")) + ","
                //+ (DateTime.UtcNow - this.Parameters.StartTime).TotalSeconds.ToString("F") + ","
                   + this.Parameters.TotalTime + ","
                   + this.Parameters.ResultCount + ","
                   + this.Parameters.RequestIdentifier + ","
                   + this.Parameters.DestinationIdentifier + ","
                   + this.Parameters.Channel + ","
                   + this.Parameters.HostName + ","
                   + this.Parameters.CacheHit;
        }
    }
}