﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using System;
    using System.Threading.Tasks;

    /*
      * Warning 
      * 
      * This file is performance critical (speed). All changes must be profiled!
      */

    public class RoomDescriptionComparer : IRoomDescriptionComparer
    {

        private readonly IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration;

        public RoomDescriptionComparer(IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration)
        {
            this.roomDescriptionKeyGeneration = roomDescriptionKeyGeneration;
        }

        public Task InitializeAsync()
        {
            return this.roomDescriptionKeyGeneration.InitializeAsync();
        }

        public void ApplyAvailabilityRoomDescriptionKey(AccommodationAvailabilityResult result)
        {
            result.RoomDescriptionKey = this.roomDescriptionKeyGeneration.GeneratKey(result.RoomDescription);
        }

        public bool EqualsRoomDescription(AccommodationAvailabilityResult result1, AccommodationAvailabilityResult result2)
        {
            return string.Equals(result1.RoomDescriptionKey, result2.RoomDescriptionKey, StringComparison.OrdinalIgnoreCase);
        }
    }
}