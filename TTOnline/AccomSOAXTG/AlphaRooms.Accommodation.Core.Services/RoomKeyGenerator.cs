﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using System;
    using System.Linq;
    using System.Text;

    public class RoomKeyGenerator : IRoomKeyGenerator
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public RoomKeyGenerator(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public void ApplyAvailabilityRoomKey(Channel channel, byte[] guestsAges, AccommodationAvailabilityResult result)
        {
            result.RoomKey = this.CreateRoomKey(
                result.Provider,
                channel,
                result.EstablishmentId,
                result.ProviderResult.CheckInDate,
                result.ProviderResult.CheckOutDate,
                guestsAges,
                result.ProviderResult.RoomCode,
                result.ProviderResult.RoomDescription,
                result.ProviderResult.BoardCode,
                result.ProviderResult.BoardDescription,
                result.ProviderResult.IsBindingRate,
                result.ProviderResult.IsNonRefundable,
                result.ProviderResult.IsOpaqueRate,
                result.ProviderResult.RateType,
                result.ProviderResult.PaymentModel,
                result.ProviderResult.CostPrice);
        }

        public void ApplyValuationRoomKey(ValuationRoomIdAvailabilityResult valuationRequestRoom)
        {
            valuationRequestRoom.RoomKey = this.CreateRoomKey(
                valuationRequestRoom.Provider,
                valuationRequestRoom.SelectedRoomDetails.Channel,
                valuationRequestRoom.SelectedRoomDetails.EstablishmentId,
                valuationRequestRoom.SelectedRoomDetails.CheckInDate,
                valuationRequestRoom.SelectedRoomDetails.CheckOutDate,
                valuationRequestRoom.SelectedRoomDetails.GuestAges,
                valuationRequestRoom.SelectedRoomDetails.ProviderRoomCode,
                valuationRequestRoom.SelectedRoomDetails.ProviderRoomDescription,
                valuationRequestRoom.SelectedRoomDetails.ProviderBoardCode,
                valuationRequestRoom.SelectedRoomDetails.ProviderBoardDescription,
                valuationRequestRoom.SelectedRoomDetails.ProviderIsBindingRate,
                valuationRequestRoom.SelectedRoomDetails.ProviderIsNonRefundable,
                valuationRequestRoom.SelectedRoomDetails.ProviderIsOpaqueRate,
                valuationRequestRoom.SelectedRoomDetails.ProviderRateType,
                valuationRequestRoom.SelectedRoomDetails.ProviderPaymentModel,
                valuationRequestRoom.SelectedRoomDetails.ProviderCostPrice);
        }

        private string CreateRoomKey(
            AccommodationProvider provider,
            Channel channel,
            Guid establishmentId,
            DateTime checkInDate,
            DateTime checkOutDate,
            byte[] guestAges,
            string roomCode,
            string roomDescription,
            string boardCode,
            string boardDescription,
            bool isBindingRate,
            bool? isNonRefundable,
            bool isOpaqueRate,
            RateType rateType,
            PaymentModel paymentModel,
            Money costPrice)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(provider.Id.ToString("00"));
            builder.Append(((int)channel).ToString("00"));
            builder.Append(establishmentId.ToShortString());
            builder.Append(checkInDate.ToString("ddMMyyyy"));
            builder.Append((checkOutDate - checkInDate).TotalDays.ToString("000"));

            if (provider.IsSearchAgeSpecific)
            {
                for (int i = 0; i < guestAges.Length; i++)
                {
                    builder.Append(guestAges[i]);
                }
            }
            else
            {
                builder.Append(guestAges.Count(i => i > configurationManager.AccommodationMaxChildAge));
                builder.Append(guestAges.Count(i => i > configurationManager.AccommodationMaxInfantAge && i <= configurationManager.AccommodationMaxChildAge));
                builder.Append(guestAges.Count(i => i <= configurationManager.AccommodationMaxInfantAge));
            }

            builder.Append(roomCode);
            builder.Append(roomDescription);
            builder.Append(boardCode);
            builder.Append(boardDescription);
            builder.Append((isBindingRate ? 1 : 0));
            builder.Append((isNonRefundable == null ? 3 : (isNonRefundable.Value ? 1 : 0)));
            builder.Append((isOpaqueRate ? 1 : 0));
            builder.Append((byte)rateType);
            builder.Append((byte)paymentModel);
            builder.Append(costPrice);

            return builder.ToString();
        }
    }
}
