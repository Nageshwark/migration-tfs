﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class CancellationLiveProcessParamService : ICancellationLiveProcessParamService
    {
        public AccommodationCancellationLiveProcessParams[] CreateCancellationLiveProcessParams(AccommodationCancellationRequest cancellationRequest, CancellationRoomBookingResult[] cancellationRequestRooms)
        {
            return cancellationRequestRooms.GroupBy(i => i.Provider).AsParallel().Select((providerRooms,j) => 
            {
                return new AccommodationCancellationLiveProcessParams()
                {
                    Provider = providerRooms.First().Provider
                    , ProcessId = (byte)(j + 1)
                    , CancellationRequest = cancellationRequest
                    , CancellationRequestRooms = providerRooms.ToArray()
                };
            }).ToArray();
        }
    }
}
