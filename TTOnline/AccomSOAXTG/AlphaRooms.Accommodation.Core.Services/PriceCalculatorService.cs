﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.SOACommon.DomainModels;
    using AlphaRooms.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PriceCalculatorService : IPriceCalculatorService
    {
        public PriceCalculatorService()
        {
        }

        public decimal CalculateRoomPrice(IAccommodationResultRoom result)
        {
            return (ArrayModule.IsNullOrEmpty(result.PriceBreakdown) ? result.SalePrice.Amount : Math.Round(result.PriceBreakdown.Sum(i => i.SalePrice.Amount), 2));
        }

        public IList<AccommodationAvailabilityResult> ApplyAvailabilityPriceCalculation(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            results.ParallelForEach(i => ProcessPriceCalculation(availabilityRequest.ChannelInfo, i));
            return results;
        }

        public AccommodationValuationResult ApplyValuationPriceCalculation(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            result.Rooms.ParallelForEach(i => ProcessPriceCalculation(valuationRequest.ChannelInfo, i));
            return result;
        }

        private void ProcessPriceCalculation(ChannelInfo channelInfo, IAccommodationResultRoom result)
        {
            result.Price = (ArrayModule.IsNullOrEmpty(result.PriceBreakdown) ? new Money(result.SalePrice.Amount, result.SalePrice.CurrencyCode) : new Money(Math.Round(result.PriceBreakdown.Sum(i => i.SalePrice.Amount), 2), result.PriceBreakdown[0].SalePrice.CurrencyCode));
            result.PricePerNight = new Money(Math.Round(result.Price.Amount / ((int)(result.ProviderResult.CheckOutDate - result.ProviderResult.CheckInDate).TotalDays), 2), result.Price.CurrencyCode);
            result.PaymentToTake = new Money((result.ProviderResult.PaymentModel != PaymentModel.CustomerPayDirect ? result.Price.Amount : 0), result.Price.CurrencyCode);
        }

    }
}
