﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationResultBuilderService : IValuationResultBuilderService
    {
        public AccommodationValuationResult CreateValuationResult(AccommodationValuationRequest valuationRequest, IList<AccommodationValuationLiveProcessParams> valuationLiveProcessParams)
        {
            var valuationRoomResults = new AccommodationValuationResultRoom[valuationRequest.SelectedRoomIds.Length];
            for (byte i = 1; i <= valuationRoomResults.Length; i++)
            {
                var liveProcessParams = valuationLiveProcessParams.First(j => j.ValuationRequestRooms.Any(k => k.RoomNumber == i));
                var valuationRoomResult = (liveProcessParams.Response != null ? liveProcessParams.Response.Results.FirstOrDefault(j => j.RoomNumber == i) : null);
                if (valuationRoomResult == null) throw new Exception("Valuation id " + valuationRequest.ValuationId.ToString() + " room number " + i.ToString() + " not available."
                    , (liveProcessParams.Response != null ? liveProcessParams.Response.Exception : null));
                valuationRoomResults[i - 1] = valuationRoomResult;
            }
            return new AccommodationValuationResult()
            {
                AvailabilityId = valuationRequest.AvailabilityId
                , ValuationId = valuationRequest.ValuationId
                , Rooms = valuationRoomResults
            };
        }
    }
}
