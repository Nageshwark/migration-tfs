﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.Utilities.Text;
    using System.Linq;

    public class AvailabilityResultRoomIdGenerator : IAvailabilityResultRoomIdGenerator
    {
        public void ApplyAvailabilityRoomId(AccommodationAvailabilityRequest availabilityRequest, AvailabilityRoomAvailabilityKey[] availabilityRequestRooms, AccommodationAvailabilityResult result)
        {
            result.RoomId = CreateRoomId(
                availabilityRequest.ChannelInfo.Channel,
                availabilityRequestRooms.First(j => j.AvailabilityRequestId == result.AvailabilityRequestId).Room.Guests.Select(x => x.Age).ToArray(),
                result);
        }

        public string CreateRoomId(Channel channel, byte[] guests, AccommodationAvailabilityResult result)
        {
            var writer = new StringCompressWriter();
            writer.AppendInt(result.Provider.Id, 2);
            writer.AppendInt(result.Supplier.Id, 5);
            writer.AppendEnum(channel, 2);
            writer.AppendGuid(result.EstablishmentId);
            writer.AppendDate(result.ProviderResult.CheckInDate);
            writer.AppendInt((int)(result.ProviderResult.CheckOutDate - result.ProviderResult.CheckInDate).TotalHours, 4);
            writer.AppendByte(result.RoomNumber, 1);
            writer.AppendInt(guests.Length, 1);

            foreach (var guest in guests)
            {
                writer.AppendByte(guest, 2);
            }

            writer.AppendString(result.ProviderResult.RoomCode);
            writer.AppendString(result.ProviderResult.RoomDescription);
            writer.AppendString(result.ProviderResult.BoardCode);
            writer.AppendString(result.ProviderResult.BoardDescription);
            writer.AppendBoolean(result.ProviderResult.IsBindingRate);
            writer.AppendBooleanOrNull(result.ProviderResult.IsNonRefundable);
            writer.AppendBoolean(result.ProviderResult.IsOpaqueRate);
            writer.AppendEnum(result.ProviderResult.RateType, 1);
            writer.AppendEnum(result.ProviderResult.PaymentModel, 1);
            writer.AppendString(result.ProviderResult.CostPrice.ToString());
            writer.AppendGuid(result.Id);

            return writer.ToString();
        }

        public AccommodationAvailabilityResultRoomIdWrapper Parse(string roomId)
        {
            var reader = new StringCompressReader(roomId);
            var providerId = reader.ReadInt(2);
            var supplierId = reader.ReadInt(5);
            var channel = reader.ReadEnum<Channel>(2);
            var establishmentId = reader.ReadGuid();
            var checkInDate = reader.ReadDate();
            var checkOutDate = checkInDate.AddHours(reader.ReadInt(4));
            var roomNumber = reader.ReadByte(1);

            var guestAges = new byte[reader.ReadInt(1)];
            for (int i = 0; i < guestAges.Length; i++)
            {
                guestAges[i] = reader.ReadByte(2);
            }

            var providerRoomCode = reader.ReadString();
            var providerRoomDescription = reader.ReadString();
            var providerBoardCode = reader.ReadString();
            var providerBoardDescription = reader.ReadString();
            var isBindingRate = reader.ReadBoolean();
            var isNonRefundable = reader.ReadBoolean();
            var isOpaqueRate = reader.ReadBoolean();
            var rateType = reader.ReadEnum<RateType>(1);
            var providerPaymentModel = reader.ReadEnum<PaymentModel>(1);
            var providerCostPriceString = reader.ReadString();
            var providerCostPrice = Money.Parse(providerCostPriceString);
            var id = reader.ReadGuid();

            return new AccommodationAvailabilityResultRoomIdWrapper(
                providerId,
                supplierId,
                channel,
                establishmentId,
                checkInDate,
                checkOutDate,
                roomNumber,
                guestAges,
                providerRoomCode,
                providerRoomDescription,
                providerBoardCode,
                providerBoardDescription,
                isBindingRate,
                isNonRefundable,
                isOpaqueRate,
                rateType,
                providerPaymentModel,
                providerCostPrice,
                id);
        }
    }
}
