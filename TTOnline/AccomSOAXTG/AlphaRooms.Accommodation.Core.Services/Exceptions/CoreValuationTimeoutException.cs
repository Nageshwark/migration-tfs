﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services.Exceptions
{
    public class CoreValuationTimeoutException : Exception
    {
        private readonly AccommodationValuationRequest valuationRequest;
        private readonly TimeSpan valuationTimeout;

        public CoreValuationTimeoutException(AccommodationValuationRequest valuationRequest, TimeSpan valuationTimeout)
            : base("Core valuation id " + valuationRequest.ValuationId + " process timed out(ValuationTimeout=" + valuationTimeout + ").")
        {
            this.valuationRequest = valuationRequest;
            this.valuationTimeout = valuationTimeout;
        }

        public AccommodationValuationRequest ValuationRequest { get { return this.valuationRequest; } }

        public TimeSpan ValuationTimeout { get { return this.valuationTimeout; } }
    }
}
