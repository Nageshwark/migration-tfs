﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services.Exceptions
{
    public class CoreAvailabilityTimeoutException : Exception 
    {
        private readonly AccommodationAvailabilityRequest availabilityRequest;
        private readonly TimeSpan accommodationAvailabilitySearchTimeout;
        private readonly List<AccommodationAvailabilityProcessParamsBase> processParams;

        public CoreAvailabilityTimeoutException(AccommodationAvailabilityRequest availabilityRequest, List<AccommodationAvailabilityProcessParamsBase> processParams, TimeSpan accommodationAvailabilitySearchTimeout)
            : base("Core availability id " + availabilityRequest.AvailabilityId + " process timed out(SearchTimeout=" + accommodationAvailabilitySearchTimeout
                  + (processParams.Any(i => i is AccommodationAvailabilityLiveProcessParams) ? ", LiveProcesses=" + processParams.OfType<AccommodationAvailabilityLiveProcessParams>().Select(i => i.Provider.Id).Distinct().ToString(",") : null)
                  + (processParams.Any(i => i is AccommodationAvailabilityCachedProcessParams) ? ", CachedProcesses=" + processParams.OfType<AccommodationAvailabilityCachedProcessParams>().First().Providers.Select(i => i.Id).ToString(",") : null)
                  + ").")
        {
            this.availabilityRequest = availabilityRequest;
            this.processParams = processParams;
            this.accommodationAvailabilitySearchTimeout = accommodationAvailabilitySearchTimeout;
        }

        public AccommodationAvailabilityRequest AvailabilityRequest { get { return this.availabilityRequest; } }

        public TimeSpan AccommodationAvailabilitySearchTimeout { get { return this.accommodationAvailabilitySearchTimeout; } }

        public List<AccommodationAvailabilityProcessParamsBase> ProcessParams { get { return this.processParams; } }
    }
}
