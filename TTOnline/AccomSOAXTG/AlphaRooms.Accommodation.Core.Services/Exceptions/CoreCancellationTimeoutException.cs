﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services.Exceptions
{
    public class CoreCancellationTimeoutException : Exception
    {
        private readonly AccommodationCancellationRequest cancellationRequest;
        private readonly TimeSpan cancellationTimeout;

        public CoreCancellationTimeoutException(AccommodationCancellationRequest cancellationRequest, TimeSpan cancellationTimeout)
            : base("Core cancellation id " + cancellationRequest.CancellationId + " process timed out(CancellationTimeout=" + cancellationTimeout + ").")
        {
            this.cancellationRequest = cancellationRequest;
            this.cancellationTimeout = cancellationTimeout;
        }
        
        public AccommodationCancellationRequest CancellationRequest { get { return this.cancellationRequest; } }

        public TimeSpan CancellationTimeout { get { return this.cancellationTimeout; } }
    }
}
