﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Services.Exceptions
{
    public class CoreBookingTimeoutException : Exception
    {
        private readonly AccommodationBookingRequest bookingRequest;
        private readonly TimeSpan bookingTimeout;

        public CoreBookingTimeoutException(AccommodationBookingRequest bookingRequest, TimeSpan bookingTimeout)
            : base("Core booking id " + bookingRequest.BookingId + " process timed out(BookingTimeout=" + bookingTimeout + ").")
        {
            this.bookingRequest = bookingRequest;
            this.bookingTimeout = bookingTimeout;
        }

        public AccommodationBookingRequest BookingRequest { get { return this.bookingRequest; } }

        public TimeSpan BookingTimeout { get { return this.bookingTimeout; } }
    }
}
