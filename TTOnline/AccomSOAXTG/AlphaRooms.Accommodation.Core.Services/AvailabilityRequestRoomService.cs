﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityRequestRoomService : IAvailabilityRequestRoomService
    {
        private readonly IAvailabilityKeyGenerator keyGenerator;
        private readonly IAccommodationConfigurationManager configurationManager;
        
        public AvailabilityRequestRoomService(IAvailabilityKeyGenerator keyGenerator, IAccommodationConfigurationManager configurationManager)
        {
            this.keyGenerator = keyGenerator;
            this.configurationManager = configurationManager;
        }

        public AvailabilityRoomAvailabilityKey[] CreateAvailabilityRequestRooms(AccommodationAvailabilityRequest availabilityRequest)
        {
            var requests = availabilityRequest.Rooms.SelectMany(i => availabilityRequest.Providers.Select(j => CreateAvailabilityRoomAvailabilityKey(availabilityRequest, i, j))).ToArray();
            requests.GroupBy(i => i.AvailabilityKey).ParallelForEach(i => ProcessSetDuplicatedRoom(i.ToArray()));
            return requests;
        }

        private void ProcessSetDuplicatedRoom(AvailabilityRoomAvailabilityKey[] availabilityRequestRoom)
        {
            var originalRoom = availabilityRequestRoom[0];
            for (int i = 1; i < availabilityRequestRoom.Length; i++) availabilityRequestRoom[i].DuplicatedRequestRoom = originalRoom;
        }

        private AvailabilityRoomAvailabilityKey CreateAvailabilityRoomAvailabilityKey(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityRequestRoom requestRoom
            , AccommodationProvider provider)
        {
            return new AvailabilityRoomAvailabilityKey()
            {
                Room = requestRoom
                , Provider = provider
                , AvailabilityKey = keyGenerator.CreateAvailabilityKey(availabilityRequest, requestRoom, provider)
                , AdultsCount = requestRoom.Guests.CountByte(i => i.Age > configurationManager.AccommodationMaxChildAge)
                , ChildrenCount = requestRoom.Guests.CountByte(i => i.Age > configurationManager.AccommodationMaxInfantAge && i.Age <= configurationManager.AccommodationMaxChildAge)
                , InfantsCount = requestRoom.Guests.CountByte(i => i.Age <= configurationManager.AccommodationMaxInfantAge)
            };
        }

        public void SetAvailabilityCacheRequest(AvailabilityRoomAvailabilityKey[] availabilityRequestRooms, AccommodationAvailabilityCachedRequest[] cachedRequests)
        {
            availabilityRequestRooms.ParallelForEach(i =>
            {
                i.CachedRequest = cachedRequests.FirstOrDefault(j => j.AvailabilityKey == i.AvailabilityKey);
                i.AvailabilityRequestId = (i.CachedRequest != null ? i.CachedRequest.Id : Guid.NewGuid());
            });
        }
    }
}
