﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Interfaces;

    public class RoomDescriptionComparerFactory : IRoomDescriptionComparerFactory
    {
        private readonly IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration;

        public RoomDescriptionComparerFactory(IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration)
        {
            this.roomDescriptionKeyGeneration = roomDescriptionKeyGeneration;
        }

        /// <summary>
        /// Responsible for creating a CachedRoomDescriptionComparer, as it is not threadsafe create a new one on every request.
        /// </summary>
        /// <returns>a new CachedRoomDescriptionComparer</returns>
        public IRoomDescriptionComparer Create()
        {
            return new CachedRoomDescriptionComparer(this.roomDescriptionKeyGeneration);
        }
    }
}
