﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System.Collections.Concurrent;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class CancellationService : ICancellationService
    {
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly ICancellationLiveProcessParamService liveProcessService;
        private readonly ICancellationRequestFactory requestFactory;
        private readonly IProviderCancellationService providerCancellationService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICancellationResultCaching resultCaching;
        private readonly ICancellationResultMappingService resultMappingService;
        private readonly ICancellationResultBuilderService resultBuilderService;
        private readonly IDiParameterContainer diParameterContainer;

        public CancellationService(IAccommodationProviderRepository accommodationProviderRepository, ICancellationLiveProcessParamService liveProcessService
            , ICancellationRequestFactory requestFactory, IProviderCancellationService providerCancellationService, IAccommodationConfigurationManager configurationManager
            , ICancellationResultCaching resultCaching, ICancellationResultMappingService resultMappingService, ICancellationResultBuilderService resultBuilderService
            , IDiParameterContainer diParameterContainer)
        {
            this.accommodationProviderRepository = accommodationProviderRepository;
            this.liveProcessService = liveProcessService;
            this.requestFactory = requestFactory;
            this.providerCancellationService = providerCancellationService;
            this.configurationManager = configurationManager;
            this.resultCaching = resultCaching;
            this.resultMappingService = resultMappingService;
            this.resultBuilderService = resultBuilderService;
            this.diParameterContainer = diParameterContainer;
        }

        public async Task<AccommodationCancellationResponse> GetCancellationResponseAsync(Guid cancellationId)
        {
            return new AccommodationCancellationResponse() { Result = await this.resultCaching.GetByCancellationIdOrNullAsync(cancellationId) };
        }

        public async Task<AccommodationCancellationResponse> ProcessCancellationAsync(AccommodationCancellationRequest cancellationRequest)
        {
            //TODO get list of bookings relate to this itinerary number
            string[] bookingItems = null;

            // create a list containing the selected room details
            var cancellationRooms = bookingItems.Select(i => new CancellationRoomBookingResult() { }).ToArray();
              
            //  create the necessary live process parameters based on the selected rooms and their providers
            var liveProcessParams = liveProcessService.CreateCancellationLiveProcessParams(cancellationRequest, cancellationRooms);

            // create tasks to perform cancellation
            var tasks = liveProcessParams.Select(i => Task.Run(async() => await ProcessCancellationLiveProviderAsync(i))).ToArray();

            // process the tasks, wait for them to complete or the process to timeout
            var searchTask = Task.Run(async() => await Task.WhenAll(tasks));
            await Task.WhenAny(searchTask, Task.Delay(this.configurationManager.AccommodationValuationTimeout));
            
            // throw an exception if the process timed out
            if (searchTask.Status != TaskStatus.RanToCompletion && liveProcessParams.All(i => i.Response == null)) throw new CoreCancellationTimeoutException(cancellationRequest, this.configurationManager.AccommodationValuationTimeout);

            // get cancellation room results
            IList<AccommodationCancellationResultRoom> cancellationRoomResults = liveProcessParams.Where(i => i.Response != null && i.Response.Exception == null).SelectMany(i => i.Response.Results).ToArray();

            // verify cancellation results and generate a cancellation package
            var result = resultBuilderService.CreateCancellationResult(cancellationRequest, cancellationRoomResults);

            // save noncached results to the db
            await resultCaching.SaveAsync(result);

            return new AccommodationCancellationResponse()
            {
                Result = result
                , ProcessResponseDetails = liveProcessParams.Select(i => new AccommodationCancellationResponseProviderDetails()
                {
                    Provider = i.Provider
                    , CancellationRequestRooms = i.CancellationRequestRooms
                    , ResultsCount = (i.Response != null ? (int?)i.Response.Results.Count : null)
                    , TimeTaken = (i.Response != null ? (TimeSpan?)i.Response.TimeTaken : null)
                    , Exception = (i.Response != null ? i.Response.Exception : null)
                }).ToArray()
            };
        }

        private async Task ProcessCancellationLiveProviderAsync(AccommodationCancellationLiveProcessParams liveProcessParams)
        {
            AccommodationProviderCancellationRequest providerRequest = null;
            var providerRequests = new ConcurrentQueue<AccommodationProviderRequest>();
            AccommodationProviderCancellationResponse providerResponse = null;
            IList<AccommodationCancellationResultRoom> mappedResults = null;
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            try
            {
                // create provider request
                providerRequest = requestFactory.CreateProvideCancellationRequest(liveProcessParams.CancellationRequest, liveProcessParams.Provider, liveProcessParams.ProcessId, liveProcessParams.CancellationRequestRooms);

                // register provider logger parameter in the di container
                this.diParameterContainer.Register(providerRequest, providerRequests);

                // perform provider live search
                providerResponse = await providerCancellationService.GetProviderCancellationResponseAsync(providerRequest);

                // map results to core availability result 
                mappedResults = resultMappingService.MapFromProviderCancellationResults(liveProcessParams, providerRequest, providerResponse.Results.ToArray());
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // unregister provider logger parameter in the di container
            if (providerRequest != null) this.diParameterContainer.Unregister(providerRequest);

            // return provider specific results list 
            liveProcessParams.Response = new CancellationProcessResponse()
            {
                Results = mappedResults ?? new AccommodationCancellationResultRoom[0]
                , ProviderRequests = providerRequests.ToArray()
                , Exception = exception
                , TimeTaken = DateTime.Now - startDate
            };
        }
    }
}
