﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.Interfaces;

    public class ProviderPerformanceLoggerService : IPerformanceLoggerService<ProviderPerformanceLog>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ProviderPerformanceLog).Name);

        private ProviderPerformanceLog parameters;

        public async Task ReportAsync(ProviderPerformanceLog providerPerformanceLog)
        {
            await Task.Run(() =>
            {
                Report(providerPerformanceLog);
            });
        }

        public void Report(ProviderPerformanceLog providerPerformanceLog)
        {
            this.parameters = providerPerformanceLog;
            ProviderPerformanceLoggerService.Log.Debug(this);
        }

        public override string ToString()
        {
            return this.parameters.ProductType + ","
                + this.parameters.OperationType + ","
                + (this.parameters.IsSuccess ? "T" : "F") + ","
                + this.parameters.TotalTime + ","
                + this.parameters.ResultCount + ","
                + this.parameters.RequestIdentifier + ","
                + this.parameters.DestinationIdentifier + ","
                + this.parameters.Provider + ","
                + this.parameters.Arrival?.ToShortDateString() + ","
                + this.parameters.Departure?.ToShortDateString() + ","
                + this.parameters.Channel + ","
                + this.parameters.HostName + ","
                + this.parameters.CacheHit;
        }
    }
}