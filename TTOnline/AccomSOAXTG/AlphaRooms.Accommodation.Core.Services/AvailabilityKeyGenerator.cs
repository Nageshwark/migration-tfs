﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityKeyGenerator : IAvailabilityKeyGenerator
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public AvailabilityKeyGenerator(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public string CreateAvailabilityKey(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityRequestRoom room, AccommodationProvider provider)
        {
            StringBuilder builder = new StringBuilder(availabilityRequest.ChannelInfo.CountryCode);
            builder.Append(((int)availabilityRequest.ChannelInfo.Channel).ToString("00"));
            builder.Append(ParseSearchType(availabilityRequest.SearchType));
            builder.Append(provider.Id.ToString("00"));
            builder.Append(availabilityRequest.CheckInDate.ToString("yyyyMMdd"));
            builder.Append((availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate).TotalDays.ToString("000"));
            builder.Append(availabilityRequest.EstablishmentId == null ? 'D' + availabilityRequest.DestinationId.Value.ToShortString() : 'E' + availabilityRequest.EstablishmentId.Value.ToShortString());
            builder.Append(room.Guests.Count(i => i.Age > provider.MaxChildAge));
            builder.Append(room.Guests.Count(i => i.Age > provider.MaxInfantAge && i.Age <= provider.MaxChildAge));
            builder.Append(room.Guests.Count(i => i.Age <= provider.MaxInfantAge));
            return builder.ToString();
        }

        private string ParseSearchType(SearchType searchType)
        {
            switch (searchType)
            {
                case SearchType.FlightAndHotel:
                    return "FHs";
                case SearchType.HotelOnly:
                    return "HOs";
                case SearchType.FlightOnly:
                    return "FOs";
                default:
                    throw new ArgumentOutOfRangeException(nameof(searchType), searchType, null);
            }
        }
    }
}
