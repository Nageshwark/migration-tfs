﻿namespace AlphaRooms.Accommodation.Core.Services
{
	using AlphaRooms.Accommodation.Core.Contracts;
	using AlphaRooms.Accommodation.Core.Interfaces;
	using AlphaRooms.Accommodation.Core.Interfaces.Caching;
	using AlphaRooms.Accommodation.Core.Provider.Contracts;
	using AlphaRooms.Accommodation.Core.Provider.Interfaces;
	using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
	using AlphaRooms.Accommodation.Core.Services.Exceptions;
	using AlphaRooms.SOACommon.Interfaces;
	using AlphaRooms.Utilities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;
	using AlphaRooms.Utilities.ExtensionMethods;
	using Provider.DomainModels;
	using B2C.Interfaces;
	using System.Diagnostics;

	public class AvailabilityService : IAvailabilityService, IAvailabilityLiveProcessService
    {
        private readonly IProviderAvailabilityService providerAvailabilityService;
        private readonly IAvailabilityCachedRequestService cachedRequestService;
        private readonly IAvailabilityResultCaching resultsCaching;
        private readonly IAvailabilityProcessParamService processParamService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAvailabilityResultMappingService resultMappingService;
        private readonly IAvailabilityResultAggregatorService roomAggregatorService;
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly IAvailabilityRequestFactory requestFactory;
        private readonly IPriceCalculatorService priceCalculatorService;
        private readonly IAvailabilityWinEventLogger eventLogger;
        private readonly IAvailabilityRequestRoomService availabilityRequestRoomService;
        private readonly IAvailabilityResultLowerPriceService resultLowerPriceService;
        private readonly IAvailabilityResultConsolidationService resultConsolidationService;
        private readonly IAvailabilityResultMarkupService resultMarkupService;
        private readonly IRoomDescriptionGenerator resultRoomDescriptionService;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IPriceHikingService priceHikingService;
        private readonly IRoomSwitchingService roomSwitchingService;
        private readonly IAvailabilityAlternativeSearchService alternativeSearchService;
        private readonly IAvailabilityFlashSaleService flashSaleService;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;
        private readonly ICoreGuidService coreGuidService;
        //private readonly IB2CRequestStatusService requestStatusService;
        private readonly IAccommodationSupplierRepository supplierRepository;
		private readonly IB2CEventLogInfo b2cEventLogInfo;

		public AvailabilityService(IProviderAvailabilityService providerAvailabilityService, IAvailabilityCachedRequestService cachedRequestService, IAvailabilityResultCaching resultsCaching
            , IAvailabilityProcessParamService processParamService, IAccommodationConfigurationManager configurationManager, IAccommodationProviderRepository accommodationProviderRepository
            , IAvailabilityRequestFactory requestFactory, IPriceCalculatorService priceCalculatorService, IAvailabilityResultMappingService resultMappingService
            , IAvailabilityResultAggregatorService roomAggregatorService, IAvailabilityWinEventLogger eventLogger, IAvailabilityRequestRoomService availabilityRequestRoomService
            , IAvailabilityResultLowerPriceService resultsLowerPriceService, IAvailabilityResultConsolidationService consolidationService, IAvailabilityResultMarkupService markupService
            , IRoomDescriptionGenerator roomDescriptionService, IAvailabilityResultRoomIdGenerator roomIdGenerator, IPriceHikingService priceHikingService
            , IRoomSwitchingService roomSwitchingService, IAvailabilityAlternativeSearchService alternativeSearchService, IAvailabilityFlashSaleService flashSaleService
            , AccommodationPromotionalDiscountService promotionalDiscountService, ICoreGuidService coreGuidService, IAccommodationSupplierRepository supplierRepository, IB2CEventLogInfo b2cEventLogInfo)
        {
            this.providerAvailabilityService = providerAvailabilityService;
            this.cachedRequestService = cachedRequestService;
            this.resultsCaching = resultsCaching;
            this.processParamService = processParamService;
            this.configurationManager = configurationManager;
            this.resultMappingService = resultMappingService;
            this.accommodationProviderRepository = accommodationProviderRepository;
            this.roomAggregatorService = roomAggregatorService;
            this.requestFactory = requestFactory;
            this.priceCalculatorService = priceCalculatorService;
            this.eventLogger = eventLogger;
            this.availabilityRequestRoomService = availabilityRequestRoomService;
            this.resultLowerPriceService = resultsLowerPriceService;
            this.resultConsolidationService = consolidationService;
            this.resultMarkupService = markupService;
            this.resultRoomDescriptionService = roomDescriptionService;
            this.roomIdGenerator = roomIdGenerator;
            this.priceHikingService = priceHikingService;
            this.roomSwitchingService = roomSwitchingService;
            this.alternativeSearchService = alternativeSearchService;
            this.flashSaleService = flashSaleService;
            this.promotionalDiscountService = promotionalDiscountService;
            this.coreGuidService = coreGuidService;
            //this.requestStatusService = requestStatusService;
            this.supplierRepository = supplierRepository;
			this.b2cEventLogInfo = b2cEventLogInfo;
		}

        public async Task<AccommodationAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationAvailabilityRequest availabilityRequest)
        {
            AccommodationAvailabilityCachedProcessParams cachedProcessParams = null;
            AccommodationAvailabilityCachedRequest[] cachedRequests = null;

            var startTimer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Start");
			Stopwatch stopwatch = new Stopwatch();

			// remove destinationid if this is a establishment search
			if (availabilityRequest.DestinationId != null && !availabilityRequest.EstablishmentId.IsNullOrEmpty())
            {
                availabilityRequest.DestinationId = null;
            }

            // create all the availability keys
            var availabilityRequestRooms = availabilityRequestRoomService.CreateAvailabilityRequestRooms(availabilityRequest);

            if (availabilityRequest.ProviderCacheEnabled)
            {
                // check the room results already in the cache
                var cacheTimer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Start_GetCachedRequests");
				stopwatch.Start();
				cachedRequests = await cachedRequestService.GetCachedRequestsOrNullAsync(availabilityRequestRooms.Select(i => i.AvailabilityKey).Distinct().ToArray(),
                                                                                         availabilityRequest.Providers, 
                                                                                         this.coreGuidService.IsMetaGuid(availabilityRequest.AvailabilityId));
                cacheTimer.Record();
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: check the room results already in the cache, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

				// set availability rooms correct cached results
				cacheTimer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Start_SetCacheRequest");
				stopwatch.Start();
				availabilityRequestRoomService.SetAvailabilityCacheRequest(availabilityRequestRooms, cachedRequests);
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: set availability rooms correct cached results, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
				cacheTimer.Record();

                // create a cache process in case there are cached content
                cachedProcessParams = await processParamService.CreateAvailabilityCachedProcessParamsOrNullAsync(availabilityRequest, availabilityRequestRooms, cachedRequests);
            }
            else
            {
                availabilityRequestRooms.ParallelForEach(i => i.AvailabilityRequestId = (i.CachedRequest != null ? i.CachedRequest.Id : Guid.NewGuid()));
            }

			// create a process list to get live room results
			stopwatch.Start();
            var liveProcessParams = await processParamService.CreateAvailabilityLiveProcessParamsAsync(availabilityRequest, availabilityRequestRooms, cachedRequests, availabilityRequest.Providers);
			stopwatch.Stop();
			b2cEventLogInfo.LogTrace("B2C Trace: create a process list to get live room results, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

			// Create tasks to get live data from providers
			stopwatch.Start();
			var tasks = liveProcessParams.Select(i => Task.Run(async () => await ProcessAvailabilityLiveProviderAsync(i))).ToList();
			stopwatch.Stop();
			b2cEventLogInfo.LogTrace("B2C Trace: Create tasks to get live data from providers, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

			// create a task to get cached data from cached providers
			if (cachedProcessParams != null)
            {
				tasks.Add(Task.Run(async () => await ProcessAvailabilityCachedProvidersAsync(cachedProcessParams)));
			}

            // we have finished starting the search, record the time taken
            startTimer.Record();

            // process the tasks, wait for them to complete or the process to timeout
            var searchTask = Task.WhenAll(tasks);
            await Task.WhenAny(searchTask, Task.Delay(this.configurationManager.AccommodationAvailabilitySearchTimeout));

            // create a list containing all the executed processes
            var processParams = liveProcessParams.Cast<AccommodationAvailabilityProcessParamsBase>().ToList();
            if (cachedProcessParams != null)
            {
                processParams.Add(cachedProcessParams);
            }

            // log availability provider exceptions in eventlog
            eventLogger.Log(availabilityRequest, processParams);

            // throw an exception if all the process timed out
            if (searchTask.Status != TaskStatus.RanToCompletion && processParams.All(i => i.Response == null))
            {
                throw new CoreAvailabilityTimeoutException(availabilityRequest, processParams, this.configurationManager.AccommodationAvailabilitySearchTimeout);
            }

            // capture post process start date
            var postProcessStartDate = DateTime.Now;

            // get all successful processes
            var successfulProcesses = processParams.Where(i => i.Response != null && i.Response.Exception == null).ToArray();

            // get successful live process responses
            var successfulLiveProcesses = successfulProcesses.OfType<AccommodationAvailabilityLiveProcessParams>().ToArray();

            // if there were any successful live processes
            AccommodationAvailabilityCachedRequest[] nonCachedRequests;
            if (successfulLiveProcesses.Any() && availabilityRequest.ProviderCacheEnabled)
            {
                // save only results from providers with core cache active
                var successfulLiveProcessesToCache = successfulLiveProcesses.Where(i => i.Provider.IsCoreCacheActive).ToArray();

                // get all results from live processes. Clone results object to prevent race conditions
                var nonCachedResults = successfulLiveProcessesToCache.SelectMany(i => i.Response.Results.Select(s => s.Clone()).Cast<AccommodationAvailabilityResult>()).ToList();

#pragma warning disable 4014
                Task.Run(async () =>
                {
                    // save noncached results to the db
                    if (nonCachedResults.Any())
                    {
                        await resultsCaching.SaveRangeAsync(liveProcessParams[0].BaseDestinationId, nonCachedResults, this.coreGuidService.IsMetaGuid(availabilityRequest.AvailabilityId));
                    }
                    // create cache request for the live processes
                    nonCachedRequests = cachedRequestService.CreateCacheRequests(availabilityRequest, successfulLiveProcessesToCache);
                    // save cache request
                    if (nonCachedRequests.Any())
                    {
                        await cachedRequestService.SaveRangeAsync(nonCachedRequests, this.coreGuidService.IsMetaGuid(availabilityRequest.AvailabilityId));
                    }
                });
#pragma warning restore 4014
            }
            else
            {
                // no new cache requests created for this search
                nonCachedRequests = new AccommodationAvailabilityCachedRequest[0];
            }

            // get cached and non cached results
            IList<AccommodationAvailabilityResult> results = successfulProcesses.SelectMany(i => i.Response.Results).ToArray();

            // perform alternative search if this is an establishment search and it returned no results
            if (results.IsEmpty() && availabilityRequest.EstablishmentId != null)
            {
                var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_ProcessAlternativeSearch");
				stopwatch.Start();
				var res = await alternativeSearchService.ProcessAlternativeSearchAsync(availabilityRequest);
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: perform alternative search if this is an establishment search and it returned no results, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
				timer.Record();
                return res;
            }

            // execute post process for the results
            if (results.Any())
            {
                var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess");
				stopwatch.Start();
				results = PostProcessAvailabilityResults(availabilityRequest, results, availabilityRequestRooms);
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: execute post process for the results, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
				timer.Record();
            }

            // get cache expire threshold
            var cacheExpireDateThreshold = resultsCaching.GetCacheExpireDateThreshold();

            var accommodationAvailabilityResponse = new AccommodationAvailabilityResponse()
            {
                Results = results ?? new AccommodationAvailabilityResult[0],
                ResultsCacheExpireDate = (results.Any(i => i.CacheExpireDate > cacheExpireDateThreshold) ? results.Where(i => i.CacheExpireDate > cacheExpireDateThreshold).Min(i => i.CacheExpireDate)
                    : cacheExpireDateThreshold + configurationManager.AccommodationAvailabilityEmptyResultsExpireTimeout),
                PostProcessStartDate = postProcessStartDate,
                ProcessResponseDetails = processParams.Select(i => new AccommodationAvailabilityResponseProviderDetails()
                {
                    Providers = (i is AccommodationAvailabilityLiveProcessParams ? new[] { ((AccommodationAvailabilityLiveProcessParams)i).Provider } : ((AccommodationAvailabilityCachedProcessParams)i).Providers),
                    RoomNumbers = i.AvailabilityRequestRooms.Select(j => j.Room.RoomNumber).ToArray(),
                    IsLiveResults = i is AccommodationAvailabilityLiveProcessParams,
                    ResultsCount = i.Response?.Results.Count,
                    TimeTaken = i.Response?.TimeTaken,
                    Exception = i.Response?.Exception
                }).ToArray()
            };

            return accommodationAvailabilityResponse;
        }

        private IList<AccommodationAvailabilityResult> PostProcessAvailabilityResults(AccommodationAvailabilityRequest availabilityRequest,IList<AccommodationAvailabilityResult> results,AvailabilityRoomAvailabilityKey[] availabilityRequestRooms)
        {
            // DO NOT ADD HERE any kind of business logic

            // set extra values set room description
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_RoomDescriptions");
            results.ParallelForEach(result =>
            {
                // set room description
                resultRoomDescriptionService.ApplyAvailabilityRoomDescriptionFormat(availabilityRequest, result);
                // set room ids
                roomIdGenerator.ApplyAvailabilityRoomId(availabilityRequest, availabilityRequestRooms, result);
            });
            timer.Record();

            return results.ToArray();
        }

        public async Task ProcessAvailabilityLiveProviderAsync(AccommodationAvailabilityLiveProcessParams liveProcessParams)
        {
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            IList<AccommodationAvailabilityResult> mappedResults = null;
			Stopwatch stopwatch = new Stopwatch();

			try
            {
                // create provider request
                var providerRequest = requestFactory.CreateProviderAvailabilityRequest(
                    liveProcessParams.AvailabilityRequest,
                    liveProcessParams.Provider,
                    liveProcessParams.ProcessId,
                    liveProcessParams.ProviderDestinationCodes,
                    liveProcessParams.ProviderEstablishmentCodes,
                    liveProcessParams.AvailabilityRequestRooms);

				// perform provider live search
				stopwatch.Start();
				var providerResponse = await providerAvailabilityService.GetProviderAvailabilityResponseAsync(providerRequest);
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: perform provider live search, SearchID: " + liveProcessParams.AvailabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

				var timer = new SearchTimerService(liveProcessParams.AvailabilityRequest.AvailabilityId, "Search_ProviderAvailability_PostProcessing");

                providerResponse.Results = this.DeleteFreeRooms(providerResponse.Results);

                // map results to core availability result 
                mappedResults = await this.resultMappingService.MapFromProviderAvailabilityResultsAsync(liveProcessParams, providerRequest, providerResponse.Results.ToArray());

                // Reorganising roomNumbers
                mappedResults = this.roomAggregatorService.AssignRoomNumbers(liveProcessParams.AvailabilityRequestRooms, mappedResults);

                // for establishment searches return only results for a specific establishment
                if (liveProcessParams.AvailabilityRequest.EstablishmentId != null)
                {
                    mappedResults = mappedResults.Where(i => i.EstablishmentId == liveProcessParams.AvailabilityRequest.EstablishmentId).ToArray();
                }

                timer.Record();
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // return provider specific results list
            liveProcessParams.Response = new AvailabilityProcessResponse()
            {
                Results = mappedResults ?? new AccommodationAvailabilityResult[0],
                IsLiveResults = true,
                Exception = exception,
                TimeTaken = DateTime.Now - startDate
            };
        }
       
        public async Task<AccommodationAvailabilityLiveProcessParams[]> CreateMetaAvailabilityLiveProcessParamsAsync(AccommodationAvailabilityRequest availabilityRequest)
        {
            var allSuppliers = await supplierRepository.GetAllDictionaryGroupByProviderIdGroupByEdiCodeAsync();
            List<AccommodationAvailabilityLiveProcessParams> liveParams = new List<AccommodationAvailabilityLiveProcessParams>();
            availabilityRequest.Providers.ForEach(i =>
            {
                AccommodationAvailabilityLiveProcessParams liveParam = new AccommodationAvailabilityLiveProcessParams();
                liveParam.Provider = i;
                liveParam.AvailabilityRequest = availabilityRequest;
                liveParam.Suppliers = allSuppliers[i.Id];
                liveParams.Add(liveParam);
            });

            liveParams.ForEach((i, j) => i.ProcessId = (byte)(j + 1));
            return liveParams.ToArray();
        }
        public async Task<AccommodationAvailabilityResponse> GetMetaAvailabilityResponseAsync( AccommodationAvailabilityRequest availabilityRequest)
		{
            DateTime startDate = DateTime.Now;
            var liveProcessParams = await this.CreateMetaAvailabilityLiveProcessParamsAsync(availabilityRequest);

            // Create tasks to get live data from providers
            var tasks = liveProcessParams.Select(i => Task.Run(async () => await this.processMetaAvailablityResponsec(i))).ToList();

            var searchTask = Task.WhenAll(tasks);
            await Task.WhenAny(searchTask, Task.Delay(this.configurationManager.AccommodationAvailabilitySearchTimeout));            

            // create a list containing all the executed processes
            var processParams = liveProcessParams.Cast<AccommodationAvailabilityProcessParamsBase>().ToList();
           
            // get all successful processes
            var successfulProcesses = processParams.Where(i => i.Response != null && i.Response.Exception == null).ToArray();
            
            IList<AccommodationAvailabilityResult> results = successfulProcesses.SelectMany(i => i.Response.Results).ToArray();

            var cacheExpireDateThreshold = resultsCaching.GetCacheExpireDateThreshold();

            var accommodationAvailabilityResponse = new AccommodationAvailabilityResponse()
            {
                Results = results ?? new AccommodationAvailabilityResult[0],
                ResultsCacheExpireDate = (results.Any(i => i.CacheExpireDate > cacheExpireDateThreshold) ? results.Where(i => i.CacheExpireDate > cacheExpireDateThreshold).Min(i => i.CacheExpireDate)
                  : cacheExpireDateThreshold + configurationManager.AccommodationAvailabilityEmptyResultsExpireTimeout),
                ProcessResponseDetails = processParams.Select(i => new AccommodationAvailabilityResponseProviderDetails()
                {
                    IsLiveResults = (bool)i.Response?.IsLiveResults,
                    ResultsCount = i.Response?.Results.Count,
                    TimeTaken = i.Response?.TimeTaken,
                    Exception = i.Response?.Exception,
                    Providers = availabilityRequest.Providers
                }).ToArray()
            };

            return accommodationAvailabilityResponse;
        }

        private async Task processMetaAvailablityResponsec(AccommodationAvailabilityLiveProcessParams liveParam)
        {
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            IList<AccommodationAvailabilityResult> mappedResults = null;

            try
            {
                var providerRequest = requestFactory.CreateMetaProviderAvailabilityRequest(liveParam.AvailabilityRequest,liveParam.Provider, liveParam.ProcessId);

                // perform provider live search
                var providerResponse = await providerAvailabilityService.GetProviderAvailabilityResponseAsync(providerRequest);

                // map results to core availability result 
                mappedResults = await this.resultMappingService.MapMetaProviderAvailabilityResultsAsync(liveParam, providerResponse.Results.ToArray());      
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // return provider specific results list
            liveParam.Response = new AvailabilityProcessResponse()
            {
                Results = mappedResults ?? new AccommodationAvailabilityResult[0],
                IsLiveResults = true,
                Exception = exception,
                TimeTaken = DateTime.Now - startDate
            };
        }

        private async Task ProcessAvailabilityCachedProvidersAsync(AccommodationAvailabilityCachedProcessParams cachedProcessParams)
        {
            Exception exception = null;
            DateTime startDate = DateTime.Now;
            IList<AccommodationAvailabilityResult> results = null;

            try
            {
                // get results for all the cache providers
                results = await resultsCaching.GetByAvailabilityRequestIdsAsync(cachedProcessParams.BaseDestinationId, cachedProcessParams.CachedRequests.Select(i => i.Id).ToArray()
                    , cachedProcessParams.CachedRequests.Select(i => i.Provider).Distinct().ToArray(), this.coreGuidService.IsMetaGuid(cachedProcessParams.AvailabilityRequest.AvailabilityId));

                // do AggregateRooms logic here
                results = roomAggregatorService.AggregateRooms(cachedProcessParams.AvailabilityRequest, cachedProcessParams.AvailabilityRequestRooms, results);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // return as a single collection
            cachedProcessParams.Response = new AvailabilityProcessResponse()
            {
                Results = results ?? new AccommodationAvailabilityResult[0],
                Exception = exception,
                TimeTaken = DateTime.Now - startDate
            };
        }

        private IEnumerable<AccommodationProviderAvailabilityResult> DeleteFreeRooms(IEnumerable<AccommodationProviderAvailabilityResult> results)
        {
            if (results.Any(r => r.SalePrice.Amount <= 0m))
            {
                this.RoomReport(results.Where(r => r.SalePrice.Amount <= 0m));

                return results.Where(r => r.SalePrice.Amount > 0m);
            }

            return results;
        }

        private void RoomReport(IEnumerable<AccommodationProviderAvailabilityResult> roomList)
        {
            // Log message can be improved. Stores as info.
            var groupedRooms = roomList
                .GroupBy(x => new { x.SupplierEdiCode, x.EstablishmentEdiCode, x.RoomDescription })
                .Distinct();

            Parallel.ForEach(groupedRooms, item =>
            {
                this.eventLogger.LogWarn("Provider: [" + item.Key.SupplierEdiCode + "] Establishment: [" + item.Key.EstablishmentEdiCode + "] Room: [" + item.Key.RoomDescription + "]. Room found with 0 price or less.");
            });
        }
    }
}
