﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AgeRuleService : IAgeRuleService
    {
        public void ApplyAvailabilityGuestTypeRule(AccommodationProvider provider, AccommodationProviderAvailabilityRequestGuest guest)
        {
            guest.Type = CreateProviderGuestType(provider, guest.Age);
        }

        public void ApplyBookingCustomerTypeRule(AccommodationProvider provider, AccommodationProviderBookingRequestCustomer customer)
        {
            customer.Type = CreateProviderGuestType(provider, customer.Age);
        }

        public void ApplyBookingGuestTypeRule(AccommodationProvider provider, AccommodationProviderBookingRequestRoomGuest guest)
        {
            guest.Type = CreateProviderGuestType(provider, guest.Age);
        }

        public ProviderAge CalculateProviderAges(AccommodationProvider provider, byte[] guestAges)
        {
            var guestTypes = guestAges.Select(i => CreateProviderGuestType(provider, i)).ToArray();
            return new ProviderAge()
            {
                Provider = provider
                , Adults = guestTypes.CountByte(i => i == GuestType.Adult)
                , Children = guestTypes.CountByte(i => i == GuestType.Child)
                , Infants = guestTypes.CountByte(i => i == GuestType.Infant)
            };
        }

        private GuestType CreateProviderGuestType(AccommodationProvider provider, byte age)
        {
            if (age >= 0 && age <= provider.MaxInfantAge) return GuestType.Infant;
            else if (age > provider.MaxInfantAge && age <= provider.MaxChildAge) return GuestType.Child;
            return GuestType.Adult; 
        }
    }
}
