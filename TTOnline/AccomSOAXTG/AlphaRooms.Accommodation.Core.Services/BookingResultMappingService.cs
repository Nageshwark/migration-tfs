﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class BookingResultMappingService : IBookingResultMappingService
    {
        private readonly ILogger logger;

        public BookingResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public IList<AccommodationBookingResultRoom> MapFromProviderBookingResults(AccommodationBookingLiveProcessParams liveProcessParams
            , AccommodationProviderBookingRequest providerRequest, AccommodationProviderBookingResult[] providerResults)
        {
            return providerResults.AsParallel().Select((providerResult, index) =>
            {
                try
                {
                    var roomRequest = liveProcessParams.BookingRequestRooms[(liveProcessParams.BookingRequestRooms.Length == 1 ? 0 : providerResult.RoomNumber - 1)];
                    var result = new AccommodationBookingResultRoom();
                    result.ProviderBookingResult = providerResult;
                    result.Provider = liveProcessParams.Provider;
                    result.ProviderId = result.Provider.Id;
                    result.Supplier = roomRequest.ValuationResultRoom.Supplier;
                    result.SupplierId = result.Supplier.Id;
                    result.RoomNumber = roomRequest.RoomNumber;
                    result.ValuationResultRoom = roomRequest.ValuationResultRoom;
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromProviderBookingResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, providerResult.ToIndentedJson());
                    return null;
                }
            }).Where(i => i != null).ToArray();
        }
    }
}
