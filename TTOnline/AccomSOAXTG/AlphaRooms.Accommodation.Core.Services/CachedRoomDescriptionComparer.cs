﻿namespace AlphaRooms.Accommodation.Core.Services
{
    using Contracts;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Responsible for proxying IRoomDescriptionKeyGeneration caching the results to save time and cpu on calls
    /// </summary>
    public class CachedRoomDescriptionComparer : IRoomDescriptionComparer
    {
        private readonly Dictionary<string, string> cachedRoomDescriptionKeys = 
            new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase); // the keys are not case case sensitive

        private readonly IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration;

        public CachedRoomDescriptionComparer(IRoomDescriptionKeyGeneration roomDescriptionKeyGeneration)
        {
            this.roomDescriptionKeyGeneration = roomDescriptionKeyGeneration;
        }

        public Task InitializeAsync()
        {
            return this.roomDescriptionKeyGeneration.InitializeAsync();
        }

        public void ApplyAvailabilityRoomDescriptionKey(AccommodationAvailabilityResult availabilityResult)
        {
            string roomDescriptionKey;

            if (!this.cachedRoomDescriptionKeys.TryGetValue(availabilityResult.RoomDescription, out roomDescriptionKey))
            {
                // cache miss
                roomDescriptionKey = this.roomDescriptionKeyGeneration.GeneratKey(availabilityResult.RoomDescription);

                // populate dictionary
                this.cachedRoomDescriptionKeys[availabilityResult.RoomDescription] = roomDescriptionKey;
            }

            availabilityResult.RoomDescriptionKey = roomDescriptionKey;
        }

        public bool EqualsRoomDescription(AccommodationAvailabilityResult result1, AccommodationAvailabilityResult result2)
        {
            throw new NotImplementedException();
        }
    }
}
