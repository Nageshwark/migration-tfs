﻿using System;

namespace AlphaRooms.Accommodation.Core.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Interfaces.Caching;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AvailabilityCachedRequestService : IAvailabilityCachedRequestService
    {
        private readonly IAvailabilityCachedRequestCaching caching;

        public AvailabilityCachedRequestService(IAvailabilityCachedRequestCaching caching)
        {
            this.caching = caching;
        }

        public AccommodationAvailabilityCachedRequest[] CreateCacheRequests(AccommodationAvailabilityRequest availabilityRequest, AccommodationAvailabilityLiveProcessParams[] successfulLiveProcesses)
        {
            DateTime expiration = successfulLiveProcesses.Select(x => x.Response.Results.Min(y => y.CacheExpireDate)).Single();
            return successfulLiveProcesses.SelectMany(i => i.AvailabilityRequestRooms.Select(j => CreateAccommodationAvailabilityCachedRequest(j, expiration)))
                .Distinct(new CustomEqualityComparer<AccommodationAvailabilityCachedRequest>((k,l) => k.AvailabilityKey == l.AvailabilityKey && k.Provider.Id == l.Provider.Id)).ToArray();
        }

        private AccommodationAvailabilityCachedRequest CreateAccommodationAvailabilityCachedRequest(AvailabilityRoomAvailabilityKey roomRequest, DateTime expiration)
        {
            return new AccommodationAvailabilityCachedRequest()
            {
                Id = roomRequest.AvailabilityRequestId
                , Provider = roomRequest.Provider
                , ProviderId = roomRequest.Provider.Id
                , AvailabilityKey = roomRequest.AvailabilityKey
                , CacheExpireDate = expiration
            };
        }

        public async Task<AccommodationAvailabilityCachedRequest[]> GetCachedRequestsOrNullAsync(string[] availabilityKeys, AccommodationProvider[] providers, bool isMeta = false)
        {
            return await caching.GetByAvailabilityKeyOrNullAsync(availabilityKeys, providers, isMeta);
        }

        public async Task SaveRangeAsync(IList<AccommodationAvailabilityCachedRequest> cachedRequests, bool isMeta = false)
        {
            await caching.SaveRangeAsync(cachedRequests, isMeta);
        }
    }
}
