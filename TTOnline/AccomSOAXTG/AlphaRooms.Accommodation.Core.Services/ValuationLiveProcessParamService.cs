﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class ValuationLiveProcessParamService : IValuationLiveProcessParamService
    {
        public AccommodationValuationLiveProcessParams[] CreateValuationLiveProcessParams(AccommodationValuationRequest valuationRequest, ValuationRoomIdAvailabilityResult[] valuationRooms)
        {
            return valuationRooms.GroupBy(i => i.AvailabilityResult.Provider).AsParallel().SelectMany(providerRooms =>
            {
                if (providerRooms.Key.ContractDependencyMode == ContractDependencyMode.Dependent)
                {
                    return providerRooms.GroupBy(i => i.AvailabilityResult.ProviderResult.Contract)
                        .SelectMany(i => i.SplitToArrays(providerRooms.Key.MaxRoomValuation != null ? providerRooms.Key.MaxRoomValuation.Value : providerRooms.Count())
                        .Select((j, k) => CreateAccommodationValuationLiveProcessParams(valuationRequest, providerRooms.Key, k, j)));
                }
                return providerRooms.SplitToArrays(providerRooms.Key.MaxRoomValuation != null ? providerRooms.Key.MaxRoomValuation.Value : providerRooms.Count())
                    .Select((i, j) => CreateAccommodationValuationLiveProcessParams(valuationRequest, providerRooms.Key, j, i));
            }).ToArray();
        }

        private AccommodationValuationLiveProcessParams CreateAccommodationValuationLiveProcessParams(AccommodationValuationRequest valuationRequest, AccommodationProvider provider, int index
            , ValuationRoomIdAvailabilityResult[] providerRooms)
        {
            providerRooms.ForEach((i, j) => i.ProviderRoomNumber = (byte)(j + 1));
            return new AccommodationValuationLiveProcessParams()
            {
                Provider = provider
                , ProcessId = (byte)(index + 1)
                , ValuationRequest = valuationRequest
                , ValuationRequestRooms = providerRooms
            };
        }
    }
}
