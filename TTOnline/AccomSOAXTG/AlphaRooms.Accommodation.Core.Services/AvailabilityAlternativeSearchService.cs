﻿using AlphaRooms.Accommodation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.Core.Services
{
    public class AvailabilityAlternativeSearchService : IAvailabilityAlternativeSearchService
    {
        private readonly IAvailabilityServiceFactory availabilityServiceFactory;
        private readonly IDestinationRepository destinationRepository;

        static AvailabilityAlternativeSearchService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationAvailabilityRequest, AccommodationAvailabilityRequest>();
            }
        }

        public AvailabilityAlternativeSearchService(IAvailabilityServiceFactory availabilityServiceFactory, IDestinationRepository destinationRepository)
        {
            this.availabilityServiceFactory = availabilityServiceFactory;
            this.destinationRepository = destinationRepository;
        }

        public async Task<AccommodationAvailabilityResponse> ProcessAlternativeSearchAsync(AccommodationAvailabilityRequest availabilityRequest)
        {
            var destination = await destinationRepository.GetByEstablishmentIdAsync(availabilityRequest.EstablishmentId.Value);
            var alternativeRequest = CreateAlternativeAvailabilityRequest(availabilityRequest, destination);
            var alternativeResponse = await availabilityServiceFactory.CreateAvailabilityService().GetAvailabilityResponseAsync(alternativeRequest);
            return alternativeResponse;
        }

        private AccommodationAvailabilityRequest CreateAlternativeAvailabilityRequest(AccommodationAvailabilityRequest availabilityRequest, Destination destination)
        {
            var alternativeRequest = Mapper.Map<AccommodationAvailabilityRequest, AccommodationAvailabilityRequest>(availabilityRequest);
            alternativeRequest.DestinationId = destination.DestinationId;
            alternativeRequest.EstablishmentId = null;
            return alternativeRequest;
        }
    }
}
