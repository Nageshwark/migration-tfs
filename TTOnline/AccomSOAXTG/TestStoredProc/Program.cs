﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Contracts;
using AlphaRooms.Accommodation.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Utilities;

namespace TestStoredProc 
{
    public class Program : DbContext
    {
        static Program()
        {
            Database.SetInitializer<Program>(null);
        }

        public Program() : base("Name=AlphabedsEntities")
        {
        }

        //public Program() : base(@"Data Source=STORMDEV\Dev;Initial Catalog=Alphabeds;Persist Security Info=True;User ID=DevUser;Password=TgmZ3mrxhJUUX22;MultipleActiveResultSets=True")
        //{   
        //}
        
        public static void Main(string[] args)
        {
           
            DateTime arrivalDate = new DateTime(2015,08,15);
            DateTime departureDate = new DateTime(2015, 08, 22);
            Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B");

            List<DirectProviderAvailabilityResponse> localAvailabilityResults = GetLocalRoomsAsync(arrivalDate, departureDate, destinationId, 2, 0, 0, 0, 0, 0, "A", null, 1);

            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            Async.ParallelForEach(localAvailabilityResults, async (availabilityResult) =>
            {
                try
                {
                    if (availabilityResult.dclTotalSaleRO != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("RO", "Room Only", availabilityResult, availabilityResult.dclTotalCostRO, availabilityResult.dclTotalSaleRO));

                    if (availabilityResult.dclTotalSaleSC != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("SC", "Self Catering", availabilityResult, availabilityResult.dclTotalCostSC, availabilityResult.dclTotalSaleSC));

                    if (availabilityResult.dclTotalSaleBB != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("BB", "Bed & Breakfast", availabilityResult, availabilityResult.dclTotalCostBB, availabilityResult.dclTotalSaleBB));

                    if (availabilityResult.dclTotalSaleHB != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("HB", "Half Board", availabilityResult, availabilityResult.dclTotalCostHB, availabilityResult.dclTotalSaleHB));

                    if (availabilityResult.dclTotalSaleFB != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("FB", "Full Board", availabilityResult, availabilityResult.dclTotalCostFB, availabilityResult.dclTotalSaleFB));

                    if (availabilityResult.dclTotalSaleAI != null)
                        availabilityResults.Enqueue(await GetRoomToAdd("AI", "All Inclusive", availabilityResult, availabilityResult.dclTotalCostAI, availabilityResult.dclTotalSaleAI));
                }

                catch (Exception ex)
                {
                    //logger.Error("DirectProvider: Error while adding rooms - " + ex.Message);
                }

            });

            Console.ReadLine();
        }

        //Fetch the available local rooms from the database
        public static List<DirectProviderAvailabilityResponse> GetLocalRoomsAsync(DateTime arrivalDate,
            DateTime departureDate, Guid fkDestinationId, int numberOfAdults, int numberOfChildren, int? childAge1,
            int? childAge2, int? childAge3, int? childAge4, string supplierId, Guid? establishmentId, int channelId)
        {
           Program obj = new Program();
            
            return
                (((IObjectContextAdapter)obj).ObjectContext.ExecuteStoreQuery<DirectProviderAvailabilityResponse>
                    (
                        "spSelectAvailableRoomsByDestination "
                        + "@dtmArrival, @dtmDeparture, @fkDestinationID, @intAdult, @intChild, @intChildAge1, "
                        + "@intChildAge2, @intChildAge3, @intChildAge4, @fkEdiProvider, @EstablishmentID, @ChannelID"
                        , new SqlParameter("dtmArrival", arrivalDate)
                        , new SqlParameter("dtmDeparture", departureDate)
                        , new SqlParameter("fkDestinationID", fkDestinationId)
                        , new SqlParameter("intAdult", numberOfAdults)
                        , new SqlParameter("intChild", numberOfChildren)
                        , new SqlParameter("intChildAge1", DBNull.Value)
                        , new SqlParameter("intChildAge2", DBNull.Value)
                        , new SqlParameter("intChildAge3", DBNull.Value)
                        , new SqlParameter("intChildAge4", DBNull.Value)
                        , new SqlParameter("fkEdiProvider", supplierId)
                        , new SqlParameter("EstablishmentID", DBNull.Value)
                        , new SqlParameter("ChannelID", channelId)
                    )).ToList();
        }

        private static async Task<AccommodationProviderAvailabilityResult> GetRoomToAdd(string boardType, string boardDescription, DirectProviderAvailabilityResponse localAvailability, Decimal? costPrice, Decimal? salePrice)
        {
            return new AccommodationProviderAvailabilityResult()
            {
                //RoomId = localAvailability.fkRoomId, // int??
                Supplier = localAvailability.strSupplierName,
                SupplierEdiCode = "A",
                EstablishmentEdiCode = localAvailability.strEdiCode,
                DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                NumberOfAdults = 2, //for the time being we consider only single requests
                NumberOfChildren = 0,
                BoardCode = boardType,
                BoardDescription = boardDescription.ToUpper(),
                OriginalBoardDescription = boardDescription.ToUpper(),
               // BoardType = boardType, // int??
                RoomCode = localAvailability.fkRoomId.ToString(),
                RoomDescription = localAvailability.strRoomTypeName,
                OriginalRoomDescription = localAvailability.strRoomTypeName,
                CostPrice = new Money(costPrice.Value, "GBP"),
                SalePrice = new Money(salePrice.Value, "GBP"),
                IncomingContract = localAvailability.CancellationTerms,
                PaymentModel = (PaymentModel)localAvailability.fkPaymentModel,
                IsBindingRate = (localAvailability.blnIsBindingRate == 1),
                EstablishmentName = localAvailability.strEstablishmentName,
                EstablishmentKey = new Guid(localAvailability.strEdiCode),
                Destination = localAvailability.strDestinationName,
                DestinationId = "151E8FB1-A132-4BB6-BB06-52800733614B"

            };

        }
    }

    public class DirectProviderAvailabilityResponse
    {
        public Guid fkRoomId { get; set; }
        public string strSupplierName { get; set; }
        public string strEstablishmentName { get; set; }
        public string strCategoryName { get; set; }
        public string strDestinationName { get; set; }
        public string strRoomTypeName { get; set; }
        public int blnIsBindingRate { get; set; }
        public int fkPaymentModel { get; set; }
        public decimal? Margin { get; set; }
        public int intMaxAgeChild { get; set; }
        public int intMaxAgeInfant { get; set; }
        public string strZoneName { get; set; }
        public string strISOCurrencyCode { get; set; }
        public string strEdiCode { get; set; }
        public string strRoomTypeInternalName { get; set; }
        public decimal? dclTotalCostRO { get; set; }
        public decimal? dclTotalCostSC { get; set; }
        public decimal? dclTotalCostBB { get; set; }
        public decimal? dclTotalCostHB { get; set; }
        public decimal? dclTotalCostFB { get; set; }
        public decimal? dclTotalCostAI { get; set; }
        public decimal? dclTotalSaleRO { get; set; }
        public decimal? dclTotalSaleSC { get; set; }
        public decimal? dclTotalSaleBB { get; set; }
        public decimal? dclTotalSaleHB { get; set; }
        public decimal? dclTotalSaleFB { get; set; }
        public decimal? dclTotalSaleAI { get; set; }
        public DateTime dtmEarliestStart { get; set; }
        public DateTime dtmLatestEnd { get; set; }
        public int intQuantityAvailable { get; set; }
        public int intQuantityBooked { get; set; }
        public string CancellationTerms { get; set; }
        public string ColumnName { get; set; }
    }
    
}
