﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories
{
    public interface IAutoSuggestRepository
    {
        List<AutoSuggest> GetAutoSuggestionsForHotelsAndDestinations(string term, int localeId);
    }
}
