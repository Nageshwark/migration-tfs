﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CCancellationLogger
    {
        Task LogAsync(AccommodationB2CCancellationRequest cancellationRequest, AccommodationB2CRequestStatus requestStatus, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeSpan, Exception exception);
    }
}
