﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2COnlineBookabilityService
    {
        DestinationHotelAutoSuggest GetHotelAndDestinationAutoSuggestions(string term, Channel channel);
    }
}
