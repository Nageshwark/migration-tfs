﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CBookingRequestValidator
    {
        void ValidateBookingRequest(AccommodationB2CBookingRequest bookingRequest);
    }
}
