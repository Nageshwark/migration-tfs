﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CValuationLogger
    {
        Task LogAsync(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CValuationRequest valuationRequest, AccommodationB2CRequestStatus requestStatus, AccommodationValuationResponse coreResponse, TimeSpan timeTaken, Exception exception);
    }
}
