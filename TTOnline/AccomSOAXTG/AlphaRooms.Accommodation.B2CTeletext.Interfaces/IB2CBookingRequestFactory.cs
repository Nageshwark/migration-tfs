﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CBookingRequestFactory
    {
        TTAccommodationBookingRequest CreateBookingRequest(AccommodationB2CBookingRequest bookingRequest, ChannelInfo channelInfo);
    }
}
