﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2CBookingService = AlphaRooms.Accommodation.B2CTeletext.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CItineraryBookingFactory
    {
        Task<B2CBookingService.CreateItineraryRequest> CreateItineraryRequestAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationB2CValuationResult B2CValuationResult, TTAccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms, TTAccommodationBookingRequestRoomPaymentDetails paymentDetails);
        B2CBookingService.UpdateBookingItemRequest CreateUpdateBookingItemRequest(Itinerary itinerary, AccommodationBookingRoom accommodationBookingRoom, AccommodationBookingResult bookingResult, AccommodationBookingResultRoom bookingRoomResult);
        B2CBookingService.DetermineItineraryStatusRequest CreateDetermineAndUpdateItineraryRequest(Itinerary itinerary);
    }
}
