﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CValuationResultMappingService
    {
        Task<AccommodationB2CValuationResult> MapFromCoreValuationResultsAsync(AccommodationB2CValuationRequest valuationRequest, AccommodationValuationResult coreAvailabilityResponse);
    }
}
