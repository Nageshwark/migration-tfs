﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CItineraryBookingService
    {
        Task<Itinerary> CreateItineraryAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationB2CValuationResult b2cValuationResult, TTAccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms, TTAccommodationBookingRequestRoomPaymentDetails paymentDetails);
        Task UpdateItineraryAsync(Itinerary itinerary, AccommodationBookingResult bookingResult);
    }
}
