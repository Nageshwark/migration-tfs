﻿namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    using System;

    public interface IB2CGuidService
    {
        Guid GenerateGuid(bool IsMeta);
        bool IsMetaGuid(Guid? guid);
    }
}