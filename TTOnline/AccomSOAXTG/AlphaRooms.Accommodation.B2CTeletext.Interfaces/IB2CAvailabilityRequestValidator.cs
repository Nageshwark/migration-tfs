﻿namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;

    public interface IB2CAvailabilityRequestValidator
    {
        void ValidateAvailabilityRequest(AccommodationB2CAvailabilityRequest availabilityRequest);
    }
}
