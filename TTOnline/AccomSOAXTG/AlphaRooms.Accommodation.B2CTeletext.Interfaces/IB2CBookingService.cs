﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CBookingService
    {
        Task<Guid> StartBookingAsync(AccommodationB2CBookingRequest bookingRequest);

        Task<AccommodationB2CBookingResponse> GetBookingResponseAsync(Guid bookingId);

        Task<AccommodationB2CBookingResponse> StartAndGetBookingResponseAsync(AccommodationB2CBookingRequest bookingRequest);
    }
}
