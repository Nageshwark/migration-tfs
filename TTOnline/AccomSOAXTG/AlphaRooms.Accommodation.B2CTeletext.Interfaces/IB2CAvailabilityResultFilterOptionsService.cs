﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CAvailabilityResultFilterOptionsService
    {
        AccommodationB2CAvailabilityResultsFilterOptions CreateEmptyFilterOptions(AccommodationB2CAvailabilityRequest availabilityRequest, ChannelInfo channelInfo);
        Task<AccommodationB2CAvailabilityResultsFilterOptions> CreateResultsFilterOptionsAsync(AccommodationB2CAvailabilityRequest availabilityRequest, ChannelInfo channelInfo, IList<AccommodationB2CAvailabilityResult> results);
        AccommodationB2CAvailabilityFilterOptionDetails CreateResultsFilterDetails(AccommodationB2CAvailabilityResultsFilterOptions resultsFilterOptions, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilityResult[] accommodationResults);
        Expression<Func<AccommodationB2CAvailabilityResult, bool>> CreateEstablishmentAccommodationFilter(AccommodationB2CAvailabilityFilter filterCriteria, FilterOptions exceptFilter);
        Expression<Func<AccommodationB2CAvailabilityResultRoom, bool>> CreateRoomAccommodationFilter(AccommodationB2CAvailabilityFilter filterCriteria, FilterOptions exceptFilter);
        B2CResultsRangeFilterOptions CreateRange(decimal[] prices, decimal? minPrice = null, decimal? maxPrice = null);
    }

    public enum FilterOptions
    {
        None
        , Board
        , District
        , Facilities
        , Payment
        , StarRatings
        , Suppliers
        , Recommendedfor
    }
}
