﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IDestinationService
    {
        List<AutoSuggest> GetAutoSuggestionsHotelAndDestination(string term, Channel channel);
    }
}
