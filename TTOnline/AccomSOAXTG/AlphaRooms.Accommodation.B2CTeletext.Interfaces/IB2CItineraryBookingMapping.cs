﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using B2CBookingService = AlphaRooms.Accommodation.B2CTeletext.Interfaces.B2BBookingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CItineraryBookingMapping
    {
        Itinerary MapToItinerary(B2CBookingService.CreateItineraryResponse response);
    }
}
