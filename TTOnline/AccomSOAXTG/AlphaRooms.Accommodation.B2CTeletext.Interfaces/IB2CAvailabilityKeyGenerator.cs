﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CAvailabilityKeyGenerator
    {
        string CreateAvailabilityKey(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationProvider[] providers);
    }
}
