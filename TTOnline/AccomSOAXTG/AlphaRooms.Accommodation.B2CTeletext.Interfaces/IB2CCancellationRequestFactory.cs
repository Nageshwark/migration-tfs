﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CCancellationRequestFactory
    {
        AccommodationCancellationRequest CreateCancellationRequest(AccommodationB2CCancellationRequest bookingRequest, ChannelInfo channelInfo);
    }
}
