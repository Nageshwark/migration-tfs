﻿namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces.Caching
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IB2CAvailabilityResultCaching
    {
        Task<AccommodationB2CAvailabilityResults> GetFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, int roomCount, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool debugging, bool isMeta = false);

        Task<AccommodationB2CAvailabilityResult> GetByEstablishmentIdAsync(Guid availabilityId, Guid baseDestinationId, Guid establishmentId, bool isMeta = false);

        Task<AccommodationB2CAvailabilityResult> GetByEstablishmentIdFilteredAndSortedAsync(Guid availabilityId, Guid baseDestinationId, Guid establishment, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool debugging, bool isMeta = false);

        Task<AccommodationB2CAvailabilityResult[]> GetByRoomIdsAsync(Guid availabilityId, Guid baseDestinationId, uint[] roomIds, bool isMeta = false);

        Task SaveRangeAsync(Guid baseDestinationId, IEnumerable<AccommodationB2CAvailabilityResult> results, bool isMeta = false);

        Task DeleteRangeAsync(Guid baseDestinationId, Guid availabilityId, bool isMeta = false);
		Task<AccommodationB2CAvailabilityResult[]> GetByTokenKeyAsync(Guid availabilityId, Guid baseDestinationId, bool isMeta = false);
	}
}
