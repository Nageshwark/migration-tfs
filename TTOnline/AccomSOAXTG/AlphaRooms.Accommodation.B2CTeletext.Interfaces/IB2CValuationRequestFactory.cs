﻿namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.SOACommon.DomainModels;

    public interface IB2CValuationRequestFactory
    {
        AccommodationValuationRequest CreateValuationRequest(AccommodationB2CValuationRequest valuationRequest, AccommodationAvailabilityResult[] rooms, ChannelInfo channelInfo);
    }
}
