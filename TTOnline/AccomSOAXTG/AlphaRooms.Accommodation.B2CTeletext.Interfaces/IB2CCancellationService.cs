﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Interfaces
{
    public interface IB2CCancellationService
    {
        Task<Guid> StartCancellationProcessAsync(AccommodationB2CCancellationRequest cancellationRequest);

        Task<AccommodationB2CCancellationResponse> GetCancellationResponseAsync(Guid cancellationId);
    }
}
