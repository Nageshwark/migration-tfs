﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.DependencyInjection;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.Core.DependencyInjection;
using AlphaRooms.Accommodation.Core.Provider.DependencyInjection;
using AlphaRooms.Accommodation.Provider.DependencyInjection;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;

namespace TestApp
{
    public class Program
    {
        private static IB2CAvailabilityService availabilityService;

        static Program()
        {
            var kernel = new StandardKernel(new B2CDependencyInjectionInstaller(),
                new CoreDependencyInjectionInstaller(), new CoreProviderDependencyInjectionInstaller(),
                new ProviderDependencyInjectionInstaller());
            availabilityService = kernel.Get<IB2CAvailabilityService>();
        }

        private static void Main(string[] args)
        {
            var request = new AccommodationB2CAvailabilityRequest()
            {
                Channel = Channel.AlphaRoomsUK,
                CheckInDate = new DateTime(2015, 10, 15), //DateTime.Now.AddMonths(3),
                CheckOutDate = new DateTime(2015, 10, 22), //DateTime.Now.AddMonths(3).AddDays(7),
                DestinationId = Guid.Parse("ae77c30b-7974-4a65-abd6-dc004d243abb"),//Benidorm
                //DestinationId=Guid.Parse("0FB282DF-6C1A-4FAA-8930-9EFC0694EA90"),
                SearchType = SearchType.HotelOnly,
                Rooms = new[] {new AccommodationB2CAvailabilityRequestRoom() {Adults = 2, RoomNumber = 1, ChildAges = new byte[0]}},
                Debugging = true
            };

            try
            {
                GetAvailability(request).Wait();

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);
            }
            Console.ReadLine();
        }

        private static async Task GetAvailability(AccommodationB2CAvailabilityRequest request)
        {
            var searchId = await availabilityService.StartAvailabilitySearchAsync(request);
            AccommodationB2CAvailabilityResponse availabilityResponse =null;
            var filterCriteria=new AccommodationB2CAvailabilityFilter(){NumberOfResults = 1000,PageNumber = 1,PageSize = 500};
            var filterOptions = new AccommodationB2CAvailabilitySort();

            do
            {
                availabilityResponse = await availabilityService.GetAvailabilityResponseAsync(searchId, filterCriteria, filterOptions, true);
            } while (availabilityResponse != null && (availabilityResponse.AvailabilityStatus != Status.Successful));

            if (availabilityResponse.AvailabilityStatus == Status.Successful)
            {
                Console.WriteLine("Total Results - " + availabilityResponse.AvailabilityTotalResultsCount);
            }
            else
                Console.WriteLine("No Results");
            Console.ReadLine();
        }
    }
}