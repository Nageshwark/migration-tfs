﻿namespace AlphaRooms.Accommodation.B2BWeb.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class B2BWebNinjectInstallerTests
    {
        [TestMethod]
        public void NinjectInitTest()
        {
            NinjectInstaller.Start();
            NinjectInstaller.Stop();
        }
    }
}
