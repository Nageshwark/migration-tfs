﻿namespace AlphaRooms.Accommodation.B2BWeb.UnitTests
{
    using AlphaRooms.Accommodation.B2BWeb.DependencyInjection;
    using Core.DependencyInjection;
    using Ninject;
    using Ninject.Modules;

    public class NinjectInstaller
    {
        public static StandardKernel Kernel { get; set; }
        public static bool IsStarted { get; set; }

        public static void Start()
        {
            IsStarted = true;
            CreateKernel();
        }

        public static void Stop()
        {
            IsStarted = false;
        }

        private static IKernel CreateKernel()
        {
            Kernel = new StandardKernel();
            RegisterServices(Kernel);

            return Kernel;
        }

        public static void RegisterServices(IKernel kernel)
        {
            var modules = new INinjectModule[]
            {
                new CoreDependencyInjectionInstaller(),
                new B2BWebDependencyInjectionInstaller()
            };

            kernel.Load(modules);
        }
    }
}
