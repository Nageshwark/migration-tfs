﻿namespace AlphaRooms.Accommodation.B2BWeb.UnitTests
{
    using AlphaRooms.Accommodation.B2B.Services;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [TestClass]
    public class B2BFlashSaleServiceTests
    {
        private Mock<IAccommodationFlashSaleRepository> accommodationFlashSaleRepositoryMock = new Mock<IAccommodationFlashSaleRepository>();

        public static Guid ESTABLISHMENT_A = Guid.NewGuid();
        public static Guid ESTABLISHMENT_B = Guid.NewGuid();
        public static Guid ESTABLISHMENT_C = Guid.NewGuid();
        public static Guid ESTABLISHMENT_D = Guid.NewGuid();

        public static int AGENT_X = 1;
        public static int AGENT_Y = 2;
        public static int AGENT_Z = 3;

        public static List<AccommodationAvailabilityResult> FakeAvailabilityResults = new List<AccommodationAvailabilityResult>()
        {
            new AccommodationAvailabilityResult() { EstablishmentId = ESTABLISHMENT_A },
            new AccommodationAvailabilityResult() { EstablishmentId = ESTABLISHMENT_B },
            new AccommodationAvailabilityResult() { EstablishmentId = ESTABLISHMENT_C },
            new AccommodationAvailabilityResult() { EstablishmentId = ESTABLISHMENT_D }
        };


        public static AccommodationB2BFlashSaleEstablishment[] FakeFlashSaleEstablishments = new AccommodationB2BFlashSaleEstablishment[]
        {
            //Agent X has access to all
            new AccommodationB2BFlashSaleEstablishment() { B2BAgentId = AGENT_X, EstablishmentId = ESTABLISHMENT_A, IsActive = true },
            new AccommodationB2BFlashSaleEstablishment() { B2BAgentId = AGENT_X, EstablishmentId = ESTABLISHMENT_B, IsActive = true },
            new AccommodationB2BFlashSaleEstablishment() { B2BAgentId = AGENT_X, EstablishmentId = ESTABLISHMENT_C, IsActive = true },

            //Agent Y has access to just B
            new AccommodationB2BFlashSaleEstablishment() { B2BAgentId = AGENT_Y, EstablishmentId = ESTABLISHMENT_B, IsActive = true },

        };

        [TestMethod]
        public async Task FlashSaleService_FilterResults_WillReturn_ExclusiveToSelfAndNonExclusive_Establishments()
        {
            // GIVEN I have an availability request from AGENT_X
            var availabilityRequest = new AccommodationAvailabilityRequest() { Agent = new B2BUser() { Id = AGENT_X } };

            // AND I have 4 hotels in results
            var availabilityResults = FakeAvailabilityResults;

            // WHEN I filter results for flash sale
            accommodationFlashSaleRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(FakeFlashSaleEstablishments);
            var serviceToTest = GetFlashSaleService();
            var results = await serviceToTest.FilterResultsForFlashSaleAsync(availabilityRequest, availabilityResults);

            // THEN I should have all 4 hotels 
            // A,B,C  - Exclusive to AGENT_X 
            // D      - Non-Exclusive to any agent
            Assert.IsNotNull(results);
            Assert.AreEqual(4, results.Count);
        }

        [TestMethod]
        public async Task FlashSaleService_FilterResults_WillNotReturn_ExclusiveEstablishmentsToOthers()
        {
            //GIVEN I have availability request from AGENT_Y
            var availabilityRequest = new AccommodationAvailabilityRequest() { Agent = new B2BUser() { Id = AGENT_Y } };

            //AND I have 4 hotels in results
            var availabilityResults = FakeAvailabilityResults;

            //WHEN I filter results for flash sale
            accommodationFlashSaleRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(FakeFlashSaleEstablishments);
            var serviceToTest = GetFlashSaleService();
            var results = await serviceToTest.FilterResultsForFlashSaleAsync(availabilityRequest, availabilityResults);

            // THEN I should have all 2 hotels 
            // B    - Exclusive to AGENT_Y 
            // D    - Non-Exclusive to any agent
            Assert.IsNotNull(results);
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public async Task FlashSaleService_FilterResults_WillOnlyReturn_NonExclusiveEstablishments()
        {
            //GIVEN I have availability request
            var availabilityRequest = new AccommodationAvailabilityRequest() { Agent = new B2BUser() { Id = AGENT_Z } };

            //AND I have 4 hotels in results
            var availabilityResults = FakeAvailabilityResults;

            //WHEN I filter results for flash sale
            accommodationFlashSaleRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(FakeFlashSaleEstablishments);
            var serviceToTest = GetFlashSaleService();
            var results = await serviceToTest.FilterResultsForFlashSaleAsync(availabilityRequest, availabilityResults);

            // THEN I should have just 1 hotel
            // D    - Non-Exclusive to any agent
            Assert.IsNotNull(results);
            Assert.AreEqual(1, results.Count);
        }

        private IAvailabilityFlashSaleService GetFlashSaleService()
        {
            return new B2BAvailabilityFlashSaleService(accommodationFlashSaleRepositoryMock.Object, new B2BFlashSaleMappingFactory());
        }
    }
}