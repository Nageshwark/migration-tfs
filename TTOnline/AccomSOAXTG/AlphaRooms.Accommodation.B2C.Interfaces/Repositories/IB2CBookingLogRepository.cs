﻿using AlphaRooms.Accommodation.B2C.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces.Repositories
{
    public interface IB2CBookingLogRepository
    {
        Task SaveRangeAsync(B2CBookingLog[] b2CBookingLog);
    }
}
