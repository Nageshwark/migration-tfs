﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.DomainModels;

namespace AlphaRooms.Accommodation.B2C.Interfaces.Repositories
{
    public interface IB2CValuationLogRepository
    {
        Task SaveRangeAsync(B2CValuationLog[] b2CValuationLog);
    }
}
