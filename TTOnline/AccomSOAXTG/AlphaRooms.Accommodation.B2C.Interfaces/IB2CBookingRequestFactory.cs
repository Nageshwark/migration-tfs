﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CBookingRequestFactory
    {
        AccommodationBookingRequest CreateBookingRequest(AccommodationB2CBookingRequest bookingRequest, ChannelInfo channelInfo);
    }
}
