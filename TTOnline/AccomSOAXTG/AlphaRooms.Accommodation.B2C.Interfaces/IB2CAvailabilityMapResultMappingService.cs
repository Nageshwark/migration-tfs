﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityMapResultMappingService
    {
        IList<AccommodationB2CAvailabilityMapResult> MapFromCoreAvailabilityResultsAsync(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationB2CAvailabilityResult> results);
    }
}
