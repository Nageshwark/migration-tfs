﻿namespace AlphaRooms.Accommodation.B2C.Interfaces.Caching
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using System;
    using System.Threading.Tasks;

    public interface IB2CRequestStatusCaching
    {
        Task<AccommodationB2CRequestStatus> GetByIdOrNullAsync(Guid requestId);

        Task<AccommodationB2CRequestStatus> GetSuccessfulByAvailabilityKeyOrNullAsync(string availabilityKey, Guid? id);

        Task SaveAsync(AccommodationB2CRequestStatus requestStatus);

        DateTime CreateResultsCacheExpireDate();

        bool IsResultsExpireDateExpired(AccommodationB2CRequestStatus requestStatus);

		Task<AccommodationB2CRequestStatus> GetSuccessfulByMetaTokenKeyOrNullAsync(string MetaTokenKey);
	}
}
