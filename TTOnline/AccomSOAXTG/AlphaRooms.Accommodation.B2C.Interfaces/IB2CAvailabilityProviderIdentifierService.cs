﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityProviderIdentifierService
    {
        Task<AccommodationProvider[]> GetActiveAvailabilityProvidersAsync(AccommodationB2CAvailabilityRequest availabilityRequest);

        Task<AccommodationProvider[]> GetMetaActiveAvailabilityProvidersAsync(AccommodationB2CAvailabilityRequest availabilityRequest);
    }
}
