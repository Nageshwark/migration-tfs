﻿namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using System;
    using System.Threading.Tasks;

    public interface IB2CAvailabilityService
    {
        Task<Guid> StartAvailabilitySearchAsync(AccommodationB2CAvailabilityRequest availabilityRequest);

        Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria, bool getFilterDetails);

        Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseByEstablishmentAsync(Guid availabilityId, Guid establishment, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria);

        Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseByRoomsAsync(Guid availabilityId, uint[] roomIds);

        Task<AccommodationB2CAvailabilityRequest> GetAvailabilityRequestAsync(Guid availabilityId);

        Task<AccommodationB2CAvailabilityResultsFilterOptions> GetAvailabilityResultsFilterOptionsAsync(Guid availabilityId);

        Task<AccommodationB2CAvailabilityMapResponse> GetAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria);

        Task<AccommodationB2CAvailabilityMapResponse> GetAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId);
		Task<AccommodationB2CAvailabilityResponse> GetMetaAvailablityResponseAsync(AccommodationB2CAvailabilityRequest availabilityRequest);
	}
}
