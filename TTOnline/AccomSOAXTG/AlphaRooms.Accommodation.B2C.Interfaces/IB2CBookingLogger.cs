﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CBookingLogger
    {
        Task LogAsync(AccommodationB2CBookingRequest bookingRequest, AccommodationB2CRequestStatus requestStatus, AccommodationBookingResponse coreBookingResponse, TimeSpan timeSpan, Exception exception);
    }
}
