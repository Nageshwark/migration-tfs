﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CCancellationResultMappingService
    {
        AccommodationB2CCancellationResult MapFromCoreCancellationResults(AccommodationB2CCancellationRequest cancellationRequest, AccommodationCancellationResult coreCancellationResult);
    }
}
