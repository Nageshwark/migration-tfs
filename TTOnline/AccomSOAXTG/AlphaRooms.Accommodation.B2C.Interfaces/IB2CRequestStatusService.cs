﻿namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using System;
    using System.Threading.Tasks;

    public interface IB2CRequestStatusService
    {
        AccommodationB2CRequestStatus CreateAvailabilityRequestStatus(AccommodationB2CAvailabilityRequest availabilityRequest);

        AccommodationB2CRequestStatus CreateValuationRequestStatus(AccommodationB2CValuationRequest valuationRequest);

        AccommodationB2CRequestStatus CreateBookingRequestStatus(AccommodationB2CBookingRequest bookingRequest);

        AccommodationB2CRequestStatus CreateCancellationRequestStatus(AccommodationB2CCancellationRequest cancellationRequest);

        Task<AccommodationB2CRequestStatus> GetRequestStatusOrNullByIdAsync(Guid id);

        Task<AccommodationB2CRequestStatus> GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(string availabilityKey, Guid? id);

        DateTime CreateResultsCacheExpireDate();

        bool IsResultsExpireDateExpired(AccommodationB2CRequestStatus requestStatus);

        Task SaveRequestStatusAsync(AccommodationB2CRequestStatus requestStatus);

        Task UpdateRequestStatus(AccommodationB2CRequestStatus requestStatus, Status status, string message);

		Task<AccommodationB2CRequestStatus> GetSuccessfulRequestStatusByMetaTokenKeyOrNullAsync(string availabilityKey);
	}
}
