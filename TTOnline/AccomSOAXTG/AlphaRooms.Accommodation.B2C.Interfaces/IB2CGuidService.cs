﻿namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    using System;

    public interface IB2CGuidService
    {
        Guid GenerateGuid(bool IsMeta);
        bool IsMetaGuid(Guid? guid);
    }
}