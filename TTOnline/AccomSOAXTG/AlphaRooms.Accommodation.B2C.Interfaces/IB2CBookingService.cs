﻿using AlphaRooms.Accommodation.B2C.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CBookingService
    {
        Task<Guid> StartBookingAsync(AccommodationB2CBookingRequest bookingRequest);

        Task<AccommodationB2CBookingResponse> GetBookingResponseAsync(Guid bookingId);
    }
}
