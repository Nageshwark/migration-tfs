﻿namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    using AlphaRooms.Accommodation.B2C.Contracts;

    public interface IB2CAvailabilityRequestValidator
    {
        void ValidateAvailabilityRequest(AccommodationB2CAvailabilityRequest availabilityRequest);
    }
}
