﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityLogger
    {
        Task LogAsync(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CRequestStatus requestStatus, AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse, TimeSpan? postProcessTimeTaken, TimeSpan timeTaken, Exception exception);
    }
}
