﻿using AlphaRooms.Accommodation.B2C.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CValuationService
    {
        Task<Guid> StartValuationAsync(AccommodationB2CValuationRequest valuationRequest);

        Task<AccommodationB2CValuationResponse> GetValuationResponseAsync(Guid valuationId);
    }
}
