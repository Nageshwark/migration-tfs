﻿using AlphaRooms.Accommodation.B2C.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CValuationRequestValidator
    {
        void ValidateValuationRequest(AccommodationB2CValuationRequest valuationRequest);
    }
}
