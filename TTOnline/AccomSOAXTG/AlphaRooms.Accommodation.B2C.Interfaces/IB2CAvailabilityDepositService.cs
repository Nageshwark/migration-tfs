﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityDepositService
    {
        Task<IList<AccommodationB2CAvailabilityResult>> UpdateDepositEligibilityAsync(IList<AccommodationB2CAvailabilityResult> results, DateTime checkInDate);
    }
}