﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityFilterCriteriaFactory
    {
        AccommodationB2CAvailabilityFilter CreateWithoutFilterOptions(AccommodationB2CAvailabilityFilter filterCriteria);
    }
}
