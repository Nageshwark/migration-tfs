﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    public interface IB2CAvailabilityRequestFactory
    {
        AccommodationAvailabilityRequest CreateAvailabilityRequest(AccommodationB2CAvailabilityRequest availabilityRequest, ChannelInfo channelInfo, AccommodationProvider[] providers);
    }
}
