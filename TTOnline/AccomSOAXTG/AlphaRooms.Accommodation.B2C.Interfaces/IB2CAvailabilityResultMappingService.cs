﻿namespace AlphaRooms.Accommodation.B2C.Interfaces
{
    using System;
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.Core.Contracts;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core.Provider.DomainModels;

    public interface IB2CAvailabilityResultMappingService
    {
        Task<IList<AccommodationB2CAvailabilityResult>> MapFromCoreAvailabilityResultsAsync(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> coreResults);
		AccommodationAvailabilityResult[] MapToCoreAvailabilityResult(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationB2CAvailabilityResult> availabilityResults, Dictionary<Guid, EstablishmentDetails> establishmentDetails);
	}
}
