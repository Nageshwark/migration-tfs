﻿using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.B2B.MarkupServices
{
    public class B2BAgentVatService : IB2BAgentVatService
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BAgentVatService(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }
        public Money CalculateAgentVat(B2BCompany company, Money agentCommission)
        {
            // TODO: GB to compare with enum of ISOCountry/channel 
            if (company == null || company.CountryIsoCode != "GB" || string.IsNullOrEmpty(company.VATNumber)) return null;
            return (agentCommission != null ? new Money((agentCommission.Amount * this.configurationManager.AccommodationVatPercentage) / 100, agentCommission.CurrencyCode) : null);
        }
    }

}