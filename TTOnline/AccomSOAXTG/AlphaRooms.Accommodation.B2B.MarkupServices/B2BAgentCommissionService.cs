﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.B2B.MarkupServices
{
    public class B2BAgentCommissionService : IB2BAgentCommissionService
    {
        
        public Money CalculateAgentCommission(decimal? commission, Money totalPrice)
        {
            return (commission != null ? new Money(((totalPrice.Amount/(1 - (commission.Value/100))) * (commission.Value / 100)), totalPrice.CurrencyCode) : null);
        }

        public decimal? GetAgentCommission(B2BUser agent)
        {
            if (agent.Commission != null) return agent.Commission;
            var company = agent.Company;
            while (company != null)
            {
                if (company.Commission != null) return company.Commission;
                company = company.ParentCompany;
            }
            return null;
        }
    }
}