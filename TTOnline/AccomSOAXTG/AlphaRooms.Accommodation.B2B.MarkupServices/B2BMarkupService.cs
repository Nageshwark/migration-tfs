﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2B.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.B2B.MarkupServices
{
    public class B2BMarkupService : IAvailabilityResultMarkupService, IValuationResultMarkupService
    {
        private readonly IMarkupRepository markupRepository;
        private readonly IProviderCommissionRepository providerCommissionRepository;
        private readonly IExchangeRateRepository exchangeRateRepository;
        private readonly IVolumeDiscountRepository volumeDiscountRepository;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BAgentCommissionService b2bAgentCommissionService;
        private readonly IB2BAgentVatService b2bAgentVatService;
        public B2BMarkupService(IMarkupRepository markupRepository, IProviderCommissionRepository providerCommissionRepository
            , IExchangeRateRepository exchangeRateRepository, IVolumeDiscountRepository volumeDiscountRepository
            , IAccommodationConfigurationManager configurationManager, IB2BAgentCommissionService b2bAgentCommissionService, IB2BAgentVatService b2bAgentVatService)
        {
            this.markupRepository = markupRepository;
            this.providerCommissionRepository = providerCommissionRepository;
            this.exchangeRateRepository = exchangeRateRepository;
            this.volumeDiscountRepository = volumeDiscountRepository;
            this.configurationManager = configurationManager;
            this.b2bAgentCommissionService = b2bAgentCommissionService;
            this.b2bAgentVatService = b2bAgentVatService;
        }

        public async Task<IList<AccommodationAvailabilityResult>> ApplyAvailabilityMarkupAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            if (results.Count == 0) return results;
            var baseDestinationId = results[0].BaseDestinationId;
            var destinationId = results[0].DestinationId;
            var markups = await this.markupRepository.GetActiveFilteredHierarchicalAsync(availabilityRequest.ChannelInfo.Channel, (availabilityRequest.Agent != null ? (int?)availabilityRequest.Agent.Id : null));
            markups = ApplyInitialFilter(markups, results.First(), DateTime.Now);
            var commissions = await this.providerCommissionRepository.GetAllDictionaryAsync();
            var exchangeRates = await this.exchangeRateRepository.GetExchangeRateAsync(results.Select(i => i.ProviderResult.CostPrice.CurrencyCode).Distinct().ToArray()
                , availabilityRequest.ChannelInfo.CurrencyCode);
            var volumeDiscounts = await this.volumeDiscountRepository.GetAllAsync();
            var agentCommissionRate = b2bAgentCommissionService.GetAgentCommission(availabilityRequest.Agent);
            results.ParallelForEach(result => ApplyPriceMarkup(false, availabilityRequest.ChannelInfo, availabilityRequest.Agent, agentCommissionRate
                , result, markups, commissions, exchangeRates, volumeDiscounts));
            return results;
        }

        public async Task<AccommodationValuationResult> ApplyValuationMarkupAsync(AccommodationValuationRequest valuationRequest, AccommodationValuationResult result)
        {
            if (result.Rooms.Length == 0) return result;
            var baseDestinationId = result.Rooms[0].BaseDestinationId;
            var destinationId = result.Rooms[0].DestinationId;
            var markups = await this.markupRepository.GetActiveFilteredHierarchicalAsync(valuationRequest.ChannelInfo.Channel, (valuationRequest.Agent != null ? (int?)valuationRequest.Agent.Id : null));
            markups = ApplyInitialFilter(markups, result.Rooms[0], DateTime.Now);
            var commissions = await this.providerCommissionRepository.GetAllDictionaryAsync();
            var exchangeRates = await this.exchangeRateRepository.GetExchangeRateAsync(result.Rooms.Select(i => i.ProviderResult.CostPrice.CurrencyCode).Distinct().ToArray()
                , valuationRequest.ChannelInfo.CurrencyCode);
            var volumeDiscounts = await this.volumeDiscountRepository.GetAllAsync();
            var agentCommissionRate = b2bAgentCommissionService.GetAgentCommission(valuationRequest.Agent);
            result.Rooms.ParallelForEach(roomResult =>
            {
                ApplyPriceMarkup(true, valuationRequest.ChannelInfo, valuationRequest.Agent, agentCommissionRate, roomResult, markups, commissions, exchangeRates, volumeDiscounts);
                roomResult.SpecificData = new Dictionary<string, string>();
                if (agentCommissionRate != null) roomResult.SpecificData.Add("AgentCommissionRate", agentCommissionRate.ToString());
            });
            return result;
        }

        private void ApplyPriceMarkup(bool isValuation, ChannelInfo channelInfo, B2BUser agent, decimal? agentCommissionRate, IAccommodationResultRoom result, Markup[] allMarkups, Dictionary<string, ProviderCommission> allCommissions
            , Dictionary<string, ExchangeRate> exchangeRates, VolumeDiscount[] volumeDiscounts)
        {
            var commission = allCommissions.GetValueOrDefault(result.Provider.EdiCode + (result.Supplier != null ? result.Supplier.EdiCode : ""));
            var exchangeRate = exchangeRates[result.ProviderResult.CostPrice.CurrencyCode];
            var markups = GetFilteredMarkups(allMarkups, result, DateTime.Now);
            var bookingDate = DateTime.Now;
            var applyPrice = !(result.ProviderResult.IsBindingRate || result.ProviderResult.PaymentModel == PaymentModel.CustomerPayDirect || result.Provider.EdiCode == "EX"); //expedia
            var applyMarkup = (commission != null ? commission.IncludeInHMD : true);
            var costPrice = new Money(result.ProviderResult.CostPrice.Amount, result.ProviderResult.CostPrice.CurrencyCode);


            // A NetStandard with a commission and no HMD is a GrossStandard that needs netPrice calculations
            if (commission != null && !(commission.IncludeInHMD) && result.ProviderResult.RateType == RateType.NetStandard)
            { result.ProviderResult.RateType = RateType.GrossStandard; }

            if (applyPrice && commission != null) costPrice.Amount -= costPrice.Amount * (commission.Commission / 100);
            var salePrice = (IsSaleAndCostPriceDifferentForSupplier(result.Provider.EdiCode, result.ProviderResult.IsBindingRate) ? new Money(result.ProviderResult.SalePrice.Amount, result.ProviderResult.SalePrice.CurrencyCode)
                : (applyMarkup ? costPrice : new Money(result.ProviderResult.CostPrice.Amount, result.ProviderResult.CostPrice.CurrencyCode)));
            salePrice = new Money(salePrice.Amount * (decimal)exchangeRate.Rate, channelInfo.CurrencyCode);
            Money additionalPrice = new Money(0, salePrice.CurrencyCode);


            if (applyPrice)
            {
                if (result.ProviderResult.PaymentModel == PaymentModel.PostPayment && volumeDiscounts.Any() && salePrice.Amount > volumeDiscounts.Min(i => i.Volume) && markups.Any())
                {
                    var volumeDiscount = volumeDiscounts.Where(i => i.Volume <= salePrice.Amount).OrderByDescending(i => i.Volume).FirstOrDefault();
                    additionalPrice.Amount -= salePrice.Amount * (volumeDiscount.Discount / 100);
                }
                if (applyMarkup)
                {
                    decimal markupSalePrice = 0, markupCostPrice = 0;
                    if (!ArrayModule.IsNullOrEmpty(markups))
                    {
                        foreach (var markup in markups)
                        {
                            if (markup.MarkupApplyType == MarkupApplyType.Value || markup.MarkupApplyType == MarkupApplyType.Override)
                            {
                                markupSalePrice = 0;
                                markupCostPrice = 0;
                            }
                            if (IsCommissionable(result.ProviderResult.RateType)) markupCostPrice += CalculateMarkup(costPrice, result.ProviderResult.CheckInDate, result.ProviderResult.CheckOutDate, markup);
                            else markupSalePrice += CalculateMarkup(salePrice, result.ProviderResult.CheckInDate, result.ProviderResult.CheckOutDate, markup);
                            if (markup.MarkupApplyType == MarkupApplyType.Override) break;
                        }
                    }
                    else markupSalePrice += (salePrice.Amount * (this.configurationManager.AccommodationBaseDiscount / 100));
                    if (markupSalePrice != 0) additionalPrice.Amount += markupSalePrice;
                    if (markupCostPrice != 0) costPrice.Amount -= markupCostPrice;
                }
            }
            costPrice.Amount = Math.Round(costPrice.Amount, 2);
            result.CostPrice = costPrice;
            salePrice.Amount = Math.Round(salePrice.Amount, 2);
            result.SalePrice = salePrice;
            additionalPrice.Amount = Math.Round(additionalPrice.Amount, 2);

            var agentCommission = b2bAgentCommissionService.CalculateAgentCommission(agentCommissionRate, new Money(salePrice.Amount + additionalPrice.Amount, salePrice.CurrencyCode)); 

            if (!isValuation)
            {
                result.PriceBreakdown = new [] { new AccommodationResultPriceItem()
                {
                    Type = AccommodationResultPriceItemType.SellingPrice,
                    CostPrice = costPrice,
                    SalePrice = new Money(salePrice.Amount + additionalPrice.Amount + (agentCommission != null ? agentCommission.Amount : 0) , salePrice.CurrencyCode)
                }};
            }
            else
            {
                var priceBreakDownList = new List<AccommodationResultPriceItem>();
                priceBreakDownList.Add(new AccommodationResultPriceItem()
                {
                    Type = AccommodationResultPriceItemType.SellingPrice,
                    CostPrice = costPrice,
                    SalePrice = new Money(salePrice.Amount + additionalPrice.Amount + (agentCommission != null ? agentCommission.Amount : 0), salePrice.CurrencyCode)
                });
                if (agentCommission != null)
                {
                    priceBreakDownList.Add(new AccommodationResultPriceItem()
                    {
                        
                        Type = AccommodationResultPriceItemType.AgentCommission,
                        CostPrice = new Money(0, agentCommission.CurrencyCode),
                        SalePrice = agentCommission
                    });
                }

                // agents can bill to a specific company, so work out which one we're dealing with
                var billingCompany = agent.BillToCompany ?? agent.Company;
                var vatPrice = b2bAgentVatService.CalculateAgentVat(billingCompany, agentCommission);
                if (vatPrice != null)
                {
                    priceBreakDownList.Add(new AccommodationResultPriceItem()
                    {
                        Type = AccommodationResultPriceItemType.Vat,
                        CostPrice = new Money(0, vatPrice.CurrencyCode),
                        SalePrice = vatPrice
                    });
                }
                result.PriceBreakdown = priceBreakDownList.ToArray();
            }
            result.Margin = 0;
            result.ExchangeRate = (decimal)exchangeRate.Rate;
        }

        private bool IsCommissionable(RateType rateType)
        {
            return (rateType == RateType.GrossNonBinding && rateType == RateType.GrossPayDirect && rateType == RateType.GrossStandard);
        }

        private decimal CalculatePriceAdjustments(Money salePrice, DateTime checkInDate, DateTime checkOutDate, PriceAdjustment priceAdjustment)
        {
            return CalculateMarkup(salePrice, checkInDate, checkOutDate, priceAdjustment.StartDate, priceAdjustment.EndDate, (decimal)priceAdjustment.ChangeToApply);
        }

        private decimal CalculateMarkup(Money costPrice, DateTime checkInDate, DateTime checkOutDate, Markup markup)
        {
            return CalculateMarkup(costPrice, checkInDate, checkOutDate, markup.BookingFromDate, markup.BookingToDate, markup.Value);
        }

        private decimal CalculateMarkup(Money price, DateTime checkInDate, DateTime checkOutDate, DateTime? fromDate, DateTime? toDate, decimal percent)
        {
            var duration = Math.Max(1, (int)(checkOutDate - checkInDate).TotalDays);
            var applyDays = GetMarkupApplyDays(checkInDate, checkOutDate, fromDate ?? DateTime.MinValue, toDate ?? DateTime.MaxValue);
            return price.Amount * (percent * ((decimal)applyDays / (decimal)duration) / 100M);
        }

        private int GetMarkupApplyDays(DateTime checkInDate, DateTime checkOutDate, DateTime startDate, DateTime endDate)
        {
            DateTime maxStartDate = (checkInDate > startDate ? checkInDate : startDate);
            DateTime minEndDate = (checkOutDate < endDate ? checkOutDate : endDate);
            return Math.Max(0, (int)(minEndDate - maxStartDate).TotalDays);
        }

        private Markup[] GetFilteredMarkups(Markup[] markups, IAccommodationResultRoom result, DateTime bookingDate)
        {
            // This was divided into common elements (ApplyInitialFilter) that can be done once, and multiple elements that need doing every time.
            int duration = (int)(result.ProviderResult.CheckOutDate - result.ProviderResult.CheckInDate).TotalDays;
            return markups.Where(i =>
                (i.ProviderId == null || i.ProviderId == result.Provider.Id)
                && (i.SupplierId == null || (result.Supplier != null && i.SupplierId == result.Supplier.Id))
                && (i.ChainId == null || i.ChainId == result.EstablishmentChainId)
                && (i.EstablishmentId == null || i.EstablishmentId == result.EstablishmentId)
                && (i.StarRating == null || i.StarRating == result.EstablishmentStarRating)
                && (i.BoardType == null || i.BoardType == result.BoardType)).OrderBy(i => i.Order).ToArray();
        }

        private Markup[] ApplyInitialFilter(Markup[] markups, IAccommodationResultRoom result, DateTime bookingDate)
        {
            // Common elements to fetch markups. Order shouldn't be important here as we will order where fewer elements appear.
            int duration = (int)(result.ProviderResult.CheckOutDate - result.ProviderResult.CheckInDate).TotalDays;
            return markups.Where(i =>
                (i.DestinationId == null || i.DestinationId == result.DestinationId || i.DestinationId == result.BaseDestinationId)
                && (i.CountryCode == null || i.CountryCode == result.CountryCode)
                && (i.BookingMinDuration == null || i.BookingMinDuration <= duration)
                && (i.BookingMaxDuration == null || i.BookingMaxDuration >= duration)
                && (i.TravelFromDate == null || i.TravelFromDate <= result.ProviderResult.CheckInDate)
                && (i.TravelToDate == null || i.TravelToDate >= result.ProviderResult.CheckOutDate)
                && (i.BookingFromDate == null || i.BookingFromDate <= bookingDate)
                && (i.BookingToDate == null || i.BookingToDate >= bookingDate)).ToArray();
        }

        private decimal GetMarkupPercent(Markup[] filteredMarkups)
        {
            decimal markupPercent = 0;
            foreach (var markup in filteredMarkups)
            {
                if (markup.MarkupApplyType == MarkupApplyType.Value) markupPercent = markup.Value;
                else if (markup.MarkupApplyType == MarkupApplyType.Adjustment) markupPercent += markup.Value;
                else if (markup.MarkupApplyType == MarkupApplyType.Override)
                {
                    markupPercent = markup.Value;
                    break;
                }
                else throw new Exception("Unexpected MarkupApplyType " + markup.MarkupApplyType.ToString() + ".");
            }
            return markupPercent;
        }

        private bool IsSaleAndCostPriceDifferentForSupplier(string providerEdiCode, bool isBindingRate)
        {
            if (isBindingRate) return true;
            if (providerEdiCode == "F" && providerEdiCode == "I") return true; // EdiTransfers EdiMinotel
            return false;
        }
    }
}
