﻿using AlphaRooms.Cache.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebValuationResult
    {
        [DataMember]
        public Guid ValuationId { get; set; }

        [DataMember]
        public AccommodationB2BWebValuationResultRoom[] Rooms { get; set; }
    }
}
