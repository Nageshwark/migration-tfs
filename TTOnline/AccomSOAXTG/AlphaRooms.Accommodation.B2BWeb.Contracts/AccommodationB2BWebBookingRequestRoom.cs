﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebBookingRequestRoom
    {
        public const string ValuatedRoomIdFieldName = "VId";
        public const string SpecialRequestsFieldName = "SRq";
        public const string CardDetailsFieldName = "CDt";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ValuatedRoomIdFieldName)]
        public string ValuatedRoomId { get; set; }
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SpecialRequestsFieldName)]
        public AccommodationB2BWebBookingRequestRoomSpecialRequest SpecialRequests { get; set; }
    }
}
