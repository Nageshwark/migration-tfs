﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System.Runtime.Serialization;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebBookingResponse
    {
        [DataMember]
        public Guid BookingId { get; set; }

        [DataMember]
        public Status BookingStatus { get; set; }

        [DataMember]
        public AccommodationB2BWebBookingResult BookingResult { get; set; }
    }
}
