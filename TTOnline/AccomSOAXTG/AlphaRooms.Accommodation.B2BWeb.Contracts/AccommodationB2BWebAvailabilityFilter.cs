﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebAvailabilityFilter
    {
        [DataMember]
        public int? NumberOfResults { get; set; }

        [DataMember]
        public int? NumberOfRoomResults { get; set; }

        [DataMember]
        public bool OnlyLowerPriceRoomResults { get; set; }

        [DataMember]
        public int? PageNumber { get; set; }

        [DataMember]
        public int? PageSize { get; set; }

        [DataMember]
        public string EstablishmentName { get; set; }

        [DataMember]
        public Money PriceMin { get; set; }

        [DataMember]
        public Money PriceMax { get; set; }
        
        [DataMember]
        public PaymentTypeType[] PaymentTypes { get; set; }

        [DataMember]
        public StarRatingType[] StarRatings { get; set; }

        [DataMember]
        public BoardType[] BoardTypes { get; set; }

        [DataMember]
        public string[] ProviderEdiCodes { get; set; }
        
        [DataMember]
        public int[] Districts { get; set; }

        [DataMember]
        public int[] FacilityIds { get; set; }

        [DataMember]
        public int[] TravellerTypeIds { get; set; }
    }
}
