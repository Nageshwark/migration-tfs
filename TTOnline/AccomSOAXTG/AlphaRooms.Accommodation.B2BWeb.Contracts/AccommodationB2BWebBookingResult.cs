﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebBookingResult
    {
        [DataMember]
        public Guid BookingId { get; set; }

        [DataMember]
        public AccommodationB2BWebBookingResultRoom[] Rooms { get; set; }
    }
}
