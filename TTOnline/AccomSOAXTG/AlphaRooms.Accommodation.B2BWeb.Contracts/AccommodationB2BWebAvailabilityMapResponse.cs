﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebAvailabilityMapResponse
    {
        [DataMember]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        public Status AvailabilityStatus { get; set; }

        [DataMember]
        public AccommodationB2BWebAvailabilityMapResult[] AvailabilityMapResults { get; set; }

        [DataMember]
        public int AvailabilityTotalResultsCount { get; set; }
    }
}
