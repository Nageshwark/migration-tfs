﻿using AlphaRooms.Accommodation.B2BWeb.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebAvailabilitySort
    {
        [DataMember]
        public AccommodationB2BWebAvailabilitySortField? SortField { get; set; }

        [DataMember]
        public AccommodationB2BWebAvailabilitySortOrder? SortOrder { get; set; }
    }
}
