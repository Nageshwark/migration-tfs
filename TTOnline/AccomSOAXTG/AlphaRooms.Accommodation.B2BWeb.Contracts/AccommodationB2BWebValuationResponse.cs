﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebValuationResponse
    {
        [DataMember]
        public Guid ValuationId { get; set; }

        [DataMember]
        public Status ValuationStatus { get; set; }

        [DataMember]
        public AccommodationB2BWebValuationResult ValuationResult { get; set; }

        [DataMember]
        public string PromotionalCode { get; set; }
    }
}
