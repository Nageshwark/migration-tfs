﻿using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    public class AccommodationB2BWebRequestStatus : ICacheExpire
    {
        public const string RequestTypeFieldName = "RTy";
        public const string StartDateFieldName = "SDt";
        public const string DurationFieldName = "SDr";
        public const string StatusFieldName = "SSt";
        public const string MessageFieldName = "Msg";
        public const string CacheExpireDateFieldName = "CED";
        public const string AvailabilityRequestFieldName = "ARq";
        public const string AvailabilityKeyFieldName = "Avk";
        public const string ResultsExpireDateFieldName = "RED";
        public const string ResultsBaseDestinationIdFieldName = "ABD";
        public const string ResultsAvailabilityIdFieldName = "RAv";
        public const string ResultsFilterOptionsFieldName = "RFt";
        public const string ValuationRequestFieldName = "VRq";
        public const string BookingRequestFieldName = "BRq";
        public const string CancellationRequestFieldName = "Crq";

        [BsonId]
        public Guid Id { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(RequestTypeFieldName)]
        public RequestType RequestType { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(StartDateFieldName)]
        public DateTime StartDate { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(DurationFieldName)]
        public int Duration { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(StatusFieldName)]
        public Status Status { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(MessageFieldName)]
        public string Message { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }

        // availability
        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityRequestFieldName)]
        public AccommodationB2BWebAvailabilityRequest AvailabilityRequest { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityKeyFieldName)]
        public string AvailabilityKey { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ResultsExpireDateFieldName)]
        public DateTime? ResultsExpireDate { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ResultsBaseDestinationIdFieldName)]
        public Guid? ResultsBaseDestinationId { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ResultsAvailabilityIdFieldName)]
        public Guid? ResultsAvailabilityId { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ResultsFilterOptionsFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptions ResultsFilterOptions { get; set; }

        // valuation
        [BsonIgnoreIfDefault]
        [BsonElement(ValuationRequestFieldName)]
        public AccommodationB2BWebValuationRequest ValuationRequest { get; set; }

        // booking
        [BsonIgnoreIfDefault]
        [BsonElement(BookingRequestFieldName)]
        public AccommodationB2BWebBookingRequest BookingRequest { get; set; }

        // cancellation
        [BsonIgnoreIfDefault]
        [BsonElement(CancellationRequestFieldName)]
        public AccommodationB2BWebCancellationRequest CancellationRequest { get; set; }
    }
}
