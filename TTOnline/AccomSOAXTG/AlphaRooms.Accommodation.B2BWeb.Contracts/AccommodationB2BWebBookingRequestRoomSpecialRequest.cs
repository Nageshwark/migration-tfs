﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebBookingRequestRoomSpecialRequest
    {
        private const string LateArrivalFieldName = "LAr";
        private const string CotRequiredFieldName = "Cot";
        private const string SeaViewsFieldName = "SVw";
        private const string AdjoiningRoomsFieldName = "AdR";
        private const string NonSmokingFieldName = "NSk";
        private const string DisabledAccessFieldName = "DAc";
        private const string OtherRequestsFieldName = "ORq";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(LateArrivalFieldName)]
        public bool LateArrival { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CotRequiredFieldName)]
        public bool CotRequired { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SeaViewsFieldName)]
        public bool SeaViews { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AdjoiningRoomsFieldName)]
        public bool AdjoiningRooms { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(NonSmokingFieldName)]
        public bool NonSmoking { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DisabledAccessFieldName)]
        public bool DisabledAccess { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(OtherRequestsFieldName)]
        public string OtherRequests { get; set; }
    }
}
