﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebBookingRequest
    {
        public const string BookingIdFieldName = "BId";
        public const string ChannelFieldName = "Chn";
        public const string AgentIdFieldName = "AgI";
        public const string AvailabilityIdFieldName = "AId";
        public const string ValuationIdFieldName = "VId";
        public const string ItineraryIdFieldName = "IId";
        public const string ItineraryReferenceFieldName = "IRf";
        public const string CustomerFieldName = "Cst";
        public const string ValuatedRoomsFieldName = "VRm";
        public const string DebuggingFieldName = "Dbg";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(BookingIdFieldName)]
        public Guid? BookingId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChannelFieldName)]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AgentIdFieldName)]
        public int AgentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ValuationIdFieldName)]
        public Guid ValuationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ItineraryIdFieldName)]
        public int ItineraryId { get; set; }
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ItineraryReferenceFieldName)]
        public string ItineraryReference { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CustomerFieldName)]
        public AccommodationB2BWebBookingRequestCustomer Customer { get; set; } 

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ValuatedRoomsFieldName)]
        public AccommodationB2BWebBookingRequestRoom[] ValuatedRooms { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DebuggingFieldName)]
        public bool Debugging { get; set; }
    }
}
