﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityResultsFilterOptions
    {

        public const string PriceMinFieldName = "PMi";
        public const string PriceMaxFieldName = "PMa";
        public const string PaymentTypesFieldName = "PyT";
        public const string StarRatingsFieldName = "SRt";
        public const string BoardsFieldName = "Brd";
        public const string FacilitiesFieldName = "Fct";
        public const string TravellerTypesFieldName = "TTs";
        public const string ProvidersFieldName = "Pvd";
        public const string EstablishmentNamesFieldName = "EtN";
        public const string DistrictsFieldName = "Dtt";
                

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PriceMinFieldName)]
        public Money PriceMin { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PriceMaxFieldName)]
        public Money PriceMax { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PaymentTypesFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionPaymentType[] PaymentTypes { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(StarRatingsFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionStarRating[] StarRatings { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(BoardsFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionBoardType[] Boards { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(FacilitiesFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionItem[] Facilities { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(TravellerTypesFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionItem[] TravellerTypes { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProvidersFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionProvider[] Providers { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentNamesFieldName)]
        public string[] EstablishmentNames { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DistrictsFieldName)]
        public AccommodationB2BWebAvailabilityResultsFilterOptionItem[] Districts { get; set; }
    }
}
