﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityResultRoom
    {
        public const string RoomIdFieldName = "RId";
        public const string RoomIdAlpha2FieldName = "RIA";
        public const string ProviderEdiCodeFieldName = "PCd";
        public const string ProviderNameFieldName = "PNm";
        public const string DestinationIdFieldName = "DsI";
        public const string EstablishmentIdFieldName = "EtI";
        public const string ProviderEstablishmentCodeFieldName = "PEC";
        public const string CheckInDateFieldName = "CIn";
        public const string CheckOutDateFieldName = "COt";
        public const string AdultsCountFieldName = "Ads";
        public const string ChildrenCountFieldName = "CC";
        public const string InfantsCountFieldName = "IC";
        public const string RoomNumberFieldName = "RmN";
        public const string ProviderRoomCodeFieldName = "PRC";
        public const string RoomDescriptionFieldName = "RDp";
        public const string BoardDescriptionFieldName = "BdD";
        public const string BoardTypeFieldName = "BdT";
        public const string IsOpaqueRateFieldName = "IOp";
        public const string IsSpecialRateFieldName = "ISp";
        public const string IsNonRefundableFieldName = "INR";
        public const string IsBindingRateFieldName = "IBR";
        public const string PaymentModelFieldName = "PMd";
        public const string ProviderPriceFieldName = "PPr";
        public const string PriceFieldName = "Prc";
        public const string PricePerNightFieldName = "PPN";
        public const string PaymentToTakeFieldName = "PTT";
        public const string ExchangeRateFieldName = "ExR";
        public const string IsCardChargeApplicableFieldName = "ICC";
        public const string MarginFieldName = "Mrg";
        public const string CacheExpireDateFieldName = "CED";
        public const string IsLowestPriceFieldName = "LPr";
        public const string RateTypeFieldName = "RTy";
        public const string HighestPriceFieldName = "HPr";
        public const string DiscountPriceFieldName = "Dis";
        public const string ProviderSpecificDataFieldName = "PSD";
        public const string SupplierEdiCodeFieldName = "s";
        public const string ProviderDestinationCodeFieldName = "dst";
        public const string ProviderContractCodeFieldName = "PC";
        public const string ProviderProviderCodeFieldName = "PPc";
        public const string ProviderRoomDescriptionFieldName = "PRD";


        [DataMember]
        [BsonId]
        public Guid Id { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomIdFieldName)]
        public string RoomId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomIdAlpha2FieldName)]
        public uint RoomIdAlpha2 { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomNumberFieldName)]
        public int RoomNumber { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderEdiCodeFieldName)]
        public string ProviderEdiCode { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderNameFieldName)]
        public string ProviderName { get; set; }

        [BsonIgnore]
        public string ProviderFilterName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationIdFieldName)]
        public Guid DestinationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentIdFieldName)]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderEstablishmentCodeFieldName)]
        public string ProviderEstablishmentCode { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CheckInDateFieldName)]
        public DateTime CheckInDate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CheckOutDateFieldName)]
        public DateTime CheckOutDate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AdultsCountFieldName)]
        public byte AdultsCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChildrenCountFieldName)]
        public byte ChildrenCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(InfantsCountFieldName)]
        public byte InfantsCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderRoomCodeFieldName)]
        public string ProviderRoomCode { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomDescriptionFieldName)]
        public string RoomDescription { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(BoardTypeFieldName)]
        public BoardType BoardType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(BoardDescriptionFieldName)]
        public string BoardDescription { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsOpaqueRateFieldName)]
        public bool IsOpaqueRate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsNonRefundableFieldName)]
        public bool IsNonRefundable { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsBindingRateFieldName)]
        public bool IsBindingRate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PaymentModelFieldName)]
        public PaymentModel PaymentModel { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderPriceFieldName)]
        public Money ProviderPrice { get; set; }

        [DataMember]
        [BsonElement(DiscountPriceFieldName)]
        [BsonIgnoreIfDefault]
        public PromotionalDiscountResult Discount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PriceFieldName)]
        public Money Price { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PricePerNightFieldName)]
        public Money PricePerNight { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PaymentToTakeFieldName)]
        public Money PaymentToTake { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ExchangeRateFieldName)]
        public decimal ExchangeRate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsCardChargeApplicableFieldName)]
        public bool IsCardChargeApplicable { get; set; }

        [DataMember]
        [BsonElement(MarginFieldName)]
        public decimal Margin { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsLowestPriceFieldName)]
        public bool IsLowestPrice { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(HighestPriceFieldName)]
        public Money HighestPrice { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RateTypeFieldName)]
        public RateType RateType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderSpecificDataFieldName)]
        public Dictionary<string, string> ProviderSpecificData { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(SupplierEdiCodeFieldName)]
        public string SupplierEdiCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderDestinationCodeFieldName)]
        public string ProviderDestinationCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderContractCodeFieldName)]
        public string ProviderContract { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderProviderCodeFieldName)]
        public string ProviderProviderCode { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(ProviderRoomDescriptionFieldName)]
        public string ProviderRoomDescription { get; set; }
    }
}
