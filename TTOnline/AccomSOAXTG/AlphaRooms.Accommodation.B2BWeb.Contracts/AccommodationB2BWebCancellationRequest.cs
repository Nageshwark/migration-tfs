﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebCancellationRequest
    {
        public const string CancellationIdFieldName = "CId";
        public const string ChannelFieldName = "Chn";
        public const string AgentIdFieldName = "AgI";
        public const string ItineraryIdFieldName = "IId";
        public const string CustomerFieldName = "Cst";
        public const string DebuggingFieldName = "Dbg";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CancellationIdFieldName)]
        public Guid? CancellationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChannelFieldName)]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AgentIdFieldName)]
        public int AgentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ItineraryIdFieldName)]
        public Guid ItineraryId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DebuggingFieldName)]
        public bool Debugging { get; set; }
    }
}
