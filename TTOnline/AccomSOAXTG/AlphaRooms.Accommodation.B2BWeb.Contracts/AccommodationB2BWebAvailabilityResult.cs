﻿using AlphaRooms.Accommodation.B2BWeb.Contracts.Enums;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityResult : ICacheExpire
    {
        public const string AvailabilityIdFieldName = "AvI";
        public const string DestinationIdFieldName = "DsI";
        public const string DestinationNameFieldName = "DsN";
        public const string EstablishmentIdFieldName = "EtI";
        public const string EstablishmentNameFieldName = "EtN";
        public const string EstablishmentStarRatingFieldName = "STr";
        public const string EstablishmentFacilitiesFieldName = "Fcs";
        public const string EstablishmentTravellerTypesFieldName = "TTs";
        public const string ResortNameFieldName = "RsN";
        public const string RoomsFieldName = "Rms";
        public const string RoomsCountFieldName = "RmC";
        public const string RoomsCheapestPriceFieldName = "CPr";
        public const string RoomsCheapestPricePerNightFieldName = "CPN";
        public const string RoomsHighestPriceFieldName = "RHg";

        public const string CountrySeoFriendlyNameFieldName = "CSN";
        public const string DestinationSeoFriendlyNameFieldName = "DSN";
        public const string EstablishmentSeoFriendlyNameFieldName = "ESN";
        public const string EstablishmentDescriptionFieldName = "EDs";
        public const string EstablishmentTypeFieldName = "ETp";
        public const string EstablishmentAddressLine1FieldName = "EA1";
        public const string EstablishmentAddressLine2FieldName = "EA2";
        public const string EstablishmentAddressCityFieldName = "EAC";
        public const string EstablishmentAddressPostCodeFieldName = "EAP";
        public const string EstablishmentTopImageUrlFieldName = "ETI";
        public const string EstablishmentReviewCountFieldName = "ERC";
        public const string EstablishmentReviewAverageScoreFieldName = "ERA";
        public const string EstablishmentTripAdvisorReviewCountFieldName = "TAC";
        public const string EstablishmentTripAdvisorAverageScoreFieldName = "TAA";
        public const string EstablishmentPopularityFieldName = "EPo";
        public const string EstablishmentLongitudeFieldName = "ELg";
        public const string EstablishmentLatitudeCountFieldName = "ELt";
        public const string CacheExpireDateFieldName = "CED";
        public const string DestinationReferenceFieldName = "DtR";
        public const string IsDestinationDistrictFieldName = "IDD";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationIdFieldName)]
        public Guid DestinationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentIdFieldName)]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentNameFieldName)]
        public string EstablishmentName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentStarRatingFieldName)]
        public StarRatingType EstablishmentStarRating { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentFacilitiesFieldName)]
        public Facility[] EstablishmentFacilities { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTravellerTypesFieldName)]
        public TravellerType[] EstablishmentTravellerTypes { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ResortNameFieldName)]
        public string ResortName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsFieldName)]
        public AccommodationB2BWebAvailabilityResultRoom[] Rooms { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCountFieldName)]
        public int RoomsCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCheapestPriceFieldName)]
        public Money RoomsCheapestPrice { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsCheapestPricePerNightFieldName)]
        public Money RoomsCheapestPricePerNight { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsHighestPriceFieldName)]
        public Money RoomsHighestPrice { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CacheExpireDateFieldName)]
        public DateTime CacheExpireDate { get; set; }

        // Details

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CountrySeoFriendlyNameFieldName)]
        public string CountrySeoFriendlyName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationNameFieldName)]
        public string DestinationName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationSeoFriendlyNameFieldName)]
        public string DestinationSeoFriendlyName { get; set; }
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentSeoFriendlyNameFieldName)]
        public string EstablishmentSeoFriendlyName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentDescriptionFieldName)]
        public string EstablishmentDescription { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTypeFieldName)]
        public EstablishmentType EstablishmentType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentAddressLine1FieldName)]
        public string EstablishmentAddressLine1 { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentAddressLine2FieldName)]
        public string EstablishmentAddressLine2 { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentAddressCityFieldName)]
        public string EstablishmentAddressCity { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentAddressPostCodeFieldName)]
        public string EstablishmentAddressPostCode { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTopImageUrlFieldName)]
        public string EstablishmentTopImageUrl { get; set; }
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentReviewCountFieldName)]
        public int EstablishmentReviewCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentReviewAverageScoreFieldName)]
        public int EstablishmentReviewAverageScore { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTripAdvisorAverageScoreFieldName)]
        public decimal? EstablishmentTripAdvisorAverageScore { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentTripAdvisorReviewCountFieldName)]
        public int? EstablishmentTripAdvisorReviewCount { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentPopularityFieldName)]
        public int EstablishmentPopularity { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentLongitudeFieldName)]
        public double? EstablishmentLongitude { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentLatitudeCountFieldName)]
        public double? EstablishmentLatitude { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationReferenceFieldName)]
        public int DestinationReference { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IsDestinationDistrictFieldName)]
        public bool IsDestinationDistrict { get; set; }
    }
}
