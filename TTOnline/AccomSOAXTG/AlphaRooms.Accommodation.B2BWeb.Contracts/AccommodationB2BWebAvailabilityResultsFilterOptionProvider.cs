﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityResultsFilterOptionProvider
    {
        public const string ProvideEdiCodeFieldName = "PrC";
        public const string ProviderNameFieldName = "PrN";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProvideEdiCodeFieldName)]
        public string ProvideEdiCode { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProviderNameFieldName)]
        public string ProviderName { get; set; } 
    }
}
