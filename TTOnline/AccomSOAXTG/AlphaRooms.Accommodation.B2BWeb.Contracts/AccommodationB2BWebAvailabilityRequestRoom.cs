﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityRequestRoom
    {
        public const string RoomNumberFieldName = "RmU";
        public const string AdultsFieldName = "Adt";
        public const string ChildrenFieldName = "Chd";
        public const string ChildAgesFieldName = "CdA";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomNumberFieldName)]
        public byte RoomNumber { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AdultsFieldName)]
        public byte Adults { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChildrenFieldName)]
        public byte Children { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChildAgesFieldName)]
        public byte[] ChildAges { get; set; }
    }
}
