﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebValuationResultRoom
    {
        [DataMember]
        public string RoomId { get; set; }

        [DataMember]
        public byte RoomNumber { get; set; }

        [DataMember]
        public string ProviderEdiCode { get; set; }

        [DataMember]
        public string ProviderName { get; set; }

        [DataMember]
        public Guid DestinationId { get; set; }

        [DataMember]
        public string DestinationName { get; set; }

        [DataMember]
        public Guid EstablishmentId { get; set; }

        [DataMember]
        public string EstablishmentName { get; set; }

        [DataMember]
        public string ProviderEstablishmentCode { get; set; }

        [DataMember]
        public DateTime CheckInDate { get; set; }

        [DataMember]
        public DateTime CheckOutDate { get; set; }

        [DataMember]
        public byte AdultsCount { get; set; }

        [DataMember]
        public byte ChildrensCount { get; set; }

        [DataMember]
        public byte InfantsCount { get; set; }

        [DataMember]
        public string RoomDescription { get; set; }

        [DataMember]
        public string ProviderRoomCode { get; set; }

        [DataMember]
        public BoardType BoardType { get; set; }

        [DataMember]
        public string BoardDescription { get; set; }

        [DataMember]
        public bool IsSpecialRate { get; set; }

        [DataMember]
        public bool IsOpaqueRate { get; set; }

        [DataMember]
        public bool IsNonRefundable { get; set; }

        [DataMember]
        public bool IsBindingRate { get; set; }

        [DataMember]
        public PaymentModel PaymentModel { get; set; }

        [DataMember]
        public Money ProviderPrice { get; set; }

        [DataMember]
        public PromotionalDiscountResult Discount { get; set; }

        [DataMember]
        public Money Price { get; set; }

        [DataMember]
        public AccommodationB2BWebValuationResultRoomPriceItem[] PriceBreakdown { get; set; }

        [DataMember]
        public Money PricePerNight { get; set; }

        [DataMember]
        public Money PaymentToTake { get; set; }

        [DataMember]
        public string CancellationPolicy { get; set; }

        [DataMember]
        public decimal ExchangeRate { get; set; }

        [DataMember]
        public bool IsCardChargeApplicable { get; set; }

        [DataMember]
        public decimal Margin { get; set; }

        [DataMember]
        public RateType RateType { get; set; }

        [DataMember]
        public AccommodationB2BWebValuationResultVccPaymentRule[] VccPaymentRules { get; set; }

        [DataMember]
        public DateTime CacheExpireDate { get; set; }

        [DataMember]
        public decimal? AgentCommissionRate { get; set; }

        [DataMember]
        public PriceHikeDetail PriceHikeDetail { get; set; }
    }
}
