﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityRequest
    {
        public const string AvailabilityIdFieldName = "AId";
        public const string ChannelFieldName = "Chn";
        public const string SearchTypeFieldName = "STp";
        public const string AgentIdFieldName = "AgI";
        public const string DestinationIdFieldName = "DsI";
        public const string EstablishmentIdFieldName = "EtI";
        public const string CheckInDateFieldName = "CID";
        public const string CheckOutDateFieldName = "COD";
        public const string RoomsFieldName = "Rms";
        public const string ProvidersToSearchFieldName = "PTS";
        public const string SuppliersToSearchFieldName = "STS";
        public const string IgnoreConsolidationFieldName = "IgC";
        public const string DebuggingFieldName = "Dbg";
        public const string PromotionalCodeFieldName = "Prm";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityIdFieldName)]
        public Guid? AvailabilityId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChannelFieldName)]
        public Channel Channel { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SearchTypeFieldName)]
        public SearchType SearchType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AgentIdFieldName)]
        public int AgentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DestinationIdFieldName)]
        public Guid? DestinationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EstablishmentIdFieldName)]
        public Guid? EstablishmentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CheckInDateFieldName)]
        public DateTime CheckInDate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(CheckOutDateFieldName)]
        public DateTime CheckOutDate { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(RoomsFieldName)]
        public AccommodationB2BWebAvailabilityRequestRoom[] Rooms { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ProvidersToSearchFieldName)]
        public int[] ProvidersToSearch { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SuppliersToSearchFieldName)]
        public string[] SuppliersToSearch { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IgnoreConsolidationFieldName)]
        public bool IgnoreConsolidation { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DebuggingFieldName)]
        public bool Debugging { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PromotionalCodeFieldName)]
        public string PromotionalCode { get; set; }
    }
}
