﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebValuationRequest
    {
        public const string ValuationIdFieldName = "VId";
        public const string ChannelFieldName = "Chn";
        public const string AvailabilityIdFieldName = "AId";
        public const string AgentIdFieldName = "AgI";
        public const string SelectedRoomIdsFieldName = "SRI";
        public const string DebuggingFieldName = "Dbg";
        public const string PromotionalCodeFieldName = "Prm";

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ValuationIdFieldName)]
        public Guid? ValuationId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ChannelFieldName)]
        public Channel Channel { get; set; }
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AgentIdFieldName)]
        public int AgentId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SelectedRoomIdsFieldName)]
        public string[] SelectedRoomIds { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(DebuggingFieldName)]
        public bool Debugging { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(PromotionalCodeFieldName)]
        public string PromotionalCode { get; set; }
    }
}
