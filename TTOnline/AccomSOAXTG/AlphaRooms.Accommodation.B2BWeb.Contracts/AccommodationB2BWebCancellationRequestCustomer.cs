﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebCancellationRequestCustomer
    {
        public const string IdFieldName = "Id";
        public const string FirstNameFieldName = "FNm";
        public const string SurnameFieldName = "SNm";
        public const string EmailAddressFieldName = "Eml";
        public const string ContactNumberFieldName = "CtN";
        
        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(IdFieldName)]
        public int Id { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(FirstNameFieldName)]
        public string FirstName { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(SurnameFieldName)]
        public string Surname { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(EmailAddressFieldName)]
        public string EmailAddress { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(ContactNumberFieldName)]
        public string ContactNumber { get; set; }
    }
}
