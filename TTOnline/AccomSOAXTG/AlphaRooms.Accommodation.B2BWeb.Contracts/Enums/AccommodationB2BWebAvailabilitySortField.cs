﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts.Enums
{
    public enum AccommodationB2BWebAvailabilitySortField
    {
        MostPopular = 0
        , CheapestPrice = 1
        , CustomerRating = 2
        , TripAdvisorRating = 3
        , StarRating = 4
        , EstablishmentName = 5
    }
}
