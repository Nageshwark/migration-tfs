﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts.Enums
{
    public enum AccommodationB2BWebAvailabilitySortOrder
    {
        Ascendant = 0
        , Descendent = 1
    }
}
