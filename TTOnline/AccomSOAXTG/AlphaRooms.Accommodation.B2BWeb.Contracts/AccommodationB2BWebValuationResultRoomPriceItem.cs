﻿using System.Runtime.Serialization;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    public class AccommodationB2BWebValuationResultRoomPriceItem
    {
        [DataMember]
        public AccommodationResultPriceItemType Type { get; set; }

        [DataMember]
        public Money SalePrice { get; set; }

        [DataMember]
        public Money CostPrice { get; set; }
    }
}