﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Contracts
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class AccommodationB2BWebAvailabilityResultsFilterOptionBoardType
    {
        public const string NameFieldName = "Nam";

        [DataMember]
        [BsonId]
        public BoardType BoardType { get; set; }

        [DataMember]
        [BsonIgnoreIfDefault]
        [BsonElement(NameFieldName)]
        public string Name { get; set; }
    }
}