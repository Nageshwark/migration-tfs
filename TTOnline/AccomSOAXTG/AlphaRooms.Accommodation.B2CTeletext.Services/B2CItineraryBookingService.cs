﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2CBookingService1 = AlphaRooms.Accommodation.B2CTeletext.Interfaces.B2BBookingService;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CItineraryBookingService : IB2CItineraryBookingService
    {
        private readonly IB2CItineraryBookingFactory itineraryBookingFactory;
        private readonly IB2CItineraryBookingMapping itineraryBookingMapping;
        private readonly IEndpointConfiguration endpointConfiguration;

        public B2CItineraryBookingService(IB2CItineraryBookingFactory itineraryBookingFactory, IB2CItineraryBookingMapping itineraryBookingMapping, IEndpointConfiguration endpointConfiguration)
        {
            this.itineraryBookingFactory = itineraryBookingFactory;
            this.itineraryBookingMapping = itineraryBookingMapping;
            this.endpointConfiguration = endpointConfiguration;
        }
        //Signature changed added valuatedRooms to get SpecialRequests if any - 14th June 2016
        public async Task<Itinerary> CreateItineraryAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationB2CValuationResult B2CValuationResult, TTAccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms, TTAccommodationBookingRequestRoomPaymentDetails paymentDetails)
        {

            var client = this.GetClientFromEndpointAddress();
            var request = await itineraryBookingFactory.CreateItineraryRequestAsync(agentId, channel, valuationId, valuationResult, B2CValuationResult, customer, valuatedRooms, paymentDetails);


            //var json = JsonConvert.SerializeObject(request);
            var response = await client.CreateItineraryAsync(request);
            return itineraryBookingMapping.MapToItinerary(response);
        }

        public async Task UpdateItineraryAsync(Itinerary itinerary, AccommodationBookingResult bookingResult)
        {
            var endpoint = this.endpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new B2CBookingService1.AlphaBookingServiceClient("*")
                : new B2CBookingService1.AlphaBookingServiceClient("*", endpoint);
            foreach (var bookingRoomResult in bookingResult.Rooms)
            {
                var accommodationBookingRoom = itinerary.AccommodationBookingRooms.First(i => i.RoomNumber == bookingRoomResult.RoomNumber);
                var request = itineraryBookingFactory.CreateUpdateBookingItemRequest(itinerary, accommodationBookingRoom, bookingResult, bookingRoomResult);
                var response = await client.UpdateBookingItemAsync(request);
            }
            var updateBookingResponse = await client.UpdateBookingStatusesForItineraryAsync(itinerary.ItineraryReference);
            var updateItineraryRequest = itineraryBookingFactory.CreateDetermineAndUpdateItineraryRequest(itinerary);
            var updateItineraryResponse = await client.DetermineAndUpdateItineraryStatusAsync(updateItineraryRequest);

            var email = await client.SendConfirmationEmailAsync(itinerary.ItineraryReference, default(int));

        }


        private B2CBookingService1.AlphaBookingServiceClient GetClientFromEndpointAddress()
        {
            var endpoint = this.endpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new B2CBookingService1.AlphaBookingServiceClient("*")
                : new B2CBookingService1.AlphaBookingServiceClient("*", endpoint);
            return client;

        }
    }
}
