﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
//using AlphaRooms.Accommodation.B2CTeletext.DomainModels.Content;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.B2CTeletext.WcfInterface.Interface;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class OnlineBookabilityService : IB2COnlineBookabilityService
    {
        private readonly B2CDestinationService destinationService;

        public OnlineBookabilityService(B2CDestinationService destinationService)
        {
            this.destinationService = destinationService;
        }

        public DestinationHotelAutoSuggest GetHotelAndDestinationAutoSuggestions(string term, Channel channel)
        {
            var suggestions = new DestinationHotelAutoSuggest();
            var destinationSuggestions = new List<DestinationAutoSuggest>();
            var hotelSuggestions = new List<HotelAutoSuggest>();

            var autoSuggestions = destinationService.GetAutoSuggestionsHotelAndDestination(term, channel);

            foreach (var suggestion in autoSuggestions)
            {
                switch (suggestion.Category)
                {
                    case AutoSuggestCategory.Destinations:
                        destinationSuggestions.Add(this.CreateDestinationAutoSuggestViewModel(suggestion, AutoSuggestCategory.Destinations));
                        break;
                    case AutoSuggestCategory.Hotels:
                        hotelSuggestions.Add(this.CreateHotelAutoSuggestViewModel(suggestion, AutoSuggestCategory.Hotels));
                        break;
                }
            }

            if (destinationSuggestions.Count > 0)
            {
                suggestions.DestinationAutoSuggest = new DestinationAutoSuggest[destinationSuggestions.Count];
                suggestions.DestinationAutoSuggest = destinationSuggestions.ToArray();
            }
            if (hotelSuggestions.Count > 0)
            {
                suggestions.HotelAutoSuggest = new HotelAutoSuggest[hotelSuggestions.Count];
                suggestions.HotelAutoSuggest = hotelSuggestions.ToArray();
            }
            return suggestions;
        }

        private DestinationAutoSuggest CreateDestinationAutoSuggestViewModel(AutoSuggest autoSuggest, AutoSuggestCategory AutoSuggestCategory)
        {
            return new DestinationAutoSuggest
            {
                Name = autoSuggest.Name,
                LocationId = autoSuggest.LocationId,
                Category = autoSuggest.Category.ToString(),
                DestinationId = autoSuggest.DestinationId != null ? autoSuggest.DestinationId.ToString() : string.Empty
            };
        }
        private HotelAutoSuggest CreateHotelAutoSuggestViewModel(AutoSuggest autoSuggest, AutoSuggestCategory AutoSuggestCategory)
        {
            return new HotelAutoSuggest
            {
                Name = autoSuggest.Name,
                LocationId = autoSuggest.LocationId,
                Category = autoSuggest.Category.ToString(),
                DestinationId = autoSuggest.DestinationId != null ? autoSuggest.DestinationId.ToString() : string.Empty
            };
        }
    }
}
