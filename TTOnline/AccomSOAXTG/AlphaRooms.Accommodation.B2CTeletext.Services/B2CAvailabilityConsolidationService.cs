﻿namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AlphaRooms.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /*
      * Warning 
      * 
      * This file is performance critical (speed). All changes must be profiled!
      */

    /// <summary>
    /// Marks the cheapest rooms are marked as "IsLowestPrice" for an establishmentId & board type
    /// de-duplicates rooms if room description is a fuzzy match and board type, board description
    /// </summary>
    public class B2CAvailabilityConsolidationService : IAvailabilityResultConsolidationService
    {
        // for deduplication
        private readonly IRoomDescriptionComparer roomDescriptionComparer;
        private readonly IAvailabilityResultProviderPriorityService providerPriorityService;

        public B2CAvailabilityConsolidationService(IRoomDescriptionComparer roomDescriptionComparer, IAvailabilityResultProviderPriorityService providerPriorityService)
        {
            this.roomDescriptionComparer = roomDescriptionComparer;
            this.providerPriorityService = providerPriorityService;
        }

        public IList<AccommodationAvailabilityResult> ConsolidateAvailabilityResults(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            // I think this is wrong and should not be done here - Dan
            results = results.Where(i => !i.ProviderResult.IsOpaqueRate).ToArray();

            if (availabilityRequest.IgnoreConsolidation)
            {
                return results;
            }

            var consolidationGroups = B2CAvailabilityConsolidationService.CreateConsolidationGroups(results);

            Parallel.ForEach(consolidationGroups, consolidationGroup =>
            {
                // now when have the groups sort them in order
                var orderedConsolidationGroup = consolidationGroup
                    .OrderBy(x => x.Price) // cheapest
                    .ThenBy(x => x.CostPrice) // most profit
                    .ThenByDescending(this.providerPriorityService.GetPriority) // provoder piority - most profit
                    .ThenBy(x => x.RoomDescription.Length) // better looking room name
                    .ThenBy(x => x.RoomDescription, StringComparer.OrdinalIgnoreCase); // to provide a stable order

                B2CAvailabilityConsolidationService.Consolidation(orderedConsolidationGroup);
                this.Deduplication(orderedConsolidationGroup);
            });

            return results;
        }

        private void Deduplication(IEnumerable<AccommodationAvailabilityResult> consolidtionGroup)
        {
            var comparer = this.CreateDeduplicationComparer();

            // the groups rooms will already be sorted by the parent method
            var deduplicationGroups = consolidtionGroup.GroupBy(i => i, comparer);

            foreach (var deduplicationGroup in deduplicationGroups)
            {
                AccommodationAvailabilityResult cheapest = null;
                AccommodationAvailabilityResult expensive = null;

                foreach (var room in deduplicationGroup)
                {
                    if (cheapest == null)
                    {
                        cheapest = room;
                    }
                    else
                    {
                        room.IsDuplicateRate = true;
                        expensive = room;
                    }
                }

                if (expensive != null)
                {
                    cheapest.HighestPrice = expensive.Price;
                }
            }
        }

        private IEqualityComparer<AccommodationAvailabilityResult> CreateDeduplicationComparer()
        {
            return new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                this.roomDescriptionComparer.EqualsRoomDescription(i, j));
        }

        private static void Consolidation(IEnumerable<AccommodationAvailabilityResult> consolidtionGroup)
        {
            // this just marks lowest price
            consolidtionGroup.First().IsLowestPrice = true;
        }

        private static IEnumerable<IEnumerable<AccommodationAvailabilityResult>> CreateConsolidationGroups(IList<AccommodationAvailabilityResult> results)
        {
            var comparer = B2CAvailabilityConsolidationService.CreateConsolidationComparer();

            return results.GroupBy(i => i, comparer);
        }

        private static IEqualityComparer<AccommodationAvailabilityResult> CreateConsolidationComparer()
        {
            return new CustomEqualityComparer<AccommodationAvailabilityResult>((i, j) =>
                i.EstablishmentId == j.EstablishmentId
                && i.BoardType == j.BoardType
                && i.RoomNumber == j.RoomNumber);
        }
    }
}
