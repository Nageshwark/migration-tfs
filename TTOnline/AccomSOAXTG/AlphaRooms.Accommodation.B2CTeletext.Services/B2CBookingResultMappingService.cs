﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CBookingResultMappingService : IB2CBookingResultMappingService
    {
        static B2CBookingResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationBookingResult, AccommodationB2CBookingResult>();
                Mapper.CreateMap<AccommodationBookingResultRoom, AccommodationB2CBookingResultRoom>();
                Mapper.CreateMap<AccommodationProviderBookingResult, AccommodationB2CBookingResultRoom>();
            }
        }
        
        private readonly ILogger logger;

        public B2CBookingResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public AccommodationB2CBookingResult MapFromCoreBookingResults(AccommodationB2CBookingRequest bookingRequest, AccommodationBookingResult coreBookingResult)
        {
            AccommodationB2CBookingResult result = null;
            try
            {
                result = Mapper.Map<AccommodationBookingResult, AccommodationB2CBookingResult>(coreBookingResult);
                result.Rooms = coreBookingResult.Rooms.AsParallel().Select((i,j) => CreateAccommodationB2CBookingResultRoom(bookingRequest, i, bookingRequest.ValuatedRooms[j])).ToArray();

            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreBookingResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreBookingResult.ToIndentedJson());
            }
            return result;
        }

        private AccommodationB2CBookingResultRoom CreateAccommodationB2CBookingResultRoom(AccommodationB2CBookingRequest bookingRequest, AccommodationBookingResultRoom coreBookingResultRoom
            , AccommodationB2CBookingRequestRoom accommodationB2CBookingRequestRoom)
        {
            var result = Mapper.Map<AccommodationProviderBookingResult, AccommodationB2CBookingResultRoom>(coreBookingResultRoom.ProviderBookingResult);
            result = Mapper.Map<AccommodationBookingResultRoom, AccommodationB2CBookingResultRoom>(coreBookingResultRoom, result);
            result.RoomId = accommodationB2CBookingRequestRoom.ValuatedRoomId;
            result.ProviderRoomCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RoomCode;
            result.ProviderName = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderName ?? coreBookingResultRoom.Provider.Name;
            result.ProviderEdiCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderProviderCode ?? coreBookingResultRoom.Provider.EdiCode;
            result.DestinationId = coreBookingResultRoom.ValuationResultRoom.DestinationId;
            result.DestinationName = ""; //coreBookingResultRoom.ValuationResultRoom.DestinationName;
            result.EstablishmentId = coreBookingResultRoom.ValuationResultRoom.EstablishmentId;
            result.EstablishmentName = ""; //coreBookingResultRoom.ValuationResultRoom.EstablishmentName;
            result.ProviderEstablishmentCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderEstablishmentCode ?? coreBookingResultRoom.ValuationResultRoom.ProviderResult.EstablishmentEdiCode;
            result.CheckInDate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CheckInDate;
            result.CheckOutDate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CheckOutDate;
            result.AdultsCount = coreBookingResultRoom.ValuationResultRoom.AdultsCount;
            result.ChildrenAndInfantsCount = (byte)coreBookingResultRoom.ValuationResultRoom.ChildrenCount + coreBookingResultRoom.ValuationResultRoom.InfantsCount;
            result.RoomNumber = coreBookingResultRoom.RoomNumber;
            result.RoomDescription = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RoomDescription;
            result.BoardCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.BoardCode;
            result.BoardType = coreBookingResultRoom.ValuationResultRoom.BoardType;
            result.BoardDescription = coreBookingResultRoom.ValuationResultRoom.ProviderResult.BoardDescription;
               //, IsSpecialRate = result.IsSpecialRate
            result.PaymentModel = coreBookingResultRoom.ValuationResultRoom.ProviderResult.PaymentModel;
            result.IsBindingRate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsBindingRate;
            result.IsOpaqueRate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsOpaqueRate;
            result.IsNonRefundable = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsNonRefundable;
            result.CancellationPolicy = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CancellationPolicy.ToString("");
            result.Price = coreBookingResultRoom.ValuationResultRoom.Price;
            result.ProviderPrice = coreBookingResultRoom.ValuationResultRoom.CostPrice;
            result.PaymentToTake = coreBookingResultRoom.ValuationResultRoom.PaymentToTake;
            result.IsCardChargeApplicable = (result.PaymentModel != PaymentModel.CustomerPayDirect);
            result.ExchangeRate = coreBookingResultRoom.ValuationResultRoom.ExchangeRate;
            result.Margin = coreBookingResultRoom.ValuationResultRoom.Margin;
            result.RateType = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RateType;
            result.KeyCollectionInformation = coreBookingResultRoom.ProviderBookingResult.KeyCollectionInformation;
            return result;
        }
    }
}
