﻿using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2CBookingService1 = AlphaRooms.Accommodation.B2CTeletext.Interfaces.B2BBookingService;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts.Enumerators;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.DomainModels.Enums;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CItineraryBookingFactory : IB2CItineraryBookingFactory
    {
        private readonly IBoardRepository boardTypeRepository;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public B2CItineraryBookingFactory(IBoardRepository boardTypeRepository, IEstablishmentDetailsRepository establishmentDetailsRepository)
        {
            this.boardTypeRepository = boardTypeRepository;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<B2CBookingService1.CreateItineraryRequest> CreateItineraryRequestAsync(int agentId, Channel channel, Guid valuationId, AccommodationValuationResult valuationResult, AccommodationB2CValuationResult B2CValuationResult, TTAccommodationBookingRequestCustomer customer, AccommodationBookingRequestRoom[] valuatedRooms, TTAccommodationBookingRequestRoomPaymentDetails paymentDetails)
        {
            var establishmentDetailsDic = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(valuationResult.Rooms[0].BaseDestinationId, channel);
            var establishmentDetails = establishmentDetailsDic[valuationResult.Rooms[0].EstablishmentId];
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            return new B2CBookingService1.CreateItineraryRequest()
            {
                AgentId = agentId,
                Customer = CreateB2BBookingCustomer(customer)
                ,
                Channel = ToB2BBookingChannel(channel)
                ,
                Rooms = valuationResult.Rooms.Select((i, index) => CreateItineraryBookingRoom(valuationId, i, establishmentDetails, boardTypes, valuatedRooms.FirstOrDefault(v => v.ValuatedRoomId == i.RoomId), B2CValuationResult.Rooms.Where(p => p.RoomId == i.RoomId).FirstOrDefault())).ToArray()
                ,
                CardDetails = CreateB2BBookingPaymentDetails(paymentDetails)
                ,
                Travellers = CreateB2BBookingPerson(valuatedRooms).ToArray()
            };
        }

        private B2CBookingService1.Channel ToB2BBookingChannel(Channel channel)
        {
            return (B2CBookingService1.Channel)channel;
        }

        private B2CBookingService1.BookingRoom CreateItineraryBookingRoom(Guid valuationId, AccommodationValuationResultRoom valuationResultRoom
            , EstablishmentDetails establishmentDetails, Dictionary<BoardType, Board> boardTypes, AccommodationBookingRequestRoom accommodationBookingRequestRoom, AccommodationB2CValuationResultRoom b2cValuationResultRoom)
        {
            return new B2CBookingService1.BookingRoom()
            {
                ValuationId = valuationId
                ,
                Room = CreateItineraryBookingRoomRoom(valuationResultRoom, establishmentDetails, boardTypes, b2cValuationResultRoom),
                Preferences = new B2CBookingService1.RoomPreference()
                {
                    AdjoiningRooms = accommodationBookingRequestRoom?.SpecialRequests?.AdjoiningRooms ?? false,
                    LateArrival = accommodationBookingRequestRoom?.SpecialRequests?.LateArrival ?? false,
                    NonSmoking = accommodationBookingRequestRoom?.SpecialRequests?.NonSmoking ?? false,
                    SeaViews = accommodationBookingRequestRoom?.SpecialRequests?.SeaViews ?? false,
                    OtherRequests = accommodationBookingRequestRoom?.SpecialRequests?.OtherRequests ?? string.Empty,
                    Cot = accommodationBookingRequestRoom?.SpecialRequests?.CotRequired ?? false,
                    DisabledFriendly = accommodationBookingRequestRoom?.SpecialRequests?.SeaViews ?? false
                }, //FOR SENDING PREFERENCES IN BOOKING SERVICE
                Guests = accommodationBookingRequestRoom.Guests.Select((i, index) => CreateB2BBookingPerson(i)).ToArray()
            };
        }

        private B2CBookingService1.ValuationRoom CreateItineraryBookingRoomRoom(AccommodationValuationResultRoom valuationResultRoom, EstablishmentDetails establishmentDetails
            , Dictionary<BoardType, Board> boards, AccommodationB2CValuationResultRoom b2cValuationResultRoom)
        {

            var agentCommissionRate = valuationResultRoom.SpecificData == null ? null : valuationResultRoom.SpecificData.GetValueOrDefault("AgentCommissionRate");
            var cancellationPolicy = valuationResultRoom.CancellationPolicies.Select(i => "<p>" + i + "</p>").ToString("");
            return new B2CBookingService1.ValuationRoom()
            {
                RoomId = valuationResultRoom.RoomNumber
                ,
                RoomId2 = valuationResultRoom.RoomId
                ,
                RoomCode = valuationResultRoom.ProviderResult.RoomCode
                ,
                Supplier = valuationResultRoom.ProviderResult.ProviderName ?? valuationResultRoom.Provider.Name
                ,
                SupplierEdiCode = valuationResultRoom.ProviderResult.ProviderProviderCode ?? valuationResultRoom.Provider.EdiCode
                ,
                DestinationId = valuationResultRoom.DestinationId
                ,
                Destination = establishmentDetails.DestinationName
                ,
                EstablishmentKey = valuationResultRoom.EstablishmentId
                ,
                EstablishmentName = establishmentDetails.EstablishmentName
                ,
                EstablishmentEdiCode = valuationResultRoom.ProviderResult.ProviderEstablishmentCode ?? valuationResultRoom.ProviderResult.EstablishmentEdiCode
                ,
                FromDate = valuationResultRoom.ProviderResult.CheckInDate
                ,
                ToDate = valuationResultRoom.ProviderResult.CheckOutDate
                ,
                NumberOfAdults = valuationResultRoom.AdultsCount
                ,
                NumberOfChildren = valuationResultRoom.ChildrenCount + valuationResultRoom.InfantsCount
                ,
                RoomNumber = valuationResultRoom.RoomNumber
                ,
                RoomDescription = valuationResultRoom.RoomDescription ?? ""
                ,
                BoardCode = valuationResultRoom.BoardType.ToString()
                ,
                BoardType = this.ToBoardType(valuationResultRoom.BoardType)
                ,
                BoardDescription = boards[valuationResultRoom.BoardType].Name

                //, IsSpecialRate = result.IsSpecialRate
                ,
                PriceElements = this.CreateAccommodationPriceElements(b2cValuationResultRoom).ToArray()
                ,
                PaymentModel = this.ToPaymentModel(valuationResultRoom.ProviderResult.PaymentModel)
                ,
                IsBindingRate = valuationResultRoom.ProviderResult.IsBindingRate
                ,
                IsOpaqueRate = valuationResultRoom.ProviderResult.IsOpaqueRate
                ,
                IsNonRefundable = valuationResultRoom.ProviderResult.IsNonRefundable
                ,
                RateType = this.ToRateType(valuationResultRoom.ProviderResult.RateType)
                ,
                Margin = valuationResultRoom.Margin
                ,
                BookingConditions = this.CreateBookingConditions(cancellationPolicy)
                ,
                VccPaymentTerms = this.CreateVccPaymentTerms(valuationResultRoom.ProviderResult.VirtualCreditCardPaymentTerms)
                ,
                AgentCommissionRate = (!string.IsNullOrEmpty(agentCommissionRate) ? decimal.Parse(agentCommissionRate) : (decimal?)null)
            };
        }

        private B2CBookingService1.VccPaymentTerm[] CreateVccPaymentTerms(AccommodationProviderValuationResultVccPaymentRule[] vccPaymentRules)
        {
            if (!vccPaymentRules.Any()) return new B2CBookingService1.VccPaymentTerm[0];
            return vccPaymentRules.Select(CreateVccPaymentTerm).ToArray();
        }

        private B2CBookingService1.VccPaymentTerm CreateVccPaymentTerm(AccommodationProviderValuationResultVccPaymentRule i)
        {
            return new B2CBookingService1.VccPaymentTerm()
            {
                PaymentDate = i.PaymentDate
                ,
                AmountToPay = ToMoney(i.Price)
            };
        }

        private B2CBookingService1.AccommodationBookingConditions CreateBookingConditions(string cancellationPolicy)
        {
            if (string.IsNullOrEmpty(cancellationPolicy)) return new B2CBookingService1.AccommodationBookingConditions();
            return new B2CBookingService1.AccommodationBookingConditions()
            {
                Descriptions = new Dictionary<B2CBookingService1.BookingConditionType, string[]>
                { { B2CBookingService1.BookingConditionType.Cancellation, new string[] { cancellationPolicy } } }
            };
        }

        private B2CBookingService1.PriceElement[] CreateAccommodationPriceElements(AccommodationValuationResultRoom valuationResultRoom)
        {
            return valuationResultRoom.PriceBreakdown.Select(i => CreatePriceElement(valuationResultRoom, i)).ToArray();
        }

        private IList<B2CBookingService1.PriceElement> CreateAccommodationPriceElements(AccommodationB2CValuationResultRoom result)
        {
            var priceElements = new List<B2CBookingService1.PriceElement>();
            if (result.Discount != null)
            {
                var cost = new B2CBookingService1.Money() { Value = result.Discount.Discount.Amount, Currency = result.Discount.Discount.CurrencyCode }; //Money(result.Discount.Discount.Amount, result.Discount.Discount.CurrencyCode);
                priceElements.Add(new B2CBookingService1.PriceElement
                {
                    Category = B2CBookingService1.BookingElementCategory.Discount,
                    InternalDescription = "Promocode discount: " + result.Discount.Voucher,
                    ExternalDescription = "Voucher discount - " + result.Discount.Description,
                    CostToCustomer = cost,
                    //Setting PaymentToTake same as CostToCustomer(-ve) as the total payment at the booking process 
                    //is calculated as a sum of all booking elements PaymentToTake, so that discount is applied
                    PaymentToTake = cost
                });
            }
            var element = new B2CBookingService1.PriceElement
            {
                Category = B2CBookingService1.BookingElementCategory.Fare,
                InternalDescription = "Accommodation",
                ExchangeRate = result.ExchangeRate,
                IsBindingRate = result.IsBindingRate,
                IsCardChargeApplicable = result.IsCardChargeApplicable,
                Margin = result.Margin
            };

            switch (result.PriceHikeDetail.Status)
            {
                case PriceHikeStatus.Acceptable:
                    element.CostFromSupplier = this.ToMoney(result.PriceHikeDetail.OriginalCost);
                    if (result.Discount != null)
                    {
                        // if a booking-level promo code is applied, the 'original price' is from availability, which does NOT have the discount applied
                        // (because the discount is only applied at valuation).  Therefore, use result.Price instead
                        element.CostToCustomer = this.ToMoney(result.Price);
                    }
                    else
                    {
                        element.CostToCustomer = this.ToMoney(result.PriceHikeDetail.PriceHikeSale);
                    }
                    break;
                case PriceHikeStatus.Unnaceptable:
                    element.CostFromSupplier = result.PriceHikeDetail.PriceHikeCost == null ? this.ToMoney(result.ProviderPrice) : this.ToMoney(result.PriceHikeDetail.PriceHikeCost);
                    // no need to check for promo code discount here, as PriceHikeSale is the valuation amount - ie, it DOES have the discount applied
                    element.CostToCustomer = result.PriceHikeDetail.PriceHikeSale == null ? this.ToMoney(result.Price) : this.ToMoney(result.PriceHikeDetail.PriceHikeSale);
                    break;
                default:
                    // this really should never happen, but let's be defensive about it..
                    element.CostFromSupplier = this.ToMoney(result.ProviderPrice);
                    element.CostToCustomer = this.ToMoney(result.Price);
                    break;
            }

            element.PaymentToTake = result.PaymentModel == PaymentModel.CustomerPayDirect
                ? new B2CBookingService1.Money() { Value = 0, Currency = result.Price.CurrencyCode }
                : element.CostToCustomer;

            priceElements.Add(element);

            //add supplier admin fee
            if (result.IsActiveAccommodationAdminFee && result.AccommodationAdminFee.Amount > 0)
            {
                var elementAccommodationAdmin = new B2CBookingService1.PriceElement
                {
                    Category = B2CBookingService1.BookingElementCategory.AccommodationAdminFee,
                    InternalDescription = "Accommodation Admin Fee",
                    ExchangeRate = result.ExchangeRate,
                    IsBindingRate = result.IsBindingRate,
                    IsCardChargeApplicable = !result.IsCardChargeApplicable,
                    Margin = result.Margin,
                    CostFromSupplier = new B2CBookingService1.Money() { Value = 0, Currency = result.Price.CurrencyCode },
                    CostToCustomer = new B2CBookingService1.Money() { Value = result.AccommodationAdminFee.Amount, Currency = result.Price.CurrencyCode }
                };

                elementAccommodationAdmin.PaymentToTake = result.PaymentModel == PaymentModel.CustomerPayDirect
                  ? elementAccommodationAdmin.CostToCustomer
                  : new B2CBookingService1.Money() { Value = 0, Currency = result.Price.CurrencyCode };

                priceElements.Add(elementAccommodationAdmin);
            }

            return priceElements;
        }

        private B2CBookingService1.PriceElement CreatePriceElement(AccommodationValuationResultRoom result, AccommodationResultPriceItem priceBreakDown)
        {
            switch (priceBreakDown.Type)
            {
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.BasePrice:
                    return CreatePriceElement(result, priceBreakDown, "Accommodation", B2CBookingService1.BookingElementCategory.Fare);
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.AccommodationAdminFee:
                    return CreatePriceElement(result, priceBreakDown, "Accommodation Admin Fee", B2CBookingService1.BookingElementCategory.AdminCharge);
                case Core.Contracts.Enumerators.AccommodationResultPriceItemType.Other:
                    return CreatePriceElement(result, priceBreakDown, "Admin Charge", B2CBookingService1.BookingElementCategory.AdminCharge);

                default:
                    throw new Exception("Unexcepted price breakdown type " + priceBreakDown.Type.ToString());
            }
        }

        private B2CBookingService1.PriceElement CreatePriceElement(AccommodationValuationResultRoom result, AccommodationResultPriceItem priceBreakDown, string elementDescription
            , B2CBookingService1.BookingElementCategory elementCategory)
        {
            return new B2CBookingService1.PriceElement()
            {
                InternalDescription = elementDescription
                ,
                Category = elementCategory
                ,
                CostFromSupplier = priceBreakDown.CostPrice == null ? new B2CBookingService1.Money() { Value = 0, Currency = priceBreakDown.SalePrice.CurrencyCode } : this.ToMoney(priceBreakDown.CostPrice)
                ,
                CostToCustomer = (elementCategory == B2CBookingService1.BookingElementCategory.Fare) ? this.ToMoney(priceBreakDown.SalePrice) : this.ToMoney(priceBreakDown.SalePrice, true)
                ,
                PaymentToTake = (elementCategory == B2CBookingService1.BookingElementCategory.Fare) ? this.ToMoney(priceBreakDown.SalePrice) : this.ToMoney(priceBreakDown.SalePrice, true)
                ,
                ExchangeRate = result.ExchangeRate
                ,
                IsBindingRate = result.ProviderResult.IsBindingRate
                ,
                IsCardChargeApplicable = (result.ProviderResult.PaymentModel != PaymentModel.CustomerPayDirect)
                ,
                Margin = result.Margin
            };
        }

        private List<B2CBookingService1.Person> CreateB2BBookingPerson(AccommodationBookingRequestRoom[] rooms)
        {
            List<B2CBookingService1.Person> persons = new List<Interfaces.B2BBookingService.Person>();
            B2CBookingService1.Person person = new Interfaces.B2BBookingService.Person();
            foreach (var room in rooms.ToList())
            {
                foreach (var guest in room.Guests)
                {
                    person = new Interfaces.B2BBookingService.Person();
                    person.Id = guest.Id;
                    person.Title = guest.TitleString;
                    person.FirstName = guest.FirstName;
                    person.Surname = guest.Surname;
                    person.ContactNumber = string.Empty;
                    person.AgeAtTimeOfTravel = guest.Age;
                    persons.Add(person);
                }
            }

            return persons;
        }


        private B2CBookingService1.Person CreateB2BBookingPerson(AccommodationBookingRequestRoomGuest customer)
        {
            return new B2CBookingService1.Person()
            {
                Id = customer.Id
                ,
                Title = customer.TitleString
                ,
                FirstName = customer.FirstName
                ,
                Surname = customer.Surname
                ,
                ContactNumber = string.Empty
                ,
                AgeAtTimeOfTravel = customer.Age
            };
        }

        private B2CBookingService1.Customer CreateB2BBookingCustomer(TTAccommodationBookingRequestCustomer customer)
        {
            return new B2CBookingService1.Customer()
            {
                Site = customer.Site,
                FirstName = customer.FirstName,
                Surname = customer.Surname,
                Title = customer.TitleString,
                ContactNumber = customer.ContactNumber,
                EmailAddress = customer.EmailAddress,
                AddressLine1 = customer.AddressLine1,
                AddressLine2 = customer.AddressLine2,
                Town = customer.Town,
                Country = customer.Country,
                PostCode = customer.PostCode,
                County = customer.County,
                UserId = customer.UserId,
                Channel = ToB2BBookingChannel(customer.Channel),
                LanguageId = customer.LanguageId,
                B2BUserId = null
            };
        }

        private B2CBookingService1.CardDetails CreateB2BBookingPaymentDetails(TTAccommodationBookingRequestRoomPaymentDetails cardDetails)
        {
            return new B2CBookingService1.CardDetails()
            {
                CardType = (B2CBookingService1.CardType)cardDetails.CardType,
                CardNumber = cardDetails.CardNumber,
                CardHolderName = cardDetails.CardHolderName,
                CardSecurityCode = cardDetails.CardSecurityCode,
                AddressLine1 = cardDetails.AddressLine1,
                AddressLine2 = cardDetails.AddressLine2,
                City = cardDetails.City,
                Country = cardDetails.Country,
                County = cardDetails.County,
                PostalCode = cardDetails.Postcode,
                CountryCode = cardDetails.Country,
                MerchantRefNum = cardDetails.MerchantRefNum,// System.Guid.NewGuid().ToString(), //TODO
                PaySafeVPSAuthCode = cardDetails.PaySafeVPSAuthCode// System.Guid.NewGuid().ToString().Substring(8), //
            };
        }

        private B2CBookingService1.Money ToMoney(Money price, bool isNegative = false)
        {
            return new B2CBookingService1.Money() { Value = isNegative ? (-price.Amount) : price.Amount, Currency = price.CurrencyCode };
        }

        private B2CBookingService1.BoardType ToBoardType(BoardType boardType)
        {
            return (B2CBookingService1.BoardType)boardType;
        }

        private B2CBookingService1.PaymentModel ToPaymentModel(PaymentModel paymentModel)
        {
            return (B2CBookingService1.PaymentModel)paymentModel;
        }

        private B2CBookingService1.RateTypes ToRateType(RateType rateType)
        {
            return (B2CBookingService1.RateTypes)rateType;
        }

        public B2CBookingService1.UpdateBookingItemRequest CreateUpdateBookingItemRequest(Itinerary itinerary, AccommodationBookingRoom accommodationBookingRoom
            , AccommodationBookingResult bookingResult, AccommodationBookingResultRoom bookingRoomResult)
        {
            var isSuccessful = (bookingRoomResult.ProviderBookingResult.BookingStatus == BookingStatus.Confirmed);
            return new B2CBookingService1.UpdateBookingItemRequest()
            {
                AccommodationBookingItemId = accommodationBookingRoom.AccommodationBookingItemId
                ,
                BookedRoom = new B2CBookingService1.BookedRoom()
                {
                    IsSuccessful = isSuccessful
                    ,
                    SupplierBookingReference = bookingRoomResult.ProviderBookingResult.ProviderBookingReference
                    ,
                    ErrorType = (isSuccessful ? B2CBookingService1.BookingErrorTypes.NoBookingError : B2CBookingService1.BookingErrorTypes.SupplierResponse)
                    ,
                    Comment = (isSuccessful ? bookingRoomResult.ProviderBookingResult.Message : null)
                    ,
                    ErrorText = (!isSuccessful ? bookingRoomResult.ProviderBookingResult.Message : null)
                }
            };
        }

        public B2CBookingService1.DetermineItineraryStatusRequest CreateDetermineAndUpdateItineraryRequest(Itinerary itinerary)
        {
            return new B2CBookingService1.DetermineItineraryStatusRequest()
            {
                ItineraryReference = itinerary.ItineraryReference
                ,
                IsBalancePayment = false
            };
        }
    }
}
