﻿using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CDestinationService : IDestinationService
    {
        private readonly IAutoSuggestRepository autoSuggestRepository;

        public B2CDestinationService
        (
            IAutoSuggestRepository autoSuggestRepository
        )
        {
            this.autoSuggestRepository = autoSuggestRepository;
        }

        public List<AutoSuggest> GetAutoSuggestionsHotelAndDestination(string term, Channel channel)
        {
            return autoSuggestRepository.GetAutoSuggestionsForHotelsAndDestinations(term, 2057);
        }
    }
}
