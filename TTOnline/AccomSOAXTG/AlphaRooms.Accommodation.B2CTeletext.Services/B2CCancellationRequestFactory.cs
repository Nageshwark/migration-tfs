﻿using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CCancellationRequestFactory : IB2CCancellationRequestFactory
    {
        static B2CCancellationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CCancellationRequest, AccommodationCancellationRequest>();
                Mapper.CreateMap<AccommodationB2CCancellationRequestCustomer, AccommodationCancellationRequestCustomer>();
            }
        }
        
        public AccommodationCancellationRequest CreateCancellationRequest(AccommodationB2CCancellationRequest cancellationRequest, ChannelInfo channelInfo)
        {
            var request = Mapper.Map<AccommodationB2CCancellationRequest, AccommodationCancellationRequest>(cancellationRequest);
            request.ChannelInfo = channelInfo;
            return request;
        }
    }
}
