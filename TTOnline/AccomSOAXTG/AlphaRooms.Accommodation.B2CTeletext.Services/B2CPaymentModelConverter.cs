﻿using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CPaymentModelConverter : IB2CPaymentModelConverter
    {
        private static readonly Dictionary<PaymentTypeType, PaymentModel[]> paymentModelDic = new Dictionary<PaymentTypeType, PaymentModel[]> { 
            { PaymentTypeType.PayAtHotel, new PaymentModel[] { PaymentModel.CustomerPayDirect } } 
            , { PaymentTypeType.PayNow, new PaymentModel[] { PaymentModel.PayOnBooking, PaymentModel.PayWithVirtualCard, PaymentModel.PostPayment } } 
        };

        public PaymentTypeType ToPaymentType(PaymentModel paymentModel)
        {
            switch (paymentModel)
            {
                case PaymentModel.CustomerPayDirect:
                    return PaymentTypeType.PayAtHotel;
                default:
                    return PaymentTypeType.PayNow;
            }
        }

        public PaymentModel[] ToPaymentModels(PaymentTypeType paymentType)
        {
            return paymentModelDic[paymentType];
        }
    }
}
