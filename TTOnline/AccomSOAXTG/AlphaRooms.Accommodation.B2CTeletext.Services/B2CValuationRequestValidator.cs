﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CValuationRequestValidator : IB2CValuationRequestValidator
    {
        public void ValidateValuationRequest(AccommodationB2CValuationRequest valuationRequest)
        {
            if (valuationRequest.AvailabilityId == Guid.Empty)
                throw new ValidationException("ValidateValuationRequest failed: the request must have AvailabilityId.");
            if (ArrayModule.IsNullOrEmpty(valuationRequest.SelectedRoomIds))
                throw new ValidationException("ValidateValuationRequest failed: the request must have SelectedRoomIds.");
        }
    }
}
