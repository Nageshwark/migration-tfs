﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CAvailabilityFlashSaleService : IAvailabilityFlashSaleService
    {
        private readonly IAccommodationFlashSaleRepository accommodationFlashSaleRepository;
        private Guid[] establishments = null;

        public B2CAvailabilityFlashSaleService(IAccommodationFlashSaleRepository accommodationFlashSaleRepository)
        {
            this.accommodationFlashSaleRepository = accommodationFlashSaleRepository;
        }

        public async Task<IList<AccommodationAvailabilityResult>> FilterResultsForFlashSaleAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            if (establishments == null) await PopulateEstablishmentsAsync();

            if (establishments == null || !establishments.Any()) return results;

            //remove any establishments that are mapped with an agent
            return results.AsParallel().Where(x => !establishments.Any(e => e == x.EstablishmentId)).ToList();
        }

        private async Task PopulateEstablishmentsAsync()
        {
            //cache establishments
            var mappings = await this.accommodationFlashSaleRepository.GetAllAsync();
            establishments = mappings.Where(x => x.IsActive).Select(x => x.EstablishmentId).Distinct().ToArray();
        }
    }
}