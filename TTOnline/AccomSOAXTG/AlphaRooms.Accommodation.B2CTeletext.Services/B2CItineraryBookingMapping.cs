﻿using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.B2BBookingService;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CItineraryBookingMapping : IB2CItineraryBookingMapping
    {
        public Itinerary MapToItinerary(CreateItineraryResponse response)
        {
            var booking =
                response.ItineraryBookings.Where(b => b.BookingType == BookingType.Accommodation)?.FirstOrDefault();
            if (!booking.HasValue) throw new Exception("No valid Itinerary booking id found");

            return new Itinerary()
            {
                ItineraryId = booking.Value.BookingId  // this is misleading, its actually the invoice id being passed to Bev5 for logs - ACS-1100
                ,
                ItineraryReference = response.ItineraryReference
                ,
                AccommodationBookingRooms = response.AccommodationBookingRoomsMap.Select(CreateAccommodationBookingRoom).ToArray()
            };
        }

        private AccommodationBookingRoom CreateAccommodationBookingRoom(CreateItineraryBookingMap accommodationBookingRoom)
        {
            return new AccommodationBookingRoom()
            {
                RoomNumber = accommodationBookingRoom.RoomNumber
                ,
                AccommodationBookingItemId = accommodationBookingRoom.BookingItemId
            };
        }
    }
}
