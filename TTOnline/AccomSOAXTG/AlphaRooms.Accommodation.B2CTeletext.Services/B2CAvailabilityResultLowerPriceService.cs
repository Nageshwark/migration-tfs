﻿namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    using Core.Contracts;
    using Core.Interfaces;
    using System.Collections.Generic;

    public class B2CAvailabilityResultLowerPriceService : IAvailabilityResultLowerPriceService
    {
        public IList<AccommodationAvailabilityResult> ApplyAvailabilityLowerPrice(IList<AccommodationAvailabilityResult> results)
        {
            // do nothing this has been moved to consolidation
            return results;
        }
    }
}
