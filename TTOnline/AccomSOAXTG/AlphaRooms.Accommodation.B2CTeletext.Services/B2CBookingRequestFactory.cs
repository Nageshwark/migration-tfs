﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CBookingRequestFactory : IB2CBookingRequestFactory
    {
        static B2CBookingRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CBookingRequest, TTAccommodationBookingRequest>()
                    .ForMember(i => i.Customer, i => i.Ignore());                    
                Mapper.CreateMap<AccommodationB2CBookingRequestCustomer, TTAccommodationBookingRequestCustomer>();
                Mapper.CreateMap<AccommodationB2CBookingRequestRoom, AccommodationBookingRequestRoom>();
                Mapper.CreateMap<AccommodationB2CBookingRequestRoomPaymentDetails, TTAccommodationBookingRequestRoomPaymentDetails>();
                Mapper.CreateMap<AccommodationB2CBookingRequestRoomGuest, AccommodationBookingRequestRoomGuest>();
                Mapper.CreateMap<AccommodationB2CBookingRequestRoomSpecialRequest, AccommodationBookingRequestRoomSpecialRequest>();
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;

        public B2CBookingRequestFactory(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public TTAccommodationBookingRequest CreateBookingRequest(AccommodationB2CBookingRequest bookingRequest, ChannelInfo channelInfo)
        {
            var request = Mapper.Map<AccommodationB2CBookingRequest, TTAccommodationBookingRequest>(bookingRequest);
            request.Customer = CreateBookingRequestCustomer(bookingRequest);
            request.ValuatedRooms = bookingRequest.ValuatedRooms.Select(this.CreateBookingRequestRoom).ToArray();
            request.ChannelInfo = channelInfo;
            return request;
        }

        private TTAccommodationBookingRequestCustomer CreateBookingRequestCustomer(AccommodationB2CBookingRequest bookingRequest)
        {
            var guest = bookingRequest.ValuatedRooms.SelectMany(i => i.Guests).FirstOrDefault(i => string.Equals(i.FirstName, bookingRequest.Customer.FirstName, StringComparison.CurrentCultureIgnoreCase)
                && string.Equals(i.Surname, bookingRequest.Customer.Surname, StringComparison.CurrentCultureIgnoreCase));
            if (guest == null) guest = bookingRequest.ValuatedRooms[0].Guests[0];
            var customer = Mapper.Map<AccommodationB2CBookingRequestCustomer, TTAccommodationBookingRequestCustomer>(bookingRequest.Customer);
            customer.TitleString = guest.TitleString;
            customer.Age = guest.Age ?? configurationManager.AccommodationAdultAge;
            return customer;
        }

        private AccommodationBookingRequestRoom CreateBookingRequestRoom(AccommodationB2CBookingRequestRoom room)
        {
            var requestRoom = Mapper.Map<AccommodationB2CBookingRequestRoom, AccommodationBookingRequestRoom>(room);
            requestRoom.Guests = room.Guests.Select(this.CreateBookingRequestRoomGuest).ToArray();
            return requestRoom;
        }

        private AccommodationBookingRequestRoomGuest CreateBookingRequestRoomGuest(AccommodationB2CBookingRequestRoomGuest guest)
        {
            var requestRoomGuest = Mapper.Map<AccommodationB2CBookingRequestRoomGuest, AccommodationBookingRequestRoomGuest>(guest);
            if (guest.Age == null) requestRoomGuest.Age = configurationManager.AccommodationAdultAge;
            return requestRoomGuest;
        }
    }
}
