﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Caching;
using AlphaRooms.Accommodation.B2CTeletext.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Services;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System.Threading;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CValuationService : IB2CValuationService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2CRequestStatusService requestStatusService;
        private readonly IB2CValuationLogger valuationLogger;
        private readonly ILogger logger;
        private readonly IB2CValuationRequestFactory requestFactory;
        private readonly IValuationService coreValuationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2CValuationResultMappingService resultsMappingService;
        private readonly IB2CValuationRequestValidator requestValidator;
        private readonly IB2CAvailabilityResultMappingService availabilityResultMappingService;
        private readonly IB2CAvailabilityResultCaching availabilityResultCaching;
        private readonly IB2CGuidService guidService;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public B2CValuationService(IB2CRequestStatusService requestStatusService, ILogger logger, IB2CValuationRequestFactory requestFactory
            , IValuationService coreValuationService, IChannelInfoService channelInfoService, IB2CValuationResultMappingService resultsMappingService
            , IB2CValuationRequestValidator requestValidator, IB2CValuationLogger valuationLogger, IAccommodationConfigurationManager configurationManager, IB2CAvailabilityResultMappingService availabilityResultMappingService, IB2CAvailabilityResultCaching availabilityResultCaching, IB2CGuidService guidService, IEstablishmentDetailsRepository establishmentDetailsRepository)
        {
            this.requestStatusService = requestStatusService;
            this.logger = logger;
            this.requestFactory = requestFactory;
            this.coreValuationService = coreValuationService;
            this.channelInfoService = channelInfoService;
            this.resultsMappingService = resultsMappingService;
            this.requestValidator = requestValidator;
            this.valuationLogger = valuationLogger;
            this.configurationManager = configurationManager;
            this.availabilityResultMappingService = availabilityResultMappingService;
            this.availabilityResultCaching = availabilityResultCaching;
            this.guidService = guidService;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<Guid> StartValuationAsync(AccommodationB2CValuationRequest valuationRequest)
        {
            // create a valuation id if the caller didn't provide one
            if (valuationRequest.ValuationId == null) valuationRequest.ValuationId = Guid.NewGuid();

            // Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateValuationRequestStatus(valuationRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            // start the valuation process
#pragma warning disable 4014
            Task.Run(async () => await this.StartValuationProcessAsync(valuationRequest, requestStatus));
#pragma warning restore 4014
            return valuationRequest.ValuationId.Value;
        }

        public async Task<AccommodationB2CValuationResponse> GetValuationResponseAsync(Guid valuationId)
        {
            AccommodationB2CValuationRequest valuationRequest = null;
            AccommodationB2CRequestStatus requestStatus = null;
            try
            {
                // get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(valuationId);

                // throw exception when request status is not available
                if (requestStatus == null)
                {
                    new AccommodationB2CValuationResponse() { ValuationId = valuationId, ValuationStatus = Status.NotFound };
                }

                // if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful)
                {
                    return new AccommodationB2CValuationResponse() { ValuationId = valuationId, ValuationStatus = requestStatus.Status };
                }

                // get availability request
                var availabilityRequest = requestStatus.AvailabilityRequest;

                // get valuation request
                valuationRequest = requestStatus.ValuationRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Valuation, valuationId.ToString(), "(accommodation)", valuationRequest.Debugging, logger))
                {
                    // perform a valuartion
                    var coreValuationResponse = await coreValuationService.GetValuationResponseAsync(valuationId);

                    // map the core results
                    var valuationResult = await resultsMappingService.MapFromCoreValuationResultsAsync(valuationRequest, coreValuationResponse.Result);

                    return new AccommodationB2CValuationResponse
                    {
                        ValuationId = valuationId,
                        ValuationStatus = requestStatus.Status,
                        ValuationResult = valuationResult,
                        PromotionalCode = valuationRequest.PromotionalCode
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetValuationResponse Error: search [{0}] {1}\n\n{2}", valuationId, ex.GetDetailedMessageWithInnerExceptions(), valuationRequest.ToIndentedJson());
                return new AccommodationB2CValuationResponse { ValuationId = valuationId, ValuationStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        public async Task<AccommodationB2CValuationResponse> StartAndGetValuationResponseAsync(AccommodationB2CValuationRequest valuationRequest)
        {
            AccommodationB2CRequestStatus requestStatus = null;
            Guid valuationId = await StartValuationAsync(valuationRequest);
            try
            {
                int pollCount = 1;

                AccommodationB2CValuationResponse response = await GetValuationResponseAsync(valuationId);
                var availabilityStatus = response.ValuationStatus;
                while ((availabilityStatus == Status.InProgress || availabilityStatus == Status.NotStarted) && pollCount < 50)
                {
                    Thread.Sleep(2000);
                    response = await GetValuationResponseAsync(valuationId);
                    availabilityStatus = response.ValuationStatus;
                    pollCount++;
                }
                return response;
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetValuationResponse Error: search [{0}] {1}\n\n{2}", valuationId, ex.GetDetailedMessageWithInnerExceptions(), valuationRequest.ToIndentedJson());
                return new AccommodationB2CValuationResponse { ValuationId = valuationId, ValuationStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        private async Task StartValuationProcessAsync(AccommodationB2CValuationRequest valuationRequest, AccommodationB2CRequestStatus requestStatus)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Valuation, valuationRequest.ValuationId.ToString(), "(accommodation)", valuationRequest.Debugging, logger))
            {
                var searchStarted = DateTime.Now;
                AccommodationB2CAvailabilityRequest availabilityRequest = null;
                AccommodationValuationResponse coreValuationResponse = null;
                Exception exception = null;
                try
                {
                    // get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(valuationRequest.Channel);

                    // update Mongo with inprogress status
                    await requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Valuation Started");

                    // Validate Request
                    requestValidator.ValidateValuationRequest(valuationRequest);

                    // Get availability request from guid
                    var availabilityRequestStatus = await requestStatusService.GetRequestStatusOrNullByIdAsync(valuationRequest.AvailabilityId);
                    availabilityRequest = availabilityRequestStatus.AvailabilityRequest;

                    // set availability request in valuation request status
                    requestStatus.AvailabilityRequest = availabilityRequest;

                    var availabilityB2CResults = await availabilityResultCaching.GetByRoomIdsAsync(
                        availabilityRequestStatus.ResultsAvailabilityId ?? Guid.Empty,
                        availabilityRequestStatus.ResultsBaseDestinationId.Value,
                        valuationRequest.SelectedRoomIds,
                        guidService.IsMetaGuid(valuationRequest.AvailabilityId));

                    if (availabilityB2CResults != null && availabilityB2CResults[0] != null)
                        availabilityB2CResults[0].BaseDestinationId = availabilityRequestStatus.ResultsBaseDestinationId.Value;

                    var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(availabilityRequestStatus.ResultsBaseDestinationId.Value, availabilityRequest.Channel);
                    // fix this shit
                    var coreAvailabilityResults = availabilityResultMappingService.MapToCoreAvailabilityResult(availabilityRequest, availabilityB2CResults, establishmentDetails);

                    // create a core valuation request
                    var coreValuationRequest = requestFactory.CreateValuationRequest(valuationRequest, coreAvailabilityResults, channelInfo);

                    // perform a valuation search
                    coreValuationResponse = await coreValuationService.ProcessValuationAsync(coreValuationRequest, coreAvailabilityResults);

                    // check if valuation failed
                    if (coreValuationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        requestStatus.Status = Status.Successful;
                        requestStatus.Message = "Accommodation Valuation Completed";
                    }
                    else
                    {
                        requestStatus.Status = Status.Failed;
                        requestStatus.Message = "Accommodation Provider Valuation Failed";
                    }
                }
                catch (Exception ex)
                {
                    // update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation Valuation Error: search [{0}] {1}\n\n{2}", valuationRequest.ValuationId, ex.GetDetailedMessageWithInnerExceptions(), valuationRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                await valuationLogger.LogAsync(availabilityRequest, valuationRequest, requestStatus, coreValuationResponse, DateTime.Now - searchStarted, exception);
            }
        }
    }
}
