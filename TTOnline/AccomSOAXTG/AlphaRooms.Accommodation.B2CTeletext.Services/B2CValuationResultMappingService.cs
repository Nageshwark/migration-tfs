﻿namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;
    using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.ExtensionMethods;
    using AutoMapper;
    using Ninject.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class B2CValuationResultMappingService : IB2CValuationResultMappingService
    {
        static B2CValuationResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationValuationResult, AccommodationB2CValuationResult>();
                Mapper.CreateMap<AccommodationValuationResultRoom, AccommodationB2CValuationResultRoom>()
                    .ForMember(i => i.CancellationPolicy, i => i.Ignore());
                Mapper.CreateMap<AccommodationProviderValuationResult, AccommodationB2CValuationResultRoom>()
                    .ForMember(i => i.CancellationPolicy, i => i.Ignore());
                Mapper.CreateMap<AccommodationProviderValuationResultVccPaymentRule, AccommodationB2CValuationResultVccPaymentRule>();
            }
        }

        private readonly ILogger logger;
        private readonly IBoardRepository boardTypeRepository;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public B2CValuationResultMappingService(ILogger logger, IEstablishmentDetailsRepository establishmentDetailsRepository, IBoardRepository boardTypeRepository)
        {
            this.logger = logger;
            this.boardTypeRepository = boardTypeRepository;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<AccommodationB2CValuationResult> MapFromCoreValuationResultsAsync(AccommodationB2CValuationRequest valuationRequest, AccommodationValuationResult coreValuationResult)
        {
            var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreValuationResult.Rooms[0].DestinationId, valuationRequest.Channel);
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            AccommodationB2CValuationResult result = null;
            try
            {
                result = Mapper.Map<AccommodationValuationResult, AccommodationB2CValuationResult>(coreValuationResult);

                result.Rooms = coreValuationResult.Rooms
                    .AsParallel()
                    .Select(i => this.CreateAccommodationB2CValuationResultRoom(
                        i,
                        boardTypes,
                        establishmentDetails,
                        i.RoomId))
                    .ToArray();
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreValuationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreValuationResult.ToIndentedJson());
            }
            return result;
        }

        private AccommodationB2CValuationResultRoom CreateAccommodationB2CValuationResultRoom(
            AccommodationValuationResultRoom coreResult,
            Dictionary<BoardType, Board> boards,
            Dictionary<Guid, EstablishmentDetails> establishmentDetails,
            string roomId)
        {
            var establishmentDetail = establishmentDetails[coreResult.EstablishmentId];

            var room = Mapper.Map<AccommodationProviderValuationResult, AccommodationB2CValuationResultRoom>(coreResult.ProviderResult);

            room = Mapper.Map<AccommodationValuationResultRoom, AccommodationB2CValuationResultRoom>(coreResult, room);
            room.Price = coreResult.IsActiveAccommodationAdminFee ? new SOACommon.Contracts.Money(coreResult.Price.Amount - coreResult.AccommodationAdminFee.Amount,coreResult.Price.CurrencyCode) : coreResult.Price;
            room.RoomId = roomId;
            room.ProviderEdiCode = coreResult.ProviderResult.ProviderProviderCode ?? coreResult.Provider.EdiCode;
            room.ProviderName = coreResult.ProviderResult.ProviderName ?? coreResult.Provider.Name;          
            room.ProviderPrice = coreResult.CostPrice;
            room.ProviderAliasName = coreResult.SupplierAlias;
            room.ProviderRoomCode = coreResult.ProviderResult.RoomCode;
            room.DestinationName = establishmentDetail.DestinationName;
            room.EstablishmentName = establishmentDetail.EstablishmentName;
            room.ProviderEstablishmentCode = coreResult.ProviderResult.ProviderEstablishmentCode ?? coreResult.ProviderResult.EstablishmentEdiCode;
            room.BoardDescription = boards[room.BoardType].Name;
            room.ChildrenAndInfantsCount = (byte)(coreResult.ChildrenCount + coreResult.InfantsCount);
            room.IsCardChargeApplicable = (room.PaymentModel != PaymentModel.CustomerPayDirect);
            room.VccPaymentRules = coreResult.ProviderResult.VirtualCreditCardPaymentTerms.Select(i => CreateAccommodationB2CValuationResultRoomVccPaymentRules(i)).ToArray();

            if (!ArrayModule.IsNullOrEmpty(coreResult.CancellationPolicies))
            {
                room.CancellationPolicy = coreResult.CancellationPolicies.Select(i => "<p>" + i + "</p>").ToString("");
            }

            room.PriceHikeDetail = coreResult.PriceHikeDetail;
            room.Discount = coreResult.DiscountPrice;

            return room;
        }

        private AccommodationB2CValuationResultVccPaymentRule CreateAccommodationB2CValuationResultRoomVccPaymentRules(AccommodationProviderValuationResultVccPaymentRule paymentRule)
        {
            return Mapper.Map<AccommodationProviderValuationResultVccPaymentRule, AccommodationB2CValuationResultVccPaymentRule>(paymentRule);
        }
    }
}
