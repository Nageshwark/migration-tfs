﻿namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    using AlphaRooms.Accommodation.B2CTeletext.Contracts;
    using AlphaRooms.Accommodation.B2CTeletext.DomainModels;
    using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
    using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.ExtensionMethods;
    using Ninject.Extensions.Logging;
    using System;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.Interfaces;
    using AutoMapper;
    using System.Web.Script.Serialization;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    public class B2CAvailabilityLogger : IB2CAvailabilityLogger
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2CAvailabilityLogRepository repository;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;
        List<string> providersId = new List<string>();

        static B2CAvailabilityLogger()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();
            }
        }

        public B2CAvailabilityLogger(ILogger logger, IAccommodationConfigurationManager configurationManager, IB2CAvailabilityLogRepository repository, IPerformanceLoggerService<PerformanceLog> performanceLoggerService, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService)
        {
            this.logger = logger;
            this.repository = repository;
            this.performanceLoggerService = performanceLoggerService;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(
            AccommodationB2CAvailabilityRequest availabilityRequest,
            AccommodationB2CRequestStatus requestStatus,
            AccommodationProvider[] providers,
            AccommodationAvailabilityResponse coreResponse,
            TimeSpan? postProcessTimeTaken,
            TimeSpan timeTaken,
            Exception exception)
        {

            this.CreatePerformanceLogs(coreResponse, availabilityRequest, requestStatus, (int)timeTaken.TotalSeconds, exception);

            if (this.configurationManager.AccommodationB2CAvailabilityLoggerIsEnabled)
            {
                try
                {

                    var metaResponseLogs = (coreResponse != null && coreResponse.Results.Count > 0) ? coreResponse.Results.First().Provider.Id == 36 ? JsonConvert.SerializeObject(coreResponse, Newtonsoft.Json.Formatting.Indented) : null : null;
                    await this.repository.SaveAsync(new B2CAvailabilityLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag,
                        AvailabilityId = availabilityRequest.AvailabilityId.Value,
                        AvailabilityType = availabilityRequest.SearchType,
                        AvailabilityDate = requestStatus.StartDate,
                        Status = requestStatus.Status,
                        Request = availabilityRequest.XmlSerialize(),
                        PostProcessTimeTaken = (postProcessTimeTaken != null ? (int?)postProcessTimeTaken.Value.TotalMilliseconds : null),
                        TimeTaken = (int)timeTaken.TotalMilliseconds,
                        ResultsCount = (coreResponse != null ? (int?)coreResponse.Results.Count() : null),
                        EstablishmentResultsCount = (coreResponse != null ? (int?)coreResponse.Results.GroupBy(i => i.EstablishmentId).Count() : null),
                        Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null),
                        Providers = CreateProviderNames(providers, coreResponse, requestStatus),
                        ProviderSearchTypes = CreateProviderSearchTypes(providers, coreResponse),
                        ProviderTimeTaken = CreateProviderTimeTaken(providers, coreResponse),
                        ProviderResultsCount = CreateProviderResults(providers, coreResponse),
                        ProviderEstablishmentResultsCount = CreateProviderEstablishmentResults(providers, coreResponse),
                        ProviderExceptions = CreateProviderExceptions(providers, coreResponse),
                        
                    });
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log accommodation availability with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        private void CreatePerformanceLogs(AccommodationAvailabilityResponse coreResponse, AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CRequestStatus requestStatus, int timeTaken, Exception exception)
        {
            var perfLog = new PerformanceLog
            {
                OperationType = "Search",
                Channel = availabilityRequest.Channel,
                ProductType = SupplierTypes.Accommodation.ToString(),
                RequestIdentifier = availabilityRequest.AvailabilityId.ToString(),
                DestinationIdentifier = availabilityRequest.DestinationId.ToString(),
                TotalTime = timeTaken,
                IsSuccess = requestStatus.Status == Status.Successful,
                ResultCount = coreResponse != null ? coreResponse.Results.Count : 0,
                HostName = this.configurationManager.AccommodationHostNameTag,
                CacheHit = coreResponse == null && exception == null ? "T" : "F",
            };

            if (coreResponse != null && coreResponse.ProcessResponseDetails != null)
            {
                foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
                {
                    var providerPerfLogs =
                       coreResponseDetail.Providers.Select(x =>
                       {
                           var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                           m.Provider = x.EdiCode;
                           m.Arrival = availabilityRequest.CheckInDate;
                           m.Departure = availabilityRequest.CheckOutDate;
                           m.TotalTime = coreResponseDetail.TimeTaken != null ? (int)coreResponseDetail.TimeTaken.Value.TotalSeconds : 0;
                           m.ResultCount = coreResponseDetail.ResultsCount ?? 0;
                           m.IsSuccess = coreResponseDetail.Exception == null;
                           m.CacheHit = coreResponseDetail.IsLiveResults ? "F" : "T";
                           return m;
                       });

                    foreach (var log in providerPerfLogs)
                    {
                        providerPerformanceLoggerService.Report(log);
                    }
                }
            }

            performanceLoggerService.Report(perfLog);
        }

        private string CreateProviderNames(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse, AccommodationB2CRequestStatus requestStatus)
        {
            if (requestStatus.Status == Status.Successful && providers == null && coreResponse == null)
            {
                return "B2CCache";
            }

            if (coreResponse == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    if (!builder.ToString().Contains(provider.Id.ToString()))
                    {
                        builder.Append(provider.Id.ToString());
                        builder.Append(";");
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }

        private string CreateProviderSearchTypes(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null) return null;
            StringBuilder builder = new StringBuilder();
            providersId.Clear();
            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    if (!providersId.Contains(provider.Id.ToString()))
                    {
                        providersId.Add(provider.Id.ToString());
                        builder.Append(coreResponseDetail.IsLiveResults ? "L" : "C");
                        builder.Append(";");
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }

        private string CreateProviderResults(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            providersId.Clear();

            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    if (!providersId.Contains(provider.Id.ToString()))
                    {
                        providersId.Add(provider.Id.ToString());
                        builder.Append(coreResponseDetail.ResultsCount != null ? coreResponseDetail.ResultsCount.ToString() : "NULL");
                        builder.Append(";");
                    }
                    if (provider.Id == 5)
                    {
                        var supplierResults = coreResponse.Results.Where(i => i.Provider == provider).GroupBy(i => i.ProviderResult.SupplierEdiCode).ToArray();
                        if (supplierResults.Any())
                        {
                            builder.Append("(");

                            foreach (var j in supplierResults)
                            {
                                builder.Append(j.Key.Replace("(", "").Replace(")", ""));
                                builder.Append("=");
                                builder.Append(j.Count());
                                builder.Append(",");
                            }

                            builder.Length -= 1;
                            builder.Append(")");
                        }
                        builder.Append(";");
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }

        private string CreateProviderEstablishmentResults(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            providersId.Clear();

            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    var providerResults = coreResponse.Results.Where(i => i.Provider == provider).ToArray();

                    if (!providersId.Contains(provider.Id.ToString()))
                    {
                        providersId.Add(provider.Id.ToString());
                        builder.Append(coreResponseDetail.ResultsCount != null ? providerResults.GroupBy(i => i.EstablishmentId).Count().ToString() : "NULL");
                        builder.Append(";");
                    }

                    if (provider.Id == 5)
                    {
                        var supplierResults = providerResults.GroupBy(i => i.ProviderResult.SupplierEdiCode).ToArray();
                        if (supplierResults.Any())
                        {
                            builder.Append("(");
                            foreach (var j in supplierResults)
                            {
                                builder.Append(j.Key.Replace("(", "").Replace(")", ""));
                                builder.Append("=");
                                builder.Append(j.GroupBy(k => k.EstablishmentId).Count());
                                builder.Append(",");
                            }
                            builder.Length -= 1;
                            builder.Append(")");
                            builder.Append(";");
                        }
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }

        private string CreateProviderTimeTaken(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            providersId.Clear();

            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    if (!providersId.Contains(provider.Id.ToString()))
                    {
                        providersId.Add(provider.Id.ToString());
                        builder.Append(coreResponseDetail.TimeTaken != null ? ((int)coreResponseDetail.TimeTaken.Value.TotalMilliseconds).ToString() : "NULL");
                        builder.Append(";");
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }

        private string CreateProviderExceptions(AccommodationProvider[] providers, AccommodationAvailabilityResponse coreResponse)
        {
            if (coreResponse == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            providersId.Clear();

            foreach (var coreResponseDetail in coreResponse.ProcessResponseDetails)
            {
                foreach (var provider in coreResponseDetail.Providers)
                {
                    if (!providersId.Contains(provider.Id.ToString()))
                    {
                        providersId.Add(provider.Id.ToString());
                        builder.Append(coreResponseDetail.Exception != null ? coreResponseDetail.Exception.GetDetailedMessageWithInnerExceptions() : "NULL");
                        builder.Append(";");
                    }
                }
            }

            if (builder.Length > 0)
            {
                builder.Length -= 1;
            }

            return builder.ToString();
        }
    }
}