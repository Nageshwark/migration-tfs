﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CAvailabilityRequestFactory : IB2CAvailabilityRequestFactory
    {
        static B2CAvailabilityRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CAvailabilityRequest, AccommodationAvailabilityRequest>()
                    .ForMember(i => i.Rooms, i => i.Ignore());
                Mapper.CreateMap<AccommodationB2CAvailabilityRequestRoom, AccommodationAvailabilityRequestRoom>()
                    .ForMember(i => i.Guests, i => i.Ignore());
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;

        public B2CAvailabilityRequestFactory(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public AccommodationAvailabilityRequest CreateAvailabilityRequest(AccommodationB2CAvailabilityRequest availabilityRequest, ChannelInfo channelInfo, AccommodationProvider[] providers)
        {
            var request = Mapper.Map<AccommodationB2CAvailabilityRequest, AccommodationAvailabilityRequest>(availabilityRequest);
            request.ProviderCacheEnabled = configurationManager.AccommodationB2CEnableProviderCache;
            request.ChannelInfo = channelInfo;
            request.Providers = providers;
            request.Rooms = availabilityRequest.Rooms.Select(i => CreateAvailabilityRequestRoom(i)).ToArray();
            request.Voucher = availabilityRequest.PromotionalCode;
            request.MetaToken = availabilityRequest.MetaToken;
            return request;
        }

        private AccommodationAvailabilityRequestRoom CreateAvailabilityRequestRoom(AccommodationB2CAvailabilityRequestRoom room)
        {
            var requestRoom = Mapper.Map<AccommodationB2CAvailabilityRequestRoom, AccommodationAvailabilityRequestRoom>(room);
            requestRoom.Guests = Enumerable.Repeat(CreateAvailabilityRequestRoomGuest(configurationManager.AccommodationAdultAge), room.Adults)
                .Concat(room.ChildAges.Select(i => CreateAvailabilityRequestRoomGuest(i))).ToArray();
            return requestRoom;
        }

        private AccommodationAvailabilityRequestRoomGuest CreateAvailabilityRequestRoomGuest(byte age)
        {
            return new AccommodationAvailabilityRequestRoomGuest() { Age = age };
        }
    }
}
