﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.Services
{
    public class B2CAvailabilityProviderIdentifierService : IB2CAvailabilityProviderIdentifierService
    {
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2CAvailabilityProviderIdentifierService(IAccommodationConfigurationManager configurationManager,  IAccommodationProviderRepository accommodationProviderRepository)
        {
            this.configurationManager = configurationManager;
            this.accommodationProviderRepository = accommodationProviderRepository;
        }

        public async Task<AccommodationProvider[]> GetActiveAvailabilityProvidersAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            if (!ArrayModule.IsNullOrEmpty(availabilityRequest.ProvidersToSearch))
			{
				return await this.accommodationProviderRepository.GetByIdsAsync(availabilityRequest.ProvidersToSearch);
			}
			else if (availabilityRequest.TC4Origin)
			{
				return new AccommodationProvider[] { };
			}
			else if (!ArrayModule.IsNullOrEmpty(configurationManager.AccommodationProvidersToSearch))
			{
				return await this.accommodationProviderRepository.GetByIdsAsync(configurationManager.AccommodationProvidersToSearch);
			}
            return await this.accommodationProviderRepository.GetActiveAsync();
        }

        public async Task<AccommodationProvider[]> GetMetaActiveAvailabilityProvidersAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            //Travelgate handler
            if (!ArrayModule.IsNullOrEmpty(configurationManager.TravelGateProvider))
            {
                return await this.accommodationProviderRepository.GetByIdsAsync(configurationManager.TravelGateProvider);
            }

            var providers = await this.accommodationProviderRepository.GetActiveAsync();

            List<AccommodationProvider> providersToSearch = new List<AccommodationProvider>();
            providers.ToList().ForEach( i=>
            {
               i.Parameters.ForEach(j =>
               {
                   if(j.ParameterName.ToLower() == "providersptosearchformetas" && j.ParameterValue.ToLower() == "true")
                   {
                       providersToSearch.Add(i);
                   }
               });
            });

            return providersToSearch.ToArray();
        }
    }
}
