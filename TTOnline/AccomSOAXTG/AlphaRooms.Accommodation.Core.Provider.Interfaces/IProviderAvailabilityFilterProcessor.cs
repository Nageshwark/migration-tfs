﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using System.Threading.Tasks;

    public interface IProviderAvailabilityFilterProcessor
    {
        Task InitializeAsync();
        bool CanProcess(AccommodationProviderAvailabilityRequest providerAvailabilityRequest, AccommodationProviderAvailabilityResult result);
    }
}
