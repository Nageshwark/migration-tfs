﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderLoggerService
    {
        void SetProviderRequest(AccommodationProviderValuationRequest request, object providerRequest);
        void SetProviderResponse(AccommodationProviderValuationRequest request, object providerResponse);
        void SetProviderRequest(AccommodationProviderValuationRequest request, string key, object providerRequest);
        void SetProviderResponse(AccommodationProviderValuationRequest request, string key, object providerResponse);

        void SetProviderRequest(AccommodationProviderBookingRequest request, object providerRequest);
        void SetProviderResponse(AccommodationProviderBookingRequest request, object providerResponse);
        void SetProviderRequest(AccommodationProviderBookingRequest request, string key, object providerRequest);
        void SetProviderResponse(AccommodationProviderBookingRequest request, string key, object providerResponse);

        void SetProviderRequest(AccommodationProviderCancellationRequest request, object bev5Request);
        void SetProviderResponse(AccommodationProviderCancellationRequest request, object results);
        void SetProviderRequest(AccommodationProviderCancellationRequest request, string key, object providerRequest);
        void SetProviderResponse(AccommodationProviderCancellationRequest request, string key, object providerResponse);
    }
}
