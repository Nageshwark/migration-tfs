﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationMetaAvailabilityProvider
    {
        Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetMetaProviderAvailabilityAsync(AccommodationAvailabilityRequest request);
    }
}
