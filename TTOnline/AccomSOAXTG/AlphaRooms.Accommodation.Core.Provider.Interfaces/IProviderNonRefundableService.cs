﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderNonRefundableService
    {
		bool IsDescriptionNonRefundable(string roomDescription);
        bool IsDescriptionNonRefundable(string roomDescription, bool isNonRefundable);
        bool IsDescriptionNonRefundable(string roomDescription, string boardDescription, bool isNonRefundable);
    }
}
