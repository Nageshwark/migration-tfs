﻿using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationMetaAvailabilityAutomatorAsync<T>
    {
         Task<T> GetMetaAvailabilityResponseAsync(AccommodationAvailabilityRequest request); 
    }
}
