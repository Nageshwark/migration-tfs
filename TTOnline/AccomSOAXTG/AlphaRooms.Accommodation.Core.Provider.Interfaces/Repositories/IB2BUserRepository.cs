﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IB2BUserRepository
    {
        Task<B2BUser[]> GetAllAsync();
        Task<B2BUser> GetByUserNameAsync(string name);
        Task<B2BUser> GetByIdAsync(int agentId);
    }
}
