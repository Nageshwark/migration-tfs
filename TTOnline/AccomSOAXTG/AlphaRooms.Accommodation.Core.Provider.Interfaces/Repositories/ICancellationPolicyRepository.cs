﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface ICancellationPolicyRepository
    {
        Task<CancellationPolicy> GetByProviderIdAndProviderPolicyIdAsync(int? providerId, byte providerPolicyId, bool isB2B);
        Task<CancellationPolicy> GetDefaultCancellationPolicyAsync(bool isB2B);
        Task<CancellationPolicy> GetNonRefundableCancellationPolicyAsync(bool isB2B);
    }
}
