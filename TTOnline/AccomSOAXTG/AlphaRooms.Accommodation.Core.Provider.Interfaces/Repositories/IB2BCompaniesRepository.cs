﻿using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IB2BCompaniesRepository
    {
        Task<B2BCompany[]> GetAllAsync();
        Task<B2BCompany> GetByIdAsync(int Id);
        Task<B2BCompany[]> GetByParentIdAsync(int Id);
    }
}