﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface ISupplierExcludedOfDepositRepository
    { 
        Task<string[]> GetDepositExcludedListOFProvidersAsync();
        bool CheckIfSupplierExistsInExcludedList(string[] excludedSupplierList, string ediCode);
    }
}
