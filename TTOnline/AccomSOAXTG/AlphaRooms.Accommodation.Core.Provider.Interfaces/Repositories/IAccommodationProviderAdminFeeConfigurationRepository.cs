﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAccommodationProviderAdminFeeConfigurationRepository
    {
        Task<Dictionary<int, AccommodationProviderAdminFeeConfiguration>> GetAllDictionaryAsync();
    }
}
