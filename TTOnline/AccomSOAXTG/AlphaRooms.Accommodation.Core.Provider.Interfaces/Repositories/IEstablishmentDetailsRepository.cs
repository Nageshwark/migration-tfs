﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IEstablishmentDetailsRepository
    {
        Task<Dictionary<Guid, EstablishmentDetails>> GetByDestinationIdDictionaryGroupEstablishmentIdAsync(Guid destinationId, Channel channel);
    }
}
