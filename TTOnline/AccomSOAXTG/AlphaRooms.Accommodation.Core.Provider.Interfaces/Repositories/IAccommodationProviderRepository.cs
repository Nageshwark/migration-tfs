﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IAccommodationProviderRepository
    {
        Task<AccommodationProvider[]> GetAllAsync();
        Task<Dictionary<int, AccommodationProvider>> GetAllDictionaryGroupByIdAsync();
        Task<AccommodationProvider[]> GetActiveAsync();
        Task<AccommodationProvider[]> GetByIdsAsync(int[] ids);
        Task<AccommodationProvider> GetByIdAsync(int providerId);
    }
}
