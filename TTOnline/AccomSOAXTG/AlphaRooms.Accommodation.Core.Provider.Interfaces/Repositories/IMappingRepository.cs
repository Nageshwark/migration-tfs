﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IMappingRepository
    {
        Task<Dictionary<string, ProviderMapping>> GetByBaseDestinationIdAsync(Guid baseDestinationId);
        Task<Dictionary<string, ProviderMapping>> GetByDestinationIdAsync(Guid baseDestinationId, Guid destinationId);
        Task<Dictionary<string, ProviderMapping>> GetByEstablishmentIdAsync(Guid baseDestinationId, Guid destinationId, Guid EstablishmentId);
    }
}
