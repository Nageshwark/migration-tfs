﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IBlockedDestinationRepository
    {
        Task<BlockedDestination[]> GetAllAsync();
        Task<BlockedDestination[]> GetByChannelAndDestinationIdHierarchicalAsync(Channel channel, Guid destinationId);
    }
}
