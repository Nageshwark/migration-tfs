﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IMarkupRepository
    {
        Task<Markup[]> GetActiveFilteredHierarchicalAsync(Channel channel, int? agentId);
    }
}
