﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRoomDescriptionRemoveWordRepository
    {
        // this is a Dictionary not a hashset. a Dictionary is guaranteed to support multiple readers concurrently, HashSet is not.
        Task<IReadOnlyDictionary<string, int>> GetAllAsync();
    }
}
