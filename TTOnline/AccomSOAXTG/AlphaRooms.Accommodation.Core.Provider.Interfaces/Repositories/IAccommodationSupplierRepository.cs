﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAccommodationSupplierRepository
    {
        Task<Dictionary<int, AccommodationSupplier>> GetAllDictionaryGroupByIdAsync();
        Task<Dictionary<int, Dictionary<string, AccommodationSupplier>>> GetAllDictionaryGroupByProviderIdGroupByEdiCodeAsync();
        Task<AccommodationSupplier[]> GetByIdsAsync(int[] ids);
        Task<AccommodationSupplier> GetByNameAsync(string name);
        Task<AccommodationSupplier> GetByEdiCodeAsync(string name);
        Task<AccommodationSupplier[]> GetByProviderCodeAsync(int providerCode);
    }
}
