﻿using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IAccommodationFlashSaleRepository
    {
        Task<AccommodationB2BFlashSaleEstablishment[]> GetAllAsync();
    }
}