﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface IStarRatingRepository
    {
        Task<Dictionary<StarRatingType, StarRating>> GetAllDictionaryAsync();
    }
}
