﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IVirtualCreditCardRuleRepository
    {
        Task<VirtualCreditCardRule> GetProviderIdAndSupplierCodeAsync(int providerId, string supplierEdiCode);
        Task<IEnumerable<VirtualCreditCardRule>> GetBySupplierIdAsync(int supplierId);
        Task<VirtualCreditCardRule> GetDefaultRuleAsync();
    }
}
