﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRoomDescriptionReplacementRepository
    {
        /// <summary>
        /// Gets all alias from the database in correct order, order is important
        /// </summary>
        /// <returns>all Alias in the order of the of priority</returns>
        Task<IReadOnlyList<Alias>> GetAllAliasAsync();
    }
}
