﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories
{
    public interface ISupplierAliasRepository
    {
        Task<SupplierAlias> GetByIdAsync(int ediCode);
        Task<SupplierAlias[]> GetByIdsAsync(int[] ediCodeS);

        Task<SupplierAlias[]> GetAllAsync();
        Task<Dictionary<int, SupplierAlias>> GetAllDictionaryAsync();
    }
}
