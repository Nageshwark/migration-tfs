﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderBookingFactory
    {
        IAccommodationBookingProvider CreateBookingProvider(string providerName);
    }
}
