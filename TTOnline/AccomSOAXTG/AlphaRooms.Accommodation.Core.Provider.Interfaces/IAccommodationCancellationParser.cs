﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationCancellationParser<T>
    {
        IEnumerable<AccommodationProviderCancellationResult> GetCancellationResults(AccommodationProviderCancellationRequest request, T response);
    }
}