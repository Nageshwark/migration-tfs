﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationValuationParser<T>
    {
        IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, T response);
    }
}