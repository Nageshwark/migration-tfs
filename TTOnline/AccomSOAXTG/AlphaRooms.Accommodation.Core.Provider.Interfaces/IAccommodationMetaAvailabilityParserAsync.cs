﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationMetaAvailabilityParserAsync<T>
    {
        IEnumerable<AccommodationProviderAvailabilityResult> GetMetaAvailabilityResults(AccommodationAvailabilityRequest request, T response);
    }
}
