﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums
{
    public enum ProviderOutputFormat
    {
        Xml,
        Json,
        Html,
    }
}