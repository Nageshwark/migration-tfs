﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationBookingAutomatorAsync<T>
    {
        Task<T> GetBookingResponseAsync(AccommodationProviderBookingRequest request);
    }
}
