﻿namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IProviderAvailabilityFilterService
    {
        Task FilterAvailabilityAsync(List<AccommodationProviderAvailabilityResult> availabilityResults, AccommodationProviderAvailabilityRequest availabilityRequest);
    }
}
