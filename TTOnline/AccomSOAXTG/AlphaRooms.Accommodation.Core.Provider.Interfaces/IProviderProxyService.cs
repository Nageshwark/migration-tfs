﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Net;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderProxyService
    {
        void SetProxy<TChannel>(ClientBase<TChannel> client, List<AccommodationProviderParameter> providerParameters) where TChannel : class;
        WebProxy SetProxy(List<AccommodationProviderParameter> providerParameters);
    }
}
