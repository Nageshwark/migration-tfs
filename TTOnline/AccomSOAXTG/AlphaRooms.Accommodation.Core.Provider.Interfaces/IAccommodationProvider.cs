﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IAccommodationProvider
    {
        /// <summary>
        /// Accommodation supplier information
        /// </summary>
        AccommodationProvider Supplier { get; set; }
    }
}
