﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderOutputLogger
    {
        void LogAvailabilityResponse(AccommodationProviderAvailabilityRequest request, ProviderOutputFormat format, object result);
        void LogAvailabilityResponse(AccommodationProviderAvailabilityRequest request, string processName, ProviderOutputFormat format, object result);
        void LogValuationResponse(AccommodationProviderValuationRequest request, ProviderOutputFormat format, object result);
        void LogValuationResponse(AccommodationProviderValuationRequest request, string processName, ProviderOutputFormat format, object result);
        void LogBookingResponse(AccommodationProviderBookingRequest request, ProviderOutputFormat format, object result);
        void LogBookingResponse(AccommodationProviderBookingRequest request, string processName, ProviderOutputFormat format, object result);
        void LogCancellationResponse(AccommodationProviderCancellationRequest request, ProviderOutputFormat format, object result);
        void LogCancellationResponse(AccommodationProviderCancellationRequest request, string processName, ProviderOutputFormat format, object result);
    }
}
