﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Interfaces
{
    public interface IProviderValuationCancellationPolicyService
    {
        Task<string> CreateProviderCancellationPolicyAsync(AccommodationProviderValuationRequest request, byte providerPolicyId);

        Task<string> CreateProviderCancellationPolicyAsync(AccommodationProviderValuationRequest request, byte providerPolicyId, params object[] objects);
    }
}
