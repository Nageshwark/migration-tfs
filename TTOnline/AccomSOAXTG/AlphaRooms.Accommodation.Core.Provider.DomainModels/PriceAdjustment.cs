﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class PriceAdjustment
    {
        //public int Id { get; set; }
        public Guid EstablishmentId { get; set; }
        public string CheapestSupplierCode { get; set; }
        public string Period { get; set; }
        public string SearchDayType { get; set; }
        public double? MinMarkup { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? CurrentMarkup { get; set; }
        public double? ChangeToApply { get; set; }
        public int? HasTR { get; set; }
        //public DateTime LogDate { get; set; }
    }
}

