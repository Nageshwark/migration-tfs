﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class RoomDescriptionMapping
    {
        public string RoomDescription { get; set; }
        public string ProviderRoomDescription { get; set; }
    }
}