﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class EstablishmentFacility
    {
        public Guid EstablishmentId { get; set; }
        public Guid FacilityId { get; set; }
    }
}
