﻿using AlphaRooms.Accommodation.Core.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationSupplier
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public string Name { get; set; }
        public string EdiCode { get; set; }
        public bool IsSupplierCancellationPolicy { get; set; }
        public bool IsUniqueContractRequired { get; set; }

    }
}
