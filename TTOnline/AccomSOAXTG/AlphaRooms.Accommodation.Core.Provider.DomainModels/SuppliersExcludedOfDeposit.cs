﻿using System;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class SuppliersExcludedOfDeposit
    {
        public int? AccommodationProviderID { get; set; }
        public int? AccommodationSupplierID { get; set; }
        public int PaymentModelID { get; set; }
        public int IsExcludedDeposit { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
