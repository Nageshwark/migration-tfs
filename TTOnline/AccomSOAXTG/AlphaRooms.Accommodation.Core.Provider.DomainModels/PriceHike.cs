﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class PriceHike
    {
        public int Id { get; set; }
        public string ClientIpAddress { get; set; }
        public decimal HikePercent { get; set; }
    }
}
