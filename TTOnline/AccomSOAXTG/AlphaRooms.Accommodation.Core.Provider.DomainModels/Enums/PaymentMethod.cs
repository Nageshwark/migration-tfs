﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum PaymentMethod
    {
        CreditCard = 0
        , DebitCard = 1
    }
}
