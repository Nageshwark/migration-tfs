﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum MarkupApplyType : byte
    {
        Value = 0
        , Adjustment = 1
        , Override = 2
    }
}
