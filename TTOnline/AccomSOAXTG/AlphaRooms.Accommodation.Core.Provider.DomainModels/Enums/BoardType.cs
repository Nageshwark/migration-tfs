﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum BoardType
    {
        RoomOnly = 1,

        SelfCatering = 2,

        Breakfast = 3,

        HalfBoard = 4,

        FullBoard = 5,

        AllInclusive = 6
    }
}
