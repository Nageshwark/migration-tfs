﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum ContractDependencyMode : byte
    {
        None = 0
        , Dependent = 1
        , UniqueContracts = 2
    }
}
