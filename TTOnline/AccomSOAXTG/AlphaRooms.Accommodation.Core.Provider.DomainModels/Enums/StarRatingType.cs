﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum StarRatingType
    {
        /// <summary>
        /// Unrated Hotels Star Rating.
        /// </summary>
        Unrated = 0,

        /// <summary>
        /// One Star.
        /// </summary>
        OneStar = 1,

        /// <summary>
        /// Two Stars.
        /// </summary>
        TwoStar = 2,

        /// <summary>
        /// Three Stars.
        /// </summary>
        ThreeStar = 3,

        /// <summary>
        /// Four Stars.
        /// </summary>
        FourStar = 4,

        /// <summary>
        /// Five Stars.
        /// </summary>
        FiveStar = 5,
    }
}
