﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum PaymentTypeType
    {
        /// <summary>
        /// Payment models that need paying straight away.
        /// </summary>
        PayNow = 1,

        /// <summary>
        /// Payment models that need paying to the hotel on arrival.
        /// </summary>
        PayAtHotel = 2
    }
}
