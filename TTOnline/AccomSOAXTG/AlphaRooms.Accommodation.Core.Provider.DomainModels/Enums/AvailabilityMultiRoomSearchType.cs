﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums
{
    public enum AvailabilityMultiRoomSearchType : byte
    {
        Normal = 0
        , OnlySameRoomType = 1
        , OnlyDifferentRoomType = 2
    }
}
