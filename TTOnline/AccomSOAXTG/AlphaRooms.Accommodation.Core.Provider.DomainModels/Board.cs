﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class Board
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
    }
}
