﻿using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationProvider
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string EdiCode { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public TimeSpan SearchTimeout { get; set; }
        public TimeSpan CacheExpireTimeout { get; set; }
        public TimeSpan? SessionExpireTimeout { get; set; }
        public byte MaxInfantAge { get; set; }
        public byte MaxChildAge { get; set; }
        public bool IsBookingEnabled { get; set; }
        public bool IsCoreCacheActive { get; set; }
        public bool IsSearchAgeSpecific { get; set; }
        public bool IsMappingEnabled { get; set; }
        public ContractDependencyMode ContractDependencyMode { get; set; }
        public AvailabilityMultiRoomSearchType MultiRoomSearchType { get; set; }
        public int? MaxRoomSearch { get; set; }
        public int? MaxDestinationSearch { get; set; }
        public int? MaxEstablishmentSearch { get; set; }
        public AvailabilitySearchMode PreferredSearchMode { get; set; }
        public int? MaxRoomValuation { get; set; }
        public int? MaxRoomBooking { get; set; }
        public int MinNonPayDirectDays { get; set; }
        public virtual List<AccommodationProviderParameter> Parameters { get; set; }
    }
}
