﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class CancellationPolicy
    {
        public int Id { get; set; }
        public bool? IsB2B { get; set; }
        public int? ProviderId { get; set; }
        public byte ProviderPolicyId { get; set; }
        public string Message { get; set; }
    }
}
