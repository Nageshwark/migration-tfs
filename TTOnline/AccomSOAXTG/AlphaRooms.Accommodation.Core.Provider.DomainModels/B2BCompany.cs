﻿using System.Collections.Generic;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class B2BCompany
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
        public int TypeId { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Fax { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string CountryIsoCode { get; set; }
        public decimal? Commission { get; set; }
        public string ABTANumber { get; set; }
        public string VATNumber { get; set; }
        public string NavisionId { get; set; }
        public bool IsSelfBilling { get; set; }
        public int? ParentCompanyId { get; set; }
        public B2BCompany ParentCompany { get; set; }
        public ICollection<B2BUser> B2BUsers { get; set; }
        public ICollection<B2BUser> B2BBillingUsers { get; set; }

    }
}