﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class SupplierAlias
    {
        public int Id { get; set; }
        public string EdiCode { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
    }
}
