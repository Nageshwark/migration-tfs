﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    [BsonIgnoreExtraElements]
    public class Facility
    {
        public const string ReferenceFieldName = "Ref";
        public const string NameFieldName = "Nam";
        public const string IsShortListedFieldName = "StL";

        [BsonId]
        public Guid Id { get; set; }
        
        [BsonElement(ReferenceFieldName)]
        public int Reference { get; set; }
        
        [BsonElement(NameFieldName)]
        public string Name { get; set; }

        [BsonElement(IsShortListedFieldName)]
        public bool IsShortListed { get; set; }
    }
}
