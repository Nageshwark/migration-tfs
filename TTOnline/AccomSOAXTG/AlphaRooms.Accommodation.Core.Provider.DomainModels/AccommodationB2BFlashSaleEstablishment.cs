﻿using System;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationB2BFlashSaleEstablishment
    {
        public int B2BAgentId { get; set; }
        public Guid EstablishmentId { get; set; }
        public bool IsActive { get; set; }
    }
}