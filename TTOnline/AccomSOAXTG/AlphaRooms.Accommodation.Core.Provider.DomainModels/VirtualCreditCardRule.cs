﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class VirtualCreditCardRule
    {
        public int Id { get; set; }
        public int? SupplierId { get; set; }
        public AccommodationSupplier Supplier { get; set; }
        public string RoomCode { get; set; }
        public decimal? AmountOnBooking { get; set; }
        public int? DaysBeforeCheckIn { get; set; }
        public decimal? AmountOnDaysBeforeCheckIn { get; set; }
        public int? DaysAfterCheckOut { get; set; }
        public decimal? AmountOnDaysAfterCheckOut { get; set; }
    }
}
