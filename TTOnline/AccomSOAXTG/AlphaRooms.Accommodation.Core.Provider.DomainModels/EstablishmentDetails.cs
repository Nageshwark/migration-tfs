﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class EstablishmentDetails
    {
        public string CountrySeoFriendlyName { get; set; }
        public Guid? ParentDestinationId { get; set; }
        public Guid DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string DestinationSeoFriendlyName { get; set; }
        public Guid EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }        
        public string EstablishmentSeoFriendlyName { get; set; }
        public string EstablishmentDescription { get; set; }
        public string EstablishmentType { get; set; }
        public string EstablishmentAddressLine1 { get; set; }
        public string EstablishmentAddressLine2 { get; set; }
        public string EstablishmentAddressCity { get; set; }
        public string EstablishmentAddressPostCode { get; set; }
        public string EstablishmentTopImageUrl { get; set; }
        public int EstablishmentReviewCount { get; set; }
        public int EstablishmentReviewAverageScore { get; set; }
        public decimal? EstablishmentTripAdvisorAverageScore { get; set; }
        public int? EstablishmentTripAdvisorReviewCount { get; set; }
        public decimal? EstablishmentTripAdvisorAverageRating { get; set; }
        public StarRatingType EstablishmentStarRating { get; set; }
        public decimal? EstablishmentLongitude { get; set; }
        public decimal? EstablishmentLatitude { get; set; }
        public int DestinationReference { get; set; }
        public bool IsDestinationDistrict { get; set; }
        public int? EstablishmentChainId { get; set; }
        public int EstablishmentPopularity { get; set; }
        public Guid[] FacilityIds { get; set; }
        public int[] TravellerTypeIds { get; set; }
        public string CountryISOCode { get; set; }
    }
}
