﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class DestinationSearchMode
    {
        public int Id { get; set; }

        public Guid DestinationId { get; set; }

        public int? ProviderId { get; set; }

        public AvailabilitySearchMode PreferredSearchMode { get; set; }

        public bool IsEnforcedSearchMode { get; set; }
    }
}
