﻿namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class EdiProviderDestinationEstablishmentMapping
    {
		public Guid ResortDestinationId { get; set; }
        public Guid DestinationId { get; set; }
        public string ProviderEdiCode { get; set; }
        public string ProviderDestinationCode { get; set; }
        public Guid EstablishmentId { get; set; }
        public string ProviderEstablishmentCode { get; set; }
    }

}