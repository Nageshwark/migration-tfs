﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationProviderAdminFeeConfiguration
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int SupplierId { get; set; }
        public int PaymentModelId { get; set; }
        public bool IsActiveAdminFee { get; set; }
        public decimal AdminFee { get; set; }
    }
}
