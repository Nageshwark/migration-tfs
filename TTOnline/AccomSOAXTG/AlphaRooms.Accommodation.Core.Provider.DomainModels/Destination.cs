﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class Destination
    {
        public Guid? ParentDestinationId { get; set; }
        public Guid DestinationId { get; set; }
        public Guid? EstablishmentId { get; set; }
        public int? EstablishmentIntID { get; set; }
    }
}
