﻿namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    using AlphaRooms.SOACommon.DomainModels.Enumerators;

    public class B2BCustomer
    {
        public int Id { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public int Site { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public int? UserId { get; set; }

        public Channel? Channel { get; set; }

        public int LanguageId { get; set; }

        public int? B2BUserId { get; set; }

        public virtual B2BUser B2BUser { get; set; }
    }
}