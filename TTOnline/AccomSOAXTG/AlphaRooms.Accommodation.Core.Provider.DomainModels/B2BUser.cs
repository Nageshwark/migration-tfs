﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class B2BUser
    {
        public int Id { get; set; }
        public bool IsEnabled { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SecurityStamp { get; set; }
        public int TypeId { get; set; }
        public decimal? Commission { get; set; }
        public string LoginIdent { get; set; }
        public int CompanyId { get; set; }
        public B2BCompany Company { get; set; }
        public int? BillToCompanyId { get; set; }
        public B2BCompany BillToCompany { get; set; }
        public virtual ICollection<AccommodationAgentConfiguration> AgentConfiguration { get; set; }
        public ICollection<AccommodationProvider> Providers { get; set; }
        public ICollection<AccommodationSupplier> Suppliers { get; set; }
        public virtual ICollection<B2BCustomer> Customers { get; set; }
    }
}
