﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationAgentConfiguration
    {
        public int Id { get; set; }
        public int AgentId { get; set; }
        public int ProviderId { get; set; }
        public int? SupplierId { get; set; }
      //  public virtual AccommodationProvider Provider { get; set; }
    //    public virtual AccommodationSupplier Supplier { get; set; }
        public bool GrossRatesEnabled { get; set; }
    }
}
