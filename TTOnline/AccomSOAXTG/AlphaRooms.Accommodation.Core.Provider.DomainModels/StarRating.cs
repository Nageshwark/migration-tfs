﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    [BsonIgnoreExtraElements]
    public class StarRating
    {
        public const string NameFieldName = "Nam";

        [BsonId]
        public int Id { get; set; }

        [BsonElement(NameFieldName)]
        public string Name { get; set; }
    }
}
