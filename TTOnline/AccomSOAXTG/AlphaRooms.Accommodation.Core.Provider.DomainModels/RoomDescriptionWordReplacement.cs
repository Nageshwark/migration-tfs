﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class RoomDescriptionWordReplacement
    {
        public string Word { get; set; }
        public string Replacement { get; set; }
        public int Order { get; set; }
    }
}
