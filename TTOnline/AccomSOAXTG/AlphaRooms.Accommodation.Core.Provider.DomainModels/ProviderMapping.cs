﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class ProviderMapping
    {
        public string[] DestinationCodes { get; set; }
        public Dictionary<Guid, EstablishmentMapping[]> EstablishmentMappings { get; set; }
    }
}
