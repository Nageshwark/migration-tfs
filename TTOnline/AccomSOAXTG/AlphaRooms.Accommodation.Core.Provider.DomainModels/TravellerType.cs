﻿namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    using MongoDB.Bson.Serialization.Attributes;

    [BsonIgnoreExtraElements]
    public class TravellerType
    {
        public const string NameFieldName = "Nam";

        [BsonId]
        public int Id { get; set; }

        [BsonElement(NameFieldName)]
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Id.ToString() + ", " + this.Name;
        }
    }
}
