﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationAgentProvider
    {
        public int Id { get; set; }
        public int AgentId { get; set; }
        public int ProviderId { get; set; }        
        public AccommodationProvider Provider { get; set; }
    }
}
