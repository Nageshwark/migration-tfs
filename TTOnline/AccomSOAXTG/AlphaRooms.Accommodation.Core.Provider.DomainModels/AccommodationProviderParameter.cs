﻿using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationProviderParameter : IProviderParameter
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }

    public static class AccommodationProviderParameterExtensions
    {
        public static string GetParameterValue(this List<AccommodationProviderParameter> value, string parameterName)
        {
            var parameter = value.FirstOrDefault(i => i.ParameterName == parameterName);
            if (parameter == null) throw new Exception("Parameter " + parameterName + " not found.");
            return parameter.ParameterValue;
        }
    }
}
