﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class VolumeDiscount
    {
        public int Id { get; set; }
        public decimal Volume { get; set; }
        public decimal Discount { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime UpdateDate { get; set; } 
    }
}
