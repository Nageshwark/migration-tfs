﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class DestinationMapping
    {
        public Guid DestinationId { get; set; }
        public string ProviderEdiCode { get ;set; }
        public string ProviderDestinationCode { get; set; }
    }
}
