﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class AccommodationProviderEmailTemplate
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int EmailTemplateTypeId { get; set; }
        public string Body { get; set; }
        public string RoomBody { get; set; }
    }
}
