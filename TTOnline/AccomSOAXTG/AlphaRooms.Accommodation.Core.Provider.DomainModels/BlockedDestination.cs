﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class BlockedDestination
    {
        public int Id { get; set; }

        public Channel? Channel { get; set; }

        public int? ProviderId { get; set; }

        public int? SupplierId { get; set; }

        public Guid DestinationId { get; set; }
    }
}
