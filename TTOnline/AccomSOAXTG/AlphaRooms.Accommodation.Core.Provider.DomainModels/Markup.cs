﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class Markup
    {
        public int Id { get ; set; }
        public bool IsActive { get; set; }
        public Channel? Channel { get; set; }
        public int? ProviderId { get; set; }
        public int? SupplierId { get; set; }
        public int? ChainId { get; set; }
        public string CountryCode { get; set; }
        public Guid? ParentDestinationID { get; set; }
        public Guid? DestinationId { get; set; }
        public Guid? EstablishmentId { get; set; }
        public StarRatingType? StarRating { get; set; }
        public int? RoomTypeId { get; set; }
        public BoardType? BoardType { get; set; }
        public int? BookingMinDuration { get; set; }
        public int? BookingMaxDuration { get; set; }
        public DateTime? BookingFromDate { get; set; }
        public DateTime? BookingToDate { get; set; }
        public DateTime? TravelFromDate { get; set; }
        public DateTime? TravelToDate { get; set; }
        public bool IsB2B { get; set; }
        public int? B2BAgentId { get; set; }
        public int Order { get; set; }
        public MarkupApplyType MarkupApplyType { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
    }
}
