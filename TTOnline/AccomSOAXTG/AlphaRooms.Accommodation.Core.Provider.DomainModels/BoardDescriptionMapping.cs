﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class BoardDescriptionMapping
    {
        public string ProviderBoardDescription { get; set; }
        public string BoardDescription { get; set; }
        public BoardType? BoardType { get; set; }
    }
}
