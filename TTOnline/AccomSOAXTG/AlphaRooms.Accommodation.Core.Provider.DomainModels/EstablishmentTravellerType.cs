﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class EstablishmentTravellerType
    {
        public Guid EstablishmentId { get; set; }
        public int TravellerTypeId { get; set; }
    }
}
