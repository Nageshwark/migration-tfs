﻿namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    using System;

    /// <summary>
    /// Represents an word/phase that will be replaced to create a duplication key.
    /// </summary>
    [Serializable]
    public class Alias
    {
        private readonly string toReplace;
        private readonly string replaceWith;

        public Alias(string toReplace, string replaceWith)
        {
            if (string.IsNullOrEmpty(toReplace))
            {
                throw new ArgumentNullException("Can not be null or empty", "toReplace");
            }

            if (replaceWith == null)
            {
                throw new ArgumentNullException("replaceWith");
            }

            this.toReplace = toReplace;
            this.replaceWith = replaceWith;
        }


        public string ToReplace
        {
            get
            {
                return this.toReplace;
            }
        }

        public string ReplaceWith
        {
            get
            {
                return this.replaceWith;
            }
        }
    }
}
