﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class ProviderCommission
    {
        public int Id { get; set; }
        public string ProviderEdiCode { get; set; }
        public string SupplierEdiCode { get; set; }
        public DateTime? FromArrivalDate { get; set; }
        public DateTime? ToArrivalDate { get; set; }
        public DateTime? FromBookingDate { get; set; }
        public DateTime? ToBookingDate { get; set; }
        public bool IncludeInHMD { get; set; }
        public decimal Commission { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
