﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class Mapping
    {
        public Dictionary<Guid, Dictionary<string, ProviderMapping>> Destinations { get; set; }
    }
}
