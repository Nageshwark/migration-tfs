﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.DomainModels
{
    public class EstablishmentMapping
    {
        public Guid DestinationId { get; set; }        
        public Guid EstablishmentId { get; set; }
		public Guid ResortDestinationId { get; set; }
        public string ProviderEdiCode { get; set; }
        public string ProviderEstablishmentCode { get; set; }
    }
}
