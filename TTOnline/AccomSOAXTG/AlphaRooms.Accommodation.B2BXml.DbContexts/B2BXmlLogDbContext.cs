﻿using AlphaRooms.Accommodation.B2BXml.DbContexts.Mapping;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.DbContexts
{
    public class B2BXmlLogDbContext : DbContext
    {
        static B2BXmlLogDbContext()
        {
            Database.SetInitializer<B2BXmlLogDbContext>(null);
        }

        public B2BXmlLogDbContext() : base("Name=AlphabedsLogsEntities")
        {
        }

        public DbSet<B2BXmlValuationLog> B2BXmlValuationLogs { get; set; }
        public DbSet<B2BXmlBookingLog> B2BXmlBookingLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new B2BXmlValuationLogMap());
            modelBuilder.Configurations.Add(new B2BXmlBookingLogMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task B2BXmlAvailabilityLogInsertAsync(string hostName, Guid searchId, DateTime searchDate, byte searchType, byte? status, string request, int? resultsCount
            , int? establishmentResultsCount, string providers, string providerSearchTypes, string providerResultsCount, string providerEstablishmentResultsCount, string providerTimeTaken
            , string providerExceptions, int? postProcessTimeTaken, int? timeTaken, string exception)
        {
            if (hostName == null) throw new ArgumentNullException("hostName");
            await this.ExecuteStoredProcedureAsync("AccommodationAvailabilityLogsInsert"
                , new SqlParameter("hostName", DataModule.DbNullIfNull(hostName))
                , new SqlParameter("searchId", DataModule.DbNullIfNull(searchId))
                , new SqlParameter("StartDate", DataModule.DbNullIfNull(searchDate))
                , new SqlParameter("searchType", DataModule.DbNullIfNull(searchType))
                , new SqlParameter("status", DataModule.DbNullIfNull(status))
                , new SqlParameter("request", DataModule.DbNullIfNull(request))
                , new SqlParameter("providers", DataModule.DbNullIfNull(providers))
                , new SqlParameter("providerSearchTypes", DataModule.DbNullIfNull(providerSearchTypes))
                , new SqlParameter("providerResultsCount", DataModule.DbNullIfNull(providerResultsCount))
                , new SqlParameter("providerEstablishmentResultsCount", DataModule.DbNullIfNull(providerEstablishmentResultsCount))
                , new SqlParameter("providerTimeTaken", DataModule.DbNullIfNull(providerTimeTaken))
                , new SqlParameter("providerExceptions", DataModule.DbNullIfNull(providerExceptions))
                , new SqlParameter("resultsCount", DataModule.DbNullIfNull(resultsCount))
                , new SqlParameter("establishmentResultsCount", DataModule.DbNullIfNull(establishmentResultsCount))
                , new SqlParameter("postProcessTimeTaken", DataModule.DbNullIfNull(postProcessTimeTaken))
                , new SqlParameter("timeTaken", DataModule.DbNullIfNull(timeTaken))
                , new SqlParameter("exception", DataModule.DbNullIfNull(exception))
                );
        }
    }
}
