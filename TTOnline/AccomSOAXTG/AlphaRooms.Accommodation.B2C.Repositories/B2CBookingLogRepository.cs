﻿using AlphaRooms.Accommodation.B2C.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.DomainModels;
using AlphaRooms.Accommodation.B2C.DbContexts;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.B2C.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.B2C.Repositories
{
    public class B2CBookingLogRepository : IB2CBookingLogRepository
    {
        private readonly IDbContextActivator<B2CLogDbContext> dbLogsContextActivator;

        public B2CBookingLogRepository(IDbContextActivator<B2CLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2CBookingLog[] b2CBookingLog)
        {
            using (B2CLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2CBookingLogs.AddRange(b2CBookingLog);
                await context.SaveChangesAsync();
            }
        }
    }
}
