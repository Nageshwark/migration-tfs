﻿using AlphaRooms.Accommodation.B2C.DbContexts;
using AlphaRooms.Accommodation.B2C.DomainModels;
using AlphaRooms.Accommodation.B2C.Interfaces.Repositories;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Repositories
{
    public class B2CAvailabilityLogRepository : IB2CAvailabilityLogRepository
    {
        private readonly IDbContextActivator<B2CLogDbContext> dbLogsContextActivator;

        public B2CAvailabilityLogRepository(IDbContextActivator<B2CLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveAsync(B2CAvailabilityLog b2CAvailabilityLog)
        {
            using (B2CLogDbContext context = this.dbLogsContextActivator.GetDbContext())
            {
                await context.B2CAvailabilityLogInsertAsync(
                    b2CAvailabilityLog.HostName, b2CAvailabilityLog.AvailabilityId, b2CAvailabilityLog.AvailabilityDate
                    , (byte)b2CAvailabilityLog.AvailabilityType, (byte?)b2CAvailabilityLog.Status, b2CAvailabilityLog.Request, b2CAvailabilityLog.ResultsCount
                    , b2CAvailabilityLog.EstablishmentResultsCount, b2CAvailabilityLog.Providers, b2CAvailabilityLog.ProviderSearchTypes, b2CAvailabilityLog.ProviderResultsCount
                    , b2CAvailabilityLog.ProviderEstablishmentResultsCount, b2CAvailabilityLog.ProviderTimeTaken, b2CAvailabilityLog.ProviderExceptions, b2CAvailabilityLog.PostProcessTimeTaken
                    , b2CAvailabilityLog.TimeTaken, b2CAvailabilityLog.Exceptions);
            }
        }
    }
}
