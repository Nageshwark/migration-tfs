﻿using AlphaRooms.Accommodation.B2BXml.Interfaces;
using AlphaRooms.Accommodation.B2BXml.WcfService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.WcfInterface
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class AccommodationB2BXmlServices : IAccommodationB2BXmlAvailabilityService, IAccommodationB2BXmlValuationService, IAccommodationB2BXmlBookingService, IAccommodationB2BXmlCancellationService
    {
        private readonly IB2BXmlAvailabilityService availabilityService;
        private readonly IB2BXmlValuationService valuationService;
        private readonly IB2BXmlBookingService bookingService;
        private readonly IB2BXmlCancellationService cancellationService;

        public AccommodationB2BXmlServices(IB2BXmlAvailabilityService availabilityService, IB2BXmlValuationService valuationService, IB2BXmlBookingService bookingService
            , IB2BXmlCancellationService cancellationService)
        {
            this.availabilityService = availabilityService;
            this.valuationService = valuationService;
            this.bookingService = bookingService;
            this.cancellationService = cancellationService;

            SetSecurityProtocolType();
        }

        private void SetSecurityProtocolType()
        {
            ServicePointManager.SecurityProtocol = 0;
            foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
            {
                switch (protocol)
                {
                    case SecurityProtocolType.Ssl3:
                    case SecurityProtocolType.Tls:
                    case SecurityProtocolType.Tls11:
                        break;
                    case SecurityProtocolType.Tls12:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol |= protocol;
                        break;
                }
            }
        }

        public async Task<string> GetAvailabilityResponseAsync(string request)
        {
            return await this.availabilityService.GetAvailabilityResponseAsync(request);
        }

        public async Task<string> GetValuationResponseAsync(string request)
        {
            return await this.valuationService.GetValuationResponseAsync(request);
        }

        public async Task<string> GetBookingResponseAsync(string request)
        {
            return await this.bookingService.GetBookingResponseAsync(request);
        }

        public async Task<string> GetCancellationResponseAsync(string request)
        {
            return await this.cancellationService.GetCancellationResponseAsync(request);
        }
    }
}
