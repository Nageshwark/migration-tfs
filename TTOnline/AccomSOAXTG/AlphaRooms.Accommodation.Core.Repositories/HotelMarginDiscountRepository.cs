﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.Accommodation.Core.Repositories
{
    public class HotelMarginDiscountRepository: IHotelMarginDiscountRepository
    {
        private readonly IDbContextActivator<MarkupServiceDbContext> markupDbContextActivator;
        private readonly ICacheProviderAsync<HotelMarginDiscount> cache;

        public HotelMarginDiscountRepository(IDbContextActivator<MarkupServiceDbContext> accommodationDbContextActivator, ICacheProviderAsync<HotelMarginDiscount> cache)
        {
            this.markupDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<IEnumerable<HotelMarginDiscount>> GetHotelMarginDiscounts(Guid destinationId, int channel,
            string ediProvider)
        {
            return await this.cache.GetOrSetAsync("Hmd" + destinationId.ToString() + channel + ediProvider,
                async() =>
                    (await
                        this.markupDbContextActivator.GetDbContext()
                            .GetHotelMarginDiscounts(destinationId, channel, ediProvider)));

        }
    }
}
