﻿

namespace AlphaRooms.Accommodation.Core.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.DbContexts;
    using AlphaRooms.Utilities.EntityFramework;
    using AlphaRooms.Utilities;
   public class DepositSchemeV1Repository : IDepositSchemeV1Repository
    {
        private readonly IDbContextActivator<DepositSchemeV1DbContext> depositSchemeContextActivator;

        public DepositSchemeV1Repository(IDbContextActivator<DepositSchemeV1DbContext> depositSchemeContextActivator){
            this.depositSchemeContextActivator = depositSchemeContextActivator;
        }
        public async Task<bool> AddDepositSchemeAsync(DepositSchemeV1 depositScheme)
        {
            this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.Add(depositScheme);

            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteDepositSchemeAsync(int id)
        {
            var depositSchemeToRemove = await this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.FirstOrDefaultAsync(x => x.Id == id);

            if (depositSchemeToRemove == null)
                return false;

            this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.Remove(depositSchemeToRemove);

            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }

        public DepositSchemeV1 GetCurrentDepositScheme()
        {
            Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, bool>> where = d => d.IsEnabled && d.StartDate <= DateTime.Today;
            Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, object>> orderBy = d => d.StartDate;

            var details = this.GetDepositSchemes(where, orderBy, true, null, 1);

            return details.Results.FirstOrDefault();
        }

        public async Task<DepositSchemeV1> GetCurrentDepositSchemeAsync()
        {
            Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, bool>> where = d => d.IsEnabled && d.StartDate <= DateTime.Today;
            Expression<Func<IDepositSchemeV1Queryable<DepositSchemeV1>, object>> orderBy = d => d.StartDate;

            var details = await this.GetDepositSchemesAsync(where, orderBy, true, null, 1);

            return details.Results.FirstOrDefault();
        }

        public Task<DepositSchemeV1> GetDepositScheme(int id)
        {
            var depositScheme = this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.FirstOrDefaultAsync(x => x.Id == id);

            return depositScheme;
        }
        public async Task<FilteredRepositoryResponse<DepositSchemeV1>> GetDepositSchemesAsync<TQueryable>(Expression<Func<TQueryable, bool>> whereClause, Expression<Func<TQueryable, object>> orderBy, bool? sortDescending, int? skipCount, int? takeCount) where TQueryable : IDepositSchemeV1Queryable<DepositSchemeV1>
        {
            var query = this.GetDepositSchemesQueryable(whereClause, orderBy, sortDescending, skipCount, takeCount);

            var schemes = await query.ToListAsync();

            return new FilteredRepositoryResponse<DepositSchemeV1>(schemes, schemes.Count);
        }

        public async Task<bool> UpdateDepositSchemeAsync(DepositSchemeV1 depositScheme)
        {
            var depositSchemeToUpdate = await this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.FirstOrDefaultAsync(x => x.Id == depositScheme.Id);

            if (depositSchemeToUpdate == null)
                return false;

            depositSchemeToUpdate.IsEnabled = depositScheme.IsEnabled;
            depositSchemeToUpdate.DepositMinHotelSpend = depositScheme.DepositMinHotelSpend;
            depositSchemeToUpdate.IsDepositAdminFeeApplicable = depositScheme.IsDepositAdminFeeApplicable;
            depositSchemeToUpdate.AdminFee = depositScheme.AdminFee;
            depositSchemeToUpdate.IsDepositTopUpPaymentEnabled = depositScheme.IsDepositTopUpPaymentEnabled;
            depositSchemeToUpdate.DepositValue = depositScheme.DepositValue;
            depositSchemeToUpdate.StartDate = depositScheme.StartDate;
            depositSchemeToUpdate.DepositTopUpDueDaysAfterBooking = depositScheme.DepositTopUpDueDaysAfterBooking;
            depositSchemeToUpdate.IsDepositTopUpPercentage = depositScheme.IsDepositTopUpPercentage;
            depositSchemeToUpdate.IsDepositTopUpAdminFee = depositScheme.IsDepositTopUpAdminFee;
            depositSchemeToUpdate.StartDate = depositScheme.StartDate;
            depositSchemeToUpdate.BalancePaymentsMinAmt = depositScheme.BalancePaymentsMinAmt;
            depositSchemeToUpdate.MaxInterimPayments = depositScheme.MaxInterimPayments;
            depositSchemeToUpdate.InterimPaymentsSchedule = depositScheme.InterimPaymentsSchedule;
            depositSchemeToUpdate.DaysBtwInterimPayments = depositScheme.DaysBtwInterimPayments;
            depositSchemeToUpdate.IsInterimAdminFeeApplicable = depositScheme.IsInterimAdminFeeApplicable;
            depositSchemeToUpdate.FinalBalanceDueDays = depositScheme.FinalBalanceDueDays;
            depositSchemeToUpdate.DepositTopUPValue = depositScheme.DepositTopUPValue;
            depositSchemeToUpdate.IsInterimPaymentsApplicable = depositScheme.IsInterimPaymentsApplicable;
            depositSchemeToUpdate.createdDate = depositScheme.createdDate;
            depositSchemeToUpdate.IsInterimPercentage = depositScheme.IsInterimPercentage;


            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }

        public FilteredRepositoryResponse<DepositSchemeV1> GetDepositSchemes<TQueryable>(
          Expression<Func<TQueryable, bool>> whereClause,
          Expression<Func<TQueryable, object>> orderBy,
          bool? sortDescending,
          int? skipCount,
          int? takeCount)
          where TQueryable : IDepositSchemeV1Queryable<DepositSchemeV1>
        {
            var schemes = this.GetDepositSchemesQueryable(whereClause, orderBy, sortDescending, skipCount, takeCount).ToList();

            return new FilteredRepositoryResponse<DepositSchemeV1>(schemes, schemes.Count);
        }
        private IQueryable<DepositSchemeV1> GetDepositSchemesQueryable<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeV1Queryable<DepositSchemeV1>
        {
            var query = this.depositSchemeContextActivator.GetDbContext().DepositSchemesV1.AsQueryable();

            if (whereClause != null)
            {
                query = query.Where((Expression<Func<DepositSchemeV1, bool>>)whereClause.ConvertParameters<TQueryable, DepositSchemeV1>());
            }

            if (orderBy != null)
            {
                var rewrittenOrderBy = orderBy.ConvertParameters<TQueryable, DepositSchemeV1>().RemoveUnaryExpressions(ExpressionType.Convert);
                query = sortDescending.GetValueOrDefault() ? query.OrderByDescending(rewrittenOrderBy) : query.OrderBy(rewrittenOrderBy);
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }

            if (skipCount.HasValue)
            {
                query = query.Skip(skipCount.Value);
            }

            if (takeCount.HasValue)
            {
                query = query.Take(takeCount.Value);
            }

            return query;
        }
    }
}
