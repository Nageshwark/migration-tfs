﻿namespace AlphaRooms.Accommodation.Core.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
    using AlphaRooms.Accommodation.Core.DomainModels;
    using AlphaRooms.Accommodation.Core.DbContexts;
    using AlphaRooms.Utilities.EntityFramework;
    using AlphaRooms.Utilities;

    public class DepositSchemeRepository : IDepositSchemeRepository
    {
        private readonly IDbContextActivator<DepositSchemeDbContext> depositSchemeContextActivator;

        public DepositSchemeRepository(IDbContextActivator<DepositSchemeDbContext> depositSchemeContextActivator)
        { this.depositSchemeContextActivator = depositSchemeContextActivator; }


        public async Task<FilteredRepositoryResponse<DepositScheme>> GetDepositSchemesAsync<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeQueryable<DepositScheme>
        {
            var query = this.GetDepositSchemesQueryable(whereClause, orderBy, sortDescending, skipCount, takeCount);

            var schemes = await query.ToListAsync();

            return new FilteredRepositoryResponse<DepositScheme>(schemes, schemes.Count);
        }

        public async Task<DepositScheme> GetCurrentDepositSchemeAsync()
        {
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, bool>> where = d => d.IsEnabled && d.StartDate <= DateTime.Today;
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, object>> orderBy = d => d.StartDate;

            var details = await this.GetDepositSchemesAsync(where, orderBy, true, null, 1);

            return details.Results.FirstOrDefault();
        }

        public Task<DepositScheme> GetDepositScheme(int id)
        {
            var depositScheme = this.depositSchemeContextActivator.GetDbContext().DepositSchemes.FirstOrDefaultAsync(x => x.Id == id);

            return depositScheme;
        }

        public async Task<bool> AddDepositSchemeAsync(DepositScheme depositScheme)
        {
            this.depositSchemeContextActivator.GetDbContext().DepositSchemes.Add(depositScheme);

            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }

        public async Task<bool> UpdateDepositSchemeAsync(DepositScheme depositScheme)
        {
            var depositSchemeToUpdate = await this.depositSchemeContextActivator.GetDbContext().DepositSchemes.FirstOrDefaultAsync(x => x.Id == depositScheme.Id);

            if (depositSchemeToUpdate == null)
                return false;

            depositSchemeToUpdate.IsEnabled = depositScheme.IsEnabled;
            depositSchemeToUpdate.DepositMinSpend = depositScheme.DepositMinSpend;
            depositSchemeToUpdate.IsPercentage = depositScheme.IsPercentage;
            depositSchemeToUpdate.DepositMinWeeks = depositScheme.DepositMinWeeks;
            depositSchemeToUpdate.DepositMinHotelSpend = depositScheme.DepositMinHotelSpend;
            depositSchemeToUpdate.DepositDefaultValue = depositScheme.DepositDefaultValue;
            depositSchemeToUpdate.BalanceDueWeeksBefore = depositScheme.BalanceDueWeeksBefore;
            depositSchemeToUpdate.BalancePaymentMinSpend = depositScheme.BalancePaymentMinSpend;
            depositSchemeToUpdate.StartDate = depositScheme.StartDate;
            depositSchemeToUpdate.DepositDueWeeksAfter = depositScheme.DepositDueWeeksAfter;
            depositSchemeToUpdate.DepositDueIsPercentage = depositScheme.DepositDueIsPercentage;
            depositSchemeToUpdate.DepositDueMinSpend = depositScheme.DepositDueMinSpend;


            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteDepositSchemeAsync(int id)
        {
            var depositSchemeToRemove = await this.depositSchemeContextActivator.GetDbContext().DepositSchemes.FirstOrDefaultAsync(x => x.Id == id);

            if (depositSchemeToRemove == null)
                return false;

            this.depositSchemeContextActivator.GetDbContext().DepositSchemes.Remove(depositSchemeToRemove);

            return await this.depositSchemeContextActivator.GetDbContext().SaveChangesAsync() > 0;
        }


        public FilteredRepositoryResponse<DepositScheme> GetDepositSchemes<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeQueryable<DepositScheme>
        {
            var schemes = this.GetDepositSchemesQueryable(whereClause, orderBy, sortDescending, skipCount, takeCount).ToList();

            return new FilteredRepositoryResponse<DepositScheme>(schemes, schemes.Count);
        }

        public DepositScheme GetCurrentDepositScheme()
        {
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, bool>> where = d => d.IsEnabled && d.StartDate <= DateTime.Today;
            Expression<Func<IDepositSchemeQueryable<DepositScheme>, object>> orderBy = d => d.StartDate;

            var details = this.GetDepositSchemes(where, orderBy, true, null, 1);

            return details.Results.FirstOrDefault();
        }


        private IQueryable<DepositScheme> GetDepositSchemesQueryable<TQueryable>(
            Expression<Func<TQueryable, bool>> whereClause,
            Expression<Func<TQueryable, object>> orderBy,
            bool? sortDescending,
            int? skipCount,
            int? takeCount)
            where TQueryable : IDepositSchemeQueryable<DepositScheme>
        {
            var query = this.depositSchemeContextActivator.GetDbContext().DepositSchemes.AsQueryable();

            if (whereClause != null)
            {
                query = query.Where((Expression<Func<DepositScheme, bool>>)whereClause.ConvertParameters<TQueryable, DepositScheme>());
            }

            if (orderBy != null)
            {
                var rewrittenOrderBy = orderBy.ConvertParameters<TQueryable, DepositScheme>().RemoveUnaryExpressions(ExpressionType.Convert);
                query = sortDescending.GetValueOrDefault() ? query.OrderByDescending(rewrittenOrderBy) : query.OrderBy(rewrittenOrderBy);
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }

            if (skipCount.HasValue)
            {
                query = query.Skip(skipCount.Value);
            }

            if (takeCount.HasValue)
            {
                query = query.Take(takeCount.Value);
            }

            return query;
        }
    }
}