﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System.Data.Entity;

namespace AlphaRooms.Accommodation.Core.Repositories
{
    public class PriceHikeRepository : IPriceHikeRepository
    {
        private readonly IDbContextActivator<PriceHikeDbContext> DbContextActivator;
        private readonly ICacheProviderAsync<PriceHikeThreshold> cache;

        public PriceHikeRepository(IDbContextActivator<PriceHikeDbContext> dbContextActivator, ICacheProviderAsync<PriceHikeThreshold> cache)
        {
            this.DbContextActivator = dbContextActivator;
            this.cache = cache;
        }

        public async Task<List<PriceHikeThreshold>> GetPriceHikeThresholds()
        {
            return await this.cache.GetOrSetAsync("PriceHikeThresholds", async () => (await this.DbContextActivator.GetDbContext().PriceHikeThreshold.ToListAsync()));
        }
    }
}
