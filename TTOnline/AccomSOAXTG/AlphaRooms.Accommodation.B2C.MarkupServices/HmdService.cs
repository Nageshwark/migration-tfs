﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Core.Interfaces;

namespace AlphaRooms.Accommodation.B2C.MarkupServices
{
    public class HmdService : IAvailabilityResultMarkupService, IValuationResultMarkupService
    {
        private ILogger logger;
        private IHotelMarginDiscountRepository hotelMarginDiscountRepository;

        public HmdService(ILogger logger, IHotelMarginDiscountRepository hotelMarginDiscountRepository)
        {
            this.logger = logger;
            this.hotelMarginDiscountRepository = hotelMarginDiscountRepository;
        }

        public async Task<IList<AccommodationAvailabilityResult>> ApplyAvailabilityMarkupAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            //Calculate the volume discounts

            //Get Hmd by callling stored proc
            var providers = results.Select(x => x.Provider.EdiCode).Distinct();
            var destinationId = availabilityRequest.DestinationId.Value;
            int channel = (int)availabilityRequest.ChannelInfo.Channel;
            ConcurrentDictionary<string,IEnumerable<HotelMarginDiscount>> hotelMarginDiscounts=new ConcurrentDictionary<string, IEnumerable<HotelMarginDiscount>>();
            //await Async.ParallelForEach(providers, async(provider) =>
            //{
            //    var hmd= await hotelMarginDiscountRepository.GetHotelMarginDiscounts(destinationId, channel, provider);
            //    hotelMarginDiscounts.TryAdd(provider, hmd);
            //});
            foreach (var provider in providers)
            {
                var hmd = (await hotelMarginDiscountRepository.GetHotelMarginDiscounts(destinationId, channel, provider)).ToList();
                hotelMarginDiscounts.TryAdd(provider, hmd);
            }
            //Sync.ParallelForEachIgnoreFailed(results, result =>
            //{
            //    IEnumerable<HotelMarginDiscount> providerHmds;
            //    hotelMarginDiscounts.TryGetValue(result.ProviderEdiCode, out providerHmds);
            //    var matchedHmds = GetHmdForEstablishment(result.EstablishmentId.ToString(),  providerHmds);
            //    if (matchedHmds.Any())
            //    {
            //        result.Price.Amount = result.Price.Amount*matchedHmds.FirstOrDefault().dclDiscount;
            //    }
            //});
            foreach (var result in results)
            {
                IEnumerable<HotelMarginDiscount> providerHmds;
                hotelMarginDiscounts.TryGetValue(result.Provider.EdiCode, out providerHmds);
                var matchedHmds = GetHmdForEstablishment(result.EstablishmentId.ToString(), providerHmds);
                if (matchedHmds.Any())
                {
                    decimal markup = 0;
                    foreach (var hmd in matchedHmds)
                    {
                        if (hmd.blnCommissionable) markup += CalculatHotelMarginDiscount(markup, availabilityRequest.CheckInDate, availabilityRequest.CheckOutDate, hmd);
                        else markup += CalculatHotelMarginDiscount(markup, availabilityRequest.CheckInDate, availabilityRequest.CheckOutDate, hmd);
                    }
                    result.Price.Amount += markup;
                }
            }
            return results;
        }

        private IEnumerable<HotelMarginDiscount> GetHmdForEstablishment(string establishmentId,  IEnumerable<HotelMarginDiscount> providerHmds)
        {
            return providerHmds.Where(x => x.strEdiCode == establishmentId);
        }

        private decimal CalculatHotelMarginDiscount(decimal saleAmount, DateTime checkInDate, DateTime checkOutDate, HotelMarginDiscount hmd)
        {
            var duration = (checkOutDate - checkInDate).TotalDays;
            if (duration <= 1) duration = 1;
            int discountDays = GetDiscountDays(checkInDate, checkOutDate, hmd.dtmStart, hmd.dtmEnd);
            var finalDiscount = hmd.dclDiscount * (decimal)(discountDays / duration);
            return saleAmount * (finalDiscount / 100);
        }

        private int GetDiscountDays(DateTime checkInDate, DateTime checkOutDate, DateTime startDate, DateTime endDate)
        {
            DateTime maxStartDate = checkInDate > startDate ? checkInDate : startDate;
            DateTime minFinishDate = checkOutDate < endDate ? checkOutDate : endDate;
            return Math.Max(0, (int)(minFinishDate - maxStartDate).TotalDays );
        }

        public async Task<AccommodationValuationResult> ApplyValuationMarkupAsync(AccommodationValuationRequest valuationRequest, AccommodationValuationResult roomResults)
        {
            return roomResults;
        }
    }
}
