﻿using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2CTeletext.DomainModels
{
    public class AutoSuggest
    {
        public string Name { get; set; }

        public int Locale { get; set; }

        public int Relevance { get; set; }

        public int CategoryId { get; set; }

        public Guid? LocationId { get; set; }

        public Guid? DestinationId { get; set; }

        public AutoSuggestCategory Category
        {
            get
            {
                return (AutoSuggestCategory)this.CategoryId;
            }
            set
            {
                this.CategoryId = (int)value;
            }
        }
    }
}
