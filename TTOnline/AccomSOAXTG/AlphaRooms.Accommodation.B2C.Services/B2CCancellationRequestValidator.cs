﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CCancellationRequestValidator : IB2CCancellationRequestValidator
    {
        public void ValidateCancellationRequest(AccommodationB2CCancellationRequest cancellationRequest)
        {
            if (cancellationRequest.ItineraryId == Guid.Empty)
                throw new ValidationException("ValidateCancellationRequest failed: the request must have ItineraryId.");
        }
    }
}
