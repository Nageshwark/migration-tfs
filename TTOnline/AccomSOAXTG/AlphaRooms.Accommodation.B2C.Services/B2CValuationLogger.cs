﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.DomainModels;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.WebScraping;
using AlphaRooms.Utilities;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts.Enums;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CValuationLogger : IB2CValuationLogger
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2CValuationLogRepository repository;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2CValuationLogger(ILogger logger, IAccommodationConfigurationManager configurationManager, IB2CValuationLogRepository repository, 
                                  IPerformanceLoggerService<PerformanceLog> performanceLoggerService, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService)
        {
            this.logger = logger;
            this.repository = repository;
            this.performanceLoggerService = performanceLoggerService;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CValuationRequest valuationRequest
            , AccommodationB2CRequestStatus requestStatus, AccommodationValuationResponse coreResponse, TimeSpan timeTaken, Exception exception)
        {

            CreatePerformanceLogs(availabilityRequest, valuationRequest, coreResponse, requestStatus, timeTaken);

            if (!this.configurationManager.AccommodationB2CValuationLoggerIsEnabled) return;
            try
            {
                B2CValuationLog[] valuationLogs;
                if (coreResponse == null)
                {
                    valuationLogs = new[] { new B2CValuationLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        , ValuationId = valuationRequest.ValuationId.Value
                        , AvailabilityId = valuationRequest.AvailabilityId
                        , ValuationDate = requestStatus.StartDate 
                        , Status = requestStatus.Status
                        , TimeTaken = (int)timeTaken.TotalMilliseconds
                        , Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        , ProviderRoomNumbers = Enumerable.Range(1, valuationRequest.SelectedRoomIds.Length).ToString(',')
                    }};
                }
                else
                {
                    valuationLogs = coreResponse.ProcessResponseDetails.Select(i => new B2CValuationLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        , ValuationId = valuationRequest.ValuationId.Value
                        , AvailabilityId = valuationRequest.AvailabilityId
                        , ValuationDate = requestStatus.StartDate
                        , Status = requestStatus.Status
                        , TimeTaken = (int)timeTaken.TotalMilliseconds
                        , Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        , ProviderId = i.Provider.Id
                        , ProviderRoomNumbers = i.ValuationRequestRooms.Select(j => j.RoomNumber).ToString(',')
                        , RecoveredSelectedRooms = i.ValuationRequestRooms.Select(j => (j.IsSelectedRoomRecovered ? 'Y' : 'N')).ToString(',')
                        , ProviderRequests = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Request)
                        , ProviderResponses = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Response)
                        , ProviderStatus = (i.Exception != null ? Status.Failed : Status.Successful)
                        , ProviderTimeTaken = (i.TimeTaken != null ? (int?)i.TimeTaken.Value.TotalMilliseconds : null)
                        , ResultsCount = i.ResultsCount
                        , ProviderException = (i.Exception != null ? i.Exception.GetDetailedMessageWithInnerExceptions() : null)
                    }).ToArray();
                }
                await this.repository.SaveRangeAsync(valuationLogs);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to log accommodation availability with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
            }
        }

        private void CreatePerformanceLogs(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CValuationRequest valuationRequest, 
                                           AccommodationValuationResponse coreResponse, AccommodationB2CRequestStatus requestStatus, TimeSpan timeTaken)
        {
            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();

            var resultCount = coreResponse?.Result?.Rooms?.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Valuation",
                RequestIdentifier = valuationRequest.AvailabilityId.ToString(),
                DestinationIdentifier = availabilityRequest.DestinationId.ToString(),
                Channel = valuationRequest.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus.Status == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            var providerPerfLogs = coreResponse?.ProcessResponseDetails.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.Arrival = availabilityRequest.CheckInDate;
                m.Departure = availabilityRequest.CheckOutDate;
                m.IsSuccess = d.Exception == null;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }

        private string CreateProviderRequests(AccommodationProviderRequest[] providerRequests, ProviderRequestType type)
        {
            if (providerRequests == null) return null;
            StringBuilder builder = new StringBuilder();
            int nullCount = 0;
            foreach (var request in providerRequests.Where(i => i.Type == type))
            {
                if (request.Key != "default" && providerRequests.Length > 1)
                {
                    builder.Append(request.Key);
                    builder.Append(": ");
                }
                if (request.Object == null)
                {
                    builder.Append("<<NULL>>");
                    nullCount++;
                }
                else if (request.Object is string)
                {
                    builder.Append(request.Object);
                }
                else if (request.Object is WebScrapeResponse)
                {
                    builder.Append(((WebScrapeResponse)request.Object).Value);
                }
                else
                {
                    try
                    {
                        builder.Append(request.Object.XmlSerialize(request.Object.GetType()));
                    }
                    catch (Exception ex)
                    {
                        builder.Append("Unable to serialize: " + ex.GetDetailedMessageWithInnerExceptions());
                    }
                }
                builder.Append(";\r\n");
            }
            if (nullCount == providerRequests.Length) return null;
            if (builder.Length > 0) builder.Length -= 3;
            return builder.ToString();
        }
    }
}
