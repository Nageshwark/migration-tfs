﻿namespace AlphaRooms.Accommodation.B2C.Services
{
	using AlphaRooms.Accommodation.B2C.Contracts;
	using AlphaRooms.Accommodation.B2C.Interfaces;
	using AlphaRooms.Accommodation.B2C.Interfaces.Caching;
	using AlphaRooms.Accommodation.Core.Contracts;
	using AlphaRooms.Accommodation.Core.Interfaces;
	using AlphaRooms.Accommodation.Core.Provider.DomainModels;
	using AlphaRooms.SOACommon.DomainModels.Enumerators;
	using AlphaRooms.SOACommon.Interfaces;
	using AlphaRooms.SOACommon.Services;
	using AlphaRooms.Utilities;
	using AlphaRooms.Utilities.ExtensionMethods;
	using Ninject.Extensions.Logging;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;
	using AlphaRooms.Accommodation.Core.Services;
	using System.Diagnostics;

	public class B2CAvailabilityService : IB2CAvailabilityService
	{
		private readonly ILogger logger;
		private readonly IB2CAvailabilityResultCaching resultsCaching;
		private readonly IB2CAvailabilityRequestValidator requestValidator;
		private readonly IB2CAvailabilityResultFilterOptionsService resultsFilterOptionsService;
		private readonly IChannelInfoService channelInfoService;
		private readonly IAvailabilityService coreAvailabilityService;
		private readonly IB2CAvailabilityRequestFactory requestFactory;
		private readonly IB2CAvailabilityResultMappingService resultsMappingService;
		private readonly IB2CRequestStatusService requestStatusService;
		private readonly IB2CAvailabilityKeyGenerator keyGenerator;
		private readonly IB2CAvailabilityLogger availabilityLogger;
		private readonly IB2CAvailabilityProviderIdentifierService providerIdentifierService;
		private readonly IAccommodationConfigurationManager configurationManager;
		private readonly IB2CAvailabilityMapResultMappingService mapResultMappingService;
		private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
		private readonly IB2CAvailabilityFilterCriteriaFactory filterCriteriaFactory;
		private readonly IB2CAvailabilityDepositService depositService;
		private readonly IB2CGuidService guidService;
		private readonly IAccommodationBusiness accommodationBusiness;
        private readonly IAvailabilityRequestRoomService availabilityRequestRoomService;
		private readonly IB2CEventLogInfo b2cEventLogInfo;


		public B2CAvailabilityService(ILogger logger, IB2CAvailabilityResultCaching resultsCaching, IB2CAvailabilityRequestValidator requestValidator
			, IB2CAvailabilityResultFilterOptionsService resultsFilterOptionsService, IChannelInfoService channelInfoService, IAvailabilityService coreAvailabilityService
			, IB2CAvailabilityRequestFactory requestFactory, IB2CAvailabilityResultMappingService resultsMappingService, IB2CRequestStatusService requestStatusService
			, IB2CAvailabilityKeyGenerator keyGenerator, IB2CAvailabilityLogger availabilityLogger, IB2CAvailabilityProviderIdentifierService providerIdentifierService
			, IAccommodationConfigurationManager configurationManager, IB2CAvailabilityMapResultMappingService mapResultMappingService, IAvailabilityResultRoomIdGenerator roomIdGenerator
			, IB2CAvailabilityFilterCriteriaFactory filterCriteriaFactory
			, IB2CAvailabilityDepositService depositService, IB2CGuidService guidService, IAccommodationBusiness accommodationBusiness, IAvailabilityRequestRoomService availabilityRequestRoomService, IB2CEventLogInfo b2cEventLogInfo)
		{
			this.logger = logger;
			this.resultsCaching = resultsCaching;
			this.requestValidator = requestValidator;
			this.resultsFilterOptionsService = resultsFilterOptionsService;
			this.channelInfoService = channelInfoService;
			this.coreAvailabilityService = coreAvailabilityService;
			this.requestFactory = requestFactory;
			this.resultsMappingService = resultsMappingService;
			this.requestStatusService = requestStatusService;
			this.keyGenerator = keyGenerator;
			this.availabilityLogger = availabilityLogger;
			this.providerIdentifierService = providerIdentifierService;
			this.configurationManager = configurationManager;
			this.mapResultMappingService = mapResultMappingService;
			this.roomIdGenerator = roomIdGenerator;
			this.filterCriteriaFactory = filterCriteriaFactory;
			this.depositService = depositService;
			this.guidService = guidService;
			this.accommodationBusiness = accommodationBusiness;
            this.availabilityRequestRoomService = availabilityRequestRoomService;
			this.b2cEventLogInfo = b2cEventLogInfo;
		}

		public async Task<Guid> StartAvailabilitySearchAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
		{
			//Don't allow empty values
			if (availabilityRequest.EstablishmentId.IsEmpty())
			{
				availabilityRequest.EstablishmentId = null;
			}
			if (availabilityRequest.DestinationId.IsEmpty())
			{
				availabilityRequest.DestinationId = null;
			}
			//create a availabilityid if the caller didn't provide one
			bool isRestartSearch = false;
			if (availabilityRequest.AvailabilityId == null)
			{
				availabilityRequest.AvailabilityId = this.guidService.GenerateGuid(availabilityRequest.TC4Origin); //Guid.NewGuid();
			}
			else
			{
				isRestartSearch = true;
			}
			//Initialize new Accommodation Status and update Mongo with Request status Not started
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			var requestStatus = requestStatusService.CreateAvailabilityRequestStatus(availabilityRequest);
			await requestStatusService.SaveRequestStatusAsync(requestStatus);
			stopwatch.Stop();
			b2cEventLogInfo.LogTrace("B2C Trace: Update Mongo with Request status Not started, SearchID: " + availabilityRequest.AvailabilityId+", Duration: " + stopwatch.ElapsedMilliseconds);

			
			//start the availability process
			stopwatch.Start();
#pragma warning disable 4014
			Task.Run(async () => await StartAvailabilityProcessAsync(availabilityRequest, requestStatus, isRestartSearch));
#pragma warning restore 4014
			stopwatch.Stop();
			b2cEventLogInfo.LogTrace("B2C Trace: MethodName: StartAvailabilityProcessAsync, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
			return availabilityRequest.AvailabilityId.Value;
		}

		public async Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria
			, bool getFilterDetails)
		{
			AccommodationB2CAvailabilityRequest availabilityRequest = null;
			AccommodationB2CRequestStatus requestStatus = null;
			try
			{
				//get the request status
				requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null)
				{
					return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };
				}

				//if we dont want any results back just return the status
				if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
				{
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
				}

				//if search is not successful, don't continue just return with the status
				if (requestStatus.Status != Status.Successful)
				{
					return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status, StartDate = requestStatus.StartDate };
				}

				//if we dont want any results back just return the status
				if (filterCriteria.NumberOfResults == 0) return new AccommodationB2CAvailabilityResponse()
				{
					AvailabilityId = availabilityId,
					AvailabilityStatus = requestStatus.Status,
					AvailabilityResults = new AccommodationB2CAvailabilityResult[0],
					StartDate = requestStatus.StartDate
				};

				//get the availability request
				availabilityRequest = requestStatus.AvailabilityRequest;

				using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
				{
					AccommodationB2CAvailabilityResults availabilityResults;
					AccommodationB2CAvailabilityFilterOptionDetails filterDetails = null;
					if (getFilterDetails)
					{
						// create filter criteria based on the original filter without the filter options
						var filterCriteriaWithoutFilterOptions = filterCriteriaFactory.CreateWithoutFilterOptions(filterCriteria);

						//get the filtered and sorted accommodations
						availabilityResults = await resultsCaching.GetFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value,
							availabilityRequest.Rooms.Length, filterCriteriaWithoutFilterOptions, sortCriteria, availabilityRequest.Debugging, this.guidService.IsMetaGuid(availabilityId));

						// create a concrete list of results
						var accommodationResults = availabilityResults.Results;

						// create filter details
						filterDetails = resultsFilterOptionsService.CreateResultsFilterDetails(requestStatus.ResultsFilterOptions, filterCriteria, accommodationResults);

						//create hotel filter
						var hotelFilter = resultsFilterOptionsService.CreateEstablishmentAccommodationFilter(filterCriteria, FilterOptions.None);

						// apply the remaining filters
						accommodationResults = accommodationResults.AsQueryable().Where(hotelFilter).ToArray();

						// create room filter
						var roomFilter = resultsFilterOptionsService.CreateRoomAccommodationFilter(filterCriteria, FilterOptions.None);

						// apply room filter for each establishment
						accommodationResults.ForEach(i => i.Rooms = i.Rooms.AsQueryable().Where(roomFilter).ToArray());

						// remove empty establishments
						accommodationResults = accommodationResults.Where(i => i.Rooms.Length > 0).ToArray();

						// get total number of establishments
						var accommodationResultsCount = accommodationResults.Length;

						// process paging
						int? resultsStart = null, resultsCount = null;
						if (filterCriteria.NumberOfResults != null)
						{
							resultsStart = null;
							resultsCount = filterCriteria.NumberOfResults.Value;
						}
						else if (filterCriteria.PageNumber != null && filterCriteria.PageSize != null)
						{
							resultsStart = (filterCriteria.PageNumber.Value - 1) * filterCriteria.PageSize.Value;
							resultsCount = filterCriteria.PageSize.Value;
						}
						IEnumerable<AccommodationB2CAvailabilityResult> results = accommodationResults;
						if (resultsStart != null)
						{
							results = results.Skip(resultsStart.Value);
						}
						if (resultsCount != null)
						{
							results = results.Take(resultsCount.Value);
						}
						accommodationResults = results.ToArray();
						// create filtered 
						availabilityResults = new AccommodationB2CAvailabilityResults()
						{
							Results = accommodationResults,
							ResultsTotalCount = accommodationResultsCount
						};
					}
					else
					{
						//get the filtered and sorted accommodations
						availabilityResults = await resultsCaching.GetFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
							, availabilityRequest.Rooms.Length, filterCriteria, sortCriteria, availabilityRequest.Debugging, this.guidService.IsMetaGuid(availabilityId));
					}

					if (availabilityRequest.Debugging)
					{
						logger.Debug("GetAvailabilityResponse for search {0} returned {1} establishments after filtering.", availabilityId, availabilityResults.Results.Length);
					}

					//convert to AccommodationAvailabilityResponse and return
					return new AccommodationB2CAvailabilityResponse
					{
						AvailabilityId = availabilityId,
						AvailabilityStatus = requestStatus.Status,
						StartDate = requestStatus.StartDate,
						AvailabilityResults = availabilityResults.Results,
						AvailabilityTotalResultsCount = availabilityResults.ResultsTotalCount,
						AvailabilityFilterDetails = filterDetails,
						PromotionalCode = availabilityRequest.PromotionalCode
					};
				}
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				return new AccommodationB2CAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed, StartDate = requestStatus != null ? requestStatus.StartDate : DateTime.MinValue };
			}
		}

		public async Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId, AccommodationB2CAvailabilityFilter filterCriteria, AccommodationB2CAvailabilitySort sortCriteria)
		{
			AccommodationB2CAvailabilityRequest availabilityRequest = null;
			AccommodationB2CRequestStatus requestStatus = null;
			try
			{
				//get the request status
				requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null) return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

				//if we dont want any results back just return the status
				if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
				{
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
				}

				//if search is not successful, don't continue just return with the status
				if (requestStatus.Status != Status.Successful) return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status, StartDate = requestStatus.StartDate };

				//if we dont want any results back just return the status
				if (filterCriteria.NumberOfResults == 0) return new AccommodationB2CAvailabilityResponse()
				{
					AvailabilityId = availabilityId,
					AvailabilityStatus = requestStatus.Status,
					AvailabilityResults = new AccommodationB2CAvailabilityResult[0],
					StartDate = requestStatus.StartDate
				};

				//get the availability request
				availabilityRequest = requestStatus.AvailabilityRequest;

				using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
				{
					//get the filtered and sorted accommodation
					var availabilityResult = await resultsCaching.GetByEstablishmentIdFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
						, establishmentId, filterCriteria, sortCriteria, availabilityRequest.Debugging, this.guidService.IsMetaGuid(availabilityId));

					if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityResponse for search {0} returned {1} rooms after filtering.", availabilityId, (availabilityResult != null ? availabilityResult.Rooms.Length : -1));

					//convert to AccommodationAvailabilityResponse and return
					return new AccommodationB2CAvailabilityResponse
					{
						AvailabilityId = availabilityId,
						AvailabilityStatus = requestStatus.Status,
						StartDate = requestStatus.StartDate,
						AvailabilityResults = (availabilityResult != null ? new[] { availabilityResult } : new AccommodationB2CAvailabilityResult[0]),
						AvailabilityTotalResultsCount = (availabilityResult != null ? 1 : 0),
						PromotionalCode = availabilityRequest.PromotionalCode
					};
				}
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				return new AccommodationB2CAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed, StartDate = requestStatus != null ? requestStatus.StartDate : DateTime.MinValue };
			}
		}

		public async Task<AccommodationB2CAvailabilityResponse> GetAvailabilityResponseByRoomsAsync(Guid availabilityId, uint[] roomIds)
		{
			AccommodationB2CAvailabilityRequest availabilityRequest = null;
			AccommodationB2CRequestStatus requestStatus = null;
			try
			{
				//get the request status
				requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null) return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

				//if we dont want any results back just return the status
				if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
				{
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
				}

				//if search is not successful, don't continue just return with the status
				if (requestStatus.Status != Status.Successful) return new AccommodationB2CAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status, StartDate = requestStatus.StartDate };

				//get the availability request
				availabilityRequest = requestStatus.AvailabilityRequest;
				using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
				{
					//get the filtered and sorted accommodation
					var availabilityResults = await resultsCaching.GetByRoomIdsAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value, roomIds, this.guidService.IsMetaGuid(availabilityId));

					if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityResponse for search {0} returned {1} rooms after filtering.", availabilityId, availabilityResults.Length);

					//convert to AccommodationAvailabilityResponse and return
					return new AccommodationB2CAvailabilityResponse
					{
						AvailabilityId = availabilityId,
						AvailabilityStatus = requestStatus.Status,
						StartDate = requestStatus.StartDate,
						AvailabilityResults = availabilityResults,
						AvailabilityTotalResultsCount = availabilityResults.Length,
						PromotionalCode = availabilityRequest.PromotionalCode
					};
				}
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				return new AccommodationB2CAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed, StartDate = requestStatus != null ? requestStatus.StartDate : DateTime.MinValue };
			}
		}

		public async Task<AccommodationB2CAvailabilityMapResponse> GetAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2CAvailabilityFilter filterCriteria)
		{
			AccommodationB2CAvailabilityRequest availabilityRequest = null;
			AccommodationB2CRequestStatus requestStatus = null;
			try
			{
				//get the request status
				requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null) return new AccommodationB2CAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

				//if we dont want any results back just return the status
				if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
				{
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
				}

				//if search is not successful, don't continue just return with the status
				if (requestStatus.Status != Status.Successful)
				{
					return new AccommodationB2CAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };
				}

				//if we dont want any results back just return the status
				if (filterCriteria.NumberOfResults == 0) return new AccommodationB2CAvailabilityMapResponse()
				{
					AvailabilityId = availabilityId,
					AvailabilityStatus = requestStatus.Status,
					AvailabilityMapResults = new AccommodationB2CAvailabilityMapResult[0]
				};

				//get the availability request
				availabilityRequest = requestStatus.AvailabilityRequest;

				using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityMapResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
				{
					//get the filtered and sorted accommodations
					var availabilityResults = await resultsCaching.GetFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
						, availabilityRequest.Rooms.Length, filterCriteria, new AccommodationB2CAvailabilitySort(), availabilityRequest.Debugging, this.guidService.IsMetaGuid(availabilityId));

					var mappedResults = mapResultMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, availabilityResults.Results);

					if (availabilityRequest.Debugging)
					{
						logger.Debug("GetAvailabilityMapResponse for search {0} returned {1} rooms after filtering.", availabilityId, mappedResults.Count());
					}

					//convert to AccommodationAvailabilityResponse and return
					return new AccommodationB2CAvailabilityMapResponse
					{
						AvailabilityId = availabilityId,
						AvailabilityStatus = requestStatus.Status,
						AvailabilityMapResults = mappedResults.ToArray(),
						AvailabilityTotalResultsCount = availabilityResults.ResultsTotalCount
					};
				}
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityMapResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				return new AccommodationB2CAvailabilityMapResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
			}
		}

		public async Task<AccommodationB2CAvailabilityMapResponse> GetAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId)
		{
			AccommodationB2CAvailabilityRequest availabilityRequest = null;
			AccommodationB2CRequestStatus requestStatus = null;
			try
			{
				//get the request status
				requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null) return new AccommodationB2CAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

				//if we dont want any results back just return the status
				if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
				{
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
				}

				//if search is not successful, don't continue just return with the status
				if (requestStatus.Status != Status.Successful) return new AccommodationB2CAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

				//get the availability request
				availabilityRequest = requestStatus.AvailabilityRequest;

				using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityMapResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
				{
					//get the filtered and sorted accommodations
					var availabilityResult = await resultsCaching.GetByEstablishmentIdAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value,
						establishmentId, this.guidService.IsMetaGuid(availabilityId));

					var mappedResults = mapResultMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, new[] { availabilityResult });

					if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityMapResponseByEstablishmentAsync for search {0} returned {1} rooms after filtering.", availabilityId, mappedResults.Count());

					//convert to AccommodationAvailabilityResponse and return
					return new AccommodationB2CAvailabilityMapResponse
					{
						AvailabilityId = availabilityId,
						AvailabilityStatus = requestStatus.Status,
						AvailabilityMapResults = mappedResults.ToArray(),
						AvailabilityTotalResultsCount = mappedResults.Count
					};
				}
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityMapResponseByEstablishmentAsync Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				return new AccommodationB2CAvailabilityMapResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
			}
		}

		public async Task<AccommodationB2CAvailabilityRequest> GetAvailabilityRequestAsync(Guid availabilityId)
		{
			try
			{
				//get the request status
				var requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				return (requestStatus == null ? null : requestStatus.AvailabilityRequest);
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityRequestAsync Error: search [{0}] {1}", availabilityId, ex.GetDetailedMessageWithInnerExceptions());
				throw ex;
			}
		}

		public async Task<AccommodationB2CAvailabilityResultsFilterOptions> GetAvailabilityResultsFilterOptionsAsync(Guid availabilityId)
		{
			try
			{
				//get the request status
				var requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

				//throw exception when request status is not available
				if (requestStatus == null) return null;

				return requestStatus.ResultsFilterOptions;
			}
			catch (Exception ex)
			{
				logger.Error("Accommodation GetAvailabilityResultsFilterOptions Error: search [{0}] {1}", availabilityId, ex.GetDetailedMessageWithInnerExceptions());
				throw ex;
			}
		}

		private async Task StartAvailabilityProcessAsync(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CRequestStatus requestStatus, bool isRestartSearch)
		{
			using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.AvailabilitySearching, availabilityRequest.AvailabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
			{
				AccommodationProvider[] providersToSearch = null;
				AccommodationProvider[] providers = null;
				AccommodationAvailabilityResponse coreAvailabilityResponse = null;
				AccommodationB2CAvailabilityResultsFilterOptions resultsFilterOptions = null;
				Guid resultsBaseDestinationId = Guid.Empty;
				Guid resultsAvailabilityId = Guid.Empty;
				Exception exception = null;
				var processStartDate = DateTime.Now;
				DateTime? postProcessStartDate = null;
				Stopwatch stopwatch = new Stopwatch();

				var searchTimer = new SearchTimerService(availabilityRequest.AvailabilityId.Value, "B2C_Search");

				try
				{
					// get channelinfo from request channel
					var channelInfo = await channelInfoService.GetChannelInfo(availabilityRequest.Channel);

					// create a empty filter option 
					resultsFilterOptions = resultsFilterOptionsService.CreateEmptyFilterOptions(availabilityRequest, channelInfo);

					// update Mongo with inprogress status
					stopwatch.Start();
					await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Search Started");
					stopwatch.Stop();
					b2cEventLogInfo.LogTrace("B2C Trace: update Mongo with inprogress status, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

					// Validate Request
					requestValidator.ValidateAvailabilityRequest(availabilityRequest);
					
					// get list of providers
					stopwatch.Start();
					providersToSearch = await providerIdentifierService.GetActiveAvailabilityProvidersAsync(availabilityRequest);
					stopwatch.Stop();
					b2cEventLogInfo.LogTrace("B2C Trace: get list of providers, SearchID: " + availabilityRequest.AvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

					// set Bev5 suppliers to search on provider, from tc4 configured suppliers to search
					if (availabilityRequest.TC4Origin && providersToSearch.Where(x => x.Id == 5).Any())
					{
						var Bev5Provider = providersToSearch.Where(x => x.Id == 5).FirstOrDefault().Parameters.Where(i => i.ParameterName == "Bev5EdiProvidersToSearch").FirstOrDefault();
						Bev5Provider.ParameterValue = string.Join(",", availabilityRequest.SuppliersToSearch);
					}

					// generate the search key
					var availabilityKey = keyGenerator.CreateAvailabilityKey(availabilityRequest, providersToSearch);

					// check if the same request was already recently performed
					stopwatch.Start();
					var cachedRequestStatus = await requestStatusService.GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(availabilityKey, availabilityRequest.AvailabilityId);
					stopwatch.Stop();
					b2cEventLogInfo.LogTrace("B2C Trace: check request on mongo, SearchKey: " + availabilityKey + ", Duration: " + stopwatch.ElapsedMilliseconds);

					// if results cache is available
					DateTime resultsCacheExpireDate = requestStatusService.CreateResultsCacheExpireDate();

					if (cachedRequestStatus != null
						&& cachedRequestStatus.Status == Status.Successful
						&& !requestStatusService.IsResultsExpireDateExpired(cachedRequestStatus))
					{
						// get parameters from the cached requests status
						stopwatch.Start();
						resultsCacheExpireDate = cachedRequestStatus.ResultsExpireDate.Value;
						resultsFilterOptions = cachedRequestStatus.ResultsFilterOptions;
						resultsBaseDestinationId = cachedRequestStatus.ResultsBaseDestinationId.Value;
						resultsAvailabilityId = cachedRequestStatus.Id;
						stopwatch.Stop();
						b2cEventLogInfo.LogTrace("B2C Trace: Results cache is available, SearchKey: " + availabilityKey + ", Duration: " + stopwatch.ElapsedMilliseconds);
					}
					else
					{
						// create a new set of results 
						resultsAvailabilityId = availabilityRequest.AvailabilityId.Value;

						// get list of providers 
						providers = providersToSearch;

						// create a core availability request
						var coreAvailabilityRequest = requestFactory.CreateAvailabilityRequest(availabilityRequest, channelInfo, providers);

						// perform a availability search
						stopwatch.Start();
						var coreTimer = new SearchTimerService(coreAvailabilityRequest.AvailabilityId, "B2C_Core_Search");
						coreAvailabilityResponse = await coreAvailabilityService.GetAvailabilityResponseAsync(coreAvailabilityRequest);
						coreTimer.Record();
						stopwatch.Stop();
						b2cEventLogInfo.LogTrace("B2C Trace: perform a availability search, SearchId: " + resultsAvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

						//use the results expire date as a request status expire
						resultsCacheExpireDate = coreAvailabilityResponse.ResultsCacheExpireDate;

						//get post process start date
						postProcessStartDate = coreAvailabilityResponse.PostProcessStartDate;

						// remove possible old results from a restart search
						if (isRestartSearch)
						{
							await resultsCaching.DeleteRangeAsync(resultsBaseDestinationId, resultsAvailabilityId, this.guidService.IsMetaGuid(availabilityRequest.AvailabilityId));
						}

						// post process only if the core returned results
						if (coreAvailabilityResponse.Results.Any())
						{
                            // get base destination
                            resultsBaseDestinationId = coreAvailabilityResponse.Results[0].BaseDestinationId;

                            // execute post process for the results
                            var timer = new SearchTimerService(availabilityRequest.AvailabilityId.Value, "B2C_Search_Postprocess");
                            accommodationBusiness.FilterBySupplier(availabilityRequest.SuppliersToSearch, null, coreAvailabilityResponse);

							stopwatch.Start();
							var coreResults = await accommodationBusiness.PostProcessAvailabilityResultsAsync(coreAvailabilityRequest, coreAvailabilityResponse.Results);
							stopwatch.Stop();
							b2cEventLogInfo.LogTrace("B2C Trace: post process the response, SearchId: " + resultsAvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
							timer.Record();

                            //map the core results
                            timer = new SearchTimerService(availabilityRequest.AvailabilityId.Value, "B2C_Search_Mapping");
							stopwatch.Start();
							var mappedResults = await resultsMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, coreResults);
							stopwatch.Stop();
							b2cEventLogInfo.LogTrace("B2C Trace: map the core results, SearchId: " + resultsAvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
							timer.Record();

                            // update rooms to mark them eligible for deposit
                            mappedResults = await depositService.UpdateDepositEligibilityAsync(mappedResults, availabilityRequest.CheckInDate);

                            // create filter and save results only if there are valid results
                            if (mappedResults.Any())
                            {
                                // create filter options based on the availability results
                                resultsFilterOptions = await resultsFilterOptionsService.CreateResultsFilterOptionsAsync(availabilityRequest, channelInfo, mappedResults);

                                // set the cache results to the same expire date
                                mappedResults.ForEach(i => i.CacheExpireDate = resultsCacheExpireDate);

								// save results on mongo
								stopwatch.Start();
								await resultsCaching.SaveRangeAsync(resultsBaseDestinationId, mappedResults, this.guidService.IsMetaGuid(availabilityRequest.AvailabilityId));
								stopwatch.Stop();
								b2cEventLogInfo.LogTrace("B2C Trace: save results on mongo, SearchId: " + resultsAvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);
							}
                        }
                    }

                    // update Mongo Status
                    requestStatus.HasTC4Origin = availabilityRequest.TC4Origin;
                    requestStatus.AvailabilityKey = availabilityKey;
                    requestStatus.ResultsFilterOptions = resultsFilterOptions;
                    requestStatus.ResultsExpireDate = resultsCacheExpireDate;
                    requestStatus.ResultsBaseDestinationId = resultsBaseDestinationId;
                    requestStatus.ResultsAvailabilityId = resultsAvailabilityId;
                    requestStatus.Status = (coreAvailabilityResponse != null && coreAvailabilityResponse.ProcessResponseDetails.Any() && coreAvailabilityResponse.ProcessResponseDetails.All(i => i.Exception != null) ? Status.Failed : Status.Successful);
                    requestStatus.Message = "Accommodation Search Completed";

                }
				catch (Exception ex)
				{
					// update Mongo Status
					requestStatus.Status = Status.Failed;
					requestStatus.Message = "Error: " + ex.Message;
					exception = ex;
					logger.Error("Accommodation StartSearchProcess Error: search [{0}] {1}\n\n{2}", availabilityRequest.AvailabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
				}

				stopwatch.Start();
				await requestStatusService.SaveRequestStatusAsync(requestStatus);
				stopwatch.Stop();
				b2cEventLogInfo.LogTrace("B2C Trace: update Mongo Status with Accommodation Search Completed, SearchId: " + resultsAvailabilityId + ", Duration: " + stopwatch.ElapsedMilliseconds);

				if (availabilityRequest.Debugging && coreAvailabilityResponse != null && coreAvailabilityResponse.Results != null)
				{
					logger.Debug("Accommodation availability search found {0} accommodations for search [{1}] \n\nRequest:\n\n{2}", coreAvailabilityResponse.Results.Count(), availabilityRequest.AvailabilityId, availabilityRequest.ToIndentedJson());
				}
				await availabilityLogger.LogAsync(
					availabilityRequest,
					requestStatus,
					providers,
					coreAvailabilityResponse,
					(postProcessStartDate != null ? (TimeSpan?)(DateTime.Now - postProcessStartDate) : null),
					DateTime.Now - processStartDate,
					exception);
				searchTimer.Record();
			}
		}

        public async Task<AccommodationB2CAvailabilityResponse> GetMetaAvailablityResponseAsync(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            AccommodationAvailabilityResponse coreAvailabilityResponse = null;
            Guid resultsBaseDestinationId = Guid.Empty;
            AccommodationB2CAvailabilityResultsFilterOptions resultsFilterOptions = null;
            Guid resultsAvailabilityId = Guid.Empty;
            IList<AccommodationB2CAvailabilityResult> availabilityResults = null;
            AccommodationB2CRequestStatus requestStatus = null;
            var processStartDate = DateTime.Now;
            DateTime? postProcessStartDate = null;
            Exception exception = null;
            AccommodationProvider[] providersToSearch = null;

            try
            {
                // check if the same request was already recently performed with token key
                requestStatus = await requestStatusService.GetSuccessfulRequestStatusByMetaTokenKeyOrNullAsync(availabilityRequest.MetaToken);
            }
            catch (Exception ex)
            {
                logger.Error("Monogo connection error{0}{1}", ex.Message, ex.StackTrace);
                return null;
            }

            // if results cache is available
            DateTime resultsCacheExpireDate = requestStatusService.CreateResultsCacheExpireDate();

            if (requestStatus != null
                        && requestStatus.Status == Status.Successful
                        && !requestStatusService.IsResultsExpireDateExpired(requestStatus))
            {
                // get parameters from the cached requests status
                resultsCacheExpireDate = requestStatus.ResultsExpireDate.Value;
                resultsFilterOptions = requestStatus.ResultsFilterOptions;
                resultsBaseDestinationId = requestStatus.ResultsBaseDestinationId.Value;
                resultsAvailabilityId = requestStatus.Id;

                //get the accommodations results from mongo by token key
                availabilityResults = await resultsCaching.GetByTokenKeyAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value, this.guidService.IsMetaGuid(resultsAvailabilityId));
            }
            else
            {
                try
                {
                    // create a new set of results 
                    resultsAvailabilityId = this.guidService.GenerateGuid(availabilityRequest.TC4Origin); //Guid.NewGuid();
                    availabilityRequest.AvailabilityId = resultsAvailabilityId;

                    // get list of providers 
                    providersToSearch = await providerIdentifierService.GetMetaActiveAvailabilityProvidersAsync(availabilityRequest);

                    var channelInfo = await channelInfoService.GetChannelInfo(availabilityRequest.Channel);

                    // create a empty filter option 
                    resultsFilterOptions = resultsFilterOptionsService.CreateEmptyFilterOptions(availabilityRequest, channelInfo);

                    availabilityRequest.Channel = channelInfo.Channel;
                    availabilityRequest.ProvidersToSearch = providersToSearch.Select(i => i.Id).ToArray();

                    requestStatus = requestStatusService.CreateAvailabilityRequestStatus(availabilityRequest);

                    await requestStatusService.SaveRequestStatusAsync(requestStatus);

                    // create a core availability request
                    var coreAvailabilityRequest = requestFactory.CreateAvailabilityRequest(availabilityRequest, channelInfo, providersToSearch);

                    // perform a meta availability response
                    coreAvailabilityResponse = await coreAvailabilityService.GetMetaAvailabilityResponseAsync(coreAvailabilityRequest);
                    resultsCacheExpireDate = coreAvailabilityResponse.ResultsCacheExpireDate;

                    //get post process start date
                    postProcessStartDate = coreAvailabilityResponse.PostProcessStartDate;


                    // post process only if the core returned results
                    if (coreAvailabilityResponse.Results.Any())
                    {
                        //post process
                        var coreResults = accommodationBusiness.TravelGatePostProcessAvailabilityResultsAsync(coreAvailabilityRequest, coreAvailabilityResponse.Results);

                        List<AccommodationB2CAvailabilityRequestRoom> requestRooms = new List<AccommodationB2CAvailabilityRequestRoom>();
                        var availabilityRequestRooms = availabilityRequestRoomService.CreateAvailabilityRequestRooms(coreAvailabilityRequest);

                        foreach (var requestedREooms in availabilityRequestRooms)
                        {
                            AccommodationB2CAvailabilityRequestRoom requestRoom = new AccommodationB2CAvailabilityRequestRoom
                            {
                                Adults = requestedREooms.AdultsCount,
                                Children = requestedREooms.ChildrenCount,
                                RoomNumber = requestedREooms.Room.RoomNumber,
                                ChildAges = requestedREooms.Room.Guests.Where(i => i.Age <= configurationManager.AccommodationMaxChildAge).Select(j => j.Age).ToArray(),
                            };
                            requestRooms.Add(requestRoom);
                        }

                        //make AccommodationB2CAvailabilityRequest and save in mongo
                        availabilityRequest.CheckInDate = coreAvailabilityRequest.CheckInDate;
                        availabilityRequest.CheckOutDate = coreAvailabilityRequest.CheckOutDate;
                        availabilityRequest.AvailabilityId = coreAvailabilityRequest.AvailabilityId;
                        availabilityRequest.Channel = availabilityRequest.Channel;
                        availabilityRequest.SearchType = SearchType.HotelOnly;
                        availabilityRequest.DestinationId = coreAvailabilityRequest.DestinationId;
                        availabilityRequest.EstablishmentId = coreAvailabilityRequest.EstablishmentId;
                        availabilityRequest.Rooms = requestRooms.ToArray();
                        availabilityRequest.MetaToken = coreAvailabilityRequest.MetaToken;
                        availabilityRequest.ProvidersToSearch = coreAvailabilityRequest.Providers.Select(i => i.Id).ToArray();
                        availabilityRequest.AvailabilityId = coreAvailabilityRequest.AvailabilityId;
                        requestStatus.AvailabilityRequest = availabilityRequest;
                        requestStatus.RequestType = RequestType.Availability;
                        requestStatus.StartDate = coreAvailabilityRequest.CheckInDate;

                        //map the core results
                        availabilityResults = await resultsMappingService.MapFromCoreAvailabilityResultsAsync(requestStatus.AvailabilityRequest, coreResults);
                        // update rooms to mark them eligible for deposit
                        availabilityResults = await depositService.UpdateDepositEligibilityAsync(availabilityResults, coreAvailabilityRequest.CheckInDate);

                        // create filter and save results only if there are valid results
                        if (availabilityResults.Any())
                        {
                            // get base destination
                            resultsBaseDestinationId = coreAvailabilityResponse.Results[0].BaseDestinationId;

                            // create filter options based on the availability results
                            resultsFilterOptions = await resultsFilterOptionsService.CreateResultsFilterOptionsAsync(requestStatus.AvailabilityRequest, channelInfo, availabilityResults);

                            availabilityResults.ForEach(i => i.CacheExpireDate = resultsCacheExpireDate);

                            // save results
                            await resultsCaching.SaveRangeAsync(resultsBaseDestinationId, availabilityResults, this.guidService.IsMetaGuid(availabilityRequest.AvailabilityId));
                        }

                        // update Mongo Status
                        requestStatus.AvailabilityKey = availabilityRequest.MetaToken;
                        requestStatus.HasTC4Origin = availabilityRequest.TC4Origin;
                        requestStatus.ResultsFilterOptions = resultsFilterOptions;
                        requestStatus.ResultsBaseDestinationId = resultsBaseDestinationId;
                        requestStatus.ResultsAvailabilityId = resultsAvailabilityId;
                        requestStatus.ResultsExpireDate = resultsCacheExpireDate;
                        requestStatus.Status = (coreAvailabilityResponse != null && coreAvailabilityResponse.ProcessResponseDetails.Any() && coreAvailabilityResponse.ProcessResponseDetails.All(i => i.Exception != null) ? Status.Failed : Status.Successful);
                        requestStatus.Message = "Accommodation Search Completed";

                        await requestStatusService.SaveRequestStatusAsync(requestStatus);


                    }
                }
                catch (Exception ex)
                {
                    logger.Error("B2C Availability error{0}{1}", ex.Message, ex.StackTrace);
                    return null;
                }

                if (availabilityRequest.Debugging && coreAvailabilityResponse != null && coreAvailabilityResponse.Results != null)
                {
                    logger.Debug("Accommodation availability search found {0} accommodations for search [{1}] \n\nRequest:\n\n{2}", coreAvailabilityResponse.Results.Count(), availabilityRequest.AvailabilityId, availabilityRequest.ToIndentedJson());
                }
                await availabilityLogger.LogAsync(
                            availabilityRequest,
                            requestStatus,
                            providersToSearch,
                            coreAvailabilityResponse,
                            (postProcessStartDate != null ? (TimeSpan?)(DateTime.Now - postProcessStartDate) : null),
                            DateTime.Now - processStartDate,
                            exception);
            }

            return new AccommodationB2CAvailabilityResponse
            {
                AvailabilityId = resultsAvailabilityId,
                AvailabilityStatus = requestStatus.Status,
                StartDate = requestStatus.StartDate,
                AvailabilityResults = availabilityResults.ToArray(),
                AvailabilityTotalResultsCount = availabilityResults.Count(),

            };
        }
    }
}
