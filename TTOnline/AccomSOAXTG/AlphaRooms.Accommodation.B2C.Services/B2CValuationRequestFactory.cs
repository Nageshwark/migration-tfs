﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.SOACommon.DomainModels;
    using AlphaRooms.Utilities;
    using AutoMapper;
    using System.Linq;

    public class B2CValuationRequestFactory : IB2CValuationRequestFactory
    {
        static B2CValuationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CValuationRequest, AccommodationValuationRequest>();
            }
        }

        public AccommodationValuationRequest CreateValuationRequest(AccommodationB2CValuationRequest valuationRequest, AccommodationAvailabilityResult[] rooms, ChannelInfo channelInfo)
        {
            var request = Mapper.Map<AccommodationB2CValuationRequest, AccommodationValuationRequest>(valuationRequest);
            request.ChannelInfo = channelInfo;
            request.SelectedRoomIds = rooms
                .Select(x => x.RoomId)
                .ToArray();

            return request;
        }
    }
}
