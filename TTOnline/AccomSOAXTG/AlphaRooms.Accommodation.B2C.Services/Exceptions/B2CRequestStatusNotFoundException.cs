﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services.Exceptions
{
    public class B2CRequestStatusNotFoundException : Exception
    {
        private readonly Guid availabilityId;

        public B2CRequestStatusNotFoundException(Guid availabilityId) : base("Request status not found (id = '" + availabilityId + "').")
        {
            this.availabilityId = availabilityId;
        }

        public Guid AvailabilityId
        {
            get { return this.availabilityId; }
        }
    }
}
