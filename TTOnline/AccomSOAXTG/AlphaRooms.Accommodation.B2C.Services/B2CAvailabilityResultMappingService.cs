﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Contracts.Enums;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Accommodation.Core.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.ExtensionMethods;
    using AutoMapper;
    using Core.Interfaces;
    using Ninject.Extensions.Logging;
    using SOACommon.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class B2CAvailabilityResultMappingService : IB2CAvailabilityResultMappingService
    {
        static B2CAvailabilityResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CAvailabilityResultRoom, AccommodationAvailabilityResult>();
            }
        }

        private readonly ILogger logger;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IBoardRepository boardTypeRepository;
        private readonly ITravellerTypeRepository travellerTypeRepository;
        private readonly IFacilityRepository facilityRepository;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IRoomKeyGenerator roomKeyGenerator;
        private readonly ISupplierAliasRepository supplierAliasRepository;

        public B2CAvailabilityResultMappingService(
            ILogger logger,
            IEstablishmentDetailsRepository establishmentDetailsRepository,
            IBoardRepository boardTypeRepository,
            IFacilityRepository facilityRepository,
            ITravellerTypeRepository travellerTypeRepository,
            IAccommodationProviderRepository providerRepository,
            IAccommodationSupplierRepository supplierRepository,
            IAccommodationConfigurationManager configurationManager,
            IAvailabilityResultRoomIdGenerator roomIdGenerator,
            IRoomKeyGenerator roomKeyGenerator,
            ISupplierAliasRepository supplierAliasRepository)
		{ 
            this.logger = logger;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
            this.boardTypeRepository = boardTypeRepository;
            this.travellerTypeRepository = travellerTypeRepository;
            this.facilityRepository = facilityRepository;
            this.providerRepository = providerRepository;
            this.supplierRepository = supplierRepository;
            this.configurationManager = configurationManager;
            this.roomIdGenerator = roomIdGenerator;
            this.roomKeyGenerator = roomKeyGenerator;
            this.supplierAliasRepository = supplierAliasRepository;
        }

        public AccommodationAvailabilityResult[] MapToCoreAvailabilityResult(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationB2CAvailabilityResult> availabilityResults, Dictionary<Guid, EstablishmentDetails> establishmentDetails)
        {
            var accommodationAvailabilityResults = new List<AccommodationAvailabilityResult>();

            foreach (var availabilityResult in availabilityResults)
            {
                foreach (var room in availabilityResult.Rooms)
                {
                    var result = this.CreateAccommodationAvailabilityResult(availabilityRequest, room, availabilityResult, establishmentDetails);

                    if (result != null)
                    {
                        accommodationAvailabilityResults.Add(result);
                    }
                }
            }

            return accommodationAvailabilityResults.ToArray();
        }

        private AccommodationAvailabilityResult CreateAccommodationAvailabilityResult(AccommodationB2CAvailabilityRequest availabilityRequest, AccommodationB2CAvailabilityResultRoom room, AccommodationB2CAvailabilityResult result, Dictionary<Guid, EstablishmentDetails> establishmentDetails)
        {
            var providerTask = providerRepository.GetAllAsync();
            var provider = providerTask.Result.FirstOrDefault(p => p.EdiCode == room.ProviderProviderCode);

            //ACS-1151 this code changes the behaviour how supplier is fetched against a provider
            var supplierTask = supplierRepository.GetByProviderCodeAsync(provider.Id);
            var supplier = supplierTask.Result.Any(x => x.EdiCode == room.SupplierEdiCode)
                ? supplierTask.Result.First(x => x.EdiCode == room.SupplierEdiCode)
                : supplierTask.Result.First(x => x.EdiCode == null);

            var destinationId = result.DestinationId;
            var baseDestinationId = result.BaseDestinationId;
            var establishmentStarRating = result.EstablishmentStarRating;
            var checkIn = availabilityRequest.CheckInDate;
            var checkOut = availabilityRequest.CheckOutDate;

            var resultRoom = Mapper.Map<AccommodationB2CAvailabilityResultRoom, AccommodationAvailabilityResult>(room);

            resultRoom.AvailabilityRequestId = availabilityRequest.AvailabilityId.Value;
            resultRoom.CacheExpireDate = result.CacheExpireDate;

            resultRoom.DiscountPrice = room.Discount;
            resultRoom.DestinationId = destinationId;
            // BaseDestinationID: This is just for the Price Adjustments in the Valuation Markup. Establishment dependent so nothing to do with destination or basedestination, but must be set.
            resultRoom.BaseDestinationId = baseDestinationId;
            resultRoom.EstablishmentStarRating = establishmentStarRating;
            resultRoom.EstablishmentId = result.EstablishmentId;
            resultRoom.RoomDescription = room.RoomDescription ?? room.ProviderRoomDescription;
            resultRoom.Provider = provider;
            resultRoom.ProviderId = resultRoom.Provider?.Id ?? 0;
            resultRoom.Supplier = supplier;
            resultRoom.SupplierId = supplier.Id;
            resultRoom.SalePrice = room.Price;
            resultRoom.Price = room.IsActiveAccommodationAdminFee ? new Money(room.Price.Amount - room.AccommodationAdminFee.Amount, room.Price.CurrencyCode) : room.Price;
            resultRoom.CostPrice = room.ProviderPrice;

            resultRoom.AdultsCount = result.RoomOccupancies.First(ro => ro.RoomNumber == room.RoomNumber).AdultsCount;
            resultRoom.ChildrenCount = result.RoomOccupancies.First(ro => ro.RoomNumber == room.RoomNumber).ChildrenAndInfantsCount;

            resultRoom.AccommodationAdminFee = room.AccommodationAdminFee;
            resultRoom.IsActiveAccommodationAdminFee = room.IsActiveAccommodationAdminFee;

            if (establishmentDetails != null && establishmentDetails.Count > 0)
            {
                var establishmentDetailsItem = establishmentDetails[resultRoom.EstablishmentId];
                resultRoom.EstablishmentChainId = establishmentDetailsItem.EstablishmentChainId;
                resultRoom.EstablishmentStarRating = establishmentDetailsItem.EstablishmentStarRating;
                resultRoom.CountryCode = establishmentDetailsItem.CountryISOCode;
            }

            // the below are missing but I do not think they are missing
            //// resultRoom.BaseDestinationId = ?
            //// resultRoom.CountryCode = ?
            //// resultRoom.EstablishmentChainId = ?
            //// resultRoom.Margin = ?
            //// resultRoom.PriceBreakdown = ?
            //// resultRoom.Source = ?

            resultRoom.ProviderResult = new AccommodationProviderAvailabilityResult
            {
                ProviderEstablishmentCode = room.ProviderEstablishmentCode,
                EstablishmentEdiCode = result.EstablishmentId.ToString(),
                ProviderName = room.ProviderName,
                ProviderEdiCode = room.ProviderEdiCode,
                ProviderAliasName = room.ProviderAliasName,
                ProviderProviderCode = room.ProviderProviderCode,
                ProviderFilterName = room.ProviderFilterName,
                CostPrice = room.ProviderPrice,
                SalePrice = room.ProviderPrice,
                IsBindingRate = room.IsBindingRate,
                IsNonRefundable = room.IsNonRefundable,
                IsOpaqueRate = room.IsOpaqueRate,
                PaymentModel = room.PaymentModel,
                RateType = room.RateType,
                ProviderSpecificData = room.ProviderSpecificData,
                SupplierEdiCode = room.SupplierEdiCode,
                DestinationEdiCode = room.ProviderDestinationCode,
                CheckInDate = checkIn,
                CheckOutDate = checkOut,
                Contract = room.ProviderContract,
                BoardCode = room.ProviderBoardCode,
                BoardDescription = room.ProviderBoardDescription,
                RoomNumber = (byte)room.RoomNumber,
                RoomDescription = room.ProviderRoomDescription,
                RoomCode = room.ProviderRoomCode,
                Adults = result.RoomOccupancies.First(ro => ro.RoomNumber == room.RoomNumber).AdultsCount,
                Children = result.RoomOccupancies.First(ro => ro.RoomNumber == room.RoomNumber).ChildrenAndInfantsCount,
                Infants = 0
            };

            // create room guest
            AccommodationB2CAvailabilityRequestRoom roomRequest = availabilityRequest.Rooms.Single(x => x.RoomNumber == room.RoomNumber);
            var guestAges = Enumerable.Repeat(configurationManager.AccommodationAdultAge, roomRequest.Adults).Concat(roomRequest.ChildAges).ToArray();

            // rebuild the Id
            resultRoom.RoomId = this.roomIdGenerator.CreateRoomId(
                availabilityRequest.Channel,
                guestAges,
                resultRoom);

            // rebuild the key
            this.roomKeyGenerator.ApplyAvailabilityRoomKey(availabilityRequest.Channel, guestAges, resultRoom);

            return resultRoom;
        }

        public async Task<IList<AccommodationB2CAvailabilityResult>> MapFromCoreAvailabilityResultsAsync(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> coreResults)
        {
            if (coreResults.IsEmpty()) return new AccommodationB2CAvailabilityResult[0];
            var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreResults[0].BaseDestinationId, availabilityRequest.Channel);
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            var travellerTypes = await travellerTypeRepository.GetAllDictionaryAsync();
            var facilities = await facilityRepository.GetAllDictionaryAsync();
            var aliasNames = await supplierAliasRepository.GetAllDictionaryAsync();

            //Removing resutls with null values, Ideally we should not have establishments with pricing as null
            coreResults = coreResults.Where(r => r.Price != null).ToList<AccommodationAvailabilityResult>();
            var results = coreResults.GroupBy(i => i.EstablishmentId).AsParallel().Select(coreResult =>
            {
                AccommodationB2CAvailabilityResult result = null;
                try
                {
                    var establishmentResults = coreResult.ToArray();
                    //if it is a multiroom search and all rooms are not available for the hotel, do not return this hotel
                    if (availabilityRequest.Rooms.Count() > 1)
                    {
                        for (var i = 1; i <= availabilityRequest.Rooms.Count(); i++)
                        {
                            if (establishmentResults.All(e => e.RoomNumber != i)) return null;
                        }
                    }

                    var establishmentId = coreResult.Key;
                    var firstRoomResult = establishmentResults[0];
                    var establishmentDetail = establishmentDetails[establishmentId];
                    var cheapestRoom = establishmentResults.MinBy(i => i.Price);

                    var cheapestPricePerPerson = availabilityRequest.Rooms.GroupBy(x => x.RoomNumber)
                        .Select(x =>
                        {
                            //get cheapest price for room
                            var minPrice = establishmentResults.Where(r => r.RoomNumber == x.Key).MinBy(i => i.Price).Price;

                            //get the total occupants for room
                            var room = availabilityRequest.Rooms.FirstOrDefault(r => r.RoomNumber == x.Key);
                            if (room == null) return null;

                            //divide the price by occupants
                            return new Money(Math.Round(minPrice.Amount / (room.Adults + room.Children), 2), cheapestRoom.Price.CurrencyCode);
                        })
                        .Where(x => x != null)
                        .MinBy(x => x);

                    TravellerType[] establishmentTravellerTypes = establishmentDetail.TravellerTypeIds.Select(i => travellerTypes[i]).ToArray();
                    if (establishmentTravellerTypes.Length == 0)
                    {
                        // if the array is empty assign null to prevent the item from being added to mongo storage
                        establishmentTravellerTypes = null;
                    }

                    result = new AccommodationB2CAvailabilityResult()
                    {
                        AvailabilityId = availabilityRequest.AvailabilityId.Value,
                        DestinationId = firstRoomResult.DestinationId,
                        EstablishmentId = establishmentId,
                        EstablishmentFacilities = establishmentDetail.FacilityIds.Select(i => facilities[i]).ToArray(),
                        EstablishmentTravellerTypes = establishmentTravellerTypes,
                        Rooms = establishmentResults.Select(i => CreateB2CAvailabilityResultRoom(i, boardTypes, availabilityRequest, aliasNames)).ToArray(),
                        RoomsCount = establishmentResults.Length,
                        RoomsCheapestPrice = cheapestPricePerPerson,
                        RoomsCheapestPricePerNight = cheapestRoom.PricePerNight,
                        CacheExpireDate = establishmentResults.Min(i => i.CacheExpireDate),
                        DestinationReference = establishmentDetail.DestinationReference,
                        IsDestinationDistrict = establishmentDetail.IsDestinationDistrict,
                        DestinationName = establishmentDetail.DestinationName,
                        DestinationSeoFriendlyName = establishmentDetail.DestinationSeoFriendlyName,
                        EstablishmentName = establishmentDetail.EstablishmentName,
                        EstablishmentSeoFriendlyName = establishmentDetail.EstablishmentSeoFriendlyName,
                        EstablishmentDescription = establishmentDetail.EstablishmentDescription,
                        EstablishmentType = (EstablishmentType)Enum.Parse(typeof(EstablishmentType), establishmentDetail.EstablishmentType),
                        EstablishmentStarRating = firstRoomResult.EstablishmentStarRating,
                        EstablishmentTopImageUrl = establishmentDetail.EstablishmentTopImageUrl,
                        EstablishmentReviewCount = establishmentDetail.EstablishmentReviewCount,
                        EstablishmentReviewAverageScore = establishmentDetail.EstablishmentReviewAverageScore,
                        EstablishmentTripAdvisorAverageScore = establishmentDetail.EstablishmentTripAdvisorAverageScore,
                        EstablishmentTripAdvisorReviewCount = establishmentDetail.EstablishmentTripAdvisorReviewCount,
                        EstablishmentTripAdvisorAverageRating = establishmentDetail.EstablishmentTripAdvisorAverageRating,
                        EstablishmentPopularity = establishmentDetail.EstablishmentPopularity,
                        EstablishmentLongitude = (double?)establishmentDetail.EstablishmentLongitude,
                        EstablishmentLatitude = (double?)establishmentDetail.EstablishmentLatitude,
                        CheckInDate = firstRoomResult.ProviderResult.CheckInDate,
                        CheckOutDate = firstRoomResult.ProviderResult.CheckOutDate,
                        RoomOccupancies = establishmentResults.Select(CreateB2CRoomOccupancy).DistinctBy(d => d.RoomNumber).ToArray(),
                    };
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromCoreAvailabilityResults Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreResult.ToIndentedJson());
                }
                return result;
            })
            .Where(i => i != null)
            .ToArray();

            uint id = 1;

            foreach (var result in results)
            {
                foreach (var room in result.Rooms)
                {
                    room.RoomIdAlpha2 = id++;
                }
            }

            return results;
        }

        private AccommodationB2CRoomOccupancy CreateB2CRoomOccupancy(AccommodationAvailabilityResult coreResult)
        {
            return new AccommodationB2CRoomOccupancy
            {
                AdultsCount = coreResult.AdultsCount,
                ChildrenAndInfantsCount = (byte)(coreResult.ChildrenCount + coreResult.InfantsCount),
                RoomNumber = coreResult.RoomNumber
            };
        }

        private AccommodationB2CAvailabilityResultRoom CreateB2CAvailabilityResultRoom(AccommodationAvailabilityResult coreResult, IDictionary<BoardType, Board> boards, AccommodationB2CAvailabilityRequest availabilityRequest, IDictionary<int, SupplierAlias> aliasNames)
        {
            var providerName = coreResult.ProviderResult.ProviderName ?? coreResult.Provider.Name;
            var providerAliasName = providerName;

            if ((aliasNames != null) && (aliasNames.Count > 0))
            {
                var aliasName = aliasNames.Where(x => x.Value.EdiCode.ToUpper().Equals(coreResult.ProviderResult.SupplierEdiCode.ToUpper()) && x.Value.Name.ToUpper().Equals(providerName.ToUpper()))

                 .Select(x => new { key = x.Key, value = x.Value })
                 .ToList();

                if ((aliasName != null) && (aliasName.Count > 0))
                {
                    providerAliasName = aliasName.FirstOrDefault().value.Alias;
                }
            }

            return new AccommodationB2CAvailabilityResultRoom()
            {
                Id = coreResult.Id,

                // Provider details
                ProviderName = coreResult.ProviderResult.ProviderName ?? coreResult.Provider.Name,
                ProviderAliasName = providerAliasName,

                ProviderProviderCode = coreResult.ProviderResult.ProviderEdiCode,
                ProviderEstablishmentCode = coreResult.ProviderResult.ProviderEstablishmentCode ?? coreResult.ProviderResult.EstablishmentEdiCode,

                ProviderEdiCode = coreResult.ProviderResult.ProviderProviderCode ?? coreResult.Provider.EdiCode,
                ProviderFilterName = coreResult.ProviderResult.ProviderFilterName ?? coreResult.Provider.Name,

                ProviderPrice = coreResult.ProviderResult.CostPrice,

                ProviderBoardCode = coreResult.ProviderResult.BoardCode,
                ProviderBoardDescription = coreResult.ProviderResult.BoardDescription,

                ProviderDestinationCode = coreResult.ProviderResult.DestinationEdiCode,
                ProviderContract = coreResult.Provider.ContractDependencyMode == ContractDependencyMode.Dependent ? coreResult.ProviderResult.Contract : null,
                ProviderRoomDescription = coreResult.ProviderResult.RoomDescription,

                ProviderSpecificData = coreResult.ProviderResult.ProviderSpecificData,

                // Supplier
                SupplierEdiCode = coreResult.ProviderResult.SupplierEdiCode,

                RoomDescription = coreResult.RoomDescription,
                RoomNumber = coreResult.RoomNumber,
                ProviderRoomCode = coreResult.ProviderResult.RoomCode,
                BoardDescription = boards[coreResult.BoardType].Name,
                BoardType = coreResult.BoardType,
                ExchangeRate = coreResult.ExchangeRate,
                HighestPrice = coreResult.HighestPrice,
                IsBindingRate = coreResult.ProviderResult.IsBindingRate,
                IsLowestPrice = coreResult.IsLowestPrice,
                IsNonRefundable = coreResult.ProviderResult.IsNonRefundable,
                IsOpaqueRate = coreResult.ProviderResult.IsOpaqueRate,
                Margin = coreResult.Margin,
                PaymentModel = coreResult.ProviderResult.PaymentModel,
                PaymentToTake = coreResult.PaymentToTake,
                Price = coreResult.IsActiveAccommodationAdminFee ? new Money(coreResult.Price.Amount - coreResult.AccommodationAdminFee.Amount, coreResult.Price.CurrencyCode) : coreResult.Price,
                PricePerNight = coreResult.PricePerNight,
                PricePerPerson = new Money((coreResult.Price.Amount / (availabilityRequest.Rooms[coreResult.RoomNumber - 1].Adults + availabilityRequest.Rooms[coreResult.RoomNumber - 1].Children)), coreResult.Price.CurrencyCode),
                RateType = coreResult.ProviderResult.RateType,
                IsDuplicateRate = coreResult.IsDuplicateRate,
                Discount = coreResult.DiscountPrice,
                IsActiveAccommodationAdminFee = coreResult.IsActiveAccommodationAdminFee,
                AccommodationAdminFee = coreResult.AccommodationAdminFee
            };
        }
    }
}