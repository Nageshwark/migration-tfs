﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services
{
	public class B2CEventLogInfo: IB2CEventLogInfo
	{
		private readonly ILogger logger;
		private readonly IAccommodationConfigurationManager configurationManager;
		public B2CEventLogInfo(IAccommodationConfigurationManager configurationManager, ILogger logger)
		{
			this.configurationManager = configurationManager;
			this.logger = logger;
		}
		public void LogTrace(string logInfo)
		{
			if (configurationManager.B2CEventLogInfoIsEnabled)
			{
				logger.Error("MachineName "+Environment.MachineName +", "+  logInfo + ", TimeStamp: " + DateTime.Now);
			}
		}
	}
}
