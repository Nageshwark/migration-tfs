﻿using AlphaRooms.Accommodation.B2C.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CAvailabilityFilterCriteriaFactory : IB2CAvailabilityFilterCriteriaFactory
    {
        static B2CAvailabilityFilterCriteriaFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CAvailabilityFilter, AccommodationB2CAvailabilityFilter>();
            }
        }

        public AccommodationB2CAvailabilityFilter CreateWithoutFilterOptions(AccommodationB2CAvailabilityFilter filterCriteria)
        {
            var result = Mapper.Map<AccommodationB2CAvailabilityFilter, AccommodationB2CAvailabilityFilter>(filterCriteria);
            result.BoardTypes = null;
            result.Districts = null;
            result.FacilityIds = null;
            result.PaymentTypes = null;
            result.ProviderEdiCodes = null;
            result.StarRatings = null;
            result.TravellerTypeIds = null;
            result.PageNumber = null;
            result.PageSize = null;
            return result;
        }
    }
}
