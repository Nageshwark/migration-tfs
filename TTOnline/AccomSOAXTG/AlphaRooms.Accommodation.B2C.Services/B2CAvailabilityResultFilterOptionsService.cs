﻿using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.SOACommon.DomainModels;
    using AlphaRooms.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class B2CAvailabilityResultFilterOptionsService : IB2CAvailabilityResultFilterOptionsService
    {
        private readonly IB2CPaymentModelConverter paymentModelConverter;
        private readonly IBoardRepository boardRepository;
        private readonly IPaymentTypeRepository paymentTypeRepository;
        private readonly IStarRatingRepository starRatingRepository;

        public B2CAvailabilityResultFilterOptionsService(IB2CPaymentModelConverter paymentModelConverter, IBoardRepository boardRepository, IPaymentTypeRepository paymentTypeRepository
            , IStarRatingRepository starRatingRepository)
        {
            this.paymentModelConverter = paymentModelConverter;
            this.boardRepository = boardRepository;
            this.paymentTypeRepository = paymentTypeRepository;
            this.starRatingRepository = starRatingRepository;
        }

        public AccommodationB2CAvailabilityResultsFilterOptions CreateEmptyFilterOptions(AccommodationB2CAvailabilityRequest availabilityRequest, ChannelInfo channelInfo)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptions()
            {
                PricePerNightMin = null,
                PricePerNightMax = null,
                PricePerPersonMin = null,
                PricePerPersonMax = null,
                Boards = new AccommodationB2CAvailabilityResultsFilterOptionBoardType[0],
                Facilities = new AccommodationB2CAvailabilityResultsFilterOptionItem[0],
                PaymentTypes = new AccommodationB2CAvailabilityResultsFilterOptionPaymentType[0],
                Providers = new AccommodationB2CAvailabilityResultsFilterOptionProvider[0],
                TravellerTypes = new AccommodationB2CAvailabilityResultsFilterOptionItem[0],
                StarRatings = new AccommodationB2CAvailabilityResultsFilterOptionStarRating[0],
                Districts = new AccommodationB2CAvailabilityResultsFilterOptionItem[0],
                HasDepositOption = false
            };
        }

        public async Task<AccommodationB2CAvailabilityResultsFilterOptions> CreateResultsFilterOptionsAsync(
            AccommodationB2CAvailabilityRequest availabilityRequest,
            ChannelInfo channelInfo,
            IList<AccommodationB2CAvailabilityResult> results)
        {
            var boards = await this.boardRepository.GetAllDictionaryAsync();
            var paymentTypes = await paymentTypeRepository.GetAllDictionaryAsync();
            var starRatings = await this.starRatingRepository.GetAllDictionaryAsync();
            IEnumerable<AccommodationB2CAvailabilityResultRoom> allRooms = results.SelectMany(i => i.Rooms).ToArray();
            IEnumerable<AccommodationB2CAvailabilityResultRoom> lowestPriceRooms;

            var prices = results.Select(x => new { CheapestPerNight = x.RoomsCheapestPricePerNight, CheapestPricePerPerson = x.RoomsCheapestPrice });

            if (availabilityRequest.IgnoreConsolidation)
            {
                // if consolidation has not been done, there are no cheapest rooms so use all rooms, as all rooms will be shown
                lowestPriceRooms = allRooms.ToArray();
            }
            else
            {
                lowestPriceRooms = allRooms.Where(i => i.IsLowestPrice).ToArray();
            }

            var pricePerPersonMin = prices.Min(y => new Money(Math.Floor(y.CheapestPricePerPerson.Amount), y.CheapestPricePerPerson.CurrencyCode));
            var pricePerPersonMax = prices.Max(y => new Money(Math.Ceiling(y.CheapestPricePerPerson.Amount), y.CheapestPricePerPerson.CurrencyCode));
            var pricePerNightMin = prices.Min(x => new Money(Math.Floor(x.CheapestPerNight.Amount), x.CheapestPerNight.CurrencyCode));
            var pricePerNightMax = prices.Max(y => new Money(Math.Ceiling(y.CheapestPerNight.Amount), y.CheapestPerNight.CurrencyCode));

            //sort rooms by prices so we can use these to calculate medians
            //pick only the cheapest price per person rooms for each hotel and sort them by price
            var sortedPrices = prices.Select(x => x.CheapestPricePerPerson.Amount).OrderBy(x => x).ToArray();

            //pick only the cheapest price per night rooms for each hotel and sort then by the price
            var sortedPerNight = prices.Select(x => x.CheapestPerNight.Amount).OrderBy(x => x).ToArray();

            return new AccommodationB2CAvailabilityResultsFilterOptions()
            {
                PricePerNightMin = pricePerNightMin,
                PricePerNightMax = pricePerNightMax,
                PricePerPersonMin = pricePerPersonMin,
                PricePerPersonMax = pricePerPersonMax,
                Boards = lowestPriceRooms.Select(j => j.BoardType).Distinct().Select(i => CreateFilterOptionBoard(i, boards)).OrderBy(i => i.Name).ToArray(),
                Facilities = results.SelectMany(i => i.EstablishmentFacilities).DistinctBy(i => i.Name).Select(i => CreateFilterOptionFacility(i)).OrderBy(i => i.Name).ToArray(),
                PaymentTypes = lowestPriceRooms.Select(j => j.PaymentModel).Select(i => paymentModelConverter.ToPaymentType(i)).Distinct().Select(i => CreateFilterOptionPaymentType(i, paymentTypes)).OrderBy(i => i.Name).ToArray(),
                Providers = allRooms.DistinctBy(i => i.ProviderEdiCode).Select(i => CreateFilterOptionProvider(i)).OrderBy(i => i.ProviderName).ToArray(),
                TravellerTypes = results.Where(i => i.EstablishmentTravellerTypes != null).SelectMany(i => i.EstablishmentTravellerTypes).DistinctBy(i => i.Name).Select(i => CreateFilterOptionTravellerType(i)).OrderBy(i => i.Name).ToArray(),
                StarRatings = results.Select(i => i.EstablishmentStarRating).Distinct().Select(i => CreateFilterOptionStarRatings(i, starRatings)).OrderBy(i => i.Name).ToArray(),
                EstablishmentNames = results.Select(i => i.EstablishmentName).Distinct().OrderBy(i => i).ToArray(),
                Districts = results.Where(i => i.DestinationId != availabilityRequest.DestinationId && i.IsDestinationDistrict).DistinctBy(i => i.DestinationId).Select(i => CreateFilterOptionDistrict(i)).OrderBy(i => i.Name).ToArray(),
                HasDepositOption = results.Any(i => i.Rooms.Any(r => r.IsEligibleForDeposit)),
                PricePerNightRange = CreateRange(sortedPerNight, pricePerNightMin.Amount, pricePerNightMax.Amount),
                PricePerPersonRange = CreateRange(sortedPrices, pricePerPersonMin.Amount, pricePerPersonMax.Amount)
            };
        }

        public B2CResultsRangeFilterOptions CreateRange(decimal[] prices, decimal? minPrice = null, decimal? maxPrice = null)
        {
            if (prices == null || prices.Length == 0)
                return new B2CResultsRangeFilterOptions()
                {
                    RangeMin = 0,
                    RangeMax = 0
                };

            var minPriceValue = minPrice.HasValue ? minPrice.Value : prices[0];
            var maxPriceValue = maxPrice.HasValue ? maxPrice.Value : prices.Last();

            //check get all unique
            var uniquePrices = prices.Distinct().ToArray();
            if (uniquePrices.Length <= 2)
                return new B2CResultsRangeFilterOptions()
                {
                    RangeMin = minPriceValue,
                    RangeMax = maxPriceValue
                };

            //calculate medians at 25%, 50% and 75%s
            //get 50% median
            int medianPosition;
            var segment50 = Median(prices, out medianPosition);
            int absoluteMedianPosition = medianPosition;

            var total = prices.Length;
            //get lower part of median
            var lowerPrices = prices.Take(absoluteMedianPosition + 1).ToArray();
            //find the median of lower part
            var segment25 = Median(lowerPrices, out medianPosition);

            //get higher part of median
            var higherPrices = prices.Skip(absoluteMedianPosition).Take(total - absoluteMedianPosition).ToArray();
            //find the median of higher part
            var segment75 = Median(higherPrices, out medianPosition);

            return new B2CResultsRangeFilterOptions()
            {
                RangeMin = minPriceValue,
                RangeMax = maxPriceValue,
                QuarterSegment = minPriceValue != segment25 && segment25 != segment50 ? segment25 : (decimal?)null,
                HalfSegment = segment50,
                ThreeQuarterSegment = maxPriceValue != segment75 && segment75 != segment50 ? segment75 : (decimal?)null
            };
        }

        private decimal Median(decimal[] xs, out int position)
        {
            var mid = (xs.Length - 1) / 2.0;
            position = (int)Math.Round(mid);
            return (xs[(int)(mid)] + xs[(int)(mid + 0.5)]) / 2;
        }

        private AccommodationB2CAvailabilityResultsFilterOptionBoardType CreateFilterOptionBoard(BoardType boardType, Dictionary<BoardType, Board> boards)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionBoardType() { BoardType = boardType, Name = boards[boardType].Name };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionPaymentType CreateFilterOptionPaymentType(PaymentTypeType paymentType, Dictionary<PaymentTypeType, PaymentType> paymentTypes)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionPaymentType() { PaymentType = paymentType, Name = paymentTypes[paymentType].Name };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionStarRating CreateFilterOptionStarRatings(StarRatingType starRating, Dictionary<StarRatingType, StarRating> starRatings)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionStarRating() { StarRating = starRating, Name = starRatings[starRating].Name };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionItem CreateFilterOptionTravellerType(TravellerType travellerType)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionItem() { Id = travellerType.Id, Name = travellerType.Name };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionItem CreateFilterOptionFacility(Facility facility)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionItem() { Id = facility.Reference, Name = facility.Name };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionProvider CreateFilterOptionProvider(AccommodationB2CAvailabilityResultRoom roomResult)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionProvider() { ProvideEdiCode = roomResult.ProviderEdiCode, ProviderName = roomResult.ProviderFilterName };
        }

        private AccommodationB2CAvailabilityResultsFilterOptionItem CreateFilterOptionDistrict(AccommodationB2CAvailabilityResult i)
        {
            return new AccommodationB2CAvailabilityResultsFilterOptionItem() { Id = i.DestinationReference, Name = i.DestinationName };
        }

        public AccommodationB2CAvailabilityFilterOptionDetails CreateResultsFilterDetails(AccommodationB2CAvailabilityResultsFilterOptions filterOptions, AccommodationB2CAvailabilityFilter filterCriteria
            , AccommodationB2CAvailabilityResult[] accommodationResults)
        {
            var filters = Sync.RunMany(
                () => CreateFilterOptionDetails(filterCriteria, filterOptions.Districts.Select(i => i.Id).ToArray(), FilterOptions.District, i => i.ToString(), accommodationResults
                    , (i, j) => i.EstablishmentResult.DestinationReference == j)
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.Facilities.Select(i => i.Id).ToArray(), FilterOptions.Facilities, i => i.ToString(), accommodationResults
                    , (i, j) => i.EstablishmentResult.EstablishmentFacilities.Any(k => k.Reference == j))
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.StarRatings.Select(i => i.StarRating).ToArray(), FilterOptions.StarRatings, i => ((int)i).ToString(), accommodationResults
                    , (i, j) => i.EstablishmentResult.EstablishmentStarRating == j)
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.TravellerTypes.Select(i => i.Id).ToArray(), FilterOptions.Recommendedfor, i => i.ToString(), accommodationResults
                    , (i, j) => i.EstablishmentResult.EstablishmentTravellerTypes.Any(k => k.Id == j))
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.Boards.Select(i => i.BoardType).ToArray(), FilterOptions.Board, i => ((int)i).ToString(), accommodationResults
                    , (i, j) => i.BoardType == j)
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.PaymentTypes.Select(i => i.PaymentType).ToArray(), FilterOptions.Payment, i => ((int)i).ToString(), accommodationResults
                    , (i, j) => paymentModelConverter.ToPaymentModels(j).Any(k => i.PaymentModel == k))
                , () => CreateFilterOptionDetails(filterCriteria, filterOptions.Providers.Select(i => i.ProvideEdiCode).ToArray(), FilterOptions.Suppliers, i => i.ToString(), accommodationResults
                    , (i, j) => i.ProviderEdiCode == j)
                );
            return new AccommodationB2CAvailabilityFilterOptionDetails() { FilterDetails = filters.ToArray() };
        }

        private IEnumerable<AccommodationB2CAvailabilityFilterOptionDetailsItem> CreateFilterOptionDetails<T>(AccommodationB2CAvailabilityFilter filterCriteria, T[] values, FilterOptions filterType
            , Func<T, string> filterIdFunc, AccommodationB2CAvailabilityResult[] accommodationResults, Func<AccommodationB2CAvailabilityResultRoom, T, bool> roomFunc)
        {
            var filteredRooms = accommodationResults.AsQueryable().Where(CreateEstablishmentAccommodationFilter(filterCriteria, filterType)).SelectMany(i => i.Rooms)
                .Where(CreateRoomAccommodationFilter(filterCriteria, filterType)).ToArray();
            return values.Select(i => new AccommodationB2CAvailabilityFilterOptionDetailsItem() { Section = filterType.ToString(), FilterId = filterIdFunc.Invoke(i), ResultsCount = filteredRooms.Count(j => roomFunc.Invoke(j, i)) });
        }

        public Expression<Func<AccommodationB2CAvailabilityResult, bool>> CreateEstablishmentAccommodationFilter(AccommodationB2CAvailabilityFilter filterCriteria, FilterOptions exceptFilter)
        {
            var query = LinqExpression.CreateTrue<AccommodationB2CAvailabilityResult>();
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.Districts) && exceptFilter != FilterOptions.District)
            {
                query = query.And(i => filterCriteria.Districts.Contains(i.DestinationReference));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.FacilityIds) && exceptFilter != FilterOptions.Facilities)
            {
                filterCriteria.FacilityIds.ForEach(i => query = query.And(j => j.EstablishmentFacilities.Any(k => k.Reference == i)));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.StarRatings) && exceptFilter != FilterOptions.StarRatings)
            {
                query = query.And(i => filterCriteria.StarRatings.Contains(i.EstablishmentStarRating));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.TravellerTypeIds) && exceptFilter != FilterOptions.Recommendedfor)
            {
                query = query.And(i => i.EstablishmentTravellerTypes.Any(j => filterCriteria.TravellerTypeIds.Contains(j.Id)));
            }
            return query;
        }

        public Expression<Func<AccommodationB2CAvailabilityResultRoom, bool>> CreateRoomAccommodationFilter(AccommodationB2CAvailabilityFilter filterCriteria, FilterOptions exceptFilter)
        {
            var query = LinqExpression.CreateTrue<AccommodationB2CAvailabilityResultRoom>();
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.BoardTypes) && exceptFilter != FilterOptions.Board)
            {
                query = query.And(i => filterCriteria.BoardTypes.Contains(i.BoardType));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.PaymentTypes) && exceptFilter != FilterOptions.Payment)
            {
                var paymentModels = filterCriteria.PaymentTypes.SelectMany(i => paymentModelConverter.ToPaymentModels(i)).ToArray();
                query = query.And(i => paymentModels.Contains(i.PaymentModel));
            }
            if (!ArrayModule.IsNullOrEmpty(filterCriteria.ProviderEdiCodes) && exceptFilter != FilterOptions.Suppliers)
            {
                query = query.And(i => filterCriteria.ProviderEdiCodes.Contains(i.ProviderEdiCode));
            }
            return query;
        }
    }
}