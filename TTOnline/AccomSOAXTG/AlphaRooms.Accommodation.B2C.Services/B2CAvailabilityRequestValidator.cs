﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Utilities.CustomExceptions;
    using AlphaRooms.SOACommon.Interfaces;
    using System;

    public class B2CAvailabilityRequestValidator : IB2CAvailabilityRequestValidator
    {
        private readonly IAccommodationConfigurationManager configurationManager;

        public B2CAvailabilityRequestValidator(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public void ValidateAvailabilityRequest(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            if (availabilityRequest.DestinationId == Guid.Empty && availabilityRequest.EstablishmentId == Guid.Empty)
            {
                throw new ValidationException("ValidateAvailabilityRequest failed: the request must have either DestinationId or EstablishmentId.");
            }
            if (availabilityRequest.CheckInDate == DateTime.MinValue)
            { 
                throw new ValidationException("ValidateAvailabilityRequest failed: CheckInDate is required.");
            }
            if (availabilityRequest.CheckOutDate == DateTime.MinValue)
            {
                throw new ValidationException("ValidateAvailabilityRequest failed: CheckOutDate is required.");
            }
            if (availabilityRequest.Rooms == null || availabilityRequest.Rooms.Length < 1 || availabilityRequest.Rooms.Length > 5)
            { 
                throw new ValidationException("ValidateAvailabilityRequest failed: Room occupancy must be provided for between 1 and 5 rooms.");
            }
            if (availabilityRequest.TC4Origin == true && (availabilityRequest.CheckInDate - DateTime.Today).TotalDays < this.configurationManager.AccommodationB2CXMLGlobalLeadTime)
            {
                throw new ValidationException("ValidateAvailabilityRequest failed: AccommodationB2CXMLGlobalLeadTime");
            }
        }
    }
}
