﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CBookingService : IB2CBookingService
    {
        private readonly IB2CRequestStatusService requestStatusService;
        private readonly ILogger logger;
        private readonly IB2CBookingRequestFactory requestFactory;
        private readonly IBookingService coreBookingService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2CBookingResultMappingService resultMappingService;
        private readonly IB2CBookingRequestValidator requestValidator;
        private readonly IB2CBookingLogger bookingLogger;

        public B2CBookingService(IB2CRequestStatusService requestStatusService, ILogger logger, IB2CBookingRequestFactory requestFactory
            , IBookingService coreBookingService, IChannelInfoService channelInfoService, IB2CBookingResultMappingService resultMappingService
            , IB2CBookingRequestValidator requestValidator, IB2CBookingLogger bookingLogger)
        {
            this.requestStatusService = requestStatusService;
            this.logger = logger;
            this.requestFactory = requestFactory;
            this.coreBookingService = coreBookingService;
            this.channelInfoService = channelInfoService;
            this.resultMappingService = resultMappingService;
            this.requestValidator = requestValidator;
            this.bookingLogger = bookingLogger;
        }

        public async Task<Guid> StartBookingAsync(AccommodationB2CBookingRequest bookingRequest)
        {
            // create a booking id if the caller didn't provide one
            if (bookingRequest.BookingId == null) bookingRequest.BookingId = Guid.NewGuid();

            // Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateBookingRequestStatus(bookingRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            // start the booking process
            #pragma warning disable 4014
            Task.Run(async() => await StartBookingProcessAsync(bookingRequest, requestStatus));
            #pragma warning restore 4014
            return bookingRequest.BookingId.Value;
        }

        public async Task<AccommodationB2CBookingResponse> GetBookingResponseAsync(Guid bookingId)
        {
            AccommodationB2CBookingRequest bookingRequest = null;
            AccommodationB2CRequestStatus requestStatus = null;
            try 
            {
                // get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(bookingId);
                                
                // throw exception when request status is not available
                if (requestStatus == null) new AccommodationB2CBookingResponse() { BookingId = bookingId, BookingStatus = Status.NotFound };

                // if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2CBookingResponse() { BookingId = bookingId, BookingStatus = requestStatus.Status };

                // get valuation request
                bookingRequest = requestStatus.BookingRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, bookingId.ToString(), "(accommodation)", bookingRequest.Debugging, logger))
                {
                    // perform a booking search
                    var coreBookingResponse = await coreBookingService.GetBookingResponseAsync(bookingId);
                    
                    // map the core results
                    var bookingResult = resultMappingService.MapFromCoreBookingResults(bookingRequest, coreBookingResponse.Result);

                    return new AccommodationB2CBookingResponse
                    {
                        BookingId = bookingId
                        , BookingStatus = requestStatus.Status
                        , BookingResult = bookingResult
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetBookingResponse Error: search [{0}] {1}\n\n{2}", bookingId, ex.GetDetailedMessageWithInnerExceptions(), bookingRequest.ToIndentedJson());
                return new AccommodationB2CBookingResponse { BookingId = bookingId, BookingStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        private async Task StartBookingProcessAsync(AccommodationB2CBookingRequest bookingRequest, AccommodationB2CRequestStatus requestStatus)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, bookingRequest.BookingId.ToString(), "(accommodation)", bookingRequest.Debugging, logger))
	        {
                var searchStarted = DateTime.Now;
                AccommodationBookingResponse coreBookingResponse = null; 
                Exception exception = null;
                try
                {
                    // get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(bookingRequest.Channel);

                    // update Mongo with inprogress status
                    await requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Booking Started");
                    
                    // Validate Request
                    requestValidator.ValidateBookingRequest(bookingRequest);
                                        
                    // create a core booking request
                    var coreBookingRequest = requestFactory.CreateBookingRequest(bookingRequest, channelInfo);

                    // perform a availability search
                    coreBookingResponse = await coreBookingService.ProcessBookingAsync(coreBookingRequest);
                    
                    // check if valuation failed
                    if (coreBookingResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        requestStatus.Status = Status.Successful;
                        requestStatus.Message = "Accommodation Booking Completed";
                    }
                    else
                    {
                        requestStatus.Status = Status.Failed;
                        requestStatus.Message = "Accommodation Provider Booking Failed";
                    }
                }
                catch (Exception ex)
                {
                    // update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation Booking Error: search [{0}] {1}\n\n{2}", bookingRequest.BookingId, ex.GetDetailedMessageWithInnerExceptions(), bookingRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                await bookingLogger.LogAsync(bookingRequest, requestStatus, coreBookingResponse, DateTime.Now - searchStarted, exception);
            }
        }
    }
}
