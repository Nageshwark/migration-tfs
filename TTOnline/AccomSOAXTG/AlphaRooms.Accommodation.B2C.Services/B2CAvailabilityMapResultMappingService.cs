﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Utilities;
    using AutoMapper;

    public class B2CAvailabilityMapResultMappingService : IB2CAvailabilityMapResultMappingService
    {
        static B2CAvailabilityMapResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2CAvailabilityResult, AccommodationB2CAvailabilityMapResult>();
            }
        }

        public IList<AccommodationB2CAvailabilityMapResult> MapFromCoreAvailabilityResultsAsync(AccommodationB2CAvailabilityRequest availabilityRequest, IList<AccommodationB2CAvailabilityResult> results)
        {
            return results.Select(i => CreateAccommodationB2CAvailabilityResult(i)).ToArray();
        }

        private AccommodationB2CAvailabilityMapResult CreateAccommodationB2CAvailabilityResult(AccommodationB2CAvailabilityResult result)
        {
            return Mapper.Map<AccommodationB2CAvailabilityResult, AccommodationB2CAvailabilityMapResult>(result);
        }
    }
}
