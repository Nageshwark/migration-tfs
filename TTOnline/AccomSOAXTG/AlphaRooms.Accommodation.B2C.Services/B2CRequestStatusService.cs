﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using AlphaRooms.Accommodation.B2C.Contracts;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Accommodation.B2C.Interfaces.Caching;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using System;
    using System.Threading.Tasks;

    public class B2CRequestStatusService : IB2CRequestStatusService
    {
        private readonly IB2CRequestStatusCaching requestStatusCaching;

        public B2CRequestStatusService(IB2CRequestStatusCaching requestStatusCaching)
        {
            this.requestStatusCaching = requestStatusCaching;
        }

        public AccommodationB2CRequestStatus CreateAvailabilityRequestStatus(AccommodationB2CAvailabilityRequest availabilityRequest)
        {
            var availabilityRequestStatus = CreateRequestStatus(availabilityRequest.AvailabilityId.Value);
            availabilityRequestStatus.AvailabilityRequest = availabilityRequest;
            availabilityRequestStatus.RequestType = RequestType.Availability;
            return availabilityRequestStatus;
        }

        public AccommodationB2CRequestStatus CreateValuationRequestStatus(AccommodationB2CValuationRequest valuationRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(valuationRequest.ValuationId.Value);
            valuationRequestStatus.ValuationRequest = valuationRequest;
            valuationRequestStatus.RequestType = RequestType.Valuation;
            return valuationRequestStatus;
        }

        public AccommodationB2CRequestStatus CreateBookingRequestStatus(AccommodationB2CBookingRequest bookingRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(bookingRequest.BookingId.Value);
            valuationRequestStatus.BookingRequest = bookingRequest;
            valuationRequestStatus.RequestType = RequestType.Booking;
            return valuationRequestStatus;
        }
        
        public AccommodationB2CRequestStatus CreateCancellationRequestStatus(AccommodationB2CCancellationRequest cancellationRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(cancellationRequest.CancellationId.Value);
            valuationRequestStatus.CancellationRequest = cancellationRequest;
            valuationRequestStatus.RequestType = RequestType.Cancellation;
            return valuationRequestStatus;
        }

        private AccommodationB2CRequestStatus CreateRequestStatus(Guid Id)
        {
            return new AccommodationB2CRequestStatus()
            {
                Id = Id
                , StartDate = DateTime.Now
                , Status = Status.NotStarted
                , Message = "Not Started"
            };
        }

        public async Task UpdateRequestStatus(AccommodationB2CRequestStatus requestStatus, Status status, string message)
        {
            requestStatus.Status = status;
            requestStatus.Message = message;
            await SaveRequestStatusAsync(requestStatus);
        }


        public async Task<AccommodationB2CRequestStatus> GetRequestStatusOrNullByIdAsync(Guid id)
        {
            return await requestStatusCaching.GetByIdOrNullAsync(id);
        }

        public async Task<AccommodationB2CRequestStatus> GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(string availabilityKey, Guid? id)
        {
            return await requestStatusCaching.GetSuccessfulByAvailabilityKeyOrNullAsync(availabilityKey,id);
        }

		public DateTime CreateResultsCacheExpireDate()
        {
            return requestStatusCaching.CreateResultsCacheExpireDate();
        }

        public bool IsResultsExpireDateExpired(AccommodationB2CRequestStatus cachedRequestStatus)
        {
            return requestStatusCaching.IsResultsExpireDateExpired(cachedRequestStatus);
        }

        public async Task SaveRequestStatusAsync(AccommodationB2CRequestStatus requestStatus)
        {
            await requestStatusCaching.SaveAsync(requestStatus);
        }

		public async Task<AccommodationB2CRequestStatus> GetSuccessfulRequestStatusByMetaTokenKeyOrNullAsync(string metaTokenKey)
		{
			return await requestStatusCaching.GetSuccessfulByMetaTokenKeyOrNullAsync(metaTokenKey);
		}
	}
}
