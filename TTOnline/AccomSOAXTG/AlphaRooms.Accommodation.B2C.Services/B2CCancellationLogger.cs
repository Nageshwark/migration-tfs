﻿using AlphaRooms.Accommodation.B2C.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CCancellationLogger : IB2CCancellationLogger
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public async Task LogAsync(AccommodationB2CCancellationRequest cancellationRequest, AccommodationB2CRequestStatus requestStatus, AccommodationCancellationResponse coreCancellationResponse
            , TimeSpan timeSpan, Exception exception)
        {
            CreatePerformanceLogs(cancellationRequest, requestStatus, coreCancellationResponse, timeSpan);
            // TODO Implement cancellation logger
        }

        private void CreatePerformanceLogs(AccommodationB2CCancellationRequest cancellationRequest, AccommodationB2CRequestStatus requestStatus, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeTaken)
        {
            var resultCount = coreCancellationResponse?.Result?.Rooms.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Cancellation",
                RequestIdentifier = cancellationRequest.ItineraryId.ToString(),
                Channel = cancellationRequest.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus.Status == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();
            var providerPerfLogs = coreCancellationResponse?.ProcessResponseDetails.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.IsSuccess = d.Exception == null;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }
    }
}
