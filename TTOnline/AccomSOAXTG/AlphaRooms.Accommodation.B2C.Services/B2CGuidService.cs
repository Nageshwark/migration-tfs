﻿namespace AlphaRooms.Accommodation.B2C.Services
{
    using System;
    using AlphaRooms.Accommodation.B2C.Interfaces;
    using AlphaRooms.Utilities;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Globalization;


    public class B2CGuidService : IB2CGuidService
    {
        private const string defaultHexSplit = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f";
        private const string HexSplitMeta = "HexSplitMeta";
        private const string HexSplitWeb = "HexSplitWeb";
        private const char HexSeparator = ',';
        private readonly string[] WebHex;
        private readonly string[] MetaHex;

        private static readonly RoundRobin<byte> MetaRobin = new RoundRobin<byte>(GetValidChars(HexSplitMeta));
        private static readonly RoundRobin<byte> WebRobin = new RoundRobin<byte>(GetValidChars(HexSplitWeb));

        // private static readonly string BatmanRobin = http://www.themarysue.com/wp-content/uploads/2016/08/Batman-and-Robin.jpg

        public B2CGuidService()
        {
            this.WebHex = GetArrayChars(HexSplitWeb);
            this.MetaHex = GetArrayChars(HexSplitMeta);
        }

        public Guid GenerateGuid(bool IsMeta)
        {
            RoundRobin<byte> validFirstChars;
            if (IsMeta)
            {
                validFirstChars = MetaRobin;
            }
            else
            {
                validFirstChars = WebRobin;
            }

            var searchGuid = Guid.NewGuid();

            if (validFirstChars == null)
            {
                return searchGuid;
            }

            var bytes = searchGuid.ToByteArray();

            var newNibble = validFirstChars.GetValue();

            // Clear out upper nibble and replace with new, whilst preserving lower nibble
            bytes[3] = (byte)((bytes[3] & 0x0f) | (newNibble << 4));
            return new Guid(bytes);
        }

        private static IList<Byte> GetValidChars(string source)
        {

            var validCharString = ConfigurationManager.AppSettings[source];
            if (validCharString == null)
            {
                validCharString = defaultHexSplit;
            }
            return ParseHexString(validCharString).ToList();
        }

        private string[] GetArrayChars(string source)
        {
            var validCharString = ConfigurationManager.AppSettings[source];
            if (validCharString != null)
            {
                return validCharString.Split(HexSeparator);
            }
            else
            {
                return GetDefaultArrayChar(source);
            }
        }

        public bool IsMetaGuid(Guid? guid)
        {
            if (guid == null || this.MetaHex == null)
            {
                return false;
            }
            else
            {
                return this.MetaHex.Contains(guid.ToString()[0].ToString());
            }
        }

        private string[] GetDefaultArrayChar(string source)
        {
            if (source.Equals(HexSplitMeta))
            {
                return null;
            }
            return defaultHexSplit.Split(HexSeparator);
        }

        private static IEnumerable<byte> ParseHexString(string hexString)
        {
            if (string.IsNullOrEmpty(hexString))
            {
                yield break;
            }

            foreach (var c in hexString)
            {
                byte b;
                if (byte.TryParse(c.ToString(), NumberStyles.HexNumber, null, out b))
                {
                    yield return b;
                }
            }
        }

    }
}
