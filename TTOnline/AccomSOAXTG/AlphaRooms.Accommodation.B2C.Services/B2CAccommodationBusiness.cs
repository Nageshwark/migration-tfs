﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Services;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CAccommodationBusiness : IAccommodationBusiness
    {
        private readonly IAvailabilityResultLowerPriceService resultLowerPriceService;
        private readonly IAvailabilityResultConsolidationService resultConsolidationService;
        private readonly IAvailabilityResultMarkupService resultMarkupService;
        private readonly IAvailabilityFlashSaleService flashSaleService;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;
        private readonly IPriceCalculatorService priceCalculatorService;

        public B2CAccommodationBusiness(IPriceCalculatorService priceCalculatorService, AccommodationPromotionalDiscountService promotionalDiscountService, IAvailabilityFlashSaleService flashSaleService, IAvailabilityResultMarkupService resultMarkupService, IAvailabilityResultConsolidationService resultConsolidationService, IAvailabilityResultLowerPriceService resultLowerPriceService)
        {
            this.priceCalculatorService = priceCalculatorService;
            this.promotionalDiscountService = promotionalDiscountService;
            this.flashSaleService = flashSaleService;
            this.resultMarkupService = resultMarkupService;
            this.resultConsolidationService = resultConsolidationService;
            this.resultLowerPriceService = resultLowerPriceService;
        }

        public void FilterBySupplier(string[] suppliersToSearch, B2BUser agent, AccommodationAvailabilityResponse coreAvailabilityResponse)
        {
            if (suppliersToSearch != null && suppliersToSearch.Any())
            {
                //Filter Suppliers and Providers  - because of Bev5/DB complications we must check both providers and suppliers 
                var results = coreAvailabilityResponse.Results;
                var supplierResults = results.Where(r => suppliersToSearch.Contains(r.Supplier.EdiCode));
                var providerResults = results.Where(r => suppliersToSearch.Contains(r.Provider.EdiCode));

                coreAvailabilityResponse.Results = supplierResults.Union(providerResults).ToArray();
            }
        }

        public async Task<IList<AccommodationAvailabilityResult>> PostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
           
            // filter payDirect nonRefundable - 
            //TODO: THIS IS A HACK. NEED TO REMOVE FROM HERE AND MOVE TO PROVIDERS
            results = results.Where(i => i.ProviderResult.PaymentModel != SOACommon.Contracts.Enumerators.PaymentModel.CustomerPayDirect
                                                    || i.ProviderResult.IsNonRefundable == false).ToArray();

            // filter results for flash sale
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_FilterFlashSale");
            results = await flashSaleService.FilterResultsForFlashSaleAsync(availabilityRequest, results);
            timer.Record();

            // apply markup 
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Markups");
            results = await resultMarkupService.ApplyAvailabilityMarkupAsync(availabilityRequest, results);
            timer.Record();

            // process room switching
            //results = roomSwitchingService.ApplyAvailabilityRoomSwitching(availabilityRequest, results);

            // process price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Pricing");
            results = priceCalculatorService.ApplyAvailabilityPriceCalculation(availabilityRequest, results);
            timer.Record();

			//Apply promotional discounts
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Promotions");
			promotionalDiscountService.ProcessDiscounts(results, availabilityRequest.Voucher, availabilityRequest.ChannelInfo.Channel);
			timer.Record();

			// consolidate the accommodation results and highest price
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Consolidate");
			results = resultConsolidationService.ConsolidateAvailabilityResults(availabilityRequest, results);
			timer.Record();

			// identify the lower price rooms
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_LowerPrice");
			results = resultLowerPriceService.ApplyAvailabilityLowerPrice(results);
			timer.Record();

			return results.ToArray();
        }

        public IList<AccommodationAvailabilityResult> TravelGatePostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_FilterFlashSale");

            // process price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Pricing");
            results = priceCalculatorService.ApplyAvailabilityPriceCalculation(availabilityRequest, results);
            timer.Record();

			//Apply promotional discounts
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Promotions");
			promotionalDiscountService.ProcessDiscounts(results, availabilityRequest.Voucher, availabilityRequest.ChannelInfo.Channel);
			timer.Record();

			// consolidate the accommodation results and highest price
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Consolidate");
			results = resultConsolidationService.ConsolidateAvailabilityResults(availabilityRequest, results);
			timer.Record();

			// identify the lower price rooms
			timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_LowerPrice");
			results = resultLowerPriceService.ApplyAvailabilityLowerPrice(results);
			timer.Record();

			return results.ToArray();
        }
    }
}
