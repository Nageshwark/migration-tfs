﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CBookingRequestValidator : IB2CBookingRequestValidator
    {
        public void ValidateBookingRequest(AccommodationB2CBookingRequest bookingRequest)
        {
            if (bookingRequest.ValuationId == Guid.Empty)
                throw new ValidationException("ValidateBookingRequest failed: the request must have ValuationId.");
            if (bookingRequest.ItineraryId == 0)
                throw new ValidationException("ValidateBookingRequest failed: the request must have ItineraryId.");
            if (bookingRequest.Customer == null)
                throw new ValidationException("ValidateBookingRequest failed: the request must have customer details.");
            if (ArrayModule.IsNullOrEmpty(bookingRequest.ValuatedRooms))
                throw new ValidationException("ValidateBookingRequest failed: the request must have valuated room details.");
        }
    }
}
