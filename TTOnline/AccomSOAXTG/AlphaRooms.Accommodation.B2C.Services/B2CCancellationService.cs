﻿using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.B2C.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CCancellationService : IB2CCancellationService
    {
        private readonly IB2CRequestStatusService requestStatusService;
        private readonly ILogger logger;
        private readonly IB2CCancellationRequestFactory requestFactory;
        private readonly ICancellationService coreCancellationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2CCancellationResultMappingService resultMappingService;
        private readonly IB2CCancellationRequestValidator requestValidator;
        private readonly IB2CCancellationLogger cancellationLogger;

        public B2CCancellationService(IB2CRequestStatusService requestStatusService, ILogger logger, IB2CCancellationRequestFactory requestFactory
            , ICancellationService coreCancellationService, IChannelInfoService channelInfoService, IB2CCancellationResultMappingService resultMappingService
            , IB2CCancellationRequestValidator requestValidator, IB2CCancellationLogger cancellationLogger)
        {
            this.requestStatusService = requestStatusService;
            this.logger = logger;
            this.requestFactory = requestFactory;
            this.coreCancellationService = coreCancellationService;
            this.channelInfoService = channelInfoService;
            this.resultMappingService = resultMappingService;
            this.requestValidator = requestValidator;
            this.cancellationLogger = cancellationLogger;
        }

        public async Task<Guid> StartCancellationProcessAsync(AccommodationB2CCancellationRequest cancellationRequest)
        {
            // create a booking id if the caller didn't provide one
            if (cancellationRequest.CancellationId == null) cancellationRequest.CancellationId = Guid.NewGuid();

            // Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateCancellationRequestStatus(cancellationRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            // start the booking process
            #pragma warning disable 4014
            Task.Run(async() => await StartCancellationProcessAsync(cancellationRequest, requestStatus));
            #pragma warning restore 4014
            return cancellationRequest.CancellationId.Value;
        }

        public async Task<AccommodationB2CCancellationResponse> GetCancellationResponseAsync(Guid cancellationId)
        {
            AccommodationB2CCancellationRequest cancellationRequest = null;
            AccommodationB2CRequestStatus requestStatus = null;
            try 
            {
                // get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(cancellationId);

                // throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2CCancellationResponse() { CancellationId = cancellationId, CancellationStatus = Status.NotFound };

                // if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2CCancellationResponse() { CancellationId = cancellationId, CancellationStatus = requestStatus.Status };

                // get valuation request
                cancellationRequest = requestStatus.CancellationRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, cancellationId.ToString(), "(accommodation)", cancellationRequest.Debugging, logger))
                {
                    // perform a cancellation search
                    var coreCancellationResponse = await coreCancellationService.GetCancellationResponseAsync(cancellationId);
                    
                    // map the core results
                    var cancellationResult = resultMappingService.MapFromCoreCancellationResults(cancellationRequest, coreCancellationResponse.Result);

                    // convert to FlightAvailabilityResponse and return
                    return new AccommodationB2CCancellationResponse
                    {
                        CancellationId = cancellationId
                        , CancellationStatus = requestStatus.Status
                        , CancellationResult = cancellationResult
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetCancellationResponse Error: search [{0}] {1}\n\n{2}", cancellationId, ex.GetDetailedMessageWithInnerExceptions(), cancellationRequest.ToIndentedJson());
                return new AccommodationB2CCancellationResponse { CancellationId = cancellationId, CancellationStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        private async Task StartCancellationProcessAsync(AccommodationB2CCancellationRequest cancellationRequest, AccommodationB2CRequestStatus requestStatus)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Cancellation, cancellationRequest.CancellationId.ToString(), "(accommodation)", cancellationRequest.Debugging, logger))
	        {
                var searchStarted = DateTime.Now;
                AccommodationCancellationResponse coreCancellationResponse = null; 
                Exception exception = null;
                try
                {
                    // get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(cancellationRequest.Channel);

                    // update Mongo with inprogress status
                    await requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Cancellation Started");
                    
                    // Validate Request
                    requestValidator.ValidateCancellationRequest(cancellationRequest);
                                        
                    // create a core cancellation request
                    var coreCancellationRequest = requestFactory.CreateCancellationRequest(cancellationRequest, channelInfo);

                    // perform a availability search
                    coreCancellationResponse = await coreCancellationService.ProcessCancellationAsync(coreCancellationRequest);
                    
                    // check if valuation failed
                    if (coreCancellationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        requestStatus.Status = Status.Successful;
                        requestStatus.Message = "Accommodation Cancellation Completed";
                    }
                    else
                    {
                        requestStatus.Status = Status.Failed;
                        requestStatus.Message = "Accommodation Provider Cancellation Failed";
                    }
                }
                catch (Exception ex)
                {
                    // update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation Cancellation Error: search [{0}] {1}\n\n{2}", cancellationRequest.CancellationId, ex.GetDetailedMessageWithInnerExceptions(), cancellationRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                await cancellationLogger.LogAsync(cancellationRequest, requestStatus, coreCancellationResponse, DateTime.Now - searchStarted, exception);
            }
        }
    }
}
