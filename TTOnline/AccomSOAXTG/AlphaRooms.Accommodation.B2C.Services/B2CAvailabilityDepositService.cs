﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.B2C.Interfaces;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Repositories;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CAvailabilityDepositService : IB2CAvailabilityDepositService
    {
        private readonly IDepositSchemeRepository depositSchemeRepository;
        private readonly ISupplierExcludedOfDepositRepository supplierExcludedOfDepositRepository;
        private const decimal DepositMinimumSpend = 100;

       
        public B2CAvailabilityDepositService(IDepositSchemeRepository depositSchemeRepository, 
            ISupplierExcludedOfDepositRepository supplierExcludedOfDepositRepository)
        {
            this.depositSchemeRepository = depositSchemeRepository;
            this.supplierExcludedOfDepositRepository = supplierExcludedOfDepositRepository;
        }

        public async Task<IList<AccommodationB2CAvailabilityResult>> UpdateDepositEligibilityAsync(IList<AccommodationB2CAvailabilityResult> results, DateTime checkInDate)
        {
            string[] depositExcludedSuppliers = null;
            var currentDepositScheme = await this.depositSchemeRepository.GetCurrentDepositSchemeAsync();

            if (!IsDepositAllowed(currentDepositScheme)) return results;

            //if checkin date is beyond the threshold no need to go through any rooms
            if (!IsBeyondWeeks(checkInDate, currentDepositScheme.DepositMinWeeks)) return results;

            depositExcludedSuppliers =await this.supplierExcludedOfDepositRepository.GetDepositExcludedListOFProvidersAsync();

            Parallel.ForEach(results, (result) =>
            {
                Parallel.ForEach(result.Rooms, (room) =>
                {
                    if (!room.IsNonRefundable //is refundable
                        && room.PaymentModel != PaymentModel.CustomerPayDirect //is not pay direct
                        && room.Price.Amount > currentDepositScheme.DepositMinHotelSpend //has minimum spend
                        &&  !this.supplierExcludedOfDepositRepository.CheckIfSupplierExistsInExcludedList(depositExcludedSuppliers, room.ProviderEdiCode))
                    {
                        room.IsEligibleForDeposit = true;
                    }
                });
            });

            return results;
        }

        private bool IsDepositAllowed(DepositScheme currentDepositScheme)
        {
            return !(currentDepositScheme.DepositMinSpend == DepositMinimumSpend);
        }

        private bool IsBeyondWeeks(DateTime checkInDate, int minimumWeeksToTravel)
        {
            var currentDate = DateTime.Now.Date;
            //check in date is not in the past
            if (checkInDate <= currentDate)
            {
                return false;
            }

            var minDate = currentDate.AddDays(minimumWeeksToTravel * 7);
            if (minDate > checkInDate)
            {
                return false;
            }

            return true;
        }


    }
}