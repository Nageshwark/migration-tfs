﻿using AlphaRooms.Accommodation.B2C.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2C.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Utilities;
using AutoMapper;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.B2C.Services
{
    public class B2CCancellationResultMappingService : IB2CCancellationResultMappingService
    {
        static B2CCancellationResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationCancellationResult, AccommodationB2CCancellationResult>();
                //Mapper.CreateMap<AccommodationCancellationResultRoom, AccommodationB2CCancellationResultRoom>();
            }
        }
        
        private readonly ILogger logger;

        public B2CCancellationResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public AccommodationB2CCancellationResult MapFromCoreCancellationResults(AccommodationB2CCancellationRequest cancellationRequest, AccommodationCancellationResult coreCancellationResult)
        {
            AccommodationB2CCancellationResult result = null;
            try
            {
                result = Mapper.Map<AccommodationCancellationResult, AccommodationB2CCancellationResult>(coreCancellationResult);
                //result.Rooms = coreCancellationResult.Rooms.Select(i => CreateAccommodationB2CCancellationResultRoom(cancellationRequest, i)).ToArray();

            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreCancellationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreCancellationResult.ToIndentedJson());
            }
            return result;
        }

        //private AccommodationB2CCancellationResultRoom CreateAccommodationB2CCancellationResultRoom(AccommodationB2CCancellationRequest cancellationRequest, AccommodationCancellationResultRoom coreCancellationResultRoom)
        //{
        //    var result = Mapper.Map<AccommodationCancellationResultRoom, AccommodationB2CCancellationResultRoom>(coreCancellationResultRoom);
        //    return result;
        //}
    }
}
