﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSValuationResponse
    {
        public HotelResRequestType SupplierRequest { get; set; }
        public HotelResResponseType SupplierResponse { get; set; }

    }
}
