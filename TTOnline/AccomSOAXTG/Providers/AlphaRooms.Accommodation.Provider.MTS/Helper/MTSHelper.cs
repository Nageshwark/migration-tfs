﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Utilities.ChmService;

namespace AlphaRooms.Accommodation.Provider.MTS.Helper
{
    public static class MTSHelper
    {
        private static Dictionary<string, string> BoardDictionary;

        static MTSHelper()
        {

            BoardDictionary = new Dictionary<string, string>()
            {
                {"BB","Bed & Breakfast" },
                {"BBU","Bed & Breakfast" },
                {"HB","Half Board" },
                {"RO","Room Only" },
                {"SC","Self Catering" },
                {"FB", "Full Board" },
                {"AI", "All Inclusive" },
                {"CB", "Breakfast" },
                {"AB","Breakfast" },
                {"AIB","All Inclusive" },
                {"AIL","All Inclusive" },
                {"AIU","All Inclusive" }
            };

        }

        public static string BoardDescription(string boardType)
        {
            return BoardDictionary.ContainsKey(boardType) ? BoardDictionary[boardType] : "Room Only";
        }
    }
}
