﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSCancellationParser : IAccommodationCancellationParserAsync<MTSCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public MTSCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, MTSCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                var cancelResponse = response.SupplierResponse;

                if (cancelResponse != null &&
                    cancelResponse.Success != null)
                {
                    var cancelInfo = cancelResponse.CancelInfoRS;

                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";

                    if (cancelInfo != null && cancelInfo.CancelRules.Any())
                    {
                        bookingResult.CancellationCost = new Money
                        {
                            Amount = Convert.ToDecimal(cancelInfo.CancelRules.First().Amount),
                            CurrencyCode = cancelInfo.CancelRules.First().CurrencyCode
                        };
                    }

                }
                else
                {
                    
                    
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    var sb = new StringBuilder();
                    
                    sb.AppendLine("There was an unknown problem with the cancellation response from MTS.");

                    if (cancelResponse !=null && cancelResponse.Errors !=null && cancelResponse.Errors.Any())
                    {
                        int i = 1;
                        sb.AppendLine("Details:");
                        foreach (ErrorType error in cancelResponse.Errors)
                        {
                            sb.AppendLine(string.Format("{0}. {1}", i, error.Value));
                            i++;
                        }    
                    }

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from MTS.");
                sb.AppendLine("Details:" + ex.ToString());
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
       
    }
}
