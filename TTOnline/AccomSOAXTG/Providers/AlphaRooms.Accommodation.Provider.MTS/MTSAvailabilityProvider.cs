﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<MTSAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<MTSAvailabilityResponse> parser;

        public MTSAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<MTSAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<MTSAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
