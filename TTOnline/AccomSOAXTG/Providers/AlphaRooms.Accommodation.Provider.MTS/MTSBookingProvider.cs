﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<MTSBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<MTSBookingResponse>> parser;

        public MTSBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<MTSBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<MTSBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
