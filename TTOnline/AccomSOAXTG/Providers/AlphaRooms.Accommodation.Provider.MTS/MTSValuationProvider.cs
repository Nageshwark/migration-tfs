﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.MTS;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<MTSValuationResponse> automator;
        private readonly IAccommodationValuationParser<MTSValuationResponse> parser;

        public MTSValuationProvider(IAccommodationValuationAutomatorAsync<MTSValuationResponse> automator,
                                                IAccommodationValuationParser<MTSValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
