﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSAvailabilityResponse
    {
        public OTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailRS SupplierResponse { get; set; }

    }
}
