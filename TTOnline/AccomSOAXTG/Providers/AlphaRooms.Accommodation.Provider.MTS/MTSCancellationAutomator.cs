﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSCancellationAutomator : IAccommodationCancellationAutomatorAsync<MTSCancellationResponse>
    {
        private const string MTSCancellationUrl = "MTSCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string MTSUsername = "MTSUsername";
        private const string MTSPassword = "MTSPassword";

        
        private readonly ILogger logger;
        private readonly IMTSProviderCancellationRequestFactory mtsProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public MTSCancellationAutomator(ILogger logger,
                                            IMTSProviderCancellationRequestFactory MTSSupplierCancellationRequestFactory)
        {
            this.logger = logger;
            this.mtsProviderCancellationRequestFactory = MTSSupplierCancellationRequestFactory;
        }
        public async Task<MTSCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            MTSCancellationResponse response = null;

            try
            {
                var supplierRequest = mtsProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }
                
                var client = ConfigureSoapCleint(request);
                

                var supplierResponse = client.cancel(supplierRequest);

                response = new MTSCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("MTSBookingAutomator.GetCancellationResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        private OTAClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            var binding = new BasicHttpBinding();
            var helper = new ProxyHelper();

            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            

            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MTSCancellationUrl)));
            var client = new OTAClient(binding, address);
            
            helper.SetProxy(client, request.Provider.Parameters);

            client.InnerChannel.OperationTimeout = request.Provider.SearchTimeout;

            return client;
        }
    }
}
