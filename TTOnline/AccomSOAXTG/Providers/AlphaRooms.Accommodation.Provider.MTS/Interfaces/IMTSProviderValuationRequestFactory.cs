﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS.Interfaces
{
    public interface IMTSProviderValuationRequestFactory
    {
        HotelResRequestType CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
