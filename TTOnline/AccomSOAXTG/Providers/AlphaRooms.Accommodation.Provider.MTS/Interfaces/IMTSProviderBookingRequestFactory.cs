﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.MTS.Interfaces
{
    public interface IMTSProviderBookingRequestFactory
    {
        HotelResRequestType CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
