﻿using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSCancellationResponse
    {
        public OTA_CancelRQ SupplierRequest { get; set; }
        public OTA_CancelRS SupplierResponse { get; set; }
    }
}
