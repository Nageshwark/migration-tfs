﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSAvailabilityParser : IAccommodationAvailabilityParser<MTSAvailabilityResponse>
    {
        private readonly ILogger logger;
        private readonly IProviderNonRefundableService nonRefundableService;

        public MTSAvailabilityParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.logger = logger;
            this.nonRefundableService = nonRefundableService;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            MTSAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse,
                    availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                var message =
                    $"MTS Supplier Data exception in Availability Parser: {Environment.NewLine} Exception Message: {Environment.NewLine}{errorMessage}{Environment.NewLine} Stack Trace: {Environment.NewLine}{ex.StackTrace}";
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
            OTA_HotelAvailRQ supplierRequest, OTA_HotelAvailRS supplierResponse,
            ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse?.RoomStays?.RoomStay != null && supplierResponse.RoomStays.RoomStay.Any());

            if (hasResults)
            {
                var roomStays = supplierResponse.RoomStays;

                Parallel.ForEach(roomStays.RoomStay, roomStay =>
                {
                    var result = new AccommodationProviderAvailabilityResult();

                    result.RoomNumber = (byte)(Convert.ToByte(roomStay.RoomStayCandidateRPH) + 1);

                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.SupplierEdiCode = request.Provider.EdiCode;

                    //TODO:Confirm PaymentModel
                    result.PaymentModel = PaymentModel.PostPayment;

                    //TODO:Confirm RateType
                    result.RateType = RateType.NetStandard;

                    result.EstablishmentEdiCode = roomStay.BasicPropertyInfo.HotelCode;

                    result.ProviderEstablishmentCode = roomStay.BasicPropertyInfo.HotelCode;


                    result.RoomDescription =
                        ((FormattedTextTextType)roomStay.RoomTypes.First().RoomDescription.Items[0]).Value;
                    result.RoomCode = roomStay.RoomTypes.First()?.RoomTypeCode;

                    result.CheckInDate = request.CheckInDate;
                    result.CheckOutDate = request.CheckOutDate;

                    result.BoardCode =
                        ((FormattedTextTextType)
                            roomStay.RoomRates.RoomRate.First().Features.First().Description.First().Items[0]).Value;
                    result.BoardDescription = MTSHelper.BoardDescription(result.BoardCode);
                    result.NumberOfAvailableRooms = roomStay.RoomRates.RoomRate.First()?.NumberOfUnits;
                    //TODO:Confirm what we should populate for CostPrice
                    result.CostPrice = new Money()
                    {
                        Amount = roomStay.RoomRates.RoomRate.First().Rates.First().Total.AmountAfterTax,
                        CurrencyCode = roomStay.RoomRates.RoomRate.First().Rates.First().Total.CurrencyCode
                    };

                    result.SalePrice = result.CostPrice;

                    //TODO:NonReundable, need an example in the response.
                    result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(result.RoomDescription, false);

                    result.Adults = result.Adults = GetGuestCount(GuestType.Adult, roomStay.GuestCounts.GuestCount);
                    result.Children = GetGuestCount(GuestType.Child, roomStay.GuestCounts.GuestCount);
                    result.Infants = GetGuestCount(GuestType.Infant, roomStay.GuestCounts.GuestCount);

                    result.ProviderSpecificData = new Dictionary<string, string>();


                    availabilityResults.Enqueue(result);

                });
            }
            else
            {
                var errors = supplierResponse?.Errors;

                if (errors == null || !errors.Any()) return;
                var sbMessage = new StringBuilder();

                sbMessage.AppendLine("MTS Availability Parser: Availability Response returned error.");
                sbMessage.AppendLine("Error Details: ");

                int i = 1;
                foreach (var error in errors)
                {
                    sbMessage.AppendLine($"{i++}. Error Code {error.Code}, Error Text: {error.ShortText}");
                }

                throw new SupplierApiDataException(sbMessage.ToString());
            }
        }

        private string GetHotelName(string hotelCode, OTA_HotelAvailRSHotelStay[] hotelStay)
        {
            string hotelName = null;
            var query = hotelStay.Where(p => string.Equals(p.BasicPropertyInfo.HotelCode, hotelCode, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            if (query.Any())
                hotelName = query.First().BasicPropertyInfo.HotelName;
            return hotelName;
        }

        private byte GetGuestCount(GuestType guestType, GuestCountTypeGuestCount[] guestList)
        {
            byte guestCount = 0;
            string ageCode = "";

            switch (guestType)
            {
                case GuestType.Adult:
                    ageCode = "10";
                    break;

                case GuestType.Child:
                    ageCode = "8";
                    break;

                case GuestType.Infant:
                    ageCode = "7";
                    break;
            }

            var query = guestList.Where(guest => guest.AgeQualifyingCode == ageCode).ToArray();
            if (query.Any())
                guestCount = (byte)query.Sum(g => g.Count);

            return guestCount;
        }


    }
}
