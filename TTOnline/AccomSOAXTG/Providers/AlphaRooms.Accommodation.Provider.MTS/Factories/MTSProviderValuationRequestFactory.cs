﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.MTS.Factories
{
    public class MTSProviderValuationRequestFactory : IMTSProviderValuationRequestFactory
    {
        private const string MTSInstance = "MTSInstance";
        private const string MTSIDContext = "MTSIDContext";
        private const string MTSID = "MTSID";
        private const string MTSType = "MTSType";
        private const string MTSBookingChannel = "MTSBookingChannel";
        public HotelResRequestType  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new HotelResRequestType
            {
                ResStatus = transactionActionType.Quote,
                ResStatusSpecified = true,
                POS = new[]
                {
                    new SourceType
                    {
                        RequestorID = new SourceTypeRequestorID
                        {
                            Instance = request.Provider.Parameters.GetParameterValue(MTSInstance),
                            ID = request.Provider.Parameters.GetParameterValue(MTSID),
                            ID_Context = request.Provider.Parameters.GetParameterValue(MTSIDContext),
                            Type = request.Provider.Parameters.GetParameterValue(MTSType)
                        },
                        BookingChannel = new SourceTypeBookingChannel
                        {
                            Type = request.Provider.Parameters.GetParameterValue(MTSBookingChannel)
                        }
                    }
                }
            };

            supplierRequest.HotelReservations = new HotelReservationsType
            {
                HotelReservation = new[]
                {
                    new HotelReservationType
                    {
                        RoomStays = CreateRoomStay(request.SelectedRooms),
                        Services = CreateServices(request.SelectedRooms),
                        ResGuests = CreateResGuests(request.SelectedRooms),
                    }
                }
            };

            return supplierRequest;
        }

        private ResGuestsTypeResGuest[] CreateResGuests(AccommodationProviderValuationRequestRoom[] selectedRooms)
        {
            var guests = new List<ResGuestsTypeResGuest>();

            int i = 0;
            foreach (var room in selectedRooms)
            {
                foreach (var guest in room.Guests)
                {
                    var resGeust = new ResGuestsTypeResGuest
                    {
                        AgeQualifyingCode = GetAgeQualifyingCode(guest.Type),
                        ResGuestRPH = i.ToString(),
                        Profiles = new []
                        {
                            new ProfilesTypeProfileInfo
                            {
                                Profile = new ProfileType
                                {
                                    Customer = new CustomerType
                                    {
                                       BirthDate = DateTime.Today.AddYears(-guest.Age).ToString("yyyy-MM-dd"),
                                       PersonName = new PersonNameType
                                       {
                                           NamePrefix = new []{"Mr"},
                                           GivenName = new []{ "Alpharooms"},
                                           Surname = "TEST"
                                       }
                                    }
                                }
                            }
                        }
                    };

                    guests.Add(resGeust);

                    i++;
                }
            }

            return guests.ToArray();
        }

        private string GetAgeQualifyingCode(GuestType type)
        {
            switch (type)
            {
                case  GuestType.Adult:
                    return "10";

                case GuestType.Child:
                    return "8";

                case  GuestType.Infant:
                    return "7";

                default:
                    return "";

            }
        }

        private ServicesTypeService[] CreateServices(AccommodationProviderValuationRequestRoom[] selectedRooms)
        {
            var services = new List<ServicesTypeService>();

            int i = 0;
            foreach (var room in selectedRooms)
            {
                var service = new ServicesTypeService
                {
                    ServiceInventoryCode = room.AvailabilityResult.BoardCode,
                    ServiceRPH = i.ToString(),
                    ServicePricingTypeSpecified = true
                };

                services.Add(service);
                i++;
            }

            return services.ToArray();
        }

        private RoomStaysTypeRoomStay[] CreateRoomStay(AccommodationProviderValuationRequestRoom [] selectedRooms)
        {
            var roomStays = new List<RoomStaysTypeRoomStay>();
            int totalGuestCounter = 0;
            int counter = 0;
            foreach (var room in selectedRooms)
            {
                var roomStay = new RoomStaysTypeRoomStay
                {
                    RoomTypes = new[]
                    {
                        new RoomTypeType
                        {
                            RoomTypeCode = room.AvailabilityResult.RoomCode
                        }
                    },
                    TimeSpan = new DateTimeSpanType
                    {
                        Start = room.AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                        End = room.AvailabilityRequest.CheckOutDate.ToString("yyyy-MM-dd")
                    },
                    BasicPropertyInfo = new BasicPropertyInfoType
                    {
                        HotelCode = room.AvailabilityResult.ProviderEstablishmentCode
                    },
                    ResGuestRPHs = GetGuestRPHs(room.Guests, totalGuestCounter),
                    ServiceRPHs = new []
                    {
                        new ServiceRPHsTypeServiceRPH
                        {
                            RPH = counter.ToString()
                        }
                    }
                };

                roomStays.Add(roomStay);

                totalGuestCounter+= room.Guests.Count();
                counter++;

            }

            return roomStays.ToArray();
        }

        private ResGuestRPHsTypeResGuestRPH[] GetGuestRPHs(AccommodationProviderAvailabilityRequestGuestCollection guests, int totalGuestCounter)
        {
            var guestRPHs = new List<ResGuestRPHsTypeResGuestRPH>();

            int i = totalGuestCounter;
            foreach (var guest in guests)
            {
                var guestRPH = new ResGuestRPHsTypeResGuestRPH
                {
                    RPH = i.ToString()

                };

                guestRPHs.Add(guestRPH);

                i++;
            }

            return guestRPHs.ToArray();
        }
    }
}
