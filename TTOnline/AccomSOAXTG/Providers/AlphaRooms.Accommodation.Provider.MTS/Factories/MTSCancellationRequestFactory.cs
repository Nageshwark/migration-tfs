﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;

namespace AlphaRooms.Accommodation.Provider.MTS.Factories
{
    public class MTSCancellationRequestFactory : IMTSProviderCancellationRequestFactory
    {
        private const string MTSInstance = "MTSInstance";
        private const string MTSIDContext = "MTSIDContext";
        private const string MTSID = "MTSID";
        private const string MTSType = "MTSType";
        private const string MTSBookingChannel = "MTSBookingChannel";

        public OTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new OTA_CancelRQ
            {
                CancelType = transactionActionType.Commit,
                POS = new []
                {
                    new SourceType
                    {
                        RequestorID = new SourceTypeRequestorID
                        {
                            Instance = request.Provider.Parameters.GetParameterValue(MTSInstance),
                            ID = request.Provider.Parameters.GetParameterValue(MTSID),
                            ID_Context = request.Provider.Parameters.GetParameterValue(MTSIDContext),
                            Type = request.Provider.Parameters.GetParameterValue(MTSType)
                        },
                        BookingChannel = new SourceTypeBookingChannel
                        {
                            Type = request.Provider.Parameters.GetParameterValue(MTSBookingChannel)
                        }
                    }
                },
                UniqueID = new []
                {
                    new OTA_CancelRQUniqueID
                    {
                        Type = "14",
                        ID_Context = "Internal",
                        ID = request.ProviderBookingReference
                    }
                }
            };
            
            return supplierRequest;
        }
    }
}
