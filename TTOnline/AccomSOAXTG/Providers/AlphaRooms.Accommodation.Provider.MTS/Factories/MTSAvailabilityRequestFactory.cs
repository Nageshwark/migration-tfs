﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.MTS.Factories
{
    public class MTSAvailabilityRequestFactory : IMTSAvailabilityRequestFactory
    {
        private const string MTSInstance = "MTSInstance";
        private const string MTSIDContext = "MTSIDContext";
        private const string MTSID = "MTSID";
        private const string MTSType = "MTSType";
        private const string MTSBookingChannel = "MTSBookingChannel";

        public OTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new OTA_HotelAvailRQ
            {

                Version = 0.1m,
                POS = new[]
                {
                    new SourceType()
                    {
                        RequestorID = new SourceTypeRequestorID
                        {
                            Instance = request.Provider.Parameters.GetParameterValue(MTSInstance),
                            ID = request.Provider.Parameters.GetParameterValue(MTSID),
                            ID_Context = request.Provider.Parameters.GetParameterValue(MTSIDContext),
                            Type = request.Provider.Parameters.GetParameterValue(MTSType)
                        },
                        BookingChannel = new SourceTypeBookingChannel
                        {
                            Type = request.Provider.Parameters.GetParameterValue(MTSBookingChannel)
                        }

                    }
                },
                AvailRequestSegments = new OTA_HotelAvailRQAvailRequestSegments
                {
                    AvailRequestSegment = new[]
                    {
                        new AvailRequestSegmentsTypeAvailRequestSegment
                        {
                            StayDateRange = new DateTimeSpanType()
                            {
                                Start = request.CheckInDate.ToString("yyyy-MM-dd"),
                                End = request.CheckOutDate.ToString("yyyy-MM-dd")
                            },
                            RoomStayCandidates = CreateRoomStay(request),
                            HotelSearchCriteria = CreateSearchCriteria(request), InfoSource = "4567"
                        }
                    }
                }
            };

            return supplierRequest;
        }

        private HotelSearchCriteriaType CreateSearchCriteria(AccommodationProviderAvailabilityRequest request)
        {
            var searchCriterion = new HotelSearchCriteriaType();

            if (request.DestinationCodes.Any())
            {
                searchCriterion.Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        RefPoint = CreeateRefPoint(request.DestinationCodes[0])
                    }
                };
            }
            else if (request.EstablishmentCodes.Any())
            {
                searchCriterion.Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        ExactMatch = true,
                        ExactMatchSpecified = true,
                        HotelRef = request.EstablishmentCodes.Select(e => new ItemSearchCriterionTypeHotelRef {HotelCode = e}).ToArray()
                    }
                };
            }

            return searchCriterion;
        }

        private ItemSearchCriterionTypeRefPoint[] CreeateRefPoint(string destinationCode)
        {
            string[] destinationCodes = destinationCode.Split('|');
            var refPoints = new List<ItemSearchCriterionTypeRefPoint>();

            if (destinationCodes[0] != "####")
            {
                refPoints.Add(new ItemSearchCriterionTypeRefPoint
                {
                    CodeContext = "Country",
                    Value = destinationCodes[0]
                });
            }

            if (destinationCodes.Length > 1 && destinationCodes[1] != "####")
            {
                refPoints.Add(new ItemSearchCriterionTypeRefPoint
                {
                    CodeContext = "Destination",
                    Value = destinationCodes[1]
                });
            }

            if (destinationCodes.Length > 2 && destinationCodes[2] != "####")
            {
                refPoints.Add(new ItemSearchCriterionTypeRefPoint
                {
                    CodeContext = "Region",
                    Value = destinationCodes[2]
                });
            }

            if (destinationCodes.Length > 3 && destinationCodes[3] != "####")
            {
                refPoints.Add(new ItemSearchCriterionTypeRefPoint
                {
                    CodeContext = "Resort",
                    Value = destinationCodes[3]
                });
            }

            return refPoints.ToArray();
        }

        private AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate[] CreateRoomStay(AccommodationProviderAvailabilityRequest request)
        {
            var roomStay = new AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate[request.Rooms.Count()];

            int i = 0;
            foreach (var room in request.Rooms)
            {
                var roomStayCandidate = new AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate
                {
                    //RPH = i.ToString(),
                    GuestCounts = new GuestCountType
                    {
                        GuestCount = new GuestCountTypeGuestCount[1 + room.Guests.ChildrenAndInfantsCount]
                    }
                };

                int counter = 0;
                //Assuming that there will be always one adult in a room
                roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount
                {
                    AgeQualifyingCode = "10",
                    Count = room.Guests.AdultsCount,
                    CountSpecified = true
                };
                counter++;

                if (room.Guests.ChildrenAndInfantsCount > 0)
                {
                    string ageQulifyingCode = string.Empty;

                    foreach (var guest in room.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant))
                    {
                        if (guest.Type == GuestType.Child)
                            ageQulifyingCode = "8";
                        else if (guest.Type == GuestType.Infant)
                            ageQulifyingCode = "7";

                        roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
                        {
                            AgeQualifyingCode = ageQulifyingCode,
                            Age = guest.Age,
                            AgeSpecified = true,
                            Count = 1,
                            CountSpecified = true
                        };

                        counter++;
                    }
                }

                roomStay[i] = roomStayCandidate;

                i++;
            }
            return roomStay;
        }

        private GuestCountTypeGuestCount[] CalculateGuestCount(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            int guestCount = 0;

            if (guests.AdultsCount > 0)
                guestCount++;

            if (guests.ChildrenCount > 0)
                guestCount++;

            if (guests.InfantsCount > 0)
                guestCount++;

            return new GuestCountTypeGuestCount[guestCount];

        }
    }
}
