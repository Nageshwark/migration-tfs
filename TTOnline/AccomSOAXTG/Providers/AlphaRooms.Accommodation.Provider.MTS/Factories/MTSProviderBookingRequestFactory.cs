﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSProviderBookingRequestFactory : IMTSProviderBookingRequestFactory
    {
        private const string MTSInstance = "MTSInstance";
        private const string MTSIDContext = "MTSIDContext";
        private const string MTSID = "MTSID";
        private const string MTSType = "MTSType";
        private const string MTSBookingChannel = "MTSBookingChannel";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HotelResRequestType CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {

            var supplierRequest = new HotelResRequestType
            {
                ResStatus = transactionActionType.Commit,
                ResStatusSpecified = true,
                POS = new[]
                {
                    new SourceType
                    {
                        RequestorID = new SourceTypeRequestorID
                        {
                            Instance = request.Provider.Parameters.GetParameterValue(MTSInstance),
                            ID = request.Provider.Parameters.GetParameterValue(MTSID),
                            ID_Context = request.Provider.Parameters.GetParameterValue(MTSIDContext),
                            Type = request.Provider.Parameters.GetParameterValue(MTSType)
                        },
                        BookingChannel = new SourceTypeBookingChannel
                        {
                            Type = request.Provider.Parameters.GetParameterValue(MTSBookingChannel)
                        }
                    }
                }
            };

            supplierRequest.HotelReservations = new HotelReservationsType
            {
                HotelReservation = new[]
                {
                    new HotelReservationType
                    {
                        RoomStays = CreateRoomStay(request.ValuatedRooms),
                        Services = CreateServices(request.ValuatedRooms),
                        ResGuests = CreateResGuests(request.ValuatedRooms, request.Customer),
                    }
                }
            };

            return supplierRequest;
        }

        private ResGuestsTypeResGuest[] CreateResGuests(AccommodationProviderBookingRequestRoom[] selectedRooms, AccommodationProviderBookingRequestCustomer customer)
        {
            var guests = new List<ResGuestsTypeResGuest>();
            int i = 0;
            foreach (var room in selectedRooms)
            {
                foreach (var guest in room.Guests)
                {
                    var resGeust = new ResGuestsTypeResGuest
                    {
                        AgeQualifyingCode = GetAgeQualifyingCode(guest.Type),
                        ResGuestRPH = i.ToString()

                    };

                    resGeust.Profiles = SetLeadPassengerDetails(guest);

                    guests.Add(resGeust);

                    i++;
                }
            }

            return guests.ToArray();
        }

        private ProfilesTypeProfileInfo[] SetLeadPassengerDetails(AccommodationProviderBookingRequestRoomGuest customer)
        {
            return
                new[]
                {
                    new ProfilesTypeProfileInfo
                    {
                        Profile = new ProfileType
                        {
                            Customer = new CustomerType
                            {
                                PersonName = new PersonNameType
                                {
                                    NameTitle = new [] { customer.Title.ToString()},
                                    GivenName = new[] { customer.FirstName},
                                    Surname = customer.Surname
                                },
                               BirthDate = DateTime.Now.AddYears(-customer.Age).ToString("yyyy-MM-dd")

                            }
                        }
                    }
                };
        }

        private string GetAgeQualifyingCode(GuestType type)
        {
            switch (type)
            {
                case GuestType.Adult:
                    return "10";

                case GuestType.Child:
                    return "8";

                case GuestType.Infant:
                    return "7";

                default:
                    return "";

            }
        }

        private ServicesTypeService[] CreateServices(AccommodationProviderBookingRequestRoom[] selectedRooms)
        {
            var services = new List<ServicesTypeService>();

            int i = 0;
            foreach (var room in selectedRooms)
            {
                var service = new ServicesTypeService
                {
                    ServiceInventoryCode = room.ValuationResult.BoardCode,
                    ServiceRPH = i.ToString(),
                    ServicePricingTypeSpecified = true
                };

                services.Add(service);
                i++;
            }

            return services.ToArray();
        }

        private RoomStaysTypeRoomStay[] CreateRoomStay(AccommodationProviderBookingRequestRoom[] selectedRooms)
        {
            var roomStays = new List<RoomStaysTypeRoomStay>();
            int totalGuestCounter = 0;

            foreach (var room in selectedRooms)
            {

                var roomStay = new RoomStaysTypeRoomStay
                {
                    RoomTypes = new[]
                    {
                        new RoomTypeType
                        {
                            RoomTypeCode = room.ValuationResult.RoomCode
                        }
                    },
                    TimeSpan = new DateTimeSpanType
                    {
                        Start = room.AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                        End = room.AvailabilityRequest.CheckOutDate.ToString("yyyy-MM-dd")
                    },
                    BasicPropertyInfo = new BasicPropertyInfoType
                    {
                        HotelCode = room.ValuationResult.ProviderEstablishmentCode
                    },
                    ResGuestRPHs = GetGuestRPHs(room.Guests, totalGuestCounter),
                    ServiceRPHs = new[]
                    {
                        new ServiceRPHsTypeServiceRPH
                        {
                            RPH = "0"
                        }
                    }
                };

                roomStays.Add(roomStay);

                totalGuestCounter = room.Guests.Count();

            }

            return roomStays.ToArray();
        }

        private ResGuestRPHsTypeResGuestRPH[] GetGuestRPHs(AccommodationProviderBookingRequestRoomGuestCollection guests, int totalGuestCounter)
        {
            var guestRPHs = new List<ResGuestRPHsTypeResGuestRPH>();
            int i = totalGuestCounter;
            foreach (var guest in guests)
            {
                var guestRPH = new ResGuestRPHsTypeResGuestRPH
                {
                    RPH = i.ToString()

                };

                guestRPHs.Add(guestRPH);

                i++;
            }

            return guestRPHs.ToArray();
        }
    }
}
