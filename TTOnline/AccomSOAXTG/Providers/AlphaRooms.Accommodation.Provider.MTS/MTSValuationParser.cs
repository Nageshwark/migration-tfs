﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSValuationParser : IAccommodationValuationParser<MTSValuationResponse>
    {
        private readonly ILogger logger;

        public MTSValuationParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, MTSValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    $"MTS Supplier Data exception in Valuation Parser: {Environment.NewLine} Exception Message: {Environment.NewLine}{errorMessage}{Environment.NewLine} Stack Trace: {Environment.NewLine}{ex.StackTrace}";
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, HotelResRequestType supplierRequest, HotelResResponseType supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }

            bool hasResults = (supplierResponse?.HotelReservations != null);

            if (hasResults)
            {
                var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
                var resGlobalInfo = supplierResponse.HotelReservations.HotelReservation.First().ResGlobalInfo;

                foreach (var roomStay in supplierResponse.HotelReservations.HotelReservation.First().RoomStays)
                {
                    var result = new AccommodationProviderValuationResult();
                    var roomNumber = int.Parse(roomStay.RPH);


                    result.RoomNumber = (byte)roomNumber;

                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.SupplierEdiCode = request.Provider.EdiCode;

                    //TODO:Confirm PaymentModel
                    result.PaymentModel = PaymentModel.PostPayment;

                    //TODO:Confirm RateType
                    result.RateType = RateType.NetStandard;

                    //ToDo:Remove this as OTS informed us that this node is not returned in production
                    if (resGlobalInfo.BasicPropertyInfo == null)
                    {
                        result.DestinationEdiCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.DestinationEdiCode;
                        result.EstablishmentEdiCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.EstablishmentEdiCode;
                        result.ProviderEstablishmentCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.ProviderEstablishmentCode;
                        result.EstablishmentName = request.SelectedRooms[roomNumber - 1].AvailabilityResult.EstablishmentName;
                    }
                    else
                    {
                        result.DestinationEdiCode = resGlobalInfo.BasicPropertyInfo.AreaID;
                        result.EstablishmentEdiCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.EstablishmentEdiCode;
                        result.ProviderEstablishmentCode = resGlobalInfo.BasicPropertyInfo.HotelCode;
                        result.EstablishmentName = resGlobalInfo.BasicPropertyInfo.HotelName;
                    }

                    result.RoomDescription = request.SelectedRooms[roomNumber - 1].AvailabilityResult.RoomDescription;
                    result.RoomCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.RoomCode;

                    result.CheckInDate = availabilityRequest.CheckInDate;
                    result.CheckOutDate = availabilityRequest.CheckOutDate;

                    result.BoardCode = request.SelectedRooms[roomNumber - 1].AvailabilityResult.BoardCode;
                    result.BoardDescription = request.SelectedRooms[roomNumber - 1].AvailabilityResult.BoardDescription;
                    result.CostPrice = new Money()
                    {
                        Amount = roomStay.Total.AmountAfterTax,
                        CurrencyCode = roomStay.Total.CurrencyCode
                    };

                    //TODO:Confirm what we should populate for CostPrice
                    result.SalePrice = result.CostPrice;

                    result.IsNonRefundable = request.SelectedRooms[roomNumber - 1].AvailabilityResult.IsNonRefundable;
                    result.CancellationPolicy = result.IsNonRefundable ? null : new[] { GetCancellationPolicy(resGlobalInfo.CancelPenalties) };

                    result.Adults = availabilityRequest.Rooms.First().Guests.AdultsCount;
                    result.Children = availabilityRequest.Rooms.First().Guests.ChildrenCount;
                    result.Infants = availabilityRequest.Rooms.First().Guests.InfantsCount;

                    availabilityResults.Enqueue(result);

                }


            }
            else
            {

                if (supplierResponse?.Errors != null && supplierResponse.Errors.Any())
                {
                    var sbMessage = new StringBuilder();
                    sbMessage.AppendLine("MTS Availability Parser: Valuation Response returned error.");
                    sbMessage.AppendLine("Error Details:");

                    int i = 1;
                    foreach (var error in supplierResponse.Errors)
                    {
                        sbMessage.AppendLine($"Error {i}:{error.Value}");
                        i++;
                    }

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }

        }

        private string GetCancellationPolicy(CancelPenaltiesType cancelPenalties)
        {
            if (cancelPenalties.CancelPenalty == null) return string.Empty;

            var sbCancellationPolicy = new StringBuilder();

            foreach (var cax in cancelPenalties.CancelPenalty)
            {
                var toDate = Convert.ToDateTime(cax.Deadline.AbsoluteDeadline).AddDays(Convert.ToInt32(cax.Deadline.OffsetUnitMultiplier)).ToString("yyyy-MM-dd");
                sbCancellationPolicy.AppendLine(
                    $"From:{cax.Deadline.AbsoluteDeadline} - To:{toDate}, Amount: {cax.AmountPercent.Amount}, Currency :{cax.AmountPercent.CurrencyCode}");

            }

            return sbCancellationPolicy.ToString();


        }

        private string GetHotelName(string hotelCode, OTA_HotelAvailRSHotelStay[] hotelStay)
        {
            var query = from OTA_HotelAvailRSHotelStay p in hotelStay
                        where string.Equals(p.BasicPropertyInfo.HotelCode, hotelCode, StringComparison.CurrentCultureIgnoreCase)
                        select p;

            return query.Any() ? query.First().BasicPropertyInfo.HotelName : null;
        }
    }
}
