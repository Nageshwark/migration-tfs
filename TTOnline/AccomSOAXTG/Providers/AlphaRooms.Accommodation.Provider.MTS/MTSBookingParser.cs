﻿
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSBookingParser : IAccommodationBookingParser<IEnumerable<MTSBookingResponse>>
    {
        private readonly ILogger logger;

        public MTSBookingParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<MTSBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();
            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    $"MTS Supplier Data exception in Booking Parser: {Environment.NewLine} Exception Message: {Environment.NewLine}{errorMessage}{Environment.NewLine} Stack Trace: {Environment.NewLine}{ex.StackTrace}";
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        HotelResRequestType supplierRequest,
                                                        HotelResResponseType supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            bool hasResults = (supplierResponse?.HotelReservations?.HotelReservation != null &&
                                supplierResponse.HotelReservations.HotelReservation.Any() &&
                                supplierResponse.ResResponseType == transactionStatusType.Committed);

            var result = new AccommodationProviderBookingResult();
            result.RoomNumber = request.ValuatedRooms[0].RoomNumber;

            if (hasResults)
            {
                var res = supplierResponse.HotelReservations.HotelReservation.First()
                    .ResGlobalInfo.HotelReservationIDs.Where(r => r.ResID_Source == "OTS");

                if (res?.Any() ?? false)
                {
                    result.ProviderBookingReference = res.First().ResID_Value;
                    result.BookingStatus = BookingStatus.Confirmed;
                }

            }
            else
            {
                //Booking Not Confirmed
                result.ProviderBookingReference = string.Empty;
                result.BookingStatus = BookingStatus.NotConfirmed;
                StringBuilder sbMessage = new StringBuilder();
                var error = supplierResponse?.Errors;
                sbMessage.AppendLine("MTS Booking Parser: Booking Response cannot be parsed because it is null.");
                if (error != null && error.Any())
                {
                    sbMessage.AppendLine("Error Details:");
                    int i = 0;
                    foreach (var e in error)
                    {
                        sbMessage.AppendLine($"Error {i}:{e.Value}");
                        i++;
                    }
                }
                logger.Error(sbMessage.ToString());
            }
            bookingResults.Enqueue(result);


        }
    }
}
