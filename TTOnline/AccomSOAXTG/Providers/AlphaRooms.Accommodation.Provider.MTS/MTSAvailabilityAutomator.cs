﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<MTSAvailabilityResponse>
    {
        private const string MTSAvailabilityUrl = "MTSAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        private readonly IMTSAvailabilityRequestFactory _MTSSupplierAvailabilityRequestFactory;
        private IProviderOutputLogger _outputLogger;

        public MTSAvailabilityAutomator(IMTSAvailabilityRequestFactory MTSAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this._MTSSupplierAvailabilityRequestFactory = MTSAvailabilityRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<MTSAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            MTSAvailabilityResponse response = null;

            try
            {
                OTA_HotelAvailRQ supplierRequest = this._MTSSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append($"Request: {Environment.NewLine}");
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("MTSAvailabilityAutomator.GetAvailabilityResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }
            return response;
        }

        private async Task<MTSAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, OTA_HotelAvailRQ  supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            OTAClient client = ConfigureSoapCleint(request);
          
            var response = await client.hotelAvailAsync(supplierRequest);
            
            var supplierResponse = new MTSAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.OTA_HotelAvailRS
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private OTAClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MTSAvailabilityUrl))); 
            OTAClient client =new OTAClient(binding, address);

            ProxyHelper helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
    }
}
