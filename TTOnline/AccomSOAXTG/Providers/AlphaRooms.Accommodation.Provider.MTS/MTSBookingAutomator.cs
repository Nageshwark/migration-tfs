﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<MTSBookingResponse>>
    {
        private const string MTSBookingUrl = "MTSBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly IProviderLoggerService loggerService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IMTSProviderBookingRequestFactory MTSProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public MTSBookingAutomator(IProviderLoggerService loggerService,
                                           IMTSProviderBookingRequestFactory MTSSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            this.loggerService = loggerService;
            this.MTSProviderBookingRequestFactory = MTSSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<MTSBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<MTSBookingResponse> responses = new List<MTSBookingResponse>();

            HotelResRequestType supplierRequest = null;
            MTSBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.MTSProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.MTSProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("MTSBookingAutomator.GetBookingResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<MTSBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, HotelResRequestType supplierRequest)
        {
            OTAClient client = ConfigureSoapCleint(request);

            if (request.Debugging)
            {
                string requesXML = supplierRequest.XmlSerialize();
            }

            var response =  await client.hotelResAsync(supplierRequest);
            this.loggerService.SetProviderRequest(request, supplierRequest);
            this.loggerService.SetProviderResponse(request, response.OTA_HotelResRS);
            var supplierResponse = new MTSBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.OTA_HotelResRS
            };
            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response);
            return supplierResponse;
        }

        private OTAClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MTSBookingUrl)));
            var client = new OTAClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
    }
}
