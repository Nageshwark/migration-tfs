﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.MTS.Helper;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS.MTSService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    public class MTSValuationAutomator : IAccommodationValuationAutomatorAsync<MTSValuationResponse>
    {
        private const string MTSValuationUrl = "MTSValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IProviderLoggerService loggerService;
        private readonly IMTSProviderValuationRequestFactory mtsProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public MTSValuationAutomator(IProviderLoggerService loggerService,
                                            IMTSProviderValuationRequestFactory MTSSupplierValuationRequestFactory,
                                                IProviderOutputLogger outputLogger)
        {
            this.loggerService = loggerService;
            this.mtsProviderValuationRequestFactory = MTSSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<MTSValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            MTSValuationResponse response = null;

            try
            {
                var supplierRequest = this.mtsProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("MTSValuationAutomator.GetValuationResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<MTSValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, HotelResRequestType supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            OTAClient client = ConfigureSoapCleint(request);

            var response = await client.hotelResAsync(supplierRequest);

            this.loggerService.SetProviderRequest(request,supplierRequest);
            this.loggerService.SetProviderResponse(request,response);

            var supplierResponse = new MTSValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.OTA_HotelResRS
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private OTAClient ConfigureSoapCleint(AccommodationProviderValuationRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));

            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MTSValuationUrl)));
            var client = new OTAClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }

    }
}
