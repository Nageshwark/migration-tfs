﻿using AlphaRooms.Accommodation.Provider.MTS.MTSService;

namespace AlphaRooms.Accommodation.Provider.MTS
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that MTS accepts and returns XML strings.
    /// </summary>
    public class MTSBookingResponse
    {
        public HotelResRequestType SupplierRequest { get; set; }
        public HotelResResponseType SupplierResponse { get; set; }
    }
}
