﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
//using Ninject.Extensions.Logging;
using System.Threading.Tasks;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecCancellationParser  : IAccommodationCancellationParserAsync<HotetecCancellationResponse>
    {
       // private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotetecCancellationParser()
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
        }


        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, HotetecCancellationResponse response)
        {
            var cancelResults = new List<AccommodationProviderCancellationResult>();
            var cancelResult = new AccommodationProviderCancellationResult();

            try
            {
                if (response.SupplierResponse.locata != null)
                {
                    cancelResult.CancellationStatus = CancellationStatus.Succeeded;
                    cancelResult.Message = "The booking: " + response.SupplierResponse.locata + " has been cancelled.";

                }
                else
                {
                    cancelResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the cancellation response from Hotetec.");
                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(request.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.XmlSerialize());

                    cancelResult.Message = sb.ToString();

                   // logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Hotetec.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            cancelResults.Add(cancelResult);

            return cancelResults;
        }

      
    }
}
