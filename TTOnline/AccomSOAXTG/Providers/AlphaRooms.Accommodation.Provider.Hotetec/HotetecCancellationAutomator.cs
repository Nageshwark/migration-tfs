﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Hotetec.Factories;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using AlphaRooms.Accommodation.Provider.Hotetec.Service;
//using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecCancellationAutomator : HotetecProviderFactoryBase, IAccommodationCancellationAutomatorAsync<HotetecCancellationResponse>
    {
        private const string HotetecCancellationUrl = "HotetecCancellationUrl";

        private readonly IWebScrapeClient webScrapeClient;
        //private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotetecProviderCancellationRequestFactory HotetecProviderCancellationRequestFactory;
        //private readonly IProviderOutputLogger outputLogger;

        public HotetecCancellationAutomator(
                                            //ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotetecProviderCancellationRequestFactory HotetecSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
            this.HotetecProviderCancellationRequestFactory = HotetecSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<HotetecCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            List<HotetecCancellationResponse> responses = new List<HotetecCancellationResponse>();
            HotetecCancellationResponse response = null;
            try
            {
                ReservaCancelarPeticion resRequest = HotetecProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                response = await GetSupplierCancellationResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("HotetecCancellationAutomator.GetCancellationResponseAsync: Hotetec supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<HotetecCancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, ReservaCancelarPeticion supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(HotetecCancellationUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<ReservaCancelarRespuesta>();

            var supplierResponse = new HotetecCancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };
            try 
	        {	        
		         disconnectsession(supplierResponse.SupplierResponse.ideses, url);
	        }
	        catch {    
	        }
           
            return supplierResponse;
        }
        private void disconnectsession(string ides, string url)
        {
            var request = new SesionCerrarPeticion();
            request.ideses = ides;
            var requestXml = request.XmlSerialize();
            var responseXml = PostXmlRequest(url, requestXml);
            var response = responseXml.Result.XmlDeSerialize<SesionCerrarRespuesta>();

        }
    }
}
