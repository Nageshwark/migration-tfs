﻿using AlphaRooms.Accommodation.Provider.HotetecService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Interfaces
{
    public interface IHotetecValuationRequestFactory
    {
        DisponibilidadHotelPeticion CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request);
    }
}
