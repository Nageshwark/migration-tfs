﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotetecService;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Interfaces
{
    public interface IHotetecAvailabilityRequestFactory
    {
        DisponibilidadHotelPeticion CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
