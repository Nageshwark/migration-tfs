﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.HotetecService;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Interfaces
{
    public interface IHotetecBookingRequestFactory
    {
        BloqueoServicioPeticion CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
        ReservaCerrarPeticion CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string sessionid, UInt16 refage);
    }
}
