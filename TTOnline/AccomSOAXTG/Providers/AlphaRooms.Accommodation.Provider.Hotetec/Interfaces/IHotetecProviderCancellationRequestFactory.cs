﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotetec.Service;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Interfaces
{
    public interface IHotetecProviderCancellationRequestFactory
    {
        ReservaCancelarPeticion CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
