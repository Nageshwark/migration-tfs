﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<HotetecAvailabilityResponse> _automator;
        private readonly IAccommodationAvailabilityParser<HotetecAvailabilityResponse> _parser;

        public HotetecAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<HotetecAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<HotetecAvailabilityResponse> parser)
        {
            this._automator = automator;
            this._parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await _automator.GetAvailabilityResponseAsync(request);
            return _parser.GetAvailabilityResults(request, responses);
        }
    }
}
