﻿using AlphaRooms.Accommodation.Provider.Hotetec.Service;
using AlphaRooms.Accommodation.Provider.HotetecService;


namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecCancellationResponse
    {
        public ReservaCancelarPeticion SupplierRequest { get; set; }
        public ReservaCancelarRespuesta SupplierResponse { get; set; }
    }
}

