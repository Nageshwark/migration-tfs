﻿using AlphaRooms.Accommodation.Provider.HotetecService;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecBookingResponse
    {
        public BloqueoServicioPeticion SupplierRequest { get; set; }
        public BloqueoServicioRespuesta SupplierResponse { get; set; }
        public ReservaCerrarPeticion SupplierCommitRequest { get; set; }
        public ReservaCerrarRespuesta SupplierCommitResponse { get; set; }
    }
}
