﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.HotetecService
{


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class DisponibilidadHotelRespuesta
    {

        private string idesesField;
        private string txterrField;

        private DisponibilidadHotelRespuestaInfhot[] infhotField;

        /// <remarks/>
        public string ideses
        {
            get
            {
                return this.idesesField;
            }
            set
            {
                this.idesesField = value;
            }
        }

        public string txterr
        {
            get { return txterrField; }
            set { txterrField = value; }
        }
        

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("infhot")]
        public DisponibilidadHotelRespuestaInfhot[] infhot
        {
            get
            {
                return this.infhotField;
            }
            set
            {
                this.infhotField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DisponibilidadHotelRespuestaInfhot
    {

        private string feciniField;

        private string fecfinField;

        private string codtseField;

        private string subtseField;

        private string nomserField;

        private string codscaField;

        private string codciaField;

        private string[] codcasField;

        private string codzgeField;

        private string coddivField;

        private decimal impbasField;

        private string codtouField;

        private ushort codserField;

        private string cupestField;

        private DisponibilidadHotelRespuestaInfhotInfhab[] infhabField;

        private byte idField;

        /// <remarks/>
        public string fecini
        {
            get
            {
                return this.feciniField;
            }
            set
            {
                this.feciniField = value;
            }
        }

        /// <remarks/>
        public string fecfin
        {
            get
            {
                return this.fecfinField;
            }
            set
            {
                this.fecfinField = value;
            }
        }

        /// <remarks/>
        public string codtse
        {
            get
            {
                return this.codtseField;
            }
            set
            {
                this.codtseField = value;
            }
        }

        /// <remarks/>
        public string subtse
        {
            get
            {
                return this.subtseField;
            }
            set
            {
                this.subtseField = value;
            }
        }

        /// <remarks/>
        public string nomser
        {
            get
            {
                return this.nomserField;
            }
            set
            {
                this.nomserField = value;
            }
        }

        /// <remarks/>
        public string codsca
        {
            get
            {
                return this.codscaField;
            }
            set
            {
                this.codscaField = value;
            }
        }

        /// <remarks/>
        public string codcia
        {
            get
            {
                return this.codciaField;
            }
            set
            {
                this.codciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("codcas")]
        public string[] codcas
        {
            get
            {
                return this.codcasField;
            }
            set
            {
                this.codcasField = value;
            }
        }

        /// <remarks/>
        public string codzge
        {
            get
            {
                return this.codzgeField;
            }
            set
            {
                this.codzgeField = value;
            }
        }

        /// <remarks/>
        public string coddiv
        {
            get
            {
                return this.coddivField;
            }
            set
            {
                this.coddivField = value;
            }
        }

        /// <remarks/>
        public decimal impbas
        {
            get
            {
                return this.impbasField;
            }
            set
            {
                this.impbasField = value;
            }
        }

        /// <remarks/>
        public string codtou
        {
            get
            {
                return this.codtouField;
            }
            set
            {
                this.codtouField = value;
            }
        }

        /// <remarks/>
        public ushort codser
        {
            get
            {
                return this.codserField;
            }
            set
            {
                this.codserField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("infhab")]
        public DisponibilidadHotelRespuestaInfhotInfhab[] infhab
        {
            get
            {
                return this.infhabField;
            }
            set
            {
                this.infhabField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DisponibilidadHotelRespuestaInfhotInfhab
    {

        private ulong codconField;

        private string codtrfField;

        private string nomtrfField;

        private string codsmoField;

        private string[] codchaField;

        private string codralField;

        private byte capmaxField;

        private byte capminField;

        private byte adlmaxField;

        private byte ninmaxField;

        private string cupestField;

        private ushort cupinvField;

        private bool cupinvFieldSpecified;

        private decimal impnocField;

        private decimal impcomField;

        private string tipaplField;

        private DisponibilidadHotelRespuestaInfhotInfhabInfmpg[] infmpgField;

        private string tippagField;

        private string codprvField;

        private string[] codttsField;

        private decimal pvpoblField;

        private bool pvpoblFieldSpecified;

        private decimal porageField;

        private bool porageFieldSpecified;

        private DisponibilidadHotelRespuestaInfhotInfhabRstcan[] rstcanField;

        private DisponibilidadHotelRespuestaInfhotInfhabInfprt infprtField;

        private uint idField;

        private byte refdisField;

        /// <remarks/>
        public ulong codcon
        {
            get
            {
                return this.codconField;
            }
            set
            {
                this.codconField = value;
            }
        }

        /// <remarks/>
        public string codtrf
        {
            get
            {
                return this.codtrfField;
            }
            set
            {
                this.codtrfField = value;
            }
        }

        /// <remarks/>
        public string nomtrf
        {
            get
            {
                return this.nomtrfField;
            }
            set
            {
                this.nomtrfField = value;
            }
        }

        /// <remarks/>
        public string codsmo
        {
            get
            {
                return this.codsmoField;
            }
            set
            {
                this.codsmoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("codcha")]
        public string[] codcha
        {
            get
            {
                return this.codchaField;
            }
            set
            {
                this.codchaField = value;
            }
        }

        /// <remarks/>
        public string codral
        {
            get
            {
                return this.codralField;
            }
            set
            {
                this.codralField = value;
            }
        }

        /// <remarks/>
        public byte capmax
        {
            get
            {
                return this.capmaxField;
            }
            set
            {
                this.capmaxField = value;
            }
        }

        /// <remarks/>
        public byte capmin
        {
            get
            {
                return this.capminField;
            }
            set
            {
                this.capminField = value;
            }
        }

        /// <remarks/>
        public byte adlmax
        {
            get
            {
                return this.adlmaxField;
            }
            set
            {
                this.adlmaxField = value;
            }
        }

        /// <remarks/>
        public byte ninmax
        {
            get
            {
                return this.ninmaxField;
            }
            set
            {
                this.ninmaxField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public ushort cupinv
        {
            get
            {
                return this.cupinvField;
            }
            set
            {
                this.cupinvField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool cupinvSpecified
        {
            get
            {
                return this.cupinvFieldSpecified;
            }
            set
            {
                this.cupinvFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public decimal impcom
        {
            get
            {
                return this.impcomField;
            }
            set
            {
                this.impcomField = value;
            }
        }

        /// <remarks/>
        public string tipapl
        {
            get
            {
                return this.tipaplField;
            }
            set
            {
                this.tipaplField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("infmpg")]
        public DisponibilidadHotelRespuestaInfhotInfhabInfmpg[] infmpg
        {
            get
            {
                return this.infmpgField;
            }
            set
            {
                this.infmpgField = value;
            }
        }

        /// <remarks/>
        public string tippag
        {
            get
            {
                return this.tippagField;
            }
            set
            {
                this.tippagField = value;
            }
        }

        /// <remarks/>
        public string codprv
        {
            get
            {
                return this.codprvField;
            }
            set
            {
                this.codprvField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("codtts")]
        public string[] codtts
        {
            get
            {
                return this.codttsField;
            }
            set
            {
                this.codttsField = value;
            }
        }

        /// <remarks/>
        public decimal pvpobl
        {
            get
            {
                return this.pvpoblField;
            }
            set
            {
                this.pvpoblField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool pvpoblSpecified
        {
            get
            {
                return this.pvpoblFieldSpecified;
            }
            set
            {
                this.pvpoblFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal porage
        {
            get
            {
                return this.porageField;
            }
            set
            {
                this.porageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool porageSpecified
        {
            get
            {
                return this.porageFieldSpecified;
            }
            set
            {
                this.porageFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("rstcan")]
        public DisponibilidadHotelRespuestaInfhotInfhabRstcan[] rstcan
        {
            get
            {
                return this.rstcanField;
            }
            set
            {
                this.rstcanField = value;
            }
        }

        /// <remarks/>
        public DisponibilidadHotelRespuestaInfhotInfhabInfprt infprt
        {
            get
            {
                return this.infprtField;
            }
            set
            {
                this.infprtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte refdis
        {
            get
            {
                return this.refdisField;
            }
            set
            {
                this.refdisField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DisponibilidadHotelRespuestaInfhotInfhabInfmpg
    {

        private string tipmpgField;

        private string tippagField;

        /// <remarks/>
        public string tipmpg
        {
            get
            {
                return this.tipmpgField;
            }
            set
            {
                this.tipmpgField = value;
            }
        }

        /// <remarks/>
        public string tippag
        {
            get
            {
                return this.tippagField;
            }
            set
            {
                this.tippagField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DisponibilidadHotelRespuestaInfhotInfhabRstcan
    {

        private string feccanField;

        private decimal impcanField;

        /// <remarks/>
        public string feccan
        {
            get
            {
                return this.feccanField;
            }
            set
            {
                this.feccanField = value;
            }
        }

        /// <remarks/>
        public decimal impcan
        {
            get
            {
                return this.impcanField;
            }
            set
            {
                this.impcanField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DisponibilidadHotelRespuestaInfhotInfhabInfprt
    {

        private string tipprtField;

        private byte valprtField;

        private bool valprtFieldSpecified;

        /// <remarks/>
        public string tipprt
        {
            get
            {
                return this.tipprtField;
            }
            set
            {
                this.tipprtField = value;
            }
        }

        /// <remarks/>
        public byte valprt
        {
            get
            {
                return this.valprtField;
            }
            set
            {
                this.valprtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool valprtSpecified
        {
            get
            {
                return this.valprtFieldSpecified;
            }
            set
            {
                this.valprtFieldSpecified = value;
            }
        }
    }




 }