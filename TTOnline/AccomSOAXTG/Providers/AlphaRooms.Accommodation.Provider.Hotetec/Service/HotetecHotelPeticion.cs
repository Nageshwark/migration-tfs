﻿namespace AlphaRooms.Accommodation.Provider.HotetecService
{
 /// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
public partial class DisponibilidadHotelPeticion {
    
    private string idesesField;
    
    private string codtouField;
    
    private string feciniField;
    
    private string fecfinField;
    
    private string[] codserField;

    private string codzgeField;
    private string[] codralField; 
    
    private DisponibilidadHotelPeticionDistri[] distriField;
    
    private string chkscmField;
    
    /// <remarks/>
    public string ideses {
        get {
            return this.idesesField;
        }
        set {
            this.idesesField = value;
        }
    }
    
    /// <remarks/>
    public string codtou {
        get {
            return this.codtouField;
        }
        set {
            this.codtouField = value;
        }
    }
    
    /// <remarks/>
    public string fecini {
        get {
            return this.feciniField;
        }
        set {
            this.feciniField = value;
        }
    }
    
    /// <remarks/>
    public string fecfin {
        get {
            return this.fecfinField;
        }
        set {
            this.fecfinField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("codser")]
    public string[] codser {
        get {
            return this.codserField;
        }
        set {
            this.codserField = value;
        }
    }

    [System.Xml.Serialization.XmlIgnore]
    public bool codserSpecified
    {
        get { return codser != null; }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("codral")]
    public string[] codral
    {
        get
        {
            return this.codralField;
        }
        set
        {
            this.codralField = value;
        }
    }
    [System.Xml.Serialization.XmlIgnore]
    public bool codralSpecified
    {
        get { return codral!=null; }
    }
       
    public string codzge
    {
        get
        {
            return this.codzgeField;
        }
        set
        {
            this.codzgeField = value;
        }
    }
    [System.Xml.Serialization.XmlIgnore]
    public bool codzgeSpecified
    {
        get { return codzge != " "; }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("distri")]
    public DisponibilidadHotelPeticionDistri[] distri {
        get {
            return this.distriField;
        }
        set {
            this.distriField = value;
        }
    }
    
    /// <remarks/>
    public string chkscm {
        get {
            return this.chkscmField;
        }
        set {
            this.chkscmField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class DisponibilidadHotelPeticionDistri {
    
    private byte numuniField;
    
    private byte numadlField;
    
    private byte numninField;
    
    private byte numbebField;
    
    private byte numancField;
    
    private int[] edaninField;
    
    private byte idField;
    
    /// <remarks/>
    public byte numuni {
        get {
            return this.numuniField;
        }
        set {
            this.numuniField = value;
        }
    }
    
    /// <remarks/>
    public byte numadl {
        get {
            return this.numadlField;
        }
        set {
            this.numadlField = value;
        }
    }
    
    /// <remarks/>
    public byte numnin {
        get {
            return this.numninField;
        }
        set {
            this.numninField = value;
        }
    }
    
    /// <remarks/>
    public byte numbeb {
        get {
            return this.numbebField;
        }
        set {
            this.numbebField = value;
        }
    }
    
    /// <remarks/>
    public byte numanc {
        get {
            return this.numancField;
        }
        set {
            this.numancField = value;
        }
    }
    
    /// <remarks/>
     [System.Xml.Serialization.XmlElement("Edanin")]
    public int[] Edanin {
        get {
            return this.edaninField;
        }
        set {
            this.edaninField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public byte id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
}
   
}
