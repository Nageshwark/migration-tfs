﻿
namespace AlphaRooms.Accommodation.Provider.HotetecService
{
    using System.Xml.Serialization;
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SesionAbrirPeticion
    {

        private string codsysField;

        private string codageField;

        private string idtusuField;

        private string pasusuField;

        /// <remarks/>
        public string codsys
        {
            get
            {
                return this.codsysField;
            }
            set
            {
                this.codsysField = value;
            }
        }

        /// <remarks/>
        public string codage
        {
            get
            {
                return this.codageField;
            }
            set
            {
                this.codageField = value;
            }
        }

        /// <remarks/>
        public string idtusu
        {
            get
            {
                return this.idtusuField;
            }
            set
            {
                this.idtusuField = value;
            }
        }

        /// <remarks/>
        public string pasusu
        {
            get
            {
                return this.pasusuField;
            }
            set
            {
                this.pasusuField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SesionCerrarPeticion
    {
        private string idesesField;

        /// <remarks/>
        public string ideses
        {
            get
            {
                return this.idesesField;
            }
            set
            {
                this.idesesField = value;
            }
        }
    }
   
}