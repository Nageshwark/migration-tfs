﻿namespace AlphaRooms.Accommodation.Provider.HotetecService
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class BloqueoServicioPeticion
    {

        private string idesesField;

        private string codtouField;

        private BloqueoServicioPeticionAdl[] pasageField;

        private BloqueoServicioPeticionBloser bloserField;

        private string accionField;

        /// <remarks/>
        public string ideses
        {
            get
            {
                return this.idesesField;
            }
            set
            {
                this.idesesField = value;
            }
        }

        /// <remarks/>
        public string codtou
        {
            get
            {
                return this.codtouField;
            }
            set
            {
                this.codtouField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("adl", IsNullable = false)]
        public BloqueoServicioPeticionAdl[] pasage
        {
            get
            {
                return this.pasageField;
            }
            set
            {
                this.pasageField = value;
            }
        }

        /// <remarks/>
        public BloqueoServicioPeticionBloser bloser
        {
            get
            {
                return this.bloserField;
            }
            set
            {
                this.bloserField = value;
            }
        }

        /// <remarks/>
        public string accion
        {
            get
            {
                return this.accionField;
            }
            set
            {
                this.accionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioPeticionAdl
    {

        private string fecnacField;

        private byte idField;

        /// <remarks/>
        public string fecnac
        {
            get
            {
                return this.fecnacField;
            }
            set
            {
                this.fecnacField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioPeticionBloser
    {

        private BloqueoServicioPeticionBloserDissmo dissmoField;

        private byte idField;

        /// <remarks/>
        public BloqueoServicioPeticionBloserDissmo dissmo
        {
            get
            {
                return this.dissmoField;
            }
            set
            {
                this.dissmoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioPeticionBloserDissmo
    {

        private byte numuniField;

        private int[] pasidField;

        private uint idField;

        /// <remarks/>
        public byte numuni
        {
            get
            {
                return this.numuniField;
            }
            set
            {
                this.numuniField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("pasid")]
        public int[] pasid
        {
            get
            {
                return this.pasidField;
            }
            set
            {
                this.pasidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
    
}
