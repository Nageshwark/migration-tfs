﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.HotetecService
{
        /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ReservaCerrarRespuesta
    {

        private string idesesField;

        private string locataField;

        private ushort refageField;

        private string cupestField;

        private string estadoField;

        private string feciniField;

        private string fecfinField;

        private string codtouField;

        private string coddivField;

        private decimal impnocField;

        private ReservaCerrarRespuestaRespas[] respasField;

        private ReservaCerrarRespuestaResser resserField;

        private ReservaCerrarRespuestaPercon perconField;

        /// <remarks/>
        public string ideses
        {
            get
            {
                return this.idesesField;
            }
            set
            {
                this.idesesField = value;
            }
        }

        /// <remarks/>
        public string locata
        {
            get
            {
                return this.locataField;
            }
            set
            {
                this.locataField = value;
            }
        }

        /// <remarks/>
        public ushort refage
        {
            get
            {
                return this.refageField;
            }
            set
            {
                this.refageField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public string estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
            }
        }

        /// <remarks/>
        public string fecini
        {
            get
            {
                return this.feciniField;
            }
            set
            {
                this.feciniField = value;
            }
        }

        /// <remarks/>
        public string fecfin
        {
            get
            {
                return this.fecfinField;
            }
            set
            {
                this.fecfinField = value;
            }
        }

        /// <remarks/>
        public string codtou
        {
            get
            {
                return this.codtouField;
            }
            set
            {
                this.codtouField = value;
            }
        }

        /// <remarks/>
        public string coddiv
        {
            get
            {
                return this.coddivField;
            }
            set
            {
                this.coddivField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("respas")]
        public ReservaCerrarRespuestaRespas[] respas
        {
            get
            {
                return this.respasField;
            }
            set
            {
                this.respasField = value;
            }
        }

        /// <remarks/>
        public ReservaCerrarRespuestaResser resser
        {
            get
            {
                return this.resserField;
            }
            set
            {
                this.resserField = value;
            }
        }

        /// <remarks/>
        public ReservaCerrarRespuestaPercon percon
        {
            get
            {
                return this.perconField;
            }
            set
            {
                this.perconField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaRespas
    {

        private string fecnacField;

        private string tippasField;

        private byte idField;

        /// <remarks/>
        public string fecnac
        {
            get
            {
                return this.fecnacField;
            }
            set
            {
                this.fecnacField = value;
            }
        }

        /// <remarks/>
        public string tippas
        {
            get
            {
                return this.tippasField;
            }
            set
            {
                this.tippasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaResser
    {

        private string feciniField;

        private string fecfinField;

        private string nomserField;

        private string codscaField;

        private string codciaField;

        private string codzgeField;

        private string codtseField;

        private string subtseField;

        private ushort codserField;

        private decimal impnocField;

        private ReservaCerrarRespuestaResserEstsmo estsmoField;

        private ushort idField;

        /// <remarks/>
        public string fecini
        {
            get
            {
                return this.feciniField;
            }
            set
            {
                this.feciniField = value;
            }
        }

        /// <remarks/>
        public string fecfin
        {
            get
            {
                return this.fecfinField;
            }
            set
            {
                this.fecfinField = value;
            }
        }

        /// <remarks/>
        public string nomser
        {
            get
            {
                return this.nomserField;
            }
            set
            {
                this.nomserField = value;
            }
        }

        /// <remarks/>
        public string codsca
        {
            get
            {
                return this.codscaField;
            }
            set
            {
                this.codscaField = value;
            }
        }

        /// <remarks/>
        public string codcia
        {
            get
            {
                return this.codciaField;
            }
            set
            {
                this.codciaField = value;
            }
        }

        /// <remarks/>
        public string codzge
        {
            get
            {
                return this.codzgeField;
            }
            set
            {
                this.codzgeField = value;
            }
        }

        /// <remarks/>
        public string codtse
        {
            get
            {
                return this.codtseField;
            }
            set
            {
                this.codtseField = value;
            }
        }

        /// <remarks/>
        public string subtse
        {
            get
            {
                return this.subtseField;
            }
            set
            {
                this.subtseField = value;
            }
        }

        /// <remarks/>
        public ushort codser
        {
            get
            {
                return this.codserField;
            }
            set
            {
                this.codserField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public ReservaCerrarRespuestaResserEstsmo estsmo
        {
            get
            {
                return this.estsmoField;
            }
            set
            {
                this.estsmoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaResserEstsmo
    {

        private string codconField;

        private string codsmoField;

        private string codchaField;

        private string codralField;

        private string cupestField;

        private string estadoField;

        private byte numuniField;

        private decimal impnocField;

        private decimal impcomField;

        private string codprvField;

        private string locataField;

        private ReservaCerrarRespuestaResserEstsmoRstcan[] rstcanField;

        private ReservaCerrarRespuestaResserEstsmoEstpas estpasField;

        private uint idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string codcon
        {
            get
            {
                return this.codconField;
            }
            set
            {
                this.codconField = value;
            }
        }

        /// <remarks/>
        public string codsmo
        {
            get
            {
                return this.codsmoField;
            }
            set
            {
                this.codsmoField = value;
            }
        }

        /// <remarks/>
        public string codcha
        {
            get
            {
                return this.codchaField;
            }
            set
            {
                this.codchaField = value;
            }
        }

        /// <remarks/>
        public string codral
        {
            get
            {
                return this.codralField;
            }
            set
            {
                this.codralField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public string estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
            }
        }

        /// <remarks/>
        public byte numuni
        {
            get
            {
                return this.numuniField;
            }
            set
            {
                this.numuniField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public decimal impcom
        {
            get
            {
                return this.impcomField;
            }
            set
            {
                this.impcomField = value;
            }
        }

        /// <remarks/>
        public string codprv
        {
            get
            {
                return this.codprvField;
            }
            set
            {
                this.codprvField = value;
            }
        }

        /// <remarks/>
        public string locata
        {
            get
            {
                return this.locataField;
            }
            set
            {
                this.locataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("rstcan")]
        public ReservaCerrarRespuestaResserEstsmoRstcan[] rstcan
        {
            get
            {
                return this.rstcanField;
            }
            set
            {
                this.rstcanField = value;
            }
        }

        /// <remarks/>
        public ReservaCerrarRespuestaResserEstsmoEstpas estpas
        {
            get
            {
                return this.estpasField;
            }
            set
            {
                this.estpasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaResserEstsmoRstcan
    {

        private string feccanField;

        private decimal impcanField;

        private string txtinfField;

        /// <remarks/>
        public string feccan
        {
            get
            {
                return this.feccanField;
            }
            set
            {
                this.feccanField = value;
            }
        }

        /// <remarks/>
        public decimal impcan
        {
            get
            {
                return this.impcanField;
            }
            set
            {
                this.impcanField = value;
            }
        }

        /// <remarks/>
        public string txtinf
        {
            get
            {
                return this.txtinfField;
            }
            set
            {
                this.txtinfField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaResserEstsmoEstpas
    {

        private byte[] pasidField;

        private string cupestField;

        private decimal impnocField;

        private decimal impcomField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("pasid")]
        public byte[] pasid
        {
            get
            {
                return this.pasidField;
            }
            set
            {
                this.pasidField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public decimal impcom
        {
            get
            {
                return this.impcomField;
            }
            set
            {
                this.impcomField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ReservaCerrarRespuestaPercon
    {

        private string nombreField;

        private string priapeField;

        private uint telField;

        /// <remarks/>
        public string nombre
        {
            get
            {
                return this.nombreField;
            }
            set
            {
                this.nombreField = value;
            }
        }

        /// <remarks/>
        public string priape
        {
            get
            {
                return this.priapeField;
            }
            set
            {
                this.priapeField = value;
            }
        }

        /// <remarks/>
        public uint tel
        {
            get
            {
                return this.telField;
            }
            set
            {
                this.telField = value;
            }
        }
    }


}
