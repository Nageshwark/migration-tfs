﻿using System.Xml.Serialization;
namespace AlphaRooms.Accommodation.Provider.HotetecService
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class BloqueoServicioRespuesta
    {

        private string idesesField;

        private string codtouField;

        private string cupestField;

        private string coddivField;

        private decimal impnocField;

        private BloqueoServicioRespuestaAdl[] pasageField;

        private BloqueoServicioRespuestaResser resserField;

        /// <remarks/>
        public string ideses
        {
            get
            {
                return this.idesesField;
            }
            set
            {
                this.idesesField = value;
            }
        }

        /// <remarks/>
        public string codtou
        {
            get
            {
                return this.codtouField;
            }
            set
            {
                this.codtouField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public string coddiv
        {
            get
            {
                return this.coddivField;
            }
            set
            {
                this.coddivField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("adl", IsNullable = false)]
        public BloqueoServicioRespuestaAdl[] pasage
        {
            get
            {
                return this.pasageField;
            }
            set
            {
                this.pasageField = value;
            }
        }

        /// <remarks/>
        public BloqueoServicioRespuestaResser resser
        {
            get
            {
                return this.resserField;
            }
            set
            {
                this.resserField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioRespuestaAdl
    {

        private string fecnacField;

        private byte idField;

        /// <remarks/>
        public string fecnac
        {
            get
            {
                return this.fecnacField;
            }
            set
            {
                this.fecnacField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioRespuestaResser
    {

        private string feciniField;

        private string fecfinField;

        private string nomserField;

        private string codscaField;

        private string codciaField;

        private string codzgeField;

        private string codtseField;

        private string subtseField;

        private ushort codserField;

        private decimal impnocField;

        private BloqueoServicioRespuestaResserEstsmo estsmoField;

        private ushort idField;

        /// <remarks/>
        public string fecini
        {
            get
            {
                return this.feciniField;
            }
            set
            {
                this.feciniField = value;
            }
        }

        /// <remarks/>
        public string fecfin
        {
            get
            {
                return this.fecfinField;
            }
            set
            {
                this.fecfinField = value;
            }
        }

        /// <remarks/>
        public string nomser
        {
            get
            {
                return this.nomserField;
            }
            set
            {
                this.nomserField = value;
            }
        }

        /// <remarks/>
        public string codsca
        {
            get
            {
                return this.codscaField;
            }
            set
            {
                this.codscaField = value;
            }
        }

        /// <remarks/>
        public string codcia
        {
            get
            {
                return this.codciaField;
            }
            set
            {
                this.codciaField = value;
            }
        }

        /// <remarks/>
        public string codzge
        {
            get
            {
                return this.codzgeField;
            }
            set
            {
                this.codzgeField = value;
            }
        }

        /// <remarks/>
        public string codtse
        {
            get
            {
                return this.codtseField;
            }
            set
            {
                this.codtseField = value;
            }
        }

        /// <remarks/>
        public string subtse
        {
            get
            {
                return this.subtseField;
            }
            set
            {
                this.subtseField = value;
            }
        }

        /// <remarks/>
        public ushort codser
        {
            get
            {
                return this.codserField;
            }
            set
            {
                this.codserField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public BloqueoServicioRespuestaResserEstsmo estsmo
        {
            get
            {
                return this.estsmoField;
            }
            set
            {
                this.estsmoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioRespuestaResserEstsmo
    {

        private string codconField;

        private string codsmoField;

        private string codchaField;

        private string codralField;

        private string cupestField;

        private string estadoField;

        private byte numuniField;

        private decimal impnocField;

        private decimal impcomField;

        private string codprvField;

        private BloqueoServicioRespuestaResserEstsmoRstcan[] rstcanField;

        private BloqueoServicioRespuestaResserEstsmoEstpas estpasField;

        private uint idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string codcon
        {
            get
            {
                return this.codconField;
            }
            set
            {
                this.codconField = value;
            }
        }

        /// <remarks/>
        public string codsmo
        {
            get
            {
                return this.codsmoField;
            }
            set
            {
                this.codsmoField = value;
            }
        }

        /// <remarks/>
        public string codcha
        {
            get
            {
                return this.codchaField;
            }
            set
            {
                this.codchaField = value;
            }
        }

        /// <remarks/>
        public string codral
        {
            get
            {
                return this.codralField;
            }
            set
            {
                this.codralField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public string estado
        {
            get
            {
                return this.estadoField;
            }
            set
            {
                this.estadoField = value;
            }
        }

        /// <remarks/>
        public byte numuni
        {
            get
            {
                return this.numuniField;
            }
            set
            {
                this.numuniField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }
               
        /// <remarks/>
        public decimal impcom
        {
            get
            {
                return this.impcomField;
            }
            set
            {
                this.impcomField = value;
            }
        }

       
        /// <remarks/>
        public string codprv
        {
            get
            {
                return this.codprvField;
            }
            set
            {
                this.codprvField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("rstcan")]
        public BloqueoServicioRespuestaResserEstsmoRstcan[] rstcan
        {
            get
            {
                return this.rstcanField;
            }
            set
            {
                this.rstcanField = value;
            }
        }

        /// <remarks/>
        public BloqueoServicioRespuestaResserEstsmoEstpas estpas
        {
            get
            {
                return this.estpasField;
            }
            set
            {
                this.estpasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioRespuestaResserEstsmoRstcan
    {

        private string feccanField;

        private decimal impcanField;

        private string txtinfField;

        /// <remarks/>
        public string feccan
        {
            get
            {
                return this.feccanField;
            }
            set
            {
                this.feccanField = value;
            }
        }

        /// <remarks/>
        public decimal impcan
        {
            get
            {
                return this.impcanField;
            }
            set
            {
                this.impcanField = value;
            }
        }

        /// <remarks/>
        public string txtinf
        {
            get
            {
                return this.txtinfField;
            }
            set
            {
                this.txtinfField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BloqueoServicioRespuestaResserEstsmoEstpas
    {

        private byte[] pasidField;

        private string cupestField;

        private decimal impnocField;

        private decimal impcomField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("pasid")]
        public byte[] pasid
        {
            get
            {
                return this.pasidField;
            }
            set
            {
                this.pasidField = value;
            }
        }

        /// <remarks/>
        public string cupest
        {
            get
            {
                return this.cupestField;
            }
            set
            {
                this.cupestField = value;
            }
        }

        /// <remarks/>
        public decimal impnoc
        {
            get
            {
                return this.impnocField;
            }
            set
            {
                this.impnocField = value;
            }
        }

        /// <remarks/>
        public decimal impcom
        {
            get
            {
                return this.impcomField;
            }
            set
            {
                this.impcomField = value;
            }
        }

    }
   }

