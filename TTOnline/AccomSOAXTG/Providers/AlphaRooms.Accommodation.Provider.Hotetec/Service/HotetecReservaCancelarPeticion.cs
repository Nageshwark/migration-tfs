﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Service
{
  
       /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class ReservaCancelarPeticion
        {

            private string idesesField;

            private string codtouField;

            private string locataField;

            private string accionField;

            /// <remarks/>
            public string ideses
            {
                get
                {
                    return this.idesesField;
                }
                set
                {
                    this.idesesField = value;
                }
            }

            /// <remarks/>
            public string codtou
            {
                get
                {
                    return this.codtouField;
                }
                set
                {
                    this.codtouField = value;
                }
            }

            /// <remarks/>
            public string locata
            {
                get
                {
                    return this.locataField;
                }
                set
                {
                    this.locataField = value;
                }
            }

            /// <remarks/>
            public string accion
            {
                get
                {
                    return this.accionField;
                }
                set
                {
                    this.accionField = value;
                }
            }
        }
   
}
