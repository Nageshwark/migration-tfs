﻿
using System.Xml.Serialization;
namespace AlphaRooms.Accommodation.Provider.HotetecService
{
 /// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class SesionAbrirRespuesta
{
    private string idesesField;

    /// <remarks/>
    public string ideses
    {
        get
        {
            return this.idesesField;
        }
        set
        {
            this.idesesField = value;
        }
    }
}
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class SesionCerrarRespuesta
{
    private string idesesField;

    /// <remarks/>
    public string ideses
    {
        get
        {
            return this.idesesField;
        }
        set
        {
            this.idesesField = value;
        }
    }
}

}

