﻿namespace AlphaRooms.Accommodation.Provider.HotetecService
{
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class ReservaCerrarPeticion
{

    private string idesesField;

    private string codtouField;

    private ReservaCerrarPeticionPercon perconField;

    private ushort refageField;

    private string accionField;

    /// <remarks/>
    
    public string ideses
    {
        get
        {
            return this.idesesField;
        }
        set
        {
            this.idesesField = value;
        }
    }

    /// <remarks/>
    public string codtou
    {
        get
        {
            return this.codtouField;
        }
        set
        {
            this.codtouField = value;
        }
    }

    /// <remarks/>
    public ReservaCerrarPeticionPercon percon
    {
        get
        {
            return this.perconField;
        }
        set
        {
            this.perconField = value;
        }
    }

    /// <remarks/>
    public ushort refage
    {
        get
        {
            return this.refageField;
        }
        set
        {
            this.refageField = value;
        }
    }

    /// <remarks/>
    public string accion
    {
        get
        {
            return this.accionField;
        }
        set
        {
            this.accionField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReservaCerrarPeticionPercon
{

    private string nombreField;

    private string priapeField;

    private uint telField;

    /// <remarks/>
    public string nombre
    {
        get
        {
            return this.nombreField;
        }
        set
        {
            this.nombreField = value;
        }
    }

    /// <remarks/>
    public string priape
    {
        get
        {
            return this.priapeField;
        }
        set
        {
            this.priapeField = value;
        }
    }

    /// <remarks/>
    public uint tel
    {
        get
        {
            return this.telField;
        }
        set
        {
            this.telField = value;
        }
    }
}


  
}
