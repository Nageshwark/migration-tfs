﻿using System;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
//using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
using System.Linq;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Provider.HotetecService;
namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecAvailabilityParser : IAccommodationAvailabilityParser<HotetecAvailabilityResponse>
    {
        //private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotetecAvailabilityParser()
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, HotetecAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotetec Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, HotetecAvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                var hotelList = supplierResponse.SupplierResponse.infhot;
                foreach (var hotel in hotelList)
                {

                    if (hotel.cupest == "DS")
                    {
                        foreach (var roomInfo in hotel.infhab)
                        {
                            var result = new AccommodationProviderAvailabilityResult();
                            result.RoomNumber = roomInfo.refdis;
                            result.DestinationEdiCode = hotel.codzge;
                            result.EstablishmentEdiCode = hotel.codser.ToString();
                            result.EstablishmentName = hotel.nomser;
                            result.RoomDescription = roomInfo.codsmo;
                            result.RoomCode = roomInfo.codcon.ToString();
                            result.CheckInDate = Getdate(hotel.fecini);
                            result.CheckOutDate = Getdate(hotel.fecfin);
                            result.BoardCode = roomInfo.codral;
                            result.BoardDescription = "";
                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.impcom > 0 ? roomInfo.impcom : roomInfo.impnoc),
                                CurrencyCode = hotel.coddiv
                            };

                            result.CostPrice = result.SalePrice;
                            result.Adults = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numadl;
                            result.Children = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numnin;
                            result.Infants = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numbeb;
                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("HotelId", hotel.id.ToString());
                            result.ProviderSpecificData.Add("RoomId", roomInfo.id.ToString());
                            result.ProviderSpecificData.Add("ideses", supplierResponse.SupplierResponse.ideses.ToString());
                            result.ProviderSpecificData.Add("Roomcodcon", roomInfo.codcon.ToString());
                            availabilityResults.Enqueue(result);

                        }

                    }
                }

            }
            else
            {
                if (supplierResponse.SupplierResponse != null && !string.IsNullOrEmpty(supplierResponse.SupplierResponse.txterr))
                {
                    var sbMessage = new StringBuilder();
                    
                    sbMessage.AppendLine("Hotetec Availability Parser: Availability retruned error.");
                    sbMessage.AppendLine(string.Format("Details: {0}", supplierResponse.SupplierResponse.txterr));
                    
                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }
        }
              
        private static bool HasResults(DisponibilidadHotelRespuesta supplierResponse)
        {
            if (supplierResponse != null && supplierResponse.infhot != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
       
        /// Convert Date time
        /// </summary>
        /// <param name="inputDate"></param>
        /// <returns></returns>
        public  DateTime Getdate(string inputDate)
        {
            string[] inputDatearray = inputDate.Split('/');
            int year = Convert.ToInt32(inputDatearray[2]);
            int month = Convert.ToInt32(inputDatearray[1]);
            int days = Convert.ToInt32(inputDatearray[0]);
            
            DateTime dtInputDate = new DateTime(year, month, days, 0, 0, 0);
            
            return dtInputDate;

        }
    }
}
