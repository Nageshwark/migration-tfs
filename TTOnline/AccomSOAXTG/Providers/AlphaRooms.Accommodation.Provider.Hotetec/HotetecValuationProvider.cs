﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Hotetec;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<HotetecValuationResponse> automator;
        private readonly IAccommodationValuationParser<HotetecValuationResponse> parser;

        public HotetecValuationProvider(IAccommodationValuationAutomatorAsync<HotetecValuationResponse> automator,IAccommodationValuationParser<HotetecValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
