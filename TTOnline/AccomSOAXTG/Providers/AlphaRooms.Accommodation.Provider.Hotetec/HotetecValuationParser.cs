﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotetec;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
//using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{

    public class HotetecValuationParser : IAccommodationValuationParser<HotetecValuationResponse>
    {
        //private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotetecValuationParser(
            //ILogger logger
            )
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, HotetecValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotetec Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, HotetecValuationResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                var reservation = supplierResponse.SupplierResponse.infhot.First();
                if (reservation.cupest == "DS")
                {
                    foreach (var roomInfo in reservation.infhab)
                    {

                        decimal price = Convert.ToDecimal(roomInfo.impnoc > 0 ? roomInfo.impnoc : roomInfo.impcom);
                        var selectedRoom = GetSelectedRoom(request.SelectedRooms, price, roomInfo.codral);
                        
                        if (selectedRoom != null)
                        {
                            //throw new SupplierApiDataException("Hotetec Valuation Parser: Unable to match the room type and Pax from  the response");
                            var result = new AccommodationProviderValuationResult();

                            result.RoomNumber = roomInfo.refdis;
                            result.DestinationEdiCode = reservation.codzge;
                            result.EstablishmentEdiCode = reservation.codser.ToString();
                            result.EstablishmentName = reservation.nomser;
                            result.RoomDescription = roomInfo.codsmo;
                            result.RoomCode = roomInfo.codcon.ToString();
                            result.CheckInDate = Getdate(reservation.fecini);
                            result.CheckOutDate = Getdate(reservation.fecfin);
                            result.BoardCode = roomInfo.codral;
                            result.BoardDescription = "";
                            
                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.impcom > 0 ? roomInfo.impcom : roomInfo.impnoc),
                                CurrencyCode = reservation.coddiv
                            };
                            result.CostPrice = result.SalePrice;
                            
                            result.Adults = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numadl;
                            result.Children = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numnin;
                            result.Infants = (byte) supplierResponse.SupplierRequest.distri[roomInfo.refdis - 1].numbeb;
                            
                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("HotelId", reservation.id.ToString());
                            result.ProviderSpecificData.Add("RoomId", roomInfo.id.ToString());
                            result.ProviderSpecificData.Add("ideses", supplierResponse.SupplierResponse.ideses.ToString());
                            
                            availabilityResults.Enqueue(result);
                        }
                    }

                }
            }
            else
            {
                if (supplierResponse.SupplierResponse != null && !string.IsNullOrEmpty(supplierResponse.SupplierResponse.txterr))
                {
                    var sbMessage = new StringBuilder();
                    
                    sbMessage.AppendLine("Hotetec Valuation Parser: Availability retruned error.");
                    sbMessage.AppendLine(string.Format("Details: {0}", supplierResponse.SupplierResponse.txterr));
                    
                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }
        }

        private AccommodationProviderValuationRequestRoom GetSelectedRoom(AccommodationProviderValuationRequestRoom[] selectedRooms, decimal price, string boardCode)
        {
                  var room = from r in selectedRooms
                       where r.AvailabilityResult.SalePrice.Amount ==  price &&
                            r.AvailabilityResult.BoardCode == boardCode 
                       select r;


            return room.FirstOrDefault();
        }
        private static bool HasResults(DisponibilidadHotelRespuesta supplierResponse)
        {
            if (supplierResponse != null && supplierResponse.infhot != null)
            {
                return true;

            }
            else
            {
                return false;
            }
        }
        public DateTime Getdate(string inputDate)
        {
            string[] inputDatearray = inputDate.Split('/');
            int year = Convert.ToInt32(inputDatearray[2]);
            int month = Convert.ToInt32(inputDatearray[1]);
            int days = Convert.ToInt32(inputDatearray[0]);
            var dtInputDate = new DateTime(year, month, days, 0, 0, 0);

            return dtInputDate;

        }
    }
}
