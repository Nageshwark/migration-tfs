﻿
using AlphaRooms.Accommodation.Provider.HotetecService;


namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecValuationResponse
    {
        public DisponibilidadHotelPeticion SupplierRequest { get; set; }
        public DisponibilidadHotelRespuesta SupplierResponse { get; set; }
    }
}
