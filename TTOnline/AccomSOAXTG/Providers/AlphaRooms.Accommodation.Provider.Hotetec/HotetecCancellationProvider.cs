﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<HotetecCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<HotetecCancellationResponse> parser;

        public HotetecCancellationProvider(IAccommodationCancellationAutomatorAsync<HotetecCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<HotetecCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
}
