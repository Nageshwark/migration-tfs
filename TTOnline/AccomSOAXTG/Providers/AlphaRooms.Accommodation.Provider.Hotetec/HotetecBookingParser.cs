﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
//using Ninject.Extensions.Logging;
using System.Collections.Concurrent;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecBookingParser : IAccommodationBookingParser<HotetecBookingResponse>
    {
        //private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotetecBookingParser()
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request,
            HotetecBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                CreateBookingResultsFromResponse(request, responses.SupplierCommitRequest, responses.SupplierCommitResponse, bookingResults);

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotetec Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                ReservaCerrarPeticion supplierRequest,
                                                ReservaCerrarRespuesta supplierResponse,
                                                ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null &&
                                supplierResponse.cupest != null 
                               );

            if (hasResults)
            {
                string bookingReference = "";
                
                var result = new AccommodationProviderBookingResult();

                result.BookingStatus = BookingStatus.Confirmed;
                result.ProviderBookingReference = supplierResponse.locata;
                result.Message = "";
                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("refage", supplierResponse.refage.ToString());
                bookingResults.Enqueue(result);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from Hotetec.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Booking Response:");
                sb.AppendLine(supplierResponse.XmlSerialize());

                throw new SupplierApiException(sb.ToString());

            }
        }
    }
}
