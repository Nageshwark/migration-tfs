﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
//using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.Hotetec.Factories;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecValuationAutomator : HotetecProviderFactoryBase, IAccommodationValuationAutomatorAsync<HotetecValuationResponse>
    {
        private const string HotetecAvailabilityUrl = "HotetecAvailabilityUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly IHotetecValuationRequestFactory _hotetecProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public HotetecValuationAutomator(ILogger logger, IAccommodationConfigurationManager configurationManager, IHotetecValuationRequestFactory hotetecSupplierValuationRequestFactory, IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._hotetecProviderValuationRequestFactory = hotetecSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<HotetecValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            HotetecValuationResponse response = null;
            try
            {
                DisponibilidadHotelPeticion supplierRequest = this._hotetecProviderValuationRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("HotetecValuationAutomator.GetValuationResponseAsync: Hotetec supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }

            return response;
        }

        private async Task<HotetecValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, DisponibilidadHotelPeticion supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(HotetecAvailabilityUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<DisponibilidadHotelRespuesta>();

            var supplierResponse = new HotetecValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
    }
}
