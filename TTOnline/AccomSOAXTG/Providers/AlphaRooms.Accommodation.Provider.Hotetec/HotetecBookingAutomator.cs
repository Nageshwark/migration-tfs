﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
//using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.Accommodation.Provider.Hotetec.Factories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecBookingAutomator : HotetecProviderFactoryBase, IAccommodationBookingAutomatorAsync<HotetecBookingResponse>
    {
        private const string HotetecBookingUrl = "HotetecBookingUrl";


        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly IHotetecBookingRequestFactory _hotetecSupplierBookingRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public HotetecBookingAutomator(ILogger logger, IHotetecBookingRequestFactory hotetecSupplierBookingRequestFactory, IProviderOutputLogger outputLogger)
        {
            this._logger = logger;
            //this._configurationManager = _configurationManager;
            this._hotetecSupplierBookingRequestFactory = hotetecSupplierBookingRequestFactory;
            this._outputLogger = outputLogger;
        }
        public async Task<HotetecBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<HotetecBookingResponse> responses = new List<HotetecBookingResponse>();
            HotetecBookingResponse InitiateResponse = null;
            ReservaCerrarPeticion CommitRequest = null;
            HotetecBookingResponse Commitresponse = null;
            try
            {
                //Step 1: Booking Initiate
                BloqueoServicioPeticion InitiateRequest = _hotetecSupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
                InitiateResponse = await GetSupplierBookingResponseAsync(request, InitiateRequest, CommitRequest, 1);


                //Step 2: Booking Commit
                bool hasResults = (
                               InitiateResponse != null &&
                               InitiateResponse.SupplierResponse.cupest != null
                              );

                if (hasResults)
                {
                    ushort bookRef = InitiateResponse.SupplierResponse.resser.id;
                    string sessionId = InitiateResponse.SupplierResponse.ideses;
                    CommitRequest = _hotetecSupplierBookingRequestFactory.CreateSupplierBookingCommitRequest(request, sessionId, bookRef);
                    Commitresponse = await GetSupplierBookingResponseAsync(request, InitiateRequest, CommitRequest, 2);

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (InitiateResponse != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(InitiateResponse.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(InitiateResponse.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }


                throw new SupplierApiException(string.Format("HotetecBookingAutomator.GetBookingResponseAsync: Hotetec supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return Commitresponse;
        }


        private async Task<HotetecBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, BloqueoServicioPeticion supplieriniRequest, ReservaCerrarPeticion supplierComRequest, int mode)
        {
            string url = request.Provider.Parameters.GetParameterValue(HotetecBookingUrl);

            string supplierRequestXml = mode == 1 ? supplieriniRequest.XmlSerialize() : supplierComRequest.XmlSerialize();


            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);


            //var response = mode == 1 ? : responseXml.Result.XmlDeSerialize<ReservaCerrarRespuesta>();
            var supplierResponse = new HotetecBookingResponse();
            if (mode == 1)
            {
                var response = responseXml.Result.XmlDeSerialize<BloqueoServicioRespuesta>();
                supplierResponse.SupplierRequest = supplieriniRequest;
                supplierResponse.SupplierResponse = response;
            }
            else
            {
                var response1 = responseXml.Result.XmlDeSerialize<ReservaCerrarRespuesta>();
                supplierResponse.SupplierCommitRequest = supplierComRequest;
                supplierResponse.SupplierCommitResponse = response1;
            }
            if (mode == 2)
            {
                disconnectsession(supplierResponse.SupplierCommitResponse.ideses, url);
            }

            if (_outputLogger != null)
                _outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private void disconnectsession(string ides, string url)
        {
            var request = new SesionCerrarPeticion();
            request.ideses = ides;
            var requestXml = request.XmlSerialize();
            var responseXml = PostXmlRequest(url, requestXml);
            var response = responseXml.Result.XmlDeSerialize<SesionCerrarRespuesta>();

        }
    }
}