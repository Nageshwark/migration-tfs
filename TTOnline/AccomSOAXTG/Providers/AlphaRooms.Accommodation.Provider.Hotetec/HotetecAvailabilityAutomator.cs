﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Hotetec.Factories;
using AlphaRooms.Accommodation.Provider.HotetecService;

namespace AlphaRooms.Accommodation.Provider.Hotetec
{
    public class HotetecAvailabilityAutomator : HotetecProviderFactoryBase, IAccommodationAvailabilityAutomatorAsync<HotetecAvailabilityResponse>
    {
        private const string HotetecAvailabilityUrl = "HotetecAvailabilityUrl";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IHotetecAvailabilityRequestFactory _HotetecSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;
        public HotetecAvailabilityAutomator(IHotetecAvailabilityRequestFactory HotetecAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            _HotetecSupplierAvailabilityRequestFactory = HotetecAvailabilityRequestFactory;
            _outputLogger = outputLogger;
        }

        public async Task<HotetecAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            HotetecAvailabilityResponse response = null;

            try
            {
                DisponibilidadHotelPeticion supplierRequest = this._HotetecSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
             
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("Hotetec AvailabilityAutomator.GetAvailabilityResponseAsync: RMI supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<HotetecAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, DisponibilidadHotelPeticion supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(HotetecAvailabilityUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);
            var response = responseXml.Result.XmlDeSerialize<DisponibilidadHotelRespuesta>();

            var supplierResponse = new HotetecAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml,  supplierResponse);

            return supplierResponse;
        }

       

    }
}
