﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;



namespace AlphaRooms.Accommodation.Provider.Hotetec.Factories
{
    public class HotetecBookingRequestFactory : HotetecProviderFactoryBase, IHotetecBookingRequestFactory
    {
        private const string Hotetecaccion = "Hotetecaccion";
        private const string Hoteteccodtou = "Hoteteccodtou";
        private const string Hotetecfecnac = "fecnac";
        private const string HoteteCancaccion = "HoteteCancaccion";
        private const string Hotetecnombre = "Hotetecnombre";
        private const string Hotetecpriape = "Hotetecpriape";
        private const string Hotetectel = "Hotetectel";
        public BloqueoServicioPeticion CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            var bookiniRequest = new BloqueoServicioPeticion()
            {
                ideses = Convert.ToString(request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["ideses"]),
                codtou = request.Provider.Parameters.GetParameterValue(Hoteteccodtou),
                pasage = Getpasage(request),
                bloser = Getbloser(request),
                accion = request.Provider.Parameters.GetParameterValue(Hotetecaccion)

            };

            return bookiniRequest;
        }
        public ReservaCerrarPeticion CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string sessionId, UInt16 refage)
        {
            var percon = new ReservaCerrarPeticionPercon
            {
                nombre = request.Provider.Parameters.GetParameterValue(Hotetecnombre),
                priape = request.Provider.Parameters.GetParameterValue(Hotetecpriape),
                tel = Convert.ToUInt32(request.Provider.Parameters.GetParameterValue(Hotetectel))
            };

            var bookingCommitRequest = new ReservaCerrarPeticion()
            {
                ideses = sessionId,
                codtou = request.Provider.Parameters.GetParameterValue(Hoteteccodtou),
                percon = percon,
                refage = refage,
                accion = request.Provider.Parameters.GetParameterValue(HoteteCancaccion),
            };

            return bookingCommitRequest;
        }

        private BloqueoServicioPeticionAdl[] Getpasage(AccommodationProviderBookingRequest request)
        {

            var pass = new List<BloqueoServicioPeticionAdl>();
            byte pid = 0;
            foreach (var roomToBook in request.ValuatedRooms)
            {
                for (byte i = 0; i < roomToBook.Guests.Count(); i++)
                {
                    pid = Convert.ToByte(pid + 1);
                    pass.Add(new BloqueoServicioPeticionAdl()
                    {

                        id = pid,
                        fecnac = DateTime.Now.AddYears(-(Convert.ToInt32(roomToBook.Guests[i].Age))).ToString("dd/MM/yyyy")
                    });
                }

            }
            return pass.ToArray();
        }

        private BloqueoServicioPeticionBloser Getbloser(AccommodationProviderBookingRequest request)
        {
            var hotel = new BloqueoServicioPeticionBloser();
            var lstroom = new List<BloqueoServicioPeticionBloserDissmo>();
            
            hotel.id = Convert.ToByte(request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["HotelId"]);
            
            var dissom = new BloqueoServicioPeticionBloserDissmo();
            
            foreach (var room in request.ValuatedRooms)
            {
                dissom.id = Convert.ToUInt32(room.ValuationResult.ProviderSpecificData["RoomId"]);
                dissom.numuni = room.ValuationResult.RoomNumber;
                dissom.pasid = new int[room.ValuationResult.Adults];
                for (byte i = 0; i < room.ValuationResult.Adults; i++)
                {
                    dissom.pasid[i] = i + 1;
                }

            }
            hotel.dissmo = dissom;
            return hotel;

        }

    }
}
