﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using AlphaRooms.Accommodation.Provider.Hotetec.Service;
using System.Collections.Generic;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Factories
{
    public class HotetecCancellationRequestFactory : HotetecProviderFactoryBase, IHotetecProviderCancellationRequestFactory
    {
        private const string HoteteCancelaccion = "HoteteCancelaccion";
        private const string Hoteteccodtou = "Hoteteccodtou";
        private const string HotetecSessionUrl = "HotetecSessionUrl";
        private const string Hotetecchkscm = "Hotetecchkscm";
        public ReservaCancelarPeticion CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var canRequest = new ReservaCancelarPeticion()
                {
                ideses=GetSessionID(request.Provider.Parameters),
                codtou=request.Provider.Parameters.GetParameterValue(Hoteteccodtou),
                locata=request.ProviderBookingReference,
                accion = request.Provider.Parameters.GetParameterValue(HoteteCancelaccion),
                };

            return canRequest;
        }
        private string GetSessionID(List<AccommodationProviderParameter> parameters)
        {
            string url = parameters.GetParameterValue(HotetecSessionUrl);
            var sessionRequestXML = CreateSession(parameters).XmlSerialize();
            sessionRequestXML = sessionRequestXML.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            var responseXml = PostXmlRequest(url, sessionRequestXML);
            var response = responseXml.Result.XmlDeSerialize<SesionAbrirRespuesta>();
            var supplierResponse = new SesionAbrirRespuesta()
            {
                ideses = response.ideses

            };
            return supplierResponse.ideses;
        }
        
    }
}
