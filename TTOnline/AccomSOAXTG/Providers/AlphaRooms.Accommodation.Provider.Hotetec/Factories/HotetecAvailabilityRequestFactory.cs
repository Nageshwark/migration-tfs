﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;
using System.Collections.Generic;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
namespace AlphaRooms.Accommodation.Provider.Hotetec.Factories
{
    public class HotetecAvailabilityRequestFactory : HotetecProviderFactoryBase, IHotetecAvailabilityRequestFactory
    {
        private const string HotetecSessionUrl = "HotetecSessionUrl";
        private const string Hotetecchkscm = "Hotetecchkscm";
        private const string Hoteteccodtou = "Hoteteccodtou";

        public DisponibilidadHotelPeticion CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var availRequest = new DisponibilidadHotelPeticion
            {
                ideses = GetSessionID(request.Provider.Parameters),
                codtou = request.Provider.Parameters.GetParameterValue(Hoteteccodtou),
                fecini = request.CheckInDate.ToString("dd/MM/yyyy"),
                fecfin = request.CheckOutDate.ToString("dd/MM/yyyy"),
                codzge = request.DestinationCodes.Any() ? request.DestinationCodes.First() : null,
                codser = request.DestinationCodes.Any() ? null : request.EstablishmentCodes,
                distri = Getdistri(request.Rooms),
                chkscm = request.Provider.Parameters.GetParameterValue(Hotetecchkscm)
            };

            return availRequest;
        }

        
        public DisponibilidadHotelPeticionDistri[] Getdistri(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            byte id = 0;
            var roomInfo = new List<DisponibilidadHotelPeticionDistri>();
            foreach (var room in rooms)
            {
                    id++;
                    var distri=new DisponibilidadHotelPeticionDistri();
                    distri.id = id;
                    distri.numuni = room.RoomNumber;
                    distri.numadl = room.Guests.AdultsCount;
                    distri.numnin = room.Guests.ChildrenCount;
                    distri.numbeb = room.Guests.InfantsCount;
                    distri.Edanin = new int[room.Guests.ChildrenCount];
                    for (byte i = 0; i < room.Guests.ChildrenCount; i++)
                    {
                        distri.Edanin[i] =room.Guests.ChildAndInfantAges[i];
                    }
                 roomInfo.Add(distri);
            }
           
            return roomInfo.ToArray();
        }
    }
}

