﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotetecService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;

namespace AlphaRooms.Accommodation.Provider.Hotetec.Factories
{
    public class HotetecProviderFactoryBase
    {
        private const string Hoteteccodsys = "Hoteteccodsys";
        private const string Hoteteccodage = "Hoteteccodage";
        private const string Hotetecidtusu = "Hotetecidtusu";
        private const string Hotetecpasusu = "Hotetecpasusu";
        private const string HotetecSessionUrl = "HotetecSessionUrl";

        public SesionAbrirPeticion CreateSession(List<AccommodationProviderParameter> parameters)
        {
            return new SesionAbrirPeticion
            {
                codage = parameters.GetParameterValue(Hoteteccodage),
                codsys = parameters.GetParameterValue(Hoteteccodsys),
                idtusu = parameters.GetParameterValue(Hotetecidtusu),
                pasusu = parameters.GetParameterValue(Hotetecpasusu)
           };
        }

        public async Task<string> PostXmlRequest(string postUrl, string request)
        {
            var webScrapeClient = new WebScrapeClient()
            {
                UseCompression = true,
                Cookies = new WebScraperCookies(),
                ContentType = WebContentType.ApplicationXml
            };

            WebScrapeResponse response = await webScrapeClient.PostAsync(postUrl, request);

            return response.Value.ToString();
        }

        public string GetSessionID(List<AccommodationProviderParameter> parameters)
        {
            string url = parameters.GetParameterValue(HotetecSessionUrl);
            var sessionRequestXML = CreateSession(parameters).XmlSerialize();
            sessionRequestXML = sessionRequestXML.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, sessionRequestXML);

            var response = responseXml.Result.XmlDeSerialize<SesionAbrirRespuesta>();

            var supplierResponse = new SesionAbrirRespuesta()
            {
                ideses = response.ideses

            };
            return supplierResponse.ideses;
        }
    }
}
