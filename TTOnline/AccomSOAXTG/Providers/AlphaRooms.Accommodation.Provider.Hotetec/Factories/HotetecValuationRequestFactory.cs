﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.HotetecService;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System.Collections.Generic;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
namespace AlphaRooms.Accommodation.Provider.Hotetec.Factories
{
    public class HotetecValuationRequestFactory: HotetecProviderFactoryBase, IHotetecValuationRequestFactory
    {
        private const string Hotetecchkscm = "Hotetecchkscm";
        private const string Hoteteccodtou = "Hoteteccodtou";

        public DisponibilidadHotelPeticion CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request)
        {
            var valuationRequest = new DisponibilidadHotelPeticion
            {
                ideses = request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["ideses"],
                codtou = request.Provider.Parameters.GetParameterValue(Hoteteccodtou),
                fecini = request.SelectedRooms.First().AvailabilityResult.CheckInDate.ToString("dd/MM/yyyy"),
                fecfin = request.SelectedRooms.First().AvailabilityResult.CheckOutDate.ToString("dd/MM/yyyy"),
                codser = new string[] {request.SelectedRooms.First().AvailabilityResult.EstablishmentEdiCode},
                codral = GetBoardCode(request),
                distri = Getdistri(request),
                chkscm = request.Provider.Parameters.GetParameterValue(Hotetecchkscm)
            };
            
            return valuationRequest;
        }
       
        private string[] GetBoardCode(AccommodationProviderValuationRequest rooms)
        {
            var codral = new List<string>();

            foreach (var room in rooms.SelectedRooms)
            {
                codral.Add(room.AvailabilityResult.BoardCode);
            }

            return codral.ToArray(); 
        }

        public DisponibilidadHotelPeticionDistri[] Getdistri(AccommodationProviderValuationRequest rooms)
        {
            byte id = 0;
            var roomInfo = new List<DisponibilidadHotelPeticionDistri>();
            
            foreach (var room in rooms.SelectedRooms)
            {
                id++;

                var distri = new DisponibilidadHotelPeticionDistri
                {
                    id = id,
                    numuni = room.RoomNumber,
                    numadl = room.Guests.AdultsCount,
                    numnin = room.Guests.ChildrenCount,
                    numbeb = room.Guests.InfantsCount,
                    Edanin = new int[room.Guests.ChildrenCount]
                };

                for (byte i = 0; i < room.Guests.ChildrenCount; i++)
                {
                    distri.Edanin[i] = room.Guests.ChildAndInfantAges[i];
                }

                roomInfo.Add(distri);
            }

            return roomInfo.ToArray();
        }

    }
}
