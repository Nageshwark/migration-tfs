﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Factories;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;



namespace AlphaRooms.Accommodation.Provider.OswaldArrigo.Factories
{
    public class OswaldArrigoValuationRequestFactory : CavalRequestFactoryBase, ICavalValuationRequestFactory
    {
        private const string CavalLogin = "CavalLogin";
        private const string CavalPassword = "CavalPassword";
        private const string CavalLanguage = "CavalLanguage";
        private const string CavalGzip = "CavalGzip";
        private const string CavalAgentId = "CavalAgentId";

        public cavalHotelBookingValuationRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            var supplierRequest = new cavalHotelBookingValuationRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(CavalGzip)),
                login = request.Provider.Parameters.GetParameterValue(CavalLogin),
                password = request.Provider.Parameters.GetParameterValue(CavalPassword),
                agentId = request.Provider.Parameters.GetParameterValue(CavalAgentId),
                language = request.Provider.Parameters.GetParameterValue(CavalLanguage),
                checkIn = availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = availabilityRequest.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.SelectedRooms),
                establishmentId = availabilityResult.EstablishmentEdiCode,
                boardCode = availabilityResult.BoardCode
            };

           
            
            return supplierRequest;
        }
    }
}
