﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.CavalBase.CommonService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;



namespace AlphaRooms.Accommodation.Provider.OswaldArrigo.Factories
{
    public class OswaldArrigoCancellationRequestFactory : ICavalCancellationRequestFactory
    {
        private const string CavalLogin = "CavalLogin";
        private const string CavalPassword = "CavalPassword";
        private const string CavalLanguage = "CavalLanguage";
        private const string CavalGzip = "CavalGzip";
        private const string CavalAgentId = "CavalAgentId";

        public cavalCancelBookingRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new cavalCancelBookingRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(CavalGzip)),
                login = request.Provider.Parameters.GetParameterValue(CavalLogin),
                password = request.Provider.Parameters.GetParameterValue(CavalPassword),
                agentId = request.Provider.Parameters.GetParameterValue(CavalAgentId),
                language = request.Provider.Parameters.GetParameterValue(CavalLanguage),
                gzipResponseSpecified = true,
                locator = request.ProviderBookingReference,
                rqId = Guid.NewGuid().ToString()

            };

            return supplierRequest;
        }
    }
}
