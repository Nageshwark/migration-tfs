﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Factories;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;


namespace AlphaRooms.Accommodation.Provider.OswaldArrigo.Factories
{
    public class OswaldArrigoAvailabilityRequestFactory : CavalRequestFactoryBase, ICavalAvailabilityRequestFactory
    {
        private const string CavalLogin = "CavalLogin";
        private const string CavalPassword = "CavalPassword";
        private const string CavalLanguage = "CavalLanguage";
        private const string CavalGzip = "CavalGzip";
        private const string CavalAgentId = "CavalAgentId";


        public cavalHotelAvailabilityRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new cavalHotelAvailabilityRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(CavalGzip)),
                login = request.Provider.Parameters.GetParameterValue(CavalLogin),
                password = request.Provider.Parameters.GetParameterValue(CavalPassword),
                agentId =  request.Provider.Parameters.GetParameterValue(CavalAgentId),
                language = request.Provider.Parameters.GetParameterValue(CavalLanguage),
                checkIn = request.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = request.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.Rooms),
            };

            if (request.DestinationCodes != null && request.DestinationCodes.Any())
                supplierRequest.cityIds = request.DestinationCodes;
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
                supplierRequest.establishmentIds = request.EstablishmentCodes;
            else
            {
                throw new System.Exception("Either DestinationCodes or EstablishmentCodes needs to be passed in");
            }

            
            return supplierRequest;
        }
    }
}
