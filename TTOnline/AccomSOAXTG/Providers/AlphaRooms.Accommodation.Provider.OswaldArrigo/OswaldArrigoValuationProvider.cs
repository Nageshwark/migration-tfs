﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.OswaldArrigo;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<OswaldArrigoValuationResponse> automator;
        private readonly IAccommodationValuationParser<OswaldArrigoValuationResponse> parser;

        public OswaldArrigoValuationProvider(IAccommodationValuationAutomatorAsync<OswaldArrigoValuationResponse> automator,
                                                IAccommodationValuationParser<OswaldArrigoValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
