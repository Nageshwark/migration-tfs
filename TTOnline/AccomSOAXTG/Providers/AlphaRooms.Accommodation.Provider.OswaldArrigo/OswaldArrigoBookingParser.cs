﻿using System.Linq;

using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoBookingParser : IAccommodationBookingParser<IEnumerable<OswaldArrigoBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public OswaldArrigoBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<OswaldArrigoBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("OswaldArrigo Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        cavalHotelBookingConfirmRQ supplierRequest,
                                                        cavalHotelBookingConfirmRS supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }



            bool hasResults = (supplierResponse != null && supplierResponse.resultCode == 200 && !string.IsNullOrEmpty(supplierResponse.locator));

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                result.ProviderBookingReference = supplierResponse.locator;

                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);
            }
            else
            {
                var sbMessage = new StringBuilder();

                sbMessage.AppendLine("OswaldArrigo Booking Parser: Booking Response cannot be parsed because it is null.");

                if (supplierResponse != null && !string.IsNullOrEmpty(supplierResponse.message))
                    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

       
    }
}
