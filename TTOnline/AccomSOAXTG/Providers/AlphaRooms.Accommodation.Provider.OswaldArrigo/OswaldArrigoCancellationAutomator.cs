﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoCancellationAutomator : CavalAutomaterBase, IAccommodationCancellationAutomatorAsync<OswaldArrigoCancellationResponse>
    {
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalCancellationRequestFactory _oswaldArrigoProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public OswaldArrigoCancellationAutomator(ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ICavalCancellationRequestFactory oswaldArrigoSupplierCancellationRequestFactory,
                                            IProviderOutputLogger outputLogger)
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._oswaldArrigoProviderCancellationRequestFactory = oswaldArrigoSupplierCancellationRequestFactory;
            this._outputLogger = outputLogger;
        }
        public async Task<OswaldArrigoCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            OswaldArrigoCancellationResponse response = null;

            try
            {
                var supplierRequest = _oswaldArrigoProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                var client = ConfigureSoapClient(request);

                var supplierResponse = client.cancelBooking(supplierRequest);

                response = new OswaldArrigoCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("OswaldArrigoBookingAutomator.GetCancellationResponseAsync: OswaldArrigo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        


       
    }
}
