﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoBookingAutomator : CavalAutomaterBase, IAccommodationBookingAutomatorAsync<IEnumerable<OswaldArrigoBookingResponse>>
    {
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalBookingRequestFactory _oswaldArrigoProviderBookingRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public OswaldArrigoBookingAutomator(   ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ICavalBookingRequestFactory oswaldArrigoSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger)
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._oswaldArrigoProviderBookingRequestFactory = oswaldArrigoSupplierBookingRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<IEnumerable<OswaldArrigoBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<OswaldArrigoBookingResponse> responses = new List<OswaldArrigoBookingResponse>();

            cavalHotelBookingConfirmRQ supplierRequest = null;
            OswaldArrigoBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this._oswaldArrigoProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this._oswaldArrigoProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("OswaldArrigoBookingAutomator.GetBookingResponseAsync: OswaldArrigo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<OswaldArrigoBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, cavalHotelBookingConfirmRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Booking);

            var response = client.confirmHotelBooking(supplierRequest);

            var supplierResponse = new OswaldArrigoBookingResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        
       
    }
}
