﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.Accommodation.Provider.OswaldArrigo;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoValuationAutomator : CavalAutomaterBase, IAccommodationValuationAutomatorAsync<OswaldArrigoValuationResponse>
    {
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalValuationRequestFactory _oswaldArrigoProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public OswaldArrigoValuationAutomator( ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ICavalValuationRequestFactory oswaldArrigoSupplierValuationRequestFactory,
                                            IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._oswaldArrigoProviderValuationRequestFactory = oswaldArrigoSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<OswaldArrigoValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            OswaldArrigoValuationResponse response = null;

            try
            {
                ValidateRequest(request);

                var supplierRequest = this._oswaldArrigoProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("OswaldArrigoValuationAutomator.GetValuationResponseAsync: OswaldArrigo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<OswaldArrigoValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Valuation);

            var response = client.getDetailedValuation(supplierRequest);

            var supplierResponse = new OswaldArrigoValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        
      
    
    }
}
