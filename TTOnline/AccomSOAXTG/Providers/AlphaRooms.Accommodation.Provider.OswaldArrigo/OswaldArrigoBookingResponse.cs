﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;


namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that OswaldArrigo accepts and returns XML strings.
    /// </summary>
    public class OswaldArrigoBookingResponse
    {
        public cavalHotelBookingConfirmRQ SupplierRequest { get; set; }
        public cavalHotelBookingConfirmRS SupplierResponse { get; set; }
    }
}
