﻿
using AlphaRooms.Accommodation.Provider.CavalBase.CommonService;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoCancellationResponse
    {
        public cavalCancelBookingRQ SupplierRequest { get; set; }
        public cavalCancelBookingRS SupplierResponse { get; set; }
    }
}
