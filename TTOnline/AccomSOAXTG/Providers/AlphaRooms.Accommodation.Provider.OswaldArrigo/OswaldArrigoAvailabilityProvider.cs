﻿using AlphaRooms.Accommodation.Provider.OswaldArrigo;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.OswaldArrigo;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<OswaldArrigoAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<OswaldArrigoAvailabilityResponse> parser;

        public OswaldArrigoAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<OswaldArrigoAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<OswaldArrigoAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
