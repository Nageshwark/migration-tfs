﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;


namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoAvailabilityResponse
    {
        public cavalHotelAvailabilityRQ SupplierRequest { get; set; }
        public cavalHotelAvailabilityRS SupplierResponse { get; set; }

    }
}
