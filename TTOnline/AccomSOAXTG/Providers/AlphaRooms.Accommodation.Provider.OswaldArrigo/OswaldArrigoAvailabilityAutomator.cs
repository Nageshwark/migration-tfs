﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using log4net.Repository.Hierarchy;


namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoAvailabilityAutomator : CavalAutomaterBase, IAccommodationAvailabilityAutomatorAsync<OswaldArrigoAvailabilityResponse>
    {
        //private readonly IWebScrapeClient webScrapeClient;
        private readonly ICavalAvailabilityRequestFactory _oswaldArrigoSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public OswaldArrigoAvailabilityAutomator(ICavalAvailabilityRequestFactory oswaldArrigoAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            _oswaldArrigoSupplierAvailabilityRequestFactory = oswaldArrigoAvailabilityRequestFactory;
            _outputLogger = outputLogger;
        }

        public async Task<OswaldArrigoAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            OswaldArrigoAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                cavalHotelAvailabilityRQ supplierRequest = this._oswaldArrigoSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("OswaldArrigoAvailabilityAutomator.GetAvailabilityResponseAsync: OswaldArrigo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<OswaldArrigoAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, cavalHotelAvailabilityRQ  supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Availability);


            cavalHotelAvailabilityRS response = client.getAvailableHotels(supplierRequest);

            var supplierResponse = new OswaldArrigoAvailabilityResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;

         
        }

       

       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("OswaldArrigo supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("OswaldArrigo supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("OswaldArrigo supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("OswaldArrigo supports upto 2 Infants");
            //}
        }
    }
}
