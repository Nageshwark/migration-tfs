﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.OswaldArrigo
{
    public class OswaldArrigoCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<OswaldArrigoCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<OswaldArrigoCancellationResponse> parser;

        public OswaldArrigoCancellationProvider(IAccommodationCancellationAutomatorAsync<OswaldArrigoCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<OswaldArrigoCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
