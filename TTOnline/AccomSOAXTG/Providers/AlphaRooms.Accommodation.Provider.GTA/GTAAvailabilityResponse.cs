﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.GTA.Entity;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAAvailabilityResponse
    {
        public Request SupplierRequest { get; set; }
        public Response SupplierResponse { get; set; }

    }
}
