﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTABookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<GTABookingResponse>>
    {
        private const string GTABookingUrl = "GTABookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IGTAProviderBookingRequestFactory GTAProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public GTABookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IGTAProviderBookingRequestFactory GTASupplierBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.GTAProviderBookingRequestFactory = GTASupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<GTABookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<GTABookingResponse> responses = new List<GTABookingResponse>();

            Request supplierRequest = null;
            GTABookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.GTAProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.GTAProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("GTABookingAutomator.GetBookingResponseAsync: GTA supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<GTABookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, Request supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(GTABookingUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<Response>();

            var supplierResponse = new GTABookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

       
    }
}
