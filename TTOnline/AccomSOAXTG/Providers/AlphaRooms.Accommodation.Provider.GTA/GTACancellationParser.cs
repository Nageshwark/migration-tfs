﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTACancellationParser : IAccommodationCancellationParserAsync<GTACancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public GTACancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, GTACancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                var cancelResponse = response.SupplierResponse;

                Money cancelationCost = null;

                if (IsSuccess(cancelResponse, ref cancelationCost))
                {

                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";

                    bookingResult.CancellationCost = cancelationCost;


                }
                else
                {
                    
                    
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    var sb = new StringBuilder();
                    
                    sb.AppendLine("There was an unknown problem with the cancellation response from GTA.");

                    try
                    {
                        var errors = (Errors)((t_SearchBookingItemResponse)(((ResponseDetails)(cancelResponse.Item)).Items[0])).Items[0];

                        int i = 1;
                        foreach (var error in errors.Error)
                        {
                            sb.AppendLine("Details:");
                            sb.AppendLine(string.Format("{0}. {1}", i, error.ErrorText));
                            i++;
                        }
                    }
                    catch{}
                    
                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from GTA.");
                sb.AppendLine("Details:" + ex.ToString());
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }

        private static bool IsSuccess(Response cancelResponse, ref Money cancellationCost)
        {
            bool isSuccess = false;

            if (cancelResponse != null &&
                cancelResponse.Item != null &&
                cancelResponse.Item.GetType() == typeof (ResponseDetails))
            {
                var responseDetails = (ResponseDetails) cancelResponse.Item;

                if (responseDetails != null && responseDetails.Items.Any() &&
                    responseDetails.Items.First().GetType() == typeof (t_SearchBookingItemResponse))
                {

                    var searchBookngItem =(t_SearchBookingItemResponse)responseDetails.Items.First();

                    if (searchBookngItem !=null && 
                        searchBookngItem.Items.Any() 
                        && searchBookngItem.Items.First().GetType()!=typeof(Errors))
                    {
                        var status = from item in searchBookngItem.Items
                            where item.GetType() == typeof(t_Status)
                            select item;

                        if (status.Any() 
                            && status.First().GetType() == typeof(t_Status) 
                            && ((t_Status)status.First()).Code.Trim().ToLower() == "x" )
                            isSuccess = true;

                        var price = from item in searchBookngItem.Items
                                    where item.GetType() == typeof(BookingPrice)
                                    select item;

                        var priceList = price as IList<object> ?? price.ToList();
                        if (priceList.Any() && priceList.First().GetType() == typeof (BookingPrice))
                        {
                            var bookngPrice = (BookingPrice) priceList.First();
                            
                            cancellationCost = new Money
                            {
                                Amount = (decimal)bookngPrice.Gross,
                                CurrencyCode = bookngPrice.Currency
                            };
                        }
                    }

                }
            }

            return isSuccess;
        }
    }
}
