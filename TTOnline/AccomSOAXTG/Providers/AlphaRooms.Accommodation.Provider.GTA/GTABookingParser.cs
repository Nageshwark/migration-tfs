﻿
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTABookingParser : IAccommodationBookingParser<IEnumerable<GTABookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public GTABookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<GTABookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("GTA Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        Request supplierRequest,
                                                        Response supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null && 
                                supplierResponse.Item !=null && 
                                supplierResponse.Item.GetType() == typeof(ResponseDetails) &&
                                ((ResponseDetails)supplierResponse.Item).Items !=null
                               );

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();

                var responseDetails = ((t_SearchBookingItemResponse)(((ResponseDetails)(supplierResponse.Item)).Items[0]));
                
                var status = GetItemByType(responseDetails, typeof(t_Status));

                if (status.Any() && ((t_Status) status.First()).Code.Trim().ToUpper() == "C")
                {
                    var references = GetItemByType(responseDetails, typeof (t_BookingReferences));

                    if (references != null && references.Any())
                    {
                        var refs = ((t_BookingReferences)references.First()).BookingReference.Where(b => b.ReferenceSource == t_ReferenceSource.api);
                           

                        if (refs.Any())
                        {
                            result.ProviderBookingReference = ((t_BookingReference)refs.First()).Value;
                            result.BookingStatus = BookingStatus.Confirmed;
                        }
                        else
                        {
                            result.BookingStatus = BookingStatus.NotConfirmed;
                        }
                       
                    }
                    
                }
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                sbMessage.AppendLine("GTA Booking Parser: Booking Response cannot be parsed because it is null.");

                throw new SupplierApiDataException(sbMessage.ToString());
            }
            
        }

        private static IEnumerable<object> GetItemByType(t_SearchBookingItemResponse itemsQuery, Type type)
        {
            return from item in itemsQuery.Items
                   where item.GetType() == type
                   select item;
        }

       
    }
}
