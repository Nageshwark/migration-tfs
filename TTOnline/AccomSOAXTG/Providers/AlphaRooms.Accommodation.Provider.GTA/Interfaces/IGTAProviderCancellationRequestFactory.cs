﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.GTA.Entity;

namespace AlphaRooms.Accommodation.Provider.GTA.Interfaces
{
    public interface IGTAProviderCancellationRequestFactory
    {
        Request CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
