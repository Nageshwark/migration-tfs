﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.GTA.Interfaces
{
    public interface IGTAProviderBookingRequestFactory
    {
        Request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
