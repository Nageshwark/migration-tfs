﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.GTA.Factories
{
    public class GTAProviderValuationRequestFactory : IGTAProviderValuationRequestFactory
    {
        private const string GTAClient = "GTAClient";
        private const string GTAEmailAddress = "GTAEmailAddress";
        private const string GTAPassword = "GTAPassword";
        private const string GTALanguage = "GTALanguage";
        private const string GTACurrency = "GTACurrency";
        private const string GTACountry = "GTACountry";

        public Request  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new Request();
            supplierRequest.Source = new Source
            {
                RequestorID = new RequestorID
                {
                    Client = request.Provider.Parameters.GetParameterValue(GTAClient),
                    EMailAddress = request.Provider.Parameters.GetParameterValue(GTAEmailAddress),
                    Password = request.Provider.Parameters.GetParameterValue(GTAPassword)
                }
                ,
                RequestorPreferences = new RequestorPreferences
                {
                    Language = request.Provider.Parameters.GetParameterValue(GTALanguage),
                    Currency = request.Provider.Parameters.GetParameterValue(GTACurrency),
                    Country = request.Provider.Parameters.GetParameterValue(GTACountry),
                    RequestMode = RequestMode.SYNCHRONOUS
                }

            };

            supplierRequest.RequestDetails = new RequestDetails
            {
                Items = new[]
                {
                    new HotelPriceBreakdownRequest
                    {
                        City = request.SelectedRooms.First().AvailabilityResult.DestinationEdiCode,
                        Item = request.SelectedRooms.First().AvailabilityResult.EstablishmentEdiCode,
                        PeriodOfStay = new PeriodOfStay
                        {
                            CheckInDate = request.SelectedRooms.First().AvailabilityResult.CheckInDate,
                            Item = CalculateDuration(request.SelectedRooms.First().AvailabilityRequest).ToString()
                        },
                        Rooms = CreatePaxRooms(request.SelectedRooms)
                        
                    }
                },
                ItemsElementName = new ItemsChoiceType[1]
            };

            supplierRequest.RequestDetails.ItemsElementName[0] = ItemsChoiceType.HotelPriceBreakdownRequest;


            return supplierRequest;
            
        }

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;
        }

        private t_Room[] CreatePaxRooms(AccommodationProviderValuationRequestRoom[] rooms)
        {
            var paxRooms = new List<t_Room>();
            var roomCodes = new RoomCodes();
            
            foreach (var room in rooms)
            {
                var code = roomCodes.GetRoomCodes(room.Guests.AdultsCount, room.Guests.ChildrenCount,
                    room.Guests.InfantsCount);

                var paxRoom = new t_Room
                {
                    Code = code.First().Code,
                    NumberOfRooms = "1",
                    Id = room.AvailabilityResult.RoomCode,
                    NumberOfChildren = room.AvailabilityResult.Children.ToString(),
                    NumberOfCots = room.AvailabilityResult.Infants.ToString(),
                };

                paxRooms.Add(paxRoom);
                
            }

            return paxRooms.ToArray();
        }

        private string[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => guest.Age.ToString()).ToArray();

        }
       
    }
}
