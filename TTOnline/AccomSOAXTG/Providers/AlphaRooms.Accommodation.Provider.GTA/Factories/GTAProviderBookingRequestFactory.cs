﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAProviderBookingRequestFactory : IGTAProviderBookingRequestFactory
    {
        private const string GTAClient = "GTAClient";
        private const string GTAEmailAddress = "GTAEmailAddress";
        private const string GTAPassword = "GTAPassword";
        private const string GTALanguage = "GTALanguage";
        private const string GTACurrency = "GTACurrency";
        private const string GTACountry = "GTACountry";
        private const string GTABookingReference = "GTABookingReference";
        

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;

            var supplierRequest = new Request();
            supplierRequest.Source = new Source
            {
                RequestorID = new RequestorID
                {
                    Client = request.Provider.Parameters.GetParameterValue(GTAClient),
                    EMailAddress = request.Provider.Parameters.GetParameterValue(GTAEmailAddress),
                    Password = request.Provider.Parameters.GetParameterValue(GTAPassword)
                }
                ,
                RequestorPreferences = new RequestorPreferences
                {
                    Language = request.Provider.Parameters.GetParameterValue(GTALanguage),
                    Currency = request.Provider.Parameters.GetParameterValue(GTACurrency),
                    Country = request.Provider.Parameters.GetParameterValue(GTACountry),
                    RequestMode = RequestMode.SYNCHRONOUS
                }

            };

            supplierRequest.RequestDetails = new RequestDetails
            {
                Items = new[]
                {
                    
                    new t_AddBookingRequest
                    {
                        Currency = valuationResult.SalePrice.CurrencyCode,
                        PaxNames = CreatePaxNames(request.ValuatedRooms),
                        BookingReference =  new t_BookingReferenceInput
                        {
                            ReferenceSource = t_ReferenceSourceInput.client,
                            ReferenceSourceSpecified = true,
                            Value = request.Provider.Parameters.GetParameterValue(GTABookingReference)
                        },
                        BookingDepartureDate = valuationResult.CheckInDate,
                        BookingItems = new t_BookingItem[]
                        {
                           
                            new t_BookingItem
                            {
                                ItemType = t_ItemType.hotel,
                                ExpectedPrice = (double) valuationResult.SalePrice.Amount,
                                ItemReference = "1",
                                ItemCity = new t_ItemCity
                                {
                                    Code = valuationResult.DestinationEdiCode,
                                },
                                Item = new Item
                                {
                                    Code = valuationResult.EstablishmentEdiCode
                                },
                                Item1 =  new t_HotelItem
                                {
                                    PeriodOfStay = new PeriodOfStay
                                    {
                                        CheckInDate = valuationResult.CheckInDate,
                                        Item = valuationResult.CheckOutDate
                                    },
                                    Item = CreateHotelRooms(request)
                                   
                                }
                            }
                        }
                    }
                },
                ItemsElementName = new ItemsChoiceType[1]
            };

            supplierRequest.RequestDetails.ItemsElementName[0] = ItemsChoiceType.AddBookingRequest;


            return supplierRequest;

            /*
         <Request>
          <Source>
            <RequestorID Client="1483" EMailAddress="M@C.IE" Password="m"/>
            <RequestorPreferences Language="en" Currency="GBP">
              <RequestMode>ASYNCHRONOUS</RequestMode>
              <ResponseURL>/ProcessResponse/GetXML</ResponseURL>
            </RequestorPreferences>
          </Source>
          <RequestDetails>
            <AddBookingRequest Currency="GBP">
              <BookingName>MC20031301</BookingName>
              <BookingReference>MC20031201a</BookingReference>
              <BookingDepartureDate>2004-01-01</BookingDepartureDate>
              <PaxNames>
                <PaxName PaxId="1"><![CDATA[Mr John Doe ]]></PaxName>
                <PaxName PaxId="2"><![CDATA[Mrs Sarah Doe ]]></PaxName>
              </PaxNames>
              <BookingItems>
                <BookingItem ItemType="hotel" ExpectedPrice ="50.00">
                  <ItemReference>1</ItemReference>
                  <ItemCity Code="AMS" />
                  <Item Code="NAD" />
                  <HotelItem>
                    <PeriodOfStay>
                      <CheckInDate>2004-01-01</CheckInDate>
                      <CheckOutDate>2004-01-11</CheckOutDate>
                    </PeriodOfStay>
                    <HotelRooms>
                      <HotelRoom Code="TB" Id="001:NAD">
                        <PaxIds>
                          <PaxId>1</PaxId>
                          <PaxId>2</PaxId>
                        </PaxIds>
                      </HotelRoom>
                    </HotelRooms>
                  </HotelItem>
                </BookingItem>
              </BookingItems>
            </AddBookingRequest>
          </RequestDetails>
        </Request>
         */
        }

        private t_PaxName[] CreatePaxNames(AccommodationProviderBookingRequestRoom[] valuatedRooms)
        {
            var paxNames = new List<t_PaxName>();
            int i = 1;
            foreach (var room in valuatedRooms)
            {
                foreach (var guest in room.Guests)
                {
                    paxNames.Add(new t_PaxName
                    {
                        PaxId = i.ToString(),
                        Value = string.Format("{0} {1} {2}", guest.Title, guest.FirstName, guest.Surname)
                    });

                    i++;
                }
            }

            return paxNames.ToArray();
        }

        private t_HotelRooms CreateHotelRooms(AccommodationProviderBookingRequest request)
        {
            var hotelRooms = new t_HotelRooms();
            var hotelRoomList = new List<t_HotelRoom>();

            foreach (var room in request.ValuatedRooms)
            {
                hotelRoomList.Add(new t_HotelRoom
                {
                    Id = room.ValuationResult.RoomCode,
                    Code = room.ValuationResult.ProviderSpecificData["RoomCode"],
                    PaxIds = CreatePaxIds(room.Guests)
                    
                });
            }

            hotelRooms.HotelRoom = hotelRoomList.ToArray();

            return hotelRooms;

        }

        private string[] CreatePaxIds(AccommodationProviderBookingRequestRoomGuestCollection guests)
        {
            var paxList = new List<string>();
            for (int i = 1; i <= guests.Count(); i++)
            {
                paxList.Add(i.ToString());
            }

            return paxList.ToArray();
        }

        


    }
}
