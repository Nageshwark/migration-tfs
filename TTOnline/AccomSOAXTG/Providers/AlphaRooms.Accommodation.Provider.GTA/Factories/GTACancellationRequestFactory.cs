﻿using System.Linq.Expressions;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;

namespace AlphaRooms.Accommodation.Provider.GTA.Factories
{
    public class GTACancellationRequestFactory : IGTAProviderCancellationRequestFactory
    {
        private const string GTAClient = "GTAClient";
        private const string GTAEmailAddress = "GTAEmailAddress";
        private const string GTAPassword = "GTAPassword";
        private const string GTALanguage = "GTALanguage";
        private const string GTACurrency = "GTACurrency";
        private const string GTACountry = "GTACountry";

        public Request CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new Request
            {
                Source = new Source
                {
                    RequestorID = new RequestorID
                    {
                        Client = request.Provider.Parameters.GetParameterValue(GTAClient),
                        EMailAddress = request.Provider.Parameters.GetParameterValue(GTAEmailAddress),
                        Password = request.Provider.Parameters.GetParameterValue(GTAPassword)
                    }
                    ,
                    RequestorPreferences = new RequestorPreferences
                    {
                        Language = request.Provider.Parameters.GetParameterValue(GTALanguage),
                        Currency = request.Provider.Parameters.GetParameterValue(GTACurrency),
                        Country = request.Provider.Parameters.GetParameterValue(GTACountry),
                        RequestMode = RequestMode.SYNCHRONOUS
                    }
                },
                RequestDetails = new RequestDetails
                {
                    Items = new []
                    {
                        new t_CancelBookingRequest
                        {
                            BookingReference = new t_BookingReferenceInput
                            {
                                ReferenceSource = t_ReferenceSourceInput.api,
                                ReferenceSourceSpecified = true,
                                Value = request.ProviderBookingReference
                            }
                        }
                    },
                    ItemsElementName = new ItemsChoiceType[1]
                }
                
            };
            
            supplierRequest.RequestDetails.ItemsElementName[0] = ItemsChoiceType.CancelBookingRequest;
            
            return supplierRequest;
        }
    }
}
