﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.Utilities;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.GTA.Factories
{
    public class GTAAvailabilityRequestFactory : IGTAAvailabilityRequestFactory
    {
        private const string GTAClient = "GTAClient";
        private const string GTAEmailAddress = "GTAEmailAddress";
        private const string GTAPassword = "GTAPassword";
        private const string GTALanguage = "GTALanguage";
        private const string GTACurrency = "GTACurrency";
        private const string GTACountry = "GTACountry";
     

        public Request CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new Request
            {
                Source = new Source
                {
                    RequestorID = new RequestorID
                    {
                        Client = request.Provider.Parameters.GetParameterValue(GTAClient),
                        EMailAddress = request.Provider.Parameters.GetParameterValue(GTAEmailAddress),
                        Password = request.Provider.Parameters.GetParameterValue(GTAPassword)
                    }
                    ,
                    RequestorPreferences = new RequestorPreferences
                    {
                        Language = request.Provider.Parameters.GetParameterValue(GTALanguage),
                        Currency = request.Provider.Parameters.GetParameterValue(GTACurrency),
                        Country = request.Provider.Parameters.GetParameterValue(GTACountry),
                        RequestMode = RequestMode.SYNCHRONOUS
                    }
                },
                RequestDetails = new RequestDetails
                {
                    Items = new[]
                    {
                        new SearchHotelPricePaxRequest
                        {
                            ItemDestination = SetItemDestination(request),
                            Item = SetItemCodes(request),
                            PeriodOfStay = new PeriodOfStay
                            {
                                CheckInDate = request.CheckInDate,
                                Item = CalculateDuration(request).ToString()
                            },
                            IncludePriceBreakdown = new Empty(),
                            PaxRooms = CreatePaxRooms(request.Rooms),
                            ImmediateConfirmationOnly = new Empty(),
                            IncludeChargeConditions = new IncludeChargeConditions()
                        }
                    },
                    ItemsElementName = new ItemsChoiceType[1]
                }
            };
           
            supplierRequest.RequestDetails.ItemsElementName[0] = ItemsChoiceType.SearchHotelPricePaxRequest;

           
            return supplierRequest;
        }

        private object SetItemCodes(AccommodationProviderAvailabilityRequest request)
        {
            if (request.EstablishmentCodes !=null && request.EstablishmentCodes.Any())
            {
                var itemCodes = new ItemCodes
                {
                    ItemCode = request.EstablishmentCodes.Select(ec => ec.Split(';')[1]).ToArray()
                
                };

                return itemCodes;
            }

            return null;

        }

        private ItemDestinationPax SetItemDestination(AccommodationProviderAvailabilityRequest request)
        {
            string destinationCode = "";
           
            if (request.DestinationCodes != null & request.DestinationCodes.Any())
            {
                destinationCode = request.DestinationCodes[0];
            }
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())        //GTA reiqures Destination even for HotelID search
            {
                destinationCode = request.EstablishmentCodes[0].Split(';')[0];
            }

            return new ItemDestinationPax
            {
                DestinationCode = destinationCode,
                DestinationType = t_DestinationTypeCityOrGeocode.city
            };

            
        }

        private t_PaxRoom[] CreatePaxRooms(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            var paxRooms = new List<t_PaxRoom>();

            int i = 1;
            foreach (var room in rooms)
            {
                var paxRoom = new t_PaxRoom
                {
                    Adults = room.Guests.AdultsCount.ToString(),
                    ChildAges = GetChildAges(room.Guests),
                    RoomIndex = i.ToString(),
                    Cots = room.Guests.InfantsCount.ToString()
                    
                };

                paxRooms.Add(paxRoom);
                i++;
            }

            return paxRooms.ToArray();
        }

        private string[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => guest.Age.ToString()).ToArray();

        }


        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;
        }

        /*
         <Request>
         <Source>
                <RequestorID Client ="794" EMailAddress="malcom@alpharooms.com" Password="PASS"/>
                <RequestorPreferences Language ="en" Currency="GBP" Country="GB">
                            <RequestMode>SYNCHRONOUS</RequestMode>
                </RequestorPreferences>
         </Source>

     <RequestDetails>
         <SearchHotelPricePaxRequest>
             <ItemDestination DestinationCode = "AMS" DestinationType = "city"/>
		
             <PeriodOfStay>
                 <CheckInDate>2015-11-01</CheckInDate>
                 <Duration>1</Duration>
             </PeriodOfStay>
		
             <IncludePriceBreakdown/>
             <IncludeChargeConditions/>
		
             <PaxRooms>
                 <PaxRoom Adults="2" Cots="0" RoomIndex="1"/>
             </PaxRooms>
         </SearchHotelPricePaxRequest>
     </RequestDetails>
 </Request>
         */


    }
}
