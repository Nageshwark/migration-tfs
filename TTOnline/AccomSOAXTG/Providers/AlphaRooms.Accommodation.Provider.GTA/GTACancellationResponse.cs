﻿using AlphaRooms.Accommodation.Provider.GTA.Entity;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTACancellationResponse
    {
        public Request SupplierRequest { get; set; }
        public Response SupplierResponse { get; set; }
    }
}
