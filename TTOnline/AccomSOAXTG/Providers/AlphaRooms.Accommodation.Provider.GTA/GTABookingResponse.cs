﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.GTA.Entity;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that GTA accepts and returns XML strings.
    /// </summary>
    public class GTABookingResponse
    {
        public Request SupplierRequest { get; set; }
        public Response SupplierResponse { get; set; }
    }
}
