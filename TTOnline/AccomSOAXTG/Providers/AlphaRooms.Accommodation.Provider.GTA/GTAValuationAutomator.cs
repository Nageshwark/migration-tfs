﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAValuationAutomator : IAccommodationValuationAutomatorAsync<GTAValuationResponse>
    {
        private const string GTAValuationUrl = "GTAValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IGTAProviderValuationRequestFactory GTAProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public GTAValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IGTAProviderValuationRequestFactory GTASupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.GTAProviderValuationRequestFactory = GTASupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<GTAValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            GTAValuationResponse response = null;

            try
            {
                var supplierRequest = this.GTAProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("GTAValuationAutomator.GetValuationResponseAsync: GTA supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<GTAValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, Request supplierRequest)
        {

            string serialisedRequest = supplierRequest.XmlSerialize();

            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(GTAValuationUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<Response>();

            var supplierResponse = new GTAValuationResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        
       
    
    }
}
