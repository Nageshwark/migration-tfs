﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.GTA
{

    public class GTAValuationParser : IAccommodationValuationParser<GTAValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public GTAValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, GTAValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("GTA Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, Request supplierRequest, Response supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }

            bool hasResults = HasResults(supplierResponse);

            if (hasResults)
            {
                var itemsQuery = (HotelPriceBreakdownResponse) ((ResponseDetails) supplierResponse.Item).Items[0];
                var cityQuery = GetItemByType(itemsQuery, typeof(City));
                var bookingPriceQuery = GetItemByType(itemsQuery, typeof (BookingPrice));
                var hotelRooms = GetItemByType(itemsQuery, typeof (HotelItemPriceBreakdown));
                var itemQuery = GetItemByType(itemsQuery, typeof (Item));                 
                

                if (hotelRooms.Any())
                {
                    var rooms = ((HotelItemPriceBreakdown) hotelRooms.First()).HotelRooms;
                    var periodOfStay = ((HotelItemPriceBreakdown)hotelRooms.First()).PeriodOfStay;
                    var bookingPrice = (BookingPrice) bookingPriceQuery.First();
                    var city = (City) cityQuery.First();
                    var item = (Item) itemQuery.First();

                    //If there is any cot defined, GTA returns additonal room wiht 0 price, we dont want to treat that as a room
                    //so filtering it out.
                    foreach (var  room in rooms.Where(r => r.RoomPrice.Gross > 0))
                    {
                        var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
                        var availResult = request.SelectedRooms.First().AvailabilityResult;

                        var result = new AccommodationProviderValuationResult();

                        //result.Id = new Guid();
                        result.RoomNumber = 1;

                        result.ProviderEdiCode = request.Provider.EdiCode;

                        //TODO:GTA - Confirm PaymentModel
                        result.PaymentModel = PaymentModel.PostPayment;

                        //TODO:GTA- Confirm RateType
                        result.RateType = RateType.GrossStandard;

                        result.DestinationEdiCode = city.Code;


                        result.EstablishmentEdiCode = item.Code;
                        result.EstablishmentName = item.Value;

                        //TODO:GTA - Could not find in the response, copying it from Avail Result
                        result.RoomDescription = availResult.RoomDescription;
                        result.RoomCode = room.Id;

                        result.CheckInDate = periodOfStay.CheckInDate;
                        result.CheckOutDate = (DateTime)periodOfStay.Item;

                        //TODO:GTA - Could not find in the response, copying it from Avail Result
                        result.BoardCode = availResult.BoardCode;
                        result.BoardDescription = availResult.BoardDescription;



                        result.SalePrice = new Money()
                        {
                            Amount = (decimal) room.RoomPrice.Gross,
                            CurrencyCode = bookingPrice.Currency
                        };

                        //TODO:GTA - Could not find in the response, copying it from Avail Result
                        result.CostPrice = result.SalePrice;

                        //TODO:GTA - Could not find in the response, copying it from Avail Result
                        if (availResult.ProviderSpecificData.ContainsKey("CancellationPolicy"))
                            result.CancellationPolicy = availResult.ProviderSpecificData["CancellationPolicy"].Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);


                        //TODO:GTA - Could not find in the response, copying it from Avail Result
                        result.IsNonRefundable = availResult.IsNonRefundable;


                        result.Adults = availabilityRequest.Rooms.First().Guests.AdultsCount;
                        result.Children = availabilityRequest.Rooms.First().Guests.ChildrenCount;
                        result.Infants = availabilityRequest.Rooms.First().Guests.InfantsCount; 
                        
                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("RoomCode", room.Code);

                        availabilityResults.Enqueue(result);

                    }

                }
            }
        
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var items = (HotelPriceBreakdownResponse)((ResponseDetails)supplierResponse.Item).Items[0];
                sbMessage.AppendLine("GTA Availability Parser: Availability Response cannot be parsed because it is null.");

                int i = 1;
                Errors erros = GetErrors(items);

                if (erros != null)
                {
                    foreach (var error in ((Errors)items.Items[0]).Error)
                    {
                        sbMessage.AppendLine(string.Format("Error {0}:{1}", i, error.ErrorText));
                        i++;
                    }
                }
                
                throw new SupplierApiDataException(sbMessage.ToString());
            }
           
        }

        private Errors GetErrors(HotelPriceBreakdownResponse items)
        {
            if (items != null &&
                items.Items != null
                && items.Items.Any()
                && items.Items[0].GetType() == typeof (Errors))
            {
                return (Errors)items.Items[0];
            }

            return null;
        }

        private static IEnumerable<object> GetItemByType(HotelPriceBreakdownResponse itemsQuery, Type type)
        {
            return from item in itemsQuery.Items 
                   where item.GetType() == type 
                   select item;
        }

        private static bool HasResults(Response supplierResponse)
        {

            if (supplierResponse != null && supplierResponse.Item != null)
            {
                var items = (HotelPriceBreakdownResponse)((ResponseDetails)supplierResponse.Item).Items[0];

                if (items.Items.Any() && items.Items[0].GetType() == typeof (Errors))
                    return false;
                else 
                    return true;
            }

            return false;
        }
    }
}
