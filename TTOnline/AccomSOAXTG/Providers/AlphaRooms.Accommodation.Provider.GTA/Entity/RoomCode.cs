﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.GTA.Entity
{
    public class RoomCode
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public int ExtraBed { get; set; }

        public RoomCode()
        {
            
        }

        public RoomCode(string id, string code, string type, int extraBed)
        {
            this.Id = id;
            this.Code = code;
            this.Type = type;
            this.ExtraBed = extraBed;
        }
    }
}
