﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.GTA.Entity
{
    public class RoomCodes:List<RoomCode>
    {
        public RoomCodes()
        {
            this.Add(new RoomCode("DB+1", "DB", "double + 1 extra bed", 1));
            this.Add(new RoomCode("TB+1", "TB", "twin + 1 extra bed", 1));
            this.Add(new RoomCode("DB+2", "DB", "double + 2 extra bed", 2));
            this.Add(new RoomCode("TB+2", "TB", "twin + 2 extra bed", 2));
            this.Add(new RoomCode("DB", "DB", "double", 0));
            this.Add(new RoomCode("Q", "Q", "quad", 0));
            this.Add(new RoomCode("SB", "SB", "single", 0));
            this.Add(new RoomCode("TB", "TB", "twin", 0));
            this.Add(new RoomCode("TR", "TR", "triple", 0));
            this.Add(new RoomCode("TS", "TS", "twin for sole use", 0));
        }

         public List<RoomCode> GetRoomCodes(int adult, int child, int cots)
         {
             if (adult == 1 && child == 0) return this.FindAll(x => x.Id == "SB" || x.Id == "TS");
             if (adult == 1 && child == 1 && cots <= 2) return this.FindAll(x => x.Id == "DB" || x.Id == "TB");
             if (adult == 1 && child == 2 && cots <= 1) return this.FindAll(x => x.Id == "DB+1" || x.Id == "TB+1");
             if (adult == 1 && child == 3 && cots <= 1) return this.FindAll(x => x.Id == "DB+2" || x.Id == "TB+2");
             
             if (adult == 2 && child == 0) return this.FindAll(x => x.Id == "DB" || x.Id == "TB");
             if (adult == 2 && child == 1 && cots <= 2) return this.FindAll(x => x.Id == "DB+1" || x.Id == "TB+1" || x.Id == "TR");
             if (adult == 2 && child == 2 && cots <= 1) return this.FindAll(x => x.Id == "DB+2" || x.Id == "TB+2" || x.Id == "Q");
             
             if (adult == 3 && child == 0 && cots <= 2) return this.FindAll(x => x.Id == "TR");
             if (adult == 3 && child == 1) return this.FindAll(x => x.Id == "Q");
             if (adult == 4 && child == 0 && cots <= 0) return this.FindAll(x => x.Id == "Q");




             #region Logic for Legacy system
             /*
             If adult = 1 AndAlso child = 0 Then Return Me.FindAll(Function(x) (x.RoomID = "SB" OrElse x.RoomID = "TS"))
             If adult = 1 AndAlso (child = 1 AndAlso cots <= 2) Then Return Me.FindAll(Function(x) (x.RoomID = "DB" OrElse x.RoomID = "TB"))
             If adult = 1 AndAlso (child = 2 AndAlso cots <= 1) Then Return Me.FindAll(Function(x) (x.RoomID = "DB+1" OrElse x.RoomID = "TB+1"))
             If adult = 1 AndAlso (child = 3 AndAlso cots <= 1) Then Return Me.FindAll(Function(x) (x.RoomID = "DB+2" OrElse x.RoomID = "TB+2"))
            
             If adult = 2 AndAlso child = 0 Then Return Me.FindAll(Function(x) (x.RoomID = "DB" OrElse x.RoomID = "TB"))
             If adult = 2 AndAlso (child = 1 AndAlso cots <= 2) Then Return Me.FindAll(Function(x) (x.RoomID = "DB+1" OrElse x.RoomID = "TB+1" OrElse x.RoomID = "TR"))
             If adult = 2 AndAlso (child = 2 AndAlso cots <= 1) Then Return Me.FindAll(Function(x) (x.RoomID = "DB+2" OrElse x.RoomID = "TB+2" OrElse x.RoomID = "Q"))
            
             if adult = 3 AndAlso (child = 0 AndAlso cots <= 2) Then Return Me.FindAll(Function(x) x.RoomID = "TR")
             If adult = 3 AndAlso child = 1 Then Return Me.FindAll(Function(x) x.RoomID = "Q")
             If adult = 4 AndAlso child = 0 Then Return Me.FindAll(Function(x) x.RoomID = "Q")
              */
             
             #endregion

             return null;
         }

        public string GetCodeByDescription(string desc)
        {
            var result = this.FirstOrDefault(x => desc.ToLower().Contains(x.Type.ToLower()));

            if (result != null)
                return result.Code;

            return "";
        }

        public RoomCode GetRoomDetails(string roomId)
        {
            return this.Find(x => x.Id == roomId);
        }
            //Return Me.Find(Function(x) x.RoomID = roomID)
       
                  
    }
}
