﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.GTA;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<GTAValuationResponse> automator;
        private readonly IAccommodationValuationParser<GTAValuationResponse> parser;

        public GTAValuationProvider(IAccommodationValuationAutomatorAsync<GTAValuationResponse> automator,
                                                IAccommodationValuationParser<GTAValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
