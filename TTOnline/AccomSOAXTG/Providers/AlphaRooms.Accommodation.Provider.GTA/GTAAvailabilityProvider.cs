﻿using AlphaRooms.Accommodation.Provider.GTA;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.GTA;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<GTAAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<GTAAvailabilityResponse> parser;

        public GTAAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<GTAAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<GTAAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
