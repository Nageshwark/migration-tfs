﻿using System;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Activation;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTACancellationAutomator : IAccommodationCancellationAutomatorAsync<GTACancellationResponse>
    {
        private const string GTACancellationUrl = "GTACancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string GTAUsername = "GTAUsername";
        private const string GTAPassword = "GTAPassword";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IGTAProviderCancellationRequestFactory gtaProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        

        public GTACancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IGTAProviderCancellationRequestFactory GTASupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.gtaProviderCancellationRequestFactory = GTASupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<GTACancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            GTACancellationResponse response = null;

            try
            {
                var supplierRequest = gtaProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                
                string serialisedRequest = supplierRequest.XmlSerialize();

                serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

                string url = request.Provider.Parameters.GetParameterValue(GTACancellationUrl);

                var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

                var supplierResponse = responseXML.Value.XmlDeSerialize<Response>();

                

                response = new GTACancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("GTABookingAutomator.GetCancellationResponseAsync: GTA supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }
        
    }
}
