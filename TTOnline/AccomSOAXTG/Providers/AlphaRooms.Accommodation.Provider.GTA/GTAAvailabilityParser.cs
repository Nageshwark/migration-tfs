﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.GTA
{
    public class GTAAvailabilityParser : IAccommodationAvailabilityParser<GTAAvailabilityResponse>
    {
        private readonly ILogger logger;
        private const string GTACurrency = "GTACurrency";

        //private readonly IAccommodationConfigurationManager configurationManager;

        public GTAAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, GTAAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse,
                    availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "GTA Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, Request supplierRequest, Response supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse);

            if (hasResults)
            {
                var items = (SearchHotelPricePaxResponse)((ResponseDetails)supplierResponse.Item).Items[0];
                var hotelDetails = (PaxHotelDetails)items.Item;

                foreach (var hotel in hotelDetails.Hotel)
                {
                    int roomCounter = 1; 
                    foreach(var paxRoom in hotel.PaxRoomSearchResults)          //Loop for Multiroom
                    {
                        //loop for different roomtypes
                        foreach (var roomType in paxRoom.RoomCategories)
                        {
                            var result = new AccommodationProviderAvailabilityResult();
                            result.RoomNumber = Convert.ToByte(paxRoom.RoomIndex);
                            result.ProviderEdiCode = request.Provider.EdiCode;
                            result.SupplierEdiCode = request.Provider.EdiCode;
                            result.EstablishmentEdiCode = hotel.Item.Code;
                            result.EstablishmentName = hotel.Item.Value;
                            result.DestinationEdiCode = hotel.City.Code;

                            result.BoardCode = roomType.Meals.Basis.Code;
                            if (roomType.Meals.Basis.Code == "N")
                                result.BoardDescription = "Room Only";
                            else
                                result.BoardDescription = string.Format("{0} {1}", roomType.Meals.Breakfast.Value, roomType.Meals.Basis.Value);

                            result.RoomCode = roomType.Id;

                            result.RoomDescription = roomType.Description;

                            //TODO:GTA -Confirm PaymentModel
                            result.PaymentModel = PaymentModel.PostPayment;
                            
                            //TODO:GTA- Confirm RateType
                            result.RateType = RateType.GrossStandard;


                            result.IsOpaqueRate = false;

                            //TODO:GTA - Review how we determine NonRef room
                            bool isNonRef = IsNonRefundable(roomType.ChargeConditions, roomType.HotelRoomPrices.First().RoomPrice.Gross);
                            result.IsNonRefundable = isNonRef;

                            if (isNonRef)
                                result.RoomDescription += " - Non Refundable";
                            

                            result.NumberOfAvailableRooms = 0;

                            decimal price = (decimal)roomType.HotelRoomPrices.First().RoomPrice.Gross;
                            string currencyCode = request.Provider.Parameters.GetParameterValue(GTACurrency);

                            result.SalePrice = new Money { Amount = price, CurrencyCode = currencyCode };

                            result.CostPrice = result.SalePrice;
                            
                            //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };
                            
                            result.Adults = request.Rooms[roomCounter- 1].Guests.AdultsCount;
                            result.Children = request.Rooms[roomCounter-1].Guests.ChildrenCount;
                            result.Infants = request.Rooms[roomCounter-1].Guests.InfantsCount;
                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate =request.CheckOutDate;

                            result.ProviderSpecificData = new Dictionary<string, string>();

                            if (roomType.ChargeConditions !=null)
                                result.ProviderSpecificData.Add("CancellationPolicy", GetCancellationPolicy(roomType.ChargeConditions));

                            if (roomType.EssentialInformation != null)
                                result.ProviderSpecificData.Add("EssentialInfo", GetEssentialInfo(roomType.EssentialInformation));

                            availabilityResults.Enqueue(result);                           
                        }

                        roomCounter++;
                    }
                }
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                //var error = supplierResponse.Errors;

                sbMessage.AppendLine("GTA Availability Parser: Availability Response cannot be parsed because it is null.");
               
                var items = (SearchHotelPricePaxResponse)((ResponseDetails)supplierResponse.Item).Items[0];
                if (items != null && items.Item.GetType() == typeof(Errors))
                {
                    int i = 1;
                    foreach (var error in ((Errors)items.Item).Error)
                    {
                        sbMessage.AppendLine(string.Format("Error {0}:{1}", i, error.ErrorText));
                        i++;
                    }
                }

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

        private bool IsNonRefundable(ChargeConditions chargeConditions, double roomPrice)
        {
            var cax = from ChargeCondition cc in chargeConditions.ChargeCondition
                      where cc.Type == "cancellation"
                      select cc;
            
            if (cax.First().Condition.Any(cc => string.IsNullOrEmpty(cc.ToDay) && cc.Charge && cc.ChargeAmount == roomPrice))
            {
                return true;
            }

            return false;
        }

        private string GetEssentialInfo(Information[] essentialInformations)
        {
            var sb = new StringBuilder();

            foreach (var ei in essentialInformations)
            {
                sb.AppendLine(string.Format("From:{0}, To:{1}, {2}", ei.DateRange.FromDate, ei.DateRange.ToDate, ei.Text));
            }

            return sb.ToString();
        }

        private string GetCancellationPolicy(ChargeConditions chargeConditions)
        {
            var policy = new StringBuilder();

            var cax = from ChargeCondition cc in chargeConditions.ChargeCondition
                where cc.Type == "cancellation"
                select cc;

            foreach (var c in cax.First().Condition)
            {
                policy.AppendLine(string.Format("FromDay:{0}, ToDay:{1}, ChargeAmount:{2}, Currency:{3}", c.FromDay,
                    c.ToDay, c.ChargeAmount, c.Currency));
            }



            return policy.ToString();

            /*
                <Condition Charge="true" ChargeAmount="167.75" Currency="GBP" FromDay="0" ToDay="3"/>
                <Condition Charge="true" ChargeAmount="83.88" Currency="GBP" FromDay="4" ToDay="7"/>
                <Condition Charge="true" ChargeAmount="41.94" Currency="GBP" FromDay="8"/>
             */
        }

        
        

        private static bool HasResults(Response supplierResponse)
        {

            if (supplierResponse != null && supplierResponse.Item != null)
            {
                var items = (SearchHotelPricePaxResponse)((ResponseDetails)supplierResponse.Item).Items[0];

                if (items.Item.GetType() == typeof(Errors))
                    return false;
                else
                    return true;
            }

            return false;
        }
       
    }
}
