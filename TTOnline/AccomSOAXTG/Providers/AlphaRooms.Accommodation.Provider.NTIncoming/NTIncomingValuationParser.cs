﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{

    public class NTIncomingValuationParser : IAccommodationValuationParser<NTIncomingValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public NTIncomingValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, NTIncomingValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("NTIncoming Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request,  OTA_HotelAvailServiceOTA_HotelAvailRQ supplierRequest, OTA_HotelAvailServiceResponseOTA_HotelAvailRS supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {

            bool hasResults = (supplierResponse != null && supplierResponse.Items != null && supplierResponse.Items.Any() && (supplierResponse.Items[0].GetType() != typeof(ErrorType[])));

            if (hasResults)
            {
                var roomStays = (OTA_HotelAvailServiceResponseOTA_HotelAvailRSRoomStays)supplierResponse.Items[1];

                //int i = 0;
                foreach (var roomStay in roomStays.RoomStay)
                //Parallel.ForEach(roomStays.RoomStay, roomStay =>
                {
                    string areaId = GetAreaId(roomStay.TPA_Extensions.Any);

                
                    //Sync.ParallelForEachIgnoreFailed(roomStay.RoomRates.RoomRate, roomRate =>
                    foreach (var roomRate in roomStay.RoomRates.RoomRate)
                    {
                        var isSelectRoom = from AccommodationProviderValuationRequestRoom r in request.SelectedRooms
                            where r.AvailabilityResult.RoomCode == roomRate.RatePlanCode
                            select r;

                        if (isSelectRoom.Any())
                        {

                            for (var roomNumber = 1; roomNumber <= request.SelectedRooms.Count(); roomNumber++)
                            {
                                var rate = GetRate(roomNumber, roomRate.Rates);

                                if (rate == null)
                                    throw new Exception("Unable to find a Rate");

                                var result = new AccommodationProviderValuationResult();

                                //result.Id = new Guid();
                                result.RoomNumber = (byte) roomNumber;

                                result.ProviderEdiCode = request.Provider.EdiCode;

                                //TODO:Confirm PaymentModel
                                result.PaymentModel = PaymentModel.PostPayment;

                                //TODO:Confirm RateType
                                result.RateType = RateType.NetStandard;

                                result.DestinationEdiCode = areaId;

                                result.EstablishmentEdiCode = roomStay.BasicPropertyInfo.HotelCode;
                                result.EstablishmentName = roomStay.BasicPropertyInfo.HotelName;

                                result.RoomDescription = ((FormattedTextTextType) (rate.RateDescription.Items[0])).Value;
                                result.RoomCode = roomRate.RatePlanCode;

                                result.CheckInDate = request.SelectedRooms[roomNumber - 1].AvailabilityRequest.CheckInDate;
                                result.CheckOutDate = request.SelectedRooms[roomNumber - 1].AvailabilityRequest.CheckOutDate;
                                
                                result.BoardDescription = roomRate.RatePlanCategory;
                                
                                result.SalePrice = new Money()
                                {
                                    Amount = roomRate.Total.AmountAfterTax,
                                    CurrencyCode = roomRate.Total.CurrencyCode
                                };

                                //TODO:Confirm what we should populate for CostPrice
                                result.CostPrice = result.SalePrice;

                                result.IsNonRefundable = request.SelectedRooms[roomNumber - 1].AvailabilityResult.IsNonRefundable;


                                
                                result.Adults = request.SelectedRooms[roomNumber - 1].Guests.AdultsCount;
                                result.Children = (byte) (request.SelectedRooms[roomNumber - 1].Guests.ChildrenCount +
                                                  request.SelectedRooms[roomNumber - 1].Guests.InfantsCount);

                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("JPCode", roomStay.BasicPropertyInfo.JPCode);
                                result.ProviderSpecificData.Add("SequenceNumber", supplierResponse.SequenceNmbr);

                                availabilityResults.Enqueue(result);
                            }

                        }
                    }//);
                }//);

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var error = (ErrorType[])supplierResponse.Items[0];

                sbMessage.AppendLine("NTIncoming Availability Parser: Availability Response cannot be parsed because it is null.");
                sbMessage.AppendLine(string.Format("Error Details: {0}", error[0].ShortText));


                throw new SupplierApiDataException(sbMessage.ToString());
            }
           
        }

        private string GetAreaId(XmlElement[] xmlElement)
        {
            string areaId = string.Empty;

            var mp = from XmlElement x in xmlElement
                     where x.Name == "HotelInfo"
                     select x;

            if (mp != null && mp.Any())
            {
                areaId = (mp.First().SelectSingleNode("AreaID")).InnerText;
            }

            return areaId;
        }

        private ArrayOfRateTypeRateRate GetRate(int roomNumber, ArrayOfRateTypeRateRate[] rates)
        {

            var selectedRate = from ArrayOfRateTypeRateRate rate in rates
                               where rate.RateSource.Contains(roomNumber.ToString())
                               select rate;

            return selectedRate.First();

        }
    }
}
