﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.NTIncoming.Helper;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;
using log4net.Repository.Hierarchy;


namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<NTIncomingAvailabilityResponse>
    {
        private const string NTIncomingAvailabilityUrl = "NTIncomingAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly INTIncomingAvailabilityRequestFactory _ntIncomingSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public NTIncomingAvailabilityAutomator(INTIncomingAvailabilityRequestFactory ntIncomingAvailabilityRequestFactory, IProviderOutputLogger ouputLogger)
        {
            this._ntIncomingSupplierAvailabilityRequestFactory = ntIncomingAvailabilityRequestFactory;
            this._outputLogger = ouputLogger;
        }

        public async Task<NTIncomingAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            NTIncomingAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                OTA_HotelAvailServiceOTA_HotelAvailRQ supplierRequest = this._ntIncomingSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("NTIncomingAvailabilityAutomator.GetAvailabilityResponseAsync: NTIncoming supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<NTIncomingAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, OTA_HotelAvailServiceOTA_HotelAvailRQ  supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            OTA_HotelAvailSoapClient client = ConfigureSoapCleint(request);
            
          
            OTA_HotelAvailServiceResponseOTA_HotelAvailRS response = client.OTA_HotelAvailService(supplierRequest);
            
            
            //supplierOutputLogger.LogAvailabilityResponse(supplierBase.Supplier, request.SearchId, request.DepartureAirportCode, request.DestinationAirportCode, request.DepartureDate, SupplierOutputFormats.Xml, supplierResponse);

            var supplierResponse = new NTIncomingAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private OTA_HotelAvailSoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            
 
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(NTIncomingAvailabilityUrl))); 
            OTA_HotelAvailSoapClient client =new OTA_HotelAvailSoapClient(binding, address);

            ProxyHelper helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }

       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("NTIncoming supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("NTIncoming supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("NTIncoming supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("NTIncoming supports upto 2 Infants");
            //}
        }
    }
}
