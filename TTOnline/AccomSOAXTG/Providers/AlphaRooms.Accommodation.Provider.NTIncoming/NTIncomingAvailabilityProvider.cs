﻿using AlphaRooms.Accommodation.Provider.NTIncoming;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.NTIncoming;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<NTIncomingAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<NTIncomingAvailabilityResponse> parser;

        public NTIncomingAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<NTIncomingAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<NTIncomingAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
