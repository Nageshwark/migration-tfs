﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingCancelService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces
{
    public interface INTIncomingProviderCancellationRequestFactory
    {
        OTA_CancelServiceOTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
