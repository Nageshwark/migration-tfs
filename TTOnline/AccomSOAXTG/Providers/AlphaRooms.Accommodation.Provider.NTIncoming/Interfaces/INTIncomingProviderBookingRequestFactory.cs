﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces
{
    public interface INTIncomingProviderBookingRequestFactory
    {
        OTA_HotelResRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
