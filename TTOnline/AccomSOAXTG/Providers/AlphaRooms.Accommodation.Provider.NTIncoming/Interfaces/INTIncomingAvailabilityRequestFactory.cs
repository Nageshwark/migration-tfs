﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces
{
    public interface INTIncomingAvailabilityRequestFactory
    {
        OTA_HotelAvailServiceOTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
