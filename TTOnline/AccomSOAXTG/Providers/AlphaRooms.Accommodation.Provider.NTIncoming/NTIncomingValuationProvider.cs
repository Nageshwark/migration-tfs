﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.NTIncoming;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<NTIncomingValuationResponse> automator;
        private readonly IAccommodationValuationParser<NTIncomingValuationResponse> parser;

        public NTIncomingValuationProvider(IAccommodationValuationAutomatorAsync<NTIncomingValuationResponse> automator,
                                                IAccommodationValuationParser<NTIncomingValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
