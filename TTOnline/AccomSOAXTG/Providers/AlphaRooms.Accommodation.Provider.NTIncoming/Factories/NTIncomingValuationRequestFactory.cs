﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Factories
{
    public class NTIncomingValuationRequestFactory : INTIncomingProviderValuationRequestFactory
    {
        private const string NTIncomingUsername = "NTIncomingUsername";
        private const string NTIncomingPassword = "NTIncomingPassword";
        private const string NTIncomingPrimaryLanguage = "NTIncomingPrimaryLanguage";

        public OTA_HotelAvailServiceOTA_HotelAvailRQ  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new OTA_HotelAvailServiceOTA_HotelAvailRQ
            {
                POS = new[] 
                {
                    new SourceType
                    {
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(NTIncomingUsername),
                        RequestorID = new SourceTypeRequestorID
                        {
                            MessagePassword = request.Provider.Parameters.GetParameterValue(NTIncomingPassword)
                        }
                    }
                },
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(NTIncomingPrimaryLanguage),
                AvailRequestSegments = new OTA_HotelAvailServiceOTA_HotelAvailRQAvailRequestSegments
                {
                    AvailRequestSegment = new[]
                    {
                        new AvailRequestSegmentsTypeAvailRequestSegment
                        {
                            StayDateRange = new DateTimeSpanType()
                            {
                                Start = request.SelectedRooms.First().AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                                End = request.SelectedRooms.First().AvailabilityRequest.CheckOutDate.ToString("yyyy-MM-dd")
                            },
                            RoomStayCandidates = CreateRoomStay(request),
                            HotelSearchCriteria = CreateSearchCriteria(request)
                        }
                    }
                }
            };
            
            return supplierRequest;
        }


        private HotelSearchCriteriaType CreateSearchCriteria(AccommodationProviderValuationRequest request)
        {
            var searchCriterion = new HotelSearchCriteriaType();
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;

            if (availabilityRequest.DestinationCodes.Any())
            {

                searchCriterion.Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        HotelRef = new ItemSearchCriterionTypeHotelRef
                        {
                            HotelCityCode = availabilityRequest.DestinationCodes[0]
                        }
                    }
                };
            }
            else if (availabilityRequest.EstablishmentCodes.Any())
            {
                searchCriterion.Criterion = availabilityRequest.EstablishmentCodes.Select(establishmentCode => new HotelSearchCriteriaTypeCriterion()
                {
                    HotelRef = new ItemSearchCriterionTypeHotelRef()
                    {
                        HotelCode = establishmentCode
                    }
                }).ToArray();
            }

            return searchCriterion;
        }

        private ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate[] CreateRoomStay(AccommodationProviderValuationRequest request)
        {
            var roomStay = new List<ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate>();


            foreach (var room in request.SelectedRooms)
            {
                var roomStayCandidate = new ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate
                {
                    GuestCounts = new GuestCountType
                    {
                        GuestCount = CalculateGuestCount(room.Guests)
                    }
                };

                int counter = 0;
                var guestCounts = new List<GuestCountTypeGuestCount>();

                guestCounts.Add(new GuestCountTypeGuestCount()
                {
                    AgeQualifyingCode = "10",
                    Count = room.Guests.AdultsCount.ToString()
                });

                //Assuming that there will be always one adult in a room
                roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
                {
                    AgeQualifyingCode = "10",
                    Count = room.Guests.AdultsCount.ToString()
                };

                if (room.Guests.ChildrenAndInfantsCount > 0)
                {
                    //If we pass 0 as the infant age we get no avail. So passing infant age as 1
                    guestCounts.AddRange(room.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant).Select(guest => new GuestCountTypeGuestCount()
                    {
                        Age = guest.Type == GuestType.Child ? guest.Age.ToString() : "1",
                        Count = "1"
                    }));
                }

                roomStayCandidate.GuestCounts.GuestCount = guestCounts.ToArray();

                roomStay.Add(roomStayCandidate);
            }

            return roomStay.ToArray();
        }

        private GuestCountTypeGuestCount[] CalculateGuestCount(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            int guestCount = 0;

            if (guests.AdultsCount > 0)
                guestCount++;

            if (guests.ChildrenCount > 0)
                guestCount++;

            if (guests.InfantsCount > 0)
                guestCount++;

            return new GuestCountTypeGuestCount[guestCount];

        }
        
       
    }
}
