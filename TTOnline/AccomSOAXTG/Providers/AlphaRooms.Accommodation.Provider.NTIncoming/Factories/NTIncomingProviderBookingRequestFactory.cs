﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingCancelService;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using SourceType = AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService.SourceType;
using SourceTypeRequestorID = AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService.SourceTypeRequestorID;
using TPA_Extensions_Type = AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService.TPA_Extensions_Type;
using UniqueID_Type = AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService.UniqueID_Type;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingProviderBookingRequestFactory : INTIncomingProviderBookingRequestFactory
    {
        private const string NTIncomingUsername = "NTIncomingUsername";
        private const string NTIncomingPassword = "NTIncomingPassword";
        private const string NTIncomingPrimaryLanguage = "NTIncomingPrimaryLanguage";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public OTA_HotelResRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new OTA_HotelResRQ
            {
                POS = new[]
                {
                    new SourceType
                    {
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(NTIncomingUsername),
                        RequestorID = new SourceTypeRequestorID
                        {
                            MessagePassword = request.Provider.Parameters.GetParameterValue(NTIncomingPassword)
                        }
                    }
                },
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(NTIncomingPrimaryLanguage),
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation = new[]
                    {
                        new HotelReservationType
                        {
                            UniqueID = new UniqueID_Type
                            {
                                ID_Context = Guid.NewGuid().ToString(), //ToDO:Booking Ref - make it as Provider Param 
                            },
                            RoomStays = CreateRoomStays(request),
                            ResGuests = CreateResGuests(request.Customer),
                            TPA_Extensions = CreateOptiions(request)
                        }
                    }
                }
            };

            return supplierRequest;


        }

        private TPA_Extensions_Type CreateOptiions(AccommodationProviderBookingRequest request)
        {
            var doc = new XmlDocument();

            var tpaExtensions = new TPA_Extensions_Type();

            var elements = new List<XmlElement>();
            //TODO:Pass as Provider param
            XmlElement element = doc.CreateElement("AcceptOnly");
            element.InnerText = "OK";
            elements.Add(element);

            element = doc.CreateElement("ShowCommission");
            element.InnerText = "1";
            elements.Add(element);

            element = doc.CreateElement("ShowNettPrice");
            element.InnerText = "1";
            elements.Add(element);

            tpaExtensions.Any = elements.ToArray();

            return tpaExtensions;
        }

        private ArrayOfRoomStaysTypeRoomStayRoomStay[] CreateRoomStays(AccommodationProviderBookingRequest request)
        {
            var roomStays = new List<ArrayOfRoomStaysTypeRoomStayRoomStay>();
            var selectedRoom = request.ValuatedRooms.First();

            roomStays.Add(new ArrayOfRoomStaysTypeRoomStayRoomStay
            {
                RatePlans = new[]
                {
                    new RatePlanType
                    {
                        RatePlanCode = request.ValuatedRooms.First().ValuationResult.RoomCode
                    }
                },
                RoomTypes = CreateRoomTypes(request.ValuatedRooms, request.Customer),
                BasicPropertyInfo = new BasicPropertyInfoType
                {
                    HotelCode = request.ValuatedRooms.First().ValuationResult.EstablishmentEdiCode
                },
                TimeSpan = new DateTimeSpanType
                {
                    Start = selectedRoom.ValuationResult.CheckInDate.ToString("yyyy-MM-dd"),
                    End = selectedRoom.ValuationResult.CheckOutDate.ToString("yyyy-MM-dd")
                },
                Total = new TotalType
                {
                    CurrencyCode = selectedRoom.ValuationResult.SalePrice.CurrencyCode,
                    AmountAfterTax = selectedRoom.ValuationResult.SalePrice.Amount,
                    AmountAfterTaxSpecified = true
                },
                TPA_Extensions = CreatedExpectedPrice(selectedRoom)
            });
                
            


            return roomStays.ToArray();
        }

        private TPA_Extensions_Type CreatedExpectedPrice(AccommodationProviderBookingRequestRoom selectedRoom)
        {
            var doc = new XmlDocument();

            XmlElement expectedPrice = doc.CreateElement("ExpectedPriceRange");
            expectedPrice.SetAttribute("min", "0");
            expectedPrice.SetAttribute("max", selectedRoom.ValuationResult.SalePrice.Amount.ToString());

            var tpaExtensions = new TPA_Extensions_Type
            {
                Any = new XmlElement[1]
            };

            tpaExtensions.Any[0] = expectedPrice;

            return tpaExtensions;
        }

        private RoomTypeType[] CreateRoomTypes(AccommodationProviderBookingRequestRoom[] selectedRooms, AccommodationProviderBookingRequestCustomer customer)
        {
            var roomTypes = new List<RoomTypeType>();
            var doc = new XmlDocument();

            foreach (var room in selectedRooms)
            {
                var roomType = new RoomTypeType();
                var guests = doc.CreateElement("Guests");

                roomType.TPA_Extensions = new TPA_Extensions_Type();
                roomType.TPA_Extensions.Any = new XmlElement[1];
                
                foreach (var guest in room.Guests)
                {
                    string firstname = string.IsNullOrEmpty(guest.FirstName) ? customer.FirstName : guest.FirstName;
                    string surname = string.IsNullOrEmpty(guest.Surname) ? customer.Surname : guest.Surname;

                    var g = doc.CreateElement("Guest");
                    
                    g.SetAttribute("Name", firstname);
                    g.SetAttribute("Surname", surname);

                    guests.AppendChild(g);
                }

                roomType.TPA_Extensions.Any[0] = guests;
                roomTypes.Add(roomType);
            }

            return roomTypes.ToArray();
        }

        private ArrayOfResGuestsTypeResGuestResGuest[] CreateResGuests(AccommodationProviderBookingRequestCustomer customer)
        {
            var guests = new List<ArrayOfResGuestsTypeResGuestResGuest>
            {
                new ArrayOfResGuestsTypeResGuestResGuest
                {
                    Profiles = new []
                    {
                        new ArrayOfProfilesTypeProfileInfoProfileInfo
                        {
                            Profile = new ProfileType
                            {
                                ProfileType1 = "1",
                                Customer = new CustomerType
                                {
                                    PersonName = new PersonNameType
                                    {
                                        GivenName = new []{ customer.FirstName},
                                        Surname = customer.Surname
                                    },
                                    Telephone = new []
                                    {
                                        new CustomerTypeTelephone
                                        {
                                            PhoneNumber = customer.ContactNumber
                                        }
                                    },
                                    Email = new []
                                    {
                                        new EmailType
                                        {
                                            Value = customer.EmailAddress
                                        }
                                    },
                                    Address = new []
                                    {
                                        new CustomerTypeAddress //TODO:Where do we populate the address from?
                                        {
                                            AddressLine = new []{""}, 
                                            PostalCode = "",
                                            CityName = "",
                                            StateProv =  new StateProvType
                                            {
                                                StateCode = "",
                                                Value = "",
                                            },
                                            CountryName = new CountryNameType
                                            {
                                                Code = "",
                                                Value = ""
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            };

            return guests.ToArray();
        }
    }
}
