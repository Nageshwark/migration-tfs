﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingCancelService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Factories
{
    public class NTIncomingCancellationRequestFactory : INTIncomingProviderCancellationRequestFactory
    {
        private const string NTIncomingUsername = "NTIncomingUsername";
        private const string NTIncomingPassword = "NTIncomingPassword";
        private const string NTIncomingPrimaryLanguage = "NTIncomingPrimaryLanguage";

        public OTA_CancelServiceOTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new OTA_CancelServiceOTA_CancelRQ
            {
                POS = new[]
                {
                    new SourceType
                    {
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(NTIncomingUsername),
                        RequestorID = new SourceTypeRequestorID
                        {
                            MessagePassword = request.Provider.Parameters.GetParameterValue(NTIncomingPassword)
                        }
                    }
                },
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(NTIncomingPrimaryLanguage),
                UniqueID = new[]
                {
                    new OTA_CancelServiceOTA_CancelRQUniqueID
                    {
                        ID = request.ProviderBookingReference
                    }
                }

            };

            return supplierRequest;
        }
    }
}
