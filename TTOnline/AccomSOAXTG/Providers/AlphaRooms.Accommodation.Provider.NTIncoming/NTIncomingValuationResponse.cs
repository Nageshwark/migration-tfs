﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingValuationResponse
    {
        public OTA_HotelAvailServiceOTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailServiceResponseOTA_HotelAvailRS SupplierResponse { get; set; }

    }
}
