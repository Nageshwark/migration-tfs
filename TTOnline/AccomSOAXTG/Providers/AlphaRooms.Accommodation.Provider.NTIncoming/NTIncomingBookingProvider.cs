﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<NTIncomingBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<NTIncomingBookingResponse>> parser;

        public NTIncomingBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<NTIncomingBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<NTIncomingBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
