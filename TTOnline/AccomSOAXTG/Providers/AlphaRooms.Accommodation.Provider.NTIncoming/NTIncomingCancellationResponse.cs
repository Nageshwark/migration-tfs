﻿using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingCancelService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingCancellationResponse
    {
        public OTA_CancelServiceOTA_CancelRQ SupplierRequest { get; set; }
        public OTA_CancelServiceResponseOTA_CancelRS SupplierResponse { get; set; }
    }
}
