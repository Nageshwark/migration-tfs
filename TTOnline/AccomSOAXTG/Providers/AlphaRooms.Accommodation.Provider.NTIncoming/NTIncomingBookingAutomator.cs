﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.NTIncoming.Helper;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<NTIncomingBookingResponse>>
    {
        private const string NTIncomingBookingUrl = "NTIncomingBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly INTIncomingProviderBookingRequestFactory _ntIncomingProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public NTIncomingBookingAutomator(   ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            INTIncomingProviderBookingRequestFactory ntIncomingSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger)
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._ntIncomingProviderBookingRequestFactory = ntIncomingSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<NTIncomingBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<NTIncomingBookingResponse> responses = new List<NTIncomingBookingResponse>();

            OTA_HotelResRQ supplierRequest = null;
            NTIncomingBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this._ntIncomingProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.NTIncomingProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("NTIncomingBookingAutomator.GetBookingResponseAsync: NTIncoming supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<NTIncomingBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, OTA_HotelResRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }


            var client = ConfigureSoapCleint(request);

            var response = client.OTA_HotelResV2Service(supplierRequest);

            var supplierResponse = new NTIncomingBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private OTA_HotelResSoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(NTIncomingBookingUrl)));
            var client = new OTA_HotelResSoapClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
       
    }
}
