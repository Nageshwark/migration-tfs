﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingAvailabilityParser:IAccommodationAvailabilityParser<NTIncomingAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public NTIncomingAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            NTIncomingAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("NTIncoming Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, OTA_HotelAvailServiceOTA_HotelAvailRQ supplierRequest, OTA_HotelAvailServiceResponseOTA_HotelAvailRS supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.Items != null && supplierResponse.Items.Any() && (supplierResponse.Items[0].GetType() != typeof(ErrorType [])));

            if (hasResults)
            {
                var  roomStays =(OTA_HotelAvailServiceResponseOTA_HotelAvailRSRoomStays)supplierResponse.Items[1];

                //int i = 0;
                foreach (var roomStay in roomStays.RoomStay)
                //Parallel.ForEach(roomStays.RoomStay, roomStay =>
                {
                    string areaId = GetAreaId(roomStay.TPA_Extensions.Any);

                    //Sync.ParallelForEachIgnoreFailed(roomStay.RoomRates.RoomRate, roomRate =>
                    foreach (var roomRate in roomStay.RoomRates.RoomRate)
                    {
                        for (var roomNumber = 1; roomNumber <= request.Rooms.Count(); roomNumber++)
                        {
                            var rate = GetRate(roomNumber, roomRate.Rates);

                            if (rate == null)
                                throw new Exception("Unable to find a Rate");

                            var result = new AccommodationProviderAvailabilityResult();

                            //result.Id = new Guid();
                            result.RoomNumber = (byte)roomNumber;

                            result.ProviderEdiCode = request.Provider.EdiCode;

                            //TODO:Confirm PaymentModel
                            result.PaymentModel = PaymentModel.PostPayment;

                            //TODO:Confirm RateType
                            result.RateType = RateType.NetStandard;
                            
                            result.DestinationEdiCode = areaId;

                            result.EstablishmentEdiCode = roomStay.BasicPropertyInfo.HotelCode;
                            result.EstablishmentName = roomStay.BasicPropertyInfo.HotelName;

                            result.RoomDescription = ((FormattedTextTextType)(rate.RateDescription.Items[0])).Value;
                            result.RoomCode = roomRate.RatePlanCode;

                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckOutDate;


                            result.BoardDescription = roomRate.RatePlanCategory;

                            

                            result.SalePrice = new Money()
                            {
                                Amount = roomRate.Total.AmountAfterTax,
                                CurrencyCode = roomRate.Total.CurrencyCode
                            };

                            //TODO:Confirm what we should populate for CostPrice
                            result.CostPrice = result.SalePrice;


                           

                            //TODO:NonReundable, need an example in the response.
                            result.IsNonRefundable = roomRate.TPA_Extensions != null && GetNonRefundable(roomRate.TPA_Extensions.Any) ;


                            //TODO:Confirm why we need Adult/Children? is that MaxAdult/MaxChild
                            result.Adults = request.Rooms[roomNumber-1].Guests.AdultsCount;
                            result.Children = (byte)(request.Rooms[roomNumber - 1].Guests.ChildrenCount + request.Rooms[roomNumber - 1].Guests.InfantsCount);

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("JPCode", roomStay.BasicPropertyInfo.JPCode);
                            result.ProviderSpecificData.Add("SequenceNumber", supplierResponse.SequenceNmbr);

                            availabilityResults.Enqueue(result);
                        }

                        
                    }//);
                }//);

            }
            else
            {
                if (supplierResponse != null && supplierResponse.Items != null && supplierResponse.Items.Any())
                {
                    var sbMessage = new StringBuilder();

                    var error = (ErrorType[])supplierResponse.Items[0];

                    if (error != null && error.First().Code != "NO_AVAIL_FOUND")
                    {
                        sbMessage.AppendLine("NTIncoming Availability Parser: Availability Response returned error.");
                        sbMessage.AppendLine(string.Format("Error Details: {0}", error[0].ShortText));


                        throw new SupplierApiDataException(sbMessage.ToString());
                    }
                }
            }
            
        }

        private string GetAreaId(XmlElement[] xmlElement)
        {
            string areaId = string.Empty;

            var mp = from XmlElement x in xmlElement
                     where x.Name == "HotelInfo"
                     select x;

            if (mp != null && mp.Any())
            {
                areaId = (mp.First().SelectSingleNode("AreaID")).InnerText;
            }

            return areaId;
        }

        private bool GetNonRefundable(XmlElement[] xmlElement)
        {
            string nonRef = string.Empty;

            var mp = from XmlElement x in xmlElement
                     where x.Name == "NonRefundable"
                     select x;

            if (mp != null && mp.Any())
            {
                nonRef = mp.First().InnerText;
            }

            return nonRef == "1";
        }

        private ArrayOfRateTypeRateRate GetRate(int roomNumber, ArrayOfRateTypeRateRate[] rates)
        {

            var selectedRate = from ArrayOfRateTypeRateRate rate in rates
                where rate.RateSource.Contains(roomNumber.ToString())
                select rate;

            return selectedRate.First();

        }
    }
}
