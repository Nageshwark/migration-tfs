﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<NTIncomingCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<NTIncomingCancellationResponse> parser;

        public NTIncomingCancellationProvider(IAccommodationCancellationAutomatorAsync<NTIncomingCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<NTIncomingCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
