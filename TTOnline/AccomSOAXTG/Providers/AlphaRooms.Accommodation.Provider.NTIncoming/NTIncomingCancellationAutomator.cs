﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.Helper;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingCancelService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.NTIncoming
{
    public class NTIncomingCancellationAutomator : IAccommodationCancellationAutomatorAsync<NTIncomingCancellationResponse>
    {
        private const string NTIncomingCancellationUrl = "NTIncomingCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly INTIncomingProviderCancellationRequestFactory NTIncomingProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public NTIncomingCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            INTIncomingProviderCancellationRequestFactory NTIncomingSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.NTIncomingProviderCancellationRequestFactory = NTIncomingSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<NTIncomingCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            NTIncomingCancellationResponse response = null;

            try
            {
                var supplierRequest = NTIncomingProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }
                
                var client = ConfigureSoapCleint(request);

                var supplierResponse = client.OTA_CancelService(supplierRequest);

                response = new NTIncomingCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("NTIncomingBookingAutomator.GetCancellationResponseAsync: NTIncoming supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        

        private OTA_CancelSoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(NTIncomingCancellationUrl)));
            var client = new OTA_CancelSoapClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
    }
}
