﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomParserBase
    {
        /// <summary>
        /// Serializes Pax Priecs as its required to be passed at the time of Valuation and Booking.
        /// </summary>
        /// <param name="guests"></param>
        /// <returns></returns>
        protected string GetPaxPriceString(GuestElement[] guests)
        {
            var sbPaxPrice = new StringBuilder();

            foreach (var guest in guests)
            {
                sbPaxPrice.AppendFormat("{0},{1},{2},{3},{4},{5}|",
                                    guest.GetType().Name.Substring(0, 1),
                                    guest.Id,
                                    guest.SellingPrice.Amount,
                                    guest.SellingPrice.Currency,
                                    guest.SellingPrice.Estimated,
                                    guest.SellingPrice.Converted);
            }

            var paxString = sbPaxPrice.ToString().Remove(sbPaxPrice.Length - 1);

            return paxString;
        }


        protected byte GetPAXCount(GuestElement[] guests, Type guestType)
        {
            var query = from GuestElement guest in guests
                        where guest.GetType() == guestType
                        select guest;

            if (query != null && query.Any())
            {
                return (byte)query.Count();
            }
            else return 0;
        }
    }
}
