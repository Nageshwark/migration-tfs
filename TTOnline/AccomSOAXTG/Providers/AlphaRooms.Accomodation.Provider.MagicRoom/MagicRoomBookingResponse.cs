﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that MagicRoom accepts and returns XML strings.
    /// </summary>
    public class MagicRoomBookingResponse
    {
        public BookingCreate SupplierRequest { get; set; }
        public BookingCreateResult SupplierResponse { get; set; }
    }

    
    
}
