﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<MagicRoomAvailabilityResponse>
    {
        private const string MagicRoomAvailabilityUrl = "MagicRoomAvailabilityUrl";
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IMagicRoomAvailabilityRequestFactory MagicRoomSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public MagicRoomAvailabilityAutomator(IMagicRoomAvailabilityRequestFactory MagicRoomAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this.MagicRoomSupplierAvailabilityRequestFactory = MagicRoomAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<MagicRoomAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            MagicRoomAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                AvailabilitySearch supplierRequest = this.MagicRoomSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                string serialisedRequestXML = supplierRequest.XmlSerialize();

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("MagicRoomAvailabilityAutomator.GetAvailabilityResponseAsync: MagicRoom supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<MagicRoomAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, AvailabilitySearch  supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();


            ServiceSoapClient client = ConfigureSoapCleint(request);
            
          

            AvailabilitySearchResult response = client.AvailabilitySearch(supplierRequest);
            outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, response);

            MagicRoomAvailabilityResponse supplierResponse = new MagicRoomAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private ServiceSoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MagicRoomAvailabilityUrl)));
            ServiceSoapClient client = new ServiceSoapClient(binding, address);

            return client;
        }

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("MagicRoom supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("MagicRoom supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("MagicRoom supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("MagicRoom supports upto 2 Infants");
            //}
        }
    }
}
