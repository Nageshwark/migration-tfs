﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomAvailabilityResponse
    {
        public AvailabilitySearch SupplierRequest { get; set; }
        public AvailabilitySearchResult SupplierResponse { get; set; }

    }
}
