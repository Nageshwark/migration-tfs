﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<MagicRoomBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<MagicRoomBookingResponse>> parser;

        public MagicRoomBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<MagicRoomBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<MagicRoomBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
