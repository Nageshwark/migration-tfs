﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomValuationResponse
    {
        public BookingCreate SupplierRequest { get; set; }
        public BookingCreateResult SupplierResponse { get; set; }
    }
}
