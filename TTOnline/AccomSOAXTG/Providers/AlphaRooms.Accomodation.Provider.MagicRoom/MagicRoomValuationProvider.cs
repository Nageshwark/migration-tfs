﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.MagicRoom;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<MagicRoomValuationResponse> automator;
        private readonly IAccommodationValuationParser<MagicRoomValuationResponse> parser;

        public MagicRoomValuationProvider(IAccommodationValuationAutomatorAsync<MagicRoomValuationResponse> automator,
                                                IAccommodationValuationParser<MagicRoomValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
