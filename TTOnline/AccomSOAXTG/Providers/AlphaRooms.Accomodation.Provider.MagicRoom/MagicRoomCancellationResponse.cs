﻿using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomCancellationResponse
    {
        public BookingCancel SupplierRequest { get; set; }
        public BookingCancelResult SupplierResponse { get; set; }
    }
}
