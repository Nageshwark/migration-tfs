﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces
{
    public interface IMagicRoomProviderBookingRequestFactory
    {
        BookingCreate CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
