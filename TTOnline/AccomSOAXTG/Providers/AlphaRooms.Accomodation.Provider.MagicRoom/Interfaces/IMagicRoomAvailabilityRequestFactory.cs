﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces
{
    public interface IMagicRoomAvailabilityRequestFactory
    {
        MagicRoomService.AvailabilitySearch CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
