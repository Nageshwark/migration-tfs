﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<MagicRoomBookingResponse>>
    {
        private const string MagicRoomBookingUrl = "MagicRoomBookingUrl";
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IMagicRoomProviderBookingRequestFactory _magicRoomProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public MagicRoomBookingAutomator(   ILogger logger,
                                           // IAccommodationConfigurationManager configurationManager,
                                            IMagicRoomProviderBookingRequestFactory MagicRoomSupplierBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._magicRoomProviderBookingRequestFactory = MagicRoomSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<MagicRoomBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            var responses = new List<MagicRoomBookingResponse>();

            BookingCreate supplierRequest = null;
            MagicRoomBookingResponse response = null;
         

            try
            {
                supplierRequest = this._magicRoomProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);


                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this._magicRoomProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingInitiateResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(request.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("MagicRoomBookingAutomator.GetBookingResponseAsync: MagicRoom supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<MagicRoomBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, BookingCreate supplierRequest)
        {
           
            var client = ConfigureSoapCleint(request);

            if (request.Debugging)
            {
                string requestXML = supplierRequest.XmlSerialize();
            }

            var supplierResponse = client.BookingCreate(supplierRequest);
            

            if (outputLogger !=null)
                outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            var response = new MagicRoomBookingResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = supplierResponse
            };

            return response;
        }


        private ServiceSoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MagicRoomBookingUrl)));
            ServiceSoapClient client = new ServiceSoapClient(binding, address);

            return client;
        }
    }
}
