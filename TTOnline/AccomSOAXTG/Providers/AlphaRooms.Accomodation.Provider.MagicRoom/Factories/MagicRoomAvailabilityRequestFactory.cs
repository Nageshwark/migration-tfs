﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;


namespace AlphaRooms.Accommodation.Provider.MagicRoom.Factories
{
    public class MagicRoomAvailabilityRequestFactory : IMagicRoomAvailabilityRequestFactory
    {
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MagicRoomOrg = "MagicRoomOrg";
        private const string MagicRoomCurrency = "MagicRoomCurrency";

        public AvailabilitySearch CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new AvailabilitySearch();

            var authority = new AuthorityElement();
            authority.User = request.Provider.Parameters.GetParameterValue(MagicRoomUsername);
            authority.Password = request.Provider.Parameters.GetParameterValue(MagicRoomPassword);
            authority.Org = request.Provider.Parameters.GetParameterValue(MagicRoomOrg);
            authority.Currency = request.Provider.Parameters.GetParameterValue(MagicRoomCurrency);
            supplierRequest.Authority = authority;

            if (request.EstablishmentCodes.Length > 0)
                supplierRequest.HotelId = Convert.ToInt32(request.EstablishmentCodes[0]);
            else if (request.DestinationCodes.Length > 0)
                supplierRequest.RegionId = Convert.ToInt32(request.DestinationCodes[0]);
            else
            {
                throw new ArgumentException("Please set a Destionation or Establishment Code");
            }
           
            supplierRequest.HotelStayDetails = new HotelStayDetailsElement();
            supplierRequest.HotelStayDetails.ArrivalDate = request.CheckInDate;
            supplierRequest.HotelStayDetails.Nights = CalculateDuration(request);

            supplierRequest.HotelStayDetails.Room = CreateRoom(request);
            
            

            return supplierRequest;
        }

        private HotelStayDetailsRoomElement [] CreateRoom(AccommodationProviderAvailabilityRequest request)
        {
            var rooms = new HotelStayDetailsRoomElement[request.Rooms.Length];
            int roomCounter = 0;
            int guestCount = 0;

            foreach (var room in request.Rooms)
            {
                var supplierRoom = new HotelStayDetailsRoomElement();
                guestCount = 0;
                supplierRoom.Guests = new GuestElement[room.Guests.Count()];

                foreach (var guest in room.Guests)
                {
                    if (guest.Type == GuestType.Adult)
                    {
                        supplierRoom.Guests[guestCount] = new AdultElement();
                    }
                    else if (guest.Type == GuestType.Child || guest.Type == GuestType.Infant)
                    {
                        supplierRoom.Guests[guestCount] = new ChildElement();
                    }
                    guestCount++;
                }

                rooms[roomCounter] = supplierRoom;
                roomCounter++;
            }

            return rooms;
        }

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return  Convert.ToInt32(duration.TotalDays);
        }
       
    }
}
