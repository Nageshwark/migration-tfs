﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomProviderBookingRequestFactory : IMagicRoomProviderBookingRequestFactory
    {
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MagicRoomOrg = "MagicRoomOrg";
        private const string MagicRoomCurrency = "MagicRoomCurrency";
        

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookingCreate CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new BookingCreate
            {
                Authority = CreateAuthorityElement(request),
                AgentReference = Guid.NewGuid().ToString(),
                CommitLevel = EnumCommitLevel.confirm,
                CommitLevelSpecified = true,
                //Customer = CreateCustomer(request.Customer),
                HotelBooking = CreateHotelBooking(request),
                //PaymentDetails = CreatePaymentDetails(request)

            };
            

            return supplierRequest;
        }

        private CreateBookingElement[] CreateHotelBooking(AccommodationProviderBookingRequest request)
        {
            return new[]
            {
                new CreateBookingElement
                {
                    QuoteId = request.ValuatedRooms.First().ValuationResult.ProviderSpecificData["QuoteId"],
                    HotelStayDetails = new HotelStayDetailsElement
                    {
                        Room = CreateRoom(request.ValuatedRooms)
                    }
                }
            };

        }

        private AuthorityElement CreateAuthorityElement(AccommodationProviderBookingRequest request)
        {
            return new AuthorityElement
            {
                User = request.Provider.Parameters.GetParameterValue(MagicRoomUsername),
                Password = request.Provider.Parameters.GetParameterValue(MagicRoomPassword),
                Org = request.Provider.Parameters.GetParameterValue(MagicRoomOrg),
                Currency = request.Provider.Parameters.GetParameterValue(MagicRoomCurrency),
                Version = "1.25"

            };
        }
        private PaymentDetailsElement CreatePaymentDetails(AccommodationProviderBookingRequest request)
        {
            return null;
        }

        

        private HotelStayDetailsRoomElement[] CreateRoom(AccommodationProviderBookingRequestRoom[] valuatedRooms)
        {
           var rooms = new List<HotelStayDetailsRoomElement>();

            foreach (var room in valuatedRooms)
            {
                var hotelStayRoom = new HotelStayDetailsRoomElement();
                var guests = new List<GuestElement>();
                var paxPriceString = room.ValuationResult.ProviderSpecificData["PaxPrice"];
                var paxPrice = paxPriceString.Split('|');

                foreach (var guestString in paxPrice)
                {
                    var details = guestString.Split(',');
                    GuestElement guest;

                    if (details[0] == "A")
                        guest = new AdultElement();
                    else
                        guest = new ChildElement();

                    guest.Title = "Mr";
                    guest.Forename = "TEST";
                    guest.Surname = "TEST";
                    guest.Id = Convert.ToInt32(details[1]);
                    guest.SellingPrice = new MoneyElement
                    {
                        Amount = Convert.ToDecimal(details[2]),
                        Currency = details[3],
                        Estimated = Convert.ToBoolean(details[4]),
                        Converted = Convert.ToBoolean(details[5])
                    };

                    guests.Add(guest);

                }

                hotelStayRoom.Guests = guests.ToArray();
                rooms.Add(hotelStayRoom);
            }

            return rooms.ToArray();
        }

       

        private CustomerElement CreateCustomer(AccommodationProviderBookingRequestCustomer customer)
        {
            throw new NotImplementedException();
        }
    }
}
