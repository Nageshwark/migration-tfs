﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;

namespace AlphaRooms.Accommodation.Provider.MagicRoom.Factories
{
    public class MagicRoomCancellationRequestFactory : IMagicRoomProviderCancellationRequestFactory
    {
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MagicRoomOrg = "MagicRoomOrg";
        private const string MagicRoomCurrency = "MagicRoomCurrency";

        public BookingCancel CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new BookingCancel
            {
                Authority = CreateAuthorityElement(request),
                BookingId = Convert.ToInt32(request.ProviderBookingReference),
                BookingIdSpecified = true,
                CommitLevel = EnumCommitLevel.confirm,
                CommitLevelSpecified = true,
                DetailLevel = EnumDetailLevel.full,
                DetailLevelSpecified = true
                

            };

            return supplierRequest;
        }
        private AuthorityElement CreateAuthorityElement(AccommodationProviderCancellationRequest request)
        {
            return new AuthorityElement
            {
                User = request.Provider.Parameters.GetParameterValue(MagicRoomUsername),
                Password = request.Provider.Parameters.GetParameterValue(MagicRoomPassword),
                Org = request.Provider.Parameters.GetParameterValue(MagicRoomOrg),
                Currency = request.Provider.Parameters.GetParameterValue(MagicRoomCurrency),
                Version = "1.25"

            };
        }
    }
}
