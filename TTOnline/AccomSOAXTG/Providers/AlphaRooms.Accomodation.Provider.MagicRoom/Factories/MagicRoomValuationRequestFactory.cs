﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;


namespace AlphaRooms.Accommodation.Provider.MagicRoom.Factories
{
    public class MagicRoomValuationRequestFactory : IMagicRoomProviderValuationRequestFactory
    {
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MagicRoomOrg = "MagicRoomOrg";
        private const string MagicRoomCurrency = "MagicRoomCurrency";


        public BookingCreate CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new BookingCreate
            {
                Authority = CreateAuthorityElement(request),
                AgentReference = Guid.NewGuid().ToString(),
                CommitLevel = EnumCommitLevel.prepare,
                CommitLevelSpecified = true,
                //Customer = CreateCustomer(request.Customer),
                HotelBooking = CreateHotelBooking(request),
                //PaymentDetails = CreatePaymentDetails(request)
               
            };


            return supplierRequest;
        }

        private AuthorityElement CreateAuthorityElement(AccommodationProviderValuationRequest request)
        {
            return new AuthorityElement
            {
                User = request.Provider.Parameters.GetParameterValue(MagicRoomUsername),
                Password = request.Provider.Parameters.GetParameterValue(MagicRoomPassword),
                Org = request.Provider.Parameters.GetParameterValue(MagicRoomOrg),
                Currency = request.Provider.Parameters.GetParameterValue(MagicRoomCurrency),
                Version = "1.25"

            };
        }


        private CreateBookingElement[] CreateHotelBooking(AccommodationProviderValuationRequest request)
        {
            return new[]
            {
                new CreateBookingElement
                {
                    QuoteId = request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["QuoteId"],
                    HotelStayDetails = new HotelStayDetailsElement
                    {
                        Room = CreateRoom(request.SelectedRooms)
                    }
                }
            };

        }

        private PaymentDetailsElement CreatePaymentDetails(AccommodationProviderValuationRequest request)
        {
            throw new NotImplementedException();
        }



        private HotelStayDetailsRoomElement[] CreateRoom(AccommodationProviderValuationRequestRoom[] selectedRooms)
        {
            var rooms = new List<HotelStayDetailsRoomElement>();

            foreach (var room in selectedRooms)
            {
                var hotelStayRoom = new HotelStayDetailsRoomElement();
                var guests = new List<GuestElement>();
                var paxPriceString = room.AvailabilityResult.ProviderSpecificData["PaxPrice"];
                var paxPrice = paxPriceString.Split('|');

                foreach (var guestString in paxPrice)
                {
                    var details = guestString.Split(',');
                    GuestElement guest;

                    if (details[0] == "A")
                        guest = new AdultElement();
                    else
                        guest = new ChildElement();

                    guest.Title = "Mr";
                    guest.Forename = "TEST";
                    guest.Surname = "TEST";
                    guest.Id = Convert.ToInt32(details[1]);
                    guest.SellingPrice = new MoneyElement
                    {
                        Amount = Convert.ToDecimal(details[2]),
                        Currency = details[3],
                        Estimated = Convert.ToBoolean(details[4]),
                        Converted = Convert.ToBoolean(details[5])
                    };

                    guests.Add(guest);

                }

                hotelStayRoom.Guests = guests.ToArray();
                rooms.Add(hotelStayRoom);
            }

            return rooms.ToArray();
            
        }

       

        
    }
}
