﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomValuationAutomator : IAccommodationValuationAutomatorAsync<MagicRoomValuationResponse>
    {
        private const string MagicRoomAvailabilityUrl = "MagicRoomAvailabilityUrl";
        private const string MagicRoomUsername = "MagicRoomUsername";
        private const string MagicRoomPassword = "MagicRoomPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IMagicRoomProviderValuationRequestFactory _magicRoomProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public MagicRoomValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IMagicRoomProviderValuationRequestFactory MagicRoomSupplierValuationRequestFactory
            , IProviderOutputLogger outputLogger
            , IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this._magicRoomProviderValuationRequestFactory = MagicRoomSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<MagicRoomValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            MagicRoomValuationResponse response = null;

            try
            {
                var supplierRequest = this._magicRoomProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("MagicRoomValuationAutomator.GetValuationResponseAsync: MagicRoom supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<MagicRoomValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, BookingCreate supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedXML = supplierRequest.XmlSerialize();
            }

            ServiceSoapClient client = ConfigureSoapCleint(request.SelectedRooms.First().AvailabilityRequest);

            this.loggerService.SetProviderRequest(request, supplierRequest);
            BookingCreateResult response = client.BookingCreate(supplierRequest);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, response);
            var supplierResponse = new MagicRoomValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private ServiceSoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MagicRoomAvailabilityUrl)));
            ServiceSoapClient client = new ServiceSoapClient(binding, address);

            return client;
        }
    
    }
}
