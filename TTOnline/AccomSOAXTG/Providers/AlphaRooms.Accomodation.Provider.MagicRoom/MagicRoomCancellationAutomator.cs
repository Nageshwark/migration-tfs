﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomCancellationAutomator : IAccommodationCancellationAutomatorAsync<MagicRoomCancellationResponse>
    {
        private const string MagicRoomCancellationUrl = "MagicRoomCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IMagicRoomProviderCancellationRequestFactory _magicRoomProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public MagicRoomCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IMagicRoomProviderCancellationRequestFactory MagicRoomSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._magicRoomProviderCancellationRequestFactory = MagicRoomSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<MagicRoomCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            MagicRoomCancellationResponse response = null;

            try
            {
                var supplierRequest = _magicRoomProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }
                
                var client = ConfigureSoapCleint(request);

                var supplierResponse = client.BookingCancel(supplierRequest);

                response = new MagicRoomCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("MagicRoomBookingAutomator.GetCancellationResponseAsync: MagicRoom supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }



        private ServiceSoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(MagicRoomCancellationUrl)));
            ServiceSoapClient client = new ServiceSoapClient(binding, address);

            return client;
        }
    }
}
