﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<MagicRoomCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<MagicRoomCancellationResponse> parser;

        public MagicRoomCancellationProvider(IAccommodationCancellationAutomatorAsync<MagicRoomCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<MagicRoomCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
