﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomAvailabilityParser : MagicRoomParserBase, IAccommodationAvailabilityParser<MagicRoomAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public MagicRoomAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            MagicRoomAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("MagicRoom Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, Accommodation.Provider.MagicRoom.MagicRoomService.AvailabilitySearch supplierRequest, Accommodation.Provider.MagicRoom.MagicRoomService.AvailabilitySearchResult supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.HotelAvailability !=null && supplierResponse.HotelAvailability.Length> 0 );
                
            
            if (hasResults)
            {
                foreach (var hotelElement in supplierResponse.HotelAvailability)
                {
                    int roomNumber = 1;

                    foreach (var room in hotelElement.Result.First().Room)
                    {
                        var result = new AccommodationProviderAvailabilityResult();

                        //result.Id = new Guid();
                        result.RoomNumber = (byte)roomNumber;

                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.PaymentModel = PaymentModel.PostPayment;

                        result.DestinationEdiCode = hotelElement.Hotel.Region.Id.ToString();
                        result.EstablishmentEdiCode = hotelElement.Hotel.Id.ToString();
                        result.EstablishmentName = hotelElement.Hotel.Name;

                        result.RoomDescription = room.RoomType.Text;
                        result.RoomCode = room.RoomType.Code;

                        result.CheckInDate = request.CheckInDate;
                        result.CheckOutDate = request.CheckOutDate;

                        result.BoardCode = room.MealType.Code;
                        result.BoardDescription = room.MealType.Text;
                        
                        
                        result.SalePrice = new Money()
                        {
                            Amount = room.SellingPrice.Amount,
                            CurrencyCode = room.SellingPrice.Currency
                        };

                        //what should we populate here? there is only one price in H4U response.
                        result.CostPrice = result.SalePrice;

                        
                        
                        result.Adults = GetPAXCount(room.Guests, typeof(AdultElement));
                        result.Children = GetPAXCount(room.Guests, typeof (ChildElement));
                        result.IsNonRefundable = room.IsNonRefundable;

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("IsPackageRate", room.IsPackageRate.ToString());
                        result.ProviderSpecificData.Add("QuoteId", hotelElement.Result.First().QuoteId);
                        result.ProviderSpecificData.Add("PaxPrice", GetPaxPriceString(room.Guests));

                        availabilityResults.Enqueue(result);

                        roomNumber++;
                    }
                }

            }
            else
            {
                string message = "MagicRoom Availability Parser: Availability Response cannot be parsed because it is null.";

                

                throw new SupplierApiDataException(message);
            }
            
        }

        
    }
}
