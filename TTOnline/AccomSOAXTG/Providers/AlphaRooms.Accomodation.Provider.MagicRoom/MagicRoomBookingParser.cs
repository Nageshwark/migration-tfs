﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomBookingParser : IAccommodationBookingParser<IEnumerable<MagicRoomBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public MagicRoomBookingParser(ILogger logger
            //IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<MagicRoomBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierResponse, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("MagicRoom Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        BookingCreateResult supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            // Debug Code
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (supplierResponse !=null && supplierResponse.Booking !=null);

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                result.ProviderBookingReference = supplierResponse.Booking.Id.ToString();
                result.ProviderSpecificData = new Dictionary<string, string>();

                var bookingIdList = new List<string>();

                foreach (var hb in supplierResponse.Booking.HotelBooking)
                {
                    bookingIdList.Add(hb.Id.ToString());
                }

                result.ProviderSpecificData.Add("HotelBookingId", string.Join(",", bookingIdList.ToArray()));

                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);

            }
            else
            {
                var sbMessage = new StringBuilder();
                
                sbMessage.AppendLine("Magic Room Booking Parser: Booking Response cannot be parsed because it is null.");

                throw new SupplierApiDataException(sbMessage.ToString());
            }
        }

        
    }
}
