﻿using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{
    public class MagicRoomAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<MagicRoomAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<MagicRoomAvailabilityResponse> parser;

        public MagicRoomAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<MagicRoomAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<MagicRoomAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
