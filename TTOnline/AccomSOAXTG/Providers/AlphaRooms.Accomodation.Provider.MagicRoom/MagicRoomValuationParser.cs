﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom.MagicRoomService;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.MagicRoom
{

    public class MagicRoomValuationParser : MagicRoomParserBase, IAccommodationValuationParser<MagicRoomValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public MagicRoomValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, MagicRoomValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("MagicRoom Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, BookingCreate supplierRequest, BookingCreateResult supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults =  (supplierResponse != null && 
                                supplierResponse.Booking!=null &&
                                supplierResponse.Booking.HotelBooking !=null && 
                                supplierResponse.Booking.HotelBooking.Any());


            if (hasResults)
            {
                int roomNumber = 1;
                var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

                foreach (var booking in supplierResponse.Booking.HotelBooking)
                {
                    var room = booking.Room.First();

                    var result = new AccommodationProviderValuationResult();

                    //result.Id = new Guid();
                    result.RoomNumber = (byte)roomNumber;

                    result.ProviderEdiCode = request.SelectedRooms.First().AvailabilityRequest.Provider.EdiCode;
                    result.PaymentModel = PaymentModel.PostPayment;

                    result.DestinationEdiCode = availabilityResult.DestinationEdiCode;
                    result.EstablishmentEdiCode = booking.HotelId.ToString();
                    result.EstablishmentName = booking.HotelName;

                    result.RoomDescription = room.RoomType.Text;
                    result.RoomCode = room.RoomType.Code;

                    result.CheckInDate = request.SelectedRooms.First().AvailabilityRequest.CheckInDate;
                    result.CheckOutDate = request.SelectedRooms.First().AvailabilityRequest.CheckOutDate;

                    result.BoardCode = room.MealType.Code;
                    result.BoardDescription = room.MealType.Text;
                    result.IsNonRefundable = room.IsNonRefundable;
                    

                    result.SalePrice = new Money()
                    {
                        Amount = room.TotalSellingPrice.Amount,
                        CurrencyCode = room.TotalSellingPrice.Currency
                    };

                    
                    result.CostPrice = result.SalePrice;

                    result.Adults = GetPAXCount(room.Guests, typeof(AdultElement));
                    result.Children = GetPAXCount(room.Guests, typeof(ChildElement));

                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("IsPackageRate", room.IsPackageRate.ToString());
                    result.ProviderSpecificData.Add("QuoteId", availabilityResult.ProviderSpecificData["QuoteId"]);
                    result.ProviderSpecificData.Add("PaxPrice", GetPaxPriceString(room.Guests));

                    availabilityResults.Enqueue(result);

                    roomNumber++;
                }
            }
            else
            {
                string message = "MagicRoom Valuation Parser: Valuation Response cannot be parsed because it is null.";



                throw new SupplierApiDataException(message);
            }
        }

        
    }
}
