﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateValuationParser : IAccommodationValuationParserAsync<TravelGateValuationResponse>
    {
        private readonly ILogger logger;
        private JsonSerializerSettings settings;
        public TravelGateValuationParser(ILogger logger)
        {
            this.logger = logger;
        }
        public async Task<IEnumerable<AccommodationProviderValuationResult>> GetValuationResultsAsync(AccommodationProviderValuationRequest request, TravelGateValuationResponse responses)
        {
            logger.Info("Meta Valuation, Travelgate valuation parser started for id - {0}", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"]);
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateValuationResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage;

                if (ex is AggregateException)
                {
                    var aggregateEx = (AggregateException)ex;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("TravelGate Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            logger.Info("Meta Valuation, Travelgate valuation parser completed for id - {0}", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"]);

            return valuationResults;
        }

        private void CreateValuationResultsFromResponse(AccommodationProviderValuationRequest request, string supplierRequest, string supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            TravelGateValuationResponseContainer response = null;

            if (supplierResponse == null)
            {
                string message = string.Format("TravelGate Valuation Parser: Valuation Response cannot be parsed because it is null response from token -{0} and optionRefId- {1}", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["token"], request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"]);
                logger.Error(message);
                new SupplierApiException(message); return;
            }
            try
            {
                response = Deserialize<TravelGateValuationResponseContainer>(supplierResponse);
            }
            catch (Exception ex)
            {
                string message = string.Format("Error occured while deserialize response data from token- {0}{1} and [optionRefId- {2}] - details from XTG; {3}SupplierResponse- {4}{5}Error Message- {6}", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["token"], Environment.NewLine, request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"], Environment.NewLine, supplierResponse, Environment.NewLine, ex.GetMessageWithInnerExceptions());
                logger.Error(message);
                new SupplierApiException(message,ex);
                return;
            }

            if (response.data.quote.hotel.errors != null)
            {
                string customError = string.Format(" Valuation Error occured while getting token -{0}{1} and [optionRefId- {2}] - details from XTG; {3} Request GraphQL - {4}{5} Response graphQL - {6}{7} Error Description - {8} ", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["token"], Environment.NewLine, request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"], Environment.NewLine, supplierRequest, Environment.NewLine, supplierResponse, Environment.NewLine, response.data.quote.hotel.errors[0].description);
                logger.Error(customError);
                new SupplierApiException(customError); return;
            }
            if (response != null)
            {
                try
                {
                    var boardBasisHelper = new BoardBasisHelper();
                    var paymentTypeHelper = new PaymentTypeHelper();
                    var availResult = request.SelectedRooms.First().AvailabilityResult;
                    var room = response.data.quote.hotel.optionQuote;
                    
                    var result = new AccommodationProviderValuationResult();
                   
                    result.RoomNumber = request.SelectedRooms.First().RoomNumber;
                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.PaymentModel = availResult.PaymentModel;
                    result.DestinationEdiCode = availResult.DestinationEdiCode;
                    result.EstablishmentEdiCode = availResult.EstablishmentEdiCode;
                    result.EstablishmentName = availResult.EstablishmentName;
                    result.RoomDescription = availResult.RoomDescription;
                    result.RoomCode = availResult.RoomCode;
                    result.CheckInDate = availResult.CheckInDate;
                    result.CheckOutDate = availResult.CheckOutDate;
                    result.Adults = availResult.Adults;
                    result.Children = availResult.Children;
                    result.ProviderName = request.SelectedRooms.First().AvailabilityResult.ProviderName;
                    result.ProviderProviderCode = request.SelectedRooms.First().AvailabilityResult.ProviderEdiCode;
                    result.IsNonRefundable = availResult.IsNonRefundable;
                    var bbDescription = boardBasisHelper.GetBoardBasis(availResult.BoardCode);

                    if (bbDescription != null)
                    {
                        result.BoardCode = bbDescription.Code;
                        result.BoardDescription = bbDescription.Name.Equals("All-Inclusive")
                                                  ? "All Inclusive"
                                                  : bbDescription.Name;
                    }

                    result.SalePrice = new Money()
                    {
                        Amount = room.price.gross == 0 ? room.price.net : room.price.gross,
                        CurrencyCode = room.price.currency
                    };

                    result.CostPrice = result.SalePrice;
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    if (room.addOns != null)
                    {

                        var addons = Deserialize<TravelGateAddons>(room.addOns.distribute);
                        //price ovrride
                        result.SalePrice = new Money()
                        {
                            Amount = addons.markups.selling[0].gross == 0 ? 0 : addons.markups.selling[0].gross,
                            CurrencyCode = addons.markups.selling[0].currency
                        };

                        result.CostPrice = new Money()
                        {
                            Amount = addons.markups.purchasing[0].net == 0 ? 0 : addons.markups.purchasing[0].net,
                            CurrencyCode = addons.markups.purchasing[0].currency
                        };
						result.EstablishmentEdiCode = addons.hotelCode;

                        //assigning exchange rate 
                        result.ProviderSpecificData.Add("exchangeRate", addons.markups.selling.FirstOrDefault().exchange.rate.ToString());
                    }

                    result.RateType = RateType.NA;
                    result.CancellationPolicy = CreateCancellationPolicy(response, request);
                    result.IsBindingRate = true;
                    result.ProviderSpecificData.Add("optionRefId", room.optionRefId);
                    result.ProviderSpecificData.Add("availbilityResult", availResult.ProviderSpecificData["distribute"]);

                    availabilityResults.Enqueue(result);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occured while parsing response data from token- {0}{1} SupplierResponse- {2}{3}Error Message- {4}", request.SelectedRooms.First().AvailabilityRequest.MetaToken, Environment.NewLine, supplierResponse, Environment.NewLine, ex.GetMessageWithInnerExceptions());
                }
            }
            else
            {
                string message = "TravelGate Valuation Parser: Valuation Response cannot be parsed because it is null.";
                throw new SupplierApiDataException(message);
            }
        }
        private T Deserialize<T>(string response)
        {
            settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<T>(response, settings);
            return root;
        }

        private string[] CreateCancellationPolicy(TravelGateValuationResponseContainer container, AccommodationProviderValuationRequest request)
        {
            List<string> policies = new List<string>();

            if (container.data.quote.hotel.optionQuote.cancelPolicy.cancelPenalties == null) return null;

            foreach (var policy in container.data.quote.hotel.optionQuote.cancelPolicy.cancelPenalties)
            {
                string cancellationPolicy = string.Format("If you choose to cancel your booking before {0} hours there will be a charge of {1}{2}", policy.hoursBefore, request.ChannelInfo.CurrencySymbol, policy.value);
                policies.Add(cancellationPolicy);
            }

            return policies.ToArray();
        }
    }
}
