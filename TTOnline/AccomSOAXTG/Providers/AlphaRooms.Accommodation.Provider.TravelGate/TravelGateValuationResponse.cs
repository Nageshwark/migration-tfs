﻿namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateValuationResponse
    {
        public string SupplierRequest;

        public string SupplierResponse;
    }
}
