﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<TravelGateValuationResponse> automator;
        private readonly IAccommodationValuationParserAsync<TravelGateValuationResponse> parser;

        public TravelGateValuationProvider(IAccommodationValuationAutomatorAsync<TravelGateValuationResponse> automator,
                                                IAccommodationValuationParserAsync<TravelGateValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return await parser.GetValuationResultsAsync(request, responses);
        }
    }
}
