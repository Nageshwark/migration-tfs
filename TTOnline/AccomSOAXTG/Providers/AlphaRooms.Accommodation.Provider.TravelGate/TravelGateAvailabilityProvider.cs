﻿using AlphaRooms.Accommodation.Core.Contracts;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System.Collections.Generic;
using System;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<TravelGateAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<TravelGateAvailabilityResponse> parser;
      
        public TravelGateAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<TravelGateAvailabilityResponse> automator, IAccommodationAvailabilityParser<TravelGateAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var response = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, response);    
        }
    }
}
