﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateAvailabilityResponse
    {
        public string SupplierResponse { get; set; }
        public string SupplierRequest { get; set; }
    }
}
