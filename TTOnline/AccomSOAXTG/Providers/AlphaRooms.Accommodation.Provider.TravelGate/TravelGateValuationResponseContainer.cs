﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateValuationResponseContainer
    {
        public TravelGateValuationData data { get; set; }
    }

    public class TravelGateValuationData
    {
        public TravelGateValuationQuote quote { get; set; }
    }

    public class TravelGateValuationQuote
    {
        public TravelGateValuationHotel hotel { get; set; }
    }

    public class TravelGateValuationHotel
    {
        public TravelGateValuationOptionQuote optionQuote { get; set; }
        public TravelGateValuationError[] errors { get; set; }
        public TravelGateValuationWarnings warnings { get; set; }
    }

    public class TravelGateValuationWarnings
    {
    }

    public class TravelGateValuationError
    {
        public string code { get; set; }
        public string type { get; set; }
        public string description { get; set; }
    }

    public class TravelGateValuationOptionQuote
    {
        public string optionRefId { get; set; }
        public string status { get; set; }
        public TravelGateValuationSurCharges surcharges { get; set; }
        public TravelGateValuationCardTypes cardTypes { get; set; }
        public string remarks { get; set; }
        public TravelGateValuationPrice price { get; set; }
        public TravelGateValuationCancelPolicy cancelPolicy { get; set; }
        public TravelGateResponseAddOns addOns { get; set; }
    }

    public class TravelGateValuationCancelPolicy
    {
        public bool refundable { get; set; }
        public TravelGateValuationCancelPenalties[] cancelPenalties { get; set; }
    }

    public class TravelGateValuationCancelPenalties
    {
        public string currency { get; set; }
        public int hoursBefore { get; set; }
        public string penaltyType { get; set; }
        public decimal value { get; set; }
    }

    public class TravelGateValuationPrice
    {
        public string currency { get; set; }
        public bool binding { get; set; }
        public decimal net { get; set; }
        public decimal gross { get; set; }
        public decimal commissionPercentage { get; set; }
    }

    public class TravelGateValuationNet
    {
        public string currency { get; set; }
        public decimal value { get; set; }
        public TravelGateValuationCommisionPercentage commissionPercentage { get; set; }
        public TravelGateValuationCommisionAmount commissionAmount { get; set; }
    }

    public class TravelGateValuationGross
    {
        public string currency { get; set; }
        public decimal value { get; set; }
    }

    public class TravelGateValuationAmount
    {
        public bool binding { get; set; }
        public string currency { get; set; }
        public decimal value { get; set; }
        public TravelGateValuationCommisionPercentage commissionPercentage { get; set; }
        public TravelGateValuationCommisionAmount commissionAmount { get; set; }
    }

    public class TravelGateValuationCommisionAmount
    {
    }

    public class TravelGateValuationCommisionPercentage
    {
    }

    public class TravelGateValuationRemarks
    {
    }

    public class TravelGateValuationCardTypes
    {
    }

    public class TravelGateValuationSurCharges
    {
    }
    public class TravelGateAddons
    {
        public string HOCOReference { get; set; }
        public string currency { get; set; }
        public bool binding { get; set; }
        public decimal net { get; set; }
        public decimal gross { get; set; }
        public string supplier { get; set; }
        public TravelGateExchange exchange { get; set; }
        public TravelGateMarkups markups { get; set; }
		public string hotelCode { get; set; }
	}

    public class TravelGateMarkups
    {
        public TravelGateMarkup[] purchasing { get; set; }
        public TravelGateMarkup[] selling { get; set; }
    }

    public class TravelGateMarkup
    {
        public string channel { get; set; }
        public string currency { get; set; }
        public bool binding { get; set; }
        public decimal net { get; set; }
        public decimal gross { get; set; }
        public TravelGateExchange exchange { get; set; }
        public TravelGateMarkupRules[] markupRules { get; set; }
    }

    public class TravelGateMarkupRules
    {
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public decimal markup { get; set; }
    }


    public class TravelGateExchange
    {
        public string currency { get; set; }
        public decimal rate { get; set; }
    }
}
