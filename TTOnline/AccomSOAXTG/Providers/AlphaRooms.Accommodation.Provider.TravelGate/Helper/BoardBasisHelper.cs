﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Helper
{
    public class BoardBasisHelper
    {
        private List<BoardBasis> _valueAdds;

        public BoardBasisHelper()
        {
            _valueAdds = new List<BoardBasis>
            {
                // need a doc
                new BoardBasis { Name = "Self catering", Code = "2"},
                new BoardBasis { Name = "Room only", Code = "1"},
                new BoardBasis { Name = "Continental Breakfast", Code = "3" },
                new BoardBasis { Name = "Half board" , Code = "4"},
                new BoardBasis { Name = "Full board", Code = "5" },
                new BoardBasis { Name = "All-Inclusive", Code = "6" },               
            };
        }
        public BoardBasis GetBoardBasis(string valueAdds)
        {
            if (valueAdds == null)
                return null;

            
                var bbQuery = from BoardBasis bb in _valueAdds
                              where bb.Code == valueAdds
                              select bb;

                if (bbQuery.Any())
                {
                    return (BoardBasis)bbQuery.First();
                }
          

            return null;
        }
    }

    public class BoardBasis
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

