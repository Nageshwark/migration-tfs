﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Helper
{
    public class ExceptionHelper
    {
        public ApiError error { get; set; }
    }

    public class ApiError
    {
        public string code { get; set; }
        public string type { get; set; }
        public string description { get; set; }
    }
}
