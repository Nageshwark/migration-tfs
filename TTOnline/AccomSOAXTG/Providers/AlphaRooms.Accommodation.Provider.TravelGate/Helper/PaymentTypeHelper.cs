﻿using AlphaRooms.SOACommon.Contracts.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Helper
{
    public class PaymentTypeHelper
    {
        public PaymentModel GatePaymentType(string supplierPaymentType)
        {
            PaymentModel paymentType;
            switch (supplierPaymentType.ToLower())
            {
                case "merchant":
                    paymentType = PaymentModel.PostPayment;
                    break;
                case "laterpay":
                    paymentType = PaymentModel.CustomerPayDirect;
                    break;
                case "cardbookingpay":
                    paymentType = PaymentModel.PayWithVirtualCard;
                    break;
                case "cardchekinpay":
                    paymentType = PaymentModel.PayWithVirtualCard;
                    break;
                default:
                    paymentType = PaymentModel.PostPayment;
                    break;
            }
            return paymentType;
        }
    }
}
