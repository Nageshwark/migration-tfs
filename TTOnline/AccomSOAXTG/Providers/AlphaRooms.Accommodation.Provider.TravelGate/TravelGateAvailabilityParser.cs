﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateAvailabilityParser : IAccommodationAvailabilityParser<TravelGateAvailabilityResponse>
    {
        private readonly ILogger logger;
        private JsonSerializerSettings settings;
        private int childAge = 17;
        private int infantAge = 1;
        private readonly IAccommodationConfigurationManager configurationManager;

        public TravelGateAvailabilityParser(ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, TravelGateAvailabilityResponse response)
        {
            return CreateAvailabilityResultsFromResponse(request, response);
        }

        private IEnumerable<AccommodationProviderAvailabilityResult> CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
           TravelGateAvailabilityResponse supplierResponse)
        {
            logger.Info("Create availability results from travelgate response for token - {0}", request.MetaToken);

            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();
            TravelGateResponseContainer response = null;

            if (supplierResponse.SupplierResponse == null)
            {
                string message = string.Format("TravelGate Availbility Parser: Availability Response cannot be parsed because it is null response from token -{0}", request.MetaToken);
                new SupplierApiException(message); return null;
            }
            try
            {
                response = Deserialize<TravelGateResponseContainer>(supplierResponse.SupplierResponse);
                // logging the complete meta response for splunk
                string responseQuery = string.Format("Travelgate Availability response: {0}{1} - {2}{3}", request.MetaToken, Environment.NewLine, supplierResponse.SupplierResponse, Environment.NewLine);

                if (this.configurationManager.EnableXTGLogging)
                    logger.Error(responseQuery);
            }
            catch (Exception ex)
            {
                string message = string.Format("Error occured while deserialize response data from token- {0}{1} SupplierResponse- {2}{3}Error Message- {4}", request.MetaToken, Environment.NewLine, supplierResponse.SupplierResponse, Environment.NewLine, ex.GetMessageWithInnerExceptions());
                logger.Error(message);
                new SupplierApiException(message, ex); return null;
            }

            Func<string, string> getRoomDescription = (roomDescripton) => !roomDescripton.Contains("Refundable") ? $"{roomDescripton} - Non Refundable" : roomDescripton;
            var hasResults = (response?.data.search.hotel?.options != null && Convert.ToInt32(response.data.search.hotel.options.Length) > 0);
            if (!hasResults)
            {
                string customError = string.Format("B2C Supplier Availability error occurred while getting token {0} details from XTG;{1} Request GraphQL - {2}{3} Response graphQL - {4}{5} Error Descriptop - {6}", request.MetaToken, Environment.NewLine, supplierResponse.SupplierRequest, Environment.NewLine, supplierResponse.SupplierResponse, Environment.NewLine, response?.data.search.hotel.errors[0].description);
                logger.Error(customError);
                new SupplierApiException(customError); return null;
            }
            var rooms = response.data.search.hotel.options;
            var PaymentTypeHelper = new PaymentTypeHelper();
            var boardBasisHelper = new BoardBasisHelper();

            try
            {
                List<AccommodationAvailabilityRequestRoomGuests> roomGuests = new List<AccommodationAvailabilityRequestRoomGuests>();
                if (request.MetaToken != null)
                {
                    //room criteria parsing
                    response.data.search.hotel.requestCriteria.occupancies.ToList().ForEach(i => {
                        AccommodationAvailabilityRequestRoomGuests roomGuest = new AccommodationAvailabilityRequestRoomGuests();
                        List<byte> ages = new List<byte>();
                        i.paxes.ToList().ForEach(j => {
                            ages.Add(j.age);
                        });

                        roomGuest.Age = ages;
                        roomGuests.Add(roomGuest);
                    });
                }
                rooms.ToList().ForEach(establishmentRoom =>
                {
                    foreach (var room in establishmentRoom.rooms)
                    {

                        var result = new AccommodationProviderAvailabilityResult
                        {
                            ProviderEdiCode = request.Provider.EdiCode,
                            CheckInDate = request.MetaToken != null ? DateTime.Parse(response.data.search.hotel.requestCriteria.checkIn) : request.CheckInDate,
                            CheckOutDate = request.MetaToken != null ? DateTime.Parse(response.data.search.hotel.requestCriteria.checkOut) : request.CheckOutDate,
                            PaymentModel = PaymentTypeHelper.GatePaymentType(establishmentRoom.paymentType),
                            EstablishmentEdiCode = establishmentRoom.hotelCode.ToString(),
                            EstablishmentName = establishmentRoom.hotelName,
                            IsNonRefundable = room.refundable == null ? establishmentRoom.cancelPolicy == null ? true : !establishmentRoom.cancelPolicy.refundable : !Boolean.Parse(room.refundable),
                        };

                        result.RoomNumber = (byte)Int32.Parse(room.occupancyRefId);
                        int adults = 0;
                        int childern = 0;
                        int infants = 0;
                        establishmentRoom.occupancies.ToList().Where(k => k.id == result.RoomNumber).ForEach(i => {
                            adults += i.paxes.CountByte(j => j.age > childAge);
                            childern += i.paxes.CountByte(j => j.age > infantAge && j.age <= childAge);
                            infants += i.paxes.CountByte(j => j.age < infantAge);
                        });

                        result.Adults = (byte)adults;
                        result.Children = (byte)childern;
                        result.Infants = (byte)infants;
                        result.RoomGuests = roomGuests;
                        result.RoomDescription = result.IsNonRefundable
                                                ? getRoomDescription(room.description)
                                                : room.description;
                        result.RoomCode = room.code.ToString();

                        var bbDescription = boardBasisHelper.GetBoardBasis(establishmentRoom.boardCode);

                        if (bbDescription != null)
                        {
                            result.BoardCode = bbDescription.Code;
                            result.BoardDescription = bbDescription.Name.Equals("All-Inclusive")
                                                      ? "All Inclusive"
                                                      : bbDescription.Name;
                        }

                        //Put by default room price we need to override by addons value. (incase addons are not coming we handle price by this)
                        result.SalePrice = new Money()
                        {
                            Amount = room.roomPrice.price.gross == 0 ? 0 : room.roomPrice.price.gross,
                            CurrencyCode = room.roomPrice.price.currency
                        };

                        result.CostPrice = new Money()
                        {
                            Amount = room.roomPrice.price.net == 0 ? 0 : room.roomPrice.price.net,
                            CurrencyCode = room.roomPrice.price.currency
                        };

                        result.RateType = RateType.NA;
                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("id", establishmentRoom.id);
                        result.ProviderSpecificData.Add("token", establishmentRoom.token);
                        result.SupplierEdiCode = establishmentRoom.supplierCode;
                        result.ProviderProviderCode = establishmentRoom.supplierCode;                        
                        if (establishmentRoom.addOns != null)
                        {
                            var addons = Deserialize<TravelGateAddons>(establishmentRoom.addOns.distribute);
                            result.ProviderSpecificData.Add("distribute", establishmentRoom.addOns.distribute.ToString());

                            //price ovrride
                            result.SalePrice = new Money()
                            {
                                Amount = addons.markups.selling[0].gross == 0 ? 0 : addons.markups.selling[0].gross,
                                CurrencyCode = addons.markups.selling[0].currency
                            };

                            result.CostPrice = new Money()
                            {
                                Amount = addons.markups.purchasing[0].net == 0 ? 0 : addons.markups.purchasing[0].net,
                                CurrencyCode = addons.markups.purchasing[0].currency
                            };

                            //assigning exchange rate 
                            result.ProviderSpecificData.Add("exchangeRate", addons.markups.selling.FirstOrDefault().exchange.rate.ToString());

                            // override edi code if addons not null
                            result.SupplierEdiCode = result.SupplierEdiCode + '-' + addons.supplier;
                            result.ProviderProviderCode = result.SupplierEdiCode;
                        }
                        availabilityResults.Enqueue(result);
                    }
                });
            }
            catch (Exception exception)
            {
                logger.Error("Error occured while parsing travelgate response for token- {0}{1} SupplierResponse- {2}{3}Error Message- {4}", request.MetaToken, Environment.NewLine, supplierResponse.SupplierResponse, Environment.NewLine, exception.GetMessageWithInnerExceptions());
            }

            logger.Info("Create availability results from travelgate response is completed for token - {0}", request.MetaToken);
            return availabilityResults;
        }
        private T Deserialize<T>(string response)
        {
            settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<T>(response, settings);
            return root;
        }
    }
}
