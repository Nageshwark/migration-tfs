﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateBookingParser : IAccommodationBookingParser<TravelGateBookingResponse>
    {
        private readonly ILogger logger;
        private JsonSerializerSettings settings;
        public TravelGateBookingParser(ILogger logger)
        {
            this.logger = logger;
        }
        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, TravelGateBookingResponse bookingResponse)
        {
            TravelGateBookingResponseContainer response = null;
            try
            {
                response = Deserialize<TravelGateBookingResponseContainer>(bookingResponse.supplierBookingResponse);
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetMessageWithInnerExceptions());
            }

            var result = new ConcurrentQueue<AccommodationProviderBookingResult>();
			if (response.data.hotelX.book.errors == null)
            {                    
                try
                {
                    string boookngRef = string.Empty;
                    if (response.data.hotelX.book.booking.addOns != null)
                    {
                        var addons = Deserialize<TravelGateAddons>(response.data.hotelX.book.booking.addOns.distribute);
                        boookngRef = addons.HOCOReference + "/" + response.data.hotelX.book.booking.reference.supplier;
					}
                    else
                    {
                        boookngRef = response.data.hotelX.book.booking.reference.supplier;
                    }
                    result.Enqueue(new AccommodationProviderBookingResult()
                    {
                        BookingStatus = BookingStatus.Confirmed,
                        ProviderBookingReference = boookngRef,
                        Message = string.Join(",", response.data.hotelX.book.booking.remarks),
					});
                }
                catch (Exception exception)
                {
                    logger.Error($"TravelGate Booking : Error in  Response  {exception.GetDetailedMessageWithInnerExceptions()} ");
                    throw new SupplierApiException($"TravelGate Booking : Error in  Response  {exception.GetDetailedMessageWithInnerExceptions()} ");
                }
            }
            else
            {
                result.Enqueue(new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    ProviderBookingReference = String.Empty
                });
                var errorMessage = $"TravelGate Booking Error: {response}";
                logger.Error(errorMessage);
            }
            return result;
        }

        private T Deserialize<T>(string response)
        {
            settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<T>(response, settings);
            return root;
        }
    }
}
