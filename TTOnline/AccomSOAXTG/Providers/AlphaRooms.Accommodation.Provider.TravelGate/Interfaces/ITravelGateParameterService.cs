﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System.Collections.Generic;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Interfaces
{
    public interface ITravelGateParameterService
    {
        void InitializeParameters(List<AccommodationProviderParameter> parameters, int channelID);
        string ServiceUrl { get; }
        string ApiKey { get; }
        string MetaRequest { get; }
        string ValuationRequest { get; }
        string BookingRequest { get; }
        string OrganicRequest { get; }
        string ClientSettings { get; }
	    T Deserialize<T>(string clientSettings);
    }
}
