﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class GraphQLQuery
    {
        public string query { get; set; }
        public object variables { get; set; }
    }
}
