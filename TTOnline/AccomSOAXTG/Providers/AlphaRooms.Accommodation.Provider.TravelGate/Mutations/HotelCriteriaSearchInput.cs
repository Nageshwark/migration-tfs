﻿using System;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Mutations
{
    public class HotelCriteriaSearchInput
    {
        public string checkIn { get; set; }
        public string checkOut { get; set; }
        public string[] hotels { get; set; }
        public string[] destinations { get; set; }
        public HotelCriteriaSearchOccupancies[] occupancies { get; set; }
        public string language { get; set; }
        public string nationality { get; set; }
        public string currency { get; set; }
        public string market { get; set; }
    }

    public class HotelCriteriaSearchOccupancies
    {
        public HotelCriteriaSearchPaxes[] paxes { get; set; }
    }

    public class HotelCriteriaSearchPaxes
    {
        public int age { get; set; }
    }

    public class HotelSettingsInputByEstablishment
    {
        public string client { get; set; }
        public bool auditTransactions { get; set; }
    }

    public class HotelSettingsInput : HotelSettingsInputByEstablishment
    {
        public Plugins[] plugins { get; set; }
    }

    public class Plugins
    {
        public string step { get; set; }

        public PluginsType[] pluginsType { get; set; }
    }

    public class PluginsType
    {
        public string name { get; set; }

        public string type { get; set; }

        public Parameters[] parameters { get; set; }
    }

    public class Parameters
    {
        public string key { get; set; }

        public string value { get; set; }
    }
}
