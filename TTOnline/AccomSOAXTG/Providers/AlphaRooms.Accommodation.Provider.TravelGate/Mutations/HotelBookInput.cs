﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Mutantions
{
    public class HotelBookInput
    {
        public string optionRefId { get; set; }
        
        public string clientReference { get; set; }
        public HotelBookInputDeltaPrice deltaPrice { get; set; }
        public HotelBookInputHolder holder { get; set; }
        public HotelBookInputRooms[] rooms { get; set; }
    }

    public class HotelBookInputRooms
    {
        public byte occupancyRefId { get; set; }
        public HotelBookInputPaxes[] paxes { get; set; }
    }

    public class HotelBookInputPaxes
    {
        public string name { get; set; }
        public string surname { get; set; }
        public int age { get; set; }
    }

    public class HotelBookInputHolder
    {
        public string name { get; set; }
        public string surname { get; set; }
    }

    public class HotelBookInputDeltaPrice
    {
        public decimal amount { get; set; }
        public decimal percent { get; set; }
        public bool applyBoth { get; set; }
    }
}
