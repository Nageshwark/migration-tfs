﻿namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateBookingResponse
    {
        public string supplierBookingRequest;
        public string supplierBookingResponse;
    }
}
