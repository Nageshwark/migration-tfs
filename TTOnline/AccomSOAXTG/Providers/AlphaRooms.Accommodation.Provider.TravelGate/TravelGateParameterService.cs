﻿using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using Newtonsoft.Json;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateParameterService : ITravelGateParameterService
    {
        private const string TravelGateServiceUrl = "TravelGateServiceUrl";
        private const string TravelGateApiKey = "TravelGateApiKey";
        private const string TravelGateMetaRequest = "TravelGateMetaRequest";
        private const string TravelGateValuationRequest = "TravelGateValuationRequest";
        private const string TravelGateBookingRequest = "TravelGateBookingRequest";
        private const string TravelGateOrganicRequest = "TravelGateOrganicRequest";
        private const string TravelGateClientSettings = "TravelGateClientSettings.";
        private JsonSerializerSettings settings;

		private List<AccommodationProviderParameter> providerParameters;
        public string ServiceUrl { get; private set; }
        public string BookingRequest { get; private set; }
        public string MetaRequest { get; private set; }
        public string ValuationRequest { get; private set; }
        public string OrganicRequest { get; private set; }
        public string ApiKey { get; private set; }
        public string ClientSettings { get; private set; }

        public void InitializeParameters(List<AccommodationProviderParameter> parameters, int channelID)
        {
            //initilase parameter according to requirement
            this.providerParameters = parameters;
            ServiceUrl = providerParameters.GetParameterValue(TravelGateServiceUrl);
            MetaRequest = providerParameters.GetParameterValue(TravelGateMetaRequest);
            ApiKey = providerParameters.GetParameterValue(TravelGateApiKey);
            ValuationRequest = providerParameters.GetParameterValue(TravelGateValuationRequest);
            BookingRequest = providerParameters.GetParameterValue(TravelGateBookingRequest);
            OrganicRequest = providerParameters.GetParameterValue(TravelGateOrganicRequest);
            ClientSettings = providerParameters.GetParameterValue(TravelGateClientSettings + channelID);
        }

		public T Deserialize<T>(string response)
		{
			settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
			var root = JsonConvert.DeserializeObject<T>(response, settings);
			return root;
		}
    }
}
