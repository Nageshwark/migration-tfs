﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.TravelGate.Helper;
using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutant;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutations;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.WebScraping;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateValuationAutomator : IAccommodationValuationAutomatorAsync<TravelGateValuationResponse>
    {
        private readonly ILogger logger;
        private readonly ITravelGateProviderValuationRequestFactory travelGateProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly ITravelGateParameterService parameterService;
        private readonly IProviderLoggerService loggerService;
        private JsonSerializerSettings settings;

        public TravelGateValuationAutomator(ILogger logger, ITravelGateProviderValuationRequestFactory TravelGateSupplierValuationRequestFactory
            , IProviderLoggerService loggerService, IProviderOutputLogger outputLogger, ITravelGateParameterService parameterService)
        {
            this.logger = logger;
            this.travelGateProviderValuationRequestFactory = TravelGateSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
            this.parameterService = parameterService;
        }

        public async Task<TravelGateValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            try
            {
                // Initialise provider parameter
                parameterService.InitializeParameters(request.Provider.Parameters, request.ChannelInfo.ChannelId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetMessageWithInnerExceptions());
            }

            TravelGateValuationResponse response = await GetSupplierValuationResponseAsync(request);
            return response;
        }

        private async Task<TravelGateValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            WebScrapeResponse response = null;
            string supplierValuationRequest = parameterService.ValuationRequest;

            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;
            var criteriaQuote = new HotelCriteriaQuoteInput();
            criteriaQuote.optionRefId = availabilityResult.ProviderSpecificData["id"];

            var fullQuery = new GraphQLQuery()
            {
                query = supplierValuationRequest,
                variables = new { criteriaQuote = criteriaQuote, settings = parameterService.Deserialize<HotelSettingsInput>(parameterService.ClientSettings) },
            };

            string supplierRequest = JsonConvert.SerializeObject(fullQuery);

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = false, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationJson };

            string url = parameterService.ServiceUrl;

            WebHeaderCollection header = new WebHeaderCollection();
            header.Add("Content-Type", "application/json");
            header.Add("Authorization", parameterService.ApiKey);
            this.loggerService.SetProviderRequest(request, supplierRequest);

            try
            {
                response = await webScrapeClient.XTGPostAsync(url, supplierRequest, header);
            }
            catch (Exception exception)
            {
                settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
                var exceptionMessage = JsonConvert.DeserializeObject<ExceptionHelper>(exception.Message, settings);

                string customError = string.Format("Error occured while getting token -{0} and [optionRefId- {1}] - details from XTG;{2} Request GraphQL - {3}{4} Response graphQL - {5}{6} Error Descriptop - {7} ", request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["token"], request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["id"], Environment.NewLine, supplierRequest, Environment.NewLine, exception.Message, Environment.NewLine, exceptionMessage.ToString());
                logger.Error("B2C Supplier Valuation error:- {0}{1}", customError, exception.StackTrace);
                throw new SupplierApiException(customError);
            }

            outputLogger.LogValuationResponse(request, ProviderOutputFormat.Json, response.Value);
            this.loggerService.SetProviderResponse(request, response.Value);

            return new TravelGateValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };
        }
    }
}
