﻿namespace AlphaRooms.Accommodation.Provider.TravelGate
{
	public class TravelGateBookingResponseContainer
	{
		public TGData data { get; set; }
	}

	public class TGData
	{
		public TGHotelX hotelX { get; set; }
	}

	public class TGHotelX
	{
		public TGBook book { get; set; }
	}

	public class TGBook
	{
		public TGAuditData auditData { get; set; }
		public TGBooking booking { get; set; }
		public object errors { get; set; }
		public object warnings { get; set; }
	}

	public class TGAuditData
	{
		public TGTransactions[] transactions { get; set; }
	}

	public class TGTransactions
	{
		public string request { get; set; }
		public string response { get; set; }
	}

	public class TGBooking
	{
		public Price price { get; set; }
		public Addons addOns { get; set; }
		public string status { get; set; }
		public string remarks { get; set; }
		public Reference reference { get; set; }
		public object holder { get; set; }
		public object hotel { get; set; }
	}

	public class Amount
	{
		public string currency { get; set; }
		public int value { get; set; }
		public bool binding { get; set; }
		public object commissionPercentage { get; set; }
		public object commissionAmount { get; set; }
	}

	public class Price
	{
		public object gross { get; set; }
		public object net { get; set; }
		public Amount amount { get; set; }
	}

	public class Reference
	{
		public string client { get; set; }
		public string supplier { get; set; }
	}

	public class Addons
	{
		public string distribute { get; set; }
	}
}
