﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.TravelGate.Entity;
using AlphaRooms.Accommodation.Provider.TravelGate.Helper;
using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutations;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.WebScraping;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<TravelGateAvailabilityResponse>
    {
        private readonly IProviderOutputLogger outputLogger;
        private readonly ITravelGateProviderAvailabilityRequestFactory TravelGateProviderAvailabilityRequestFactory;
        private readonly ILogger logger;
        private readonly ITravelGateParameterService parameterService;
        private JsonSerializerSettings settings;
        private readonly IAccommodationConfigurationManager configurationManager;

        public TravelGateAvailabilityAutomator(ITravelGateProviderAvailabilityRequestFactory TravelGateProviderAvailabilityRequestFactory,
            ITravelGateParameterService parameterService,
            IProviderOutputLogger outputLogger, ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.outputLogger = outputLogger;
            this.TravelGateProviderAvailabilityRequestFactory = TravelGateProviderAvailabilityRequestFactory;
            this.parameterService = parameterService;
            this.configurationManager = configurationManager;
            this.logger = logger;
        }
        public async Task<TravelGateAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            try
            {
                // Initialise provider parameter
                parameterService.InitializeParameters(request.Provider.Parameters, request.ChannelInfo.ChannelId);
            }
            catch (Exception ex)
            {
                logger.Error("B2C Availability initialize parameter failed error details- {1}", ex.GetMessageWithInnerExceptions());
            }

			// get request type, meta search or organic search
            RequestType requestType = request.MetaToken != null ? RequestType.Meta : RequestType.Organic;

			// get travelgate availability response
            TravelGateAvailabilityResponse availabilityResponse = await GetSupplierAvailabilityResponseAsync(request, requestType);
            return availabilityResponse;
        }

        private async Task<TravelGateAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, RequestType requestType)
        {
            WebScrapeResponse response = null;

			// get supplier request, organic search request or meta search request
            string supplierRequest = requestType == RequestType.Organic ? parameterService.OrganicRequest : parameterService.MetaRequest;

	        // create graphqlquery for travelgate api call
	        GraphQLQuery fullQuery = null;
            HotelSettingsInput HotelCriteriaSettings = parameterService.Deserialize<HotelSettingsInput>(parameterService.ClientSettings);

            if (requestType.Equals(RequestType.Meta))
            {
                HotelCriteriaSettings.plugins = null; 
                fullQuery = new GraphQLQuery()
                {
                    query = supplierRequest,
                    variables = new { token = request.MetaToken, settings = HotelCriteriaSettings },
                };
            }
            else
            {
				// prepare organic search request
                HotelCriteriaSearchInput hotelCriteriaSearchInput = new HotelCriteriaSearchInput();
                hotelCriteriaSearchInput = this.PrepareOrganicRequest(request);
                if (hotelCriteriaSearchInput.hotels != null && hotelCriteriaSearchInput.hotels.Any()) // Establishment Search
                {
                    HotelCriteriaSettings.plugins = null;
                }
                fullQuery = new GraphQLQuery()
                {
                    query = supplierRequest,
                    variables = new { criteriaSearch = hotelCriteriaSearchInput, settings = HotelCriteriaSettings },
                };
            }

			// serialize graphqlquery(string) to jsonContent
			string jsonContent = JsonConvert.SerializeObject(fullQuery, Newtonsoft.Json.Formatting.Indented);

            // logging the request graph QL and query variables for splunk
            string requestQuery = string.Format("TravelGate Availbility request: {0}{1}{2}", jsonContent, Environment.NewLine, request.AvailabilityId);

            if (this.configurationManager.EnableXTGLogging)
                logger.Error(requestQuery);

            // initialize webscrapeclient to call travelgate api
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = false, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationJson };
            string url = parameterService.ServiceUrl;
            

			// prepare webheader, content-type and authorization
            WebHeaderCollection header = new WebHeaderCollection();
            header.Add("Content-Type", "application/json");
            header.Add("Authorization", parameterService.ApiKey);

			// post graphqlquery to apitravelgate api, and get availability response
			try
			{
                response = await webScrapeClient.XTGPostAsync(url, jsonContent, header);
            }
            catch (Exception exception) // log exception while getting response from travelgate api
			{
                settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
                var exceptionMessage = JsonConvert.DeserializeObject<ExceptionHelper>(exception.Message, settings);

                string customErrorMessage = string.Format("Error occurred while getting token {0} details from XTG;{1} Request GraphQL - {2}{3} Response graphQL - {4}{5} Error Descriptop - {6}", request.MetaToken, Environment.NewLine, jsonContent, Environment.NewLine, exception.Message, Environment.NewLine, exceptionMessage.error.description);
                logger.Error("B2C Supplier Availability error:- {0}{1}", customErrorMessage, exception.StackTrace);
                throw new SupplierApiException(customErrorMessage, exception);
            }
            this.outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Json, response.Value);

            return new TravelGateAvailabilityResponse() { SupplierRequest = supplierRequest, SupplierResponse = response.Value };
        }

        private HotelCriteriaSearchInput PrepareOrganicRequest(AccommodationProviderAvailabilityRequest request)
        {
            var occupancies = new List<HotelCriteriaSearchOccupancies>();

			// get hotel criteria search occupancies
            occupancies.Add(new HotelCriteriaSearchOccupancies()
            {
                paxes = this.Getpaxes(request)
            });

            // check search mode Destination\Establishment, and prepare hotel criteria search input
            if (request.DestinationCodes.Any()) // Destination Search
            {
                return new HotelCriteriaSearchInput()
                {
                    checkIn = request.CheckInDate.ToString("yyyy-MM-dd"),
                    checkOut = request.CheckOutDate.ToString("yyyy-MM-dd"),
                    destinations = request.DestinationCodes,
                    occupancies = occupancies.ToArray(),
                    language = request.ChannelInfo.LanguageCode,
                    nationality = GetNationality(request.Channel), // Get Nationality code for channel
                    currency = request.ChannelInfo.CurrencyCode,
                    market = request.ChannelInfo.CountryCode
                };
            }
            else // Establishment Search
            {
                return new HotelCriteriaSearchInput()
                {
                    checkIn = request.CheckInDate.ToString("yyyy-MM-dd"),
                    checkOut = request.CheckOutDate.ToString("yyyy-MM-dd"),
                    hotels = request.EstablishmentCodes,
                    occupancies = occupancies.ToArray(),
                    language = request.ChannelInfo.LanguageCode,
                    nationality = GetNationality(request.Channel), // Get Nationality code for channel
                    currency = request.ChannelInfo.CurrencyCode,
                    market = request.ChannelInfo.CountryCode
                };
            }
        }

        private string GetNationality(SOACommon.DomainModels.Enumerators.Channel channel)
        {
            switch (channel)
            {
                case SOACommon.DomainModels.Enumerators.Channel.AlphaRoomsUK:
                    return "UK";
                case SOACommon.DomainModels.Enumerators.Channel.AlphaRoomsIE:
                    return "IE";
                case SOACommon.DomainModels.Enumerators.Channel.AlphaRoomsUS:
                    return "US";
                case SOACommon.DomainModels.Enumerators.Channel.TeletextIE:
                    return "IE";
                case SOACommon.DomainModels.Enumerators.Channel.TeletextHolidaysUK:
                    return "UK";
                default:
                    return "UK";
            }
        }



        private HotelCriteriaSearchPaxes[] Getpaxes(AccommodationProviderAvailabilityRequest request)
        {
            var paxes = new List<HotelCriteriaSearchPaxes>();
            foreach (var room in request.Rooms)
            {
                for (int i = 0; i < room.Guests.AdultsCount; ++i)
                {
                    paxes.Add(new HotelCriteriaSearchPaxes()
                    {
                        age = Convert.ToInt32(room.Guests[i].Age),
                    });
                }
                for (int i = 0; i < room.Guests.ChildrenAndInfantsCount; ++i)
                {
                    paxes.Add(new HotelCriteriaSearchPaxes()
                    {
                        age = Convert.ToInt32(room.Guests.ChildAndInfantAges[i]),
                    });
                }
            }
            return paxes.ToArray();
        }
}
}
