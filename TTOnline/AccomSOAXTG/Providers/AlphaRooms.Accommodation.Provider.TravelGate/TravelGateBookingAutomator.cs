﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.TravelGate.Helper;
using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutantions;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutations;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.WebScraping;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateBookingAutomator : IAccommodationBookingAutomatorAsync<TravelGateBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IProviderOutputLogger outputLogger;
        private readonly ITravelGateParameterService parameterService;
        private readonly IProviderLoggerService loggerService;
        private readonly ITravelGateProviderBookingRequestFactory travelGateProviderBookingRequestFactory;
        private JsonSerializerSettings settings;

        public TravelGateBookingAutomator(ILogger logger, ITravelGateProviderBookingRequestFactory travelGateProviderBookingRequestFactory
               , IProviderLoggerService loggerService, IProviderOutputLogger outputLogger, ITravelGateParameterService parameterService)
        {
            this.logger = logger;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
            this.parameterService = parameterService;
            this.travelGateProviderBookingRequestFactory = travelGateProviderBookingRequestFactory;
        }

        public async Task<TravelGateBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            try
            {
                // Initialise provider parameter
                parameterService.InitializeParameters(request.Provider.Parameters, request.ChannelInfo.ChannelId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetMessageWithInnerExceptions());
            }

            TravelGateBookingResponse response = await GetSupplierBookingResponseAsync(request);
            return response;
        }

        private async Task<TravelGateBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.FirstOrDefault()?.ValuationResult;

            if (valuationResult == null) return null;

            var supplierRequestQuery = parameterService.BookingRequest;

            WebScrapeResponse response = null;
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = false, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationJson };

            var rooms = new List<HotelBookInputRooms>();
            rooms.Add(new HotelBookInputRooms()
            {
                occupancyRefId = valuationResult.RoomNumber,
                paxes = this.Getpaxes(request)
            });

            HotelBookInput boookingInput = new HotelBookInput()
            {
                optionRefId = valuationResult.ProviderSpecificData["optionRefId"],
                clientReference = request.ItineraryReference,
                deltaPrice = new HotelBookInputDeltaPrice()
                {
                    amount = 10,
                    percent = 10,
                    applyBoth = true
                },
                holder = new HotelBookInputHolder()
                {
                    name = request.Customer.FirstName,
                    surname = request.Customer.Surname
                },
                rooms = rooms.ToArray()
            };

            var fullQuery = new GraphQLQuery()
            {
                query = supplierRequestQuery,
                variables = new { BookInput = boookingInput, settings = parameterService.Deserialize<HotelSettingsInput>(parameterService.ClientSettings) },
            };
            string supplierRequest = JsonConvert.SerializeObject(fullQuery);

            string url = parameterService.ServiceUrl;

            WebHeaderCollection header = new WebHeaderCollection();
            header.Add("Content-Type", "application/json");
            header.Add("Authorization", parameterService.ApiKey);
            this.loggerService.SetProviderRequest(request, supplierRequest);

            try
            {
                response = await webScrapeClient.XTGPostAsync(url, supplierRequest, header);
            }
            catch (Exception exception)
            {
                settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
                var exceptionMessage = JsonConvert.DeserializeObject<ExceptionHelper>(exception.Message, settings);

                string customError = string.Format("Error occured while getting optionRefId -[{0}]  - details from XTG;{1} Request GraphQL - {2}{3} Response graphQL - {4}{5} Error Description - {6} ", valuationResult.ProviderSpecificData["optionRefId"], Environment.NewLine, supplierRequest, Environment.NewLine, exception.Message, Environment.NewLine, exceptionMessage.ToString());
                logger.Error("B2C Supplier Booking error:- {0}{1}", customError, exception.StackTrace);
                throw new SupplierApiException(customError, exception);
            }

            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Json, response.Value);
            this.loggerService.SetProviderResponse(request, response.Value);

            return new TravelGateBookingResponse()
            {
                supplierBookingRequest = supplierRequest,
                supplierBookingResponse = response.Value
            };
        }
        private HotelBookInputPaxes[] Getpaxes(AccommodationProviderBookingRequest request)
        {
            var paxes = new List<HotelBookInputPaxes>();
            foreach (var room in request.ValuatedRooms)
            {
                for (int i = 0; i < room.Guests.AdultsCount; ++i)
                {
                    paxes.Add(new HotelBookInputPaxes()
                    {
                        age = Convert.ToInt32(room.Guests[i].Age),
                        name = room.Guests[0].FirstName,
                        surname = room.Guests[0].Surname,
                    });
                }
                for (int i = 0; i < room.Guests.ChildrenAndInfantsCount; ++i)
                {
                    paxes.Add(new HotelBookInputPaxes()
                    {
                        age = Convert.ToInt32(room.Guests.ChildAndInfantAges[i]),
                        name = room.Guests[0].FirstName,
                        surname = room.Guests[0].Surname,
                    });
                }
            }
            return paxes.ToArray();
        }
    }
}
