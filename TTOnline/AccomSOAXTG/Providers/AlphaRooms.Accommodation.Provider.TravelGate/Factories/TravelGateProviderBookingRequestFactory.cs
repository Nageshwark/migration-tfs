﻿using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Factories
{
    public class TravelGateProviderBookingRequestFactory : ITravelGateProviderBookingRequestFactory
    {
        private readonly ITravelGateParameterService parameterService;
        private readonly ILogger logger;
        public TravelGateProviderBookingRequestFactory(ITravelGateParameterService parameterService, ILogger logger)
        {
            this.parameterService = parameterService;
            this.logger = logger;
        }
        public string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            string jsonContent = string.Empty;
            try
            {
                //initialize parameter service with parameters
                parameterService.InitializeParameters(request.Provider.Parameters, request.ChannelInfo.ChannelId);


                //string supplierValuationRequest = parameterService.MetaValuationRequest;

                // var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

                //var fullQuery = new GraphQLQuery()
                // {
                //      query = supplierValuationRequest,
                //      variables = new { optionRefId = availabilityResult.ProviderSpecificData["id"] },
                //  };

                //  jsonContent = JsonConvert.SerializeObject(fullQuery);

            }
            catch (Exception ex)
            {
                logger.Error(ex.GetMessageWithInnerExceptions());
            }
            return jsonContent;
        }
    }
}
