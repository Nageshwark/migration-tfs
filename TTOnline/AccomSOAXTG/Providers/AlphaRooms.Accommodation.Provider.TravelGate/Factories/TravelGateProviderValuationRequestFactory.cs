﻿using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using Newtonsoft.Json;
using AlphaRooms.WebScraping;
using System.Net;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Provider.TravelGate.Mutant;

namespace AlphaRooms.Accommodation.Provider.TravelGate.Factories
{
    public class TravelGateProviderValuationRequestFactory : ITravelGateProviderValuationRequestFactory
    {
        private readonly ITravelGateParameterService parameterService;
        private readonly ILogger logger;
        public TravelGateProviderValuationRequestFactory(ITravelGateParameterService parameterService, ILogger logger)
        {
            this.parameterService = parameterService;
            this.logger = logger;
        }
        public string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            string jsonContent = string.Empty;
            try
            {
                //initialize parameter service with parameters
                parameterService.InitializeParameters(request.Provider.Parameters, request.ChannelInfo.ChannelId);

                string supplierValuationRequest = parameterService.ValuationRequest;

                var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

                var fullQuery = new GraphQLQuery()
                {
                    query = supplierValuationRequest,
                    variables = new HotelCriteriaQuoteInput{ optionRefId = availabilityResult.ProviderSpecificData["id"] },
                };

                jsonContent = JsonConvert.SerializeObject(fullQuery);

            }
            catch(Exception ex)
            {
                logger.Error(ex.GetMessageWithInnerExceptions());
            }
            return jsonContent;
        }
    }
}
