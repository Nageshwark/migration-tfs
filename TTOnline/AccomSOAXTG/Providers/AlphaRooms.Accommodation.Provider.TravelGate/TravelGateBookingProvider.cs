﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<TravelGateBookingResponse> automator;
        private readonly IAccommodationBookingParser<TravelGateBookingResponse> parser;

        public TravelGateBookingProvider(IAccommodationBookingAutomatorAsync<TravelGateBookingResponse> automator,
                                        IAccommodationBookingParser<TravelGateBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
