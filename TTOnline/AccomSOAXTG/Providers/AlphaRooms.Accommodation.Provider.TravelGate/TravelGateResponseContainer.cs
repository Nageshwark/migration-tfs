﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.TravelGate
{
    public class TravelGateResponseContainer
    {
        public TravelGateResponseData data { get; set; }
    }
    public class TravelGateResponseData
    {
        public TravelGateResponseSearch search { get; set; }
    }
    public class TravelGateResponseSearch
    {
        public TravelGateResponsehotel hotel { get; set; }
    }

    public class TravelGateResponsehotel
    {
        public string context { get; set; }
        public TravelGateResponseAuditData auditData { get; set; }
        public TravelGateResponseOptions[] options { get; set; }
        public TravelGateResponseErrors[] errors { get; set; }
        public TravelGateResponsewarnings[] warnings { get; set; }
        public TravelGateResponsecriteria requestCriteria { get; set; }
    }

    public class TravelGateResponseAuditData
    {
        public string[] transactions { get; set; }
        public string timeStamp { get; set; }
        public string processTime { get; set; }
    }

    public class TravelGateResponsecriteria
    {
        public string checkIn { get; set; }
        public string checkOut { get; set; }
        public string[] hotels { get; set; }
        public TravelGateResponseOccupancies[] occupancies { get; set; }
    }

    public class TravelGateResponsewarnings
    {
        public string code { get; set; }
        public string description { get; set; }
        public string type { get; set; }
    }

    public class TravelGateResponseErrors
    {
        public string code { get; set; }
        public string description { get; set; }
        public string type { get; set; }
    }

    public class TravelGateResponseOptions
    {
        public string supplierCode { get; set; }
        public string accessCode { get; set; }
        public string[] markets { get; set; }
        public string hotelCode { get; set; }
        public string hotelName { get; set; }
        public string boardCode { get; set; }
        public string id { get; set; }
        public string paymentType { get; set; }
        public TravelGateResponseAddOns addOns { get; set; }
        public string status { get; set; }
        public TravelGateResponseOccupancies[] occupancies { get; set; }
        public TravelGateResponseRooms[] rooms { get; set; }
        public TravelGateResponsePrice price { get; set; }
        public string[] rateRules { get; set; }
        public TravelGateResponseSupplements[] supplements { get; set; }
        public TravelGateResponseCancelPolicy cancelPolicy { get; set; }
        public TravelGateResponseSupplements[] surcharges { get; set; }
        public string token { get; set; }
    }

    public class TravelGateResponseAddOns
    {
        public string distribute { get; set; }
    }

    public class TravelGateResponseCancelPolicy
    {
        public bool refundable { get; set; }
        public TravelGateResponseCancelPenalties[] cancelPenalties { get; set; }
    }

    public class TravelGateResponseCancelPenalties
    {
        public string appliedOver { get; set; }
        public string currency { get; set; }
        public string hoursBefore { get; set; }
        public string penaltyType { get; set; }
        public string value { get; set; }
    }

    public class TravelGateResponseSupplements
    {
        public string chargeType { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string durationType { get; set; }
        public string effectiveDate { get; set; }
        public string expireDate { get; set; }
        public bool mandatory { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }
        public string supplementType { get; set; }
        public string unit { get; set; }
        public TravelGateResponseResort resort { get; set; }
    }

    public class TravelGateResponseResort
    {
        public string code { get; set; }
        public string description { get; set; }
        public string name { get; set; }
    }

    public class TravelGateResponsePrice
    {
        public string currency { get; set; }
        public decimal net { get; set; }
        public decimal gross { get; set; }
        public bool binding { get; set; }
        public decimal commissionPercentage { get; set; }
    }

    public class TravelGateResponseRooms
    {
        public string code { get; set; }
        public string occupancyRefId { get; set; }
        public string description { get; set; }
        public string refundable { get; set; }
        public int units { get; set; }
        public TravelGateResponseRoomsPrice roomPrice { get; set; }
        public TravelGateResponseRoomsbeds[] beds { get; set; }
        public TravelGateResponseRoomsRatePlans[] ratePlans { get; set; }
        public TravelGateResponsePromotions[] promotions { get; set; }
    }

    public class TravelGateResponsePromotions
    {
        public string code { get; set; }
        public string effectiveDate { get; set; }
        public string expireDate { get; set; }
        public string name { get; set; }
    }

    public class TravelGateResponseRoomsRatePlans
    {
        public string code { get; set; }
        public string effectiveDate { get; set; }
        public string expireDate { get; set; }
        public string name { get; set; }
    }

    public class TravelGateResponseRoomsbeds
    {
        public string type { get; set; }
        public string description { get; set; }
        public bool shared { get; set; }
        public int count { get; set; }
    }

    public class TravelGateResponseRoomsPrice
    {
        public TravelGateResponsePrice price { get; set; }
        // public TravelGateResponseBeds beds { get; set; }
    }


    public class TravelGateResponseBreakdown
    {
        public string effectiveDate { get; set; }
        public string expireDate { get; set; }
        public TravelGateResponsePrice price { get; set; }
    }

    public class TravelGateResponseOccupancies
    {
        public byte id { get; set; }
        public TravelGateResponseOccupanciesPaxes[] paxes { get; set; }
    }

    public class TravelGateResponseOccupanciesPaxes
    {
        public byte age { get; set; }
    }
}
