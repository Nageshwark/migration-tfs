﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Provider.CarlsonRezidor
{
    public class CarlsonRezidorAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        public CarlsonRezidorAvailabilityProvider(ILogger logger, IConnectivityLogService connectivityLogService,
            IAccommodationConfigurationManager configurationManager)
            : base(logger, connectivityLogService, configurationManager)
        {
        }

        public override Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetSupplierAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
