﻿using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsAccommodationRoom
    {
        public string BoardDescription { get; set; }
        public string RoomDescription { get; set; }
        public string SpecialRequest { get; set; }
        public string SupplierBookingReference { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }

        public int NumberOfAdults { get; set; }

        public Money CostFromSupplier { get; set; }
        public Money CostToCustomer { get; set; }

        public PaymentModel PaymentMethod { get; set; }
        public IEnumerable<AccommodationProviderValuationResultVccPaymentRule> VccPaymentTerms { get; set; }

        public byte[] ChildrenAges { get; set; }
    }
}
