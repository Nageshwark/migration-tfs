﻿using System;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsValuationResponse
    {
        public byte RoomNumber { get; set; }
        public string ProviderEdiCode { get; set; }
        public string EstablishmentEdiCode { get; set; }
        public string EstablishmentName { get; set; }
        public byte Adults { get; set; }
        public byte Children { get; set; }
        public byte Infants { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string BoardCode { get; set; }
        public string BoardDescription { get; set; }
        public string RoomCode { get; set; }
        public string RoomDescription { get; set; }
        public Money SalePrice { get; set; }
        public Money CostPrice { get; set; }
        public PaymentModel PaymentModel { get; set; }
        public bool IsBindingRate { get; set; }
        public bool IsOpaqueRate { get; set; }
        public bool? IsNonRefundable { get; set; }
        public RateType RateType { get; set; }
        public string CancellationPolicy { get; set; }
        public bool IsValidRoom { get; set; }

    }
}
