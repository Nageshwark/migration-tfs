﻿using System;
using System.Collections.Generic;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsAccommodation
    {
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public Guid EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }

        public ICollection<AriaContractsAccommodationRoom> AccommodationRooms { get; set; }
    }
}
