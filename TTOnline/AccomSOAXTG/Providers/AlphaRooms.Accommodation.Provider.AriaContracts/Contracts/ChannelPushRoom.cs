﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class ChannelPushRoom
    {
        public string code { get; set; }
        public string channelPushDataXML { get; set; }
    }
}
