﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Availability;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsAvailabilityResponse : AvailabilityResult
    {
        //public Guid fkRoomId { get; set; }
        //public string strSupplierName { get; set; }
        //public string strEstablishmentName { get; set; }
        //public string strCategoryName { get; set; }
        //public string strDestinationName { get; set; }
        //public string strRoomTypeName { get; set; }
        //public int blnIsBindingRate { get; set; }
        //public int fkPaymentModel { get; set; }
        //public decimal Margin { get; set; }
        //public int intMaxAgeChild { get; set; }
        //public int intMaxAgeInfant { get; set; }
        //public string strZoneName { get; set; }
        //public string strISOCurrencyCode { get; set; }
        //public string strEdiCode { get; set; }
        //public string strRoomTypeInternalName { get; set; }
        //public decimal? dclTotalCostRO { get; set; }
        //public decimal? dclTotalCostSC { get; set; }
        //public decimal? dclTotalCostBB { get; set; }
        //public decimal? dclTotalCostHB { get; set; }
        //public decimal? dclTotalCostFB { get; set; }
        //public decimal? dclTotalCostAI { get; set; }
        //public decimal? dclTotalSaleRO { get; set; }
        //public decimal? dclTotalSaleSC { get; set; }
        //public decimal? dclTotalSaleBB { get; set; }
        //public decimal? dclTotalSaleHB { get; set; }
        //public decimal? dclTotalSaleFB { get; set; }
        //public decimal? dclTotalSaleAI { get; set; }
        //public DateTime dtmEarliestStart { get; set; }
        //public DateTime dtmLatestEnd { get; set; }
        //public int intQuantityAvailable { get; set; }
        //public int intQuantityBooked { get; set; }
        //public string CancellationTerms { get; set; }
        //public int RoomId { get; set; }
        public bool IsNetted { get; set; }
        
    }
    
}
