﻿using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsBookingResponse
    {
        public string ProviderEdiCode { get; set; }
        public string BookingReference { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public bool SwitchToDirect { get; set; }
        public byte RoomNumber { get; set; }
    }
}