﻿using System;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class AriaContractsProviderEmail
    {
        //public Site Site { get; set; } TODO: Added from Bev5 Core. Check this.
        public int InvoiceId { get; set; }
        public string ItineraryReference { get; set; }
        public DateTime BookingDate { get; set; }
        public string LeadPassenger { get; set; }

        public AriaContractsAccommodation Accommodation { get; set; }

        public string Site { get; set; }

        // Payment Info is required if one of room is directpay or payonbooking
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CVV { get; set; }


        public bool HasPaymentInformation
        {
            get
            {
                return !(string.IsNullOrWhiteSpace(CardHolderName)
                    || string.IsNullOrWhiteSpace(CardNumber)
                    || string.IsNullOrWhiteSpace(CVV)
                    || ExpiryDate.HasValue == false);
            }
        }
    }
}
