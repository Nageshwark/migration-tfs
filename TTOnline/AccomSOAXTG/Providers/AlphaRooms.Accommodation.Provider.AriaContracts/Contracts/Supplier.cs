﻿using System;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class Supplier
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string LegalName { get; set; }

        public string ReservationsEmail { get; set; }

        public string FaxNumbers { get; set; }

        public string EdiProviderCode { get; set; }

        public AlphaRole AlphaRole { get; set; }

        public string EmergencyTelephone { get; set; }

        public string TransferSupplierName { get; set; }
    }
}
