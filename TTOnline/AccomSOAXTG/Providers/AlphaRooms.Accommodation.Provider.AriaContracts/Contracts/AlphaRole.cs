﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public enum AlphaRole
    {
        Principal = 1,
        Agent = 2,
        SubAgent = 3
    }
}
