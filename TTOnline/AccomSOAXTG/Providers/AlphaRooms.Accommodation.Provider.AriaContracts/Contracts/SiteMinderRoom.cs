﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Contracts
{
    public class SiteMinderRoom
    {
        public string hotelcode { get; set; }
        public string roomcode { get; set; }
        public string ratecode { get; set; }
        public string roomdescription { get; set; }
        public string ratedescription { get; set; }
    }
}
