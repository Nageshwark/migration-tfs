﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class SiteMinderService : ISiteMinderService
    {
        private const string SiteMinderEMEAUser = "SiteMinderEMEAUser";
        private const string SiteMinderEMEAPassword = "SiteMinderEMEAPassword";
        private const string SiteMinderAPACUser = "SiteMinderAPACUser";
        private const string SiteMinderAPACPassword = "SiteMinderAPACPassword";
        private const string SMCompanyNameTypeCode = "SMCompanyNameTypeCode";
        private const string SMCompanyNameTypeValue = "SMCompanyNameTypeValue";

        private readonly IProviderLoggerService loggerService;

        public SiteMinderService(IProviderLoggerService loggerService)
        {
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<AriaContractsBookingResponse>> ConfirmSiteMinderBooking(AccommodationProviderBookingRequest request, Dictionary<string, string> channelPushInfo
            , IAriaContractsRepository ariaContractsRepository)
        {

            var reference = "";
            var status = BookingStatus.NotAttempted;
                         
            if (channelPushInfo.ContainsValue("SM") || channelPushInfo.ContainsValue("SME"))
            {
                com.siteminder.smtpi.OTA_HotelResNotifRQ siteMinderObject = null;

                var channel = String.Empty;

                Parallel.ForEach(request.ValuatedRooms, room =>
                {
                    channel = channelPushInfo[room.ValuationResult.RoomCode];

                    var siteMinderRoom = ariaContractsRepository.GetSiteMinderRoom(room.ValuationResult.RoomCode).Result;

                    if (siteMinderRoom == null)
                    {
                        throw new Exception(
                            "SiteMinder EMEA Error: Room Id: " + room.ValuationResult.RoomCode  + " ;marked as SiteMinder does not exist in database");
                    }

                    CreateSiteMinderEMEAObject(room, siteMinderRoom, ref siteMinderObject, request);

                });

                if (!string.IsNullOrEmpty(channel) && (channel.Equals("SM") || channel.Equals("SME")))
                {
                    var xmlDoc = "<UsernameToken><Username>" + request.Provider.Parameters.GetParameterValue(SiteMinderEMEAUser) + "</Username><Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + request.Provider.Parameters.GetParameterValue(SiteMinderEMEAPassword) + "</Password></UsernameToken>";
                    var doc = new XmlDocument();
                    doc.LoadXml(xmlDoc); 
                    XmlElement token = doc.DocumentElement;
                    var tokens = new List<XmlElement>();
                    tokens.Add(token);

                    var service = new com.siteminder.smtpi.SiteConnectService
                    {
                        Security = new com.siteminder.smtpi.SecurityHeaderType
                        {
                            MustUnderstand = true,
                            Any = tokens.ToArray()
                        }
                    };

                    this.loggerService.SetProviderRequest(request, "SM", siteMinderObject);
                    com.siteminder.smtpi.OTA_HotelResNotifRS response = service.HotelResNotifRQ(siteMinderObject);
                    this.loggerService.SetProviderResponse(request, "SM", response);

                    if (response != null && response.Items.Any(x => x is com.siteminder.smtpi.SuccessType))
                    {
                        com.siteminder.smtpi.HotelReservationsType reservationObject = (com.siteminder.smtpi.HotelReservationsType) response.Items.First(x => x.GetType() == typeof (com.siteminder.smtpi.HotelReservationsType));
                        reference = reservationObject.HotelReservation[0].ResGlobalInfo.HotelReservationIDs[0].ResID_Value;
                        status = BookingStatus.Confirmed;
                    }

                    //TODO: Log to database. In case of exception, set the status as false.
                }
                
            }

            else if (channelPushInfo.ContainsValue("APAC") || channelPushInfo.ContainsValue("APACE"))
            {
                com.siteminder.apac.smtpi.OTA_HotelResNotifRQ siteMinderObject = null;

                var channel = String.Empty;

                Parallel.ForEach(request.ValuatedRooms, room =>
                {
                    channel = channelPushInfo[room.ValuationResult.RoomCode];

                    var siteMinderRoom = ariaContractsRepository.GetSiteMinderRoom(room.ValuationResult.RoomCode).Result;

                    if (siteMinderRoom == null)
                    {
                        throw new Exception("SiteMinder APAC Error: Room Id: " + room.ValuationResult.RoomCode + " ;marked as SiteMinder does not exist in database");
                    }

                    CreateSiteMinderAPACObject(room, siteMinderRoom, ref siteMinderObject, request);

                });

                if (!string.IsNullOrEmpty(channel) && (channel.Equals("APAC") || channel.Equals("APACE")))
                {
                    var xmlDoc = "<UsernameToken><Username>" + request.Provider.Parameters.GetParameterValue(SiteMinderAPACUser) + "</Username><Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + request.Provider.Parameters.GetParameterValue(SiteMinderAPACPassword) + "</Password></UsernameToken>";
                    var doc = new XmlDocument();
                    doc.LoadXml(xmlDoc);
                    XmlElement token = doc.DocumentElement;
                    var tokens = new List<XmlElement>();
                    tokens.Add(token);

                    var service = new com.siteminder.apac.smtpi.SiteConnectService
                    {
                        Security = new com.siteminder.apac.smtpi.SecurityHeaderType
                        {
                            MustUnderstand = true,
                            Any = tokens.ToArray()
                        }
                    };

                    this.loggerService.SetProviderRequest(request, "SM", siteMinderObject);
                    com.siteminder.apac.smtpi.OTA_HotelResNotifRS response = service.HotelResNotifRQ(siteMinderObject);
                    this.loggerService.SetProviderResponse(request, "SM", response);

                    if (response != null && response.Items.Any(x => x is com.siteminder.apac.smtpi.SuccessType))
                    {
                        com.siteminder.apac.smtpi.HotelReservationsType reservationObject = (com.siteminder.apac.smtpi.HotelReservationsType)response.Items.First(x => x.GetType() == typeof(com.siteminder.apac.smtpi.HotelReservationsType));
                        reference = reservationObject.HotelReservation[0].ResGlobalInfo.HotelReservationIDs[0].ResID_Value;
                        status = BookingStatus.Confirmed;
                    }

                    //TODO: Log to database. In case of exception, set the status as false.
                }

            }

            //Set the booking result
            var bookingResponseList = new List<AriaContractsBookingResponse>();
            var bookingResponse = new AriaContractsBookingResponse();
            bookingResponse.BookingReference = reference;
            bookingResponse.BookingStatus = status;
            bookingResponse.ProviderEdiCode = request.Provider.EdiCode;
            // bookingResponse.SwitchToDirect = request. //TODO: field needs to be added to the request
            bookingResponseList.Add(bookingResponse);
            return bookingResponseList;
        }
        
        //SiteMinder EMEA
        private void CreateSiteMinderEMEAObject(AccommodationProviderBookingRequestRoom room, SiteMinderRoom siteMinderRoom, ref com.siteminder.smtpi.OTA_HotelResNotifRQ siteMinderObject,AccommodationProviderBookingRequest bookingReq)
        {
            if (siteMinderObject == null)
            {
                siteMinderObject = new com.siteminder.smtpi.OTA_HotelResNotifRQ();

                //RoomRate
                com.siteminder.smtpi.RoomStayTypeRoomRatesRoomRate[] roomRate =
                {
                    new com.siteminder.smtpi.RoomStayTypeRoomRatesRoomRate
                    {
                        RoomTypeCode = siteMinderRoom.roomcode,
                        NumberOfUnits = "1",
                        RatePlanCode = siteMinderRoom.ratecode
                    }
                };

                //GuestCounts
                var guestCount = new List<com.siteminder.smtpi.GuestCountTypeGuestCount>();

                var totalChildren = room.ValuationResult.Children + room.ValuationResult.Infants;

                if (room.ValuationResult.Children > 0 || room.ValuationResult.Infants > 0)
                {
                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = totalChildren.ToString(),
                        AgeQualifyingCode = "8"
                    });
                }
                else
                {
                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                }

                //RatePlans
                var rateplanTypes = new List<com.siteminder.smtpi.RatePlanType>();
                var itemsElementName = new List<com.siteminder.smtpi.ItemsChoiceType>();
                itemsElementName.Add(com.siteminder.smtpi.ItemsChoiceType.Text);
                var rateFormattedText = new com.siteminder.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.ratedescription
                };
                var rateItems = new List<com.siteminder.smtpi.FormattedTextTextType>();
                rateItems.Add(rateFormattedText);

                rateplanTypes.Add(new com.siteminder.smtpi.RatePlanType
                {
                    RatePlanCode = siteMinderRoom.ratecode,
                    RatePlanDescription = new com.siteminder.smtpi.ParagraphType
                    {
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = rateItems.ToArray()
                    }
                });

                //RoomTypes
                var roomTypes = new List<com.siteminder.smtpi.RoomTypeType>();
                var typeFormattedText = new com.siteminder.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.roomdescription
                };
                var typeItems = new List<com.siteminder.smtpi.FormattedTextTextType>();
                typeItems.Add(typeFormattedText);

                roomTypes.Add(new com.siteminder.smtpi.RoomTypeType
                {
                    RoomTypeCode = siteMinderRoom.roomcode,
                    RoomDescription = new com.siteminder.smtpi.ParagraphType
                    {
                        Name = siteMinderRoom.roomdescription,
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = typeItems.ToArray()
                    }
                });

                //RoomStays
                com.siteminder.smtpi.RoomStaysTypeRoomStay[] roomStays =
                {
                    new com.siteminder.smtpi.RoomStaysTypeRoomStay
                    {
                        GuestCounts =
                            new com.siteminder.smtpi.GuestCountType
                            {
                                IsPerRoom = false,
                                IsPerRoomSpecified = true,
                                GuestCount = guestCount.ToArray()
                            },

                        TimeSpan =
                            new com.siteminder.smtpi.DateTimeSpanType
                            {
                                Start = room.ValuationResult.CheckInDate.ToString("yyyy-MM-dd"),
                                End = room.ValuationResult.CheckOutDate.ToString("yyyy-MM-dd")
                            },

                        RoomRates = new com.siteminder.smtpi.RoomStayTypeRoomRates {RoomRate = roomRate},

                        Total = new com.siteminder.smtpi.TotalType
                        {
                            AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                            AmountAfterTaxSpecified = true,
                            AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                            AmountBeforeTaxSpecified = true,
                            CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode
                        },

                        BasicPropertyInfo =
                            new com.siteminder.smtpi.BasicPropertyInfoType {HotelCode = siteMinderRoom.hotelcode},

                        RatePlans = rateplanTypes.ToArray(),

                        RoomTypes = roomTypes.ToArray(),
                    }
                };

                //ProfileType
                var personNames = new List<com.siteminder.smtpi.PersonNameType>();
                string[] givenNames = {bookingReq.Customer.FirstName};
                personNames.Add(new com.siteminder.smtpi.PersonNameType
                {
                    GivenName = givenNames,
                    Surname = bookingReq.Customer.Surname
                });
                var telephone = new List<com.siteminder.smtpi.CustomerTypeTelephone>();
                telephone.Add(new com.siteminder.smtpi.CustomerTypeTelephone
                {PhoneNumber = bookingReq.Customer.ContactNumber});

                var profile = new com.siteminder.smtpi.ProfileType
                {
                    ProfileType1 = "1",
                    Customer = new com.siteminder.smtpi.CustomerType
                    {
                        PersonName = personNames.ToArray(),
                        Telephone = telephone.ToArray()
                    }
                };

                //Profile
                var profiles = new List<com.siteminder.smtpi.ProfilesTypeProfileInfo>();
                profiles.Add(new com.siteminder.smtpi.ProfilesTypeProfileInfo {Profile = profile});

                com.siteminder.smtpi.ResGuestType[] resGuests =
                {
                    new com.siteminder.smtpi.ResGuestType {Profiles = profiles.ToArray()}
                };

                var resGlobalInfo = new com.siteminder.smtpi.ResGlobalInfoType
                {
                    Total = new com.siteminder.smtpi.TotalType
                    {
                        CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode,
                        AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                        AmountAfterTaxSpecified = true,
                        AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                        AmountBeforeTaxSpecified = true
                    },

                    Profiles = profiles.ToArray()
                };

                //HotelReservationType
                var hotelReservations = new List<com.siteminder.smtpi.HotelReservationType>();
                var uniqueId = new List<com.siteminder.smtpi.UniqueID_Type>();
                uniqueId.Add(new com.siteminder.smtpi.UniqueID_Type
                {
                    ID = bookingReq.ItineraryId.ToString(),
                    Type = "14"
                });
                hotelReservations.Add(new com.siteminder.smtpi.HotelReservationType
                {
                    CreateDateTime = Convert.ToDateTime((DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:sszz"))),
                    CreateDateTimeSpecified = true,
                    UniqueID = uniqueId.ToArray(),
                    RoomStays = roomStays,
                    ResGuests = resGuests,
                    ResGlobalInfo = resGlobalInfo
                });

                //Set the values to siteMinder object
                siteMinderObject.Version = (decimal) 1.0;
                siteMinderObject.EchoToken = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:sszzz");
                siteMinderObject.ResStatus = com.siteminder.smtpi.TransactionActionType.Commit;
                siteMinderObject.ResStatusSpecified = true;

                com.siteminder.smtpi.SourceType[] sourceType =
                {
                    new com.siteminder.smtpi.SourceType
                    {
                        RequestorID = new com.siteminder.smtpi.SourceTypeRequestorID() {Type = "22", ID = "ARC"},
                        BookingChannel = new com.siteminder.smtpi.SourceTypeBookingChannel()
                        {
                            Primary = true,
                            Type = "1",
                            PrimarySpecified = true,
                            CompanyName =
                                new com.siteminder.smtpi.CompanyNameType()
                                {
                                    Code = bookingReq.Provider.Parameters.GetParameterValue(SMCompanyNameTypeCode),
                                    Value = bookingReq.Provider.Parameters.GetParameterValue(SMCompanyNameTypeValue)
                                }
                        }
                    }
                };

                siteMinderObject.POS = sourceType;
                siteMinderObject.HotelReservations = new com.siteminder.smtpi.HotelReservationsType
                {
                    HotelReservation = hotelReservations.ToArray()
                };
            }

            else
            {
                //Update room information

                //RoomRate
                com.siteminder.smtpi.RoomStayTypeRoomRatesRoomRate[] roomRate =
                {
                    new com.siteminder.smtpi.RoomStayTypeRoomRatesRoomRate
                    {
                        RoomTypeCode = siteMinderRoom.roomcode,
                        NumberOfUnits = "1",
                        RatePlanCode = siteMinderRoom.ratecode
                    }
                };

                //GuestCounts
                var guestCount = new List<com.siteminder.smtpi.GuestCountTypeGuestCount>();
                var totalChildren = room.ValuationResult.Children + room.ValuationResult.Infants;

                if (room.ValuationResult.Children > 0 || room.ValuationResult.Infants > 0)
                {
                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = totalChildren.ToString(),
                        AgeQualifyingCode = "8"
                    });

                }
                else
                {
                    guestCount.Add(new com.siteminder.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                }

                var roomstaysArray = siteMinderObject.HotelReservations.HotelReservation[0].RoomStays;
                Array.Resize(ref roomstaysArray, siteMinderObject.HotelReservations.HotelReservation[0].RoomStays.Length + 1);
                siteMinderObject.HotelReservations.HotelReservation[0].RoomStays = roomstaysArray;

                //RatePlans
                var rateplanTypes = new List<com.siteminder.smtpi.RatePlanType>();
                var itemsElementName = new List<com.siteminder.smtpi.ItemsChoiceType>();
                itemsElementName.Add(com.siteminder.smtpi.ItemsChoiceType.Text);
                var rateFormattedText = new com.siteminder.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.ratedescription
                };
                var rateItems = new List<com.siteminder.smtpi.FormattedTextTextType>();
                rateItems.Add(rateFormattedText);

                rateplanTypes.Add(new com.siteminder.smtpi.RatePlanType
                {
                    RatePlanCode = siteMinderRoom.ratecode,
                    RatePlanDescription = new com.siteminder.smtpi.ParagraphType
                    {
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = rateItems.ToArray()
                    }
                });

                //RoomTypes
                var roomTypes = new List<com.siteminder.smtpi.RoomTypeType>();
                var typeFormattedText = new com.siteminder.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.roomdescription
                };
                var typeItems = new List<com.siteminder.smtpi.FormattedTextTextType>();
                typeItems.Add(typeFormattedText);

                roomTypes.Add(new com.siteminder.smtpi.RoomTypeType
                {
                    RoomTypeCode = siteMinderRoom.roomcode,
                    RoomDescription = new com.siteminder.smtpi.ParagraphType
                    {
                        Name = siteMinderRoom.roomdescription,
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = typeItems.ToArray()
                    }
                });

                siteMinderObject.HotelReservations.HotelReservation[0].RoomStays[siteMinderObject.HotelReservations.HotelReservation[0].RoomStays.Length - 1]
                    
                    = new com.siteminder.smtpi.RoomStaysTypeRoomStay
                        {
                            GuestCounts = new com.siteminder.smtpi.GuestCountType
                                            {
                                                IsPerRoom = false,
                                                IsPerRoomSpecified = true,
                                                GuestCount = guestCount.ToArray()
                                            },

                            TimeSpan = new com.siteminder.smtpi.DateTimeSpanType
                                        {
                                            Start = room.ValuationResult.CheckInDate.ToString("yyyy-MM-dd"),
                                            End = room.ValuationResult.CheckOutDate.ToString("yyyy-MM-dd")
                                        },

                            RoomRates = new com.siteminder.smtpi.RoomStayTypeRoomRates {RoomRate = roomRate},

                            Total = new com.siteminder.smtpi.TotalType
                            {
                                AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                                AmountAfterTaxSpecified = true,
                                AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                                AmountBeforeTaxSpecified = true,
                                CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode
                            },

                            BasicPropertyInfo = new com.siteminder.smtpi.BasicPropertyInfoType {HotelCode = siteMinderRoom.hotelcode},

                            RatePlans = rateplanTypes.ToArray(),

                            RoomTypes = roomTypes.ToArray(),
                        };

                //Update Total price
                siteMinderObject.HotelReservations.HotelReservation[0].ResGlobalInfo.Total.AmountAfterTax +=  room.ValuationResult.CostPrice.Amount;
                siteMinderObject.HotelReservations.HotelReservation[0].ResGlobalInfo.Total.AmountBeforeTax += room.ValuationResult.CostPrice.Amount;
            }
        
        }

        //SiteMinder APAC
        private void CreateSiteMinderAPACObject(AccommodationProviderBookingRequestRoom room, SiteMinderRoom siteMinderRoom, ref com.siteminder.apac.smtpi.OTA_HotelResNotifRQ siteMinderObject, AccommodationProviderBookingRequest bookingReq)
        {
            if (siteMinderObject == null)
            {
                siteMinderObject = new com.siteminder.apac.smtpi.OTA_HotelResNotifRQ();

                //RoomRate
                com.siteminder.apac.smtpi.RoomStayTypeRoomRatesRoomRate[] roomRate =
                {
                    new com.siteminder.apac.smtpi.RoomStayTypeRoomRatesRoomRate
                    {
                        RoomTypeCode = siteMinderRoom.roomcode,
                        NumberOfUnits = "1",
                        RatePlanCode = siteMinderRoom.ratecode
                    }
                };

                //GuestCounts
                var guestCount = new List<com.siteminder.apac.smtpi.GuestCountTypeGuestCount>();
                var totalChildren = room.ValuationResult.Children + room.ValuationResult.Infants;

                if (room.ValuationResult.Children > 0 || room.ValuationResult.Infants > 0)
                {
                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = totalChildren.ToString(),
                        AgeQualifyingCode = "8"
                    });
                }
                else
                {
                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                }

                //RatePlans
                var rateplanTypes = new List<com.siteminder.apac.smtpi.RatePlanType>();
                var itemsElementName = new List<com.siteminder.apac.smtpi.ItemsChoiceType>();
                itemsElementName.Add(com.siteminder.apac.smtpi.ItemsChoiceType.Text);
                var rateFormattedText = new com.siteminder.apac.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.ratedescription
                };
                var rateItems = new List<com.siteminder.apac.smtpi.FormattedTextTextType>();
                rateItems.Add(rateFormattedText);

                rateplanTypes.Add(new com.siteminder.apac.smtpi.RatePlanType
                {
                    RatePlanCode = siteMinderRoom.ratecode,
                    RatePlanDescription = new com.siteminder.apac.smtpi.ParagraphType
                    {
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = rateItems.ToArray()
                    }
                });

                //RoomTypes
                var roomTypes = new List<com.siteminder.apac.smtpi.RoomTypeType>();
                var typeFormattedText = new com.siteminder.apac.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.roomdescription
                };
                var typeItems = new List<com.siteminder.apac.smtpi.FormattedTextTextType>();
                typeItems.Add(typeFormattedText);

                roomTypes.Add(new com.siteminder.apac.smtpi.RoomTypeType
                {
                    RoomTypeCode = siteMinderRoom.roomcode,
                    RoomDescription = new com.siteminder.apac.smtpi.ParagraphType
                    {
                        Name = siteMinderRoom.roomdescription,
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = typeItems.ToArray()
                    }
                });

                //RoomStays
                com.siteminder.apac.smtpi.RoomStaysTypeRoomStay[] roomStays =
                {
                    new com.siteminder.apac.smtpi.RoomStaysTypeRoomStay
                    {
                        GuestCounts =
                            new com.siteminder.apac.smtpi.GuestCountType
                            {
                                IsPerRoom = false,
                                IsPerRoomSpecified = true,
                                GuestCount = guestCount.ToArray()
                            },

                        TimeSpan =
                            new com.siteminder.apac.smtpi.DateTimeSpanType
                            {
                                Start = room.ValuationResult.CheckInDate.ToString("yyyy-MM-dd"),
                                End = room.ValuationResult.CheckOutDate.ToString("yyyy-MM-dd")
                            },

                        RoomRates = new com.siteminder.apac.smtpi.RoomStayTypeRoomRates {RoomRate = roomRate},

                        Total = new com.siteminder.apac.smtpi.TotalType
                        {
                            AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                            AmountAfterTaxSpecified = true,
                            AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                            AmountBeforeTaxSpecified = true,
                            CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode
                        },

                        BasicPropertyInfo =
                            new com.siteminder.apac.smtpi.BasicPropertyInfoType {HotelCode = siteMinderRoom.hotelcode},

                        RatePlans = rateplanTypes.ToArray(),

                        RoomTypes = roomTypes.ToArray(),
                    }
                };

                //ProfileType
                var personNames = new List<com.siteminder.apac.smtpi.PersonNameType>();
                string[] givenNames = { bookingReq.Customer.FirstName };
                personNames.Add(new com.siteminder.apac.smtpi.PersonNameType
                {
                    GivenName = givenNames,
                    Surname = bookingReq.Customer.Surname
                });
                var telephone = new List<com.siteminder.apac.smtpi.CustomerTypeTelephone>();
                telephone.Add(new com.siteminder.apac.smtpi.CustomerTypeTelephone
                { PhoneNumber = bookingReq.Customer.ContactNumber });

                var profile = new com.siteminder.apac.smtpi.ProfileType
                {
                    ProfileType1 = "1",
                    Customer = new com.siteminder.apac.smtpi.CustomerType
                    {
                        PersonName = personNames.ToArray(),
                        Telephone = telephone.ToArray()
                    }
                };

                //Profile
                var profiles = new List<com.siteminder.apac.smtpi.ProfilesTypeProfileInfo>();
                profiles.Add(new com.siteminder.apac.smtpi.ProfilesTypeProfileInfo { Profile = profile });

                com.siteminder.apac.smtpi.ResGuestType[] resGuests =
                {
                    new com.siteminder.apac.smtpi.ResGuestType {Profiles = profiles.ToArray()}
                };

                var resGlobalInfo = new com.siteminder.apac.smtpi.ResGlobalInfoType
                {
                    Total = new com.siteminder.apac.smtpi.TotalType
                    {
                        CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode,
                        AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                        AmountAfterTaxSpecified = true,
                        AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                        AmountBeforeTaxSpecified = true
                    },

                    Profiles = profiles.ToArray()
                };

                //HotelReservationType
                var hotelReservations = new List<com.siteminder.apac.smtpi.HotelReservationType>();
                var uniqueId = new List<com.siteminder.apac.smtpi.UniqueID_Type>();
                uniqueId.Add(new com.siteminder.apac.smtpi.UniqueID_Type
                {
                    ID = bookingReq.ItineraryId.ToString(),
                    Type = "14"
                });
                hotelReservations.Add(new com.siteminder.apac.smtpi.HotelReservationType
                {
                    CreateDateTime = Convert.ToDateTime((DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:sszz"))),
                    CreateDateTimeSpecified = true,
                    UniqueID = uniqueId.ToArray(),
                    RoomStays = roomStays,
                    ResGuests = resGuests,
                    ResGlobalInfo = resGlobalInfo
                });

                //Set the values to siteMinder object
                siteMinderObject.Version = (decimal)1.0;
                siteMinderObject.EchoToken = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:sszzz");
                siteMinderObject.ResStatus = com.siteminder.apac.smtpi.TransactionActionType.Commit;
                siteMinderObject.ResStatusSpecified = true;

                com.siteminder.apac.smtpi.SourceType[] sourceType =
                {
                    new com.siteminder.apac.smtpi.SourceType
                    {
                        RequestorID = new com.siteminder.apac.smtpi.SourceTypeRequestorID() {Type = "22", ID = "ARC"},
                        BookingChannel = new com.siteminder.apac.smtpi.SourceTypeBookingChannel()
                        {
                            Primary = true,
                            Type = "1",
                            PrimarySpecified = true,
                            CompanyName =
                                new com.siteminder.apac.smtpi.CompanyNameType()
                                {
                                    Code = bookingReq.Provider.Parameters.GetParameterValue(SMCompanyNameTypeCode),
                                    Value = bookingReq.Provider.Parameters.GetParameterValue(SMCompanyNameTypeValue)
                                }
                        }
                    }
                };

                siteMinderObject.POS = sourceType;
                siteMinderObject.HotelReservations = new com.siteminder.apac.smtpi.HotelReservationsType
                {
                    HotelReservation = hotelReservations.ToArray()
                };
            }

            else
            {
                //Update room information

                //RoomRate
                com.siteminder.apac.smtpi.RoomStayTypeRoomRatesRoomRate[] roomRate =
                {
                    new com.siteminder.apac.smtpi.RoomStayTypeRoomRatesRoomRate
                    {
                        RoomTypeCode = siteMinderRoom.roomcode,
                        NumberOfUnits = "1",
                        RatePlanCode = siteMinderRoom.ratecode
                    }
                };

                //GuestCounts
                var guestCount = new List<com.siteminder.apac.smtpi.GuestCountTypeGuestCount>();
                var totalChildren = room.ValuationResult.Children + room.ValuationResult.Infants;

                if (room.ValuationResult.Children > 0 || room.ValuationResult.Infants > 0)
                {
                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = totalChildren.ToString(),
                        AgeQualifyingCode = "8"
                    });

                }
                else
                {
                    guestCount.Add(new com.siteminder.apac.smtpi.GuestCountTypeGuestCount
                    {
                        Count = room.ValuationResult.Adults.ToString(),
                        AgeQualifyingCode = "10"
                    });

                }

                var roomstaysArray = siteMinderObject.HotelReservations.HotelReservation[0].RoomStays;
                Array.Resize(ref roomstaysArray, siteMinderObject.HotelReservations.HotelReservation[0].RoomStays.Length + 1);
                siteMinderObject.HotelReservations.HotelReservation[0].RoomStays = roomstaysArray;

                //RatePlans
                var rateplanTypes = new List<com.siteminder.apac.smtpi.RatePlanType>();
                var itemsElementName = new List<com.siteminder.apac.smtpi.ItemsChoiceType>();
                itemsElementName.Add(com.siteminder.apac.smtpi.ItemsChoiceType.Text);
                var rateFormattedText = new com.siteminder.apac.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.ratedescription
                };
                var rateItems = new List<com.siteminder.apac.smtpi.FormattedTextTextType>();
                rateItems.Add(rateFormattedText);

                rateplanTypes.Add(new com.siteminder.apac.smtpi.RatePlanType
                {
                    RatePlanCode = siteMinderRoom.ratecode,
                    RatePlanDescription = new com.siteminder.apac.smtpi.ParagraphType
                    {
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = rateItems.ToArray()
                    }
                });

                //RoomTypes
                var roomTypes = new List<com.siteminder.apac.smtpi.RoomTypeType>();
                var typeFormattedText = new com.siteminder.apac.smtpi.FormattedTextTextType
                {
                    Value = siteMinderRoom.roomdescription
                };
                var typeItems = new List<com.siteminder.apac.smtpi.FormattedTextTextType>();
                typeItems.Add(typeFormattedText);

                roomTypes.Add(new com.siteminder.apac.smtpi.RoomTypeType
                {
                    RoomTypeCode = siteMinderRoom.roomcode,
                    RoomDescription = new com.siteminder.apac.smtpi.ParagraphType
                    {
                        Name = siteMinderRoom.roomdescription,
                        ItemsElementName = itemsElementName.ToArray(),
                        Items = typeItems.ToArray()
                    }
                });

                siteMinderObject.HotelReservations.HotelReservation[0].RoomStays[siteMinderObject.HotelReservations.HotelReservation[0].RoomStays.Length - 1]

                    = new com.siteminder.apac.smtpi.RoomStaysTypeRoomStay
                    {
                        GuestCounts = new com.siteminder.apac.smtpi.GuestCountType
                        {
                            IsPerRoom = false,
                            IsPerRoomSpecified = true,
                            GuestCount = guestCount.ToArray()
                        },

                        TimeSpan = new com.siteminder.apac.smtpi.DateTimeSpanType
                        {
                            Start = room.ValuationResult.CheckInDate.ToString("yyyy-MM-dd"),
                            End = room.ValuationResult.CheckOutDate.ToString("yyyy-MM-dd")
                        },

                        RoomRates = new com.siteminder.apac.smtpi.RoomStayTypeRoomRates { RoomRate = roomRate },

                        Total = new com.siteminder.apac.smtpi.TotalType
                        {
                            AmountAfterTax = room.ValuationResult.CostPrice.Amount,
                            AmountAfterTaxSpecified = true,
                            AmountBeforeTax = room.ValuationResult.CostPrice.Amount,
                            AmountBeforeTaxSpecified = true,
                            CurrencyCode = room.ValuationResult.CostPrice.CurrencyCode
                        },

                        BasicPropertyInfo = new com.siteminder.apac.smtpi.BasicPropertyInfoType { HotelCode = siteMinderRoom.hotelcode },

                        RatePlans = rateplanTypes.ToArray(),

                        RoomTypes = roomTypes.ToArray(),
                    };

                //Update Total price
                siteMinderObject.HotelReservations.HotelReservation[0].ResGlobalInfo.Total.AmountAfterTax += room.ValuationResult.CostPrice.Amount;
                siteMinderObject.HotelReservations.HotelReservation[0].ResGlobalInfo.Total.AmountBeforeTax  += room.ValuationResult.CostPrice.Amount;
            }

        }
    }

}