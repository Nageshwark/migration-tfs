﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<IEnumerable<AriaContractsAvailabilityResponse>> automator;
        private readonly IAccommodationAvailabilityParserAsync<IEnumerable<AriaContractsAvailabilityResponse>> parser;

        public AriaContractsAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<IEnumerable<AriaContractsAvailabilityResponse>> automator
            , IAccommodationAvailabilityParserAsync<IEnumerable<AriaContractsAvailabilityResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var availabilityResponse = await automator.GetAvailabilityResponseAsync(request);
            return await parser.GetAvailabilityResultsAsync(request, availabilityResponse);
        }
    }
}
