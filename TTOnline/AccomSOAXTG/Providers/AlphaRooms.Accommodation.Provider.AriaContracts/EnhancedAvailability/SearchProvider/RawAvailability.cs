﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SearchProvider
{
    using System;

    public class RawAvailability
    {
        public Guid RoomId { get; set; }
        public string RoomDescription { get; set; }
        public string RoomFullDescription { get; set; }

        public string EdiCode { get; set; }
        public Guid SupplierId { get; set; }

        public bool IsBindingRate { get; set; }
        public bool IsExtranetNetted { get; set; }

        public bool IsNonRefundable { get; set; }

        public int PaymentModel { get; set; }

        public decimal? Margin { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? SalePriceRO {  get; set;}
        public decimal? SalePriceSC{ get; set;}
        public decimal? SalePriceBB{ get; set;}
        public decimal? SalePriceHB{ get; set;}
        public decimal? SalePriceFB{ get; set;}
        public decimal? SalePriceAI{ get; set;}

        public int QuantityAvailable { get; set; }
    }
}
