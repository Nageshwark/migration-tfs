﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SearchProvider
{
    using Helper;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Responsible for query the database for availability
    /// </summary>
    internal class DatabaseSearchProvider : ISearchProvider
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DatabaseSearchProvider));
        private readonly string connectionString;

        public DatabaseSearchProvider(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Can not be null or empty", nameof(connectionString));
            }

            this.connectionString = connectionString;
        }

        /// <summary>
        /// searches the DB using spSelectAvailableRoomsByDestination
        /// </summary>
        /// <param name="availabilityRequest">the request to search for</param>
        /// <returns>a list of (Very) raw results</returns>
        public List<RawAvailability> Search(AvailabilityRequest availabilityRequest)
        {
            List<RawAvailability> results = new List<RawAvailability>();

            SqlParameter[] searchParameters = GetSqlParameters(availabilityRequest);

            DateTime startTime = DateTime.UtcNow;

            // fire the monster SP spSelectAvailableRoomsByDestination
            using (SqlDataReader dataReader = SqlHelper.ExecuteReader(this.connectionString, "[spSelectAvailableRoomsByDestination_v1.11]", searchParameters))
            {
                while (dataReader.Read())
                {
                    RawAvailability availabilityRow = new RawAvailability
                    {
                        // used index for speed, read the data fast as
                        RoomId = (Guid)dataReader[0],
                        SupplierId = (Guid)dataReader[1],
                        RoomFullDescription = (string)dataReader[2],
                        IsBindingRate = (bool)dataReader[3],
                        IsExtranetNetted = (bool)dataReader[4],
                        IsNonRefundable = (bool)dataReader[5],
                        PaymentModel = (int)dataReader[6],
                        Margin = LoadNullableDecimal(dataReader[7]),
                        CurrencyCode = (string)dataReader[8],
                        EdiCode = (string)dataReader[9],
                        RoomDescription = (string)dataReader[10],

                        SalePriceRO = LoadNullableDecimal(dataReader[11]),
                        SalePriceSC = LoadNullableDecimal(dataReader[12]),
                        SalePriceBB = LoadNullableDecimal(dataReader[13]),
                        SalePriceHB = LoadNullableDecimal(dataReader[14]),
                        SalePriceFB = LoadNullableDecimal(dataReader[15]),
                        SalePriceAI = LoadNullableDecimal(dataReader[16]),
                        QuantityAvailable = (int)dataReader[17],
                    };

                    results.Add(availabilityRow);
                }
            }

            DatabaseSearchProvider.Log.Debug($"spSelectAvailableRoomsByDestination took {DateTime.UtcNow - startTime} to excute. Returned {results.Count} results");

            return results;
        }

        private static SqlParameter[] GetSqlParameters(AvailabilityRequest request)
        {
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@dtmArrival", request.ArrivalDate),
                new SqlParameter("@dtmDeparture", request.DepartureDate),
                new SqlParameter("@fkDestinationID", request.DestinationId),
                new SqlParameter("@fkEdiProvider", "A"),
                new SqlParameter("@ChannelID", request.ChannelId),
                new SqlParameter("@intAdult", request.AdultNumbers),
                new SqlParameter("@intChild", request.ChildNumbers),
                new SqlParameter("@intChildAge1", request.ChildNumbers > 0 ? (object)request.ChildAges[0] : DBNull.Value),
                new SqlParameter("@intChildAge2", request.ChildNumbers > 1 ? (object)request.ChildAges[1] : DBNull.Value),
                new SqlParameter("@intChildAge3", request.ChildNumbers > 2 ? (object)request.ChildAges[2] : DBNull.Value),
                new SqlParameter("@intChildAge4", request.ChildNumbers > 3 ? (object)request.ChildAges[3] : DBNull.Value),
            };

            return sqlParameters;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static decimal? LoadNullableDecimal(Object value)
        {
            decimal? results;

            if (DBNull.Value.Equals(value))
            {
                results = new decimal?();
            }
            else
            {
                results = new decimal?((decimal)value);
            }

            return results;
        }
    }
}