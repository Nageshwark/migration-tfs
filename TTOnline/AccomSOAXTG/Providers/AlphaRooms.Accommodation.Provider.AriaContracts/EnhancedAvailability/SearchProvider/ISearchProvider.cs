﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SearchProvider
{
    using System.Collections.Generic;

    /// <summary>
    /// Responsible for query the database for availability
    /// </summary>
    internal interface ISearchProvider
    {
        /// <summary>
        /// searches the DB using spSelectAvailableRoomsByDestination
        /// </summary>
        /// <param name="availabilityRequest">the request to search for</param>
        /// <returns>a list of (Very) raw results</returns>
        List<RawAvailability> Search(AvailabilityRequest availabilityRequest);
    }
}
