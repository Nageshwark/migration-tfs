﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability
{
    using System;

    /// <summary>
    /// Represents a request for accommodation availability search.
    /// Readonly
    /// </summary>
    public class AvailabilityRequest
    {
        private readonly int channelId;
        private readonly Guid destinationId;
        private readonly DateTime arrivalDate;
        private readonly DateTime departureDate;
        private readonly int adultNumbers;
        private readonly int childNumbers;
        private readonly byte[] childAges;

        public AvailabilityRequest(
            int channelId,
            Guid destinationId,
            DateTime arrivalDate,
            DateTime departureDate,
            int adultNumbers,
            int childNumbers,
            byte[] childAges)
        {
            this.channelId = channelId;
            this.destinationId = destinationId;

            if (arrivalDate < DateTime.UtcNow.Date)
            {
                throw new ArgumentException("Can not search for a past date", nameof(arrivalDate));
            }

            if (departureDate <= arrivalDate)
            {
                throw new ArgumentException("departure must be after arrival", nameof(departureDate));
            }

            this.arrivalDate = arrivalDate;
            this.departureDate = departureDate;

            if (0 >= adultNumbers)
            {
                throw new ArgumentException("Must be aleast one adult", nameof(adultNumbers));
            }

            if (0 > childNumbers || childNumbers > 4)
            {
                throw new ArgumentException("Must be between 0 & 4", nameof(childNumbers));
            }

            this.adultNumbers = adultNumbers;
            this.childNumbers = childNumbers;

            if (childAges == null)
            {
                throw new ArgumentNullException(nameof(childAges));
            }

            if (this.childNumbers != childAges.Length)
            {
                throw new ArgumentException("Must provide all child ages", nameof(childAges));
            }

            // needed to be sorted Descending, I do not know why the old one did this
            var newChildAges = (byte[])childAges.Clone();
            Array.Sort(newChildAges);
            Array.Reverse(newChildAges);

            this.childAges = newChildAges;

            // some rooms have a minimum of 2 adults, so if there is an adult and a child, that child is taken into account as an adult, because thems the rules
            if (this.adultNumbers == 1 && this.childNumbers > 0)
            {
                this.adultNumbers = 2;
                --this.childNumbers;

                byte[] alteredChildAges = new byte[this.childNumbers];
                Array.Copy(this.childAges, 1, alteredChildAges, 0, alteredChildAges.Length);
                this.childAges = alteredChildAges;
            }
        }

        public int ChannelId
        {
            get
            {
                return this.channelId;
            }
        }

        public Guid DestinationId
        {
            get
            {
                return this.destinationId;
            }
        }

        public DateTime ArrivalDate
        {
            get
            {
                return this.arrivalDate;
            }
        }

        public DateTime DepartureDate
        {
            get
            {
                return this.departureDate;
            }
        }

        public int AdultNumbers
        {
            get
            {
                return this.adultNumbers;
            }
        }

        public int ChildNumbers
        {
            get
            {
                return this.childNumbers;
            }
        }

        public byte[] ChildAges
        {
            get
            {
                return this.childAges;
            }
        }
    }
}