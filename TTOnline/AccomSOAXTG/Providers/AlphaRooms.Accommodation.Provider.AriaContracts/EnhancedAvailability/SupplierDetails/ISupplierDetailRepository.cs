﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SupplierDetails
{
    using System;

    /// <summary>
    /// Responsible for providing details about a room provider.
    /// Must be thread safe as it will be called be multiple threads at the same time
    /// </summary>
    internal interface ISupplierDetailRepository
    {
        /// <summary>
        /// Get the details of the supplier given its key  
        /// </summary>
        /// <param name="supplierId">the key of the supplier</param>
        /// <returns>the suppiers details</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">If the provide supplier Id does not exist</exception>
        SupplierDetail GetDetails(Guid supplierId);
    }
}