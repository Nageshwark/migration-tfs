﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SupplierDetails
{
    using System;

    internal struct SupplierDetail
    {
        private readonly string name;
        private readonly string terms;

        public SupplierDetail(string name, string terms)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Can not be null or empty", nameof(name));
            }

            if (string.IsNullOrEmpty(terms))
            {
                throw new ArgumentException("Can not be null or empty", nameof(terms));
            }

            this.name = name;
            this.terms = terms;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string Terms
        {
            get
            {
                return this.terms;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}