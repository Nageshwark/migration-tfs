﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SupplierDetails
{
    internal interface ISupplierDetailRepositoryFactory
    {
        ISupplierDetailRepository Create(string connectionString);
    }
}