﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SupplierDetails
{
    using log4net;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Responsible for providing details about a room provider.
    /// Must be thread safe as it will be called be multiple threads at the same time
    /// </summary>
    internal class SupplierDetailRepository : ISupplierDetailRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SupplierDetailRepository));

        // this is thread safe as it is only ever read from https://msdn.microsoft.com/en-us/library/xfhwa508(v=vs.110).aspx
        private readonly Dictionary<Guid, SupplierDetail> contracts;

        public SupplierDetailRepository(Dictionary<Guid, SupplierDetail> contracts)
        {
            if (contracts == null)
            {
                throw new ArgumentNullException(nameof(contracts));
            }

            this.contracts = contracts;
        }

        /// <summary>
        /// Get the details of the supplier given its key  
        /// </summary>
        /// <param name="supplierId">the key of the supplier</param>
        /// <returns>the suppiers details</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">If the provide supplier Id does not exist</exception>
        public SupplierDetail GetDetails(Guid supplierId)
        {
            SupplierDetail details;

            try
            {
                details = this.contracts[supplierId];
            }
            catch (KeyNotFoundException)
            {
                Log.Error("Could not find supplier for key : " + supplierId);
                throw;
            }

            return details;
        }
    }
}
