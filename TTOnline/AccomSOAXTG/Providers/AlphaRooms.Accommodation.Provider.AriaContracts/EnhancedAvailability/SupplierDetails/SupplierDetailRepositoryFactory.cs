﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.SupplierDetails
{
    using Helper;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    internal class SupplierDetailRepositoryFactory : ISupplierDetailRepositoryFactory
    {
        public ISupplierDetailRepository Create(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Can not be null or empty.", nameof(connectionString));
            }

            Dictionary<Guid, SupplierDetail> contracts = new Dictionary<Guid, SupplierDetail>();

            const string spName = "[GetSupplierDetails]";

            using (SqlDataReader result = SqlHelper.ExecuteReader(connectionString, spName, null))
            {
                while (result.Read())
                {
                    Guid supplierId = (Guid)result[0];

                    string supplierName = (string)result[1];
                    string cancellationTerms = result[2] as string;
                    string noShowTerms = result[3] as string;

                    SupplierDetail details = new SupplierDetail(
                        supplierName,
                        cancellationTerms + "==" + noShowTerms);

                    contracts.Add(
                        supplierId,
                        details);
                }
            }

            return new SupplierDetailRepository(contracts);
        }
    }
}
