﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability
{
    using System;

    public class AvailabilityResult : ICloneable
    {
        public Guid RoomId { get; set; }
        public string RoomDescription { get; set; }
        public string RoomFullDescription { get; set; }

        public string BoardType { get; set; }
        public string BoardDescription { get; set; }

        public string EdiCode { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string CancellationTerms { get; set; }

        public bool IsBindingRate { get; set; }
        public bool IsNonRefundable { get; set; }

        public int PaymentModel { get; set; }

        public decimal Margin { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Cost { get; set; }
        public decimal Sale { get; set; }

        public AvailabilityResult Clone()
        {
            return (AvailabilityResult)this.MemberwiseClone();
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
