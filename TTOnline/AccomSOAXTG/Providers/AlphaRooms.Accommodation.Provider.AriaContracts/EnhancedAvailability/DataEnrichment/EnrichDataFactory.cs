﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    using BookingAdjustments;
    using System;

    /// <summary>
    /// Responsible for creating the data enrichment instances
    /// Is thread safe, will be called by multiple threads at the same time 
    /// </summary>
    internal class EnrichDataFactory : IEnrichDataFactory
    {
        // these are stateless
        private static readonly EnrichRoomName RoomNameEnrichment = new EnrichRoomName();
        private static readonly EnrichRoomBoardPrice RoomBoardPriceEnrichment = new EnrichRoomBoardPrice();

        private readonly BookingAdjustmentRepository bookingAdjustmentRepository;

        public EnrichDataFactory(BookingAdjustmentRepository bookingAdjustmentRepository)
        {
            if (bookingAdjustmentRepository == null)
            {
                throw new ArgumentNullException(nameof(bookingAdjustmentRepository));
            }

            this.bookingAdjustmentRepository = bookingAdjustmentRepository;
        }

        public IEnrichData[] Create(AvailabilityRequest request)
        {
            var bookingAdjustmentsEnrichment = new EnrichWithBookingAdjustments(request, this.bookingAdjustmentRepository);

            IEnrichData[] dataEnricments = new IEnrichData[]
            {
                RoomNameEnrichment,
                RoomBoardPriceEnrichment,
                bookingAdjustmentsEnrichment
            };

            return dataEnricments;
        }
    }
}
