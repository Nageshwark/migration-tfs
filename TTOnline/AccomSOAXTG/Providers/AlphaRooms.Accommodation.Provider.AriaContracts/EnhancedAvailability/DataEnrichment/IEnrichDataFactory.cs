﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    internal interface IEnrichDataFactory
    {
        IEnrichData[] Create(AvailabilityRequest request);
    }
}
