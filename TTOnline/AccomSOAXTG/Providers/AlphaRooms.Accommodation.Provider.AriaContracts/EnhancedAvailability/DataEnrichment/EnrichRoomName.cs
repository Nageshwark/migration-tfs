﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    using SearchProvider;
    using System;
    using Helper;

    /// <summary>
    /// Responsible for adding Non Refundable to room descriptions that are missing it
    /// </summary>
    internal class EnrichRoomName : IEnrichData
    {
        public void Enrich(RawAvailability availability)
        {
            if (availability.IsNonRefundable
                && !availability.RoomFullDescription.Contains("non refundable", StringComparison.OrdinalIgnoreCase))
            {
                availability.RoomFullDescription += " - Non Refundable";
            }
        }
    }
}