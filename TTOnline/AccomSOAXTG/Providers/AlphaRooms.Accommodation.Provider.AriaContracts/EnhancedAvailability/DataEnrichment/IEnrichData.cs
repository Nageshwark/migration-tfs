﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    using SearchProvider;

    /// <summary>
    /// Add, corrects or improves the raw data base rows
    /// </summary>
    internal interface IEnrichData
    {
        /// <summary>
        /// Enrichs the data row passed to it
        /// </summary>
        /// <param name="availability">never null</param>
        void Enrich(RawAvailability availability);
    }
}
