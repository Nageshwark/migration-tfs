﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    using SearchProvider;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Responsible for correcting the type of board type offered by the row
    /// </summary>
    internal class EnrichRoomBoardPrice : IEnrichData
    {
        public void Enrich(RawAvailability availability)
        {
            // need to fix this shit 

            // Absolutely no fucking idea why we do this
            // correct ing some data issue?? 

            if (availability.SalePriceRO.HasValue &&
                !availability.SalePriceSC.HasValue &&
                !availability.SalePriceBB.HasValue &&
                !availability.SalePriceHB.HasValue &&
                !availability.SalePriceFB.HasValue &&
                !availability.SalePriceAI.HasValue)
            {
                string[] FixedBoards = new string[] { "(SC)", "(BB)", "(HB)", "(FB)", "(AI)" };

                // this must be computationally expensive !!! What are we doing would not IndexOf work!!!!!!
                Match m = Regex.Match(availability.RoomDescription, @"\(..\)", RegexOptions.RightToLeft);

                if (m != null && FixedBoards.Contains(m.Value.ToUpper()))
                {
                    switch (m.Value)
                    {
                        case "(SC)":
                            availability.SalePriceSC = availability.SalePriceRO;
                            break;
                        case "(BB)":
                            availability.SalePriceBB = availability.SalePriceRO;
                            break;
                        case "(HB)":
                            availability.SalePriceHB = availability.SalePriceRO;
                            break;
                        case "(FB)":
                            availability.SalePriceFB = availability.SalePriceRO;
                            break;
                        case "(AI)":
                            availability.SalePriceAI = availability.SalePriceRO;
                            break;
                    }

                    availability.SalePriceRO = null;
                }
            }
        }
    }
}
