﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.DataEnrichment
{
    using BookingAdjustments;
    using SearchProvider;
    using System;
    using System.Linq;

    /// <summary>
    /// Applies fees stored in the tblAdjustmentRoom into the availability passed.
    /// Is thread safe, maybe be called by multiple threads at the same time.
    /// </summary>
    internal class EnrichWithBookingAdjustments : IEnrichData
    {
        private readonly AvailabilityRequest request;
        private readonly BookingAdjustmentRepository repository;

        public EnrichWithBookingAdjustments(AvailabilityRequest request, BookingAdjustmentRepository repository)
        {
            if(request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }

            this.request = request;
            this.repository = repository;
        }

        /// <summary>
        /// Enrichs the data row passed to it
        /// </summary>
        /// <param name="availability">never null</param>
        public void Enrich(RawAvailability availability)
        {
            if (availability == null)
            {
                throw new ArgumentNullException(nameof(availability));
            }

            // get all adjustments that should be applied
            var bookingAdjustments = this.repository.Get(availability.RoomId, this.request.ArrivalDate, this.request.DepartureDate);

            // the fees are applied per room type
            decimal nonPerPersonTotal = 0;
            decimal roomOnly = 0;
            decimal selfCatering = 0;
            decimal breakfast = 0;
            decimal halfBoard = 0;
            decimal fullBoard = 0;
            decimal allInclusive = 0;

            foreach (var bookingAdjustment in bookingAdjustments)
            {
                int daysMultiplier;

                if (bookingAdjustment.IsPerNight)
                {
                    // if the fee is per night work out how many night the fee applies to this request for
                    daysMultiplier = this.GetDaysMultiplier(bookingAdjustment);
                }
                else
                {
                    // this is a one of fee, there are not many of these
                    daysMultiplier = 1;
                }

                if (!bookingAdjustment.IsPerPerson)
                {
                    // does not matter how many is in the room
                    nonPerPersonTotal += daysMultiplier * bookingAdjustment.Value;
                }
                else
                {
                    decimal amout = 0;

                    if (bookingAdjustment.Adult)
                    {
                        int totalOfAdults = this.GetTotalAdults(bookingAdjustment);
                        amout += daysMultiplier * (totalOfAdults * bookingAdjustment.Value);
                    }

                    if (bookingAdjustment.Youth)
                    {
                        int totalOfYouths = this.GetTotalYouths(bookingAdjustment);
                        amout += daysMultiplier * (totalOfYouths * bookingAdjustment.Value);
                    }

                    if (bookingAdjustment.Child)
                    {
                        int totalOfChildren = this.GetTotalChildren(bookingAdjustment);
                        amout += daysMultiplier * (totalOfChildren * bookingAdjustment.Value);
                    }

                    if (bookingAdjustment.Infant)
                    {
                        int totalOfInfant = this.GetTotalInfants(bookingAdjustment);
                        amout += daysMultiplier * (totalOfInfant * bookingAdjustment.Value);
                    }

                    // add total
                    if (bookingAdjustment.IsRO)
                    {
                        roomOnly += amout;
                    }

                    if (bookingAdjustment.IsSC)
                    {
                        selfCatering += amout;
                    }

                    if (bookingAdjustment.IsBB)
                    {
                        breakfast += amout;
                    }

                    if (bookingAdjustment.IsHB)
                    {
                        halfBoard += amout;
                    }

                    if (bookingAdjustment.IsFB)
                    {
                        fullBoard += amout;
                    }

                    if (bookingAdjustment.IsAI)
                    {
                        allInclusive += amout;
                    }
                }
            }

            // after all of the adjustments add the values to our availability row
            if (availability.SalePriceRO.HasValue)
            {
                availability.SalePriceRO += (nonPerPersonTotal + roomOnly);
            }

            if (availability.SalePriceSC.HasValue)
            {
                availability.SalePriceSC += (nonPerPersonTotal + selfCatering);
            }

            if (availability.SalePriceBB.HasValue)
            {
                availability.SalePriceBB += (nonPerPersonTotal + breakfast);
            }

            if (availability.SalePriceHB.HasValue)
            {
                availability.SalePriceHB += (nonPerPersonTotal + halfBoard);
            }

            if (availability.SalePriceFB.HasValue)
            {
                availability.SalePriceFB += (nonPerPersonTotal + fullBoard);
            }

            if (availability.SalePriceAI.HasValue)
            {
                availability.SalePriceAI += (nonPerPersonTotal + allInclusive);
            }
        }

        /// <summary>
        /// Work out how many night the fee applies to this request for
        /// </summary>
        /// <param name="bookingAdjustment">the Booking Adjustment to get the number of days it applies for</param>
        /// <returns>the number of day it applies for</returns>
        private int GetDaysMultiplier(BookingAdjustment bookingAdjustment)
        {
            // max date
            DateTime startDate = this.request.ArrivalDate > bookingAdjustment.StartDate ? this.request.ArrivalDate : bookingAdjustment.StartDate;

            DateTime endDate;
            int exclusivity;

            // min date
            if (this.request.DepartureDate < bookingAdjustment.EndDate)
            {
                endDate = this.request.DepartureDate;

                // as this is the customers last night they do not stay this night so we can not charge them for it
                const int Exclusive = 0;
                exclusivity = Exclusive;
            }
            else
            {
                endDate =  bookingAdjustment.EndDate;

                // this is the adjustment last night not the customers so we must charge the customers for it
                const int Inclusive = 1;
                exclusivity = Inclusive;
            }

            TimeSpan difference = endDate - startDate;

            return difference.Days + exclusivity;
        }

        // these GetTotalX methods where copied from the DB versions
        private int GetTotalAdults(BookingAdjustment adjustment)
        {
            int minAdultAge;

            if (adjustment.MaxAgeYouth > 0)
            {
                minAdultAge = adjustment.MaxAgeYouth;
            }
            else if (adjustment.MaxAgeChild > 0)
            {
                minAdultAge = adjustment.MaxAgeChild;
            }
            else
            {
                minAdultAge = adjustment.MaxAgeInfant;
            }

            return this.request.AdultNumbers + this.request.ChildAges.Count(x => x > minAdultAge);
        }

        private int GetTotalYouths(BookingAdjustment adjustment)
        {
            if (adjustment.MaxAgeYouth == 0)
            {
                return 0;
            }

            return this.request.ChildAges.Count(x => x <= adjustment.MaxAgeYouth && x > adjustment.MaxAgeChild && x > adjustment.MaxAgeInfant);
        }

        private int GetTotalChildren(BookingAdjustment adjustment)
        {
            if (adjustment.MaxAgeChild == 0)
            {
                return 0;
            }

            return this.request.ChildAges.Count(x => x <= adjustment.MaxAgeChild && x > adjustment.MaxAgeInfant);
        }

        private int GetTotalInfants(BookingAdjustment adjustment)
        {
            if (adjustment.MaxAgeInfant == 0)
            {
                return 0;
            }

            return this.request.ChildAges.Count(x => x <= adjustment.MaxAgeInfant);
        }
    }
}
