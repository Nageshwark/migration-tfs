﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.Searching
{
    using SearchProvider;
    using SupplierDetails;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Responsible for converting from the raw DB rows into useful AvailabilityResult 
    /// not thread safe can reuse
    /// </summary>
    internal class AvailabilityResultGeneration
    {
        private readonly ISupplierDetailRepository supplierRepository;
        private List<AriaContractsAvailabilityResponse> availabilityResults = null;

        public AvailabilityResultGeneration(ISupplierDetailRepository supplierRepository)
        {
            if (supplierRepository == null)
            {
                throw new ArgumentNullException(nameof(supplierRepository));
            }

            this.supplierRepository = supplierRepository;
        }

        public List<AriaContractsAvailabilityResponse> ConvertAvailability(List<RawAvailability> rawAvailability)
        {
            if (rawAvailability == null)
            {
                throw new ArgumentNullException(nameof(rawAvailability));
            }

            this.availabilityResults = new List<AriaContractsAvailabilityResponse>();

            for (int i = 0; i < rawAvailability.Count; i++)
            {
                decimal margin = CalulateMargin(rawAvailability[i]);
                SupplierDetail supplierDetails = this.supplierRepository.GetDetails(rawAvailability[i].SupplierId);
                string terms = CreateTerms(supplierDetails.Terms, rawAvailability[i].IsNonRefundable);

                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceRO, "RO", "Room Only");
                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceSC, "SC", "Self Catering");
                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceBB, "BB", "Bed & Breakfast");
                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceHB, "HB", "Half Board");
                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceFB, "FB", "Full Board");
                this.TryAddAvailability(rawAvailability[i], margin, terms, supplierDetails, rawAvailability[i].SalePriceAI, "AI", "All Inclusive");


            }

            return this.availabilityResults;
        }

        private void TryAddAvailability(RawAvailability rawAvailability, decimal margin, string cancellationTerms, SupplierDetail supplierDetail, decimal? salePrice, string boardType, string boardDescription)
        {
            if (salePrice.HasValue)
            {
                AriaContractsAvailabilityResponse availabilityResult = ConvertAvailability(rawAvailability, margin, cancellationTerms, supplierDetail, salePrice.Value, boardType, boardDescription);
                this.availabilityResults.Add(availabilityResult);
            }
        }

        private static AriaContractsAvailabilityResponse ConvertAvailability(RawAvailability rawAvailability, decimal margin, string cancellationTerms, SupplierDetail supplierDetail, decimal salePrice, string boardType, string boardDescription)
        {
            decimal costPrice;

            // Record cost price same as sale price if supplier is going to take payment
            if (rawAvailability.PaymentModel == 2 // PaymentModel.PayOnBooking
                || rawAvailability.PaymentModel == 3) // PaymentModel.CustomerPayDirect
            {
                costPrice = salePrice;
            }
            else
            {
                costPrice = CalculateCostPrice(margin, salePrice);
            }

            var availabilityResult = new AriaContractsAvailabilityResponse
            {
                RoomId = rawAvailability.RoomId,
                RoomDescription = rawAvailability.RoomDescription,
                RoomFullDescription = rawAvailability.RoomFullDescription,

                BoardType = boardType,
                BoardDescription = boardDescription,

                EdiCode = rawAvailability.EdiCode,
                SupplierId = rawAvailability.SupplierId,
                SupplierName = supplierDetail.Name,
                CancellationTerms = cancellationTerms,

                // if it's netted, then don't show it as a binding rate
                IsBindingRate = rawAvailability.IsBindingRate && !rawAvailability.IsExtranetNetted,
                IsNonRefundable = rawAvailability.IsNonRefundable,

                PaymentModel = rawAvailability.PaymentModel,
                Margin = margin,
                CurrencyCode = rawAvailability.CurrencyCode,
                Cost = costPrice,
                Sale = salePrice,
                IsNetted = rawAvailability.IsExtranetNetted
            };

            return availabilityResult;
        }

        // only used in one place
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static decimal CalculateCostPrice(decimal margin, decimal salePrice)
        {
            // ya thats right we calualted the sale price now we magical calualte the cost price by using the margin

            if (margin == 0)
            {
                return salePrice;
            }

            decimal calculatedCost = salePrice * (1 - (0.01m * margin));
            return calculatedCost;
        }

        // only used in one place
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static decimal CalulateMargin(RawAvailability rawResult)
        {
            decimal margin;
            if (!rawResult.IsBindingRate)
            {
                margin = 0;
            }
            else if (rawResult.Margin.HasValue)
            {
                margin = rawResult.Margin.Value;
            }
            else
            {
                const decimal DefaultMargin = 15m;
                margin = DefaultMargin;
            }

            return margin;
        }

        // only used in one place
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string CreateTerms(string terms, bool isNonRefundable)
        {
            string refundable = isNonRefundable ? "true==" : "false==";

            string incomingContract = String.Concat(
                refundable,
                terms);

            return incomingContract;
        }
    }
}