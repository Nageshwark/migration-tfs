﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.Searching
{
    using DataEnrichment;
    using SearchProvider;
    using SupplierDetails;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Responsible for searching for availability of Aria Contracts.
    /// Is thread safe
    /// </summary>
    internal class AriaContractsAvailability : IAriaContractsAvailability
    {
        // to search the DB
        private readonly ISearchProvider searchProvider;
        // to provide the supplier details
        private readonly ISupplierDetailRepository supplierRepository;
        // add/correct data that comes from the DB
        private readonly IEnrichDataFactory dataEnrichmentFactory;

        public AriaContractsAvailability(ISearchProvider searchProvider, ISupplierDetailRepository supplierRepository, IEnrichDataFactory dataEnrichmentFactory)
        {
            if (searchProvider == null)
            {
                throw new ArgumentNullException(nameof(searchProvider));
            }

            if (supplierRepository == null)
            {
                throw new ArgumentNullException(nameof(supplierRepository));
            }

            if (dataEnrichmentFactory == null)
            {
                throw new ArgumentNullException(nameof(dataEnrichmentFactory));
            }

            this.searchProvider = searchProvider;
            this.supplierRepository = supplierRepository;
            this.dataEnrichmentFactory = dataEnrichmentFactory;
        }

        /// <summary>
        /// performs a search using the passed request
        /// </summary>
        /// <param name="request">must not ne null, the request to search for</param>
        /// <returns>a list of rooms available for the request</returns>
        public List<AriaContractsAvailabilityResponse> Search(AvailabilityRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            List<RawAvailability> rawResults = this.GetAvailability(request);

            var availabilityConverter = new AvailabilityResultGeneration(this.supplierRepository);
            List<AriaContractsAvailabilityResponse> availabilityResults = availabilityConverter.ConvertAvailability(rawResults);

            return availabilityResults;
        }

        private List<RawAvailability> GetAvailability(AvailabilityRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            // search the DB
            List<RawAvailability> rawResults = this.searchProvider.Search(request);

            IEnrichData[] dataEnrichments = this.dataEnrichmentFactory.Create(request);

            // correct data, maybe reverse these, think about Cache friendliness?
            for (int i = 0; i < rawResults.Count; i++)
            {
                for (int j = 0; j < dataEnrichments.Length; j++)
                {
                    dataEnrichments[j].Enrich(rawResults[i]);
                }
            }

            return rawResults;
        }
    }
}