﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Helper;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.BookingAdjustments
{
    using log4net;
    using System;
    using System.Threading;

    /// <summary>
    /// Responsible for keeping a value to do date by using regular updates on a timed bases.
    /// If thread safe, will be called by multiple threads at the same time
    /// </summary>
    /// <typeparam name="T">the value to cache, must be a class, can not be null</typeparam>
    internal class RefreshedValue<T> : RefreshedValue where T : class
    {
        private readonly Func<T> refreshFunc;
        private readonly Timer timer;

        private T value;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="refresh">the func to call to refresh the value, will only be called by one thread at once, can not be null</param>
        /// <param name="updateFrequency">the timespan between updates</param>
        public RefreshedValue(Func<T> refresh, TimeSpan updateFrequency)
        {
            if (refresh == null)
            {
                throw new ArgumentNullException(nameof(refresh));
            }

            this.refreshFunc = refresh;

            // The weak ref is to pass into the timer. If the timer has a strong reffernce it will prevent this object from being GC if the client forgets to dispose of us
            var weakRef = new WeakReference<RefreshedValue>(this);

            this.timer = new Timer(
                RefreshedValue.TimerCallback,
                weakRef,
                0, // refreshes the data immediately so it is there for the first caller
                updateFrequency.Milliseconds);
        }

        public T GetValue()
        {
            if (this.IsDisposed)
            {
                throw new ObjectDisposedException(nameof(RefreshedValue<T>));
            }

            T localValue = this.value;

            // this can happen if refresh has failed or not yet completed
            if (localValue == null)
            {
                lock (this.timer)
                {
                    // can use this.value as it is locked and can not be changed
                    if (this.value == null)
                    {
                        // if an exception is thrown by the refresh method it will be thrown here as well
                        this.Refresh();
                    }

                    localValue = this.value;
                }
            }

            return localValue;
        }

        protected sealed override void Refresh()
        {
            lock (this.timer)
            {
                try
                {
                    T refreshedValue = this.refreshFunc();

                    if (refreshedValue == null)
                    {
                        throw new InvalidOperationException("refresh function can not return null.");
                    }

                    this.value = refreshedValue;
                }
                catch (Exception ex)
                {
                    // set this to null so the next thread will try to refresh
                    this.value = null;
                    RefreshedValue.Log.Error("Error refreshing value", ex);

                    throw;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.timer != null)
                {
                    this.timer.Dispose();
                }
            }

            base.Dispose(disposing);
        }
    }

    internal abstract class RefreshedValue : Disposable
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(RefreshedValue));

        protected abstract void Refresh();

        // used by the timer thread
        protected static void TimerCallback(object state)
        {
            try
            {
                var weakRef = (WeakReference<RefreshedValue>)state;

                RefreshedValue refreshedValue;
                if (weakRef.TryGetTarget(out refreshedValue))
                {
                    refreshedValue.Refresh();
                }
            }
            catch (Exception ex)
            {
                // do not throw on the timers thread
                RefreshedValue.Log.Error("Exception from the timers thread", ex);
            }
        }
    }
}