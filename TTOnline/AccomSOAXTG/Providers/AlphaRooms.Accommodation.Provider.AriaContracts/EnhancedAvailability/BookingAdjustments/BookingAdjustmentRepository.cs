﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.BookingAdjustments
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Responsible for providing the correct BookingAdjustments.
    /// This Thread safe, needs to be thread safe will get used by multiple threads at the same time  
    /// </summary>
    internal class BookingAdjustmentRepository
    {
        // this is thread safe as it is only ever read from
        private readonly Dictionary<Guid, BookingAdjustment[]> bookingAdjustments;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="bookingAdjustments">collection that can be searched for BookingAdjustments, key is room key</param>
        internal BookingAdjustmentRepository(Dictionary<Guid, BookingAdjustment[]> bookingAdjustments)
        {
            if(bookingAdjustments == null)
            {
                throw new ArgumentNullException(nameof(bookingAdjustments));
            }

            this.bookingAdjustments = bookingAdjustments;
        }

        /// <summary>
        /// Gets the booking adjustments for the past data
        /// </summary>
        /// <param name="roomId">The id of the room to get adjustments for</param>
        /// <param name="arrivalDate">the date the customer arrives</param>
        /// <param name="departureDate">the date the cusomer leaves</param>
        /// <returns>the BookingAdjustment needed for</returns>
        public IEnumerable<BookingAdjustment> Get(Guid roomId, DateTime arrivalDate, DateTime departureDate)
        {
            IEnumerable<BookingAdjustment> bookingAdjustment;

            BookingAdjustment[] roomAdjustments;
            if (this.bookingAdjustments.TryGetValue(roomId, out roomAdjustments))
            {
                //EndDate >= @dtmArrival
                //AND
                //StartDate < @dtmDeparture
                bookingAdjustment = roomAdjustments.Where(x => x.EndDate >= arrivalDate && x.StartDate < departureDate);
            }
            else
            {
                bookingAdjustment = Enumerable.Empty<BookingAdjustment>();
            }

            return bookingAdjustment;
        }
    }
}
