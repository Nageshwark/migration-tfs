﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.BookingAdjustments
{
    using Helper;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    /// <summary>
    /// Responsible for creating BookingAdjustmentRepository's
    /// If thread safe
    /// </summary>
    internal class BookingAdjustmentRepositoryFactory
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(BookingAdjustmentRepositoryFactory));

        private readonly string connectionString;

        public BookingAdjustmentRepositoryFactory(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Can not be null or empty.", nameof(connectionString));
            }

            this.connectionString = connectionString;
        }

        public BookingAdjustmentRepository Create()
        {
            Dictionary<Guid, BookingAdjustment[]> allBookingAdjustments = BookingAdjustmentRepositoryFactory.GetAllBookingAdjustments(this.connectionString);
            BookingAdjustmentRepository bookingAdjustmentRepository = new BookingAdjustmentRepository(allBookingAdjustments);
            return bookingAdjustmentRepository;
        }

        /// <summary>
        /// Uses the database to read the current BookingAdjustment
        /// </summary>
        /// <param name="connectionString">the database to use</param>
        /// <returns>correctly indexed BookingAdjustment for all rooms with current and future adjustments</returns>
        private static Dictionary<Guid, BookingAdjustment[]> GetAllBookingAdjustments(string connectionString)
        {
            Dictionary<Guid, BookingAdjustment[]> allBookingAdjustments = new Dictionary<Guid, BookingAdjustment[]>();

            DateTime startTime = DateTime.UtcNow;
            int numberOfAdjustments = 0;

            SqlParameter[] parm = new SqlParameter[]
            {
                // only get current and future adjustments
                new SqlParameter("@dtmEndAfter", DateTime.UtcNow.Date)
            };

            Guid? currentRoomId = null;
            List<BookingAdjustment> currentRoomAdjustments = new List<BookingAdjustment>();

            using (SqlDataReader dataReader = SqlHelper.ExecuteReader(connectionString, "[GetBookingAdjustments]", parm))
            {
                while (dataReader.Read())
                {
                    ++numberOfAdjustments;

                    // read by index to make it fast

                    Guid roomId = (Guid)dataReader[0];
                    DateTime startDate = (DateTime)dataReader[1];
                    DateTime endDate = (DateTime)dataReader[2];

                    decimal value = (decimal)dataReader[3];

                    bool isPerNight = (bool)dataReader[4];
                    bool isPerPerson = (bool)dataReader[5];

                    bool adult;
                    bool youth;
                    bool child;
                    bool infant;

                    bool isRO;
                    bool isSC;
                    bool isBB;
                    bool isHB;
                    bool isFB;
                    bool isAI;

                    int maxAgeYouth;
                    int maxAgeChild;
                    int maxAgeInfant;

                    if (isPerPerson)
                    {
                        adult = (bool)dataReader[6];
                        youth = (bool)dataReader[7];
                        child = (bool)dataReader[8];
                        infant = (bool)dataReader[9];

                        isRO = (bool)dataReader[10];
                        isSC = (bool)dataReader[11];
                        isBB = (bool)dataReader[12];
                        isHB = (bool)dataReader[13];
                        isFB = (bool)dataReader[14];
                        isAI = (bool)dataReader[15];

                        maxAgeYouth = (int)dataReader[16];
                        maxAgeChild = (int)dataReader[17];
                        maxAgeInfant = (int)dataReader[18];
                    }
                    else
                    {
                        // if the row is not PerPerson these details are never used so don't even bother read them
                        isRO = false;
                        isSC = false;
                        isBB = false;
                        isHB = false;
                        isFB = false;
                        isAI = false;

                        maxAgeYouth = 0;
                        maxAgeChild = 0;
                        maxAgeInfant = 0;

                        adult = false;
                        youth = false;
                        child = false;
                        infant = false;
                    }

                    BookingAdjustment bookingAdjustment = new BookingAdjustment(
                        startDate,
                        endDate,
                        isPerNight,
                        isPerPerson,
                        value,
                        maxAgeYouth,
                        maxAgeChild,
                        maxAgeInfant,
                        isRO,
                        isSC,
                        isBB,
                        isHB,
                        isFB,
                        isAI,
                        adult,
                        youth,
                        child,
                        infant);

                    if (!currentRoomId.HasValue)
                    {
                        // for the first row
                        currentRoomId = roomId;
                    }
                    else if (currentRoomId.Value != roomId)
                    {
                        // is a new room, save add the old rooms adjustments to the dictionary
                        allBookingAdjustments.Add(currentRoomId.Value, currentRoomAdjustments.ToArray());
                        currentRoomId = roomId;
                        currentRoomAdjustments.Clear();
                    }

                    currentRoomAdjustments.Add(bookingAdjustment);
                }
            }

            // the left overs that have not been added to the dictionary
            if (currentRoomAdjustments.Count > 0)
            {
                allBookingAdjustments.Add(currentRoomId.Value, currentRoomAdjustments.ToArray());
            }

            if (numberOfAdjustments > 50000)
            {
                BookingAdjustmentRepositoryFactory.Log.Warn($"A code BookingAdjustment have grown by 10x the expected level. Check memory usage. Read: {numberOfAdjustments} rows in {DateTime.UtcNow - startTime}");
            }
            else if (numberOfAdjustments > 11000)
            {
                BookingAdjustmentRepositoryFactory.Log.Info($"A code BookingAdjustment have grown by 2x the expected level. Check memory usage. Read: {numberOfAdjustments} rows in {DateTime.UtcNow - startTime}");
            }
            else
            {
                BookingAdjustmentRepositoryFactory.Log.Debug($"Loaded All Booking Adjustments successfully in {numberOfAdjustments} rows in {DateTime.UtcNow - startTime}");
            }

            return allBookingAdjustments;
        }
    }
}