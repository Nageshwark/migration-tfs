﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.BookingAdjustments
{
    using System;

    /// <summary>
    /// Read-only immutable this is the information needed to add adjustments to the room prices.
    /// </summary>
    internal class BookingAdjustment
    {
        private readonly DateTime startDate;
        private readonly DateTime endDate;
        private readonly bool isPerNight;
        private readonly bool isPerPerson;
        private readonly decimal value;
        private readonly int maxAgeYouth;
        private readonly int maxAgeChild;
        private readonly int maxAgeInfant;
        private readonly bool isRO;
        private readonly bool isSC;
        private readonly bool isBB;
        private readonly bool isHB;
        private readonly bool isFB;
        private readonly bool isAI;
        private readonly bool adult;
        private readonly bool youth;
        private readonly bool child;
        private readonly bool infant;

        public BookingAdjustment(
            DateTime startDate,
            DateTime endDate,
            bool isPerNight,
            bool isPerPerson,
            decimal value,
            int maxAgeYouth,
            int maxAgeChild,
            int maxAgeInfant,
            bool isRO,
            bool isSC,
            bool isBB,
            bool isHB,
            bool isFB,
            bool isAI,
            bool adult,
            bool youth,
            bool child,
            bool infant)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.isPerNight = isPerNight;
            this.isPerPerson = isPerPerson;
            this.value = value;
            this.maxAgeYouth = maxAgeYouth;
            this.maxAgeChild = maxAgeChild;
            this.maxAgeInfant = maxAgeInfant;
            this.isRO = isRO;
            this.isSC = isSC;
            this.isBB = isBB;
            this.isHB = isHB;
            this.isFB = isFB;
            this.isAI = isAI;
            this.adult = adult;
            this.youth = youth;
            this.child = child;
            this.infant = infant;
        }

        public DateTime StartDate { get { return this.startDate; } }
        public DateTime EndDate { get { return this.endDate; } }

        public bool IsPerNight { get { return this.isPerNight; } }

        public bool IsPerPerson { get { return this.isPerPerson; } }

        public decimal Value { get { return this.value; } }

        // max ages these are taken from the room type table, may need to break these out later
        public int MaxAgeYouth { get { return this.maxAgeYouth; } }
        public int MaxAgeChild { get { return this.maxAgeChild; } }
        public int MaxAgeInfant { get { return this.maxAgeInfant; } }



        // room types
        public bool IsRO { get { return this.isRO; } }
        public bool IsSC { get { return this.isSC; } }
        public bool IsBB { get { return this.isBB; } }
        public bool IsHB { get { return this.isHB; } }
        public bool IsFB { get { return this.isFB; } }
        public bool IsAI { get { return this.isAI; } }

        // for who
        public bool Adult { get { return this.adult; } }
        public bool Youth { get { return this.youth; } }
        public bool Child { get { return this.child; } }
        public bool Infant { get { return this.infant; } }
    }
}
