﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    using Contracts;
    using Interfaces;
    using Core.Provider.Contracts;
    using Core.Provider.Interfaces;

    public class AriaContractsBookingParser : IAccommodationBookingParserAsync<IEnumerable<AriaContractsBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAriaContractsRepository ariaContractsRepository;

        public AriaContractsBookingParser(ILogger logger, IAccommodationConfigurationManager configurationManager, IAriaContractsRepository ariaContractsRepository)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ariaContractsRepository = ariaContractsRepository;
        }

        public async Task<IEnumerable<AccommodationProviderBookingResult>> GetBookingResultsAsync(AccommodationProviderBookingRequest request, IEnumerable<AriaContractsBookingResponse> response)
        {

            var bookingResults = response.Select(r => new AccommodationProviderBookingResult()
            {
                BookingStatus = r.BookingStatus,
                ProviderBookingReference = r.BookingReference,
                RoomNumber = r.RoomNumber
            });

            return bookingResults;
        }
    }
}
