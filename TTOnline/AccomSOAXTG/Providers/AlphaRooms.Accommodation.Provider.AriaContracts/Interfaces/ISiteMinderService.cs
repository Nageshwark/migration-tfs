﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces
{
    public interface ISiteMinderService
    {
        Task<IEnumerable<AriaContractsBookingResponse>> ConfirmSiteMinderBooking(AccommodationProviderBookingRequest request, Dictionary<string, string> channelPushInfo, IAriaContractsRepository ariaContractsRepository);
    }
}
