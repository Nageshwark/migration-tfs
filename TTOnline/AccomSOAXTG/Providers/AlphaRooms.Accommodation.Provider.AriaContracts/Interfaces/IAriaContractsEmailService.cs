﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces
{
    public interface IAriaContractsEmailService
    {
        Task CreateProviderConfirmationEmail(AccommodationProviderBookingRequest request, List<AriaContractsBookingResponse> bookingResponses, IAccommodationProviderEmailTemplateRepository emailTemplateRepository, IAriaContractsRepository ariaContractsRepository, ISmtpClientService smtpClient);
    }
}
