﻿using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces
{
    public interface IAriaContractsCancellationPolicyService
    {
        Task<string> CreateLocalACodeConditionNonRefundableText(AccommodationProviderValuationRequest request);
        Task<string> CreateLocalACodeCancellationConditionDaysBeforeText(AccommodationProviderValuationRequest request, string days, string fee);
        Task<string> CreateLocalACodeConditionNoShowText(AccommodationProviderValuationRequest request, string fee);
    }
}
