﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces
{
    public interface IFaxService
    {
        void SendFaxSecure(string reference, string[] faxNumbers, byte[] filesData, string fileTypes, string fileSizes);
    }
}
