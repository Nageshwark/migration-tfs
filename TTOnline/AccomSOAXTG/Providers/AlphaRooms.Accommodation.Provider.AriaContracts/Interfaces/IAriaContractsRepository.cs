﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces
{
    public interface IAriaContractsRepository
    {
        //Task<IList<AriaContractsAvailabilityResponse>> GetLocalRoomsAsync(AccommodationProviderAvailabilityRequest request);
        //Task<AriaContractsValuationResponse> ValuateRoomAsync(AccommodationProviderValuationRequest request);
        Task<Supplier> GetSupplierByName(string name);
        Task<string> GetChannelPushInfo(string roomCode);
        Task<SiteMinderRoom> GetSiteMinderRoom(string roomCode);
        bool IsExtranetNetted(Guid roomId);
    }
}
