﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability
{
    using System.Collections.Generic;

    /// <summary>
    /// Responsible for searching for availability of Aria Contracts.
    /// Is thread safe
    /// </summary>
    public interface IAriaContractsAvailability
    {
        /// <summary>
        /// performs a search using the passed request
        /// </summary>
        /// <param name="request">must not ne null, the request to search for</param>
        /// <returns>a list of rooms available for the request</returns>
        List<AriaContractsAvailabilityResponse> Search(AvailabilityRequest request);
    }
}