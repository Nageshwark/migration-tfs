﻿using AlphaRooms.Accommodation.Provider.AriaContracts.Mapping;
using AlphaRooms.Utilities;
using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsAccommodationDbContext : DbContext
    {
        static AriaContractsAccommodationDbContext()
        {
            Database.SetInitializer<AriaContractsAccommodationDbContext>(null);
        }

        public AriaContractsAccommodationDbContext() : base("Name=AlphabedsEntities")
        {   
        }
        
        public DbSet<Supplier> Suppliers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SupplierMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task<string> GetChannelPushInfo(string roomCode)
        {
            var channelPushCode = (await this.ExecuteStoredProcedureAsync<ChannelPushRoom>("ChannelPushRoomCheckRoom"
                , new SqlParameter("RoomId", roomCode))).ToArray();

            var channelPushInfo = channelPushCode == null || channelPushCode.Length < 1 ? "None" : channelPushCode[0].code;
            return channelPushInfo;
        }

        public async Task<SiteMinderRoom> GetSiteMinderRoom(string roomCode)
        {
            var room = (await this.ExecuteStoredProcedureAsync<SiteMinderRoom>("SiteMinderCheckRoom"
                , new SqlParameter("roomID", roomCode))).ToArray();
         
            return room[0];
        }

        public bool IsExtranetNetted(Guid roomId)
        {
            var isNetted = this.ExecuteStoredProcedureAsync<bool>("IsExtranetNetted"
                , new SqlParameter("RoomId", roomId)).Result.ToArray();

            return isNetted[0];
        }
        
    }
}
