﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.Helper
{
    using System;

    internal static class StringExtensions
    {
        /// <summary>
        /// Same as standered string Contains, but allowing a comparisonType.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="comparisonType"></param>
        /// <returns></returns>
        public static bool Contains(this string obj, string value, StringComparison comparisonType)
        {
            /*
             * Source code is exact same as standard string compare copied from decompiled
             */

            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return obj.IndexOf(value, comparisonType) >= 0;
        }
    }
}
