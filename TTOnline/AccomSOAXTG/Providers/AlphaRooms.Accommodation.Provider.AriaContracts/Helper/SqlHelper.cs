﻿namespace AlphaRooms.Accommodation.Provider.AriaContracts.Availability.Helper
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Responsible for providing boilerplate raw SQL code.
    /// </summary>
    internal static class SqlHelper
    {
        /// <summary>
        /// Runs a Stored Procedure and returns the resulting SqlDataReader
        /// </summary>
        /// <param name="connectionString">the connection string for the DB</param>
        /// <param name="spName">the name of the SP</param>
        /// <param name="commandParameters">The Parms of the passed SP</param>
        /// <returns>MUST BE disposed of, the results data</returns>
        internal static SqlDataReader ExecuteReader(string connectionString, string spName, SqlParameter[] commandParameters)
        {
            if (String.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Can not be null or empty", nameof(connectionString));
            }

            if (String.IsNullOrEmpty(spName))
            {
                throw new ArgumentException("Can not be null or empty", nameof(spName));
            }

            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(connectionString);

                using (SqlCommand cmd = new SqlCommand(spName, sqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (commandParameters != null)
                    {
                        cmd.Parameters.AddRange(commandParameters);
                    }

                    sqlConnection.Open();

                    // CloseConnection is needed to ensure the connection is correctly returned to the connection pool
                    SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                    return result;
                }
            }
            catch (Exception)
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Dispose();
                }

                throw;
            }
        }
    }
}