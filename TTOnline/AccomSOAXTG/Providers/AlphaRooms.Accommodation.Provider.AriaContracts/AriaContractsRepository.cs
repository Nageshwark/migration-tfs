﻿using System;
using System.Data.Entity;
using System.Linq;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.Utilities.EntityFramework;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsRepository : IAriaContractsRepository 
    {
        private readonly IDbContextActivator<AriaContractsAccommodationDbContext> accommodationDbContext;
        private readonly IProviderLoggerService providerLoggerService;

        public AriaContractsRepository(IDbContextActivator<AriaContractsAccommodationDbContext> accommodationDbContext, IProviderLoggerService providerLoggerService)
        {
            this.accommodationDbContext = accommodationDbContext;
            this.providerLoggerService = providerLoggerService;
        }

        public Task<Supplier> GetSupplierByName(string name)
        {
            var context = this.accommodationDbContext.GetDbContext();

            return context.Suppliers.Where(x => x.Name == name).FirstOrDefaultAsync();
        }

        public async Task<string> GetChannelPushInfo(string roomCode)
        {
            return await accommodationDbContext.GetDbContext().GetChannelPushInfo(roomCode);
        }

        public async Task<SiteMinderRoom> GetSiteMinderRoom(string roomCode)
        {
            return await accommodationDbContext.GetDbContext().GetSiteMinderRoom(roomCode);
        }

        public bool IsExtranetNetted(Guid roomId)
        {
            return accommodationDbContext.GetDbContext().IsExtranetNetted(roomId);
        }
    }
}
