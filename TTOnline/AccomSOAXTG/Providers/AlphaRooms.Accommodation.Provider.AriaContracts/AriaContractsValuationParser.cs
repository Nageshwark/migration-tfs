﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsValuationParser : IAccommodationValuationParserAsync<IEnumerable<AriaContractsValuationResponse>>
    {
        private readonly ILogger logger;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IAriaContractsCancellationPolicyService cancellationPolicyService;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public AriaContractsValuationParser(ILogger logger, IProviderOutputLogger outputLogger, IAriaContractsCancellationPolicyService cancellationPolicyService, IEstablishmentDetailsRepository establishmentDetailsRepository)
        {
            this.logger = logger;
            this.outputLogger = outputLogger;
            this.cancellationPolicyService = cancellationPolicyService;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<IEnumerable<AccommodationProviderValuationResult>> GetValuationResultsAsync(AccommodationProviderValuationRequest request, IEnumerable<AriaContractsValuationResponse> response)
        {
            AccommodationProviderValuationResult[] result;

            try
            {
                var tasks = response.Where(r => r.IsValidRoom).Select(r => GetProviderValuationResultAsync(r, request));

                result = await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                string message =
                    $"Aria Contracts Supplier Data exception in Valuation Parser: {Environment.NewLine} Exception Message: {Environment.NewLine}{ex.Message}{Environment.NewLine} Stack Trace: {Environment.NewLine}{ex.StackTrace}";
                throw new SupplierApiDataException(message);
            }

            return result;
        }

        private async Task<AccommodationProviderValuationResult> GetProviderValuationResultAsync(AriaContractsValuationResponse response, AccommodationProviderValuationRequest request)
        {
            var cancelationPolicies = new List<string>();

            var selectedRoom = request.SelectedRooms.FirstOrDefault(x => x.RoomNumber == response.RoomNumber);
            var destinationId = Guid.Parse(selectedRoom.AvailabilityRequest.DestinationCodes[0]);

            if (!string.IsNullOrEmpty(response.CancellationPolicy))
            {

                var splits = response.CancellationPolicy.Split(new[] { "==" }, StringSplitOptions.None);

                if (splits.Length == 3)
                {
                    if (splits[0].ToLower() == "true")
                    {
                        cancelationPolicies.Add(await this.cancellationPolicyService.CreateLocalACodeConditionNonRefundableText(request));
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(splits[1]))
                        {
                            foreach (var t in splits[1].Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                var x = t.Split(new[] { '|' });
                                cancelationPolicies.Add(await this.cancellationPolicyService.CreateLocalACodeCancellationConditionDaysBeforeText(request, x[0], x[1]));
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(splits[2]))
                        {
                            cancelationPolicies.Add(await this.cancellationPolicyService.CreateLocalACodeConditionNoShowText(request, splits[2].Trim()));
                        }
                    }
                }
            }

            // Get establishment name
            var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(destinationId, request.ChannelInfo.Channel);
            var establishmentId = Guid.Parse(response.EstablishmentEdiCode);
            var establishmentName = establishmentDetails[establishmentId].EstablishmentName;

            var providerValuationResult = new AccommodationProviderValuationResult()
            {
                RoomNumber = response.RoomNumber,
                ProviderName = selectedRoom?.AvailabilityResult?.ProviderName,
                ProviderEdiCode = response.ProviderEdiCode,
                SupplierEdiCode = selectedRoom?.AvailabilityResult?.SupplierEdiCode,
                EstablishmentEdiCode = response.EstablishmentEdiCode,
                EstablishmentName = establishmentName, //response.EstablishmentName,
                Adults = response.Adults,
                Children = response.Children,
                Infants = response.Infants,
                CheckInDate = response.CheckInDate,
                CheckOutDate = response.CheckOutDate,
                BoardCode = response.BoardCode,
                BoardDescription = response.BoardDescription,
                RoomCode = response.RoomCode,
                RoomDescription = response.RoomDescription,
                SalePrice = response.SalePrice,
                CostPrice = response.CostPrice,
                PaymentModel = response.PaymentModel,
                IsBindingRate = response.IsBindingRate,
                IsOpaqueRate = response.IsOpaqueRate,
                IsNonRefundable = response.IsNonRefundable != null && response.IsNonRefundable.Value,
                RateType = response.RateType,
                CancellationPolicy = cancelationPolicies.ToArray()
            };

            return providerValuationResult;
        }
    }
}
