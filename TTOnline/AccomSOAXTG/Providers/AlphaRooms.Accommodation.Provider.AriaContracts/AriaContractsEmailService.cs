﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsEmailService : IAriaContractsEmailService
    {
        readonly IFaxService FaxService;

        private string toEmailAddress = string.Empty;
        private const string contractAdminTo = "a@alpharooms.com";
        private const string contractAdminFromPart = "suppliers@";
        private const string contractAdminQueries = "directcontractqueries@alpharooms.com";

        public AriaContractsEmailService(IFaxService faxService)
        {
            this.FaxService = faxService;
        }
        public async Task CreateProviderConfirmationEmail(AccommodationProviderBookingRequest request, List<AriaContractsBookingResponse> bookingResponses, IAccommodationProviderEmailTemplateRepository emailTemplateRepository, IAriaContractsRepository ariaContractsRepository, ISmtpClientService smtpClient)
        {
            var room = request.ValuatedRooms[0];
            var accommodation = new AriaContractsAccommodation()
            {
                CheckinDate = room.AvailabilityRequest.CheckInDate,
                CheckoutDate = room.AvailabilityRequest.CheckOutDate,
                EstablishmentId = new Guid(room.ValuationResult.EstablishmentEdiCode),
                EstablishmentName = room.ValuationResult.EstablishmentName,
                AccommodationRooms = new List<AriaContractsAccommodationRoom>()
            };

            for (int index = 0; index < request.ValuatedRooms.Length; index++)
            {
                var valuatedRoom = request.ValuatedRooms[index];
                var specialReq = String.Empty;

                if (valuatedRoom.SpecialRequests != null)
                {
                    List<string> specialRequests = new List<string>();

                    if (valuatedRoom.SpecialRequests.LateArrival)
                        specialRequests.Add("Late Arrival");
                    if (valuatedRoom.SpecialRequests.CotRequired)
                        specialRequests.Add("Cot required");
                    if (valuatedRoom.SpecialRequests.SeaViews)
                        specialRequests.Add("Sea views");
                    if (valuatedRoom.SpecialRequests.AdjoiningRooms)
                        specialRequests.Add("Adjoining rooms");
                    if (valuatedRoom.SpecialRequests.NonSmoking)
                        specialRequests.Add("Non smoking");
                    if (valuatedRoom.SpecialRequests.DisabledAccess)
                        specialRequests.Add("Disabled access");
                    if (!string.IsNullOrWhiteSpace(valuatedRoom.SpecialRequests.OtherRequests))
                        specialRequests.Add(valuatedRoom.SpecialRequests.OtherRequests);
                    specialReq = string.Join(", ", specialRequests);
                }

                var accommodationRoom = new AriaContractsAccommodationRoom()
                {
                    BoardDescription = valuatedRoom.ValuationResult.BoardDescription,
                    CostFromSupplier = valuatedRoom.ValuationResult.CostPrice,
                    NumberOfAdults = valuatedRoom.ValuationResult.Adults,
                    ChildrenAges = valuatedRoom.Guests.ChildAndInfantAges,
                    PaymentMethod = valuatedRoom.ValuationResult.PaymentModel,
                    RoomDescription = valuatedRoom.ValuationResult.RoomDescription,
                    SpecialRequest = specialReq,
                    SupplierBookingReference = bookingResponses[index].BookingReference,
                    SupplierCode = request.Provider.EdiCode,
                    SupplierName = valuatedRoom.ValuationResult.ProviderName,
                    VccPaymentTerms = valuatedRoom.ValuationResult.VirtualCreditCardPaymentTerms
                };

                accommodation.AccommodationRooms.Add(accommodationRoom);
            }

            var leadPassenger = request.Customer;

            var providerEmail = new AriaContractsProviderEmail()
            {
                BookingDate = DateTime.Now,
                InvoiceId = request.ItineraryId,
                ItineraryReference = request.ItineraryReference,
                LeadPassenger = leadPassenger.FirstName + " " + leadPassenger.Surname,
                Site = GetSite(request.ChannelInfo.Site),
                Accommodation = accommodation
            };

            await SendProviderConfirmationEmailAndFax(providerEmail, request.Provider.Id, emailTemplateRepository, ariaContractsRepository, smtpClient);

        }

        private string GetSite(Sites site)
        {
            var result = "";

            switch (site)
            {
                case Sites.AlphaRooms:
                    result = "Alpharooms.com";
                    break;

                case Sites.Holexa:
                    result = "holexa.com";
                    break;

                case Sites.TotalStayHost2Host:
                    result = "totalstay.vacenza.com";
                    break;

                case Sites.Vacenza:
                    result = "vacenza.com";
                    break;

                case Sites.BetaBeds:
                    result = "BetaBeds.com";
                    break;
            }
            return result;
        }

        private async Task SendProviderConfirmationEmailAndFax(AriaContractsProviderEmail providerEmail, int providerId, IAccommodationProviderEmailTemplateRepository emailTemplateRepository, IAriaContractsRepository ariaContractsRepository, ISmtpClientService smtpClient)
        {
            var emailTemplate = (await emailTemplateRepository.GetByProviderIdAndTemplateTypeIdAsync(providerId, 1)).ToArray(); // template type id = 1 is for booking confirmation email
            var emailRoomBody = emailTemplate[0].RoomBody;
            var emailBody = emailTemplate[0].Body;

            string site = providerEmail.Site.ToLower();

            var supplierrooms = providerEmail.Accommodation.AccommodationRooms
                                        .Where(t => t.SupplierCode == "A")
                                        .GroupBy(t => t.SupplierName);

            foreach (var booking in supplierrooms)
            {
                var supplierName = booking.Key;
                var counter = 1;
                decimal totalCost = 0M;
                string currency = "";

                var bodyHtml = new StringBuilder();
                var roomBodyHtml = new StringBuilder();
                bodyHtml.Append(emailBody);
                bodyHtml.Replace("##fromSite##", site);
                bodyHtml.Replace("##SupplierBookingReference##", booking.First().SupplierBookingReference);
                bodyHtml.Replace("##suppliername##", supplierName);
                bodyHtml.Replace("##InvoiceId##", providerEmail.InvoiceId.ToString());
                bodyHtml.Replace("##ItineraryReference##", providerEmail.ItineraryReference);
                bodyHtml.Replace("##BookingDate##", providerEmail.BookingDate.ToString("dd MMM yyyy HH:mm"));
                bodyHtml.Replace("##establishment.Name##", providerEmail.Accommodation.EstablishmentName);
                bodyHtml.Replace("##LeadPassenger##", providerEmail.LeadPassenger);

                foreach (var room in booking)
                {
                    var roomBody = new StringBuilder();

                    if (!string.IsNullOrEmpty(emailRoomBody))
                    {
                        roomBody.Append(emailRoomBody);
                        roomBody.Replace("##counter##", counter.ToString());
                        roomBody.Replace("##RoomDescription##", room.RoomDescription);
                        roomBody.Replace("##BoardDescription##", room.BoardDescription);
                        roomBody.Replace("##SupplierBookingReference##", room.SupplierBookingReference);
                        roomBody.Replace("##CheckinDate##", providerEmail.Accommodation.CheckinDate.ToString("dd MMM yyyy HH:mm"));
                        roomBody.Replace("##NumberOfNights##", providerEmail.Accommodation.CheckoutDate.Subtract(providerEmail.Accommodation.CheckinDate).Days.ToString());
                        roomBody.Replace("##NumberOfAdults##", room.NumberOfAdults.ToString());
                        roomBody.Replace("##ChildrenCount##", room.ChildrenAges.Length.ToString());
                        roomBody.Replace("##CHILDREN DETAILS##", room.ChildrenAges.Length > 0 ? "(" + string.Join(",", room.ChildrenAges.Select(t => t.ToString())) + ")" : "");
                        roomBody.Replace("##SpecialRequest##", room.SpecialRequest);
                        roomBody.Replace("##CostFromSupplier.Value##", room.CostFromSupplier.Amount.ToString());
                        roomBody.Replace("##CostFromSupplier.Currency##", room.CostFromSupplier.CurrencyCode);
                        roomBody.Replace("##PaymentTerm##", GetPaymentTerm(room, site));

                        counter++;
                        totalCost += room.CostFromSupplier.Amount;
                        if (string.IsNullOrEmpty(currency)) currency = room.CostFromSupplier.CurrencyCode;

                    }

                    roomBodyHtml.Append(roomBody);
                }

                bodyHtml.Replace("##ROOM BODY##", roomBodyHtml.ToString());
                bodyHtml.Replace("##totalCost##", totalCost.ToString());
                bodyHtml.Replace("##currency##", currency);

                var supplier = ariaContractsRepository.GetSupplierByName(supplierName).Result;

                var emailAddress = string.IsNullOrEmpty(toEmailAddress) ? supplier.ReservationsEmail : toEmailAddress;

                var tos = new List<string>(emailAddress.ToLower().Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries)) { contractAdminTo };

                var message = new MailMessage
                {
                    From = new MailAddress(contractAdminFromPart + site),
                    Subject = site + " reservation invoiceid " + providerEmail.InvoiceId,
                    IsBodyHtml = true,
                    Body = bodyHtml.ToString()
                };

                message.To.Add(string.Join(",", tos.Distinct()));
                smtpClient.SendEmail(message);

                //Send fax if the fax numbers are provided
                if (!string.IsNullOrEmpty(supplier.FaxNumbers))
                {
                  SendFax(providerEmail,booking.ToList(), bodyHtml.ToString(), providerEmail.Accommodation.EstablishmentName,supplier.FaxNumbers,smtpClient);  
                }
            }
        }

        void SendFax(AriaContractsProviderEmail itinerary, IEnumerable<AriaContractsAccommodationRoom> rooms, string message, string establishmentName, string faxNumbers, ISmtpClientService smptpClient)
        {
            var payOnBookingRooms = rooms.Where(t => t.PaymentMethod == PaymentModel.PayOnBooking);
            var customerPayDirectRooms = rooms.Where(t => t.PaymentMethod == PaymentModel.CustomerPayDirect);

            if (payOnBookingRooms.Any() || customerPayDirectRooms.Any())
            {
                try
                {
                    if (itinerary.HasPaymentInformation == false)
                        throw new ArgumentException("No Customer Payment card details information found.");

                    if (faxNumbers == null)
                        throw new ArgumentNullException("No supplier fax number available.");

                    byte[] bytes1 = GetBytes(message);
                    byte[] bytes2 = new byte[0];
                    byte[] bytes3 = new byte[0];

                    string filesSize = bytes1.Length.ToString();
                    string filesType = "HTML";

                    if (payOnBookingRooms.Any())
                    {
                        bytes2 = FaxAttachment(itinerary, payOnBookingRooms, establishmentName, "Customer Card Details");
                        filesSize += (";" + bytes2.Length);
                        filesType += ";HTML";
                    }

                    if (customerPayDirectRooms.Any())
                    {
                        bytes3 = FaxAttachment(itinerary, customerPayDirectRooms, establishmentName, "Customer Card Details For Reference Purpose Only");
                        filesSize += (";" + bytes3.Length);
                        filesType += ";HTML";
                    }

                    byte[] data = new byte[bytes1.Length + bytes2.Length + bytes3.Length];
                    Buffer.BlockCopy(bytes1, 0, data, 0, bytes1.Length);
                    Buffer.BlockCopy(bytes2, 0, data, bytes1.Length, bytes2.Length);
                    Buffer.BlockCopy(bytes3, 0, data, bytes1.Length + bytes2.Length, bytes3.Length);

                    var faxNumbersArr = faxNumbers.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);

                    this.FaxService.SendFaxSecure(itinerary.ItineraryReference, faxNumbersArr, data, filesType, filesSize);
                }
                catch (Exception ex)
                {
                    SendFaxErrorEmail(itinerary, ex.Message, smptpClient);
                }
            }
        }
        private string GetPaymentTerm(AriaContractsAccommodationRoom room, string fromSite)
        {
            if (room.PaymentMethod == PaymentModel.PostPayment) return String.Format("Please invoice to {0}", fromSite);
            if (room.PaymentMethod == PaymentModel.PayOnBooking) return "Customer card details are supplied with fax for payment.";
            if (room.PaymentMethod == PaymentModel.CustomerPayDirect) return "Customer will pay directly to hotel, customer card details are supplied with fax for reference purpose only.";
            if (room.PaymentMethod == PaymentModel.PayWithVirtualCard)
            {
                var terms = new List<string>();

                if (room.VccPaymentTerms != null)
                {
                    terms.AddRange(room.VccPaymentTerms.Select(vccPayment => $"Card for amount {vccPayment.Price.Amount:0.00} {vccPayment.Price.CurrencyCode} on {vccPayment.PaymentDate:dd MMM yyyy}"));
                }

                if (terms.Any())
                    return "Virtual credit card details will be available on Extranet as scheduled below, please login <a href='http://supplierextranet.alpharooms.com/' target='_blank'>here</a> to retrieve payment details later.<br />"
                        + string.Join("<br />", terms);
                else
                    return "Virtual credit card details will be shortly available on Extranet, please login <a href='http://supplierextranet.alpharooms.com/' target='_blank'>here</a> to retrieve payment details later.";
            }
            return "";
        }

        byte[] FaxAttachment(AriaContractsProviderEmail itinerary, IEnumerable<AriaContractsAccommodationRoom> rooms, string establishmentName, string description)
        {
            var roomcontent = new StringBuilder();
            foreach (var room in rooms)
            {
                roomcontent.AppendFormat("<tr><td>\"{0}\",</td><td>\"{1}\",</td><td>{2},</td><td>{3}{4}</td></tr>",
                    room.RoomDescription,
                    room.BoardDescription,
                    room.NumberOfAdults,
                    room.ChildrenAges.Length,
                    room.ChildrenAges.Length > 0 ? string.Format(" ({0})", string.Join(",", room.ChildrenAges.Select(t => t.ToString()))) : "");
            }

            var content = new StringBuilder();
            content.Append("<!DOCTYPE html><html><body>");
            content.AppendFormat("<h3>{0}</h3>", description);
            content.Append("<table>");
            content.AppendFormat("<tr><td>Itinerary:</td><td>{0}</td></tr>", itinerary.ItineraryReference);
            content.AppendFormat("<tr><td>Hotel:</td><td>{0}</td></tr>", establishmentName);
            content.AppendFormat("<tr><td>Card Holder Name:</td><td>{0}</td></tr>", itinerary.CardHolderName);
            content.AppendFormat("<tr><td>Card Number:</td><td>{0}</td></tr>", itinerary.CardNumber);
            content.AppendFormat("<tr><td>Card Expiry Date:</td><td>{0:MM yyyy}</td></tr>", itinerary.ExpiryDate);
            content.AppendFormat("<tr><td>Card Security Code:</td><td>{0}</td></tr>", itinerary.CVV);
            content.AppendFormat("<tr><td>Amount:</td><td>{0:0.00} {1}</td></tr>", rooms.Sum(t => t.CostFromSupplier.Amount), rooms.First().CostFromSupplier.CurrencyCode);
            content.AppendFormat("<tr><td>For Rooms:</td><td><table>{0}<table></td></tr>", roomcontent);
            content.Append("</table></body></html>");

            return GetBytes(content.ToString());
        }

        byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * 2];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        
        void SendFaxErrorEmail(AriaContractsProviderEmail accommodation, string error, ISmtpClientService smtpClient)
        {
            var fromSite = accommodation.Site.ToLower();

            var mail = new MailMessage();
            mail.From = new MailAddress(contractAdminFromPart + fromSite);
            mail.To.Add(contractAdminQueries);
            mail.IsBodyHtml = true;
            mail.Subject = "Send Fax failed for " + fromSite + " reservation invoiceid " + accommodation.InvoiceId;
            mail.Body = string.Format("Unable to send fax with customer card details for {0} reservation invoceid {1} booking reference {2}<br/>Reason - {3}<br/><br/>Though booking confirmation email sent to supplier.",
                                    fromSite, accommodation.InvoiceId, accommodation.ItineraryReference, error);

            smtpClient.SendEmail(mail);
            
        }
    }
}
