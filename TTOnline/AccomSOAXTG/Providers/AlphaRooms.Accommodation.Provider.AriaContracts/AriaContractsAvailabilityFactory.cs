﻿

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    using Availability;
    using Availability.BookingAdjustments;
    using Availability.DataEnrichment;
    using Helper;
    using Availability.Searching;
    using Availability.SearchProvider;
    using Availability.SupplierDetails;
    using System;

    /// <summary>
    /// Responsible for creating IAriaContractsAvailability instances.
    /// This is threadsafe, will be called by multiple threads.
    /// This is to side step the fact of no DI in Bev5 and allow minimum dependences for portability.
    /// </summary>
    public class AriaContractsAvailabilityFactory : Disposable
    {
        private readonly string connectionString;
        private readonly DatabaseSearchProvider searchProvider;
        // This is disposable
        private readonly RefreshedValue<BookingAdjustmentRepository> refreshedBookingAdjustmentRepository;

        // Lazy load this to avoid throwing in ctor. Do not use Lazy<T> has it will cache failures in creation instead of retrying which we would prefer
        private ISupplierDetailRepository supplierRepository;

        /// <summary>
        /// ctor creates a new instance of of this factory
        /// </summary>
        /// <param name="connectionString">the database to use</param>
        /// <param name="bookingAdjustmentsRefreshRate">the amount of time between updates for the booking adjustments</param>
        public AriaContractsAvailabilityFactory(string connectionString, TimeSpan bookingAdjustmentsRefreshRate)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Can not be null or empty.", nameof(connectionString));
            }

            this.connectionString = connectionString;

            // these will not throw in there constructor so construct here
            BookingAdjustmentRepositoryFactory bookingAdjustmentRepositoryFactory = new BookingAdjustmentRepositoryFactory(connectionString);
            this.refreshedBookingAdjustmentRepository = new RefreshedValue<BookingAdjustmentRepository>(bookingAdjustmentRepositoryFactory.Create, bookingAdjustmentsRefreshRate);
            this.searchProvider = new DatabaseSearchProvider(this.connectionString);
        }

        public IAriaContractsAvailability Create()
        {
            if (this.IsDisposed)
            {
                throw new ObjectDisposedException(nameof(AriaContractsAvailabilityFactory));
            }

            // should only be null if never called before
            if (this.supplierRepository == null)
            {
                lock (this.connectionString)
                {
                    if (this.supplierRepository == null)
                    {
                        this.supplierRepository = this.GetSupplierDetails();
                    }
                }
            }

            // this can throw, get the lastest BookingAdjustmentRepository for this request, this allows multi-room searches to use the one AriaContractsAvailability with the same BookingAdjustment data
            BookingAdjustmentRepository bookingAdjustmentRepository = this.refreshedBookingAdjustmentRepository.GetValue();
            EnrichDataFactory dataEnrichmentFactory = new EnrichDataFactory(bookingAdjustmentRepository);

            AriaContractsAvailability availability = new AriaContractsAvailability(
                this.searchProvider,
                this.supplierRepository,
                dataEnrichmentFactory);

            return availability;
        }

        private ISupplierDetailRepository GetSupplierDetails()
        {
            SupplierDetailRepositoryFactory repositoryFactory = new SupplierDetailRepositoryFactory();
            // this uses the DB so can throw
            ISupplierDetailRepository supplierRepository = repositoryFactory.Create(this.connectionString);
            return supplierRepository;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.refreshedBookingAdjustmentRepository != null)
                {
                    // tidy up
                    this.refreshedBookingAdjustmentRepository.Dispose();
                }
            }

            base.Dispose(disposing);
        }
    }
}
