﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Provider;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<IEnumerable<AriaContractsValuationResponse>> automator;
        private readonly IAccommodationValuationParserAsync<IEnumerable<AriaContractsValuationResponse>> parser;

        public AriaContractsValuationProvider(IAccommodationValuationAutomatorAsync<IEnumerable<AriaContractsValuationResponse>> automator
            , IAccommodationValuationParserAsync<IEnumerable<AriaContractsValuationResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var response = await automator.GetValuationResponseAsync(request);
            return await parser.GetValuationResultsAsync(request, response);
        }
    }
}
