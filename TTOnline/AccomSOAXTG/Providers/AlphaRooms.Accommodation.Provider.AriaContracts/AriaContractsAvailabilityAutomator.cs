﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.AriaContracts.Availability;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<IEnumerable<AriaContractsAvailabilityResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAriaContractsRepository ariaContractsRepository;
        private readonly IProviderOutputLogger outputLogger;

        private readonly AriaContractsAvailabilityFactory factory;

        public AriaContractsAvailabilityAutomator(ILogger logger, IAccommodationConfigurationManager configurationManager, IAriaContractsRepository ariaContractsRepository
            , IProviderOutputLogger outputLogger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ariaContractsRepository = ariaContractsRepository;
            this.outputLogger = outputLogger;

            var conn = ConfigurationManager.ConnectionStrings["AlphabedsEntities"].ConnectionString;

            factory = new AriaContractsAvailabilityFactory(conn, configurationManager.RefreshBookingAdjustmentsEvery);
        }

        public async Task<IEnumerable<AriaContractsAvailabilityResponse>> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            try
            {
                List<AriaContractsAvailabilityResponse> results = null;

                IAriaContractsAvailability ariaContractsAvailability = factory.Create();

                results = request.Rooms.Select(room => new AvailabilityRequest(
                                                                        (int)request.Channel,
                                                                        Guid.Parse(request.DestinationCodes[0]),
                                                                        request.CheckInDate,
                                                                        request.CheckOutDate,
                                                                        room.Guests.AdultsCount,
                                                                        room.Guests.ChildrenAndInfantsCount,
                                                                        room.Guests.ChildAndInfantAges))
                                        .SelectMany(rq => ariaContractsAvailability.Search(rq))
                                        .ToList();

                this.outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, results);

                return results;
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("Aria Contracts availability exception :: Request - " + request.ToString() + "  " + ex.Message + "   Stack: " + ex.StackTrace, ex);
            }           
        }      
    }
}
