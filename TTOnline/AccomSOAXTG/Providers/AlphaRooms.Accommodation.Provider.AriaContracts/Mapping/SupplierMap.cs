﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;

namespace AlphaRooms.Accommodation.Provider.AriaContracts.Mapping
{
    public class SupplierMap : EntityTypeConfiguration<Supplier>
    {
        public SupplierMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                 .IsRequired()
                 .HasMaxLength(50);

            this.Property(t => t.LegalName)
                .HasMaxLength(50);


            // Table & Column Mappings
            this.ToTable("tblSupplier");

            this.Property(t => t.Id).HasColumnName("pkID");
            this.Property(t => t.Name).HasColumnName("strName");
            this.Property(t => t.LegalName).HasColumnName("strLegalName");
            this.Property(t => t.ReservationsEmail).HasColumnName("strReservationsEmail");
            this.Property(t => t.FaxNumbers).HasColumnName("strFax");
            this.Property(t => t.EdiProviderCode).HasColumnName("EdiProviderCode");
            this.Property(t => t.AlphaRole).HasColumnName("AlphaRole");
            this.Property(t => t.EmergencyTelephone).HasColumnName("strEmergencyTelephone");
            this.Property(t => t.TransferSupplierName).HasColumnName("strTransferSupplierName");
        }
    }
}
