﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities.Providers;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsAvailabilityParser : IAccommodationAvailabilityParserAsync<IEnumerable<AriaContractsAvailabilityResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAriaContractsRepository ariaContractsRepository;

        private const string ProviderEdiCode = ProviderCodes.AriaContracts;

        public AriaContractsAvailabilityParser(ILogger logger, IAccommodationConfigurationManager configurationManager, IAriaContractsRepository ariaContractsRepository)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ariaContractsRepository = ariaContractsRepository;
        }

        public async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetAvailabilityResultsAsync(AccommodationProviderAvailabilityRequest request, IEnumerable<AriaContractsAvailabilityResponse> response)
        {
            if (response == null)
            {
                throw new SupplierApiDataException("Aria Contracts response is null - Request - " + request.ToString());
            }

            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            await Async.ParallelForEach(response, async (availabilityResult) =>
            {
                try
                {
                    availabilityResults.Enqueue(await CreateAvailabilityResult(availabilityResult, request));
                }
                catch (Exception ex)
                {
                    logger.Error("Aria Contracts availability: Error while adding rooms - " + ex.Message);
                }
            });

            return availabilityResults;
        }

        private async Task<AccommodationProviderAvailabilityResult> CreateAvailabilityResult(AriaContractsAvailabilityResponse localAvailability, AccommodationProviderAvailabilityRequest request)
        {
            var providerAvailabilityResult = new AccommodationProviderAvailabilityResult()
            {
                RoomNumber = request.Rooms[0].RoomNumber,
                ProviderEdiCode = ProviderEdiCode,
                ProviderName = localAvailability.SupplierName,
                ProviderFilterName = request.Provider.Name,
                SupplierEdiCode = localAvailability.SupplierName,
                EstablishmentEdiCode = localAvailability.EdiCode,
                DestinationEdiCode = request.DestinationCodes.Length > 0 ? request.DestinationCodes[0] : string.Empty,
                Adults = request.Rooms[0].Guests.AdultsCount,
                Children = request.Rooms[0].Guests.ChildrenCount,
                Infants = request.Rooms[0].Guests.InfantsCount,
                CheckInDate = request.CheckInDate,
                CheckOutDate = request.CheckOutDate,
                BoardCode = localAvailability.BoardType,
                BoardDescription = localAvailability.BoardDescription,
                RoomCode = localAvailability.RoomId.ToString(),
                RoomDescription = localAvailability.RoomFullDescription,
                CostPrice = new Money(localAvailability.Cost, localAvailability.CurrencyCode),
                SalePrice = new Money(localAvailability.Sale, localAvailability.CurrencyCode),
                CommissionAmount = new Money(localAvailability.Margin, localAvailability.CurrencyCode),
                PaymentModel = localAvailability.PaymentModel > 0 ? (PaymentModel)localAvailability.PaymentModel : PaymentModel.PostPayment,
                IsBindingRate = localAvailability.IsBindingRate,
                ProviderSpecificData = new Dictionary<string, string>() { { "CT", localAvailability.CancellationTerms } },
                IsNonRefundable = localAvailability.IsNonRefundable                
            };

            providerAvailabilityResult.RateType = GetRateType(providerAvailabilityResult.PaymentModel, providerAvailabilityResult.IsBindingRate, localAvailability.IsNetted);

            // Record cost price same as sale price if supplier is going to take payment //TODO Copied from bev5. Need to check
            if (providerAvailabilityResult.PaymentModel == PaymentModel.CustomerPayDirect || providerAvailabilityResult.PaymentModel == PaymentModel.PayOnBooking)
            {
                providerAvailabilityResult.CostPrice = providerAvailabilityResult.SalePrice;
            }

            return providerAvailabilityResult;
        }

        private RateType GetRateType(PaymentModel paymentModel, bool bindingRate, bool isNettedPrice)
        {
            var rateType = RateType.NetStandard;

            if (bindingRate)
            {
                if (isNettedPrice)
                    rateType = RateType.GrossNonBinding;
                else if (paymentModel == PaymentModel.CustomerPayDirect)
                    rateType = RateType.GrossPayDirect;
                else if (paymentModel != PaymentModel.CustomerPayDirect)
                    rateType = RateType.GrossStandard;
            }

            return rateType;
        }

    }
}
