﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<AriaContractsBookingResponse>> automator;
        private readonly IAccommodationBookingParserAsync<IEnumerable<AriaContractsBookingResponse>> parser;

        public AriaContractsBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<AriaContractsBookingResponse>> automator
            , IAccommodationBookingParserAsync<IEnumerable<AriaContractsBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var bookingResponse = await automator.GetBookingResponseAsync(request);
            return await parser.GetBookingResultsAsync(request, bookingResponse);
        }
    }
}
