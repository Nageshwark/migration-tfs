﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsValuationAutomator : IAccommodationValuationAutomatorAsync<IEnumerable<AriaContractsValuationResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAriaContractsRepository AriaContractsRepository;

        public AriaContractsValuationAutomator(ILogger logger, IAccommodationConfigurationManager configurationManager, IAriaContractsRepository AriaContractsRepository)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.AriaContractsRepository = AriaContractsRepository;
        }

        public async Task<IEnumerable<AriaContractsValuationResponse>> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {

            var result = new AriaContractsValuationResponse[0];
            try
            {
                await Task.Run(() =>
                {
                    result = request.SelectedRooms.Select(selectedRoom => new AriaContractsValuationResponse
                    {
                        IsValidRoom = true,
                        CancellationPolicy = selectedRoom.AvailabilityResult.ProviderSpecificData?["CT"],
                        RoomNumber = selectedRoom.RoomNumber,
                        ProviderEdiCode = selectedRoom.AvailabilityResult.ProviderEdiCode,
                        EstablishmentEdiCode = selectedRoom.AvailabilityResult.EstablishmentEdiCode,
                        EstablishmentName = selectedRoom.AvailabilityResult.EstablishmentName,
                        Adults = selectedRoom.AvailabilityResult.Adults,
                        Children = selectedRoom.AvailabilityResult.Children,
                        Infants = selectedRoom.AvailabilityResult.Infants,
                        CheckInDate = selectedRoom.AvailabilityResult.CheckInDate,
                        CheckOutDate = selectedRoom.AvailabilityResult.CheckOutDate,
                        BoardCode = selectedRoom.AvailabilityResult.BoardCode,
                        BoardDescription = selectedRoom.AvailabilityResult.BoardDescription,
                        RoomCode = selectedRoom.AvailabilityResult.RoomCode,
                        RoomDescription = selectedRoom.AvailabilityResult.RoomDescription,
                        SalePrice = selectedRoom.AvailabilityResult.SalePrice,
                        CostPrice = selectedRoom.AvailabilityResult.CostPrice,
                        PaymentModel = selectedRoom.AvailabilityResult.PaymentModel,
                        IsBindingRate = selectedRoom.AvailabilityResult.IsBindingRate,
                        IsOpaqueRate = selectedRoom.AvailabilityResult.IsOpaqueRate,
                        IsNonRefundable = selectedRoom.AvailabilityResult.IsNonRefundable,
                        RateType = selectedRoom.AvailabilityResult.RateType
                    }).ToArray();
                });
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("Aria Contracts valuation exception - valuation request - " + request.ToString() + "  " + ex.Message + "   Stack: " + ex.StackTrace, ex);
            }
            return result;
        }
    }
}
