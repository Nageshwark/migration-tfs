﻿using System;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.AriaContracts.InterFaxWSOutbound;
using AlphaRooms.Accommodation.Provider.AriaContracts.InterFaxWSOutboundSecure;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class InterFaxService : IFaxService
    {
        private readonly ILogger logger;

        // production credential (alphaint, Alfie159)
        // test/dev credential (alphadk, a1phadev4fax)
        string username = "alphadk";
        string password = "a1phadev4fax";
        string urlInterFax = "http://ws.interfax.net/dfs.asmx?wsdl";
        string urlInterFaxSecure = "https://ws-sl.fax.tc/Outbound.asmx?wsdl";

        public InterFaxService(ILogger logger)
        {
            this.logger = logger;
        }

        public void CancelFax(int transactionId)
        {
            InterFax service = new InterFax { Url = urlInterFax };
            long st = service.CancelFax(username, password, transactionId);

            if (st == 0)
                Console.WriteLine("Fax cancelled for transaction: " + transactionId);
            else
                Console.WriteLine("Fax cancellation error: " + st + ", " + GetErrorDescription(Convert.ToInt32(st)));
        }

        public void SendFaxSecure(string reference, string[] faxNumbers, byte[] filesData, string fileTypes,
            string fileSizes)
        {
            var transcationId = SendFaxSecure(faxNumbers, filesData, fileTypes, fileSizes);

            logger.Info("Send Fax details: Itinerary reference {0}, transactionId {1}.", reference, transcationId);
        }

        /// <summary>
        /// The most feature-filled fax sending method. Allows full control over all aspects of fax transmission.
        /// </summary>
        /// <param name="faxNumbers">Array of destination fax numbers in standard international notation e.g. +44-207-3456789</param>
        /// <param name="filesData">base64Binary - Binary data of the document(s). If two or more documents are uploaded, concatenate their binary data.
        /// The total length of this parameter is the sum of all items in the FileSizes parameter.</param>
        /// <param name="fileTypes">List of FileType, semicolon-separated, e.g:TXT;DOC;HTML;HTML</param>
        /// <param name="fileSizes">List of numbers, semicolon-separated, each defining the length of a document (before base64-encoding).</param>
        public string SendFaxSecure(string[] faxNumbers, byte[] filesData, string fileTypes, string fileSizes)
        {
            string strfaxNumbers = string.Join(";", faxNumbers);
            int retriesToPerform = 3;
            string csid = null;
            // "My CSID";
            string pageHeader = DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "  From: {from}  To: {to}  Page: {pagenumber} of {totalpages}";
            string pageSize = null;
            // "A4";
            string pageorientation = null;
            // "Portrait";
            bool isHighResolution = true;
            //Set false to speed up your transmission
            bool isFineRendering = true;
            //fine will fit more graphics, while normal (false) will fit more textual documents
            Outbound service = new Outbound { Url = urlInterFaxSecure };
            long transactionId = service.SendFaxEx_3(username, password, strfaxNumbers, null, filesData, fileTypes, fileSizes, retriesToPerform, csid, pageHeader,
            null, pageSize, pageorientation, isHighResolution, isFineRendering);

            if (transactionId > 0)
                logger.Info("Secure Send fax(SendFaxEx_3) successful, transaction id:" + transactionId);
            else
                logger.Info("Secure Send fax(SendFaxEx_3) error: " + transactionId + ", " + GetErrorDescriptionSecure(Convert.ToInt32(transactionId)));

            return transactionId.ToString();
        }

        private string GetErrorDescription(int errorCode)
        {
            using (InterFax service = new InterFax())
            {
                return service.GetErrorDescription(errorCode);
            }
        }

        private string GetErrorDescriptionSecure(int errorCode)
        {
            using (Outbound service = new Outbound { Url = urlInterFaxSecure })
            {
                return service.GetErrorDescription(errorCode);
            }
        }

    }
}
