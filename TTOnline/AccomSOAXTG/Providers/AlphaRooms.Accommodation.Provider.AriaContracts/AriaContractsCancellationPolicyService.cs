﻿using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsCancellationPolicyService : IAriaContractsCancellationPolicyService
    {
        private readonly IProviderValuationCancellationPolicyService cancellationPolicyService;

        public AriaContractsCancellationPolicyService(IProviderValuationCancellationPolicyService cancellationPolicyService)
        {
            this.cancellationPolicyService = cancellationPolicyService;
        }

        public async Task<string> CreateLocalACodeConditionNonRefundableText(AccommodationProviderValuationRequest request)
        {
            return await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 1);
        }

        public async Task<string> CreateLocalACodeCancellationConditionDaysBeforeText(AccommodationProviderValuationRequest request, string days, string fee)
        {
            return await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 2, days, fee);
        }

        public async Task<string> CreateLocalACodeConditionNoShowText(AccommodationProviderValuationRequest request, string fee)
        {
            return await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 3, fee);
        }
    }
}
