﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.AriaContracts
{
    public class AriaContractsBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<AriaContractsBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAriaContractsRepository ariaContractsRepository;
        private readonly IAccommodationProviderEmailTemplateRepository emailTemplateRepository;
        private readonly ISmtpClientService smtpClient;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IAriaContractsEmailService emailService;
        private readonly ISiteMinderService siteMinderService;
        private readonly IProviderLoggerService loggerService;

        public AriaContractsBookingAutomator(ILogger logger, IAccommodationConfigurationManager configurationManager, IAriaContractsRepository ariaContractsRepository
            , IAccommodationProviderEmailTemplateRepository emailTemplateRepository, ISmtpClientService smtpClient, IProviderOutputLogger outputLogger, IAriaContractsEmailService emailService, ISiteMinderService siteMinderService, IProviderLoggerService loggerService)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ariaContractsRepository = ariaContractsRepository;
            this.emailTemplateRepository = emailTemplateRepository;
            this.smtpClient = smtpClient;
            this.outputLogger = outputLogger;
            this.emailService = emailService;
            this.siteMinderService = siteMinderService;
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<AriaContractsBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            var bookingResponseList = new List<AriaContractsBookingResponse>();
            var bookingReference = string.Empty;

            try
            {
                var supplierRequest = "";
                StringBuilder sbParameters = new StringBuilder();
                sbParameters.AppendFormat("<AriaContractsBookingRequest> <ProviderEdiCode> {0}", request.Provider.EdiCode);
                sbParameters.AppendFormat(" </ProviderEdiCode> <ItineraryId> {0}", request.ItineraryId);
                sbParameters.AppendFormat(" </ItineraryId> </AriaContractsBookingRequest>");
                supplierRequest = sbParameters.ToString();

                loggerService.SetProviderRequest(request, supplierRequest);

                var channelPushInfo = IsChannelPush(request);

                foreach (var valuatedRoom in request.ValuatedRooms)
                {
                    if (channelPushInfo[valuatedRoom.ValuationResult.RoomCode] == "None")
                    {
                        AccommodationProviderValuationResult room = valuatedRoom.ValuationResult;
                        var bookingResponse = new AriaContractsBookingResponse();

                        bookingReference = $"{request.Provider.EdiCode}B{request.ItineraryId}:{(room.RoomNumber)}";
                        bookingResponse.BookingReference = bookingReference;
                        bookingResponse.BookingStatus = BookingStatus.Confirmed;
                        bookingResponse.ProviderEdiCode = request.Provider.EdiCode;
                        bookingResponse.RoomNumber = room.RoomNumber;

                        loggerService.SetProviderResponse(request,bookingResponse);

                        // bookingResponse.SwitchToDirect = request. //TODO: field needs to be added to the request
                        bookingResponseList.Add(bookingResponse);
                    }
                }

                //Channel push booking : separated from the above section as multibooking can be done 
                if (channelPushInfo.ContainsValue("SM") || channelPushInfo.ContainsValue("SME") || channelPushInfo.ContainsValue("APAC") || channelPushInfo.ContainsValue("APACE"))
                {
                    //call the siteminder booking methods
                    bookingResponseList = (List<AriaContractsBookingResponse>) await siteMinderService.ConfirmSiteMinderBooking(request, channelPushInfo,ariaContractsRepository);
                }

                //Send email to the supplier asking them to confirm the reservation. In case of Siteminder, check if the send email service flag is set or not
                if (channelPushInfo.ContainsValue("None") || channelPushInfo.ContainsValue("SME") || channelPushInfo.ContainsValue("APACE"))
                {
                    await emailService.CreateProviderConfirmationEmail(request, bookingResponseList, emailTemplateRepository, ariaContractsRepository, smtpClient);
                }
            }
            catch (Exception ex)
            {
                bookingResponseList.ForEach(response => response.BookingStatus = BookingStatus.NotConfirmed);
                outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, bookingResponseList);
                logger.Error("Aria Contracts booking: Error while booking rooms -  Request - " + request.ToString() + "  " + ex.Message + "   Stack: " + ex.StackTrace, ex);
                throw new SupplierApiException("Aria Contracts booking exception :: Request - " + request.ToString() + "  " + ex.Message + "   Stack: " + ex.StackTrace, ex);
            }

            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, bookingResponseList);

            return bookingResponseList;
        }

        private Dictionary<string,string> IsChannelPush(AccommodationProviderBookingRequest request)
        {
            var channelPushInfo = new Dictionary<string,string>();

            foreach (var room in request.ValuatedRooms)
            {
                if (!channelPushInfo.ContainsKey(room.ValuationResult.RoomCode))
                {
                    channelPushInfo.Add(room.ValuationResult.RoomCode, GetChannelPushInfo(room.ValuationResult.RoomCode));
                }
            }

            return channelPushInfo;
        }

        private string GetChannelPushInfo(string roomCode)
        {
            var channelPushInfo =  ariaContractsRepository.GetChannelPushInfo(roomCode);
            return channelPushInfo.Result;
        }
    }
}
