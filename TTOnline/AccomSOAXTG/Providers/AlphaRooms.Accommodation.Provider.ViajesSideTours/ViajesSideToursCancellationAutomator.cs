﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursCancellationAutomator :ViajesSideToursAutomaterBase, IAccommodationCancellationAutomatorAsync<ViajesSideToursCancellationResponse>
    {
        private const string ViajesSideToursCancellationUrl = "ViajesSideToursCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesSideToursCancellationRequestFactory _ViajesSideToursCancellationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViajesSideToursCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesSideToursCancellationRequestFactory viajesSideToursSupplierCancellationRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._ViajesSideToursCancellationRequestFactory = viajesSideToursSupplierCancellationRequestFactory;
            this._outputLogger = outputLogger;
        }
        public async Task<ViajesSideToursCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ViajesSideToursCancellationResponse supplierResponse = null;

            try
            {
                var supplierRequest = _ViajesSideToursCancellationRequestFactory.CreateSupplierCancelRequest(request);

                var client = ConfigureSoapClient(request.Provider.Parameters, true);

                var username = request.Provider.Parameters.GetParameterValue(ViajesSideToursUsername);
                var password = request.Provider.Parameters.GetParameterValue(ViajesSideToursPassword);

                var responseXml = client.service("1503", username, password, supplierRequest);

                var response = responseXml.XmlDeSerialize<CancellationResponse>();

                supplierResponse = new ViajesSideToursCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = response
                };

                

                return supplierResponse;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (supplierResponse != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(supplierResponse.SupplierRequest);
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(supplierResponse.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesSideToursBookingAutomator.GetCancellationResponseAsync: ViajesSideTours supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

       

       
    }
}
