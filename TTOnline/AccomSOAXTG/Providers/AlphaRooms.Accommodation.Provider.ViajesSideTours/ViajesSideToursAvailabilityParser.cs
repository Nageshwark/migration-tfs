﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursAvailabilityParser:ViajesSideToursParserBase, IAccommodationAvailabilityParser<ViajesSideToursAvailabilityResponse>
    {
        private const string ViajesSideToursCurrencyCode = "ViajesSideToursCurrencyCode";

        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesSideToursAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            ViajesSideToursAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesSideTours Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, string supplierRequest, AvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null &&
                               supplierResponse.BUILDING_SET != null &&
                               supplierResponse.BUILDING_SET.Any()

                               );
                                
                                

            if (hasResults)
            {
                string currencyCode = request.Provider.Parameters.GetParameterValue(ViajesSideToursCurrencyCode);

                foreach (var building in supplierResponse.BUILDING_SET.First().BUILDING)
                {
                    foreach (var board in building.BOARD)
                    {
                        int roomNumber = 1;
                        foreach (var room in board.ROOM)
                        {
                            var result = new AccommodationProviderAvailabilityResult();
                            var accom = room.ACOMODATION.First();

                            result.RoomNumber = (byte)roomNumber;
                            result.ProviderEdiCode = request.Provider.EdiCode;
                            result.SupplierEdiCode = request.Provider.EdiCode;
                            result.EstablishmentName = building.name;
                            result.EstablishmentEdiCode = building.id;

                            result.DestinationEdiCode = building.destination;
                            
                            result.BoardCode = board.id;
                            result.BoardDescription = board.name;

                            result.RoomCode = room.ACOMODATION.First().id;
                            result.RoomDescription = room.ACOMODATION.First().name;

                            result.PaymentModel = PaymentModel.PostPayment;
                            result.RateType = RateType.NetStandard;
                            result.IsOpaqueRate = false;
                            result.IsNonRefundable = accom.non_refundable == "1";
                            
                            if (accom.price != room.min_price)
                                throw new SupplierApiException("Accom.Price != Min_Price");

                            result.SalePrice = new Money
                            {
                                Amount = decimal.Parse(accom.price),
                                CurrencyCode = currencyCode
                            };

                            result.CostPrice = result.SalePrice;
                            //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                            result.Adults = byte.Parse(room.adults);
                            result.Children = byte.Parse(room.children);
                            result.Infants = byte.Parse(room.babies);
                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckInDate;

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("Notes", accom.NOTES);

                            availabilityResults.Enqueue(result);

                            roomNumber++;
                        }
                        
                    }
                    
                }
               
                
            }
            else
            {
                if (supplierResponse != null && supplierResponse.ERROR != null && supplierResponse.ERROR.Cod != "402")
                {
                    var sbMessage = new StringBuilder();
                    sbMessage.AppendLine(    "ViajesSideTours Availability Parser: Error returned in Availability Response.");

                    sbMessage.AppendLine(string.Format("Details:{0}", supplierResponse.ERROR.Msg));

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }

        }

        
    }
}
