﻿using System.Linq;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursBookingParser : IAccommodationBookingParser<ViajesSideToursBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesSideToursBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, ViajesSideToursBookingResponse response)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesSideTours Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        string supplierRequest,
                                                        ReservationResponse supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            
            bool hasResults = (supplierResponse != null && supplierResponse.RESERVATION !=null && supplierResponse.RESERVATION.Any() );

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                result.ProviderBookingReference = supplierResponse.RESERVATION.First().locata;

                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);
            }
            else
            {
                var sbMessage = new StringBuilder();

                sbMessage.AppendLine("ViajesSideTours Booking Parser: Booking Response cannot be parsed because it is null.");

                //if (supplierResponse != null && !string.IsNullOrEmpty(supplierResponse.message))
                //    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

       
    }
}
