﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;

using log4net.Repository.Hierarchy;


namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursAvailabilityAutomator:ViajesSideToursAutomaterBase, IAccommodationAvailabilityAutomatorAsync<ViajesSideToursAvailabilityResponse>
    {
        private const string ViajesSideToursAvailabilityUrl = "ViajesSideToursAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IViajesSideToursAvailabilityRequestFactory _viajesSideToursSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _providerOutputLogger;

        public ViajesSideToursAvailabilityAutomator(IViajesSideToursAvailabilityRequestFactory viajesSideToursAvailabilityRequestFactory, IProviderOutputLogger providerOutputLogger)
        {
            this._viajesSideToursSupplierAvailabilityRequestFactory = viajesSideToursAvailabilityRequestFactory;
            _providerOutputLogger = providerOutputLogger;
        }

        public async Task<ViajesSideToursAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            ViajesSideToursAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                string supplierRequest = this._viajesSideToursSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViajesSideToursAvailabilityAutomator.GetAvailabilityResponseAsync: ViajesSideTours supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<ViajesSideToursAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, string  supplierRequest)
        {
            var client = ConfigureSoapClient(request.Provider.Parameters, true);
            
            var username = request.Provider.Parameters.GetParameterValue(ViajesSideToursUsername);
            var password = request.Provider.Parameters.GetParameterValue(ViajesSideToursPassword);

            var responseXml = client.service("1400", username, password, supplierRequest);

            var response = responseXml.XmlDeSerialize<AvailabilityResponse>();

            var supplierResponse = new ViajesSideToursAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _providerOutputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

       

       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("ViajesSideTours supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("ViajesSideTours supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("ViajesSideTours supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("ViajesSideTours supports upto 2 Infants");
            //}
        }
    }
}
