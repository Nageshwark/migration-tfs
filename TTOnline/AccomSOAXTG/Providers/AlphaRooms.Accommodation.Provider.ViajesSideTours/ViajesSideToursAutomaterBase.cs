﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Helper;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.ViajesSideTourService;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursAutomaterBase
    {
        private const string ViajesSideToursAvailabilityUrl = "ViajesSideToursAvailabilityUrl";
        private const string ViajesSideToursValuationUrl = "ViajesSideToursAvailabilityUrl";
        private const string ViajesSideToursBookingUrl = "ViajesSideToursBookingUrl";
        private const string ViajesSideToursCancellationUrl = "ViajesSideToursCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        
        private const string ViajesSideToursLanguageCode = "ViajesSideToursLanguageCode";
        private const  string ViajesSideToursCountryCode= "ViajesSideToursCountryCode";
        protected const string ViajesSideToursUsername = "ViajesSideToursUsername";
        protected const string ViajesSideToursPassword = "ViajesSideToursPassword";

        protected SIDETOURSPortTypeClient ConfigureSoapClient(List<AccommodationProviderParameter> parameters, bool addAuthHeaders)
        {
            ICollection<BindingElement> bindingElements = new List<BindingElement>();
            HttpTransportBindingElement httpBindingElement = new HttpTransportBindingElement();
            httpBindingElement.MaxReceivedMessageSize = long.Parse(parameters.GetParameterValue(MaxReceivedMessageSize));

            CustomTextMessageBindingElement textBindingElement = new CustomTextMessageBindingElement();
            textBindingElement.Encoding = "iso-8859-1";
            bindingElements.Add(textBindingElement);
            bindingElements.Add(httpBindingElement);
            
            CustomBinding binding = new CustomBinding(bindingElements);
            

            
            EndpointAddress address = null;
            
            if (this.GetType() == typeof(ViajesSideToursAvailabilityAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesSideToursAvailabilityUrl)));
            else if (this.GetType() == typeof(ViajesSideToursValuationAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesSideToursValuationUrl)));
            else if (this.GetType() == typeof(ViajesSideToursBookingAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesSideToursBookingUrl)));
            else if (GetType() == typeof(ViajesSideToursCancellationAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesSideToursCancellationUrl)));

            var client = new SIDETOURSPortTypeClient(binding, address);

            //var proxyHelper = new ProxyHelper();
            //proxyHelper.SetProxy(client, parameters);

            
            return client;
        }

       

    }
}
