﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that ViajesSideTours accepts and returns XML strings.
    /// </summary>
    public class ViajesSideToursBookingResponse
    {
        public string SupplierRequest { get; set; }
        public ReservationResponse SupplierResponse { get; set; }
    }
}
