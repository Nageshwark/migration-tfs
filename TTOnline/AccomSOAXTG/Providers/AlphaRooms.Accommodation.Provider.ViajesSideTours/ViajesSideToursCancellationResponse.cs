﻿
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursCancellationResponse
    {
        public string SupplierRequest { get; set; }
        public CancellationResponse SupplierResponse { get; set; }
    }
}
