﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity
{
    [XmlRoot(ElementName = "PENALTY")]
    public class PENALTY
    {
        [XmlAttribute(AttributeName = "room_id")]
        public string Room_id { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string Price { get; set; }
    }

    [XmlRoot(ElementName = "CANCELLATION")]
    public class CANCELLATION
    {
        [XmlElement(ElementName = "PENALTY")]
        public List<PENALTY> PENALTY { get; set; }
        [XmlAttribute(AttributeName = "locata")]
        public string Locata { get; set; }
        [XmlAttribute(AttributeName = "result")]
        public string Result { get; set; }
        [XmlAttribute(AttributeName = "expenses")]
        public string Expenses { get; set; }
    }

    [XmlRoot(ElementName = "RESPONSE")]
    public class CancellationResponse
    {
        [XmlElement(ElementName = "CANCELLATION")]
        public CANCELLATION CANCELLATION { get; set; }
        [XmlAttribute(AttributeName = "cod")]
        public string Cod { get; set; }
    }
}
