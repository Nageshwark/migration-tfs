﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViajesSideTours;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ViajesSideToursValuationResponse> automator;
        private readonly IAccommodationValuationParser<ViajesSideToursValuationResponse> parser;

        public ViajesSideToursValuationProvider(IAccommodationValuationAutomatorAsync<ViajesSideToursValuationResponse> automator,
                                                IAccommodationValuationParser<ViajesSideToursValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
