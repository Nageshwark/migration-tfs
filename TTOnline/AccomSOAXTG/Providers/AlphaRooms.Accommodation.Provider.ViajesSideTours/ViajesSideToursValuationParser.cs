﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{

    public class ViajesSideToursValuationParser : IAccommodationValuationParser<ViajesSideToursValuationResponse>
    {
        private readonly ILogger logger;
        private string ViajesSideToursCurrencyCode = "ViajesSideToursCurrencyCode";
        //private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesSideToursValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, ViajesSideToursValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesSideTours Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, string supplierRequest, ReservationResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = ( supplierResponse != null && 
                                supplierResponse.RESERVATION != null && 
                                supplierResponse.RESERVATION.Any() &&
                                supplierResponse.RESERVATION.First().ROOM != null
                               );
            

            if (hasResults)
            {
                string currencyCode = request.Provider.Parameters.GetParameterValue(ViajesSideToursCurrencyCode);
                int roomNumber = 1;
                var reservation = supplierResponse.RESERVATION.First();

                foreach (var room in reservation.ROOM)
                {
                    var availResult = request.SelectedRooms[roomNumber - 1].AvailabilityResult;
                    var result = new AccommodationProviderValuationResult();

                    result.RoomNumber = (byte)roomNumber;
                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.SupplierEdiCode = request.Provider.EdiCode;
                    result.EstablishmentName = availResult.EstablishmentName;
                    result.EstablishmentEdiCode = reservation.building_id;

                    result.DestinationEdiCode = availResult.DestinationEdiCode;

                    result.BoardCode = reservation.board_id;
                    result.BoardDescription = availResult.BoardDescription;
                    result.RoomCode = room.id_acomod;

                    result.RoomDescription = availResult.RoomDescription;
                    result.PaymentModel = availResult.PaymentModel;
                    result.RateType = availResult.RateType;
                    result.IsNonRefundable = room.non_refundable == "1";
                    
                    result.SalePrice = new Money { Amount = decimal.Parse(room.price), CurrencyCode = currencyCode };
                    result.CostPrice = result.SalePrice;
                   

                    result.Adults = byte.Parse(room.adults);
                    result.Children = byte.Parse(room.children);
                    //result.Infants = byte.Parse(room.babies);
                    result.CheckInDate = availResult.CheckInDate;
                    result.CheckOutDate =availResult.CheckOutDate;
                    result.CancellationPolicy = GetCancellationPolicy(room.CANCEL_POLICIES);

                    result.ProviderSpecificData = new Dictionary<string, string>();

                    roomNumber++;

                    availabilityResults.Enqueue(result);
                    
                }               

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("ViajesSideTours Availability Parser: Availability Response cannot be parsed because it is null.");

                //if (supplierResponse != null && !string.IsNullOrEmpty(supplierResponse.message))
                //    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));

                throw new SupplierApiDataException(sbMessage.ToString());
            }
           
        }

        private string[] GetCancellationPolicy(RESPONSERESERVATIONROOMCANCEL_POLICIESPOLICY[] cancelPolicies)
        {
            return cancelPolicies.Select(policy => string.Format("From:{0}, To:{1}, Price:{2}", policy.from, policy.to, policy.price)).ToArray();
        }
    }
}
