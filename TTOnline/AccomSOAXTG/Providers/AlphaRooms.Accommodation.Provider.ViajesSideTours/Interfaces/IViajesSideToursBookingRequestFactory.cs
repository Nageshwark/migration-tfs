﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces
{
    public interface IViajesSideToursBookingRequestFactory
    {
        string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
