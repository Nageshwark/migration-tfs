﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;


namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces
{
    public interface IViajesSideToursAvailabilityRequestFactory
    {
        string CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
