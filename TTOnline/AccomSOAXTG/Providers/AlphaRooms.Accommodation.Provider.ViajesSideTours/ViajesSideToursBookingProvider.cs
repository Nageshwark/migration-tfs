﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesSideTours;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<ViajesSideToursBookingResponse> automator;
        private readonly IAccommodationBookingParser<ViajesSideToursBookingResponse> parser;

        public ViajesSideToursBookingProvider(IAccommodationBookingAutomatorAsync<ViajesSideToursBookingResponse> automator,
                                        IAccommodationBookingParser<ViajesSideToursBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
