﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursValuationResponse
    {
        public string SupplierRequest { get; set; }
        public ReservationResponse SupplierResponse { get; set; }

    }
}
