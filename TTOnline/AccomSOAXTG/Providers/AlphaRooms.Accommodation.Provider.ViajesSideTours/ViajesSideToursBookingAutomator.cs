﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursBookingAutomator :ViajesSideToursAutomaterBase, IAccommodationBookingAutomatorAsync<ViajesSideToursBookingResponse>
    {
        private const string ViajesSideToursBookingUrl = "ViajesSideToursBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesSideToursBookingRequestFactory IViajesSideToursBookingRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViajesSideToursBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesSideToursBookingRequestFactory viajesSideToursSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.IViajesSideToursBookingRequestFactory = viajesSideToursSupplierBookingRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<ViajesSideToursBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ViajesSideToursBookingResponse> responses = new List<ViajesSideToursBookingResponse>();

            string supplierRequest = null;
            ViajesSideToursBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.IViajesSideToursBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.IViajesSideToursBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesSideToursBookingAutomator.GetBookingResponseAsync: ViajesSideTours supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ViajesSideToursBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, string supplierRequest)
        {
            var client = ConfigureSoapClient(request.Provider.Parameters, true);

            var username = request.Provider.Parameters.GetParameterValue(ViajesSideToursUsername);
            var password = request.Provider.Parameters.GetParameterValue(ViajesSideToursPassword);

            var responseXml = client.service("500", username, password, supplierRequest);

            var response = responseXml.XmlDeSerialize<ReservationResponse>();

            var supplierResponse = new ViajesSideToursBookingResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
    }
}
