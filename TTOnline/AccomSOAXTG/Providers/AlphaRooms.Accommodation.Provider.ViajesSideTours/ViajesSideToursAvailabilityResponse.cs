﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.ViajesSideTourService;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursAvailabilityResponse
    {
        public string SupplierRequest { get; set; }
        public AvailabilityResponse SupplierResponse { get; set; }

    }
}
