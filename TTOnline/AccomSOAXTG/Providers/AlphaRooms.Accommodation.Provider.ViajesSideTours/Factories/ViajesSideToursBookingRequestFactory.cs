﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories;


namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursBookingRequestFactory : ViajesSideToursRequestFactoryBase, IViajesSideToursBookingRequestFactory
    {
        private const string ViajesSideToursLogin = "ViajesSideToursLogin";
        private const string ViajesSideToursPassword = "ViajesSideToursPassword";
        private const string ViajesSideToursLanguage = "ViajesSideToursLanguage";
        private const string ViajesSideToursGzip = "ViajesSideToursGzip";
        private const string ViajesSideToursAgentId = "ViajesSideToursAgentId";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var availabilityRequest = request.ValuatedRooms.First().AvailabilityRequest;
            var availabilityResult = request.ValuatedRooms.First().ValuationResult;

            var supplierRequest = new StringBuilder();

            supplierRequest.Append("<PARAMETERS ");
            supplierRequest.AppendFormat(" language=\"{0}\"", request.Provider.Parameters.GetParameterValue(ViajesSideToursLanguage));
            supplierRequest.AppendFormat(" building_id=\"{0}\"", availabilityResult.EstablishmentEdiCode);

            supplierRequest.AppendFormat(" check_in=\"{0}\"", availabilityRequest.CheckInDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" check_out=\"{0}\"", availabilityRequest.CheckOutDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" file=\"{0}\"", "test note");
            supplierRequest.AppendFormat(" board_id=\"{0}\"", availabilityResult.BoardCode);
            supplierRequest.Append(">");

            foreach (var room in request.ValuatedRooms)
            {
                supplierRequest.Append(string.Format("<ROOM adults=\"{0}\" children=\"{1}\" {2} contact=\"{3}\" aco=\"{4}\" idmv=\"1\" />",
                                                                    room.Guests.AdultsCount,
                                                                    room.Guests.ChildrenAndInfantsCount,
                                                                    GetChildAges(room.Guests.ChildAndInfantAges),
                                                                    string.Format("{0} {1} {2}", request.Customer.Title.ToString(), request.Customer.FirstName ,  request.Customer.Surname),
                                                                    room.ValuationResult.RoomCode
                                                                    ));
            }

            supplierRequest.Append("</PARAMETERS>");

            return supplierRequest.ToString();
        }


      
        
    }
}
