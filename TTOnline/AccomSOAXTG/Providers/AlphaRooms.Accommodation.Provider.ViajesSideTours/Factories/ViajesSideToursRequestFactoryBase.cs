﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;


namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories
{
    public abstract class ViajesSideToursRequestFactoryBase
    {

        protected string GetChildAges(byte[] childAndInfantAges)
        {
            var sbAges = new StringBuilder();

            int counter = 0;
            foreach (var age in childAndInfantAges)
            {
                sbAges.AppendFormat("age{0}=\"{1}\" ", counter, age);
                counter++;
            }

            return sbAges.ToString();
        }
    }
}
