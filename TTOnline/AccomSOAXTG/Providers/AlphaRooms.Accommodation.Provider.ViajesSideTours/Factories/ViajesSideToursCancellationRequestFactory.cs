﻿using System;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;


namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories
{
    public class ViajesSideToursCancellationRequestFactory : IViajesSideToursCancellationRequestFactory
    {
        private const string ViajesSideToursLogin = "ViajesSideToursLogin";
        private const string ViajesSideToursPassword = "ViajesSideToursPassword";
        private const string ViajesSideToursLanguage = "ViajesSideToursLanguage";
        private const string ViajesSideToursGzip = "ViajesSideToursGzip";
        private const string ViajesSideToursAgentId = "ViajesSideToursAgentId";

        public string CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            //<PARAMETERS locata="1839618" action=""></PARAMETERS>
            var supplierRequest = new StringBuilder();

            supplierRequest.Append(string.Format("<PARAMETERS locata=\"{0}\" action=\"{1}\"></PARAMETERS>", request.ProviderBookingReference, "cancel"));

            return supplierRequest.ToString();
        }
    }
}
