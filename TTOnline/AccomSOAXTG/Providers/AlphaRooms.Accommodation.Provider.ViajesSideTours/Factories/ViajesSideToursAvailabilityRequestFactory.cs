﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories
{
    public class ViajesSideToursAvailabilityRequestFactory :ViajesSideToursRequestFactoryBase, IViajesSideToursAvailabilityRequestFactory
    {
        private const string ViajesSideToursPayMode = "ViajesSideToursPayMode";
        private const string ViajesSideToursLanguage = "ViajesSideToursLanguage";




        public string CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new StringBuilder();

            supplierRequest.Append("<PARAMETERS ");
            if (request.DestinationCodes !=null && request.DestinationCodes.Any())
                supplierRequest.AppendFormat(" destination=\"{0}\"", request.DestinationCodes.First());
            else
                supplierRequest.AppendFormat(" building_id=\"{0}\"", GetBuildingIDs(request.EstablishmentCodes));
            
            supplierRequest.AppendFormat(" check_in=\"{0}\"", request.CheckInDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" check_out=\"{0}\"", request.CheckOutDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" language=\"{0}\"", request.Provider.Parameters.GetParameterValue(ViajesSideToursLanguage));
            supplierRequest.AppendFormat(" pay_mode=\"{0}\"", request.Provider.Parameters.GetParameterValue(ViajesSideToursPayMode));
            supplierRequest.Append(">");

            foreach (var room in request.Rooms)
            {
                supplierRequest.Append(string.Format("<ROOM adults=\"{0}\" children=\"{1}\" {2} idmv=\"1\" />", room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount, GetChildAges(room.Guests.ChildAndInfantAges)));
            }

            supplierRequest.Append("</PARAMETERS>");

            return supplierRequest.ToString();
        }

        private string GetBuildingIDs(string[] establishmentCodes)
        {
            if (establishmentCodes.Length == 1)
                return establishmentCodes.First();
            else
            {
                var ids = new StringBuilder();

                foreach (var establishmentCode in establishmentCodes)
                {
                    ids.AppendFormat("{0},", establishmentCode);
                }

                return ids.ToString().Substring(0, ids.Length - 1);
            }
        }
    }
}
