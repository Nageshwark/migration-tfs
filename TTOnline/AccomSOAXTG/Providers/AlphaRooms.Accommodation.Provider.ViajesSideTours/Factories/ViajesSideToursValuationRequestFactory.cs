﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories
{
    public class ViajesSideToursValuationRequestFactory : ViajesSideToursRequestFactoryBase, IViajesSideToursValuationRequestFactory
    {
        private const string ViajesSideToursLanguage = "ViajesSideToursLanguage";

        public string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        { 
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            var supplierRequest = new StringBuilder();

            supplierRequest.Append("<PARAMETERS ");
            supplierRequest.AppendFormat(" language=\"{0}\"", request.Provider.Parameters.GetParameterValue(ViajesSideToursLanguage));
            supplierRequest.AppendFormat(" building_id=\"{0}\"", availabilityResult.EstablishmentEdiCode);
            
            supplierRequest.AppendFormat(" check_in=\"{0}\"", availabilityRequest.CheckInDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" check_out=\"{0}\"", availabilityRequest.CheckOutDate.ToString("yyyy-MM-dd"));
            supplierRequest.AppendFormat(" file=\"{0}\"", "test note");
            supplierRequest.AppendFormat(" board_id=\"{0}\"", availabilityResult.BoardCode);
            supplierRequest.Append(">");

            foreach (var room in request.SelectedRooms)
            {
                supplierRequest.Append(string.Format("<ROOM adults=\"{0}\" children=\"{1}\" {2} contact=\"{3}\" aco=\"{4}\" idmv=\"1\" />", 
                                                                    room.Guests.AdultsCount, 
                                                                    room.Guests.ChildrenAndInfantsCount, 
                                                                    GetChildAges(room.Guests.ChildAndInfantAges),
                                                                    "Mr Test",
                                                                    room.AvailabilityResult.RoomCode
                                                                    ));
            }

            supplierRequest.Append("</PARAMETERS>");

            return supplierRequest.ToString();
        }
    }
}
