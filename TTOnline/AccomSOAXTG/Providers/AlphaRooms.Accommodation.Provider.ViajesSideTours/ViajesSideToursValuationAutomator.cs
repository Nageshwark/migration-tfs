﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Entity;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursValuationAutomator :ViajesSideToursAutomaterBase, IAccommodationValuationAutomatorAsync<ViajesSideToursValuationResponse>
    {
        private const string ViajesSideToursValuationUrl = "ViajesSideToursValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly IViajesSideToursValuationRequestFactory _viajesSideToursValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViajesSideToursValuationAutomator( ILogger logger,
                                                IAccommodationConfigurationManager configurationManager,
                                                IViajesSideToursValuationRequestFactory viajesSideToursSupplierValuationRequestFactory,
                                                IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._viajesSideToursValuationRequestFactory = viajesSideToursSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<ViajesSideToursValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ViajesSideToursValuationResponse response = null;

            try
            {
                var supplierRequest = this._viajesSideToursValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViajesSideToursValuationAutomator.GetValuationResponseAsync: ViajesSideTours supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ViajesSideToursValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, string supplierRequest)
        {
            var client = ConfigureSoapClient(request.Provider.Parameters, true);

            var username = request.Provider.Parameters.GetParameterValue(ViajesSideToursUsername);
            var password = request.Provider.Parameters.GetParameterValue(ViajesSideToursPassword);

            var responseXml = client.service("505", username, password, supplierRequest);

            var response = responseXml.XmlDeSerialize<ReservationResponse>();

            var supplierResponse = new ViajesSideToursValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
    }
}
