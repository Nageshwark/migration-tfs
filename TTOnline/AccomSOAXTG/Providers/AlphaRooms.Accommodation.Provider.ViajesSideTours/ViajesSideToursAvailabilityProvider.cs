﻿using AlphaRooms.Accommodation.Provider.ViajesSideTours;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViajesSideTours;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ViajesSideTours;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.ViajesSideTourService;

namespace AlphaRooms.Accommodation.Provider.ViajesSideTours
{
    public class ViajesSideToursAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ViajesSideToursAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<ViajesSideToursAvailabilityResponse> parser;

        public ViajesSideToursAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ViajesSideToursAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ViajesSideToursAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
