﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Helper;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<SerhsBookingResponse>>
    {
        private const string SerhsBookingUrl = "SerhsBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ISerhsProviderBookingRequestFactory serhsProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public SerhsBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ISerhsProviderBookingRequestFactory SerhsSupplierBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.serhsProviderBookingRequestFactory = SerhsSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<SerhsBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<SerhsBookingResponse> responses = new List<SerhsBookingResponse>();

            request supplierRequest = null;
            Entity.Confirm.Request.request supplierConfirmRequest = null;
            SerhsBookingResponse bookingResponse = null;

            try
            {
                //Step1. First fire Accommodation_Booking message to do the pre-booking and get the Pre-Booking Code
                supplierRequest = this.serhsProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                bookingResponse = await GetSupplierBookingResponseAsync(request, supplierRequest);
                

                //Step 2.  If Step 1 is successful proceed to Call Confirm to confirm booking
                if (CanContinue(bookingResponse))
                {
                    supplierConfirmRequest = this.serhsProviderBookingRequestFactory.CreateSupplierConfirmRequest(request, bookingResponse.Booking.SupplierResponse);
                    var confirmResponse = await GetSupplierConfirmResponseAsync(request, supplierConfirmRequest);

                    bookingResponse.Confirm = new SerhsConfirm
                    {
                        SupplierRequest = confirmResponse.Confirm.SupplierRequest,
                        SupplierResponse = confirmResponse.Confirm.SupplierResponse
                    };
 
                    
                    responses.Add(bookingResponse);

                }
                else
                {
                    throw new SupplierApiException("SerhsBookingAutomator.GetBookingResponseAsync: Serhs supplier api exception. Error Message:Pre-booking Unsuccesful, Can not Continue");
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (bookingResponse != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Booking Request:");
                    sb.AppendLine(bookingResponse.Booking.SupplierRequest.XmlSerialize());
                    sb.AppendLine("Confirm Request:");
                    sb.AppendLine(bookingResponse.Confirm.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(bookingResponse.Booking.SupplierResponse.XmlSerialize());
                    sb.AppendLine("Confirm Response:");
                    sb.AppendLine(bookingResponse.Confirm.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("SerhsBookingAutomator.GetBookingResponseAsync: Serhs supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private static bool CanContinue(SerhsBookingResponse response)
        {
            return  response.Booking.SupplierResponse !=null &&
                    response.Booking.SupplierResponse.preBooking !=null && 
                    !string.IsNullOrEmpty(response.Booking.SupplierResponse.preBooking.code);
        }

        private async Task<SerhsBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, request supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(SerhsBookingUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<response>();

            var supplierResponse = new SerhsBookingResponse()
            {
                Booking = new SerhsBooking
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = response
                }
            };

            return supplierResponse;
        }

        private async Task<SerhsBookingResponse> GetSupplierConfirmResponseAsync(AccommodationProviderBookingRequest request, Entity.Confirm.Request.request supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(SerhsBookingUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<Entity.Confirm.Response.response>();

            var supplierResponse = new SerhsBookingResponse()
            {
                Confirm = new SerhsConfirm
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = response
                }
            };

            return supplierResponse;
        }

       
    }
}
