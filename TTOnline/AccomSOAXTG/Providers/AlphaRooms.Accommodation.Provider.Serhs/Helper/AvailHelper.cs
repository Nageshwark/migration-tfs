﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Response;

namespace AlphaRooms.Accommodation.Provider.Serhs.Helper
{
    public class AvailHelper
    {
        public decimal ParseAmount(string amount)
        {
            return decimal.Parse(amount, CultureInfo.GetCultureInfo("es-ES"));
        }

        public string[] GetCancellationPolicyString(cancelPolicy[] cancelPolicies)
        {
            return cancelPolicies.Select(cPolicy => string.Format("ReleaseDate:{0}, Amount:{1}", cPolicy.releaseDate, cPolicy.value)).ToArray();
        }

        public string GetExtraChargeString(extraCharge[] extraCharges)
        {
            var stringBuilder = new StringBuilder();

            foreach (var extraCharge in extraCharges)
            {
                stringBuilder.AppendLine(string.Format("Name:{0}, Code:{1}, Type:{2}, Obligatory:{3}", extraCharge.name, extraCharge.code, extraCharge.type, extraCharge.obligatory));
            }

            return stringBuilder.ToString();
        }
    }
}
