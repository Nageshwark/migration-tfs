﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Helper;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsBookingParser : IAccommodationBookingParser<IEnumerable<SerhsBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public SerhsBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<SerhsBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Serhs Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        SerhsBookingResponse response,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierBookingResponseXML = response.Booking.SupplierResponse.XmlSerialize();
                string supplierConfirmResponseXML = response.Confirm.SupplierResponse.XmlSerialize();

            }
            
            
            
            bool hasResults = ( response.Confirm.SupplierResponse!= null &&
                                response.Confirm.SupplierResponse.status == 0 && 
                                response.Confirm.SupplierResponse.bookings != null &&
                                response.Confirm.SupplierResponse.bookings.Any() &&
                                !string.IsNullOrEmpty(response.Confirm.SupplierResponse.bookings.First().code));

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                result.ProviderBookingReference = response.Confirm.SupplierResponse.bookings.First().code;
                result.ProviderSpecificData = new Dictionary<string, string>();
                //result.ProviderSpecificData.Add("resIDSource", resIdSource);
                    
                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);
                
            }
            else
            {
                var sbMessage = new StringBuilder();
                
                sbMessage.AppendLine("Serhs Booking Parser: Booking Response cannot be parsed because it is null or Booking can not be confirmed.");

                if (response.Confirm.SupplierResponse != null &&
                    response.Confirm.SupplierResponse.bookings != null &&
                    response.Confirm.SupplierResponse.bookings.Any() &&
                    !string.IsNullOrEmpty(response.Confirm.SupplierResponse.bookings.First().errorCode))
                {
                    sbMessage.AppendLine(string.Format("Error Details:{0}", response.Confirm.SupplierResponse.bookings.First().errorCode));
                }
                
                throw new SupplierApiDataException(sbMessage.ToString());
            }
            
        }

       
    }
}
