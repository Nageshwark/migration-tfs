﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Response;


namespace AlphaRooms.Accommodation.Provider.Serhs
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Serhs accepts and returns XML strings.
    /// </summary>
    public class SerhsBookingResponse
    {
        public SerhsBooking Booking { get; set; }
        public SerhsConfirm Confirm { get; set; }
    }

    public class SerhsBooking
    {
        public request SupplierRequest { get; set; }
        public response SupplierResponse { get; set; }
    }

    public class SerhsConfirm
    {
        public Entity.Confirm.Request.request SupplierRequest { get; set; }
        public Entity.Confirm.Response.response SupplierResponse { get; set; }
    }
}
