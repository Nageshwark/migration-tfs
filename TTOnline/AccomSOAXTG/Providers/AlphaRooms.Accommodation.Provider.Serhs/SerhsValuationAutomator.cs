﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsValuationAutomator : IAccommodationValuationAutomatorAsync<SerhsValuationResponse>
    {
        private const string SerhsValuationUrl = "SerhsValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ISerhsProviderValuationRequestFactory _serhsProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public SerhsValuationAutomator( ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ISerhsProviderValuationRequestFactory serhsSupplierValuationRequestFactory,
                                                IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._serhsProviderValuationRequestFactory = serhsSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<SerhsValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            SerhsValuationResponse response = null;

            try
            {
                var supplierRequest = this._serhsProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("SerhsValuationAutomator.GetValuationResponseAsync: Serhs supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<SerhsValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, request supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(SerhsValuationUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<response>();

            var supplierResponse = new SerhsValuationResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
        
    }
}
