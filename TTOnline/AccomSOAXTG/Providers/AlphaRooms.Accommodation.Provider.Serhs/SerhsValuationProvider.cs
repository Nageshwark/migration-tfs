﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Serhs;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<SerhsValuationResponse> automator;
        private readonly IAccommodationValuationParser<SerhsValuationResponse> parser;

        public SerhsValuationProvider(IAccommodationValuationAutomatorAsync<SerhsValuationResponse> automator,
                                                IAccommodationValuationParser<SerhsValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
