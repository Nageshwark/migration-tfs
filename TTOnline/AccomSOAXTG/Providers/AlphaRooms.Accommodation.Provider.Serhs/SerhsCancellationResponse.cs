﻿using AlphaRooms.Accommodation.Provider.Serhs.Entity.Cancel.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Cancel.Response;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsCancellationResponse
    {
        public request SupplierRequest { get; set; }
        public response SupplierResponse { get; set; }
    }
}
