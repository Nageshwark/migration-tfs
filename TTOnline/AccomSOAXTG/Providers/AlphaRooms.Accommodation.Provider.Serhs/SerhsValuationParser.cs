﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Serhs
{

    public class SerhsValuationParser : IAccommodationValuationParser<SerhsValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public SerhsValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, SerhsValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Serhs Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request,  request supplierRequest, response supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {

            bool hasResults = (supplierResponse != null && supplierResponse.accommodations != null && supplierResponse.accommodations.Any());

            if (hasResults)
            {
                var helper = new AvailHelper();

                foreach (var hotel in supplierResponse.accommodations)
                {
                    foreach (var concept in hotel.concepts)
                    {
                        foreach (var board in concept.boards)
                        {
                            bool isNonRefundable = (board.offer != null && board.offer.code == "99999999999999999999999999999999");
                            var isSelectRoom = from AccommodationProviderValuationRequestRoom r in request.SelectedRooms
                                               where r.AvailabilityResult.RoomCode == concept.code && r.AvailabilityResult.BoardCode == board.code && r.AvailabilityResult.IsNonRefundable == isNonRefundable
                                               select r;

                            if (isSelectRoom.Any())
                            {
                                for (int roomCounter = 0; roomCounter < request.SelectedRooms.Length; roomCounter++)
                                {
                                    var result = new AccommodationProviderValuationResult();

                                    result.RoomNumber = (byte) (roomCounter + 1);

                                    result.ProviderEdiCode = request.Provider.EdiCode;
                                    result.SupplierEdiCode = result.ProviderEdiCode;

                                    result.EstablishmentEdiCode = hotel.code;
                                    result.EstablishmentName = hotel.name;
                                    result.DestinationEdiCode = hotel.city.code;

                                    result.BoardCode = board.code;
                                    result.BoardDescription = board.name;
                                    result.RoomCode = concept.code;

                                    result.RoomDescription = concept.name;

                                    //TODO:Review PaymentModel for Serhs
                                    result.PaymentModel = PaymentModel.PostPayment;


                                    //TODO:Review RateType
                                    result.RateType = RateType.NetStandard;


                                    //TODO:Review How we set Nonref
                                    result.IsNonRefundable = isNonRefundable;
                                    result.IsOpaqueRate = (board.offer != null &&
                                                           board.offer.code == "88888888888888888888888888888888");


                                    //result.NumberOfAvailableRooms = 0;

                                    var totalPrice = helper.ParseAmount(board.price.amount);
                                    result.SalePrice = new Money
                                    {
                                        Amount = totalPrice / request.SelectedRooms.Length,
                                        CurrencyCode = board.price.currencyCode.ToString()
                                    };

                                    //TODO:Is this logic Correct?
                                    if (!string.IsNullOrEmpty(board.price.minAmount))
                                    {
                                        result.CostPrice = new Money
                                        {
                                            Amount = helper.ParseAmount(board.price.minAmount) / request.SelectedRooms.Length,
                                            CurrencyCode = board.price.currencyCode.ToString()
                                        };
                                        result.IsBindingRate = true;
                                    }
                                    else
                                        result.CostPrice = result.SalePrice;

                                    //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };


                                    result.Adults = request.SelectedRooms[roomCounter].Guests.AdultsCount;
                                    result.Children = request.SelectedRooms[roomCounter].Guests.ChildrenCount;
                                    result.Infants = request.SelectedRooms[roomCounter].Guests.InfantsCount;

                                    result.CheckInDate = request.SelectedRooms.First().AvailabilityRequest.CheckInDate;
                                    result.CheckOutDate = request.SelectedRooms.First().AvailabilityRequest.CheckOutDate;

                                    result.CancellationPolicy =
                                        helper.GetCancellationPolicyString(concept.cancelPolicies);

                                    result.ProviderSpecificData = new Dictionary<string, string>
                                    {
                                        {"Ticket", board.ticket},
                                        {"ExtraCharges", helper.GetExtraChargeString(board.extraCharges)},
                                        {
                                            "RoomType",
                                            request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData[
                                                "RoomType"]
                                        },
                                    };

                                    if (board.offer != null && !string.IsNullOrEmpty(board.offer.code))
                                        result.ProviderSpecificData.Add("Offers",
                                            string.Format("Type:{0}, Code:{1}, Description: {2}", board.offer.type,
                                                board.offer.code, board.offer.description));

                                    availabilityResults.Enqueue(result);
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                //var error = supplierResponse.Errors;
                
                sbMessage.AppendLine("Serhs Availability Parser: Availability Response cannot be parsed because it is null.");

                //if (error != null && error.Any())
                //    sbMessage.AppendLine(string.Format("Error Details: {0}", error[0].ShortText));


                throw new SupplierApiDataException(sbMessage.ToString());
            }
           
        }
        
    }
}
