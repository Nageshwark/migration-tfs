﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Cancel.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Helper;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsCancellationAutomator : IAccommodationCancellationAutomatorAsync<SerhsCancellationResponse>
    {
        private const string SerhsCancellationUrl = "SerhsCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ISerhsProviderCancellationRequestFactory serhsProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public SerhsCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ISerhsProviderCancellationRequestFactory SerhsSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.serhsProviderCancellationRequestFactory = SerhsSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<SerhsCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            SerhsCancellationResponse response = null;

            try
            {
                var supplierRequest = this.serhsProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                string serialisedRequest = supplierRequest.XmlSerialize();

                serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

                string url = request.Provider.Parameters.GetParameterValue(SerhsCancellationUrl);

                var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

                var supplierResponse = responseXML.Value.XmlDeSerialize<response>();

                response = new SerhsCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("SerhsBookingAutomator.GetCancellationResponseAsync: Serhs supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

    }
}
