﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Response;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsAvailabilityResponse
    {
        public request SupplierRequest { get; set; }
        public response SupplierResponse { get; set; }

    }
}
