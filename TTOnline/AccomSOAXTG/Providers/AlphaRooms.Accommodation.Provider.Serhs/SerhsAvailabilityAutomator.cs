﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Serhs.Entity;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;


namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<SerhsAvailabilityResponse>
    {
        private const string SerhsAvailabilityUrl = "SerhsAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly ISerhsAvailabilityRequestFactory SerhsSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public SerhsAvailabilityAutomator(ISerhsAvailabilityRequestFactory SerhsAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this.SerhsSupplierAvailabilityRequestFactory = SerhsAvailabilityRequestFactory;
            _outputLogger = outputLogger;
        }

        public async Task<SerhsAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            SerhsAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                var supplierRequest = this.SerhsSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("SerhsAvailabilityAutomator.GetAvailabilityResponseAsync: Serhs supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<SerhsAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, request  supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();
            
            serialisedRequest = serialisedRequest.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(SerhsAvailabilityUrl);

            var responseXML = await webScrapeClient.PostAsync(url, serialisedRequest);

            var response = responseXML.Value.XmlDeSerialize<response>();
        
            var supplierResponse = new SerhsAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

    
       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("Serhs supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("Serhs supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("Serhs supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("Serhs supports upto 2 Infants");
            //}
        }
    }
}
