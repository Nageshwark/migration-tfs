﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Serhs.Factories
{
    public class SerhsValuationRequestFactory : ISerhsProviderValuationRequestFactory
    {
        private const string SerhsClientCode = "SerhsClientCode";
        private const string SerhsPassword = "SerhsPassword";

        private const string SerhsAvailVersion = "SerhsAvailVersion";
        private const string SerhsBranch = "SerhsBranch";
        private const string SerhsLanguageCode = "SerhsLanguageCode";
        private const string SerhsPriceType = "SerhsPriceType";
        private const string SerhsRoomType = "SerhsRoomType";

        public request  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            
            uint roomType = Convert.ToUInt32(request.Provider.Parameters.GetParameterValue(SerhsRoomType));

            var supplierRequest = new request
            {
                type = "ACCOMMODATIONS_AVAIL",
                version = request.Provider.Parameters.GetParameterValue(SerhsAvailVersion),
                sessionId = Guid.NewGuid().ToString(),
                client = new client
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsClientCode),
                    branch = request.Provider.Parameters.GetParameterValue(SerhsBranch),
                    password = request.Provider.Parameters.GetParameterValue(SerhsPassword)
                },
                language = new language
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsLanguageCode)
                },
                searchCriteria = CreateSearchCriteria(request),
                period = new period
                {
                    start = Convert.ToUInt32(request.SelectedRooms.First().AvailabilityResult.CheckInDate.ToString("yyyyMMdd")),
                    end = Convert.ToUInt32(request.SelectedRooms.First().AvailabilityResult.CheckOutDate.ToString("yyyyMMdd"))
                },
                rooms = CreateRoomPax(request.SelectedRooms, roomType)
            };


            return supplierRequest;
        }

        private criterionType[] CreateSearchCriteria(AccommodationProviderValuationRequest request)
        {
            var availResult = request.SelectedRooms.First().AvailabilityResult;

            var criterionTypes = new List<criterionType>
            {
                new criterionType
                {
                    type = 0,
                    code = "accommodationCode",
                    value = availResult.EstablishmentEdiCode
                },
                new criterionType
                {
                    type = 2,
                    code = "concept",
                    value = availResult.RoomCode
                },
                new criterionType
                {
                    type = 2,
                    code = "board",
                    value = availResult.BoardCode
                },
                new criterionType
                {
                    type = 2,
                    code = "priceType",
                    value = request.Provider.Parameters.GetParameterValue(SerhsPriceType)
                },
                new criterionType
                {
                    type = 2,
                    code = "cancelPolicies",
                    value = "1"
                }
            };
            

            return criterionTypes.ToArray();
        }

        private roomType[] CreateRoomPax(AccommodationProviderValuationRequestRoom [] rooms, uint roomType)
        {
            var roomTypes = new List<roomType>();
            int roomCounter = 1;
            foreach (var room in rooms)
            {
                roomTypes.Add(new roomType
                {
                    type = roomType,
                    adults = room.Guests.AdultsCount,
                    children = room.Guests.ChildrenCount,
                    child = GetChildren(room),
                    typeSpecified = true,
                    childrenSpecified = room.Guests.ChildrenCount > 0
                });

                roomCounter++;
            }

            return roomTypes.ToArray();
        }

        private child[] GetChildren(AccommodationProviderValuationRequestRoom room)
        {
            var childrens = new List<child>();

            foreach (var guest in room.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant))
            {
                childrens.Add(new child { age = guest.Age });
            }

            return childrens.ToArray();
        }

        
       
    }
}
