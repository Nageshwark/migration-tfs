﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Serhs.Entity;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.Utilities;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.Serhs.Factories
{
    public class SerhsAvailabilityRequestFactory : ISerhsAvailabilityRequestFactory
    {
        private const string SerhsClientCode = "SerhsClientCode";
        private const string SerhsPassword = "SerhsPassword";
        
        private const string SerhsAvailVersion = "SerhsAvailVersion";
        private const string SerhsBranch = "SerhsBranch";
        private const string SerhsLanguageCode = "SerhsLanguageCode";
        private const string SerhsPriceType = "SerhsPriceType";
        private const string SerhsRoomType = "SerhsRoomType";
     

        public request CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            uint roomType = Convert.ToUInt32(request.Provider.Parameters.GetParameterValue(SerhsRoomType)); //Hotel or Apartment Search

            var supplierRequest = new request
            {
                type = "ACCOMMODATIONS_AVAIL",
                version = request.Provider.Parameters.GetParameterValue(SerhsAvailVersion),
                sessionId = Guid.NewGuid().ToString(),
                client = new client
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsClientCode),
                    branch = request.Provider.Parameters.GetParameterValue(SerhsBranch),
                    password = request.Provider.Parameters.GetParameterValue(SerhsPassword)
                },
                language = new language
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsLanguageCode)
                },
                searchCriteria = CreateSearchCriteria(request),
                period = new period
                {
                    start = Convert.ToUInt32(request.CheckInDate.ToString("yyyyMMdd")),
                    end = Convert.ToUInt32(request.CheckOutDate.ToString("yyyyMMdd"))
                },
                rooms = CreateRoomPax(request.Rooms, roomType)
            };


            return supplierRequest;
        }

        private roomType[] CreateRoomPax(AccommodationProviderAvailabilityRequestRoom[] rooms, uint roomType)
        {
            var roomTypes = new List<roomType>();
            int roomCounter = 1;
            foreach (var room in rooms)
            {
                roomTypes.Add(new roomType { 
                    type = roomType,
                    adults = room.Guests.AdultsCount,
                    children = room.Guests.ChildrenCount,
                    child = GetChildren(room),
                    typeSpecified = true,
                    childrenSpecified = room.Guests.ChildrenCount > 0
                });

                roomCounter++;
            }

            return roomTypes.ToArray();
        }

        private child[] GetChildren(AccommodationProviderAvailabilityRequestRoom room)
        {
            var childrens = new List<child>();

            foreach (var guest in room.Guests.Where(guest=>guest.Type == GuestType.Child || guest.Type== GuestType.Infant))
            {
                childrens.Add(new child { age = guest.Age });
            }

            return childrens.ToArray();
        }

        private criterionType[] CreateSearchCriteria(AccommodationProviderAvailabilityRequest request)
        {
            var criterionTypes = new List<criterionType>();
                
            if (request.DestinationCodes.Any())
            {
                string[] codes = request.DestinationCodes[0].Split(';');

                if (codes[0] != "-1")
                {
                    criterionTypes.Add(new criterionType
                    {
                        type = 0,
                        code = "country",
                        value = codes[0]
                    });
                }

                if (codes.Length > 1 && codes[1] != "-1")
                {
                    criterionTypes.Add(new criterionType
                    {
                        type = 0,
                        code = "region",
                        value = codes[1]
                    });
                }

                if (codes.Length > 2 && codes[2] != "-1")
                {
                    criterionTypes.Add(new criterionType
                    {
                        type = 0,
                        code = "area",
                        value = codes[2]
                    });
                }

                if (codes.Length > 3 && codes[3] != "-1")
                {
                    criterionTypes.Add(new criterionType
                    {
                        type = 0,
                        code = "city",
                        value = codes[3]
                    });
                }
            }
            else if (request.EstablishmentCodes.Any())
            {
                criterionTypes = request.EstablishmentCodes.Select(establishmentCode => new criterionType
                {
                    type = 0,
                    code = "accommodationCode",
                    value = establishmentCode
                }).ToList();
            }

            
            criterionTypes.Add(
                new criterionType
                {
                    type = 2,
                    code = "priceType",
                    value = request.Provider.Parameters.GetParameterValue(SerhsPriceType)
                });

            criterionTypes.Add(
                new criterionType
                {
                    type = 2,
                    code = "cancelPolicies",
                    value = "1"
                });


            return criterionTypes.ToArray();
        }


        /*
         * <request type="ACCOMMODATIONS_AVAIL" version="4.1" sessionId="12312312">
          <client code="UKALPH" branch="4409" password="p0ksntw3cv"/>
          <language code="ENG"/>
          <searchCriteria>
            <criterion type="0" code="country" value=""/>
            <criterion type="0" code="area" value=""/>
            <criterion type="0" code="region" value=""/>
            <criterion type="0" code="city" value="59325"/>
            <criterion type="1" code="accommodationCode" value=""/>
            <criterion type="1" code="accommodationType" value="0"/>
            <criterion type="1" code="category" value=""/>
            <criterion type="2" code="priceType" value="3"/>
            <criterion type="2" code="offer" value=""/>
            <criterion type="2" code="concept" value=""/>
            <criterion type="2" code="board" value=""/>
            <criterion type="2" code="cancelPolicies" value="1"/>
          </searchCriteria>
          <period start="20151001" end="20151008"/>
          <rooms>
            <room type="1" adults="2" children="2">
              <child age="5"/>
              <child age="7"/>
            </room>
            <room type="2" adults="2" children="2">
              <child age="5"/>
              <child age="7"/>
            </room>
          </rooms>
        </request>

*/
        

        private string[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => guest.Age.ToString()).ToArray();

        }


        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;
        }

        


    }
}
