﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Response;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Confirm.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using accommodation = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.accommodation;
using adult = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.adult;
using child = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.child;
using client = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.client;
using language = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.language;
using period = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.period;
using preBooking = AlphaRooms.Accommodation.Provider.Serhs.Entity.Confirm.Request.preBooking;
using request = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.request;
using roomType = AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request.roomType;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsProviderBookingRequestFactory : ISerhsProviderBookingRequestFactory
    {
        private const string SerhsClientCode = "SerhsClientCode";
        private const string SerhsPassword = "SerhsPassword";

        private const string SerhsAvailVersion = "SerhsAvailVersion";
        private const string SerhsBranch = "SerhsBranch";
        private const string SerhsLanguageCode = "SerhsLanguageCode";
        private const string SerhsRoomType = "SerhsRoomType";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            uint roomType = Convert.ToUInt32(request.Provider.Parameters.GetParameterValue(SerhsRoomType)); //Hotel or Apartment Search
            var valuationResult = request.ValuatedRooms.First().ValuationResult;

            var supplierRequest = new request
            {
                type = "ACCOMMODATION_BOOKING",
                version = request.Provider.Parameters.GetParameterValue(SerhsAvailVersion),
                sessionId = Guid.NewGuid().ToString(),
                client = new client
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsClientCode),
                    branch = request.Provider.Parameters.GetParameterValue(SerhsBranch),
                    password = request.Provider.Parameters.GetParameterValue(SerhsPassword)
                },
                language = new language
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsLanguageCode)
                },
                period = new period
                {
                    start = Convert.ToUInt32(valuationResult.CheckInDate.ToString("yyyyMMdd")),
                    end = Convert.ToUInt32(valuationResult.CheckOutDate.ToString("yyyyMMdd"))
                },
                accommodation = new accommodation
                {
                    code = valuationResult.EstablishmentEdiCode,
                    concept = valuationResult.RoomCode,
                    board = valuationResult.BoardCode,
                    ticket = valuationResult.ProviderSpecificData["Ticket"],
                },
                rooms = CreateRoomPax(request.ValuatedRooms.First())
            };


            return supplierRequest;

            /*
             * <request type="ACCOMMODATION_BOOKING" version="4.1" sessionId="12312312"> 
             * <client code="[client code]" branch="[branch code]" password="[travel agency password]"/>
                <language code="SPA"/> 
             * <accommodation code="LLCASI" concept="D01" board="HB" offer="" ticket=" 0150LLCASIHBDB00000000000000000000000"/>
                <period start="20080810" end="20080817"/>
                <rooms>
                <room type="1" adults="2" children="2">
                <adult name="Name1" surname="SurName1" dateOfBirth="19700112" documentType="2" document="97897978X"/>
                <adult name="Name2" surname="SurName2" dateOfBirth="19701101" documentType="2" document="65656565E"/>
                <child age="5" name="Name3" surname="SurName3" dateOfBirth="20010828" documentType="0" document=""/>
                <child age="3" name="Name4" surname="SurName4" dateOfBirth="20030415" documentType="0" document=""/>
                </room>
                </rooms>
                </request>
             */
        }

        public Entity.Confirm.Request.request CreateSupplierConfirmRequest(AccommodationProviderBookingRequest request, response bookingResponse)
        {
            var addressDetails = GetAddressDetails(request);

            var supplierRequest = new Entity.Confirm.Request.request
            {
                type = requestTypeType.CONFIRM,
                version = request.Provider.Parameters.GetParameterValue(SerhsAvailVersion),
                sessionId = Guid.NewGuid().ToString(),
                client = new Entity.Confirm.Request.client
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsClientCode),
                    branch = request.Provider.Parameters.GetParameterValue(SerhsBranch),
                    password = request.Provider.Parameters.GetParameterValue(SerhsPassword)
                },
                language = new Entity.Confirm.Request.language
                {
                    code =
                        (languageCode)
                            Enum.Parse(typeof (languageCode),
                                request.Provider.Parameters.GetParameterValue(SerhsLanguageCode))
                },
                agent = new agent
                {
                    name = "",
                    email = ""
                },
                customer = new customerType
                {
                    name = request.Customer.FirstName,
                    surname = request.Customer.Surname,
                    //address = addressDetails.AddressLine1,
                    //city = new customerTypeCity
                    //{
                    //    name = addressDetails.City,
                    //    zipCode = addressDetails.Postcode
                    //},
                    //country = new customerTypeCountry{name = addressDetails.Country},

                },
                preBookings = new []
                {
                    new preBooking
                    {
                        confirmed = 1,
                        type = preBookingType.A,
                        code = bookingResponse.preBooking.code,
                        clientReference = ""
                    }
                }


            };

            return supplierRequest;

            /*
             * <request type="CONFIRM" version="4.1" sessionId="1233413242134"> 
             * <client code="[client code]" branch="[branch code]" password="[travel agency password]"/>
                <language code="SPA"/>
                <agent name="test agent" email="test@test-mail.com"/>
                <customer name="Name" surname="SurName">
                <address>Address</address>
                <city name="City" zipCode="09498"/>
                <region name="Barcelona"/>
                <country name="Spain"/>
                <document type="0" code="7596795X"/>
                <contactInfo email="client@clientemail.com" mobilePhone="00346736787"/>
                <remarks></remarks>
                </customer>
                <preBookings>
                <preBooking confirmed="1" type="A" code="1814652" clientReference=""/>
                <preBooking confirmed="1" type="S" code="1257877" clientReference=""/>
                <preBooking confirmed="1" type="R" code="7658759" clientReference=""/>
                </preBookings>
                </request>
             */
        }

        private AccommodationProviderBookingRequestRoomPaymentDetails GetAddressDetails(AccommodationProviderBookingRequest request)
        {
            AccommodationProviderBookingRequestRoomPaymentDetails paymentDetails;
            if (request.ValuatedRooms.First().PaymentDetails != null)
            {
                paymentDetails = request.ValuatedRooms.First().PaymentDetails;
            }
            else
            {
                //"TODO: Need to implement default alpha address, if no address is defined in request"
                throw new NotImplementedException(
                    "TODO: Need to implement default alpha address, if no address is defined in request");
            }

            return paymentDetails;
        }

        
        private requestRooms CreateRoomPax(AccommodationProviderBookingRequestRoom room)
        {
            var selecteRoom = new requestRooms();
            var roomType = new roomType
            {
                type = Convert.ToUInt32(room.ValuationResult.ProviderSpecificData["RoomType"]), //Hotel or Apartment
                adults = room.Guests.AdultsCount,
                children = room.Guests.ChildrenCount,
                childrenSpecified = room.Guests.ChildrenCount > 0
            };

            var adults = new List<adult>();
            var children = new List<child>();

            foreach (var guest in room.Guests)
            {
                if (guest.Type == GuestType.Adult)
                {
                    adults.Add(new adult
                    {
                        name = string.Format("{0} {1}", guest.FirstName, guest.Surname)
                    });
                }
                else
                {
                    children.Add(new child
                    {
                        name = string.Format("{0} {1}", guest.FirstName, guest.Surname),
                        age = guest.Age
                    });
                }
            }

            roomType.adult = adults.ToArray();
            roomType.child = children.ToArray();

            selecteRoom.room = roomType;


            return selecteRoom;
        }

        
    }
}
