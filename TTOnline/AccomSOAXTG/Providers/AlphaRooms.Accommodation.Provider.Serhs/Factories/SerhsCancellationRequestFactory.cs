﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Cancel.Request;

namespace AlphaRooms.Accommodation.Provider.Serhs.Factories
{
    public class SerhsCancellationRequestFactory : ISerhsProviderCancellationRequestFactory
    {
        private const string SerhsClientCode = "SerhsClientCode";
        private const string SerhsPassword = "SerhsPassword";

        private const string SerhsAvailVersion = "SerhsAvailVersion";
        private const string SerhsBranch = "SerhsBranch";
        private const string SerhsLanguageCode = "SerhsLanguageCode";
        private const string SerhsAgentName = "SerhsAgentName";
        private const string SerhsAgentEmail = "SerhsAgentEmail";

        public request CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new request
            {
                type = "CANCEL",
                version = request.Provider.Parameters.GetParameterValue(SerhsAvailVersion),
                sessionId = Guid.NewGuid().ToString(),
                client = new client
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsClientCode),
                    branch = request.Provider.Parameters.GetParameterValue(SerhsBranch),
                    password = request.Provider.Parameters.GetParameterValue(SerhsPassword)
                },
                language = new language
                {
                    code = request.Provider.Parameters.GetParameterValue(SerhsLanguageCode)
                },
                booking = new booking
                {
                    code = request.ProviderBookingReference,
                    confirmed = 1
                },
                agentInfo = new agentInfo
                {
                    name = request.Provider.Parameters.GetParameterValue(SerhsAgentName),
                    email = request.Provider.Parameters.GetParameterValue(SerhsAgentEmail)
                }

            };

            return supplierRequest;

            /*
            <request type="CANCEL" version="4.1" sessionId="12312312"> 
             * <client code="[client code]" branch="[branch code]" password="[travel agency password]"/>
            <language code="SPA"/>
            <booking code="4312342" confirmed="0" clientReference="[recomendado]"/>
            <agentInfo name="Email Agent" email="email.agente@serhstourim.com" phone="0034939898"/>
            <remarks/>
            </request>
             */
        }
    }
}
