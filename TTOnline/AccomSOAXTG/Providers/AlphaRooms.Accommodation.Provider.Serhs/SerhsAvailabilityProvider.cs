﻿using AlphaRooms.Accommodation.Provider.Serhs;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Serhs;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Serhs
{
    public class SerhsAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<SerhsAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<SerhsAvailabilityResponse> parser;

        public SerhsAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<SerhsAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<SerhsAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
