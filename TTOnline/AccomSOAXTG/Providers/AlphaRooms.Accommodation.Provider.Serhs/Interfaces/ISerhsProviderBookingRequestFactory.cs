﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Request;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Booking.Response;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.Serhs.Interfaces
{
    public interface ISerhsProviderBookingRequestFactory
    {
        request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);

        Entity.Confirm.Request.request CreateSupplierConfirmRequest(AccommodationProviderBookingRequest request, response bookingResponse);
    }
}
