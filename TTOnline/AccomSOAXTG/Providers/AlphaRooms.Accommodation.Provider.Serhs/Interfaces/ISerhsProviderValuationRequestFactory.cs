﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Serhs.Entity.Avail.Request;


namespace AlphaRooms.Accommodation.Provider.Serhs.Interfaces
{
    public interface ISerhsProviderValuationRequestFactory
    {
        request CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
