﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.Serhs.Entity.Cancel.Response
{
    //------------------------------------------------------------------------------
    // <auto-generated>
    //     This code was generated by a tool.
    //     Runtime Version:4.0.30319.42000
    //
    //     Changes to this file may cause incorrect behavior and will be lost if
    //     the code is regenerated.
    // </auto-generated>
    //------------------------------------------------------------------------------

    

    // 
    // This source code was auto-generated by xsd, Version=4.6.81.0.
    // 


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class response : responseType
    {

        private cancel cancelField;

        private booking bookingField;

        private officeContact officeContactField;

        private cancelPolicy[] cancelPoliciesField;

        private amount_detailsType amount_detailsField;

        private responseType1 typeField;

        private bool typeFieldSpecified;

        private string versionField;

        /// <remarks/>
        public cancel cancel
        {
            get
            {
                return this.cancelField;
            }
            set
            {
                this.cancelField = value;
            }
        }

        /// <remarks/>
        public booking booking
        {
            get
            {
                return this.bookingField;
            }
            set
            {
                this.bookingField = value;
            }
        }

        /// <remarks/>
        public officeContact officeContact
        {
            get
            {
                return this.officeContactField;
            }
            set
            {
                this.officeContactField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("cancelPolicy", IsNullable = false)]
        public cancelPolicy[] cancelPolicies
        {
            get
            {
                return this.cancelPoliciesField;
            }
            set
            {
                this.cancelPoliciesField = value;
            }
        }

        /// <remarks/>
        public amount_detailsType amount_details
        {
            get
            {
                return this.amount_detailsField;
            }
            set
            {
                this.amount_detailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public responseType1 type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool typeSpecified
        {
            get
            {
                return this.typeFieldSpecified;
            }
            set
            {
                this.typeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class cancel
    {

        private string codeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class responseType
    {

        private uint statusField;

        /// <remarks/>
        public uint status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class cancelPolicy
    {

        private string offerIdField;

        private uint releaseDateField;

        private bool releaseDateFieldSpecified;

        private string releaseHourField;

        private uint typeField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string offerId
        {
            get
            {
                return this.offerIdField;
            }
            set
            {
                this.offerIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint releaseDate
        {
            get
            {
                return this.releaseDateField;
            }
            set
            {
                this.releaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool releaseDateSpecified
        {
            get
            {
                return this.releaseDateFieldSpecified;
            }
            set
            {
                this.releaseDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string releaseHour
        {
            get
            {
                return this.releaseHourField;
            }
            set
            {
                this.releaseHourField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class booking
    {

        private bookingCancelled cancelledField;

        private string codeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bookingCancelled cancelled
        {
            get
            {
                return this.cancelledField;
            }
            set
            {
                this.cancelledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum bookingCancelled
    {

        /// <remarks/>
        yes,

        /// <remarks/>
        no,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class officeContact
    {

        private string nameField;

        private string emailField;

        private string phoneField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("amount_details", Namespace = "", IsNullable = false)]
    public partial class amount_detailsType
    {

        private string nett_amountField;

        private string commission_percentField;

        private string commission_amountField;

        private string commission_tax_percentField;

        private string commission_tax_amountField;

        private string your_margin_percentField;

        private string your_margin_amountField;

        private string total_amountField;

        private amount_detailsTypeCurrencyCode currencyCodeField;

        private bool currencyCodeFieldSpecified;

        /// <remarks/>
        public string nett_amount
        {
            get
            {
                return this.nett_amountField;
            }
            set
            {
                this.nett_amountField = value;
            }
        }

        /// <remarks/>
        public string commission_percent
        {
            get
            {
                return this.commission_percentField;
            }
            set
            {
                this.commission_percentField = value;
            }
        }

        /// <remarks/>
        public string commission_amount
        {
            get
            {
                return this.commission_amountField;
            }
            set
            {
                this.commission_amountField = value;
            }
        }

        /// <remarks/>
        public string commission_tax_percent
        {
            get
            {
                return this.commission_tax_percentField;
            }
            set
            {
                this.commission_tax_percentField = value;
            }
        }

        /// <remarks/>
        public string commission_tax_amount
        {
            get
            {
                return this.commission_tax_amountField;
            }
            set
            {
                this.commission_tax_amountField = value;
            }
        }

        /// <remarks/>
        public string your_margin_percent
        {
            get
            {
                return this.your_margin_percentField;
            }
            set
            {
                this.your_margin_percentField = value;
            }
        }

        /// <remarks/>
        public string your_margin_amount
        {
            get
            {
                return this.your_margin_amountField;
            }
            set
            {
                this.your_margin_amountField = value;
            }
        }

        /// <remarks/>
        public string total_amount
        {
            get
            {
                return this.total_amountField;
            }
            set
            {
                this.total_amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public amount_detailsTypeCurrencyCode currencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool currencyCodeSpecified
        {
            get
            {
                return this.currencyCodeFieldSpecified;
            }
            set
            {
                this.currencyCodeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum amount_detailsTypeCurrencyCode
    {

        /// <remarks/>
        EUR,

        /// <remarks/>
        USD,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum responseType1
    {

        /// <remarks/>
        ACCOMMODATIONS_AVAIL,

        /// <remarks/>
        ACCOMMODATION_BOOKING,

        /// <remarks/>
        CONFIRM,

        /// <remarks/>
        CANCEL,

        /// <remarks/>
        AREAS,

        /// <remarks/>
        DESTINATIONS,

        /// <remarks/>
        ACCOMMODATIONS,
    }
}
