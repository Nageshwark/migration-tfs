﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Request;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Response;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.ViajesOlympiaService;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaAvailabilityAutomator : ViajesOlympiaAutomatorBase, IAccommodationAvailabilityAutomatorAsync<ViajesOlympiaAvailabilityResponse>
    {
        private const string ViajesOlympiaUrl = "ViajesOlympiaUrl";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IViajesOlympiaAvailabilityRequestFactory _viajesOlympiaSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;


        public ViajesOlympiaAvailabilityAutomator(IViajesOlympiaAvailabilityRequestFactory viajesOlympiaAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            _viajesOlympiaSupplierAvailabilityRequestFactory = viajesOlympiaAvailabilityRequestFactory;
            _outputLogger = outputLogger;
        }

        public async Task<ViajesOlympiaAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            ViajesOlympiaAvailabilityResponse response = null;

            try
            {
                //ValidateRequest(request);

                DevuelveDisponibilidadPB supplierRequest = this._viajesOlympiaSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("ViajesOlympiaAvailabilityAutomator.GetAvailabilityResponseAsync: ViajesOlympia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<ViajesOlympiaAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, DevuelveDisponibilidadPB supplierRequest)
        {
            ServiceSoapClient client = ConfigureSoapCient(request.Provider.Parameters);
            string availRes = client.DevuelveDisponibilidadPB(supplierRequest.User, supplierRequest.password, supplierRequest.Pais, supplierRequest.provincia, supplierRequest.Poblacion, supplierRequest.Categoria, supplierRequest.Codigo, supplierRequest.FechaEntradaSt, Convert.ToInt32(supplierRequest.Noches), supplierRequest.ListHab, supplierRequest.Opciones);
            
            //availRes = availRes.Replace("<Respuesta>", "").Replace("</Respuesta>", "").Trim();
            //var response = availRes.XmlDeSerialize<Disponibilidad>();
            var response = availRes.XmlDeSerialize<Respuesta>();

            var supplierResponse = new ViajesOlympiaAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

    }
}
