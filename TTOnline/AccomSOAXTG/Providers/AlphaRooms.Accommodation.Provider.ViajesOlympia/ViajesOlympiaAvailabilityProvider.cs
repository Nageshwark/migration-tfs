﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ViajesOlympiaAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<ViajesOlympiaAvailabilityResponse> parser;

        public ViajesOlympiaAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ViajesOlympiaAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ViajesOlympiaAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
