﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces
{
    public interface IViajesOlympiaBookingRequestFactory
    {
        ReservaEnFirmePB CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
    }
}
