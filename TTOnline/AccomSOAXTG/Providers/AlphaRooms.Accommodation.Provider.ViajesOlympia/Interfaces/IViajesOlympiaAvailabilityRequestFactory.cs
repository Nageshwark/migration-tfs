﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces
{
    public interface IViajesOlympiaAvailabilityRequestFactory
    {
        DevuelveDisponibilidadPB CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
