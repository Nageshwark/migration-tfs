﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Request;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces
{
    public interface IViajesOlympiaProviderCancellationRequestFactory
    {
        CancelaReserva CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
