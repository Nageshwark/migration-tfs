﻿

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaValuationResponse
    {
        public DevuelveDisponibilidadPB SupplierRequest { get; set; }
        public Disponibilidad SupplierResponse { get; set; }
    }
}
