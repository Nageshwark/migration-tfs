﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViajesOlympia;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ViajesOlympiaValuationResponse> automator;
        private readonly IAccommodationValuationParser<ViajesOlympiaValuationResponse> parser;

        public ViajesOlympiaValuationProvider(IAccommodationValuationAutomatorAsync<ViajesOlympiaValuationResponse> automator,
                                                IAccommodationValuationParser<ViajesOlympiaValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
