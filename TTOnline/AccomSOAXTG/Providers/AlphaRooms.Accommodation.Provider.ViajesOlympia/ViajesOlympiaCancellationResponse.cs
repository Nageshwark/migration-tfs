﻿

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaCancellationResponse
    {
        public CancelaReserva SupplierRequest { get; set; }
        public CancelaReservaResult SupplierResponse { get; set; }
    }
}
