﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<ViajesOlympiaCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<ViajesOlympiaCancellationResponse> parser;

        public ViajesOlympiaCancellationProvider(IAccommodationCancellationAutomatorAsync<ViajesOlympiaCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<ViajesOlympiaCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
}
