﻿using System;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaAvailabilityParser : IAccommodationAvailabilityParser<ViajesOlympiaAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private const string ViajesOlympiaCurrency = "ViajesOlympiaCurrency";
        public ViajesOlympiaAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, ViajesOlympiaAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                if (request.Debugging)
                {
                    string serialisedResponse = response.SupplierResponse.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesOlympia Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, ViajesOlympiaAvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                foreach (var hotel in supplierResponse.SupplierResponse.Disponibilidad.Hoteles)
                {
                    foreach (var roomInfo in hotel.HabitacionesList)
                    {
                        foreach (var roomType in roomInfo.DatosHabitacion)
                        {
                            if (roomType.disponible == "OK")
                            {
                                
                                var result = new AccommodationProviderAvailabilityResult();

                                result.RoomNumber = Convert.ToByte(roomInfo.Ord);
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;

                                result.DestinationEdiCode = hotel.CodigoProvincia;
                                result.EstablishmentEdiCode = hotel.Codigo.ToString();
                                result.EstablishmentName = hotel.Nombre;

                                result.RoomDescription = roomType.Servicio;
                                result.RoomCode = roomType.CodigoServicio;

                                //result.CheckInDate = Convert.ToDateTime(hotel.FechaEntrada.Replace(" 0:00:00", ""));
                                //result.CheckOutDate = Convert.ToDateTime(hotel.FechaSalida.Replace(" 0:00:00", ""));

                                result.CheckInDate = request.CheckInDate;
                                result.CheckOutDate = request.CheckOutDate;

                                result.BoardCode = roomType.regimen;
                                result.BoardDescription = roomType.regimen;
                                result.SalePrice = new Money()
                                {
                                    Amount = Convert.ToDecimal(roomType.precio),
                                    CurrencyCode = request.Provider.Parameters.GetParameterValue(ViajesOlympiaCurrency)
                                };

                                result.CostPrice = result.SalePrice;

                                result.Adults = roomInfo.Adultos ;
                                result.Children = roomInfo.ninos.num;

                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                result.IsOpaqueRate = false;
                                result.IsNonRefundable = false;
                                result.NumberOfAvailableRooms = 0;
                                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("ViajesOlympiaCodigoPeticion", supplierResponse.SupplierResponse.Disponibilidad.CodigoPeticion);

                                availabilityResults.Enqueue(result);
                            }
                        }

                    }
                }
            }
        }

        private bool HasResults(Respuesta supplierResponse)
        {

            if (supplierResponse != null && supplierResponse.Disponibilidad !=null && supplierResponse.Disponibilidad.Hoteles != null)
            {
                if (supplierResponse.Disponibilidad.Hoteles.Any())
                    return true;
                else
                    return false;
            }

            return false;
        }

    }
}
