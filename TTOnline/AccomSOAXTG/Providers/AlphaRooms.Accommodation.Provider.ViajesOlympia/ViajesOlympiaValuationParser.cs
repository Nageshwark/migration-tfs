﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{

    public class ViajesOlympiaValuationParser : IAccommodationValuationParser<ViajesOlympiaValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private const string ViajesOlympiaCurrency = "ViajesOlympiaCurrency";

        public ViajesOlympiaValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, ViajesOlympiaValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    //string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesOlympia Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, ViajesOlympiaValuationResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                var hotel = supplierResponse.SupplierResponse.Hoteles.First();

                foreach (var room in request.SelectedRooms)
                {
                    var roomInfo = from r in hotel.HabitacionesList
                        where r.Ord == room.RoomNumber
                        select r;

                    var matchedRoom = from ri in roomInfo.First().DatosHabitacion
                        where
                            ri.CodigoServicio == room.AvailabilityResult.RoomCode &&
                            ri.regimen == room.AvailabilityResult.BoardCode && ri.disponible.ToUpper() == "OK"
                        select ri;

                    if (matchedRoom.Any())
                    {
                        var roomType = matchedRoom.First();
                        var result = new AccommodationProviderValuationResult();

                        result.RoomNumber = room.RoomNumber;
                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.SupplierEdiCode = request.Provider.EdiCode;

                        result.DestinationEdiCode = hotel.CodigoProvincia;
                        result.EstablishmentEdiCode = hotel.Codigo.ToString();
                        result.EstablishmentName = hotel.Nombre;

                        result.RoomDescription = roomType.Servicio;
                        result.RoomCode = roomType.CodigoServicio;

                        result.CheckInDate = room.AvailabilityResult.CheckInDate;
                        result.CheckOutDate = room.AvailabilityResult.CheckOutDate;

                        result.BoardCode = roomType.regimen;
                        result.BoardDescription = roomType.regimen;
                        result.SalePrice = new Money()
                        {
                            Amount = Convert.ToDecimal(roomType.precio),
                            CurrencyCode = request.Provider.Parameters.GetParameterValue(ViajesOlympiaCurrency)
                        };

                        result.CostPrice = result.SalePrice;

                        result.Adults = roomInfo.First().Adultos;
                        result.Children = roomInfo.First().ninos.num;

                        result.PaymentModel = PaymentModel.PostPayment;
                        result.RateType = RateType.NetStandard;
                        result.IsOpaqueRate = false;
                        result.IsNonRefundable = false;
                        result.NumberOfAvailableRooms = 0;
                        //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("ViajesOlympiaCodigoPeticion", supplierResponse.SupplierResponse.CodigoPeticion);

                        availabilityResults.Enqueue(result);
                    }


                }
               
                
            }
        }

        private bool HasResults(Disponibilidad supplierResponse)
        {
            return  supplierResponse != null && 
                    supplierResponse.Hoteles != null && 
                    supplierResponse.Hoteles.Any();
        }
    }
}
