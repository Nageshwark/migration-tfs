﻿
namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaAvailabilityResponse
    {
        public DevuelveDisponibilidadPB SupplierRequest { get; set; }
        public Respuesta SupplierResponse { get; set; }
    }
}
