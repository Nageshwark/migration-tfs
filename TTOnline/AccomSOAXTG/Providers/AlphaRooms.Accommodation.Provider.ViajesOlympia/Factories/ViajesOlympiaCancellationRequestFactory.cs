﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using System;
using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper;


namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories
{
    public class ViajesOlympiaCancellationRequestFactory : ViajesOlympiaProviderFactoryBase, IViajesOlympiaProviderCancellationRequestFactory
    {
        private const string ViajesOlympiaUserName = "ViajesOlympiaUserName";
        private const string ViajesOlympiaPassword = "ViajesOlympiaPassword";
        public CancelaReserva CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            CancelaReserva canRequest = new CancelaReserva()
            {
                User = request.Provider.Parameters.GetParameterValue(ViajesOlympiaUserName),
                password = request.Provider.Parameters.GetParameterValue(ViajesOlympiaPassword),
                IdReserva = request.ProviderBookingReference,
                TipoSolicitud = "XI",
                FechaCancelacion = DateTime.Today.ToString("dd/MM/yyyy"),
                Opciones = ""
            };

            return canRequest;
        }
    }
}
