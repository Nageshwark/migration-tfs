﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper;
using System.Collections.Generic;
using System.Linq;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories
{
    public class ViajesOlympiaValuationRequestFactory: ViajesOlympiaProviderFactoryBase, IViajesOlympiaValuationRequestFactory
    {
        private const string ViajesOlympiaUserName = "ViajesOlympiaUserName";
        private const string ViajesOlympiaPassword = "ViajesOlympiaPassword";

        public DevuelveDisponibilidadPB CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request)
        {
            var avilRequest = request.SelectedRooms.First().AvailabilityRequest;
            var hotelCode = request.SelectedRooms.First().AvailabilityResult.EstablishmentEdiCode;

            var helper = new ViajesOlympiaHelper();

            var availRequest = new DevuelveDisponibilidadPB()
            {
                User = request.Provider.Parameters.GetParameterValue(ViajesOlympiaUserName),
                password = request.Provider.Parameters.GetParameterValue(ViajesOlympiaPassword),
                Pais = "",
                provincia = "",
                Poblacion = "",
                Categoria = "",
                Codigo = "<Proveedores><Proveedor>" + hotelCode + "</Proveedor></Proveedores>",
                FechaEntradaSt = avilRequest.CheckInDate.ToString("dd/MM/yyyy"),
                Noches = (avilRequest.CheckOutDate - avilRequest.CheckInDate).TotalDays.ToString(),
                ListHab = helper.GetRoomInfoArray(request),
                Opciones = helper.GetOptions(request.Provider.Parameters)
            };

            return availRequest;
        }

    }
}
