﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper;


namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories
{
    public class ViajesOlympiaAvailabilityRequestFactory : IViajesOlympiaAvailabilityRequestFactory
    {
        private const string ViajesOlympiaUserName = "ViajesOlympiaUserName";
        private const string ViajesOlympiaPassword = "ViajesOlympiaPassword";

        public DevuelveDisponibilidadPB CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var helper = new ViajesOlympiaHelper();

            var availRequest = new DevuelveDisponibilidadPB()
            {
                User = request.Provider.Parameters.GetParameterValue(ViajesOlympiaUserName),
                password = request.Provider.Parameters.GetParameterValue(ViajesOlympiaPassword),
                Pais= request.DestinationCodes!=null && request.DestinationCodes.Any() ? request.DestinationCodes.First().Split(';')[0] : "",
                provincia= request.DestinationCodes!=null && request.DestinationCodes.Any() ? request.DestinationCodes.First().Split(';')[1] : "",
                Poblacion="",
                Categoria="",
                Codigo = helper.GetHotelCodes(request),
                FechaEntradaSt = request.CheckInDate.ToString("dd/MM/yyyy"),
                Noches = (request.CheckOutDate - request.CheckInDate).TotalDays.ToString(),
                ListHab = helper.GetRoomInfoArray(request),
                Opciones = helper.GetOptions(request.Provider.Parameters)
            };

            return availRequest;
        }


    }

}

