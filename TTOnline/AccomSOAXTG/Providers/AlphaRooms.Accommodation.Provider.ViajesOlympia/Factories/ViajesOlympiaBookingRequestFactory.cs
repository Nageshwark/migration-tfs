﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper;



namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories
{
    public class ViajesOlympiaBookingRequestFactory : ViajesOlympiaProviderFactoryBase, IViajesOlympiaBookingRequestFactory
    {
        private const string ViajesOlympiaUserName = "ViajesOlympiaUserName";
        private const string ViajesOlympiaPassword = "ViajesOlympiaPassword";
        private const string ViajesOlympiaCurrencyCode = "ViajesOlympiaCurrencyCode";
        private const string ViajesOlympiaCodigoPeticion = "ViajesOlympiaCodigoPeticion";

        public ReservaEnFirmePB CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            var helper = new ViajesOlympiaHelper();
            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms.First().ValuationResult;
            ReservaEnFirmePB supplierRequest = new ReservaEnFirmePB()
            {
                User = request.Provider.Parameters.GetParameterValue(ViajesOlympiaUserName),
                password = request.Provider.Parameters.GetParameterValue(ViajesOlympiaPassword),
                CodigoProveedor = roomToBook.EstablishmentEdiCode.ToString(),
                FechaEntradaSt = roomToBook.CheckInDate.ToString("dd/MM/yyyy"),
                Noches = (roomToBook.CheckOutDate - roomToBook.CheckInDate).TotalDays.ToString(),
                Titular = request.ValuatedRooms.First().Guests[0].FirstName + " " + request.ValuatedRooms.First().Guests[0].Surname,
                DNI = "",
                ListHab = helper.GetRoomInfoArray(request.ValuatedRooms),
                CodigoPeticion = (roomToBook.ProviderSpecificData.ContainsKey(ViajesOlympiaCodigoPeticion) ? roomToBook.ProviderSpecificData[ViajesOlympiaCodigoPeticion] : "0"),
                Localizadorcte = request.BookingId.ToString(),
                Opciones = helper.GetOptions(request.Provider.Parameters)
            };

            return supplierRequest;
        }

    }
}
