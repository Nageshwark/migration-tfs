﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper
{
    public class ViajesOlympiaHelper : ViajesOlympiaProviderFactoryBase
    {
        private const string ViajesOlympiaRoomTypeCode = "ViajesOlympiaRoomTypeCode";
        private const string ViajesOlympiaBedTypeCode = "ViajesOlympiaBedTypeCode";

        public string GetRoomInfoArray(AccommodationProviderBookingRequestRoom[] rooms)
        {
            var listHab = new StringBuilder();
            listHab.Append("<HabitacionesList>");

            foreach (var room in rooms)
            {
                listHab.AppendFormat("<Habitacion Ord=\"{0}\"><CodigoServicio>{1}</CodigoServicio></Habitacion>",
                     room.ValuationResult.RoomNumber.ToString(), room.ValuationResult.RoomCode);
            }

            listHab.Append("</HabitacionesList>");
            return listHab.ToString();
        }

        public string GetRoomInfoArray(AccommodationProviderAvailabilityRequest request)
        {
            var listHab = new StringBuilder();
            listHab.Append("<HabitacionesList>");

            foreach (var room in request.Rooms)
            {
                listHab.AppendFormat("<Habitacion Ord=\"{0}\"><Adultos>{1}</Adultos><ninos num=\"{2}\">{3}</ninos><NumHab>1</NumHab><Regimen>ALL</Regimen></Habitacion>",
                     room.RoomNumber, room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount, GetChildrenAges(room.Guests));
            }

            listHab.Append("</HabitacionesList>");
            return listHab.ToString();
        }

        public string GetRoomInfoArray(AccommodationProviderValuationRequest request)
        {
            var listHab = new StringBuilder();
            listHab.Append("<HabitacionesList>");

            foreach (var room in request.SelectedRooms)
            {
                listHab.AppendFormat("<Habitacion Ord=\"{0}\"><Adultos>{1}</Adultos><ninos num=\"{2}\">{3}</ninos><NumHab>1</NumHab><Regimen>ALL</Regimen></Habitacion>",
                     room.RoomNumber, room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount, GetChildrenAges(room.Guests));
            }

            listHab.Append("</HabitacionesList>");
            return listHab.ToString();
        }

        public string GetChildrenAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            var edad = new StringBuilder();
            if (guests.ChildrenAndInfantsCount > 0)
            {
                foreach (byte age in guests.ChildAndInfantAges)
                {
                    edad.AppendFormat("<edad>{0}</edad>", age);
                }
            }

            return edad.ToString();
        }
        

        public string GetHotelCodes(AccommodationProviderAvailabilityRequest request)
        {
            var sbHotels = new StringBuilder();
           
            if (request.EstablishmentCodes.Any())
            {
                sbHotels.Append("<Proveedores>");
                foreach (string hotelCode in request.EstablishmentCodes)
                {
                    sbHotels.AppendFormat("<Proveedor>{0}</Proveedor>", hotelCode);
                }
                sbHotels.Append("</Proveedores>");
            }

            return sbHotels.ToString();
        }


        public string GetOptions(List<AccommodationProviderParameter> parameters)
        {
            var sbOptions = new StringBuilder();

            sbOptions.AppendLine("<![CDATA[");
            sbOptions.AppendLine(string.Format("<PermitirRQ>{0}</PermitirRQ>", "Si"));  //Pass it in as Provider Parameter
            sbOptions.AppendLine(string.Format("<MostrarGtosClx>{0}</MostrarGtosClx>", "Si"));
            sbOptions.AppendLine(string.Format("<MostrarPVP>{0}</MostrarPVP>", "Si"));
            sbOptions.AppendLine(string.Format("<MostrarOfertasMayores55>{0}</MostrarOfertasMayores55>", "Si"));
            sbOptions.AppendLine(string.Format("<MostrarRestricciones>{0}</MostrarRestricciones>", "Si"));
            sbOptions.AppendLine(string.Format("<Idioma>{0}</Idioma>", "02"));
            sbOptions.AppendLine(string.Format("<MostrarTiposOfertas>{0}</MostrarTiposOfertas>", "Si"));
            sbOptions.AppendLine(string.Format("<MostrarEscapadas>{0}</MostrarEscapadas>", "Si"));
            sbOptions.AppendLine("]]>");

            return sbOptions.ToString();
        }

       
    }
}
