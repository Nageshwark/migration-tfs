﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Request;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Response;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.ViajesOlympiaService;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaCancellationAutomator : ViajesOlympiaAutomatorBase, IAccommodationCancellationAutomatorAsync<ViajesOlympiaCancellationResponse>
    {

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesOlympiaProviderCancellationRequestFactory ViajesOlympiaProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ViajesOlympiaCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesOlympiaProviderCancellationRequestFactory ViajesOlympiaSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.ViajesOlympiaProviderCancellationRequestFactory = ViajesOlympiaSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<ViajesOlympiaCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            List<ViajesOlympiaCancellationResponse> responses = new List<ViajesOlympiaCancellationResponse>();
            ViajesOlympiaCancellationResponse response = null;
            try
            {
                CancelaReserva resRequest = ViajesOlympiaProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                response = await GetSupplierCancellationResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesOlympiaCancellationAutomator.GetCancellationResponseAsync: ViajesOlympia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ViajesOlympiaCancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, CancelaReserva supplierRequest)
        {
            ServiceSoapClient client = ConfigureSoapCient(request.Provider.Parameters);
            string cancelRes = client.CancelaReserva(supplierRequest.User, supplierRequest.password, supplierRequest.IdReserva, supplierRequest.TipoSolicitud, supplierRequest.FechaCancelacion, supplierRequest.Opciones);
            var response = cancelRes.XmlDeSerialize<CancelaReservaResult>();

            var supplierResponse = new ViajesOlympiaCancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
