﻿
namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaBookingResponse
    {
        public ReservaEnFirmePB SupplierRequest { get; set; }
        public Reserva SupplierResponse { get; set; }
    }
}
