﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Response;
namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaBookingParser : IAccommodationBookingParser<ViajesOlympiaBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesOlympiaBookingParser(ILogger logger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, ViajesOlympiaBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                CreateBookingResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, bookingResults);

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesOlympia Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                ReservaEnFirmePB supplierRequest,
                                                Reserva supplierResponse,
                                                ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null
                               );

            if (hasResults && supplierResponse.LocalizadorOlympia != null)
            {
                string rgId = supplierResponse.LocalizadorOlympia;

                foreach (var res in supplierResponse.Hoteles.Hotel.HabitacionesList)
                {
                    var result = new AccommodationProviderBookingResult();

                    result.BookingStatus = BookingStatus.Confirmed;
                    result.ProviderBookingReference = rgId.ToString();
                    result.Message = "";
                    
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("ViajesOlympiaRoomReservationId_" + res.Ord, res.DatosHabitacion.CodigoServicio.ToString());

                    if (res.DatosHabitacion.Cancelaciones !=null)
                        result.ProviderSpecificData.Add("ViajesOlympiaRoomCancellationInfo_" + res.Ord, res.DatosHabitacion.Cancelaciones.XmlSerialize());

                    result.RoomNumber = res.Ord;

                    bookingResults.Enqueue(result);

                }

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("ViajesOlympia Booking Parser: Error in Boooking.");

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }
    }
}
