﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Request;
//using AlphaRooms.Accommodation.Provider.ViajesOlympia.Service.Response;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.ViajesOlympiaService;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaBookingAutomator : ViajesOlympiaAutomatorBase, IAccommodationBookingAutomatorAsync<ViajesOlympiaBookingResponse>
    {
        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesOlympiaBookingRequestFactory _viajesOlympiaSupplierBookingRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViajesOlympiaBookingAutomator(IViajesOlympiaBookingRequestFactory viajesOlympiaSupplierBookingRequestFactory, IProviderOutputLogger outputLogger )
        {
            
            //this.configurationManager = configurationManager;
            this._viajesOlympiaSupplierBookingRequestFactory = viajesOlympiaSupplierBookingRequestFactory;
            this._outputLogger = outputLogger;
        }


        private async Task<ViajesOlympiaBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, ReservaEnFirmePB supplierRequest)
        {
            ServiceSoapClient client = ConfigureSoapCient(request.Provider.Parameters);

            string bookRes = client.ReservaEnFirmePB(supplierRequest.User, supplierRequest.password, supplierRequest.CodigoProveedor, supplierRequest.FechaEntradaSt, Convert.ToInt32(supplierRequest.Noches), supplierRequest.Titular, supplierRequest.DNI, supplierRequest.ListHab, supplierRequest.CodigoPeticion, supplierRequest.Localizadorcte, supplierRequest.Opciones);
            
            bookRes = bookRes.Replace("<Respuesta>", "").Replace("</Respuesta>", "").Trim();
            var response = bookRes.XmlDeSerialize<Reserva>();

            var supplierResponse = new ViajesOlympiaBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }


        public async Task<ViajesOlympiaBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ViajesOlympiaBookingResponse> responses = new List<ViajesOlympiaBookingResponse>();
            ViajesOlympiaBookingResponse response = null;
            try
            {
                ReservaEnFirmePB resRequest = _viajesOlympiaSupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
                response = await GetSupplierBookingResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesOlympiaBookingAutomator.GetBookingResponseAsync: ViajesOlympia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }
    }
}
