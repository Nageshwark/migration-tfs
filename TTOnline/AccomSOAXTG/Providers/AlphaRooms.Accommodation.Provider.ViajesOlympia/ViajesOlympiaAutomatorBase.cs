﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.WebScraping;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.ViajesOlympiaService;
using System.ServiceModel;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Helper;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaAutomatorBase
    {
        private const string ViajesOlympiaAvailabilityUrl = "ViajesOlympiaAvailabilityUrl";
        private const string ViajesOlympiaValuationUrl = "ViajesOlympiaValuationUrl";
        private const string ViajesOlympiaBookingUrl = "ViajesOlympiaBookingUrl";
        private const string ViajesOlympiaCancellationUrl = "ViajesOlympiaCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string ViajesOlympiaSendTimeOut = "ViajesOlympiaSendTimeOut";

        public ServiceSoapClient ConfigureSoapCient(List<AccommodationProviderParameter> parameters)
        {
            string url = GetUrl(parameters);
            string messageSize = parameters.GetParameterValue(MaxReceivedMessageSize);
            int timeOut = Convert.ToInt32(parameters.GetParameterValue(ViajesOlympiaSendTimeOut));

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(messageSize);
            binding.SendTimeout = new TimeSpan(0, timeOut, 0);

            EndpointAddress address = new EndpointAddress(new Uri(url));
            ServiceSoapClient client = new ServiceSoapClient(binding, address);

            return client;
        }

        private string GetUrl(List<AccommodationProviderParameter> parameters)
        {
           if (GetType() == typeof(ViajesOlympiaAvailabilityAutomator))
                return parameters.GetParameterValue(ViajesOlympiaAvailabilityUrl);
            else if (GetType() == typeof(ViajesOlympiaValuationAutomator))
                return parameters.GetParameterValue(ViajesOlympiaValuationUrl);
            else if (GetType() == typeof(ViajesOlympiaBookingAutomator))
                return parameters.GetParameterValue(ViajesOlympiaBookingUrl);
            else if (GetType() == typeof(ViajesOlympiaCancellationAutomator))
                return parameters.GetParameterValue(ViajesOlympiaCancellationUrl);

            return "";
        }
    }
}
