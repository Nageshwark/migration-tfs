﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.ViajesOlympiaService;

namespace AlphaRooms.Accommodation.Provider.ViajesOlympia
{
    public class ViajesOlympiaValuationAutomator : ViajesOlympiaAutomatorBase, IAccommodationValuationAutomatorAsync<ViajesOlympiaValuationResponse>
    {
        private const string ViajesOlympiaUrl = "ViajesOlympiaUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesOlympiaValuationRequestFactory ViajesOlympiaProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ViajesOlympiaValuationAutomator( IViajesOlympiaValuationRequestFactory ViajesOlympiaSupplierValuationRequestFactory,
                                                IProviderOutputLogger outputLogger )
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
            this.ViajesOlympiaProviderValuationRequestFactory = ViajesOlympiaSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<ViajesOlympiaValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ViajesOlympiaValuationResponse response = null;
            try
            {
                DevuelveDisponibilidadPB supplierRequest = this.ViajesOlympiaProviderValuationRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("ViajesOlympiaValuationAutomator.GetValuationResponseAsync: ViajesOlympia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }

            return response;
        }

        private async Task<ViajesOlympiaValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, DevuelveDisponibilidadPB supplierRequest)
        {

            ServiceSoapClient client = ConfigureSoapCient(request.Provider.Parameters);
            string availRes = client.DevuelveDisponibilidadPB(supplierRequest.User, supplierRequest.password, supplierRequest.Pais, supplierRequest.provincia, supplierRequest.Poblacion, supplierRequest.Categoria, supplierRequest.Codigo, supplierRequest.FechaEntradaSt, Convert.ToInt32(supplierRequest.Noches), supplierRequest.ListHab, supplierRequest.Opciones);
            availRes = availRes.Replace("<Respuesta>", "").Replace("</Respuesta>", "").Trim();
            var response = availRes.XmlDeSerialize<Disponibilidad>();

            var supplierResponse = new ViajesOlympiaValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
    }
}
