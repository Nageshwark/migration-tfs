﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<YouTravelAvailabilityResponse> automator;
        private readonly IAccommodationValuationParser<YouTravelAvailabilityResponse> parser;

        public YouTravelValuationProvider(IAccommodationValuationAutomatorAsync<YouTravelAvailabilityResponse> automator,
                                                IAccommodationValuationParser<YouTravelAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
