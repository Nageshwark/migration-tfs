﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<YouTravelBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<YouTravelBookingResponse>> parser;

        public YouTravelBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<YouTravelBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<YouTravelBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
