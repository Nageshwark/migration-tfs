﻿namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelAvailabilityResponse
    {
        public string SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
        public string HotelDetails { get; set; }
    }
}
