﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<YouTravelBookingResponse>>
    {
        private const string YouTravelBookingUrl = "YouTravelBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IYouTravelProviderBookingRequestFactory YouTravelProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public YouTravelBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IYouTravelProviderBookingRequestFactory YouTravelSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger,
                                            IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.YouTravelProviderBookingRequestFactory = YouTravelSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<YouTravelBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<YouTravelBookingResponse> responses = new List<YouTravelBookingResponse>();

            string supplierRequest = null;
            YouTravelBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.YouTravelProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.YouTravelProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse);
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("YouTravelBookingAutomator.GetBookingResponseAsync: YouTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<YouTravelBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, string supplierRequest)
        {
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationJson };

            string url = request.Provider.Parameters.GetParameterValue(YouTravelBookingUrl);
            string urlWithParameters = url + supplierRequest;

            this.loggerService.SetProviderRequest(request, urlWithParameters);
            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParameters);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response);

            YouTravelBookingResponse supplierResponse = new YouTravelBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };

            return supplierResponse;
        }
    }
}
