﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.YouTravel.Entity;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelValuationAutomator : IAccommodationValuationAutomatorAsync<YouTravelAvailabilityResponse>
    {
        private const string YouTravelValuationUrl = "YouTravelAvailabilityUrl";
        private const string YouTravelHotelDetailsUrl = "YouTravelHotelDetailsUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IYouTravelProviderValuationRequestFactory youTravelProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public YouTravelValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IYouTravelProviderValuationRequestFactory YouTravelSupplierValuationRequestFactory
            , IProviderOutputLogger outputLogger
            , IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.youTravelProviderValuationRequestFactory = YouTravelSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<YouTravelAvailabilityResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            YouTravelAvailabilityResponse response = null;

            try
            {
                string supplierRequest = this.youTravelProviderValuationRequestFactory.CreateSupplierValuationRequest(request);
                
                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("YouTravelValuationAutomator.GetValuationResponseAsync: YouTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            try
            {
                string hotelDetailsRq = this.youTravelProviderValuationRequestFactory.CreateHotelDetailsRequest(request);
                string hotelDetailsRs = await GetHotelDetailsResponseAsync(request, hotelDetailsRq);

                HotelDetails.HtSearchRq searchResponse = hotelDetailsRs.XmlDeSerialize<HotelDetails.HtSearchRq>();

                response.HotelDetails = (searchResponse != null && searchResponse.Success.ToLower() == "true") ? searchResponse.Hotel.Erratas : String.Empty;
            }

            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("YouTravelValuationAutomator.GetHotelDetailsAsync: YouTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }
       
        private async Task<YouTravelAvailabilityResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, string supplierRequest)
        {

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(YouTravelValuationUrl);
            string urlWithParams = url + supplierRequest;
            this.loggerService.SetProviderRequest(request, urlWithParams);
            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParams);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, response);
            YouTravelAvailabilityResponse supplierResponse = new YouTravelAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };


           
            return supplierResponse;
        }

        private async Task<string> GetHotelDetailsResponseAsync(AccommodationProviderValuationRequest request, string hotelDetailsRq)
        {
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(YouTravelHotelDetailsUrl);

            string urlWithParams = url + hotelDetailsRq;

            this.loggerService.SetProviderRequest(request, urlWithParams);

            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParams);

            this.loggerService.SetProviderResponse(request, response);

            return response.Value;
        }


    }
}
