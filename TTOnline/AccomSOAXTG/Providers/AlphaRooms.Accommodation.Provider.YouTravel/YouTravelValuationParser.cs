﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Entity;
using AlphaRooms.Accommodation.Provider.YouTravel.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{

    public class YouTravelValuationParser : IAccommodationValuationParser<YouTravelAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public YouTravelValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, YouTravelAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, response.HotelDetails, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("YouTravel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, string supplierRequest, string supplierResponse, string hotelDetails, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            HtSearchRq searchResponse = supplierResponse.XmlDeSerialize<HtSearchRq>();

            if (searchResponse != null && searchResponse.session != null && searchResponse.session.First().Hotel.Any())
            {
                var hotel = searchResponse.session.First().Hotel.First();

                Sync.ParallelForEachIgnoreFailed(hotel.Room_1.First().Room, room =>
                {
                    var isSelectRoom = from AccommodationProviderValuationRequestRoom r in request.SelectedRooms
                        where r.AvailabilityResult.RoomCode == room.Id
                        select r;
                    
                    if (isSelectRoom.Any())
                    {
                        var result = new AccommodationProviderValuationResult();

                        result.RoomNumber = isSelectRoom.First().AvailabilityResult.RoomNumber;

                        result.ProviderEdiCode = isSelectRoom.First().AvailabilityRequest.Provider.EdiCode;
                        result.PaymentModel = PaymentModel.PostPayment;

                        result.DestinationEdiCode = hotel.Destination;
                        result.EstablishmentEdiCode = hotel.ID;
                        result.EstablishmentName = hotel.Hotel_Name;

                        result.RoomDescription = room.Type;
                        result.RoomCode = room.Id;

                        result.CheckInDate = isSelectRoom.First().AvailabilityRequest.CheckInDate;
                        result.CheckOutDate = isSelectRoom.First().AvailabilityRequest.CheckOutDate;
                        result.Adults = Convert.ToByte(hotel.Room_1.First().Passengers.First().Adults);
                        result.Children = Convert.ToByte(hotel.Room_1.First().Passengers.First().Children);
                        result.Infants = Convert.ToByte(hotel.Room_1.First().Passengers.First().Infants);

                        result.BoardCode = room.Board;


                        BoardBasisMapper bbMapper = new BoardBasisMapper();

                        result.BoardDescription = bbMapper.GetBoardBasisName(result.BoardCode);


                        result.SalePrice = new Money()
                        {
                            Amount = Convert.ToDecimal(room.Rates.First().Final_Rate),
                            CurrencyCode = searchResponse.session.First().Currency
                        };

                        result.CostPrice = new Money()
                        {
                            Amount = Convert.ToDecimal(room.Rates.First().Final_Rate),
                            CurrencyCode = searchResponse.session.First().Currency
                        };

                        result.IsNonRefundable = !Convert.ToBoolean(room.Refundable);

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("SessionID", searchResponse.session.First().id);
                        result.ProviderSpecificData.Add("KeyCollectionInformation", hotelDetails);

                        availabilityResults.Enqueue(result);
                    }
                });
            }
            else
            {
                if (searchResponse != null && !string.IsNullOrEmpty(searchResponse.ErrorMsg) && searchResponse.ErrorMsg.ToLower() != "no results found.")
                {
                    var sbMessage = new StringBuilder();

                    sbMessage.AppendLine("YouTravel Valuation Parser: Availability Response cannot be parsed because it is null.");

                    sbMessage.AppendLine(string.Format("Error Details: {0}", searchResponse.ErrorMsg));

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
                
                
            }
            
        }
    }
}
