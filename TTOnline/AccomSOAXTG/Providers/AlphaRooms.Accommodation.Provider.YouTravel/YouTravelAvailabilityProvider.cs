﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<YouTravelAvailabilityResponse> parser;

        public YouTravelAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<YouTravelAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}

