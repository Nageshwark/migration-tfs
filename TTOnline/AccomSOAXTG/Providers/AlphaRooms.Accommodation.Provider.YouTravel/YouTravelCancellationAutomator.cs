﻿using System;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Helper;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelCancellationAutomator : IAccommodationCancellationAutomatorAsync<YouTravelCancellationResponse>
    {
        private const string YouTravelCancellationUrl = "YouTravelCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string YouTravelUsername = "YouTravelUsername";
        private const string YouTravelPassword = "YouTravelPassword";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IYouTravelProviderCancellationRequestFactory youTravelProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private string YouTravelCancellationTimeOut;

        public YouTravelCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IYouTravelProviderCancellationRequestFactory YouTravelSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.youTravelProviderCancellationRequestFactory = YouTravelSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<YouTravelCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            YouTravelCancellationResponse response = null;

            try
            {
                var supplierRequest = youTravelProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

                string url = request.Provider.Parameters.GetParameterValue(YouTravelCancellationUrl);
                string urlWithParams = url + supplierRequest;

                WebScrapeResponse supplierResponse = await webScrapeClient.GetAsync(urlWithParams);
                
                response = new YouTravelCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse.Value
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("YouTravelBookingAutomator.GetCancellationResponseAsync: YouTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        
    }
}
