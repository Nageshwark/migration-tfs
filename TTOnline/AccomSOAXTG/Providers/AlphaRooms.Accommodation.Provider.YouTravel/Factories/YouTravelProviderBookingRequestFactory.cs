﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Provider.YouTravel.Helper;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelProviderBookingRequestFactory : IYouTravelProviderBookingRequestFactory
    {
        private const string YouTravelDefaultLanguage = "YouTravelDefaultLanguage";
        private const string YouTravelUsername = "YouTravelUsername";
        private const string YouTravelPassword = "YouTravelPassword";
        

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            string supplierRequest = "";


            var roomToBook = request.ValuatedRooms[0].ValuationResult;
            var availabilityRequest = request.ValuatedRooms.First().AvailabilityRequest;
            var duration = availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate;

            var sbParameters = new StringBuilder();
            sbParameters.Append("?");
            sbParameters.AppendFormat("Session_ID={0}", roomToBook.ProviderSpecificData["SessionID"]);
            sbParameters.Append("&");
            sbParameters.AppendFormat("LangID={0}", request.Provider.Parameters.GetParameterValue(YouTravelDefaultLanguage));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Username={0}", request.Provider.Parameters.GetParameterValue(YouTravelUsername));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Password={0}", request.Provider.Parameters.GetParameterValue(YouTravelPassword));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Checkin_Date={0}", availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Nights={0}", duration.TotalDays);
            sbParameters.Append("&");
            sbParameters.AppendFormat("Rooms={0}", request.ValuatedRooms.Count());
            sbParameters.Append("&");
            sbParameters.Append(GetRoomPaxSting(request.ValuatedRooms));
            sbParameters.Append("&");
            sbParameters.AppendFormat("HID={0}", roomToBook.EstablishmentEdiCode);
            
            sbParameters.Append("&");
            sbParameters.AppendFormat("Customer_title={0}", request.Customer.Title.ToString());
            sbParameters.Append("&");
            sbParameters.AppendFormat("Customer_firstname={0}", request.Customer.FirstName);
            sbParameters.Append("&");
            sbParameters.AppendFormat("Customer_Lastname={0}", request.Customer.Surname);

            sbParameters.Append("&");
            sbParameters.AppendFormat("Email={0}", request.Customer.EmailAddress);

            supplierRequest = sbParameters.ToString().Replace("&&", "&");

            return supplierRequest;
        }

       

        private string GetRoomPaxSting(AccommodationProviderBookingRequestRoom [] rooms)
        {
            StringBuilder sbRoomPax = new StringBuilder();


            int roomCounter = 1;
            foreach (var room in rooms)
            {
                sbRoomPax.AppendFormat("ADLTS_{0}={1}", roomCounter, room.Guests.AdultsCount);
                sbRoomPax.Append("&");

                sbRoomPax.AppendFormat("CHILD_{0}={1}", roomCounter, room.Guests.ChildrenAndInfantsCount);
                sbRoomPax.Append("&");

                if (room.Guests.ChildrenCount > 0)
                {
                    

                    int childCounter = 1;
                    foreach (var guest in room.Guests)
                    {
                        if (guest.Type == GuestType.Child || guest.Type == GuestType.Infant)
                        {
                            var age = guest.Age == 0 ? 1 : guest.Age; // if child age is 0 youtravel throw exeption "Child age cannot be blank"
                            sbRoomPax.AppendFormat("ChildAgeR{0}C{1}={2}", roomCounter, childCounter, age);
                            sbRoomPax.Append("&");
                            childCounter++;
                        }
                    }
                }

                if (roomCounter == 1)
                    sbRoomPax.AppendFormat("&RID={0}", room.ValuationResult.RoomCode);
                else
                    sbRoomPax.AppendFormat("&RID_{0}={1}", roomCounter, room.ValuationResult.RoomCode);

                sbRoomPax.AppendFormat("&room{0}_Rate={1}", roomCounter, room.ValuationResult.SalePrice.Amount);

                roomCounter++;
            }

            return sbRoomPax.ToString();
        }
    }
}
