﻿using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Factories
{
    public class YouTravelCancellationRequestFactory : IYouTravelProviderCancellationRequestFactory
    {
        private const string YouTravelUsername = "YouTravelUsername";
        private const string YouTravelPassword = "YouTravelPassword";
        private const string YouTravelDefaultLanguage = "YouTravelDefaultLanguage";

        public string CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            

            var sbParameters = new StringBuilder();
            sbParameters.Append("?");
            sbParameters.AppendFormat("LangID={0}", request.Provider.Parameters.GetParameterValue(YouTravelDefaultLanguage));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Username={0}", request.Provider.Parameters.GetParameterValue(YouTravelUsername));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Password={0}", request.Provider.Parameters.GetParameterValue(YouTravelPassword));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Booking_ref={0}", request.ProviderBookingReference);
            
            //?Booking_ref=10231122311233112&Username=xmltestme&Password=testme

            string param = sbParameters.ToString().Replace("&&", "&");

            return param;
        }
    }
}
