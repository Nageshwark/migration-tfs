﻿using System;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Factories
{
    public class YouTravelValuationRequestFactory : IYouTravelProviderValuationRequestFactory
    {
        private const string YouTravelUsername = "YouTravelUsername";
        private const string YouTravelPassword = "YouTravelPassword";
        private const string YouTravelDefaultLanguage = "YouTravelDefaultLanguage";


        public string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            //Get the first rooms availability request as its same for all the rooms
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            TimeSpan duration = availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate;

            StringBuilder sbParameters = new StringBuilder();
            sbParameters.Append("?");
            sbParameters.AppendFormat("LangID={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelDefaultLanguage));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Username={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelUsername));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Password={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelPassword));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Checkin_Date={0}", availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Nights={0}", duration.TotalDays);
            sbParameters.Append("&");
            sbParameters.AppendFormat("Rooms={0}", request.SelectedRooms.Count());
            sbParameters.Append("&");
            sbParameters.Append(GetRoomPaxSting(request));
            sbParameters.Append("&");
            sbParameters.AppendFormat("HID={0}", request.SelectedRooms.First().AvailabilityResult.ProviderEstablishmentCode);

            //?HID=112,543&LangID=EN&Username=xmltestme&Nights=5&Checkin_Date=10/08/2015&ADLTS_1=2&Rooms=1&Password=testme

            string param = sbParameters.ToString().Replace("&&", "&");

            return param;
        }

        private string GetRoomPaxSting(AccommodationProviderValuationRequest request)
        {
            StringBuilder sbRoomPax = new StringBuilder();

            int roomCounter = 1;
            foreach (var room in request.SelectedRooms)
            {
                sbRoomPax.AppendFormat("ADLTS_{0}={1}", roomCounter, room.Guests.AdultsCount);
                sbRoomPax.Append("&");

                if (room.Guests.ChildrenAndInfantsCount > 0)
                {
                    sbRoomPax.AppendFormat("CHILD_{0}={1}", roomCounter, room.Guests.ChildrenCount + room.Guests.InfantsCount);
                    sbRoomPax.Append("&");

                    int childCounter = 1;

                    foreach (var guest in room.Guests)
                    {
                        if (guest.Type == GuestType.Child || guest.Type == GuestType.Infant)
                        {
                            var age = guest.Age == 0 ? 1 : guest.Age; // if child age is 0 youtravel throw exeption "Child age cannot be blank"

                            sbRoomPax.AppendFormat("ChildAgeR{0}C{1}={2}", roomCounter, childCounter, age);
                            sbRoomPax.Append("&");
                            childCounter++;
                        }

                    }
                }

                roomCounter++;
            }

            string param = sbRoomPax.ToString().Replace("&&", "&");

            return param;
        }

        public string CreateHotelDetailsRequest(AccommodationProviderValuationRequest request)
        {
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;

            StringBuilder sbParameters = new StringBuilder();
            sbParameters.Append("?");
            sbParameters.AppendFormat("LangID={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelDefaultLanguage));
            sbParameters.Append("&");
            sbParameters.AppendFormat("HID={0}", request.SelectedRooms.First().AvailabilityResult.ProviderEstablishmentCode);
            sbParameters.Append("&");
            sbParameters.AppendFormat("Username={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelUsername));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Password={0}", availabilityRequest.Provider.Parameters.GetParameterValue(YouTravelPassword));

            string param = sbParameters.ToString().Replace("&&", "&");

            return param;

        }
    }
}
