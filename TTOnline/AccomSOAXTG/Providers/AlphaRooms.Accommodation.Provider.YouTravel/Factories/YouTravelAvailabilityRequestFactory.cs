﻿using System;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Factories
{
    public class YouTravelAvailabilityRequestFactory:IYouTravelAvailabilityRequestFactory
    {
        private const string YouTravelUsername = "YouTravelUsername";
        private const string YouTravelPassword = "YouTravelPassword";
        private const string YouTravelDefaultLanguage = "YouTravelDefaultLanguage";

        public string CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            StringBuilder sbParameters = new StringBuilder();
            sbParameters.Append("?");
            sbParameters.AppendFormat("LangID={0}", request.Provider.Parameters.GetParameterValue(YouTravelDefaultLanguage));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Username={0}", request.Provider.Parameters.GetParameterValue(YouTravelUsername));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Password={0}", request.Provider.Parameters.GetParameterValue(YouTravelPassword));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Checkin_Date={0}", request.CheckInDate.ToString("dd/MM/yyyy"));
            sbParameters.Append("&");
            sbParameters.AppendFormat("Nights={0}", duration.TotalDays);
            sbParameters.Append("&");
            sbParameters.AppendFormat("Rooms={0}", request.Rooms.Count());
            sbParameters.Append("&");
            sbParameters.Append(GetRoomPaxSting(request));
            sbParameters.Append("&");

            if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
                sbParameters.AppendFormat("HID={0}", string.Join(",", request.EstablishmentCodes));
            else
                sbParameters.AppendFormat("Dstn={0}", request.DestinationCodes[0]);

            //?LangID=EN&Username=xmltestme&Nights=5&Checkin_Date=10/08/2015&ADLTS_1=2&Rooms=1&Password=testme&RSRT=135

            string param = sbParameters.ToString().Replace("&&", "&");

            return param;
        }

        private string GetHotelIDs(string[] establishmentIds)
        {
            return string.Join(",", establishmentIds);
        }

        private string GetRoomPaxSting(AccommodationProviderAvailabilityRequest request)
        {
            StringBuilder sbRoomPax = new StringBuilder();

            int roomCounter = 1;
            foreach (var room in request.Rooms)
            {
                sbRoomPax.AppendFormat("ADLTS_{0}={1}", roomCounter, room.Guests.AdultsCount);
                sbRoomPax.Append("&");

                if (room.Guests.ChildrenAndInfantsCount > 0)
                {
                    sbRoomPax.AppendFormat("CHILD_{0}={1}", roomCounter, room.Guests.ChildrenCount + room.Guests.InfantsCount);
                    sbRoomPax.Append("&");

                    int childCounter = 1;
                    
                    foreach (var guest in room.Guests)
                    {
                        if (guest.Type == GuestType.Child || guest.Type == GuestType.Infant)
                        {
                            var age = guest.Age == 0 ? 1 : guest.Age; // if child age is 0 youtravel throw exeption "Child age cannot be blank"
                            sbRoomPax.AppendFormat("ChildAgeR{0}C{1}={2}", roomCounter, childCounter, age);
                            sbRoomPax.Append("&");
                            childCounter++;
                        }                 
                        
                    }
                }

                roomCounter++;
            }

            string param = sbRoomPax.ToString().Replace("&&", "&");

            return param;
        }
    }
}
