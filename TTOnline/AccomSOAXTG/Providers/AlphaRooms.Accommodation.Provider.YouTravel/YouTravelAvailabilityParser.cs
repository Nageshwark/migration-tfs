﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Entity;
using AlphaRooms.Accommodation.Provider.YouTravel.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelAvailabilityParser : IAccommodationAvailabilityParser<YouTravelAvailabilityResponse>
    {
        private readonly ILogger logger;


        public YouTravelAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            YouTravelAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.SupplierResponse;
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "YouTravel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, string supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            HtSearchRq searchResponse = supplierResponse.XmlDeSerialize<HtSearchRq>();

            if (searchResponse != null && searchResponse.session != null && searchResponse.session.First().Hotel.Any())
            {
                string currency = searchResponse.session.First().Currency;
                string sessionId = searchResponse.session.First().id;

                Parallel.ForEach(searchResponse.session.First().Hotel, hotel =>
             {
                 Sync.ParallelForEachIgnoreFailed(hotel.Room_1.First().Room, room =>
                 {

                     var result = FillRowDetails(request, hotel, room, searchResponse.session.First(), hotel.Room_1.First().Passengers.First());
                     result.RoomNumber = 1;
                     result.Infants = request.Rooms[0].Guests.InfantsCount;  //Response is not returning Infant Count, so reading from Request.

                     availabilityResults.Enqueue(result);
                 });

                 if (hotel.Room_2 != null)
                 {
                     Sync.ParallelForEachIgnoreFailed(hotel.Room_2.First().Room, room =>
                     {
                         var result = FillRowDetails(request, hotel, room, searchResponse.session.First(), hotel.Room_1.First().Passengers.First());
                         result.RoomNumber = 2;
                         result.Infants = request.Rooms[1].Guests.InfantsCount;

                         availabilityResults.Enqueue(result);
                     });
                 }

                 if (hotel.Room_3 != null)
                 {
                     Sync.ParallelForEachIgnoreFailed(hotel.Room_3.First().Room, room =>
                     {
                         var result = FillRowDetails(request, hotel, room, searchResponse.session.First(), hotel.Room_1.First().Passengers.First());
                         result.RoomNumber = 3;
                         result.Infants = request.Rooms[2].Guests.InfantsCount;

                         availabilityResults.Enqueue(result);
                     });
                 }
             });

            }
            else
            {
                if (searchResponse != null && !string.IsNullOrEmpty(searchResponse.ErrorMsg) && searchResponse.ErrorMsg.ToLower() != "no results found.")
                {
                    var sbMessage = new StringBuilder();

                    sbMessage.AppendLine("YouTravel Availability Parser: Availability Response cannot be parsed because it is null.");

                    sbMessage.AppendLine(string.Format("Error Details: {0}", searchResponse.ErrorMsg));

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }
        }

        private static AccommodationProviderAvailabilityResult FillRowDetails(AccommodationProviderAvailabilityRequest request,
            HtSearchRqSessionHotel hotel, HtSearchRqSessionHotelRoom_1Room room, HtSearchRqSession session, HtSearchRqSessionHotelRoom_1Passengers pax)
        {
            var result = new AccommodationProviderAvailabilityResult();

            result.ProviderEdiCode = result.SupplierEdiCode = request.Provider.EdiCode;
            result.PaymentModel = PaymentModel.PostPayment;

            result.DestinationEdiCode = hotel.Destination;
            result.EstablishmentEdiCode = hotel.ID;
            result.EstablishmentName = hotel.Hotel_Name;

            result.RoomDescription = room.Type;
            result.RoomCode = room.Id;

            result.CheckInDate = request.CheckInDate;
            result.CheckOutDate = request.CheckOutDate;

            result.BoardCode = room.Board;

            BoardBasisMapper bbMapper = new BoardBasisMapper();

            result.BoardDescription = bbMapper.GetBoardBasisName(result.BoardCode);


            result.SalePrice = new Money()
            {
                Amount = Convert.ToDecimal(room.Rates.First().Final_Rate),
                CurrencyCode = session.Currency
            };

            result.CostPrice = new Money()
            {
                Amount = Convert.ToDecimal(room.Rates.First().Final_Rate),
                CurrencyCode = session.Currency
            };

            result.IsNonRefundable = !Convert.ToBoolean(room.Refundable);


            result.Adults = Convert.ToByte(pax.Adults);
            result.Children = Convert.ToByte(pax.Children);
            result.Infants = Convert.ToByte(pax.Infants);

            result.ProviderSpecificData = new Dictionary<string, string>();
            result.ProviderSpecificData.Add("SessionID", session.id);

            return result;
        }
    }
}


