﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Interfaces
{
    public interface IYouTravelAvailabilityRequestFactory
    {
        string CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);

    }
}
