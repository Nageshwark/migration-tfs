﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Interfaces
{
    public interface IYouTravelProviderValuationRequestFactory
    {
        string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
        string CreateHotelDetailsRequest(AccommodationProviderValuationRequest request);
    }
}
