﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Interfaces
{
    public interface IYouTravelProviderBookingRequestFactory
    {
        string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
