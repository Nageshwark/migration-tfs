﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.WebScraping;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse>
    {
        private const string YouTravelAvailabilityUrl = "YouTravelAvailabilityUrl";


        private readonly IWebScrapeClient webScrapeClient;
        private readonly IYouTravelAvailabilityRequestFactory youTravelSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public YouTravelAvailabilityAutomator(IYouTravelAvailabilityRequestFactory youTravelAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this.youTravelSupplierAvailabilityRequestFactory = youTravelAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<YouTravelAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            YouTravelAvailabilityResponse response = null;

            ValidateRequest(request);

            try
            {
                var supplierRequest = this.youTravelSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("YouTravelAvailabilityAutomator.GetAvailabilityResponseAsync: YouTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            if (request.Rooms.Count() > 3)
                throw new ApplicationException("YouTravel supports upto 3 Rooms");

            foreach (var room in request.Rooms)
            {
                if (room.Guests.AdultsCount > 6)
                    throw new ApplicationException("YouTravel supports upto 6 Adults");

                if (room.Guests.ChildrenCount + room.Guests.InfantsCount > 4)
                    throw new ApplicationException("YouTravel supports upto 4 Children");
            }
        }

        private async Task<YouTravelAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, string  supplierRequest)
        {
            

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(YouTravelAvailabilityUrl);
            string urlWithParams = url + supplierRequest;

            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParams);
            this.outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, response);

            YouTravelAvailabilityResponse supplierResponse = new YouTravelAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };

            return supplierResponse;
        }

    }
}
