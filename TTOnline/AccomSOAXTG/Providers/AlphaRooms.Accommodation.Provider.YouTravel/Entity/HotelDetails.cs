﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.YouTravel.Entity
{
    public class HotelDetails
    {

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class HtSearchRq
        {

            private string successField;

            private string langIDField;

            private string destinationField;

            private ushort hIDField;

            private HtSearchRqHotel hotelField;

            /// <remarks/>
            public string Success
            {
                get { return this.successField; }
                set { this.successField = value; }
            }

            /// <remarks/>
            public string LangID
            {
                get { return this.langIDField; }
                set { this.langIDField = value; }
            }

            /// <remarks/>
            public string Destination
            {
                get { return this.destinationField; }
                set { this.destinationField = value; }
            }

            /// <remarks/>
            public ushort HID
            {
                get { return this.hIDField; }
                set { this.hIDField = value; }
            }

            /// <remarks/>
            public HtSearchRqHotel Hotel
            {
                get { return this.hotelField; }
                set { this.hotelField = value; }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class HtSearchRqHotel
        {

            private byte youtravel_RatingField;

            private string official_RatingField;

            private string board_TypeField;

            private string hotel_DescField;

            private string[] hotel_PhotosField;

            private string[] hotel_FacilitiesField;

            private HtSearchRqHotelRoom[] room_TypesField;

            private string aI_TypeField;

            private string aI_DescField;

            private string[] aI_FacilitiesField;

            private string erratasField;

            private string nameField;

            /// <remarks/>
            public byte Youtravel_Rating
            {
                get { return this.youtravel_RatingField; }
                set { this.youtravel_RatingField = value; }
            }

            /// <remarks/>
            public string Official_Rating
            {
                get { return this.official_RatingField; }
                set { this.official_RatingField = value; }
            }

            /// <remarks/>
            public string Board_Type
            {
                get { return this.board_TypeField; }
                set { this.board_TypeField = value; }
            }

            /// <remarks/>
            public string Hotel_Desc
            {
                get { return this.hotel_DescField; }
                set { this.hotel_DescField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("Photo", IsNullable = false)]
            public string[] Hotel_Photos
            {
                get { return this.hotel_PhotosField; }
                set { this.hotel_PhotosField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("Facility", IsNullable = false)]
            public string[] Hotel_Facilities
            {
                get { return this.hotel_FacilitiesField; }
                set { this.hotel_FacilitiesField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("Room", IsNullable = false)]
            public HtSearchRqHotelRoom[] Room_Types
            {
                get { return this.room_TypesField; }
                set { this.room_TypesField = value; }
            }

            /// <remarks/>
            public string AI_Type
            {
                get { return this.aI_TypeField; }
                set { this.aI_TypeField = value; }
            }

            /// <remarks/>
            public string AI_Desc
            {
                get { return this.aI_DescField; }
                set { this.aI_DescField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("AI_Facility", IsNullable = false)]
            public string[] AI_Facilities
            {
                get { return this.aI_FacilitiesField; }
                set { this.aI_FacilitiesField = value; }
            }

            /// <remarks/>
            public string Erratas
            {
                get { return this.erratasField; }
                set { this.erratasField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Name
            {
                get { return this.nameField; }
                set { this.nameField = value; }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class HtSearchRqHotelRoom
        {

            private string[] facilityField;

            private string nameField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Facility")]
            public string[] Facility
            {
                get { return this.facilityField; }
                set { this.facilityField = value; }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name
            {
                get { return this.nameField; }
                set { this.nameField = value; }
            }
        }
    }
}