﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Entity;
using AlphaRooms.Accommodation.Provider.YouTravel.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelCancellationParser : IAccommodationCancellationParserAsync<YouTravelCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public YouTravelCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, YouTravelCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                var searchResponse = response.SupplierResponse.XmlDeSerialize<HtSearchRq>();
                bool isSuccess;
                
                if (searchResponse != null && bool.TryParse(searchResponse.Success, out isSuccess) && isSuccess)
                {

                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";   

                }
                else
                {
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    var sb = new StringBuilder();
                    
                    if (searchResponse != null && !string.IsNullOrEmpty(searchResponse.ErrorMsg))
                    {
                        sb.AppendLine("There was an error with cancellatin, plese see thhe response from YouTravel below.");
                        sb.AppendLine(searchResponse.ErrorMsg);
                    }
                    else
                        sb.AppendLine("There was an unknown problem with the cancellation response from YouTravel.");

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from YouTravel.");
                sb.AppendLine("Details:" + ex.ToString());
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
       
    }
}
