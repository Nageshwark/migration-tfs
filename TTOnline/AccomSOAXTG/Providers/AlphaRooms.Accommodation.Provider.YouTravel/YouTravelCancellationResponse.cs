﻿
namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelCancellationResponse
    {
        public string SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
    }
}
