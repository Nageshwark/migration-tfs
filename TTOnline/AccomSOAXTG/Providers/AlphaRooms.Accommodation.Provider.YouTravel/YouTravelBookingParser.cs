﻿using AlphaRooms.Accommodation.Provider.YouTravel.Entity;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelBookingParser : IAccommodationBookingParser<IEnumerable<YouTravelBookingResponse>>
    {
        private readonly ILogger logger;
        

        public YouTravelBookingParser(  ILogger logger)
        {
            this.logger = logger;
            
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<YouTravelBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

                
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("YouTravel Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        string supplierRequest,
                                                        string supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (string.IsNullOrWhiteSpace(supplierResponse))
            {
                string message = string.Format("YouTravel Booking Parser: Booking Response cannot be parsed because it is null. Booking Request Id: {0}", request.BookingId);

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }

            string bookingResponse = "";

            // Debug Code
            if (request.Debugging)
            {
                //string serialisedRequest = supplierRequest.XmlSerialize();
                //string serialisedResponse = bookingResponse.XmlSerialize();
            }

           
            var response = supplierResponse.XmlDeSerialize<HotelBookingResponse>();
 
            // Check that no error was included in the response - if an error is returned, log the error in the event log, but DO NOT throw an exception. Instead, return a failed booking.
            if (response != null && response.IsSuccess )
            {
                // NOTE: This will process a single room booking ONLY. It is not designed to handle multiple room bookings.
                CreateAvailabilityResultFromResponse(request, supplierRequest, response, bookingResults);
            }
            else
            {
                
            
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("YouTravel Response Error.");
                sb.AppendLine("YouTravel returned one or more errors in their booking response:");
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest);
                sb.AppendLine();


                sb.AppendLine("Error Details:");
                sb.Append(response.ErrorMsg);
               

                // Log the error to the event log
                if (logger != null)
                    logger.Error(sb.ToString());

                // Create a failed booking result
                AccommodationProviderBookingResult failedResult = new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    Message = sb.ToString()
                };

                bookingResults.Enqueue(failedResult);
            }            
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderBookingResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supplierRequest"></param>
        /// <param name="response"></param>
        /// <param name="bookingResults"></param>
        private void CreateAvailabilityResultFromResponse(  AccommodationProviderBookingRequest request,
                                                            string supplierRequest,
                                                            HotelBookingResponse response,
                                                            ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();
            result.RoomNumber = request.ValuatedRooms[0].RoomNumber;
            try
            {

                result.ProviderBookingReference = response.Booking_ref;

                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("ReservationStatusCode", response.Voucher_Url);

                result.BookingStatus = BookingStatus.Confirmed;

                result.KeyCollectionInformation = request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["KeyCollectionInformation"];

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from YouTravel.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest);
                sb.AppendLine();
                sb.AppendLine("Booking Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Enqueue(result);
        }

        
    }
}
