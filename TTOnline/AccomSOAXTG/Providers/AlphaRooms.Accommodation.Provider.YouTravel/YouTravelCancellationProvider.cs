﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.YouTravel
{
    public class YouTravelCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<YouTravelCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<YouTravelCancellationResponse> parser;

        public YouTravelCancellationProvider(IAccommodationCancellationAutomatorAsync<YouTravelCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<YouTravelCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
