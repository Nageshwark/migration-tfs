﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.CavalBase
{
    public abstract class CavalValuationParserBase
    {
        protected virtual AccommodationProviderValuationResult FillRowDetails(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRS supplierResponse, valuatedOccupation room, int roomNumber)
        {
            var result = new AccommodationProviderValuationResult();
            var availabilityResultQuery = request.SelectedRooms.Where(x => x.AvailabilityResult.RoomNumber == roomNumber && x.AvailabilityResult.Adults == room.adultsPerRoom && x.AvailabilityResult.Children == room.childrenPerRoom);

            if (availabilityResultQuery.Any())
            {
                var availabilityResult = availabilityResultQuery.First().AvailabilityResult;

                result.RoomNumber = (byte) roomNumber;
                result.ProviderEdiCode = availabilityResult.ProviderEdiCode;
                result.EstablishmentName = supplierResponse.establishmentName;
                result.EstablishmentEdiCode = supplierResponse.establishmentId;
                result.DestinationEdiCode = availabilityResult.DestinationEdiCode;

                result.BoardDescription = availabilityResult.BoardDescription;
                result.BoardCode = availabilityResult.BoardCode;
                result.RoomCode = room.roomCode;

                result.RoomDescription = room.roomName;
                result.PaymentModel = PaymentModel.PostPayment;
                result.RateType = RateType.NetStandard;
                result.IsOpaqueRate = false;
                result.IsNonRefundable = false;
                result.NumberOfAvailableRooms = 0;

                var amount = supplierResponse.netPrice.value/request.SelectedRooms.Length;

                result.SalePrice = new Money
                {
                    Amount = (decimal) amount,
                    CurrencyCode = supplierResponse.netPrice.currencyCode
                };

                result.CostPrice = result.SalePrice;
                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                result.Adults = (byte) room.adultsPerRoom;
                result.Children = (byte) room.childrenPerRoom;
                result.Infants = 0;
                result.CheckInDate = availabilityResult.CheckInDate;
                result.CheckOutDate = availabilityResult.CheckOutDate;

                result.CancellationPolicy = GetCancellationPolicy(supplierResponse.cancellationCosts);

                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("TotalPrice", supplierResponse.netPrice.value.ToString());
            }

            return result;
        }
        protected virtual string[] GetCancellationPolicy(cancellationCost[] cancellationCosts)
        {
            return cancellationCosts.Select(cancellationCost => string.Format("From:{0}, Amount:{1}, Currency:{2}", cancellationCost.from, cancellationCost.netPrice.value, cancellationCost.netPrice.currencyCode)).ToArray();
        }
    }
}
