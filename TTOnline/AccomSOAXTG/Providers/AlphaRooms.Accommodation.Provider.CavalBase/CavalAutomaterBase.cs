﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.CommonService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.CavalBase
{
    public class CavalAutomaterBase
    {
        private const string CavalAvailabilityUrl = "CavalAvailabilityUrl";
        private const string CavalValuationUrl = "CavalAvailabilityUrl";
        private const string CavalBookingUrl = "CavalBookingUrl";
        private const string CavalCancellationUrl = "CavalCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        protected HotelBookingServiceClient ConfigureSoapClient(List<AccommodationProviderParameter> parameters, RequestType requestType)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(parameters.GetParameterValue(MaxReceivedMessageSize));

            EndpointAddress address = null;

            switch (requestType)
            {
                case RequestType.Availability:
                    address = new EndpointAddress(new Uri(parameters.GetParameterValue(CavalAvailabilityUrl)));
                    break;

                case RequestType.Valuation:
                    address = new EndpointAddress(new Uri(parameters.GetParameterValue(CavalValuationUrl)));
                    break;

                case RequestType.Booking:
                    address = new EndpointAddress(new Uri(parameters.GetParameterValue(CavalBookingUrl)));
                    break;
            }


            HotelBookingServiceClient client = new HotelBookingServiceClient(binding, address);

            return client;
        }

        protected CommonsBookingServiceClient ConfigureSoapClient(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));

            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(CavalCancellationUrl)));
                    
            return new CommonsBookingServiceClient(binding, address);
            
        }

        protected void ValidateRequest(AccommodationProviderValuationRequest request)
        {
            if (request.SelectedRooms.Length == 1) return;

            var key = request.SelectedRooms.First().AvailabilityResult.ProviderSpecificData["Key"];

            var otherRooms = request.SelectedRooms.Skip(1).ToArray();

            var unPairedRooms = otherRooms.Any(x =>  !x.AvailabilityResult.ProviderSpecificData["Key"].Contains(key));

            if (unPairedRooms)
                throw new ApplicationException("CavalAutomatorBase.ValidateRequest: Selected rooms contains unpaired rooms. Only paired multi-rooms can be Searched/Booked.");

        }
    }
}
