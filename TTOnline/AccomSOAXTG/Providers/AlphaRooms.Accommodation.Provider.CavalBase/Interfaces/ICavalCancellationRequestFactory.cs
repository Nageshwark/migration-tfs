﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CommonService;

namespace AlphaRooms.Accommodation.Provider.CavalBase.Interfaces
{
    public interface ICavalCancellationRequestFactory
    {
        cavalCancelBookingRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
