﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;

namespace AlphaRooms.Accommodation.Provider.CavalBase.Interfaces
{
    public interface ICavalAvailabilityRequestFactory
    {
        cavalHotelAvailabilityRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
