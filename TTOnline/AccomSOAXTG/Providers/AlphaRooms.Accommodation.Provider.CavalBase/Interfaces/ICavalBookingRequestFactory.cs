﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;

namespace AlphaRooms.Accommodation.Provider.CavalBase.Interfaces
{
    public interface ICavalBookingRequestFactory
    {
        cavalHotelBookingConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
