﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;

namespace AlphaRooms.Accommodation.Provider.CavalBase.Factories
{
    public abstract class CavalRequestFactoryBase
    {
        protected virtual availRQOccupation[] CreateOccupancy(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            var availRqOccupations = new List<availRQOccupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new availRQOccupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenAndInfantsCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }

        protected  virtual occupation[] CreateOccupancy(AccommodationProviderValuationRequestRoom[] rooms)
        {
            var availRqOccupations = new List<occupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new occupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenAndInfantsCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1,
                    roomCode = room.AvailabilityResult.RoomCode
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }

        protected virtual int?[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => (int?)guest.Age).ToArray();

        }
    }
}
