﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.CavalBase
{
    public abstract class CavalAvailabilityParserBase
    {
        protected virtual bool HasResult(cavalHotelAvailabilityRS supplierResponse)
        {
            return ( supplierResponse != null && 
                     supplierResponse.resultCode == 200 &&
                     supplierResponse.availableEstablishments != null);
        }


        /// <summary>
        /// Price is returned as combined prices for the requested rooms.  This methods devides the prices by the number of rooms generates a row for each rooms
        /// </summary>
        /// <param name="board"></param>
        /// <param name="roomOccupations"></param>
        /// <param name="request"></param>
        /// <param name="establishment"></param>
        /// <param name="availabilityResults"></param>
        protected virtual void GenerateRoomPrices(boardPrice board, roomOccupation[] roomOccupations, AccommodationProviderAvailabilityRequest request, availableEstablishment establishment, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            int roomNumber = 1;
            foreach (var room in roomOccupations)
            {
                var result = new AccommodationProviderAvailabilityResult();

                result.RoomNumber = (byte)roomNumber;
                result.ProviderEdiCode = request.Provider.EdiCode;
                result.EstablishmentName = establishment.establishmentName;
                result.EstablishmentEdiCode = establishment.establishmentId;
                result.DestinationEdiCode = establishment.cityId;

                result.BoardDescription = board.boardName;
                result.BoardCode = board.boardCode;
                result.RoomCode = room.roomCode;
                result.RoomDescription = room.roomName.Trim();
                result.PaymentModel = PaymentModel.PostPayment;
                result.RateType = RateType.NetStandard;
                result.IsOpaqueRate = false;
                result.IsNonRefundable = false;
                result.NumberOfAvailableRooms = 0;

                var amount = board.netPrice.value / roomOccupations.Length;

                result.SalePrice = new Money
                {
                    Amount = (decimal)amount,
                    CurrencyCode = board.netPrice.currencyCode
                };

                result.CostPrice = result.SalePrice;
                //result.CostPrice = new Money
                //{
                //    Amount = (decimal) board.oldNetPrice.value/request.Rooms.Length,
                //    CurrencyCode = board.oldNetPrice.currencyCode
                //};

                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                result.Adults = (byte)room.adultsPerRoom;
                result.Children = (byte)room.childrenPerRoom;
                result.Infants = 0;
                result.CheckInDate = request.CheckInDate;
                result.CheckOutDate = request.CheckOutDate;

                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("TotalPrice", board.netPrice.value.ToString());
                result.ProviderSpecificData.Add("Key", board.key);

                availabilityResults.Enqueue(result);

                roomNumber++;
            }

        }

    }
}
