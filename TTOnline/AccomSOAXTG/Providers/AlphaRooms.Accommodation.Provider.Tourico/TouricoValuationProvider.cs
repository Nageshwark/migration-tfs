﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Tourico;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoValuationProvider : AccommodationValuationBase
    {
        //private readonly IAccommodationValuationAutomatorAsync<TouricoValuationResponse> automator;
        //private readonly IAccommodationValuationParser<TouricoValuationResponse> parser;

        private readonly IAccommodationValuationAutomatorAsync<TouricoValuationResponse> automator;
        private readonly IAccommodationValuationParser<TouricoValuationResponse> parser;

        public TouricoValuationProvider(IAccommodationValuationAutomatorAsync<TouricoValuationResponse> automator,
                                                IAccommodationValuationParser<TouricoValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }


    }
}
