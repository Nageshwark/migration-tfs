﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoValuationResponse
    {
        public object SupplierRequest { get; set; }
        public SearchResult SupplierResponse { get; set; }
    }
}
