﻿//using AlphaRooms.Configuration;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Provider.Tourico;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;
using AlphaRooms.Utilities;
using Ninject;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<TouricoAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<TouricoAvailabilityResponse> parser;

        public TouricoAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<TouricoAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<TouricoAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
