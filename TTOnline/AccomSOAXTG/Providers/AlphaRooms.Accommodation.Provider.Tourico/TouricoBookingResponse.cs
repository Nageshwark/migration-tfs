﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoBookingResponse
    {
        public TouricoService.BookV3Request SupplierRequest { get; set; }
        public TouricoService.BookHotelV3Response SupplierResponse { get; set; }
    }
}
