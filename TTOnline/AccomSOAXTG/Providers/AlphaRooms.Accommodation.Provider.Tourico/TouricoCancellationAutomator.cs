﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoReservationService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using Culture = AlphaRooms.Accommodation.Provider.Tourico.TouricoService.Culture;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoCancellationAutomator : IAccommodationCancellationAutomatorAsync<TouricoCancellationResponse>
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string TouricoCancallationUrl = "TouricoCancallationUrl";
        private const string TouricoHotelServiceUrl = "TouricoHotelServiceUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ITouricoProviderCancellationRequestFactory _touricoProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public TouricoCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ITouricoProviderCancellationRequestFactory TouricoSupplierCancellationRequestFactory
                                            //,IProviderOutputLogger outputLogger
                                                )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            _touricoProviderCancellationRequestFactory = TouricoSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<TouricoCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            TouricoCancellationResponse response = new TouricoCancellationResponse();

            try
            {
                RGInfoRequest cancelRequest = _touricoProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                response = await GetSupplierCancellationResponseAsync(request, cancelRequest);

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("TouricoBookingAutomator.GetCancellationResponseAsync: Tourico supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        private async Task<TouricoCancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, RGInfoRequest supplierRequest)
        {

            var resClient = ConfigureReservationSoapClient(request);

            var authHeader = _touricoProviderCancellationRequestFactory.CreateAuthHeader(request.Provider.Parameters);

            var client = ConfigureSoapClient(request);
            RGInfoResults rgInfoResults = client.GetRGInfo(authHeader, supplierRequest);

            var logHeader = _touricoProviderCancellationRequestFactory.CreateLoginHeader(request.Provider.Parameters);

            var canclResponse = new CancelReservationResponse();

            foreach (var res in rgInfoResults.ResGroup.Reservations)
            {
                int resId = res.reservationId;
                canclResponse.CancelReservationResult = resClient.CancelReservation(logHeader, resId);
            }

            var supplierResponse = new TouricoCancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = canclResponse
            };

            return supplierResponse;
        }

        private HotelFlowClient ConfigureSoapClient(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(TouricoHotelServiceUrl)));
            var client = new HotelFlowClient(binding, address);

            return client;
        }

        private ReservationsServiceSoapClient ConfigureReservationSoapClient(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(TouricoCancallationUrl)));
            var client = new ReservationsServiceSoapClient(binding, address);

            return client;
        }
    }
}
