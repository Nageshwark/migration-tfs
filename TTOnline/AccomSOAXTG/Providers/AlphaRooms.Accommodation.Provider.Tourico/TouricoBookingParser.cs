﻿
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoBookingParser : IAccommodationBookingParser<IEnumerable<TouricoBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public TouricoBookingParser(ILogger logger
            //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<TouricoBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Tourico Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        BookV3Request supplierRequest,
                                                        BookHotelV3Response supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null
                               );

            if (hasResults)
            {
                int rgId = supplierResponse.BookHotelV3Result.ResGroup.rgId;

                int roomNumber = 1;
                foreach (var res in supplierResponse.BookHotelV3Result.ResGroup.Reservations)
                {
                    var result = new AccommodationProviderBookingResult();

                    result.BookingStatus = BookingStatus.Confirmed;
                    result.ProviderBookingReference = rgId.ToString();
                    result.Message = "";
                    
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("TouricoRoomReservationId_" + roomNumber, res.reservationId.ToString());

                    result.RoomNumber = (byte)roomNumber;

                    bookingResults.Enqueue(result);

                    roomNumber++;
                }

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }


    }
}
