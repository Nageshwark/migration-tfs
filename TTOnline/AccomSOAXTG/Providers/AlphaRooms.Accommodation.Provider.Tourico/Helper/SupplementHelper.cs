﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Tourico.Helper
{
    public class SupplementHelper
    {
        public string GetSupplementString(Supplement[] selctedSupplements)
        {
            var supplmentString = new StringBuilder();

            foreach (var supplement in selctedSupplements)
            {
                supplmentString.AppendLine(string.Format("Name:{0}, Amount:{1}, Mandatory:{2}, ChargeType:{3}, Type:{4}",
                                                            supplement.suppName, supplement.price, supplement.suppIsMandatory, supplement.suppChargeType.ToString(), supplement.supptType));
            }

            return supplmentString.ToString();
        }

        public decimal GetSupplementCost(Supplement[] selctedSupplements)
        {
            decimal supplementCost = 0;

            foreach (var supplement in selctedSupplements.Where(x=>x.suppIsMandatory && x.suppChargeType== ChargeType.Addition))
            {
                if (supplement.GetType() == typeof(PerPersonSupplement))     //PerPersonSupplement
                {
                    supplementCost += ((PerPersonSupplement) supplement).SuppAgeGroup.Sum(sup => sup.suppQuantity*sup.suppPrice);
                }
                else if (supplement.GetType() == typeof(PerRoomSupplement))    //PerRoomSuplement
                {
                    supplementCost += supplement.price;
                }
                
            }
           

            return supplementCost;
        }

        /// <summary>
        /// Deserialises SelectedSupplement array from a string and converts into a SupplementInfo array needed at the time of booking
        /// </summary>
        /// <param name="supplementData">Seralised version of the SelectedSupplement Array</param>
        /// <returns>SupplementInfo Array</returns>
        public SupplementInfo[] GetSelectedSupplements(string supplementData)
        {
            var supplements = new List<SupplementInfo>();

            if (string.IsNullOrEmpty(supplementData))
                return null;

            var selectedSupplements = supplementData.XmlDeSerialize<Supplement[]>();

            if (selectedSupplements != null && selectedSupplements.Any())
            {
                foreach (var supplement in selectedSupplements.Where(s => s.suppIsMandatory))
                {
                    var suppInfo = new SupplementInfo
                    {
                        SupAgeGroup = GetSupAgeGroup(supplement),
                        suppId = supplement.suppId,
                        suppType = supplement.supptType,
                    };

                    supplements.Add(suppInfo);
                }
            }

            return supplements.ToArray();
        }

        private SuppAges[] GetSupAgeGroup(Supplement supplement)
        {
            var suppAgeses = new List<SuppAges>();

            if (supplement.GetType() == typeof(PerPersonSupplement))
            {
                foreach (var supplementAge in ((PerPersonSupplement)supplement).SuppAgeGroup)
                {
                    var supAge = new SuppAges
                    {
                        suppFrom = supplementAge.suppFrom,
                        suppPrice = supplementAge.suppPrice,
                        suppQuantity = supplementAge.suppQuantity,
                        suppTo = supplementAge.suppTo
                    };

                    suppAgeses.Add(supAge);
                }
            }
            else
                return null;

            return suppAgeses.ToArray();
        }
    }
}
