﻿
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;

using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<TouricoAvailabilityResponse>
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string TouricoMaxPrice = "TouricoVersion";
        private const string TouricoStarLevel = "TouricoStarLevel";
        private const string TouricoAvailableOnly = "TouricoAvailableOnly";
        private const string TouricoExactDestination = "TouricoExactDestination";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string TouricoAvailabilityUrl = "TouricoAvailabilityUrl";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ITouricoProviderAvailabilityRequestFactory _touricoProviderAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public TouricoAvailabilityAutomator(ILogger logger,
            //IAccommodationConfigurationManager configurationManager,
                                                ITouricoProviderAvailabilityRequestFactory TouricoProviderAvailabilityRequestFactory
            //IProviderOutputLogger outputLogger
                                                )
        {
            this.logger = logger;
            // this.configurationManager = configurationManager;
            this._touricoProviderAvailabilityRequestFactory = TouricoProviderAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<TouricoAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            var response = new TouricoAvailabilityResponse();

            try
            {

                var supplierRequest = this._touricoProviderAvailabilityRequestFactory.CreateProviderAvailabilityRequest(request);
                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);


            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");

                    if (response.SupplierResponse != null)
                    {
                        sb.AppendLine(response.XmlSerialize());
                    }
                    else
                    {
                        sb.AppendLine("Supplier Request Not Present!");
                    }

                    sb.AppendLine();
                    sb.AppendLine("Response:");

                    if (response.SupplierResponse != null)
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    sb.AppendLine();

                }

                throw new SupplierApiException(
                    string.Format(
                        "TouricoAvailabilityAutomator.GetAvailabilityResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                    ex);
            }

            return response;
        }

        private async Task<TouricoAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, object supplierRequest)
        {
            var client = ConfigureSoapClient(request);
            SearchResult response;

            if (supplierRequest.GetType() == typeof (SearchHotelsRequest))
            {
                var searchHotelsRequest = (SearchHotelsRequest) supplierRequest;

                if (request.Debugging)
                {
                    string seralisedRequest = searchHotelsRequest.XmlSerialize();
                }

                response = client.SearchHotels(searchHotelsRequest.AuthenticationHeader, searchHotelsRequest.request);
            }
            else
            {
                var searchHotelsByIdRequest = (SearchHotelsByIdRequest) supplierRequest;

                if (request.Debugging)
                {
                    string seralisedRequest = searchHotelsByIdRequest.XmlSerialize();
                }

                var authHeader = _touricoProviderAvailabilityRequestFactory.CreateAuthHeader(request.Provider.Parameters);

                response = client.SearchHotelsById(authHeader, searchHotelsByIdRequest);
            }
            

            var supplierResponse = new TouricoAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private HotelFlowClient ConfigureSoapClient(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(TouricoAvailabilityUrl)));
            var client = new HotelFlowClient(binding, address);

            return client;
        }

       
    }
}
