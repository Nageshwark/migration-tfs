﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoReservationService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoCancellationResponse
    {
        public RGInfoRequest SupplierRequest { get; set; }
        public CancelReservationResponse SupplierResponse { get; set; }
    }
}
