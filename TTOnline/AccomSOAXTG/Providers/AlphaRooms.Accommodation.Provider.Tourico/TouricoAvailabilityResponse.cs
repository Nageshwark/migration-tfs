﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;


namespace AlphaRooms.Accommodation.Provider.Tourico
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Tourico accepts and returns XML strings.
    /// </summary>
    public class TouricoAvailabilityResponse
    {
        public object SupplierRequest { get; set; }
        public SearchResult SupplierResponse { get; set; }
    }
}
