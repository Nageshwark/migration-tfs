﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico;
using AlphaRooms.Accommodation.Provider.Tourico.Helper;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{

    public class TouricoValuationParser : IAccommodationValuationParser<TouricoValuationResponse>
    {
        private readonly ILogger logger;

        //private readonly IAccommodationConfigurationManager configurationManager;

        public TouricoValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }


        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, TouricoValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateValuationResultsFromResponse(request, response.SupplierResponse, valuationResults);
               
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Tourico Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationResultsFromResponse(AccommodationProviderValuationRequest request, TouricoService.SearchResult supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> valuationResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (supplierResponse != null && supplierResponse.HotelList != null && supplierResponse.HotelList.Any());

            if (hasResults)
            {
                var hotel = supplierResponse.HotelList.First();
                var availabilityResult = request.SelectedRooms.First().AvailabilityResult;
                var supplementHelper = new SupplementHelper();

                foreach (var roomType in hotel.RoomTypes.Where(rt=>rt.hotelRoomTypeId == Convert.ToInt32(availabilityResult.RoomCode)))
                {
                    foreach (var room in roomType.Occupancies.First().Rooms)
                    {
                        var occpncy = roomType.Occupancies.First();

                        var result = new AccommodationProviderValuationResult();

                        result.RoomNumber = (byte) room.seqNum;

                        result.DestinationEdiCode = hotel.Location.city;
                        result.EstablishmentEdiCode = hotel.hotelId.ToString();
                        result.EstablishmentName = hotel.name;

                        result.RoomDescription = roomType.roomTypeCategory;
                        result.RoomCode = roomType.hotelRoomTypeId.ToString();

                        result.CheckInDate = availabilityResult.CheckInDate;
                        result.CheckOutDate = availabilityResult.CheckOutDate;

                        if (occpncy.BoardBases.Any())
                        {
                            result.BoardCode = occpncy.BoardBases.First().bbId.ToString();
                            result.BoardDescription = occpncy.BoardBases.First().bbName.ToString();
                        }

                        result.ProviderName = hotel.provider;

                        Decimal suppCost = 0;
                        if (occpncy.SelctedSupplements.Any())
                        {
                            suppCost = supplementHelper.GetSupplementCost(occpncy.SelctedSupplements);
                        }

                        result.SalePrice = new Money()
                        {
                            Amount = suppCost + Convert.ToDecimal(occpncy.occupPrice),
                            CurrencyCode = hotel.currency
                        };


                        result.CostPrice = result.SalePrice;

                        result.Adults = (byte) occpncy.Rooms[room.seqNum - 1].AdultNum;
                        result.Children = (byte) occpncy.Rooms[room.seqNum - 1].ChildNum;

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("TouricoHotelRoomTypeId", roomType.hotelRoomTypeId.ToString());
                        result.ProviderSpecificData.Add("TouricoRoomId", roomType.roomId.ToString());
                        result.ProviderSpecificData.Add("Supplement", supplementHelper.GetSupplementString(occpncy.SelctedSupplements));
                       
                        if (occpncy.SelctedSupplements.Any())
                            result.ProviderSpecificData.Add("SupplementData", occpncy.SelctedSupplements.XmlSerialize());

                        valuationResults.Enqueue(result);
                    }
                }
            }
            else
            {
                string message = "Tourico Valuation Parser: Valuation Response cannot be parsed because it is null.";

                throw new SupplierApiDataException(message);
            }

        }

        

    }
}
