﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<TouricoBookingResponse>>
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string TouricoBookingUrl = "TouricoBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ITouricoProviderBookingRequestFactory _touricoProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public TouricoBookingAutomator(ILogger logger,
            //IAccommodationConfigurationManager configurationManager,
                                            ITouricoProviderBookingRequestFactory TouricoSupplierBookingRequestFactory
            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            configurationManager = configurationManager;
            _touricoProviderBookingRequestFactory = TouricoSupplierBookingRequestFactory;
            outputLogger = outputLogger;
        }

        public async Task<IEnumerable<TouricoBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<TouricoBookingResponse> responses = new List<TouricoBookingResponse>();

            BookV3Request supplierRequest = null;
            TouricoBookingResponse response = null;

            try
            {
                supplierRequest = _touricoProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("TouricoBookingAutomator.GetBookingResponseAsync: Tourico supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<TouricoBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, BookV3Request supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request);

            var authHeader = _touricoProviderBookingRequestFactory.CreateAuthHeader(request.Provider.Parameters);

            var bookResponse = await client.BookHotelV3Async(authHeader, supplierRequest); 

            var supplierResponse = new TouricoBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = bookResponse
            };

            return supplierResponse;
        }

        private HotelFlowClient ConfigureSoapClient(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(TouricoBookingUrl)));
            var client = new HotelFlowClient(binding, address);

            return client;
        }
    }
}
