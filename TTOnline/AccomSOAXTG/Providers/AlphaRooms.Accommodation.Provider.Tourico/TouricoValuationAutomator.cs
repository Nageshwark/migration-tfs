﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoValuationAutomator : IAccommodationValuationAutomatorAsync<TouricoValuationResponse>
    {
        private const string TouricoValuationUrl = "TouricoValuationUrl";
        private const string TouricoUsername = "TouricoUsername";
        private const string TouricoPassword = "TouricoPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";


        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ITouricoProviderValuationRequestFactory _touricoProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public TouricoValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ITouricoProviderValuationRequestFactory TouricoSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            _touricoProviderValuationRequestFactory = TouricoSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<TouricoValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            CheckAvailabilityAndPricesRequest supplierRequest = null;
            TouricoValuationResponse response = null;
            try
            {
                supplierRequest = _touricoProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetAvailabilityResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");

                    if (response.SupplierResponse != null)
                    {
                        sb.AppendLine(response.XmlSerialize());
                    }
                    else
                    {
                        sb.AppendLine("Supplier Request Not Present!");
                    }

                    sb.AppendLine();
                    sb.AppendLine("Response:");

                    if (response.SupplierResponse != null)
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    sb.AppendLine();

                }

                throw new SupplierApiException(string.Format("TouricoAvailabilityAutomator.GetAvailabilityResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }


        public async Task<TouricoValuationResponse> GetAvailabilityResponseAsync(AccommodationProviderValuationRequest request, CheckAvailabilityAndPricesRequest supplierRequest)
        {
            if (request.Debugging)
            {
                string seralisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request);
            SearchResult response = client.CheckAvailabilityAndPrices(supplierRequest.AuthenticationHeader, supplierRequest.request);

            var supplierResponse = new TouricoValuationResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private HotelFlowClient ConfigureSoapClient(AccommodationProviderValuationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(TouricoValuationUrl)));
            var client = new HotelFlowClient(binding, address);

            return client;
        }
    }
}
