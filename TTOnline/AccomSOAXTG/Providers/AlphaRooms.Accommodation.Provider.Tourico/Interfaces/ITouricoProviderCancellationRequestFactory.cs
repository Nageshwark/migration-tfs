﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoReservationService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Tourico.Interfaces
{
    public interface ITouricoProviderCancellationRequestFactory
    {
        RGInfoRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
        AuthenticationHeader CreateAuthHeader(List<AccommodationProviderParameter> parameters);
        LoginHeader CreateLoginHeader(List<AccommodationProviderParameter> parameters);
    }
}
