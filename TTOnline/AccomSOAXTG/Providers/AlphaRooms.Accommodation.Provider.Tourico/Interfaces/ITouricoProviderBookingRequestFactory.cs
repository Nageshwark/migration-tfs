﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico.Interfaces
{
    public interface ITouricoProviderBookingRequestFactory
    {
        BookV3Request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
        AuthenticationHeader CreateAuthHeader(List<AccommodationProviderParameter> parameters);
    }
}
