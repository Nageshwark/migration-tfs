﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Tourico.Interfaces
{
    public interface ITouricoProviderValuationRequestFactory
    {
        TouricoService.CheckAvailabilityAndPricesRequest CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
