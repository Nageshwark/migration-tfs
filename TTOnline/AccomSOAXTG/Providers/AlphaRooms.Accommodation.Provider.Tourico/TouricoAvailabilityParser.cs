﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
//using System.Runtime.Serialization.Json;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico.Helper;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Tourico
{
    public class TouricoAvailabilityParser : IAccommodationAvailabilityParser<TouricoAvailabilityResponse>
    {

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;


        public TouricoAvailabilityParser(ILogger logger)
        //IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request, TouricoAvailabilityResponse responses)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, responses.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "Tourico Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, TouricoService.SearchResult supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (supplierResponse != null && supplierResponse.HotelList != null && supplierResponse.HotelList.Any());

            if (hasResults)
            {
                var supplementHelper = new SupplementHelper();

                foreach (var hotel in supplierResponse.HotelList)
                {
                    foreach (var roomType in hotel.RoomTypes)
                    {
                        int roomNumber = 1;
                        foreach (var room in roomType.Occupancies.First().Rooms)
                        {
                            var occpncy = roomType.Occupancies.First();

                            var result = new AccommodationProviderAvailabilityResult();

                            result.RoomNumber = (byte)room.seqNum;

                            result.DestinationEdiCode = hotel.Location.city;
                            result.EstablishmentEdiCode = hotel.hotelId.ToString();
                            result.EstablishmentName = hotel.name;

                            result.RoomDescription = roomType.roomTypeCategory;
                            result.RoomCode = roomType.hotelRoomTypeId.ToString();

                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckOutDate;

                            if (occpncy.BoardBases.Any())
                            {
                                result.BoardCode = occpncy.BoardBases.First().bbId.ToString();
                                result.BoardDescription = occpncy.BoardBases.First().bbName.ToString();
                            }

                            result.ProviderName = hotel.provider;

                            Decimal suppCost = 0;
                            if (occpncy.SelctedSupplements.Any())
                            {
                                suppCost = supplementHelper.GetSupplementCost(occpncy.SelctedSupplements);
                            }

                            result.SalePrice = new Money()
                            {
                                Amount = suppCost + Convert.ToDecimal(occpncy.occupPrice),
                                CurrencyCode = hotel.currency
                            };


                            result.CostPrice = result.SalePrice;

                            result.Adults = (byte)occpncy.Rooms[room.seqNum-1].AdultNum;
                            result.Children = (byte)occpncy.Rooms[room.seqNum-1].ChildNum;

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("TouricoHotelRoomTypeId", roomType.hotelRoomTypeId.ToString());
                            result.ProviderSpecificData.Add("TouricoRoomId", roomType.roomId.ToString());
                            result.ProviderSpecificData.Add("Supplement", supplementHelper.GetSupplementString(occpncy.SelctedSupplements));

                            availabilityResults.Enqueue(result);

                            roomNumber++;
                        }
                    }
                }
            }
            else
            {
                string message = "Tourico Availability Parser: Availability Response cannot be parsed because it is null.";

                throw new SupplierApiDataException(message);
            }
        }
    }
}
