﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoReservationService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Provider.Tourico.Factories
{
    public class TouricoProviderCancellationlRequestFactory :TouricoProviderFactoryBase, ITouricoProviderCancellationRequestFactory
    {
        public RGInfoRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new TouricoService.GetRGInfoRequest
            {
                request = new RGInfoRequest()
                {
                    RGId = Convert.ToInt32(request.ProviderBookingReference)
                },
                AuthenticationHeader = CreateAuthHeader(request.Provider.Parameters)
            };


            return supplierRequest.request;
        }
    }
}
