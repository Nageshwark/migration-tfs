﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoReservationService;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using Culture = AlphaRooms.Accommodation.Provider.Tourico.TouricoService.Culture;

namespace AlphaRooms.Accommodation.Provider.Tourico.Factories
{
    public abstract class TouricoProviderFactoryBase
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        
        public AuthenticationHeader CreateAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new AuthenticationHeader
            {
                LoginName = parameters.GetParameterValue(TouricoLoginName),
                Password = parameters.GetParameterValue(TouricoPassword),
                Version = parameters.GetParameterValue(TouricoVersion),
                CultureSpecified = true,
                Culture = Culture.en_GB
            };
        }

        public LoginHeader CreateLoginHeader(List<AccommodationProviderParameter> parameters)
        {
            return new LoginHeader()
            {
                username = parameters.GetParameterValue(TouricoLoginName),
                password = parameters.GetParameterValue(TouricoPassword),
                version = parameters.GetParameterValue(TouricoVersion),
                culture = TouricoReservationService.Culture.en_GB
            };
        }
    }
}
