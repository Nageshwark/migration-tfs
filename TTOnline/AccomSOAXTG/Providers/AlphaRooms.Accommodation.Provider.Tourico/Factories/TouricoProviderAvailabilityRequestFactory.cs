﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico.Factories
{
    public class TouricoProviderAvailabilityRequestFactory :TouricoProviderFactoryBase, ITouricoProviderAvailabilityRequestFactory
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string TouricoMaxPrice = "TouricoMaxPrice";
        private const string TouricoStarLevel = "TouricoStarLevel";
        private const string TouricoAvailableOnly = "TouricoAvailableOnly";
        private const string TouricoExactDestination = "TouricoExactDestination";

        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>--
        public object CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            object availRequest = null;
            if (request.DestinationCodes.Any())
            {
                availRequest = CreateSearchHotelsRequest(request);
            }
            else if (request.EstablishmentCodes.Any())
            {
                availRequest = CreateSearchHotelsByIdRequest(request);
            }
            return availRequest;
        }


        private SearchHotelsRequest CreateSearchHotelsRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new SearchHotelsRequest
            {
                AuthenticationHeader = CreateAuthHeader(request.Provider.Parameters),
                request = new SearchRequest
                {
                    Destination = request.DestinationCodes[0].ToString(),
                    CheckIn = Convert.ToDateTime(request.CheckInDate.ToString("yyyy-MM-dd")),
                    CheckOut = Convert.ToDateTime(request.CheckOutDate.ToString("yyyy-MM-dd")),
                    RoomsInformation = GetRoomInfoArray(request).ToArray(),
                    MaxPrice = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoMaxPrice)),
                    StarLevel = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoStarLevel)),
                    AvailableOnly = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(TouricoAvailableOnly)),
                    ExactDestination = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(TouricoExactDestination))
                }
            };

            return supplierRequest;
        }

        

        private List<RoomInfo> GetRoomInfoArray(AccommodationProviderAvailabilityRequest request)
        {
            var lstTourRoomInfo = new List<RoomInfo>();

            foreach (var room in request.Rooms)
            {
                var tourRoomInfo = new RoomInfo();
                tourRoomInfo.AdultNum = room.Guests.AdultsCount;
                tourRoomInfo.ChildNum = room.Guests.ChildrenCount;

                var lstChildAges = new List<ChildAge>();

                if (room.Guests.ChildrenCount > 0)
                {
                    for (int intChld = 0; intChld <= room.Guests.ChildrenCount - 1; intChld++)
                    {
                        lstChildAges.Add(new ChildAge() { age = room.Guests.ChildAndInfantAges[intChld] });
                    }
                }

                tourRoomInfo.ChildAges = lstChildAges.ToArray();

                lstTourRoomInfo.Add(tourRoomInfo);
            }

            return lstTourRoomInfo;
        }

        private SearchHotelsByIdRequest CreateSearchHotelsByIdRequest(AccommodationProviderAvailabilityRequest request)
        {
            var tourSrchHtlReq = new SearchHotelsByIdRequest
            {
                HotelIdsInfo = CreateHotelIdsInfo(request.EstablishmentCodes),
                CheckIn = Convert.ToDateTime(request.CheckInDate.ToString("yyyy-MM-dd")),
                CheckOut = Convert.ToDateTime(request.CheckOutDate.ToString("yyyy-MM-dd")),
                RoomsInformation = GetRoomInfoArray(request).ToArray(),
                MaxPrice = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoMaxPrice)),
                StarLevel = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoStarLevel)),
                AvailableOnly = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(TouricoAvailableOnly))
            };
            
            return tourSrchHtlReq;
        }

        private HotelIdInfo[] CreateHotelIdsInfo(string[] establishmentCodes)
        {
            return establishmentCodes.Select(establishmentCode => new HotelIdInfo
            {
                id = Convert.ToInt32(establishmentCode)
            }).ToArray();
        }
    }
}
