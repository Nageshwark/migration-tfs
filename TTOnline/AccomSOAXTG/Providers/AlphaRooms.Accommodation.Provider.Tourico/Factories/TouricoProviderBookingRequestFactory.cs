﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Tourico.Helper;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Tourico.Factories
{
    public class TouricoProviderBookingRequestFactory :TouricoProviderFactoryBase, ITouricoProviderBookingRequestFactory
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string TouricoMaxPrice = "TouricoVersion";

        private const string TouricoRecordLocatorId = "TouricoRecordLocatorId";
        private const string TouricoHotelRoomTypeId = "TouricoHotelRoomTypeId";
        private const string TouricoRoomId = "TouricoRoomId";
        private const string TouricoDeltaPrice = "TouricoDeltaPrice";

        public BookV3Request CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms.First().ValuationResult;
            var deltaPercent = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoDeltaPrice));
            var deltaPrice = (request.ValuatedRooms.Select(r => r.ValuationResult.SalePrice.Amount).Sum() * deltaPercent) / 100;

            var supplierRequest = new BookV3Request
            {
                RecordLocatorId = 0,
                HotelId = Convert.ToInt32(roomToBook.EstablishmentEdiCode),
                HotelRoomTypeId = Convert.ToInt32(roomToBook.RoomCode),
                CheckIn = Convert.ToDateTime(roomToBook.CheckInDate.ToString("yyyy-MM-dd")),
                CheckOut = Convert.ToDateTime(roomToBook.CheckOutDate.ToString("yyyy-MM-dd")),
                RoomsInfo = CreateRoomResInfo(request),
                PaymentType = PaymentTypes.Obligo,
                AgentRefNumber = Guid.NewGuid().ToString(),
                ContactInfo = "",
                RequestedPrice = request.ValuatedRooms.Select(r=>r.ValuationResult.SalePrice.Amount).Sum(),
                DeltaPrice = deltaPrice,
                Currency = roomToBook.SalePrice.CurrencyCode,
                IsOnlyAvailable = true
                
            };
            
            return supplierRequest;
        }

        private RoomReserveInfo[] CreateRoomResInfo(AccommodationProviderBookingRequest request)
        {
            var roomReserveInfos = new List<RoomReserveInfo>();
            var helper = new SupplementHelper();

            foreach (var room in request.ValuatedRooms)
            {
                string supplementData = string.Empty;

                if (room.ValuationResult.ProviderSpecificData.ContainsKey("SupplementData"))
                    supplementData = room.ValuationResult.ProviderSpecificData["SupplementData"];

                roomReserveInfos.Add(new RoomReserveInfo
                {
                    AdultNum = room.Guests.AdultsCount,
                    ChildNum = room.Guests.ChildrenAndInfantsCount,
                    ChildAges = GetChildAges(room.Guests),
                    ContactPassenger = new ContactPassenger
                    {
                        FirstName = request.Customer.FirstName,
                        LastName = request.Customer.Surname,
                        HomePhone = request.Customer.ContactNumber,
                        MobilePhone = ""
                    },
                    SelectedSupplements = helper.GetSelectedSupplements(supplementData),
                    //Note = room.SpecialRequests.
                    
                });
            }
            
        return roomReserveInfos.ToArray();

        }

        

        private ChildAge[] GetChildAges(AccommodationProviderBookingRequestRoomGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child || g.Type == GuestType.Infant).Select(guest => new ChildAge
            {
                age = guest.Age
            }).ToArray();
        }
    }
}
