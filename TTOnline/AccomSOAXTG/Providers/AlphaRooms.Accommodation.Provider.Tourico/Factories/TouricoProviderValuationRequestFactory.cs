﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico.TouricoService;

namespace AlphaRooms.Accommodation.Provider.Tourico.Factories
{
    public class TouricoProviderValuationRequestFactory :TouricoProviderFactoryBase, ITouricoProviderValuationRequestFactory
    {
        private const string TouricoLoginName = "TouricoLoginName";
        private const string TouricoPassword = "TouricoPassword";
        private const string TouricoVersion = "TouricoVersion";
        private const string TouricoMaxPrice = "TouricoMaxPrice";
        private const string TouricoStarLevel = "TouricoStarLevel";
        private const string TouricoAvailableOnly = "TouricoAvailableOnly";
        private const string TouricoExactDestination = "TouricoExactDestination";
        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CheckAvailabilityAndPricesRequest CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new CheckAvailabilityAndPricesRequest
            {
                AuthenticationHeader = CreateAuthHeader(request.Provider.Parameters),
                request = CreateSearchHotelsByIdRequest(request)
            };

            return supplierRequest;
        }

        private List<RoomInfo> GetRoomInfo(AccommodationProviderValuationRequest request)
        {
            var lstTourRoomInfo = new List<RoomInfo>();

            foreach (var room in request.SelectedRooms)
            {
                var tourRoomInfo = new RoomInfo();
                tourRoomInfo.AdultNum = room.Guests.AdultsCount;
                tourRoomInfo.ChildNum = room.Guests.ChildrenCount;

                var lstChildAges = new List<ChildAge>();

                if (room.Guests.ChildrenCount > 0)
                {
                    for (int intChld = 0; intChld <= room.Guests.ChildrenCount - 1; intChld++)
                    {
                        lstChildAges.Add(new ChildAge() { age = room.Guests.ChildAndInfantAges[intChld] });
                    }
                }

                tourRoomInfo.ChildAges = lstChildAges.ToArray();

                lstTourRoomInfo.Add(tourRoomInfo);
            }

            return lstTourRoomInfo;
        }

        private SearchHotelsByIdRequest CreateSearchHotelsByIdRequest(AccommodationProviderValuationRequest request)
        {
            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;
            
            var supplierRequest = new SearchHotelsByIdRequest
            {
                HotelIdsInfo = new[]
                {
                    new HotelIdInfo
                    {
                        id = Convert.ToInt32(availabilityResult.EstablishmentEdiCode)
                    }
                },
                CheckIn = Convert.ToDateTime(availabilityResult.CheckInDate.ToString("yyyy-MM-dd")),
                CheckOut = Convert.ToDateTime(availabilityResult.CheckOutDate.ToString("yyyy-MM-dd")),
                RoomsInformation = GetRoomInfo(request).ToArray(),
                MaxPrice = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoMaxPrice)),
                StarLevel = Convert.ToDecimal(request.Provider.Parameters.GetParameterValue(TouricoStarLevel)),
                AvailableOnly = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(TouricoAvailableOnly))
            };


            return supplierRequest;
        }
        

    }
}
