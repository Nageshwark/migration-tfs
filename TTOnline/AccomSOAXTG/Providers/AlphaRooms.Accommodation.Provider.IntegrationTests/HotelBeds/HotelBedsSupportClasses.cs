﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;


namespace AlphaRooms.Accommodation.IntegrationTests.HotelBeds
{
    public static class HotelBedsSupportClasses
    {
        public static List<AccommodationProviderBookingRequestRoomSpecialRequest> SpecialBookingRequests { get; set; }

        public static List<string> MaleAdultTitles { get; set; }
        public static List<string> MaleChildTitles { get; set; }

        public static List<string> FemaleAdultTitles { get; set; }
        public static List<string> FemaleChildTitles { get; set; }

        public static List<string> MaleFirstNames { get; set; }
        public static List<string> FemaleFirstNames { get; set; }
        public static List<string> LastNames { get; set; }

        public static List<string> AddressLine1 { get; set; }
        public static List<string> AddressLine2 { get; set; }
        public static List<string> Cities { get; set; }
        public static List<string> Counties { get; set; }
        public static List<string> Postcodes { get; set; }

        public static List<string> HomePhones { get; set; }
        public static List<string> MobilePhones { get; set; }
        public static List<string> EmailAddresses { get; set; }

        public static List<TestCreditCard> TestCreditCards { get; set; }

        public class TestCreditCard
        {
            public CardType CardType { get; set; }
            public string CardNumber { get; set; }
        }

        static HotelBedsSupportClasses()
        {
            SpecialBookingRequests = new List<AccommodationProviderBookingRequestRoomSpecialRequest>()
            {
                new AccommodationProviderBookingRequestRoomSpecialRequest() {},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {AdjoiningRooms = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {CotRequired = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {DisabledAccess = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {LateArrival = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {NonSmoking = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {SeaViews = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {OtherRequests = "Other Requests..."},
                new AccommodationProviderBookingRequestRoomSpecialRequest() {AdjoiningRooms = true, CotRequired = true},
                new AccommodationProviderBookingRequestRoomSpecialRequest()
                {
                    AdjoiningRooms = true,
                    CotRequired = true,
                    DisabledAccess = true
                },
                new AccommodationProviderBookingRequestRoomSpecialRequest()
                {
                    AdjoiningRooms = true,
                    CotRequired = true,
                    DisabledAccess = true,
                    LateArrival = true
                },
                new AccommodationProviderBookingRequestRoomSpecialRequest()
                {
                    AdjoiningRooms = true,
                    CotRequired = true,
                    DisabledAccess = true,
                    LateArrival = true,
                    NonSmoking = true
                },
                new AccommodationProviderBookingRequestRoomSpecialRequest()
                {
                    AdjoiningRooms = true,
                    CotRequired = true,
                    DisabledAccess = true,
                    LateArrival = true,
                    NonSmoking = true,
                    SeaViews = true
                },
                new AccommodationProviderBookingRequestRoomSpecialRequest()
                {
                    AdjoiningRooms = true,
                    CotRequired = true,
                    DisabledAccess = true,
                    LateArrival = true,
                    NonSmoking = true,
                    SeaViews = true,
                    OtherRequests = "Other Room Requests..."
                },
            };

            MaleAdultTitles = new List<string>
            {
                "Mr"
            };

            MaleChildTitles = new List<string>
            {
                "Mr",
                "Master"
            };

            FemaleAdultTitles = new List<string>
            {
                "Mrs",
                "Miss",
                "Ms"
            };

            FemaleChildTitles = new List<string>
            {
                "Miss"
            };

            MaleFirstNames = new List<string>
            {
                "Liam",
                "Noah",
                "Ethan",
                "Mason",
                "Logan",
                "Lucas",
                "Jacob",
                "Aiden",
                "Jackson",
                "Jack",
                "Elijah",
                "Benjamin",
                "James",
                "Luke",
                "Alexander",
                "Michael",
                "William",
                "Oliver",
                "Gabriel",
                "Daniel",
                "Carter",
                "Henry",
                "Owen",
                "Matthew",
                "Ryan",
                "Jayden",
                "Wyatt",
                "Caleb",
                "Nathan",
                "Andrew",
                "Dylan",
                "Joshua",
                "Isaac",
                "Sebastian",
                "David",
                "Connor",
                "Hunter",
                "Eli",
                "Landon",
                "Samuel"
            };

            FemaleFirstNames = new List<string>
            {
                "Emma",
                "Olivia",
                "Sophia",
                "Ava",
                "Isabella",
                "Mia",
                "Charlotte",
                "Emily",
                "Abigail",
                "Harper",
                "Avery",
                "Ella",
                "Madison",
                "Amelia",
                "Lily",
                "Chloe",
                "Sofia",
                "Evelyn",
                "Zoey",
                "Hannah",
                "Aria",
                "Grace",
                "Addison",
                "Aubrey",
                "Ellie",
                "Audrey",
                "Zoe",
                "Natalie",
                "Scarlett",
                "Elizabeth",
                "Layla",
                "Victoria",
                "Brooklyn",
                "Lillian",
                "Lucy",
                "Mila",
                "Claire",
                "Anna",
                "Nora",
                "Savannah"
            };

            LastNames = new List<string>
            {
                "Brown",
                "Smith",
                "Patel",
                "Jones",
                "Williams",
                "Johnson",
                "Taylor",
                "Thomas",
                "Roberts",
                "Khan",
                "Lewis",
                "Jackson",
                "Clarke",
                "James",
                "Phillips",
                "Wilson",
                "Ali",
                "Mason",
                "Mitchell",
                "Rose",
                "Davis",
                "Davies",
                "Cox",
                "Alexander",
                "Martin",
                "Bernard",
                "Dubois",
                "Thomas",
                "Robert",
                "Richard",
                "Petit",
                "Durand",
                "Leroy",
                "Moreau",
                "Simon",
                "Laurent",
                "Lefebvre",
                "Michel"
            };

            AddressLine1 = new List<string>
            {
                "53 Larking Drive",
                "Apt. 2a, Grosvenor Court",
                "101 Pentonville Road",
                "2 St. Michaels Ave.",
                "60 New Park Drive",
                "25 Mont Blanc Bld.",
                "12 Municipal Way",
                "The Mount"
            };

            AddressLine2 = new List<string>
            {
                "Bancroft Heights",
                "Leverstock Green",
                "Highgate",
                "Cambden",
                "Flintwick",
                "Warner's End",
                "Gadebridge",
                "Oxnards",
                "Piggotts"
            };

            Cities = new List<string>
            {
                "London",
                "Hemel Hemsptead",
                "Bridgetown",
                "St. Johns",
                "St. Albans",
                "Hull",
                "Leeds",
                "Birmingham",
                "York"
            };

            Counties = new List<string>
            {
                "Humberside",
                "Hertfordshire",
                "Bucks",
                "West Yorkshire",
                "Cheshire",
                "Cornwall",
                "Somerset",
                "Leicestershire",
                "Northumberland"
            };

            Postcodes = new List<string>
            {
                "HP2 4QQ",
                "AL7 6DF",
                "LS8 7TY",
                "CD5 9PP",
                "LE7 5JJ",
                "PZ1 1HG",
                "SO2 7JP",
                "BM4 1LM",
                "SE23 5PQ"
            };

            HomePhones = new List<string>
            {
                "01442 568941",
                "05842 657943",
                "01727 684572",
                "01582 564321",
                "0207 564 8465",
                "0208 544 3669",
                "01636 555465",
                "01442 845584",
                "01923 566322"
            };

            MobilePhones = new List<string>
            {
                "0711 156 8941",
                "0722 265 7943",
                "0733 368 4572",
                "0744 456 4321",
                "0755 564 8465",
                "0766 544 3669",
                "0777 655 5465",
                "0788 884 5584",
                "0799 956 6322"
            };

            EmailAddresses = new List<string>
            {
                "peter@hotmail.com",
                "jane@yahoo.com",
                "michael@gmail.com",
                "jones@rocketmail.com",
                "patekl@gmail.com",
                "ronnie@hotmail.com",
                "mary@mymail.com",
                "elisa@rodgers.co.uk",
                "fred@yahoo.com"
            };

            TestCreditCards = new List<TestCreditCard>
            {
                new TestCreditCard() {CardType = CardType.AmericanExpress, CardNumber = "374101012180018"},
                new TestCreditCard() {CardType = CardType.DinersClub, CardNumber = "36256052780018"},
                new TestCreditCard() {CardType = CardType.MasterCard, CardNumber = "5101081245786907"},
                new TestCreditCard() {CardType = CardType.EuroCard, CardNumber = "36256052780018"},
                new TestCreditCard() {CardType = CardType.JCB, CardNumber = "3528000700000000"},
                new TestCreditCard() {CardType = CardType.Maestro, CardNumber = "6706952236150021479"},
                // check this card number
                new TestCreditCard() {CardType = CardType.VisaElectron, CardNumber = "4918344654013833"},
                new TestCreditCard() {CardType = CardType.VisaCredit, CardNumber = "4918344654013833"}
            };
        }
    }

    /// <summary>
    /// Required to remove the ProviderSpecific property which cannot be serialised.
    /// </summary>
    public class HotelBedsMapper
    {
        static HotelBedsMapper()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationProviderAvailabilityResult, TestAvailabilityResult>();
                Mapper.CreateMap<AccommodationProviderValuationResult, TestValuationResult>();
            }
        }

        public static TestAvailabilityResult CloneAvailabilityResult(AccommodationProviderAvailabilityResult availabilityResult)
        {
            TestAvailabilityResult testResult = Mapper.Map<AccommodationProviderAvailabilityResult, TestAvailabilityResult>(availabilityResult);

            StringBuilder sb = new StringBuilder();

            foreach (var entry in availabilityResult.ProviderSpecificData)
            {
                sb.AppendLine(entry.Key + ": ");
                sb.AppendLine(ReplaceSpecialChars(entry.Value));
            }

            testResult.ProviderSpecificDetails = sb.ToString();

            return testResult;
        }

        public static TestValuationResult CloneValuationResult(AccommodationProviderValuationResult valuationResult)
        {
            TestValuationResult testResult = Mapper.Map<AccommodationProviderValuationResult, TestValuationResult>(valuationResult);

            StringBuilder sb = new StringBuilder();

            foreach (var entry in valuationResult.ProviderSpecificData)
            {
                sb.AppendLine(entry.Key + ": ");
                sb.AppendLine(ReplaceSpecialChars(entry.Value));
            }

            testResult.ProviderSpecificDetails = sb.ToString();

            return testResult;
        }

        private static string XmlDecode(string value)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<root>" + value + "</root>");
            return xmlDoc.InnerText;
        }

        public static string ReplaceSpecialChars(string originalText)
        {
            string replacedText = originalText.Replace("&lt;", "<")
                                                .Replace("&amp;", "&")
                                                .Replace("&gt;", ">")
                                                .Replace("&quot;", "\"")
                                                .Replace("&apos;", "'");

            return replacedText;
        }
    }

    public class TestAvailabilityResult
    {
        public Guid Id { get; set; }
        public int RoomNumber { get; set; }
        public string ProviderEdiCode { get; set; }

        public string DestinationEdiCode { get; set; }

        public string EstablishmentEdiCode { get; set; }
        public string EstablishmentName { get; set; }

        public int Adults { get; set; }
        public int Children { get; set; }
        public int Infants { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public string BoardCode { get; set; }
        public string BoardDescription { get; set; }

        public string RoomCode { get; set; }
        public string RoomDescription { get; set; }

        public Money SalePrice { get; set; }
        public Money CostPrice { get; set; }

        public PaymentModel PaymentModel { get; set; }

        public bool IsOpaqueRate { get; set; }
        public bool IsSpecialRate { get; set; }
        public bool? IsNonRefundableRate { get; set; }
        public bool IsBindingRate { get; set; }

        public RateType RateType { get; set; }

        public int? NumberOfAvailableRooms { get; set; }

        public string ProviderSpecificDetails { get; set; }
    }

    public class TestValuationResult
    {
        public Guid Id { get; set; }
        public int RoomNumber { get; set; }

        public string ProviderName { get; set; }
        public string ProviderEdiCode { get; set; }

        public string DestinationEdiCode { get; set; }

        public string EstablishmentEdiCode { get; set; }
        public string EstablishmentName { get; set; }

        public int Adults { get; set; }
        public int Children { get; set; }
        public int Infants { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public string BoardCode { get; set; }
        public string BoardDescription { get; set; }

        public string RoomCode { get; set; }
        public string RoomDescription { get; set; }

        public Money SalePrice { get; set; }
        public Money CostPrice { get; set; }

        public decimal CommissionPercent { get; set; }
        public Money CommissionAmount { get; set; }

        public PaymentModel PaymentModel { get; set; }

        public bool IsOpaqueRate { get; set; }
        public bool IsSpecialRate { get; set; }
        public bool? IsNonRefundableRate { get; set; }
        public bool IsBindingRate { get; set; }

        public RateType RateType { get; set; }

        public string CancellationPolicy { get; set; }

        public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }

        public string ProviderSpecificDetails { get; set; }
    }

}
