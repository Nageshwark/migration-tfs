﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.HotelBeds
{
    public class Provider_HotelBeds_ValuationTests
    {
        /// <summary>
        /// Run once at the beginning of a test run (which could be composed of multiple tests).
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        private DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = RandomNumberGenerator.Between(1, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }

        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
                                                                                AccommodationProviderAvailabilityResult[] selectedRooms)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();
            valuationRequest.Provider = availabilityRequest.Provider;
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;

            valuationRequest.SelectedRooms = selectedRooms.Select((i, j) => new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                ,
                RoomNumber = (byte)(j + 1)
                ,
                Guests = availabilityRequest.Rooms.First().Guests
                ,
                AvailabilityResult = i
            }).ToArray();

            return valuationRequest;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = (byte)RandomNumberGenerator.Between(18, 80) });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityKey = Guid.NewGuid().ToString(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private AccommodationProvider CreateProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 2;
            accommodationProvider.EdiCode = "B";
            accommodationProvider.Name = "HotelBeds";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsApiVersion",	        ParameterValue = "2013/12"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsDefaultLanguage",		ParameterValue = "ENG"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsUserName",			ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsPassword",			ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsAvailabilityUrl",		ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"}, 
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsValuationUrl",		ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsBookingUrl",			ParameterValue = "https://testconfirm.interface-xml.com/platform/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsCancellationUrl",		ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsPayDirectRate",		ParameterValue = "GrossPayDirect"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetNonBindingRate",	ParameterValue = "NetStandard"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetBindingRate",		ParameterValue = "NetBinding"},
            };

            return accommodationProvider;
        }

        [Test]
        public async Task HotelBeds_Valuation_ByDestination_1Room_WillReturnValuation()
        {
            // NOTE: "BGI8" is a destination with just one hotel -> very useful for testing
            // TPA2 has only a handful of hotels
            List<string> destinations = new List<string>() { "TPA2", "BGI3", "PYX2", "CNS", "FLR", "ALC99" };
            //List<string> destinations = new List<string>() { "PYX2" };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // 1. Availability
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(300));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(16));             // Max 16 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create the Availability Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from provider
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Valuation_ByDestination_2Rooms_WillReturnValuation()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "BGI3", "PYX2", "CNS", "FLR", "ALC99", "CAN1", "MAU7", "OSL" };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms : 2 adults, no children
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Valuation_ByDestination_5Rooms_WillReturnValuation()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "BGI3", "PYX2", "CNS", "FLR", "PTY1", "PBI11", "BOU", "TFS", };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 5 rooms, two of which are identical
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 1, childAges: new byte[] { 4 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 8 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRoom = HotelBedsMapper.ReplaceSpecialChars(serialisedSelectedRoom);

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Valuation_ByEstablishment_1Room_WillReturnValuation()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100051", "100385", "101117", "108797", "122263", "122290", "1545", "163949", "199233", "3082", "31174", "47651", "5889", "58973", "6781", "6811", "68323", "69635" };
            List<string> establishments3 = new List<string>() { "71057", "81494", "89220", "9575", "99058", "13160", "155512", "131276", "114704", "212838", "232677", "47691", "47739", "5901", "68022" };
            List<string> establishments4 = new List<string>() { "144260", "152570", "163517", "172514", "19125", "209206", "236686", "390488", "71413", "227051", "47628", "5884", "5914", "68025", "68159", "68681", "69629" };
            List<string> establishments5 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };
            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };

            // Amsterdam establishments
            //List<string> establishments5 = new List<string>() { "87225", "17400", "5897", "148029", "9077", "17823", "186341", "18196", "8827", "77636", "5889", "214414", "210720", "15447", "79612", "362151", "214196", "236916", "87267", "214250", "91758", "367202", "214938", "85451", "126231", "91657", "90391", "67579", "89836" };
            //List<string> establishments5 = new List<string>() { "236916" };
            //List<List<string>> establishments = new List<List<string>>() { establishments5 };



            //// Establishment EdiCodes for "Planet Hollywood Resort And Casino" located on "The Strip", Las Vegas:
            //// Note that availability is returned ONLY for codes 5165, 56293 and 12982
            //List<string> establishments6 = new List<string>()   { 
            //                                                    "CE;2067244", "CE;2067244", "MIT;363806", "MIT;38461", "MIT;73773", "TCH;2056257", "IN019824", "HR;26151", "12982", "13709", "13710", "16136", 
            //                                                    "16401", "39340", "56293", "T24822", "LASALA", "163430", "28082", "28089", "LAS;ALA", "LAS;PLA5", "680182", "692882", "695104", "734483", "734505", 
            //                                                    "825471", "863185", "950595", "952685", "29356", "2390018", "302339", "60925", "CAL292700", "CAQ896600", "CAT406600", "MC_ALT:115967", 
            //                                                    "MC_ALT:28711", "MC_MGR:13474", "MC_MIN:38461", "MC_TRH:3265751", "MC_TRH:3750531", "MC_TRH:39913", "MC_TRH:4268836", "MC_TRH:48944", 
            //                                                    "7384", "1223655", "2303", "94726", "18250", "20685", "5165", "7081", "0000092185", "0000256931", "0000274540" 
            //                                                    };

            //List<List<string>> establishments = new List<List<string>>() { establishments6 };

            //for (int i = 0; i < 20; i++)
            //{

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));
                    RunAvailabilityAssertions(availabilityRequest, string.Empty, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            //}
        }

        [Test]
        public async Task HotelBeds_Valuation_ByEstablishment_2Rooms_WillReturnValuation()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100051", "100385", "101117", "108797", "122263", "122290", "1545", "163949", "199233", "3082", "31174", "47651", "5889", "58973", "6781", "6811", "68323", "69635" };
            List<string> establishments3 = new List<string>() { "71057", "81494", "89220", "9575", "99058", "13160", "155512", "131276", "114704", "212838", "232677", "47691", "47739", "5901", "68022" };
            List<string> establishments4 = new List<string>() { "144260", "152570", "163517", "172514", "19125", "209206", "236686", "390488", "71413", "227051", "47628", "5884", "5914", "68025", "68159", "68681", "69629" };
            List<string> establishments5 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };

            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms : 2 identical rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));
                    RunAvailabilityAssertions(availabilityRequest, string.Empty, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Valuation_ByEstablishment_5Rooms_WillReturnValuation()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100385", "5165", "56293", "12982" };
            List<string> establishments3 = new List<string>() { "12744", "2753" };          // Hotels on The Strip, Las Vegas
            List<string> establishments4 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };

            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4 };

            //List<string> establishments1 = new List<string>() { "100311" };
            //List<List<string>> establishments = new List<List<string>>() { establishments1 };


            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms : 2 identical rooms
                    // Rooms : 5 rooms, two of which are identical
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 1, childAges: new byte[] { 4 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 8 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));
                    RunAvailabilityAssertions(availabilityRequest, string.Empty, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        //var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();
                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        private void RunAvailabilityAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode, List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom = results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false, "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren = resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);
                Assert.IsTrue(hasWrongNumberOfChildren == false, "At least one room result has the wrong number of children.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false, "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<byte> validRoomIds = request.Rooms.Select(room => room.RoomNumber).ToList();
            //foreach (int roomId in validRoomIds)
            //{
            //    int count = results.Count(room => room.RoomNumber == roomId);
            //    Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            //}

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false, "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }

        /// <summary>
        /// Check that the valuation result matches the selected availability result.
        /// </summary>
        /// <param name="selectedRoom"></param>
        /// <param name="valuationResult"></param>
        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {
            TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
            string serialisedAvailability = testAvailabilityResult.XmlSerialize();
            serialisedAvailability = HotelBedsMapper.ReplaceSpecialChars(serialisedAvailability);

            TestValuationResult testValuationResult = HotelBedsMapper.CloneValuationResult(valuationResult);
            string serialisedValuation = testValuationResult.XmlSerialize();
            serialisedValuation = HotelBedsMapper.ReplaceSpecialChars(serialisedValuation);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Availability Result:");
            sb.AppendLine(serialisedAvailability);
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                if (valuationResult.CostPrice.Amount < selectedRoom.CostPrice.Amount - 2.00m ||
                    valuationResult.CostPrice.Amount > selectedRoom.CostPrice.Amount + 2.00m)
                {
                    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);
                }

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                if (valuationResult.SalePrice.Amount < selectedRoom.SalePrice.Amount - 2.00m ||
                    valuationResult.SalePrice.Amount > selectedRoom.SalePrice.Amount + 2.00m)
                {
                    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);
                }

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        /// <summary>
        /// Required to remove the ProviderSpecific property which cannot be serialised.
        /// </summary>
        private class HotelBedsMapper
        {
            static HotelBedsMapper()
            {
                lock (Automapper.MapperCreateMapLock)
                {
                    Mapper.CreateMap<AccommodationProviderAvailabilityResult, TestAvailabilityResult>();
                    Mapper.CreateMap<AccommodationProviderValuationResult, TestValuationResult>();
                }
            }

            public static TestAvailabilityResult CloneAvailabilityResult(AccommodationProviderAvailabilityResult availabilityResult)
            {
                TestAvailabilityResult testResult = Mapper.Map<AccommodationProviderAvailabilityResult, TestAvailabilityResult>(availabilityResult);

                StringBuilder sb = new StringBuilder();

                foreach (var entry in availabilityResult.ProviderSpecificData)
                {
                    sb.AppendLine(entry.Key + ": ");
                    sb.AppendLine(ReplaceSpecialChars(entry.Value));
                }

                testResult.ProviderSpecificDetails = sb.ToString();

                return testResult;
            }

            public static TestValuationResult CloneValuationResult(AccommodationProviderValuationResult valuationResult)
            {
                TestValuationResult testResult = Mapper.Map<AccommodationProviderValuationResult, TestValuationResult>(valuationResult);

                StringBuilder sb = new StringBuilder();

                foreach (var entry in valuationResult.ProviderSpecificData)
                {
                    sb.AppendLine(entry.Key + ": ");
                    sb.AppendLine(ReplaceSpecialChars(entry.Value));
                }

                testResult.ProviderSpecificDetails = sb.ToString();

                return testResult;
            }

            private static string XmlDecode(string value)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml("<root>" + value + "</root>");
                return xmlDoc.InnerText;
            }

            public static string ReplaceSpecialChars(string originalText)
            {
                string replacedText = originalText.Replace("&lt;", "<")
                                                    .Replace("&amp;", "&")
                                                    .Replace("&gt;", ">")
                                                    .Replace("&quot;", "\"")
                                                    .Replace("&apos;", "'");

                return replacedText;
            }
        }

        public class TestAvailabilityResult
        {
            public Guid Id { get; set; }
            public int RoomNumber { get; set; }
            public string ProviderEdiCode { get; set; }

            public string DestinationEdiCode { get; set; }

            public string EstablishmentEdiCode { get; set; }
            public string EstablishmentName { get; set; }

            public int Adults { get; set; }
            public int Children { get; set; }
            public int Infants { get; set; }

            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }

            public string BoardCode { get; set; }
            public string BoardDescription { get; set; }

            public string RoomCode { get; set; }
            public string RoomDescription { get; set; }

            public Money SalePrice { get; set; }
            public Money CostPrice { get; set; }

            public PaymentModel PaymentModel { get; set; }

            public bool IsOpaqueRate { get; set; }
            public bool IsSpecialRate { get; set; }
            public bool? IsNonRefundableRate { get; set; }
            public bool IsBindingRate { get; set; }

            public RateType RateType { get; set; }

            public int? NumberOfAvailableRooms { get; set; }

            public string ProviderSpecificDetails { get; set; }
        }

        public class TestValuationResult
        {
            public Guid Id { get; set; }
            public int RoomNumber { get; set; }

            public string ProviderName { get; set; }
            public string ProviderEdiCode { get; set; }

            public string DestinationEdiCode { get; set; }

            public string EstablishmentEdiCode { get; set; }
            public string EstablishmentName { get; set; }

            public int Adults { get; set; }
            public int Children { get; set; }
            public int Infants { get; set; }

            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }

            public string BoardCode { get; set; }
            public string BoardDescription { get; set; }

            public string RoomCode { get; set; }
            public string RoomDescription { get; set; }

            public Money SalePrice { get; set; }
            public Money CostPrice { get; set; }

            public decimal CommissionPercent { get; set; }
            public Money CommissionAmount { get; set; }

            public PaymentModel PaymentModel { get; set; }

            public bool IsOpaqueRate { get; set; }
            public bool IsSpecialRate { get; set; }
            public bool? IsNonRefundableRate { get; set; }
            public bool IsBindingRate { get; set; }

            public RateType RateType { get; set; }

            public string CancellationPolicy { get; set; }

            public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }

            public string ProviderSpecificDetails { get; set; }
        }
    }
}

