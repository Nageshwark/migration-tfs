﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Configuration;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.HotelBeds
{
    public class Provider_HotelBeds_AvailabilityTests
    {
        private IChannelInfoService channelInfoService;
        private AccommodationConfigurationManager configurationManager;
        //private IDbConnection mongoDbHelper;

        //private IDbMultiDatabaseController mongoMultiDatabaseController;
        //private static IMongoClient mongoClient;
        //private static IMongoDatabase mongoDatabase;

        //private IDbMultiDatabaseController mongoMultiDatabaseController;
        //private MongoServer mongoServer;

        //private ISupplierService supplierService;
        //private IDestinationSupplierRepository destinationSupplierRepository;

        /// <summary>
        /// Run once at the beginning of a test run (which could be composed of multiple tests).
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            // IMPORTANT: Configure Mongo to use a custom serialiser for DateTime data types (prevents dates being stored as UTC in mongo).
            // Execute this code just once, regardless of the number of tests being run.
            // Executing this code more than once per session will generate an exception.
            //LocalTimeMongoSerializerManager.RegisterLocalTimeMongoSerializer();

            NinjectInstaller.Start();

            //// Remove all bindings for the classes under test
            //NinjectInstaller.Kernel.Unbind<IAccommodationAvailabilityProvider>();
            //NinjectInstaller.Kernel.Unbind<HotelBedsAvailabilityProvider>();

            //// Add bindings for the classes under test
            //NinjectInstaller.Kernel.Bind<HotelBedsAvailabilityProvider>().ToSelf();

            // Create the mongo db helper class that is used to clear the mongo collections on the local db
            //mongoDbHelper = NinjectInstaller.Kernel.Get<IDbConnection>();

            //configurationManager = (AccommodationConfigurationManager)NinjectInstaller.Kernel.Get<IConfigurationManager>();

            //mongoMultiDatabaseController = new DbMultiDatabaseController(configurationManager);
            //mongoClient = new MongoClient(configurationManager.ConnectionString);
            //mongoDatabase = mongoClient.GetDatabase(configurationManager.ResultsDatabaseName);

            //mongoMultiDatabaseController = new DbMultiDatabaseController(configurationManager);
            //mongoServer = new MongoClient(configurationManager.ConnectionString).GetServer();

            //supplierService = NinjectInstaller.Kernel.Get<ISupplierService>();

            //destinationSupplierRepository = NinjectInstaller.Kernel.Get<IDestinationSupplierRepository>();
            //searchKeyGenerator = NinjectInstaller.Kernel.Get<IFlightSearchKeyGenerator>();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        private DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = RandomNumberGenerator.Between(1, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }


        //private async Task ClearMongoRequestStatus()
        //{
        //    //IMongoCollection<AccommodationRequestStatus> mongoCollection = mongoDatabase.GetCollection<AccommodationRequestStatus>(this.configurationManager.RequestStatusCollection);
        //    //var filter = new BsonDocument();        // Create an empty filter document to return ALL documents in the collection
        //    //await mongoCollection.DeleteManyAsync(filter);

        //    MongoCollection<AccommodationRequestStatus> mongoCollection = mongoDbHelper.GetMongoCollection<AccommodationRequestStatus>(this.configurationManager.RequestStatusCollection);
        //    mongoCollection.RemoveAll();
        //}

        //private async Task ClearMongoDbAvailability()
        //{
        //    List<string> accommodationResultDatabases = new List<string>();

        //    if (configurationManager.MultiDatabasesEnabled)
        //    {
        //        accommodationResultDatabases = new List<string>() { "accommodationresults",    "accommodationresults_01", "accommodationresults_02", "accommodationresults_03", 
        //                                                            "accommodationresults_04", "accommodationresults_05", "accommodationresults_06", "accommodationresults_07", 
        //                                                            "accommodationresults_08", "accommodationresults_09", "accommodationresults_10" };
        //    }
        //    else
        //    {
        //        accommodationResultDatabases = new List<string>() { "accommodationresults" };
        //    }

        //    // Loop through all the databases that could be holding flight search results, and remove all the objects in each db
        //    foreach (var db in accommodationResultDatabases)
        //    {
        //        //IMongoDatabase mongoDb = mongoClient.GetDatabase(configurationManager.ResultsDatabaseName);
        //        //IMongoCollection<AccommodationProviderAvailabilityResult> mongoCollection = mongoDb.GetCollection<AccommodationProviderAvailabilityResult>(configurationManager.AccommodationSearchResultsCollection);
        //        //await mongoCollection.DeleteManyAsync(new BsonDocument());

        //        MongoDatabase mongoDb = mongoServer.GetDatabase(db);
        //        MongoCollection<AccommodationProviderAvailabilityResult> mongoCollection = mongoDb.GetCollection<AccommodationProviderAvailabilityResult>(this.configurationManager.AccommodationSearchResultsCollection);


        //        // Check that the collection is empty before proceeding
        //        int timeout = 30000;	// 30 seconds
        //        bool isFinished = false;
        //        DateTime timeoutTime = DateTime.Now.AddMilliseconds(timeout);

        //        while (!isFinished && DateTime.Now < timeoutTime)
        //        {
        //            //if (await mongoCollection.CountAsync(new BsonDocument()) == 0)
        //            if (mongoCollection.Count() == 0)
        //            {
        //                isFinished = true;
        //            }
        //            else
        //            {
        //                Thread.Sleep(500); // sleep the current thread for 0.5 seconds
        //            }
        //        }

        //        // Throw an exception if the search was unsuccessful nor timed out -> no point continuing the test.
        //        if (!isFinished)
        //        {
        //            throw new Exception("Failure in deleting AccommodateSearchResults from the Mongo db collection.");
        //        }
        //    }
        //}

        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    IEnumerable<AccommodationProviderAvailabilityRequestRoom> rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms.ToArray();

            return request;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = (byte)RandomNumberGenerator.Between(18, 80) });
            }

            foreach (var age in childAges)
            {
                if (age >= 2)
                {
                    guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Child, Age = age });
                }
                else if (age < 2)
                {
                    guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Infant, Age = age });
                }
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private AccommodationProvider CreateProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 2;
            accommodationProvider.EdiCode = "B";
            accommodationProvider.Name = "HotelBeds";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsApiVersion",	        ParameterValue = "2013/12"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsDefaultLanguage",		ParameterValue = "ENG"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsUserName",            ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsPassword",            ParameterValue = "ALPHAXML2UK145212"},

                //new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsAvailabilityUrl",     ParameterValue = "http://api.interface-xml.com/appservices/http/FrontendService"},
                //new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsValuationUrl",        ParameterValue = "http://api.interface-xml.com/appservices/http/FrontendService"},

                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsAvailabilityUrl",     ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsValuationUrl",        ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},

                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPayDirect",     ParameterValue = "https://testconfirm.interface-xml.com/platform/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPostPayment",   ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsCancellationUrl",         ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},

                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsPayDirectRate",		ParameterValue = "GrossPayDirect"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetNonBindingRate",	ParameterValue = "NetStandard"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetBindingRate",		ParameterValue = "NetBinding"},
            };

            return accommodationProvider;
        }

        [Test]
        public async Task HotelBeds_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel - great for tracing in code!!
            List<string> destinations = new List<string>() { "TPA2", "BGI3", "PYX2", "CNS", "FLR", "ALC99" };
            //List<string> destinations = new List<string>() { "BGI8" };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error -> do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Availability_ByDestination_2Rooms_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel - great for testing
            List<string> destinations = new List<string>() { "BGI8", "BGI3", "PYX2", "CNS", "FLR", "ALC99", "CAN1", "MAU7", "OSL" };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Availability_ByDestination_5Rooms_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { /* "BGI3", */ "PYX2", "CNS", "FLR", "PTY1", "PBI11", "BOU", "TFS", };

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 5 rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { 4, 6 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 11 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms);

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Availability_ByEstablishment_1Room_WillReturnAvailability()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100051", "100385", "101117", "108797", "122263", "122290", "1545", "163949", "199233", "3082", "31174", "47651", "5889", "58973", "6781", "6811", "68323", "69635" };
            List<string> establishments3 = new List<string>() { "71057", "81494", "89220", "9575", "99058", "13160", "155512", "131276", "114704", "212838", "232677", "47691", "47739", "5901", "68022" };
            List<string> establishments4 = new List<string>() { "144260", "152570", "163517", "172514", "19125", "209206", "236686", "390488", "71413", "227051", "47628", "5884", "5914", "68025", "68159", "68681", "69629" };
            List<string> establishments5 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };

            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };

            //// Planet Hollywood Resort And Casino located in The Strip, Las Vegas. 
            //// Note that availability is returned ONLY for codes 5165, 56293 and 12982
            //List<string> establishments6 = new List<string>()   { 
            //                                                    "CE;2067244", "CE;2067244", "MIT;363806", "MIT;38461", "MIT;73773", "TCH;2056257", "IN019824", "HR;26151", "12982", "13709", "13710", "16136", 
            //                                                    "16401", "39340", "56293", "T24822", "LASALA", "163430", "28082", "28089", "LAS;ALA", "LAS;PLA5", "680182", "692882", "695104", "734483", "734505", 
            //                                                    "825471", "863185", "950595", "952685", "29356", "2390018", "302339", "60925", "CAL292700", "CAQ896600", "CAT406600", "MC_ALT:115967", 
            //                                                    "MC_ALT:28711", "MC_MGR:13474", "MC_MIN:38461", "MC_TRH:3265751", "MC_TRH:3750531", "MC_TRH:39913", "MC_TRH:4268836", "MC_TRH:48944", 
            //                                                    "7384", "1223655", "2303", "94726", "18250", "20685", "5165", "7081", "0000092185", "0000256931", "0000274540" 
            //                                                    };

            //List<List<string>> establishments = new List<List<string>>() { establishments6 };

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    RunAssertions(request, string.Empty, results);

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Availability_ByEstablishment_2Rooms_WillReturnAvailability()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100051", "100385", "101117", "108797", "122263", "122290", "1545", "163949", "199233", "3082", "31174", "47651", "5889", "58973", "6781", "6811", "68323", "69635" };
            List<string> establishments3 = new List<string>() { "71057", "81494", "89220", "9575", "99058", "13160", "155512", "131276", "114704", "212838", "232677", "47691", "47739", "5901", "68022" };
            List<string> establishments4 = new List<string>() { "144260", "152570", "163517", "172514", "19125", "209206", "236686", "390488", "71413", "227051", "47628", "5884", "5914", "68025", "68159", "68681", "69629" };
            List<string> establishments5 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };

            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms : 2 identical rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    RunAssertions(request, string.Empty, results);

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        [Test]
        public async Task HotelBeds_Availability_ByEstablishment_5Rooms_WillReturnAvailability()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100385", "5165", "56293", "12982" };
            List<string> establishments3 = new List<string>() { "12744", "2753" };          // Hotels on The Strip, Las Vegas
            List<string> establishments4 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };

            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4 };

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms : 5 rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { 4, 6 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 9 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    RunAssertions(request, string.Empty, results);

                    await Task.Delay(2000);
                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        private void RunAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode, List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom = results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false, "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren = resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);
                Assert.IsTrue(hasWrongNumberOfChildren == false, "At least one room result has the wrong number of children.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false, "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<byte> validRoomIds = request.Rooms.Select(room => room.RoomNumber).ToList();
            //foreach (int roomId in validRoomIds)
            //{
            //    int count = results.Count(room => room.RoomNumber == roomId);
            //    Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            //}

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false, "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }

    }
}
