﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Configuration;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.HotelBeds
{
    public class Provider_HotelBeds_BookingTests
    {
        /// <summary>
        /// Run once at the beginning of a test run (which could be composed of multiple tests).
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        private DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = RandomNumberGenerator.Between(1, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }

        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(   AccommodationProviderAvailabilityRequest availabilityRequest,
                                                                                AccommodationProviderAvailabilityResult[] selectedRooms)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();

            valuationRequest.Provider = availabilityRequest.Provider;
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;
            valuationRequest.ChannelInfo = new ChannelInfo() { Channel = availabilityRequest.ChannelInfo.Channel };
            valuationRequest.AvailabilityId = availabilityRequest.AvailabilityId;

            List<AccommodationProviderValuationRequestRoom> roomsToBeValued = new List<AccommodationProviderValuationRequestRoom>();

            foreach (var selectedRoom in selectedRooms)
            {
                AccommodationProviderValuationRequestRoom roomToBeValued = new AccommodationProviderValuationRequestRoom();

                roomToBeValued.AvailabilityRequest = availabilityRequest;
                roomToBeValued.AvailabilityResult = selectedRoom;
                roomToBeValued.RoomNumber = selectedRoom.RoomNumber;

                AccommodationProviderAvailabilityRequestRoom requestRoom = availabilityRequest.Rooms.First(room => room.RoomNumber == selectedRoom.RoomNumber);
                roomToBeValued.Guests = new AccommodationProviderAvailabilityRequestGuestCollection(requestRoom.Guests.ToList());

                roomsToBeValued.Add(roomToBeValued);
            }

            valuationRequest.SelectedRooms = roomsToBeValued.ToArray();

            return valuationRequest;
        }



        private AccommodationProviderBookingRequest CreateBookingRequest(   AccommodationProvider provider,
                                                                            Channel channel,
                                                                            List<AccommodationProviderBookingRequestRoom> bookingRequestRooms,
                                                                            AccommodationProviderValuationRequest valuationRequest)
        {
            AccommodationProviderBookingRequest bookingRequest = new AccommodationProviderBookingRequest();

            bookingRequest.BookingId = Guid.NewGuid();
            bookingRequest.ValuationId = valuationRequest.ValuationId;
            bookingRequest.AvailabilityId = valuationRequest.AvailabilityId;

            bookingRequest.Provider = provider;
            bookingRequest.ChannelInfo = new ChannelInfo() { Channel = channel };
            bookingRequest.ValuatedRooms = bookingRequestRooms.ToArray();

            bookingRequest.Debugging = true;
            bookingRequest.Customer = CreateCustomer();

            bookingRequest.ItineraryId = RandomNumberGenerator.Between(0, 999999999);

            return bookingRequest;
        }

        private List<AccommodationProviderBookingRequestRoomGuest> CreateGuests(AccommodationProviderValuationRequestRoom roomOccupancy)
        {
            List<AccommodationProviderBookingRequestRoomGuest> guests = new List<AccommodationProviderBookingRequestRoomGuest>();
            int i = 1;

            // NOTE: only supports a single room at present
            foreach (var roomguest in roomOccupancy.Guests)
            {
                AccommodationProviderBookingRequestRoomGuest guest = new AccommodationProviderBookingRequestRoomGuest();

                // Adult/Child
                if (roomguest.Type == GuestType.Adult)
                {
                    guest.Type = GuestType.Adult;
                }
                else
                {
                    guest.Type = GuestType.Child;
                }

                Gender gender = (Gender)RandomNumberGenerator.Between(1, 2);

                // Set the Title
                if (gender == Gender.Male && guest.Type == GuestType.Adult)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleAdultTitles.Count - 1)]);
                }
                else if (gender == Gender.Female && guest.Type == GuestType.Adult)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleAdultTitles.Count - 1)]);
                }
                else if (gender == Gender.Male && guest.Type == GuestType.Child)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleChildTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleChildTitles.Count - 1)]);
                }
                else if (gender == Gender.Female && guest.Type == GuestType.Child)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleChildTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleChildTitles.Count - 1)]);
                }

                // Set the first name
                if (gender == Gender.Male)
                {
                    guest.FirstName = HotelBedsSupportClasses.MaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleFirstNames.Count - 1)];
                }
                else
                {
                    guest.FirstName = HotelBedsSupportClasses.FemaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleFirstNames.Count - 1)];
                }

                guest.Surname = HotelBedsSupportClasses.LastNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.LastNames.Count - 1)];
                guest.Age = (byte)Int16.Parse(roomguest.Age.ToString());

                guest.Id = i;
                i++;

                guests.Add(guest);
            }

            // Set the lead guest
            var adultguest = guests.First(pax => pax.Type == GuestType.Adult);
            return guests;
        }

        private AccommodationProviderBookingRequestCustomer CreateCustomer()
        {
            AccommodationProviderBookingRequestCustomer customer = new AccommodationProviderBookingRequestCustomer();

            Gender gender = (Gender)RandomNumberGenerator.Between(1, 2);

            // Set the Title
            if (gender == Gender.Male)
            {
                customer.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleAdultTitles.Count - 1)]);
            }
            else if (gender == Gender.Female)
            {
                customer.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleAdultTitles.Count - 1)]);
            }
            
            // Set the first name
            if (gender == Gender.Male)
            {
                customer.FirstName = HotelBedsSupportClasses.MaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleFirstNames.Count - 1)];
            }
            else
            {
                customer.FirstName = HotelBedsSupportClasses.FemaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleFirstNames.Count - 1)];
            }

            // Non gender specific details
            customer.TitleString = customer.Title.ToString();
            customer.Surname = HotelBedsSupportClasses.LastNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.LastNames.Count - 1)];
            customer.EmailAddress = HotelBedsSupportClasses.EmailAddresses[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.EmailAddresses.Count - 1)];
            customer.ContactNumber = HotelBedsSupportClasses.HomePhones[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.HomePhones.Count - 1)];
            customer.Age = (byte)RandomNumberGenerator.Between(25, 80);
            customer.Type = GuestType.Adult;

            return customer;
        }

        private AccommodationProviderBookingRequestRoomPaymentDetails CreateCardDetails(string cardHolderName, AccommodationProviderValuationResult valuationResult)
        {
            AccommodationProviderBookingRequestRoomPaymentDetails card = new AccommodationProviderBookingRequestRoomPaymentDetails();

            card.CardHolderName = cardHolderName;
            card.AddressLine1 = HotelBedsSupportClasses.AddressLine1[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.AddressLine1.Count - 1)];
            card.AddressLine2 = HotelBedsSupportClasses.AddressLine2[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.AddressLine2.Count - 1)];
            card.City = HotelBedsSupportClasses.Cities[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Cities.Count - 1)];
            //card.County = HotelBedsSupportClasses.Counties[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Counties.Count - 1)];
            card.Postcode = HotelBedsSupportClasses.Postcodes[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Postcodes.Count - 1)];
            card.Country = "United Kingdom";
            //card.CountryCode = "UK";

            // Get a matching credit card for the card type and the fare currency
            try
            {
                HotelBedsSupportClasses.TestCreditCard selectedCard = null;
                List<AccommodationProviderPaymentOption> acceptedCreditCards = valuationResult.ProviderPaymentOptions;

                // Get a list of all the test cards which match the cards accepted by the hotel
                var matchingTestCards = (from testCard in HotelBedsSupportClasses.TestCreditCards
                                         join acceptedCard in acceptedCreditCards
                                         on testCard.CardType equals acceptedCard.CardType
                                         select testCard).ToList();

                // Select a test card at random
                if (matchingTestCards != null && matchingTestCards.Count > 0)
                {
                    selectedCard = matchingTestCards[RandomNumberGenerator.Between(0, matchingTestCards.Count() - 1)];
                    card.CardNumber = selectedCard.CardNumber;
                    card.CardType = selectedCard.CardType;
                }
                else
                {
                    throw new NullReferenceException("Unable to Locate a matching test credit card to use with the Valuation Result.");
                }

                card.CardExpireDate = new MonthYear(6,19);
                card.CardSecurityCode = "123";
            }
            catch (Exception ex)
            {
                throw new Exception("Credit Card Problem.", ex);
            }

            return card;
        }

        private AccommodationProviderBookingRequestRoomSpecialRequest CreateSpecialBookingRequest()
        {
            var specialRequest = HotelBedsSupportClasses.SpecialBookingRequests[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.SpecialBookingRequests.Count - 1)];
            return specialRequest;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = (byte)RandomNumberGenerator.Between(18, 80) });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        /// <summary>
        /// Note that if a boolean parameter has a value of false, then that criteria is simply excluded from the search. This means that the search could actually return the value that was included in the List<>
        /// parameter that goes with boolean flag. This is done because exclusing data makes it more likely that no matching record will be found, which is not desirable and will fail the test.
        /// </summary>
        /// <param name="availabilityRequestRoom"></param>
        /// <param name="results"></param>
        /// <param name="matchContract"></param>
        /// <param name="contracts"></param>
        /// <param name="matchEstablishment"></param>
        /// <param name="establishmentEdiCodes"></param>
        /// <param name="matchRoomCode"></param>
        /// <param name="roomCodes"></param>
        /// <returns></returns>
        private AccommodationProviderAvailabilityResult GetAvailabilityRoom(AccommodationProviderAvailabilityRequestRoom availabilityRequestRoom,
                                                                            List<AccommodationProviderAvailabilityResult> results,
                                                                            bool matchContract,
                                                                            List<string> contracts,
                                                                            bool matchEstablishment,
                                                                            List<string> establishmentEdiCodes,
                                                                            bool matchRoomCode,
                                                                            List<string> roomCodes)
        {
            AccommodationProviderAvailabilityResult selectedRoom = null;
            List<AccommodationProviderAvailabilityResult> matchingResults = null;

            int noAdults = availabilityRequestRoom.Guests.AdultsCount;
            int noChildren = availabilityRequestRoom.Guests.ChildrenCount + availabilityRequestRoom.Guests.InfantsCount;
            int roomNumber = availabilityRequestRoom.RoomNumber;

            if (!contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber).ToList();
                    }
                }
            }
            else if (matchContract && contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract)).ToList();
                    }
                }
            }
            else if (!matchContract && contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {

                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract)).ToList();
                    }
                }
            }

            if (matchingResults.Count > 0)
            {
                int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                selectedRoom = matchingResults[roomIndex];
            }

            return selectedRoom;
        }

        private List<AccommodationProviderBookingRequestRoom> CreateBookingRequestRooms(AccommodationProviderValuationRequest valuationRequest, List<AccommodationProviderValuationResult> valuationResults)
        {
            List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = new List<AccommodationProviderBookingRequestRoom>();

            AccommodationProviderBookingRequestRoomPaymentDetails cardDetails = null;

            foreach (var valuationResult in valuationResults)
            {
                AccommodationProviderBookingRequestRoom bookingRequestRoom = new AccommodationProviderBookingRequestRoom();

                AccommodationProviderValuationRequestRoom valuationRequestRoom = valuationRequest.SelectedRooms.Where(room => room.RoomNumber == valuationResult.RoomNumber).First();
                Assert.IsNotNull(valuationRequestRoom, string.Format("Room {0}: Unable to identify the AccommodationProviderValuationRequestRoom for this room number - test cannot continue.", valuationResult.RoomNumber));

                bookingRequestRoom.ValuationResult = valuationResult;
                bookingRequestRoom.AvailabilityRequest = valuationRequestRoom.AvailabilityRequest;
                bookingRequestRoom.SpecialRequests = CreateSpecialBookingRequest();
                bookingRequestRoom.RoomNumber = valuationResult.RoomNumber;

                // Create the guests
                List<AccommodationProviderBookingRequestRoomGuest> guests = CreateGuests(valuationRequestRoom);
                bookingRequestRoom.Guests = new AccommodationProviderBookingRequestRoomGuestCollection(guests);

                // Only get card payment details if the room is a customer pay direct room, and no card details have so far been registered.
                if (valuationResult.PaymentModel == PaymentModel.CustomerPayDirect &&
                    cardDetails == null)
                {
                    var guest = guests.Where(gst => gst.Type == GuestType.Adult).OrderByDescending(gst => gst.Age).First();
                    string name = guest.FirstName + " " + guest.Surname;
                    cardDetails = CreateCardDetails(name, valuationResult);
                }

                if (valuationResult.PaymentModel == PaymentModel.CustomerPayDirect)
                {
                    bookingRequestRoom.PaymentDetails = cardDetails;
                }

                bookingRequestRooms.Add(bookingRequestRoom);
            }

            return bookingRequestRooms;
        }

        private AccommodationProvider CreateProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 2;
            accommodationProvider.EdiCode = "B";
            accommodationProvider.Name = "HotelBeds";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsApiVersion",	        ParameterValue = "2013/12"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsDefaultLanguage",		ParameterValue = "ENG"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsUserName",            ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsPassword",            ParameterValue = "ALPHAXML2UK145212"},
                //new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsUserName",            ParameterValue = "CARSOLIZETEST"},        // Temporary credentials
                //new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsPassword",            ParameterValue = "CARSOLIZETEST"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsAvailabilityUrl",         ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsValuationUrl",            ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPayDirect",     ParameterValue = "https://testconfirm.interface-xml.com/platform/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPostPayment",   ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsCancellationUrl",         ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsPayDirectRate",		ParameterValue = "GrossPayDirect"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetNonBindingRate",	ParameterValue = "NetStandard"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsNetBindingRate",		ParameterValue = "NetBinding"},
            };

            return accommodationProvider;
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_1Room_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel -> very useful for testing
            // TPA2 has only a handful of hotels
            List<string> destinations = new List<string>() { "TPA2", "PYX2", "CNS", "FLR", "ALC99", "BEN" };
            //List<string> destinations = new List<string>() { "BGI8" };
            //List<string> destinations = new List<string>() { "BCN" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // 1. AVAILABILITY
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(300)); // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(16));       // Max 16 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] {destinationEdiCode};
                    string[] establishmentEdiCodes = new string[] {};

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] {});
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create the Availability Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from provider
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. VALUATION
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelHeavy Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_2Rooms_PayDirect_SameContract_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "PYX2", "CNS", "LVS8", "FAO", "BAI" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms : 2 adults, no children
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation

                    // Select Pay Direct rooms only
                    results = results.Where(room => room.PaymentModel == PaymentModel.CustomerPayDirect).ToList();

                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);

                    // For Room 2: Select a matching availability result - same contract and same hotel
                    AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                    matchContract: true, contracts: new List<string>() { firstRoomResult.Contract },
                                                                                                    matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(secondRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("Error getting object from pool"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelBeds Cache Retrieval Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_2Rooms_PayDirect_DifferentContracts_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "HKT", "LPA", "CFU", "FAO", "BAI" };
            //List<string> destinations = new List<string>() { "PYX2", "LPA", "ATE", "FAO" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms : 2 adults, no children
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation

                    // Select Pay Direct rooms only
                    results = results.Where(room => room.PaymentModel == PaymentModel.CustomerPayDirect).ToList();

                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);

                    results = results.Where(result => !result.Contract.Equals(firstRoomResult.Contract, StringComparison.InvariantCultureIgnoreCase) && !result.EstablishmentEdiCode.Equals(firstRoomResult.EstablishmentEdiCode, StringComparison.InvariantCultureIgnoreCase)).ToList();

                    // For Room 2: Select a matching availability result - same contract and same hotel
                    AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                    matchContract: false, contracts: new List<string>() { firstRoomResult.Contract },
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(secondRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("Error getting object from pool"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. 'Error getting object from pool' Error. Cache Retrieval error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_2Rooms_PostPayment_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "LPA", "FAO", "FLR", "ALC99", "CAN1", "MAU7", "OSL" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms : 2 adults, no children
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation

                    // Select Post Payment rooms only
                    results = results.Where(room => room.PaymentModel == PaymentModel.PostPayment).ToList();

                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);

                    // For Room 2: Select a matching availability result - 
                    AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(secondRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelHeavy Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_2Rooms_PayDirectANDPostPayment_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "LON", "CFU", "LVS8", "FAO", "HKT" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 2 identical rooms : 2 adults, no children
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { });
                    rooms.Add(room1);
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation

                    // Select Post Payment rooms only
                    List<AccommodationProviderAvailabilityResult>  resultsPP = results.Where(room => room.PaymentModel == PaymentModel.PostPayment).ToList();

                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random - Post Payment
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: resultsPP,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify a Post Payment availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);

                    // For Room 2: Select a matching availability result - Pay Direct
                    List<AccommodationProviderAvailabilityResult> resultsPD = results.Where(room => room.PaymentModel == PaymentModel.CustomerPayDirect).ToList();

                    AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: resultsPD,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify a Pay Direct availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(secondRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelHeavy Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByDestination_5Rooms_WillBook()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            //List<string> destinations = new List<string>() { "BGI3", "PYX2", "CNS", "FLR", "PTY1", "PBI11", "BOU", "TFS", };
            List<string> destinations = new List<string>() { "LVS8", "BEN" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 5 rooms, two of which are identical
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { 4, 6 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 8 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));
                    RunAvailabilityAssertions(availabilityRequest, destinationEdiCode, results);


                    // 2. Valuation
                    // Select Rooms from Availability Results
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // Temporary condition to ensure at least 2 direct pay rooms
                    int counter = 0;

                    do
                    {
                        selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                        // Select 5 rooms from three different contracts, but all in the same hotel.
                        // Room 1: Get any result at random
                        AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(firstRoomResult);

                        // Room 2: 
                        AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(secondRoomResult);

                        // Room 3: 
                        AccommodationProviderAvailabilityResult thirdRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[2], results: results,
                                                                                                        matchContract: false, contracts: new List<string>() { },
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: false, roomCodes: new List<string>() { firstRoomResult.RoomCode });
                        Assert.IsNotNull(thirdRoomResult, string.Format("Room 3: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(thirdRoomResult);

                        // Room 4: 
                        AccommodationProviderAvailabilityResult fourthRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[3], results: results,
                                                                                                        matchContract: true, contracts: new List<string>() { secondRoomResult.Contract },
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: true, roomCodes: new List<string>() { secondRoomResult.RoomCode });
                        Assert.IsNotNull(fourthRoomResult, string.Format("Room 4: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(fourthRoomResult);

                        // Room 5: 
                        AccommodationProviderAvailabilityResult fifthRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[4], results: results,
                                                                                                        matchContract: false, contracts: new List<string>() { firstRoomResult.Contract, secondRoomResult.Contract },
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(fifthRoomResult, string.Format("Room 5: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(fifthRoomResult);

                        counter++;

                    } while (selectedRooms.Count(room => room.PaymentModel == PaymentModel.CustomerPayDirect) < 2 && counter < 10);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelHeavy Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Booking_ByEstablishment_1Room_WillBook()
        {
            // Note that some of these establishments will not return availability!!
            List<string> establishments1 = new List<string>() { "100311", "118314", "13383", "160874", "193738", "216983", "252275", "381376", "7870", "232683" };
            List<string> establishments2 = new List<string>() { "100051", "100385", "101117", "108797", "122263", "122290", "1545", "163949", "199233", "3082", "31174", "47651", "5889", "58973", "6781", "6811", "68323", "69635" };
            List<string> establishments3 = new List<string>() { "71057", "81494", "89220", "9575", "99058", "13160", "155512", "131276", "114704", "212838", "232677", "47691", "47739", "5901", "68022" };
            List<string> establishments4 = new List<string>() { "144260", "152570", "163517", "172514", "19125", "209206", "236686", "390488", "71413", "227051", "47628", "5884", "5914", "68025", "68159", "68681", "69629" };
            List<string> establishments5 = new List<string>() { "144281", "152600", "163532", "172536", "191293", "209210", "236801", "390922", "7151", "232083", "232674" };
            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };

            //List<string> establishments5 = new List<string>() { "73839" };      // Gives a price mismatch with non refundable rates
            //List<string> establishments5 = new List<string>() { "" };
            //List<List<string>> establishments = new List<List<string>>() { establishments5 };


            //// Establishment EdiCodes for "Planet Hollywood Resort And Casino" located on "The Strip", Las Vegas:
            //// Note that availability is returned ONLY for codes 5165, 56293 and 12982
            //List<string> establishments6 = new List<string>()   { 
            //                                                    "CE;2067244", "CE;2067244", "MIT;363806", "MIT;38461", "MIT;73773", "TCH;2056257", "IN019824", "HR;26151", "12982", "13709", "13710", "16136", 
            //                                                    "16401", "39340", "56293", "T24822", "LASALA", "163430", "28082", "28089", "LAS;ALA", "LAS;PLA5", "680182", "692882", "695104", "734483", "734505", 
            //                                                    "825471", "863185", "950595", "952685", "29356", "2390018", "302339", "60925", "CAL292700", "CAQ896600", "CAT406600", "MC_ALT:115967", 
            //                                                    "MC_ALT:28711", "MC_MGR:13474", "MC_MIN:38461", "MC_TRH:3265751", "MC_TRH:3750531", "MC_TRH:39913", "MC_TRH:4268836", "MC_TRH:48944", 
            //                                                    "7384", "1223655", "2303", "94726", "18250", "20685", "5165", "7081", "0000092185", "0000256931", "0000274540" 
            //                                                    };

            //List<List<string>> establishments = new List<List<string>>() { establishments6 };

            //for (int i = 0; i < 20; i++)
            //{

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (List<string> establishmentList in establishments)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));         // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = establishmentList.ToArray();

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));
                    RunAvailabilityAssertions(availabilityRequest, string.Empty, results);


                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // For Room 1: Select a matching availability result at random - Post Payment
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for establishments {0} - test cannot continue.", establishmentList.ToString()));
                    selectedRooms.Add(firstRoomResult);

                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("HotelHeavy"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Establishments: {0}. HotelHeavy Error.", establishmentList.ToString(';')));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Establishments: {0}. No availability results returned.", establishmentList.ToString(';')));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Establishments: {0}. (500) Internal Server Error.", establishmentList.ToString(';')));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", establishmentList.ToString(';')));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        /// <summary>
        /// Note that trying to value rooms from establishments that belong to different invoicing offices (i.e. different countries) within the same series of chained requests is not possible and will
        /// generate an ERR_730|BD error: !REC-05529. Different companies are mixed in the same booking locator!
        /// 
        /// Email from HotelBeds:
        /// For invoicing reasons we cannot allow the creation of a booking with hotels belonging to two (or more) different invoicing companies. 
        /// In other words you cannot make a booking in Spain and another one in Portugal because the two countries are managed differently internally and the invoicing companies differ. 
        /// A better explanation would be mixing different invoice currencies(GBP in UK and EUR in France, just across the Eurotunnel). 
        /// In order for you to identify the invoicing company you could look at the room or board type.For instance, a Double room(DBL) gets concatenated with 
        /// an invoicing company code(E10 for most of Europe, P01 for Portugal, T01 for Turkey, U02 for UK, to give you a few examples).
        /// 
        /// However, this error should NOT occur in any of these tests as all the establishments in each list come from a single parent destination (and therefore country).
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task HotelBeds_Booking_ByEstablishment_2Rooms_WillBook()
        {
            // Note that some of these establishments may not return availability!!

            // All hotels in establishments2 from Parent destination: Goa, Child destination: Calangute
            List<string> establishments1 = new List<string>() { "126638", "134939", "140710", "131538", "196961", "94707", "146465", "189815", "101078", "189814", "164017", "367617", "98855", "95060" };

            // All hotels in establishments2 from Parent destination: Koh Samui, Child destination: Chaweng
            List<string> establishments2 = new List<string>() { "73261", "261686", "108093", "80121", "78907", "93302", "80125", "80657", "25316", "34339", "60709", "78853", "34338", "34344", "235506", "25359", "52708", "163266", "253715", "357503", "148740", "34343" };

            // All hotels in establishments2 from Parent destination: Algarve, Child destinations: Alvor, Armacao De Pera, Burgao, Carvoeiro
            List<string> establishments3 = new List<string>() { "54756", "857", "15869", "19676", "5289", "203097", "4941", "30016", "780", "811", "133509", "778", "14907", "62", "100817", "196227", "3041", "127973" };

            // All hotels in establishments2 from Parent destination: Orlando Area (Florida), Child destinations: Kissimmee, Lake Buena Vista
            List<string> establishments4 = new List<string>() { "63019", "16198", "12964", "12965", "12962", "68552", "60446", "19310", "136260", "13301", "16962", "82774", "16144", "162972" };

            // All hotels in establishments2 from Parent destination: Venice (Ital), Child destinations: Quarto D'Altino, Sestiere San Polo, Venecia Sestiere Castello
            List<string> establishments5 = new List<string>() { "188986", "126237", "205705", "211499", "211600", "153557", "205837", "211656", "127237", "211403", "207298", "207122", "137182", "162621", "205003", "106968" }; 




            List<List<string>> establishments = new List<List<string>>() { establishments1, establishments2, establishments3, establishments4, establishments5 };
            //List<List<string>> establishments = new List<List<string>>() { establishments1 };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            //for (int y = 0; y < 10; y++)
            //{
                foreach (List<string> establishmentList in establishments)
                {
                    try
                    {
                        // Create the supplier
                        AccommodationProvider provider = CreateProvider();

                        // Request Parameters
                        DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200)); // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                        DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14)); // Max 14 days duration.
                        SearchType searchType = SearchType.FlightAndHotel;
                        Channel channel = Channel.AlphaRoomsUK;

                        string[] destinationEdiCodes = new string[] {};

                        string[] establishmentEdiCodes = establishmentList.ToArray();

                        // Rooms : 2 identical rooms
                        List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                        AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] {});
                        AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new byte[] {});
                        rooms.Add(room1);
                        rooms.Add(room2);

                        // Create the request
                        AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                        // Create Provider
                        IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                        // Get availability results from supplier
                        IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                        List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                        // Availability Assertions
                        Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));
                        RunAvailabilityAssertions(availabilityRequest, string.Empty, results);


                        // 2. Valuation

                        // Select Post Payment rooms only
                        List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                        results = results.Where(result => result.PaymentModel == PaymentModel.PostPayment).ToList();

                        // For Room 1: Select a matching availability result at random
                        AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for establishments {0} - test cannot continue.", establishmentList.ToString()));
                        selectedRooms.Add(firstRoomResult);

                        // For Room 2: Select a matching availability result at random
                        AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for establishments {0} - test cannot continue.", establishmentList.ToString()));
                        selectedRooms.Add(secondRoomResult);

                        // For debugging
                        List<string> serialisedSelectedRooms = new List<string>();

                        foreach (var selectedRoom in selectedRooms)
                        {
                            TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                            string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                            serialisedSelectedRooms.Add(serialisedSelectedRoom);
                        }


                        // Create the valuation request
                        AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                        // Create the Valuation Provider
                        IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                        // Get valuation result from Provider
                        IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                        List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                        // Valuation Assertions
                        Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for establishments {0} - test cannot continue.", string.Join(", ", establishmentList.ToArray())));

                        foreach (var valuationResult in valuationResults)
                        {
                            AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                            if (selectedRoom != null)
                            {
                                RunValuationAssertions(selectedRoom, valuationResult);
                            }
                            else
                            {
                                Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                            }
                        }


                        // 3. BOOKING
                        // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                        List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                        // Create the Booking Request
                        AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                        // Create the Booking Provider
                        IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                        // Get valuation result from Provider
                        IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                        List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                        // Run Booking Assertions
                        if (bookResults != null && bookResults.Any())
                        {
                            foreach (var bookingResult in bookingResults)
                            {
                                RunBookingAssertions(bookingResult);
                            }
                        }

                    await Task.Delay(500);
                    }
                    catch (Exception ex)
                    {
                        // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                        if (ex.Message.Contains("HotelHeavy"))
                        {
                            // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                            errorHotelHeavyCount++;
                            sb.AppendLine(string.Format("Establishments: {0}. HotelHeavy Error.", establishmentList.ToString(';')));
                        }
                        else if (ex.Message.Contains("No availability results returned"))
                        {
                            // Do nothing -> continue to next destination in loop.
                            errorNoAvailabilityCount++;
                            sb.AppendLine(string.Format("Establishments: {0}. No availability results returned.", establishmentList.ToString(';')));
                        }
                        else if (ex.Message.Contains("(500) Internal Server Error"))
                        {
                            // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                            error500Count++;
                            sb.AppendLine(string.Format("Establishments: {0}. (500) Internal Server Error.", establishmentList.ToString(';')));
                        }
                        else if (ex.Message.Contains("The underlying connection was closed"))
                        {
                            // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                            errorConnectionClosedCount++;
                            sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", establishmentList.ToString(';')));
                        }
                        else
                        {
                                throw ex;
                            }
                        }
                }
            //}

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        private void RunAvailabilityAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode, List<AccommodationProviderAvailabilityResult> results)
        {
            try
            {
                // 1. That all rooms have the correct number of adults and children
                foreach (var room in request.Rooms)
                {
                    IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom = results.Where(result => result.RoomNumber == room.RoomNumber);

                    // Number of Adults
                    bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                    Assert.IsTrue(hasWrongNumberOfAdults == false, "At least one room result has the wrong number of adults.");

                    // Number of children
                    bool hasWrongNumberOfChildren = resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);
                    Assert.IsTrue(hasWrongNumberOfChildren == false, "At least one room result has the wrong number of children.");
                }

                // 2. That all results have a provider room code
                bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
                Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

                // 3. That all results have a provider room code
                bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
                Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

                // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
                bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
                Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

                // 5. That all results have an establishment name
                bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
                Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

                // 6. That all results have an establishment Edi Code
                bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
                Assert.IsTrue(hasNoEstablishmentEdiCode == false, "At least one room result is missing an establishment Edi Code.");

                // 7. That all results have a check in date
                bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
                Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

                // 8. That all results have a check out date
                bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
                Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

                // 9. That every room has at least one adult in it
                bool hasNoAdult = results.Any(a => a.Adults == 0);
                Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

                // 10. That the RoomId of each room falls within the room numbers provided in the request -> cannot rely on all rooms being returned in availability results. Therefore, disable this test.
                List<byte> validRoomIds = request.Rooms.Select(room => room.RoomNumber).ToList();
                //foreach (int roomId in validRoomIds)
                //{
                //    int count = results.Count(room => room.RoomNumber == roomId);
                //    var currentRoom = request.Rooms.First(room => room.RoomNumber == roomId);
                //    Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results. Room Requested: Adults: {1}, Children: {2}", roomId, currentRoom.Guests.Adults, currentRoom.Guests.Children + currentRoom.Guests.Infants));
                //}

                bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
                Assert.IsTrue(hasIncorrectRoomId == false, "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

                // 11. That all sale prices have a currency
                bool hasNoSalePriceCurrency = false;
                foreach (var result in results)
                {
                    if (result.SalePrice != null &&
                        string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                    {
                        hasNoSalePriceCurrency = true;
                        break;
                    }
                }
                Assert.IsTrue(hasNoSalePriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

                // 12. That all cost prices have a currency
                bool hasNoCostPriceCurrency = false;
                foreach (var result in results)
                {
                    if (result.CostPrice != null &&
                        string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                    {
                        hasNoCostPriceCurrency = true;
                        break;
                    }
                }
                Assert.IsTrue(hasNoCostPriceCurrency == false, "At least one room result has no currency set for the SalePrice.");

                // 13. That all results have either a cost price or a sale price 
                bool hasNoPrice = false;
                foreach (var result in results)
                {
                    if (result.CostPrice == null && result.SalePrice == null)
                    {
                        hasNoPrice = true;
                        break;
                    }
                }
                Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Check that the valuation result matches the selected availability result.
        /// </summary>
        /// <param name="selectedRoom"></param>
        /// <param name="valuationResult"></param>
        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {
            TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
            string serialisedAvailability = testAvailabilityResult.XmlSerialize();
            serialisedAvailability = HotelBedsMapper.ReplaceSpecialChars(serialisedAvailability);

            TestValuationResult testValuationResult = HotelBedsMapper.CloneValuationResult(valuationResult);
            string serialisedValuation = testValuationResult.XmlSerialize();
            serialisedValuation = HotelBedsMapper.ReplaceSpecialChars(serialisedValuation);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Availability Result:");
            sb.AppendLine(serialisedAvailability);
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                // Remove Cost Price Test for booking tests ONLY
                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                //if (valuationResult.CostPrice.Amount < selectedRoom.CostPrice.Amount - 2.00m ||
                //    valuationResult.CostPrice.Amount > selectedRoom.CostPrice.Amount + 2.00m)
                //{
                //    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);
                //}

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                // Remove Cost Price Test for booking Tests ONLY
                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                //if (valuationResult.SalePrice.Amount < selectedRoom.SalePrice.Amount - 2.00m ||
                //    valuationResult.SalePrice.Amount > selectedRoom.SalePrice.Amount + 2.00m)
                //{
                //    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);
                //}

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        /// <summary>
        /// Check that the booking result is confirmed.
        /// </summary>
        /// <param name="bookingResult"></param>
        private void RunBookingAssertions(AccommodationProviderBookingResult bookingResult)
        {
            try
            {
                Assert.IsTrue(bookingResult.BookingStatus == BookingStatus.Confirmed, string.Format("Booking Failure: Booking Status is NOT Confirmed. Message: {0}", bookingResult.Message));

                Assert.IsTrue(Regex.IsMatch(bookingResult.ProviderBookingReference, @"^\d+-\d+"), string.Format("Booking Failure: Booking Reference is either not set, or has an invalid format. Message: {0}", bookingResult.Message));

                Assert.IsTrue(bookingResult.RoomNumber != 0, string.Format("Booking Failure: Booking Result Room Number is set to zero!. Message: {0}", bookingResult.Message));
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        /// <summary>
        /// Required to remove the ProviderSpecific property which cannot be serialised.
        /// </summary>
        private class HotelBedsMapper
        {
            static HotelBedsMapper()
            {
                lock (Automapper.MapperCreateMapLock)
                {
                    Mapper.CreateMap<AccommodationProviderAvailabilityResult, TestAvailabilityResult>();
                    Mapper.CreateMap<AccommodationProviderValuationResult, TestValuationResult>();
                }
            }

            public static TestAvailabilityResult CloneAvailabilityResult(AccommodationProviderAvailabilityResult availabilityResult)
            {
                TestAvailabilityResult testResult = Mapper.Map<AccommodationProviderAvailabilityResult, TestAvailabilityResult>(availabilityResult);

                StringBuilder sb = new StringBuilder();

                foreach (var entry in availabilityResult.ProviderSpecificData)
                {
                    sb.AppendLine(entry.Key + ": ");
                    sb.AppendLine(ReplaceSpecialChars(entry.Value));
                }

                testResult.ProviderSpecificDetails = sb.ToString();

                return testResult;
            }

            public static TestValuationResult CloneValuationResult(AccommodationProviderValuationResult valuationResult)
            {
                TestValuationResult testResult = Mapper.Map<AccommodationProviderValuationResult, TestValuationResult>(valuationResult);

                StringBuilder sb = new StringBuilder();

                foreach (var entry in valuationResult.ProviderSpecificData)
                {
                    sb.AppendLine(entry.Key + ": ");
                    sb.AppendLine(ReplaceSpecialChars(entry.Value));
                }

                testResult.ProviderSpecificDetails = sb.ToString();

                return testResult;
            }

            private static string XmlDecode(string value)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml("<root>" + value + "</root>");
                return xmlDoc.InnerText;
            }

            public static string ReplaceSpecialChars(string originalText)
            {
                string replacedText = originalText.Replace("&lt;", "<")
                                                    .Replace("&amp;", "&")
                                                    .Replace("&gt;", ">")
                                                    .Replace("&quot;", "\"")
                                                    .Replace("&apos;", "'");

                return replacedText;
            }
        }

        public class TestAvailabilityResult
        {
            public Guid Id { get; set; }
            public int RoomNumber { get; set; }
            public string ProviderEdiCode { get; set; }

            public string DestinationEdiCode { get; set; }

            public string EstablishmentEdiCode { get; set; }
            public string EstablishmentName { get; set; }

            public int Adults { get; set; }
            public int Children { get; set; }
            public int Infants { get; set; }

            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }

            public string BoardCode { get; set; }
            public string BoardDescription { get; set; }

            public string RoomCode { get; set; }
            public string RoomDescription { get; set; }

            public Money SalePrice { get; set; }
            public Money CostPrice { get; set; }

            public PaymentModel PaymentModel { get; set; }

            public bool IsOpaqueRate { get; set; }
            public bool IsSpecialRate { get; set; }
            public bool? IsNonRefundableRate { get; set; }
            public bool IsBindingRate { get; set; }

            public RateType RateType { get; set; }

            public int? NumberOfAvailableRooms { get; set; }

            public string ProviderSpecificDetails { get; set; }
        }

        public class TestValuationResult
        {
            public Guid Id { get; set; }
            public int RoomNumber { get; set; }

            public string ProviderName { get; set; }
            public string ProviderEdiCode { get; set; }

            public string DestinationEdiCode { get; set; }

            public string EstablishmentEdiCode { get; set; }
            public string EstablishmentName { get; set; }

            public int Adults { get; set; }
            public int Children { get; set; }
            public int Infants { get; set; }

            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }

            public string BoardCode { get; set; }
            public string BoardDescription { get; set; }

            public string RoomCode { get; set; }
            public string RoomDescription { get; set; }

            public Money SalePrice { get; set; }
            public Money CostPrice { get; set; }

            public decimal CommissionPercent { get; set; }
            public Money CommissionAmount { get; set; }

            public PaymentModel PaymentModel { get; set; }

            public bool IsOpaqueRate { get; set; }
            public bool IsSpecialRate { get; set; }
            public bool? IsNonRefundableRate { get; set; }
            public bool IsBindingRate { get; set; }

            public RateType RateType { get; set; }

            public string CancellationPolicy { get; set; }

            public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }

            public string ProviderSpecificDetails { get; set; }
        }
    }
}
