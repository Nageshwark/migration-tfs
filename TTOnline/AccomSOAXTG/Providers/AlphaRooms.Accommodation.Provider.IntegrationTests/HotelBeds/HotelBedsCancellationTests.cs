﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.IntegrationTests.HotelBeds
{
    public class HotelBedsCancellationTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        private DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = RandomNumberGenerator.Between(1, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }

        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
                                                                                AccommodationProviderAvailabilityResult[] selectedRooms)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();

            valuationRequest.Provider = availabilityRequest.Provider;
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;
            valuationRequest.ChannelInfo = new ChannelInfo() { Channel = availabilityRequest.ChannelInfo.Channel };
            valuationRequest.AvailabilityId = availabilityRequest.AvailabilityId;

            List<AccommodationProviderValuationRequestRoom> roomsToBeValued = new List<AccommodationProviderValuationRequestRoom>();

            foreach (var selectedRoom in selectedRooms)
            {
                AccommodationProviderValuationRequestRoom roomToBeValued = new AccommodationProviderValuationRequestRoom();

                roomToBeValued.AvailabilityRequest = availabilityRequest;
                roomToBeValued.AvailabilityResult = selectedRoom;
                roomToBeValued.RoomNumber = selectedRoom.RoomNumber;

                AccommodationProviderAvailabilityRequestRoom requestRoom = availabilityRequest.Rooms.First(room => room.RoomNumber == selectedRoom.RoomNumber);
                roomToBeValued.Guests = new AccommodationProviderAvailabilityRequestGuestCollection(requestRoom.Guests.ToList());

                roomsToBeValued.Add(roomToBeValued);
            }

            valuationRequest.SelectedRooms = roomsToBeValued.ToArray();

            return valuationRequest;
        }



        private AccommodationProviderBookingRequest CreateBookingRequest(AccommodationProvider provider,
                                                                            Channel channel,
                                                                            List<AccommodationProviderBookingRequestRoom> bookingRequestRooms,
                                                                            AccommodationProviderValuationRequest valuationRequest)
        {
            AccommodationProviderBookingRequest bookingRequest = new AccommodationProviderBookingRequest();

            bookingRequest.BookingId = Guid.NewGuid();
            bookingRequest.ValuationId = valuationRequest.ValuationId;
            bookingRequest.AvailabilityId = valuationRequest.AvailabilityId;

            bookingRequest.Provider = provider;
            bookingRequest.ChannelInfo = new ChannelInfo() { Channel = channel };
            bookingRequest.ValuatedRooms = bookingRequestRooms.ToArray();

            bookingRequest.Debugging = true;
            bookingRequest.Customer = CreateCustomer();

            bookingRequest.ItineraryId = RandomNumberGenerator.Between(0, 999999999);

            return bookingRequest;
        }

        private List<AccommodationProviderBookingRequestRoomGuest> CreateGuests(AccommodationProviderValuationRequestRoom roomOccupancy)
        {
            List<AccommodationProviderBookingRequestRoomGuest> guests = new List<AccommodationProviderBookingRequestRoomGuest>();
            int i = 1;

            // NOTE: only supports a single room at present
            foreach (var roomguest in roomOccupancy.Guests)
            {
                AccommodationProviderBookingRequestRoomGuest guest = new AccommodationProviderBookingRequestRoomGuest();

                // Adult/Child
                if (roomguest.Type == GuestType.Adult)
                {
                    guest.Type = GuestType.Adult;
                }
                else
                {
                    guest.Type = GuestType.Child;
                }

                Gender gender = (Gender)RandomNumberGenerator.Between(1, 2);

                // Set the Title
                if (gender == Gender.Male && guest.Type == GuestType.Adult)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleAdultTitles.Count - 1)]);
                }
                else if (gender == Gender.Female && guest.Type == GuestType.Adult)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleAdultTitles.Count - 1)]);
                }
                else if (gender == Gender.Male && guest.Type == GuestType.Child)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleChildTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleChildTitles.Count - 1)]);
                }
                else if (gender == Gender.Female && guest.Type == GuestType.Child)
                {
                    guest.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleChildTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleChildTitles.Count - 1)]);
                }

                // Set the first name
                if (gender == Gender.Male)
                {
                    guest.FirstName = HotelBedsSupportClasses.MaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleFirstNames.Count - 1)];
                }
                else
                {
                    guest.FirstName = HotelBedsSupportClasses.FemaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleFirstNames.Count - 1)];
                }

                guest.Surname = HotelBedsSupportClasses.LastNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.LastNames.Count - 1)];
                guest.Age = (byte)Int16.Parse(roomguest.Age.ToString());

                guest.Id = i;
                i++;

                guests.Add(guest);
            }

            // Set the lead guest
            var adultguest = guests.First(pax => pax.Type == GuestType.Adult);
            return guests;
        }

        private AccommodationProviderBookingRequestCustomer CreateCustomer()
        {
            AccommodationProviderBookingRequestCustomer customer = new AccommodationProviderBookingRequestCustomer();

            Gender gender = (Gender)RandomNumberGenerator.Between(1, 2);

            // Set the Title
            if (gender == Gender.Male)
            {
                customer.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.MaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleAdultTitles.Count - 1)]);
            }
            else if (gender == Gender.Female)
            {
                customer.Title = (GuestTitle)Enum.Parse(typeof(GuestTitle), HotelBedsSupportClasses.FemaleAdultTitles[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleAdultTitles.Count - 1)]);
            }

            // Set the first name
            if (gender == Gender.Male)
            {
                customer.FirstName = HotelBedsSupportClasses.MaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.MaleFirstNames.Count - 1)];
            }
            else
            {
                customer.FirstName = HotelBedsSupportClasses.FemaleFirstNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.FemaleFirstNames.Count - 1)];
            }

            // Non gender specific details
            customer.TitleString = customer.Title.ToString();
            customer.Surname = HotelBedsSupportClasses.LastNames[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.LastNames.Count - 1)];
            customer.EmailAddress = HotelBedsSupportClasses.EmailAddresses[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.EmailAddresses.Count - 1)];
            customer.ContactNumber = HotelBedsSupportClasses.HomePhones[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.HomePhones.Count - 1)];
            customer.Age = (byte)RandomNumberGenerator.Between(25, 80);
            customer.Type = GuestType.Adult;

            return customer;
        }

        private AccommodationProviderBookingRequestRoomPaymentDetails CreateCardDetails(string cardHolderName, AccommodationProviderValuationResult valuationResult)
        {
            AccommodationProviderBookingRequestRoomPaymentDetails card = new AccommodationProviderBookingRequestRoomPaymentDetails();

            card.CardHolderName = cardHolderName;
            card.AddressLine1 = HotelBedsSupportClasses.AddressLine1[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.AddressLine1.Count - 1)];
            card.AddressLine2 = HotelBedsSupportClasses.AddressLine2[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.AddressLine2.Count - 1)];
            card.City = HotelBedsSupportClasses.Cities[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Cities.Count - 1)];
            //card.County = HotelBedsSupportClasses.Counties[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Counties.Count - 1)];
            card.Postcode = HotelBedsSupportClasses.Postcodes[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.Postcodes.Count - 1)];
            card.Country = "United Kingdom";
            //card.CountryCode = "UK";

            // Get a matching credit card for the card type and the fare currency
            try
            {
                HotelBedsSupportClasses.TestCreditCard selectedCard = null;
                List<AccommodationProviderPaymentOption> acceptedCreditCards = valuationResult.ProviderPaymentOptions;

                // Get a list of all the test cards which match the cards accepted by the hotel
                var matchingTestCards = (from testCard in HotelBedsSupportClasses.TestCreditCards
                                         join acceptedCard in acceptedCreditCards
                                         on testCard.CardType equals acceptedCard.CardType
                                         select testCard).ToList();

                // Select a test card at random
                if (matchingTestCards != null && matchingTestCards.Count > 0)
                {
                    selectedCard = matchingTestCards[RandomNumberGenerator.Between(0, matchingTestCards.Count() - 1)];
                    card.CardNumber = selectedCard.CardNumber;
                    card.CardType = selectedCard.CardType;
                }
                else
                {
                    throw new NullReferenceException("Unable to Locate a matching test credit card to use with the Valuation Result.");
                }

                card.CardExpireDate = new MonthYear(6, 19);
                card.CardSecurityCode = "123";
            }
            catch (Exception ex)
            {
                throw new Exception("Credit Card Problem.", ex);
            }

            return card;
        }

        private AccommodationProviderBookingRequestRoomSpecialRequest CreateSpecialBookingRequest()
        {
            var specialRequest = HotelBedsSupportClasses.SpecialBookingRequests[RandomNumberGenerator.Between(0, HotelBedsSupportClasses.SpecialBookingRequests.Count - 1)];
            return specialRequest;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = (byte)RandomNumberGenerator.Between(18, 80) });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        /// <summary>
        /// Note that if a boolean parameter has a value of false, then that criteria is simply excluded from the search. This means that the search could actually return the value that was included in the List<>
        /// parameter that goes with boolean flag. This is done because exclusing data makes it more likely that no matching record will be found, which is not desirable and will fail the test.
        /// </summary>
        /// <param name="availabilityRequestRoom"></param>
        /// <param name="results"></param>
        /// <param name="matchContract"></param>
        /// <param name="contracts"></param>
        /// <param name="matchEstablishment"></param>
        /// <param name="establishmentEdiCodes"></param>
        /// <param name="matchRoomCode"></param>
        /// <param name="roomCodes"></param>
        /// <returns></returns>
        private AccommodationProviderAvailabilityResult GetAvailabilityRoom(AccommodationProviderAvailabilityRequestRoom availabilityRequestRoom,
                                                                            List<AccommodationProviderAvailabilityResult> results,
                                                                            bool matchContract,
                                                                            List<string> contracts,
                                                                            bool matchEstablishment,
                                                                            List<string> establishmentEdiCodes,
                                                                            bool matchRoomCode,
                                                                            List<string> roomCodes)
        {
            AccommodationProviderAvailabilityResult selectedRoom = null;
            List<AccommodationProviderAvailabilityResult> matchingResults = null;

            int noAdults = availabilityRequestRoom.Guests.AdultsCount;
            int noChildren = availabilityRequestRoom.Guests.ChildrenCount + availabilityRequestRoom.Guests.InfantsCount;
            int roomNumber = availabilityRequestRoom.RoomNumber;

            if (!contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber).ToList();
                    }
                }
            }
            else if (matchContract && contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    contracts.Contains(result.Contract)).ToList();
                    }
                }
            }
            else if (!matchContract && contracts.Any())
            {
                if (matchRoomCode)
                {
                    if (matchEstablishment)
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    roomCodes.Contains(result.RoomCode)).ToList();
                    }
                }
                else
                {
                    if (matchEstablishment)
                    {

                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract) &&
                                                                    establishmentEdiCodes.Contains(result.EstablishmentEdiCode)).ToList();
                    }
                    else
                    {
                        matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren &&
                                                                    result.RoomNumber == roomNumber &&
                                                                    !contracts.Contains(result.Contract)).ToList();
                    }
                }
            }

            if (matchingResults.Count > 0)
            {
                int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                selectedRoom = matchingResults[roomIndex];
            }

            return selectedRoom;
        }

        private List<AccommodationProviderBookingRequestRoom> CreateBookingRequestRooms(AccommodationProviderValuationRequest valuationRequest, List<AccommodationProviderValuationResult> valuationResults)
        {
            List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = new List<AccommodationProviderBookingRequestRoom>();

            AccommodationProviderBookingRequestRoomPaymentDetails cardDetails = null;

            foreach (var valuationResult in valuationResults)
            {
                AccommodationProviderBookingRequestRoom bookingRequestRoom = new AccommodationProviderBookingRequestRoom();

                AccommodationProviderValuationRequestRoom valuationRequestRoom = valuationRequest.SelectedRooms.Where(room => room.RoomNumber == valuationResult.RoomNumber).First();
                Assert.IsNotNull(valuationRequestRoom, string.Format("Room {0}: Unable to identify the AccommodationProviderValuationRequestRoom for this room number - test cannot continue.", valuationResult.RoomNumber));

                bookingRequestRoom.ValuationResult = valuationResult;
                bookingRequestRoom.AvailabilityRequest = valuationRequestRoom.AvailabilityRequest;
                bookingRequestRoom.SpecialRequests = CreateSpecialBookingRequest();
                bookingRequestRoom.RoomNumber = valuationResult.RoomNumber;

                // Create the guests
                List<AccommodationProviderBookingRequestRoomGuest> guests = CreateGuests(valuationRequestRoom);
                bookingRequestRoom.Guests = new AccommodationProviderBookingRequestRoomGuestCollection(guests);

                // Only get card payment details if the room is a customer pay direct room, and no card details have so far been registered.
                if (valuationResult.PaymentModel == PaymentModel.CustomerPayDirect &&
                    cardDetails == null)
                {
                    var guest = guests.Where(gst => gst.Type == GuestType.Adult).OrderByDescending(gst => gst.Age).First();
                    string name = guest.FirstName + " " + guest.Surname;
                    cardDetails = CreateCardDetails(name, valuationResult);
                }

                if (valuationResult.PaymentModel == PaymentModel.CustomerPayDirect)
                {
                    bookingRequestRoom.PaymentDetails = cardDetails;
                }

                bookingRequestRooms.Add(bookingRequestRoom);
            }

            return bookingRequestRooms;
        }

        private AccommodationProviderCancellationRequest CreateCancellationRequest(AccommodationProviderBookingRequest bookingRequest, AccommodationProviderBookingResult bookingResult)
        {
            AccommodationProviderCancellationRequest cancellationRequest = new AccommodationProviderCancellationRequest();

            cancellationRequest.CancellationId = Guid.NewGuid();
            cancellationRequest.ItineraryId = RandomNumberGenerator.Between(1000000, 9999999);
            cancellationRequest.EstablishmentEdiCode = bookingRequest.ValuatedRooms.First().ValuationResult.EstablishmentEdiCode;

            cancellationRequest.CheckInDate = bookingRequest.ValuatedRooms.First().ValuationResult.CheckInDate;
            cancellationRequest.CheckOutDate = bookingRequest.ValuatedRooms.First().ValuationResult.CheckOutDate;
            cancellationRequest.CustomerFirstName = bookingRequest.ValuatedRooms.First().Guests.First().FirstName;

            cancellationRequest.CustomerSurName = bookingRequest.ValuatedRooms.First().Guests.First().Surname;
            cancellationRequest.Debugging = true;
            cancellationRequest.Provider = bookingRequest.ValuatedRooms.First().AvailabilityRequest.Provider;
            cancellationRequest.ProviderBookingReference = bookingResult.ProviderBookingReference;

            return cancellationRequest;
        }

        /// <summary>
        /// Check that the booking result is confirmed.
        /// </summary>
        /// <param name="bookingResult"></param>
        private void RunBookingAssertions(AccommodationProviderBookingResult bookingResult)
        {
            try
            {
                Assert.IsTrue(bookingResult.BookingStatus == BookingStatus.Confirmed, string.Format("Booking Failure: Booking Status is NOT Confirmed. Message: {0}", bookingResult.Message));

                Assert.IsTrue(Regex.IsMatch(bookingResult.ProviderBookingReference, @"^\d+-\d+"), string.Format("Booking Failure: Booking Reference is either not set, or has an invalid format. Message: {0}", bookingResult.Message));

                Assert.IsTrue(bookingResult.RoomNumber != 0, string.Format("Booking Failure: Booking Result Room Number is set to zero!. Message: {0}", bookingResult.Message));
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        /// <summary>
        /// Check that the cancellation result is confirmed.
        /// </summary>
        /// <param name="cancellationResult"></param>
        private void RunCancellationAssertions(AccommodationProviderCancellationResult cancellationResult)
        {
            try
            {
                Assert.IsTrue(cancellationResult.CancellationStatus == CancellationStatus.Succeeded, string.Format("Cancellation Failure: Cancellation is NOT Confirmed. Message: {0}", cancellationResult.Message));

                Assert.IsTrue(Regex.IsMatch(cancellationResult.ProviderCancellationReference, @"^\d+-\d+"), string.Format("Cancellation Failure: Cancellation Reference is either not set, or has an invalid format. Message: {0}", cancellationResult.Message));
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        private AccommodationProvider CreateProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 2;
            accommodationProvider.EdiCode = "B";
            accommodationProvider.Name = "HotelBeds";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsApiVersion",              ParameterValue = "2013/12"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsDefaultLanguage",         ParameterValue = "ENG"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsUserName",                ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsPassword",                ParameterValue = "ALPHAXML2UK145212"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsAvailabilityUrl",         ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsValuationUrl",            ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPayDirect",     ParameterValue = "https://testconfirm.interface-xml.com/platform/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsBookingUrlPostPayment",   ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsCancellationUrl",         ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsPayDirectRate",           ParameterValue = "GrossPayDirect"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsNetNonBindingRate",       ParameterValue = "NetStandard"},
                new AccommodationProviderParameter{ProviderId = 2,  ParameterName = "HotelBedsNetBindingRate",          ParameterValue = "NetBinding"},
            };

            return accommodationProvider;
        }

        /// <summary>
        /// This test cannot be used because it is based on a booking reference number that is no longer valid. If you try to cancel it, you will receive an Invalid Credentials error message.
        /// Email from HotelBeds on 29/09/2015:
        /// "As a booking gets cancelled or queried (purchase detail) the XML credentials from the request get validated against the ones loaded in our system as the booking's creators. 
        /// In the testing environments the bookings get overwritten periodically with the ones from the live environment as test gets recovered.
        /// As the live booking with the same reference was confirmed by someone else you will get this credentials validation error."
        /// </summary>
        /// <returns></returns>
        //[Test]
        //public async Task SingleRoomCancellation_Success()
        //{
        //    IAccommodationCancellationProvider cancelProvider = NinjectInstaller.Kernel.Get<IAccommodationCancellationProvider>("HotelBeds");

        //    var provider = CreateProvider();

        //    var channelInfo = new ChannelInfo()
        //    {
        //        Channel = Channel.AlphaRoomsUK
        //    };

        //    var providerCancelRequest = new AccommodationProviderCancellationRequest()
        //    {
        //        CancellationId = Guid.NewGuid(),
        //        ItineraryId = 12345,
        //        Provider = provider,
        //        ProviderBookingReference = "235-3009471", 
        //        CheckInDate = new DateTime(2016, 05, 24),
        //        CheckOutDate = new DateTime(2016, 05, 25),
        //        EstablishmentEdiCode = "",
        //        CustomerFirstName = "Test",
        //        CustomerSurName = "Test"
        //    };

        //    var cancelResult = await cancelProvider.CancelProviderBookingAsync(providerCancelRequest);

        //    //1. Results not equal to null
        //    Assert.IsNotNull(cancelResult, "The cancellation result is null.");
        //    Assert.IsTrue(cancelResult.Any(), "Test failed. No results returned.");
        //}

               [Test]
        public async Task HotelBeds_Cancellation_1Room_WillCancel()
        {
            // Perform availability, valuation and booking for one room. Then cancel.
            // NOTE: "BGI8" is a destination with just one hotel -> very useful for testing
            // TPA2 has only a handful of hotels
            List<string> destinations = new List<string>() { "TPA2", "PYX2", "CNS", "FLR", "ALC99", "BEN" };
            //List<string> destinations = new List<string>() { "BGI8" };
            //List<string> destinations = new List<string>() { "BCN" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // 1. AVAILABILITY
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(300)); // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(16));       // Max 16 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create the Availability Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from provider
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 2. VALUATION
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    List<AccommodationProviderAvailabilityResult> resultsFiltered = results.Where(result => result.PaymentModel == PaymentModel.CustomerPayDirect).ToList();
                    if (resultsFiltered.Count == 0)
                    {
                        resultsFiltered = results.Where(result => result.PaymentModel == PaymentModel.PostPayment).ToList();
                    }


                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: resultsFiltered,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    Assert.IsTrue(bookingResults.Count >= 1, string.Format("No booking results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }


                    // 4. CANCELLATION
                    // Create the cancellation request
                    AccommodationProviderCancellationRequest cancellationRequest = CreateCancellationRequest(bookingRequest, bookingResults.First());

                    // Create the Cancellation Provider
                    IAccommodationCancellationProvider cancellationProvider = NinjectInstaller.Kernel.Get<IAccommodationCancellationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderCancellationResult> cancelResults = await cancellationProvider.CancelProviderBookingAsync(cancellationRequest);
                    List<AccommodationProviderCancellationResult> cancellationResults = cancelResults.ToList();

                    Assert.IsTrue(cancellationResults.Count >= 1, string.Format("No cancellation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    // Run Cancellation Assertions
                    if (cancellationResults != null && cancellationResults.Any())
                    {
                        RunCancellationAssertions(cancellationResults.First());
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("Error getting object from pool"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelBeds Cache Retrieval Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Cancellation_1Room_TryToCancelTwice_WillFail()
        {
            // Perform availability, valuation and booking for one room. Then cancel.
            // NOTE: "BGI8" is a destination with just one hotel -> very useful for testing
            // TPA2 has only a handful of hotels
            List<string> destinations = new List<string>() { "TPA2", "PYX2", "CNS", "FLR", "ALC99", "BEN" };
            //List<string> destinations = new List<string>() { "BGI8" };
            //List<string> destinations = new List<string>() { "BCN" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // 1. AVAILABILITY
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(300)); // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(16));       // Max 16 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };
                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create the Availability Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from provider
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 2. VALUATION
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    List<AccommodationProviderAvailabilityResult> resultsFiltered = results.Where(result => result.PaymentModel == PaymentModel.CustomerPayDirect).ToList();
                    if (resultsFiltered.Count == 0)
                    {
                        resultsFiltered = results.Where(result => result.PaymentModel == PaymentModel.PostPayment).ToList();
                    }


                    // For Room 1: Select a matching availability result at random
                    AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: resultsFiltered,
                                                                                                    matchContract: false, contracts: new List<string>(),
                                                                                                    matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                    matchRoomCode: false, roomCodes: new List<string>());
                    Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                    selectedRooms.Add(firstRoomResult);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get booking result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    Assert.IsTrue(bookingResults.Count >= 1, string.Format("No booking results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }


                    // 4. CANCELLATION
                    // Create the cancellation request
                    AccommodationProviderCancellationRequest cancellationRequest = CreateCancellationRequest(bookingRequest, bookingResults.First());

                    // Create the Cancellation Provider
                    IAccommodationCancellationProvider cancellationProvider = NinjectInstaller.Kernel.Get<IAccommodationCancellationProvider>("HotelBeds");

                    // Get cancellation result from Provider
                    IEnumerable<AccommodationProviderCancellationResult> cancelResults = await cancellationProvider.CancelProviderBookingAsync(cancellationRequest);
                    List<AccommodationProviderCancellationResult> cancellationResults = cancelResults.ToList();

                    Assert.IsTrue(cancellationResults.Count >= 1, string.Format("No cancellation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    // Run Cancellation Assertions
                    if (cancellationResults != null && cancellationResults.Any())
                    {
                        RunCancellationAssertions(cancellationResults.First());
                    }


                    // 5. CANCEL TWICE -> WILL FAIL
                    IEnumerable<AccommodationProviderCancellationResult> cancelResultsFailed = await cancellationProvider.CancelProviderBookingAsync(cancellationRequest);
                    AccommodationProviderCancellationResult cancelResultFailed = cancelResultsFailed.First();

                    Assert.IsTrue(cancelResultFailed.CancellationStatus == CancellationStatus.Failed, "Duplicate cancellation should have a status of failed as it cannot be cancelled twice!");
                    Assert.IsTrue(cancelResultFailed.Message.Contains("The booking has already been cancelled", StringComparison.InvariantCultureIgnoreCase), "Duplicate cancellation should return 'The booking has already been cancelled' in the message.");

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("Error getting object from pool"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelBeds Cache Retrieval Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

        [Test]
        public async Task HotelBeds_Cancellation_5Rooms_WillCancel()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            //List<string> destinations = new List<string>() { "BGI3", "PYX2", "CNS", "FLR", "PTY1", "PBI11", "BOU", "TFS", };
            List<string> destinations = new List<string>() { "LVS8", "BEN" };

            // Error Counter
            int error500Count = 0;
            int errorConnectionClosedCount = 0;
            int errorHotelHeavyCount = 0;
            int errorNoAvailabilityCount = 0;
            StringBuilder sb = new StringBuilder();

            foreach (var destinationEdiCode in destinations)
            {
                try
                {
                    // Create the supplier
                    AccommodationProvider provider = CreateProvider();

                    // Request Parameters
                    DateTime checkInDate = GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));       // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                    DateTime checkOutDate = GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));             // Max 14 days duration.
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms : 5 rooms, two of which are identical
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(roomNumber: 1, numberAdults: 2, childAges: new byte[] { });
                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(roomNumber: 2, numberAdults: 2, childAges: new byte[] { 4, 6 });
                    AccommodationProviderAvailabilityRequestRoom room3 = CreateProviderRequestRoom(roomNumber: 3, numberAdults: 2, childAges: new byte[] { 1, 9 });
                    AccommodationProviderAvailabilityRequestRoom room4 = CreateProviderRequestRoom(roomNumber: 4, numberAdults: 2, childAges: new byte[] { 6, 10, 8 });
                    AccommodationProviderAvailabilityRequestRoom room5 = CreateProviderRequestRoom(roomNumber: 5, numberAdults: 2, childAges: new byte[] { });

                    rooms.Add(room1);
                    rooms.Add(room2);
                    rooms.Add(room3);
                    rooms.Add(room4);
                    rooms.Add(room5);

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("HotelBeds");

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Availability Assertions
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 2. Valuation
                    // Select Rooms from Availability Results
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    // Temporary condition to ensure at least 2 direct pay rooms
                    int counter = 0;

                    do
                    {
                        selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                        // Select 5 rooms from three different contracts, but all in the same hotel.
                        // Room 1: Get any result at random
                        AccommodationProviderAvailabilityResult firstRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[0], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(firstRoomResult, string.Format("Room 1: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(firstRoomResult);

                        // Room 2: 
                        AccommodationProviderAvailabilityResult secondRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[1], results: results,
                                                                                                        matchContract: false, contracts: new List<string>(),
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(secondRoomResult, string.Format("Room 2: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(secondRoomResult);

                        // Room 3: 
                        AccommodationProviderAvailabilityResult thirdRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[2], results: results,
                                                                                                        matchContract: false, contracts: new List<string>() { },
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: false, roomCodes: new List<string>() { firstRoomResult.RoomCode });
                        Assert.IsNotNull(thirdRoomResult, string.Format("Room 3: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(thirdRoomResult);

                        // Room 4: 
                        AccommodationProviderAvailabilityResult fourthRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[3], results: results,
                                                                                                        matchContract: true, contracts: new List<string>() { secondRoomResult.Contract },
                                                                                                        matchEstablishment: true, establishmentEdiCodes: new List<string>() { firstRoomResult.EstablishmentEdiCode },
                                                                                                        matchRoomCode: true, roomCodes: new List<string>() { secondRoomResult.RoomCode });
                        Assert.IsNotNull(fourthRoomResult, string.Format("Room 4: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(fourthRoomResult);

                        // Room 5: 
                        AccommodationProviderAvailabilityResult fifthRoomResult = GetAvailabilityRoom(availabilityRequestRoom: availabilityRequest.Rooms[4], results: results,
                                                                                                        matchContract: false, contracts: new List<string>() { firstRoomResult.Contract, secondRoomResult.Contract },
                                                                                                        matchEstablishment: false, establishmentEdiCodes: new List<string>(),
                                                                                                        matchRoomCode: false, roomCodes: new List<string>());
                        Assert.IsNotNull(fifthRoomResult, string.Format("Room 5: Unable to identify an availability result for destination {0} - test cannot continue.", destinationEdiCode));
                        selectedRooms.Add(fifthRoomResult);

                        counter++;

                    } while (selectedRooms.Count(room => room.PaymentModel == PaymentModel.CustomerPayDirect) < 2 && counter < 10);


                    // For debugging
                    List<string> serialisedSelectedRooms = new List<string>();

                    foreach (var selectedRoom in selectedRooms)
                    {
                        TestAvailabilityResult testAvailabilityResult = HotelBedsMapper.CloneAvailabilityResult(selectedRoom);
                        string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                        serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    }


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));


                    // 3. BOOKING
                    // Need to create a series of AccommodationProviderBookingRequestRoom objects, one per valued room
                    List<AccommodationProviderBookingRequestRoom> bookingRequestRooms = CreateBookingRequestRooms(valuationRequest, valuationResults);

                    // Create the Booking Request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(provider, channel, bookingRequestRooms, valuationRequest);

                    // Create the Booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("HotelBeds");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResults = bookResults.ToList();

                    // Run Booking Assertions
                    if (bookResults != null && bookResults.Any())
                    {
                        foreach (var bookingResult in bookingResults)
                        {
                            RunBookingAssertions(bookingResult);
                        }
                    }


                    // 4. CANCELLATION
                    // Process each result in turn, and cancel the booking
                    foreach (var bookingResult in bookingResults)
                    {
                        // Create the cancellation request
                        AccommodationProviderCancellationRequest cancellationRequest = CreateCancellationRequest(bookingRequest, bookingResult);

                        // Create the Cancellation Provider
                        IAccommodationCancellationProvider cancellationProvider = NinjectInstaller.Kernel.Get<IAccommodationCancellationProvider>("HotelBeds");

                        // Get valuation result from Provider
                        IEnumerable<AccommodationProviderCancellationResult> cancelResults = await cancellationProvider.CancelProviderBookingAsync(cancellationRequest);
                        List<AccommodationProviderCancellationResult> cancellationResults = cancelResults.ToList();

                        Assert.IsTrue(cancellationResults.Count >= 1, string.Format("No cancellation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                        // Run Cancellation Assertions
                        if (cancellationResults != null && cancellationResults.Any())
                        {
                            RunCancellationAssertions(cancellationResults.First());
                        }
                    }

                    await Task.Delay(500);
                }
                catch (Exception ex)
                {
                    // Specific errors may be encapsulated by the SupplierApiException class, and therefore it is necessary to check the error message to see what the exact error is.
                    if (ex.Message.Contains("Error getting object from pool"))
                    {
                        // Internal HotelBeds Error - cannot be corrected at our end. Ignore error - do nothing and continue to next destination in loop.
                        errorHotelHeavyCount++;
                        sb.AppendLine(string.Format("Destination: {0}. HotelBeds Cache Retrieval Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("No availability results returned"))
                    {
                        // Do nothing -> continue to next destination in loop.
                        errorNoAvailabilityCount++;
                        sb.AppendLine(string.Format("Destination: {0}. No availability results returned.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("(500) Internal Server Error"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        error500Count++;
                        sb.AppendLine(string.Format("Destination: {0}. (500) Internal Server Error.", destinationEdiCode));
                    }
                    else if (ex.Message.Contains("The underlying connection was closed"))
                    {
                        // To capture "The remote server returned an error: (500) Internal Server Error."  Ignore error - do nothing and continue to next destination in loop.
                        errorConnectionClosedCount++;
                        sb.AppendLine(string.Format("Destination: {0}. The underlying connection was closed. The connection was closed unexpectedly.", destinationEdiCode));
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            Assert.True(error500Count == 0 && errorHotelHeavyCount == 0 && errorNoAvailabilityCount == 0 && errorConnectionClosedCount == 0, sb.ToString());
        }

    }
}
