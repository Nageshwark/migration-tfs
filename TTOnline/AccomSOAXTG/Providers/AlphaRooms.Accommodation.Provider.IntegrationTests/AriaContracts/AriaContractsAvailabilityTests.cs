﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.AriaContracts;
using AlphaRooms.Configuration;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.IntegrationTests.AriaContracts
{
    public class AriaContractsAvailabilityTests
    {
        private Random randomGenerator;

        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
            
            randomGenerator = new Random();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]
        public async Task SearchByDestination_NoChildren_WillReturnAvailability()
        {
            Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
            DateTime checkInDate = new DateTime(2015,11,15); //GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(90));
            DateTime checkOutDate = checkInDate.AddDays(7);
            int numberAdults = 2;
            byte[] childAges = new byte[0];

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[0];
            var rooms = new AccommodationProviderAvailabilityRequestRoom[] { CreateProviderRequestRoom(1, numberAdults, childAges) };

            // create provider request
            AccommodationProviderAvailabilityRequest providerRequest = CreateProviderRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);

            //Create DirectProvider
            IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Aria Contracts");

            // process avauilability
            IEnumerable<AccommodationProviderAvailabilityResult> availabilityResults = await availabilityProvider.GetProviderAvailabilityAsync(providerRequest);

            //Assertions

            //1. Results not equal to null
            Assert.IsNotNull(availabilityResults, "The availability results is null.");
            Assert.IsTrue(availabilityResults.Any(), "Test failed. No results returned.");
        }
       

        [Test]
        public async Task SearchByDestinationAndEstablishment_NoChildren_WillReturnAvailability()
        {
            try
            {
                Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
                Guid establishmentId = new Guid("74677872-1ea1-4f11-9e7e-4b710c179316"); //Pyr Marbella
                DateTime checkInDate = new DateTime(2015,08,15); //GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(90));
                DateTime checkOutDate = checkInDate.AddDays(7);
                int numberAdults = 2;
                byte[] childAges = new byte[0];

                var channelInfo = new ChannelInfo()
                {
                    Channel = Channel.AlphaRoomsUK
                };

                var provider = new AccommodationProvider()
                {
                    Id = 1,
                    EdiCode = "A",
                    Name = "Aria Contracts",
                    IsActive = true
                };

                var destinationCodes = new string[] { destinationId.ToString() };
                var establishmentCodes = new string[] { establishmentId .ToString() };
                var rooms = new AccommodationProviderAvailabilityRequestRoom[] { CreateProviderRequestRoom(1, numberAdults, childAges) };

                // create provider request
                AccommodationProviderAvailabilityRequest providerRequest = CreateProviderRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);

                //Create Provider
                IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Aria Contracts");

                // process avauilability
                IEnumerable<AccommodationProviderAvailabilityResult> availabilityResults = await availabilityProvider.GetProviderAvailabilityAsync(providerRequest);

                //Assertions

                //1. Results not equal to null
                Assert.IsNotNull(availabilityResults, "The availability results is null.");
                Assert.IsTrue(availabilityResults.Count() > 0, "Test failed. No results returned.");
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task SearchByDestination_WithChildren_WillReturnAvailability()
        {
            Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
            DateTime checkInDate = new DateTime(2015,08,15); //GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(90));
            DateTime checkOutDate = checkInDate.AddDays(7);
            int numberAdults = 2;
            byte[] childAges = new byte[] { 6 };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[0];
            var rooms = new AccommodationProviderAvailabilityRequestRoom[] { CreateProviderRequestRoom(1, numberAdults, childAges) };

            // create provider request
            AccommodationProviderAvailabilityRequest providerRequest = CreateProviderRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);

            //Create DirectProvider
            IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Aria Contracts");

            // process avauilability
            IEnumerable<AccommodationProviderAvailabilityResult> availabilityResults = await availabilityProvider.GetProviderAvailabilityAsync(providerRequest);

            //Assertions

            //1. Results not equal to null
            Assert.IsNotNull(availabilityResults, "The availability results is null.");
            Assert.IsTrue(availabilityResults.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task SearchByDestinationAndEstablishment_WithChildren_WillReturnAvailability()
        {
            try
            {
                Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
                Guid establishmentId = new Guid("74677872-1ea1-4f11-9e7e-4b710c179316"); //Pyr Marbella
                DateTime checkInDate = new DateTime(2015,08,15); //GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(90));
                DateTime checkOutDate = checkInDate.AddDays(7);
                int numberAdults = 2;
                byte[] childAges = new byte[] { 6 };

                var channelInfo = new ChannelInfo()
                {
                    Channel = Channel.AlphaRoomsUK
                };

                var provider = new AccommodationProvider()
                {
                    Id = 1,
                    EdiCode = "A",
                    Name = "Aria Contracts",
                    IsActive = true
                };

                var destinationCodes = new string[] { destinationId.ToString() };
                var establishmentCodes = new string[] { establishmentId.ToString() };
                var rooms = new AccommodationProviderAvailabilityRequestRoom[] { CreateProviderRequestRoom(1, numberAdults, childAges) };

                // create provider request
                AccommodationProviderAvailabilityRequest providerRequest = CreateProviderRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);

                //Create DirectProvider
                IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Aria Contracts");

                // process avauilability
                IEnumerable<AccommodationProviderAvailabilityResult> availabilityResults = await availabilityProvider.GetProviderAvailabilityAsync(providerRequest);

                //Assertions

                //1. Results not equal to null
                Assert.IsNotNull(availabilityResults, "The availability results is null.");
                Assert.IsTrue(availabilityResults.Count() > 0, "Test failed. No results returned.");
            }

            catch (Exception ex)
            {
                throw;
            }
        }


        private AccommodationProviderAvailabilityRequest CreateProviderRequest(AccommodationProvider provider, ChannelInfo channelInfo, string[] destinationCodes, string[] establishmentCodes
           , DateTime checkInDate, DateTime checkOutDate, AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            return new AccommodationProviderAvailabilityRequest()
            {
                AvailabilityId = Guid.NewGuid()
                ,
                SearchType = SearchType.HotelOnly
                ,
                Provider = provider
                ,
                ChannelInfo = channelInfo
                ,
                DestinationCodes = destinationCodes
                ,
                EstablishmentCodes = establishmentCodes
                ,
                CheckInDate = checkInDate
                ,
                CheckOutDate = checkOutDate
                ,
                Rooms = rooms
            };
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();
            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }
            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }
            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid()
                , RoomNumber = roomNumber
                , Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = randomGenerator.Next(0, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }
    }
}
