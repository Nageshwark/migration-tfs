﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.AriaContracts
{
    public class AriaContractsBookingTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]
        public async Task SingleRoomBooking_Successful()
        {
            //Create Provider

            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                EstablishmentName = "Melia Marbella Banus",
                DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                CheckInDate = new DateTime(2015, 10, 10),
                CheckOutDate = new DateTime(2015, 10, 15),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                RoomDescription = "Studio",
                CostPrice = new Money((decimal)1334.40, "EUR"),
                SalePrice = new Money((decimal)1668.00, "EUR"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                ContactNumber = "123456789",
                Age = 30,
                EmailAddress = "TestA@test.com",
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                Customer = customer,
                ItineraryId = 12345,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1
                    , ValuationResult = valuationResult
                    , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 })
                    , PaymentDetails = cardDetails
                } }
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            //Assert.IsNotNull(bookingResult, "The valuation result is null.");
            //Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task MultiRoomBooking_Successful()
        {
            //Create Provider

            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var room1 = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                EstablishmentName = "Melia Marbella Banus",
                DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                CheckInDate = new DateTime(2015, 10, 10),
                CheckOutDate = new DateTime(2015, 10, 15),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                RoomDescription = "Studio",
                CostPrice = new Money((decimal)1334.40, "EUR"),
                SalePrice = new Money((decimal)1668.00, "EUR"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var room2 = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                EstablishmentName = "Melia Marbella Banus",
                DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                CheckInDate = new DateTime(2015, 10, 10),
                CheckOutDate = new DateTime(2015, 10, 15),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                RoomDescription = "Double",
                CostPrice = new Money((decimal)1450.40, "EUR"),
                SalePrice = new Money((decimal)1775.00, "EUR"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(5, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1"
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                ValuatedRooms = new[] { new AccommodationProviderBookingRequestRoom()
                {
                    Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new []{ guest1, guest2 })
                    , PaymentDetails = cardDetails
                    , RoomNumber = 1
                    , ValuationResult = room1
                }
                , new AccommodationProviderBookingRequestRoom()
                {
                    PaymentDetails = cardDetails
                    , RoomNumber = 2
                    , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new []{ guest1, guest2 })
                    , ValuationResult = room2
                }}
                ,
                ItineraryId = 12345
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            //Assert.IsNotNull(bookingResult, "The valuation result is null.");
            //Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task SingleRoomBooking_SiteMinderSME_Successful()
        {
            //Create Provider

            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true,
                Parameters = new List<AccommodationProviderParameter>
                {
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "TestPassword"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "TestPassword"}
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "AlphaRoomsEMEA"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "iHT1q4E5Phy0euy"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "AlphaRoomsAPAC"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "19Us5khSGSE6J8x"}
                }
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK,
                Site = Sites.AlphaRooms
            };

            var destinationEdiCode = "";
            string[] destinationEdiCodes = new string[] { destinationEdiCode };

            var establishmentEdiCode = "a251ee88-b062-460a-826b-51e9fd9efa5f"; // Liberty Apts
            string[] establishmentEdiCodes = new string[] { establishmentEdiCode };

            var checkInDate = new DateTime(2016, 01, 30);
            var checkOutDate = new DateTime(2016, 01, 31);

            List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
            AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
            rooms.Add(room1);

            var availabilityRequest = CreateAvailabilityRequest(provider, SearchType.HotelOnly, Channel.AlphaRoomsUK,
                checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes,
                rooms.ToArray());

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                ProviderName = "LIBERTY APTS",
                EstablishmentEdiCode = establishmentEdiCode,
                EstablishmentName = "Liberty Apartments By Bridge Street Worldwide",
                DestinationEdiCode = destinationEdiCode,
                CheckInDate = checkInDate,
                CheckOutDate = checkOutDate,
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "3F5365D4-56FE-4E01-B38D-051F787B799A", // SiteMinder - SME
                RoomDescription = "Studio",
                CostPrice = new Money((decimal)94.30, "GBP"),
                SalePrice = new Money((decimal)115.00, "GBP"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                ContactNumber = "123456789",
                Age = 30,
                EmailAddress = "TestA@test.com",
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                Customer = customer,
                ItineraryId = 12345,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1,
                    AvailabilityRequest = availabilityRequest,
                    ValuationResult = valuationResult,
                    Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 }),
                    PaymentDetails = cardDetails
                }}
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1.Results not equal to null
            Assert.IsNotNull(bookingResult, "The valuation result is null.");
            Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task MultiRoomBooking_SiteMinderSME_Successful()
        {
            //Create Provider

            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true,
                Parameters = new List<AccommodationProviderParameter>
                {
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "TestPassword"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "TestPassword"}
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "AlphaRoomsEMEA"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "iHT1q4E5Phy0euy"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "AlphaRoomsAPAC"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "19Us5khSGSE6J8x"}
                }
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK,
                Site = Sites.AlphaRooms
            };

            var destinationEdiCode = "";
            string[] destinationEdiCodes = new string[] { destinationEdiCode };

            var establishmentEdiCode = "a251ee88-b062-460a-826b-51e9fd9efa5f"; // Liberty Apts
            string[] establishmentEdiCodes = new string[] { establishmentEdiCode };

            var checkInDate = new DateTime(2016, 01, 30);
            var checkOutDate = new DateTime(2016, 01, 31);

            List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
            AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
            AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 1, new byte[] { });
            rooms.Add(room1);
            rooms.Add(room2);

            var availabilityRequest = CreateAvailabilityRequest(provider, SearchType.HotelOnly, Channel.AlphaRoomsUK,
                checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes,
                rooms.ToArray());

            var valuationResult1 = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                ProviderName = "LIBERTY APTS",
                EstablishmentEdiCode = establishmentEdiCode,
                EstablishmentName = "Liberty Apartments By Bridge Street Worldwide",
                DestinationEdiCode = destinationEdiCode,
                CheckInDate = checkInDate,
                CheckOutDate = checkOutDate,
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "3F5365D4-56FE-4E01-B38D-051F787B799A", // SiteMinder - SME
                RoomDescription = "1-Bed Town Side",
                CostPrice = new Money((decimal)94.30, "GBP"),
                SalePrice = new Money((decimal)115.00, "GBP"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var valuationResult2 = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                ProviderName = "LIBERTY APTS",
                EstablishmentEdiCode = establishmentEdiCode,
                EstablishmentName = "Liberty Apartments By Bridge Street Worldwide",
                DestinationEdiCode = destinationEdiCode,
                CheckInDate = checkInDate,
                CheckOutDate = checkOutDate,
                Adults = 1,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "5CF09FA8-912B-437E-BDF8-8153623E0765", // SiteMinder - SME
                RoomDescription = "1-Bed Town Side Deluxe",
                CostPrice = new Money((decimal)85.00, "GBP"),
                SalePrice = new Money((decimal)100.00, "GBP"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                ContactNumber = "123456789",
                Age = 30,
                EmailAddress = "TestA@test.com",
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest3 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestC",
                Surname = "TestC",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
            };

            var valuatedRooms = new[]
            {
                new AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1,
                    AvailabilityRequest = availabilityRequest,
                    ValuationResult = valuationResult1,
                    Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] {guest1, guest2}),
                    PaymentDetails = cardDetails
                },

                new AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 2,
                    AvailabilityRequest = availabilityRequest,
                    ValuationResult = valuationResult2,
                    Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] {guest3}),
                    PaymentDetails = cardDetails
                }
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                Customer = customer,
                ItineraryId = 12345,
                ValuatedRooms = valuatedRooms
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1.Results not equal to null
            Assert.IsNotNull(bookingResult, "The valuation result is null.");
            Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task SingleRoomBooking_WithChildren_SiteMinderSME_Successful()
        {
            //Create Provider
            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true,
                Parameters = new List<AccommodationProviderParameter>
                {
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "TestPassword"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "TestPassword"}
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "AlphaRoomsEMEA"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "iHT1q4E5Phy0euy"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "AlphaRoomsAPAC"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "19Us5khSGSE6J8x"}
                }
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK,
                Site = Sites.AlphaRooms
            };

            var destinationEdiCode = "";
            string[] destinationEdiCodes = new string[] { destinationEdiCode };

            var establishmentEdiCode = "a251ee88-b062-460a-826b-51e9fd9efa5f"; // Liberty Apts
            string[] establishmentEdiCodes = new string[] { establishmentEdiCode };

            var checkInDate = new DateTime(2016, 01, 30);
            var checkOutDate = new DateTime(2016, 01, 31);

            List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
            AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
            rooms.Add(room1);

            var availabilityRequest = CreateAvailabilityRequest(provider, SearchType.HotelOnly, Channel.AlphaRoomsUK,
                checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes,
                rooms.ToArray());

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                ProviderName = "LIBERTY APTS",
                EstablishmentEdiCode = establishmentEdiCode,
                EstablishmentName = "Liberty Apartments By Bridge Street Worldwide",
                DestinationEdiCode = destinationEdiCode,
                CheckInDate = checkInDate,
                CheckOutDate = checkOutDate,
                Adults = 2,
                Children = 1,
                Infants = 1,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "3F5365D4-56FE-4E01-B38D-051F787B799A", // SiteMinder - SME
                RoomDescription = "1-Bed Town Side",
                CostPrice = new Money((decimal)94.30, "GBP"),
                SalePrice = new Money((decimal)115.00, "GBP"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                ContactNumber = "123456789",
                Age = 30,
                EmailAddress = "TestA@test.com",
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest3 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestC",
                Surname = "TestC",
                Age = 3,
                Type = GuestType.Child
            };

            var guest4 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestD",
                Surname = "TestD",
                Age = 1,
                Type = GuestType.Infant
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                Customer = customer,
                ItineraryId = 12345,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1,
                    AvailabilityRequest = availabilityRequest,
                    ValuationResult = valuationResult,
                    Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2, guest3, guest4 }),
                    PaymentDetails = cardDetails
                }}
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1.Results not equal to null
            Assert.IsNotNull(bookingResult, "The valuation result is null.");
            Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        //[Test]
        //public async Task SingleRoomBooking_SiteMinderSM_Successful() // TODO : need to find a hotelcode with available results
        //{
        //    //Create Provider

        //    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

        //    var provider = new AccommodationProvider()
        //    {
        //        Id = 1,
        //        EdiCode = "A",
        //        Name = "Aria Contracts",
        //        IsActive = true,
        //        Parameters = new List<AccommodationProviderParameter>
        //        {
        //            new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "TestUser"},
        //            new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "TestPassword"},
        //            new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "TestUser"},
        //            new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "TestPassword"}
        //            //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "AlphaRoomsEMEA"},
        //            //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "iHT1q4E5Phy0euy"},
        //            //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "AlphaRoomsAPAC"},
        //            //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "19Us5khSGSE6J8x"}
        //        }
        //    };

        //    var channelInfo = new ChannelInfo()
        //    {
        //        Channel = Channel.AlphaRoomsUK
        //    };

        //    var valuationResult = new AccommodationProviderValuationResult()
        //    {
        //        ProviderEdiCode = "A",
        //        EstablishmentEdiCode = "", 
        //        EstablishmentName = "",
        //        DestinationEdiCode = "",
        //        CheckInDate = new DateTime(2016, 01, 30),
        //        CheckOutDate = new DateTime(2016, 01, 31),
        //        Adults = 2,
        //        Children = 0,
        //        BoardCode = "RO",
        //        BoardDescription = "ROOM ONLY",
        //        RoomCode = "3F5365D4-56FE-4E01-B38D-051F787B799A", // SiteMinder - SME
        //        RoomDescription = "Studio",
        //        CostPrice = new Money((decimal)94.30, "GBP"),
        //        SalePrice = new Money((decimal)115.00, "GBP"),
        //        PaymentModel = PaymentModel.PostPayment,
        //        ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
        //    };

        //    var customer = new AccommodationProviderBookingRequestCustomer()
        //    {
        //        TitleString = "Mr",
        //        FirstName = "TestA",
        //        Surname = "TestA",
        //        ContactNumber = "123456789",
        //        Age = 30,
        //        EmailAddress = "TestA@test.com",
        //        Type = GuestType.Adult
        //    };

        //    var guest1 = new AccommodationProviderBookingRequestRoomGuest()
        //    {
        //        TitleString = "Mr",
        //        FirstName = "TestA",
        //        Surname = "TestA",
        //        Age = 30,
        //        Type = GuestType.Adult
        //    };

        //    var guest2 = new AccommodationProviderBookingRequestRoomGuest()
        //    {
        //        TitleString = "Mrs",
        //        FirstName = "TestB",
        //        Surname = "TestB",
        //        Age = 30,
        //        Type = GuestType.Adult
        //    };

        //    var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
        //    {
        //        CardType = CardType.MasterCard,
        //        CardNumber = "123456789",
        //        CardExpireDate = new MonthYear(05, 2016),
        //        CardHolderName = "TestA TestA",
        //        CardSecurityCode = "123",
        //        AddressLine1 = "1",
        //        AddressLine2 = "Test",
        //        City = "London",
        //        Postcode = "SW1",
        //    };

        //    //Create AccommodationProviderBookingRequest

        //    var providerBookingRequest = new AccommodationProviderBookingRequest()
        //    {
        //        Provider = provider,
        //        ChannelInfo = channelInfo,
        //        Customer = customer,
        //        ItineraryId = 12345,
        //        ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
        //        {
        //            RoomNumber = 1
        //            , ValuationResult = valuationResult
        //            , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 })
        //            , PaymentDetails = cardDetails
        //        } }
        //    };

        //    var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

        //    //1. Results not equal to null
        //    //Assert.IsNotNull(bookingResult, "The valuation result is null.");
        //    //Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        //}

        [Test]
        public async Task SingleRoomBooking_SiteMinderAPACE_Successful()
        {
            //Create Provider

            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Aria Contracts");

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true,
                Parameters = new List<AccommodationProviderParameter>
                {
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "TestPassword"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "TestUser"},
                    new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "TestPassword"}
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAUser", ParameterValue = "AlphaRoomsEMEA"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderEMEAPassword", ParameterValue = "iHT1q4E5Phy0euy"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACUser", ParameterValue = "AlphaRoomsAPAC"},
                    //new AccommodationProviderParameter{ProviderId = 1, ParameterName = "SiteMinderAPACPassword", ParameterValue = "19Us5khSGSE6J8x"}
                }
            };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "A",
                EstablishmentEdiCode = "4e1f2635-e1f3-49dd-bc7c-3293274e2dec",
                EstablishmentName = "Clarion Suites Gateway",
                DestinationEdiCode = "",
                CheckInDate = new DateTime(2016, 03, 01),
                CheckOutDate = new DateTime(2016, 03, 02),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "ROOM ONLY",
                RoomCode = "FF31BCBF-5322-42FA-9128-03205CD7AC16", // SiteMinder - APACE
                RoomDescription = "Studio",
                CostPrice = new Money((decimal)94.30, "USD"),
                SalePrice = new Money((decimal)115.00, "USD"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }


            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                ContactNumber = "123456789",
                Age = 30,
                EmailAddress = "TestA@test.com",
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mr",
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                TitleString = "Mrs",
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "123456789",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "TestA TestA",
                CardSecurityCode = "123",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                ChannelInfo = channelInfo,
                Customer = customer,
                ItineraryId = 12345,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1
                    , ValuationResult = valuationResult
                    , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 })
                    , PaymentDetails = cardDetails
                } }
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            //Assert.IsNotNull(bookingResult, "The valuation result is null.");
            //Assert.IsTrue(bookingResult.Count() > 0, "Test failed. No results returned.");
        }

        //[Test]
        //public async Task SingleRoomBooking_WithChildren_Successful()
        //{
        //    //TODO Booking request need to be formated in order to include the adults and children in each room.
        //}

        //[Test]
        //public async Task MultiRoomBooking_WithChildren_Successful()
        //{
        //    //TODO Booking request need to be formated in order to include the adults and children in each room.
        //}

        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                   SearchType searchType,
                                                                                   Channel channel,
                                                                                   DateTime checkInDate,
                                                                                   DateTime checkOutDate,
                                                                                   string[] destinationEdiCodes,
                                                                                   string[] establishmentEdiCodes,
                                                                                   AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }
    }
}
