﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;

namespace AlphaRooms.Accommodation.IntegrationTests.AriaContracts
{
    public class AriaContractsValuationTests
    {
        private Random randomGenerator;

        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();

            randomGenerator = new Random();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]
        public async Task Valuation_WillReturnAvailability()
        {
            //Create Provider
            IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Aria Contracts");

            Guid destinationId = new Guid("B4A276B8-BBD6-4744-B287-B5EF7795A4DF"); //Tenerife
            Guid establishmentId = new Guid("043A4B49-0789-47C4-AA83-11549C6747F4"); //Blue Sea Hotel Puerto Resort
            var roomDescription = "Superior Double / Twin Room";
            DateTime checkInDate = new DateTime(2016, 01, 01);
            DateTime checkOutDate = checkInDate.AddDays(7);
            int numberAdults = 2;
            int[] childAges = new int[] { 2 };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[] { establishmentId.ToString() };

            AccommodationProviderValuationRequest request = CreateValuationRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, roomDescription);

            var valuationResult = await valuationProvider.GetProviderValuationAsync(request);

            //1. Results not equal to null
            Assert.IsNotNull(valuationResult, "The valuation result is null.");
            Assert.IsTrue(valuationResult.Count() > 0, "Test failed. No results returned.");
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProvider provider, ChannelInfo channelInfo, string[] destinationCodes, string[] establishmentCodes
           , DateTime checkInDate, DateTime checkOutDate, string roomDescription)
        {
            var rooms = new AccommodationProviderAvailabilityRequestRoom[] { CreateProviderRequestRoom() };

            // create provider availability request
            AccommodationProviderAvailabilityRequest providerAvailabilityRequest = CreateProviderAvailabilityRequest(provider, channelInfo, destinationCodes, establishmentCodes, rooms, checkInDate, checkOutDate);
            AccommodationProviderValuationRequestRoom[] providerAvailabilityResults = CreateProviderAvailabilityResults(roomDescription, providerAvailabilityRequest, checkInDate, checkOutDate).ToArray();
           
            return new AccommodationProviderValuationRequest()
            {
                ValuationId = Guid.NewGuid(), 
                SelectedRooms = providerAvailabilityResults,
                Provider = provider
            };
        }

        private List<AccommodationProviderValuationRequestRoom> CreateProviderAvailabilityResults(string roomDescription, AccommodationProviderAvailabilityRequest providerAvailabilityRequest, DateTime checkInDate, DateTime checkOutDate)
        {
           var v = new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityResult = new AccommodationProviderAvailabilityResult()
                {
                    //Id = Guid.NewGuid(),
                    ProviderEdiCode = "A",
                    EstablishmentEdiCode = "043A4B49-0789-47C4-AA83-11549C6747F4",
                    DestinationEdiCode = "B4A276B8-BBD6-4744-B287-B5EF7795A4DF",
                    CheckInDate = checkInDate,
                    CheckOutDate = checkOutDate,
                    Adults = 2,
                    Children = 1,
                    BoardCode = "HB",
                    BoardDescription = "HALF BOARD",
                    RoomCode = "592D15F4-AD66-4196-B6DB-1779C3317D08",
                    RoomDescription = roomDescription,
                    CostPrice = new Money((decimal)1334.40, "EUR"),
                    SalePrice = new Money((decimal)1668.00, "EUR"),
                    PaymentModel = PaymentModel.PostPayment,
                    ProviderSpecificData = new Dictionary<string, string>() { { "CancellationTerms", "Cancellation policy" } }
                }
                , AvailabilityRequest = providerAvailabilityRequest
                , RoomNumber = 1
                , Guests = providerAvailabilityRequest.Rooms.First().Guests
            };

            var availabilityResults = new List<AccommodationProviderValuationRequestRoom> {v};
            
            return availabilityResults;

        }

        private AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProvider provider, ChannelInfo channelInfo, string[] destinationCodes, string[] establishmentCodes
         , AccommodationProviderAvailabilityRequestRoom[] rooms, DateTime checkInDate, DateTime checkOutDate)
        {
            return new AccommodationProviderAvailabilityRequest()
            {
                AvailabilityId = Guid.NewGuid()
                ,
                SearchType = SearchType.HotelOnly
                ,
                Provider = provider
                ,
                ChannelInfo = channelInfo
                ,
                DestinationCodes = destinationCodes
                ,
                EstablishmentCodes = establishmentCodes
                ,
                CheckInDate = checkInDate
                ,
                CheckOutDate = checkOutDate
                ,
                
                Rooms = rooms
            };
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom()
        {
            //var guestRequestList = new List<AccommodationProviderAvailabilityGuestRequest>();

            var guestRequestAdult1 = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 25,
                Type = GuestType.Adult
            };

            var guestRequestAdult2 = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 25,
                Type = GuestType.Adult
            };

            var guestRequestChild = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 2,
                Type = GuestType.Child
            };

            var guestRequestList = new List<AccommodationProviderAvailabilityRequestGuest>() { guestRequestAdult1, guestRequestAdult2, guestRequestChild };

            var guests = new AccommodationProviderAvailabilityRequestGuestCollection(guestRequestList);

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid()
                ,
                RoomNumber = 1
                ,
                Guests = guests
            };
        }

        [Test]
        public async Task Valuation_WillNotReturnAvailability()
        {
            //Create Provider
            IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Aria Contracts");

            Guid destinationId = new Guid("B4A276B8-BBD6-4744-B287-B5EF7795A4DF"); //Tenerife
            Guid establishmentId = new Guid("1E99BB4D-438B-4846-8862-363EE7CEB7D0");
            var roomDescription = "Executive Suite";
            DateTime checkInDate = new DateTime(2015, 12, 16);
            DateTime checkOutDate = checkInDate.AddDays(7);
            int numberAdults = 2;
            int[] childAges = new int[] { 6 };

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = new AccommodationProvider()
            {
                Id = 1,
                EdiCode = "A",
                Name = "Aria Contracts",
                IsActive = true
            };

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[] { establishmentId.ToString() };

            AccommodationProviderValuationRequest request = CreateValuationRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, roomDescription);

            var valuationResult = await valuationProvider.GetProviderValuationAsync(request);

            //we are throwing exception in the parser.
        }

    }
}
