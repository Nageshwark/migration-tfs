﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.IntegrationTests.HotelBeds;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.MagicRoom
{

    class Provider_MagicRoom_ValuationTests
    {
        private Random random;

        
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        [SetUp]
        public void Setup()
        {
            random = new Random();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        protected  AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 4;
            accommodationProvider.EdiCode = "D";
            accommodationProvider.Name = "MagicRoom";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                
                new AccommodationProviderParameter { ProviderId = 4,	ParameterName = "MagicRoomUsername",			    ParameterValue = "Manager"},
                new AccommodationProviderParameter { ProviderId = 4,	ParameterName = "MagicRoomPassword",			    ParameterValue = "M4gicr00ms"},
                new AccommodationProviderParameter { ProviderId = 4,     ParameterName = "MagicRoomOrg",                     ParameterValue = "XMLDemo"},
                new AccommodationProviderParameter { ProviderId = 4,     ParameterName = "MagicRoomCurrency",                ParameterValue = "GBP"},
                new AccommodationProviderParameter { ProviderId = 4,	ParameterName = "MagicRoomAvailabilityUrl",		    ParameterValue = "http://integration.magicrooms.co.uk/MRIntegrationServices/ASMX/XMLService.asmx"}, 
                new AccommodationProviderParameter { ProviderId = 4,	ParameterName = "MaxReceivedMessageSize",		    ParameterValue = "20000000"},
                
            };


            return accommodationProvider;
        }



        

        
        [Test]
        public async Task MagicRoom_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "504" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 01);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("MagicRoom");//new MagicRoomAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(availabilityRequest, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("MagicRoom");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunValuationAssertions(selectedRoom, valuationResults.First());
                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(int roomNumber, int numberAdults, int[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = (byte)age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = (byte)roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProvider provider,
                                                                                   SearchType searchType,
                                                                                   Channel channel,
                                                                                   DateTime checkInDate,
                                                                                   DateTime checkOutDate,
                                                                                   string[] destinationEdiCodes,
                                                                                   string[] establishmentEdiCodes,
                                                                                   AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            //Probably need to set the AccommodationProviderAvailabilityResult
            AccommodationProviderValuationRequest request = new AccommodationProviderValuationRequest();
            request.ValuationId = Guid.NewGuid();
            request.Debugging = true;
            var availabilityRequest = new AccommodationProviderAvailabilityRequest();
            availabilityRequest.Provider = provider;

            availabilityRequest.AvailabilityId = Guid.NewGuid();
            availabilityRequest.ChannelInfo = new ChannelInfo() { Channel = channel };

            availabilityRequest.SearchType = searchType;
            availabilityRequest.Debugging = true;

            availabilityRequest.CheckInDate = checkInDate;
            availabilityRequest.CheckOutDate = checkOutDate;

            availabilityRequest.DestinationCodes = destinationEdiCodes;
            availabilityRequest.EstablishmentCodes = establishmentEdiCodes;
            request.SelectedRooms = rooms.Select(i => new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                ,
                RoomNumber = i.RoomNumber
                ,
                Guests = i.Guests
                //TODO , AvailabilityResult                
            }).ToArray();

            return request;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
        
                                                                        AccommodationProviderAvailabilityResult selectedRoom)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;
            valuationRequest.SelectedRooms = new List<AccommodationProviderValuationRequestRoom>() { new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                , RoomNumber = availabilityRequest.Rooms.First().RoomNumber
                , Guests = availabilityRequest.Rooms.First().Guests
                , AvailabilityResult = selectedRoom
            }}.ToArray();

            return valuationRequest;
        }

        
        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {
            

            TestValuationResult testValuationResult =  MagicRoomMapper.CloneValuationResult(valuationResult);
            string serialisedValuation = ""; //testValuationResult.XmlSerialize();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.Amount == selectedRoom.CostPrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.Amount == selectedRoom.SalePrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        private void RunAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode,
            List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom =
                    results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false,
                    "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren =
                    resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);
                Assert.IsTrue(hasWrongNumberOfChildren == false,
                    "At least one room result has the wrong number of children.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false,
                "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<int> validRoomIds = request.Rooms.Select(room => (int)room.RoomNumber).ToList();
            foreach (int roomId in validRoomIds)
            {
                int count = results.Count(room => room.RoomNumber == roomId);
                Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            }

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false,
                "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }
    }

    public class TestValuationResult
    {
        public Guid Id { get; set; }
        public int RoomNumber { get; set; }

        public string ProviderName { get; set; }
        public string ProviderEdiCode { get; set; }

        public string DestinationEdiCode { get; set; }

        public string EstablishmentEdiCode { get; set; }
        public string EstablishmentName { get; set; }

        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public int NumberOfInfants { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public string BoardCode { get; set; }
        public string BoardDescription { get; set; }

        public string RoomCode { get; set; }
        public string RoomDescription { get; set; }

        public Money SalePrice { get; set; }
        public Money CostPrice { get; set; }

        public decimal CommissionPercent { get; set; }
        public Money CommissionAmount { get; set; }

        public PaymentModel PaymentModel { get; set; }

        public bool IsOpaqueRate { get; set; }
        public bool IsSpecialRate { get; set; }
        public bool? IsNonRefundableRate { get; set; }
        public bool IsBindingRate { get; set; }

        public RateType RateType { get; set; }

        public string CancellationPolicy { get; set; }

        public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }
    }

    public class MagicRoomMapper
    {
        static MagicRoomMapper()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationProviderValuationResult, TestValuationResult>();
            }
        }

        

        public static TestValuationResult CloneValuationResult(AccommodationProviderValuationResult valuationResult)
        {
            TestValuationResult testResult = Mapper.Map<AccommodationProviderValuationResult, TestValuationResult>(valuationResult);

            return testResult;
        }
    }
}
