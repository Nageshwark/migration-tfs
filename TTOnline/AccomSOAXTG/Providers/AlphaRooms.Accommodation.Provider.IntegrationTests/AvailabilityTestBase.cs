﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests
{
    public abstract class AvailabilityTestBase<T> where T: AccommodationAvailabilitySearchBase
    {
        private T provider;
        protected abstract T ConstructProvider();
        protected abstract AccommodationProvider ConfigureProvider();

        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }


        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.provider = this.ConstructProvider();
            
        }

        protected async Task TestAvailability(AccommodationProviderAvailabilityRequest request)
        {
            var results = await provider.GetProviderAvailabilityAsync(request);
        }

        protected AccommodationProviderAvailabilityRequest GetAvailabilityRequest(Channel channel, string currencyCode, DateTime checkinDtae, DateTime checkoutDate, List<AccommodationProviderAvailabilityRequestRoom> rooms, List<string> providerEstablishmentCodes, List<string> providerDestinationCodes)
        {
            var request = new AccommodationProviderAvailabilityRequest()
            {
                ChannelInfo = new ChannelInfo{Channel = channel, CurrencyCode = currencyCode},
                CheckInDate = checkinDtae, //DateTime.Now.AddMonths(3),
                CheckOutDate = checkoutDate, //DateTime.Now.AddMonths(3).AddDays(7),
                EstablishmentCodes = providerEstablishmentCodes.ToArray(),
                DestinationCodes = providerDestinationCodes.ToArray(),
                Rooms = rooms.ToArray(),
                Debugging = true
            };
            request.Provider =this.ConfigureProvider();
            return request;
        }

        protected AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(int roomNumber, int numberAdults, int[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = (byte)age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityKey = Guid.NewGuid().ToString(),
                RoomNumber = (byte)roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }
    }
}
