﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Accommodation.Provider.Hotels4U.Factories;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Hotels4U
{
    public class Provider_Hotels4U_AvailabilityTests : AvailabilityIntegrationTestBase//<Hotels4UAvailabilityProvider>
    {

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        protected override AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 2;
            accommodationProvider.EdiCode = "B";
            accommodationProvider.Name = "Hotels4U";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes
            
            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsApiVersion",	        ParameterValue = "2013/12"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsDefaultLanguage",		ParameterValue = "ENG"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "Hotels4UUsername",			    ParameterValue = "userone"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "Hotels4UPassword",			    ParameterValue = "nicole1"},
                //new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "Hotels4UAvailabilityUrl",		ParameterValue = "http://ota.hotels4u.com/Dispatcher.asmx"}, 
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "Hotels4UAvailabilityUrl",		ParameterValue = "http://prelive-ota.h4u.me/Dispatcher.asmx"}, 
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "MaxReceivedMessageSize",		ParameterValue = "20000000"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsBookingUrl",			ParameterValue = "https://testconfirm.interface-xml.com/platform/http/FrontendService"},
                new AccommodationProviderParameter{ProviderId = 2,	ParameterName = "HotelBedsCancellationUrl",		ParameterValue = "http://testapi.interface-xml.com/appservices/http/FrontendService"},
            };

            return accommodationProvider;
        }

        [Test]
        public async Task Hotels4U_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "EGSSH" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2016, 03, 24);    
                    DateTime checkOutDate =  checkInDate.AddDays(5);      
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] {};

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);

                  

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    //IAccommodationAvailabilityAutomatorAsync<Hotels4UAvailabilityResponse> automator = new Hotels4UAvailabilityAutomator(new Hotels4UAvailabilityRequestFactory());
                    //IAccommodationAvailabilityParser<Hotels4UAvailabilityResponse > parser = new Hotels4UAvailabilityParser(null, null);
                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");//new Hotels4UAvailabilityProvider(automator, parser);
                    
                        
                    

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Hotels4U_Availability_ByDestination_1Room_WithChildAndInfant_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "EGSSH" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] {6, 1});
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");//new Hotels4UAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Hotels4U_Availability_ByDestination_2Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "EGSSH" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2016, 03, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);


                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new byte[] { });
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");//new Hotels4UAvailabilityProvider(automator, parser);



                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    //RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Hotels4U_Availability_ByEstablishment_1Room_WillReturnAvailability()
        {
           

            try
            {
                
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] {  };

                    string[] establishmentEdiCodes = new string[] { "H4U;10545" };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");//new Hotels4UAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", establishmentEdiCodes[0]));

                    //RunAssertions(request, destinationEdiCode, results);

               
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        
        public async Task Hotels4U_Availability_ByPlace_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "EGSSH" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

   IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");//new Hotels4UAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    //RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }


        protected override IAccommodationAvailabilityProvider ConstructProvider()
        {
            IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Hotels4U");

            return availabilityProvider;
        }

        private void RunAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode,
    List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom =
                    results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false,
                    "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren =
                    resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);
                Assert.IsTrue(hasWrongNumberOfChildren == false,
                    "At least one room result has the wrong number of children.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false,
                "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<int> validRoomIds = request.Rooms.Select(room => (int)room.RoomNumber).ToList();
            foreach (int roomId in validRoomIds)
            {
                int count = results.Count(room => room.RoomNumber == roomId);
                Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            }

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false,
                "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }
    }
}
