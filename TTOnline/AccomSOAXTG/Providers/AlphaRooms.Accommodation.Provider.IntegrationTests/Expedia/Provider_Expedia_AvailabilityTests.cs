﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
//using AlphaRooms.SOACommon.Interfaces;
using log4net.Repository;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Expedia
{
    public class Provider_Expedia_AvailabilityTests : AvailabilityTestBase<ExpediaAvailabilityProvider>
    {

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        protected override AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 7;
            accommodationProvider.EdiCode = "F";
            accommodationProvider.Name = "Expedia";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes
            
            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                /*
                
        
        private const string ExpediaCustomerIP = "ExpediaCustomerIP";
        private const string ExpediaUserAgent = "ExpediaUserAgent";
        private const string ExpediaSessionId = "ExpediaSessionId";
        
        
        
                */
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaAPIKey",             			    ParameterValue = "an2b9xv8uzjrhumchqpdqqeg"},
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaSecret",             			    ParameterValue = "H2RaHSnS"},
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaCID",             			        ParameterValue = "337847"},
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaLocale",             			    ParameterValue = "en_UK"},
                new AccommodationProviderParameter { ProviderId = 7,    ParameterName = "ExpediaCurrencyCode",                      ParameterValue = "GBP" },
                //new AccommodationProviderParameter { ProviderId = 7,    ParameterName = "ExpediaCustomerIP",                        ParameterValue = "127.0.0.1" },
                //new AccommodationProviderParameter { ProviderId = 7,    ParameterName = "ExpediaUserAgent",                         ParameterValue = "Google Chrome" },
                new AccommodationProviderParameter { ProviderId = 7,    ParameterName = "ExpediaSessionId",                         ParameterValue = Guid.NewGuid().ToString() },
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaMinorRevision",             		ParameterValue = "30"},
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaMaxNumberOfHotel",             		ParameterValue = "200"},

                
                new AccommodationProviderParameter { ProviderId = 7,	ParameterName = "ExpediaAvailabilityUrl",		                ParameterValue = "http://dev.api.ean.com/ean-services/rs/hotel/v3/list"},                 
            };

            if (ConfigurationManager.AppSettings["UseProxy"] != null)
            {
                bool useProxy = false;// Convert.ToBoolean(ConfigurationManager.AppSettings["UseProxy"]);
            
                accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 7, ParameterName = "UseProxy", ParameterValue = useProxy.ToString() });

                if (useProxy)
                {
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 7, ParameterName = "ProxyUrl",       ParameterValue = ConfigurationManager.AppSettings["ProxyUrl"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 7, ParameterName = "ProxyPort",      ParameterValue = ConfigurationManager.AppSettings["ProxyPort"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 7, ParameterName = "ProxyUsername",  ParameterValue = ConfigurationManager.AppSettings["ProxyUsername"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 7, ParameterName = "ProxyPassword",  ParameterValue = ConfigurationManager.AppSettings["ProxyPassword"] });                    
                }
            }

            return accommodationProvider;
        }

        [Test]
        public async Task Expedia_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            //"Seattle;WA;US" 
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "NL;213EBA87-5D1C-4694-B7C5-B08C3033BECA"};

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(3);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                   
                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Expedia");


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Expedia_Availability_ByDestination_1Room_WithChildAndInfant_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "NL;213EBA87-5D1C-4694-B7C5-B08C3033BECA" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(3);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] {6, 1});
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Expedia");//new ExpediaAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Expedia_Availability_ByDestination_2Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "NL;213EBA87-5D1C-4694-B7C5-B08C3033BECA" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(3);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new int[] { });
                    rooms.Add(room2);


                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Expedia");


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAssertions(request, destinationEdiCode, results);

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Expedia_Availability_ByEstablishment_1Room_WillReturnAvailability()
        {
           

            try
            {
                
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] {  };

                    string[] establishmentEdiCodes = new string[] { "115072", "112182", "132138" };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Expedia");//new ExpediaAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", establishmentEdiCodes[0]));

                    RunAssertions(request, establishmentEdiCodes[0], results);

               
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        
        
        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }


        protected override ExpediaAvailabilityProvider ConstructProvider()
        {
            //return NinjectInstaller.Kernel.Get<ExpediaAvailabilityProvider>();
            return null;
        }

        private void RunAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode,
            List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom =
                    results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false,
                    "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren =
                    resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount + room.Guests.InfantsCount);

                Assert.IsTrue(hasWrongNumberOfChildren == false,
                    "At least one room result has the wrong number of children.");

                // Number of Infant
                //bool hasWrongNumberOfInfant =
                //    resultsForRoom.Any(a => a.Infants != room.Guests.Infants);

                //Assert.IsTrue(hasWrongNumberOfInfant == false,
                //    "At least one room result has the wrong number of infant.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false,
                "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<int> validRoomIds = request.Rooms.Select(room => (int)room.RoomNumber).ToList();
            foreach (int roomId in validRoomIds)
            {
                int count = results.Count(room => room.RoomNumber == roomId);
                Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            }

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false,
                "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }
    }
}
