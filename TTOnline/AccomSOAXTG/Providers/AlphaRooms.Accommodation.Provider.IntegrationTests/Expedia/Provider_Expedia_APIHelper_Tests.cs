﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Expedia.Helper;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Expedia
{
    public class Provider_Expedia_APIHelper_Tests
    {
        [Test]
        public void Expedia_APIKey_WillReturnSignature()
        {
            APIKeyHelper helper = new APIKeyHelper();

            string key = "an2b9xv8uzjrhumchqpdqqeg";
            string secret = "H2RaHSnS";

            string sig = helper.GetSignature(key, secret);
            
            Console.WriteLine(sig);

            Assert.IsFalse(string.IsNullOrEmpty(sig));

        }
    }
}
