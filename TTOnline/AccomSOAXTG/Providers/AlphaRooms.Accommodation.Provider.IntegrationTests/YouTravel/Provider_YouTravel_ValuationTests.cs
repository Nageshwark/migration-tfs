﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.IntegrationTests.Hotels4U;
using AlphaRooms.Accommodation.Provider.Hotels4U.Factories;
using AlphaRooms.Accommodation.Provider.YouTravel;
using AlphaRooms.Accommodation.Provider.YouTravel.Factories;
using AlphaRooms.Accommodation.Provider.YouTravel;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
//using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using NUnit.Framework;
using AlphaRooms.Accommodation.Core.Provider.Services;

namespace AlphaRooms.Accommodation.IntegrationTests.YouTravel
{
    public class Provider_YouTravel_ValuationTests 
    {
        private Random random;


        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        [SetUp]
        public void Setup()
        {
            random = new Random();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }


        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        protected  AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 3;
            accommodationProvider.EdiCode = "C";
            accommodationProvider.Name = "YouTravel";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes
            
            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter{ProviderId = 4,	ParameterName = "YouTravelDefaultLanguage",		ParameterValue = "EN"},
                new AccommodationProviderParameter{ProviderId = 4,	ParameterName = "YouTravelUsername",			ParameterValue = "xmltestme"},
                new AccommodationProviderParameter{ProviderId = 4,	ParameterName = "YouTravelPassword",			ParameterValue = "testme"},
                new AccommodationProviderParameter{ProviderId = 4,  ParameterName = "YouTravelAvailabilityUrl",     ParameterValue = "http://testxml.youtravel.com/webservicestest/"},
                new AccommodationProviderParameter{ProviderId = 4,  ParameterName = "YouTravelBookingUrl",          ParameterValue = "http://testxml.youtravel.com/webservicestest/bookings.asp/"},
                new AccommodationProviderParameter{ProviderId = 4,	ParameterName = "MaxReceivedMessageSize",		ParameterValue = "20000000"}
            };

            return accommodationProvider;
        }

        [Test]
        public async Task YouTravel_Valuation_ByResort_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "AYT" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);    
                    DateTime checkOutDate =  checkInDate.AddDays(5);      
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] {};

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                  

                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse> automator = new YouTravelAvailabilityAutomator(new YouTravelAvailabilityRequestFactory()
                        , new ProviderOutputLogger(new ProviderOutputLoggerSettings() { ProviderOutputPath = "C:\\" }, null));
                    IAccommodationAvailabilityParser<YouTravelAvailabilityResponse> parser = new YouTravelAvailabilityParser(null);
                    IAccommodationAvailabilityProvider availabilityProvider = new YouTravelAvailabilityProvider(automator, parser);
                    
                        
                    

                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("YouTravel");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunValuationAssertions(selectedRoom, valuationResults.First());

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task YouTravel_Valuation_ByResort_2Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "AYT" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new int[] { });
                    rooms.Add(room2);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse> automator = new YouTravelAvailabilityAutomator(new YouTravelAvailabilityRequestFactory()
                        , new ProviderOutputLogger(new ProviderOutputLoggerSettings() { ProviderOutputPath = "C:\\" }, null));
                    IAccommodationAvailabilityParser<YouTravelAvailabilityResponse> parser = new YouTravelAvailabilityParser(null);
                    IAccommodationAvailabilityProvider availabilityProvider = new YouTravelAvailabilityProvider(automator, parser);




                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("YouTravel");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunValuationAssertions(selectedRoom, valuationResults.First());

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }


        protected  YouTravelAvailabilityProvider ConstructProvider()
        {
            //return NinjectInstaller.Kernel.Get<Hotels4UAvailabilityProvider>();
            return null;
        }

        protected AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(int roomNumber, int numberAdults, int[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = (byte)age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = (byte)roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
                                                                       AccommodationProviderAvailabilityResult selectedRoom)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;
            valuationRequest.SelectedRooms = new List<AccommodationProviderValuationRequestRoom>() { new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                , RoomNumber = availabilityRequest.Rooms.First().RoomNumber
                , Guests = availabilityRequest.Rooms.First().Guests
                , AvailabilityResult = selectedRoom
            }}.ToArray();

            return valuationRequest;
        }

        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {


            TestValuationResult testValuationResult = Hotels4UMapper.CloneValuationResult(valuationResult);
            string serialisedValuation = ""; //testValuationResult.XmlSerialize();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.Amount == selectedRoom.CostPrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.Amount == selectedRoom.SalePrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }
    }
}
