﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.IntegrationTests.HotelBeds;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace AlphaRooms.Accommodation.IntegrationTests.Jumbo
{

    class Provider_Jumbo_ValuationTests
    {
        private Random random;

        
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }

        [SetUp]
        public void Setup()
        {
            random = new Random();
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        protected  AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 5;
            accommodationProvider.EdiCode = "D";
            accommodationProvider.Name = "Jumbo";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes

            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboAgencyCode",             			    ParameterValue = "36"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboBranchCode",             			    ParameterValue = "1"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboPointOfSaleId",             			ParameterValue = "1"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboLanguage",             			    ParameterValue = "en"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboMaxNumRows",             			    ParameterValue = "4096"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboMaxRoomCombination",             		ParameterValue = "2"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboMaxIsOnlineOnly",             	    ParameterValue = "true"},
                new AccommodationProviderParameter { ProviderId = 5,	ParameterName = "JumboAvailabilityUrl",		                ParameterValue = "http://test.xtravelsystem.com:80/public/v1_0rc1/hotelBookingHandler"},                 
            };

            if (ConfigurationManager.AppSettings["UseProxy"] != null)
            {
                bool useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["UseProxy"]);

                accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 5, ParameterName = "UseProxy", ParameterValue = useProxy.ToString() });

                if (useProxy)
                {
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 5, ParameterName = "ProxyUrl", ParameterValue = ConfigurationManager.AppSettings["ProxyUrl"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 5, ParameterName = "ProxyPort", ParameterValue = ConfigurationManager.AppSettings["ProxyPort"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 5, ParameterName = "ProxyUsername", ParameterValue = ConfigurationManager.AppSettings["ProxyUsername"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 5, ParameterName = "ProxyPassword", ParameterValue = ConfigurationManager.AppSettings["ProxyPassword"] });
                }
            }

            return accommodationProvider;
        }



        [Test]
        public async Task Jumbo_Availability_ByEstablishment_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { };
            string[] establishments = new string[] { "51884" };

            try
            {
                foreach (var establishment in establishments)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 11);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = new string[] { establishment };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] {  });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Jumbo");//new JumboAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Jumbo");
                    
                    
                   

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", establishment));

                    RunValuationAssertions(selectedRoom, valuationResults.First());



                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       // [Test]
        public async Task Jumbo_Availability_ByEstablishment_1Room_WithChildAndInfant_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { };
            string[] establishments = new string[] { "H4U;10545" };

            try
            {
                foreach (var establishment in establishments)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] {  };

                    string[] establishmentEdiCodes = new string[] { establishment };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { 6, 1 });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Jumbo");//new JumboAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Jumbo");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", establishment));
                    
                    RunValuationAssertions(selectedRoom, valuationResults.First());

                    

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Jumbo_Availability_ByEstablishment_2Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { };
            string[] establishments = new string[] { "51884" };

            try
            {
                foreach (var establishment in establishments)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 11);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    string[] establishmentEdiCodes = new string[] { establishment };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room2);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Jumbo");//new JumboAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Jumbo");
                    valuationRequest.Provider = provider;

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 2, string.Format("No valuation results returned for destination {0} - test cannot continue.", establishment));

                    RunValuationAssertions(selectedRoom, valuationResults.First());



                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       // [Test]
        public async Task Jumbo_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "SSH" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 24);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Jumbo");//new JumboAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    //RunAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    int roomIndex = random.Next(0, results.Count - 1);
                    AccommodationProviderAvailabilityResult selectedRoom = results[roomIndex];

                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRoom);

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Jumbo");

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count == 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunValuationAssertions(selectedRoom, valuationResults.First());

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        


        protected AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(int roomNumber, int numberAdults, int[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age = 25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = (byte)age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityKey = Guid.NewGuid().ToString(),
                RoomNumber = (byte)roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProvider provider,
                                                                                   SearchType searchType,
                                                                                   Channel channel,
                                                                                   DateTime checkInDate,
                                                                                   DateTime checkOutDate,
                                                                                   string[] destinationEdiCodes,
                                                                                   string[] establishmentEdiCodes,
                                                                                   AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            //Probably need to set the AccommodationProviderAvailabilityResult
            AccommodationProviderValuationRequest request = new AccommodationProviderValuationRequest();
            request.ValuationId = Guid.NewGuid();
            request.Debugging = true;
            var availabilityRequest = new AccommodationProviderAvailabilityRequest();
            availabilityRequest.Provider = provider;

            availabilityRequest.AvailabilityId = Guid.NewGuid();
            availabilityRequest.ChannelInfo = new ChannelInfo() { Channel = channel };

            availabilityRequest.SearchType = searchType;
            availabilityRequest.Debugging = true;

            availabilityRequest.CheckInDate = checkInDate;
            availabilityRequest.CheckOutDate = checkOutDate;

            availabilityRequest.DestinationCodes = destinationEdiCodes;
            availabilityRequest.EstablishmentCodes = establishmentEdiCodes;
            request.SelectedRooms = rooms.Select(i => new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                ,
                RoomNumber = i.RoomNumber
                ,
                Guests = i.Guests
                //TODO , AvailabilityResult =
            }).ToArray();
            return request;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
        
                                                                        AccommodationProviderAvailabilityResult selectedRoom)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;
            valuationRequest.SelectedRooms = new List<AccommodationProviderValuationRequestRoom>() { new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                , RoomNumber = availabilityRequest.Rooms.First().RoomNumber
                , Guests = availabilityRequest.Rooms.First().Guests
                , AvailabilityResult = selectedRoom
            }}.ToArray();

            return valuationRequest;
        }

        
        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }

        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {
            

            TestValuationResult testValuationResult =  JumboMapper.CloneValuationResult(valuationResult);
            string serialisedValuation = ""; //testValuationResult.XmlSerialize();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.Amount == selectedRoom.CostPrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.Amount == selectedRoom.SalePrice.Amount, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }
    }

    public class TestValuationResult
    {
        public Guid Id { get; set; }
        public int RoomNumber { get; set; }

        public string ProviderName { get; set; }
        public string ProviderEdiCode { get; set; }

        public string DestinationEdiCode { get; set; }

        public string EstablishmentEdiCode { get; set; }
        public string EstablishmentName { get; set; }

        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public int NumberOfInfants { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public string BoardCode { get; set; }
        public string BoardDescription { get; set; }

        public string RoomCode { get; set; }
        public string RoomDescription { get; set; }

        public Money SalePrice { get; set; }
        public Money CostPrice { get; set; }

        public decimal CommissionPercent { get; set; }
        public Money CommissionAmount { get; set; }

        public PaymentModel PaymentModel { get; set; }

        public bool IsOpaqueRate { get; set; }
        public bool IsSpecialRate { get; set; }
        public bool? IsNonRefundableRate { get; set; }
        public bool IsBindingRate { get; set; }

        public RateType RateType { get; set; }

        public string CancellationPolicy { get; set; }

        public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }
    }

    public class JumboMapper
    {
        static JumboMapper()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationProviderValuationResult, TestValuationResult>();
            }
        }

        

        public static TestValuationResult CloneValuationResult(AccommodationProviderValuationResult valuationResult)
        {
            TestValuationResult testResult = Mapper.Map<AccommodationProviderValuationResult, TestValuationResult>(valuationResult);

            return testResult;
        }
    }
}
