﻿using AlphaRooms.Accommodation.Core.DependencyInjection;
using AlphaRooms.Accommodation.Core.Provider.DependencyInjection;
using AlphaRooms.Accommodation.Provider.DependencyInjection;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.IntegrationTests
{
    public class NinjectInstaller
    {
        public static StandardKernel Kernel { get; set; }
        public static bool IsStarted { get; set; }

        public static void Start()
        {
            IsStarted = true;
            CreateKernel();
        }

        public static void Stop()
        {
            IsStarted = false;
        }

        private static IKernel CreateKernel()
        {
            Kernel = new StandardKernel();
            RegisterServices(Kernel);

            return Kernel;
        }

        public static void RegisterServices(IKernel kernel)
        {
            var modules = new INinjectModule[]
            {   
                new CoreDependencyInjectionInstaller()
                , new CoreProviderDependencyInjectionInstaller()
                , new ProviderDependencyInjectionInstaller()
            };

            kernel.Load(modules);
        }
    }
}
