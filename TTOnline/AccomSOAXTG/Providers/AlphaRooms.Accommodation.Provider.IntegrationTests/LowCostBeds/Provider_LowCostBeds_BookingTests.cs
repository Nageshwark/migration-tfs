﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.IntegrationTests.HotelBeds;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using log4net.Repository;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.LowCostBeds
{
    public class Provider_LowCostBeds_BookingTests : AvailabilityTestBase<LowCostBedsAvailabilityProvider>
    {

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        protected override AccommodationProvider ConfigureProvider()
        {
            // Always manually configure the provider instead of getting the data from the database. This avoids problems of misconfiguration. 
            AccommodationProvider accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 6;
            accommodationProvider.EdiCode = "E";
            accommodationProvider.Name = "LowCostBeds";
            accommodationProvider.IsActive = true;
            accommodationProvider.IsCoreCacheActive = true;
            accommodationProvider.MaxChildAge = 11;
            accommodationProvider.SearchTimeout = new TimeSpan(0, 2, 00);		// 2 minutes
            
            // NOTE: These are TEST Settings.
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsLongin",             			    ParameterValue = "v2tEsTALPHA"},
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsPassword",             			    ParameterValue = "alphav2test"},
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsLanguage",             			    ParameterValue = "en"},
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsMaxNumRows",             			ParameterValue = "4096"},
                //new AccommodationProviderParameter { ProviderId = 6,    ParameterName = "LowCostCurrencyCode",                          ParameterValue = "GBP" },
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsAvailabilityUrl",		            ParameterValue = "http://lcbtestxmlv2.ivector.co.uk/soap/book.asmx"},
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsBookingUrl",		                ParameterValue = "http://lcbtestxmlv2.ivector.co.uk/soap/book.asmx"}, 
                new AccommodationProviderParameter { ProviderId = 6,	ParameterName = "LowCostBedsTradeReference",		            ParameterValue = Guid.NewGuid().ToString()}
            };

            if (ConfigurationManager.AppSettings["UseProxy"] != null)
            {
                bool useProxy = false;// Convert.ToBoolean(ConfigurationManager.AppSettings["UseProxy"]);
            
                accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 6, ParameterName = "UseProxy", ParameterValue = useProxy.ToString() });

                if (useProxy)
                {
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 6, ParameterName = "ProxyUrl",       ParameterValue = ConfigurationManager.AppSettings["ProxyUrl"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 6, ParameterName = "ProxyPort",      ParameterValue = ConfigurationManager.AppSettings["ProxyPort"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 6, ParameterName = "ProxyUsername",  ParameterValue = ConfigurationManager.AppSettings["ProxyUsername"] });
                    accommodationProvider.Parameters.Add(new AccommodationProviderParameter { ProviderId = 6, ParameterName = "ProxyPassword",  ParameterValue = ConfigurationManager.AppSettings["ProxyPassword"] });                    
                }
            }

            return accommodationProvider;
        }

        [Test]
        public async Task LowCostBeds_Bookng_ByDestination_1Room_WithCard_WillReturnBooking()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "64;324" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2016, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(7);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");//new LowCostBedsAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAvailabilityAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in request.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    
                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(request, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");
                    

                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    var hasNonRefundable = from result in valuationResults
                                           where result.IsNonRefundable == true || result.RoomDescription.ToLower().Contains("refundable", StringComparison.InvariantCulture)
                                           select result;

                    Assert.IsFalse(hasNonRefundable.Any(), "Can not Test reservation with Non refundable roomtype!, aboring test");

                    // 3. Booking
                    // Create the booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("LowCostBeds");

                    //Force it to use Card Payment
                    //valuationRequest.SelectedRooms.First().AvailabilityResult.PaymentModel = PaymentModel.CustomerPayDirect;


                    // Create the boking request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(valuationResults, valuationRequest, true);
                    bookingRequest.Provider = provider;

                    
                    // Get booking result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookingResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResultList = bookingResults.ToList();

                    foreach (var booking in bookingResultList)
                    {
                        RunBookingAssertions(booking);
                    }

                    await Task.Delay(500); ;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task LowCostBeds_Booking_ByDestination_1Room_WithNoCard_WillReturnBooking()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "64;324" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2016, 10, 10);
                    DateTime checkOutDate = checkInDate.AddDays(7);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");//new LowCostBedsAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAvailabilityAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in request.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }



                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(request, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");


                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    var hasNonRefundable = from result in valuationResults
                                           where result.IsNonRefundable == true || result.RoomDescription.ToLower().Contains("refundable", StringComparison.InvariantCulture)
                                           select result;

                    Assert.IsFalse(hasNonRefundable.Any(), "Can not Test reservation with Non refundable roomtype!, aboring test");

                    // 3. Booking
                    // Create the booking Provider
                    IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("LowCostBeds");

                    // Create the boking request
                    AccommodationProviderBookingRequest bookingRequest = CreateBookingRequest(valuationResults, valuationRequest, false);
                    bookingRequest.Provider = provider;

                    // Get booking result from Provider
                    IEnumerable<AccommodationProviderBookingResult> bookingResults = await bookingProvider.MakeProviderBookingAsync(bookingRequest);
                    List<AccommodationProviderBookingResult> bookingResultList = bookingResults.ToList();

                    foreach (var booking in bookingResultList)
                    {
                        RunBookingAssertions(booking);
                    }

                    await Task.Delay(500); ;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[Test]
        public async Task LowCostBeds_Valuation_ByDestination_1Room_WithChildAndInfant_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "64;324" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 01);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] {6, 1});
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");//new LowCostBedsAvailabilityProvider(automator, parser);


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAvailabilityAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in request.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    // For debugging
                    //List<string> serialisedSelectedRooms = new List<string>();

                    //foreach (var selectedRoom in selectedRooms)
                    //{
                    //    TestAvailabilityResult testAvailabilityResult = LowCostBedsMapper.CloneAvailabilityResult(selectedRoom);
                    //    string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                    //    serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    //}


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(request, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");


                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[Test]
        public async Task LowCostBeds_Valuation_ByDestination_2Room_WillReturnAvailability()
        {
            // NOTE: "BGI8" is a destination with just one hotel
            List<string> destinations = new List<string>() { "64;324" };

            try
            {
                foreach (var destinationEdiCode in destinations)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 09, 01);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { destinationEdiCode };

                    string[] establishmentEdiCodes = new string[] { };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);


                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new int[] { });
                    rooms.Add(room2);

                    // Create the request
                    AccommodationProviderAvailabilityRequest request = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");//new LowCostBedsAvailabilityProvider(automator, parser);



                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    RunAvailabilityAssertions(request, destinationEdiCode, results);

                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in request.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    // For debugging
                    //List<string> serialisedSelectedRooms = new List<string>();

                    //foreach (var selectedRoom in selectedRooms)
                    //{
                    //    TestAvailabilityResult testAvailabilityResult = LowCostBedsMapper.CloneAvailabilityResult(selectedRoom);
                    //    string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                    //    serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    //}


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(request, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");


                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1, string.Format("No valuation results returned for destination {0} - test cannot continue.", destinationEdiCode));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom = selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(string.Format("Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}", valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[Test]
        public async Task LowCostBeds_Valuation_ByEstablishment_1Room_WillReturnAvailability()
        {
            string[] establishmentEdiCodes = new string[] { "5848", "385" };

            try
            {
                //foreach (var establishment in establishments)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 11);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    //string[] establishmentEdiCodes = new string[] { establishment };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();


                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", establishmentEdiCodes[0]));

                    RunAvailabilityAssertions(availabilityRequest, establishmentEdiCodes[0], results);

                    

                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    // For debugging
                    //List<string> serialisedSelectedRooms = new List<string>();

                    //foreach (var selectedRoom in selectedRooms)
                    //{
                    //    TestAvailabilityResult testAvailabilityResult = LowCostBedsMapper.CloneAvailabilityResult(selectedRoom);
                    //    string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                    //    serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    //}


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");


                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1,
                        string.Format("No valuation results returned for destination {0} - test cannot continue.",
                            establishmentEdiCodes[0]));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom =
                            selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(
                                string.Format(
                                    "Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}",
                                    valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }

               
                   

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[Test]
        public async Task LowCostBeds_Valuation_ByEstablishment_2Room_WillReturnAvailability()
        {
            string[] establishmentEdiCodes = new string[] { "5848", "385" };

            try
            {
                //foreach (var establishment in establishments)
                {
                    // Clear all existing request status and availability results from Mongo.
                    //ClearMongoRequestStatus();
                    //ClearMongoDbAvailability();

                    // Create the supplier
                    AccommodationProvider provider = ConfigureProvider();

                    // Request Parameters
                    DateTime checkInDate = new DateTime(2015, 10, 11);
                    DateTime checkOutDate = checkInDate.AddDays(5);
                    SearchType searchType = SearchType.FlightAndHotel;
                    Channel channel = Channel.AlphaRoomsUK;

                    string[] destinationEdiCodes = new string[] { };

                    //string[] establishmentEdiCodes = new string[] { establishment };

                    // Rooms
                    List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                    AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new int[] { });
                    rooms.Add(room1);

                    AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new int[] { });
                    rooms.Add(room2);



                    // Create the request
                    AccommodationProviderAvailabilityRequest availabilityRequest = CreateAvailabilityRequest(provider, searchType, channel, checkInDate, checkOutDate, destinationEdiCodes, establishmentEdiCodes, rooms.ToArray());

                    // Create Provider
                    //IAccommodationAvailabilityProvider availabilityProvider = ConstructProvider();

                    IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("LowCostBeds");


                    // Get availability results from supplier
                    IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(availabilityRequest);
                    List<AccommodationProviderAvailabilityResult> results = availResults.ToList();


                    // Assertions
                    // 1. There are results
                    Assert.IsTrue(results.Count > 0, string.Format("No availability results returned for destination {0} - test cannot continue.", establishmentEdiCodes[0]));

                    RunAvailabilityAssertions(availabilityRequest, establishmentEdiCodes[0], results);



                    // 2. Valuation
                    // Select a room at random
                    List<AccommodationProviderAvailabilityResult> selectedRooms = new List<AccommodationProviderAvailabilityResult>();

                    foreach (var room in availabilityRequest.Rooms)
                    {
                        int noAdults = room.Guests.AdultsCount;
                        int noChildren = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                        int roomNumber = room.RoomNumber;

                        var matchingResults = results.Where(result => result.Adults == noAdults && result.Children == noChildren && result.RoomNumber == roomNumber).ToList();

                        if (matchingResults.Count > 0)
                        {
                            int roomIndex = RandomNumberGenerator.Between(0, matchingResults.Count - 1);
                            AccommodationProviderAvailabilityResult selectedRoom = matchingResults[roomIndex];
                            selectedRooms.Add(selectedRoom);
                        }
                    }


                    // For debugging
                    //List<string> serialisedSelectedRooms = new List<string>();

                    //foreach (var selectedRoom in selectedRooms)
                    //{
                    //    TestAvailabilityResult testAvailabilityResult = LowCostBedsMapper.CloneAvailabilityResult(selectedRoom);
                    //    string serialisedSelectedRoom = testAvailabilityResult.XmlSerialize();

                    //    serialisedSelectedRooms.Add(serialisedSelectedRoom);
                    //}


                    // Create the valuation request
                    AccommodationProviderValuationRequest valuationRequest = CreateValuationRequest(availabilityRequest, selectedRooms.ToArray());
                    valuationRequest.Provider = provider;

                    // Create the Valuation Provider
                    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("LowCostBeds");


                    // Get valuation result from Provider
                    IEnumerable<AccommodationProviderValuationResult> valResults = await valuationProvider.GetProviderValuationAsync(valuationRequest);
                    List<AccommodationProviderValuationResult> valuationResults = valResults.ToList();

                    // Valuation Assertions
                    Assert.IsTrue(valuationResults.Count >= 1,
                        string.Format("No valuation results returned for destination {0} - test cannot continue.",
                            establishmentEdiCodes[0]));

                    foreach (var valuationResult in valuationResults)
                    {
                        AccommodationProviderAvailabilityResult selectedRoom =
                            selectedRooms.First(room => room.RoomNumber == valuationResult.RoomNumber);

                        if (selectedRoom != null)
                        {
                            RunValuationAssertions(selectedRoom, valuationResult);
                        }
                        else
                        {
                            Assert.Fail(
                                string.Format(
                                    "Unable to located Room Availability Result for Establishment Code: {0}, RoomNumber: {1}",
                                    valuationResult.EstablishmentEdiCode, valuationResult.RoomNumber));
                        }
                    }

                    await Task.Delay(500);
                }




            }
            catch (Exception ex)
            {
                throw;
            }
        }

        
        
        private AccommodationProviderAvailabilityRequest CreateAvailabilityRequest(AccommodationProvider provider,
                                                                                    SearchType searchType,
                                                                                    Channel channel,
                                                                                    DateTime checkInDate,
                                                                                    DateTime checkOutDate,
                                                                                    string[] destinationEdiCodes,
                                                                                    string[] establishmentEdiCodes,
                                                                                    AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            AccommodationProviderAvailabilityRequest request = new AccommodationProviderAvailabilityRequest();

            request.Provider = provider;

            request.AvailabilityId = Guid.NewGuid();
            request.ChannelInfo = new ChannelInfo() { Channel = channel };

            request.SearchType = searchType;
            request.Debugging = true;

            request.CheckInDate = checkInDate;
            request.CheckOutDate = checkOutDate;

            request.DestinationCodes = destinationEdiCodes;
            request.EstablishmentCodes = establishmentEdiCodes;

            request.Rooms = rooms;

            return request;
        }


        protected override LowCostBedsAvailabilityProvider ConstructProvider()
        {
            //return NinjectInstaller.Kernel.Get<LowCostBedsAvailabilityProvider>();
            return null;
        }

        private void RunAvailabilityAssertions(AccommodationProviderAvailabilityRequest request, string destinationEdiCode,
            List<AccommodationProviderAvailabilityResult> results)
        {
            // 1. That all rooms have the correct number of adults and children
            foreach (var room in request.Rooms)
            {
                IEnumerable<AccommodationProviderAvailabilityResult> resultsForRoom =
                    results.Where(result => result.RoomNumber == room.RoomNumber);

                // Number of Adults
                bool hasWrongNumberOfAdults = resultsForRoom.Any(a => a.Adults != room.Guests.AdultsCount);
                Assert.IsTrue(hasWrongNumberOfAdults == false,
                    "At least one room result has the wrong number of adults.");

                // Number of children
                bool hasWrongNumberOfChildren =
                    resultsForRoom.Any(a => a.Children != room.Guests.ChildrenCount);

                Assert.IsTrue(hasWrongNumberOfChildren == false,
                    "At least one room result has the wrong number of children.");

                // Number of children
                bool hasWrongNumberOfInfant =
                    resultsForRoom.Any(a => a.Infants != room.Guests.InfantsCount);

                Assert.IsTrue(hasWrongNumberOfInfant == false,
                    "At least one room result has the wrong number of infant.");
            }

            // 2. That all results have a provider room code
            bool hasNoRoomCode = results.Any(a => string.IsNullOrWhiteSpace(a.RoomCode));
            Assert.IsTrue(hasNoRoomCode == false, "At least one room result has a missing room code.");

            // 3. That all results have a provider room code
            bool hasZeroCostPrice = results.Any(a => a.CostPrice.Amount == 0.00m);
            Assert.IsTrue(hasZeroCostPrice == false, "At least one room result has a zero cost price.");

            // 4. That the all results have a destination code (NOTE: this may NOT match the code that was used to create the request as the db codes do not exactly match those returned from the supplier).
            bool hasNoDestination = results.Any(a => string.IsNullOrWhiteSpace(a.DestinationEdiCode));
            Assert.IsTrue(hasNoDestination == false, "At least one room result is missing a Destination Edi code.");

            // 5. That all results have an establishment name
            bool hasNoEstablishmentName = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentName));
            Assert.IsTrue(hasNoEstablishmentName == false, "At least one room result is missing an establishment name.");

            // 6. That all results have an establishment Edi Code
            bool hasNoEstablishmentEdiCode = results.Any(a => string.IsNullOrWhiteSpace(a.EstablishmentEdiCode));
            Assert.IsTrue(hasNoEstablishmentEdiCode == false,
                "At least one room result is missing an establishment Edi Code.");

            // 7. That all results have a check in date
            bool hasNoCheckInDate = results.Any(a => a.CheckInDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckInDate == false, "At least one room result is missing the check in date.");

            // 8. That all results have a check out date
            bool hasNoCheckOutDate = results.Any(a => a.CheckOutDate == DateTime.MinValue);
            Assert.IsTrue(hasNoCheckOutDate == false, "At least one room result is missing the check out date.");

            // 9. That every room has at least one adult in it
            bool hasNoAdult = results.Any(a => a.Adults == 0);
            Assert.IsTrue(hasNoAdult == false, "At least one room result has no adult availability.");

            // 10. That the RoomId of each room falls within the room numbers provided in the request
            List<int> validRoomIds = request.Rooms.Select(room => (int)room.RoomNumber).ToList();
            foreach (int roomId in validRoomIds)
            {
                int count = results.Count(room => room.RoomNumber == roomId);
                Assert.IsTrue(count > 0, string.Format("The RoomId {0} is missing from the results.", roomId));
            }

            bool hasIncorrectRoomId = results.Any(a => !validRoomIds.Contains(a.RoomNumber));
            Assert.IsTrue(hasIncorrectRoomId == false,
                "At least one room result has a RoomId value that is not found in the Rooms collection on the Request.");

            // 11. That all sale prices have a currency
            bool hasNoSalePriceCurrency = false;
            foreach (var result in results)
            {
                if (result.SalePrice != null &&
                    string.IsNullOrWhiteSpace(result.SalePrice.CurrencyCode))
                {
                    hasNoSalePriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoSalePriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 12. That all cost prices have a currency
            bool hasNoCostPriceCurrency = false;
            foreach (var result in results)
            {
                if (result.CostPrice != null &&
                    string.IsNullOrWhiteSpace(result.CostPrice.CurrencyCode))
                {
                    hasNoCostPriceCurrency = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoCostPriceCurrency == false,
                "At least one room result has no currency set for the SalePrice.");

            // 13. That all results have either a cost price or a sale price 
            bool hasNoPrice = false;
            foreach (var result in results)
            {
                if (result.CostPrice == null && result.SalePrice == null)
                {
                    hasNoPrice = true;
                    break;
                }
            }
            Assert.IsTrue(hasNoPrice == false, "At least one room result has no CostPrice or SalePrice.");
        }

        /// <summary>
        /// Check that the valuation result matches the selected availability result.
        /// </summary>
        /// <param name="selectedRoom"></param>
        /// <param name="valuationResult"></param>
        private void RunValuationAssertions(AccommodationProviderAvailabilityResult selectedRoom, AccommodationProviderValuationResult valuationResult)
        {
            TestAvailabilityResult testAvailabilityResult = LowCostBedsMapper.CloneAvailabilityResult(selectedRoom);
            //string serialisedAvailability = testAvailabilityResult.XmlSerialize();
            //serialisedAvailability = LowCostBedsMapper.ReplaceSpecialChars(serialisedAvailability);

            TestValuationResult testValuationResult = LowCostBedsMapper.CloneValuationResult(valuationResult);
            //string serialisedValuation = testValuationResult.XmlSerialize();
            //serialisedValuation = LowCostBedsMapper.ReplaceSpecialChars(serialisedValuation);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("Availability Result:");
            //sb.AppendLine(serialisedAvailability);
            sb.AppendLine();
            sb.AppendLine("Valuation Result:");
            //sb.AppendLine(serialisedValuation);

            string serialisedData = sb.ToString();

            try
            {
                Assert.IsTrue(valuationResult.BoardCode == selectedRoom.BoardCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardCode." + serialisedData);

                Assert.IsTrue(valuationResult.BoardDescription == selectedRoom.BoardDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : BoardDescription." + serialisedData);

                Assert.IsTrue(valuationResult.CheckInDate == selectedRoom.CheckInDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckInDate." + serialisedData);

                Assert.IsTrue(valuationResult.CheckOutDate == selectedRoom.CheckOutDate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CheckOutDate." + serialisedData);

                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                if (valuationResult.CostPrice.Amount < selectedRoom.CostPrice.Amount - 2.00m ||
                    valuationResult.CostPrice.Amount > selectedRoom.CostPrice.Amount + 2.00m)
                {
                    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.Amount." + serialisedData);
                }

                Assert.IsTrue(valuationResult.CostPrice.CurrencyCode == selectedRoom.CostPrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : CostPrice.CurrencyCode." + serialisedData);

                Assert.IsTrue(valuationResult.Adults == selectedRoom.Adults, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfAdults." + serialisedData);

                Assert.IsTrue(valuationResult.Children == selectedRoom.Children, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfChildren." + serialisedData);

                Assert.IsTrue(valuationResult.Infants == selectedRoom.Infants, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : NumberOfInfants." + serialisedData);

                Assert.IsTrue(valuationResult.DestinationEdiCode == selectedRoom.DestinationEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : DestinationEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentEdiCode == selectedRoom.EstablishmentEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.EstablishmentName == selectedRoom.EstablishmentName, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : EstablishmentName." + serialisedData);

                Assert.IsTrue(valuationResult.IsBindingRate == selectedRoom.IsBindingRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsBindingRate." + serialisedData);

                //TODO:Enable it back once the LCB bug (Avail & Valuation returns different result for this field!) is fixed.  
                //Assert.IsTrue(valuationResult.IsNonRefundable == selectedRoom.IsNonRefundable, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsNonRefundableRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsOpaqueRate == selectedRoom.IsOpaqueRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsOpaqueRate." + serialisedData);

                Assert.IsTrue(valuationResult.IsSpecialRate == selectedRoom.IsSpecialRate, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : IsSpecialRate." + serialisedData);

                Assert.IsTrue(valuationResult.PaymentModel == selectedRoom.PaymentModel, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : PaymentModel." + serialisedData);

                Assert.IsTrue(valuationResult.ProviderEdiCode == selectedRoom.ProviderEdiCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : ProviderEdiCode." + serialisedData);

                Assert.IsTrue(valuationResult.RateType == selectedRoom.RateType, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RateType." + serialisedData);

                Assert.IsTrue(valuationResult.RoomCode == selectedRoom.RoomCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomCode." + serialisedData);

                Assert.IsTrue(valuationResult.RoomDescription == selectedRoom.RoomDescription, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomDescription." + serialisedData);

                Assert.IsTrue(valuationResult.RoomNumber == selectedRoom.RoomNumber, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : RoomNumber." + serialisedData);

                // Give leeway of 2.00 each way as sometimes valuation amounts do not quite match the availability amounts
                if (valuationResult.SalePrice.Amount < selectedRoom.SalePrice.Amount - 2.00m ||
                    valuationResult.SalePrice.Amount > selectedRoom.SalePrice.Amount + 2.00m)
                {
                    Assert.Fail("Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.Amount." + serialisedData);
                }

                Assert.IsTrue(valuationResult.SalePrice.CurrencyCode == selectedRoom.SalePrice.CurrencyCode, "Data mismatch between AccommodationProviderAvailabilityResult and AccommodationProviderValuationResult : SalePrice.CurrencyCode." + serialisedData);
            }
            catch (Exception ex)
            {
                // Catch block to enable us to capture objects when running in debug mode
                throw ex;
            }
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProviderAvailabilityRequest availabilityRequest,
                                                                                AccommodationProviderAvailabilityResult[] selectedRooms)
        {
            AccommodationProviderValuationRequest valuationRequest = new AccommodationProviderValuationRequest();
            valuationRequest.ValuationId = Guid.NewGuid();
            valuationRequest.Debugging = true;

            valuationRequest.SelectedRooms = selectedRooms.Select(i => new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = availabilityRequest
                , Guests = availabilityRequest.Rooms.First().Guests
                , AvailabilityResult = i
            }).ToArray();

            return valuationRequest;
        }

        private AccommodationProviderBookingRequest CreateBookingRequest(List<AccommodationProviderValuationResult> valuationResults, AccommodationProviderValuationRequest valuationRequest, bool useCard)
        {
            var bookingRequest = new AccommodationProviderBookingRequest();
            bookingRequest.ValuationId = Guid.NewGuid();
            bookingRequest.Debugging = true;

            bookingRequest.ValuatedRooms = valuationResults.Select(result => new AccommodationProviderBookingRequestRoom
            {
                AvailabilityRequest = valuationRequest.SelectedRooms.First().AvailabilityRequest,
                Guests = ConvertGuests(valuationRequest.SelectedRooms.First().Guests),
                ValuationResult = result
            }).ToArray();


            bookingRequest.Customer = new AccommodationProviderBookingRequestCustomer()
            {
                Title = GuestTitle.Mr,
                FirstName = "Test Booking",
                Surname = "Test Booking",
                Type = GuestType.Adult,
                EmailAddress = "test@test.net",
                ContactNumber = "011111111111111"
            };

            if (useCard)
            {
                bookingRequest.ValuatedRooms[0].ValuationResult.PaymentModel = PaymentModel.CustomerPayDirect;
                bookingRequest.ValuatedRooms[0].PaymentDetails = new AccommodationProviderBookingRequestRoomPaymentDetails                 
                {
                    CardHolderName = "Test Booking",
                    AddressLine1 = "travelnow",
                    CardExpireDate = new MonthYear(08, 2017),
                    CardNumber = "5401999999999999",
                    CardSecurityCode = "123",
                    CardType = CardType.VisaCredit,
                    City = "London",
                    Country = "GB",
                    Postcode = "W1 ABC",
                    PaymentMethod = PaymentMethod.CreditCard
                };
            }

            //to make sure Credit Card is used.
            //bookingRequest.ValuatedRooms[0].ValuationResult.PaymentModel = PaymentModel.CustomerPayDirect;

            foreach (var room in bookingRequest.ValuatedRooms)
            {
                foreach (var guest in room.Guests)
                {
                    if (string.IsNullOrEmpty(guest.FirstName))
                    {
                        guest.Title = GuestTitle.Mr;
                        guest.FirstName = "A";
                        guest.Surname = "Test";
                    }
                }
                
            }
            return bookingRequest;
        }

        private AccommodationProviderBookingRequestRoomGuestCollection ConvertGuests(AccommodationProviderAvailabilityRequestGuestCollection availGutestCollection)
        {

            var query = from AccommodationProviderAvailabilityRequestGuest g in availGutestCollection
                        select new AccommodationProviderBookingRequestRoomGuest
                        {
                            Age = g.Age,
                            Type = g.Type
                        };

            return new AccommodationProviderBookingRequestRoomGuestCollection(query.ToArray());
        }

        public void RunBookingAssertions(AccommodationProviderBookingResult booking)
        {
            Assert.IsTrue(booking.BookingStatus == BookingStatus.Confirmed, "BookingStatus must be Confirmed");
            Assert.IsNotNullOrEmpty(booking.ProviderBookingReference, "Booking Reference number must not be empty");
        }
    }

    
}
