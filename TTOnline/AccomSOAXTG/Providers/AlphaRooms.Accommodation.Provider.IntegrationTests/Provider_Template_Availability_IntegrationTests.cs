﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Provider.AlphaRoomsDirectProvider;
using AlphaRooms.Provider.TemplateProvider;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;
namespace AlphaRooms.Accommodation.IntegrationTests
{
    public class ProviderTemplateAvailabilityIntegrationTests:AvailabilityTestBase<TemplateAvailabilityProvider>
    {

        protected override TemplateAvailabilityProvider ConstructProvider()
        {
            TemplateAvailabilityProvider provider = NinjectInstaller.Kernel.Get<TemplateAvailabilityProvider>();

            return provider;
        }

        [Test]
        public async Task CustomTestExample()
        {
            var rooms = new List<AccommodationProviderAvailabilityRequestRoom>
            {
                CreateProviderRequestRoom(1,2,new int[]{})
            };
            List<string> establishmentCodes= new List<string>{"abc"};
            List<string> destinationCodes = new List<string> { "London" };
            var request = GetAvailabilityRequest(Channel.AlphaRoomsUK, "GBP", DateTime.Now.AddMonths(4), DateTime.Now.AddMonths(4).AddDays(7), rooms, establishmentCodes, destinationCodes);

            await TestAvailability(request);
        }

        protected override AccommodationProvider ConfigureProvider()
        {
            var accommodationProvider=new AccommodationProvider();
            accommodationProvider.Id= 50;//Setting dummy ID
            //Define the Accommodation Provider Parameters. All the Provider configuration/parameters must go in here.- This actual service will be fetched from the db when the Provider is loaded
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter {ProviderId = 50, ParameterName = "TemplateProviderSearchAPiUrl", ParameterValue = "http://template.com/Search"},
                new AccommodationProviderParameter {ProviderId = 50, ParameterName = "TemplateProviderAPIUsername", ParameterValue = "test"},
                new AccommodationProviderParameter {ProviderId = 50, ParameterName = "TemplateProviderAPIPassword", ParameterValue = "test123"},
            };

            return accommodationProvider;
        }
    }
}
