﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Provider;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests
{
    public abstract class AvailabilityIntegrationTestBase//: AccommodationAvailabilitySearchBase
    {
        private IAccommodationAvailabilityProvider provider;
        protected abstract IAccommodationAvailabilityProvider ConstructProvider();
        protected abstract AccommodationProvider ConfigureProvider();

        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
        }


        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.provider = this.ConstructProvider();
            
        }

        protected async Task TestAvailability(AccommodationProviderAvailabilityRequest request)
        {
            var results = await provider.GetProviderAvailabilityAsync(request);
        }

        protected AccommodationProviderAvailabilityRequest GetAvailabilityRequest(Channel channel, string currencyCode, DateTime checkinDtae, DateTime checkoutDate, List<AccommodationProviderAvailabilityRequestRoom> rooms, string[] providerEstablishmentCodes, string[] providerDestinationCodes)
        {
            var request = new AccommodationProviderAvailabilityRequest()
            {
                ChannelInfo = new ChannelInfo{Channel = channel, CurrencyCode = currencyCode},
                CheckInDate = checkinDtae, //DateTime.Now.AddMonths(3),
                CheckOutDate = checkoutDate, //DateTime.Now.AddMonths(3).AddDays(7),
                EstablishmentCodes = providerEstablishmentCodes,
                DestinationCodes = providerDestinationCodes,
                Rooms = rooms.ToArray(),
                Debugging = true
            };
            request.Provider =this.ConfigureProvider();
            return request;
        }

        protected AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(byte roomNumber, int numberAdults, byte[] childAges)
        {
            var guests = new List<AccommodationProviderAvailabilityRequestGuest>();

            for (int i = 0; i < numberAdults; i++)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = GuestType.Adult, Age =25 });
            }

            foreach (var age in childAges)
            {
                guests.Add(new AccommodationProviderAvailabilityRequestGuest() { Type = (age < 2 ? GuestType.Infant : GuestType.Child), Age = age });
            }

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = roomNumber,
                Guests = new AccommodationProviderAvailabilityRequestGuestCollection(guests)
            };
        }

        protected DateTime GetRandomDateBetween(DateTime startDate, DateTime endDate)
        {
            var range = endDate - startDate;

            var days = RandomNumberGenerator.Between(1, (int)range.TotalDays);

            return startDate.AddDays(days).Date;
        }
    }
}
