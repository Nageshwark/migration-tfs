﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Bev5
{
    public class Bev5ValuationTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();
           
        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]
        public async Task SingleroomValuation__WillReturnAvailability()
        {
            //Create Provider
            IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Bev5");

            Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
            Guid establishmentId = new Guid("74677872-1ea1-4f11-9e7e-4b710c179316"); //Melia Marbella Banus
            DateTime checkInDate = new DateTime(2015, 08, 15);
            DateTime checkOutDate = checkInDate.AddDays(7);

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = ConfigureProvider();

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[] { establishmentId.ToString() };

            var rooms = new[] { CreateProviderRequestRoom(1) };

            AccommodationProviderAvailabilityRequest providerAvailabilityRequest = CreateProviderAvailabilityRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);

            var availabilityResults = new List<AccommodationProviderValuationRequestRoom>();

            var incomingContract = @"<SerhsIncomingContract>
                  <Ticket>H4sIAAAAAAAAAAXBy5aCIAAA0H0_k4iKLlrgI6XoZPYCd2EOnknQI46aXz_3BkVfP4waG6ql8vXrtJIKPnmh-nu4nbI4hIm7TI0ss6a5WxP1USVFRUW6TYNXRzmp2n18tXDI8nZTI2BiufIHQGBkQDvmBNR1b50zn0RtDC1P5cNEvQ4WUaBWx13E75c8WeqOMUblcvQQZf2KOC2TDb7l7VsnmhXQGg_vY0-Gj_r6daaHm5mjQLAZHwYxY2NLe7382KCj-G9J3dY4_LLb_QMryuH82gAAAA==</Ticket>
                  <IsNonRefundable>true</IsNonRefundable>
                </SerhsIncomingContract>";

            var availabilityResult1 = new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = providerAvailabilityRequest
                ,
                RoomNumber = providerAvailabilityRequest.Rooms.First().RoomNumber
                ,
                Guests = providerAvailabilityRequest.Rooms.First().Guests
                ,
                AvailabilityResult = new AccommodationProviderAvailabilityResult()
                {
                    ProviderEdiCode = "C",
                    RoomNumber = 1,
                    EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                    EstablishmentName = "Melia",
                    DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                    CheckInDate = new DateTime(2015, 08, 15),
                    CheckOutDate = new DateTime(2015, 08, 22),
                    Adults = 2,
                    Children = 1,
                    BoardCode = "RO",
                    BoardDescription = "ROOM ONLY",
                    RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                    RoomDescription = "Studio",
                    CostPrice = new Money((decimal)1334.40, "EUR"),
                    SalePrice = new Money((decimal)1668.00, "EUR"),
                    PaymentModel = PaymentModel.PostPayment,
                    ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract }, { "TotalDiscountAmount", "0" }, { "Margin", "0" } },
                    IsNonRefundable = true
                }

            };

            availabilityResults.Add(availabilityResult1);

            var providerAvailabilityResults = availabilityResults.ToArray();

            var request = new AccommodationProviderValuationRequest
            {
                ValuationId = Guid.NewGuid(),
                //AvailabilityRequest = providerAvailabilityRequest,
                SelectedRooms = providerAvailabilityResults,
                Provider = provider,
                ChannelInfo = channelInfo
            };

            var valuationResult = await valuationProvider.GetProviderValuationAsync(request);

            //1. Results not equal to null
            Assert.IsNotNull(valuationResult, "The valuation result is null.");
            Assert.IsTrue(valuationResult.Count() > 0, "Test failed. No results returned.");
        }

        [Test]
        public async Task MultiroomValuation_WithChildren__WillReturnAvailability()
        {
            //Create Provider
            IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Bev5");

            Guid destinationId = new Guid("151E8FB1-A132-4BB6-BB06-52800733614B"); //Puerto Banus
            Guid establishmentId = new Guid("74677872-1ea1-4f11-9e7e-4b710c179316"); //Melia Marbella Banus
            DateTime checkInDate = new DateTime(2015, 08, 15);
            DateTime checkOutDate = checkInDate.AddDays(7);

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var provider = ConfigureProvider();

            var destinationCodes = new string[] { destinationId.ToString() };
            var establishmentCodes = new string[] { establishmentId.ToString() };

            AccommodationProviderValuationRequest request = CreateValuationRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate);

            var valuationResult = await valuationProvider.GetProviderValuationAsync(request);

            //1. Results not equal to null
            Assert.IsNotNull(valuationResult, "The valuation result is null.");
            Assert.IsTrue(valuationResult.Count() > 0, "Test failed. No results returned.");
        }

        //GTA valuation
        //[Test]
        //public async Task MultiroomValuation_WithChildren__WillReturnAvailability()
        //{
        //    //Create Provider
        //    IAccommodationValuationProvider valuationProvider = NinjectInstaller.Kernel.Get<IAccommodationValuationProvider>("Bev5");

        //    var destinationId = "LON"; //GTA - London
        //    var establishmentId = "GEN";
        //    DateTime checkInDate = new DateTime(2016, 06, 15);
        //    DateTime checkOutDate = new DateTime(2016, 06, 16);

        //    var channelInfo = new ChannelInfo()
        //    {
        //        Channel = Channel.AlphaRoomsUK
        //    };

        //    var provider = ConfigureProvider();

        //    var destinationCodes = new string[] { destinationId.ToString() };
        //    var establishmentCodes = new string[] { establishmentId.ToString() };

        //    AccommodationProviderValuationRequest request = CreateValuationRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate);

        //    var valuationResult = await valuationProvider.GetProviderValuationAsync(request);

        //    //1. Results not equal to null
        //    Assert.IsNotNull(valuationResult, "The valuation result is null.");
        //    Assert.IsTrue(valuationResult.Count() > 0, "Test failed. No results returned.");
        //}


        protected AccommodationProvider ConfigureProvider()
        {
            var accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 5;
            //Define the Accommodation Provider Parameters. All the Provider configuration/parameters must go in here.- This actual service will be fetched from the db when the Provider is loaded
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5SearchEndpoint", ParameterValue = "http://localhost:63620/Bev5SOAService.svc"},
                //new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5EdiProvidersToSearch", ParameterValue = "BK,EX,Y,LR,OT,NT,LC,4,C,H,M,MC,PX,SH,T,CS,V,EC,GT,S,U,SG,12,3,AV,D,GP,HB,MP,14,16,6,7,CN,G,GI,HC,RU,TS,TT,VM,11"},//2- Viajes olympia taken out due to error
                
            };

            return accommodationProvider;
        }

        private AccommodationProviderValuationRequest CreateValuationRequest(AccommodationProvider provider, ChannelInfo channelInfo, string[] destinationCodes, string[] establishmentCodes
           , DateTime checkInDate, DateTime checkOutDate)
        {
            var rooms = new[] { CreateProviderRequestRoom(1), CreateProviderRequestRoom(2) };

            // create provider availability request
            AccommodationProviderAvailabilityRequest providerAvailabilityRequest = CreateProviderAvailabilityRequest(provider, channelInfo, destinationCodes, establishmentCodes, checkInDate, checkOutDate, rooms);
            AccommodationProviderValuationRequestRoom[] providerAvailabilityResults = CreateProviderAvailabilityResults(providerAvailabilityRequest).ToArray();

            return new AccommodationProviderValuationRequest
            {
                ValuationId = Guid.NewGuid(),
                //AvailabilityRequest = providerAvailabilityRequest,
                SelectedRooms = providerAvailabilityResults,
                Provider = provider,
                ChannelInfo = channelInfo

            };
        }

        private AccommodationProviderAvailabilityRequestRoom CreateProviderRequestRoom(int roomNumber)
        {
            //var guestRequestList = new List<AccommodationProviderAvailabilityGuestRequest>();

            var guestRequestAdult1 = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 30,
                Type = GuestType.Adult
            };

            var guestRequestAdult2 = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 30,
                Type = GuestType.Adult
            };

            var guestRequestChild = new AccommodationProviderAvailabilityRequestGuest()
            {
                Age = 5,
                Type = GuestType.Child
            };

            var guestRequestList = new List<AccommodationProviderAvailabilityRequestGuest> { guestRequestAdult1, guestRequestAdult2 }; // { guestRequestAdult1, guestRequestAdult2, guestRequestChild };

            var guests = new AccommodationProviderAvailabilityRequestGuestCollection(guestRequestList);

            return new AccommodationProviderAvailabilityRequestRoom()
            {
                AvailabilityRequestId = Guid.NewGuid(),
                RoomNumber = (byte) roomNumber,
                Guests = guests
            };
        }

        private List<AccommodationProviderValuationRequestRoom> CreateProviderAvailabilityResults(AccommodationProviderAvailabilityRequest providerAvailabilityRequest)
        {
            var availabilityResults = new List<AccommodationProviderValuationRequestRoom>();

            var incomingContract = @"<SerhsIncomingContract>
                  <Ticket>H4sIAAAAAAAAAAXBy5aCIAAA0H0_k4iKLlrgI6XoZPYCd2EOnknQI46aXz_3BkVfP4waG6ql8vXrtJIKPnmh-nu4nbI4hIm7TI0ss6a5WxP1USVFRUW6TYNXRzmp2n18tXDI8nZTI2BiufIHQGBkQDvmBNR1b50zn0RtDC1P5cNEvQ4WUaBWx13E75c8WeqOMUblcvQQZf2KOC2TDb7l7VsnmhXQGg_vY0-Gj_r6daaHm5mjQLAZHwYxY2NLe7382KCj-G9J3dY4_LLb_QMryuH82gAAAA==</Ticket>
                  <IsNonRefundable>false</IsNonRefundable>
                </SerhsIncomingContract>";

            var availabilityResult1 = new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = providerAvailabilityRequest
                ,
                RoomNumber = providerAvailabilityRequest.Rooms[0].RoomNumber
                ,
                Guests = providerAvailabilityRequest.Rooms[0].Guests
                ,
                AvailabilityResult = new AccommodationProviderAvailabilityResult()
                {
                    ProviderEdiCode = "C",
                    RoomNumber = 1,
                    EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                    DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                    CheckInDate = new DateTime(2015, 08, 15),
                    CheckOutDate = new DateTime(2015, 08, 22),
                    Adults = 2,
                    Children = 1,
                    BoardCode = "RO",
                    BoardDescription = "ROOM ONLY",
                    RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                    RoomDescription = "Studio",
                    CostPrice = new Money((decimal)1334.40, "EUR"),
                    SalePrice = new Money((decimal)1668.00, "EUR"),
                    PaymentModel = PaymentModel.PostPayment,
                    ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract }, { "TotalDiscountAmount", "0" }, { "Margin", "0" } },
                    IsNonRefundable = true
                }

            };

            var availabilityResult2 = new AccommodationProviderValuationRequestRoom()
            {
                AvailabilityRequest = providerAvailabilityRequest
                ,
                RoomNumber = providerAvailabilityRequest.Rooms[1].RoomNumber
                ,
                Guests = providerAvailabilityRequest.Rooms[1].Guests
                ,
                AvailabilityResult = new AccommodationProviderAvailabilityResult
                {
                    ProviderEdiCode = "C",
                    RoomNumber = 2,
                    EstablishmentEdiCode = "74677872-1ea1-4f11-9e7e-4b710c179316",
                    DestinationEdiCode = "151E8FB1-A132-4BB6-BB06-52800733614B",
                    CheckInDate = new DateTime(2015, 08, 15),
                    CheckOutDate = new DateTime(2015, 08, 22),
                    Adults = 2,
                    Children = 1,
                    BoardCode = "TW",
                    BoardDescription = "TWIN ROOM",
                    RoomCode = "84E1B5B9-A740-4B13-AF99-A7E5CA341539",
                    RoomDescription = "DeluxeRoom",
                    CostPrice = new Money((decimal)1500.40, "EUR"),
                    SalePrice = new Money((decimal)1700.00, "EUR"),
                    PaymentModel = PaymentModel.PostPayment,
                    ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract }, { "TotalDiscountAmount", "0" }, { "Margin", "0" } },
                    IsNonRefundable = true
                }
            };

            availabilityResults.Add(availabilityResult1);
            availabilityResults.Add(availabilityResult2);

            return availabilityResults;

        }

        //GTA 
        //private List<AccommodationProviderValuationRequestRoom> CreateProviderAvailabilityResults(AccommodationProviderAvailabilityRequest providerAvailabilityRequest)
        //{
        //    var availabilityResults = new List<AccommodationProviderValuationRequestRoom>();

        //    var incomingContract1 = @"<ContractProperties>
        //            <RoomCode> Dormitories Twin </RoomCode>
        //            <Cost> 71.50 </Cost>
        //            <ExtraBeds> 0 </ExtraBeds>
        //            <EssentialInformation>
        //                <string> NO CHILDREN ARE PERMITTED IN THIS HOSTEL</string>
        //            </EssentialInformation>
        //        </ContractProperties> ";

        //    var incomingContract2 = @"<ContractProperties>
        //            <RoomCode>Shared Facilities Twin</RoomCode>
        //            <Cost>84.75</Cost>
        //            <ExtraBeds>0</ExtraBeds>
        //            <EssentialInformation>
        //                <string>NO CHILDREN ARE PERMITTED IN THIS HOSTEL</string>
        //            </EssentialInformation>
        //        </ContractProperties> ";

        //    var availabilityResult1 = new AccommodationProviderValuationRequestRoom()
        //    {
        //        AvailabilityRequest = providerAvailabilityRequest
        //        ,
        //        RoomNumber = providerAvailabilityRequest.Rooms[0].RoomNumber
        //        ,
        //        Guests = providerAvailabilityRequest.Rooms[0].Guests
        //        ,
        //        AvailabilityResult = new AccommodationProviderAvailabilityResult()
        //        {
        //            ProviderEdiCode = "GT",
        //            RoomNumber = 1,
        //            EstablishmentEdiCode = "", // 
        //            DestinationEdiCode = "LON",
        //            CheckInDate = new DateTime(2016, 06, 15),
        //            CheckOutDate = new DateTime(2016, 06, 16),
        //            Adults = 2,
        //            BoardCode = "BC",
        //            BoardDescription = "BreakfastContinental",
        //            RoomCode = "001:GEN1:9725:S9502:10859:45163",
        //            RoomDescription = "Dormitories Twin",
        //            CostPrice = new Money((decimal)71.50, "GBP"),
        //            SalePrice = new Money((decimal)71.50, "GBP"),
        //            PaymentModel = PaymentModel.PostPayment,
        //            ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract1 }, { "TotalDiscountAmount", "0" }, { "Margin", "0" } },
        //            IsNonRefundable = true
        //        }

        //    };

        //    var availabilityResult2 = new AccommodationProviderValuationRequestRoom()
        //    {
        //        AvailabilityRequest = providerAvailabilityRequest
        //        ,
        //        RoomNumber = providerAvailabilityRequest.Rooms[1].RoomNumber
        //        ,
        //        Guests = providerAvailabilityRequest.Rooms[1].Guests
        //        ,
        //        AvailabilityResult = new AccommodationProviderAvailabilityResult
        //        {
        //            ProviderEdiCode = "GT",
        //            RoomNumber = 2,
        //            EstablishmentEdiCode = "",
        //            DestinationEdiCode = "LON",
        //            CheckInDate = new DateTime(2016, 06, 15),
        //            CheckOutDate = new DateTime(2016, 06, 16),
        //            Adults = 2,
        //            BoardCode = "BC",
        //            BoardDescription = "BreakfastContinental",
        //            RoomCode = "001:GEN:9725:S9502:10859:45176",
        //            RoomDescription = "Shared Facilities Twin",
        //            CostPrice = new Money((decimal)84.75, "GBP"),
        //            SalePrice = new Money((decimal)84.75, "GBP"),
        //            PaymentModel = PaymentModel.PostPayment,
        //            ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract2 }, { "TotalDiscountAmount", "0" }, { "Margin", "0" } },
        //            IsNonRefundable = true
        //        }
        //    };

        //    availabilityResults.Add(availabilityResult1);
        //    availabilityResults.Add(availabilityResult2);

        //    return availabilityResults;

        //}

        private AccommodationProviderAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProvider provider, ChannelInfo channelInfo, string[] destinationCodes, string[] establishmentCodes
          , DateTime checkInDate, DateTime checkOutDate, AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            return new AccommodationProviderAvailabilityRequest()
            {
                AvailabilityId = Guid.NewGuid(),
                SearchType = SearchType.HotelOnly,
                Provider = provider,
                ChannelInfo = channelInfo,
                DestinationCodes = destinationCodes,
                EstablishmentCodes = establishmentCodes,
                CheckInDate = checkInDate,
                CheckOutDate = checkOutDate,
                Rooms = rooms
            };
        }
        
    }
}
