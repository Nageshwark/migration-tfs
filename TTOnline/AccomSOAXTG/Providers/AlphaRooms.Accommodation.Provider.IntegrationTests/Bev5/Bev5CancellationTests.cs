﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Bev5
{
    public class Bev5CancellationTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();

        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]

        public async Task CancelSingleRoomBooking_Success()
        {
            IAccommodationCancellationProvider cancelProvider = NinjectInstaller.Kernel.Get<IAccommodationCancellationProvider>("Bev5");

            var provider = ConfigureProvider();
            provider.EdiCode = "GT";

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var providerCancelRequest = new AccommodationProviderCancellationRequest()
            {
                CancellationId = Guid.NewGuid(),
                ItineraryId = 12345,
                Provider = provider,
                ProviderBookingReference = "751041", // GTA
                CheckInDate = new DateTime(2016,05,24),
                CheckOutDate = new DateTime(2016,05,25),
                EstablishmentEdiCode = "",
                CustomerFirstName = "Test",
                CustomerSurName = "Test"
            };

            var cancelResult = await cancelProvider.CancelProviderBookingAsync(providerCancelRequest);

            //1. Results not equal to null
            Assert.IsNotNull(cancelResult, "The cancellation result is null.");
            Assert.IsTrue(cancelResult.Any(), "Test failed. No results returned.");
        }

        protected AccommodationProvider ConfigureProvider()
        {
            var accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 5;
            //Define the Accommodation Provider Parameters. All the Provider configuration/parameters must go in here.- This actual service will be fetched from the db when the Provider is loaded
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5SearchEndpoint", ParameterValue = "http://localhost:63620/Bev5SOAService.svc"}
            };

            return accommodationProvider;
        }
    }
}
