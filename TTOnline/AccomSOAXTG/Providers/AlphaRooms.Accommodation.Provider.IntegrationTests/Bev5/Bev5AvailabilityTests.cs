﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Bev5
{
    public class Bev5AvailabilityTests:AvailabilityIntegrationTestBase
    {
        protected override IAccommodationAvailabilityProvider ConstructProvider()
        {
            IAccommodationAvailabilityProvider availabilityProvider = NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Bev5");

            return availabilityProvider;
        }

        protected override AccommodationProvider ConfigureProvider()
        {
            var accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 5;
            //Define the Accommodation Provider Parameters. All the Provider configuration/parameters must go in here.- This actual service will be fetched from the db when the Provider is loaded
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5SearchEndpoint", ParameterValue = "http://localhost:63620/Bev5SOAService.svc"},
                new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5EdiProvidersToSearch", ParameterValue = "GT"} //"BK,EX,Y,LR,OT,NT,LC,4,C,H,M,MC,PX,SH,T,CS,V,EC,GT,S,U,SG,12,3,AV,D,GP,HB,MP,14,16,6,7,CN,G,GI,HC,RU,TS,TT,VM,11"},//2- Viajes olympia taken out due to error
                
            };

            return accommodationProvider;
        }


        [Test]
        public async Task Bev5_Availability_ByDestination_1Room_WillReturnAvailability()
        {
            try
            {
                //test provincia for Hotusa
                //string destinationId = "F2EA0EEE-0C47-45BD-A939-8B792171D2CC"; // MVMOL

                //Benidorm
                //string destinationId = "ae77c30b-7974-4a65-abd6-dc004d243abb"; 
                //Paris
                //string destinationId = "c2f9ea36-633c-440f-ac12-3503450bc64b";
                //Mallorca
                //string destinationId = "fdcecbb6-b92e-4495-baa4-700f5d046687";
                //Algarve
                //string destinationId = "94012e2b-a209-4b31-aba7-aefe704e5874";
                //Costa del sol
                //string destinationId = "defc22a6-cfb7-42d5-aa5a-ea1b0a4a09d6";
                //Tenerife
                //string destinationId = "b4a276b8-bbd6-4744-b287-b5ef7795a4df";

                //Rome
                //string destinationId="0c58183e-5dbf-466e-be0d-544cc11084fe" ;

                //Newyork
                //string destinationId="7b04ed07-52e1-415e-b246-2b597973f6e2" ;

                //London
                string destinationId = "EEABD919-D17A-461A-93BE-953631B063AC";

                // Create the supplier
                AccommodationProvider provider = ConfigureProvider();

                // Request Parameters
                DateTime checkInDate = new DateTime(2015, 09, 02); // GetRandomDateBetween(DateTime.Today, DateTime.Today.AddDays(200));
                                                                   // HotelBeds releases availability up to 1 season (about 1 year) in advance.
                DateTime checkOutDate = new DateTime(2015, 09, 03); // GetRandomDateBetween(checkInDate, checkInDate.AddDays(14));
                                                                    // Max 14 days duration.
                SearchType searchType = SearchType.HotelOnly; //SearchType.FlightAndHotel;
                Channel channel = Channel.AlphaRoomsUK;

                string[] destinationEdiCodes = new string[] { destinationId };

                string[] establishmentEdiCodes = new string[] { };

                // Rooms
                List<AccommodationProviderAvailabilityRequestRoom> rooms = new List<AccommodationProviderAvailabilityRequestRoom>();
                AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                rooms.Add(room1);

                // Create the request
                AccommodationProviderAvailabilityRequest request = GetAvailabilityRequest(channel, "GBP", checkInDate,
                    checkOutDate, rooms, establishmentEdiCodes, destinationEdiCodes);

                // Create Provider
                IAccommodationAvailabilityProvider availabilityProvider =
                    NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Bev5");

                // Get availability results from supplier
                IEnumerable<AccommodationProviderAvailabilityResult> availResults = await availabilityProvider.GetProviderAvailabilityAsync(request);
                List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                // Assertions
                // 1. There are results
                Assert.IsTrue(results.Count > 0,
                    string.Format("No availability results returned for destination {0} - test cannot continue.",
                        destinationId));


                await Task.Delay(500);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [Test]
        public async Task Bev5_Availability_ByDestination_2Rooms_WillReturnAvailability()
        {
            try
            {
                //London
                string destinationId = "EEABD919-D17A-461A-93BE-953631B063AC";

                // Create the supplier
                AccommodationProvider provider = ConfigureProvider();

                // Request Parameters
                DateTime checkInDate = new DateTime(2015, 09, 02); 
                DateTime checkOutDate = new DateTime(2015, 09, 03);
                Channel channel = Channel.AlphaRoomsUK;

                string[] destinationEdiCodes = new string[] { destinationId };

                string[] establishmentEdiCodes = new string[] { };

                // Rooms
                List<AccommodationProviderAvailabilityRequestRoom> rooms =
                    new List<AccommodationProviderAvailabilityRequestRoom>();
                AccommodationProviderAvailabilityRequestRoom room1 = CreateProviderRequestRoom(1, 2, new byte[] { });
                rooms.Add(room1);


                AccommodationProviderAvailabilityRequestRoom room2 = CreateProviderRequestRoom(2, 2, new byte[] { });
                rooms.Add(room2);

                // Create the request
                AccommodationProviderAvailabilityRequest request = GetAvailabilityRequest(channel, "GBP", checkInDate,
                    checkOutDate, rooms, establishmentEdiCodes, destinationEdiCodes);

                // Create Provider
                IAccommodationAvailabilityProvider availabilityProvider =
                    NinjectInstaller.Kernel.Get<IAccommodationAvailabilityProvider>("Bev5");

                // Get availability results from supplier
                IEnumerable<AccommodationProviderAvailabilityResult> availResults =
                    await availabilityProvider.GetProviderAvailabilityAsync(request);
                List<AccommodationProviderAvailabilityResult> results = availResults.ToList();

                // Assertions
                // 1. There are results
                Assert.IsTrue(results.Count > 0,
                    string.Format("No availability results returned for destination {0} - test cannot continue.",
                        destinationId));


                await Task.Delay(500);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
