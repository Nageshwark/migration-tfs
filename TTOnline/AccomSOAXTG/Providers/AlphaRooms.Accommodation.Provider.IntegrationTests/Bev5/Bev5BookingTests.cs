﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject;
using NUnit.Framework;

namespace AlphaRooms.Accommodation.IntegrationTests.Bev5
{
    public class Bev5BookingTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
            NinjectInstaller.Start();

        }

        /// <summary>
        /// Run once at the end of a test run (which could be composed of multiple tests)
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            NinjectInstaller.Stop();
        }

        /// <summary>
        /// Run once per test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Run once per test
        /// </summary>
        [TearDown]
        public void LocalTearDown()
        {
        }

        [Test]
        public async Task SingleroomBooking_Success()
        {
            //Create Provider
            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Bev5");

            var provider = ConfigureProvider();

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var contract = @"DB#1#VR#185.26#0#RO#OK#20151115#20151116#EU#2-0#0#0#0#";

            var incomingContract = @"<HotusaIncomingContract>
                                        <PerDayBoardCodes>DB#1#VR#185.26#0#RO#OK#20151115#20151116#EU#2-0#0#0#0#</PerDayBoardCodes>
                                        <IsNonRefundable>false</IsNonRefundable>
                                        <Affiliation>RS</Affiliation>
                                    </HotusaIncomingContract> ";

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "H",
                EstablishmentEdiCode = "789421", //Test hotel in MVMOL province. This doesn't exist in the database, hence need to be added before testing through the site.
                EstablishmentName = "TEST INFORMATICA PROMETEO KEYTEL",
                DestinationEdiCode = "F2EA0EEE-0C47-45BD-A939-8B792171D2CC",
                CheckInDate = new DateTime(2015, 11, 15),
                CheckOutDate = new DateTime(2015, 11, 16),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "RoomOnly",
                RoomCode = "DB",
                RoomDescription = "full size bed (1 big bed)",
                CostPrice = new Money((decimal)185.26, "EUR"),
                SalePrice = new Money((decimal)185.26, "EUR"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract }, { "TotalDiscountAmount", "0" }, { "Margin", "0" }, { "Contract", contract } }

            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                Title = GuestTitle.Mr
                ,
                FirstName = "TestA"
                ,
                Surname = "TestA"
                ,
                ContactNumber = "123456789"
                ,
                Age = 30
                ,
                EmailAddress = "TestA@test.com"
                ,
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mr,
                FirstName = "TestA",
                Surname = "TestA",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mrs,
                FirstName = "TestB",
                Surname = "TestB",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "5569510003306537",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "Simon Wilson",
                CardSecurityCode = "851",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
                Country = "England",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                Customer = customer,
                ChannelInfo = channelInfo,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = 1
                    , ValuationResult = valuationResult
                    , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 })
                    , PaymentDetails = cardDetails
                    ,SpecialRequests = new AccommodationProviderBookingRequestRoomSpecialRequest {OtherRequests = "Test Booking, please cancel" }
                } }
                ,
                ItineraryId = 12345
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            Assert.IsNotNull(bookingResult, "The booking result is null.");
            Assert.IsTrue(bookingResult.Any(), "Test failed. No results returned.");
        }

        [Test]
        public async Task SingleroomBooking_WithChildren_Success()
        {
            //Create Provider
            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Bev5");

            var provider = ConfigureProvider();

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var contract = @"DB#1#VR#185.26#0#RO#OK#20151115#20151116#EU#2-0#0#0#0#";

            var incomingContract = @"<HotusaIncomingContract>
                                        <PerDayBoardCodes>DB#1#VR#185.26#0#RO#OK#20151115#20151116#EU#2-0#0#0#0#</PerDayBoardCodes>
                                        <IsNonRefundable>false</IsNonRefundable>
                                        <Affiliation>RS</Affiliation>
                                    </HotusaIncomingContract> ";

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "H",
                EstablishmentEdiCode = "789421", //Test hotel in MVMOL province. This doesn't exist in the database, hence need to be added before testing through the site.
                EstablishmentName = "TEST INFORMATICA PROMETEO KEYTEL",
                DestinationEdiCode = "F2EA0EEE-0C47-45BD-A939-8B792171D2CC",
                CheckInDate = new DateTime(2015, 11, 15),
                CheckOutDate = new DateTime(2015, 11, 16),
                Adults = 2,
                Children = 0,
                BoardCode = "RO",
                BoardDescription = "RoomOnly",
                RoomCode = "DB",
                RoomDescription = "full size bed (1 big bed)",
                CostPrice = new Money((decimal)185.26, "EUR"),
                SalePrice = new Money((decimal)185.26, "EUR"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "IncomingContract", incomingContract }, { "TotalDiscountAmount", "0" }, { "Margin", "0" }, { "Contract", contract } }

            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                Title = GuestTitle.Mr
                ,
                FirstName = "TestA"
                ,
                Surname = "Test"
                ,
                ContactNumber = "123456789"
                ,
                Age = 30
                ,
                EmailAddress = "TestA@test.com"
                ,
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mr,
                FirstName = "TestA",
                Surname = "Test",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mrs,
                FirstName = "TestB",
                Surname = "Test",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest3 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Master,
                FirstName = "TestC",
                Surname = "Test",
                Age = 3,
                Type = GuestType.Child
            };

            var guest4 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Master,
                FirstName = "TestD",
                Surname = "Test",
                Age = 12,
                Type = GuestType.Child
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "5569510003306537",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "Simon Wilson",
                CardSecurityCode = "851",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
                Country = "England",
            };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                Customer = customer,
                ChannelInfo = channelInfo,
                ValuatedRooms = new[] { new  AccommodationProviderBookingRequestRoom()
                {
                    RoomNumber = (byte)1
                    , ValuationResult = valuationResult
                    , Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2, guest3, guest4 })
                    , PaymentDetails = cardDetails
                    ,SpecialRequests = new AccommodationProviderBookingRequestRoomSpecialRequest {OtherRequests = "Test Booking, please cancel" }
                } }
                ,
                ItineraryId = 12345
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            Assert.IsNotNull(bookingResult, "The booking result is null.");
            Assert.IsTrue(bookingResult.Any(), "Test failed. No results returned.");
        }

        [Test]
        public async Task MultiroomBooking_WithOutChildren_Success()
        {
            //Create Provider
            IAccommodationBookingProvider bookingProvider = NinjectInstaller.Kernel.Get<IAccommodationBookingProvider>("Bev5");

            var provider = ConfigureProvider();

            var channelInfo = new ChannelInfo()
            {
                Channel = Channel.AlphaRoomsUK
            };

            var contract = @"<ContractProperties>
                    <RoomCode> Dormitories Twin </RoomCode>
                    <Cost> 71.50 </Cost>
                    <ExtraBeds> 0 </ExtraBeds>
                    <EssentialInformation>
                        <string> NO CHILDREN ARE PERMITTED IN THIS HOSTEL</string>
                    </EssentialInformation>
                </ContractProperties>";

            var valuationResult = new AccommodationProviderValuationResult()
            {
                ProviderEdiCode = "GT",
                EstablishmentEdiCode = "LON;GEN",
                EstablishmentName = "Generator Hostel London",
                DestinationEdiCode = "LON",
                CheckInDate = new DateTime(2016, 06, 15),
                CheckOutDate = new DateTime(2016, 06, 16),
                Adults = 2,
                Children = 0,
                BoardCode = "BC",
                BoardDescription = "BreakfastContinental",
                RoomCode = "001:GEN1:9725:S9502:10859:45163",
                RoomDescription = "Dormitories Twin",
                CostPrice = new Money((decimal)71.50, "GBP"),
                SalePrice = new Money((decimal)71.50, "GBP"),
                PaymentModel = PaymentModel.PostPayment,
                ProviderSpecificData = new Dictionary<string, string>() { { "TotalDiscountAmount", "0" }, { "Margin", "0" }, { "Contract", contract } }

            };

            var customer = new AccommodationProviderBookingRequestCustomer()
            {
                Title = GuestTitle.Mr
                ,
                FirstName = "TestA"
                ,
                Surname = "Test"
                ,
                ContactNumber = "123456789"
                ,
                Age = 30
                ,
                EmailAddress = "TestA@test.com"
                ,
                Type = GuestType.Adult
            };

            var guest1 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mr,
                FirstName = "TestA",
                Surname = "Test",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest2 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mrs,
                FirstName = "TestB",
                Surname = "Test",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest3 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mr,
                FirstName = "TestC",
                Surname = "Test1",
                Age = 30,
                Type = GuestType.Adult
            };

            var guest4 = new AccommodationProviderBookingRequestRoomGuest()
            {
                Title = GuestTitle.Mrs,
                FirstName = "TestD",
                Surname = "Test1",
                Age = 30,
                Type = GuestType.Adult
            };

            var cardDetails = new AccommodationProviderBookingRequestRoomPaymentDetails()
            {
                CardType = CardType.MasterCard,
                CardNumber = "5569510003306537",
                CardExpireDate = new MonthYear(05, 2016),
                CardHolderName = "Simon Wilson",
                CardSecurityCode = "851",
                AddressLine1 = "1",
                AddressLine2 = "Test",
                City = "London",
                Postcode = "SW1",
                Country = "England",
            };

            var room1 = new AccommodationProviderBookingRequestRoom()
            {
                RoomNumber = (byte)1
                ,
                ValuationResult = valuationResult
                ,
                Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest1, guest2 })
                ,
                PaymentDetails = cardDetails
                ,
                SpecialRequests =
                    new AccommodationProviderBookingRequestRoomSpecialRequest
                    {
                        OtherRequests = "Test Booking, please cancel"
                    }
            };

            var room2 = new AccommodationProviderBookingRequestRoom()
            {
                RoomNumber = (byte)2
                ,
                ValuationResult = valuationResult
                ,
                Guests = new AccommodationProviderBookingRequestRoomGuestCollection(new[] { guest3, guest4 })
                ,
                PaymentDetails = cardDetails
                ,
                SpecialRequests =
                    new AccommodationProviderBookingRequestRoomSpecialRequest
                    {
                        OtherRequests = "Test Booking, please cancel"
                    }
            };

            var valuatedRooms = new AccommodationProviderBookingRequestRoom[] { room1, room2 };

            //Create AccommodationProviderBookingRequest

            var providerBookingRequest = new AccommodationProviderBookingRequest()
            {
                Provider = provider,
                Customer = customer,
                ChannelInfo = channelInfo,
                ValuatedRooms = valuatedRooms,
                ItineraryId = 12345
            };

            var bookingResult = await bookingProvider.MakeProviderBookingAsync(providerBookingRequest);

            //1. Results not equal to null
            Assert.IsNotNull(bookingResult, "The booking result is null.");
            Assert.IsTrue(bookingResult.Any(), "Test failed. No results returned.");
        }

        protected AccommodationProvider ConfigureProvider()
        {
            var accommodationProvider = new AccommodationProvider();
            accommodationProvider.Id = 5;
            //Define the Accommodation Provider Parameters. All the Provider configuration/parameters must go in here.- This actual service will be fetched from the db when the Provider is loaded
            accommodationProvider.Parameters = new List<AccommodationProviderParameter>
            {
                new AccommodationProviderParameter {ProviderId = 5, ParameterName = "Bev5SearchEndpoint", ParameterValue = "http://localhost:63620/Bev5SOAService.svc"}
            };

            return accommodationProvider;
        }
    }
}
