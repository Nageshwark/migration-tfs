﻿using AlphaRooms.Accommodation.Provider.RMIService;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIAvailabilityResponse
    {
        public SearchRequest SupplierRequest { get; set; }
        public SearchResponse SupplierResponse { get; set; }
    }
}
