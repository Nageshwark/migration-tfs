﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.RMIService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.RMI.Factories;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIValuationAutomator : RMIProviderFactoryBase, IAccommodationValuationAutomatorAsync<RMIValuationResponse>
    {
        private const string RMIAvailabilityUrl = "RMIAvailabilityUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IRMIValuationRequestFactory RMIProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public RMIValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IRMIValuationRequestFactory RMISupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.RMIProviderValuationRequestFactory = RMISupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<RMIValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            RMIValuationResponse response = null;
            try
            {
                SearchRequest supplierRequest = this.RMIProviderValuationRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("RMIValuationAutomator.GetValuationResponseAsync: RMI supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }

            return response;
        }

        private async Task<RMIValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, SearchRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(RMIAvailabilityUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<SearchResponse>();

            var supplierResponse = new RMIValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
