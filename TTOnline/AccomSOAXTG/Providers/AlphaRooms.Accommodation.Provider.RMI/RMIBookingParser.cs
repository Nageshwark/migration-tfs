﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
//using AlphaRooms.Accommodation.Provider.RMIService.Response;
namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIBookingParser : IAccommodationBookingParser<RMIBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public RMIBookingParser(ILogger logger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, RMIBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                CreateBookingResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, bookingResults);

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("RMI Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                BookRequest supplierRequest,
                                                BookResponse supplierResponse,
                                                ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null &&
                                supplierResponse.BookingDetails != null &&
                                supplierResponse.BookingDetails.RoomBookings != null &&
                                supplierResponse.BookingDetails.RoomBookings.RoomBooking !=null &&
                                supplierResponse.BookingDetails.RoomBookings.RoomBooking.Any()
                               );

            if (hasResults && supplierResponse.ReturnStatus.Success == "True")
            {
                string bookingReference = supplierResponse.BookingDetails.BookingReference;
                
                var result = new AccommodationProviderBookingResult();

                result.BookingStatus = BookingStatus.Confirmed;
                result.ProviderBookingReference = bookingReference;
                result.Message = "";
                
                bookingResults.Enqueue(result);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from RMI.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Booking Response:");
                sb.AppendLine(supplierResponse.XmlSerialize());

                throw new SupplierApiException(sb.ToString());

            }
        }
    }
}
