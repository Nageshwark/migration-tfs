﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.RMIService;

namespace AlphaRooms.Accommodation.Provider.RMI.Interfaces
{
    public interface IRMIAvailabilityRequestFactory
    {
        SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
