﻿using AlphaRooms.Accommodation.Provider.RMIService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.RMI.Interfaces
{
    public interface IRMIValuationRequestFactory
    {
        SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request);
    }
}
