﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
//using AlphaRooms.Accommodation.Provider.RMIService.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.RMIService;

namespace AlphaRooms.Accommodation.Provider.RMI.Interfaces
{
    public interface IRMIBookingRequestFactory
    {
        BookRequest CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
    }
}
