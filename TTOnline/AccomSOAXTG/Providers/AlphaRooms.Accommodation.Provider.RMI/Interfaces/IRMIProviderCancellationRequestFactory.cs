﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.RMIService;

//using AlphaRooms.Accommodation.Provider.RMIService.Request;

namespace AlphaRooms.Accommodation.Provider.RMI.Interfaces
{
    public interface IRMIProviderCancellationRequestFactory
    {
        CancelRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
