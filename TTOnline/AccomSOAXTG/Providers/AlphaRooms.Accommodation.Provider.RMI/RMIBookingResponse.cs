﻿using AlphaRooms.Accommodation.Provider.RMIService;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIBookingResponse
    {
        public BookRequest SupplierRequest { get; set; }
        public BookResponse SupplierResponse { get; set; }
    }
}
