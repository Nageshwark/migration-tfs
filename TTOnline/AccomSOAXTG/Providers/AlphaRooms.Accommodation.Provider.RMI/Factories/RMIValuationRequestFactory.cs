﻿
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;
using AlphaRooms.Accommodation.Provider.RMI.Helper;
namespace AlphaRooms.Accommodation.Provider.RMI.Factories
{
    public class RMIValuationRequestFactory: RMIProviderFactoryBase, IRMIValuationRequestFactory
    {
        private const string RMIUserName = "RMIUserName";
        private const string RMIPassword = "RMIPassword";

        public SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request)
        {
            var helper = new RMIHelper();

            SearchRequest availRequest = new SearchRequest()
            {
                LoginDetails = CreateAuthHeader(request.Provider.Parameters),
                SearchDetails = helper.GetSearchDetails(request)
            };
            return availRequest;
        }

    }
}
