﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;
//using AlphaRooms.Accommodation.Provider.RMIService.Request;


namespace AlphaRooms.Accommodation.Provider.RMI.Factories
{
    public class RMICancellationRequestFactory : RMIProviderFactoryBase, IRMIProviderCancellationRequestFactory
    {
        public CancelRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {

            CancelRequest canRequest = new CancelRequest()
            {
                LoginDetails = CreateCancelAuthHeader(request.Provider.Parameters),
                BookingReference = request.ProviderBookingReference,
                Reason = "on customer request"
            };

            return canRequest;
        }
    }
}
