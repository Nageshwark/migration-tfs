﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.RMI.Helper;


namespace AlphaRooms.Accommodation.Provider.RMI.Factories
{
    public class RMIAvailabilityRequestFactory : RMIProviderFactoryBase, IRMIAvailabilityRequestFactory
    {
        private const string RMIUserName = "RMIUserName";
        private const string RMIPassword = "RMIPassword";

        public SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var helper = new RMIHelper();

            SearchRequest availRequest = new SearchRequest()
            {
                LoginDetails = CreateAuthHeader(request.Provider.Parameters),
                SearchDetails = helper.GetSearchDetails(request)
            };

            return availRequest;
        }

    }

}

