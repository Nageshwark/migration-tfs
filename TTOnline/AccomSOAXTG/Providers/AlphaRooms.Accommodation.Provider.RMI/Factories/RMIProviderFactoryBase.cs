﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.RMIService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.WebScraping;

namespace AlphaRooms.Accommodation.Provider.RMI.Factories
{
   public class RMIProviderFactoryBase
    {
        private const string RMIUserName = "RMIUserName";
        private const string RMIPassword = "RMIPassword";
        private const string RMIVersion = "RMIVersion";

        public SearchRequestLoginDetails CreateAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new SearchRequestLoginDetails
            {
                Login = parameters.GetParameterValue(RMIUserName),
                Password = parameters.GetParameterValue(RMIPassword),
                Version = parameters.GetParameterValue(RMIVersion)
            };
        }

        public BookRequestLoginDetails CreateBookingAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new BookRequestLoginDetails
            {
                Login = parameters.GetParameterValue(RMIUserName),
                Password = parameters.GetParameterValue(RMIPassword),
                Version = parameters.GetParameterValue(RMIVersion)
            };
        }

        public CancelRequestLoginDetails CreateCancelAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new CancelRequestLoginDetails
            {
                Login = parameters.GetParameterValue(RMIUserName),
                Password = parameters.GetParameterValue(RMIPassword),
                Version = parameters.GetParameterValue(RMIVersion)
            };
        }

        public async Task<string> PostXmlRequest(string postUrl, string request)
       {
           var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

           WebScrapeResponse response = await webScrapeClient.PostAsync(postUrl, request);

            return response.Value.ToString();
       }
    }
}
