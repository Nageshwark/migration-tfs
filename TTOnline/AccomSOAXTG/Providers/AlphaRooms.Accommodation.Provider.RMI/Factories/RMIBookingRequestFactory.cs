﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;



namespace AlphaRooms.Accommodation.Provider.RMI.Factories
{
    public class RMIBookingRequestFactory : RMIProviderFactoryBase, IRMIBookingRequestFactory
    {
        private const string RMIUserName = "RMIUserName";
        private const string RMIPassword = "RMIPassword";
        private const string RMICurrencyCode = "RMICurrencyCode";

        public BookRequest CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            BookRequest bookRequest = new BookRequest()
            {
                LoginDetails = CreateBookingAuthHeader(request.Provider.Parameters),
                BookDetails = GetReservationDetails(request)
            };

            return bookRequest;
        }


        private BookRequestBookDetails GetReservationDetails(AccommodationProviderBookingRequest request)
        {
            var roomToBook = request.ValuatedRooms.First().ValuationResult;
            var bookDtls = new BookRequestBookDetails()
            {
                ArrivalDate = roomToBook.CheckInDate.ToString("yyyy-MM-dd"),
                Duration = (byte)(roomToBook.CheckOutDate - roomToBook.CheckInDate).TotalDays,
                RoomBookings = GetRoomBookings(request),
                LeadGuest = GetLeadGuest(request.ValuatedRooms.First())
            };

            return bookDtls;
        }

        private BookRequestBookDetailsRoomBooking[] GetRoomBookings(AccommodationProviderBookingRequest request)
        {
            var lstRooms = new List<BookRequestBookDetailsRoomBooking>();

            foreach (var room in request.ValuatedRooms)
            {
                lstRooms.Add(new BookRequestBookDetailsRoomBooking()
                {
                    Adults = room.Guests.AdultsCount,
                    Children = room.Guests.ChildrenCount,
                    Infants = room.Guests.InfantsCount,
                    MealBasisID = room.ValuationResult.BoardCode,
                    RoomID = room.ValuationResult.RoomCode,
                    Guests = GetBookGuests(room)
                });
            }

            return lstRooms.ToArray();

        }

        private BookRequestBookDetailsLeadGuest GetLeadGuest(AccommodationProviderBookingRequestRoom room)
        {
            var firstAdlt = room.Guests.Where(g => g.Type == GuestType.Adult).First();
            var leadGst = new BookRequestBookDetailsLeadGuest()
            {
                FirstName = firstAdlt.FirstName,
                LastName = firstAdlt.Surname,
                Title = firstAdlt.Title.ToString()
            };
            return leadGst;
        }

        private BookRequestBookDetailsRoomBookingGuest[] GetBookGuests(AccommodationProviderBookingRequestRoom room)
        {
            var lstGuests = new List<BookRequestBookDetailsRoomBookingGuest>();
            return CreateAdultList(room).Concat(CreateChildList(room)).ToArray();
        }

        private List<BookRequestBookDetailsRoomBookingGuest> CreateChildList(AccommodationProviderBookingRequestRoom room)
        {
            var childList = new List<BookRequestBookDetailsRoomBookingGuest>();

            foreach (var guest in room.Guests.Where(g => g.Type == GuestType.Child))
            {
                childList.Add(new BookRequestBookDetailsRoomBookingGuest
                {
                    Type = "Child",
                    FirstName = guest.FirstName,
                    LastName = guest.Surname,
                    Age = guest.Age
                });
            }

            return childList;
        }

        private List<BookRequestBookDetailsRoomBookingGuest> CreateAdultList(AccommodationProviderBookingRequestRoom room)
        {
            var adultList = new List<BookRequestBookDetailsRoomBookingGuest>();
            int intAdlt = 0;
            foreach (var guest in room.Guests.Where(g => g.Type == GuestType.Adult))
            {
                if (intAdlt != 0)
                {
                    adultList.Add(new BookRequestBookDetailsRoomBookingGuest
                    {
                        Type = "Adult",
                        FirstName = guest.FirstName,
                        LastName = guest.Surname
                    });
                }
                intAdlt = 1;
            }

            return adultList;
        }

    }
}
