﻿using System;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
using System.Linq;
//using AlphaRooms.Accommodation.Provider.RMIService.Request;
//using AlphaRooms.Accommodation.Provider.RMIService.Response;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Provider.RMIService;
namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIAvailabilityParser : IAccommodationAvailabilityParser<RMIAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public RMIAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            RMIAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("RMI Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, RMIAvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);
            byte roomNo = 1;
            if (hasResults)
            {
                var hotelList = supplierResponse.SupplierResponse.PropertyResults;
                foreach (var hotel in hotelList)
                {
                    foreach (var roomInfo in hotel.RoomTypes)
                    {
                        if (roomInfo.OnRequest == "False")
                        {
                            var result = new AccommodationProviderAvailabilityResult();

                            result.RoomNumber = roomNo;

                            result.DestinationEdiCode = hotel.Region;
                            result.EstablishmentEdiCode = hotel.PropertyID.ToString();
                            result.EstablishmentName = hotel.PropertyName;

                            result.RoomDescription = roomInfo.Name;
                            result.RoomCode = roomInfo.RoomID;

                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckOutDate;

                            result.BoardCode = roomInfo.MealBasisID.ToString();
                            result.BoardDescription = "";

                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.Total),
                                CurrencyCode = "USD"
                            };

                            result.CostPrice = result.SalePrice;

                            result.Adults = roomInfo.Adults;
                            result.Children = roomInfo.Children;
                            result.Infants = roomInfo.Infants;

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("RMIRoomTypeCode", roomInfo.RoomID.ToString());
                            result.ProviderSpecificData.Add("RMIBedBoardCode", roomInfo.MealBasisID.ToString());
                            result.ProviderSpecificData.Add("RMICurrencyCode", "USD");

                            availabilityResults.Enqueue(result);

                        }
                    }

                }
            }
        }
              
        private static bool HasResults(SearchResponse supplierResponse)
        {
            if (supplierResponse != null && supplierResponse.ReturnStatus.Success == "True")
            {
                var items = supplierResponse.PropertyResults;

                if (supplierResponse.PropertyResults.Any())
                    return true;
                else
                    return false;
            }

            return false;
        }

    }
}
