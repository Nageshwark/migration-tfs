﻿using AlphaRooms.Accommodation.Provider.RMIService;


namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMICancellationResponse
    {
        public CancelRequest SupplierRequest { get; set; }
        public CancelResponse SupplierResponse { get; set; }
    }
}
