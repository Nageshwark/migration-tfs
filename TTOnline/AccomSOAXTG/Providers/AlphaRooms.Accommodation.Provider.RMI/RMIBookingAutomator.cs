﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
//using AlphaRooms.Accommodation.Provider.RMIService.Request;
//using AlphaRooms.Accommodation.Provider.RMIService.Response;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.RMIService;
using AlphaRooms.Accommodation.Provider.RMI.Factories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIBookingAutomator : RMIProviderFactoryBase, IAccommodationBookingAutomatorAsync<RMIBookingResponse>
    {
        private const string RMIBookingUrl = "RMIBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IRMIBookingRequestFactory _RMISupplierBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public RMIBookingAutomator(ILogger logger,
            // IAccommodationConfigurationManager configurationManager,
                                            IRMIBookingRequestFactory RMISupplierBookingRequestFactory
            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._RMISupplierBookingRequestFactory = RMISupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }


        private async Task<RMIBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, BookRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(RMIBookingUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<BookResponse>();

            var supplierResponse = new RMIBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }


        public async Task<RMIBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<RMIBookingResponse> responses = new List<RMIBookingResponse>();
            RMIBookingResponse response = null;
            try
            {
                BookRequest resRequest = _RMISupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
                response = await GetSupplierBookingResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("RMIBookingAutomator.GetBookingResponseAsync: RMI supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }
    }
}
