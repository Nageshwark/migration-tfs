﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.RMI;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<RMIValuationResponse> automator;
        private readonly IAccommodationValuationParser<RMIValuationResponse> parser;

        public RMIValuationProvider(IAccommodationValuationAutomatorAsync<RMIValuationResponse> automator,IAccommodationValuationParser<RMIValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
