﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
//using Ninject;
//using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<RMIBookingResponse> automator;
        private readonly IAccommodationBookingParser<RMIBookingResponse> parser;

        public RMIBookingProvider(IAccommodationBookingAutomatorAsync<RMIBookingResponse> automator,
                                        IAccommodationBookingParser<RMIBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
