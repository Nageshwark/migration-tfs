﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.RMI;
using AlphaRooms.Accommodation.Provider.RMIService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.RMI
{

    public class RMIValuationParser : IAccommodationValuationParser<RMIValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public RMIValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, RMIValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("RMI Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, RMIValuationResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);
            byte roomNo = 1;
            if (hasResults)
            {
                var hotelList = supplierResponse.SupplierResponse.PropertyResults;
                foreach (var hotel in hotelList)
                {
                    foreach (var roomInfo in hotel.RoomTypes)
                    {
                        if (roomInfo.OnRequest == "False" && roomInfo.RoomID == request.SelectedRooms[0].AvailabilityResult.RoomCode)
                        {
                            var result = new AccommodationProviderValuationResult();

                            result.RoomNumber = roomNo;

                            result.DestinationEdiCode = hotel.Region;
                            result.EstablishmentEdiCode = hotel.PropertyID.ToString();
                            result.EstablishmentName = hotel.PropertyName;

                            result.RoomDescription = roomInfo.Name;
                            result.RoomCode = roomInfo.RoomID.ToString();

                            result.CheckInDate = request.SelectedRooms[0].AvailabilityRequest.CheckInDate;
                            result.CheckOutDate = request.SelectedRooms[0].AvailabilityRequest.CheckOutDate;

                            result.BoardCode = roomInfo.MealBasisID.ToString();
                            result.BoardDescription = "";

                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.Total),
                                CurrencyCode = "USD"
                            };

                            result.CostPrice = result.SalePrice;

                            result.Adults = roomInfo.Adults;
                            result.Children = roomInfo.Children;
                            result.Infants = roomInfo.Infants;

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("RMIRoomTypeCode", roomInfo.RoomID.ToString());
                            result.ProviderSpecificData.Add("RMIBedBoardCode", roomInfo.MealBasisID.ToString());
                            result.ProviderSpecificData.Add("RMICurrencyCode", "USD");

                            availabilityResults.Enqueue(result);

                        }
                    }

                }
            }
        }

        private static bool HasResults(SearchResponse supplierResponse)
        {
            if (supplierResponse != null && supplierResponse.ReturnStatus.Success == "True")
            {
                var items = supplierResponse.PropertyResults;

                if (supplierResponse.PropertyResults.Any())
                    return true;
                else
                    return false;
            }

            return false;
        }
    }
}
