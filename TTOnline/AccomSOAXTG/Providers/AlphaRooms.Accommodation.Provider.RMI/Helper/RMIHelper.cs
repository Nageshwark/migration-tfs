﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.RMI.Factories;
using AlphaRooms.Accommodation.Provider.RMIService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.RMI.Helper
{
    public class RMIHelper : RMIProviderFactoryBase
    {
       public enum Reqtype
        {
            Search,
            Valuate
        }

        public SearchRequestSearchDetails GetSearchDetails(AccommodationProviderAvailabilityRequest request)
        {
            return GetSearchDetails(request,Reqtype.Search);
        }

        public SearchRequestSearchDetails GetSearchDetails(AccommodationProviderValuationRequest request)
        {
            return GetSearchDetails(request, Reqtype.Valuate);
        }

        private SearchRequestSearchDetails GetSearchDetails(object request,Reqtype reqtype)
        {
            var avilRequest = new AccommodationProviderAvailabilityRequest();

            if (reqtype == Reqtype.Search)
                avilRequest = (AccommodationProviderAvailabilityRequest)request;
            else
                avilRequest = ((AccommodationProviderValuationRequest)request).SelectedRooms.First().AvailabilityRequest;
            
            var srchDtls = new SearchRequestSearchDetails()
            {
                ArrivalDate = avilRequest.CheckInDate.ToString("yyyy-MM-dd"),
                Duration = (byte)(avilRequest.CheckOutDate - avilRequest.CheckInDate).TotalDays,
                MealBasisID = GetMealBasisId(request, reqtype),
                RoomRequests = GetRoomInfoArray(avilRequest.Rooms)
            };

            if (avilRequest.DestinationCodes != null && avilRequest.DestinationCodes.Any())
            {
                srchDtls.RegionID = GetDestination(avilRequest);
            }
            else if (avilRequest.EstablishmentCodes.Any())
            {
                srchDtls.PropertyID = GetHotelCodes(avilRequest);
            }
            else
            {
                throw new Exception("Plase provide DestinationCode or EstablishmentCode");
            }

            return srchDtls;
        }

        private string GetMealBasisId(object request, Reqtype reqtype)
        {
            string mealId = "0";
            if (reqtype == Reqtype.Search)
                mealId = "0";
            else
            {
                var availResult = ((AccommodationProviderValuationRequest)request).SelectedRooms.First().AvailabilityResult;
                mealId = availResult.BoardCode;
            }
            return mealId;
        }

        public SearchRequestSearchDetailsRoomRequest[] GetRoomInfoArray(AccommodationProviderAvailabilityRequestRoom [] rooms)
        {
            var roomInfo = new List<SearchRequestSearchDetailsRoomRequest>();
            foreach (var room in rooms)
            {
                roomInfo.Add(new SearchRequestSearchDetailsRoomRequest()
                {
                    Adults = room.Guests.AdultsCount,
                    Children = room.Guests.ChildrenCount,
                    Infants = room.Guests.InfantsCount,
                    ChildAges = GetChildrenAges(room)
                });
            }

            return roomInfo.ToArray();
        }

        public SearchRequestSearchDetailsRoomRequestChildAge[] GetChildrenAges(AccommodationProviderAvailabilityRequestRoom room)
        {
            var lstChildAges = new List<SearchRequestSearchDetailsRoomRequestChildAge>();

            if (room.Guests.ChildrenCount > 0)
            {
                for (int intChld = 0; intChld <= room.Guests.ChildrenCount - 1; intChld++)
                {
                    lstChildAges.Add(new SearchRequestSearchDetailsRoomRequestChildAge()
                    {
                        Age = room.Guests.ChildAndInfantAges[intChld]
                    });
                }
            }

            return lstChildAges.ToArray();
        }

        public string[] GetHotelCodes(AccommodationProviderAvailabilityRequest request)
        {
            string[] hotelCodes = new string[0];
            int count = 0;
            if (request.EstablishmentCodes.Any())
            {
                hotelCodes = new string[request.EstablishmentCodes.Length];
                foreach (string code in request.EstablishmentCodes)
                {
                    hotelCodes[count] = code;
                    count++;
                }
            }

            return hotelCodes;
        }

        public string GetDestination(AccommodationProviderAvailabilityRequest request)
        {
            if (request.DestinationCodes.Any())
            {
                return request.DestinationCodes[0];
            }
            return "";
        }
    }
}
