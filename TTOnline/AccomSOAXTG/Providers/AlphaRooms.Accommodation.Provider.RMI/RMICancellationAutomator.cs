﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.RMI.Factories;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.RMIService;
//using AlphaRooms.Accommodation.Provider.RMIService.Request;
//using AlphaRooms.Accommodation.Provider.RMIService.Response;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMICancellationAutomator : RMIProviderFactoryBase, IAccommodationCancellationAutomatorAsync<RMICancellationResponse>
    {
        private const string RMICancellationUrl = "RMICancellationUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IRMIProviderCancellationRequestFactory RMIProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public RMICancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IRMIProviderCancellationRequestFactory RMISupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.RMIProviderCancellationRequestFactory = RMISupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<RMICancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            List<RMICancellationResponse> responses = new List<RMICancellationResponse>();
            RMICancellationResponse response = null;
            try
            {
                CancelRequest resRequest = RMIProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                response = await GetSupplierCancellationResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("RMICancellationAutomator.GetCancellationResponseAsync: RMI supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<RMICancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, CancelRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(RMICancellationUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<CancelResponse>();

            var supplierResponse = new RMICancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
