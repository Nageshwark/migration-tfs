﻿using AlphaRooms.Accommodation.Provider.RMIService;


namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIValuationResponse
    {
        public SearchRequest SupplierRequest { get; set; }
        public SearchResponse SupplierResponse { get; set; }
    }
}
