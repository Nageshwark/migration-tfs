﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System.Web;
using AlphaRooms.Accommodation.Provider.RMI.Factories;
using AlphaRooms.Accommodation.Provider.RMIService;

namespace AlphaRooms.Accommodation.Provider.RMI
{
    public class RMIAvailabilityAutomator : RMIProviderFactoryBase, IAccommodationAvailabilityAutomatorAsync<RMIAvailabilityResponse>
    {
        private const string RMIAvailabilityUrl = "RMIAvailabilityUrl";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IRMIAvailabilityRequestFactory _RMISupplierAvailabilityRequestFactory;


        public RMIAvailabilityAutomator(IRMIAvailabilityRequestFactory RMIAvailabilityRequestFactory)
        {
            _RMISupplierAvailabilityRequestFactory = RMIAvailabilityRequestFactory;
        }

        public async Task<RMIAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            RMIAvailabilityResponse response = null;

            try
            {
                SearchRequest supplierRequest = this._RMISupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("RMIAvailabilityAutomator.GetAvailabilityResponseAsync: RMI supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<RMIAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, SearchRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(RMIAvailabilityUrl);

            string supplierRequestXml = supplierRequest.XmlSerialize();

            supplierRequestXml = supplierRequestXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            var responseXml = PostXmlRequest(url, supplierRequestXml);

            var response = responseXml.Result.XmlDeSerialize<SearchResponse>();

            var supplierResponse = new RMIAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

    }
}
