﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Helper
{
    public class BoardBasisMapper
    {
        private Dictionary<string, string> boardBasis; 

        public BoardBasisMapper()
        {
            boardBasis = new Dictionary<string, string>();
            boardBasis.Add("RO", "Room Only");
            boardBasis.Add("SC", "Self Catering");
            boardBasis.Add("HB", "Half Board");
            boardBasis.Add("FB", "Full Board");
            boardBasis.Add("BB", "Bed & Breakfast");
            boardBasis.Add("AI", "All Inclusive");
        }

        public string GetBoardBasisName(string boardCode)
        {
            if (boardBasis.ContainsKey(boardCode))
            {
                return boardBasis[boardCode];
            }
            else
            {
                return "";
            }
        }
    }
}
