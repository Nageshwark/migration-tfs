﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Helper
{
    public class ResponseHelper
    {
        public bool HasResResults(OTA_CancelRS supplierResponse)
        {
            if (supplierResponse != null 
                && supplierResponse.Items != null 
                && supplierResponse.Items.Any() 
                && !GetItemByType(supplierResponse.Items, typeof(ErrorType[])).Any())
            {

                return true;
            }

            return false;
        }

        public IEnumerable<object> GetItemByType(object [] itemsArray, Type type)
        {
            return from item in itemsArray
                   where item.GetType() == type
                   select item;
        }
    }
}
