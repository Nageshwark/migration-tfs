﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Helper
{
    public class DataCleanser
    {
        public string CleanRoomDescription(string description)
        {
            description = Regex.Replace(description, "All-Inclusive", "All Inclusive", RegexOptions.IgnoreCase);
              
            int pos = description.LastIndexOf("-");

            if (pos > -1)
            {
                return description.Substring(0, pos).Trim();
            }

            return description;
        }

        public string CleanBoardDescription(string description)
        {
            description = Regex.Replace(description, "All-Inclusive", "All Inclusive", RegexOptions.IgnoreCase);

            int pos = description.LastIndexOf("-");

            if (pos > -1)
            {
                return description.Remove(0, pos+1).Trim();
            }

            return description;
        }
        /*
         * Private Shared Function getRoomDescription(description As String) As String
        Dim l As Integer = description.LastIndexOf("-")
        If (l <> 0) Then
        Return description.Substring(0, l)
        Else
        Return description
        End If
        End Function

        Private Shared Function getBoardDescription(description As String) As String
        Dim l As Integer = description.LastIndexOf("-")
        If (l <> 0) Then
        Return description.Remove(0, l + 1)
        Else
        Return "Room Only"
        End If
        End Function
         */
    }
}
