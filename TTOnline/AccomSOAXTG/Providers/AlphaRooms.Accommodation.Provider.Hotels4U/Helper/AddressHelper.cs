﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Helper
{
    public class AddressHelper
    {
        public Address GetAddress(string address)
        {
            return new JavaScriptSerializer().Deserialize<Address>(address);
        }

    }

    public class Address
    {
        public string AddressLine { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }

    }
}
