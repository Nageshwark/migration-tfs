﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Helper;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UAvailabilityParser:IAccommodationAvailabilityParser<Hotels4UAvailabilityResponse>
    {
        private readonly ILogger logger;
        private readonly IProviderNonRefundableService nonRefundableService;

        public Hotels4UAvailabilityParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.logger = logger;
            this.nonRefundableService = nonRefundableService;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            Hotels4UAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotels4U Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRQ supplierRequest, Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRS supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse);
            var dataCleanser = new DataCleanser();
            if (hasResults)
            {
                OTA_HotelAvailRSRoomStays  roomStays = GetRoomStays(supplierResponse.Items);

                //int i = 0;

                Parallel.ForEach(roomStays.RoomStay, roomStay =>
                {
                    int roomNumber = 1;
                    Sync.ParallelForEachIgnoreFailed(roomStay.RoomTypes, roomType =>
                    {
                        var result = new AccommodationProviderAvailabilityResult();

                        //result.Id = new Guid();
                        result.RoomNumber = 1;

                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.SupplierEdiCode = request.Provider.EdiCode; // This must be provider's edi code for non technological providers.

                        result.PaymentModel = PaymentModel.PostPayment;

                        //TODO:What should it be if response does not contain destination code?
                        if (request.DestinationCodes.Length > 0)
                            result.DestinationEdiCode = request.DestinationCodes[0];
                        else if (request.EstablishmentCodes.Length > 0)
                            result.DestinationEdiCode = request.EstablishmentCodes[0];
                        
                        if (roomStay.BasicPropertyInfo.Address != null)
                            result.DestinationEdiCode = roomStay.BasicPropertyInfo.Address.CityName;

                        result.EstablishmentEdiCode = string.Format("{0};{1}", roomStay.BasicPropertyInfo.ChainCode,
                            roomStay.BasicPropertyInfo.HotelCode);
                        result.EstablishmentName = roomStay.BasicPropertyInfo.HotelName;

                        string roomDescription = ((FormattedTextTextType) (roomType.RoomDescription.Items[0])).Value;

                        result.RoomDescription = dataCleanser.CleanRoomDescription(roomDescription);
                        result.RoomCode = roomType.RoomTypeCode;

                        result.CheckInDate = request.CheckInDate;
                        result.CheckOutDate = request.CheckOutDate;

                        

                        //For Hotels4U own contracted stock Board Code is Last two Charecters of the RoomCode
                        if (roomStay.BasicPropertyInfo.ChainCode.Equals("H4U", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var bbMapper = new BoardBasisMapper();
                            result.BoardCode = roomType.RoomTypeCode.Substring(roomType.RoomTypeCode.Length - 2);
                            result.BoardDescription = bbMapper.GetBoardBasisName(result.BoardCode);
                        }
                        else
                        {
                            result.BoardDescription = dataCleanser.CleanBoardDescription(roomDescription);
                        }

                        //result.BoardDescription = "Do we try to parse it out of the Room Text?";
                        result.ProviderSpecificData = new Dictionary<string, string>();
                        
                        var rateQuery = from RoomRateType r in roomStay.RoomRates
                            where r.RoomTypeCode == roomType.RoomTypeCode
                            select r;

                        if (rateQuery.Any())
                        {
                            var rate = rateQuery.First().Rates.First();

                            result.SalePrice = new Money()
                            {
                                Amount = rate.Total.AmountAfterTax,
                                CurrencyCode = rate.Total.CurrencyCode
                            };

                            //what should we populate here? there is only one price in H4U response.
                            result.CostPrice = result.SalePrice;

                            result.RateType = RateType.NetStandard;

                            if (rate.Discount != null)
                            {
                                result.ProviderSpecificData.Add("DiscountCode", rate.Discount.FirstOrDefault().DiscountCode);
                                result.ProviderSpecificData.Add("DiscountReason", rate.Discount.FirstOrDefault().DiscountReason.Items.First().ToString());
                            }

                            if (rate.TPA_Extensions != null)
                            {
                               result.ProviderSpecificData.Add("WasPrice", rate.TPA_Extensions.Any.FirstOrDefault().InnerText);
                            }
                        }
                        else
                        {
                            throw new SupplierApiDataException("No rate found");
                        }

                        //TODO:Confirm why we need Adult/Children? is that MaxAdult/MaxChild
                        result.Adults = request.Rooms.First().Guests.AdultsCount;
                        result.Children = (byte)(request.Rooms.First().Guests.ChildrenCount + request.Rooms.First().Guests.InfantsCount);
                        
                        if (!string.IsNullOrEmpty(roomType.Quantity))
                            result.NumberOfAvailableRooms = Convert.ToByte(roomType.Quantity);

                        result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(result.RoomDescription, result.BoardDescription, result.IsNonRefundable);


                        availabilityResults.Enqueue(result);

                        roomNumber++;
                    });
                });

            }
            //else
            //{
            //    string message = "Hotels4U Availability Parser: Availability Response cannot be parsed because it is null.";

                

            //    throw new SupplierApiDataException(message);
            //}
            
        }

        private  bool HasResults(OTA_HotelAvailRS supplierResponse)
        {
            return (supplierResponse != null && supplierResponse.Items != null && supplierResponse.Items.Any() && GetRoomStays(supplierResponse.Items)!= null);
        }

        private OTA_HotelAvailRSRoomStays GetRoomStays(object[] roomStays)
        {
            var query = from rs in roomStays
                where rs.GetType() == typeof (OTA_HotelAvailRSRoomStays)
                select rs;

            if (query != null && query.Any())
                return (OTA_HotelAvailRSRoomStays) query.First();
            else
                return null;
        }
    }
}
