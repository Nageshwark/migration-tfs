﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Discovery;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<Hotels4UAvailabilityResponse>
    {
        private const string Hotels4UAvailabilityUrl = "Hotels4UAvailabilityUrl";
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IHotels4UAvailabilityRequestFactory hotels4USupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public Hotels4UAvailabilityAutomator(IHotels4UAvailabilityRequestFactory hotels4UAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this.hotels4USupplierAvailabilityRequestFactory = hotels4UAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<Hotels4UAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            Hotels4UAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                OTA_HotelAvailRQ supplierRequest = this.hotels4USupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("Hotels4UAvailabilityAutomator.GetAvailabilityResponseAsync: Hotels4U supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<Hotels4UAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, OTA_HotelAvailRQ  supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            SoapDispatcherSoapClient client = ConfigureSoapCleint(request);
            
          
            OTA_Header header = new OTA_Header();
            header.userid = request.Provider.Parameters.GetParameterValue(Hotels4UUsername);
            header.password = request.Provider.Parameters.GetParameterValue(Hotels4UPassword);

            OTA_HotelAvailRS response = client.sendOTAHotelAvail(header, supplierRequest);
            outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, response);

            Hotels4UAvailabilityResponse supplierResponse = new Hotels4UAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private SoapDispatcherSoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(Hotels4UAvailabilityUrl))); 
            SoapDispatcherSoapClient client =new SoapDispatcherSoapClient(binding, address);

            return client;
        }

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            if (request.Rooms.Count() > 3)
                throw new ApplicationException("Hotels4U supports upto 3 Rooms");

            foreach (var room in request.Rooms)
            {
                if (room.Guests.AdultsCount > 6)
                    throw new ApplicationException("Hotels4U supports upto 6 Adults");

                if (room.Guests.ChildrenCount  > 4)
                    throw new ApplicationException("Hotels4U supports upto 4 Children");

                if (room.Guests.InfantsCount > 2)
                    throw new ApplicationException("Hotels4U supports upto 2 Infants");
            }
        }
    }
}
