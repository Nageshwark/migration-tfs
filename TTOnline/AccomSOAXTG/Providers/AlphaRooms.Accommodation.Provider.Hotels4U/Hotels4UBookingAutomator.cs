﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<Hotels4UBookingResponse>>
    {
        private const string Hotels4UBookingUrl = "Hotels4UBookingUrl";
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotels4UProviderBookingRequestFactory hotels4UProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public Hotels4UBookingAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotels4UProviderBookingRequestFactory Hotels4USupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger,
                                            IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.hotels4UProviderBookingRequestFactory = Hotels4USupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<Hotels4UBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            var responses = new List<Hotels4UBookingResponse>();

            OTA_HotelResRQ supplierCommitRequest = null;
            Hotels4UBookingResponse response = null;


            try
            {
                //Step 1: Booking Initiate
                var supplierInitiateRequest = this.hotels4UProviderBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);

                if (request.Debugging)
                {
                    string serialisedRequestXML = supplierInitiateRequest.XmlSerialize();
                    string serialisedRequestJSON = supplierInitiateRequest.ToIndentedJson();
                }

                var supplierInitiateResponse = await GetSupplierBookingInitiateResponseAsync(request, supplierInitiateRequest, false);

                //Step 2: Booking Commit
                if (supplierInitiateResponse.Items[0].GetType() == typeof(SuccessType))
                {
                    var res = (HotelReservationsType)supplierInitiateResponse.Items[1];
                    string resId = res.HotelReservation.First().ResGlobalInfo.HotelReservationIDs.First().ResID_Value;

                    supplierCommitRequest = this.hotels4UProviderBookingRequestFactory.CreateSupplierBookingCommitRequest(request, resId);

                    var supplierCommitResponse = await GetSupplierBookingInitiateResponseAsync(request, supplierCommitRequest, true);

                    var bookingInit = new Hotels4UBookingResponseBase
                    {
                        SupplierRequest = supplierInitiateRequest,
                        SupplierResponse = supplierInitiateResponse
                    };

                    var bookingCommit = new Hotels4UBookingResponseBase
                    {
                        SupplierRequest = supplierCommitRequest,
                        SupplierResponse = supplierCommitResponse
                    };

                    response = new Hotels4UBookingResponse
                    {
                        BookingInit = bookingInit,
                        BookingCommit = bookingCommit
                    };

                    responses.Add(response);

                }
                else if (supplierInitiateResponse.Items[0].GetType() == typeof(ErrorsType))
                {
                    var errors = (ErrorsType)supplierInitiateResponse.Items[0];

                    StringBuilder sbError = new StringBuilder();

                    foreach (var error in errors.Error)
                    {
                        sbError.AppendLine(string.Format("Error Code: {0}, Type: {1}, Tex: {2} ", error.Code, error.Type, error.ShortText));
                    }

                    var sb = new StringBuilder();

                    if (supplierInitiateResponse != null)
                    {
                        sb.AppendLine();
                        sb.AppendLine("Request:");
                        sb.AppendLine(supplierInitiateRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Response:");
                        sb.AppendLine(supplierInitiateResponse.XmlSerialize());
                        sb.AppendLine();
                    }

                    throw new SupplierApiException(string.Format("Hotels4UBookingAutomator.GetBookingResponseAsync: Hotels4U supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                                  Environment.NewLine,
                                                                  Environment.NewLine, sbError.ToString(), Environment.NewLine, sb.ToString(), Environment.NewLine));

                }


                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.Hotels4UProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingInitiateResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(request.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("Hotels4UBookingAutomator.GetBookingResponseAsync: Hotels4U supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<OTA_HotelResRS> GetSupplierBookingInitiateResponseAsync(AccommodationProviderBookingRequest request, OTA_HotelResRQ supplierRequest, bool isCommit)
        {
            SoapDispatcherSoapClient client = ConfigureSoapCleint(request);


            OTA_Header header = new OTA_Header();
            header.userid = request.Provider.Parameters.GetParameterValue(Hotels4UUsername);
            header.password = request.Provider.Parameters.GetParameterValue(Hotels4UPassword);

            string requestXML = supplierRequest.XmlSerialize();

            this.loggerService.SetProviderRequest(request, supplierRequest);
            OTA_HotelResRS response = client.sendOTAHotelRes(header, supplierRequest);
            this.loggerService.SetProviderResponse(request, response);

            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response);



            return response;
        }


        private SoapDispatcherSoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(Hotels4UBookingUrl)));
            SoapDispatcherSoapClient client = new SoapDispatcherSoapClient(binding, address);

            return client;
        }
    }
}
