﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Factories
{
    public class Hotels4UCancellationRequestFactory : IHotels4UProviderCancellationRequestFactory
    {
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";

        public OTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new OTA_CancelRQ
            {
                CancelType = TransactionActionType.Cancel,
                Version = 1,
                POS = new[]
                {
                    new SourceType
                    {
                        AgentSine = request.Provider.Parameters.GetParameterValue(Hotels4UUsername),
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(Hotels4UPassword)
                    }
                },
               
                UniqueID = new[]
                {
                    new UniqueID_Type
                    {
                        Type = "14",
                        ID = request.ProviderBookingReference
                    }
                }

            };

            return supplierRequest;
        }
    }
}
