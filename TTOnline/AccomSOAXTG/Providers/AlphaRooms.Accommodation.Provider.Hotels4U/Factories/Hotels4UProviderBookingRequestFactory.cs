﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Helper;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UProviderBookingRequestFactory : IHotels4UProviderBookingRequestFactory
    {
        private const string Hotels4UApiVersion = "Hotels4UApiVersion";
        private const string Hotels4UDefaultLanguage = "Hotels4UDefaultLanguage";
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";
        private const string Hotels4uBookingConfirmationEmail = "Hotels4uBookingConfirmationEmail";
        private const string Hotels4uAlpharoomsAddress = "Hotels4uAlpharoomsDefaultAddress";
        private const string Hotels4uB2BAddress = "Hotels4uB2BAddress";
        private readonly IProviderSpecialRequestService specialRequestService;
        private readonly IHotels4UHelperService helperService;

        public Hotels4UProviderBookingRequestFactory(IHotels4UHelperService helperService, IProviderSpecialRequestService specialRequestService)
        {
            this.helperService = helperService;
            this.specialRequestService = specialRequestService;
        }

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public OTA_HotelResRQ CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new OTA_HotelResRQ();

            supplierRequest.ResStatus = TransactionActionType.Initiate;
            supplierRequest.Version = 1;

            supplierRequest.POS = new SourceType[1];
            supplierRequest.POS[0] = new SourceType()
            {
                AgentSine = request.Provider.Parameters.GetParameterValue(Hotels4UUsername),
                AgentDutyCode = request.Provider.Parameters.GetParameterValue(Hotels4UPassword)
            };

            var hotelReservations = new HotelReservationsType();
            var hotelReservation = new HotelReservationType();
            hotelReservations.HotelReservation = new HotelReservationType[1];

            hotelReservation.RoomStays = new HotelReservationTypeRoomStays();
            hotelReservation.RoomStays.RoomStay = CreateRoomStay(request);

            hotelReservation.ResGuests = CreateResGuests(request);

            hotelReservations.HotelReservation[0] = hotelReservation;

            supplierRequest.HotelReservations = hotelReservations;



            return supplierRequest;
        }

        public OTA_HotelResRQ CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string resId)
        {
            var supplierRequest = new OTA_HotelResRQ();

            supplierRequest.ResStatus = TransactionActionType.Commit;
            supplierRequest.Version = 1;

            supplierRequest.POS = new SourceType[1];
            supplierRequest.POS[0] = new SourceType()
            {
                AgentSine = request.Provider.Parameters.GetParameterValue(Hotels4UUsername),
                AgentDutyCode = request.Provider.Parameters.GetParameterValue(Hotels4UPassword)
            };

            var hotelReservations = new HotelReservationsType();
            var hotelReservation = new HotelReservationType();
            var resGlobalInfo = new ResGlobalInfoType();

            hotelReservations.HotelReservation = new HotelReservationType[1];

            resGlobalInfo.HotelReservationIDs = new HotelReservationIDsTypeHotelReservationID[1];
            resGlobalInfo.HotelReservationIDs[0] = new HotelReservationIDsTypeHotelReservationID
            {
                ResID_Type = "23",
                ResID_Value = resId
            };

            hotelReservation.ResGlobalInfo = resGlobalInfo;

            hotelReservations.HotelReservation[0] = hotelReservation;

            supplierRequest.HotelReservations = hotelReservations;

            return supplierRequest;

        }

        private ResGuestsTypeResGuest[] CreateResGuests(AccommodationProviderBookingRequest request)
        {
            var resGuests = new ResGuestsTypeResGuest[1];
            var resGuest = new ResGuestsTypeResGuest();
            var profiles = new ProfilesTypeProfileInfo[1];
            var profile = new ProfilesTypeProfileInfo();
            var customer = new CustomerType();

            resGuests[0] = resGuest;
            resGuest.Profiles = profiles;
            resGuest.Profiles[0] = profile;

            if ((request.ValuatedRooms == null || request.ValuatedRooms.Length == 0) || (request.ValuatedRooms[0].Guests==null || !request.ValuatedRooms[0].Guests.Any()))
            {
                customer.PersonName = new PersonNameType
                {
                    NameTitle = new string[] { request.Customer.Title.ToString() },
                    GivenName = new string[] { request.Customer.FirstName },
                    Surname = request.Customer.Surname
                };
            }
            else
            {
                customer.PersonName = new PersonNameType
                {
                    NameTitle = new string[] { request.ValuatedRooms[0].Guests[0].Title.ToString() },
                    GivenName = new string[] { request.ValuatedRooms[0].Guests[0].FirstName },
                    Surname = request.ValuatedRooms[0].Guests[0].Surname
                };
            }

            customer.Telephone = new CustomerTypeTelephone[1];
            customer.Telephone[0] = new CustomerTypeTelephone
            {
                PhoneNumber = request.Customer.ContactNumber
            };

            customer.Email = new EmailType[1];
            customer.Email[0] = new EmailType { Value = request.Provider.Parameters.GetParameterValue(Hotels4uBookingConfirmationEmail) };

            var card = request.ValuatedRooms.First().PaymentDetails;
            customer.Address = new CustomerTypeAddress[1];
            if (card != null)
            {
               
                customer.Address[0] = new CustomerTypeAddress
                {
                    AddressLine = new[] { card.AddressLine1 },
                    CityName = card.City,
                    PostalCode = card.Postcode,
                    CountryName = new CountryNameType
                    {
                        Code = card.Country
                    }
                };
            }
            else
            {
               
                //Check if Alpharooms or B2B
                var channelInfo = request.ChannelInfo;
                Address address;
                switch (channelInfo.Channel)
                {
                    case Channel.AlphaRoomsUK:
                    case Channel.AlphaRoomsIE:
                    case Channel.AlphaRoomsUS:
                    case Channel.TeletextHolidaysUK:
                        address = new AddressHelper().GetAddress(
                            request.Provider.Parameters.GetParameterValue(Hotels4uAlpharoomsAddress));
                        break;

                    case Channel.BetaBedsUK:
                    case Channel.BetaBedsIE:
                    case Channel.BetaBedsUS:
                       address = new AddressHelper().GetAddress(
                           request.Provider.Parameters.GetParameterValue(Hotels4uB2BAddress));
                        break;
                   
                    default:
                        throw new SupplierApiException("Unknow Channel");

                }
              
                customer.Address[0] = new CustomerTypeAddress
                {
                    AddressLine = new[] {address.AddressLine}, CityName = address.CityName, PostalCode = address.PostalCode, CountryName = new CountryNameType
                    {
                        Code = address.CountryCode
                    }
                };
            }

            profile.Profile = new ProfileType
            {
                Customer = customer
            };

            return resGuests;
        }


        private HotelReservationTypeRoomStaysRoomStay[] CreateRoomStay(AccommodationProviderBookingRequest request)
        {
            var roomStay = new List<HotelReservationTypeRoomStaysRoomStay>();


            int counter = 0;
            foreach (var room in request.ValuatedRooms)
            {
                var roomStayCandidate = new HotelReservationTypeRoomStaysRoomStay();

                CreatePAX(roomStayCandidate, room);

                //strangely you only need to add RoomTypeCode and Property info for the first RoomStay
                if (counter == 0)
                {
                    roomStayCandidate.RoomTypes = new RoomTypeType[1];
                    roomStayCandidate.RoomTypes[0] = new RoomTypeType
                    {
                        RoomTypeCode = room.ValuationResult.RoomCode
                    };

                    roomStayCandidate.BasicPropertyInfo = new BasicPropertyInfoType
                    {
                        ChainCode = room.ValuationResult.EstablishmentEdiCode.Split(';')[0], HotelCode = room.ValuationResult.EstablishmentEdiCode.Split(';')[1]
                    };

                    if (request.ValuatedRooms.First().SpecialRequests != null)
                        roomStayCandidate.SpecialRequests = CreateSpecialRequest(request);

                    roomStayCandidate.BasicPropertyInfo = new BasicPropertyInfoType
                    {
                        ChainCode = room.ValuationResult.EstablishmentEdiCode.Split(';')[0], HotelCode = room.ValuationResult.EstablishmentEdiCode.Split(';')[1]
                    };

                    roomStayCandidate.TimeSpan = new DateTimeSpanType
                    {
                        Start = room.AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"), Duration = helperService.CalculateDuration(room.AvailabilityRequest.CheckInDate, room.AvailabilityRequest.CheckOutDate)
                    };
                }

                roomStay.Add(roomStayCandidate);

                counter++;
            }


            return roomStay.ToArray();
        }

        private void CreatePAX(HotelReservationTypeRoomStaysRoomStay roomStayCandidate, AccommodationProviderBookingRequestRoom room)
        {
            roomStayCandidate.GuestCounts = new GuestCountType();

            roomStayCandidate.GuestCounts.GuestCount = helperService.CalculateGuestCount(room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount);

            int counter = 0;
            //Assuming that there will be always one adult in a room
            roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
            {
                AgeQualifyingCode = "10", Count = room.Guests.AdultsCount.ToString()
            };
            counter++;

            if (room.Guests.ChildrenAndInfantsCount > 0)
            {
                foreach (var childAndInfantAge in room.Guests.ChildAndInfantAges)
                {
                    roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
                    {
                        AgeQualifyingCode = childAndInfantAge < 2 ? "7" : "8", Age = childAndInfantAge.ToString()
                    };
                    counter++;
                }
            }
        }

        private SpecialRequest[] CreateSpecialRequest(AccommodationProviderBookingRequest request)
        {
            string specialRequestString = specialRequestService.GetSpecialRequestString(request.ValuatedRooms.First().SpecialRequests);

            var specialRequests = new List<SpecialRequest>
            {
                new SpecialRequest
                {
                    ItemsElementName = new[]
                    {
                        ItemsChoiceType1.Text,
                    },
                    Items = new[]
                    {
                        new FormattedTextTextType()
                        {
                            Value = specialRequestString
                        }
                    }
                }
            };

            return specialRequests.ToArray();
        }
    }
}
