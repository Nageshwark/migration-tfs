﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AvailRequestSegmentsType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.AvailRequestSegmentsType;
using AvailRequestSegmentsTypeAvailRequestSegment = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.AvailRequestSegmentsTypeAvailRequestSegment;
using DateTimeSpanType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.DateTimeSpanType;
using GuestCountType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.GuestCountType;
using GuestCountTypeGuestCount = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.GuestCountTypeGuestCount;
using HotelSearchCriterionType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.HotelSearchCriterionType;
using ItemSearchCriterionTypeHotelRef = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.ItemSearchCriterionTypeHotelRef;
using OTA_HotelAvailRQ = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRQ;
using RoomStayCandidateType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.RoomStayCandidateType;
using SourceType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.SourceType;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Factories
{
    public class Hotels4UAvailabilityRequestFactory : IHotels4UAvailabilityRequestFactory
    {
        private readonly IHotels4UHelperService helperService;
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";

        public Hotels4UAvailabilityRequestFactory(IHotels4UHelperService helperService)
        {
            this.helperService = helperService;
        }

        public OTA_HotelAvailRQ CreateSupplierAvailabilityRequest(
            AccommodationProviderAvailabilityRequest request)
        {
            OTA_HotelAvailRQ supplierRequest = new OTA_HotelAvailRQ();

            supplierRequest.Version = 1;
            supplierRequest.POS = new SourceType[1];
            supplierRequest.POS[0] = new SourceType()
            {
                AgentSine = request.Provider.Parameters.GetParameterValue(Hotels4UUsername), 
                AgentDutyCode = request.Provider.Parameters.GetParameterValue(Hotels4UPassword)
            };

            supplierRequest.AvailRequestSegments = new AvailRequestSegmentsType();
            AvailRequestSegmentsTypeAvailRequestSegment availRequestSegment = new AvailRequestSegmentsTypeAvailRequestSegment();
            availRequestSegment.StayDateRange = new DateTimeSpanType()
            {
                Start = request.CheckInDate.ToString("yyyy-MM-dd"),
                Duration = helperService.CalculateDuration(request.CheckInDate, request.CheckOutDate)
            };

            availRequestSegment.RoomStayCandidates = CreateRoomStay(request);
            availRequestSegment.HotelSearchCriteria = CreateSearchCriteria(request);

            supplierRequest.AvailRequestSegments.AvailRequestSegment = new AvailRequestSegmentsTypeAvailRequestSegment[1];
            supplierRequest.AvailRequestSegments.AvailRequestSegment[0] = availRequestSegment;
            
            return supplierRequest;
        }

        private HotelSearchCriterionType[] CreateSearchCriteria(AccommodationProviderAvailabilityRequest request)
        {
            HotelSearchCriterionType[] searchCriterion = new HotelSearchCriterionType[1];
            
            searchCriterion[0] = new HotelSearchCriterionType();

            if (request.DestinationCodes.Count() > 0)
            {
                //By Airport Code
                
                //searchCriterion[0].HotelRef = new ItemSearchCriterionTypeHotelRef()
                //{
                //    HotelCityCode = request.DestinationCodes[0]
                //};
                

                //By Place Code

                searchCriterion[0].HotelRef = new ItemSearchCriterionTypeHotelRef()
                {
                    BrandCode = "PLACE",
                    BrandName = request.DestinationCodes[0]
                };
                
            }
            else if (request.EstablishmentCodes.Count() > 0)
            {
                searchCriterion[0].HotelRef = new ItemSearchCriterionTypeHotelRef()
                {
                    ChainCode = request.EstablishmentCodes[0].Split(';')[0],
                    HotelCode = request.EstablishmentCodes[0].Split(';')[1]
                };
            }


            return searchCriterion;
        }

        private  RoomStayCandidateType [] CreateRoomStay(AccommodationProviderAvailabilityRequest request)
        {
            RoomStayCandidateType [] roomStay = new RoomStayCandidateType[request.Rooms.Count()];

            int i = 0;
            foreach (var room in request.Rooms)
            {
                roomStay[i] = helperService.CreateRoomStayCandidate(room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount, room.Guests.ChildAndInfantAges);

                i++;
            }
            return roomStay;
        }
    }
}
