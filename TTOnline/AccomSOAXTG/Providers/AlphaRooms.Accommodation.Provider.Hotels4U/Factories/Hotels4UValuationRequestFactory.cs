﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AvailRequestSegmentsType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.AvailRequestSegmentsType;
using AvailRequestSegmentsTypeAvailRequestSegment = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.AvailRequestSegmentsTypeAvailRequestSegment;
using DateTimeSpanType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.DateTimeSpanType;
using GuestCountType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.GuestCountType;
using GuestCountTypeGuestCount = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.GuestCountTypeGuestCount;
using HotelSearchCriterionType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.HotelSearchCriterionType;
using ItemSearchCriterionTypeHotelRef = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.ItemSearchCriterionTypeHotelRef;
using OTA_HotelAvailRQ = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRQ;
using RoomStayCandidateType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.RoomStayCandidateType;
using SourceType = AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService.SourceType;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Factories
{
    public class Hotels4UValuationRequestFactory : IHotels4UProviderValuationRequestFactory
    {
        private readonly IHotels4UHelperService helperService;
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";

        public Hotels4UValuationRequestFactory(IHotels4UHelperService helperService)
        {
            this.helperService = helperService;
        }

        public OTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request)
        {
            OTA_HotelAvailRQ supplierRequest = new OTA_HotelAvailRQ();
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            supplierRequest.Version = 1;
            supplierRequest.POS = new SourceType[1];
            supplierRequest.POS[0] = new SourceType()
            {
                AgentSine = availabilityRequest.Provider.Parameters.GetParameterValue(Hotels4UUsername), 
                AgentDutyCode = availabilityRequest.Provider.Parameters.GetParameterValue(Hotels4UPassword)
            };

            supplierRequest.AvailRequestSegments = new AvailRequestSegmentsType();
            AvailRequestSegmentsTypeAvailRequestSegment availRequestSegment = new AvailRequestSegmentsTypeAvailRequestSegment();
            availRequestSegment.StayDateRange = new DateTimeSpanType()
            {
                Start = availabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                Duration = helperService.CalculateDuration(availabilityRequest.CheckInDate, availabilityRequest.CheckOutDate)
            };

            availRequestSegment.RoomStayCandidates = CreateRoomStay(request);
            availRequestSegment.HotelSearchCriteria = CreateSearchCriteria(request);

            supplierRequest.AvailRequestSegments.AvailRequestSegment = new AvailRequestSegmentsTypeAvailRequestSegment[1];
            supplierRequest.AvailRequestSegments.AvailRequestSegment[0] = availRequestSegment;
            
            return supplierRequest;
        }

        private HotelSearchCriterionType[] CreateSearchCriteria(AccommodationProviderValuationRequest request)
        {
            HotelSearchCriterionType[] searchCriterion = new HotelSearchCriterionType[1];
            
            searchCriterion[0] = new HotelSearchCriterionType();

            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            if (request.SelectedRooms.Any() && availabilityResult.ProviderEstablishmentCode.Contains(';'))
            {
                searchCriterion[0].HotelRef = new ItemSearchCriterionTypeHotelRef()
                {
                    ChainCode = availabilityResult.ProviderEstablishmentCode.Split(';')[0],
                    HotelCode = availabilityResult.ProviderEstablishmentCode.Split(';')[1]
                };
            }
            
            return searchCriterion;
        }

        private RoomStayCandidateType[] CreateRoomStay(AccommodationProviderValuationRequest request)
        {
            RoomStayCandidateType[] roomStay = new RoomStayCandidateType[request.SelectedRooms.Count()];

            int i = 0;
            foreach (var room in request.SelectedRooms)
            {
                roomStay[i] = helperService.CreateRoomStayCandidate(room.Guests.AdultsCount, room.Guests.ChildrenAndInfantsCount, room.Guests.ChildAndInfantAges);

                i++;
            }
            return roomStay;
        }
    }
}
