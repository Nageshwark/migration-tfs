﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Helper;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Xml;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{

    public class Hotels4UValuationParser : IAccommodationValuationParser<Hotels4UValuationResponse>
    {
        private readonly ILogger logger;
        private readonly IProviderNonRefundableService nonRefundableService;

        public Hotels4UValuationParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.logger = logger;
            this.nonRefundableService = nonRefundableService;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, Hotels4UValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, response.BookingRules, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotels4U Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRQ supplierRequest
            , Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelAvailRS supplierResponse, Accommodation.Provider.Hotels4U.Hotels4UService.OTA_HotelBookingRuleRS bookingRules
            , ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse);
            var dataCleanser = new DataCleanser();
            
            if (hasResults)
            {
                OTA_HotelAvailRSRoomStays  roomStays = GetRoomStays(supplierResponse.Items);

                
                int roomCounter = 0;

                Parallel.ForEach(roomStays.RoomStay, roomStay =>
                {
                    Sync.ParallelForEachIgnoreFailed(roomStay.RoomTypes, roomType =>
                    {

                        var isSelectRoom = from AccommodationProviderValuationRequestRoom r in request.SelectedRooms
                                           where r.AvailabilityResult.RoomCode == roomType.RoomTypeCode
                            select r;

                        if (isSelectRoom.Any())
                        {

                            var result = new AccommodationProviderValuationResult();

                            result.RoomNumber = isSelectRoom.First().AvailabilityResult.RoomNumber;

                            result.ProviderEdiCode = isSelectRoom.First().AvailabilityRequest.Provider.EdiCode;
                            result.SupplierEdiCode = roomStay.BasicPropertyInfo.ChainCode;
                            result.PaymentModel = PaymentModel.PostPayment;

                            //TODO:What should it be if response does not contain destination code?
                            if (request.SelectedRooms[roomCounter].AvailabilityRequest.DestinationCodes.Length > 0)
                                result.DestinationEdiCode = request.SelectedRooms[roomCounter].AvailabilityRequest.DestinationCodes[0];
                            else if (request.SelectedRooms[roomCounter].AvailabilityRequest.EstablishmentCodes.Length > 0)
                                result.DestinationEdiCode = request.SelectedRooms[roomCounter].AvailabilityRequest.EstablishmentCodes[0];

                            if (roomStay.BasicPropertyInfo.Address != null)
                                result.DestinationEdiCode = roomStay.BasicPropertyInfo.Address.CityName;

                            result.EstablishmentEdiCode = string.Format("{0};{1}", roomStay.BasicPropertyInfo.ChainCode,
                                roomStay.BasicPropertyInfo.HotelCode);

                            result.EstablishmentName = roomStay.BasicPropertyInfo.HotelName;

                            string roomDescription = ((FormattedTextTextType)(roomType.RoomDescription.Items[0])).Value;

                            result.RoomDescription = dataCleanser.CleanRoomDescription(roomDescription);
                            result.RoomCode = roomType.RoomTypeCode;

                            result.CheckInDate = isSelectRoom.First().AvailabilityRequest.CheckInDate;
                            result.CheckOutDate = isSelectRoom.First().AvailabilityRequest.CheckOutDate;


                            //For Hotels4U own contracted stock Board Code is Last two Charecters of the RoomCode
                            if (roomStay.BasicPropertyInfo.ChainCode.Equals("H4U",
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                BoardBasisMapper bbMapper = new BoardBasisMapper();
                                result.BoardCode = roomType.RoomTypeCode.Substring(roomType.RoomTypeCode.Length - 2);
                                result.BoardDescription = bbMapper.GetBoardBasisName(result.BoardCode);
                            }
                            else
                            {
                                result.BoardDescription = dataCleanser.CleanBoardDescription(roomDescription);
                            }


                            var rateQuery = from RoomRateType r in roomStay.RoomRates
                                where r.RoomTypeCode == roomType.RoomTypeCode
                                select r;

                            if (rateQuery.Any())
                            {
                                result.SalePrice = new Money()
                                {
                                    Amount = rateQuery.First().Rates.First().Total.AmountAfterTax,
                                    CurrencyCode = rateQuery.First().Rates.First().Total.CurrencyCode
                                };

                                //what should we populate here? there is only one price in H4U response.
                                result.CostPrice = result.SalePrice;
                                
                                result.RateType = RateType.NetStandard;
                            }
                            else
                            {
                                throw new SupplierApiDataException("No rate found");
                            }

                            var guests = request.SelectedRooms.First().AvailabilityRequest.Rooms[roomCounter].Guests;

                            result.Adults = guests.AdultsCount;
                            result.Children = (byte)(guests.ChildrenCount + guests.InfantsCount);

                            result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(result.RoomDescription, result.BoardDescription, result.IsNonRefundable);

                            XmlElement localTax = null;
                            if (rateQuery.Any() && rateQuery.First().Rates.Any() && rateQuery.First().Rates.First().TPA_Extensions != null)
                            {
                                localTax = rateQuery.First().Rates.First().TPA_Extensions.Any.FirstOrDefault(i => string.Equals(i.Name, "LocalTax", StringComparison.CurrentCultureIgnoreCase));
                            }
                            result.CancellationPolicy = CreateCancellationTerms(request, result, rateQuery.First().Rates.First().Total.CurrencyCode, (localTax != null ? localTax.InnerText : null), bookingRules);

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("KeyCollectionInformation", String.Empty);

                            Parallel.ForEach(bookingRules.Items, item =>
                            {
                                if (item.GetType().FullName.Contains("OTA_HotelBookingRuleRSRuleMessage"))
                                {
                                    var rules = (OTA_HotelBookingRuleRSRuleMessage)item;
                                    Parallel.ForEach<ArrayOfBookingRuleBookingRule>(rules.BookingRules, rule => 
                                    {
                                        if (rule.Code.ToLower() == "errata")
                                        {
                                            Parallel.ForEach(rule.Description[0].Items, obj =>
                                            {
                                                if (obj.GetType() == typeof (FormattedTextTextType))
                                                {
                                                    result.ProviderSpecificData["KeyCollectionInformation"] = ((FormattedTextTextType)obj).Value;
                                                }

                                            });
                                        }
                                        
                                    } );

                                }
                                
                            });

                            availabilityResults.Enqueue(result);

                            roomCounter++;
                        }
                    });
                });

            }
            else
            {
                string message = "Hotels4U Availability Parser: Availability Response cannot be parsed because it is null.";

                throw new SupplierApiDataException(message);
            }
            
        }

        private bool HasResults(OTA_HotelAvailRS supplierResponse)
        {
            return (supplierResponse != null && supplierResponse.Items != null && supplierResponse.Items.Any() && GetRoomStays(supplierResponse.Items) != null);
        }

        private OTA_HotelAvailRSRoomStays GetRoomStays(object[] roomStays)
        {
            var query = from rs in roomStays
                        where rs.GetType() == typeof(OTA_HotelAvailRSRoomStays)
                        select rs;

            if (query != null && query.Any())
                return (OTA_HotelAvailRSRoomStays)query.First();
            else
                return null;
        }

        private string[] CreateCancellationTerms(AccommodationProviderValuationRequest request, AccommodationProviderValuationResult result, string currencyCode, string localtax, OTA_HotelBookingRuleRS bookingRules)
        {
            var hotels4uContract = CreateHotels4UIncomingContract(request, result, currencyCode, localtax, bookingRules);
            var currency = string.Empty;
            var cancellationText = new List<string>();
            foreach (var can in hotels4uContract.CancellationPolicies)
            {
                cancellationText.Add(string.Format("From {0} to {1} {2}% of total amount will be charged as cancellation penalty.",
                                                            can.FromDate.ToShortDateString(), can.ToDate.ToShortDateString(), decimal.Round(can.Percent, 0)));
                if (string.IsNullOrEmpty(currency))
                {
                    currency = can.Currency;
                }
            }

            if (!string.IsNullOrEmpty(hotels4uContract.LocalTaxAmount) || !string.IsNullOrEmpty(hotels4uContract.Errata) || !string.IsNullOrEmpty(hotels4uContract.Extra))
            {
                cancellationText.Add("<b>Additional information</b>");
                //Tax amount
                if (!string.IsNullOrEmpty(hotels4uContract.LocalTaxAmount) && (hotels4uContract.CancellationPolicies != null))
                {
                    cancellationText.Add(string.Format("Local tax amount to be paid on destination {0} {1}",  hotels4uContract.LocalTaxAmount, currency));
                }
                //Erratas
                if (!string.IsNullOrEmpty(hotels4uContract.Errata))
                {
                    cancellationText.Add(hotels4uContract.Errata);
                }
                //Extra information
                if (!string.IsNullOrEmpty(hotels4uContract.Extra))
                {
                    cancellationText.Add(hotels4uContract.Extra);
                }
            }
            return cancellationText.ToArray();
        }

        private Hotels4UIncomingContractInformation CreateHotels4UIncomingContract(AccommodationProviderValuationRequest request, AccommodationProviderValuationResult result, string currencyCode
            , string localTaxAmount, OTA_HotelBookingRuleRS bookingRules)
        {
            if (result.IsNonRefundable)
            {
                return new Hotels4UIncomingContractInformation()
                {
                    LocalTaxAmount = localTaxAmount
                    , CancellationPolicies = new[]
                    {
                        new Hotels4UCancellationPolicy()
                        {
                            FromDate = DateTime.Now
                            , ToDate = request.SelectedRooms[0].AvailabilityResult.CheckInDate
                            , Currency = currencyCode
                            , Percent = 100M
                        }
                    }
                };
            }
            var incomingContract = new Hotels4UIncomingContractInformation();
            var listOfPolicies = new List<Hotels4UCancellationPolicy>();
            foreach (var item in bookingRules.Items)
            {
                if (item is OTA_HotelBookingRuleRSRuleMessage)
                {
                    foreach (var rule in (item as OTA_HotelBookingRuleRSRuleMessage).BookingRules)
                    {
                        if (string.Equals(rule.Code, "cancel", StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (rule.CancelPenalties != null && rule.CancelPenalties.CancelPenalty != null && rule.CancelPenalties.CancelPenalty.Length > 0)
                            {
                                foreach (var cP in rule.CancelPenalties.CancelPenalty)
                                {
                                    if (cP.AmountPercent != null && cP.Deadline != null)
                                    {
                                        listOfPolicies.Add(new Hotels4UCancellationPolicy()
                                        {
                                            ToDate = cP.Deadline.AbsoluteDeadline
                                            , Percent = cP.AmountPercent.Percent
                                            , Currency = currencyCode
                                        });
                                    }
                                }
                            }
                        }
                        else if (string.Equals(rule.Code, "errata", StringComparison.CurrentCultureIgnoreCase))
                        {
                            foreach (var o in rule.Description[0].Items)
                            {
                                if (o is FormattedTextTextType) incomingContract.Errata = ((FormattedTextTextType)o).Value;
                            }
                        }
                        else if (string.Equals(rule.Code, "policy", StringComparison.CurrentCultureIgnoreCase))
                        {
                            foreach (var o in rule.Description[0].Items)
                            {
                                if (o is FormattedTextTextType) incomingContract.Extra = ((FormattedTextTextType)o).Value;
                            }
                        }
                    }
                }
            }
            incomingContract.CancellationPolicies = listOfPolicies.ToArray();
            return incomingContract;
        }
    }

    public class Hotels4UCancellationPolicy
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Percent { get; set; }
        public string Currency { get; set; }
    }

    public class Hotels4UIncomingContractInformation
    {
        public Hotels4UCancellationPolicy[] CancellationPolicies { get; set; }
        public string Errata { get; set; }
        public string Extra { get; set; }
        public string LocalTaxAmount { get; set; }
    }
}
