﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UValuationAutomator : IAccommodationValuationAutomatorAsync<Hotels4UValuationResponse>
    {
        private const string Hotels4UAvailabilityUrl = "Hotels4UAvailabilityUrl";
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotels4UProviderValuationRequestFactory hotels4UProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public Hotels4UValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotels4UProviderValuationRequestFactory Hotels4USupplierValuationRequestFactory
            , IProviderOutputLogger outputLogger
            , IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.hotels4UProviderValuationRequestFactory = Hotels4USupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<Hotels4UValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            Hotels4UValuationResponse response = null;

            try
            {
                OTA_HotelAvailRQ supplierRequest = this.hotels4UProviderValuationRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("Hotels4UValuationAutomator.GetValuationResponseAsync: Hotels4U supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<Hotels4UValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, OTA_HotelAvailRQ supplierRequest)
        {

            SoapDispatcherSoapClient client = ConfigureSoapCleint(request);


            OTA_Header header = new OTA_Header();
            header.userid = request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(Hotels4UUsername);
            header.password = request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(Hotels4UPassword);

            this.loggerService.SetProviderRequest(request, "header", header);
            this.loggerService.SetProviderRequest(request, "request", supplierRequest);
            OTA_HotelAvailRS response = client.sendOTAHotelAvail(header, supplierRequest);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, response);
            var hotelBookingRuleRq = CreateHotelBookingRuleRequest(request);
            var hotelBookingRuleRs = client.sendOTAHotelBookingRule(header, hotelBookingRuleRq);
            Hotels4UValuationResponse supplierResponse = new Hotels4UValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response,
                BookingRules = hotelBookingRuleRs
            };

            return supplierResponse;
        }

        private OTA_HotelBookingRuleRQ CreateHotelBookingRuleRequest(AccommodationProviderValuationRequest request)
        {
            var hotelBookingRuleRq = new OTA_HotelBookingRuleRQ()
            {
                EchoToken = "Echo",
                RuleMessage = new OTA_HotelBookingRuleRQRuleMessage()
                {
                    ChainCode = request.SelectedRooms.First().AvailabilityResult.ProviderEstablishmentCode.Split(';')[0],
                    HotelCode = request.SelectedRooms.First().AvailabilityResult.ProviderEstablishmentCode.Split(';')[1],
                    StatusApplication = new OTA_HotelBookingRuleRQRuleMessageStatusApplication()
                    {
                        Start = request.SelectedRooms.First().AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                        End = request.SelectedRooms.First().AvailabilityRequest.CheckOutDate.ToString("yyyy-MM-dd"),
                        RatePlanCode = request.SelectedRooms.First().AvailabilityResult.RoomCode
                    }

                }
            };
            return hotelBookingRuleRq;
        }

        private SoapDispatcherSoapClient ConfigureSoapCleint(AccommodationProviderValuationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.SelectedRooms.First().AvailabilityRequest.Provider.Parameters.GetParameterValue(Hotels4UAvailabilityUrl)));
            SoapDispatcherSoapClient client = new SoapDispatcherSoapClient(binding, address);

            return client;
        }
    }
}
