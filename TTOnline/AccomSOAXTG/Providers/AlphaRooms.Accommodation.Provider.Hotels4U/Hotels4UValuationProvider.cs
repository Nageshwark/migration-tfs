﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Hotels4U;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<Hotels4UValuationResponse> automator;
        private readonly IAccommodationValuationParser<Hotels4UValuationResponse> parser;

        public Hotels4UValuationProvider(IAccommodationValuationAutomatorAsync<Hotels4UValuationResponse> automator,
                                                IAccommodationValuationParser<Hotels4UValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
