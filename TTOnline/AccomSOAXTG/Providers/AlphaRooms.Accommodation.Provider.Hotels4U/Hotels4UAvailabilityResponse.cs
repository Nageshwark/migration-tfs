﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UAvailabilityResponse
    {
        public OTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailRS SupplierResponse { get; set; }

    }
}
