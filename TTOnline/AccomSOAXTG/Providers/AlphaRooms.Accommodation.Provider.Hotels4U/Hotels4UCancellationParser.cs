﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Helper;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UCancellationParser : IAccommodationCancellationParserAsync<Hotels4UCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public Hotels4UCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, Hotels4UCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                var helper = new ResponseHelper();

                var cancelResponse = response.SupplierResponse;

                if (cancelResponse != null &&
                    cancelResponse.Items != null &&
                    cancelResponse.Items.Any() && helper.GetItemByType(cancelResponse.Items, typeof(SuccessType)).Any())
                {
                    var cancelInfo = helper.GetItemByType(cancelResponse.Items, typeof (CancelInfoRSType));

                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";   

                }
                else
                {
                    var errorQuery = helper.GetItemByType(cancelResponse.Items, typeof (ErrorsType));
                    
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    var sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the cancellation response from Hotels4U.");

                    if (errorQuery != null && errorQuery.Any())
                    {
                        var errors = (ErrorsType) errorQuery.First();
                        int i = 1;
                        sb.AppendLine("Details:");
                        foreach (ErrorType error in errors.Error)
                        {
                            sb.AppendLine(string.Format("{0}. {1}", i, error.ShortText));
                            i++;
                        }    
                    }

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Hotels4U.");
                sb.AppendLine("Details:" + ex.ToString());
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
       
    }
}
