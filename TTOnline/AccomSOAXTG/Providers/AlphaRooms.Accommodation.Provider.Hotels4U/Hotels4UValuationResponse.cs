﻿using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UValuationResponse
    {
        public OTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailRS SupplierResponse { get; set; }
        public OTA_HotelBookingRuleRS BookingRules { get; set; }
    }
}
