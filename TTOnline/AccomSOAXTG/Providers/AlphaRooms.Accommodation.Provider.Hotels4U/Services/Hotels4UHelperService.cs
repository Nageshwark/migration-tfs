﻿using System;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Services
{
    public class Hotels4UHelperService : IHotels4UHelperService
    {
        public GuestCountTypeGuestCount[] CalculateGuestCount(int adults, int children)
        {
            int guestCount = adults + children;
            return new GuestCountTypeGuestCount[guestCount];
        }

        public RoomStayCandidateType CreateRoomStayCandidate(int adults, int children, byte[] childAges)
        {
            RoomStayCandidateType roomStayCandidate = new RoomStayCandidateType();
            roomStayCandidate.GuestCounts = new GuestCountType();

            List<GuestCountTypeGuestCount> guests = new List<GuestCountTypeGuestCount>();

            if (adults > 0)
            {
                guests.Add(new GuestCountTypeGuestCount()
                {
                    AgeQualifyingCode = "10",
                    Count = adults.ToString()
                });
            }

            foreach (var childAge in childAges)
            {
                guests.Add(new GuestCountTypeGuestCount()
                {
                    AgeQualifyingCode = childAge < 2 ? "7" : "8",
                    Age = childAge.ToString()
                });
            }

            roomStayCandidate.GuestCounts.GuestCount = guests.ToArray();
            return roomStayCandidate;
        }

        public string CalculateDuration(DateTime checkInDate, DateTime checkOutDate)
        {
            TimeSpan duration = checkOutDate - checkInDate;

            return string.Format("P{0}N", duration.TotalDays);
        }
    }
}