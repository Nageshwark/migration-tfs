﻿using System;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Helper;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UCancellationAutomator : IAccommodationCancellationAutomatorAsync<Hotels4UCancellationResponse>
    {
        private const string Hotels4UCancellationUrl = "Hotels4UCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string Hotels4UUsername = "Hotels4UUsername";
        private const string Hotels4UPassword = "Hotels4UPassword";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotels4UProviderCancellationRequestFactory hotels4UProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private string Hotels4UCancellationTimeOut;

        public Hotels4UCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotels4UProviderCancellationRequestFactory Hotels4USupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.hotels4UProviderCancellationRequestFactory = Hotels4USupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<Hotels4UCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            Hotels4UCancellationResponse response = null;

            try
            {
                var supplierRequest = hotels4UProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }
                
                var client = ConfigureSoapCleint(request);
                var header = new OTA_Header
                {
                    userid = request.Provider.Parameters.GetParameterValue(Hotels4UUsername),
                    password = request.Provider.Parameters.GetParameterValue(Hotels4UPassword)
                };

                var supplierResponse = client.sendOTACancel(header, supplierRequest);

                response = new Hotels4UCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("Hotels4UBookingAutomator.GetCancellationResponseAsync: Hotels4U supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        

        private SoapDispatcherSoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            

            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(Hotels4UCancellationUrl)));
            var client = new SoapDispatcherSoapClient(binding, address);

            client.InnerChannel.OperationTimeout = request.Provider.SearchTimeout;
            
            return client;
        }
    }
}
