﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<Hotels4UCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<Hotels4UCancellationResponse> parser;

        public Hotels4UCancellationProvider(IAccommodationCancellationAutomatorAsync<Hotels4UCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<Hotels4UCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
