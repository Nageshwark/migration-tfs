﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces
{
    public interface IHotels4UProviderBookingRequestFactory
    {
        OTA_HotelResRQ CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
        OTA_HotelResRQ CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string resId);
    }
}
