﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces
{
    public interface IHotels4UHelperService
    {
        GuestCountTypeGuestCount[] CalculateGuestCount(int adults, int children);
        RoomStayCandidateType CreateRoomStayCandidate(int adults, int children, byte[] childAges);
        string CalculateDuration(DateTime checkInDate, DateTime checkOutDate);
    }
}