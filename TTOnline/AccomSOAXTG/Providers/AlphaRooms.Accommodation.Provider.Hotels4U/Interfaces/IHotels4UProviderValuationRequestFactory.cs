﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces
{
    public interface IHotels4UProviderValuationRequestFactory
    {
        Hotels4UService.OTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request);
    }
}
