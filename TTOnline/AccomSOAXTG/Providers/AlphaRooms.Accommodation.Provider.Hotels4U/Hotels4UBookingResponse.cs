﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Hotels4U accepts and returns XML strings.
    /// </summary>
    public class Hotels4UBookingResponse
    {
        public Hotels4UBookingResponseBase BookingInit { get; set; }
        public Hotels4UBookingResponseBase BookingCommit { get; set; }
    }

    public class Hotels4UBookingResponseBase
    {
        public OTA_HotelResRQ SupplierRequest { get; set; }
        public OTA_HotelResRS SupplierResponse { get; set; }
    }
    
}
