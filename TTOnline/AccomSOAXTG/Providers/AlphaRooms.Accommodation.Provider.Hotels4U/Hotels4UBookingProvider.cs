﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<Hotels4UBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<Hotels4UBookingResponse>> parser;

        public Hotels4UBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<Hotels4UBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<Hotels4UBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
