﻿using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UCancellationResponse
    {
        public OTA_CancelRQ SupplierRequest { get; set; }
        public OTA_CancelRS SupplierResponse { get; set; }
    }
}
