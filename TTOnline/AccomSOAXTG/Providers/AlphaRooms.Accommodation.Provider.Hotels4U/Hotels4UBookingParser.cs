﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.Hotels4U.Hotels4UService;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UBookingParser : IAccommodationBookingParser<IEnumerable<Hotels4UBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public Hotels4UBookingParser(ILogger logger
            //IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<Hotels4UBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotels4U Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        Hotels4UBookingResponse bookingResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (bookingResponse == null)
            {
                string message = string.Format("Hotels4U Booking Parser: Booking Response cannot be parsed because it is null. Booking Request Id: {0}", request.BookingId);

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }



            // Debug Code
            if (request.Debugging)
            {
                string serialisedResponse = bookingResponse.XmlSerialize();
            }

            // Check that no error was included in the response - if an error is returned, log the error in the event log, but DO NOT throw an exception. Instead, return a failed booking.
            if (bookingResponse.BookingCommit.SupplierResponse.Items != null && bookingResponse.BookingCommit.SupplierResponse.Items[0].GetType() == typeof(SuccessType))
            {
                // NOTE: This will process a single room booking ONLY. It is not designed to handle multiple room bookings.
                CreateAvailabilityResultFromResponse(request, bookingResponse, bookingResults);
            }
            else
            {

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Hotels4U Response Error.");
                sb.AppendLine("Hotels4U returned one or more errors in their booking response:");
                sb.AppendLine("Booking Request:");
                sb.AppendLine(bookingResponse.XmlSerialize());
                sb.AppendLine();

                if (bookingResponse.BookingCommit.SupplierResponse.Items != null)
                {
                    var error = (ErrorType)bookingResponse.BookingCommit.SupplierResponse.Items[0];

                    if (error != null)
                    {
                        sb.AppendLine(string.Format("Error Code: {0}, Type: {1}, Tex: {2} ", error.Code, error.Type, error.ShortText));
                    }
                }



                // Log the error to the event log
                if (logger != null)
                    logger.Error(sb.ToString());

                // Create a failed booking result
                var failedResult = new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    Message = sb.ToString()
                };

                bookingResults.Enqueue(failedResult);

            }
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderBookingResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="bookingResponse"></param>
        /// <param name="bookingResults"></param>
        private void CreateAvailabilityResultFromResponse(AccommodationProviderBookingRequest request,
                                                            Hotels4UBookingResponse bookingResponse,
                                                            ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();
            result.RoomNumber = request.ValuatedRooms[0].RoomNumber;
            try
            {
                var reservation = ((HotelReservationsType) bookingResponse.BookingCommit.SupplierResponse.Items[1]).HotelReservation.First();
                

                result.ProviderBookingReference = reservation.ResGlobalInfo.HotelReservationIDs.First().ResID_Value;
                result.KeyCollectionInformation = request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["KeyCollectionInformation"];

                result.BookingStatus = BookingStatus.Confirmed;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from Hotels4U.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request/Response:");
                sb.AppendLine(bookingResponse.XmlSerialize());
                sb.AppendLine();
                

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Enqueue(result);
        }
    }
}
