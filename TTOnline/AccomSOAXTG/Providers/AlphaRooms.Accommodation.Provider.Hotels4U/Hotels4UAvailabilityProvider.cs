﻿using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotels4U
{
    public class Hotels4UAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<Hotels4UAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<Hotels4UAvailabilityResponse> parser;

        public Hotels4UAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<Hotels4UAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<Hotels4UAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
