﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.SunHotels;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsValuationProvider : AccommodationValuationBase
    {
        //private readonly IAccommodationValuationAutomatorAsync<SunHotelsValuationResponse> automator;
        //private readonly IAccommodationValuationParser<SunHotelsValuationResponse> parser;
        private readonly IAccommodationValuationAutomatorAsync <SunHotelsValuationResponse> automator;
        private readonly IAccommodationValuationParser<SunHotelsValuationResponse> parser;


       // public SunHotelsValuationProvider(IAccommodationValuationAutomatorAsync<SunHotelsValuationResponse> automator,
        //                                        IAccommodationValuationParser<SunHotelsValuationResponse> parser)

        public SunHotelsValuationProvider(IAccommodationValuationAutomatorAsync<SunHotelsValuationResponse> automator,
            IAccommodationValuationParser<SunHotelsValuationResponse> parser)
       {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
