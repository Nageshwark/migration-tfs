﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;



namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsValuationAutomator : IAccommodationValuationAutomatorAsync<SunHotelsValuationResponse>
    {
        private const string SunHotelsValuationUrl = "SunHotelsValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private readonly ISunHotelsProviderValuationRequestFactory _sunhotelsProviderValuationRequestFactory;

        public SunHotelsValuationAutomator(//ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ISunHotelsProviderValuationRequestFactory sunHotelsSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
            this._sunhotelsProviderValuationRequestFactory = sunHotelsSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<SunHotelsValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            SunHotelsValuationResponse response = null;

            try
            {
                SearchRequest supplierRequest = this._sunhotelsProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("SunHotelsValuationAutomator.GetValuationResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<SunHotelsValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, SearchRequest supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            StaticXMLAPISoapClient client = ConfigureSoapCleint(request);

            SearchResult response = client.Search(
                                            supplierRequest.userName,
                                            supplierRequest.password,
                                            supplierRequest.language,
                                            supplierRequest.currencies,
                                            supplierRequest.checkInDate,
                                            supplierRequest.checkOutDate,
                                            supplierRequest.numberOfRooms,
                                            supplierRequest.destination,
                                            supplierRequest.destinationID,
                                            supplierRequest.hotelIDs,
                                            supplierRequest.resortIDs,
                                            supplierRequest.accommodationTypes,
                                            supplierRequest.numberOfAdults,
                                            supplierRequest.numberOfChildren,
                                            supplierRequest.infant,
                                            supplierRequest.sortBy,
                                            supplierRequest.sortOrder,
                                            supplierRequest.exactDestinationMatch,
                                            supplierRequest.blockSuperdeal,
                                            supplierRequest.mealIds,
                                            supplierRequest.showCoordinates,
                                            supplierRequest.showReviews,
                                            supplierRequest.referencePoint,
                                            supplierRequest.maxDistanceFromReferencePoint,
                                            supplierRequest.minStarRating,
                                            supplierRequest.maxStarRating,
                                            supplierRequest.featureIds,
                                            supplierRequest.minPrice,
                                            supplierRequest.maxPrice,
                                            supplierRequest.themeIds,
                                            supplierRequest.excludeSharedRooms,
                                            supplierRequest.excludeSharedFacilities,
                                            supplierRequest.prioritizedHotelIds,
                                            supplierRequest.totalRoomsInBatch,
                                            supplierRequest.paymentMethodId
                                            );

             var supplierResponse = new SunHotelsValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }


        private StaticXMLAPISoapClient ConfigureSoapCleint(AccommodationProviderValuationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(SunHotelsValuationUrl)));
            StaticXMLAPISoapClient client = new StaticXMLAPISoapClient(binding, address);

            return client;
        }
      
    
    }
}
