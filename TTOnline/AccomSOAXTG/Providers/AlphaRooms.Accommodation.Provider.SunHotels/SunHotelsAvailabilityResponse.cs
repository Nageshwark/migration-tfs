﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsAvailabilityResponse
    {
        public SearchRequest SupplierRequest { get; set; }
        public SearchResult SupplierResponse { get; set; }
    }
}