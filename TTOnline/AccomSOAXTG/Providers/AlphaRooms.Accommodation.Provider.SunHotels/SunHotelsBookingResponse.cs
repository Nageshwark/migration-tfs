﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsBookingResponse
    {
        public BookRequest SupplierRequest { get; set; }
        public BookResult SupplierResponse { get; set; }
    }
}