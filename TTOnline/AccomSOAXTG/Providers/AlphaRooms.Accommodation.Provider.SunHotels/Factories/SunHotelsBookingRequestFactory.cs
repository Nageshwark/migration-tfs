﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.Utilities;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.SunHotels.Factories
{
    public class SunHotelsBookingRequestFactory : ISunHotelsBookingRequestFactory
    {
        private const string SunHotelsDefaultLanguage = "SunHotelsDefaultLanguage";
        private const string SunHotelsuser = "SunHotelsUserName";
        private const string SunHotelspassword = "SunHotelsPassword";
        private const string Currencies = "currencies";
        private const string PaymentMethod = "paymentmethod";
        private const string MealId = "mealId";
        private readonly IProviderSpecialRequestService specialRequestService;

        public SunHotelsBookingRequestFactory(IProviderSpecialRequestService specialRequestService)
        {
            this.specialRequestService = specialRequestService;
        }

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookRequest CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            string currency = request.Provider.Parameters.GetParameterValue(Currencies);
            string[] currencycode = currency.Split(',');
            BookRequest supplierRequest = new BookRequest
            {
                userName = request.Provider.Parameters.GetParameterValue(SunHotelsuser),
                password = request.Provider.Parameters.GetParameterValue(SunHotelspassword),
                language = request.Provider.Parameters.GetParameterValue(SunHotelsDefaultLanguage),
                currency = currencycode[0],
                email = request.Customer.EmailAddress,
                checkInDate = request.ValuatedRooms[0].ValuationResult.CheckInDate,
                checkOutDate = request.ValuatedRooms[0].ValuationResult.CheckOutDate,
                roomId = Convert.ToInt32(request.ValuatedRooms[0].ValuationResult.RoomCode),
                rooms = request.ValuatedRooms.Length,
                adults = request.ValuatedRooms.Sum(g => g.Guests.AdultsCount),
                children = request.ValuatedRooms.Sum(g => g.Guests.ChildrenCount),
                infant = request.ValuatedRooms.Sum(g => g.Guests.InfantsCount),
                mealId = Convert.ToInt32(request.ValuatedRooms[0].ValuationResult.BoardCode),
                yourRef = request.BookingId.ToString(),
                paymentMethodId = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(PaymentMethod)),
                adultsList = CreateAdultList(request.ValuatedRooms),
                childrenList = CreateChildList(request.ValuatedRooms),
                specialrequest = specialRequestService.GetSpecialRequestString(request.ValuatedRooms.First().SpecialRequests)
            };
            
            supplierRequest.yourRef = Convert.ToString(request.BookingId);
            Child childname = new Child();
            List<Child> childlist = new List<Child>();
            supplierRequest.childrenList = childlist.ToArray();

            supplierRequest.paymentMethodId = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(PaymentMethod));
            return supplierRequest;
        }

        private Child[] CreateChildList(AccommodationProviderBookingRequestRoom[] valuatedRooms)
        {
            var childList = new List<Child>();

            foreach (var room in valuatedRooms)
            {
                foreach (var guest in room.Guests.Where(g => g.Type == GuestType.Child))
                {
                    childList.Add(new Child
                    {
                        Age = guest.Age,
                        FirstName = "",
                        LastName = ""
                    });
                }
            }

            return childList.ToArray();
        }

        private Adult[] CreateAdultList(AccommodationProviderBookingRequestRoom[] valuatedRooms)
        {
            var adultList = new List<Adult>();

            foreach (var room in valuatedRooms)
            {
                foreach (var guest in room.Guests.Where(g=>g.Type== GuestType.Adult))
                {
                    adultList.Add(new Adult
                    {
                        FirstName = guest.FirstName + " " + guest.Surname
                    });
                }
            }

            return adultList.ToArray();
        }
    }
}
