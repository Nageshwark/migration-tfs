﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;


namespace AlphaRooms.Accommodation.Provider.SunHotels.Factories
{
    public class SunHotelsCancellationRequestFactory : ISunHotelsProviderCancellationRequestFactory
    {
        private const string SunHotelsDefaultLanguage = "SunHotelsDefaultLanguage";
        private const string SunHotelsuser = "SunHotelsUserName";
        private const string SunHotelspassword = "SunHotelsPassword";
        private const string Currencies = "currencies";
        
        public CancelBookingRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new CancelBookingRequest
            {
                userName = request.Provider.Parameters.GetParameterValue(SunHotelsuser),
                password = request.Provider.Parameters.GetParameterValue(SunHotelspassword),
                bookingID = request.ProviderBookingReference,
                language = request.Provider.Parameters.GetParameterValue(SunHotelsDefaultLanguage)
            };
            return supplierRequest;
        }
    }
}
