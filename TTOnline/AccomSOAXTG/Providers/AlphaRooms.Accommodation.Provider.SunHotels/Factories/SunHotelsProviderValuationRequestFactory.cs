﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels.Factories
{
    public class SunHotelsProviderValuationRequestFactory : ISunHotelsProviderValuationRequestFactory 
    {
        private const string SunHotelsDefaultLanguage = "SunHotelsDefaultLanguage";
        private const string SunHotelsuser = "SunHotelsUserName";
        private const string SunHotelspassword = "SunHotelsPassword";
        private const string Currencies = "currencies";

        public SearchRequest  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var availRequest = request.SelectedRooms.First().AvailabilityRequest;
            string currency = request.Provider.Parameters.GetParameterValue(Currencies);
            string[] currencycode = currency.Split(',');

            var supplierRequest = new SearchRequest
            {
                userName = request.Provider.Parameters.GetParameterValue(SunHotelsuser),
                password = request.Provider.Parameters.GetParameterValue(SunHotelspassword),
                language = request.Provider.Parameters.GetParameterValue(SunHotelsDefaultLanguage),
                currencies = currencycode,
                checkInDate = availRequest.CheckInDate.ToUniversalTime().Date,
                checkOutDate = availRequest.CheckOutDate.ToUniversalTime().Date,
                numberOfRooms = availRequest.Rooms.Count(),
                numberOfAdults = request.SelectedRooms.Sum(g=>g.Guests.AdultsCount),
                numberOfChildren = request.SelectedRooms.Sum(g=>g.Guests.ChildrenCount),
                infant = request.SelectedRooms.Sum(g => g.Guests.InfantsCount),
                hotelIDs = new []
                {
                    Convert.ToInt32(request.SelectedRooms.First().AvailabilityResult.EstablishmentEdiCode) 
                }
            };
            
            return supplierRequest;
        }
          
    }
}
