﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.SunHotels.Factories
{
    public class SunHotelsAvailabilityRequestFactory : ISunHotelsAvailabilityRequestFactory
    {
        private const string SunHotelsDefaultLanguage = "SunHotelsDefaultLanguage";
        private const string SunHotelsuser = "SunHotelsUserName";
        private const string SunHotelspassword = "SunHotelsPassword";
        private const string Currencies = "currencies";

        public SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            string currency = request.Provider.Parameters.GetParameterValue(Currencies);
            string[] currencycode = currency.Split(',');

            var supplierRequest = new SearchRequest
            {
                userName = request.Provider.Parameters.GetParameterValue(SunHotelsuser),
                password = request.Provider.Parameters.GetParameterValue(SunHotelspassword),
                language = request.Provider.Parameters.GetParameterValue(SunHotelsDefaultLanguage),
                currencies = currencycode,
                checkInDate = request.CheckInDate.ToUniversalTime().Date,
                checkOutDate = request.CheckOutDate.ToUniversalTime().Date,
                numberOfRooms = request.Rooms[0].RoomNumber,
                numberOfAdults = request.Rooms.Sum(g => g.Guests.AdultsCount),
                numberOfChildren = request.Rooms.Sum(g => g.Guests.ChildrenCount),
                infant = request.Rooms.Sum(g => g.Guests.InfantsCount),
            };
           
            
           
            if (request.DestinationCodes.Any())
            {
                supplierRequest.destinationID =Convert.ToInt32(request.DestinationCodes[0]);
            }
            else
            {
                int[] hotelIDs = Array.ConvertAll(request.EstablishmentCodes, int.Parse);
                supplierRequest.hotelIDs = hotelIDs;
            }
            
            return supplierRequest;
        }
    }
}


