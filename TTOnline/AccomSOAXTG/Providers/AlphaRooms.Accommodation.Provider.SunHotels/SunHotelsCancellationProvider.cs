﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<SunHotelsCancellationResponse> _automator;
        private readonly IAccommodationCancellationParserAsync<SunHotelsCancellationResponse> _parser;

        public SunHotelsCancellationProvider(IAccommodationCancellationAutomatorAsync<SunHotelsCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<SunHotelsCancellationResponse> parser)
        {
            this._automator = automator;
            this._parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await _automator.GetCancellationResponseAsync(request);
            return await _parser.GetCancellationResultsAsync(request, response);
        }
    }
}
