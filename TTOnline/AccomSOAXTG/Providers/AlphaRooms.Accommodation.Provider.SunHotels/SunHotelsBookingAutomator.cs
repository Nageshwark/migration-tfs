﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using System.ServiceModel;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsBookingAutomator : IAccommodationBookingAutomatorAsync<SunHotelsBookingResponse>
    {

        private const string SunHotelBookingUrl = "SunHotelsBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private readonly ISunHotelsBookingRequestFactory _sunHotelsBookingRequestFactory;

        public SunHotelsBookingAutomator(ISunHotelsBookingRequestFactory sunHotelsBookingRequestFactory)
        {
            this._sunHotelsBookingRequestFactory = sunHotelsBookingRequestFactory;
        }
        public async Task<SunHotelsBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {

            List<SunHotelsBookingResponse> responses = new List<SunHotelsBookingResponse>();
            SunHotelsBookingResponse response = null;
            try
            {
                BookRequest supplierRequest = this._sunHotelsBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookResponseAsync(request, supplierRequest);
                responses.Add(response);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("SunHotelsBookingAutomator.GetAvailabilityResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<SunHotelsBookingResponse> GetSupplierBookResponseAsync(AccommodationProviderBookingRequest request,
            BookRequest supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();
            StaticXMLAPISoapClient client = ConfigureSoapCleint(request);
            BookResult response = client.Book(
                    supplierRequest.userName,
                    supplierRequest.password,
                    supplierRequest.currency,
                    supplierRequest.language,
                    supplierRequest.email,
                    
                    supplierRequest.checkInDate,
                    supplierRequest.checkOutDate,
                    supplierRequest.roomId,
                    supplierRequest.rooms,
                    supplierRequest.adults,
                    supplierRequest.children,
                    supplierRequest.infant,
                    supplierRequest.yourRef,
                    supplierRequest.specialrequest,
                    supplierRequest.mealId,
                    supplierRequest.adultsList,
                    supplierRequest.childrenList,
                    supplierRequest.paymentMethodId,
                    supplierRequest.creditCardType,
                    supplierRequest.creditCardNumber,
                    supplierRequest.creditCardHolder,
                    supplierRequest.creditCardCVV2,
                    supplierRequest.creditCardExpYear,
                    supplierRequest.creditCardExpMonth,
                    supplierRequest.customerEmail,
                    supplierRequest.invoiceRef
                   );

            var supplierResponse = new SunHotelsBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private StaticXMLAPISoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(SunHotelBookingUrl)));
            StaticXMLAPISoapClient client = new StaticXMLAPISoapClient(binding, address);

            //ProxyHelper helper = new ProxyHelper();
            //helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }

    }
}
