﻿using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;



namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsCancellationResponse
    {
        public CancelBookingRequest SupplierRequest { get; set; }
        public CancellationResult SupplierResponse { get; set; }
    }
}
