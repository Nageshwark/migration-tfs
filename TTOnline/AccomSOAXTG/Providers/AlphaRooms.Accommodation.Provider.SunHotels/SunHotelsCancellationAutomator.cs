﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using System.ServiceModel;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelCancellationAutomator : IAccommodationCancellationAutomatorAsync<SunHotelsCancellationResponse>
    {
        private const string SunHotelCancellationUrl = "SunHotelsCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private readonly ISunHotelsProviderCancellationRequestFactory _sunHotelsProviderCancellationRequestFactory;

        public SunHotelCancellationAutomator(//ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ISunHotelsProviderCancellationRequestFactory sunhotelsSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
            this._sunHotelsProviderCancellationRequestFactory = sunhotelsSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<SunHotelsCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            
            CancelBookingRequest cancelRequest = _sunHotelsProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
            var serialisedRequest = cancelRequest.XmlSerialize();
            StaticXMLAPISoapClient client = ConfigureSoapCleint(request);
               var response = client.CancelBooking(
                    cancelRequest.userName,
                    cancelRequest.password,
                    cancelRequest.bookingID,
                    cancelRequest.language
                    
                   );
             var supplierResponse = new SunHotelsCancellationResponse()
            {
                SupplierRequest = cancelRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private StaticXMLAPISoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(SunHotelCancellationUrl)));
            StaticXMLAPISoapClient client = new StaticXMLAPISoapClient(binding, address);

            //ProxyHelper helper = new ProxyHelper();
            //helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }

    }
}