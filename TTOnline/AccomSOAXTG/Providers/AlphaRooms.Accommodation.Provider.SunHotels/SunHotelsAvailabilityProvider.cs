﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.SunHotels;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<SunHotelsAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<SunHotelsAvailabilityResponse> parser;

        public SunHotelsAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<SunHotelsAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<SunHotelsAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
