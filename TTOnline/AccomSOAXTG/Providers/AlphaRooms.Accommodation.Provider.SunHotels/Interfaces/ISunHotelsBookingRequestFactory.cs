﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService ;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
namespace AlphaRooms.Accommodation.Provider.SunHotels.Interfaces
{
    public interface ISunHotelsBookingRequestFactory
    {
         BookRequest CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);   
    }
}
