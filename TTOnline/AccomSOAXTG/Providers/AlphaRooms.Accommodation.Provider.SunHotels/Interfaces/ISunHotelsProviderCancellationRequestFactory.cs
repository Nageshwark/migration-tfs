﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels.Interfaces
{
    public interface ISunHotelsProviderCancellationRequestFactory
    {
        CancelBookingRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
