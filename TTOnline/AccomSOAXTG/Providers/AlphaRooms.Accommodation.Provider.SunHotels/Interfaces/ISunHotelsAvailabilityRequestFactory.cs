﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels.Interfaces
{
    public interface ISunHotelsAvailabilityRequestFactory
    {
        SearchRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);   
    }
}
