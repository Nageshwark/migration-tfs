﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.SOACommon.Interfaces;
using BookingStatus = AlphaRooms.SOACommon.DomainModels.Enumerators.BookingStatus;


namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsBookingParser : IAccommodationBookingParser<SunHotelsBookingResponse>
    {
         private readonly IAccommodationConfigurationManager _configurationManager;

        public SunHotelsBookingParser()
        {
            this._configurationManager = _configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, SunHotelsBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();
            AccommodationProviderBookingResult bookingResult;
            var result = new AccommodationProviderBookingResult();

            if (responses != null &&
                responses.SupplierResponse != null &&
                responses.SupplierResponse.booking != null &&
                !string.IsNullOrEmpty(responses.SupplierResponse.booking.bookingnumber))
            {

                result.ProviderBookingReference = responses.SupplierResponse.booking.bookingnumber;
                result.Message = string.Empty;
                if (responses.SupplierResponse.booking.Error != null)
                {
                    result.Message = responses.SupplierResponse.booking.Error.Message;
                }
                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);
            }
            else
            {
                var sbMessage = new StringBuilder();
                sbMessage.AppendLine("NTIncoming Booking Parser: Booking Response cannot be parsed because it is null.");

                if (responses != null && responses.SupplierResponse != null && responses.SupplierResponse.Error != null)
                {
                    var error = responses.SupplierResponse.Error;

                    sbMessage.AppendLine("Error Details:");
                    
                    sbMessage.AppendLine(string.Format("ErrorType:{0}, Message:{1}", error.ErrorType, error.Message));
                }


                throw new SupplierApiDataException(sbMessage.ToString());
            }
            return bookingResults;

        }

       
    }
}
