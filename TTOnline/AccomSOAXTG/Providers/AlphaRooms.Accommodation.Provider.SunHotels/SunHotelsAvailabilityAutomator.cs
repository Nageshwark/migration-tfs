﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using System.ServiceModel;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using SearchRequest = AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService.SearchRequest;
using SearchResult = AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService.SearchResult;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<SunHotelsAvailabilityResponse>
    {
        
        private const string SunHotelAvailabilityUrl = "SunHotelAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private readonly ISunHotelsAvailabilityRequestFactory _sunHotelSupplierAvailabilityRequestFactory;

        public SunHotelsAvailabilityAutomator(ISunHotelsAvailabilityRequestFactory sunHotelSupplierAvailabilityRequestFactory)
        {
            this._sunHotelSupplierAvailabilityRequestFactory = sunHotelSupplierAvailabilityRequestFactory;
        }

        public async Task<SunHotelsAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            SunHotelsAvailabilityResponse response = null;

            try
            {
                SearchRequest supplierRequest = this._sunHotelSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("SunHotelsAvailabilityAutomator.GetAvailabilityResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<SunHotelsAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, 
            SearchRequest  supplierRequest)
        {
            var serialisedRequest = supplierRequest.XmlSerialize();
            StaticXMLAPISoapClient client = ConfigureSoapCleint(request);
            
            SearchResult response = client.Search(
                    supplierRequest.userName, 
                    supplierRequest.password, 
                    supplierRequest.language, 
                    supplierRequest.currencies,
                    supplierRequest.checkInDate, 
                    supplierRequest.checkOutDate, 
                    supplierRequest.numberOfRooms, 
                    supplierRequest.destination, 
                    supplierRequest.destinationID, 
                    supplierRequest.hotelIDs, 
                    supplierRequest.resortIDs,
                    supplierRequest.accommodationTypes,
                    supplierRequest.numberOfAdults, 
                    supplierRequest.numberOfChildren, 
                    supplierRequest.infant, 
                    supplierRequest.sortBy, 
                    supplierRequest.sortOrder, 
                    supplierRequest.exactDestinationMatch,                    
                    supplierRequest.blockSuperdeal,                     
                    supplierRequest.mealIds,                
                    supplierRequest.showCoordinates, 
                    supplierRequest.showReviews,
                    supplierRequest.referencePoint,
                    supplierRequest.maxDistanceFromReferencePoint, 
                    supplierRequest.minStarRating, 
                    supplierRequest.maxStarRating, 
                    supplierRequest.featureIds, 
                    supplierRequest.minPrice, 
                    supplierRequest.maxPrice, 
                    supplierRequest.themeIds, 
                    supplierRequest.excludeSharedRooms, 
                    supplierRequest.excludeSharedFacilities, 
                    supplierRequest.prioritizedHotelIds, 
                    supplierRequest.totalRoomsInBatch, 
                    supplierRequest.paymentMethodId 
                     );
                       
            var supplierResponse = new SunHotelsAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private StaticXMLAPISoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            
 
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(SunHotelAvailabilityUrl)));
            StaticXMLAPISoapClient client = new StaticXMLAPISoapClient(binding, address);

            //ProxyHelper helper = new ProxyHelper();
            //helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
      
    }
}
