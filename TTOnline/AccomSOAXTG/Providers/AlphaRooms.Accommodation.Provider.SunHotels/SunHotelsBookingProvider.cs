﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Provider.SunHotels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<SunHotelsBookingResponse> _automator;
        private readonly IAccommodationBookingParser<SunHotelsBookingResponse> _parser;

        public SunHotelsBookingProvider(IAccommodationBookingAutomatorAsync<SunHotelsBookingResponse> automator,
                                                IAccommodationBookingParser<SunHotelsBookingResponse> parser)
        {
            this._automator = automator;
            this._parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var responses = await _automator.GetBookingResponseAsync(request);
            return _parser.GetBookingResults(request, responses);
        }
    }
}
