﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;


namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsAvailabilityParser : IAccommodationAvailabilityParser<SunHotelsAvailabilityResponse>
    {
       public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            SunHotelsAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse,
                    availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "SunHotels Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
           SearchRequest supplierRequest, SearchResult supplierResponse,
            ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = supplierResponse.hotels.Any();

            if (hasResults)
            {
                foreach (var hotel in supplierResponse.hotels)
                {
                    var staticHotel = (StaticHotel)hotel;

                    foreach (var roomType in hotel.roomtypes)
                    {
                        foreach (var room in roomType.rooms)
                        {
                            
                            foreach (var meal in room.meals)
                            {
                                var result = new AccommodationProviderAvailabilityResult();
                                
                                
                                result.RoomNumber = (byte)1;
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;
                                result.EstablishmentName = staticHotel.name;
                                result.EstablishmentEdiCode = staticHotel.hotelid.ToString();
                                result.DestinationEdiCode = staticHotel.destination_id.ToString();

                                result.BoardCode = meal.id.ToString();
                                result.BoardDescription = ((StaticRoomMeal)meal).name;
                                result.RoomCode = room.id.ToString();

                                result.RoomDescription = ((StaticRoomTypeWithRooms)roomType).roomtype;
                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                result.IsOpaqueRate = false;
                                result.IsNonRefundable = false;
                                result.NumberOfAvailableRooms = 0;

                                result.SalePrice = new Money()
                                {
                                    Amount = meal.prices.FirstOrDefault().Value,
                                    CurrencyCode = meal.prices.FirstOrDefault().currency
                                };
                                result.CostPrice = result.SalePrice;
                                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };


                                result.Adults = (byte)supplierRequest.numberOfAdults;
                                result.Children = (byte)supplierRequest.numberOfChildren;
                                result.Infants = (byte)supplierRequest.infant;

                                result.CheckInDate = supplierRequest.checkInDate;
                                result.CheckOutDate = supplierRequest.checkOutDate;

                                result.ProviderSpecificData = new Dictionary<string, string>();

                                

                                availabilityResults.Enqueue(result);
                            }
                        }
                    }
                }
                

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var error = supplierResponse.Error;

                sbMessage.AppendLine("SunHotels Availability Parser: Availability Response cannot be parsed because it is null.");

                if (error != null )
                    sbMessage.AppendLine(string.Format("Error Details: {0}", error.Message));


                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

        //private string GetHotelName(string hotelCode, OTA_HotelAvailRSHotelStay[] hotelStay)
        //{
        //    //var query = from OTA_HotelAvailRSHotelStay p in hotelStay
        //    //            where p.BasicPropertyInfo.HotelCode.ToLower() == hotelCode.ToLower()
        //    //            select p;

        //    //if (query != null && query.Any())
        //    //    return query.First().BasicPropertyInfo.HotelName;
        //    //else
        //    //    return null;
        //}

        //private byte GetGuestCount(GuestType guestType, GuestCountTypeGuestCount[] guestList)
        //{
        //    string ageCode = "";

        //    switch (guestType)
        //    {
        //        case GuestType.Adult:
        //            ageCode = "10";
        //            break;

        //        case GuestType.Child:
        //            ageCode = "8";
        //            break;

        //        case GuestType.Infant:
        //            ageCode = "7";
        //            break;
        //    }

        //    var query = from GuestCountTypeGuestCount guest in guestList
        //                where guest.AgeQualifyingCode == ageCode
        //                select guest.Count;

        //    if (query != null && query.Any())
        //        return (byte)query.First();
        //    else
        //        return 0;
        //}
    }
}
