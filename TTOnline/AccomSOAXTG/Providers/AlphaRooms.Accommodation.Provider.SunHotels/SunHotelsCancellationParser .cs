﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsCancellationlationParser : IAccommodationCancellationParserAsync<SunHotelsCancellationResponse>
    {

        public SunHotelsCancellationlationParser()
        {
          
        }
        public async System.Threading.Tasks.Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, SunHotelsCancellationResponse response)
        {
            var cancellationResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();
            
           
            if (response.SupplierResponse.Code == 1)
            {
                cancellationResult.CancellationStatus = CancellationStatus.Succeeded;
            }
            else
            {
                cancellationResult.CancellationStatus = CancellationStatus.Failed;
            }
            //cancellationResult.Message = response.SupplierResponse.Error.Message;
            
           cancellationResults.Add(cancellationResult);
            return cancellationResults;
        }
      
    }
 
}
