﻿
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{
    public class SunHotelsValuationResponse
    {
       public SearchRequest SupplierRequest { get; set; }
       public SearchResult SupplierResponse { get; set; }

    }
}
