﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels.SunHotelsService;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using System.Text;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.Provider.SunHotels
{

    public class SunHotelsValuationParser : IAccommodationValuationParser<SunHotelsValuationResponse>
    {

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(
            AccommodationProviderValuationRequest request, SunHotelsValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse,
                    valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "SunHotels Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request,
            SearchRequest supplierRequest, SearchResult supplierResponse,
            ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXml = supplierResponse.XmlSerialize();
            }

            bool hasResults = supplierResponse.hotels.Any();
            int roomId = Convert.ToInt32(request.SelectedRooms.First().AvailabilityResult.RoomCode);

            if (hasResults)
            {
                foreach (Hotel hotel in supplierResponse.hotels)
                {
                    var staticHotel = (StaticHotel)hotel;

                    foreach (var roomType in hotel.roomtypes)
                    {
                        foreach (var room in roomType.rooms.Where(r=>r.id == roomId))
                        {
                            foreach (var meal in room.meals)
                            {
                                var result = new AccommodationProviderValuationResult();

                                
                                result.RoomNumber = (byte)1;
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;
                                result.EstablishmentName = staticHotel.name;
                                result.EstablishmentEdiCode = staticHotel.hotelid.ToString();
                                result.DestinationEdiCode = staticHotel.destination_id.ToString();

                                result.BoardCode = meal.id.ToString();
                                result.BoardDescription = ((StaticRoomMeal)meal).name;
                                result.RoomCode = room.id.ToString();

                                result.RoomDescription = ((StaticRoomTypeWithRooms)roomType).roomtype;
                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                result.IsOpaqueRate = false;
                                result.IsNonRefundable = false;
                                result.NumberOfAvailableRooms = 0;

                                result.SalePrice = new Money()
                                {
                                    Amount = meal.prices.FirstOrDefault().Value,
                                    CurrencyCode = meal.prices.FirstOrDefault().currency
                                };
                                result.CostPrice = result.SalePrice;
                                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };
                                
                                result.Adults = (byte)supplierRequest.numberOfAdults;
                                result.Children = (byte)supplierRequest.numberOfChildren;
                                result.Infants = (byte) supplierRequest.infant;

                                result.CheckInDate = supplierRequest.checkInDate;
                                result.CheckOutDate = supplierRequest.checkOutDate;

                                result.CancellationPolicy = GetCancellationPolicy(room.cancellation_policies);

                                result.ProviderSpecificData = new Dictionary<string, string>();

                                availabilityResults.Enqueue(result);
                            }
                        }
                    }
                }


            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var error = supplierResponse.Error;

                sbMessage.AppendLine(
                    "SunHotels valuation Parser: Availability Response cannot be parsed because it is null.");

                if (error != null)
                    sbMessage.AppendLine(string.Format("Error Details: {0}", error.Message));


                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

        private string[] GetCancellationPolicy(PercentageCancellationPolicy[] cancellationPolicies)
        {
            return cancellationPolicies.Select(cancellationPolicy => string.Format("Deadline:{0}, Percentage:{1}, Text:{2}",
                                                cancellationPolicy.deadline,
                                                cancellationPolicy.percentage,
                                                ((StaticPercentageCancellationPolicy)cancellationPolicy).text)).ToArray();
        }
    }
}