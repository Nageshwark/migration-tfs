﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{

    public class JumboValuationParser : IAccommodationValuationParser<JumboValuationResponse>
    {
        private readonly IProviderNonRefundableService nonRefundableService;
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public JumboValuationParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.nonRefundableService = nonRefundableService;
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        
        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, JumboValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateValuationResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateValuationResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Jumbo Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationResultsFromResponse(AccommodationProviderValuationRequest request, valuateExtendsV16 supplierRequest, valuateExtendsV16Response supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.result != null);


            if (hasResults)
            {

                int roomNumber = 1;

                foreach (var room in supplierResponse.result.occupations)
                {
                    var result = new AccommodationProviderValuationResult();
                    var valuationResult = supplierResponse.result;
                    var checkinDate = Convert.ToDateTime(valuationResult.checkIn);
                    var checkoutDate = Convert.ToDateTime(valuationResult.checkOut);

                    result.RoomNumber = (byte)GetRoomNumber(room.roomTypeCode, request.SelectedRooms, supplierResponse.result.occupations, roomNumber);
                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.EstablishmentEdiCode = valuationResult.establishment.id;
                    result.EstablishmentName = valuationResult.establishment.name;
                    result.DestinationEdiCode = valuationResult.establishment.address.cityCode;

                    result.CheckInDate = checkinDate.Date;
                    result.CheckOutDate = checkoutDate.Date;

                    //TODO:Review for Multiroom
                    result.Adults = (byte)room.adults;
                    result.Children = (byte)room.children;
                    result.RoomCode = room.roomTypeCode;
                    result.RoomDescription = room.roomTypeName;
                    result.BoardCode = room.boardTypeCode;
                    result.BoardDescription = room.boardTypeName;
                    result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(room.roomTypeName, room.boardTypeName, result.IsNonRefundable);

                    result.CancellationPolicy = GetCancellationPolicy(valuationResult.cancellationTerms);
                    result.PaymentModel = SetPaymentModel(valuationResult.remarks);

                    result.SalePrice = new Money()
                    {
                        Amount = Convert.ToDecimal(room.amount.value),
                        CurrencyCode = room.amount.currencyCode
                    };

                    result.CostPrice = result.SalePrice;

                    //TODO: set net or cost price based on this?
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("IsNetPrice", IsNetPrice(valuationResult.remarks).ToString());

                    /*
                                
                    result.PaymentModel = paymentModel;

                
                    // TODO: Do we need infants in our system?
                    // NOTE: Unable to set the number of infants from the response as HotelBeds considers infants to be children. Therefore, Number of children = children + infants.
                
                    result.Infants = availabilityResult.Infants;
                

                    result.IsOpaqueRate = isOpaqueRate;
                    result.IsNonRefundableRate = isNonRefundableRate;

                    result.ProviderPaymentOptions = acceptedPaymentCards;
                

                    // Binding Rate
                    result.IsBindingRate = true;
                

                    result.CommissionPercent = decimal.Parse(service.Commission);                
                    result.CommissionAmount = new Money(commissionAmount, valuationCurrency);
                
                    // Rate Type
                    //result.RateType = GetRateType(request, result.IsBindingRate, result.PaymentModel);

                    //result.CancellationPolicy = sb.ToString();
                    */

                    availabilityResults.Enqueue(result);
                    roomNumber++;
                }
            }
            else
            {
                string message = "Jumbo Valuation Parser: Valuation Response cannot be parsed because it is null.";



                throw new SupplierApiDataException(message);
            }
        }

        
        private byte GetRoomNumber(string roomTypeCode, AccommodationProviderValuationRequestRoom[] rooms, ValuatedOccupationV16[] occupations, int roomNumber)
        {
            //First Select the Room object that has Pax from the RoomTypeCode
            var selectedRoom = from r in occupations
                               where r.roomTypeCode == roomTypeCode
                               select r;

            //Now find the matching room in the Request object and fetch the corresponding RoomNumber
            var roomNumberQuery = from r in rooms
                                  where
                                      r.Guests.AdultsCount == selectedRoom.First().adults &&
                                      r.Guests.ChildrenAndInfantsCount == selectedRoom.First().children
                                  select r.RoomNumber;

            if (roomNumberQuery.Any() && roomNumberQuery.Count() == 1)
                return roomNumberQuery.FirstOrDefault();
            else
            {
                return (byte)roomNumber;
            }

        }

        private PaymentModel SetPaymentModel(Comment[] comments)
        {
            PaymentModel paymentModel = PaymentModel.PostPayment;

            var direClientPayment = from Comment c in comments
                                    where c.type == "Direct Client Payment"
                                    select c;

            if (direClientPayment.Any() && !string.IsNullOrEmpty(direClientPayment.First().text))
            {
                bool isDirecClientPayment;
                bool.TryParse(direClientPayment.First().text, out isDirecClientPayment);

                if (isDirecClientPayment)
                    paymentModel = PaymentModel.CustomerPayDirect;
            }

            return paymentModel;
        }

        private string[] GetCancellationPolicy(CancellationTerm[] cancellationTerms)
        {
            return cancellationTerms.Select(policy => string.Format("From:{0}, ApplicableTo:{1}, Amount:{2}, Percent:{3}", policy.from, policy.appliableTo, policy.amount, policy.percent)).ToArray();
        }

        private string GetCommentValue(CommentsRS[] comments, string commentType)
        {
            string text = string.Empty;

            var query = from CommentsRS c in comments
                                    where c.type == commentType
                                    select c;

            if (query.Any() && !string.IsNullOrEmpty(query.First().text))
            {
                text = query.First().text;
            }

            return text;
        }

        private bool IsNetPrice(Comment[] comments)
        {
            bool isNetPrice = false;

            var query = from Comment c in comments
                        where c.type == "PVP / Retail Price"
                        select c;

            if (query.Any() && !string.IsNullOrEmpty(query.First().text))
            {
                bool.TryParse(query.First().text, out isNetPrice);

            }

            return isNetPrice;
        }
    }
}
