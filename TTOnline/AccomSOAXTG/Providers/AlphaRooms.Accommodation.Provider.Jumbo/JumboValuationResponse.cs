﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboValuationResponse
    {
        public valuateExtendsV16 SupplierRequest { get; set; }

        public valuateExtendsV16Response SupplierResponse { get; set; }
    }
}
