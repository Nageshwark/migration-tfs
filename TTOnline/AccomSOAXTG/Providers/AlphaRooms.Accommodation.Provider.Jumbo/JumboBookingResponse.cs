﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Jumbo accepts and returns XML strings.
    /// </summary>
    public class JumboBookingResponse
    {
        public confirmExtendsV16 SupplierRequest { get; set; }
        public confirmExtendsV16Response SupplierResponse { get; set; }
    }
}
