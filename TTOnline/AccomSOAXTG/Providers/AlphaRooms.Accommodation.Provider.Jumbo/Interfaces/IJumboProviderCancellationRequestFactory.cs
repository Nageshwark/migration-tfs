﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Jumbo.Service.BasketHandlerService;

namespace AlphaRooms.Accommodation.Provider.Jumbo.Interfaces
{
    public interface IJumboProviderCancellationRequestFactory
    {
        cancel CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
