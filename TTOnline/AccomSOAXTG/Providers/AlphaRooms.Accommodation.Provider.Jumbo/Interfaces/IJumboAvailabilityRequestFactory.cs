﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;

namespace AlphaRooms.Accommodation.Provider.Jumbo.Interfaces
{
    public interface IJumboAvailabilityRequestFactory
    {
        availableHotelsByMultiQueryV17 CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
