﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.Jumbo.Interfaces
{
    public interface IJumboProviderBookingRequestFactory
    {
        confirmExtendsV16 CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
