﻿using System;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service.BasketHandlerService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboCancellationAutomator : IAccommodationCancellationAutomatorAsync<JumboCancellationResponse>
    {
        private const string JumboCancellationUrl = "JumboCancellationUrl";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IJumboProviderCancellationRequestFactory JumboProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public JumboCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IJumboProviderCancellationRequestFactory JumboSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.JumboProviderCancellationRequestFactory = JumboSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<JumboCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            JumboCancellationResponse response = null;

            try
            {
                var supplierRequest = JumboProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                string url = request.Provider.Parameters.GetParameterValue(JumboCancellationUrl);

                var jumboService = new BasketHandlerService();
                jumboService.Url = url;
                jumboService.Proxy = SetProxy(request);



                var supplierResponse = jumboService.cancel(supplierRequest);

                response = new JumboCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("JumboBookingAutomator.GetCancellationResponseAsync: Jumbo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        private WebProxy SetProxy(AccommodationProviderCancellationRequest request)
        {
            WebProxy proxy = null;

            try
            {
                bool useProxy = false;

                if (request.Provider.Parameters.Exists(a => a.ParameterName == UseProxy))
                {
                    useProxy = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(UseProxy));

                    if (useProxy)
                    {
                        string proxyUrl = request.Provider.Parameters.GetParameterValue(ProxyUrl);
                        int proxyPort = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(ProxyPort));
                        string proxyUsername = request.Provider.Parameters.GetParameterValue(ProxyUsername);
                        string proxyPassword = request.Provider.Parameters.GetParameterValue(ProxyPassword);


                        proxy = new WebProxy(proxyUrl, proxyPort);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    }

                }

            }
            catch //Suppress any exception if no Proxy Details was not defined (default behaviour)
            {
            }

            return proxy;

        }

       
    }
}
