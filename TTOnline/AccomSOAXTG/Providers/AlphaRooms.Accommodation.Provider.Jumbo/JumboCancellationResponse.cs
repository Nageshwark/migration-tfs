﻿
using AlphaRooms.Accommodation.Provider.Jumbo.Service.BasketHandlerService;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboCancellationResponse
    {
        public cancel SupplierRequest { get; set; }
        public cancelResponse SupplierResponse { get; set; }
    }
}
