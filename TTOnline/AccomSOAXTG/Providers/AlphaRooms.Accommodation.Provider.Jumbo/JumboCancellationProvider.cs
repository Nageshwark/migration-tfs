﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<JumboCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<JumboCancellationResponse> parser;

        public JumboCancellationProvider(IAccommodationCancellationAutomatorAsync<JumboCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<JumboCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
