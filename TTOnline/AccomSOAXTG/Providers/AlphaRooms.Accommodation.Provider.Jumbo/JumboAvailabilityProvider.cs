﻿using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<JumboAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<JumboAvailabilityResponse> parser;

        public JumboAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<JumboAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<JumboAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
