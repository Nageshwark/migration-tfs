﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;


namespace AlphaRooms.Accommodation.Provider.Jumbo.Factories
{
    public class JumboValuationRequestFactory : IJumboProviderValuationRequestFactory
    {
        private const string JumboCurrency = "JumboCurrency";
        private const string JumboAgencyCode = "JumboAgencyCode";
        private const string JumboBranchCode = "JumboBranchCode";
        private const string JumboPointOfSaleId = "JumboPointOfSaleId";
        private const string JumboLanguage = "JumboLanguage";
        private const string JumboMaxNumRows = "JumboMaxNumRows";
        private const string JumboMaxRoomCombination = "JumboMaxRoomCombination";
        private const string JumboMaxIsOnlineOnly = "JumboMaxIsOnlineOnly";


        public valuateExtendsV16 CreateSupplierValuationRequest(AccommodationProviderValuationRequest request, AccommodationProviderValuationRequestRoom [] selectedRooms)
        {
            var supplierRequest = new valuateExtendsV16
            {
                ValuationRQV15_1 = new ValuationRQV15
                {
                    agencyCode = request.Provider.Parameters.GetParameterValue(JumboAgencyCode),
                    brandCode = request.Provider.Parameters.GetParameterValue(JumboBranchCode),
                    pointOfSaleId = request.Provider.Parameters.GetParameterValue(JumboPointOfSaleId),
                    language = request.Provider.Parameters.GetParameterValue(JumboLanguage),
                    onlyOnline = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(JumboMaxIsOnlineOnly)),
                    
                    checkin = selectedRooms.First().AvailabilityRequest.CheckInDate,
                    checkout = selectedRooms.First().AvailabilityRequest.CheckOutDate,
                    occupancies = CreateOccupancy(selectedRooms),
                    establishmentId = selectedRooms.First().AvailabilityResult.EstablishmentEdiCode,
                }
            };
            
            return supplierRequest;
        }

        private RoomOccupancy[] CreateOccupancy( AccommodationProviderValuationRequestRoom [] selectedRooms)
        {
            var occupancies = new List<RoomOccupancy>();
            
            foreach (var room in selectedRooms)
            {
                var occupancy = new RoomOccupancy();

                occupancy.numberOfRooms = 1;
                occupancy.adults = room.Guests.AdultsCount;
                occupancy.children = room.Guests.ChildrenCount + room.Guests.InfantsCount;

                if (occupancy.children > 0)
                    occupancy.childrenAges = GetChildAges(room.Guests);

                occupancy.boardTypeCode = room.AvailabilityResult.BoardCode;
                occupancy.roomTypeCode = room.AvailabilityResult.RoomCode;

              
                occupancies.Add(occupancy);
            }

            return occupancies.ToArray();
        }

        private int[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {

            var ages = from AccommodationProviderAvailabilityRequestGuest guest in guests
                       where guest.Type == GuestType.Child || guest.Type == GuestType.Infant
                       select (int)guest.Age;

            return ages.ToArray();
        }
       

     

        
    }
}
