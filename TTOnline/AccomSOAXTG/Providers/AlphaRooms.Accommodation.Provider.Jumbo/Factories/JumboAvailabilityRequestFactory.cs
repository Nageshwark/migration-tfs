﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;


namespace AlphaRooms.Accommodation.Provider.Jumbo.Factories
{
    public class JumboAvailabilityRequestFactory : IJumboAvailabilityRequestFactory
    {
        private const string JumboCurrency = "JumboCurrency";
        private const string JumboAgencyCode = "JumboAgencyCode";
        private const string JumboBranchCode = "JumboBranchCode";
        private const string JumboPointOfSaleId = "JumboPointOfSaleId";
        private const string JumboLanguage = "JumboLanguage";
        private const string JumboMaxNumRows = "JumboMaxNumRows";
        private const string JumboMaxRoomCombination = "JumboMaxRoomCombination";
        private const string JumboMaxIsOnlineOnly = "JumboMaxIsOnlineOnly";

        public availableHotelsByMultiQueryV17 CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var availRequest = new AvailableHotelsByMultiQueryRQV16
            {
                agencyCode = request.Provider.Parameters.GetParameterValue(JumboAgencyCode),
                brandCode = request.Provider.Parameters.GetParameterValue(JumboBranchCode),
                pointOfSaleId = request.Provider.Parameters.GetParameterValue(JumboPointOfSaleId),
                language = request.Provider.Parameters.GetParameterValue(JumboLanguage),
                numRows = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(JumboMaxNumRows)),
                maxRoomCombinationsPerEstablishment = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(JumboMaxRoomCombination)),
                onlyOnline = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(JumboMaxIsOnlineOnly)),
                fromPrice = 0,
                fromRow = 0,
                toPrice = 999999,
                checkin = request.CheckInDate,
                checkout = request.CheckOutDate,
                occupancies = CreateOccupancy(request)
            };

            if (request.EstablishmentCodes.Length > 0)
                availRequest.establishmentId = request.EstablishmentCodes;
            else if (request.DestinationCodes.Length > 0)
            {
                var destinationCode = request.DestinationCodes.First();

                if (destinationCode.StartsWith("G"))
                {
                    var areaCode = new string[] { destinationCode.Remove(0, 1) };
                    availRequest.areaCode = areaCode;
                }
                else
                {
                    availRequest.cityCode = new string[] { destinationCode };
                }
            }
            else
            {
                throw new ArgumentException("Please set a Destionation or Establishment Code");
            }

            var supplierRequest = new availableHotelsByMultiQueryV17
            {
                AvailableHotelsByMultiQueryRQV16_1 = availRequest
            };
            
            return supplierRequest;
        }

        private Occupancy[] CreateOccupancy(AccommodationProviderAvailabilityRequest request)
        {
            var occupancies = new Occupancy[request.Rooms.Count()];

            int roomCounter = 0;
            foreach (var room in request.Rooms)
            {
                var occupancy =new Occupancy();

                occupancy.numberOfRooms = 1;
                occupancy.adults = room.Guests.AdultsCount;
                occupancy.children = room.Guests.ChildrenCount + room.Guests.InfantsCount;

                if (occupancy.children > 0)
                    occupancy.childrenAges = GetChildAges(room.Guests);

                occupancies[roomCounter] = occupancy;
                roomCounter++;
            }

            return occupancies;
        }

        private int[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {

            var ages = from AccommodationProviderAvailabilityRequestGuest guest in guests
                where guest.Type == GuestType.Child || guest.Type == GuestType.Infant
                select (int)guest.Age;

            return ages.ToArray();
        }

        

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return  Convert.ToInt32(duration.TotalDays);
        }
       
    }
}
