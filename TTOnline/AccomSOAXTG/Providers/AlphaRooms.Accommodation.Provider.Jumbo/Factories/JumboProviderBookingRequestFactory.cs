﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboProviderBookingRequestFactory : IJumboProviderBookingRequestFactory
    {
        
        private const string JumboAgencyCode = "JumboAgencyCode";
        private const string JumboBranchCode = "JumboBranchCode";
        private const string JumboPointOfSaleId = "JumboPointOfSaleId";
        private const string JumboLanguage = "JumboLanguage";
        private const string JumboMaxIsOnlineOnly = "JumboMaxIsOnlineOnly";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public confirmExtendsV16 CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;

            var supplierRequest = new confirmExtendsV16
            {
                ConfirmRQV15_1 = new ConfirmRQV15
                {
                    agencyCode = request.Provider.Parameters.GetParameterValue(JumboAgencyCode),
                    brandCode = request.Provider.Parameters.GetParameterValue(JumboBranchCode),
                    pointOfSaleId = request.Provider.Parameters.GetParameterValue(JumboPointOfSaleId),
                    language = request.Provider.Parameters.GetParameterValue(JumboLanguage),
                    onlyOnline = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(JumboMaxIsOnlineOnly)),
                    
                    establishmentId = valuationResult.EstablishmentEdiCode,
                    checkin = valuationResult.CheckInDate,
                    checkout = valuationResult.CheckOutDate,
                    occupancies = CreateOccupancy(request.ValuatedRooms),
                    titular = request.Customer.FirstName + " " + request.Customer.Surname,
                    sendDocumentationTo = request.Customer.EmailAddress
                }
               
            };

            

            return supplierRequest;


        }

        private RoomOccupancy[] CreateOccupancy(AccommodationProviderBookingRequestRoom[] rooms)
        {
            var occupancies = new List<RoomOccupancy>();

            foreach (var room in rooms)
            {
                var occupancy = new RoomOccupancy();

                occupancy.numberOfRooms = 1;
                occupancy.adults = room.Guests.AdultsCount;
                occupancy.children = room.Guests.ChildrenCount + room.Guests.InfantsCount;
                occupancy.roomTypeCode = room.ValuationResult.RoomCode;
                occupancy.boardTypeCode = room.ValuationResult.BoardCode;

                if (occupancy.children > 0)
                    occupancy.childrenAges = GetChildAges(room.Guests);

                occupancies.Add(occupancy);
            }

            return occupancies.ToArray();
        }

        private int[] GetChildAges(AccommodationProviderBookingRequestRoomGuestCollection guests)
        {

            var ages = from AccommodationProviderBookingRequestRoomGuest guest in guests
                       where guest.Type == GuestType.Child || guest.Type == GuestType.Infant
                       select (int)guest.Age;

            return ages.ToArray();
        }
        
    }
}
