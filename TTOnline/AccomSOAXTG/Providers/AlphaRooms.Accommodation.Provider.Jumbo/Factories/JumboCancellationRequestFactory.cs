﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service.BasketHandlerService;

namespace AlphaRooms.Accommodation.Provider.Jumbo.Factories
{
    public class JumboCancellationRequestFactory : IJumboProviderCancellationRequestFactory
    {
     
     
        private const string JumboAgencyCode = "JumboAgencyCode";
        private const string JumboBranchCode = "JumboBranchCode";
        private const string JumboPointOfSaleId = "JumboPointOfSaleId";
        private const string JumboLanguage = "JumboLanguage";
     
        public cancel CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new cancel
            {
                CancelRQ_1 = new CancelRQ
                {
                    agencyCode = request.Provider.Parameters.GetParameterValue(JumboAgencyCode),
                    brandCode = request.Provider.Parameters.GetParameterValue(JumboBranchCode),
                    pointOfSaleId = request.Provider.Parameters.GetParameterValue(JumboPointOfSaleId),
                    language = request.Provider.Parameters.GetParameterValue(JumboLanguage),
                    basketId = request.ProviderBookingReference
                }

            };

            return supplierRequest;
        }
    }
}
