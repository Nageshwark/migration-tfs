﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Jumbo;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<JumboValuationResponse> automator;
        private readonly IAccommodationValuationParser<JumboValuationResponse> parser;

        public JumboValuationProvider(IAccommodationValuationAutomatorAsync<JumboValuationResponse> automator,
                                                IAccommodationValuationParser<JumboValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
