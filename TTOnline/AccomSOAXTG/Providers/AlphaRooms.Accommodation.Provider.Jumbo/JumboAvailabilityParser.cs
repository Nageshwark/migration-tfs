﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboAvailabilityParser:IAccommodationAvailabilityParser<JumboAvailabilityResponse>
    {
        private readonly IProviderNonRefundableService nonRefundableService;
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public JumboAvailabilityParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.nonRefundableService = nonRefundableService;
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            JumboAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Jumbo Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, availableHotelsByMultiQueryV17 supplierRequest, availableHotelsByMultiQueryV17Response supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.result !=null && supplierResponse.result.availableHotels !=null && supplierResponse.result.availableHotels.Length> 0 );
                
            
            if (hasResults)
            {
                
                foreach (var hotel in supplierResponse.result.availableHotels)
                {
                    foreach (var roomCombination in hotel.roomCombinations)
                    {
                       
                        foreach (var price in roomCombination.prices)
                        {
                            int roomNumber = 1;
                            foreach (var roomPrice in price.roomPrices)
                            {
                                var result = new AccommodationProviderAvailabilityResult();

                                //result.Id = new Guid();
                                result.RoomNumber = GetRoomNumber(roomPrice.typeCode, request.Rooms, roomCombination.rooms, roomNumber);

                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;
                                result.PaymentModel = SetPaymentModel(hotel.establishment.comments);

                                result.DestinationEdiCode = hotel.establishment.address.cityCode;
                                result.EstablishmentEdiCode = hotel.establishment.id.PadLeft(10,'0');
                                result.EstablishmentName = hotel.establishment.name;

                                result.RoomDescription = roomPrice.typeName;
                                result.RoomCode = roomPrice.typeCode;

                                result.CheckInDate = request.CheckInDate;
                                result.CheckOutDate = request.CheckOutDate;


                                result.BoardCode = price.boardCategoryCode;
                                result.BoardDescription = price.boardTypeName;
                                result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(roomPrice.typeName, result.BoardDescription, result.IsNonRefundable);

                                result.SalePrice = new Money()
                                {
                                    Amount = Convert.ToDecimal(roomPrice.price),
                                    CurrencyCode = price.amount.currencyCode
                                };

                                //what should we populate here? there is only one price in H4U response.
                                result.CostPrice = result.SalePrice;

                                result.Adults = (byte)roomCombination.rooms[roomNumber - 1].adults;
                                result.Children = (byte)roomCombination.rooms[roomNumber - 1].children;

                                //TODO: set net or cost price based on this?
                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("IsNetPrice", IsNetPrice(hotel.establishment.comments).ToString());

                                availabilityResults.Enqueue(result);

                                roomNumber++;
                            }
                            
                        }


                        
                        
                    }
                }

            }
            else
            {
                string message = "Jumbo Availability Parser: Availability Response cannot be parsed because it is null.";

                

                //throw new SupplierApiDataException(message);
            }
            
        }

        /// <summary>
        /// As the Rooms returned in the reponse are not orddered and matched according to the requested rooms.  This method  
        /// allocates the correct room number based on the occupany in the response.  If multiple room has the same occupancy
        /// then use the RoomNumber Counter.
        /// </summary>
        /// <param name="typeCode">Roomtype Code</param>
        /// <param name="rooms">Requested Rooms</param>
        /// <param name="supplierRooms">Rooms in the Response </param>
        /// <param name="number"></param>
        /// <returns></returns>
        private byte GetRoomNumber(string typeCode, AccommodationProviderAvailabilityRequestRoom[] rooms, Room[] supplierRooms, int roomNnumber)
        {
            //First Select the Room object that has Pax from the RoomTypeCode
            var selectedRoom = from r in supplierRooms where r.typeCode == typeCode
                               select r;

            //Now find the matching room in the Request object and fetch the corresponding RoomNumber
            var roomNumberQuery = from r in rooms
                where
                    r.Guests.AdultsCount == selectedRoom.First().adults &&
                    r.Guests.ChildrenAndInfantsCount == selectedRoom.First().children
                select r.RoomNumber;

            if (roomNumberQuery.Any() && roomNumberQuery.Count() == 1)
                return roomNumberQuery.FirstOrDefault();
            else
            {
                return (byte)roomNnumber;
            }

        }


        private PaymentModel SetPaymentModel(CommentsRS[] comments)
        {
            PaymentModel paymentModel = PaymentModel.PostPayment;

            var direClientPayment = from CommentsRS c in comments
                where c.type == "Direct Client Payment"
                select c;

            if (direClientPayment.Any() && !string.IsNullOrEmpty(direClientPayment.First().text))
            {
                bool isDirecClientPayment;
                bool.TryParse(direClientPayment.First().text, out isDirecClientPayment);

                if (isDirecClientPayment)
                    paymentModel= PaymentModel.CustomerPayDirect;
            }

            return paymentModel;
        }

        private bool IsNetPrice(CommentsRS[] comments)
        {
            bool isNetPrice = false;

            var query = from CommentsRS c in comments
                                    where c.type == "PVP / Retail Price"
                                    select c;

            if (query.Any() && !string.IsNullOrEmpty(query.First().text))
            {
                bool.TryParse(query.First().text, out isNetPrice);
                
            }

            return isNetPrice;
        }
    }
}
