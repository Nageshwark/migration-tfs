﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboCancellationParser : IAccommodationCancellationParserAsync<JumboCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public JumboCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, JumboCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();

            try
            {
                var supplierResponse = response.SupplierResponse;

                if (request.Debugging)
                {
                    string supplierResponseXML = supplierResponse.XmlSerialize();
                }

                bool hasResults = supplierResponse != null && supplierResponse.result != null ;
                
                if (hasResults)
                {
                    cancellationResult.CancellationStatus = CancellationStatus.Succeeded;
                    cancellationResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";
                    
                    // decimal cancelCost = cancelResponse.Purchase.TotalPrice;
                    //bookingResult.CancellationCost = new Money(cancelCost, cancelResponse.Purchase.Currency.code);
                   
                }
                else
                {
                    cancellationResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the cancellation response from Jumbo.");
                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    cancellationResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Jumbo.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(cancellationResult);

            return bookingResults;
        }
       
    }
}
