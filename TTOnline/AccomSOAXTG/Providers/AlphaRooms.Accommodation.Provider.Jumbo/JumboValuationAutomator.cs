﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboValuationAutomator : IAccommodationValuationAutomatorAsync<JumboValuationResponse>
    {
        private const string JumboAvailabilityUrl = "JumboAvailabilityUrl";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";
        


        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IJumboProviderValuationRequestFactory _jumboProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public JumboValuationAutomator(    //IAccommodationConfigurationManager configurationManager,
                                            IJumboProviderValuationRequestFactory jumboSupplierValuationRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            
            //this.configurationManager = configurationManager;
            this._jumboProviderValuationRequestFactory = jumboSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<JumboValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            
            JumboValuationResponse response = null;
            valuateExtendsV16 supplierRequest = null;
            try
            {
                supplierRequest = this._jumboProviderValuationRequestFactory.CreateSupplierValuationRequest(request, request.SelectedRooms);

                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierValuationResponseAsync(request, supplierRequest));
                response = await GetSupplierValuationResponseAsync(request, supplierRequest);


            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    
                        sb.AppendLine();
                        sb.AppendLine("Request:");
                        sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Response:");
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());
                        sb.AppendLine();
                    
                }

                throw new SupplierApiException(string.Format("JumboValuationAutomator.GetValuationResponseAsync: Jumbo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task <JumboValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, valuateExtendsV16 supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            string url = request.Provider.Parameters.GetParameterValue(JumboAvailabilityUrl);
            var jumboService = new HotelBookingHandlerService();

            jumboService.Url = url;
            jumboService.Proxy = SetProxy(request);
            
            var supplierResponse = jumboService.valuateExtendsV16(supplierRequest);
            

            JumboValuationResponse response = new JumboValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = supplierResponse
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, response);

            return response;
        }

        private WebProxy SetProxy(AccommodationProviderValuationRequest request)
        {
            WebProxy proxy = null;

            try
            {
                bool useProxy = false;

                if (request.Provider.Parameters.Exists(a => a.ParameterName == UseProxy))
                {
                    useProxy = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(UseProxy));

                    if (useProxy)
                    {
                        string proxyUrl = request.Provider.Parameters.GetParameterValue(ProxyUrl);
                        int proxyPort = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(ProxyPort));
                        string proxyUsername = request.Provider.Parameters.GetParameterValue(ProxyUsername);
                        string proxyPassword = request.Provider.Parameters.GetParameterValue(ProxyPassword);


                        proxy = new WebProxy(proxyUrl, proxyPort);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    }

                }

            }
            catch //Suppress any exception if no Proxy Details was not defined (default behaviour)
            {
            }

            return proxy;

        }


    
    }
}
