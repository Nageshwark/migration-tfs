﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<JumboBookingResponse>>
    {
        private const string JumboBookingUrl = "JumboBookingUrl";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IJumboProviderBookingRequestFactory JumboProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public JumboBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IJumboProviderBookingRequestFactory JumboSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.JumboProviderBookingRequestFactory = JumboSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<JumboBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<JumboBookingResponse> responses = new List<JumboBookingResponse>();

            confirmExtendsV16 supplierRequest = null;
            JumboBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.JumboProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.JumboProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("JumboBookingAutomator.GetBookingResponseAsync: Jumbo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<JumboBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, confirmExtendsV16 supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            string url = request.Provider.Parameters.GetParameterValue(JumboBookingUrl);

            HotelBookingHandlerService jumboService = new HotelBookingHandlerService();
            jumboService.Url = url;
            jumboService.Proxy = SetProxy(request);



            var supplierResponse = jumboService.confirmExtendsV16(supplierRequest);

            JumboBookingResponse response = new JumboBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = supplierResponse
            };

            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response);

            return response;
        }

       

        private WebProxy SetProxy(AccommodationProviderBookingRequest request)
        {
            WebProxy proxy = null;

            try
            {
                bool useProxy = false;

                if (request.Provider.Parameters.Exists(a => a.ParameterName == UseProxy))
                {
                    useProxy = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(UseProxy));

                    if (useProxy)
                    {
                        string proxyUrl = request.Provider.Parameters.GetParameterValue(ProxyUrl);
                        int proxyPort = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(ProxyPort));
                        string proxyUsername = request.Provider.Parameters.GetParameterValue(ProxyUsername);
                        string proxyPassword = request.Provider.Parameters.GetParameterValue(ProxyPassword);


                        proxy = new WebProxy(proxyUrl, proxyPort);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    }

                }

            }
            catch //Suppress any exception if no Proxy Details was not defined (default behaviour)
            {
            }

            return proxy;

        }
       
    }
}
