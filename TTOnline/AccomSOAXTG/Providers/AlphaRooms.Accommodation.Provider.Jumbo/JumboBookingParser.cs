﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboBookingParser : IAccommodationBookingParser<IEnumerable<JumboBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public JumboBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<JumboBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Jumbo Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        confirmExtendsV16 supplierRequest,
                                                        confirmExtendsV16Response supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }

            bool hasResults = supplierResponse != null && supplierResponse.result != null ;

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                result.ProviderBookingReference = supplierResponse.result.basket.basketId.ToString();

                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("Status", supplierResponse.result.status);
                result.ProviderSpecificData.Add("ServiceId", supplierResponse.result.serviceId);

                StringBuilder sbMessage = new StringBuilder();
                foreach (var c in supplierResponse.result.comments)
                {
                    sbMessage.AppendLine(string.Format("{0}:{1}", c.type, c.text));
                }

                result.Message = sbMessage.ToString();

                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);

            }
            else
            {
                var sbMessage = new StringBuilder();
                
                sbMessage.AppendLine("Jumbo Booking Parser: Booking Response cannot be parsed because it is null.");
                
                throw new SupplierApiDataException(sbMessage.ToString());
            }
            
        }

       
    }
}
