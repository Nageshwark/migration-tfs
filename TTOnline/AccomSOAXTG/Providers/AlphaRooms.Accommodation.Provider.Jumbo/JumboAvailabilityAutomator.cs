﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;


namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboAvailabilityAutomator:IAccommodationAvailabilityAutomatorAsync<JumboAvailabilityResponse>
    {
        private const string JumboAvailabilityUrl = "JumboAvailabilityUrl";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort ="ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IJumboAvailabilityRequestFactory _jumboSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public JumboAvailabilityAutomator(IJumboAvailabilityRequestFactory jumboAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            this._jumboSupplierAvailabilityRequestFactory = jumboAvailabilityRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<JumboAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            JumboAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                availableHotelsByMultiQueryV17 supplierRequest = this._jumboSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("JumboAvailabilityAutomator.GetAvailabilityResponseAsync: Jumbo supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<JumboAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, availableHotelsByMultiQueryV17 supplierRequest)
        {
            
            string url = request.Provider.Parameters.GetParameterValue(JumboAvailabilityUrl);
            string serialisedRequest = supplierRequest.XmlSerialize();

            HotelBookingHandlerService jumboService = new HotelBookingHandlerService();
            jumboService.Url = url;
            jumboService.Proxy = SetProxy(request);

            var response = jumboService.availableHotelsByMultiQueryV17(supplierRequest);
            
            
            JumboAvailabilityResponse supplierResponse = new JumboAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };


            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

        private WebProxy SetProxy(AccommodationProviderAvailabilityRequest request)
        {
            WebProxy proxy = null;

            try
            {
                bool useProxy = false;
                
                if (request.Provider.Parameters.Exists(a=>a.ParameterName == UseProxy))
                {
                    useProxy = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(UseProxy));

                    if (useProxy)
                    {
                        string proxyUrl = request.Provider.Parameters.GetParameterValue(ProxyUrl);
                        int proxyPort = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(ProxyPort));
                        string proxyUsername = request.Provider.Parameters.GetParameterValue(ProxyUsername);
                        string proxyPassword = request.Provider.Parameters.GetParameterValue(ProxyPassword);

                        
                        proxy = new WebProxy(proxyUrl, proxyPort);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    }

                }

            }
            catch //Suppress any exception if no Proxy Details was not defined (default behaviour)
            {
            }

            return proxy;

        }
        

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("Jumbo supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("Jumbo supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("Jumbo supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("Jumbo supports upto 2 Infants");
            //}
        }
    }
}
