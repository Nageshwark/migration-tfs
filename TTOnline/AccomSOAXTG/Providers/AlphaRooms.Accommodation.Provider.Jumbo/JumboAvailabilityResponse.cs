﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Jumbo.Service;

namespace AlphaRooms.Accommodation.Provider.Jumbo
{
    public class JumboAvailabilityResponse
    {
        public availableHotelsByMultiQueryV17 SupplierRequest { get; set; }
        public availableHotelsByMultiQueryV17Response SupplierResponse { get; set; }

    }
}
