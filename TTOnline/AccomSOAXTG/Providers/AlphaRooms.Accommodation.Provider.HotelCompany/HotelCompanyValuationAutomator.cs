﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyValuationAutomator : IAccommodationValuationAutomatorAsync<HotelCompanyValuationResponse>
    {
        private readonly IProviderLoggerService loggerService;
        private readonly ILogger logger;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IHotelCompanyValuationRequestFactory valuationRequestFactory;
        public HotelCompanyValuationAutomator(IProviderLoggerService loggerService,
            ILogger logger, IProviderOutputLogger outputLogger, IHotelCompanyValuationRequestFactory valuationRequestFactory)
        {
            this.loggerService = loggerService;
            this.logger = logger;
            this.outputLogger = outputLogger;
            this.valuationRequestFactory = valuationRequestFactory;
            
        }
        public async Task<HotelCompanyValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            try
            {
                var supplierRequest = valuationRequestFactory.GetValuationRequest(request);
                var response = await GetProviderResponseAsync(request, supplierRequest);
                return response;
            }
            catch (Exception exception)
            {
                var exceptionMessage = exception.GetDetailedMessageWithInnerExceptions();
                logger.Error(exceptionMessage);
                throw new SupplierApiException(exceptionMessage);
            }
        }

        private async Task<HotelCompanyValuationResponse> GetProviderResponseAsync(AccommodationProviderValuationRequest request,
            Tuple<Authentication, ReserveAccommodation> supplierRequest)
        {

            var valuationClient = valuationRequestFactory.GetBookingServiceClient(request.Provider);
            var authentication = supplierRequest.Item1;
            var reserveAccommodationRequest = supplierRequest.Item2;
            var valuationResponse =
                 await valuationClient.ReserveAccommodationAsync(authentication, reserveAccommodationRequest);

            loggerService.SetProviderRequest(request, reserveAccommodationRequest);
            loggerService.SetProviderResponse(request, valuationResponse);

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task.Run(() => outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, valuationResponse));
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            return new HotelCompanyValuationResponse()
            {
                ReserveAccommodationResult = valuationResponse.ToReserveAccommodationResult,
                Authentication = authentication
            };

        }
    }
}
