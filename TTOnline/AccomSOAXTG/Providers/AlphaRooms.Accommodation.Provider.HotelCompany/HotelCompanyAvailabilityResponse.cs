﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanySearchService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyAvailabilityResponse
    {
        public SearchAvailabilityResponse SupplierResponse { get; set; }
        public Authentication SupplierAuthentication { get; set; }
        public AccommodationProviderAvailabilityRequest AvailabilityRequest { get; set; }

    }
}
