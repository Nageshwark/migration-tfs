﻿using System;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyBookingParser : IAccommodationBookingParser<HotelCompanyBookingResponse>
    {
        private readonly ILogger logger;
        public HotelCompanyBookingParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, HotelCompanyBookingResponse bookingResponse)
        {
            var response = bookingResponse.BookingResponse;
            var result = new List<AccommodationProviderBookingResult>();
            if (string.IsNullOrEmpty(response.ErrorMessage))
            {
                try
                {
                    result.Add(new AccommodationProviderBookingResult()
                    {
                        BookingStatus = BookingStatus.Confirmed,
                        ProviderBookingReference = response.BookingReferenceNo,
                        Message = string.Join(",", response.AdditionalInfo)
                    });                 

                }
                catch (Exception exception)
                {
                    logger.Error($"HotelCompany Booking : Error in  Response  {exception.GetDetailedMessageWithInnerExceptions()} ");
                    throw new SupplierApiException($"HotelCompany Booking : Error in  Response  {exception.GetDetailedMessageWithInnerExceptions()} ");
                }
            }
            else
            {
                result.Add(new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    ProviderBookingReference = String.Empty                   
                });
                var errorMessage = $"HotelCompany Booking Error: {response.ErrorMessage}";
                logger.Error(errorMessage);
                
            }

            return result;
        }
    }
}
