﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<HotelCompanyValuationResponse> valuationAutomator;
        private readonly IAccommodationValuationParser<HotelCompanyValuationResponse> valuationParser;

        public HotelCompanyValuationProvider(IAccommodationValuationAutomatorAsync<HotelCompanyValuationResponse> valuationAutomator,
            IAccommodationValuationParser<HotelCompanyValuationResponse> valuationParser)
        {
            this.valuationAutomator = valuationAutomator;
            this.valuationParser = valuationParser;
        }
        public async override Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var response = await valuationAutomator.GetValuationResponseAsync(request);
            return  valuationParser.GetValuationResults(request, response);
        }
    }
}
