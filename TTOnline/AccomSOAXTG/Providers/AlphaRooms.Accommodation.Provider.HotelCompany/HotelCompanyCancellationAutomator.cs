﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyCancellationAutomator : IAccommodationCancellationAutomatorAsync<HotelCompanyCancellationResponse>
    {
        private readonly IProviderLoggerService loggerService;
        private readonly IProviderOutputLogger outputLogger;
        private readonly ILogger logger;

        public HotelCompanyCancellationAutomator(IProviderLoggerService loggerService, 
            IProviderOutputLogger outputLogger, ILogger logger)
        {
            this.loggerService = loggerService;
            this.logger = logger;
            this.outputLogger = outputLogger;
        }

        public async Task<HotelCompanyCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }
    }
}
