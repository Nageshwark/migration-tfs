﻿using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyValuationResponse
    {
        public ReservationResponse ReserveAccommodationResult { get; set; }
        public Authentication Authentication { get; set; }
    }
}