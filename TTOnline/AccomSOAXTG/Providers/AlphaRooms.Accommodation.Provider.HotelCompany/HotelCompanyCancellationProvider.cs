﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<HotelCompanyCancellationResponse> cancellationAutomator;
        private readonly IAccommodationCancellationParser<HotelCompanyCancellationResponse> cancellationParser;

        public HotelCompanyCancellationProvider(
            IAccommodationCancellationAutomatorAsync<HotelCompanyCancellationResponse> cancellationAutomator,
            IAccommodationCancellationParser<HotelCompanyCancellationResponse> cancellationParser)
        {
            this.cancellationAutomator = cancellationAutomator;
            this.cancellationParser = cancellationParser;
        }

        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await cancellationAutomator.GetCancellationResponseAsync(request);
            return cancellationParser.GetCancellationResults(request, response);
        }
    }
}
