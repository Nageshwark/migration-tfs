﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotelCompany.Factories;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanySearchService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<HotelCompanyAvailabilityResponse>
    {
        private readonly IProviderLoggerService loggerService;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IHotelCompanyAvailabilityRequestFactory availabilityRequestFactory;
        private readonly ILogger logger;

        public HotelCompanyAvailabilityAutomator(IProviderLoggerService loggerService, IProviderOutputLogger outputLogger,
            IHotelCompanyAvailabilityRequestFactory availabilityRequestFactory, ILogger logger)
        {
            this.loggerService = loggerService;
            this.outputLogger = outputLogger;
            this.availabilityRequestFactory = availabilityRequestFactory;
            this.logger = logger;
        }

        public async Task<HotelCompanyAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            var requestfactory = new HotelCompanyAvailabilityRequestFactory();
            var supplierRequest = requestfactory.GetSupplierSearchRequest(request);
            HotelCompanyAvailabilityResponse respone;
            try
            {
                respone = await GetSupplierResponseAsync(request, supplierRequest);
            }
            catch (Exception exception)
            {
                var errorMessage = new StringBuilder();
                errorMessage.AppendFormat(
                    $"HotelCompanyAvailability Error : {exception.GetDetailedMessageWithInnerExceptions()}");
                logger.Error(errorMessage.ToString());
                throw new SupplierApiException(errorMessage.ToString());

            }
            return respone;

        }

        private async Task<HotelCompanyAvailabilityResponse> GetSupplierResponseAsync(
            AccommodationProviderAvailabilityRequest request, Tuple<Login, CheckAvailability> supplierRequest)
        {
            var client = availabilityRequestFactory.GetSearchServiceSoapClient(request);
            var response = await client.SearchAccommodationAsync(supplierRequest.Item1, supplierRequest.Item2);
#pragma warning disable 4014
            Task.Run(() => outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, response));
#pragma warning restore 4014
            return new HotelCompanyAvailabilityResponse
            {
                AvailabilityRequest = request,
                SupplierAuthentication = response.Authentication,
                SupplierResponse = response.ToSearchAccommodationsResult
            };

        }
    }
}