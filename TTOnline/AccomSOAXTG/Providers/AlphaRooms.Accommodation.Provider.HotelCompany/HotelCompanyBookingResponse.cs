﻿using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyBookingResponse
    {
        public BookingResponse BookingResponse { get; set; }
    }
}