﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanySearchService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces
{
    public interface IHotelCompanyAvailabilityRequestFactory
    {
        B2BSearchServiceSoapClient GetSearchServiceSoapClient(AccommodationProviderAvailabilityRequest request);
        /// <summary>
        /// Returns the availability and login objects for search request
        /// </summary>
        ///      <param name="request">AccommodationValuation Request</param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Authentication Object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : ReserveAccommodation Object </item>
        /// </list>
        /// </returns>
        Tuple<Login, CheckAvailability> GetSupplierSearchRequest(AccommodationProviderAvailabilityRequest request);
    }
}