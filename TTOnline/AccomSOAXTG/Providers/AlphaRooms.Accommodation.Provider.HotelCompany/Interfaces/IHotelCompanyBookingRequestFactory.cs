﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces
{
    public interface IHotelCompanyBookingRequestFactory
    {
        /// <summary>
        /// Returns the BookAccommodation and Authentication objects for search request
        /// </summary>
        ///      <param name="request">AccommodationValuation Request</param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Authentication Object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : BookAccommodation Object </item>
        /// </list>
        /// </returns>
        Tuple<Authentication, BookAccommodation> CreateBookingRequest(AccommodationProviderBookingRequest request, ReservationResponse reserveAccommodationResult);
        /// <summary>
        /// Returns the ReserveAccommodation and Authentication objects for search request
        /// </summary>
        ///      <param name="request">AccommodationValuation Request</param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Authentication Object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : ReserveAccommodation Object </item>
        /// </list>
        /// </returns>
        Tuple<Authentication, ReserveAccommodation> GetReserveRequest(AccommodationProviderBookingRequest request);
        B2BBookingServiceSoapClient GetBookingServiceClient(AccommodationProvider provider);

    }
}