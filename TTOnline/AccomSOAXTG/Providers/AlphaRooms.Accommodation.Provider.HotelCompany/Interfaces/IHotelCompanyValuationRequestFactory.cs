using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces
{
    public interface IHotelCompanyValuationRequestFactory
    {
        /// <summary>
        /// Returns the availability and login objects for search request
        /// </summary>
        ///      <param name="request">AccommodationValuation Request</param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Authentication Object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : ReserveAccommodation Object </item>
        /// </list>
        /// </returns>
       Tuple<Authentication, ReserveAccommodation> GetValuationRequest(AccommodationProviderValuationRequest request);

        B2BBookingServiceSoapClient GetBookingServiceClient(AccommodationProvider provider);
    }
}