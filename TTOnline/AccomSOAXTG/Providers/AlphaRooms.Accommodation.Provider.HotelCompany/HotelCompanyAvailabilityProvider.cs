﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<HotelCompanyAvailabilityResponse> accommodationAvailabilityAutomator;
        private readonly IAccommodationAvailabilityParser<HotelCompanyAvailabilityResponse> accommodationAvailabilityParser;

        public HotelCompanyAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<HotelCompanyAvailabilityResponse> accommodationAvailabilityAutomator,
            IAccommodationAvailabilityParser<HotelCompanyAvailabilityResponse> accommodationAvailabilityParser)
        {
            this.accommodationAvailabilityAutomator = accommodationAvailabilityAutomator;
            this.accommodationAvailabilityParser = accommodationAvailabilityParser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var response = await accommodationAvailabilityAutomator.GetAvailabilityResponseAsync(request);
            return accommodationAvailabilityParser.GetAvailabilityResults(request, response);

        }
    }
}
