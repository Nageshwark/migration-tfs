﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<HotelCompanyBookingResponse> bookingAutomator;
        private readonly IAccommodationBookingParser<HotelCompanyBookingResponse> bookingParser;

        public HotelCompanyBookingProvider(
            IAccommodationBookingAutomatorAsync<HotelCompanyBookingResponse> bookingAutomator,
            IAccommodationBookingParser<HotelCompanyBookingResponse> bookingParser)
        {
            this.bookingAutomator = bookingAutomator;
            this.bookingParser = bookingParser;
        }
        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await bookingAutomator.GetBookingResponseAsync(request);
            return bookingParser.GetBookingResults(request, response);
        }
    }
}
