﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyBookingAutomater : IAccommodationBookingAutomatorAsync<HotelCompanyBookingResponse>
    {
        private readonly IProviderLoggerService loggerService;
        private readonly ILogger logger;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IHotelCompanyBookingRequestFactory bookingRequestFactory;

        public HotelCompanyBookingAutomater(IProviderLoggerService loggerService,
            ILogger logger, IProviderOutputLogger outputLogger, IHotelCompanyBookingRequestFactory bookingRequestFactory)
        {
            this.loggerService = loggerService;
            this.logger = logger;
            this.outputLogger = outputLogger;
            this.bookingRequestFactory = bookingRequestFactory;
        }

        public async Task<HotelCompanyBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            try
            {
                //Reserve
                var supplierReserveRequest = bookingRequestFactory.GetReserveRequest(request);
                var reserveResponse = await GetReserveResponse(request, supplierReserveRequest);

				if (string.IsNullOrEmpty(reserveResponse.ReserveAccommodationResult.ErrorMessage))
				{
					// check reservation status, if it is "Reserved" then continue the Booking otherwise skip the Booking
					if (reserveResponse.ReserveAccommodationResult.HotelReservations != null && reserveResponse.ReserveAccommodationResult.HotelReservations.Length > 0 
						&& !string.IsNullOrEmpty(reserveResponse.ReserveAccommodationResult.HotelReservations[0].ResStatus) && reserveResponse.ReserveAccommodationResult.HotelReservations[0].ResStatus.Equals("Reserved"))
					{
						var supplierRequest = bookingRequestFactory.CreateBookingRequest(request, reserveResponse.ReserveAccommodationResult);
						var bookingResponse = await GetBookingAsync(request, supplierRequest);
						return bookingResponse;
					}
				}

                var message = $"TrulyTravel Booking ReserveAccommodation Failed with message: " +
                              $"{reserveResponse.ReserveAccommodationResult.ErrorMessage}";
                logger.Error(message);
                throw new SupplierApiException(message);
            }
            catch (Exception exception)
            {
                var exceptionMessage = exception.GetDetailedMessageWithInnerExceptions();
                logger.Error(exceptionMessage);
                throw new SupplierApiException(exceptionMessage);
            }

        }

        private async Task<HotelCompanyValuationResponse> GetReserveResponse(AccommodationProviderBookingRequest request, Tuple<Authentication, ReserveAccommodation> supplierReserveRequest)
        {
            var bookingClient = bookingRequestFactory.GetBookingServiceClient(request.Provider);
            var response = await bookingClient.ReserveAccommodationAsync(supplierReserveRequest.Item1,
                supplierReserveRequest.Item2);
			loggerService.SetProviderRequest(request, supplierReserveRequest.Item2);
            loggerService.SetProviderResponse(request, response);

#pragma warning disable 4014
            Task.Run(() => outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response));
#pragma warning restore 4014

            return new HotelCompanyValuationResponse()
            {
                Authentication = supplierReserveRequest.Item1,
                ReserveAccommodationResult = response.ToReserveAccommodationResult
            };
        }

        private async Task<HotelCompanyBookingResponse> GetBookingAsync(AccommodationProviderBookingRequest request, Tuple<Authentication, BookAccommodation> supplierRequest)
        {
            var bookingClient = bookingRequestFactory.GetBookingServiceClient(request.Provider);
            var response = await bookingClient.BookAccommodationAsync(supplierRequest.Item1, supplierRequest.Item2);

            loggerService.SetProviderRequest(request, supplierRequest.Item2);
            loggerService.SetProviderResponse(request, response);

#pragma warning disable 4014
            Task.Run(() => outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response));
#pragma warning restore 4014
            return new HotelCompanyBookingResponse()
            {
                BookingResponse = response.ToBookAccommodationResult
            };
        }
    }
}
