﻿using System;
using System.Linq;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Provider.HotelCompany.Helpers;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;


namespace AlphaRooms.Accommodation.Provider.HotelCompany.Factories
{
    public class HotelCompanyValuationRequestFactory : IHotelCompanyValuationRequestFactory
    {
        /// <summary>
        /// Returns the availability and login objects for search request
        /// </summary>
        ///      <param name="request">AccommodationValuation Request</param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Authentication Object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : ReserveAccommodation Object </item>
        /// </list>
        /// </returns>
        public Tuple<Authentication, ReserveAccommodation> GetValuationRequest(AccommodationProviderValuationRequest request)
        {
            var availabilityResult = request.SelectedRooms.FirstOrDefault()?.AvailabilityResult;
            if (availabilityResult == null) return null;

            var reserveAccommodation = new ReserveAccommodation();
            reserveAccommodation.BasicProperty = new BasicPropertyInfo()
            {
                HotelCode = availabilityResult.ProviderEstablishmentCode
            };
            reserveAccommodation.BookingRequestCode =
                availabilityResult.ProviderSpecificData[HotelCompanyHelper.BookingRequestCode];
            reserveAccommodation.SearchRequestCode =
                availabilityResult.ProviderSpecificData[HotelCompanyHelper.SearchRequestCode];
            reserveAccommodation.RoomStays = GetRoomStays(request);
            reserveAccommodation.Customer = GetCustomerDetails(request);

            var authentication = new Authentication
            {
                AuthenticationID = availabilityResult.ProviderSpecificData[HotelCompanyHelper.AuthenticationId],
                SessionID = availabilityResult.ProviderSpecificData[HotelCompanyHelper.SessionId]
            };

            return Tuple.Create(authentication, reserveAccommodation);
        }
        public B2BBookingServiceSoapClient GetBookingServiceClient(AccommodationProvider provider)
        {
            var basicHttpBinding = new BasicHttpBinding
            {
                MaxReceivedMessageSize =
                   long.Parse(provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.MaxReceivedMessageSize)))
            };
            var endPoint = new EndpointAddress(provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyBookingUrl)));
            var client = new B2BBookingServiceSoapClient(basicHttpBinding, endPoint);
            return client;
        }

        private ResCustomerDetails GetCustomerDetails(AccommodationProviderValuationRequest request)
        {
            return new ResCustomerDetails()
            {
                Title = GuestTitle.Mr.ToString(),
                FirstName = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyUsername),
                LastName = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyUsername),
                AddressLine = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity),
                City = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity),
                CountryCode = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCountryCode),
                PostCode = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPostCode)
            };
        }

        private CheckAvailabilityRoomsRoom[] GetRoomStays(AccommodationProviderValuationRequest request)
        {
            var guests = request.SelectedRooms.First().Guests;
            var ages = string.Join(",", guests.Where(i => i.Type == GuestType.Child).Select(i => i.Age.ToString()));

            var room = new[]
            {
                new CheckAvailabilityRoomsRoom()
                {
                    Adults = guests.AdultsCount,
                    Infants = guests.InfantsCount,
                    Children = new CheckAvailabilityRoomsRoomChildren()
                    {
                        Count = guests.ChildrenCount,
                        Ages = ages
                    },
                    RoomLeadPax = new RoomLeadPassenger()
                    {
                         Title = GuestTitle.Mr.ToString(),
                         FirstName = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyUsername),
                         LastName = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyUsername)
                    }

                }
            };
            return room;
        }
    }
}
