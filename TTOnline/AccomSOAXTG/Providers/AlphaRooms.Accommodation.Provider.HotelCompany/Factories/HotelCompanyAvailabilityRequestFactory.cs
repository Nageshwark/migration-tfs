﻿using System;
using System.Linq;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelCompany.Helpers;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanySearchService;


namespace AlphaRooms.Accommodation.Provider.HotelCompany.Factories
{
    public class HotelCompanyAvailabilityRequestFactory : IHotelCompanyAvailabilityRequestFactory
    {
       
        /// <summary>
        /// Returns the availability and login objects for search request
        /// </summary>
        ///      <param name="request"></param>
        /// <returns>
        /// <list type="bullet">
        ///     <item><see cref="Tuple{T1,T2}.Item1" /> : Login object </item> 
        ///     <item><see cref="Tuple{T1,T2}.Item2" /> : Availability object </item>
        /// </list>
        /// </returns>
        public Tuple<Login, CheckAvailability> GetSupplierSearchRequest(AccommodationProviderAvailabilityRequest request)
        {
            var availability = new CheckAvailability
            {
                Pagination = new CheckAvailabilityPagination
                {
                    NumberOfHotels =
                        int.Parse(request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyHotelsNumber))),
                    PageNo = int.Parse(request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyPageNumber)))
                },
                HotelCriteria = GetHotelCriteria(request),
                Stay = new CheckAvailabilityStay
                {
                    CheckinDate = request.CheckInDate.ToString("dd/MM/yyyy"),
                    CheckoutDate = request.CheckOutDate.ToString("dd/MM/yyyy")
                },
                Rooms = GetOccupancy(request)


            };
            var login = new Login
            {
                UserName = request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyUsername)),
                Password = request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyPassword))
            };

            return Tuple.Create(login, availability);
        }

        private CheckAvailabilityRoomsRoom[] GetOccupancy(AccommodationProviderAvailabilityRequest request)
        {
            var guests = request.Rooms.First().Guests;
            var ages = string.Join(",", guests.Where(i => i.Type == GuestType.Child).Select(i => i.Age.ToString()));

            var room = new[]
            {
                new CheckAvailabilityRoomsRoom()
                {
                    Adults = guests.AdultsCount,
                    Infants = guests.InfantsCount,
                    Children = new CheckAvailabilityRoomsRoomChildren()
                    {
                        Count = guests.ChildrenCount,
                        Ages = ages
                    }
                }
            };
            return room;
        }

        private CheckAvailabilityHotelCriteria GetHotelCriteria(AccommodationProviderAvailabilityRequest request)
        {
            //code = PMI;1234
            var destinationCode = request.DestinationCodes.First();
            var hasResort = destinationCode.Contains(";");
            var destination = destinationCode.Split(';')[0];
            var resort = hasResort ? destinationCode.Split(';')[1] : null;
            var availabilityCriteria = new CheckAvailabilityHotelCriteria
            {
                Destination = destination,
                
            };
            if (request.EstablishmentCodes.Any())
            {
                availabilityCriteria.BuildingCode = request.EstablishmentCodes.First();
            }
            if (hasResort)
            {
                availabilityCriteria.ResortCode = resort;
            }
            return availabilityCriteria;
        }

        public B2BSearchServiceSoapClient GetSearchServiceSoapClient(AccommodationProviderAvailabilityRequest request)
        {
            var basicHttpBinding = new BasicHttpBinding
            {
                MaxReceivedMessageSize =
                    long.Parse(request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.MaxReceivedMessageSize)))
            };
            var endPoint = new EndpointAddress(request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyAvailabilityUrl)));
            var client = new B2BSearchServiceSoapClient(basicHttpBinding, endPoint);
            return client;
        }
    }
}
