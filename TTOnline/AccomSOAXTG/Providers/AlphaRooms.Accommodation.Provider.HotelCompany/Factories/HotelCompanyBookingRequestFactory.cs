﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelCompany.Helpers;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;

namespace AlphaRooms.Accommodation.Provider.HotelCompany.Factories
{
    public class HotelCompanyBookingRequestFactory : IHotelCompanyBookingRequestFactory
    {
        public Tuple<Authentication, BookAccommodation> CreateBookingRequest(AccommodationProviderBookingRequest request, ReservationResponse reserveAccommodationResult)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;
            var valuatedRoom = request.ValuatedRooms.First();
            var bookAccommodation = new BookAccommodation
            {
                BookingRequestCode = reserveAccommodationResult.BookingRequestCode,
                Customer = GetCustomerDetails(request),
                ReservationCode = reserveAccommodationResult.HotelReservations.First().ReservationId.ToString(),
                SpecialRequests = GetSpecialRequests(valuatedRoom),
                YourReference = request.ItineraryReference
            };
            var authentication = new Authentication()
            {
                AuthenticationID = valuationResult.ProviderSpecificData[HotelCompanyHelper.AuthenticationId],
                SessionID = valuationResult.ProviderSpecificData[HotelCompanyHelper.SessionId]
            };

            return Tuple.Create(authentication, bookAccommodation);
        }

        private string GetSpecialRequests(AccommodationProviderBookingRequestRoom valuatedRoom)
        {
            var sbSpecialRequest = new StringBuilder();
            var specialRequests = valuatedRoom.SpecialRequests;
            foreach (var property in from property in typeof(AccommodationProviderBookingRequestRoomSpecialRequest)
                                     .GetProperties()
                                     .Where(p => p.PropertyType == typeof(bool))
                                     let propertyValue = (bool)property.GetValue(specialRequests)
                                     where propertyValue == true
                                     select property)
            {
                sbSpecialRequest.Append($"{property.Name},");
            }

            if (!string.IsNullOrEmpty(specialRequests.OtherRequests))
            {
                sbSpecialRequest.Append(specialRequests.OtherRequests);
            }
            return sbSpecialRequest.ToString().TrimEnd(',');
        }

        public Tuple<Authentication, ReserveAccommodation> GetReserveRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.FirstOrDefault()?.ValuationResult;
            if (valuationResult == null) return null;

            var reserveAccommodation = new ReserveAccommodation
            {
                BasicProperty = new BasicPropertyInfo()
                {
                    HotelCode = valuationResult.EstablishmentEdiCode
                },
                BookingRequestCode = valuationResult.ProviderSpecificData[HotelCompanyHelper.BookingRequestCode],
                SearchRequestCode = valuationResult.ProviderSpecificData[HotelCompanyHelper.SearchRequestCode],
                RoomStays = GetRoomStays(request),
                Customer = GetResCustomerDetails(request)
            };


            var authentication = new Authentication
            {
                AuthenticationID = valuationResult.ProviderSpecificData[HotelCompanyHelper.AuthenticationId],
                SessionID = valuationResult.ProviderSpecificData[HotelCompanyHelper.SessionId]
            };

            return Tuple.Create(authentication, reserveAccommodation);
        }
        public B2BBookingServiceSoapClient GetBookingServiceClient(AccommodationProvider provider)
        {
            var basicHttpBinding = new BasicHttpBinding
            {
                MaxReceivedMessageSize =
                   long.Parse(provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.MaxReceivedMessageSize)))
            };
            var endPoint = new EndpointAddress(provider.Parameters.GetParameterValue(nameof(HotelCompanyHelper.HotelCompanyBookingUrl)));
            var client = new B2BBookingServiceSoapClient(basicHttpBinding, endPoint);
            return client;
        }
      
        private ResCustomerDetails GetResCustomerDetails(AccommodationProviderBookingRequest request)
        {
            var leadPassenger = request.Customer;
            string leadGuestFirstName = string.Empty;
            string leadGuestLastName = string.Empty;
            foreach (var roomToBook in request.ValuatedRooms)
            {
                //For single and multipleroom lead guests all the time we are sending the first leadguest firstname and surname as of now.
                //for (byte i = 0; i < (roomToBook.Guests.Count()-1); i++)
                //{
                leadGuestFirstName = roomToBook.Guests[0].FirstName;
                leadGuestLastName = roomToBook.Guests[0].Surname;
                //}
            }
            return new ResCustomerDetails()
            {
                Title = leadPassenger.Title.ToString(),
                FirstName = leadGuestFirstName,
                LastName = leadGuestLastName,
                Email = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyEmail),
                PostCode = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPostCode),
                City = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity),
                MobilePhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone),
                AddressLine = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity) + " " +
                request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPostCode),
                WorkPhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone),
                HomePhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone),
                CountryCode = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCountryCode)
            };
        }
        private CustomerDetails GetCustomerDetails(AccommodationProviderBookingRequest request)
        {
            var leadPassenger = request.Customer;

			string leadGuestFirstName = string.Empty;
            string leadGuestLastName = string.Empty;
            foreach (var roomToBook in request.ValuatedRooms)
            {
                //For single and multipleroom lead guests all the time we are sending the first leadguest firstname and surname as of now.
                //for (byte i = 0; i < (roomToBook.Guests.Count()-1); i++)
                //{
                leadGuestFirstName = roomToBook.Guests[0].FirstName;
                leadGuestLastName = roomToBook.Guests[0].Surname;
                //}
            }
            return new CustomerDetails()
            {
                Title = leadPassenger.Title.ToString(),
                FirstName = leadGuestFirstName,
                LastName = leadGuestLastName,
                Email = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyEmail),
                PostCode = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPostCode),
                City = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity),
                MobilePhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone),
                AddressLineOne = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyCity) + " " +
                request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPostCode),
                WorkPhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone),
                HomePhone = request.Provider.Parameters.GetParameterValue(HotelCompanyHelper.HotelCompanyPhone)
            };
        }

        private CheckAvailabilityRoomsRoom[] GetRoomStays(AccommodationProviderBookingRequest request)
        {
            var guests = request.ValuatedRooms.First().Guests;
            var leadPassenger = request.Customer;
            var ages = string.Join(",", guests.Where(i => i.Type == GuestType.Child).Select(i => i.Age.ToString()));

            var room = new[]
            {
                new CheckAvailabilityRoomsRoom()
                {
                    Adults = guests.AdultsCount,
                    Infants = guests.InfantsCount,
                    Children = new CheckAvailabilityRoomsRoomChildren()
                    {
                        Count = guests.ChildrenCount,
                        Ages = ages
                    },
                    RoomLeadPax = new RoomLeadPassenger()
                    {
                         FirstName = leadPassenger.FirstName,
                         LastName =leadPassenger.Surname,
                         Title = leadPassenger.Title.ToString()
                    }

                }
            };
            return room;
        }

    }
}
