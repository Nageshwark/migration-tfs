﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.TrulyTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.TrulyTravel.TrulyTravelBookingService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.TrulyTravel
{
    public class TrulyTravelBookingAutomator : IAccommodationBookingAutomatorAsync<TrulyTravelBookingResponse>
    {
        private readonly IProviderLoggerService loggerService;
        private readonly ILogger logger;
        private readonly IProviderOutputLogger outputLogger;
        private readonly ITrulyTravelBookingRequestFactory bookingRequestFactory;

        public TrulyTravelBookingAutomator(IProviderLoggerService loggerService,
            ILogger logger, IProviderOutputLogger outputLogger, ITrulyTravelBookingRequestFactory bookingRequestFactory)
        {
            this.loggerService = loggerService;
            this.logger = logger;
            this.outputLogger = outputLogger;
            this.bookingRequestFactory = bookingRequestFactory;
        }

        public async Task<TrulyTravelBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            try
            {
                //Reserve
                var supplierReserveRequest = bookingRequestFactory.GetReserveRequest(request);
                var reserveResponse = await GetReserveResponse(request, supplierReserveRequest);

                if (string.IsNullOrEmpty(reserveResponse.ReserveAccommodationResult.ErrorMessage))
                {
                    var supplierRequest = bookingRequestFactory.CreateBookingRequest(request, reserveResponse.ReserveAccommodationResult);
                    var bookingResponse = await GetBookingAsync(request, supplierRequest);
                    return bookingResponse;
                }

                var message = $"TrulyTravel Booking ReserveAccommodation Failed with message: {reserveResponse.ReserveAccommodationResult.ErrorMessage}";
                logger.Error(message);
                throw new SupplierApiException(message);
            }
            catch (Exception exception)
            {
                var exceptionMessage = exception.GetDetailedMessageWithInnerExceptions();
                logger.Error(exceptionMessage);
                throw new SupplierApiException(exceptionMessage);
            }

        }

        private async Task<TrulyTravelValuationResponse> GetReserveResponse(AccommodationProviderBookingRequest request, Tuple<Authentication, ReserveAccommodation> supplierReserveRequest)
        {
            var bookingClient = bookingRequestFactory.GetBookingServiceClient(request.Provider);
            var response = await bookingClient.ReserveAccommodationAsync(supplierReserveRequest.Item1,
                supplierReserveRequest.Item2);

            return new TrulyTravelValuationResponse()
            {
                Authentication = supplierReserveRequest.Item1,
                ReserveAccommodationResult = response.ToReserveAccommodationResult
            };
        }

        private async Task<TrulyTravelBookingResponse> GetBookingAsync(AccommodationProviderBookingRequest request, Tuple<Authentication, BookAccommodation> supplierRequest)
        {
            var bookingClient = bookingRequestFactory.GetBookingServiceClient(request.Provider);
            var response = await bookingClient.BookAccommodationAsync(supplierRequest.Item1, supplierRequest.Item2);

            loggerService.SetProviderRequest(request, supplierRequest.Item2);
            loggerService.SetProviderResponse(request, response);

#pragma warning disable 4014
            Task.Run(() => outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response));
#pragma warning restore 4014
            return new TrulyTravelBookingResponse()
            {
                BookingResponse = response.ToBookAccommodationResult
            };
        }
    }
}
