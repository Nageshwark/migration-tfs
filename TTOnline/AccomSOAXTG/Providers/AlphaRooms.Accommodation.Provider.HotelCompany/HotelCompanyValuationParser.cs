﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Helpers;
using AlphaRooms.Accommodation.Provider.HotelCompany.Service_References.HotelCompanyBookingService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyValuationParser : IAccommodationValuationParser<HotelCompanyValuationResponse>
    {
        private readonly ILogger logger;
        public HotelCompanyValuationParser(ILogger logger)
        {
            this.logger = logger;
        }
        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, HotelCompanyValuationResponse response)
        {
            var valuationResults = new List<AccommodationProviderValuationResult>();
            try
            {
                var accommodationResponse = response.ReserveAccommodationResult;
                var availabilityResult = request.SelectedRooms.FirstOrDefault()?.AvailabilityResult;
                if (availabilityResult == null) return null;

                foreach (var reservation in accommodationResponse.HotelReservations)
                {
					// check reservation status, if it is "Reserved" then continue otherwise skip to add Valuation Result
					if (!string.IsNullOrEmpty(reservation.ResStatus) && reservation.ResStatus.Equals("Reserved"))
					{
						foreach (var roomstay in reservation.RoomStays)
						{
							var valuationResult = new AccommodationProviderValuationResult();
							valuationResult.Adults = (byte)roomstay.Adults;
							valuationResult.Children = (byte)(roomstay.Children?.Count).GetValueOrDefault();
							valuationResult.Infants = (byte)roomstay.Infants;
							valuationResult.RoomCode = roomstay.RoomType.RoomTypeCode;
							valuationResult.BoardCode = roomstay.RoomType.MealPlan.Code;
							valuationResult.BoardDescription = HotelCompanyHelper.GetBoardType(valuationResult.BoardCode);

							valuationResult.CheckInDate = DateTime.ParseExact(reservation.Stay.CheckinDate, "dd/MM/yyyy",
								CultureInfo.InvariantCulture);
							valuationResult.CheckOutDate = DateTime.ParseExact(reservation.Stay.CheckoutDate, "dd/MM/yyyy",
								CultureInfo.InvariantCulture);
							valuationResult.CostPrice = new Money()
							{
								Amount = roomstay.RoomType.AmountAfterTax,
								CurrencyCode = roomstay.RoomType.CurrencyCode
							};
							valuationResult.EstablishmentEdiCode = reservation.BasicPropertyInfo.HotelCode;
							valuationResult.RoomDescription = roomstay.RoomType.RoomDescription.Value;
							valuationResult.EstablishmentName = reservation.BasicPropertyInfo.HotelName;
							valuationResult.IsNonRefundable = availabilityResult.IsNonRefundable;
							valuationResult.PaymentModel = PaymentModel.PostPayment;
							valuationResult.ProviderEdiCode = availabilityResult.ProviderEdiCode;
							valuationResult.DestinationEdiCode = availabilityResult.DestinationEdiCode;
							valuationResult.CancellationPolicy =
						  valuationResult.IsNonRefundable ? null : GetCancellationPolicy(reservation.BasicPropertyInfo.CancelPolicy);
							valuationResult.ProviderSpecificData = new Dictionary<string, string>()
						{
							{HotelCompanyHelper.ReservationId, reservation.ReservationId.ToString()},
							{HotelCompanyHelper.BookingRequestCode, availabilityResult.ProviderSpecificData[HotelCompanyHelper.BookingRequestCode] },
							{HotelCompanyHelper.SearchRequestCode,availabilityResult.ProviderSpecificData[HotelCompanyHelper.SearchRequestCode] },
							{HotelCompanyHelper.SessionId, availabilityResult.ProviderSpecificData[HotelCompanyHelper.SessionId] },
							{HotelCompanyHelper.AuthenticationId,availabilityResult.ProviderSpecificData[HotelCompanyHelper.AuthenticationId] }

						};

							valuationResults.Add(valuationResult);

						}
					}
					else
						logger.Error("HotelCompany Room Valluation Failed");

				}

                return valuationResults;
            }
            catch (Exception ex)
            {
                var errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                logger.Error(errorMessage);
                throw new SupplierApiException($"HotelCompany Valluation Error : {errorMessage}");
            }

        }

        private string[] GetCancellationPolicy(CancelPolicy cancelPolicy)
        {
            return !string.IsNullOrEmpty(cancelPolicy?.DateTo) ? new[] { $"Cancellation after {DateTime.ParseExact(cancelPolicy.DateFrom, "yyyyMMdd", CultureInfo.InvariantCulture).Date} but before {DateTime.ParseExact(cancelPolicy.DateTo, "yyyyMMdd", CultureInfo.InvariantCulture).Date} penality {cancelPolicy.Price}" }
            : new[] { cancelPolicy?.Description ?? string.Empty };
        }
    }
}
