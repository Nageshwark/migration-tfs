﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany.Helpers;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelCompany
{
    public class HotelCompanyAvailabilityParser : IAccommodationAvailabilityParser<HotelCompanyAvailabilityResponse>
    {
        private readonly ILogger logger;
        private readonly string HotelCompanyCurrency;
        private readonly IProviderNonRefundableService nonRefundableService;
        private readonly string NoAvailabilityMessage = "No Results Found";

        public HotelCompanyAvailabilityParser(ILogger logger, IProviderNonRefundableService nonRefundableService)
        {
            this.logger = logger;
            this.nonRefundableService = nonRefundableService;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            HotelCompanyAvailabilityResponse response)
        {
            if (string.IsNullOrEmpty(response.SupplierResponse?.ErrorMessage))
            {
                var availabilityResults = new ConcurrentBag<AccommodationProviderAvailabilityResult>();
                try
                {
                    var requestedRoom = request.Rooms.First();
                    var currencyCode = request.Provider.Parameters.GetParameterValue(nameof(HotelCompanyCurrency));
                    Parallel.For((long)0, response.SupplierResponse?.Accommodations.Length ?? 0, (hotelIndex) =>
                    {
                        var accommodation = response.SupplierResponse.Accommodations[hotelIndex];
                        foreach (var room in accommodation.Rooms)
                        {
                            foreach (var roomPrice in room.RoomPrice)
                            {
                                var availabityResult = new AccommodationProviderAvailabilityResult();
                                availabityResult.PaymentModel = PaymentModel.PostPayment;
                                availabityResult.EstablishmentName = accommodation.Building;
                                availabityResult.ProviderEdiCode = request.Provider.EdiCode;
                                availabityResult.SupplierEdiCode = room.BookingRequestCode.SupplierChainCode == "0"
                                    ? availabityResult.ProviderEdiCode
                                    : room.BookingRequestCode.SupplierChainCode;

                                availabityResult.Adults = requestedRoom.Guests.AdultsCount;
                                availabityResult.Children = requestedRoom.Guests.ChildrenCount;
                                availabityResult.Infants = requestedRoom.Guests.InfantsCount;

                                availabityResult.BoardCode = room.MealPlan;
                                availabityResult.BoardDescription = HotelCompanyHelper.GetBoardType(room.MealPlan);
                                availabityResult.CheckInDate = request.CheckInDate;
                                availabityResult.CheckOutDate = request.CheckOutDate;
                                availabityResult.EstablishmentEdiCode = accommodation.BuildingCode.Value;
                                availabityResult.DestinationEdiCode =
                                    string.Concat(request.DestinationCodes.First().Take(3));
                                availabityResult.CostPrice = new Money
                                {
                                    CurrencyCode = currencyCode,
                                    Amount = decimal.Parse(roomPrice.Value)
                                };
                                availabityResult.SalePrice = availabityResult.CostPrice;
                                availabityResult.RoomNumber = byte.Parse(roomPrice.RoomNo);
                                availabityResult.RoomCode = room.BookingRequestCode.Value;
                                availabityResult.RoomDescription = roomPrice.Description;
                                availabityResult.IsNonRefundable = room.BookingRequestCode.IsNonRefundable != "0";
                                if (!availabityResult.IsNonRefundable)
                                {
                                    availabityResult.IsNonRefundable =
                                        nonRefundableService.IsDescriptionNonRefundable(availabityResult.RoomDescription, false);
                                }

                                availabityResult.ProviderSpecificData = new Dictionary<string, string>()
                               {
                                   {
                                      HotelCompanyHelper.AuthenticationId,
                                       response.SupplierAuthentication.AuthenticationID
                                   },
                                   {
                                       HotelCompanyHelper.SessionId,
                                       response.SupplierAuthentication.SessionID
                                   },
                                   {
                                        HotelCompanyHelper.BookingRequestCode,
                                        room.BookingRequestCode.Value},
                                   {
                                       HotelCompanyHelper.SearchRequestCode,
                                       response.SupplierResponse.SearchRequestCode
                                   }

                               };

                                availabilityResults.Add(availabityResult);
                            }
                        }
                    });

                    return availabilityResults.ToArray();
                }
                catch (Exception exception)
                {
                    logger.Error($"HotelCompany Availability : Error in  Response  {exception.GetDetailedMessageWithInnerExceptions()} ");
                    throw new SupplierApiException($"HotelCompany Availability : Error in  Response {exception.GetDetailedMessageWithInnerExceptions()}");
                }

            }

            logger.Error($"HotelCompany Availability : Error in  Response  {response.SupplierResponse?.ErrorMessage} ");
            if (response.SupplierResponse?.ErrorMessage.Contains(NoAvailabilityMessage,
                    StringComparison.InvariantCultureIgnoreCase) ?? false)
            {
                return Enumerable.Empty<AccommodationProviderAvailabilityResult>();
            }

            throw new SupplierApiException($"HotelCompany Availability : Error in  Response {response.SupplierResponse?.ErrorMessage}");

        }
    }
}

