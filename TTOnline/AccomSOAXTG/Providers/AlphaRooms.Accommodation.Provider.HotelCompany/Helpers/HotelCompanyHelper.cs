﻿using System.Collections.Generic;

namespace AlphaRooms.Accommodation.Provider.HotelCompany.Helpers
{
    internal class HotelCompanyHelper
    {
        //Parameters
        internal const string HotelCompanyUsername = "HotelCompanyUsername";
        internal const string HotelCompanyPassword = "HotelCompanyPassword";
        internal const string HotelCompanyHotelsNumber = "HotelCompanyHotelsNumber";
        internal const string HotelCompanyPageNumber = "HotelCompanyPageNumber";
        internal const string HotelCompanyAvailabilityUrl = "HotelCompanyAvailabilityUrl";
        internal const string HotelCompanyValuationUrl = "HotelCompanyValuationUrl";
        internal const string HotelCompanyBookingUrl = "HotelCompanyBookingUrl";
        internal const string HotelCompanyCancellationUrl = "HotelCompanyCancellationUrl";
        internal const string HotelCompanyCurrency = "HotelCompanyCurrency";
        internal const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        internal const string HotelCompanyPostCode = "HotelCompanyPostCode";
        internal const string HotelCompanyCountryCode = "HotelCompanyCountryCode";
        internal const string HotelCompanyCity = "HotelCompanyBookingCity";
        internal const string HotelCompanyPhone = "HotelCompanyBookingPhone";
        internal const string HotelCompanyEmail = "HotelCompanyBookingEmail";

        //ProviderSpecific data
        internal const string AuthenticationId = "AuID";
        internal const string SessionId = "SID";
        internal const string BookingRequestCode = "BRC";
        internal const string SearchRequestCode = "SRC";
        internal const string ReservationId = "ReID";
        private static Dictionary<string, string> BoarDictionary { get; }

        public static string GetBoardType(string boardName)
        {
            var defaultBoardType = "Room Only";
            BoarDictionary.TryGetValue(boardName, out defaultBoardType);
            return defaultBoardType;
        }

        static HotelCompanyHelper()
        {
            BoarDictionary = new Dictionary<string, string>()
            {
                {"BB","Bed & Breakfast" },
                {"BBU","Bed & Breakfast" },
                {"HB","Half Board" },
                {"RO","Room Only" },
                {"SC","Self Catering" },
                {"FB", "Full Board" },
                {"AI", "All Inclusive" },
                {"CB", "Breakfast" },
                {"AB","Breakfast" },
                {"AIB","All Inclusive" },
                {"AIL","All Inclusive" },
                {"AIU","All Inclusive" }
            };
        }

    }
}
