﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;


namespace AlphaRooms.Accommodation.Provider.Hotusa.Factories
{
    public class HotusaCancellationRequestFactory : IHotusaProviderCancellationRequestFactory
    {
        public peticion CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            peticion supplierRequest = new peticion();
            supplierRequest.tipo = 401;
            supplierRequest.parametros = new parametros();

            supplierRequest.parametros.localizador_largo = request.ProviderBookingReference.Split(';')[0] ;
            supplierRequest.parametros.localizador_corto = request.ProviderBookingReference.Split(';')[1];

            return supplierRequest;
        }
    }
}
