﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;


namespace AlphaRooms.Accommodation.Provider.Hotusa.Factories
{
    public class HotusaValuationRequestFactory : IHotusaProviderValuationRequestFactory
    {
        private const string afiliacio = "afiliacio";
        private const string Language = "Language";
        private const string Duplicate = "Duplicate";
        private const string Compressed = "Compressed";
        private const string Restrictions = "Restrictions";
        private const string usuario = "usuario";

        public peticion CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var availabilityRequet = request.SelectedRooms.First().AvailabilityRequest;

            var supplierRequest = new peticion();

            supplierRequest.tipo = 110;
            supplierRequest.parametros = new parametros();

            supplierRequest.parametros.HotelIds = request.SelectedRooms.FirstOrDefault().AvailabilityResult.EstablishmentEdiCode + '#';
            supplierRequest.parametros.radio = "9";
            supplierRequest.parametros.regimen = request.SelectedRooms.FirstOrDefault().AvailabilityResult.BoardCode;

            supplierRequest.parametros.numhab1 = "1";
            supplierRequest.parametros.paxes1 = request.SelectedRooms[0].Guests.AdultsCount.ToString() + '-' + request.SelectedRooms[0].Guests.ChildrenCount.ToString();

            if (request.SelectedRooms.Count() > 1)
            {
                supplierRequest.parametros.numhab2 = "1";
                supplierRequest.parametros.paxes2 = request.SelectedRooms[1].Guests.AdultsCount.ToString() + '-' +
                                                     request.SelectedRooms[1].Guests.ChildrenCount.ToString();
            }

            if (request.SelectedRooms.Count() > 2)
            {
                supplierRequest.parametros.numhab3 = "1";
                supplierRequest.parametros.paxes3 = request.SelectedRooms[2].Guests.AdultsCount.ToString() + '-' +
                                                     request.SelectedRooms[2].Guests.ChildrenCount.ToString();
            }

            supplierRequest.parametros.usuario = request.Provider.Parameters.GetParameterValue(usuario);
            supplierRequest.parametros.afiliacion = request.Provider.Parameters.GetParameterValue(afiliacio);

            supplierRequest.parametros.idioma = request.Provider.Parameters.GetParameterValue(Language); //Language as English
            supplierRequest.parametros.duplicidad = request.Provider.Parameters.GetParameterValue(Duplicate); //No duplicate
            supplierRequest.parametros.restricciones = request.Provider.Parameters.GetParameterValue(Restrictions);
            supplierRequest.parametros.comprimido = request.Provider.Parameters.GetParameterValue(Compressed);
            supplierRequest.parametros.fechaentrada = availabilityRequet.CheckInDate.ToString("MM/dd/yyyy"); //"11/13/2011";
            supplierRequest.parametros.fechasalida = availabilityRequet.CheckOutDate.ToString("MM/dd/yyyy");  //"11/17/2011";

            return supplierRequest;
        }

        private string getPaxes(AccommodationProviderValuationRequest request)
        {
            string paxes = "";
            int adults = request.SelectedRooms[0].Guests.AdultsCount;
            int child = request.SelectedRooms[0].Guests.ChildrenCount;

            paxes = adults + "-" + child;
            return paxes;
        }        
        
    }
}
