﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using Ninject.Infrastructure.Language;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaCancellationlationParser : IAccommodationCancellationParserAsync<HotusaCancellationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotusaCancellationlationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }


        public async System.Threading.Tasks.Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, HotusaCancellationResponse response)
        {
            var cancellationResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();

            foreach (object item in response.SupplierResponse.parametros.Items)
            {
                if (item is estado)
                {
                    if (((estado)item).Text != "00")
                    {
                        cancellationResult.CancellationStatus = CancellationStatus.Failed;
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("There was an unknown problem with the cancellation response from Hotusa.");
                        sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                        sb.AppendLine("Cancellation Request:");
                        sb.AppendLine(request.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Cancellation Response:");
                        sb.AppendLine(response.XmlSerialize());

                        cancellationResult.Message = sb.ToString();

                        logger.Error(sb.ToString());
                    }
                }
            }

            cancellationResult.CancellationStatus = CancellationStatus.Succeeded;
            cancellationResult.ProviderCancellationReference = response.SupplierResponse.parametros.localizador;
            cancellationResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled."
                                         + response.SupplierResponse.parametros.localizador_baja;

            cancellationResults.Add(cancellationResult);

            return cancellationResults;
        }
      
    }
}
