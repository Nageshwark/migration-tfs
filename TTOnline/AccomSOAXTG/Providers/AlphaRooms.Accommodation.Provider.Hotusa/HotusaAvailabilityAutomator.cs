﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System.Web;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<HotusaAvailabilityResponse>
    {
        private const string HotusaAvailabilityUrl = "HotusaAvailabilityUrl";
        private const string secacc = "secacc";
        private const string codigousu = "codigousu";
        private const string clausu = "clausu";
        private const string afiliacio = "afiliacio";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IHotusaAvailabilityRequestFactory _hotusaSupplierAvailabilityRequestFactory;


        public HotusaAvailabilityAutomator(IHotusaAvailabilityRequestFactory hotusaAvailabilityRequestFactory)
        {
            _hotusaSupplierAvailabilityRequestFactory = hotusaAvailabilityRequestFactory;
        }

        public async Task<HotusaAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            HotusaAvailabilityResponse response = null;

            try
            {
                //ValidateRequest(request);

                peticion supplierRequest = this._hotusaSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("HotusaAvailabilityAutomator.GetAvailabilityResponseAsync: Hotusa supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<HotusaAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, peticion supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };
            StringBuilder url = new StringBuilder();

            url.AppendFormat(request.Provider.Parameters.GetParameterValue(HotusaAvailabilityUrl));
            url.AppendFormat("?secacc={0}", request.Provider.Parameters.GetParameterValue(secacc));
            url.AppendFormat("&codigousu={0}", request.Provider.Parameters.GetParameterValue(codigousu));
            url.AppendFormat("&clausu={0}", request.Provider.Parameters.GetParameterValue(clausu));
            url.AppendFormat("&afiliacio={0}", request.Provider.Parameters.GetParameterValue(afiliacio));
            url.AppendFormat("&xml={0}", HttpUtility.UrlEncode(serialisedRequest));

            WebScrapeResponse responseXML = await webScrapeClient.PostAsync(url.ToString(),"");
            var response = responseXML.Value.XmlDeSerialize<respuesta>();
            var supplierResponse = new HotusaAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };
            return supplierResponse;
        }

    }
}
