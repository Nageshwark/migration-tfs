﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaBookingAutomator : IAccommodationBookingAutomatorAsync<HotusaBookingResponse>
    {
        private const string HotusaAvailabilityUrl = "HotusaAvailabilityUrl";
        private const string secacc = "secacc";
        private const string codigousu = "codigousu";
        private const string clausu = "clausu";
        private const string afiliacio = "afiliacio";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotusaBookingRequestFactory _hotusaSupplierBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotusaBookingAutomator(ILogger logger,
            // IAccommodationConfigurationManager configurationManager,
                                            IHotusaBookingRequestFactory hotusaSupplierBookingRequestFactory
            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._hotusaSupplierBookingRequestFactory = hotusaSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }


        public async Task<HotusaBookingResponse> GetBookingResponseAsync(
            AccommodationProviderBookingRequest request)
        {

            //Step 1: Booking Initiate
            var supplierInitiateRequest =
                this._hotusaSupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
            var supplierInitiateResponse =
                await GetSupplierBookingResponseAsync(request, supplierInitiateRequest, false);

            //Step 2: Booking Commit
            string reserveId = "";
            foreach (object item in supplierInitiateResponse.SupplierResponse.parametros.Items)
            {
                if (item is error)
                {

                }
                else if (item is n_localizador)
                {
                    reserveId = ((n_localizador)item).Text;
                }
            }

            peticion supplierCommitRequest =
                this._hotusaSupplierBookingRequestFactory.CreateSupplierBookingCommitRequest(request, reserveId);

            HotusaBookingResponse supplierCommitResponse =
                await GetSupplierBookingResponseAsync(request, supplierCommitRequest, true);

            return supplierCommitResponse;
        }

        private async Task<HotusaBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, peticion supplierRequest, bool isCommit)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };
            StringBuilder url = new StringBuilder();

            url.AppendFormat(request.Provider.Parameters.GetParameterValue(HotusaAvailabilityUrl));
            url.AppendFormat("?secacc={0}", request.Provider.Parameters.GetParameterValue(secacc));
            url.AppendFormat("&codigousu={0}", request.Provider.Parameters.GetParameterValue(codigousu));
            url.AppendFormat("&clausu={0}", request.Provider.Parameters.GetParameterValue(clausu));
            url.AppendFormat("&afiliacio={0}", request.Provider.Parameters.GetParameterValue(afiliacio));
            url.AppendFormat("&xml={0}", HttpUtility.UrlEncode(serialisedRequest));

            WebScrapeResponse responseXML = await webScrapeClient.PostAsync(url.ToString(), "");
            var response = responseXML.Value.XmlDeSerialize<respuesta>();
            var supplierResponse = new HotusaBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };
            return supplierResponse;
        }
    }
}
