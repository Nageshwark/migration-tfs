﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<HotusaCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<HotusaCancellationResponse> parser;

        public HotusaCancellationProvider(IAccommodationCancellationAutomatorAsync<HotusaCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<HotusaCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
}
