﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.Hotusa.Service.Request
{
    public class peticion
    {
        /// <remarks />
        public int tipo { get; set; }

        /// <remarks />
        public string nombre { get; set; }

        /// <remarks />
        public string agencia { get; set; }

        /// <remarks />
        public parametros parametros { get; set; }
    }

    public class parametros
    {
        /// <remarks />
        public string pais { get; set; }

        /// <remarks />
        public string radio { get; set; }

        /// <remarks />
        [XmlElement("hotel")]
        public string HotelIds { get; set; }

        /// <remarks />
        public string codigo { get; set; }

        /// <remarks />
        public string idioma { get; set; }

        /// <remarks />
        public string provincia { get; set; }

        /// <remarks />
        public string marca { get; set; }

        /// <remarks />
        public string afiliacion { get; set; }

        /// <remarks />
        public string ultact { get; set; }

        /// <remarks />
        public string fechaalta { get; set; }

        /// <remarks />
        public string baja { get; set; }

        /// <remarks />
        public string codhot { get; set; }

        /*Availability Request Parameters */

        /// <remarks />
        public string codishotel { get; set; }

        /// <remarks />
        public string regimen { get; set; }

        /// <remarks />
        public string numhab1 { get; set; }

        /// <remarks />
        public string numhab2 { get; set; }

        /// <remarks />
        public string numhab3 { get; set; }

        /// <remarks />
        public string paxes1 { get; set; }

        /// <remarks />
        public string paxes2 { get; set; }

        /// <remarks />
        public string paxes3 { get; set; }

        /// <remarks />
        public string fechaentrada { get; set; }

        /// <remarks />
        public string fechasalida { get; set; }

        /// <remarks />
        public string usuario { get; set; }

        /// <remarks />
        public string duplicidad { get; set; }

        /// <remarks />
        public string restricciones { get; set; }

        /// <remarks />
        public string comprimido { get; set; }

        /* Till Here */

        /* Reserve Request Params */

        /// <remarks />
        public string codigo_hotel { get; set; }

        /// <remarks />
        public string nombre_cliente { get; set; }

        /// <remarks />
        public string observaciones { get; set; }

        /// <remarks />
        public string num_mensaje { get; set; }

        /// <remarks />
        public string num_expediente { get; set; }

        /// <remarks />
        public string forma_pago { get; set; }

        /// <remarks />
        public string tipo_targeta { get; set; }

        /// <remarks />
        public string num_targeta { get; set; }

        /// <remarks />
        public string mes_expiracion_targeta { get; set; }

        /// <remarks />
        public string ano_expiracion_targeta { get; set; }

        /// <remarks />
        public string titular_targeta { get; set; }

        /// <remarks />
        public string email { get; set; }

        /// <remarks />
        public string telefono { get; set; }

        /// <remarks />
        public res res { get; set; }

        /// <remarks />
        public codigo_cliente codigo_cliente { get; set; }

        /// <remarks />
        public activar_prefs activar_prefs { get; set; }

        /// <remarks />
        public string localizador { get; set; }

        /// <remarks />
        public string accion { get; set; }

        //Cancellation Properties

        /// <remarks />
        public string localizador_largo { get; set; }

        /// <remarks />
        public string localizador_corto { get; set; }
    }

    public class res
    {
        /// <remarks />
        [XmlElement("lin")]
        public List<lin> lin { get; set; }
    }

    public class lin
    {
        /// <remarks />
        [XmlText]
        public string Text { get; set; }
    }

    public class codigo_cliente
    {
        /// <remarks />
        [XmlText]
        public string[] Text { get; set; }
    }

    public class activar_prefs
    {
        /// <remarks />
        [XmlText]
        public string[] Text { get; set; }
    }
   
}