﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaBookingParser : IAccommodationBookingParser<HotusaBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public HotusaBookingParser(ILogger logger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, HotusaBookingResponse response)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();
            AccommodationProviderBookingResult bookingResult;
            foreach (object item in response.SupplierResponse.parametros.Items)
            {
                if (item is error)
                {
                    bookingResult = new AccommodationProviderBookingResult();
                    bookingResult.BookingStatus =  BookingStatus.NotConfirmed;
                    bookingResult.Message = ((error) item).descripcion;
                    bookingResults.Enqueue(bookingResult);
                    return bookingResults;
                }
            }

            bookingResult = new AccommodationProviderBookingResult();
            bookingResult.BookingStatus  = BookingStatus.Confirmed;
            bookingResult.ProviderBookingReference = response.SupplierResponse.parametros.localizador + ";" + response.SupplierResponse.parametros.localizador_corto;
            bookingResults.Enqueue(bookingResult);
            return bookingResults;
        }
    }
}
