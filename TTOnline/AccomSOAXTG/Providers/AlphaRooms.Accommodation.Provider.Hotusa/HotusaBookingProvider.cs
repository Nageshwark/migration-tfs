﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<HotusaBookingResponse> automator;
        private readonly IAccommodationBookingParser<HotusaBookingResponse> parser;

        public HotusaBookingProvider(IAccommodationBookingAutomatorAsync<HotusaBookingResponse> automator,
                                        IAccommodationBookingParser<HotusaBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
