﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaValuationAutomator : IAccommodationValuationAutomatorAsync<HotusaValuationResponse>
    {
        private const string HotusaAvailabilityUrl = "HotusaAvailabilityUrl";
        private const string secacc = "secacc";
        private const string codigousu = "codigousu";
        private const string clausu = "clausu";
        private const string afiliacio = "afiliacio";
        


        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotusaProviderValuationRequestFactory HotusaProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotusaValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotusaProviderValuationRequestFactory HotusaSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.HotusaProviderValuationRequestFactory = HotusaSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<HotusaValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            HotusaValuationResponse response = null;
            try
            {
                peticion supplierRequest = this.HotusaProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception)
            {
                
                throw;
            }


            return response;
        }

        private async Task<HotusaValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, peticion supplierRequest)
        {
            string serialisedRequest = supplierRequest.XmlSerialize();

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };
            StringBuilder url = new StringBuilder();

            url.AppendFormat(request.Provider.Parameters.GetParameterValue(HotusaAvailabilityUrl));
            url.AppendFormat("?secacc={0}", request.Provider.Parameters.GetParameterValue(secacc));
            url.AppendFormat("&codigousu={0}", request.Provider.Parameters.GetParameterValue(codigousu));
            url.AppendFormat("&clausu={0}", request.Provider.Parameters.GetParameterValue(clausu));
            url.AppendFormat("&afiliacio={0}", request.Provider.Parameters.GetParameterValue(afiliacio));
            url.AppendFormat("&xml={0}", HttpUtility.UrlEncode(serialisedRequest));

            WebScrapeResponse responseXML = await webScrapeClient.PostAsync(url.ToString(), "");
            var response = responseXML.Value.XmlDeSerialize<respuesta>();
            var supplierResponse = new HotusaValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };
            return supplierResponse;
        }
    }
}
