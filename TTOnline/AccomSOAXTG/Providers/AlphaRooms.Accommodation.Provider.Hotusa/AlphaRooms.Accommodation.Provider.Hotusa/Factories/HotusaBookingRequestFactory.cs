﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;


namespace AlphaRooms.Accommodation.Provider.Hotusa.Factories
{
    public class HotusaBookingRequestFactory : IHotusaBookingRequestFactory
    {
        
        public peticion CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new peticion();

            supplierRequest.tipo = 202;
            supplierRequest.parametros = new parametros();

            supplierRequest.parametros.codigo_hotel = request.ValuatedRooms[0].ValuationResult.EstablishmentEdiCode;

            supplierRequest.parametros.nombre_cliente = request.Customer.Title + " " +
                                                    request.Customer.FirstName + " "
                                                    + request.Customer.Surname;

            if (request.ValuatedRooms[0].SpecialRequests != null)
            {
                supplierRequest.parametros.observaciones = request.ValuatedRooms[0].SpecialRequests.ToString();
            }
            
            supplierRequest.parametros.forma_pago = "25";
            
            supplierRequest.parametros.res = new res();
            supplierRequest.parametros.res.lin = new System.Collections.Generic.List<lin>();
            foreach (string _lin in request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["SellingLines"].Split(';'))
            {
                lin newline = new lin();
                newline.Text = _lin;
                supplierRequest.parametros.res.lin.Add(newline);
            }

            return supplierRequest;
        }

        public peticion CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string resId)
        {
            var supplierRequest = new peticion();

            supplierRequest.tipo = 3;
            supplierRequest.parametros = new parametros();

            supplierRequest.parametros.localizador = resId;
            supplierRequest.parametros.accion = "AE";

            return supplierRequest;

        }
    }
}
