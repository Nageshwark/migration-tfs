﻿using System;
using System.Linq;

using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using Ninject.Infrastructure.Language;


namespace AlphaRooms.Accommodation.Provider.Hotusa.Factories
{
    public class HotusaAvailabilityRequestFactory : IHotusaAvailabilityRequestFactory
    {
        private const string afiliacio = "afiliacio";
        private const string Language = "Language";
        private const string Duplicate = "Duplicate";
        private const string Compressed = "Compressed";
        private const string Restrictions = "Restrictions";
        private const string usuario = "usuario";

        public peticion CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var availRequest = new peticion();

            availRequest.tipo = 110;
            availRequest.parametros = new parametros();

            if (request.DestinationCodes.Any())
            {
                availRequest.parametros.pais = request.DestinationCodes[0].Split(';')[0];
                availRequest.parametros.provincia = request.DestinationCodes[0].Split(';')[1];
            }
            else
            {
                availRequest.parametros.HotelIds = String.Join("#", request.EstablishmentCodes.AsEnumerable().Select(h => h));
                availRequest.parametros.HotelIds = availRequest.parametros.HotelIds + '#';
            }
            availRequest.parametros.radio = "9";
            
            availRequest.parametros.numhab1 = "1";
            availRequest.parametros.paxes1 = (request.Rooms[0].Guests.AdultsCount.ToString() + '-' + request.Rooms[0].Guests.ChildrenCount.ToString());

            if (request.Rooms.Count() > 1)
            {
                availRequest.parametros.numhab2 = "1";
                availRequest.parametros.paxes2 = (request.Rooms[1].Guests.AdultsCount.ToString() + '-' + request.Rooms[1].Guests.ChildrenCount.ToString());         
            }

            if (request.Rooms.Count() > 2)
            {
                availRequest.parametros.numhab3 = "1";
                availRequest.parametros.paxes3 = (request.Rooms[2].Guests.AdultsCount.ToString() + '-' +
                                                  request.Rooms[2].Guests.ChildrenCount.ToString());
            }

            availRequest.parametros.usuario = request.Provider.Parameters.GetParameterValue(usuario);
            availRequest.parametros.afiliacion = request.Provider.Parameters.GetParameterValue(afiliacio);
            availRequest.parametros.idioma = request.Provider.Parameters.GetParameterValue(Language); //Language as English
            availRequest.parametros.duplicidad = request.Provider.Parameters.GetParameterValue(Duplicate); //No duplicate
            availRequest.parametros.restricciones = request.Provider.Parameters.GetParameterValue(Restrictions);
            availRequest.parametros.comprimido = request.Provider.Parameters.GetParameterValue(Compressed);  
            availRequest.parametros.fechaentrada = request.CheckInDate.ToString("MM/dd/yyyy"); //"11/13/2011";
            availRequest.parametros.fechasalida = request.CheckOutDate.ToString("MM/dd/yyyy");  //"11/17/2011";
                
            
            return availRequest;
        }

        private string getPaxes(AccommodationProviderAvailabilityRequest request)
        {
            string paxes = "";
            int adults = request.Rooms[0].Guests.AdultsCount;
            int child = request.Rooms[0].Guests.ChildrenCount;


            paxes = adults + "-" + child;
            return paxes;
        }
    }

}

