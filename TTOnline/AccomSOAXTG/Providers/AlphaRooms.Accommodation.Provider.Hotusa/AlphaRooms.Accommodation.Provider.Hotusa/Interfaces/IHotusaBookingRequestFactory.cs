﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Hotusa.Interfaces
{
    public interface IHotusaBookingRequestFactory
    {
        peticion CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
        peticion CreateSupplierBookingCommitRequest(AccommodationProviderBookingRequest request, string resId);
    }
}
