﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<HotusaAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<HotusaAvailabilityResponse> parser;

        public HotusaAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<HotusaAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<HotusaAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
