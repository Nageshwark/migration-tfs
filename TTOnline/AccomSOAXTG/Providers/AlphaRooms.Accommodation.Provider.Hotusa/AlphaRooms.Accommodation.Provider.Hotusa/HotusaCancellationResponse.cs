﻿using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;


namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaCancellationResponse
    {
        public peticion SupplierRequest { get; set; }
        public respuesta SupplierResponse { get; set; }
    }
}
