﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaCancellationAutomator : IAccommodationCancellationAutomatorAsync<HotusaCancellationResponse>
    {
        private const string HotusaAvailabilityUrl = "HotusaAvailabilityUrl";
        private const string secacc = "secacc";
        private const string codigousu = "codigousu";
        private const string clausu = "clausu";
        private const string afiliacio = "afiliacio";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotusaProviderCancellationRequestFactory hotusaProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotusaCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IHotusaProviderCancellationRequestFactory hotusaSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.hotusaProviderCancellationRequestFactory = hotusaSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<HotusaCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            HotusaCancellationResponse SupplierResponse = new HotusaCancellationResponse();
            string serialisedRequest = "";

            try
            {
                peticion cancelRequest = hotusaProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                serialisedRequest = cancelRequest.XmlSerialize();
                
                    StringBuilder url = new StringBuilder();

                    url.AppendFormat(request.Provider.Parameters.GetParameterValue(HotusaAvailabilityUrl));
                    url.AppendFormat("?secacc={0}", request.Provider.Parameters.GetParameterValue(secacc));
                    url.AppendFormat("&codigousu={0}", request.Provider.Parameters.GetParameterValue(codigousu));
                    url.AppendFormat("&clausu={0}", request.Provider.Parameters.GetParameterValue(clausu));
                    url.AppendFormat("&afiliacio={0}", request.Provider.Parameters.GetParameterValue(afiliacio));
                    url.AppendFormat("&xml={0}", HttpUtility.UrlEncode(serialisedRequest));

                    WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };


                    WebScrapeResponse responseXml = await webScrapeClient.PostAsync(url.ToString(), "");

                    var response = responseXml.Value.XmlDeSerialize<respuesta>();
                    SupplierResponse = new HotusaCancellationResponse
                    {
                        SupplierRequest = cancelRequest,
                        SupplierResponse = response
                    };

                    return SupplierResponse;
                              
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
