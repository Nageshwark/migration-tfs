﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotusa;
using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{

    public class HotusaValuationParser : IAccommodationValuationParser<HotusaValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public HotusaValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, HotusaValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    //string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Hotusa Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, peticion supplierRequest, respuesta supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            if (supplierResponse.param.Item is error)
            {
                error err = (error)supplierResponse.param.Item;
                StringBuilder sbMessage = new StringBuilder();

                sbMessage.AppendLine("Hotusa Availability Parser: Availability Response cannot be parsed because it is null.");
                sbMessage.AppendLine(string.Format("Error Details: {0}", err));

                throw new SupplierApiDataException(sbMessage.ToString());
            }
            else if (supplierResponse.param.Item is hotls)
            {
                hotls hotels = (hotls)supplierResponse.param.Item;
                bool hasResults = int.Parse(hotels.num) > 0;
                byte roomNo = 0;
                if (hasResults)
                {
                    foreach (hot hotel in hotels.hot)
                    {
                        roomNo = 1;
                        foreach (pax occuancy in hotel.res)
                        {
                            byte rms = 0;
                            foreach (hab room in occuancy.hab)
                            {
                                if (room.cod !=
                                    request.SelectedRooms[roomNo-1].AvailabilityResult.ProviderSpecificData["RoomCode"].ToString())
                                    continue;
                                foreach (reg board in room.reg)
                                {
                                    if (board.cod !=
                                        request.SelectedRooms[roomNo-1].AvailabilityResult.ProviderSpecificData["Board"].ToString())
                                        continue;

                                    rms = byte.Parse(board.lin[0].Text[0].ToString().Split('#')[1]); //).Text.ToString().split('#')[1];
                                    for (byte cnt = 0; cnt < rms; cnt++)
                                    {
                                        var result = new AccommodationProviderValuationResult();


                                        result.RoomNumber = (byte)(roomNo + cnt);
                                        result.ProviderEdiCode = request.Provider.EdiCode;
                                        result.EstablishmentName = hotel.nom;
                                        result.EstablishmentEdiCode = hotel.cod;
                                        result.DestinationEdiCode = hotel.prn;


                                        result.BoardCode = board.cod;
                                        //result.BoardDescription = board.cod;

                                        result.RoomCode = room.cod;

                                        result.RoomDescription = room.desc;
                                        result.PaymentModel = PaymentModel.PostPayment;
                                        result.RateType = RateType.NetStandard;
                                        result.IsOpaqueRate = false;

                                        result.IsNonRefundable = board.nr == "1" ? true : false;
                                        //result.NumberOfAvailableRooms = 0;

                                        result.SalePrice = new Money()
                                        {
                                            Amount = Convert.ToDecimal(board.prr)/rms,
                                            CurrencyCode = "EUR"
                                        };

                                        result.CostPrice = result.SalePrice;
                                        //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                                        if (!occuancy.cod.Contains("-"))
                                        {
                                            occuancy.cod = occuancy.cod + "-0";
                                        }
                                        result.Adults = byte.Parse(occuancy.cod.Split('-')[0]);
                                        result.Children = byte.Parse(occuancy.cod.Split('-')[1]);
                                        result.Infants = 0;
                                        result.CheckInDate = request.SelectedRooms[0].AvailabilityResult.CheckInDate;
                                        result.CheckOutDate = request.SelectedRooms[0].AvailabilityResult.CheckOutDate;

                                        result.ProviderSpecificData = new Dictionary<string, string>();

                                        result.ProviderSpecificData.Add("SellingLines",
                                            String.Join(";", board.lin.Select(h => h.Text[0])));
                                        result.ProviderSpecificData.Add("RoomCode", room.cod);
                                        result.ProviderSpecificData.Add("Board", board.cod);

                                        availabilityResults.Enqueue(result);
                                    }
                                    
                                }
                                roomNo = rms;
                            }
                            roomNo++;
                        }
                    }
                }

            }


        }
    }
}
