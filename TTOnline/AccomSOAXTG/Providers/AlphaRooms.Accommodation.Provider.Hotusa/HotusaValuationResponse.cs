﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaValuationResponse
    {
        public peticion SupplierRequest { get; set; }
        public respuesta SupplierResponse { get; set; }
    }
}
