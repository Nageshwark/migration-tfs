﻿using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AlphaRooms.Accommodation.Provider.Hotusa.Service;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Response;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaBookingResponse
    {
        public peticion SupplierRequest { get; set; }
        public respuesta SupplierResponse { get; set; }
    }
}
