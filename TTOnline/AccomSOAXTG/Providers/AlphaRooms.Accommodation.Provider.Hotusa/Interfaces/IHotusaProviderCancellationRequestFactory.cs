﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Hotusa.Service.Request;

namespace AlphaRooms.Accommodation.Provider.Hotusa.Interfaces
{
    public interface IHotusaProviderCancellationRequestFactory
    {
        peticion CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
