﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Hotusa;

namespace AlphaRooms.Accommodation.Provider.Hotusa
{
    public class HotusaValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<HotusaValuationResponse> automator;
        private readonly IAccommodationValuationParser<HotusaValuationResponse> parser;

        public HotusaValuationProvider(IAccommodationValuationAutomatorAsync<HotusaValuationResponse> automator,
                                                IAccommodationValuationParser<HotusaValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
