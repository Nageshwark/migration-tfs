﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsProviderAvailabilityRequestFactory : IHotelBedsProviderAvailabilityRequestFactory
    {
        /// <summary>
        /// Helper class used for grouping rooms together by number of adults, number of children, and child ages
        /// </summary>
        private class Room
        {
            public int Adults { get; set; }
            public int ChildrenAndInfants { get; set; }
            public int[] ChildAges { get; set; }
            public string ChildAgesCsv { get; set; }
        }


        private const string HotelBedsApiVersion = "HotelBedsApiVersion";
        private const string HotelBedsDefaultLanguage = "HotelBedsDefaultLanguage";
        private const string HotelBedsUserName = "HotelBedsUserName";
        private const string HotelBedsPassword = "HotelBedsPassword";

        // private readonly ISupplierService supplierService;

        public HotelBedsProviderAvailabilityRequestFactory()
        {
            // this.supplierService = supplierService;
        }

        /// <summary>
        /// Note:  Limitations for HotelValuedAvail request:
        /// A maximum of 31 nights per hotel service can be requested.
        /// A maximum of 5 rooms per hotel service can be requested.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HotelValuedAvailRQ CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request, bool isPackage)
        {
            List<AccommodationProviderAvailabilityRequestRoom> roomList = request.Rooms.ToList();

            string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
            string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
            string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
            string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

            string checkInDate = request.CheckInDate.ToString("yyyyMMdd");
            string checkOutDate = request.CheckOutDate.ToString("yyyyMMdd");

            HotelValuedAvailRQ supplierRequest = new HotelValuedAvailRQ();

            supplierRequest.echoToken = "EchoToken";
            supplierRequest.sessionId = Guid.NewGuid().ToString().Remove(24);
            supplierRequest.Version = apiVersion;

            supplierRequest.Language = defaultLanguage;

            supplierRequest.Credentials.User = userName;
            supplierRequest.Credentials.Password = password;

            supplierRequest.PaginationData.PageNumber = 1;
            supplierRequest.PaginationData.ItemsPerPage = 999;

            supplierRequest.CheckInDate.Date = checkInDate;
            supplierRequest.CheckOutDate.Date = checkOutDate;

            if (isPackage)
            {
                supplierRequest.ExtraParamList = new ExtraParamList();
                supplierRequest.ExtraParamList.ExtendedData.Type = "EXT_ADDITIONAL_PARAM";
                supplierRequest.ExtraParamList.ExtendedData.Name = "PARAM_SHOW_OPAQUE_CONTRACT";
                supplierRequest.ExtraParamList.ExtendedData.Value = "Y";
            }
            else
            {
                supplierRequest.ShowDirectPayment = "Y";
            }

            // HotelBeds only allows one destination to be searched at a time (though you can search for multiple different zone codes within a single destination).
            // Note that you only need to provide either a single destination, or a list of hotels, not both.
            if (request.DestinationCodes != null && request.DestinationCodes.Any())
            {
                string destinationCode = GetDestination(request.DestinationCodes.First());
                supplierRequest.Destination = new Contracts.AvailabilityRequest.Destination() { Type = "SIMPLE", Code = destinationCode };

                string zoneCode = GetZoneCode(request.DestinationCodes.First());
                if (!string.IsNullOrWhiteSpace(zoneCode))
                {
                    supplierRequest.Destination.ZoneList = new List<Zone>() { new Zone() { Type = "SIMPLE", Code = zoneCode } };
                }
            }
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
            {
                // Search by specific hotel codes which can be located in any destination. Searching by hotel is therefore not limited by destination.
                // NOTE: establishment EDI codes must be between 1 and 10 characters long.
                supplierRequest.HotelCodeList = new HotelCodeList();
                supplierRequest.HotelCodeList.WithinResults = "Y";

                foreach (string establishmentEdiCode in request.EstablishmentCodes)
                {
                    if (establishmentEdiCode.Length >= 1 && establishmentEdiCode.Length <= 10)
                    {
                        supplierRequest.HotelCodeList.ProductCode.Add(establishmentEdiCode);
                    }
                }
            }

            // Add one <HotelOccupancy> element per room in the request.
            // DO NOT group rooms by number of adults/children and their ages as this makes valuation impossible
            foreach (var room in roomList)
            {
                HotelOccupancy hotelOccupancy = new HotelOccupancy();
                hotelOccupancy.RoomCount = 1;

                Occupancy occupancy = new Occupancy();
                occupancy.AdultCount = room.Guests.AdultsCount;
                occupancy.ChildCount = room.Guests.ChildrenCount + room.Guests.InfantsCount;

                if (room.Guests.ChildrenCount + room.Guests.InfantsCount > 0)
                {
                    occupancy.GuestList = new List<Customer>();

                    var children = room.Guests.Where(guest => guest.Type != GuestType.Adult);

                    foreach (var child in children)
                    {
                        occupancy.GuestList.Add(new Customer() { Age = child.Age, Type = "CH" });
                    }
                }

                hotelOccupancy.Occupancy = occupancy;
                supplierRequest.OccupancyList.Add(hotelOccupancy);
            }

            //return new HotelBedsAvailabilityResponse()
            //{
            //    SupplierRequest = supplierRequest,
            //    isPackageRequest = isPackage,
            //    Rooms = roomList
            //};

            return supplierRequest;
        }

        /// <summary>
        /// Note:  Limitations for HotelValuedAvail request:
        /// A maximum of 31 nights per hotel service can be requested.
        /// A maximum of 5 rooms per hotel service can be requested.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HotelBedsAvailabilityResponse CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request, List<AccommodationProviderAvailabilityRequestRoom> roomList, bool isPackage)
        {
            string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
            string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
            string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
            string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

            string checkInDate = request.CheckInDate.ToString("yyyyMMdd");
            string checkOutDate = request.CheckOutDate.ToString("yyyyMMdd");

            HotelValuedAvailRQ supplierRequest = new HotelValuedAvailRQ();

            supplierRequest.echoToken = "EchoToken";
            supplierRequest.sessionId = Guid.NewGuid().ToString().Remove(24);
            supplierRequest.Version = apiVersion;

            supplierRequest.Language = defaultLanguage;

            supplierRequest.Credentials.User = userName;
            supplierRequest.Credentials.Password = password;

            supplierRequest.PaginationData.PageNumber = 1;
            supplierRequest.PaginationData.ItemsPerPage = 999;

            supplierRequest.CheckInDate.Date = checkInDate;
            supplierRequest.CheckOutDate.Date = checkOutDate;

            if (isPackage)
            {
                supplierRequest.ExtraParamList = new ExtraParamList();
                supplierRequest.ExtraParamList.ExtendedData.Type = "EXT_ADDITIONAL_PARAM";
                supplierRequest.ExtraParamList.ExtendedData.Name = "PARAM_SHOW_OPAQUE_CONTRACT";
                supplierRequest.ExtraParamList.ExtendedData.Value = "Y";
            }
            else
            {
                supplierRequest.ShowDirectPayment = "Y";
            }

            // HotelBeds only allows one destination to be searched at a time (though you can search for multiple different zone codes within a single destination).
            // Note that you only need to provide either a single destination, or a list of hotels, not both.
            if (request.DestinationCodes != null && request.DestinationCodes.Any())
            {
                string destinationCode = GetDestination(request.DestinationCodes.First());
                supplierRequest.Destination = new Contracts.AvailabilityRequest.Destination() { Type = "SIMPLE", Code = destinationCode };

                string zoneCode = GetZoneCode(request.DestinationCodes.First());
                if (!string.IsNullOrWhiteSpace(zoneCode))
                {
                    supplierRequest.Destination.ZoneList = new List<Zone>() { new Zone() { Type = "SIMPLE", Code = zoneCode } };
                }
            }
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
            {
                // Search by specific hotel codes which can be located in any destination. Searching by hotel is therefore not limited by destination.
                // NOTE: establishment EDI codes must be between 1 and 10 characters long.
                supplierRequest.HotelCodeList = new HotelCodeList();
                supplierRequest.HotelCodeList.WithinResults = "Y";

                foreach (string establishmentEdiCode in request.EstablishmentCodes)
                {
                    if (establishmentEdiCode.Length >= 1 && establishmentEdiCode.Length <= 10)
                    {
                        supplierRequest.HotelCodeList.ProductCode.Add(establishmentEdiCode);
                    }
                }
            }

            // Add one <HotelOccupancy> element per room in the request.
            // DO NOT group rooms by number of adults/children and their ages as this makes valuation impossible
            foreach (var room in roomList)
            {
                HotelOccupancy hotelOccupancy = new HotelOccupancy();
                hotelOccupancy.RoomCount = 1;

                Occupancy occupancy = new Occupancy();
                occupancy.AdultCount = room.Guests.AdultsCount;
                occupancy.ChildCount = room.Guests.ChildrenCount + room.Guests.InfantsCount;

                if (room.Guests.ChildrenCount + room.Guests.InfantsCount > 0)
                {
                    occupancy.GuestList = new List<Customer>();

                    var children = room.Guests.Where(guest => guest.Type != GuestType.Adult);

                    foreach (var child in children)
                    {
                        occupancy.GuestList.Add(new Customer() { Age = child.Age, Type = "CH" });
                    }
                }

                hotelOccupancy.Occupancy = occupancy;
                supplierRequest.OccupancyList.Add(hotelOccupancy);
            }

            return new HotelBedsAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                isPackageRequest = isPackage,
                Rooms = roomList
            };
        }


        /// <summary>
        /// The provider destination edi code (for example, PMI87) that is supplied from the db will have the format <DestinationCode><ZoneCode>.
        /// The destination will always be alphabetic, e.g. PMI, whilst the zone code will always be numeric, e.g. 27.
        /// However, some provider destination edi codes do not have zone codes, and consist of a destimation code only, e.g. PMI.
        /// </summary>
        /// <param name="providerDestinationEdiCode"></param>
        /// <returns></returns>
        private string GetDestination(string providerDestinationEdiCode)
        {
            string destinationCode = providerDestinationEdiCode;

            //if (Regex.IsMatch(providerDestinationEdiCode, "[a-zA-Z]+"))
            //{
            //    destinationCode = Regex.Match(providerDestinationEdiCode, "[a-zA-Z]+").Groups[0].Value;
            //}

            int number = 0;
            for (int i = 0; i < providerDestinationEdiCode.Length; i++)
            {
                if (int.TryParse(providerDestinationEdiCode[i].ToString(), out number))
                {
                    destinationCode = providerDestinationEdiCode.Substring(0, i);
                    break;
                }
            }

            return destinationCode;
        }

        private string GetZoneCode(string providerDestinationEdiCode)
        {
            string zoneCode = string.Empty;

            //if (Regex.IsMatch(providerDestinationEdiCode, "[0-9]+"))
            //{
            //    zoneCode = Regex.Match(providerDestinationEdiCode, "[0-9]+").Groups[0].Value;
            //}

            int number = 0;
            for (int i = 0; i < providerDestinationEdiCode.Length; i++)
            {
                if (int.TryParse(providerDestinationEdiCode[i].ToString(), out number))
                {
                    zoneCode = providerDestinationEdiCode.Substring(i);
                    break;
                }
            }

            return zoneCode;
        }
    }
}
