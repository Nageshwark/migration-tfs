﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Factories
{
    public class HotelBedsCancellationRequestFactory : IHotelBedsProviderCancellationRequestFactory
    {
        private const string HotelBedsApiVersion = "HotelBedsApiVersion";
        private const string HotelBedsDefaultLanguage = "HotelBedsDefaultLanguage";
        private const string HotelBedsUserName = "HotelBedsUserName";
        private const string HotelBedsPassword = "HotelBedsPassword";
        public PurchaseCancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
            string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
            string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
            string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

            var bookingReference = request.ProviderBookingReference.Split('-').ToArray();
            
            var providerCancelRequest = new PurchaseCancelRQ
                {
                    Type = "C",
                    Language = defaultLanguage,
                    Version = apiVersion,
                    Credentials = new Credentials {User = userName, Password = password},
                    PurchaseReference = new PurchaseReference { FileNumber = Convert.ToInt32(bookingReference[1]), IncomingOffice = new IncomingOffice {Code = Convert.ToUInt16(bookingReference[0])} }
                };

           return providerCancelRequest;

        }
    }
}
