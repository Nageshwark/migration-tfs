﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsProviderBookingRequestFactory : IHotelBedsProviderBookingRequestFactory
    {
        private const string HotelBedsApiVersion = "HotelBedsApiVersion";
        private const string HotelBedsDefaultLanguage = "HotelBedsDefaultLanguage";
        private const string HotelBedsUserName = "HotelBedsUserName";
        private const string HotelBedsPassword = "HotelBedsPassword";

        public HotelBedsBookingResponse CreateSupplierBookingRequest(AccommodationProviderBookingRequest request, List<KeyValuePair<string, AccommodationProviderBookingRequestRoom>> roomsToBeBooked, string key)
        {
            // Get all the rooms that belong to the PurchaseToken (the key is the PurchaseToken)
            var roomsForPurchaseToken = roomsToBeBooked.Where(room => room.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase)).OrderBy(room => room.Value.ValuationResult.RoomNumber);

            // Build a semi colon separated list of room numbers for the request EchoToken
            StringBuilder sb = new StringBuilder("Room Numbers:");

            foreach (var room in roomsForPurchaseToken)
            {
                sb.Append(room.Value.ValuationResult.RoomNumber);
                sb.Append(",");
            }

            string echoToken = sb.ToString();
            echoToken = echoToken.TrimEnd(',');            // remove final semi colon


            PurchaseConfirmRQ supplierRequest = new PurchaseConfirmRQ();

            string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
            string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
            string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
            string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

            supplierRequest.Version = apiVersion;
            supplierRequest.Language = defaultLanguage;
            supplierRequest.EchoToken = echoToken;

            supplierRequest.Credentials.User = userName;
            supplierRequest.Credentials.Password = password;

            supplierRequest.ConfirmationData.PurchaseToken = roomsForPurchaseToken.First().Value.ValuationResult.ProviderSpecificData["PurchaseToken"];

            string checkInDate = roomsForPurchaseToken.First().Value.ValuationResult.CheckInDate.ToString("yyyyMMdd");
            string checkOutDate = roomsForPurchaseToken.First().Value.ValuationResult.CheckOutDate.ToString("yyyyMMdd");

            // Booking Holder
            var leadPassenger = request.Customer;
            supplierRequest.ConfirmationData.Holder.Type = "AD";        // Adult
            supplierRequest.ConfirmationData.Holder.Name = leadPassenger.FirstName;
            supplierRequest.ConfirmationData.Holder.LastName = leadPassenger.Surname;
            supplierRequest.ConfirmationData.Holder.Age = int.Parse(leadPassenger.Age.ToString());
            supplierRequest.ConfirmationData.Holder.CustomerId = "Hldr";           // Customer ID must be between 1 and 5 characters long.

            // Our reference - this should be unique for every request (as requested by HotelBeds). Therefore, attach the list of room numbers to it. 20 characters MAXIMUM.
            string agencyRef = request.ItineraryId.ToString() + "/" + Regex.Replace(echoToken, "Room Numbers:", string.Empty);
            agencyRef = agencyRef.Left(20);
            supplierRequest.ConfirmationData.AgencyReference = agencyRef;

            // For each room that is to be booked under the purchase token, we need to create a new <ServiceData> element
            int i = 1;

            foreach (var roomForPurchaseToken in roomsForPurchaseToken)
            {
                AccommodationProviderValuationResult valuationResult = roomForPurchaseToken.Value.ValuationResult;

                ConfirmationServiceDataHotel serviceData = new ConfirmationServiceDataHotel();

                serviceData.SPUI = valuationResult.ProviderSpecificData["ServiceSPUI"];

                // Create the customer list: adults first, followed by children
                var adults = roomForPurchaseToken.Value.Guests.Where(guest => guest.Type == GuestType.Adult);

                foreach (var adult in adults)
                {
                    Customer customer = new Customer();

                    customer.Type = "AD";
                    customer.Age = int.Parse(adult.Age.ToString());
                    customer.Name = adult.FirstName;
                    customer.LastName = adult.Surname;

                    customer.CustomerId = i;
                    i++;

                    serviceData.CustomerList.Add(customer);
                }

                // Children
                var children = roomForPurchaseToken.Value.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant).OrderBy(guest => guest.Age);

                foreach (var child in children)
                {
                    Customer customer = new Customer();

                    customer.Type = "CH";
                    customer.Age = int.Parse(child.Age.ToString());
                    customer.Name = child.FirstName;
                    customer.LastName = child.Surname;

                    customer.CustomerId = i;
                    i++;

                    serviceData.CustomerList.Add(customer);
                }

                // Add Comments (special requests) if there are any
                StringBuilder commentBuilder = new StringBuilder();
                int j = 1;

                if (request.ValuatedRooms[0].SpecialRequests.AdjoiningRooms ||
                    request.ValuatedRooms[0].SpecialRequests.CotRequired ||
                    request.ValuatedRooms[0].SpecialRequests.DisabledAccess ||
                    request.ValuatedRooms[0].SpecialRequests.LateArrival ||
                    request.ValuatedRooms[0].SpecialRequests.NonSmoking ||
                    request.ValuatedRooms[0].SpecialRequests.SeaViews ||
                    !string.IsNullOrWhiteSpace(request.ValuatedRooms[0].SpecialRequests.OtherRequests))
                {
                    commentBuilder.AppendLine("Guest has requested:");

                    if (request.ValuatedRooms[0].SpecialRequests.AdjoiningRooms)
                    {
                        commentBuilder.AppendLine(j + ": Adjoining Rooms.");
                        j++;
                    }

                    if (request.ValuatedRooms[0].SpecialRequests.CotRequired)
                    {
                        commentBuilder.AppendLine(j + ": Infant Cot.");
                        j++;
                    }

                    if (request.ValuatedRooms[0].SpecialRequests.DisabledAccess)
                    {
                        commentBuilder.AppendLine(j + ": Disabled Friendly Room.");
                        j++;
                    }

                    if (request.ValuatedRooms[0].SpecialRequests.LateArrival)
                    {
                        commentBuilder.AppendLine(j + ": Late Arrival.");
                        j++;
                    }

                    if (request.ValuatedRooms[0].SpecialRequests.NonSmoking)
                    {
                        commentBuilder.AppendLine(j + ": Non Smoking Room.");
                        j++;
                    }

                    if (request.ValuatedRooms[0].SpecialRequests.SeaViews)
                    {
                        commentBuilder.AppendLine(j + ": Room with a Sea View");
                        j++;
                    }

                    if (!string.IsNullOrWhiteSpace(request.ValuatedRooms[0].SpecialRequests.OtherRequests))
                    {
                        commentBuilder.AppendLine(j + ": " + request.ValuatedRooms[0].SpecialRequests.OtherRequests);
                        j++;
                    }

                    Comment comment = new Comment();
                    comment.Type = "SERVICE";   // SERVICE = comments appear on voucher AND are sent to office/hotel; INCOMING = comments are sent to the office/hotel and do NOT appear on the voucher
                    comment.Value = commentBuilder.ToString();

                    List<Comment> commentList = new List<Comment>();
                    commentList.Add(comment);

                    serviceData.CommentList = commentList;
                }

                // Add the ServiceData element to the list
                supplierRequest.ConfirmationData.ConfirmationServiceDataList.Add(serviceData);
            }

            // If any of the rooms are customer pay direct, then a card will need to be included in the booking request.
            // Note that, for a particular purchase token, either ALL the rooms or NONE of the rooms will be pay direct -> you will NEVER get a mix of post payment and pay direct rooms under the same purchase token
            bool isPayDirect = false;

            if (roomsForPurchaseToken.Any(room => room.Value.ValuationResult.PaymentModel == PaymentModel.CustomerPayDirect))
            {
                isPayDirect = true;

                // Get the payment details
                var paymentDetails = roomsForPurchaseToken.Where(room => room.Value.PaymentDetails != null).FirstOrDefault();                 
                if (paymentDetails.Value != null)
                {
                    var creditCardInfo = paymentDetails.Value.PaymentDetails;

                    PaymentData paymentData = new PaymentData();
                    paymentData.PaymentCard.CardHolderName = creditCardInfo.CardHolderName;
                    paymentData.PaymentCard.CardNumber = creditCardInfo.CardNumber;
                    paymentData.PaymentCard.CardType = GetHotelBedsCreditCardCode(creditCardInfo.CardType);
                    paymentData.PaymentCard.ExpiryDate = creditCardInfo.CardExpireDate.Month.ToString("00") +
                                                         creditCardInfo.CardExpireDate.Year.ToString("00");
                    paymentData.PaymentCard.CardCVC = creditCardInfo.CardSecurityCode;

                    // the credit card infor would appear to attach to the First ServiceData element. It does NOT form a separate section under the <ConfirmationData> element.
                    supplierRequest.ConfirmationData.ConfirmationServiceDataList.First().PaymentData = paymentData;
                }
            }

            HotelBedsBookingResponse requestContainer = new HotelBedsBookingResponse();

            requestContainer.SupplierRequest = supplierRequest;
            requestContainer.Rooms = roomsForPurchaseToken.Select(room => room.Value);
            requestContainer.IsPayDirect = isPayDirect;
            requestContainer.PurchaseToken = key;
            requestContainer.SupplierResponse = string.Empty;

            return requestContainer;
        }






        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //public PurchaseConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request, AccommodationProviderBookingRequestRoom room)
        //{
        //    string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
        //    string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
        //    string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
        //    string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

        //    AccommodationProviderValuationResult roomToBook = room.ValuationResult;

        //    string checkInDate = roomToBook.CheckInDate.ToString("yyyyMMdd");
        //    string checkOutDate = roomToBook.CheckOutDate.ToString("yyyyMMdd");

        //    PurchaseConfirmRQ supplierRequest = new PurchaseConfirmRQ();

        //    supplierRequest.Version = apiVersion;
        //    supplierRequest.Language = defaultLanguage;
        //    supplierRequest.EchoToken = "RoomNumber:" + roomToBook.RoomNumber;

        //    supplierRequest.Credentials.User = userName;
        //    supplierRequest.Credentials.Password = password;

        //    supplierRequest.ConfirmationData.PurchaseToken = roomToBook.ProviderSpecificData["PurchaseToken"];

        //    // Booking Holder
        //    var leadPassenger = request.Customer;
        //    supplierRequest.ConfirmationData.Holder.Type = "AD";        // Adult
        //    supplierRequest.ConfirmationData.Holder.Name = leadPassenger.FirstName;
        //    supplierRequest.ConfirmationData.Holder.LastName = leadPassenger.Surname;
        //    supplierRequest.ConfirmationData.Holder.Age = int.Parse(leadPassenger.Age.ToString());
        //    supplierRequest.ConfirmationData.Holder.CustomerId = leadPassenger.Id.ToString();

        //    supplierRequest.ConfirmationData.AgencyReference = request.ItineraryId.ToString();

        //    supplierRequest.ConfirmationData.ConfirmationServiceDataList.ServiceData.SPUI = roomToBook.ProviderSpecificData["ServiceSPUI"];

        //    // Add customers
        //    int i = 1;
        //    foreach (var pax in request.ValuatedRooms[0].Guests)
        //    {
        //        Customer customer = new Customer();

        //        if (pax.Type == GuestType.Adult)
        //        {
        //            customer.Type = "AD";
        //        }
        //        else
        //        {
        //            customer.Type = "CH";
        //        }

        //        customer.Age = int.Parse(pax.Age.ToString());
        //        customer.Name = pax.FirstName;
        //        customer.LastName = pax.Surname;

        //        customer.CustomerId = i;
        //        i++;

        //        supplierRequest.ConfirmationData.ConfirmationServiceDataList.ServiceData.CustomerList.Add(customer);
        //    }

        //    // Add Payment Card details, if the hotel is CustomerPayDirect
        //    if (roomToBook.PaymentModel == PaymentModel.CustomerPayDirect &&
        //        request.ValuatedRooms[0].PaymentDetails != null)
        //    {
        //        PaymentData paymentData = new PaymentData();
        //        paymentData.PaymentCard.CardHolderName = request.ValuatedRooms[0].PaymentDetails.CardHolderName;
        //        paymentData.PaymentCard.CardNumber = request.ValuatedRooms[0].PaymentDetails.CardNumber;
        //        paymentData.PaymentCard.CardType = GetHotelBedsCreditCardCode(request.ValuatedRooms[0].PaymentDetails.CardType);
        //        paymentData.PaymentCard.ExpiryDate = request.ValuatedRooms[0].PaymentDetails.CardExpireDate.Month.ToString("00") +
        //                                             request.ValuatedRooms[0].PaymentDetails.CardExpireDate.Year.ToString("00");
        //        paymentData.PaymentCard.CardCVC = request.ValuatedRooms[0].PaymentDetails.CardSecurityCode;

        //        supplierRequest.ConfirmationData.ConfirmationServiceDataList.ServiceData.PaymentData = paymentData;
        //    }

        //    // Add Comments
        //    StringBuilder sb = new StringBuilder();
        //    int j = 1;

        //    if (request.ValuatedRooms[0].SpecialRequests.AdjoiningRooms ||
        //        request.ValuatedRooms[0].SpecialRequests.Cot ||
        //        request.ValuatedRooms[0].SpecialRequests.DisabledFriendly ||
        //        request.ValuatedRooms[0].SpecialRequests.LateArrival ||
        //        request.ValuatedRooms[0].SpecialRequests.NonSmoking ||
        //        request.ValuatedRooms[0].SpecialRequests.SeaViews ||
        //        !string.IsNullOrWhiteSpace(request.ValuatedRooms[0].SpecialRequests.OtherRequests))
        //    {
        //        sb.AppendLine("Guest has requested:");

        //        if (request.ValuatedRooms[0].SpecialRequests.AdjoiningRooms)
        //        {
        //            sb.AppendLine(j + ": Adjoining Rooms.");
        //            j++;
        //        }

        //        if (request.ValuatedRooms[0].SpecialRequests.Cot)
        //        {
        //            sb.AppendLine(j + ": Infant Cot.");
        //            j++;
        //        }

        //        if (request.ValuatedRooms[0].SpecialRequests.DisabledFriendly)
        //        {
        //            sb.AppendLine(j + ": Disabled Friendly Room.");
        //            j++;
        //        }

        //        if (request.ValuatedRooms[0].SpecialRequests.LateArrival)
        //        {
        //            sb.AppendLine(j + ": Late Arrival.");
        //            j++;
        //        }

        //        if (request.ValuatedRooms[0].SpecialRequests.NonSmoking)
        //        {
        //            sb.AppendLine(j + ": Non Smoking Room.");
        //            j++;
        //        }

        //        if (request.ValuatedRooms[0].SpecialRequests.SeaViews)
        //        {
        //            sb.AppendLine(j + ": Room with a Sea View");
        //            j++;
        //        }

        //        if (!string.IsNullOrWhiteSpace(request.ValuatedRooms[0].SpecialRequests.OtherRequests))
        //        {
        //            sb.AppendLine(j + ": " + request.ValuatedRooms[0].SpecialRequests.OtherRequests);
        //            j++;
        //        }

        //        Comment specialRequests = new Comment();
        //        specialRequests.Type = "INCOMING";
        //        //specialRequests.Value = sb.ToString();
        //        specialRequests.Value = "TEST";

        //        List<Comment> commentList = new List<Comment>();
        //        commentList.Add(specialRequests);

        //        supplierRequest.ConfirmationData.ConfirmationServiceDataList.ServiceData.CommentList = commentList;
        //    }

        //    return supplierRequest;
        //}

        private string GetHotelBedsCreditCardCode(CardType alphaCardType)
        {
            string hotelBedsCardCode = string.Empty;

            switch (alphaCardType)
            {
                case CardType.AmericanExpress:
                    hotelBedsCardCode = "AE";
                    break;

                case CardType.DinersClub:
                    hotelBedsCardCode = "DC";
                    break;

                case CardType.MasterCard:
                case CardType.MastercardDebit:
                    hotelBedsCardCode = "MC";
                    break;

                case CardType.EuroCard:
                    hotelBedsCardCode = "AP";
                    break;

                case CardType.JCB:
                    hotelBedsCardCode = "JC";
                    break;

                case CardType.Maestro:
                    hotelBedsCardCode = "MA";
                    break;

                case CardType.VisaElectron:
                    hotelBedsCardCode = "VE";
                    break;

                case CardType.VisaCredit:
                case CardType.VisaDebit:
                    hotelBedsCardCode = "VI";
                    break;
            }

            return hotelBedsCardCode;
        }
    }
}
