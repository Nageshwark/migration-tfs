﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
using ValuationRequest = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    /// <summary>
    /// The ServiceAddRQ request Business Rules:
    /// The ServiceAddRQ can be used in one of two ways:
    /// 1. A single request can be created for multiple rooms of any type (both pay direct and post payment), providing that all rooms have the same contract/hotel
    /// 2. For post payment rooms only, each room can be valued in a single request, but these requests can be chained together, one after the other to given an eventual end result which is a valuation for all the rooms in the chain. This method is not 
    /// limited by contract/hotel restrictions, but it can't be used for direct pay rooms.
    /// This system will use a combination of both methods.
    /// For pay direct rooms, they will be grouped by contract/hotel and a single request will be created for all the rooms in the contract/hotel.
    /// For all post payment rooms, one request will be created per room, and these will be chained together to give the final result.
    /// </summary>
    public class HotelBedsProviderValuationRequestFactory : IHotelBedsProviderValuationRequestFactory
    {
        private const string HotelBedsApiVersion = "HotelBedsApiVersion";
        private const string HotelBedsDefaultLanguage = "HotelBedsDefaultLanguage";
        private const string HotelBedsUserName = "HotelBedsUserName";
        private const string HotelBedsPassword = "HotelBedsPassword";

        public HotelBedsValuationResponse CreateSupplierValuationRequestForPayDirect(AccommodationProviderValuationRequest request,
                                                                                     List<KeyValuePair<string, AccommodationProviderValuationRequestRoom>> selectedRooms,
                                                                                     string key)
        {
            // Get all the rooms that belong to the contract/hotel
            var roomsForContract = selectedRooms.Where(room => room.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase));
            IEnumerable<AccommodationProviderValuationRequestRoom> roomsToBeValued = roomsForContract.Select(room => room.Value);

            // Build a semi colon separated list of room numbers for the request EchoToken
            StringBuilder sb = new StringBuilder("Pay Direct Room Numbers:");

            foreach (var room in roomsForContract)
            {
                sb.Append(room.Value.AvailabilityResult.RoomNumber);
                sb.Append(",");
            }

            string echoToken = sb.ToString();
            echoToken = echoToken.TrimEnd(',');            // remove final semi colon

            bool isPayDirect = true;

            HotelBedsValuationResponse valuationContainer = BuildValuationRequest(request, roomsToBeValued, isPayDirect, echoToken);

            return valuationContainer;
        }

        public HotelBedsValuationResponse CreateSupplierValuationRequestForPostPayment(AccommodationProviderValuationRequest request, AccommodationProviderValuationRequestRoom selectedRoom)
        {
            IEnumerable<AccommodationProviderValuationRequestRoom> roomsToBeValued = new List<AccommodationProviderValuationRequestRoom>() { selectedRoom };
            string echoToken = "Post Payment Room Numbers:" + selectedRoom.AvailabilityResult.RoomNumber;
            bool isPayDirect = false;

            HotelBedsValuationResponse valuationContainer = BuildValuationRequest(request, roomsToBeValued, isPayDirect, echoToken);

            return valuationContainer;
        }

        private HotelBedsValuationResponse BuildValuationRequest(   AccommodationProviderValuationRequest request,
                                                                    IEnumerable<AccommodationProviderValuationRequestRoom> roomsToBeValued,
                                                                    bool isPayDirect,
                                                                    string echoToken)
        {
            roomsToBeValued = roomsToBeValued.OrderBy(room => room.RoomNumber);

            ServiceAddRQ supplierRequest = new ServiceAddRQ();

            string apiVersion = request.Provider.Parameters.GetParameterValue(HotelBedsApiVersion);
            string defaultLanguage = request.Provider.Parameters.GetParameterValue(HotelBedsDefaultLanguage);
            string userName = request.Provider.Parameters.GetParameterValue(HotelBedsUserName);
            string password = request.Provider.Parameters.GetParameterValue(HotelBedsPassword);

            supplierRequest.Version = apiVersion;
            supplierRequest.Language = defaultLanguage;
            supplierRequest.EchoToken = echoToken;     // Must be between 1 and 50 characters long or HotelBeds generates an error in the response.

            supplierRequest.Credentials.User = userName;
            supplierRequest.Credentials.Password = password;

            supplierRequest.Service.availToken = roomsToBeValued.First().AvailabilityResult.ProviderSpecificData["AvailabilityToken"];
            supplierRequest.Service.ContractList = roomsToBeValued.First().AvailabilityResult.ProviderSpecificData["ContractList"].XmlDeSerialize<ContractList>();

            supplierRequest.Service.DateFrom.Date = roomsToBeValued.First().AvailabilityRequest.CheckInDate.ToString("yyyyMMdd");
            supplierRequest.Service.DateTo.Date = roomsToBeValued.First().AvailabilityRequest.CheckOutDate.ToString("yyyyMMdd");

            supplierRequest.Service.HotelInfo.Code = roomsToBeValued.First().AvailabilityResult.EstablishmentEdiCode;
            supplierRequest.Service.HotelInfo.Destination.Code = roomsToBeValued.First().AvailabilityResult.ProviderSpecificData["DestinationCode"];
            supplierRequest.Service.HotelInfo.Destination.Type = "SIMPLE";

            // Add the list of rooms to be valued.
            List<ValuationRequest.AvailableRoom> rooms = new List<AvailableRoom>();

            foreach (var room in roomsToBeValued)
            {
                ValuationRequest.AvailableRoom roomDetails = new ValuationRequest.AvailableRoom();

                roomDetails.HotelOccupancy.RoomCount = 1;
                roomDetails.HotelOccupancy.Occupancy.AdultCount = room.Guests.AdultsCount;
                roomDetails.HotelOccupancy.Occupancy.ChildCount = room.Guests.ChildrenCount + room.Guests.InfantsCount;

                // Add child ages if children are present in the room
                if (room.Guests.ChildrenCount + room.Guests.InfantsCount > 0)
                {
                    // Get just the children, ordered by age (IMPORTANT -> this same order will be used when making a booking)
                    IEnumerable<AccommodationProviderAvailabilityRequestGuest> children = room.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant).OrderBy(guest => guest.Age);

                    List<ValuationRequest.Customer> guestList = new List<ValuationRequest.Customer>();

                    foreach (var child in children)
                    {
                        ValuationRequest.Customer childCustomer = new ValuationRequest.Customer();
                        childCustomer.Age = child.Age;
                        childCustomer.Type = "CH";

                        guestList.Add(childCustomer);
                    }

                    roomDetails.HotelOccupancy.Occupancy.GuestList = guestList;
                }

                roomDetails.HotelRoom.Board.Code = room.AvailabilityResult.BoardCode;
                roomDetails.HotelRoom.Board.Type = room.AvailabilityResult.ProviderSpecificData["BoardType"];

                roomDetails.HotelRoom.RoomType.Code = room.AvailabilityResult.RoomCode;
                roomDetails.HotelRoom.RoomType.Characteristic = room.AvailabilityResult.ProviderSpecificData["RoomCharacteristic"];
                roomDetails.HotelRoom.RoomType.Type = room.AvailabilityResult.ProviderSpecificData["RoomType"];

                rooms.Add(roomDetails);
            }

            supplierRequest.Service.AvailableRooms = rooms;

            // Create the container to hold the request
            HotelBedsValuationResponse valuationContainer = new HotelBedsValuationResponse();
            valuationContainer.SupplierRequest = supplierRequest;
            valuationContainer.IsPayDirect = isPayDirect;
            valuationContainer.Rooms = roomsToBeValued;
            valuationContainer.SupplierResponse = null;

            return valuationContainer;
        }

    }
}
