﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AvailabilityResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse;
using BookingRequest = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest;
using BookingResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingResponse;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsBookingParser : IAccommodationBookingParser<IEnumerable<HotelBedsBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public HotelBedsBookingParser(  ILogger logger,
                                        IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<HotelBedsBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("HotelBeds Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        HotelBedsBookingResponse supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (string.IsNullOrWhiteSpace(supplierResponse.SupplierResponse))
            {
                string message = string.Format("HotelBeds Booking Parser: Booking Response cannot be parsed because it is null. Booking Request Id: {0}", request.BookingId);

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }

            BookingResponse.PurchaseConfirmRS bookingResponse = supplierResponse.SupplierResponse.XmlDeSerialize<BookingResponse.PurchaseConfirmRS>();

            // Debug Code
            if (request.Debugging)
            {
                string serialisedRequest = supplierResponse.SupplierRequest.XmlSerialize();
                string serialisedResponse = bookingResponse.XmlSerialize();
            }

            // Check that no error was included in the response - if an error is returned, log the error in the event log, but DO NOT throw an exception. Instead, return a failed booking.
            if (bookingResponse != null && bookingResponse.ErrorList != null && bookingResponse.ErrorList.Any())
            {
                StringBuilder sb1 = new StringBuilder();
                sb1.AppendLine("HotelBeds Response Error.");
                sb1.AppendLine("HotelBeds returned one or more errors in their booking response:");
                sb1.AppendLine("Booking Request:");
                sb1.AppendLine(supplierResponse.SupplierRequest.XmlSerialize());
                sb1.AppendLine();
                sb1.AppendLine("Booking Response:");
                sb1.AppendLine(bookingResponse.XmlSerialize());
                sb1.AppendLine();

                StringBuilder sb2 = new StringBuilder();
                foreach (var error in bookingResponse.ErrorList)
                {
                    if (error.DetailedMessage.Contains("ERR_741", StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Special error message if an "ERR_741, There is no sufficient space in the service for all the passengers." error message is received. 
                        // This occurs if there is no longer the required room availability at the time of booking. 
                        // Email from HotelBeds on 25th September 2015:
                        // "Our system only blocks a room from being returned as available if it gets confirmed. 
                        // So you could get it returned as available as someone else already has the room in the shopping cart and are getting ready to confirm it. 
                        // So by the time you get to perform aby operation with this room it will already confirmed therefore unavailable."
                        sb2.AppendLine("HotelBeds Booking Error:");
                        sb2.AppendLine("One or more of the requested rooms is sold out and cannot be booked.");
                        sb2.AppendLine("Booking cannot complete.");
                        sb2.AppendLine();
                    }
                    else if (error.DetailedMessage.Contains("OSC_057", StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Payment error: "OSC_057 - Payment Error - WLP - 2"
                        // Need to note the type of credit card.
                        sb2.AppendLine("HotelBeds Booking Error:");
                        sb2.AppendLine("Payment Error.");
                        var paymentDetails = request.ValuatedRooms.First().PaymentDetails;
                        sb2.AppendLine("Credit Card Type: " + (paymentDetails != null ? paymentDetails.CardType.ToString() : "none"));
                        sb2.AppendLine("Expiry Date: " + (paymentDetails != null ? paymentDetails.CardExpireDate.ToString() : "none"));
                    }
                    else
                    {
                        sb2.AppendLine("HotelBeds Booking Error:");
                        sb2.AppendLine(error.Message);
                        sb2.AppendLine(error.DetailedMessage);
                        sb2.AppendLine();
                    }
                }

                // Log the error to the event log
                string message = sb1.ToString() + sb2.ToString();
                logger.Error(message);

                // Create a failed booking result
                foreach (var room in supplierResponse.Rooms)
                {
                    AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();

                    result.BookingStatus = BookingStatus.NotConfirmed;
                    result.Message = sb2.ToString();
                    result.ProviderBookingReference = string.Empty;
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.RoomNumber = room.RoomNumber;

                    bookingResults.Enqueue(result);
                }
            }
            else
            {
                // Response is success -> create the booking results for the rooms that were valued.
                CreateAvailabilityResultFromResponse(request, supplierResponse, bookingResponse, bookingResults);
            }
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderBookingResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supplierResponse"></param>
        /// <param name="bookingResponse"></param>
        /// <param name="bookingResults"></param>
        private void CreateAvailabilityResultFromResponse(  AccommodationProviderBookingRequest request,
                                                            HotelBedsBookingResponse supplierResponse,
                                                            BookingResponse.PurchaseConfirmRS bookingResponse,
                                                            ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            try
            {
                string bookingReference = string.Empty;
                BookingStatus bookingStatus = BookingStatus.NotConfirmed;
                Dictionary<string, string> providerSpecificData = new Dictionary<string, string>();
                string message = string.Empty;

                if (bookingResponse.Purchase != null &&
                    bookingResponse.Purchase.Reference != null &&
                    bookingResponse.Purchase.Reference.IncomingOffice != null &&
                    bookingResponse.Purchase.Reference.FileNumber > 0 &&
                    bookingResponse.Purchase.Reference.IncomingOffice.Code > 0 &&
                    bookingResponse.Purchase.Status.Equals("BOOKING", StringComparison.InvariantCultureIgnoreCase))
                {
                    bookingReference = bookingResponse.Purchase.Reference.IncomingOffice.Code + "-" + bookingResponse.Purchase.Reference.FileNumber;
                    bookingStatus = BookingStatus.Confirmed;
                    providerSpecificData = new Dictionary<string, string>() { { "PaymentText", bookingResponse.Purchase.PaymentData.Description } };
                    message = string.Empty;

                    if (bookingResponse.Purchase.ServiceList != null &&
                        bookingResponse.Purchase.ServiceList.Any() &&
                        bookingResponse.Purchase.ServiceList.First().ContractList != null &&
                        bookingResponse.Purchase.ServiceList.First().ContractList.Contract != null &&
                        bookingResponse.Purchase.ServiceList.First().ContractList.Contract.CommentList.Any())
                    {
                        message = bookingResponse.Purchase.ServiceList.First().ContractList.Contract.CommentList.First().Value;
                    }

                    // Create one booking result per room that was booked
                    foreach (var room in supplierResponse.Rooms)
                    {
                        AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();

                        result.BookingStatus = bookingStatus;
                        result.Message = message;
                        result.ProviderBookingReference = bookingReference;
                        result.ProviderSpecificData = providerSpecificData;
                        result.RoomNumber = room.RoomNumber;

                        bookingResults.Enqueue(result);
                    }
                }
                else
                {
                    // Booking response is NOT valid, but there is NO error in the response
                    bookingStatus = BookingStatus.NotConfirmed;
                    
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the booking response from HotelBeds.");
                    sb.AppendLine("Booking Request Id = " + request.BookingId);
                    sb.AppendLine("Booking Request:");
                    sb.AppendLine(supplierResponse.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Booking Response:");
                    sb.AppendLine(bookingResponse.XmlSerialize());

                    message = sb.ToString();

                    logger.Error(sb.ToString());

                    // Create one booking result per room that failed
                    foreach (var room in supplierResponse.Rooms)
                    {
                        AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();

                        result.BookingStatus = bookingStatus;
                        result.Message = message;
                        result.ProviderBookingReference = bookingReference;
                        result.ProviderSpecificData = providerSpecificData;
                        result.RoomNumber = room.RoomNumber;

                        bookingResults.Enqueue(result);
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from HotelBeds.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierResponse.SupplierRequest.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Booking Response:");
                sb.AppendLine(bookingResponse.XmlSerialize());
                sb.AppendLine();

                throw new SupplierApiException(sb.ToString());
            }
        }
    }
}
