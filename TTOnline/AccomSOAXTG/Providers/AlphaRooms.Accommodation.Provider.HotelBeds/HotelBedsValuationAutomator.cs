﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using ValuationRequest = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest;
using ValuationResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationResponse;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsValuationAutomator :
        IAccommodationValuationAutomatorAsync<IEnumerable<HotelBedsValuationResponse>>
    {
        private const string HotelBedsValuationUrl = "HotelBedsValuationUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotelBedsProviderValuationRequestFactory hotelBedsProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotelBedsValuationAutomator(ILogger logger,
            IAccommodationConfigurationManager configurationManager,
            IHotelBedsProviderValuationRequestFactory hotelBedsSupplierValuationRequestFactory
            ,
            IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.hotelBedsProviderValuationRequestFactory = hotelBedsSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<HotelBedsValuationResponse>> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            List<HotelBedsValuationResponse> supplierRequests = new List<HotelBedsValuationResponse>();
            IEnumerable<HotelBedsValuationResponse> payDirectRequests = null;
            IEnumerable<HotelBedsValuationResponse> postPayRequests = null;

            IEnumerable<HotelBedsValuationResponse> supplierResponses = null;

            try
            {
                // Needs to divide selected results by whether they are Pay Direct or not
                // Pay direct rooms must be grouped by establishment and contract, with each group being valued through in a separate request.
                // Non pay direct (i.e. post payment) rooms can be chained together in a series of valuation requests/responses, and so at this stage only one request per room is required.
                List<KeyValuePair<string, AccommodationProviderValuationRequestRoom>> roomsByContract = new List<KeyValuePair<string, AccommodationProviderValuationRequestRoom>>();

                List<AccommodationProviderValuationRequestRoom> payDirectRooms = request.SelectedRooms.Where(room => room.AvailabilityResult.PaymentModel == PaymentModel.CustomerPayDirect).ToList();

                if (payDirectRooms.Any())
                {
                    // Group by contract & establishment
                    var roomGroups = payDirectRooms.GroupBy(room => new {   Contract = room.AvailabilityResult.Contract.ToUpper(),
                                                                            Establishment = room.AvailabilityResult.EstablishmentEdiCode,
                                                                            AvailToken = room.AvailabilityResult.ProviderSpecificData["AvailabilityToken"]
                                                                        })
                                                                        .Select(group => new { Key = group.Key, Items = group.OrderBy(item => item.RoomNumber) });

                    // Convert the grouped rooms into a flat list for ease of handling
                    foreach (var roomGroup in roomGroups)
                    {
                        foreach (var room in roomGroup.Items)
                        {
                            string key = string.Format(@"{0}\{1}\{2}", roomGroup.Key.Contract, roomGroup.Key.Establishment, room.AvailabilityResult.ProviderSpecificData["AvailabilityToken"]);
                            roomsByContract.Add(new KeyValuePair<string, AccommodationProviderValuationRequestRoom>(key, room));
                        }
                    }

                    // Get a distinct list of keys -> each distinct key will be used to create ONE valuation request.
                    IEnumerable<string> keys = roomsByContract.Select(room => room.Key).Distinct();

                    // Need to throw exception if ANY problems
                    payDirectRequests = Sync.ParallelForEach(keys, (key) => this.hotelBedsProviderValuationRequestFactory.CreateSupplierValuationRequestForPayDirect(request, roomsByContract, key));
                    supplierRequests.AddRange(payDirectRequests);
                }


                // Post Payment Requests
                var postPaymentRooms = request.SelectedRooms.Where(room => room.AvailabilityResult.PaymentModel != PaymentModel.CustomerPayDirect).ToList();

                if (postPaymentRooms.Any())
                {
                    // Create one request for each selected post pay room. Need to throw exception if ANY problems
                    postPayRequests = Sync.ParallelForEach(postPaymentRooms, (postPayRoom) => this.hotelBedsProviderValuationRequestFactory.CreateSupplierValuationRequestForPostPayment(request, postPayRoom));
                    supplierRequests.AddRange(postPayRequests);
                }

                // Debug Code
                if (request.Debugging)
                {
                    List<string> allRequests = new List<string>();

                    foreach (var supplierRequest in supplierRequests)
                    {
                        allRequests.Add(supplierRequest.SupplierRequest.XmlSerialize());
                    }
                }

                // Get the supplier response
                supplierResponses = await GetSupplierValuationResponseAsync(request, supplierRequests);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (supplierResponses != null && supplierResponses.Any())
                {
                    foreach (HotelBedsValuationResponse supplierResponse in supplierResponses)
                    {
                        sb.AppendLine();
                        sb.AppendLine("Request:");
                        if (supplierResponse.SupplierRequest != null)
                        {
                            sb.AppendLine(supplierResponse.SupplierRequest.XmlSerialize());
                        }
                        else
                        {
                            sb.AppendLine("HotelBeds ServiceAddRQ Request Not Present!");
                        }

                        sb.AppendLine();
                        sb.AppendLine("Response:");
                        sb.AppendLine(supplierResponse.SupplierResponse);
                        sb.AppendLine();
                    }
                }

                throw new SupplierApiException(string.Format("HotelBedsValuationAutomator.GetValuationResponseAsync: HotelBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                                Environment.NewLine,
                                                                Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                    ex);
            }

            return supplierResponses;
        }

        private async Task<List<HotelBedsValuationResponse>> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, List<HotelBedsValuationResponse> supplierRequests)
        {
            List<HotelBedsValuationResponse> responses = new List<HotelBedsValuationResponse>();
            string url = request.Provider.Parameters.GetParameterValue(HotelBedsValuationUrl);

            List<Task<IEnumerable<HotelBedsValuationResponse>>> tasks = new List<Task<IEnumerable<HotelBedsValuationResponse>>>();

            // Post Payment rooms will be processed in series, resulting in the final valuation response containing the purchaseToken which will apply to all the post payment rooms
            var postPaymentRequests = supplierRequests.Where(req => !req.IsPayDirect).ToList();

            if (postPaymentRequests.Count() > 0)
            {
                var postPaymentResponses = Task.Run(async() => await GetPostPaymentValuationResponseAsync(request, postPaymentRequests, url));
                tasks.Add(postPaymentResponses);
            }

            // 2. PayDirect rooms have to be processed in their own valuation requests, on their own, one room at a time
            var payDirectRequests = supplierRequests.Where(req => req.IsPayDirect).ToList();

            if (payDirectRequests.Count() > 0)
            {
                foreach (var payDirectRequest in payDirectRequests)
                {
                    var payDirectResponse = Task.Run(async() => await GetPayDirectValuationResponseAsync(request, payDirectRequest, url));
                    tasks.Add(payDirectResponse);
                }
            }

            // Make sure that all valuation requests have completed before continuing
            await Task.WhenAll(tasks);

            // Return all responses
            foreach (var task in tasks)
            {
                responses.AddRange(task.Result);
            }

            return responses;
        }

        private async Task<IEnumerable<HotelBedsValuationResponse>> GetPayDirectValuationResponseAsync(AccommodationProviderValuationRequest request,
                                                                                                        HotelBedsValuationResponse supplierRequest,
                                                                                                        string url)
        {
            List<HotelBedsValuationResponse> responses = new List<HotelBedsValuationResponse>();

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            // Hard code the Sequence Number of this request to zero in the request EchoToken
            supplierRequest.SupplierRequest.EchoToken += "/SeqNo:0";

            string serialisedRequest = supplierRequest.SupplierRequest.XmlSerialize();

            WebScrapeResponse supplierResponse = await webScrapeClient.PostAsync(url, serialisedRequest);

            // Get list of room numbers booked in this request -> this is needed for the logger to ensure a unique file name to avoid threads attempting to write to the same file simultaneously.
            string roomNos = string.Empty;
            foreach (var room in supplierRequest.Rooms)
            {
                roomNos += room.RoomNumber.ToString() + "-";
            }

            roomNos = roomNos.TrimEnd('-');

            outputLogger.LogValuationResponse(request, "HB_Valuation_PayDirect_RoomNos_" + roomNos, ProviderOutputFormat.Xml, supplierResponse);

            HotelBedsValuationResponse response = new HotelBedsValuationResponse();
            response.SupplierRequest = supplierRequest.SupplierRequest;
            response.Rooms = supplierRequest.Rooms;
            response.IsPayDirect = true;
            response.SequenceNo = 0; // Hard code to 0
            response.SupplierResponse = supplierResponse.Value;

            responses.Add(response);

            return responses;
        }

        /// <summary>
        /// Need to chain the individual valuation requests together in serial sequence. Each successful valuation call will return a purchase token that will be used to populate the next vakluation request.
        /// Eventually, the final valuation request will encompass all previous valuation requests and the final purchase token returned will be valid for all rooms in the valuation request.
        /// Any valuation failure should cause the entire process to abort through an exception.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supplierRequests"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private async Task<IEnumerable<HotelBedsValuationResponse>> GetPostPaymentValuationResponseAsync(AccommodationProviderValuationRequest request,
                                                                                                            IEnumerable<HotelBedsValuationResponse> supplierRequests,
                                                                                                            string url)
        {
            int seqNo = 0;
            string purchaseToken = string.Empty;
            List<HotelBedsValuationResponse> responses = new List<HotelBedsValuationResponse>();

            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            // Order rooms by room number to give a predicable room order.
            // IMPORTANT: for posy payment rooms, each supplier request (of type HotelBedsValuationResponse) will only ever have ONE AccommodationProviderValuationRequestRoom object in the Rooms collection
            var orderedRequests = supplierRequests.OrderBy(x => x.Rooms.First().RoomNumber);

            foreach (var supplierRequest in orderedRequests)
            {
                ValuationResponse.ServiceAddRS valuationResponse = null;

                // Set the Sequence Number of this request in the EchoToken of the request
                supplierRequest.SupplierRequest.EchoToken += "/SeqNo:" + seqNo;

                // If multiple chained valuation requests need to be created, then only do this for the second and later requests (SeqNo > 1)
                if (seqNo > 0)
                {
                    supplierRequest.SupplierRequest.PurchaseToken = purchaseToken;
                }

                string serialisedRequest = supplierRequest.SupplierRequest.XmlSerialize();

                WebScrapeResponse supplierResponse = await webScrapeClient.PostAsync(url, serialisedRequest);

                // Need a unique file name for each request that is sent out to avoid "file in use" conflicts.
                outputLogger.LogValuationResponse(request, "HB_Valuation_PostPay_SeqNo_" + seqNo, ProviderOutputFormat.Xml, supplierResponse);

                HotelBedsValuationResponse response = new HotelBedsValuationResponse();
                response.SupplierRequest = supplierRequest.SupplierRequest;
                response.Rooms = supplierRequest.Rooms;
                response.IsPayDirect = false;
                response.SequenceNo = seqNo;
                response.SupplierResponse = supplierResponse.Value;


                // Check that the response does not contain any errors. Throw an exception if any errors are found
                if (!string.IsNullOrWhiteSpace(response.SupplierResponse))
                {
                    valuationResponse = response.SupplierResponse.XmlDeSerialize<ValuationResponse.ServiceAddRS>();

                    // Debug Code
                    if (request.Debugging)
                    {
                        string serialisedReq = supplierRequest.SupplierRequest.XmlSerialize();
                        string serialisedResp = valuationResponse.XmlSerialize();
                    }

                    if (valuationResponse.ErrorList.Any())
                    {
                        // Check that no error was included in the response - if an error is returned, throw an exception
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("HotelBeds Response Error.");
                        sb.AppendLine("HotelBeds returned one or more errors in their valuation response:");
                        sb.AppendLine("Valuation Request:");
                        sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Response Error:");

                        foreach (var error in valuationResponse.ErrorList)
                        {
                            sb.AppendLine(error.Message);
                            sb.AppendLine(error.DetailedMessage);
                            sb.AppendLine();
                        }

                        // Log the error to the event log and throw an exception
                        string message = sb.ToString();
                        logger.Error(message);
                        throw new SupplierApiException(message);
                    }
                }
                else
                {
                    // Response was empty -> log error and throw an exception
                    string message = "HotelBeds Valuation Response is empty. Sequence No. " + seqNo;
                    logger.Error(message);
                    throw new SupplierApiException(message);
                }

                // Extract the purchase token from the response, for use in the next request
                purchaseToken = valuationResponse.Purchase.purchaseToken;

                seqNo++;

                // Add the valuation response to the collection that will be returned
                responses.Add(response);
            }

            return responses;
        }
    }
}
