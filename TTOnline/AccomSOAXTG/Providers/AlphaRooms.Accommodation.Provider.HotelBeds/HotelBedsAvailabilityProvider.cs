﻿using AlphaRooms.Configuration;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;
using AlphaRooms.Utilities;
using Ninject;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<IEnumerable<HotelBedsAvailabilityResponse>> automator;
        private readonly IAccommodationAvailabilityParser<IEnumerable<HotelBedsAvailabilityResponse>> parser;

        public HotelBedsAvailabilityProvider(   IAccommodationAvailabilityAutomatorAsync<IEnumerable<HotelBedsAvailabilityResponse>> automator,
                                                IAccommodationAvailabilityParser<IEnumerable<HotelBedsAvailabilityResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
