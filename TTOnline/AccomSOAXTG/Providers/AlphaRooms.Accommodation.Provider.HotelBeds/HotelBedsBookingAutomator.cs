﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<HotelBedsBookingResponse>>
    {
        private const string HotelBedsBookingUrlPayDirect = "HotelBedsBookingUrlPayDirect";
        private const string HotelBedsBookingUrlPostPayment = "HotelBedsBookingUrlPostPayment";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotelBedsProviderBookingRequestFactory hotelBedsProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotelBedsBookingAutomator(ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            IHotelBedsProviderBookingRequestFactory hotelBedsSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.hotelBedsProviderBookingRequestFactory = hotelBedsSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<HotelBedsBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            IEnumerable<HotelBedsBookingResponse> supplierRequests = null;
            IEnumerable<HotelBedsBookingResponse> responses = new List<HotelBedsBookingResponse>();
            //List<HotelBedsBookingResponse> responses = new List<HotelBedsBookingResponse>();

            try
            {
                // Group all the valued rooms in the request by the ValuationResult.ProviderSpecificData["PurchaseToken"], ordered by room number asc.
                // Note that valuation requests are sent to HotelBeds in room number order, and are returned in the valuation response in the same order.
                // To make a booking for multiple rooms, room occupants MUST be sent in the same order in the Booking request as they are sent/received in the valuation request/response.
                var roomGroups = request.ValuatedRooms.GroupBy(room => room.ValuationResult.ProviderSpecificData["PurchaseToken"] )
                                                      .Select(group => new { Key = group.Key, Items = group.OrderBy(item => item.RoomNumber) });

                // Convert all the items in the groups into a Name/Value collection for ease of implementation
                List<KeyValuePair<string, AccommodationProviderBookingRequestRoom>> roomsByPurchaseToken = new List<KeyValuePair<string, AccommodationProviderBookingRequestRoom>>();

                foreach (var roomGroup in roomGroups)
                {
                    foreach (var room in roomGroup.Items)
                    {
                        string key = roomGroup.Key.ToString();
                        roomsByPurchaseToken.Add(new KeyValuePair<string, AccommodationProviderBookingRequestRoom>(key, room));
                    }
                }

                // Get a distinct list of keys -> each distinct key will be used to create ONE booking request.
                IEnumerable<string> keys = roomsByPurchaseToken.Select(room => room.Key).Distinct();

                // Create booking requests. Need to make sure that any errors here cause an exception to be thrown
                supplierRequests = Sync.ParallelForEach(keys, (key) => this.hotelBedsProviderBookingRequestFactory.CreateSupplierBookingRequest(request, roomsByPurchaseToken, key));

                // Send to HotelBeds and get responses -> note that ANY errors encountered here will cause an exception to be thrown and the booking process will be aborted
                responses = await Async.ParallelForEach(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (responses.Any())
                {
                    foreach (var response in responses)
                    {
                        sb.AppendLine();
                        sb.AppendLine("Request:");
                        sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Response:");
                        XDocument formattedResponse = XDocument.Parse(response.SupplierResponse);
                        sb.AppendLine(formattedResponse.ToString());
                        sb.AppendLine();
                    }
                }

                throw new SupplierApiException(string.Format("HotelBedsBookingAutomator.GetBookingResponseAsync: HotelBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<HotelBedsBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, HotelBedsBookingResponse supplierRequest)
        {
            string url = string.Empty;
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            // IMPORTANT:
            // In theit test environment, HotelBeds only allows pay direct rooms to be booked using the https://testconfirm.interface-xml.com/platform/http/FrontendService url.
            // Post Payment rooms should be booked using http://testapi.interface-xml.com/appservices/http/FrontendService.
            // TODO: I have no idea about which url to use for cancellations!!
            if (supplierRequest.IsPayDirect)
            {
                url = request.Provider.Parameters.GetParameterValue(HotelBedsBookingUrlPayDirect);
            }
            else
            {
                url = request.Provider.Parameters.GetParameterValue(HotelBedsBookingUrlPostPayment);
            }

            string serialisedRequest = supplierRequest.SupplierRequest.XmlSerialize();

            WebScrapeResponse response = await webScrapeClient.PostAsync(url, serialisedRequest);

            // Get list of room numbers booked in this request -> this is needed for the logger
            string roomNos = string.Empty;
            foreach (var room in supplierRequest.Rooms)
            {
                roomNos += room.RoomNumber.ToString() + "-";
            }

            roomNos = roomNos.TrimEnd('-');

            outputLogger.LogBookingResponse(request, "HB_Booking_RoomNos_" + roomNos, ProviderOutputFormat.Xml, response);

            HotelBedsBookingResponse supplierResponse = new HotelBedsBookingResponse()
            {
                SupplierRequest = supplierRequest.SupplierRequest,
                Rooms = supplierRequest.Rooms,
                PurchaseToken = supplierRequest.PurchaseToken,
                IsPayDirect = supplierRequest.IsPayDirect,
                SupplierResponse = response.Value
            };

            return supplierResponse;
        }









        //private HotelBedsBookingResponse GetSupplierBookingResponseSync(AccommodationProviderBookingRequest request, HotelBedsBookingResponse supplierRequest)
        //{
        //    string url = string.Empty;
        //    WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

        //    // IMPORTANT:
        //    // In theit test environment, HotelBeds only allows pay direct rooms to be booked using the https://testconfirm.interface-xml.com/platform/http/FrontendService url.
        //    // Post Payment rooms should be booked using http://testapi.interface-xml.com/appservices/http/FrontendService.
        //    // TODO: I have no idea about which url to use for cancellations!!
        //    if (supplierRequest.IsPayDirect)
        //    {
        //        url = request.Provider.Parameters.GetParameterValue(HotelBedsBookingUrlPayDirect);
        //    }
        //    else
        //    {
        //        url = request.Provider.Parameters.GetParameterValue(HotelBedsBookingUrlPostPayment);
        //    }

        //    string serialisedRequest = supplierRequest.SupplierRequest.XmlSerialize();

        //    WebScrapeResponse response = webScrapeClient.Post(url, serialisedRequest);

        //    // Get list of room numbers booked in this request -> this is needed for the logger
        //    string roomNos = string.Empty;
        //    foreach (var room in supplierRequest.Rooms)
        //    {
        //        roomNos += room.RoomNumber.ToString() + "-";
        //    }

        //    roomNos = roomNos.TrimEnd('-');

        //    outputLogger.LogBookingResponse(request, "HB_Booking_RoomNos_" + roomNos, ProviderOutputFormat.Xml, response);

        //    HotelBedsBookingResponse supplierResponse = new HotelBedsBookingResponse()
        //    {
        //        SupplierRequest = supplierRequest.SupplierRequest,
        //        Rooms = supplierRequest.Rooms,
        //        PurchaseToken = supplierRequest.PurchaseToken,
        //        IsPayDirect = supplierRequest.IsPayDirect,
        //        SupplierResponse = response.Value
        //    };

        //    return supplierResponse;
        //}
    }
}
