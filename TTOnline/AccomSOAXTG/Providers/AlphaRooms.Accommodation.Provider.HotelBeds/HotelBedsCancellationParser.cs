﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationResponse;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using CancellationResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationResponse;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsCancellationParser : IAccommodationCancellationParserAsync<HotelBedsCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public HotelBedsCancellationParser(ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, HotelBedsCancellationResponse response)
        {
            var cancellationResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();

            try
            {
                CancellationResponse.PurchaseCancelRS cancelResponse = response.SupplierResponse.XmlDeSerialize<CancellationResponse.PurchaseCancelRS>();

                if (cancelResponse != null &&
                    cancelResponse.Purchase != null &&
                    cancelResponse.Purchase.Reference != null &&
                    cancelResponse.Purchase.Reference.IncomingOffice != null &&
                    cancelResponse.Purchase.Reference.FileNumber > 0 &&
                    cancelResponse.Purchase.Reference.IncomingOffice.Code > 0 &&
                    cancelResponse.Purchase.Status.Equals("CANCELLED", StringComparison.InvariantCultureIgnoreCase))
                {
                    cancellationResult.CancellationStatus = CancellationStatus.Succeeded;
                    cancellationResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";
                    decimal cancelCost = cancelResponse.Purchase.TotalPrice;
                    cancellationResult.CancellationCost = new Money(cancelCost, cancelResponse.Purchase.Currency.Code);
                    cancellationResult.ProviderCancellationReference = cancelResponse.Purchase.Reference.IncomingOffice.Code.ToString() + "-" + cancelResponse.Purchase.Reference.FileNumber.ToString();
                }
                else
                {
                    StringBuilder sb1 = new StringBuilder();

                    sb1.AppendLine("There was a problem with the cancellation response from HotelBeds.");
                    sb1.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb1.AppendLine("Cancellation Request:");
                    sb1.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb1.AppendLine();
                    sb1.AppendLine("Cancellation Response:");
                    sb1.AppendLine(cancelResponse.XmlSerialize());
                    sb1.AppendLine();

                    StringBuilder sb2 = new StringBuilder();

                    if (cancelResponse != null && cancelResponse.ErrorList != null && cancelResponse.ErrorList.Any())
                    {
                        foreach (var error in cancelResponse.ErrorList)
                        {
                            // Special error message if an "ERR_292, Shopping cart status error!. Only the shopping carts with confirmed status can be cancelled" error message is received. 
                            // This occurs if the booking has already been cancelled. 
                            if (error.DetailedMessage.Contains("ERR_292", StringComparison.CurrentCultureIgnoreCase))
                            {
                                sb2.AppendLine("HotelBeds Booking Cancellation Error:");
                                sb2.AppendLine("The booking has already been cancelled.");
                                sb2.AppendLine("It cannot be cancelled more than once.");
                                sb2.AppendLine();
                            }
                            else
                            {
                                sb2.AppendLine("HotelBeds Booking Cancellation Error:");
                                sb2.AppendLine(error.Message);
                                sb2.AppendLine(error.DetailedMessage);
                                sb2.AppendLine();
                            }
                        }
                    }

                    // Log the error to the event log
                    string message = sb1.ToString() + sb2.ToString();
                    logger.Error(message);

                    cancellationResult.CancellationStatus = CancellationStatus.Failed;

                    // Use the short error message on the cancellation result if any error messages were returned by the provider. This message will be viewed by the user, eg in Admin Panel
                    if (string.IsNullOrWhiteSpace(sb2.ToString()))
                    {
                        cancellationResult.Message = sb1.ToString();
                    }
                    else
                    {
                        cancellationResult.Message = sb2.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from HotelBeds.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(response.SupplierRequest.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                XDocument formattedResponse = XDocument.Parse(response.SupplierResponse);
                sb.AppendLine(formattedResponse.ToString());

                throw new SupplierApiException(sb.ToString());
            }

            cancellationResults.Add(cancellationResult);

            return cancellationResults;
        }
    }
}
