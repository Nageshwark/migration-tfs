﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that HotelBeds accepts and returns XML strings.
    /// </summary>
    public class HotelBedsBookingResponse
    {
        public PurchaseConfirmRQ SupplierRequest { get; set; }
        public IEnumerable<AccommodationProviderBookingRequestRoom> Rooms { get; set; }
        public bool IsPayDirect { get; set; }
        public string PurchaseToken { get; set; }
        public string SupplierResponse { get; set; }
    }
}
