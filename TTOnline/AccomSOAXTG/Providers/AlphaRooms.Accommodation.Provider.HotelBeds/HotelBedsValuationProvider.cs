﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<IEnumerable<HotelBedsValuationResponse>> automator;
        private readonly IAccommodationValuationParserAsync<IEnumerable<HotelBedsValuationResponse>> parser;

        public HotelBedsValuationProvider(  IAccommodationValuationAutomatorAsync<IEnumerable<HotelBedsValuationResponse>> automator,
                                            IAccommodationValuationParserAsync<IEnumerable<HotelBedsValuationResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return await parser.GetValuationResultsAsync(request, responses);
        }
    }
}
