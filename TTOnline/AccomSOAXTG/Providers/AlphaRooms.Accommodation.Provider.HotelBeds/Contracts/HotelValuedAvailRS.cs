﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse
{
    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class HotelValuedAvailRS
    {
        public AuditData AuditData { get; set; }

        public PaginationData PaginationData { get; set; }

        public Error[] ErrorList { get; set; }

        [XmlElement("ServiceHotel")]
        public List<ServiceHotel> ServiceHotels { get; set; }

        [XmlAttribute]
        public int timeToExpiration { get; set; }

        [XmlAttribute]
        public int totalItems { get; set; }

        [XmlAttribute]
        public string echoToken { get; set; }

        public HotelValuedAvailRS()
        {
            ErrorList = new Error[] { };
            ServiceHotels = new List<ServiceHotel>();
        }
    }

    [Serializable]
    public class AuditData
    {
        public int ProcessTime { get; set; }

        public string Timestamp { get; set; }

        public string RequestHost { get; set; }

        public string ServerName { get; set; }

        public string ServerId { get; set; }

        public string SchemaRelease { get; set; }

        public string HydraCoreRelease { get; set; }

        public string HydraEnumerationsRelease { get; set; }

        public int MerlinRelease { get; set; }
    }

    [Serializable]
    public class PaginationData
    {
        [XmlAttribute]
        public int currentPage { get; set; }

        [XmlAttribute]
        public int totalPages { get; set; }
    }

    [Serializable]
    public class Error
    {
        public string Code { get; set; }

        public long Timestamp { get; set; }

        public string Message { get; set; }

        public string DetailedMessage { get; set; }
    }

    [Serializable]
    public class ServiceHotel
    {
        public string DirectPayment { get; set; }

        public ContractList ContractList { get; set; }

        public DateFrom DateFrom { get; set; }

        public DateTo DateTo { get; set; }

        public Currency Currency { get; set; }

        public ServiceExtraInfoList ServiceExtraInfoList { get; set; }

        public string PackageRate { get; set; }

        public string TravelAgent { get; set; }

        public HotelInfo HotelInfo { get; set; }

        [XmlElement("AvailableRoom")]
        public AvailableRoom[] AvailableRooms { get; set; }

        [XmlAttribute]
        public string availToken { get; set; }
    }

    [Serializable]
    public class ContractList
    {
        public Contract Contract { get; set; }
    }

    [Serializable]
    public class Contract
    {
        public string Name { get; set; }

        public IncomingOffice IncomingOffice { get; set; }

        public Classification Classification { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class Classification
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class DateFrom
    {
        [XmlAttribute("date")]
        public int Date { get; set; }
    }

    [Serializable]
    public class DateTo
    {
        [XmlAttribute("date")]
        public int Date { get; set; }
    }

    [Serializable]
    public class Currency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class ServiceExtraInfoList
    {
        public ExtendedData ExtendedData { get; set; }
    }

    [Serializable]
    public class ExtendedData
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ProductHotel : HotelInfo
    {
    }

    [Serializable]
    [XmlInclude(typeof(ProductHotel))]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class HotelInfo
    {
        public int Code { get; set; }

        public string Name { get; set; }

        [XmlArrayItemAttribute("Image", IsNullable = false)]
        public InfoImage[] Images { get; set; }

        public HotelInfoCategory Category { get; set; }

        public HotelInfoDestination Destination { get; set; }

        public ChildAge ChildAge { get; set; }

        public Position Position { get; set; }
    }

    [Serializable]
    public class InfoImage
    {
        public string Type { get; set; }

        public int Order { get; set; }

        public int VisualizationOrder { get; set; }

        public string Url { get; set; }
    }

    [Serializable]
    public class HotelInfoCategory
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("shortname")]
        public string Shortname { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class HotelInfoDestination
    {
        public string Name { get; set; }

        public List<Zone> ZoneList { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }
    
    [Serializable]
    public class Zone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public int Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class ChildAge
    {
        [XmlAttribute("ageFrom")]
        public int AgeFrom { get; set; }

        [XmlAttribute("ageTo")]
        public int AgeTo { get; set; }
    }

    [Serializable]
    public class Position
    {
        [XmlAttribute("latitude")]
        public decimal Latitude { get; set; }

        [XmlAttribute("longitude")]
        public decimal Longitude { get; set; }
    }

    [Serializable]
    public class AvailableRoom
    {
        public HotelOccupancy HotelOccupancy { get; set; }

        public HotelRoom HotelRoom { get; set; }
    }

    [Serializable]
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }

        public Occupancy Occupancy { get; set; }
    }

    [Serializable]
    public class Occupancy
    {
        public int AdultCount { get; set; }

        public int ChildCount { get; set; }
    }

    [Serializable]
    public class HotelRoom
    {
        public RoomBoard Board { get; set; }

        public RoomType RoomType { get; set; }

        public string ProductType { get; set; }

        public int FreeNights { get; set; }

        public bool FreeNightsSpecified { get; set; }

        public RoomPrice Price { get; set; }

        public RoomRate Rate { get; set; }

        [XmlAttribute]
        public string SHRUI { get; set; }

        [XmlAttribute("availCount")]
        public int AvailCount { get; set; }

        [XmlAttribute("onRequest")]
        public string OnRequest { get; set; }
    }

    [Serializable]
    public class RoomBoard
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("shortname")]
        public string Shortname { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class RoomType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("characteristic")]
        public string Characteristic { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class RoomPrice
    {
        public decimal Amount { get; set; }

        public SellingPrice SellingPrice { get; set; }

        public decimal NetPrice { get; set; }

        [XmlIgnoreAttribute]
        public bool NetPriceSpecified { get; set; }

        public decimal Commission { get; set; }

        [XmlIgnoreAttribute]
        public bool CommissionSpecified { get; set; }
    }

    [Serializable]
    public class SellingPrice
    {
        [XmlAttribute("mandatory")]
        public string Mandatory { get; set; }

        [XmlTextAttribute]
        public decimal Value { get; set; }
    }

    [Serializable]
    public class RoomRate
    {
        public string Name { get; set; }

        public string Description { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    public class Comparer : IEqualityComparer<HotelRoom>
    {
        public bool Equals(HotelRoom x, HotelRoom y)
        {
            return x.SHRUI == y.SHRUI;
        }

        public int GetHashCode(HotelRoom obj)
        {
            unchecked  // overflow is fine
            {
                int hash = 17;
                hash = hash * 23 + (obj.SHRUI ?? "").GetHashCode();
                hash = hash * 23 + (obj.RoomType.Value ?? "").GetHashCode();
                hash = hash * 23 + (obj.Board.Value ?? "").GetHashCode();
                return hash;
            }
        }
    }
}
