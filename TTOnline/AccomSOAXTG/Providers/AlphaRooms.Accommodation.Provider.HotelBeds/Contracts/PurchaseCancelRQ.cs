﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationRequest
{
    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class PurchaseCancelRQ
    {
        public string Language { get; set; }

        public Credentials Credentials { get; set; }

        public PurchaseReference PurchaseReference { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }
    }

    [Serializable]
    public class Credentials
    {
        public string User { get; set; }

        public string Password { get; set; }
    }

    [Serializable]
    public class PurchaseReference
    {
        public int FileNumber { get; set; }

        public IncomingOffice IncomingOffice { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public ushort Code { get; set; }
    }
}
