﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest
{
    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class ServiceAddRQ
    {
        [XmlAttribute("purchaseToken")]
        public string PurchaseToken { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }

        public string Language { get; set; }

        [XmlAttribute("echoToken")]
        public string EchoToken { get; set; }

        public Credentials Credentials { get; set; }

        public Service Service { get; set; }

        public ServiceAddRQ()
        {
            Service = new ServiceHotel();
            Credentials = new Credentials();
        }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ServiceHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ServiceHotel : Service
    {
    }

    [Serializable]
    public class Credentials
    {
        public string User { get; set; }

        public string Password { get; set; }
    }

    [Serializable]
    [XmlInclude(typeof(ServiceHotel))]
    public abstract class Service
    {
        public ContractList ContractList { get; set; }

        public DateFrom DateFrom { get; set; }

        public DateTo DateTo { get; set; }

        public HotelInfo HotelInfo { get; set; }

        // Note: the XmlElement attribute removes the <AvailableRooms> element from the xml output, as this is not used in the ServiceAdd request, even though one ServiceAdd request can handle multiple Available Rooms!
        [XmlElement("AvailableRoom")]
        public List<AvailableRoom> AvailableRooms { get; set; }

        [XmlAttribute]
        public string availToken { get; set; }

        public Service()
        {
            HotelInfo = new ProductHotel();
            DateFrom = new DateFrom();
            DateTo = new DateTo();

            AvailableRooms = new List<AvailableRoom>();
            AvailableRooms.Add(new AvailableRoom()); 
        }
    }

    [Serializable]
    public class ContractList
    {
        public Contract Contract { get; set; }
    }

    [Serializable]
    public class Contract
    {
        public string Name { get; set; }

        public IncomingOffice IncomingOffice { get; set; }

        public Classification Classification { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class Classification
    {
        [XmlAttribute]
        public string code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class DateFrom
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class DateTo
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ProductHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ProductHotel : HotelInfo
    {
    }

    [Serializable]
    [XmlInclude(typeof(ProductHotel))]
    public class HotelInfo
    {
        public string Code { get; set; }

        public Destination Destination { get; set; }

        public HotelInfo()
        {
            Destination = new Destination();
        }
    }

    [Serializable]
    public class Destination
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class AvailableRoom
    {
        public HotelOccupancy HotelOccupancy { get; set; }

        public HotelRoom HotelRoom { get; set; }

        public AvailableRoom()
        {
            HotelOccupancy = new HotelOccupancy();
            HotelRoom = new HotelRoom();
        }
    }

    [Serializable]
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }

        public Occupancy Occupancy { get; set; }

        public HotelOccupancy()
        {
            Occupancy = new Occupancy();
        }
    }

    [Serializable]
    public class Occupancy
    {
        public int AdultCount { get; set; }

        public int ChildCount { get; set; }

        public List<Customer> GuestList { get; set; }
    }

    [Serializable]
    public class Customer
    {
        public int Age { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class HotelRoom
    {
        public RoomBoard Board { get; set; }

        public RoomType RoomType { get; set; }

        public HotelRoom()
        {
            Board = new RoomBoard();
            RoomType = new RoomType();
        }
    }

    [Serializable]
    public class RoomBoard
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class RoomType
    {
        [XmlAttribute("characteristic")]
        public string Characteristic { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
