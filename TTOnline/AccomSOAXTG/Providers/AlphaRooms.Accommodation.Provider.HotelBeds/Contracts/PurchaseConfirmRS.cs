﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingResponse
{
    [Serializable]
    [XmlType(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class PurchaseConfirmRS
    {
        public AuditData AuditData { get; set; }

        public Purchase Purchase { get; set; }

        public List<Error> ErrorList { get; set; }

        [XmlAttribute("echoToken")]
        public string EchoToken { get; set; }
    }

    [Serializable]
    public class Error
    {
        public string Code { get; set; }

        public long Timestamp { get; set; }

        public string Message { get; set; }

        public string DetailedMessage { get; set; }
    }

    [Serializable]
    public class AuditData
    {
        public int ProcessTime { get; set; }

        public string Timestamp { get; set; }

        public string RequestHost { get; set; }

        public string ServerName { get; set; }

        public string ServerId { get; set; }

        public string SchemaRelease { get; set; }

        public string HydraCoreRelease { get; set; }

        public string HydraEnumerationsRelease { get; set; }

        public string MerlinRelease { get; set; }
    }

    [Serializable]
    public class Purchase
    {
        public Reference Reference { get; set; }

        public string Status { get; set; }

        public Agency Agency { get; set; }

        public string Language { get; set; }

        public CreationDate CreationDate { get; set; }

        public string CreationUser { get; set; }

        public Holder Holder { get; set; }

        public string AgencyReference { get; set; }

        public List<Service> ServiceList { get; set; }

        public Currency Currency { get; set; }

        public PaymentData PaymentData { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal PendingAmount { get; set; }

        [XmlAttribute("PurchaseToken")]
        public string purchaseToken { get; set; }

        [XmlAttribute("timeToExpiration")]
        public int TimeToExpiration { get; set; }
    }

    [Serializable]
    public class Reference
    {
        public int FileNumber { get; set; }

        public IncomingOffice IncomingOffice { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class Agency
    {
        public string Code { get; set; }

        public string Branch { get; set; }
    }

    [Serializable]
    public class CreationDate
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class Holder
    {
        public int Age { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    //[Serializable]
    //public class PurchaseConfirmRSPurchaseServiceList
    //{
    //    public Service Service { get; set; }
    //}

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ServiceHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ServiceHotel : Service
    {
    }

    [Serializable]
    [XmlInclude(typeof(ServiceHotel))]
    public abstract class Service
    {
        public ServiceReference Reference { get; set; }

        public string Status { get; set; }

        public string DirectPayment { get; set; }

        public ContractList ContractList { get; set; }

        public ServiceSupplier Supplier { get; set; }

        [XmlArrayItem("Comment", IsNullable = false)]
        public ServiceComment[] CommentList { get; set; }

        public ServiceDateFrom DateFrom { get; set; }

        public ServiceDateTo DateTo { get; set; }

        public ServiceCurrency Currency { get; set; }

        public List<Card> AcceptedCardTypes { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal NetPrice { get; set; }

        public decimal Commission { get; set; }

        public SellingPrice SellingPrice { get; set; }

        public List<SupplementListPrice> SupplementList { get; set; }

        public AdditionalCostList AdditionalCostList { get; set; }

        [XmlArrayItem("ModificationPolicy", IsNullable = false)]
        public List<string> ModificationPolicyList { get; set; }

        public HotelInfo HotelInfo { get; set; }

        public AvailableRoom AvailableRoom { get; set; }

        [XmlAttribute()]
        public string SPUI { get; set; }
    }

    [Serializable]
    public class ServiceReference
    {
        public string FileNumber { get; set; }

        public ServiceReferenceIncomingOffice IncomingOffice { get; set; }
    }

    [Serializable]
    public class ServiceReferenceIncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class ContractList
    {
        public Contract Contract { get; set; }
    }

    [Serializable]
    public class Contract
    {
        public string Name { get; set; }

        public ContractIncomingOffice IncomingOffice { get; set; }

        [XmlArrayItem("Comment", IsNullable = false)]
        public List<ContractComment> CommentList { get; set; }
    }

    [Serializable]
    public class ContractIncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    //[Serializable]
    //public class ContractCommentList
    //{
    //    public ContractComment Comment { get; set; }
    //}

    [Serializable]
    public class ContractComment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class ServiceSupplier
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("vatNumber")]
        public string VATNumber { get; set; }
    }

    [Serializable]
    public class ServiceComment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class ServiceDateFrom
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class ServiceDateTo
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class ServiceCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class Card
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class SellingPrice
    {
        [XmlAttribute("mandatory")]
        public string Mandatory { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    //[Serializable]
    //public class SupplementList
    //{
    //    public SupplementListPrice Price { get; set; }
    //}

    [Serializable]
    public class SupplementListPrice
    {
        public decimal Amount { get; set; }

        public SupplementPriceDateFrom DateTimeFrom { get; set; }

        public SupplementPriceDateTo DateTimeTo { get; set; }

        public string Description { get; set; }

        [XmlAttribute("unitCount")]
        public int UnitCount { get; set; }

        [XmlAttribute("paxCount")]
        public int PaxCount { get; set; }
    }

    [Serializable]
    public class SupplementPriceDateFrom
    {
        [XmlAttribute("date")]
        public string DateFrom { get; set; }
    }

    [Serializable]
    public class SupplementPriceDateTo
    {
        [XmlAttribute("date")]
        public string DateTo { get; set; }
    }

    [Serializable]
    public class AdditionalCostList
    {
        public AdditionalCostListCurrency Currency { get; set; }

        public decimal PvpEquivalent { get; set; }

        [XmlElementAttribute("AdditionalCost")]
        public List<AdditionalCost> AdditionalCost { get; set; }
    }

    [Serializable]
    public class AdditionalCostListCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class AdditionalCost
    {
        public AdditionalCostPrice Price { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class AdditionalCostPrice
    {
        public decimal Amount { get; set; }
    }

    // Will deserialise into this element: <HotelInfo xsi:type="ProductHotel">
    // The type specified in the xsi:type attribute MUST derive from the top level base class, which is HotelInfo in this case.
    // xsi:type simply specifies a subtype of the supertype (the supertype is the class name that is specified in the element name, i.e. <HotelInfo ... >
    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ProductHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ProductHotel : HotelInfo
    {
    }

    [Serializable]
    [XmlInclude(typeof(ProductHotel))]
    public class HotelInfo
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public HotelInfoCategory Category { get; set; }

        public Destination Destination { get; set; }
    }

    [Serializable]
    public class HotelInfoCategory
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class Destination
    {
        public string Name { get; set; }

        public ZoneList ZoneList { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class ZoneList
    {
        public Zone Zone { get; set; }
    }

    [Serializable]
    public class Zone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public int Code { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class AvailableRoom
    {
        public HotelOccupancy HotelOccupancy { get; set; }

        public HotelRoom HotelRoom { get; set; }
    }

    [Serializable]
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }

        public Occupancy Occupancy { get; set; }
    }

    [Serializable]
    public class Occupancy
    {
        public int AdultCount { get; set; }

        public int ChildCount { get; set; }

        [XmlArrayItem("Customer", IsNullable = false)]
        public List<Customer> GuestList { get; set; }
    }

    [Serializable]
    public class Customer
    {
        public int CustomerId { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class HotelRoom
    {
        public RoomBoard Board { get; set; }

        public RoomType RoomType { get; set; }

        public HotelRoomPrice Price { get; set; }

        public List<CancellationPolicy> CancellationPolicies { get; set; }

        [XmlArrayItem("ExtendedData", IsNullable = false)]
        public HotelRoomExtendedData[] HotelRoomExtraInfo { get; set; }

        [XmlAttribute()]
        public string SHRUI { get; set; }

        [XmlAttribute("availCount")]
        public int AvailCount { get; set; }

        [XmlAttribute("status")]
        public string Status { get; set; }
    }

    [Serializable]
    public class RoomBoard
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class RoomType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("characteristic")]
        public string Characteristic { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    [Serializable]
    public class HotelRoomPrice
    {
        public decimal Amount { get; set; }
    }

    [Serializable]
    public class CancellationPolicy
    {
        [XmlAttribute("amount")]
        public decimal Amount { get; set; }

        [XmlAttribute("dateFrom")]
        public string DateFrom { get; set; }

        [XmlAttribute("time")]
        public string Time { get; set; }
    }

    [Serializable]
    public class HotelRoomExtendedData
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }

    [Serializable]
    public class Currency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class PaymentData
    {
        public PaymentType PaymentType { get; set; }

        public InvoiceCompany InvoiceCompany { get; set; }

        public string Description { get; set; }
    }

    [Serializable]
    public class PaymentType
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class InvoiceCompany
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string RegistrationNumber { get; set; }
    }
}
