﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest
{
    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class HotelValuedAvailRQ
    {
        public string Language { get; set; }

        public Credentials Credentials { get; set; }

        public PaginationData PaginationData { get; set; }

        public CheckInDate CheckInDate { get; set; }

        public CheckOutDate CheckOutDate { get; set; }

        public ExtraParamList ExtraParamList { get; set; }

        public Destination Destination { get; set; }

        public HotelCodeList HotelCodeList { get; set; }

        public string ShowDirectPayment { get; set; }

        public List<HotelOccupancy> OccupancyList { get; set; }

        [XmlAttribute]
        public string echoToken { get; set; }

        [XmlAttribute]
        public string sessionId { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }

        public HotelValuedAvailRQ()
        {
            this.Credentials = new Credentials();
            this.PaginationData = new PaginationData();
            this.CheckInDate = new CheckInDate();
            this.CheckOutDate = new CheckOutDate();
            this.OccupancyList = new List<HotelOccupancy>();
            this.Language = "ENG";      // default language
        }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class Credentials
    {
        public string User { get; set; }

        public string Password { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class PaginationData
    {
        [XmlAttribute("pageNumber")]
        public int PageNumber { get; set; }

        [XmlAttribute("itemsPerPage")]
        public int ItemsPerPage { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class CheckInDate
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class CheckOutDate
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class Destination
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        public List<Zone> ZoneList { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class Zone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class HotelCodeList
    {
        [XmlElementAttribute("ProductCode")]
        public List<string> ProductCode { get; set; }

        [XmlAttribute("withinResults")]
        public string WithinResults { get; set; }

        // Default constructor
        public HotelCodeList ()
        {
            ProductCode = new List<string>();
        }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }

        public Occupancy Occupancy { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class Occupancy
    {
        public int AdultCount { get; set; }

        public int ChildCount { get; set; }

        public List<Customer> GuestList { get; set; }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class Customer
    {
        public int Age { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }


    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ExtraParamList
    {
        public ExtendedData ExtendedData { get; set; }

        public ExtraParamList()
        {
            this.ExtendedData = new ExtendedData();
        }
    }

    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public partial class ExtendedData
    {
        public string Name { get; set; }

        public string Value { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
