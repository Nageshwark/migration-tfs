﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationResponse
{    
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class ServiceAddRS
    {
        public AuditData AuditData { get; set; }

        public Purchase Purchase { get; set; }

        public List<Error> ErrorList { get; set; }

        [XmlAttribute("echoToken")]
        public string EchoToken { get; set; }
    }

    [Serializable]
    public class Error
    {
        public string Code { get; set; }

        public string Timestamp { get; set; }

        public string Message { get; set; }

        public string DetailedMessage { get; set; }
    }

    [Serializable]
    public class AuditData
    {
        public int ProcessTime { get; set; }

        public string Timestamp { get; set; }

        public string RequestHost { get; set; }

        public string ServerName { get; set; }

        public string ServerId { get; set; }

        public string SchemaRelease { get; set; }

        public string HydraCoreRelease { get; set; }

        public string HydraEnumerationsRelease { get; set; }

        public string MerlinRelease { get; set; }
    }

    [Serializable]
    public class Purchase
    {
        public string Status { get; set; }

        public PurchaseAgency Agency { get; set; }

        public string Language { get; set; }

        public string CreationUser { get; set; }

        //public ServiceList ServiceList { get; set; }

        [XmlArrayItemAttribute("Service", IsNullable = false)]
        public List<Service> ServiceList { get; set; }

        public PurchaseCurrency Currency { get; set; }

        public PaymentData PaymentData { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal PendingAmount { get; set; }

        [XmlAttribute]
        public string purchaseToken { get; set; }

        [XmlAttribute]
        public int timeToExpiration { get; set; }
    }

    [Serializable]
    public class PurchaseAgency
    {
        public int Code { get; set; }

        public int Branch { get; set; }
    }

    //[Serializable]
    //public class ServiceList
    //{
    //    [XmlElement("Service")]
    //    public List<Service> Service { get; set; }

    //}

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ServiceHotel : Service
    {
    }

    [Serializable]
    [XmlInclude(typeof(ServiceHotel))]
    public class Service
    {
        public string Status { get; set; }

        public string DirectPayment { get; set; }

        public ContractList ContractList { get; set; }

        public ServiceSupplier Supplier { get; set; }

        public DateFrom DateFrom { get; set; }

        public DateTo DateTo { get; set; }

        public ServiceCurrency Currency { get; set; }

        [XmlArrayItemAttribute("Card", IsNullable = false)]
        public Card[] AcceptedCardTypes { get; set; }

        public decimal TotalAmount { get; set; }

        public string NetPrice { get; set; }

        public string Commission { get; set; }

        public SellingPrice SellingPrice { get; set; }

        public DiscountList DiscountList { get; set; }

        public SupplementList SupplementList { get; set; }

        public AdditionalCostList AdditionalCostList { get; set; }

        [XmlArrayItemAttribute("ModificationPolicy", IsNullable = false)]
        public string[] ModificationPolicyList { get; set; }

        public HotelInfo HotelInfo { get; set; }

        [XmlElement("AvailableRoom")]
        public List<AvailableRoom> AvailableRooms { get; set; }

        [XmlAttribute]
        public string SPUI { get; set; }
    }

    [Serializable]
    public class ContractList
    {
        public Contract Contract { get; set; }
    }

    [Serializable]
    public class Contract
    {
        public string Name { get; set; }

        public IncomingOffice IncomingOffice { get; set; }

        public CommentList CommentList { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class CommentList
    {
        public Comment Comment { get; set; }
    }

    [Serializable]
    public class Comment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class ServiceSupplier
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("vatNumber")]
        public string VatNumber { get; set; }
    }

    [Serializable]
    public class DateFrom
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class DateTo
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class ServiceCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class Card
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class SellingPrice
    {
        [XmlAttribute("mandatory")]
        public string Mandatory { get; set; }

        [XmlTextAttribute]
        public decimal Value { get; set; }
    }

    [Serializable]
    public class DiscountList
    {
        public DiscountListPrice Price { get; set; }
    }

    [Serializable]
    public class DiscountListPrice
    {
        public decimal Amount { get; set; }

        public DiscountListPriceDateTimeFrom DateTimeFrom { get; set; }

        public DiscountListPriceDateTimeTo DateTimeTo { get; set; }

        public string Description { get; set; }

        [XmlAttribute("unitCount")]
        public int UnitCount { get; set; }

        [XmlAttribute("paxCount")]
        public int PaxCount { get; set; }
    }

    [Serializable]
    public class DiscountListPriceDateTimeFrom
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class DiscountListPriceDateTimeTo
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class AdditionalCostList
    {
        public AdditionalCostListCurrency Currency { get; set; }

        public string PvpEquivalent { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AdditionalCost")]
        public AdditionalCost[] AdditionalCosts { get; set; }
    }

    [Serializable]
    public class AdditionalCostListCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class AdditionalCost
    {
        public AdditionalCostPrice Price { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class AdditionalCostPrice
    {
        public decimal Amount { get; set; }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ProductHotel : HotelInfo
    {
    }

    [Serializable]
    [XmlInclude(typeof(ProductHotel))]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class HotelInfo
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public InfoCategory Category { get; set; }

        public Destination Destination { get; set; }
    }

    [Serializable]
    public class InfoCategory
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class Destination
    {
        public string Name { get; set; }

        public ZoneList ZoneList { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class ZoneList
    {
        public Zone Zone { get; set; }
    }

    [Serializable]
    public class Zone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public int Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class AvailableRoom
    {
        public RoomOccupancy HotelOccupancy { get; set; }

        public HotelRoom HotelRoom { get; set; }
    }

    [Serializable]
    public class RoomOccupancy
    {
        public int RoomCount { get; set; }

        public Occupancy Occupancy { get; set; }
    }

    [Serializable]
    public class Occupancy
    {
        public int AdultCount { get; set; }

        public int ChildCount { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Customer", IsNullable = false)]
        public Customer[] GuestList { get; set; }
    }

    [Serializable]
    public class Customer
    {
        public int CustomerId { get; set; }

        public int Age { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("isTravelAgent")]
        public string IsTravelAgent { get; set; }
    }

    [Serializable]
    public class HotelRoom
    {
        public RoomBoard Board { get; set; }

        public RoomType RoomType { get; set; }

        public string ProductType { get; set; }

        public RoomPrice Price { get; set; }

        public List<CancellationPolicy> CancellationPolicies { get; set; }

        public RoomTaxList TaxList { get; set; }

        [XmlAttribute]
        public string SHRUI { get; set; }

        [XmlAttribute("availCount")]
        public int AvailCount { get; set; }

        [XmlAttribute("status")]
        public string Status { get; set; }
    }

    [Serializable]
    public class RoomBoard
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class RoomType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("characteristic")]
        public string Characteristic { get; set; }

        [XmlTextAttribute]
        public string Value { get; set; }
    }

    [Serializable]
    public class RoomPrice
    {
        public decimal Amount { get; set; }
    }

    //[Serializable]
    //public class CancellationPolicies
    //{
    //    public CancellationPolicy CancellationPolicy { get; set; }
    //}

    [Serializable]
    public class CancellationPolicy
    {
        [XmlAttribute("amount")]
        public decimal Amount { get; set; }

        [XmlAttribute("dateFrom")]
        public string DateFrom { get; set; }

        [XmlAttribute("time")]
        public string Time { get; set; }
    }

    [Serializable]
    public class RoomTaxList
    {
        [XmlAttribute]
        public string allIncluded { get; set; }
    }

    [Serializable]
    public class PurchaseCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class PaymentData
    {
        public PaymentType PaymentType { get; set; }

        public object InvoiceCompany { get; set; }

        public string Description { get; set; }
    }

    [Serializable]
    public class PaymentType
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class SupplementList
    {
        [XmlElement("Price")]
        public List<SupplementListPrice> SupplementListPrices { get; set; }
    }

    [Serializable]
    public class SupplementListPrice
    {
        public decimal Amount { get; set; }

        public SupplementDateTimeFrom DateTimeFrom { get; set; }

        public SupplementDateTimeTo DateTimeTo { get; set; }

        public string Description { get; set; }

        [XmlAttribute("unitCount")]
        public int UnitCount { get; set; }

        [XmlAttribute("paxCount")]
        public int PaxCount { get; set; }
    }

    [Serializable]
    public class SupplementDateTimeFrom
    {
        [XmlAttribute("date")]
        public int Date { get; set; }
    }

    [Serializable]
    public class SupplementDateTimeTo
    {
        [XmlAttribute("date")]
        public int Date { get; set; }
    }
}
