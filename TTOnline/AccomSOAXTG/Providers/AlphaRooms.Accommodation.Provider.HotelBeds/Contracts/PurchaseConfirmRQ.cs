﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest
{
    /// <summary>
    /// Note that one PurchaseConfirmRQ can only contains one purchaseToken value.
    /// The purchaseToken is obtained from the <Purchase> element in the ServiceAddRS response, and each ServiceAddRS response will only have a single purchaseToken.
    /// Therefore, if we are sending one ServiceAddRQ per room, then each room would have to be booked separately, using a series of PurchaseConfirmRQ requests (one per room).
    /// </summary>
    /// 
    [Serializable]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class PurchaseConfirmRQ
    {
        public string Language { get; set; }

        public Credentials Credentials { get; set; }

        public ConfirmationData ConfirmationData { get; set; }

        [XmlAttribute("echoToken")]
        public string EchoToken { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }

        public PurchaseConfirmRQ()
        {
            this.Credentials = new Credentials();
            this.ConfirmationData = new ConfirmationData();
        }
    }

    [Serializable]
    public class Credentials
    {
        public string User { get; set; }

        public string Password { get; set; }
    }

    [Serializable]
    public class ConfirmationData
    {
        public Holder Holder { get; set; }

        public string AgencyReference { get; set; }

        public List<ServiceData> ConfirmationServiceDataList { get; set; }

        [XmlAttribute("purchaseToken")]
        public string PurchaseToken { get; set; }

        public PaymentData PaymentData { get; set; }

        public ConfirmationData()
        {
            this.Holder = new Holder();
            this.ConfirmationServiceDataList = new List<ServiceData>();
        }
    }

    [Serializable]
    public class Holder
    {
        public string CustomerId { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class ServiceDataList
    {
        public List<ConfirmationServiceDataHotel> ServiceData { get; set; }

        public ServiceDataList()
        {
            this.ServiceData = new List<ConfirmationServiceDataHotel>();
        }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ConfirmationServiceDataHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ConfirmationServiceDataHotel : ServiceData
    {
    }

    [Serializable]
    [XmlInclude(typeof(ConfirmationServiceDataHotel))]
    public abstract class ServiceData
    {
        [XmlArrayItem("Customer", IsNullable = false)]
        public List<Customer> CustomerList { get; set; }

        public PaymentData PaymentData { get; set; }

        [XmlArrayItem("Comment", IsNullable = true)]
        public List<Comment> CommentList { get; set; }

        [XmlAttribute()]
        public string SPUI { get; set; }

        public ServiceData()
        {
            this.CustomerList = new List<Customer>();
        }
    }

    [Serializable]
    public class Customer
    {
        public int CustomerId { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class Comment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlTextAttribute()]
        public string Value { get; set; }
    }

    [Serializable]
    public class PaymentData
    {
        public PaymentCard PaymentCard { get; set; }

        public PaymentData()
        {
            this.PaymentCard = new PaymentCard();
        }
    }

    [Serializable]
    public class PaymentCard
    {
        public string CardType { get; set; }

        public string CardNumber { get; set; }

        public string CardHolderName { get; set; }

        public string ExpiryDate { get; set; }

        public string CardCVC { get; set; }
    }
}
