﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationResponse
{
    [Serializable]
    [XmlType(AnonymousType = true, Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages", IsNullable = false)]
    public class PurchaseCancelRS
    {
        public AuditData AuditData { get; set; }

        public Purchase Purchase { get; set; }

        public Currency Currency { get; set; }

        public decimal Amount { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        public List<Error> ErrorList { get; set; }

        [XmlAttribute("echoToken")]
        public string EchoToken { get; set; }
    }

    [Serializable]
    public class Error
    {
        public string Code { get; set; }

        public long Timestamp { get; set; }

        public string Message { get; set; }

        public string DetailedMessage { get; set; }
    }

    [Serializable]
    public class AuditData
    {
        public int ProcessTime { get; set; }

        public string Timestamp { get; set; }

        public string RequestHost { get; set; }

        public string ServerName { get; set; }

        public string ServerId { get; set; }

        public string SchemaRelease { get; set; }

        public string HydraCoreRelease { get; set; }

        public string HydraEnumerationsRelease { get; set; }

        public string MerlinRelease { get; set; }
    }

    [Serializable]
    public class Purchase
    {
        public Reference Reference { get; set; }

        public string Status { get; set; }

        public Agency Agency { get; set; }

        public string Language { get; set; }

        public string CreationDate { get; set; }

        public string CreationUser { get; set; }

        public Holder Holder { get; set; }

        public string AgencyReference { get; set; }

        public List<Service> ServiceList { get; set; }

        public PurchaseCurrency Currency { get; set; }

        public PaymentData PaymentData { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal PendingAmount { get; set; }

        [XmlAttribute("purchaseToken")]
        public string PurchaseToken { get; set; }

        [XmlAttribute("timeToExpiration")]
        public int TimeToExpiration { get; set; }
    }

    [Serializable]
    public class Reference
    {
        public int FileNumber { get; set; }

        public ReferenceIncomingOffice IncomingOffice { get; set; }
    }

    [Serializable]
    public class ReferenceIncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class Agency
    {
        public int Code { get; set; }

        public string Branch { get; set; }
    }

    [Serializable]
    public class Holder
    {
        public int Age { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    [XmlType("ServiceHotel", Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ServiceHotel : Service
    {
    }

    [Serializable]
    [XmlInclude(typeof(ServiceHotel))]
    public abstract class Service
    {
        public ServiceReference Reference { get; set; }

        public string Status { get; set; }
    
        public string DirectPayment { get; set; }

        public ContractList ContractList { get; set; }
        
        public Supplier Supplier { get; set; }
        
        public ServiceCommentList CommentList { get; set; }
        
        public DateFrom DateFrom { get; set; }
        
        public DateTo DateTo { get; set; }
        
        public ServiceCurrency Currency { get; set; }
        
        public decimal TotalAmount { get; set; }
        
        public AdditionalCostList AdditionalCostList { get; set; }
        
        [XmlArrayItem("ModificationPolicy", IsNullable = false)]
        public List<string> ModificationPolicyList { get; set; }
        
        public HotelInfo HotelInfo { get; set; }
        
        public AvailableRoom AvailableRoom { get; set; }

        [XmlAttribute]
        public string SPUI { get; set; }
    }

    [Serializable]
    public class ServiceReference
    {
        public string FileNumber { get; set; }

        public IncomingOffice IncomingOffice { get; set; }
    }

    [Serializable]
    public class IncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class ContractList
    {
        public Contract Contract { get; set; }
    }

    [Serializable]
    public class Contract
    {
        public string Name { get; set; }

        public ContractIncomingOffice IncomingOffice { get; set; }

        public ContractCommentList CommentList { get; set; }
    }

    [Serializable]
    public class ContractIncomingOffice
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
    }

    [Serializable]
    public class ContractCommentList
    {
        public ContractCommentListComment Comment { get; set; }
    }

    [Serializable]
    public class ContractCommentListComment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class Supplier
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("vatNumber")]
        public string VatNumber { get; set; }
    }

    [Serializable]
    public class ServiceCommentList
    {
        public ServiceCommentListComment Comment { get; set; }
    }

    [Serializable]
    public class ServiceCommentListComment
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class DateFrom
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class DateTo
    {
        [XmlAttribute("date")]
        public string Date { get; set; }
    }

    [Serializable]
    public class ServiceCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class AdditionalCostList
    {
        public AdditionalCostListCurrency Currency { get; set; }
        
        public decimal PvpEquivalent { get; set; }

        [XmlArrayItem("AdditionalCost", IsNullable = false)]
        public List<AdditionalCost> AdditionalCost { get; set; }
    }

    [Serializable]
    public class AdditionalCostListCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class AdditionalCost
    {
        public AdditionalCostPrice Price { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }

    [Serializable]
    public class AdditionalCostPrice
    {
        public decimal Amount { get; set; }
    }

    [Serializable]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class ProductHotel : HotelInfo
    {
    }

    [Serializable]
    [XmlInclude(typeof(ProductHotel))]
    [XmlRoot(Namespace = "http://www.hotelbeds.com/schemas/2005/06/messages")]
    public class HotelInfo
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public HotelInfoCategory Category { get; set; }

        public Destination Destination { get; set; }
    }

    [Serializable]
    public class HotelInfoCategory
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class Destination
    {
        public string Name { get; set; }

        public DestinationZoneList ZoneList { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class DestinationZoneList
    {
        public DestinationZoneListZone Zone { get; set; }
    }

    [Serializable]
    public class DestinationZoneListZone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public int Code { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class AvailableRoom
    {
        public HotelOccupancy HotelOccupancy { get; set; }
        
        public HotelRoom HotelRoom { get; set; }
    }

    [Serializable]
    public class HotelOccupancy
    {
        public int RoomCount { get; set; }
        
        public HotelOccupancyOccupancy Occupancy { get; set; }
    }

    [Serializable]
    public class HotelOccupancyOccupancy
    {
        public int AdultCount { get; set; }
        
        public int ChildCount { get; set; }
    }

    [Serializable]
    public class HotelRoom
    {
        public HotelRoomBoard Board { get; set; }
        
        public HotelRoomRoomType RoomType { get; set; }
        
        public HotelRoomPrice Price { get; set; }
        
        [XmlArrayItem("ExtendedData", IsNullable = false)]
        public List<HotelRoomExtendedData> HotelRoomExtraInfo { get; set; }

        [XmlAttribute]
        public string SHRUI { get; set; }

        [XmlAttribute("availCount")]
        public int AvailCount { get; set; }

        [XmlAttribute("status")]
        public string Status { get; set; }
    }

    [Serializable]
    public class HotelRoomBoard
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class HotelRoomRoomType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("characteristic")]
        public string Characteristic { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class HotelRoomPrice
    {
        public decimal Amount { get; set; }
    }

    [Serializable]
    public class HotelRoomExtendedData
    {
        public string Name { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

    [Serializable]
    public class PurchaseCurrency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class PaymentData
    {
        public PaymentDataPaymentType PaymentType { get; set; }
        
        public PaymentDataInvoiceCompany InvoiceCompany { get; set; }
        
        public string Description { get; set; }
    }

    [Serializable]
    public class PaymentDataPaymentType
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }

    [Serializable]
    public class PaymentDataInvoiceCompany
    {
        public string Code { get; set; }
        
        public string Name { get; set; }
        
        public string RegistrationNumber { get; set; }
    }

    [Serializable]
    public class Currency
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
        
    }
}
