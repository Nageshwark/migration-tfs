﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsCancellationAutomator : IAccommodationCancellationAutomatorAsync<HotelBedsCancellationResponse>
    {
        private const string HotelBedsCancellationUrl = "HotelBedsCancellationUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotelBedsProviderCancellationRequestFactory hotelBedsProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotelBedsCancellationAutomator(  ILogger logger,
                                                IAccommodationConfigurationManager configurationManager,
                                                IHotelBedsProviderCancellationRequestFactory hotelBedsSupplierCancellationRequestFactory,
                                                IProviderOutputLogger outputLogger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.hotelBedsProviderCancellationRequestFactory = hotelBedsSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<HotelBedsCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            HotelBedsCancellationResponse response = new HotelBedsCancellationResponse();

            try
            {
                PurchaseCancelRQ cancelRequest = hotelBedsProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

                string url = request.Provider.Parameters.GetParameterValue(HotelBedsCancellationUrl);
                string serialisedRequest = cancelRequest.XmlSerialize();

                WebScrapeResponse supplierResponse = await webScrapeClient.PostAsync(url, serialisedRequest);

                //outputLogger.LogCancellationResponse(request, ProviderOutputFormat.Xml, response); TODO: add the logging method for cancellation

                response = new HotelBedsCancellationResponse
                {
                    SupplierRequest = cancelRequest,
                    SupplierResponse = supplierResponse.Value
                };

                return response;
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                sb.AppendLine("HotelBeds Cancellation Automator: GetCancellationResponseAsync()");
                sb.AppendLine("Error encountered when attempting to cancel an existing booking.");
                sb.AppendLine("Request:");
                sb.AppendLine(response.SupplierRequest.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Response:");
                XDocument formattedResponse = XDocument.Parse(response.SupplierResponse);
                sb.AppendLine(formattedResponse.ToString());
                sb.AppendLine();

                throw new SupplierApiException(string.Format("HotelBedsBookingAutomator.GetCancellationResponseAsync: HotelBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine, 
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }
    }
}
