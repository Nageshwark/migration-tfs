﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<IEnumerable<HotelBedsAvailabilityResponse>>
    {
        private const string HotelBedsAvailabilityUrl = "HotelBedsAvailabilityUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotelBedsProviderAvailabilityRequestFactory hotelBedsProviderAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public HotelBedsAvailabilityAutomator(ILogger logger,
                                                IAccommodationConfigurationManager configurationManager,
                                                IHotelBedsProviderAvailabilityRequestFactory hotelBedsProviderAvailabilityRequestFactory,
                                                IProviderOutputLogger outputLogger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.hotelBedsProviderAvailabilityRequestFactory = hotelBedsProviderAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<HotelBedsAvailabilityResponse>> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            //List<HotelBedsAvailabilityResponse> responses = new List<HotelBedsAvailabilityResponse>();

            IEnumerable<HotelBedsAvailabilityResponse> supplierRequestsNonPackage = null;
            IEnumerable<HotelBedsAvailabilityResponse> supplierRequestsPackage = null;
            List<HotelBedsAvailabilityResponse> supplierRequests = new List<HotelBedsAvailabilityResponse>();

            IEnumerable< HotelBedsAvailabilityResponse > responses = null;

            // 1. The IsSearchAgeSpecific flag
            // The IsSearchAgeSpecific flag in the AccommodationProviders table in the db determines whether the request passed to this method is segragated by child ages or not.
            // Normally, the core will NOT differentiate rooms by child age. For example, two rooms, each of 2 adults and two children will be considered the same room, even if the child ages are the same.
            // The core will therefore consider the rooms to be the same, and will consolidate the two rooms into a single room in the request (see explanation below).
            // However, if the IsSearchAgeSpecific flag is set to true in the db, then the core will only consolidate the rooms if the child ages are the same. If the child ages are different, then
            // the core will pass all the rooms through in the request, and consolidation will NOT occur.

            // 2. Room consolidation in the core.
            // The way the core works is that it consolidates rooms with only adult occupants and with children who are of the same age. 
            // Therefore, if the user requests 6 rooms, with the following occupancies:
            // Room 1 : 2 Adults
            // Room 2 : 2 Adults
            // Room 3 : 2 Adults and 1 child, aged 8  
            // Room 4 : 2 Adults and 1 child, aged 8  
            // Room 5 : 2 Adults and 2 children, aged 4 and 9  
            // Room 6 : 2 Adults and 2 children, aged 6 and 11  
            // For this room list, the core will consolidate the room occupancies and pass in the following rooms in the availability request (NOTE: IsSearchAgeSpecific flag = true):
            // Room 1 : 2 Adults
            // Room 2 : 2 Adults and 1 child, aged 8
            // Room 3 : 2 Adults and 2 children, aged 4 and 9
            // Room 4 : 2 Adults and 2 children, aged 6 and 11
            // Note that rooms have been consolidated based on adult count and child count/child ages
            // The database AccommodationProviders table has a flag called MaxRoomSearch which can be either 1 or NULL. A value of 1 indicates that the availability request will only contain one room. 
            // A value of NULL will mean that all the rooms (AFTER consolidation) will be passed in the request. HotelBeds is set to use NULL.

            // 3. Package Rates
            // Also, we have to send out TWO requests per availability request.
            // The first request is to ask for contracts which are either post payment or direct pay.
            // The second request is to ask for package rate contracts.
            // According to HotelBeds (email 18th August 2015 - case #01649570):
            // "Most of our Opaque contracts have an "older brother" which is a normal contract. The from this the Opaque one is created by applying the package discount and flagging the rate as a package one. 
            // Requesting the Opaque with the Y value blocks the return of the original contracts. 
            // When you want to sell packages you send the request with Y and all contracts you receive can be sold as part of a package (those with package rate = Y normally have a discount for selling them together with another service)."
            // Also, the supplier has a field which determines whether we sell package rates even in a Hotels Only search.
            // If this is set to true, then we will always get package rates as well as standard post payment and pay direct rates
            // If this is set to false, we should only request package rates if the request.SearchType == SearchType.FlightAndHotel.

            // Child Ages with HotelBeds
            // There is an issue in the HotelBeds availability request/response. In the request you specify the ages of any children in the rooms. 
            // This information IS stored on the HotelBeds server and it IS associated with each room result that is returned in the response.
            // However, the response does NOT echo back the child ages, and therefore it is impossible for us to determine which room result goes with which room occupancy 
            // (e.g. does a room result go with the occupancy with 2 children, aged 5 and 8, or with the occupancy with 2 children, aged 4 and 9?).
            // Because of this issue, multiple room occupancies with the same number of children have to be split apart and sent out as separate supplier requests.

            try
            {

                List < List < AccommodationProviderAvailabilityRequestRoom >> segregatedRoomLists = new List<List<AccommodationProviderAvailabilityRequestRoom>>();
                
                // 1. Group the room requests by number of adults and children/infants, and order each group by RoomNumber
                var occupancyGroups = request.Rooms.GroupBy(room => new { Adults = room.Guests.AdultsCount, Children = room.Guests.ChildrenCount + room.Guests.InfantsCount })
                                                   .Select(group => new { Key = group.Key, Items = group.OrderBy(item => item.RoomNumber) });
                
                foreach (var occupancyGroup in occupancyGroups)
                {
                    // Add all rooms with no children to the first list. Rooms with adults only do not need to be split into separate requests - they can all go into one request.
                    if (occupancyGroup.Key.Children == 0)
                    {
                        if (segregatedRoomLists.Count == 0)
                        {
                            segregatedRoomLists.Add(new List<AccommodationProviderAvailabilityRequestRoom>());
                        }
                        
                        foreach (var room in occupancyGroup.Items)
                        {
                            segregatedRoomLists[0].Add(room);
                        }
                    }
                    else
                    {
                        // Rooms with the same number of children have to be split up and stored in different lists.
                        // Assign the first room in each group to the first list of rooms in segregatedRoomLists.
                        // Subsequent rooms in the groups will have to go into different lists (and therefore different requests).
                        var roomList = occupancyGroup.Items.ToList();
                        
                        foreach (var room in roomList)
                        {
                            int index = roomList.IndexOf(room);         // Slow function, but OK for our very small lists
                            
                            // See if our list of room lists contains anything at that index position. If not, add a new List of rooms to the master list
                            if (segregatedRoomLists.Count < index + 1)
                            {
                                segregatedRoomLists.Add(new List<AccommodationProviderAvailabilityRequestRoom>());
                            }
                            
                            segregatedRoomLists[index].Add(room);
                        }
                    }
                }

                supplierRequestsNonPackage = Sync.ParallelForEachIgnoreFailed(segregatedRoomLists, rooms => this.hotelBedsProviderAvailabilityRequestFactory.CreateProviderAvailabilityRequest(request, rooms, false));
                supplierRequestsPackage = Sync.ParallelForEachIgnoreFailed(segregatedRoomLists, rooms => this.hotelBedsProviderAvailabilityRequestFactory.CreateProviderAvailabilityRequest(request, rooms, true));
                supplierRequests.AddRange(supplierRequestsNonPackage);
                supplierRequests.AddRange(supplierRequestsPackage);

                responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, async(supplierRequest) => await GetSupplierAvailabilityResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (responses.Any())
                {
                    foreach (var response in responses)
                    {
                        sb.AppendLine();
                        sb.AppendLine("Request:");
                        if (response.SupplierRequest != null)
                        {
                            sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        }
                        else
                        {
                            sb.AppendLine("HotelBeds HotelValuedAvailRQ Request Not Present!");
                        }

                        sb.AppendLine();
                        sb.AppendLine("Response:");
                        sb.AppendLine(response.SupplierResponse);
                        sb.AppendLine();
                    }
                }

                throw new SupplierApiException(string.Format("HotelBedsAvailabilityAutomator.GetAvailabilityResponseAsync: HotelBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<HotelBedsAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, HotelBedsAvailabilityResponse responseContainer)
        {
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(HotelBedsAvailabilityUrl);
            string serialisedRequest = responseContainer.SupplierRequest.XmlSerialize();

            WebScrapeResponse response = await webScrapeClient.PostAsync(url, serialisedRequest);


            // Need to set a process name in order to generate two log files -> one for the package rate response("Package"), and one for post payment and direct payment responses ("NotPackage").
            // This is required otherwise a "File in Use" exception is generated when trying to write to the log file.
            this.outputLogger.LogAvailabilityResponse(request, (responseContainer.isPackageRequest ? string.Format("PackageRates_{0}Rooms_{1}",responseContainer.Rooms.Count, Guid.NewGuid().ToString().Substring(0, 4)) :
                                                                                                     string.Format("StandardRates_{0}Rooms_{1}", responseContainer.Rooms.Count, Guid.NewGuid().ToString().Substring(0, 4))), 
                                                                                                     ProviderOutputFormat.Xml, 
                                                                                                     response);
            responseContainer.SupplierResponse = response.Value;

            return responseContainer;
        }
    }
}
