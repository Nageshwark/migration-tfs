﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsCancellationPolicyService : IHotelBedsCancellationPolicyService
    {
        private readonly IProviderValuationCancellationPolicyService cancellationPolicyService;

        public HotelBedsCancellationPolicyService(IProviderValuationCancellationPolicyService cancellationPolicyService)
        {
            this.cancellationPolicyService = cancellationPolicyService;
        }

        public async Task<string> GetCancellationTextNonRefundable(AccommodationProviderValuationRequest request)
        {
            string policy = await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 1);
            return policy;
        }

        public async Task<string> GetCancellationTextDatesBefore(AccommodationProviderValuationRequest request, string fromDate, string currency, string fee)
        {
            string policy = await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 2, fromDate, currency, fee);
            return policy;
        }

        public async Task<string> GetCancellationTextNoShow(AccommodationProviderValuationRequest request, string currency, string fee)
        {
            string policy = await this.cancellationPolicyService.CreateProviderCancellationPolicyAsync(request, 3, currency, fee);
            return policy;
        }
    }
}
