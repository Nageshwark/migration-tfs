﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management.Instrumentation;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsAvailabilityParser : IAccommodationAvailabilityParser<IEnumerable<HotelBedsAvailabilityResponse>>
    {
        private const string HotelBedsPayDirectRate = "HotelBedsPayDirectRate";
        private const string HotelBedsNetBindingRate = "HotelBedsNetBindingRate";
        private const string HotelBedsNetNonBindingRate = "HotelBedsNetNonBindingRate";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public HotelBedsAvailabilityParser(ILogger logger,
                                            IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request,
                                                                                           IEnumerable<HotelBedsAvailabilityResponse> responses)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateAvailabilityResultsFromResponse(request, response, availabilityResults));
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("HotelBeds Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
                                                           HotelBedsAvailabilityResponse response,
                                                           ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            if (!string.IsNullOrWhiteSpace(response.SupplierResponse))
            {
                HotelValuedAvailRS availabilityResponse = response.SupplierResponse.XmlDeSerialize<HotelValuedAvailRS>();

                if (!availabilityResponse.ErrorList.Any())
                {
                    Sync.ParallelForEachIgnoreFailed(availabilityResponse.ServiceHotels, (serviceHotel) => CreateAvailabilityResultFromResponse(request, response.Rooms, serviceHotel, availabilityResults));

                    //// DEBUG CODE ONLY:
                    //foreach (var serviceHotel in availabilityResponse.ServiceHotels)
                    //{
                    //    CreateAvailabilityResultFromResponse(request, serviceHotel, availabilityResults);
                    //}
                }
                else
                {
                    // Errors have been returned in the xml response
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("HotelBeds Response Error.");
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();

                    sb.AppendLine("HotelBeds returned one or more errors in their availability response:");

                    foreach (var error in availabilityResponse.ErrorList)
                    {
                        sb.AppendLine(error.Message);
                        sb.AppendLine(error.DetailedMessage);
                        sb.AppendLine();
                    }

                    // Log the error to the event log
                    string message = sb.ToString();
                    logger.Error(message);

                    // Throw an exception. This will be logged in the availability log in the db.
                    throw new SupplierApiException(message);
                }
            }
            else
            {
                // Response is empty
                string message = "HotelBeds Availability Parser: Availability Response cannot be parsed because it is null.";

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }
        }

        // Example <AvailableRoom> element:
        //
        //<AvailableRoom>
        //  <HotelOccupancy>
        //    <RoomCount>1</RoomCount>
        //    <Occupancy>
        //      <AdultCount>2</AdultCount>
        //      <ChildCount>0</ChildCount>
        //    </Occupancy>
        //  </HotelOccupancy>
        //  <HotelRoom SHRUI="+ftXCmd7PJ7cBGUxjJTpEw==" availCount="16" onRequest="N">
        //    <Board type="SIMPLE" code="RO-E10" shortname="RO">ROOM ONLY</Board>
        //    <RoomType type="SIMPLE" code="TPL-E10" characteristic="ST">TRIPLE STANDARD</RoomType>
        //    <ProductType>P</ProductType>
        //    <Price>
        //      <Amount>418.330</Amount>
        //    </Price>
        //  </HotelRoom>
        //</AvailableRoom>
        private void CreateAvailabilityResultFromResponse(  AccommodationProviderAvailabilityRequest request,
                                                            List<AccommodationProviderAvailabilityRequestRoom> rooms,
                                                            ServiceHotel serviceHotel,
                                                            ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            string providerName = request.Provider.Name;
            string providerEdiCode = request.Provider.EdiCode;

            PaymentModel paymentModel = PaymentModel.PostPayment;
            if (serviceHotel.DirectPayment.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
            {
                paymentModel = PaymentModel.CustomerPayDirect;
            }

            // Note: In BEV5, an opaque rate is true if the PackageRate response is "Y" AND the availability search request has the IsPackageSearch flag set to True.
            // The SOA Accommodation IsPackageSearch flag equivalnt is request.SearchType == SearchType.FlightAndHotel  (flight and hotel search = a package).
            bool isOpaqueRate = false;
            if (serviceHotel.PackageRate.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
            {
                isOpaqueRate = true;
            }

            bool isNonRefundableRate = false;
            if (serviceHotel.ContractList.Contract.Classification.Code.Equals("NRF", StringComparison.InvariantCultureIgnoreCase))
            {
                isNonRefundableRate = true;
            }

            string establishmentEdiCode = serviceHotel.HotelInfo.Code.ToString(CultureInfo.InvariantCulture);
            string establishmentName = serviceHotel.HotelInfo.Name;

            // Assemble the Destination Code from the three letter main destination code, e.g. BGI = Barbados, and the specific numetic zone code for the area within the destination that the hotel is located in, e.g. 3
            string destinationEdiCode = serviceHotel.HotelInfo.Destination.Code;

            if (serviceHotel.HotelInfo.Destination.ZoneList != null && serviceHotel.HotelInfo.Destination.ZoneList.Any())
            {
                // Extract the first zone code (there should only be one) and append it to the destination code
                string zoneCode = serviceHotel.HotelInfo.Destination.ZoneList.First().Code.ToString(CultureInfo.InvariantCulture);
                destinationEdiCode = destinationEdiCode + zoneCode;
            }

            string currency = serviceHotel.Currency.Code;
            string contractList = serviceHotel.ContractList.XmlSerialize();
            string contract = serviceHotel.ContractList.Contract.Name;

            // 1. Group the room requests by number of adults and children/infants in each room that was used to create the request, and order each group by RoomNumber
            var occupancyGroups = rooms.GroupBy(room => new { Adults = room.Guests.AdultsCount, Children = room.Guests.ChildrenCount + room.Guests.InfantsCount })
                                       .Select(group => new { Key = group.Key, Items = group.OrderBy(item => item.RoomNumber) });

            // 2. For each room occupancy group, select all the available rooms from the hotel that match the adult and child count
            foreach (var occupancyGroup in occupancyGroups)
            {
                // Get a distinct list of hotel rooms, eliminating duplicates (i.e. rooms that have the same SHRUI).
                // This is important in the situation where we send 3 room requests, e.g. each for 3 adults, as three separate <HotelOccupancy> elements in the request. 
                // In a request of this structure, if the hotel only has one room available for 3 adults, it will return this one room back to us 3 TIMES 
                // (because in the availability request we sent through three separate <HotelOccupancy> elements, one for each room requested instead of sending through one <HotelOccupancy> element with a RoomCount of 3).
                // If we then try to book the room 3 times, only one of the bookings will succeed, and the other 2 will fail (because the hotel actually only has one of these rooms available, even though it returned it 3 times in the response).
                // However, in the availability results, the room which is repeated 3 times can be identified because all 3 instances of that room have the same SHRUI code.
                // Therefore, if we perform a DISTINCT select on the rooms, based on SHRUI code, we should eliminate all the duplicates, leaving only a single room which has an AvailCount which states how many rooms are actually available of that type.
                // This should eliminate the problem of the same room appearing multiple times, so giving the illusion that availability of that room is greater than it actually is.
                var distinctRooms = serviceHotel.AvailableRooms.DistinctBy(room => room.HotelRoom.SHRUI);
                
                // Match the list of distinct rooms with each occupancy group
                var matchingRooms = distinctRooms.Where(room => room.HotelOccupancy.Occupancy.AdultCount == occupancyGroup.Key.Adults &&
                                                                room.HotelOccupancy.Occupancy.ChildCount == occupancyGroup.Key.Children);

                // 3. Loop through all the rooms in each roomGroup and create an availability result for each available room
                int numberOfRoomsInOccupancyGroup = occupancyGroup.Items.Count();
                for (int i = 0; i < numberOfRoomsInOccupancyGroup; i++)
                {
                    foreach (var room in matchingRooms)
                    {
                        // Only add the room if the Availability Count indicates that there are sufficient rooms available, e.g. 2 rooms may be requested, but only 1 is available
                        if (room.HotelRoom.AvailCount > i)
                        {
                            // Create room result
                            AccommodationProviderAvailabilityResult result = new AccommodationProviderAvailabilityResult();

                            //result.Id = Guid.NewGuid();
                            result.RoomNumber = occupancyGroup.Items.ElementAt(i).RoomNumber;

                            result.ProviderEdiCode = providerEdiCode;
                            //result.SupplierEdiCode = string.Empty;        // Not required for HotelBeds.
                            result.PaymentModel = paymentModel;

                            result.DestinationEdiCode = destinationEdiCode;
                            result.EstablishmentEdiCode = establishmentEdiCode;
                            result.EstablishmentName = establishmentName;

                            result.Contract = contract;

                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckOutDate;

                            // NOTE: Unable to set the number of infants from the response as HotelBeds considers infants to be children. Therefore, number of children = children + infants.
                            result.Children = (byte)room.HotelOccupancy.Occupancy.ChildCount;
                            result.Adults = (byte)room.HotelOccupancy.Occupancy.AdultCount;

                            result.RoomCode = room.HotelRoom.RoomType.Code;
                            result.RoomDescription = room.HotelRoom.RoomType.Value;

                            result.BoardCode = room.HotelRoom.Board.Code;
                            result.BoardDescription = room.HotelRoom.Board.Value;

                            result.IsOpaqueRate = isOpaqueRate;
                            result.IsNonRefundable = isNonRefundableRate;

                            result.NumberOfAvailableRooms = (byte?)room.HotelRoom.AvailCount;

                            // Assign Prices, which depends on whether the rate is binding or not
                            result.IsBindingRate = false;
                            if (room.HotelRoom.Price.SellingPrice != null && 
                                room.HotelRoom.Price.SellingPrice.Mandatory.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                            {
                                result.IsBindingRate = true;
                            }

                            if (result.IsBindingRate)
                            {
                                decimal costOfOneRoom = room.HotelRoom.Price.NetPrice / room.HotelOccupancy.RoomCount;
                                result.CostPrice = new Money(costOfOneRoom, currency);

                                decimal salePriceOfOneRoom = room.HotelRoom.Price.SellingPrice.Value / room.HotelOccupancy.RoomCount;
                                result.SalePrice = new Money(salePriceOfOneRoom, currency);

                                if (room.HotelRoom.Price.Commission > 0.00m)
                                {
                                    result.CommissionPercent = room.HotelRoom.Price.Commission;

                                    decimal commissionAmount = salePriceOfOneRoom - costOfOneRoom;
                                    result.CommissionAmount = new Money(commissionAmount, currency);
                                }
                            }
                            else
                            {
                                decimal costOfOneRoom = room.HotelRoom.Price.Amount / room.HotelOccupancy.RoomCount;
                                result.CostPrice = new Money(costOfOneRoom, currency);

                                decimal salePriceAmount = result.CostPrice.Amount;
                                result.SalePrice = new Money(salePriceAmount, result.CostPrice.CurrencyCode);
                            }

                            result.RateType = GetRateType(request, result.IsBindingRate, result.PaymentModel);

                            result.ProviderSpecificData = new Dictionary<string, string>() {    { "ContractList", contractList },
                                                                                                { "AvailabilityToken", serviceHotel.availToken },
                                                                                                { "DestinationCode", serviceHotel .HotelInfo.Destination.Code},
                                                                                                { "BoardType", room.HotelRoom.Board.Type },
                                                                                                { "RoomCharacteristic", room.HotelRoom.RoomType.Characteristic },
                                                                                                { "RoomType", room.HotelRoom.RoomType.Type },
                                                                                                { "PackageRate", serviceHotel.PackageRate}
                                                                                            };

                            availabilityResults.Enqueue(result);
                        }
                    }
                }
            }
        }

        private RateType GetRateType(AccommodationProviderAvailabilityRequest request, bool isBindingRate, PaymentModel paymentModel)
        {
            RateType rateType;

            // Note that the order in which these conditions are executed is very important
            if (paymentModel == PaymentModel.CustomerPayDirect)
            {
                var providerParameter = request.Provider.Parameters.FirstOrDefault(param => param.ParameterName.Equals(HotelBedsPayDirectRate, StringComparison.InvariantCultureIgnoreCase));
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsPayDirectRate);
            }
            else if (isBindingRate)
            {
                var providerParameter = request.Provider.Parameters.FirstOrDefault(param => param.ParameterName.Equals(HotelBedsNetBindingRate, StringComparison.InvariantCultureIgnoreCase));
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsNetBindingRate);
            }
            else
            {
                var providerParameter = request.Provider.Parameters.FirstOrDefault(param => param.ParameterName.Equals(HotelBedsNetNonBindingRate, StringComparison.InvariantCultureIgnoreCase));
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsNetNonBindingRate);
            }

            return rateType;
        }

        private RateType ParseRateTypeParameter(AccommodationProviderParameter parameter, string parameterName)
        {
            RateType rateType;
            RateType tempRate;

            if (parameter != null)
            {
                string rateDescription = parameter.ParameterValue;

                if (Enum.TryParse(rateDescription, true, out tempRate))
                {
                    rateType = tempRate;
                }
                else
                {
                    throw new InvalidCastException(string.Format("The RateType description specified in the HotelBeds Provider Parameter ({0}) cannot be cast into a valid RateType enumeration.", parameterName));
                }
            }
            else
            {
                throw new InstanceNotFoundException((string.Format("The HotelBeds Provider Parameter named {0} was not found. Is it missing from the Database?", parameterName)));
            }

            return rateType;
        }

    }
}
