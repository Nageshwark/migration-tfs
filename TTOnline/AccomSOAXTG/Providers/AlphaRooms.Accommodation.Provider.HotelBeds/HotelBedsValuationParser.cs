﻿using System.Management.Instrumentation;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AvailabilityResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse;
using ValuationRequest = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest;
using ValuationResponse = AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationResponse;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsValuationParser : IAccommodationValuationParserAsync<IEnumerable<HotelBedsValuationResponse>>
    {
        private const string HotelBedsPayDirectRate = "HotelBedsPayDirectRate";
        private const string HotelBedsNetBindingRate = "HotelBedsNetBindingRate";
        private const string HotelBedsNetNonBindingRate = "HotelBedsNetNonBindingRate";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IHotelBedsCancellationPolicyService cancellationPolicyService;

        public HotelBedsValuationParser(ILogger logger,
            IAccommodationConfigurationManager configurationManager,
            IHotelBedsCancellationPolicyService cancellationPolicyService)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.cancellationPolicyService = cancellationPolicyService;
        }

        public async Task<IEnumerable<AccommodationProviderValuationResult>> GetValuationResultsAsync(AccommodationProviderValuationRequest request, IEnumerable<HotelBedsValuationResponse> responses)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // Need to use different processing for pay direct valuations and post payment valuations.
                // All post payment valuations will have been chained and returned in a single valuation response.
                // Each pay direct room will have been valued separately as they are cannot be combined with with post pay rooms in a chained valuation
                List<Task> tasks = new List<Task>();

                // Post Payment rooms
                var postPayResponses = responses.Where(response => !response.IsPayDirect);

                if (postPayResponses.Count() > 0)
                {
                    var postPaymentTask = Task.Run(async() => await CreateValuationResultsFromResponse(request, postPayResponses, false, valuationResults));
                    tasks.Add(postPaymentTask);
                }

                // Pay Direct Rooms
                var payDirectResponses = responses.Where(response => response.IsPayDirect);

                if (payDirectResponses.Count() > 0)
                {
                    foreach (var payDirectResponse in payDirectResponses)
                    {
                        var payDirectTask = Task.Run(async() => await CreateValuationResultsFromResponse(request, new List<HotelBedsValuationResponse>() {payDirectResponse}, true, valuationResults));
                        tasks.Add(payDirectTask);
                    }
                }

                // Make sure that all valuation parsing has completed before continuing
                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format( "HotelBeds Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        /// <summary>
        /// Note: the "List<AccommodationProviderValuationRequestRoom> rooms" parameter can contain one or more rooms.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supplierRequest"></param>
        /// <param name="supplierResponse"></param>
        /// <param name="rooms"></param>
        /// <param name="valuationResults"></param>
        private async Task CreateValuationResultsFromResponse(  AccommodationProviderValuationRequest request,
                                                                IEnumerable<HotelBedsValuationResponse> responses,
                                                                bool isPayDirect,
                                                                ConcurrentQueue<AccommodationProviderValuationResult> valuationResults)
        {
            HotelBedsValuationResponse response = null;

            // Get the specific response that needs to be deserialised.
            // For pay direct rooms, the responses collection will only have one item in it.
            // For post payment rooms, the responses collection could contain multiple items, each with a different SequenceNo. The response with the highest sequence no will contain the final valuation result to be used.
            if (isPayDirect)
            {
                response = responses.First();
            }
            else
            {
                response = responses.OrderBy(supplierResponse => supplierResponse.SequenceNo).Last();
            }

            if (!string.IsNullOrWhiteSpace(response.SupplierResponse))
            {
                ValuationResponse.ServiceAddRS valuationResponse = response.SupplierResponse.XmlDeSerialize<ValuationResponse.ServiceAddRS>();

                // Debug Code
                if (request.Debugging)
                {
                    string serialisedRequest = response.SupplierRequest.XmlSerialize();
                    string serialisedResponse = valuationResponse.XmlSerialize();
                }

                if (!valuationResponse.ErrorList.Any() &&
                    valuationResponse.Purchase.ServiceList.Count == responses.Count())
                {
                    if (isPayDirect)
                    {
                        await CreatePayDirectValuationResultAsync(request, valuationResponse, response, valuationResults);
                    }
                    else
                    {
                        await CreatePostPaymentValuationResultAsyn(request, valuationResponse, responses, valuationResults);
                    }
                }
                else if (valuationResponse.ErrorList.Any())
                {
                    // Check that no error was included in the response - if an error is returned, throw an exception
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("HotelBeds Valuation Error.");
                    sb.AppendLine("Valuation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response Error:");

                    foreach (var error in valuationResponse.ErrorList)
                    {
                        sb.AppendLine(error.Message);
                        sb.AppendLine(error.DetailedMessage);
                        sb.AppendLine();
                    }

                    string message = sb.ToString();

                    // Log the error to the event log
                    logger.Error(message);

                    // Throw an exception. This will be logged in the availability log in the db.
                    throw new SupplierApiException(message);
                }
                else if (valuationResponse.Purchase.ServiceList.Count != responses.Count())
                {
                    // Number of rooms in the deserialised response does not match the number of requests sent - one or more rooms are missing from the valuation response --> INVALID!!
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("HotelBeds Valuation Error.");
                    sb.AppendLine("The number of rooms returned in the valuation response does not match the number of rooms that were requested.");
                    sb.AppendLine("Valuation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();

                    foreach (var resp in responses)
                    {
                        sb.AppendLine("Response:");
                        sb.AppendLine(response.SupplierResponse);
                    }

                    sb.AppendLine("Response Error:");

                    if (valuationResponse.ErrorList != null)
                    {
                        foreach (var error in valuationResponse.ErrorList)
                        {
                            sb.AppendLine(error.Message);
                            sb.AppendLine(error.DetailedMessage);
                            sb.AppendLine();
                        }
                    }

                    string message = sb.ToString();

                    // Log the error to the event log
                    logger.Error(message);

                    // Throw an exception. This will be logged in the availability log in the db.
                    throw new SupplierApiException(message);
                }
            }
            else
            {
                // Response was empty
                string message = "HotelBeds Valuation Parser: Valuation Response cannot be parsed because it is null.";

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }
        }

        /// <summary>
        /// Creates valuation results for direct pay valuation requests
        /// </summary>
        /// <param name="request"></param>
        /// <param name="valuationData"></param>
        /// <param name="response"></param>
        /// <param name="valuationResults"></param>
        /// <returns></returns>
        private async Task CreatePayDirectValuationResultAsync( AccommodationProviderValuationRequest request,
                                                                ValuationResponse.ServiceAddRS valuationData,
                                                                HotelBedsValuationResponse response,
                                                                ConcurrentQueue<AccommodationProviderValuationResult> valuationResults)
        {
            List<AccommodationProviderValuationResult> tempResults = new List<AccommodationProviderValuationResult>();

            // There should only be one service element in the ServiceList
            var service = valuationData.Purchase.ServiceList.First();

            // Get the total price off all the rooms in the availability results AND the valuation response (IMPORTANT: individual room prices are NOT available/reconstructable in multi room valuation requests).
            // Note that the price in the valuation response may be held in two elements. 
            // For Direct pay rooms where the payment at hotel currency differs from the currency returned in the availability results, the valuation response will
            // contain an AdditionalCostList collection which will give the overall price of all the rooms in the valuation response in the availabiloity currency.
            // For direct pay rooms in which the pay at hotel currency is the same as the availability currency, an AdditionalCostList collection may well not be present, and the total amount/currency can be read
            // directly from the Service.TotalAmount and the Service.Currency elements.
            decimal totalAvailabilityPrice = response.Rooms.Sum(room => room.AvailabilityResult.SalePrice.Amount);
            Money totalAvailabilityAmount = new Money(totalAvailabilityPrice, response.Rooms.First().AvailabilityResult.SalePrice.CurrencyCode);

            Money totalValuationAmount = GetTotalAmount(service);
            Money totalValuationAdditionalCostListPrice = GetAdditionalCostListTotalPrice(service);

            // Important: for multi room valuation requests, it is impossible to get the price of an individual room.
            // Available Rooms are listed in the response in the same order that they are specified in the request, i.e. by room number order
            for (int i = 0; i < response.Rooms.Count(); i++)
            {
                ValuationResponse.AvailableRoom availableRoom = valuationData.Purchase.ServiceList.First().AvailableRooms[i];
                AccommodationProviderValuationRequestRoom selectedRoom = response.Rooms.ToList()[i];
                AccommodationProviderAvailabilityResult availabilityResult = selectedRoom.AvailabilityResult;

                AccommodationProviderValuationResult result = new AccommodationProviderValuationResult();

                result.Contract = service.ContractList.Contract.Name.Trim();

                result.Adults = (byte) availableRoom.HotelOccupancy.Occupancy.AdultCount;
                result.Children = availabilityResult.Children;
                result.Infants = availabilityResult.Infants;

                result.BoardCode = availableRoom.HotelRoom.Board.Code;
                result.BoardDescription = availableRoom.HotelRoom.Board.Value;

                result.CheckInDate = availabilityResult.CheckInDate;
                result.CheckOutDate = availabilityResult.CheckOutDate;

                result.IsNonRefundable = availabilityResult.IsNonRefundable;
                result.IsOpaqueRate = availabilityResult.IsOpaqueRate;
                result.IsBindingRate = availabilityResult.IsBindingRate;
                result.IsSpecialRate = false;

                result.PaymentModel = availabilityResult.PaymentModel;
                result.RateType = availabilityResult.RateType;

                result.DestinationEdiCode = availabilityResult.DestinationEdiCode;
                result.EstablishmentEdiCode = service.HotelInfo.Code.ToString();
                result.EstablishmentName = service.HotelInfo.Name;

                result.ProviderEdiCode = availabilityResult.ProviderEdiCode;
                result.ProviderName = availabilityResult.ProviderName;
                result.SupplierEdiCode = availabilityResult.SupplierEdiCode;

                result.RoomCode = availableRoom.HotelRoom.RoomType.Code;
                result.RoomDescription = availableRoom.HotelRoom.RoomType.Value;

                // IMPORTANT: due to the consolidation of requested rooms in the availability request by the framework, e.g. a request for 2 rooms for 2 adults each will be consolidated into one room for 2 adults,
                // the RoomNumber of the availability result may not match the RoomNumber of the AccommodationProviderValuationRequestRoom. The room number of the AccommodationProviderAvailabilityResult cannot be
                // trusted - use the room number of the AccommodationProviderValuationRequestRoom which should be correct.
                result.RoomNumber = selectedRoom.RoomNumber;

                result.ProviderPaymentOptions = GetAcceptedPaymentCards(service);
                result.CancellationPolicy = new[] { await GetCancellationPoliciesAsync(request, availableRoom, service.Currency.Code, availabilityResult.IsNonRefundable) };
                result.ProviderSpecificData = CreateProviderSpecificData(response.SequenceNo, service, valuationData.Purchase.purchaseToken, availableRoom);

                // Note: do not set NumberOfAvailableRooms as the ServiceAddRS will always return a count of 1 regardless of the actual number of rooms available!

                // Check prices
                if (totalAvailabilityAmount.CurrencyCode.Equals(totalValuationAmount.CurrencyCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (totalAvailabilityAmount.Amount == totalValuationAmount.Amount)
                    {
                        // Prices match
                        result.CommissionAmount = availabilityResult.CommissionAmount;
                        result.CommissionPercent = availabilityResult.CommissionPercent;
                        result.CostPrice = availabilityResult.CostPrice;
                        result.SalePrice = availabilityResult.SalePrice;
                    }
                    else
                    {
                        // Needs to allocate the total price between the rooms in the request
                        CalculateValuationPrices(totalAvailabilityAmount, totalValuationAmount, availabilityResult, result);
                    }
                }
                else
                {
                    // Currency code mismatch -> use the AdditionalCostListData instead of the Service.TotalAmount element
                    if (totalValuationAdditionalCostListPrice == null)
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("HotelBeds Valuation Parser");
                        sb.AppendLine("Unable to get prices for Pay Direct Room. The AdditionalCostList element is not found in the Valuation Response.");
                        sb.AppendLine("The currency of the availability result is " + totalAvailabilityAmount.CurrencyCode);
                        sb.AppendLine("Valuation Response:");
                        sb.AppendLine(valuationData.XmlSerialize());

                        string message = sb.ToString();

                        throw new SupplierApiDataException(message);
                    }
                    else if (totalAvailabilityAmount.CurrencyCode.Equals(totalValuationAdditionalCostListPrice.CurrencyCode))
                    {
                        if (totalAvailabilityAmount.Amount == totalValuationAdditionalCostListPrice.Amount)
                        {
                            // Prices match
                            result.CommissionAmount = availabilityResult.CommissionAmount;
                            result.CommissionPercent = availabilityResult.CommissionPercent;
                            result.CostPrice = availabilityResult.CostPrice;
                            result.SalePrice = availabilityResult.SalePrice;
                        }
                        else
                        {
                            // Needs to allocate the total price between the rooms in the request
                            CalculateValuationPrices(totalAvailabilityAmount, totalValuationAdditionalCostListPrice, availabilityResult, result);
                        }
                    }
                    else if (!totalAvailabilityAmount.CurrencyCode.Equals(totalValuationAdditionalCostListPrice.CurrencyCode))
                    {
                        // Problem: no currency match
                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("HotelBeds Valuation Parser");
                        sb.AppendLine("Unable to get prices for Pay Direct Room. Cannot match the currency of the availability result with any of the currencies listed in the Valuation response.");
                        sb.AppendLine("The currency of the availability result is " + totalAvailabilityAmount.CurrencyCode);
                        sb.AppendLine("The currency of the AdditionalCostList element in the Valuation response is " + totalValuationAdditionalCostListPrice.CurrencyCode);
                        sb.AppendLine("Valuation Response:");
                        sb.AppendLine(valuationData.XmlSerialize());

                        string message = sb.ToString();

                        throw new SupplierApiDataException(message);
                    }
                }

                tempResults.Add(result);
            }

            // Need to account for any rounding errors in the event of availability and valuation price differences
            if (tempResults.First().SalePrice.CurrencyCode.Equals(totalValuationAmount.CurrencyCode, StringComparison.InvariantCultureIgnoreCase))
            {
                decimal valuationTotal = tempResults.Sum(room => room.SalePrice.Amount);

                if (valuationTotal != totalValuationAmount.Amount)
                {
                    AdjustPrices(tempResults.First().SalePrice, totalValuationAmount, valuationTotal);
                }
            }
            else if (totalValuationAdditionalCostListPrice != null &&
                     tempResults.First().SalePrice.CurrencyCode.Equals(totalValuationAdditionalCostListPrice.CurrencyCode, StringComparison.InvariantCultureIgnoreCase))
            {
                decimal valuationTotal = tempResults.Sum(room => room.SalePrice.Amount);

                if (valuationTotal != totalValuationAdditionalCostListPrice.Amount)
                {
                    AdjustPrices(tempResults.First().SalePrice, totalValuationAdditionalCostListPrice, valuationTotal);
                }
            }

            // Return results
            foreach (var valuedResult in tempResults)
            {
                valuationResults.Enqueue(valuedResult);
            }
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderValuationResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="finalResponse"></param>
        /// <param name="responses"></param>
        /// <param name="valuationResults"></param>
        private async Task CreatePostPaymentValuationResultAsyn(AccommodationProviderValuationRequest request,
                                                                ValuationResponse.ServiceAddRS finalResponse,
                                                                IEnumerable<HotelBedsValuationResponse> responses,
                                                                ConcurrentQueue<AccommodationProviderValuationResult> valuationResults)
        {
            // Order the responses by sequence number asc. This is the order in which they will appear in the supplier response.
            // The order of Service elements in the valuation response for post payment rooms is CRITICAL, and allows us to link the individual room valuation response with the room that has been selected by the user.
            // Also, this order MUST be replicated in terms of guest names/numbers/ages when making a booking.
            var orderedResponses = responses.OrderBy(resp => resp.SequenceNo); // Order Asc.

            foreach (var orderedResponse in orderedResponses)
            {
                int seqNo = orderedResponse.SequenceNo;
                var selectedRoom = orderedResponse.Rooms.First();

                // Locate the valuation response from the final list of valued rooms returned by HotelBeds, that matches the selected room
                var service = finalResponse.Purchase.ServiceList[seqNo];

                AccommodationProviderValuationResult result = new AccommodationProviderValuationResult();

                result.RoomNumber = selectedRoom.RoomNumber;

                result.ProviderName = request.Provider.Name;
                result.ProviderEdiCode = request.Provider.EdiCode;
                //result.SupplierEdiCode = string.Empty;        // Not required for HotelBeds.

                // TODO: Should we assemble the destination code from the code + zone id, or should we just use the code?
                // Assemble the Destination Code from the three letter main destination code, e.g. BGI = Barbados, and the specific zone code for the area within the destination that the hotel is located in.
                string destinationEdiCode = service.HotelInfo.Destination.Code;

                if (service.HotelInfo.Destination.ZoneList != null &&
                    service.HotelInfo.Destination.ZoneList.Zone != null)
                {
                    // Extract the first zone code (there should only be one) and append it to the destination code
                    string zoneCode = service.HotelInfo.Destination.ZoneList.Zone.Code.ToString(CultureInfo.InvariantCulture);
                    destinationEdiCode = destinationEdiCode + zoneCode;
                }

                result.DestinationEdiCode = destinationEdiCode;
                result.EstablishmentEdiCode = service.HotelInfo.Code.ToString(CultureInfo.InvariantCulture);
                result.EstablishmentName = service.HotelInfo.Name;

                result.Contract = service.ContractList.Contract.Name;

                int fromYear = int.Parse(service.DateFrom.Date.Substring(0, 4));
                int fromMonth = int.Parse(service.DateFrom.Date.Substring(4, 2));
                int fromDay = int.Parse(service.DateFrom.Date.Substring(6, 2));
                result.CheckInDate = new DateTime(fromYear, fromMonth, fromDay);

                int toYear = int.Parse(service.DateTo.Date.Substring(0, 4));
                int toMonth = int.Parse(service.DateTo.Date.Substring(4, 2));
                int toDay = int.Parse(service.DateTo.Date.Substring(6, 2));
                result.CheckOutDate = new DateTime(toYear, toMonth, toDay);

                result.Adults = (byte)service.AvailableRooms.First().HotelOccupancy.Occupancy.AdultCount;
                result.Infants = selectedRoom.AvailabilityResult.Infants;
                result.Children = selectedRoom.AvailabilityResult.Children;

                result.RoomCode = service.AvailableRooms.First().HotelRoom.RoomType.Code;
                result.RoomDescription = service.AvailableRooms.First().HotelRoom.RoomType.Value;

                result.BoardCode = service.AvailableRooms.First().HotelRoom.Board.Code;
                result.BoardDescription = service.AvailableRooms.First().HotelRoom.Board.Value;

                // FINANCIAL DATA:
                result.IsOpaqueRate = selectedRoom.AvailabilityResult.IsOpaqueRate;
                result.IsNonRefundable = selectedRoom.AvailabilityResult.IsNonRefundable;

                // Binding Rate
                result.IsBindingRate = false;
                if (service.SellingPrice != null &&
                    service.SellingPrice.Mandatory.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.IsBindingRate = true;
                }

                // Payment Model
                PaymentModel paymentModel = PaymentModel.PostPayment;
                bool isPayDirect = false;
                if (service.DirectPayment.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                {
                    paymentModel = PaymentModel.CustomerPayDirect;
                    isPayDirect = true;
                }

                // Rate Type
                result.PaymentModel = paymentModel;
                result.RateType = GetRateType(request, result.IsBindingRate, result.PaymentModel);

                // Set Selling Price, Cost Price and Commission
                // If the room is a Pay Direct room, we have to be careful!! The valuation <TotalAmount> element will NOT match the availability result <SellingPrice> if the currency used has changed, e.g. from EUR in 
                // the Availability Result to THB (Thai Bart) in the valuation result (for hotels in Thailand). In this case, we have to match the availability currency with the currency listed in the <AdditionalCostList> section
                // of the Valuation Result. If they match, the <AdditionalCostList> section should be used to get the selling price, commission and cost price. If there is no currency match between the availability result
                // and either the valuation currency or the currency in the <AdditionalCostList> section, then parsing will fail for this valuation result and an exception will be thrown.
                // IMPORTANT: 
                // This assumes that each selected room is specified separated in the ServiceAddRQ request such that the <RoomCount> for each selected room is ALWAYS set to 1, and that room counts are 
                // never combined in the valuation request (i.e. RoomCount > 1 will never occur).
                // With a RoomCount of 1, each room in the valuation response corresponds to a single room in the request. 
                // Therefore it is not necessary to divide the price by the room count to get a per room price.
                string availabilityResultCurrency = selectedRoom.AvailabilityResult.SalePrice.CurrencyCode;
                string valuationCurrency = service.Currency.Code;

                if (result.PaymentModel == PaymentModel.CustomerPayDirect)
                {
                    // Match availability currency with the main valuation currency
                    if (availabilityResultCurrency.Equals(valuationCurrency, StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.SalePrice = new Money(service.TotalAmount, valuationCurrency);
                        result.CostPrice = new Money(decimal.Parse(service.NetPrice), valuationCurrency);
                        result.CommissionPercent = decimal.Parse(service.Commission);

                        decimal commissionAmount = result.SalePrice.Amount - result.CostPrice.Amount;
                        result.CommissionAmount = new Money(commissionAmount, valuationCurrency);
                    }
                    else if (service.AdditionalCostList != null &&
                             service.AdditionalCostList.Currency != null &&
                             availabilityResultCurrency.Equals(service.AdditionalCostList.Currency.Code, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // if the currencies match, then get the selling price, cost price and commission from the <AdditionalCostList> section.
                        // NOTE: we do not consider Commission VAT as this always comes across as a zero value.
                        string additionalCostListCurrency = service.AdditionalCostList.Currency.Code;

                        if (service.AdditionalCostList.PvpEquivalent != null)
                        {
                            // Sale Price
                            result.SalePrice = new Money(decimal.Parse(service.AdditionalCostList.PvpEquivalent),
                                additionalCostListCurrency);

                            // Commission Percentage
                            if (
                                service.AdditionalCostList.AdditionalCosts.Any(cost => cost.Type.Equals("COMMISSION_PCT", StringComparison.InvariantCultureIgnoreCase)))
                            {
                                decimal commissionPercentage = service.AdditionalCostList.AdditionalCosts.First(cost => cost.Type.Equals("COMMISSION_PCT", StringComparison.InvariantCultureIgnoreCase)).Price.Amount;
                                result.CommissionPercent = commissionPercentage;
                            }

                            // Commission Amount
                            if (
                                service.AdditionalCostList.AdditionalCosts.Any(cost => cost.Type.Equals("AG_COMMISSION", StringComparison.InvariantCultureIgnoreCase)))
                            {
                                decimal commissionAmount = service.AdditionalCostList.AdditionalCosts.First(cost => cost.Type.Equals("AG_COMMISSION", StringComparison.InvariantCultureIgnoreCase)).Price.Amount;
                                result.CommissionAmount = new Money(commissionAmount, additionalCostListCurrency);
                            }

                            // Cost Price
                            decimal costPrice = result.SalePrice.Amount - result.CommissionAmount.Amount;
                            result.CostPrice = new Money(costPrice, additionalCostListCurrency);
                        }
                    }
                    else
                    {
                        // No match is found for the availability result currency: throw an exception
                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("HotelBedsValuationParser: Error parsing valuation result. Unable to match the currency of the selected Availability Result with the currency of the Valuation Result.");
                        sb.AppendLine("Room No.: " + selectedRoom.AvailabilityResult.RoomNumber);
                        sb.AppendLine("Availability Result Currency: " + availabilityResultCurrency);
                        sb.AppendLine("HotelBeds Valuation Response:");
                        sb.AppendLine(finalResponse.XmlSerialize());

                        throw new SupplierApiDataException(sb.ToString());
                    }
                }
                else
                {
                    // For all other Payment Models (i.e. PostPayment), just use the <TotalAmount> element for sale and cost prices
                    result.SalePrice = new Money(service.TotalAmount, valuationCurrency);
                    result.CostPrice = new Money(service.TotalAmount, valuationCurrency);

                    result.CommissionPercent = 0.00m;
                    result.CommissionAmount = null;
                }

                // Map accepted credit cards
                result.ProviderPaymentOptions = GetAcceptedPaymentCards(service);

                // Map cancellation policies
                result.CancellationPolicy = new[] { await GetCancellationPoliciesAsync(request, service.AvailableRooms.First(), service.Currency.Code, selectedRoom.AvailabilityResult.IsNonRefundable) };

                // Map "other" required data
                result.ProviderSpecificData = CreateProviderSpecificData(seqNo, service, finalResponse.Purchase.purchaseToken, service.AvailableRooms.First());

                valuationResults.Enqueue(result);
            }
        }

        /// <summary>
        /// IMPORTANT: Codes "MC" and "VI" apply to BOTH credit AND debit cards.
        /// </summary>
        /// <param name="cardCode"></param>
        /// <returns></returns>
        private List<CardType> GetCreditCardTypes(string cardCode)
        {
            List<CardType> cards = new List<CardType>();

            switch (cardCode)
            {
                case "AE":
                    cards.Add(CardType.AmericanExpress);
                    break;

                case "AP":
                    cards.Add(CardType.EuroCard);
                    break;

                case "DC":
                    cards.Add(CardType.DinersClub);
                    break;

                case "JC":
                    cards.Add(CardType.JCB);
                    break;

                case "MA":
                    cards.Add(CardType.Maestro);
                    break;

                case "MC":
                    cards.Add(CardType.MasterCard);
                    cards.Add(CardType.MastercardDebit);
                    break;

                case "VE":
                    cards.Add(CardType.VisaElectron);
                    break;

                case "VI":
                    cards.Add(CardType.VisaCredit);
                    cards.Add(CardType.VisaDebit);
                    break;

                // E6 = Euro 6000 card - Not Supported
                case "E6":
                    cards.Add(CardType.Unknown);
                    break;

                // EC = ECMC card - Not Supported
                case "EC":
                    cards.Add(CardType.Unknown);
                    break;

                // If card type not found, return unknown
                default:
                    cards.Add(CardType.Unknown);
                    break;
            }

            return cards;
        }

        private RateType GetRateType(AccommodationProviderValuationRequest request, bool isBindingRate, PaymentModel paymentModel)
        {
            RateType rateType;

            // Note that the order in which these conditions are executed is very important
            if (paymentModel == PaymentModel.CustomerPayDirect)
            {
                var providerParameter = request.Provider.Parameters.GetParameterValue(HotelBedsPayDirectRate);
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsPayDirectRate);
            }
            else if (isBindingRate)
            {
                var providerParameter = request.Provider.Parameters.GetParameterValue(HotelBedsNetBindingRate);
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsNetBindingRate);
            }
            else
            {
                var providerParameter = request.Provider.Parameters.GetParameterValue(HotelBedsNetNonBindingRate);
                rateType = ParseRateTypeParameter(providerParameter, HotelBedsNetNonBindingRate);
            }
            return rateType;
        }

        private RateType ParseRateTypeParameter(string parameterValue, string parameterName)
        {
            RateType rateType;
            RateType tempRate;

            string rateDescription = parameterValue;

            if (Enum.TryParse(rateDescription, true, out tempRate))
            {
                rateType = tempRate;
            }
            else
            {
                throw new InvalidCastException(string.Format("The RateType description specified in the HotelBeds Provider Parameter ({0}) cannot be cast into a valid RateType enumeration.", parameterName));
            }
            return rateType;
        }

        // Gets the total price of entire valuation from the AdditionalCostList collection, if it exists.
        private Money GetAdditionalCostListTotalPrice(ValuationResponse.Service service)
        {
            Money totalPrice = null;

            if (service.AdditionalCostList != null)
            {
                decimal pvpEquivPrice = 0.00m;
                decimal commissionAmount = 0.00m;
                decimal commissionVAT = 0.00m;
                decimal priceSum = 0.00m;
                string currencyCode = string.Empty;

                if (service.AdditionalCostList.PvpEquivalent != null)
                {
                    pvpEquivPrice = decimal.Parse(service.AdditionalCostList.PvpEquivalent);
                }

                if (service.AdditionalCostList.AdditionalCosts.Where(cost => cost.Type.Equals("AG_COMMISSION", StringComparison.InvariantCultureIgnoreCase)) != null)
                {
                    var commissionCost = service.AdditionalCostList.AdditionalCosts.Where(cost => cost.Type.Equals("AG_COMMISSION", StringComparison.InvariantCultureIgnoreCase)).First();
                    commissionAmount = commissionCost.Price.Amount;
                }

                if (service.AdditionalCostList.AdditionalCosts.Where(cost => cost.Type.Equals("COMMISSION_VAT", StringComparison.InvariantCultureIgnoreCase)) != null)
                {
                    var commissionTax = service.AdditionalCostList.AdditionalCosts.Where(cost => cost.Type.Equals("COMMISSION_VAT", StringComparison.InvariantCultureIgnoreCase)).First();
                    commissionVAT = commissionTax.Price.Amount;
                }

                currencyCode = service.AdditionalCostList.Currency.Code;

                priceSum = pvpEquivPrice + commissionAmount + commissionVAT;

                totalPrice = new Money(priceSum, currencyCode);
            }

            return totalPrice;
        }

        // Gets the total price of the entire valuation, from the TotalAmount element
        private Money GetTotalAmount(ValuationResponse.Service service)
        {
            string currencyCode = service.Currency.Code;

            decimal totalPrice = service.TotalAmount;

            Money totalAmount = new Money(totalPrice, currencyCode);

            return totalAmount;
        }

        private void CalculateValuationPrices(Money totalAvailabilityAmount , Money totalValuationAmount , AccommodationProviderAvailabilityResult availabilityResult, AccommodationProviderValuationResult result)
        {
            // Price mismatch: need to allocate the total prices between the two rooms. This is arbitrary as there is not way of getting individual room prices from a valuation response (for a multi room request)
            // We will therefore allocate prices according to the relative percentage that each availability result contributes to the total.
            decimal percentOfTotal = availabilityResult.SalePrice.Amount / totalAvailabilityAmount.Amount;
            result.SalePrice = new Money(totalValuationAmount.Amount * percentOfTotal, availabilityResult.SalePrice.CurrencyCode);

            // Commission percentage should not change
            result.CommissionPercent = availabilityResult.CommissionPercent;

            if (result.CommissionPercent > 0.00m)
            {
                result.CommissionAmount = new Money(result.SalePrice.Amount * (result.CommissionPercent / (decimal)100), availabilityResult.SalePrice.CurrencyCode);
                result.CostPrice = new Money(result.SalePrice.Amount - result.CommissionAmount.Amount, availabilityResult.CostPrice.CurrencyCode);
            }
            else
            {
                // Can't readily calculate the cost price
                result.CostPrice = availabilityResult.CostPrice;
            }
        }

        // Manually adjust prices to account for any rounding errors
        private void AdjustPrices(Money salePrice, Money totalValuationAmount, decimal sumOfSalePrices)
        {
            // Calculate difference - it should only be very small and due to rounding errors
            decimal difference = totalValuationAmount.Amount - sumOfSalePrices;

            // Allocate the difference to the first result
            salePrice.Amount += difference;
        }

        private string GetCurrency(ValuationResponse.Service service)
        {
            string currency = string.Empty;

            if (service.AdditionalCostList != null)
            {
                if (service.AdditionalCostList.Currency != null)
                {
                    currency = service.AdditionalCostList.Currency.Code;
                }
            }
            else
            {
                currency = service.Currency.Code;
            }

            return currency;
        }

        private async Task<string> GetCancellationPoliciesAsync(    AccommodationProviderValuationRequest request,
                                                                    ValuationResponse.AvailableRoom room, 
                                                                    string currency, 
                                                                    bool isNonRefundable)
        {
            string cancellationPolicies = String.Empty;

            // Cancellation Policies : generate a string that contains a description of the cancellation policies for the valuation
            // TODO: How to convert cancellation penalty amounts into channel currency??
            // TODO: The only way that cancellation charges can be converted to a different currency is if the amount and currency are stored in a separate data field (see BEV5 AlphaRooms.Common.CancellationTermData class).
            if (isNonRefundable)
            {
                cancellationPolicies = await this.cancellationPolicyService.GetCancellationTextNonRefundable(request);
            }
            else if (room.HotelRoom.CancellationPolicies.Any())
            {
                var policiesOrderedByDate = room.HotelRoom.CancellationPolicies.OrderBy(policy => policy.DateFrom);

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Room Cancellation Policies:");

                foreach (var policy in policiesOrderedByDate)
                {
                    int year = int.Parse(policy.DateFrom.Substring(0, 4));
                    int month = int.Parse(policy.DateFrom.Substring(4, 2));
                    int day = int.Parse(policy.DateFrom.Substring(6, 2));
                    int hour = int.Parse(policy.Time.Substring(0, 2));
                    int minutes = int.Parse(policy.Time.Substring(2, 2));
                    DateTime fromDate = new DateTime(year, month, day, hour, minutes, 0);

                    string terms = await this.cancellationPolicyService.GetCancellationTextDatesBefore( request, 
                                                                                                        fromDate.ToString("HH:mm dd MMMM yyyy", CultureInfo.InvariantCulture), 
                                                                                                        currency, 
                                                                                                        policy.Amount.ToString("########.00", CultureInfo.InvariantCulture));
                    sb.AppendLine(terms);
                }

                cancellationPolicies = sb.ToString();
            }

            return cancellationPolicies;
        }

        private Dictionary<string, string> CreateProviderSpecificData(int sequenceNo, ValuationResponse.Service service, string purchaseToken, ValuationResponse.AvailableRoom availableRoom)
        {
            Dictionary<string, string> providerSpecificData = new Dictionary<string, string>()
            {
                {"SeqNo", sequenceNo.ToString()},
                {"ContractList", service.ContractList.XmlSerialize()},
                {"PurchaseToken", purchaseToken},
                {"RoomSHRUI", availableRoom.HotelRoom.SHRUI},
                {"CancellationPolicies", availableRoom.HotelRoom.CancellationPolicies.XmlSerialize() },
                {"ServiceSPUI", service.SPUI},
                {"RoomCharacteristic", availableRoom.HotelRoom.RoomType.Characteristic}
            };

            return providerSpecificData;
        }

        private List<AccommodationProviderPaymentOption> GetAcceptedPaymentCards(ValuationResponse.Service service)
        {
            List<AccommodationProviderPaymentOption> acceptedPaymentCards = null;

            // Map acceptable credit cards
            if (service.AcceptedCardTypes != null)
            {
                acceptedPaymentCards = new List<AccommodationProviderPaymentOption>();

                foreach (var acceptedCard in service.AcceptedCardTypes)
                {
                    // Get a list of the card types associated with that code (could be more than one).
                    List<CardType> cardTypes = GetCreditCardTypes(acceptedCard.Type);

                    foreach (var cardType in cardTypes)
                    {
                        AccommodationProviderPaymentOption acceptedPaymentCard = new AccommodationProviderPaymentOption();
                        acceptedPaymentCard.CardType = cardType;
                        acceptedPaymentCard.CardTypeText = cardType.GetDescription();

                        if (acceptedPaymentCard.CardType != CardType.Unknown)
                        {
                            acceptedPaymentCards.Add(acceptedPaymentCard);
                        }
                    }
                }
            }

            return acceptedPaymentCards;
        }
    }
}
