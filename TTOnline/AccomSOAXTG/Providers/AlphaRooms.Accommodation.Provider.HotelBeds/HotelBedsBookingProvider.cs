﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<HotelBedsBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<HotelBedsBookingResponse>> parser;

        public HotelBedsBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<HotelBedsBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<HotelBedsBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
