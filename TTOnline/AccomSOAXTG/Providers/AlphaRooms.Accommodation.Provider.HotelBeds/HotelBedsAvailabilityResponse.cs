﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityResponse;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that HotelBeds accepts and returns XML strings.
    /// </summary>
    public class HotelBedsAvailabilityResponse
    {
        public HotelValuedAvailRQ SupplierRequest { get; set; }
        public bool isPackageRequest { get; set; }
        public List<AccommodationProviderAvailabilityRequestRoom> Rooms { get; set; }
        public string SupplierResponse { get; set; }
    }
}
