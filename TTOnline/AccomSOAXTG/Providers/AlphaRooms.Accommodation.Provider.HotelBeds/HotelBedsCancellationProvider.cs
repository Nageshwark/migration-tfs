﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<HotelBedsCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<HotelBedsCancellationResponse> parser;

        public HotelBedsCancellationProvider(IAccommodationCancellationAutomatorAsync<HotelBedsCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<HotelBedsCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
