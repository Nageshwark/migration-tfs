﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationRequest;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces
{
    public interface IHotelBedsProviderCancellationRequestFactory
    {
        PurchaseCancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
