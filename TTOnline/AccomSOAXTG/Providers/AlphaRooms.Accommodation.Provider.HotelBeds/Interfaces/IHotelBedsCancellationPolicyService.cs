﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces
{
    public interface IHotelBedsCancellationPolicyService
    {
        Task<string> GetCancellationTextNonRefundable(AccommodationProviderValuationRequest request);
        Task<string> GetCancellationTextDatesBefore(AccommodationProviderValuationRequest request, string fromDate, string currency, string fee);
        Task<string> GetCancellationTextNoShow(AccommodationProviderValuationRequest request, string currency, string fee);
    }
}
