﻿using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.BookingRequest;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces
{
    public interface IHotelBedsProviderBookingRequestFactory
    {
        HotelBedsBookingResponse CreateSupplierBookingRequest(AccommodationProviderBookingRequest request, List<KeyValuePair<string, AccommodationProviderBookingRequestRoom>> roomsToBeBooked, string key);

        //PurchaseConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request, AccommodationProviderBookingRequestRoom room);
    }
}
