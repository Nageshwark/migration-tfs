﻿using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.ValuationRequest;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces
{
    public interface IHotelBedsProviderValuationRequestFactory
    {
        HotelBedsValuationResponse CreateSupplierValuationRequestForPayDirect(AccommodationProviderValuationRequest request, List<KeyValuePair<string, AccommodationProviderValuationRequestRoom>> selectedRooms, string contract);

        HotelBedsValuationResponse CreateSupplierValuationRequestForPostPayment(AccommodationProviderValuationRequest request, AccommodationProviderValuationRequestRoom selectedRoom);
    }
}
