﻿using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.AvailabilityRequest;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces
{
    public interface IHotelBedsProviderAvailabilityRequestFactory
    {
        HotelBedsAvailabilityResponse CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request,
                                                                        List<AccommodationProviderAvailabilityRequestRoom> roomList,
                                                                        bool isPackage);

        HotelValuedAvailRQ CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request, bool isPackage);
    }
}
