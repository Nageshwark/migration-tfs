﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.HotelBeds.Contracts.CancellationRequest;

namespace AlphaRooms.Accommodation.Provider.HotelBeds
{
    public class HotelBedsCancellationResponse
    {
        public PurchaseCancelRQ SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
    }
}
