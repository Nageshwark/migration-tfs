﻿using System;
using System.Security.Cryptography;
using System.Text;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Expedia.Helper
{


    public class APIKeyHelper  :IExpediaApiKeyHelper
    {
        public string GetSignature(string apiKey, string secret)
        {
            string sig = MD5Hash(apiKey + secret + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);

            return sig;
        }
        
        private string MD5Hash(string strInput)
        {
            MD5 md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(strInput));
            
            StringBuilder sBuilder = new StringBuilder();
            
            for (int nIndex = 0; nIndex < data.Length; ++nIndex)
            {
                sBuilder.Append(data[nIndex].ToString("x2"));
            }
            
            return sBuilder.ToString();
        }

        /*
        private static void Main(string[] args)
        {
            string service = "http://api.ean.com/ean-services/rs/hotel/";
            string version = "v3/";
            string method = "list/";
            string hotelId = "201252";
            string otherElemntsStr =
                "&cid=[x]&minorRev=[x]&customerUserAgent=[xxx]&customerIpAddress=[xxx]&locale=en_US&currencyCode=USD";
            string apiKey = "xxx-YourAPIkey-xxx";
            string secret = "xxYourSecretxx";
            string sig =
                MD5GenerateHash(apiKey + secret + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            string fullURL = service + version + method + "?apiKey=" + apiKey + "&sig=" + sig + otherElemntsStr +
                             "&hotelIdList=" + hotelId;

            // Create the web request
            HttpWebRequest request = WebRequest.Create(fullURL) as HttpWebRequest;

            // Get response
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Write response to the console
                Console.WriteLine(reader.ReadToEnd());
            }

        }
        */

    }
}


