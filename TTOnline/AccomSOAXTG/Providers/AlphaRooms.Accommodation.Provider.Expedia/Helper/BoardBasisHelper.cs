﻿using System.Collections.Generic;
using System.Linq;

namespace AlphaRooms.Accommodation.Provider.Expedia.Helper
{
    public class BoardBasisHelper
    {
        private List<BoardBasis> _valueAdds;

        public BoardBasisHelper()
        {
            _valueAdds = new List<BoardBasis>
            {
                new BoardBasis { Name = "Continental Breakfast", Code = "BB"},
                new BoardBasis { Name = "Full Breakfast", Code = "BB"},
                new BoardBasis { Name = "English Breakfast", Code = "BB" },
                new BoardBasis { Name = "Free Lunch" , Code = ""},
                new BoardBasis { Name = "Free Dinner", Code = "" },
                new BoardBasis { Name = "All-Inclusive", Code = "AI" },
                new BoardBasis { Name = "Continental Breakfast for 2", Code = "BB" },
                new BoardBasis { Name = "Breakfast for 2", Code = "BB" },
                new BoardBasis { Name = "Breakfast Buffet", Code = "BB" },
                new BoardBasis { Name = "Half Board", Code = "HB" },
                new BoardBasis { Name = "Full Board" , Code = "FB"},
                
            };
        }
        public BoardBasis GetBoardBasis(Entity.ValueAdds valueAdds)
        {
            if (valueAdds == null) 
                return null;

            foreach (var va in valueAdds.ValueAdd)
            {
                var bbQuery = from BoardBasis bb in _valueAdds
                    where bb.Name == va.description
                    select bb;

                if (bbQuery.Any())
                {
                    return (BoardBasis)bbQuery.First(); 
                }
            }

            return null;
        }
    }

    public class BoardBasis
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
