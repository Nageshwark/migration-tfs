﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.CustomExceptions;
    using AlphaRooms.WebScraping;
    using Ninject.Extensions.Logging;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

    public class ExpediaValuationAutomator : IAccommodationValuationAutomatorAsync<ExpediaValuationResponse>
    {
        private const string ExpediaValuationUrl = "ExpediaValuationUrl";
        private const string ExpediaUsername = "ExpediaUsername";
        private const string ExpediaPassword = "ExpediaPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly ILogger logger;
        private readonly IExpediaProviderValuationRequestFactory ExpediaProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public ExpediaValuationAutomator( ILogger logger, IExpediaProviderValuationRequestFactory ExpediaSupplierValuationRequestFactory
            , IProviderLoggerService loggerService, IProviderOutputLogger outputLogger)
        {
            this.logger = logger;
            this.ExpediaProviderValuationRequestFactory = ExpediaSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<ExpediaValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            string supplierRequest = null;
            ExpediaValuationResponse response = null;
            try
            {
                supplierRequest = this.ExpediaProviderValuationRequestFactory.CreateSupplierValuationRequest(request);
                response = await GetSupplierValuationResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ExpediaValuationAutomator.GetValuationResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ExpediaValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, string supplierRequest)
        {
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            string url = request.Provider.Parameters.GetParameterValue(ExpediaValuationUrl);
            string urlWithParams = url + supplierRequest;
            this.loggerService.SetProviderRequest(request, urlWithParams);
            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParams);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Json, response);

            ExpediaValuationResponse supplierResponse = new ExpediaValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };

            return supplierResponse;
        }

       
    
    }
}
