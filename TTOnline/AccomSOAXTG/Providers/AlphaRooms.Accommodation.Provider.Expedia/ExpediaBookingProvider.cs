﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<ExpediaBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<ExpediaBookingResponse>> parser;

        public ExpediaBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<ExpediaBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<ExpediaBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
