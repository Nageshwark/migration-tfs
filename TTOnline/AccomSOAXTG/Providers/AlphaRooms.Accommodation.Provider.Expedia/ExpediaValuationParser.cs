﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Accommodation.Provider.Expedia.Entity;
    using AlphaRooms.Accommodation.Provider.Expedia.Helper;
    using AlphaRooms.SOACommon.Contracts;
    using AlphaRooms.SOACommon.Contracts.Enumerators;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.CustomExceptions;
    using Newtonsoft.Json;
    using Ninject.Extensions.Logging;

    public class ExpediaValuationParser : IAccommodationValuationParser<ExpediaValuationResponse>
    {
        private readonly ILogger logger;
        public ExpediaValuationParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, ExpediaValuationResponse responses)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateValuationResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage;

                if (ex is AggregateException)
                {
                    var aggregateEx = (AggregateException) ex;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Expedia Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationResultsFromResponse(AccommodationProviderValuationRequest request, string supplierRequest, string supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            var response = Deserialize<HotelRoomAvailabilityResponseContainer>(supplierResponse);
            bool hasResults = (supplierResponse != null);

            if (hasResults)
            {
                var boardBasisHelper = new BoardBasisHelper();
                var availResult = request.SelectedRooms.First().AvailabilityResult;
                var roomResponse = response.HotelRoomAvailabilityResponse.HotelRoomResponse;
                var rooms = response.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room;
                var rateInfo = response.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo;
                var roomCount = rateInfo.RoomGroup.Room.Count;
                int roomCounter = 1;
               
                foreach (var room in rooms)
                {
                    var result = new AccommodationProviderValuationResult();

                    result.RoomNumber = (byte)roomCounter;
                    result.ProviderEdiCode = request.Provider.EdiCode;

                    //TODO
                    result.PaymentModel = PaymentModel.PostPayment;

                    result.DestinationEdiCode = response.HotelRoomAvailabilityResponse.HotelCity;
                    result.EstablishmentEdiCode = response.HotelRoomAvailabilityResponse.HotelId.ToString(CultureInfo.InvariantCulture);
                    result.EstablishmentName = response.HotelRoomAvailabilityResponse.HotelName;

                    result.RoomDescription = availResult.RoomDescription;
                    result.RoomCode = roomResponse.RoomTypeCode.ToString(CultureInfo.InvariantCulture);

                    result.CheckInDate = availResult.CheckInDate;
                    result.CheckOutDate = availResult.CheckOutDate;

                    result.Adults = (byte)room.NumberOfAdults;
                    result.Children = (byte)room.NumberOfChildren;

                    var bbDescription = boardBasisHelper.GetBoardBasis(response.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds);

                    if (bbDescription != null)
                    {
                        result.BoardCode = bbDescription.Code;
                        result.BoardDescription = bbDescription.Name.Equals("All-Inclusive") ? "All Inclusive" : bbDescription.Name;
                    }

                    result.IsNonRefundable = roomResponse.RateInfos.RateInfo.NonRefundable;

                    decimal surcharge = Convert.ToDecimal(rateInfo.ChargeableRateInfo.surchargeTotal);
                    decimal total = GetRoomTotal(room.ChargeableNightlyRates, surcharge, roomCount);
                    string currencyCode = rateInfo.ChargeableRateInfo.CurrencyCode;

                    result.SalePrice = new Money()
                    {
                        Amount = total,
                        CurrencyCode = currencyCode
                    };
                    result.CostPrice = result.SalePrice;
                    result.NumberOfAvailableRooms = rateInfo.CurrentAllotment;
                    result.CostPrice = result.SalePrice;
                    result.RateType = RateType.GrossStandard;
                    result.PaymentModel = request.SelectedRooms[0].AvailabilityResult.PaymentModel;
                    result.CancellationPolicy = CreateCancellationPolicy(rateInfo);
                    result.ProviderSpecificData = new Dictionary<string, string>
                    {
                        {"RateKey", room.RateKey},
                        {"SupplierType", roomResponse.SupplierType},
                        {"BookingTotal", roomResponse.RateInfos.RateInfo.ChargeableRateInfo.Total},
                        {"RateCode", roomResponse.RateCode.ToString()},
                        {
                            "KeyCollectionInformation",
                            (!string.IsNullOrEmpty(response.HotelRoomAvailabilityResponse.CheckInInstructions)
                                ? response.HotelRoomAvailabilityResponse.CheckInInstructions
                                : String.Empty)
                        },
                        {"CurrencyCode", currencyCode},
                        {"BedTypeId", roomResponse.BedTypes.BedType.First().Id}
                    };
                    //TODO:Expedia requires BedTypeID to be passed at the time of booking. Need to agree how to handle this if there are more then one BedTypes
                    availabilityResults.Enqueue(result);
                }
                
            }
            else
            {
                string message = "Expedia Valuation Parser: Valuation Response cannot be parsed because it is null.";
                throw new SupplierApiDataException(message);
            }
        }

        private string[] CreateCancellationPolicy(RateInfo rateInfo)
        {
            decimal tax;
            decimal.TryParse(rateInfo.ChargeableRateInfo.surchargeTotal, out tax);
            if (tax == 0) return new[] { rateInfo.CancellationPolicy };
            return new[]
            {
                rateInfo.CancellationPolicy
                , "<b>Taxes and Fees included in your final price: </b> " + rateInfo.ChargeableRateInfo.CurrencyCode + " " + tax.ToString("#.00")
            };
        }

        public T Deserialize<T>(string response)
        {
            var objString = response;
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<T>(objString, settings);
            return root;
        }

        private decimal GetRoomTotal(IEnumerable<ChargeableNightlyRates> rates, decimal surcharge, int roomCount)
        {
            var sum = rates.AsEnumerable().Sum(r => r.Rate);
            return sum + (surcharge / roomCount);
        }
    }
}
