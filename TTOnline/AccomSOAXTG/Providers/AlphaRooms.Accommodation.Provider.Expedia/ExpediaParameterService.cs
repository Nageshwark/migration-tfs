﻿using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaParameterService : IExpediaParameterService
    {
        private const string ExpediaAPIKey = "ExpediaAPIKey";
        private const string ExpediaCID = "ExpediaCID";
        private const string ExpediaSecret = "ExpediaSecret";
        private const string ExpediaOpaqueAPIKey = "ExpediaOpaqueAPIKey";
        private const string ExpediaOpaqueCID = "ExpediaOpaqueCID";
        private const string ExpediaOpaqueSecret = "ExpediaOpaqueSecret";

        private const string ExpediaLocale = "ExpediaLocale";
        private const string ExpediaCurrencyCode = "ExpediaCurrencyCode";

        private const string ExpediaMinorRevision = "ExpediaMinorRevision";
        private const string ExpediaMaxNumberOfHotel = "ExpediaMaxNumberOfHotel";

        private const string ExpediaHotelCollectEnabled = "ExpediaHotelCollectEnabled";
        private const string ExpediaHotelCollectCID = "ExpediaHotelCollectCID";
        private const string ExpediaHotelCollectAPIKey = "ExpediaHotelCollectAPIKey";
        private const string ExpediaHotelCollectSecret = "ExpediaHotelCollectSecret";
        private const string ExpediaApiExperience = "ExpediaApiExperience";

        private const string ExpediaSendReservationEmail = "ExpediaSendReservationEmail";

        private const string ExpediaLastName = "ExpediaLastName";
        private const string ExpediaCreditCardType = "ExpediaCreditCardType";
        private const string ExpediaCreditCardNumber = "ExpediaCreditCardNumber";
        private const string ExpediaCreditCardIdentifier = "ExpediaCreditCardIdentifier";
        private const string ExpediaEmail = "ExpediaEmail";
        private const string ExpediaAddress1 = "ExpediaAddress1";
        private const string ExpediaCity = "ExpediaCity";
        private const string ExpediaCountryCode = "ExpediaCountryCode";
        private const string ExpediaPostalCode = "ExpediaPostalCode";
        private const string ExpediaStateProvinceCode = "ExpediaStateProvinceCode";
        private const string ExpediaHomePhone = "ExpediaHomePhone";
        private const string ExpediaAvailabilityUrl = "ExpediaAvailabilityUrl";
        private const string ExpediaPostPaymentEnabled = "ExpediaPostPaymentEnabled";
        private const string ExpediaOpaqueEnabled = "ExpediaOpaqueEnabled";
        private List<AccommodationProviderParameter> providerParameters;

        public void InitializeParameters(List<AccommodationProviderParameter> parameters)
        {
            this.providerParameters = parameters;
            Locale = providerParameters.GetParameterValue(ExpediaLocale);
            CurrencyCode = providerParameters.GetParameterValue(ExpediaCurrencyCode);
            MinorRevision = providerParameters.GetParameterValue(ExpediaMinorRevision);
            MaxNumberOfHotel = providerParameters.GetParameterValue(ExpediaMaxNumberOfHotel);
            bool hotelCollectEnabled;
            bool.TryParse(providerParameters.GetParameterValue(ExpediaHotelCollectEnabled), out hotelCollectEnabled);
            HotelCollectEnabled = hotelCollectEnabled;
            ApiExperience = providerParameters.GetParameterValue(ExpediaApiExperience);

            SendReservationEmail = providerParameters.GetParameterValue(ExpediaSendReservationEmail);
            LastName = providerParameters.GetParameterValue(ExpediaLastName);
            CreditCardType = providerParameters.GetParameterValue(ExpediaCreditCardType);
            CreditCardNumber = providerParameters.GetParameterValue(ExpediaCreditCardNumber);
            CreditCardIdentifier = providerParameters.GetParameterValue(ExpediaCreditCardIdentifier);
            Email = providerParameters.GetParameterValue(ExpediaEmail);
            Address1 = providerParameters.GetParameterValue(ExpediaAddress1);
            City = providerParameters.GetParameterValue(ExpediaCity);
            CountryCode = providerParameters.GetParameterValue(ExpediaCountryCode);
            PostalCode = providerParameters.GetParameterValue(ExpediaPostalCode);
            StateProvinceCode = providerParameters.GetParameterValue(ExpediaStateProvinceCode);
            HomePone = providerParameters.GetParameterValue(ExpediaHomePhone);

            AvailabilityUrl = providerParameters.GetParameterValue(ExpediaAvailabilityUrl);
            var opaqueEnabled = false;
            bool.TryParse(providerParameters.GetParameterValue(ExpediaOpaqueEnabled), out opaqueEnabled);
            this.OpaqueEnabled = opaqueEnabled;
            var postPaymentEnabled = false;
            bool.TryParse(providerParameters.GetParameterValue(ExpediaPostPaymentEnabled), out postPaymentEnabled);
            this.PostPaymentEnabled = postPaymentEnabled;

        }

        public void GetCredentials(RequestType requestType, out string cid, out string apiKey, out string apiSecret)
        {
            cid = apiKey = apiSecret = string.Empty;
            switch (requestType)
            {
                case RequestType.Opaque:
                    cid = providerParameters.GetParameterValue(ExpediaOpaqueCID);
                    apiKey = providerParameters.GetParameterValue(ExpediaOpaqueAPIKey);
                    apiSecret = providerParameters.GetParameterValue(ExpediaOpaqueSecret);
                    break;
                case RequestType.HotelCollect:
                    cid = providerParameters.GetParameterValue(ExpediaHotelCollectCID);
                    apiKey = providerParameters.GetParameterValue(ExpediaHotelCollectAPIKey);
                    apiSecret = providerParameters.GetParameterValue(ExpediaHotelCollectSecret);
                    break;
                default:
                    cid = providerParameters.GetParameterValue(ExpediaCID);
                    apiKey = providerParameters.GetParameterValue(ExpediaAPIKey);
                    apiSecret = providerParameters.GetParameterValue(ExpediaSecret);
                    break;
            }
        }

        public string Locale { get; private set; }
        public string CurrencyCode { get; private set; }
        public string MinorRevision { get; private set; }
        public string MaxNumberOfHotel { get; private set; }
        public bool HotelCollectEnabled { get; private set; }
        public string ApiExperience { get; private set; }
        //public string BookingReference { get; private set; }
        public string SendReservationEmail { get; private set; }
        public string LastName { get; private set; }
        public string CreditCardType { get; private set; }
        public string CreditCardNumber { get; private set; }
        public string CreditCardIdentifier { get; private set; }
        public string Email { get; private set; }
        public string Address1 { get; private set; }
        public string City { get; private set; }
        public string CountryCode { get; private set; }
        public string PostalCode { get; private set; }
        public string StateProvinceCode { get; private set; }
        public string HomePone { get; private set; }
        public string AvailabilityUrl { get; private set; }
        public bool OpaqueEnabled { get; private set; }
        public bool PostPaymentEnabled { get; private set; }

    }
}