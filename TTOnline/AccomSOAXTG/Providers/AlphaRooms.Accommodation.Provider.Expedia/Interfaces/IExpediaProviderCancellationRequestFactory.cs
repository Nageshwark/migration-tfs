﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaProviderCancellationRequestFactory
    {
        string CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
