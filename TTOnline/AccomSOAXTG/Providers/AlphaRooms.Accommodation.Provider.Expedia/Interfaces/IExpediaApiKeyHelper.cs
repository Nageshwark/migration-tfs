﻿namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaApiKeyHelper
    {
        string GetSignature(string apiKey, string secret);
    }
}