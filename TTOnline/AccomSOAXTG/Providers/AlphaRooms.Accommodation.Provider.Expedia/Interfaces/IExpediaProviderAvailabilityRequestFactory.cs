﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;

namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaProviderAvailabilityRequestFactory
    {
        ExpediaAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
        ExpediaAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request, RequestType requestType);
    }
}
