﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaProviderValuationRequestFactory
    {
        string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
