﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaProviderBookingRequestFactory
    {
        string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
