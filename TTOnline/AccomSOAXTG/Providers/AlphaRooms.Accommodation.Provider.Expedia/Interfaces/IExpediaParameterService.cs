﻿using System.Collections.Generic;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;

namespace AlphaRooms.Accommodation.Provider.Expedia.Interfaces
{
    public interface IExpediaParameterService
    {
        void InitializeParameters(List<AccommodationProviderParameter> parameters);
        void GetCredentials(RequestType requestType, out string cid, out string apiKey, out string apiSecret);

        string Locale { get; }
        string CurrencyCode { get; }
        string MinorRevision { get; }
        string MaxNumberOfHotel { get; }
        bool HotelCollectEnabled { get; }
        string ApiExperience { get; }
       
        string SendReservationEmail { get; }
        string LastName { get; }
        string CreditCardType { get; }
        string CreditCardNumber { get; }
        string CreditCardIdentifier { get; }
        string Email { get; }
        string Address1 { get; }
        string City { get; }
        string CountryCode { get; }
        string PostalCode { get; }
        string StateProvinceCode { get; }
        string HomePone { get; }
       
        string AvailabilityUrl { get; }
        bool OpaqueEnabled { get; }
        bool PostPaymentEnabled { get; }

    }
}