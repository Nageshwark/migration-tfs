﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ExpediaValuationResponse> automator;
        private readonly IAccommodationValuationParser<ExpediaValuationResponse> parser;

        public ExpediaValuationProvider(IAccommodationValuationAutomatorAsync<ExpediaValuationResponse> automator,
                                                IAccommodationValuationParser<ExpediaValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
