﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaAvailabilityRequest
    {
        public string APIParameters { get; set; }
        public bool IsOpaque { get; set; }
        public bool IsHotelCollect { get; set; } 
    }
}