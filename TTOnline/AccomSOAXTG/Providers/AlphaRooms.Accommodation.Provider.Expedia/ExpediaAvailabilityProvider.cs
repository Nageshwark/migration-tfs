﻿//using AlphaRooms.Configuration;

using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<IEnumerable<ExpediaAvailabilityResponse>> automator;
        private readonly IAccommodationAvailabilityParser<IEnumerable<ExpediaAvailabilityResponse>> parser;

        public ExpediaAvailabilityProvider(   IAccommodationAvailabilityAutomatorAsync<IEnumerable<ExpediaAvailabilityResponse>> automator,
                                                IAccommodationAvailabilityParser<IEnumerable<ExpediaAvailabilityResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
