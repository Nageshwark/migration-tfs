﻿using AlphaRooms.Accommodation.Provider.Expedia.Entity;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaBookingParser : IAccommodationBookingParser<IEnumerable<ExpediaBookingResponse>>
    {
        private readonly ILogger logger;

        public ExpediaBookingParser(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<ExpediaBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Expedia Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        string supplierRequest,
                                                        string supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (string.IsNullOrWhiteSpace(supplierResponse))
            {
                string message = string.Format("Expedia Booking Parser: Booking Response cannot be parsed because it is null. Booking Request Id: {0}", request.BookingId);

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }

            var response = Deserialize<HotelRoomReservationResponseContainer>(supplierResponse);

            // Check that no error was included in the response - if an error is returned, log the error in the event log, but DO NOT throw an exception. Instead, return a failed booking.
            if (response.HotelRoomReservationResponse.EanWsError != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Expedia Response Error.");
                sb.AppendLine("Expedia returned one or more errors in their booking response:");
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest);
                sb.AppendLine();


                sb.AppendLine("Error Details:");
                sb.AppendFormat("Handling:{0}", response.HotelRoomReservationResponse.EanWsError.Handling);
                sb.AppendFormat("Category:{0}", response.HotelRoomReservationResponse.EanWsError.Category);
                sb.AppendFormat("ExceptionConditionId:{0}", response.HotelRoomReservationResponse.EanWsError.ExceptionConditionId);
                sb.AppendFormat("PresentationMessage:{0}", response.HotelRoomReservationResponse.EanWsError.PresentationMessage);
                sb.AppendFormat("VerboseMessage:{0}", response.HotelRoomReservationResponse.EanWsError.VerboseMessage);



                // Log the error to the event log
                logger?.Error(sb.ToString());

                // Create a failed booking result
                AccommodationProviderBookingResult failedResult = new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    Message = sb.ToString()
                };

                bookingResults.Enqueue(failedResult);
            }
            else
            {
                // NOTE: This will process a single room booking ONLY. It is not designed to handle multiple room bookings.
                CreateAvailabilityResultFromResponse(request, supplierRequest, response, bookingResults);
            }
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderBookingResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supplierRequest"></param>
        /// <param name="response"></param>
        /// <param name="bookingResults"></param>
        private void CreateAvailabilityResultFromResponse(AccommodationProviderBookingRequest request,
                                                            string supplierRequest,
                                                            HotelRoomReservationResponseContainer response,
                                                            ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();
            result.RoomNumber = request.ValuatedRooms[0].RoomNumber;
            try
            {
                var reservation = response.HotelRoomReservationResponse;

                result.ProviderBookingReference = reservation.ItineraryId + ":" + (reservation.ConfirmationNumbers.Any() ? string.Join(",",reservation.ConfirmationNumbers) : string.Empty);
                result.KeyCollectionInformation = request.ValuatedRooms[0].ValuationResult.ProviderSpecificData["KeyCollectionInformation"];

                result.ProviderSpecificData = new Dictionary<string, string>();

                result.ProviderSpecificData.Add("ReservationStatusCode", reservation.ReservationStatusCode);
                result.ProviderSpecificData.Add("ProcessedWithConfirmation", reservation.ProcessedWithConfirmation.ToString());


                int counter = 1;
                foreach (var confirmationNumber in reservation.ConfirmationNumbers)
                {
                    result.ProviderSpecificData.Add("ConfirmationNumber_" + counter, confirmationNumber.ToString());

                    counter++;
                }
                result.BookingStatus = BookingStatus.Confirmed;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from Expedia.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Request:");
                sb.AppendLine(supplierRequest);
                sb.AppendLine();
                sb.AppendLine("Booking Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Enqueue(result);
        }

        public T Deserialize<T>(string response)
        {
            var objString = response;
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<T>(objString, settings);
            return root;
        }
    }
}
