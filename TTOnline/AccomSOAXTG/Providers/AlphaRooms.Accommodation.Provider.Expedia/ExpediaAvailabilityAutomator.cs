﻿
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using RequestType = AlphaRooms.Accommodation.Provider.Expedia.Entity.RequestType;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<IEnumerable<ExpediaAvailabilityResponse>>
    {
        private readonly IProviderOutputLogger outputLogger;
        private readonly IExpediaProviderAvailabilityRequestFactory expediaProviderAvailabilityRequestFactory;
        private readonly IExpediaParameterService parameterService;

        public ExpediaAvailabilityAutomator(
            IExpediaProviderAvailabilityRequestFactory expediaProviderAvailabilityRequestFactory,
            IExpediaParameterService parameterService,
            IProviderOutputLogger outputLogger
            )
        {
            this.outputLogger = outputLogger;
            this.expediaProviderAvailabilityRequestFactory = expediaProviderAvailabilityRequestFactory;
            this.parameterService = parameterService;
        }

        public async Task<IEnumerable<ExpediaAvailabilityResponse>> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            //initialize parameter service with parameters
            parameterService.InitializeParameters(request.Provider.Parameters);
            IEnumerable<ExpediaAvailabilityResponse> responses = null;

            try
            {
                var requestTypes = new List<RequestType>();
                if (this.parameterService.PostPaymentEnabled)
                {
                    requestTypes.Add(RequestType.General);
                }
                if (this.parameterService.OpaqueEnabled && request.SearchType == SearchType.FlightAndHotel)
                {
                    requestTypes.Add(RequestType.Opaque);
                }
                //Pay direct hotels
                if (parameterService.HotelCollectEnabled)
                {
                    requestTypes.Add(RequestType.HotelCollect);
                }
                //Call service and get responses in parallel
                responses = await Async.ParallelForEachIgnoreFailed(requestTypes, async (requestType) =>
                await GetSupplierAvailabilityResponseAsync(request, requestType));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (responses.Any())
                {
                    foreach (var response in responses)
                    {
                        sb.AppendLine();
                        sb.AppendLine("Request:");

                        if (response.SupplierResponse != null)
                        {
                            sb.AppendLine(response.XmlSerialize());
                        }
                        else
                        {
                            sb.AppendLine("Supplier Request Not Present!");
                        }

                        sb.AppendLine();
                        sb.AppendLine("Response:");

                        if (response.SupplierResponse != null)
                            sb.AppendLine(response.SupplierResponse.XmlSerialize());

                        sb.AppendLine();
                    }
                }

                throw new SupplierApiException(string.Format("ExpediaAvailabilityAutomator.GetAvailabilityResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<ExpediaAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, RequestType requestType)
        {
            var supplierRequest = expediaProviderAvailabilityRequestFactory.CreateProviderAvailabilityRequest(request, requestType);
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };
            string url = parameterService.AvailabilityUrl;
            string urlWithParams = url + supplierRequest.APIParameters;
            WebScrapeResponse response = await webScrapeClient.GetAsync(urlWithParams);
#pragma warning disable 4014
            Task.Run(
#pragma warning restore 4014
                () =>
                    outputLogger.LogAvailabilityResponse(request, requestType.ToString(), ProviderOutputFormat.Json,
                        response.Value));
            return new ExpediaAvailabilityResponse() { SupplierRequest = supplierRequest, SupplierResponse = response.Value };
        }
    }
}
