﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Expedia accepts and returns XML strings.
    /// </summary>
    public class ExpediaAvailabilityResponse
    {
        public ExpediaAvailabilityRequest SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
    }
}
