﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaValuationResponse
    {
        public string SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
    }
}
