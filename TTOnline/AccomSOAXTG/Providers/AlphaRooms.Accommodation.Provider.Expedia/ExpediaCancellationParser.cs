﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.CustomExceptions;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaCancellationParser : IAccommodationCancellationParserAsync<ExpediaCancellationResponse>
    {
        private readonly ILogger logger;

        public ExpediaCancellationParser(ILogger logger)
        {
            this.logger = logger;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, ExpediaCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                
                var supplierResponse = Deserialize(response.SupplierResponse);
                
                if (supplierResponse != null && 
                    supplierResponse.HotelRoomCancellationResponse !=null &&
                    supplierResponse.HotelRoomCancellationResponse.EanWsError==null)
                {

                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";   

                }
                else
                {
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    var sb = new StringBuilder();
                    
                    if (supplierResponse != null && supplierResponse.HotelRoomCancellationResponse!=null && supplierResponse.HotelRoomCancellationResponse.EanWsError!=null)
                    {
                        string error = supplierResponse.HotelRoomCancellationResponse.EanWsError.VerboseMessage;
                        sb.AppendLine("There was an error with cancellation, plese see thhe response from Expedia below.");
                        sb.AppendLine(error);
                    }
                    else
                        sb.AppendLine("There was an unknown problem with the cancellation response from Expedia.");

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Expedia.");
                sb.AppendLine("Details:" + ex.ToString());
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }

        public HotelRoomCancellationResponseContainer Deserialize(string response)
        {
            var objString = response;
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<HotelRoomCancellationResponseContainer>(objString, settings);
            return root;
        }
    }
}
