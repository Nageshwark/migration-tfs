﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using AlphaRooms.Accommodation.Provider.Expedia.Helper;
using Newtonsoft.Json;

namespace AlphaRooms.Accommodation.Provider.Expedia.Entity
{
    [DataContract]
    public class CachedSupplierResponse
    {
        [DataMember(Name = "@supplierCacheTolerance")]
        public string supplierCacheTolerance { get; set; }

        [DataMember(Name = "@cachedTime")]
        public string cachedTime { get; set; }


        [DataMember(Name = "@supplierRequestNum")]
        public string supplierRequestNum { get; set; }

        [DataMember(Name = "@supplierResponseNum")]
        public string supplierResponseNum { get; set; }

        [DataMember(Name = "@supplierResponseTime")]
        public string supplierResponseTime { get; set; }

        [DataMember(Name = "@candidatePreptime")]
        public string candidatePreptime { get; set; }

        [DataMember(Name = "@otherOverheadTime")]
        public string otherOverheadTime { get; set; }

        [DataMember(Name = "@tpidUsed")]
        public string tpidUsed { get; set; }

        [DataMember(Name = "@matchedCurrency")]
        public string matchedCurrency { get; set; }

        [DataMember(Name = "@matchedLocale")]
        public string matchedLocale { get; set; }
    }

    [DataContract]
    public class ChargeableNightlyRates
    {
        [DataMember(Name = "@baseRate")]
        public string BaseRate { get; set; }

        [DataMember(Name = "@rate")]
        public decimal Rate { get; set; }

        [DataMember(Name = "@promo")]
        public string promo { get; set; }
    }

    [DataContract]
    public class Room
    {
        [DataMember(Name = "numberOfAdults")]
        public int NumberOfAdults { get; set; }

        [DataMember(Name = "numberOfChildren")]
        public int NumberOfChildren { get; set; }
        
        [DataMember(Name = "rateKey")]
        public string RateKey { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }
    
        [DataMember(Name = "bedTypeId")]
        public int BedTypeId { get; set; }
    
        [DataMember(Name = "bedTypeDescription")]
        public string BedTypeDescription { get; set; }

        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<ChargeableNightlyRates>))]
        public List<ChargeableNightlyRates>  ChargeableNightlyRates { get; set; }
    }

    [DataContract]
    public class RoomGroup
    {
        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<Room>))]
        public List<Room> Room { get; set; }

        
    }

    [DataContract]
    public class NightlyRate
    {
        [DataMember(Name = "@baseRate")]
        public string baseRate { get; set; }

        [DataMember(Name = "@rate")]
        public string rate { get; set; }

        [DataMember(Name = "@promo")]
        public string promo { get; set; }
    }

    [DataContract]
    public class NightlyRatesPerRoom
    {
        [DataMember(Name = "@size")]
        public string size { get; set; }

        [DataMember]
        public NightlyRate NightlyRate { get; set; }
    }

    [DataContract]
    public class Surcharge
    {
        [DataMember(Name = "@type")]
        public string type { get; set; }

        [DataMember(Name = "@amount")]
        public string amount { get; set; }
    }

    [DataContract]
    public class Surcharges
    {
        public string size { get; set; }
        public Surcharge Surcharge { get; set; }
    }

    [DataContract]
    public class ChargeableRateInfo
    {
        [DataMember(Name = "@averageBaseRate")]
        public string averageBaseRate { get; set; }

        [DataMember(Name = "@averageRate")]
        public string averageRate { get; set; }

        [DataMember(Name = "@commissionableUsdTotal")]
        public string CommissionableUsdTotal { get; set; }

        [DataMember(Name = "@currencyCode")]
        public string CurrencyCode { get; set; }

        [DataMember(Name = "@maxNightlyRate")]
        public string maxNightlyRate { get; set; }

        [DataMember(Name = "@nightlyRateTotal")]
        public string nightlyRateTotal { get; set; }

        [DataMember(Name = "@surchargeTotal")]
        public string surchargeTotal { get; set; }

        [DataMember(Name = "@total")]
        public string Total { get; set; }

        public NightlyRatesPerRoom NightlyRatesPerRoom { get; set; }
        public Surcharges Surcharges { get; set; }
    }

    [DataContract]
    public class CancelPolicyInfo
    {
        [DataMember]
        public int versionId { get; set; }

        [DataMember]
        public string cancelTime { get; set; }

        [DataMember]
        public int startWindowHours { get; set; }

        [DataMember]
        public int nightCount { get; set; }

        [DataMember]
        public string currencyCode { get; set; }

        [DataMember]
        public string timeZoneDescription { get; set; }

        [DataMember]
        public int? percent { get; set; }
    }

    [DataContract]
    public class CancelPolicyInfoList
    {
        [DataMember]
        public List<CancelPolicyInfo> CancelPolicyInfo { get; set; }
    }

    [DataContract]
    public class HotelFee
    {
        [DataMember(Name = "@description")]
        public string description { get; set; }
        
        [DataMember(Name = "@amount")]
        public string amount { get; set; }
    }

    [DataContract]
    public class HotelFees
    {
        [DataMember(Name = "@size")]
        public string size { get; set; }

        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<HotelFee>))]
        public List<HotelFee> HotelFee { get; set; }
    }

    [DataContract]
    public class RateInfo
    {
        [DataMember(Name = "@priceBreakdown")]
        public string PriceBreakdown { get; set; }

        [DataMember(Name = "@promo")]
        public string Promo { get; set; }

        [DataMember(Name = "@rateChange")]
        public string RateChange { get; set; }
        
        [DataMember]
        public RoomGroup RoomGroup { get; set; }
        
        [DataMember]
        public ChargeableRateInfo ChargeableRateInfo { get; set; }

        [DataMember(Name = "cancellationPolicy")]
        public string CancellationPolicy { get; set; }
        
        [DataMember]
        public CancelPolicyInfoList CancelPolicyInfoList { get; set; }

        [DataMember(Name = "nonRefundable")]
        public bool NonRefundable { get; set; }

        [DataMember(Name = "rateType")]
        public string RateType { get; set; }
        
        [DataMember(Name = "currentAllotment")]
        public int CurrentAllotment { get; set; }
        
        [DataMember]
        public HotelFees HotelFees { get; set; }

        [DataMember(Name = "promoId")]
        public int? PromoId { get; set; }

        [DataMember(Name = "promoDescription")]
        public string PromoDescription { get; set; }

        [DataMember(Name = "promoType")]
        public string PromoType { get; set; }

         [DataMember(Name = "GuaranteeRequired")]
        public bool GuaranteeRequired { get; set; }

        [DataMember(Name = "depositRequired")]
        public bool DepositRequired { get; set; }

        [DataMember(Name = "taxRate")]
        public double TaxRate { get; set; }
    }

    [DataContract]
    public class RateInfos
    {
        [DataMember(Name = "@size")]
        public string size { get; set; }

        [DataMember]
        public RateInfo RateInfo { get; set; }
    }

    [DataContract]
    public class ValueAdd
    {
        [DataMember]
        public string description { get; set; }
    }

    [DataContract]
    public class ValueAdds
    {
        [DataMember(Name = "@size")]
        public string size { get; set; }

        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<ValueAdd>))]
        public List<ValueAdd> ValueAdd { get; set; }
    }

    [DataContract]
    public class BedTypes
    {
        [DataMember(Name = "@size")]
        public string size { get; set; }

        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<BedType>))]
        public List<BedType> BedType { get; set; }
    }

    [DataContract]
    public class BedType
    {
        [DataMember(Name = "@id")]
        public string Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }
    }

    [DataContract]
    public class RoomRateDetails
    {
        [DataMember(Name = "roomTypeCode")]
        public int RoomTypeCode { get; set; }

        [DataMember(Name = "rateCode")]
        public int RateCode { get; set; }

        [DataMember(Name = "maxRoomOccupancy")]
        public int MaxRoomOccupancy { get; set; }

        [DataMember(Name = "quotedRoomOccupancy")]
        public int QuotedRoomOccupancy { get; set; }

        [DataMember]
        public int minGuestAge { get; set; }

        [DataMember(Name = "roomDescription")]
        public string RoomDescription { get; set; }

        [DataMember]
        public bool propertyAvailable { get; set; }

        [DataMember]
        public bool propertyRestricted { get; set; }

        [DataMember(Name = "expediaPropertyId")]
        public int ExpediaPropertyId { get; set; }
        
        [DataMember]
        public RateInfos RateInfos { get; set; }
        
        [DataMember]
        public ValueAdds ValueAdds { get; set; }
        
        [DataMember]
        public BedTypes BedTypes { get; set; }
        
        [DataMember]
        public string smokingPreferences { get; set; }
    }

    [DataContract]
   public class RoomRateDetailsList
    {
        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<RoomRateDetails>))]
        public List<RoomRateDetails> RoomRateDetails { get; set; }
    }

    [DataContract]
    public class HotelSummary
    {
        [DataMember(Name = "@order")]
        public string order { get; set; }

        [DataMember(Name = "@ubsScore")]
        public string ubsScore { get; set; }

        [DataMember(Name = "hotelId")]
        public int HotelId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "address1")]
        public string Address1 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "stateProvinceCode")]
        public string StateProvinceCode { get; set; }

        [DataMember]
        public string postalCode { get; set; }

        [DataMember]
        public string countryCode { get; set; }

        [DataMember]
        public string airportCode { get; set; }

        [DataMember]
        public string supplierType { get; set; }

        [DataMember]
        public int propertyCategory { get; set; }

        [DataMember]
        public double hotelRating { get; set; }

        [DataMember]
        public int confidenceRating { get; set; }

        [DataMember]
        public int amenityMask { get; set; }

        [DataMember]
        public string locationDescription { get; set; }

        [DataMember]
        public string shortDescription { get; set; }

        [DataMember]
        public double highRate { get; set; }

        [DataMember]
        public double lowRate { get; set; }

        [DataMember]
        public string rateCurrencyCode { get; set; }

        [DataMember]
        public double latitude { get; set; }

        [DataMember]
        public double longitude { get; set; }

        [DataMember]
        public double proximityDistance { get; set; }

        [DataMember]
        public string proximityUnit { get; set; }

        [DataMember]
        public bool hotelInDestination { get; set; }

        [DataMember]
        public string thumbNailUrl { get; set; }

        [DataMember]
        public string deepLink { get; set; }
        
        [DataMember]
        public RoomRateDetailsList RoomRateDetailsList { get; set; }
    }

    [DataContract]
    public class HotelList
    {
        [DataMember(Name = "@size")]
        public string Size { get; set; }

        [DataMember(Name = "@activePropertyCount")]
        public string activePropertyCount { get; set; }

        [DataMember]
        [JsonConverter(typeof(SingleValueArrayConverter<HotelSummary>))]
        public List<HotelSummary> HotelSummary { get; set; }
    }

    [DataContract]
    public class HotelListResponse
    {
        [DataMember]
        public string customerSessionId { get; set; }

        [DataMember]
        public int numberOfRoomsRequested { get; set; }

        [DataMember]
        public bool moreResultsAvailable { get; set; }

        [DataMember]
        public string cacheKey { get; set; }

        [DataMember]
        public string cacheLocation { get; set; }

        [DataMember]
        public CachedSupplierResponse cachedSupplierResponse { get; set; }
        
        [DataMember]
        public HotelList HotelList { get; set; }
    }

    [DataContract]
    public class HotelListResponseContainer
    {
        [DataMember]
        public HotelListResponse HotelListResponse { get; set; }
    }

    //--------------------------------------------------------------------------------

    //Start of Avail Response Classes
   
    public class HotelRoomResponse
    {
        [DataMember(Name = "rateCode")]
        public int RateCode { get; set; }

        [DataMember(Name = "roomTypeCode")]
        public int RoomTypeCode { get; set; }

        [DataMember(Name = "rateDescription")]
        public string RateDescription { get; set; }

        [DataMember(Name = "roomTypeDescription")]
        public string RoomTypeDescription { get; set; }

        [DataMember(Name = "supplierType")]
        public string SupplierType { get; set; }

        [DataMember(Name = "propertyId")]
        public int PropertyId { get; set; }

        [DataMember]
        public BedTypes BedTypes { get; set; }

        [DataMember(Name = "smokingPreferences")]
        public string SmokingPreferences { get; set; }

        [DataMember(Name = "rateOccupancyPerRoom")]
        public int RateOccupancyPerRoom { get; set; }

        [DataMember(Name = "quotedOccupancy")]
        public int QuotedOccupancy { get; set; }

        [DataMember(Name = "minGuestAge")]
        public int MinGuestAge { get; set; }

        [DataMember]
        public RateInfos RateInfos { get; set; }

   
        public ValueAdds ValueAdds { get; set; }

        [DataMember(Name = "deepLink")]
        public string DeepLink { get; set; }
    }

    public class HotelRoomAvailabilityResponse
    {
        [DataMember(Name = "@size")]
        public string Size { get; set;}

        [DataMember(Name = "customerSessionId")]
        public string CustomerSessionId { get; set; }

        [DataMember(Name = "hotelId")]
        public int HotelId { get; set; }

        [DataMember(Name = "arrivalDate")]
        public string ArrivalDate { get; set; }

        [DataMember(Name = "departureDate")]
        public string DepartureDate { get; set; }


        [DataMember(Name = "hotelName")]
        public string HotelName { get; set; }

        [DataMember(Name = "hotelAddress")]
        public string HotelAddress { get; set; }

        [DataMember(Name = "hotelCity")]
        public string HotelCity { get; set; }

        [DataMember(Name = "hotelStateProvince")]
        public string HotelStateProvince { get; set; }

        [DataMember(Name = "hotelCountry")]
        public string HotelCountry { get; set; }

        [DataMember(Name = "numberOfRoomsRequested")]
        public int NumberOfRoomsRequested { get; set; }

        [DataMember(Name = "checkInInstructions")]
        public string CheckInInstructions { get; set; }


        public HotelRoomResponse HotelRoomResponse { get; set; }
    }

    public class HotelRoomAvailabilityResponseContainer
    {
        [DataMember]
        public HotelRoomAvailabilityResponse HotelRoomAvailabilityResponse { get; set; }
    }


    /*
     * Start of Autogenerated Classes for HotelRoomReservationResponse
     */

    public class ServerInfo
    {
        [DataMember(Name = "@instance")]
        public string Instance { get; set; }

        [DataMember(Name = "@timestamp")]
        public string Timestamp { get; set; }

        [DataMember(Name = "@serverTime")]
        public string ServerTime { get; set; }
    }

    public class EanWsError
    {
        [DataMember(Name = "itineraryId")]
        public int ItineraryId { get; set; }

        [DataMember(Name = "handling")]
        public string Handling { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "exceptionConditionId")]
        public int ExceptionConditionId { get; set; }

        [DataMember(Name = "presentationMessage")]
        public string PresentationMessage { get; set; }

        [DataMember(Name = "verboseMessage")]
        public string VerboseMessage { get; set; }

        [DataMember]
        public ServerInfo ServerInfo { get; set; }
    }


    public class HotelRoomReservationResponse
    {
        [DataMember]
        public EanWsError EanWsError { get; set; }

        [DataMember(Name = "customerSessionId")]
        public string CustomerSessionId { get; set; }

        [DataMember(Name = "itineraryId")]
        public int ItineraryId { get; set; }

        [DataMember(Name = "confirmationNumbers")]
        [JsonConverter(typeof(SingleValueArrayConverter<string>))]
        public List<string> ConfirmationNumbers { get; set; }

        [DataMember(Name = "processedWithConfirmation")]
        public bool ProcessedWithConfirmation { get; set; }

        [DataMember(Name = "supplierType")]
        public string SupplierType { get; set; }

        [DataMember(Name = "reservationStatusCode")]
        public string ReservationStatusCode { get; set; }

        [DataMember(Name = "existingItinerary")]
        public bool ExistingItinerary { get; set; }

        [DataMember(Name = "numberOfRoomsBooked")]
        public int NumberOfRoomsBooked { get; set; }

        [DataMember(Name = "drivingDirections")]
        public string DrivingDirections { get; set; }

        [DataMember(Name = "checkInInstructions")]
        public string CheckInInstructions { get; set; }

        [DataMember(Name = "arrivalDate")]
        public string ArrivalDate { get; set; }


        [DataMember(Name = "departureDate")]
        public string DepartureDate { get; set; }

        [DataMember(Name = "hotelName")]
        public string HotelName { get; set; }

        [DataMember(Name = "hotelAddress")]
        public string HotelAddress { get; set; }

        [DataMember(Name = "hotelCity")]
        public string HotelCity { get; set; }

        [DataMember(Name = "hotelStateProvinceCode")]
        public string HotelStateProvinceCode { get; set; }

        [DataMember(Name = "hotelPostalCode")]
        public string HotelPostalCode { get; set; }

        [DataMember(Name = "hotelCountryCode")]
        public string HotelCountryCode { get; set; }

        [DataMember(Name = "roomDescription")]
        public string RoomDescription { get; set; }

        [DataMember(Name = "rateOccupancyPerRoom")]
        public int RateOccupancyPerRoom { get; set; }

        [DataMember]
        public RateInfos RateInfos { get; set; }

        [DataMember]
        public ValueAdds ValueAdds { get; set; }
    }

    public class HotelRoomReservationResponseContainer
    {
        public HotelRoomReservationResponse HotelRoomReservationResponse { get; set; }
    }

    /*      Start of Autogenerated classes for Cancel Response
     */

    public class HotelRoomCancellationResponse
    {
        [DataMember(Name = "customerSessionId")]
        public string CustomerSessionId { get; set; }
        
        [DataMember]
        public EanWsError EanWsError { get; set; }

        [DataMember(Name = "cancellationNumber")]
        public string CancellationNumber { get; set; }
    }

    public class HotelRoomCancellationResponseContainer
    {
        public HotelRoomCancellationResponse HotelRoomCancellationResponse { get; set; }
    }
}

