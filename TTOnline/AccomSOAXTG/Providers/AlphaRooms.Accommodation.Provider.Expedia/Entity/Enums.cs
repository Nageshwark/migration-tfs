﻿using System;

namespace AlphaRooms.Accommodation.Provider.Expedia.Entity
{
    [Flags]
    public enum RequestType
    {
        General,
        Opaque,
        HotelCollect
    }
}