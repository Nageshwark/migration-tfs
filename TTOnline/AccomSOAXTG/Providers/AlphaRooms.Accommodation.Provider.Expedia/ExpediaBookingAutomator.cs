﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<ExpediaBookingResponse>>
    {
        private const string ExpediaBookingUrl = "ExpediaBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IExpediaProviderBookingRequestFactory ExpediaProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public ExpediaBookingAutomator(ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            IExpediaProviderBookingRequestFactory ExpediaSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger,
                                            IProviderLoggerService loggerService)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ExpediaProviderBookingRequestFactory = ExpediaSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<ExpediaBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ExpediaBookingResponse> responses = new List<ExpediaBookingResponse>();

            string supplierRequest = null;
            ExpediaBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.ExpediaProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse);
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ExpediaBookingAutomator.GetBookingResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<ExpediaBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, string supplierRequest)
        {
            WebScrapeClient webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationJson };

            string url = request.Provider.Parameters.GetParameterValue(ExpediaBookingUrl);
            string urlWithParameters = url + supplierRequest;

            this.loggerService.SetProviderRequest(request, urlWithParameters);
            WebScrapeResponse response = await webScrapeClient.PostAsync(urlWithParameters, "");
            this.loggerService.SetProviderResponse(request, response);
            outputLogger.LogBookingResponse(request, ProviderOutputFormat.Json, response);

            ExpediaBookingResponse supplierResponse = new ExpediaBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response.Value
            };

            return supplierResponse;
        }
    }
}
