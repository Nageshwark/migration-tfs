﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;
using AlphaRooms.Accommodation.Provider.Expedia.Helper;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaAvailabilityParser : IAccommodationAvailabilityParser<IEnumerable<ExpediaAvailabilityResponse>>
    {

        private readonly ILogger logger;
        private JsonSerializerSettings settings;
        public ExpediaAvailabilityParser(ILogger logger)

        {
            this.logger = logger;

        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, IEnumerable<ExpediaAvailabilityResponse> responses)
        {
            var availabilityResults = new BlockingCollection<AccommodationProviderAvailabilityResult>();
            var exceptions = new BlockingCollection<Exception>();

            var actions = responses.Select(response =>
            (Action)(() =>
             CreateAvailabilityResultsFromResponse(request,response, availabilityResults,exceptions)))
           .ToArray();

            Parallel.Invoke(actions);

            if (exceptions.Any())
                exceptions.ForEach((exception) =>
                logger.Error(exception.GetMessageWithInnerExceptions()));

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
            ExpediaAvailabilityResponse supplierResponse,
            BlockingCollection<AccommodationProviderAvailabilityResult> availabilityResults,
            BlockingCollection<Exception> exceptions)
        {
            var response = Deserialize(supplierResponse.SupplierResponse);
            Func<string, string> getRoomDescription = (roomDescripton) => !roomDescripton.Contains("Refundable") ? $"{roomDescripton} - Non Refundable" : roomDescripton;
            var hasResults = (response?.HotelListResponse?.HotelList != null && Convert.ToInt32(response.HotelListResponse.HotelList.Size) > 0);
            if (!hasResults) { exceptions.Add(new SupplierApiException("No Results found")); return; }
            var hotels = response.HotelListResponse?.HotelList?.HotelSummary;
            var parallelPartioner = Partitioner.Create(hotels, true);
            var boardBasisHelper = new BoardBasisHelper();

            try
            {
                Parallel.ForEach(parallelPartioner, (hotel) =>
                {
                    foreach (var roomRateDetails in hotel.RoomRateDetailsList.RoomRateDetails)
                    {
                        var rateInfo = roomRateDetails.RateInfos.RateInfo;
                        var roomCount = rateInfo.RoomGroup.Room.Count;
                        var rooms = rateInfo.RoomGroup.Room;
                        int roomCounter = 1;
                        foreach (var room in rooms)
                        {

                            var result = new AccommodationProviderAvailabilityResult
                            {
                                RoomNumber = (byte)roomCounter,
                                ProviderEdiCode = request.Provider.EdiCode,
                                SupplierEdiCode = hotel.supplierType,
                                CheckInDate = request.CheckInDate,
                                CheckOutDate = request.CheckOutDate,
                                PaymentModel = supplierResponse.SupplierRequest.IsHotelCollect
                                    ? PaymentModel.CustomerPayDirect
                                    : PaymentModel.PostPayment,
                                DestinationEdiCode = hotel.City,
                                EstablishmentEdiCode = hotel.HotelId.ToString(),
                                EstablishmentName = hotel.Name,
                                IsNonRefundable = rateInfo.NonRefundable
                            };

                            //If the original request was a hotel collect request, this is a pay direct


                            result.RoomDescription = result.IsNonRefundable
                                ? getRoomDescription(roomRateDetails.RoomDescription)
                                : roomRateDetails.RoomDescription;

                            result.RoomCode = roomRateDetails.RoomTypeCode.ToString();
                            result.CheckInDate = request.CheckInDate;
                            result.CheckOutDate = request.CheckOutDate;

                            result.Adults = (byte)room.NumberOfAdults;
                            result.Children = (byte)room.NumberOfChildren;
                            var bbDescription = boardBasisHelper.GetBoardBasis(roomRateDetails.ValueAdds);

                            if (bbDescription != null)
                            {
                                result.BoardCode = bbDescription.Code;
                                result.BoardDescription = bbDescription.Name.Equals("All-Inclusive")
                                    ? "All Inclusive"
                                    : bbDescription.Name;
                            }


                            decimal surcharge = Convert.ToDecimal(rateInfo.ChargeableRateInfo.surchargeTotal);
                            decimal total = GetRoomTotal(room.ChargeableNightlyRates, surcharge, roomCount);
                            string currencyCode = rateInfo.ChargeableRateInfo.CurrencyCode;



                            result.SalePrice = new Money()
                            {
                                Amount = total,
                                CurrencyCode = currencyCode
                            };

                            //what should we populate here? there is only one price in H4U response.
                            result.CostPrice = result.SalePrice;

                            result.NumberOfAvailableRooms = rateInfo.CurrentAllotment;

                            result.RateType = RateType.GrossStandard;

                            result.ProviderSpecificData = new Dictionary<string, string>
                            {
                                {
                                    "RateCode", roomRateDetails.RateCode.ToString(CultureInfo.InvariantCulture)
                                },
                                {
                                    "MaxRoomOccupancy", roomRateDetails.MaxRoomOccupancy.ToString()
                                },
                                {
                                    "QuotedRoomOccupancy", roomRateDetails.QuotedRoomOccupancy.ToString()
                                },
                                {
                                    "ExpediaPropertyId", roomRateDetails.ExpediaPropertyId.ToString()
                                },
                                {
                                    "RateKey", room.RateKey
                                },
                            };

                            //If the original request was an Opaque rate request, the price is an opaque rate
                            if (supplierResponse.SupplierRequest.IsOpaque)
                            {
                                result.IsOpaqueRate = true;
                            }
                            availabilityResults.Add(result);
                            roomCounter++;
                        }
                    }
                });

            }
            catch (Exception exception)
            {
                exceptions.Add(exception);
            }

        }

        private decimal GetRoomTotal(IEnumerable<ChargeableNightlyRates> rates, decimal surcharge, int roomCount)
        {
            return rates.Sum(r => r.Rate) + (surcharge / roomCount);
        }

        private HotelListResponseContainer Deserialize(string response)
        {
            settings = settings ?? new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
            var root = JsonConvert.DeserializeObject<HotelListResponseContainer>(response, settings);
            return root;
        }


    }
}
