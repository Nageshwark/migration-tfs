﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaCancellationAutomator : IAccommodationCancellationAutomatorAsync<ExpediaCancellationResponse>
    {
        private const string ExpediaCancellationUrl = "ExpediaCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string ExpediaUsername = "ExpediaUsername";
        private const string ExpediaPassword = "ExpediaPassword";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IExpediaProviderCancellationRequestFactory expediaProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private string ExpediaCancellationTimeOut;

        public ExpediaCancellationAutomator(ILogger logger,
                                            IExpediaProviderCancellationRequestFactory ExpediaSupplierCancellationRequestFactory
                                           
            )
        {
            this.logger = logger;
            this.expediaProviderCancellationRequestFactory = ExpediaSupplierCancellationRequestFactory;
            
        }
        public async Task<ExpediaCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ExpediaCancellationResponse response = null;

            try
            {
                var supplierRequest = expediaProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

                string url = request.Provider.Parameters.GetParameterValue(ExpediaCancellationUrl);
                string urlWithParams = url + supplierRequest;

                WebScrapeResponse supplierResponse = await webScrapeClient.GetAsync(urlWithParams);
                
                response = new ExpediaCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse.Value
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }
                logger.Error(sb.ToString());
                throw new SupplierApiException(string.Format("ExpediaBookingAutomator.GetCancellationResponseAsync: Expedia supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        
    }
}
