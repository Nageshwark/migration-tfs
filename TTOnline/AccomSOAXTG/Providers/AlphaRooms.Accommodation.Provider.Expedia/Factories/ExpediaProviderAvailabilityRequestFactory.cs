﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using System;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Expedia.Entity;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaProviderAvailabilityRequestFactory : IExpediaProviderAvailabilityRequestFactory
    {
        private readonly IExpediaParameterService parameterService;
        private readonly IExpediaApiKeyHelper apiKeyHelper;
        private readonly ILogger logger;

        public ExpediaProviderAvailabilityRequestFactory(IExpediaParameterService parameterService, 
            IExpediaApiKeyHelper apiKeyHelper,
            ILogger logger)
        {
            this.parameterService = parameterService;
            this.apiKeyHelper = apiKeyHelper;
            this.logger = logger;
        }

        /// <summary>
        /// Note:  Limitations for HotelValuedAvail request:
        /// A maximum of 31 nights per hotel service can be requested.
        /// A maximum of 5 rooms per hotel service can be requested.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExpediaAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            return CreateProviderAvailabilityRequest(request, RequestType.General);
        }

        public ExpediaAvailabilityRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request, RequestType requestType)
        {
            //initialize parameter service with parameters
            parameterService.InitializeParameters(request.Provider.Parameters);

            ExpediaAvailabilityRequest availabilityRequest = new ExpediaAvailabilityRequest()
            {
                IsHotelCollect = requestType.HasFlag(RequestType.HotelCollect),
                IsOpaque = requestType.HasFlag(RequestType.Opaque)
            };
            string supplierRequest, apiKey, cid, apiSecret;

            StringBuilder sbParameters = new StringBuilder();
            
            //Get API Key, Customer Id & Secret based on general/opaque/hotel collect request
            
            parameterService.GetCredentials(requestType, out cid, out apiKey, out apiSecret);

            sbParameters.AppendFormat("?apiKey={0}", apiKey);
            sbParameters.AppendFormat("&cid={0}", cid);
           
            if (!requestType.HasFlag(RequestType.Opaque)) sbParameters.AppendFormat("&customerUserAgent={0}", "MOBILE_SITE");//request.Provider.Parameters.GetParameterValue(ExpediaUserAgent));
            sbParameters.AppendFormat("&customerSessionId={0}", Guid.NewGuid());// request.Provider.Parameters.GetParameterValue(ExpediaSessionId));
            sbParameters.AppendFormat("&locale={0}", parameterService.Locale);

            //Use the currency code from channel
            string currencyCode = request.ChannelInfo.CurrencyCode;

            //if the channel currency is not available, use the one from expedia parameters
            if (string.IsNullOrEmpty(currencyCode))
                currencyCode = parameterService.CurrencyCode;
            sbParameters.AppendFormat("&currencyCode={0}", currencyCode);

            if (request.DestinationCodes != null && request.DestinationCodes.Any())
            {
                if (request.DestinationCodes[0].IndexOf(";") > -1)
                {
                    sbParameters.AppendFormat("&countryCode={0}", request.DestinationCodes[0].Split(';')[0]);
                    sbParameters.AppendFormat("&destinationId={0}", request.DestinationCodes[0].Split(';')[1]);
                }
                else
                {
                    sbParameters.AppendFormat("&destinationId={0}", request.DestinationCodes[0]);
                }
            }
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
            {
                sbParameters.AppendFormat("&hotelIdList={0}", string.Join(",", request.EstablishmentCodes));
            }

            sbParameters.AppendFormat("&arrivalDate={0}", request.CheckInDate.ToString("MM/dd/yyyy"));
            sbParameters.AppendFormat("&departureDate={0}", request.CheckOutDate.ToString("MM/dd/yyyy"));
            sbParameters.Append(GetRoomPaxString(request.Rooms));

            sbParameters.AppendFormat("&minorRev={0}", parameterService.MinorRevision);
            sbParameters.AppendFormat("&numberOfResults={0}", parameterService.MaxNumberOfHotel);
            sbParameters.AppendFormat("&maxRatePlanCount=99");
            sbParameters.Append("&includeDetails=true");
            if (requestType.HasFlag(RequestType.Opaque))
            {
                sbParameters.Append("&rateType=MerchantPackage");
            }
            if (requestType.HasFlag(RequestType.HotelCollect))
            {
                sbParameters.Append("&rateType=DirectAgency");
            }

            sbParameters.AppendFormat("&sig={0}", apiKeyHelper.GetSignature(apiKey, apiSecret));

            supplierRequest = sbParameters.ToString();

            availabilityRequest.APIParameters = supplierRequest;

            logger.Debug("Request: {0} \nParameters:{1}", requestType, supplierRequest);

            return availabilityRequest;
        }

        private string GetRoomPaxString(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            StringBuilder roomPaxBuilder = new StringBuilder();

            int roomCounter = 1;
            foreach (var room in rooms)
            {
                roomPaxBuilder.AppendFormat("&room{0}=", roomCounter);
                roomPaxBuilder.AppendFormat("{0}", room.Guests.AdultsCount);

                foreach (var age in room.Guests.ChildAndInfantAges)
                {
                    roomPaxBuilder.AppendFormat(",{0}", age);
                }

                roomCounter++;
            }

            return roomPaxBuilder.ToString();
        }

        private string CreateRoomRequests(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            return "";
        }



        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;


        }

    }
}
