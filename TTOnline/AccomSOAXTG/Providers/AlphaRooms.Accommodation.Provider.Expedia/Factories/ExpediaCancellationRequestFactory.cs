﻿using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Expedia.Helper;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Expedia.Factories
{
    public class ExpediaCancellationRequestFactory : IExpediaProviderCancellationRequestFactory
    {
        private const string ExpediaAPIKey = "ExpediaAPIKey";
        private const string ExpediaCID = "ExpediaCID";
        private const string ExpediaSessionId = "ExpediaSessionId";
        private const string ExpediaLocale = "ExpediaLocale";
        private const string ExpediaCurrencyCode = "ExpediaCurrencyCode";
        private const string ExpediaSecret = "ExpediaSecret";
        private const string ExpediaMinorRevision = "ExpediaMinorRevision";
        private const string ExpediaApiExperience = "ExpediaApiExperience";

        public string CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            string supplierRequest;

            StringBuilder sbParameters = new StringBuilder();
            

            sbParameters.AppendFormat("?apiKey={0}", request.Provider.Parameters.GetParameterValue(ExpediaAPIKey));
            sbParameters.AppendFormat("&cid={0}", request.Provider.Parameters.GetParameterValue(ExpediaCID));
            //sbParameters.AppendFormat("&customerIpAddress={0}", request.Provider.Parameters.GetParameterValue(ExpediaCustomerIP));
            //sbParameters.AppendFormat("&customerUserAgent={0}", request.Provider.Parameters.GetParameterValue(ExpediaUserAgent));
            sbParameters.AppendFormat("&apiExperience={0}", request.Provider.Parameters.GetParameterValue(ExpediaApiExperience));
            sbParameters.AppendFormat("&customerSessionId={0}", request.Provider.Parameters.GetParameterValue(ExpediaSessionId));
            sbParameters.AppendFormat("&locale={0}", request.Provider.Parameters.GetParameterValue(ExpediaLocale));
            sbParameters.AppendFormat("&currencyCode={0}", request.Provider.Parameters.GetParameterValue(ExpediaCurrencyCode));
            sbParameters.AppendFormat("&minorRev={0}", request.Provider.Parameters.GetParameterValue(ExpediaMinorRevision));
            sbParameters.AppendFormat("&itineraryId={0}", request.ProviderBookingReference);
            sbParameters.AppendFormat("&confirmationNumber={0}", request.ProviderSpecificData["ConfirmationNumber"]);
            sbParameters.AppendFormat("&email={0}", request.ProviderSpecificData["Email"]);
            

            sbParameters.AppendFormat("&sig={0}", GetSignature(request));

            supplierRequest = sbParameters.ToString();

            return supplierRequest;
        }

        private string GetSignature(AccommodationProviderCancellationRequest request)
        {
            APIKeyHelper helper = new APIKeyHelper();

            string apiKey = request.Provider.Parameters.GetParameterValue(ExpediaAPIKey);
            string apiSecret = request.Provider.Parameters.GetParameterValue(ExpediaSecret);

            return helper.GetSignature(apiKey, apiSecret);
        }
    }
}
