﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities.CustomExceptions;
using RequestType = AlphaRooms.Accommodation.Provider.Expedia.Entity.RequestType;

namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaProviderBookingRequestFactory : IExpediaProviderBookingRequestFactory
    {
        private readonly IExpediaParameterService parameterService;
        private readonly IExpediaApiKeyHelper apiKeyHelper;

        public ExpediaProviderBookingRequestFactory(IExpediaParameterService parameterService, IExpediaApiKeyHelper apiKeyHelper)
        {
            this.parameterService = parameterService;
            this.apiKeyHelper = apiKeyHelper;
        }

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            //initialize parameter service with parameters
            parameterService.InitializeParameters(request.Provider.Parameters);

            string supplierRequest = "";

            StringBuilder sbParameters = new StringBuilder();

            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms[0].ValuationResult;
            var requestType = roomToBook.PaymentModel == PaymentModel.CustomerPayDirect
              ? RequestType.HotelCollect
              : RequestType.General;
            //get credentials
            string apiKey, cid, apiSecret;
            parameterService.GetCredentials(requestType, out cid, out apiKey, out apiSecret);

            sbParameters.AppendFormat("?apiKey={0}", apiKey);
            sbParameters.AppendFormat("&apiExperience={0}", parameterService.ApiExperience);
            sbParameters.AppendFormat("&cid={0}", cid);
            sbParameters.AppendFormat("&customerSessionId={0}", Guid.NewGuid());//request.Provider.Parameters.GetParameterValue(ExpediaSessionId));
            sbParameters.AppendFormat("&locale={0}", parameterService.Locale);

            //Use the currencyCode sent by Expedia in Valuation
            string currencyCode = roomToBook.ProviderSpecificData["CurrencyCode"];

            sbParameters.AppendFormat("&currencyCode={0}", currencyCode);
            sbParameters.AppendFormat("&hotelId={0}", roomToBook.EstablishmentEdiCode);
            sbParameters.AppendFormat("&arrivalDate={0}", roomToBook.CheckInDate.ToString("MM/dd/yyyy"));
            sbParameters.AppendFormat("&departureDate={0}", roomToBook.CheckOutDate.ToString("MM/dd/yyyy"));
            sbParameters.AppendFormat("&roomTypeCode={0}", roomToBook.RoomCode);
            sbParameters.AppendFormat("&rateCode={0}", roomToBook.ProviderSpecificData["RateCode"]);
            sbParameters.AppendFormat("&rateKey={0}", roomToBook.ProviderSpecificData["RateKey"]);
            sbParameters.AppendFormat("&supplierType={0}", roomToBook.ProviderSpecificData["SupplierType"]);
            if (requestType.HasFlag(RequestType.HotelCollect))
            {
                sbParameters.Append("&rateType=DirectAgency");
            }
            int roomCounter = 1;
            foreach (var room in request.ValuatedRooms)
            {
                sbParameters.AppendFormat("&room{0}FirstName={1}", roomCounter, request.Customer.FirstName);
                sbParameters.AppendFormat("&room{0}LastName={1}", roomCounter, request.Customer.Surname);
                sbParameters.AppendFormat("&room{0}BedTypeId={1}", roomCounter, room.ValuationResult.ProviderSpecificData["BedTypeId"]);

                roomCounter++;
            }

            sbParameters.Append(GetRoomPaxSting(request.ValuatedRooms));

            sbParameters.Append(CreateReservationParameters(request));


            sbParameters.AppendFormat("&minorRev={0}", parameterService.MinorRevision);
            sbParameters.AppendFormat("&affiliateConfirmationId={0}", Guid.NewGuid());//request.Provider.Parameters.GetParameterValue(ExpediaBookingReference)); //GUID to prevent duplicate bookings
            sbParameters.AppendFormat("&sendReservationEmail={0}", parameterService.SendReservationEmail);
            sbParameters.AppendFormat("&chargeableRate={0}", roomToBook.ProviderSpecificData["BookingTotal"]);

            sbParameters.AppendFormat("&sig={0}", apiKeyHelper.GetSignature(apiKey, apiSecret));

            supplierRequest = sbParameters.ToString();

            return supplierRequest;
        }

        private string CreateReservationParameters(AccommodationProviderBookingRequest request)
        {
            StringBuilder sbParameters = new StringBuilder();
            sbParameters.AppendFormat("&email={0}", parameterService.Email);
            sbParameters.AppendFormat("&homePhone={0}", parameterService.HomePone);
            var card = request.ValuatedRooms.First().PaymentDetails;
            if (card != null) sbParameters.AppendFormat("&firstName={0}", card.CardHolderName);
            else sbParameters.AppendFormat("&firstName={0}", request.Customer.FirstName);
            var paymentModel = request.ValuatedRooms.First().ValuationResult.PaymentModel;
            if (card == null && paymentModel == PaymentModel.CustomerPayDirect) throw new SupplierApiException("Paydirect booking: No card details found");

            if (paymentModel == PaymentModel.CustomerPayDirect)
            {

                sbParameters.AppendFormat("&lastName={0}", parameterService.LastName);
                sbParameters.AppendFormat("&creditCardType={0}", SetCardType(card.CardType));
                sbParameters.AppendFormat("&creditCardNumber={0}", card.CardNumber);
                sbParameters.AppendFormat("&creditCardIdentifier={0}", card.CardSecurityCode);
                sbParameters.AppendFormat("&creditCardExpirationMonth={0}", card.CardExpireDate.Month);
                sbParameters.AppendFormat("&creditCardExpirationYear={0}", card.CardExpireDate.Year);
                sbParameters.AppendFormat("&address1={0}", card.AddressLine1);
                sbParameters.AppendFormat("&city={0}", card.City);
                sbParameters.AppendFormat("&stateProvinceCode={0}", "");
                sbParameters.AppendFormat("&countryCode={0}", card.Country);
                sbParameters.AppendFormat("&postalCode={0}", card.Postcode);
            }

            else
            {

                //We use Expedia card details to pay supplier
                sbParameters.AppendFormat("&lastName={0}", parameterService.LastName);
                sbParameters.AppendFormat("&creditCardType={0}", parameterService.CreditCardType);
                sbParameters.AppendFormat("&creditCardNumber={0}", parameterService.CreditCardNumber);
                sbParameters.AppendFormat("&creditCardIdentifier={0}", parameterService.CreditCardIdentifier);
                sbParameters.AppendFormat("&creditCardExpirationMonth={0}", DateTime.Today.AddMonths(9).ToString("MM"));
                sbParameters.AppendFormat("&creditCardExpirationYear={0}", DateTime.Today.AddMonths(9).ToString("yyyy"));

                sbParameters.AppendFormat("&address1={0}", parameterService.Address1);
                sbParameters.AppendFormat("&city={0}", parameterService.City);
                sbParameters.AppendFormat("&stateProvinceCode={0}", parameterService.StateProvinceCode);
                sbParameters.AppendFormat("&countryCode={0}", parameterService.CountryCode);
                sbParameters.AppendFormat("&postalCode={0}", parameterService.PostalCode);

            }

            return sbParameters.ToString();
        }

        private string SetCardType(CardType cardType)
        {
            switch (cardType)
            {
                case CardType.VisaCredit:
                case CardType.VisaDebit:
                case CardType.VisaDelta:
                case CardType.VisaPurchasing:
                    return "VI";

                case CardType.MasterCard:
                case CardType.MastercardDebit:
                    return "CA";

                case CardType.Discover:
                    return "DS";

                case CardType.DinersClub:
                    return "DC";

                case CardType.VisaElectron:
                    return "E";

                case CardType.JCB:
                    return "JC";

                case CardType.Maestro:
                    return "TO";



                default:
                    throw new Exception("Card Type Delta not supported");



                    /*
                 * VI = Visa
                    AX = American Express --
                    BC = BC Card
                    CA = MasterCard
                    DS = Discover
                    DC = Diners Club
                    T = Carta Si
                    R = Carte Bleue
                    E = Electron
                    JC = Japan Credit Bureau
                    TO = Maestro
                 */
            }
        }
        //private string GetSignature(AccommodationProvider provider)
        //{
        //    APIKeyHelper helper = new APIKeyHelper();

        //    string apiKey = provider.Parameters.GetParameterValue(ExpediaAPIKey);
        //    string apiSecret = provider.Parameters.GetParameterValue(ExpediaSecret);

        //    return helper.GetSignature(apiKey, apiSecret);
        //}

        private string GetRoomPaxSting(AccommodationProviderBookingRequestRoom[] rooms)
        {
            StringBuilder roomPaxBuilder = new StringBuilder();

            int roomCounter = 1;
            foreach (var room in rooms)
            {
                roomPaxBuilder.AppendFormat("&room{0}=", roomCounter);
                roomPaxBuilder.AppendFormat("{0}", room.Guests.AdultsCount);

                foreach (var age in room.Guests.ChildAndInfantAges)
                {
                    roomPaxBuilder.AppendFormat(",{0}", age);
                }

                roomCounter++;
            }

            return roomPaxBuilder.ToString();
        }
    }
}
