﻿namespace AlphaRooms.Accommodation.Provider.Expedia
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
    using System;
    using System.Linq;
    using System.Text;
    using AlphaRooms.Accommodation.Provider.Expedia.Entity;
    using AlphaRooms.SOACommon.Contracts.Enumerators;

    public class ExpediaProviderValuationRequestFactory : IExpediaProviderValuationRequestFactory
    {
        private readonly IExpediaParameterService parameterService;
        private readonly IExpediaApiKeyHelper apiKeyHelper;

        public ExpediaProviderValuationRequestFactory(IExpediaParameterService parameterService, IExpediaApiKeyHelper apiKeyHelper)
        {
            this.parameterService = parameterService;
            this.apiKeyHelper = apiKeyHelper;
        }

        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            //initialize parameter service with parameters
            parameterService.InitializeParameters(request.Provider.Parameters);

            string supplierRequest = "";

            StringBuilder sbParameters = new StringBuilder();

            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;
            var requestType = availabilityResult.PaymentModel == PaymentModel.CustomerPayDirect
                ? RequestType.HotelCollect
                : RequestType.General;

            //get credentials
            string apiKey, cid, apiSecret;
            parameterService.GetCredentials(requestType, out cid, out apiKey, out apiSecret);
            sbParameters.AppendFormat("?apiKey={0}", apiKey);
            sbParameters.AppendFormat("&apiExperience={0}", parameterService.ApiExperience);
            sbParameters.AppendFormat("&cid={0}", cid);
            sbParameters.AppendFormat("&customerSessionId={0}", Guid.NewGuid());
            sbParameters.AppendFormat("&locale={0}", parameterService.Locale);

            //Use the currency code from channel
            string currencyCode = request.ChannelInfo.CurrencyCode;

            //if the channel currency is not available, use the one from expedia parameters
            if (string.IsNullOrEmpty(currencyCode))
                currencyCode = parameterService.CurrencyCode;
            sbParameters.AppendFormat("&currencyCode={0}", currencyCode);

            sbParameters.AppendFormat("&hotelId={0}", availabilityResult.ProviderEstablishmentCode);
            sbParameters.AppendFormat("&arrivalDate={0}", availabilityResult.CheckInDate.ToString("MM/dd/yyyy"));
            sbParameters.AppendFormat("&departureDate={0}", availabilityResult.CheckOutDate.ToString("MM/dd/yyyy"));
            
            sbParameters.AppendFormat("&roomTypeCode={0}", availabilityResult.RoomCode);
            sbParameters.AppendFormat("&rateCode={0}", availabilityResult.ProviderSpecificData["RateCode"]);
            sbParameters.AppendFormat("&rateKey={0}", availabilityResult.ProviderSpecificData["RateKey"]);

            sbParameters.Append(GetRoomPaxSting(request.SelectedRooms));

            sbParameters.AppendFormat("&minorRev={0}", parameterService.MinorRevision);
            sbParameters.Append("&includeDetails=true");  

            sbParameters.AppendFormat("&sig={0}", apiKeyHelper.GetSignature(apiKey, apiSecret));
            if (requestType.HasFlag(RequestType.HotelCollect))
            {
                sbParameters.Append("&rateType=DirectAgency");
            }
            supplierRequest = sbParameters.ToString();

            return supplierRequest;
        }

        private string GetRoomPaxSting(AccommodationProviderValuationRequestRoom [] rooms)
        {
            StringBuilder roomPaxBuilder = new StringBuilder();

            int roomCounter = 1;
            foreach (var room in rooms)
            {
                roomPaxBuilder.AppendFormat("&room{0}=", roomCounter);
                roomPaxBuilder.AppendFormat("{0}", room.Guests.AdultsCount);

                foreach (var age in room.Guests.ChildAndInfantAges)
                {
                    roomPaxBuilder.AppendFormat(",{0}", age);
                }
                roomCounter++;
            }
            return roomPaxBuilder.ToString();
        }
    }
}
