﻿
namespace AlphaRooms.Accommodation.Provider.Expedia
{
    public class ExpediaCancellationResponse
    {
        public string SupplierRequest { get; set; }
        public string SupplierResponse { get; set; }
    }
}
