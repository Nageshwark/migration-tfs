﻿using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelCancelService;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelCancellationResponse
    {
        public OTA_CancelServiceOTA_CancelRQ SupplierRequest { get; set; }
        public OTA_CancelServiceResponseOTA_CancelRS SupplierResponse { get; set; }
    }
}
