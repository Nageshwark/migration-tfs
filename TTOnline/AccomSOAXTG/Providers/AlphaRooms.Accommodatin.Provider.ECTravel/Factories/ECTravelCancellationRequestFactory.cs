﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelCancelService;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Factories
{
    public class ECTravelCancellationRequestFactory : IECTravelProviderCancellationRequestFactory
    {
        private const string ECTravelUsername = "ECTravelUsername";
        private const string ECTravelPassword = "ECTravelPassword";
        private const string ECTravelPrimaryLanguage = "ECTravelPrimaryLanguage";

        public OTA_CancelServiceOTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new OTA_CancelServiceOTA_CancelRQ
            {
                POS = new[]
                {
                    new SourceType
                    {
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(ECTravelUsername),
                        RequestorID = new SourceTypeRequestorID
                        {
                            MessagePassword = request.Provider.Parameters.GetParameterValue(ECTravelPassword)
                        }
                    }
                },
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(ECTravelPrimaryLanguage),
                UniqueID = new[]
                {
                    new OTA_CancelServiceOTA_CancelRQUniqueID
                    {
                        ID = request.ProviderBookingReference
                    }
                }

            };

            return supplierRequest;
        }
    }
}
