﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelService;

using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Factories
{
    public class ECTravelValuationRequestFactory : IECTravelProviderValuationRequestFactory
    {
        private const string ECTravelUsername = "ECTravelUsername";
        private const string ECTravelPassword = "ECTravelPassword";
        private const string ECTravelPrimaryLanguage = "ECTravelPrimaryLanguage";

        public OTA_HotelAvailServiceOTA_HotelAvailRQ  CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var supplierRequest = new OTA_HotelAvailServiceOTA_HotelAvailRQ
            {
                POS = new[]
                {
                    new SourceType
                    {
                        AgentDutyCode = request.Provider.Parameters.GetParameterValue(ECTravelUsername),
                        RequestorID = new SourceTypeRequestorID
                        {
                            MessagePassword = request.Provider.Parameters.GetParameterValue(ECTravelPassword)
                        }
                    }
                },
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(ECTravelPrimaryLanguage),
                AvailRequestSegments = new OTA_HotelAvailServiceOTA_HotelAvailRQAvailRequestSegments
                {
                    AvailRequestSegment = new[]
                    {
                        new AvailRequestSegmentsTypeAvailRequestSegment
                        {
                            StayDateRange = new DateTimeSpanType()
                            {
                                Start = request.SelectedRooms.First().AvailabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                                End = request.SelectedRooms.First().AvailabilityRequest.CheckOutDate.ToString("yyyy-MM-dd")
                            },
                            RoomStayCandidates = CreateRoomStay(request),
                            HotelSearchCriteria = CreateSearchCriteria(request)
                        }
                    }
                }
            };
            
            return supplierRequest;
        }


        private HotelSearchCriteriaType CreateSearchCriteria(AccommodationProviderValuationRequest request)
        {
            var searchCriterion = new HotelSearchCriteriaType();
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;

            if (availabilityRequest.DestinationCodes.Any())
            {

                searchCriterion.Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        HotelRef = new ItemSearchCriterionTypeHotelRef
                        {
                            HotelCityCode = availabilityRequest.DestinationCodes[0]
                        }
                    }
                };
            }
            else if (availabilityRequest.EstablishmentCodes.Any())
            {
                searchCriterion.Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        HotelRef = new ItemSearchCriterionTypeHotelRef
                        {
                            HotelCode = availabilityRequest.EstablishmentCodes[0]
                        }
                    }
                };
            }

            return searchCriterion;
        }

        private ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate[] CreateRoomStay(AccommodationProviderValuationRequest request)
        {
            var roomStay = new ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate[request.SelectedRooms.Count()];

            int i = 0;
            foreach (var room in request.SelectedRooms)
            {
                var roomStayCandidate = new ArrayOfAvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidateRoomStayCandidate
                {
                    GuestCounts = new GuestCountType
                    {
                        GuestCount = CalculateGuestCount(room.Guests)
                    }
                };

                int counter = 0;
                //Assuming that there will be always one adult in a room
                roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
                {
                    AgeQualifyingCode = "10",
                    Count = room.Guests.AdultsCount.ToString()
                };
                counter++;

                if (room.Guests.ChildrenAndInfantsCount > 0)
                {
                    foreach (var guest in room.Guests.Where(guest => guest.Type == GuestType.Child || guest.Type == GuestType.Infant))
                    {
                        roomStayCandidate.GuestCounts.GuestCount[counter] = new GuestCountTypeGuestCount()
                        {
                            Age = guest.Age.ToString(),
                            Count = "1"
                        };

                        counter++;
                    }
                }




                roomStay[i] = roomStayCandidate;

                i++;
            }
            return roomStay;
        }

        private GuestCountTypeGuestCount[] CalculateGuestCount(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            int guestCount = 0;

            if (guests.AdultsCount > 0)
                guestCount++;

            if (guests.ChildrenCount > 0)
                guestCount++;

            if (guests.InfantsCount > 0)
                guestCount++;

            return new GuestCountTypeGuestCount[guestCount];

        }
        
       
    }
}
