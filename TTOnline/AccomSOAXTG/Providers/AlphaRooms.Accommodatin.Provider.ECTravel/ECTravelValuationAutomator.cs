﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.Helper;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelValuationAutomator : IAccommodationValuationAutomatorAsync<ECTravelValuationResponse>
    {
        private const string ECTravelValuationUrl = "ECTravelValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IECTravelProviderValuationRequestFactory ECTravelProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ECTravelValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IECTravelProviderValuationRequestFactory ECTravelSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.ECTravelProviderValuationRequestFactory = ECTravelSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<ECTravelValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ECTravelValuationResponse response = null;

            try
            {
                var supplierRequest = this.ECTravelProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ECTravelValuationAutomator.GetValuationResponseAsync: ECTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ECTravelValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, OTA_HotelAvailServiceOTA_HotelAvailRQ supplierRequest)
        {
            var client = ConfigureSoapCleint(request);

            var response = client.OTA_HotelAvailService(supplierRequest);

            var supplierResponse = new ECTravelValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        
        private OTA_HotelAvailSoapClient ConfigureSoapCleint(AccommodationProviderValuationRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ECTravelValuationUrl)));
            var client = new OTA_HotelAvailSoapClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
    
    }
}
