﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.ECTravel.Helper;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelHotelResService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelBookingParser : IAccommodationBookingParser<IEnumerable<ECTravelBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ECTravelBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<ECTravelBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ECTravel Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        OTA_HotelResRQ supplierRequest,
                                                        OTA_HotelResRS supplierResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = supplierResponse.XmlSerialize();
            }
            
            var helper = new ResponseHelper();
            
            bool hasResults = helper.HasResResults(supplierResponse);

            if (hasResults)
            {
                var resQuery = helper.GetItemByType(supplierResponse.Items, typeof(HotelReservationsType));

                if (resQuery !=null)
                {
                    var res = (HotelReservationsType)resQuery.First();

                    string resIdSource = res.HotelReservation.First().ResGlobalInfo.HotelReservationIDs.First().ResID_Source;
                    string resId = res.HotelReservation.First().ResGlobalInfo.HotelReservationIDs.First().ResID_Value;

                    var result = new AccommodationProviderBookingResult();
                    result.ProviderBookingReference = resId;
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("resIDSource", resIdSource);
                    
                    result.BookingStatus = BookingStatus.Confirmed;

                    bookingResults.Enqueue(result);
                }
            }
            else
            {
                var sbMessage = new StringBuilder();

                var error = helper.GetItemByType(supplierResponse.Items, typeof(ErrorType[]));

                sbMessage.AppendLine("ECTravel Booking Parser: Booking Response cannot be parsed because it is null.");

                if (error != null && error.Any())
                {
                    var apiExceptions = (ErrorType[]) error.First();

                    sbMessage.AppendLine("Error Details:");

                    int i = 0;
                    foreach (var e in apiExceptions)
                    {
                        sbMessage.AppendLine(string.Format("Error {0}:{1}", i, e.ShortText));
                        i++;
                    }
                }



                throw new SupplierApiDataException(sbMessage.ToString());
            }
            
        }

       
    }
}
