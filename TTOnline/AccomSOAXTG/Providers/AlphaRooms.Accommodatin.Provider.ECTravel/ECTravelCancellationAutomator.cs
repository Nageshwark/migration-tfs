﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.Helper;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelCancelService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelCancellationAutomator : IAccommodationCancellationAutomatorAsync<ECTravelCancellationResponse>
    {
        private const string ECTravelCancellationUrl = "ECTravelCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IECTravelProviderCancellationRequestFactory ECTravelProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ECTravelCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IECTravelProviderCancellationRequestFactory ECTravelSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ECTravelProviderCancellationRequestFactory = ECTravelSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<ECTravelCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ECTravelCancellationResponse response = null;

            try
            {
                var supplierRequest = ECTravelProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }
                
                var client = ConfigureSoapCleint(request);

                var supplierResponse = client.OTA_CancelService(supplierRequest);

                response = new ECTravelCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ECTravelBookingAutomator.GetCancellationResponseAsync: ECTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        

        private OTA_CancelSoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ECTravelCancellationUrl)));
            var client = new OTA_CancelSoapClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
    }
}
