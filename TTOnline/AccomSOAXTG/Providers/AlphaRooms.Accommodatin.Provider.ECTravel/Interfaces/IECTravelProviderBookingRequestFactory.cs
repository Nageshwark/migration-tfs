﻿using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelHotelResService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Interfaces
{
    public interface IECTravelProviderBookingRequestFactory
    {
        OTA_HotelResRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
