﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;


namespace AlphaRooms.Accommodation.Provider.ECTravel.Interfaces
{
    public interface IECTravelProviderValuationRequestFactory
    {
        OTA_HotelAvailServiceOTA_HotelAvailRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
