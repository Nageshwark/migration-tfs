﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelService;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Interfaces
{
    public interface IECTravelAvailabilityRequestFactory
    {
        OTA_HotelAvailServiceOTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
