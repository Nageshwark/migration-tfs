﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelCancelService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Interfaces
{
    public interface IECTravelProviderCancellationRequestFactory
    {
        OTA_CancelServiceOTA_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
