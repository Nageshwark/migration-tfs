﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelService;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelAvailabilityResponse
    {
        public OTA_HotelAvailServiceOTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailServiceResponseOTA_HotelAvailRS SupplierResponse { get; set; }

    }
}
