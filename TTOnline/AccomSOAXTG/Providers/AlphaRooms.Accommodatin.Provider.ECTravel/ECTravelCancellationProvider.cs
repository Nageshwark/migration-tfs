﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<ECTravelCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<ECTravelCancellationResponse> parser;

        public ECTravelCancellationProvider(IAccommodationCancellationAutomatorAsync<ECTravelCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<ECTravelCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);

            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
