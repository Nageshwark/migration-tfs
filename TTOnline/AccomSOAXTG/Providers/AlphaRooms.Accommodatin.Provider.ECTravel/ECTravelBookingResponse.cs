﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelHotelResService;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that ECTravel accepts and returns XML strings.
    /// </summary>
    public class ECTravelBookingResponse
    {
        public OTA_HotelResRQ SupplierRequest { get; set; }
        public OTA_HotelResRS SupplierResponse { get; set; }
    }
}
