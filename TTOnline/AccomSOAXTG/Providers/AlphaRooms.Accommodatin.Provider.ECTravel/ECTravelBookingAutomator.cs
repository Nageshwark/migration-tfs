﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.Helper;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelHotelResService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<ECTravelBookingResponse>>
    {
        private const string ECTravelBookingUrl = "ECTravelBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IECTravelProviderBookingRequestFactory ECTravelProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ECTravelBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IECTravelProviderBookingRequestFactory ECTravelSupplierBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ECTravelProviderBookingRequestFactory = ECTravelSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<ECTravelBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ECTravelBookingResponse> responses = new List<ECTravelBookingResponse>();

            OTA_HotelResRQ supplierRequest = null;
            ECTravelBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.ECTravelProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.ECTravelProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ECTravelBookingAutomator.GetBookingResponseAsync: ECTravel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<ECTravelBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, OTA_HotelResRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }


            var client = ConfigureSoapCleint(request);

            var response = client.OTA_HotelResV2Service(supplierRequest);

            var supplierResponse = new ECTravelBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private OTA_HotelResSoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            var address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ECTravelBookingUrl)));
            var client = new OTA_HotelResSoapClient(binding, address);

            var helper = new ProxyHelper();
            helper.SetProxy(client, request.Provider.Parameters);

            return client;
        }
       
    }
}
