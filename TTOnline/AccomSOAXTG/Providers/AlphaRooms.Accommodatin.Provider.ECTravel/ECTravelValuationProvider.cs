﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ECTravel;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ECTravelValuationResponse> automator;
        private readonly IAccommodationValuationParser<ECTravelValuationResponse> parser;

        public ECTravelValuationProvider(IAccommodationValuationAutomatorAsync<ECTravelValuationResponse> automator,
                                                IAccommodationValuationParser<ECTravelValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
