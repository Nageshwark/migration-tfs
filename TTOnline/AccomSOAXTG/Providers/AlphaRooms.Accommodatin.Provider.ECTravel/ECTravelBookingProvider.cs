﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<ECTravelBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<ECTravelBookingResponse>> parser;

        public ECTravelBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<ECTravelBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<ECTravelBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
