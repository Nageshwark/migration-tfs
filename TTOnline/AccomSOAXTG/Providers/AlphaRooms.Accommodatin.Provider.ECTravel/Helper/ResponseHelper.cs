﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Provider.ECTravel.ECTravelHotelResService;

namespace AlphaRooms.Accommodation.Provider.ECTravel.Helper
{
    public class ResponseHelper
    {
        public bool HasResResults(OTA_HotelResRS supplierResponse)
        {
            if (supplierResponse != null 
                && supplierResponse.Items != null 
                && supplierResponse.Items.Any() 
                && !GetItemByType(supplierResponse.Items, typeof(ErrorType[])).Any())
            {

                return true;
            }

            return false;
        }

        public IEnumerable<object> GetItemByType(object [] itemsArray, Type type)
        {
            return from item in itemsArray
                   where item.GetType() == type
                   select item;
        }

        public bool IsNonRefundable(XmlElement[] tpaExtensions)
        {
            bool isNonRefundable = false;

            var value = GetValue(tpaExtensions, "NonRefundable");

            isNonRefundable = (value == "1");
            
            return isNonRefundable;

        }

        public string GetValue(XmlElement[] tpaExtensions, string elementName)
        {
            string value="";

            var mp = from XmlElement x in tpaExtensions
                     where x.Name == elementName
                     select x;

            var xmlElements = mp as XmlElement[] ?? mp.ToArray();

            if (xmlElements.Any())
            {
                value = xmlElements.First().InnerText;
            }

            return value;

        }
    }
}
