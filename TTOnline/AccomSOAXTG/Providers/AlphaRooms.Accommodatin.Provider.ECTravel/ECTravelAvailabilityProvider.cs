﻿using AlphaRooms.Accommodation.Provider.ECTravel;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ECTravel;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.ECTravel
{
    public class ECTravelAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ECTravelAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<ECTravelAvailabilityResponse> parser;

        public ECTravelAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ECTravelAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ECTravelAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
