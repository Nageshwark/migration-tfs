﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;
using AlphaRooms.Utilities.WCF;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosBookingAutomator :CosmosAutomaterBase, IAccommodationBookingAutomatorAsync<IEnumerable<CosmosBookingResponse>>
    {
        private const string CosmosBookingUrl = "CosmosBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICosmosBookingRequestFactory ICosmosBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private OTABookingServicePort_V2011bClient client;
        public CosmosBookingAutomator(   ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ICosmosBookingRequestFactory CosmosSupplierBookingRequestFactory, IProviderProxyService proxyService
            //IProviderOutputLogger outputLogger
            ): base(proxyService)        {
            
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ICosmosBookingRequestFactory = CosmosSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<CosmosBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<CosmosBookingResponse> responses = new List<CosmosBookingResponse>();

            OTA_HotelResRQ supplierRequest = null;
            CosmosBookingResponse response = null;
            

            try
            {
                //Configure the Client so we can re-use for both request
                this.client = ConfigureSoapClient(request.Provider.Parameters);

                //Step1. Use HotelRes(Hold) to reserve Hotel
                supplierRequest = this.ICosmosBookingRequestFactory.CreateBookingRequest(request);
                var hotelResResponse  = await GetSupplierBookingResponseAsync(request, supplierRequest);
                

                if (IsSuccess(hotelResResponse))
                {
                    //Step 2: Use HotelRes(Modify) to add Customer Details
                    var hotelResModifyRequest = this.ICosmosBookingRequestFactory.CreateModifyRequest(hotelResResponse, request);
                    var hotelResModifyResponse = await GetSupplierModifyResponseAsync(request, hotelResModifyRequest);

                    if (IsSuccess(hotelResModifyResponse))
                    {
                        //Step 2: Booking Confirmation
                        var bookingConfirmRequest = this.ICosmosBookingRequestFactory.CreateBookingConfirmRequest(hotelResResponse, request);

                        var bookingConfirmResponse = await GetSupplierConfirmBookingResponseAsync(request, bookingConfirmRequest);
                        
                        response = new CosmosBookingResponse
                        {
                            SupplierResRequest = supplierRequest,
                            SupplierResResponse = hotelResResponse,
                            SupplierConfirmRequest = bookingConfirmRequest,
                            SupplierConfirmResponse = bookingConfirmResponse
                        };
                        responses.Add(response);
                    }
                    else
                    {
                        HandleError(hotelResModifyResponse);
                    }
                }
                else
                {
                    HandleError(hotelResResponse);
                }

                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierResRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("CosmosBookingAutomator.GetBookingResponseAsync: Cosmos supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<OTAX_BookingConfirmRS> GetSupplierConfirmBookingResponseAsync(AccommodationProviderBookingRequest request, BookingConfirmRQType supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            //this.client = ConfigureSoapClient(request.Provider.Parameters);
            var supplierResponse = client.OTAX_BookingConfirmRQ(supplierRequest);
            
            return supplierResponse;
        }

        
        private async Task<HotelResResponseType> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, OTA_HotelResRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            string cookie = request.ValuatedRooms.First().ValuationResult.ProviderSpecificData["Cookie"];

            //this.client = ConfigureSoapClient(request.Provider.Parameters);

            var custom = new CustomBehavior();
            custom.Headers = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("Cookie", cookie)
            };

            client.ChannelFactory.Endpoint.Behaviors.Add(custom);

            var response = client.OTA_HotelResRQ(supplierRequest);
            
            return response;
        }

        private async Task<HotelResResponseType> GetSupplierModifyResponseAsync(AccommodationProviderBookingRequest request, OTA_HotelResRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            //this.client = ConfigureSoapClient(request.Provider.Parameters);
            var response = client.OTA_HotelResRQ(supplierRequest);

            return response;
        }

        private bool IsSuccess(HotelResResponseType supplierResponse)
        {
            var successQuery = GetItemByType(supplierResponse.Items, typeof (SuccessType));
                               

            bool hasResults = (supplierResponse.Items != null &&
                                supplierResponse.Items.Any() &&
                                successQuery.Any() &&
                                !string.IsNullOrEmpty(supplierResponse.CorrelationID)
                                );

            return hasResults;
        }

        protected IEnumerable<object> GetItemByType(object[] itemsQuery, Type type)
        {
            return from item in itemsQuery
                   where item.GetType() == type
                   select item;
        }

        private void HandleError(HotelResResponseType response)
        {
            var sbMessage = new StringBuilder();
            sbMessage.AppendLine("Cosmos Booking Automator: Adding item to Basket failed. Aborting Booking Proces.");

            if (response != null && GetItemByType(response.Items, typeof(ErrorsType)).Any())
            {
                var errors = (ErrorsType)GetItemByType(response.Items, typeof(ErrorsType)).First();
                int i = 1;
                sbMessage.AppendLine("Error Details:");
                foreach (var error in errors.Error)
                {
                    sbMessage.AppendLine(string.Format("{0}. {1}", i, error.ShortText));
                    i++;
                }
            }

            throw new SupplierApiDataException(sbMessage.ToString());
        }
       
    }
}
