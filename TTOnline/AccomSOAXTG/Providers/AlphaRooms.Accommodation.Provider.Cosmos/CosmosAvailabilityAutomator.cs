﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using log4net.Repository.Hierarchy;


namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosAvailabilityAutomator:CosmosAutomaterBase, IAccommodationAvailabilityAutomatorAsync<CosmosAvailabilityResponse>
    {
        private const string CosmosAvailabilityUrl = "CosmosAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly ICosmosAvailabilityRequestFactory _CosmosSupplierAvailabilityRequestFactory;

        public CosmosAvailabilityAutomator(ICosmosAvailabilityRequestFactory CosmosAvailabilityRequestFactory, IProviderProxyService proxyService 
          
            ): base(proxyService)   
            
        {
           
            this._CosmosSupplierAvailabilityRequestFactory = CosmosAvailabilityRequestFactory;
        }

        public async Task<CosmosAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            CosmosAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                OTA_HotelAvailRQ supplierRequest = this._CosmosSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("CosmosAvailabilityAutomator.GetAvailabilityResponseAsync: Cosmos supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<CosmosAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, OTA_HotelAvailRQ  supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }
            
            var client = ConfigureSoapClient(request.Provider.Parameters);
            
            var response = client.OTA_HotelAvailRQ(supplierRequest);
            
            
            //supplierOutputLogger.LogAvailabilityResponse(supplierBase.Supplier, request.SearchId, request.DepartureAirportCode, request.DestinationAirportCode, request.DepartureDate, SupplierOutputFormats.Xml, supplierResponse);

            var supplierResponse = new CosmosAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

       

       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("Cosmos supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("Cosmos supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("Cosmos supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("Cosmos supports upto 2 Infants");
            //}
        }
    }
}
