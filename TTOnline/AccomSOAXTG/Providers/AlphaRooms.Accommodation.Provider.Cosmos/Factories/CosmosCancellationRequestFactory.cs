﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Factories
{
    public class CosmosCancellationRequestFactory : CosmosRequestFactoryBase, ICosmosCancellationRequestFactory
    {
        private const string CosmosLogin = "CosmosLogin";
        private const string CosmosPassword = "CosmosPassword";
        private const string CosmosLanguage = "CosmosLanguage";
        private const string CosmosGzip = "CosmosGzip";
        private const string CosmosAgentId = "CosmosAgentId";

        public OTAX_CancelRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new OTAX_CancelRQ
            {
                Version = 0,
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(CosmosLanguage),
                POS = SetPOS(request.Provider.Parameters),
                CancelType = TransactionActionType.Book,
                UniqueID = new []
                {
                    new TBX_UniqueID
                    {
                        Type = TBX_CodeRefType.Booking,
                        ID = request.ProviderBookingReference
                    }
                }
            };

            return supplierRequest;
        }
    }
}
