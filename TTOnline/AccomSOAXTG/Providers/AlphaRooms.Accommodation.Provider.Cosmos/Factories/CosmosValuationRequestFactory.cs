﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Factories
{
    public class CosmosValuationRequestFactory : CosmosAvailabilityRequestFactoryBase, ICosmosValuationRequestFactory
    {
        public OTA_HotelAvailRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            var supplierRequest = new OTA_HotelAvailRQ
            {
                PrimaryLangID = "en",
                AvailRatesOnly = true,
                AvailRatesOnlySpecified = true,
                MaxResponses = "200",
                RateDetailsInd = false,
                RateDetailsIndSpecified = true,
                POS = SetPOS(request.Provider.Parameters),
                AvailRequestSegments = new OTA_HotelAvailRQAvailRequestSegments
                {
                    AvailRequestSegment = new[]
                    {
                        new AvailRequestSegmentsTypeAvailRequestSegment
                        {
                            StayDateRange = new DateTimeSpanType
                            {
                                Start = availabilityRequest.CheckInDate.ToString("yyyy-MM-dd"),
                                End = availabilityRequest.CheckOutDate.ToString("yyyy-MM-dd"),
                                Duration = ((availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate).Days).ToString()
                            },
                            RoomStayCandidates = CreateRoomStay(request.SelectedRooms),
                            HotelSearchCriteria = CreateSearchCriteria(new []{availabilityResult.EstablishmentEdiCode}, null)
                        }
                    }
                }
            };

            return supplierRequest;
        }


        
       
    }
}
