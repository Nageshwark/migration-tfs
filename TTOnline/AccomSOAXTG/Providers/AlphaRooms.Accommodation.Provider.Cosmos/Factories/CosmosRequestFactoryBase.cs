﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Factories
{
    public abstract class CosmosRequestFactoryBase
    {
        private const string CosmosCurrencyCode = "CosmosCurrencyCode";
        private const string CosmosCompanyName = "CosmosCompanyName";
        private const string CosmosChannelType = "CosmosChannelType";
        private const string CosmosClientGroup = "CosmosClientGroup";
        private const string CosmosTravelClient = "CosmosTravelClient";
        private const string CosmosERSPUserID = "CosmosERSPUserID";
        private const string CosmosUsername = "CosmosUsername";

        
        protected SourceType[] SetPOS(List<AccommodationProviderParameter> parameters)
        {
            return new[]
            {
                new SourceType
                {
                    ISOCurrency = parameters.GetParameterValue(CosmosCurrencyCode),
                    RequestorID = new SourceTypeRequestorID
                    {
                        Type = "4"
                    }
                    ,
                    BookingChannel = new SourceTypeBookingChannel
                    {
                        Type = parameters.GetParameterValue(CosmosChannelType),
                        CompanyName = new CompanyNameType
                        {
                            Code = parameters.GetParameterValue(CosmosCompanyName).Split(';')[0],
                            Division = parameters.GetParameterValue(CosmosCompanyName).Split(';')[1],
                            Department = parameters.GetParameterValue(CosmosCompanyName).Split(';')[2],
                        }
                    }
                },
                new SourceType
                {
                    AgentSine = parameters.GetParameterValue(CosmosUsername),
                    ERSP_UserID = parameters.GetParameterValue(CosmosERSPUserID),
                    RequestorID = new SourceTypeRequestorID
                    {
                        Type = "21"
                    }
                },
                new SourceType
                {
                    RequestorID = new SourceTypeRequestorID
                    {
                        Type = "9",
                        ID = parameters.GetParameterValue(CosmosClientGroup)
                    }
                },
                new SourceType
                {
                    RequestorID = new SourceTypeRequestorID
                    {
                        Type = "5",
                        ID = parameters.GetParameterValue(CosmosTravelClient)
                    }
                }
            };
        }

        protected string GetAgeQulifyingCode(GuestType guest)
        {
            string ageCode = "";

            switch (guest)
            {
                case GuestType.Adult:
                    ageCode = "10";
                    break;
                case GuestType.Child:
                    ageCode = "8";
                    break;
                default:
                    ageCode = "7";
                    break;
            }

            return ageCode;
        }
    }
}
