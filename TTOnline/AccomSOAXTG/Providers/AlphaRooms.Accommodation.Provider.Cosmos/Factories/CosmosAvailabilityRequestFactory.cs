﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Factories
{
    public class CosmosAvailabilityRequestFactory : CosmosAvailabilityRequestFactoryBase, ICosmosAvailabilityRequestFactory
    {
        private const string CosmosCountryCode = "CosmosCountryCode";
        private const string CosmosLanguageCode = "CosmosLanguageCode";




        public OTA_HotelAvailRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new OTA_HotelAvailRQ
            {
                PrimaryLangID = "en",
                AvailRatesOnly = true,
                AvailRatesOnlySpecified = true,
                MaxResponses = "200",
                RateDetailsInd = false,
                RateDetailsIndSpecified = true,
                POS = SetPOS(request.Provider.Parameters),
                AvailRequestSegments = new OTA_HotelAvailRQAvailRequestSegments
                {
                    AvailRequestSegment =  new []
                    {
                        new AvailRequestSegmentsTypeAvailRequestSegment
                        {
                            StayDateRange = new DateTimeSpanType
                            {
                                Start = request.CheckInDate.ToString("yyyy-MM-dd"),
                                End = request.CheckOutDate.ToString("yyyy-MM-dd"),
                                Duration = ((request.CheckOutDate - request.CheckInDate).Days).ToString()
                            },
                            RoomStayCandidates = CreateRoomStay(request.Rooms),
                            HotelSearchCriteria = CreateSearchCriteria(request.EstablishmentCodes, request.DestinationCodes)
                        }
                    }
                }
            };
            
            return supplierRequest;
        }
    }
}
