﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Factories
{
    public abstract class CosmosAvailabilityRequestFactoryBase: CosmosRequestFactoryBase
    {

        
        protected AvailRequestSegmentsTypeAvailRequestSegmentHotelSearchCriteria CreateSearchCriteria(string[] establishmentCodes, string[] destinationCodes)
        {
            bool isHotelId = false;
            AvailRequestSegmentsTypeAvailRequestSegmentHotelSearchCriteria criteria = null;

            if (establishmentCodes != null && establishmentCodes.Any())
                isHotelId = true;


            criteria = new AvailRequestSegmentsTypeAvailRequestSegmentHotelSearchCriteria
            {
                Criterion = new[]
                {
                    new HotelSearchCriteriaTypeCriterion
                    {
                        HotelRef = new[]
                        {
                            new ItemSearchCriterionTypeHotelRef
                            {
                                HotelCityCode = isHotelId ? null : destinationCodes.First(),
                                HotelCode = isHotelId ? establishmentCodes.First() : null
                            }
                        }
                    }
                }
            };
            

            return criteria;
        }

        protected AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate[] CreateRoomStay(AccommodationProviderValuationRequestRoom[] rooms)
        {
            var roomStays = new List<AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate>();

            foreach (var room in rooms)
            {
                roomStays.Add(new AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate
                {
                    Quantity = "1",
                    GuestCounts = new GuestCountType
                    {
                        GuestCount = CreateGuestCounts(room.Guests)
                    }
                });

            }

            return roomStays.ToArray();
        }

        protected AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate[] CreateRoomStay(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            var roomStays = new List<AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate>();

            foreach (var room in rooms)
            {
                roomStays.Add(new AvailRequestSegmentsTypeAvailRequestSegmentRoomStayCandidate
                {
                    Quantity = "1",
                    GuestCounts = new GuestCountType
                    {
                        GuestCount = CreateGuestCounts(room.Guests)
                    }
                });

            }

            return roomStays.ToArray();
        }

        protected GuestCountTypeGuestCount[] CreateGuestCounts( AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            var guestCounts = new List<GuestCountTypeGuestCount>();
            
            foreach (var guest in guests)
            {
                guestCounts.Add(new GuestCountTypeGuestCount
                {
                    AgeQualifyingCode = GetAgeQulifyingCode(guest.Type),
                    Age = guest.Type == GuestType.Adult ? "30" : guest.Age.ToString(),
                    Count = "1"
                });
            }

            return guestCounts.ToArray();
        }
    }
}
