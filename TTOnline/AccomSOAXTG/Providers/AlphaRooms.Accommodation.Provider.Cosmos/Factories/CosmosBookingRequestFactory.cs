﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Provider.Cosmos.Factories;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Activation;
using GuestType = AlphaRooms.Accommodation.Core.Provider.Contracts.GuestType;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosBookingRequestFactory : CosmosRequestFactoryBase, ICosmosBookingRequestFactory
    {
       
        private const string CosmosLanguage = "CosmosLanguage";
        private const string CosmosCurrencyCode = "CosmosCurrencyCode";
        private const string CosmosERSPUserID = "CosmosERSPUserID";

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public OTA_HotelResRQ CreateBookingRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;
            
            var supplierRequest = new OTA_HotelResRQ
            {
                Version = 0,
                TransactionIdentifier = valuationResult.ProviderSpecificData["TransactionID"],
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(CosmosLanguage),
                ResStatus = TransactionActionType.Hold,
                ResStatusSpecified = true,
                POS = SetPOS(request.Provider.Parameters),
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation =  new []
                    {
                        new HotelReservationsTypeHotelReservation {}
                    },
                    TPA_Extensions = new TPA_ExtensionsType
                    {
                        Any = CreateAdditionalResultData(request)
                    }
                }
                
              
            };

            return supplierRequest;
        }

        /// <summary>
        /// This creates the Res Modify request that is reqired to Add Passenger details and other details before booking can be confirmed
        /// </summary>
        /// <param name="resResponse"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public OTA_HotelResRQ CreateModifyRequest(HotelResResponseType resResponse, AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new OTA_HotelResRQ
            {
                Version = 0,
                TransactionIdentifier = resResponse.TransactionIdentifier,
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(CosmosLanguage),
                CorrelationID = resResponse.CorrelationID,
                ResStatus =   TransactionActionType.Modify,
                ResStatusSpecified = true,
                POS = SetPOS(request.Provider.Parameters),
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation = new []
                    {
                        new HotelReservationsTypeHotelReservation
                        {
                            ResGuests = CreateResGuests(request.ValuatedRooms, request.Customer)
                        }
                    }
                }

            };


            return supplierRequest;
        }

        
        public BookingConfirmRQType CreateBookingConfirmRequest(HotelResResponseType resResponse, AccommodationProviderBookingRequest request)
        {
            var supplierRequest = new BookingConfirmRQType
            {
                Version = 001,
                TransactionIdentifier = resResponse.TransactionIdentifier,
                CorrelationID = resResponse.CorrelationID,
                PrimaryLangID = request.Provider.Parameters.GetParameterValue(CosmosLanguage),
                POS = new []
                {
                    new SourceType
                    {
                        ISOCurrency = request.Provider.Parameters.GetParameterValue(CosmosCurrencyCode)
                    }
                },
                UniqueID = new[]
                {
                    new TBX_UniqueID
                    {
                        Type = TBX_CodeRefType.Customer,
                        ID = request.Provider.Parameters.GetParameterValue("CosmosBookingRef"),
                        //Ref = request.Provider.Parameters.GetParameterValue("CosmosBookingRef")
                    },
                    new TBX_UniqueID
                    {
                        Type = TBX_CodeRefType.User,
                        ID = request.Provider.Parameters.GetParameterValue(CosmosERSPUserID)
                    }
                }

            };

            return supplierRequest;
        }

        private ResGuestType[] CreateResGuests(AccommodationProviderBookingRequestRoom[] valuatedRooms, AccommodationProviderBookingRequestCustomer customer)
        {
            var resGuests = new List<ResGuestType>();

            foreach (var room in valuatedRooms)
            {
                foreach (var guest in room.Guests)
                {
                    var age = guest.Type == GuestType.Adult ? 30 : guest.Age;
                    var resGuest = new ResGuestType
                    {
                        ResGuestRPH = (resGuests.Count + 1).ToString(),
                        AgeQualifyingCode = GetAgeQulifyingCode(guest.Type),
                        Age = age.ToString(),
                        Profiles = new[]
                        {
                            new ProfilesTypeProfileInfo
                            {
                                Profile = new ProfileType
                                {
                                    Customer = new CustomerType
                                    {
                                         BirthDate = DateTime.Now.AddYears(-age),
                                         BirthDateSpecified = true,
                                         PersonName = new []
                                         {
                                             new PersonNameType
                                             {
                                                NamePrefix = new []
                                                {
                                                    guest.Title == GuestTitle.Unknown? "Mr" : guest.Title.ToString()
                                                },
                                                GivenName = new []{ guest.FirstName},
                                                Surname = guest.Surname
                                             }
                                         },
                                         Telephone = new []
                                         {
                                             new CustomerTypeTelephone
                                             {
                                                 PhoneNumber = customer.ContactNumber
                                             }
                                         },
                                         Email = new []
                                         {
                                             new CustomerTypeEmail
                                             {
                                                 Value = customer.EmailAddress
                                             }
                                         }
                                    }
                                }
                            }
                        }
                    };

                    //Lead Guest Identifier
                    if (resGuests.Count == 0)
                    {
                        resGuest.Profiles.First().UniqueID = new[]
                        {
                            new UniqueID_Type
                            {
                                Type = "23",
                                ID = "23"
                            }
                        };
                    }

                    resGuests.Add(resGuest);
                }

            }

            return resGuests.ToArray();
        }

        private XmlElement[] CreateAdditionalResultData(AccommodationProviderBookingRequest request)
        {
            var additionalResultData = new AdditionalResultData
            {
                ResultIndex = request.ValuatedRooms.First().ValuationResult.ProviderSpecificData["RPH"]
            };

            string xml = additionalResultData.XmlSerialize();

            return new [] {CreateElement(xml)};

        }

        private  XmlElement CreateElement(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc.DocumentElement;
        }
    }
}
