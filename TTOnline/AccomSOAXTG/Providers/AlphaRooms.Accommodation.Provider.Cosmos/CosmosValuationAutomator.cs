﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosValuationAutomator :CosmosAutomaterBase, IAccommodationValuationAutomatorAsync<CosmosValuationResponse>
    {
        private const string CosmosValuationUrl = "CosmosValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICosmosValuationRequestFactory _CosmosValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public CosmosValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ICosmosValuationRequestFactory CosmosSupplierValuationRequestFactory
                                           , IProviderProxyService proxyService
            //IProviderOutputLogger outputLogger
            ) : base(proxyService)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this._CosmosValuationRequestFactory = CosmosSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<CosmosValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            CosmosValuationResponse response = null;

            try
            {
                var supplierRequest = this._CosmosValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("CosmosValuationAutomator.GetValuationResponseAsync: Cosmos supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<CosmosValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, OTA_HotelAvailRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            OTA_HotelAvailRS response;
            string cookies = string.Empty;

            var client = ConfigureSoapClient(request.Provider.Parameters);

            using (new OperationContextScope(client.InnerChannel))
            {
                response = client.OTA_HotelAvailRQ(supplierRequest);
                var headers = (HttpResponseMessageProperty)OperationContext.Current.IncomingMessageProperties["httpResponse"];

                if (headers != null)
                    cookies = headers.Headers["Set-Cookie"];
            }

            var supplierResponse = new CosmosValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response,
                Cookies = cookies
            };

            return supplierResponse;
        }

        
      
    
    }
}
