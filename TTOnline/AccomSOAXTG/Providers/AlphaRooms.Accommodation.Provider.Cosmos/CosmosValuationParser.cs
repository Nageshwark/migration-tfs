﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{

    public class CosmosValuationParser : CosmosParserBase, IAccommodationValuationParser<CosmosValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public CosmosValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, CosmosValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.SupplierResponse.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response, valuationResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Cosmos Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, CosmosValuationResponse response , ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            OTA_HotelAvailRQ supplierRequest = response.SupplierRequest;
            OTA_HotelAvailRS supplierResponse = response.SupplierResponse;

            bool hasResults = HasResult(supplierResponse);

            if (hasResults)
            {
                var roomStays = (OTA_HotelAvailRSRoomStays)GetItemByType(supplierResponse.Items, typeof(OTA_HotelAvailRSRoomStays)).First();
                var hotelStays = (OTA_HotelAvailRSHotelStays)(GetItemByType(supplierResponse.Items, typeof(OTA_HotelAvailRSHotelStays))).First();
                var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

                foreach (var roomStay in roomStays.RoomStay)
                {
                    var hotel = GetHotelDetails(roomStay.RPH, hotelStays);

                    foreach (var rate in roomStay.RoomRates.RoomRate)
                    {
                        var isSelectRoom = from AccommodationProviderValuationRequestRoom r in request.SelectedRooms
                            where r.AvailabilityResult.RoomCode == roomStay.RoomTypes.First().RoomTypeCode &&
                                  r.AvailabilityResult.ProviderSpecificData["RatePlanID"] == rate.RatePlanID &&
                                  r.AvailabilityResult.ProviderSpecificData["RPH"] == roomStay.RPH
                            select r;

                        if (isSelectRoom.Any())
                        {
                            var result = new AccommodationProviderValuationResult();

                            result.RoomNumber = (byte) 1;
                            result.ProviderEdiCode = request.Provider.EdiCode;

                            if (string.IsNullOrEmpty(hotel.HotelCodeContext))
                                result.SupplierEdiCode = request.Provider.EdiCode;
                            else
                                result.SupplierEdiCode = hotel.HotelCodeContext;

                            result.EstablishmentName = hotel.HotelName;
                            result.EstablishmentEdiCode = hotel.HotelCode;
                            result.DestinationEdiCode = hotel.HotelCityCode;

                            var boardCode = GetBoardCode(rate.RatePlanID, roomStay.RatePlans);
                            result.BoardCode = boardCode;
                            result.BoardDescription = rate.RoomRateDescription.First().Name;

                            result.RoomCode = roomStay.RoomTypes.First().RoomTypeCode;
                            result.RoomDescription = roomStay.RoomTypes.First().RoomType;

                            result.PaymentModel = PaymentModel.PostPayment;
                            result.RateType = RateType.NetStandard;
                            result.IsOpaqueRate = false;
                            result.IsNonRefundable = false;


                            result.SalePrice = new Money
                            {
                                Amount = rate.Total.AmountAfterTax,
                                CurrencyCode = rate.Total.CurrencyCode
                            };
                            result.CostPrice = result.SalePrice;
                            //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                            result.Adults = request.SelectedRooms.First().Guests.AdultsCount;
                            result.Children = request.SelectedRooms.First().Guests.ChildrenCount;
                            result.Infants = request.SelectedRooms.First().Guests.InfantsCount;
                            result.CheckInDate = availabilityResult.CheckInDate;
                            result.CheckOutDate = availabilityResult.CheckOutDate;

                            result.ProviderSpecificData = new Dictionary<string, string>();
                            result.ProviderSpecificData.Add("TransactionID", supplierResponse.TransactionIdentifier);
                            result.ProviderSpecificData.Add("RPH", roomStay.RPH);
                            result.ProviderSpecificData.Add("Cookie", response.Cookies);

                            availabilityResults.Enqueue(result);
                        }

                    }
                }
            }
            else
            {
                var sbMessage = new StringBuilder();
                sbMessage.AppendLine("Cosmos Valuation Parser: Availability Response cannot be parsed because it is null.");

                if (supplierResponse != null && GetItemByType(supplierResponse.Items, typeof(ErrorsType)).Any())
                {
                    var errors = (ErrorsType)GetItemByType(supplierResponse.Items, typeof(ErrorsType)).First();
                    int i = 1;
                    sbMessage.AppendLine("Error Details:");
                    foreach (var error in errors.Error)
                    {
                        sbMessage.AppendLine(string.Format("{0}. {1}", i, error.ShortText));
                        i++;
                    }

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }

           
        }

    }
}
