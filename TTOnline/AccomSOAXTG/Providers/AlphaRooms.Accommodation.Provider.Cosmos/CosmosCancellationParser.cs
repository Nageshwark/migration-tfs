﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosCancellationParser :CosmosParserBase, IAccommodationCancellationParserAsync<CosmosCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public CosmosCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, CosmosCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                var supplierResponse = response.SupplierResponse;

                bool hasResults = (supplierResponse.Items != null &&
                                supplierResponse.Items.Any() &&
                                (GetItemByType(supplierResponse.Items, typeof(SuccessType)).Any())
                                );

                if (hasResults)
                {
                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    //bookingResult.Message = response.SupplierResponse.message;
                }
                else
                {
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was a problem with the cancellation response from Cosmos.");

                    if (supplierResponse != null && GetItemByType(supplierResponse.Items, typeof(ErrorsType)).Any())
                    {
                        var errors = (ErrorsType)GetItemByType(supplierResponse.Items, typeof(ErrorsType)).First();
                        int i = 1;
                        sb.AppendLine("Error Details:");
                        foreach (var error in errors.Error)
                        {
                            sb.AppendLine(string.Format("{0}. {1}", i, error.ShortText));
                            i++;
                        }
                    }

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);

                    if (response != null)
                    {
                        sb.AppendLine("Cancellation Request:");
                        sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Cancellation Response:");
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    }

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Cosmos.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
       
    }
}
