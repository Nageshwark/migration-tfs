﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosBookingParser : CosmosParserBase, IAccommodationBookingParser<IEnumerable<CosmosBookingResponse>>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public CosmosBookingParser(  ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<CosmosBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {

                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierResRequest, response.SupplierResResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Cosmos Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(  AccommodationProviderBookingRequest request,
                                                        CosmosBookingResponse response,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXML = response.SupplierConfirmResponse.XmlSerialize();
            }



            bool hasResults = (response.SupplierConfirmResponse.Items != null &&
                                response.SupplierConfirmResponse.Items.Any() &&  
                                (GetItemByType(response.SupplierConfirmResponse.Items, typeof(SuccessType)).Any())) ;

            if (hasResults)
            {
                var result = new AccommodationProviderBookingResult();
                var reservation = (TBX_ReservationType)(GetItemByType(response.SupplierConfirmResponse.Items, typeof(TBX_ReservationType)).First());

                var uniqueId = reservation.UniqueID.FirstOrDefault(t => t.Type == "14");
                
                if (uniqueId != null)
                    result.ProviderBookingReference = uniqueId.ID;

                result.BookingStatus = BookingStatus.Confirmed;

                bookingResults.Enqueue(result);
            }
            else
            {
                
                var sbMessage = new StringBuilder();
                sbMessage.AppendLine("Cosmos Booking Parser: Availability Response cannot be parsed because it is null.");

                if (response.SupplierConfirmResponse != null && GetItemByType(response.SupplierConfirmResponse.Items, typeof(ErrorsType)).Any())
                {
                    var errors = (ErrorsType)GetItemByType(response.SupplierConfirmResponse.Items, typeof(ErrorsType)).First();
                    int i = 1;
                    sbMessage.AppendLine("Error Details:");
                    foreach (var error in errors.Error)
                    {
                        sbMessage.AppendLine(string.Format("{0}. {1}", i, error.ShortText));
                        i++;
                    }

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }

        }

       
    }
}
