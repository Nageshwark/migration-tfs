﻿
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosCancellationResponse
    {
        public OTAX_CancelRQ SupplierRequest { get; set; }
        public OTAX_CancelRS SupplierResponse { get; set; }
    }
}
