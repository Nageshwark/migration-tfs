﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosAvailabilityResponse
    {
        public OTA_HotelAvailRQ SupplierRequest { get; set; }
        public OTA_HotelAvailRS SupplierResponse { get; set; }

    }
}
