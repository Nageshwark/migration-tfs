﻿using AlphaRooms.Accommodation.Provider.Cosmos;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Cosmos;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<CosmosAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<CosmosAvailabilityResponse> parser;

        public CosmosAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<CosmosAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<CosmosAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
