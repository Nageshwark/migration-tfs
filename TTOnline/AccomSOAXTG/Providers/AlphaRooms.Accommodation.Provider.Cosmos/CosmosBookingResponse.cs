﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that Cosmos accepts and returns XML strings.
    /// </summary>
    public class CosmosBookingResponse
    {
        public OTA_HotelResRQ SupplierResRequest { get; set; }
        public HotelResResponseType SupplierResResponse { get; set; }

        public BookingConfirmRQType SupplierConfirmRequest { get; set; }
        public OTAX_BookingConfirmRS SupplierConfirmResponse { get; set; }
    }
}
