using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public abstract class CosmosParserBase
    {
        protected IEnumerable<object> GetItemByType(object[] itemsQuery, Type type)
        {
            return from item in itemsQuery
                   where item.GetType() == type
                   select item;
        }

        protected bool HasResult(OTA_HotelAvailRS supplierResponse)
        {
            bool hasResult = false;

            bool hasData = ( supplierResponse.Items != null &&
                                supplierResponse.Items.Any() &&  
                                (GetItemByType(supplierResponse.Items, typeof(SuccessType)).Any()) 
                                );

            if (hasData)
            {
                var roomStays = (OTA_HotelAvailRSRoomStays) GetItemByType(supplierResponse.Items, typeof (OTA_HotelAvailRSRoomStays)).First();

                if (roomStays.RoomStay != null)
                    hasResult = true;

            }

            return hasResult;
        }

        protected BasicPropertyInfoType GetHotelDetails(string rph, OTA_HotelAvailRSHotelStays hotelStays)
        {
            var hotelStayQuery = from hotelStay in hotelStays.HotelStay
                                 where hotelStay.RoomStayRPH.Contains(rph)
                                 select hotelStay.BasicPropertyInfo;

            return hotelStayQuery.FirstOrDefault();

        }

        protected string GetBoardCode(string ratePlanId, RatePlanType[] ratePlans)
        {
            string boardCode = string.Empty;

            var ratePlanQuery = from rp in ratePlans
                                where rp.RatePlanID == ratePlanId
                                select rp;

            if (ratePlanQuery.Any())
                boardCode = ratePlanQuery.First().MealsIncluded.MealPlanCodes.FirstOrDefault();

            return boardCode;
        }

    }
}