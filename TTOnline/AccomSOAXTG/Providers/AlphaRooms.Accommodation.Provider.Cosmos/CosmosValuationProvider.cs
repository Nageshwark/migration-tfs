﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Cosmos;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<CosmosValuationResponse> automator;
        private readonly IAccommodationValuationParser<CosmosValuationResponse> parser;

        public CosmosValuationProvider(IAccommodationValuationAutomatorAsync<CosmosValuationResponse> automator,
                                                IAccommodationValuationParser<CosmosValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
