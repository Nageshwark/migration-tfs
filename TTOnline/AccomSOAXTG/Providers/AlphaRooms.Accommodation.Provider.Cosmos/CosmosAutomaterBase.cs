﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.Accommodation.Provider.Cosmos.Entity;
using AlphaRooms.Accommodation.Provider.Cosmos.SecurityService;
using AlphaRooms.Utilities.ExtensionMethods;
using SourceType = AlphaRooms.Accommodation.Provider.Cosmos.CosmosService.SourceType;


namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosAutomaterBase
    {
        private const string CosmosAvailabilityUrl = "CosmosAvailabilityUrl";
        private const string CosmosValuationUrl = "CosmosValuationUrl";
        private const string CosmosSecurityUrl = "CosmosSecurityUrl";
        private const string CosmosBookingUrl = "CosmosBookingUrl";
        private const string CosmosCancellationUrl = "CosmosCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string CosmosUsername = "CosmosUsername";
        private const string CosmosPassword = "CosmosPassword";

        private IProviderProxyService proxyService { get; set; }

        public CosmosAutomaterBase(IProviderProxyService proxyService)
        {
            this.proxyService = proxyService;
        }

        protected OTABookingServicePort_V2011bClient ConfigureSoapClient(List<AccommodationProviderParameter> parameters)
        {
            var addressHeader = GetSecurityHeader(parameters);
            var binding = new BasicHttpBinding();
          

            binding.MaxReceivedMessageSize = Convert.ToInt32(parameters.GetParameterValue(MaxReceivedMessageSize));
           
            EndpointAddress address = null;
            
            if (this.GetType() == typeof(CosmosAvailabilityAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(CosmosAvailabilityUrl)), addressHeader);
            else if (this.GetType() == typeof(CosmosValuationAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(CosmosValuationUrl)), addressHeader);
            else if (this.GetType() == typeof(CosmosBookingAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(CosmosBookingUrl)), addressHeader);
            else if (this.GetType() == typeof(CosmosCancellationAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(CosmosCancellationUrl)), addressHeader);

            
            var client = new OTABookingServicePort_V2011bClient(binding, address);          
            
            return client;
        }

        private AddressHeader[] GetSecurityHeader(List<AccommodationProviderParameter> parameters)
        {
            string username = parameters.GetParameterValue(CosmosUsername);
           

            var security = new Security();
            var usernameToken = new UsernameToken
            {
                Username = username,
                Password = GetAuthenticationToken(parameters), 
            };
            security.UsernameToken = usernameToken;

            //Serialize object to xml
            XmlObjectSerializer xmlObjectSerializer = new DataContractSerializer(typeof (Security), "Security", "");

            //Create address header with security header
            AddressHeader addressHeader = AddressHeader.CreateAddressHeader("Security",
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", security,
                xmlObjectSerializer);
            
            return new []{addressHeader};
        }

        private string GetAuthenticationToken(List<AccommodationProviderParameter> parameters)
        {
            string key = string.Empty;
            var binding = new BasicHttpBinding();
           

            binding.MaxReceivedMessageSize = Convert.ToInt32(parameters.GetParameterValue(MaxReceivedMessageSize));

            EndpointAddress address = null;


            address = new EndpointAddress(new Uri(parameters.GetParameterValue(CosmosSecurityUrl)));

            var client = new OTASecurityPort_V2011bClient(binding, address);
            
            proxyService.SetProxy(client, parameters);

            var supplierRequest = new SecurityService.OTAX_AuthTokenRQ
            {
                POS = new[]
                {
                    new SecurityService.SourceType()
                },
                AuthTokenType = AuthTokenType.Request,
                AuthTokenCredentials = new AuthTokenCredentials
                {
                    UserName = parameters.GetParameterValue(CosmosUsername),
                    Password = parameters.GetParameterValue(CosmosPassword)
                }

            };

            try
            {
                var supplierResponse = client.OTAX_AuthTokenRQ(supplierRequest);

                if (supplierResponse != null && supplierResponse.Items != null)
                {
                    var keyQuery = from item in supplierResponse.Items
                        where item is string
                        select item;

                    if (keyQuery.Any())
                        key = keyQuery.First().ToString();
                }
                else
                {
                    throw new Exception("CosmosAutomatorBase: Unable to Retrieve Authentication Token");
                }

            }
            catch (Exception exception)
            {
                throw new Exception("CosmosAutomatorBase: Unable to Retrieve Authentication Token. Details: " + exception.Message);
            }
            

            return key;

        }
    }
}
