﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Interfaces
{
    public interface ICosmosValuationRequestFactory
    {
        OTA_HotelAvailRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
