﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Cosmos.CosmosService;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Interfaces
{
    public interface ICosmosBookingRequestFactory
    {
        OTA_HotelResRQ CreateBookingRequest(AccommodationProviderBookingRequest request);

        OTA_HotelResRQ CreateModifyRequest(HotelResResponseType resResponse, AccommodationProviderBookingRequest request);

        BookingConfirmRQType CreateBookingConfirmRequest(HotelResResponseType resResponse, AccommodationProviderBookingRequest request);
    }
}
