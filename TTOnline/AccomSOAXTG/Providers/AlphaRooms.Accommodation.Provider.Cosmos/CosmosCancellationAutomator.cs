﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.Cosmos
{
    public class CosmosCancellationAutomator :CosmosAutomaterBase, IAccommodationCancellationAutomatorAsync<CosmosCancellationResponse>
    {
        private const string CosmosCancellationUrl = "CosmosCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ICosmosCancellationRequestFactory _CosmosCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public CosmosCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ICosmosCancellationRequestFactory CosmosSupplierCancellationRequestFactory, IProviderProxyService proxyService
            //IProviderOutputLogger outputLogger
            ) : base(proxyService)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._CosmosCancellationRequestFactory = CosmosSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<CosmosCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            CosmosCancellationResponse response = null;

            try
            {
                var supplierRequest = _CosmosCancellationRequestFactory.CreateSupplierCancelRequest(request);

                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                var client = ConfigureSoapClient(request.Provider.Parameters);

                var supplierResponse = client.OTAX_CancelRQ(supplierRequest);

                response = new CosmosCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("CosmosBookingAutomator.GetCancellationResponseAsync: Cosmos supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }



        
    }
}
