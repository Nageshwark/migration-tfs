﻿using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Entity
{
    public class UsernameToken : IXmlSerializable
    {

        public string Username { get; set; }
        public string Password { get; set; }

        public XmlSchema GetSchema() { return null; }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();

            Username = reader.ReadElementString("Username");
            reader.ReadStartElement();

            Password = reader.ReadElementString("Password");
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("Username", Username);
            writer.WriteElementString("Password", Password);
        }
    }
}
