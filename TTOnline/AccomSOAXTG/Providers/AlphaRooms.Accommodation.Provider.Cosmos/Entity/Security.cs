﻿using System;
using System.Runtime.Serialization;

namespace AlphaRooms.Accommodation.Provider.Cosmos.Entity
{
    [Serializable]
    [DataContract(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")] // This object serialize specific namespace
    public class Security
    {
        [DataMember] // This object serialize without namespace
        public UsernameToken UsernameToken;
    }
}
