﻿using AlphaRooms.Accommodation.Provider.Avra;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<AvraAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<AvraAvailabilityResponse> parser;

        public AvraAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<AvraAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<AvraAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
