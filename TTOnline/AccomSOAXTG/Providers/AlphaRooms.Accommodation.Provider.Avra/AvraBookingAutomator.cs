﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using System.Web;
using AlphaRooms.Accommodation.Provider.Avra.BookRequest;


namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraBookingAutomator : IAccommodationBookingAutomatorAsync<AvraBookingResponse>
    {
        private const string AvraAvailabilityUrl = "AvraAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

       
        private readonly IAccommodationConfigurationManager configurationManager;
        private IAvraBookingRequestFactory _avraBookingRequestFactory;
        
        private readonly IProviderOutputLogger outputLogger;

        public AvraBookingAutomator(//ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IAvraBookingRequestFactory avraBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            //this.logger = logger;
            this.configurationManager = configurationManager;
            this._avraBookingRequestFactory = avraBookingRequestFactory;
            this.outputLogger = outputLogger;
        }




        public async Task<AvraBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            AvraBookingResponse response = null;

            try
            {

                ReservationSchema supplierRequest = this._avraBookingRequestFactory.CreateSupplierBookingRequest(request);

                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("AvraAvailabilityAutomator.GetAvailabilityResponseAsync: Avra supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<AvraBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, ReservationSchema supplierRequest)
        {
            string serialisedRequest = HttpUtility.UrlEncode(supplierRequest.XmlSerialize());
            
            CyberlogicReservationsSoapClient client = ConfigureSoapClient(request);
            
            var response = client.Reservation(serialisedRequest).OuterXml;
            AlphaRooms.Accommodation.Provider.Avra.BookResponse.ReservationSchema booking = response.XmlDeSerialize<AlphaRooms.Accommodation.Provider.Avra.BookResponse.ReservationSchema>();

            var supplierResponse = new AvraBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = booking
            };

            return supplierResponse;
        }

        private CyberlogicReservationsSoapClient ConfigureSoapClient(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(AvraAvailabilityUrl)));
            CyberlogicReservationsSoapClient client = new CyberlogicReservationsSoapClient(binding, address);


            return client;
        }
    }
}
