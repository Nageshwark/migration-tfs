﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using Ninject.Extensions.Logging;
using System.Web;
using AlphaRooms.Accommodation.Provider.Avra.Response;


namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraValuationAutomator : IAccommodationValuationAutomatorAsync<AvraValuationResponse>
    {
        private const string AvraAvailabilityUrl = "AvraAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAvraValuationRequestFactory _avraProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly ILogger _logger;

        public AvraValuationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IAvraValuationRequestFactory avraSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this._logger = logger;
            //this.configurationManager = configurationManager;
            this._avraProviderValuationRequestFactory = avraSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<AvraValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            AvraValuationResponse response = null;

            try
            {
                SearchHotelsRequest supplierRequest = this._avraProviderValuationRequestFactory.CreateSupplierValuationRequest(request);
                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("AvraAvailabilityAutomator.GetAvailabilityResponseAsync: Avra supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<AvraValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, SearchHotelsRequest supplierRequest)
        {
            string serialisedRequest = HttpUtility.UrlEncode(supplierRequest.XmlSerialize());

            CyberlogicReservationsSoapClient client = ConfigureSoapClient(request);


            var response = client.HotelsSearch(serialisedRequest).OuterXml;
            SearchHotels searchHotels = response.XmlDeSerialize<SearchHotels>();

            var supplierResponse = new AvraValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = searchHotels
            };

            return supplierResponse;
        }

        private CyberlogicReservationsSoapClient ConfigureSoapClient(AccommodationProviderValuationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(AvraAvailabilityUrl)));
            CyberlogicReservationsSoapClient client = new CyberlogicReservationsSoapClient(binding, address);

            return client;
        }

    }
}
