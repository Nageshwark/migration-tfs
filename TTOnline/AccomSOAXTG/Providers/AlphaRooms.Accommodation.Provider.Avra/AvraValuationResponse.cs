﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.Response;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraValuationResponse
    {
        public SearchHotelsRequest SupplierRequest { get; set; }
        public SearchHotels SupplierResponse { get; set; }

    }
}
