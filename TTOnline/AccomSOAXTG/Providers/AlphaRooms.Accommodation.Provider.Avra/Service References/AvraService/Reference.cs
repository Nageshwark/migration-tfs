﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlphaRooms.Accommodation.Provider.Avra.AvraService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.cyberlogic.gr/webservices/", ConfigurationName="AvraService.CyberlogicReservationsSoap")]
    public interface CyberlogicReservationsSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PlaceSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PlaceSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PlaceSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PlaceSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PlaceDescription", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PlaceDescription(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PlaceDescription", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PlaceDescriptionAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionPriceZones", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ExcursionPriceZones(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionPriceZones", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionPriceZonesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionAccommodation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ExcursionAccommodation(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionAccommodation", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionAccommodationAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ExcursionSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionDescription", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ExcursionDescription(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionDescription", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionDescriptionAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ExcursionList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ExcursionList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageDestinations", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PackageDestinations(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageDestinations", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PackageDestinationsAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PackageSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PackageSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageRoomPrices", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PackageRoomPrices(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageRoomPrices", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PackageRoomPricesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PackageList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PackageListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageCategories", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode PackageCategories(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/PackageCategories", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> PackageCategoriesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalAccommodation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode RentalAccommodation(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalAccommodation", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RentalAccommodationAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalStations", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode RentalStations(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalStations", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RentalStationsAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalSuppliers", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode RentalSuppliers(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalSuppliers", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RentalSuppliersAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalModels", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode RentalModels(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalModels", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RentalModelsAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode RentalSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/RentalSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RentalSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferVehicles", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode TransferVehicles(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferVehicles", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> TransferVehiclesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferRoutePoints", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode TransferRoutePoints(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferRoutePoints", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> TransferRoutePointsAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode TransferSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> TransferSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode TransferList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/TransferList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> TransferListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelsSearch", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelsSearch(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelsSearch", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelsSearchAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelsSearch_MongoDB", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelsSearch_MongoDB(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelsSearch_MongoDB", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelsSearch_MongoDBAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelRoomList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/FullRoomList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode FullRoomList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/FullRoomList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> FullRoomListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelInfo(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelInfoAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelRoomInfo(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomInfoAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomAvailability", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelRoomAvailability(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelRoomAvailability", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomAvailabilityAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/LocationCategories", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode LocationCategories(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/LocationCategories", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> LocationCategoriesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/Reservation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode Reservation(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/Reservation", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ReservationList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ReservationList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ReservationList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationListAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ReservationCancellation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ReservationCancellation(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ReservationCancellation", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationCancellationAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ConfirmPendingReservation", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode ConfirmPendingReservation(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/ConfirmPendingReservation", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> ConfirmPendingReservationAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/MyContracts", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode MyContracts(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/MyContracts", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> MyContractsAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/StopSales", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode StopSales(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/StopSales", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> StopSalesAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelOffers", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode HotelOffers(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/HotelOffers", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> HotelOffersAsync(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/OfferList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Xml.XmlNode OfferList(string xml);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.cyberlogic.gr/webservices/OfferList", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> OfferListAsync(string xml);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface CyberlogicReservationsSoapChannel : AlphaRooms.Accommodation.Provider.Avra.AvraService.CyberlogicReservationsSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CyberlogicReservationsSoapClient : System.ServiceModel.ClientBase<AlphaRooms.Accommodation.Provider.Avra.AvraService.CyberlogicReservationsSoap>, AlphaRooms.Accommodation.Provider.Avra.AvraService.CyberlogicReservationsSoap {
        
        public CyberlogicReservationsSoapClient() {
        }
        
        public CyberlogicReservationsSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CyberlogicReservationsSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CyberlogicReservationsSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CyberlogicReservationsSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Xml.XmlNode PlaceSearch(string xml) {
            return base.Channel.PlaceSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PlaceSearchAsync(string xml) {
            return base.Channel.PlaceSearchAsync(xml);
        }
        
        public System.Xml.XmlNode PlaceDescription(string xml) {
            return base.Channel.PlaceDescription(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PlaceDescriptionAsync(string xml) {
            return base.Channel.PlaceDescriptionAsync(xml);
        }
        
        public System.Xml.XmlNode ExcursionPriceZones(string xml) {
            return base.Channel.ExcursionPriceZones(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionPriceZonesAsync(string xml) {
            return base.Channel.ExcursionPriceZonesAsync(xml);
        }
        
        public System.Xml.XmlNode ExcursionAccommodation(string xml) {
            return base.Channel.ExcursionAccommodation(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionAccommodationAsync(string xml) {
            return base.Channel.ExcursionAccommodationAsync(xml);
        }
        
        public System.Xml.XmlNode ExcursionSearch(string xml) {
            return base.Channel.ExcursionSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionSearchAsync(string xml) {
            return base.Channel.ExcursionSearchAsync(xml);
        }
        
        public System.Xml.XmlNode ExcursionDescription(string xml) {
            return base.Channel.ExcursionDescription(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionDescriptionAsync(string xml) {
            return base.Channel.ExcursionDescriptionAsync(xml);
        }
        
        public System.Xml.XmlNode ExcursionList(string xml) {
            return base.Channel.ExcursionList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ExcursionListAsync(string xml) {
            return base.Channel.ExcursionListAsync(xml);
        }
        
        public System.Xml.XmlNode PackageDestinations(string xml) {
            return base.Channel.PackageDestinations(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PackageDestinationsAsync(string xml) {
            return base.Channel.PackageDestinationsAsync(xml);
        }
        
        public System.Xml.XmlNode PackageSearch(string xml) {
            return base.Channel.PackageSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PackageSearchAsync(string xml) {
            return base.Channel.PackageSearchAsync(xml);
        }
        
        public System.Xml.XmlNode PackageRoomPrices(string xml) {
            return base.Channel.PackageRoomPrices(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PackageRoomPricesAsync(string xml) {
            return base.Channel.PackageRoomPricesAsync(xml);
        }
        
        public System.Xml.XmlNode PackageList(string xml) {
            return base.Channel.PackageList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PackageListAsync(string xml) {
            return base.Channel.PackageListAsync(xml);
        }
        
        public System.Xml.XmlNode PackageCategories(string xml) {
            return base.Channel.PackageCategories(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> PackageCategoriesAsync(string xml) {
            return base.Channel.PackageCategoriesAsync(xml);
        }
        
        public System.Xml.XmlNode RentalAccommodation(string xml) {
            return base.Channel.RentalAccommodation(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RentalAccommodationAsync(string xml) {
            return base.Channel.RentalAccommodationAsync(xml);
        }
        
        public System.Xml.XmlNode RentalStations(string xml) {
            return base.Channel.RentalStations(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RentalStationsAsync(string xml) {
            return base.Channel.RentalStationsAsync(xml);
        }
        
        public System.Xml.XmlNode RentalSuppliers(string xml) {
            return base.Channel.RentalSuppliers(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RentalSuppliersAsync(string xml) {
            return base.Channel.RentalSuppliersAsync(xml);
        }
        
        public System.Xml.XmlNode RentalModels(string xml) {
            return base.Channel.RentalModels(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RentalModelsAsync(string xml) {
            return base.Channel.RentalModelsAsync(xml);
        }
        
        public System.Xml.XmlNode RentalSearch(string xml) {
            return base.Channel.RentalSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RentalSearchAsync(string xml) {
            return base.Channel.RentalSearchAsync(xml);
        }
        
        public System.Xml.XmlNode TransferVehicles(string xml) {
            return base.Channel.TransferVehicles(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> TransferVehiclesAsync(string xml) {
            return base.Channel.TransferVehiclesAsync(xml);
        }
        
        public System.Xml.XmlNode TransferRoutePoints(string xml) {
            return base.Channel.TransferRoutePoints(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> TransferRoutePointsAsync(string xml) {
            return base.Channel.TransferRoutePointsAsync(xml);
        }
        
        public System.Xml.XmlNode TransferSearch(string xml) {
            return base.Channel.TransferSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> TransferSearchAsync(string xml) {
            return base.Channel.TransferSearchAsync(xml);
        }
        
        public System.Xml.XmlNode TransferList(string xml) {
            return base.Channel.TransferList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> TransferListAsync(string xml) {
            return base.Channel.TransferListAsync(xml);
        }
        
        public System.Xml.XmlNode HotelsSearch(string xml) {
            return base.Channel.HotelsSearch(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelsSearchAsync(string xml) {
            return base.Channel.HotelsSearchAsync(xml);
        }
        
        public System.Xml.XmlNode HotelsSearch_MongoDB(string xml) {
            return base.Channel.HotelsSearch_MongoDB(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelsSearch_MongoDBAsync(string xml) {
            return base.Channel.HotelsSearch_MongoDBAsync(xml);
        }
        
        public System.Xml.XmlNode HotelList(string xml) {
            return base.Channel.HotelList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelListAsync(string xml) {
            return base.Channel.HotelListAsync(xml);
        }
        
        public System.Xml.XmlNode HotelRoomList(string xml) {
            return base.Channel.HotelRoomList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomListAsync(string xml) {
            return base.Channel.HotelRoomListAsync(xml);
        }
        
        public System.Xml.XmlNode FullRoomList(string xml) {
            return base.Channel.FullRoomList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> FullRoomListAsync(string xml) {
            return base.Channel.FullRoomListAsync(xml);
        }
        
        public System.Xml.XmlNode HotelInfo(string xml) {
            return base.Channel.HotelInfo(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelInfoAsync(string xml) {
            return base.Channel.HotelInfoAsync(xml);
        }
        
        public System.Xml.XmlNode HotelRoomInfo(string xml) {
            return base.Channel.HotelRoomInfo(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomInfoAsync(string xml) {
            return base.Channel.HotelRoomInfoAsync(xml);
        }
        
        public System.Xml.XmlNode HotelRoomAvailability(string xml) {
            return base.Channel.HotelRoomAvailability(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelRoomAvailabilityAsync(string xml) {
            return base.Channel.HotelRoomAvailabilityAsync(xml);
        }
        
        public System.Xml.XmlNode LocationCategories(string xml) {
            return base.Channel.LocationCategories(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> LocationCategoriesAsync(string xml) {
            return base.Channel.LocationCategoriesAsync(xml);
        }
        
        public System.Xml.XmlNode Reservation(string xml) {
            return base.Channel.Reservation(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationAsync(string xml) {
            return base.Channel.ReservationAsync(xml);
        }
        
        public System.Xml.XmlNode ReservationList(string xml) {
            return base.Channel.ReservationList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationListAsync(string xml) {
            return base.Channel.ReservationListAsync(xml);
        }
        
        public System.Xml.XmlNode ReservationCancellation(string xml) {
            return base.Channel.ReservationCancellation(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ReservationCancellationAsync(string xml) {
            return base.Channel.ReservationCancellationAsync(xml);
        }
        
        public System.Xml.XmlNode ConfirmPendingReservation(string xml) {
            return base.Channel.ConfirmPendingReservation(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> ConfirmPendingReservationAsync(string xml) {
            return base.Channel.ConfirmPendingReservationAsync(xml);
        }
        
        public System.Xml.XmlNode MyContracts(string xml) {
            return base.Channel.MyContracts(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> MyContractsAsync(string xml) {
            return base.Channel.MyContractsAsync(xml);
        }
        
        public System.Xml.XmlNode StopSales(string xml) {
            return base.Channel.StopSales(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> StopSalesAsync(string xml) {
            return base.Channel.StopSalesAsync(xml);
        }
        
        public System.Xml.XmlNode HotelOffers(string xml) {
            return base.Channel.HotelOffers(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> HotelOffersAsync(string xml) {
            return base.Channel.HotelOffersAsync(xml);
        }
        
        public System.Xml.XmlNode OfferList(string xml) {
            return base.Channel.OfferList(xml);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> OfferListAsync(string xml) {
            return base.Channel.OfferListAsync(xml);
        }
    }
}
