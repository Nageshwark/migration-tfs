﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<AvraCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<AvraCancellationResponse> parser;

        public AvraCancellationProvider(IAccommodationCancellationAutomatorAsync<AvraCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<AvraCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);

            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
