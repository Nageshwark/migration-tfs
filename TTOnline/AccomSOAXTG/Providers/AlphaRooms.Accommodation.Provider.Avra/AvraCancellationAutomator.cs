﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Web;
using AlphaRooms.Accommodation.Provider.Avra.CancelRequest;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraCancellationAutomator : IAccommodationCancellationAutomatorAsync<AvraCancellationResponse>
    {
        private const string AvraAvailabilityUrl = "AvraAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAvraCancellationRequestFactory _avraCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public AvraCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IAvraCancellationRequestFactory avraCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._avraCancellationRequestFactory = avraCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<AvraCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            AvraCancellationResponse response = null;

            try
            {
                ReservationCancellation supplierRequest = this._avraCancellationRequestFactory.CreateSupplierCancelRequest(request);
                response = await GetSupplierCancellationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("AvraAvailabilityAutomator.GetAvailabilityResponseAsync: Avra supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }
            return response;
        }

        private async Task<AvraCancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, AlphaRooms.Accommodation.Provider.Avra.CancelRequest.ReservationCancellation supplierRequest)
        {
            string serialisedRequest = HttpUtility.UrlEncode(supplierRequest.XmlSerialize());
            
            CyberlogicReservationsSoapClient client = ConfigureSoapClient(request);
            
            var response = client.ReservationCancellation(serialisedRequest).OuterXml;
            AlphaRooms.Accommodation.Provider.Avra.CancelResponse.ReservationCancellation cancelresponse = response.XmlDeSerialize<AlphaRooms.Accommodation.Provider.Avra.CancelResponse.ReservationCancellation>();

            var supplierResponse = new AvraCancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = cancelresponse
            };

            return supplierResponse;
        }

        private CyberlogicReservationsSoapClient ConfigureSoapClient(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(AvraAvailabilityUrl)));
            CyberlogicReservationsSoapClient client = new CyberlogicReservationsSoapClient(binding, address);


            return client;
        }
    }
}
