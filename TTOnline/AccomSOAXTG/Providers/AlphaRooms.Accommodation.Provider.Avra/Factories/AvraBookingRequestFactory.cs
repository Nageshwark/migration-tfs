﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.BookRequest;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraBookingRequestFactory : IAvraBookingRequestFactory
    {
        private const string AvraUsername = "AvraUsername";
        private const string AvraPassword = "AvraPassword";
        private const string AvraPrimaryLanguage = "AvraPrimaryLanguage";


        public ReservationSchema CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms.First().ValuationResult;

            var supplierRequest = new ReservationSchema
            {
                Username = request.Provider.Parameters.GetParameterValue(AvraUsername),
                Password = request.Provider.Parameters.GetParameterValue(AvraPassword),
                Reference = request.BookingId.ToString(),
                BookingType = 1,
                BookingStatus = 1,
                UseHotelTariff = true,
                ArrivalDate = roomToBook.CheckInDate,
                DepartureDate = roomToBook.CheckOutDate,
                Adults = GetAdults(request),
                Services = HotelServices(request),
                //ClientTransferArrivalType = 1,
                //ClientTransferDepartureType = 1
            };
            return supplierRequest;
        }

        private PersonSchema[] GetAdults(AccommodationProviderBookingRequest request)
        {
            List<PersonSchema> persons = new List<PersonSchema>();

            foreach(var rm in request.ValuatedRooms)
            {
                for (int cnt = 1; cnt <= rm.Guests.AdultsCount; cnt++)
                {
                    var p = new PersonSchema{
                        Title = (int)request.Customer.Title,
                        Name = request.Customer.FirstName,
                        Surname = request.Customer.Surname,
                        Reference = "A" + cnt.ToString()
                    };
                    persons.Add(p);
                }
            }

            return persons.ToArray();
        }

        private PersonSchema[] GetChildren(AccommodationProviderBookingRequest request)
        {
            List<PersonSchema> persons = new List<PersonSchema>();

            foreach(var rm in request.ValuatedRooms)
            {
                for (int cnt = 1; cnt <= rm.Guests.ChildrenCount; cnt++)
                {
                    var p = new PersonSchema{
                        Title = (int)request.Customer.Title,
                        Name = request.Customer.FirstName,
                        Surname = request.Customer.Surname,
                        Reference = "C" + cnt.ToString()
                    };
                    persons.Add(p);
                }
            }

            return persons.ToArray();
        }

        private ReservationSchemaServices HotelServices(AccommodationProviderBookingRequest request)
        {
            ReservationSchemaServices reservationServices = new ReservationSchemaServices();
            reservationServices.Hotels = GetSelectedHotel(request);
            
            return reservationServices;
        }

        private HotelSchema[] GetSelectedHotel(AccommodationProviderBookingRequest request)
        {
            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms.First().ValuationResult;

            List<HotelSchema> hlist = new List<HotelSchema>();
            HotelSchema h = new HotelSchema
            {
                ArrivalDate = roomToBook.CheckInDate,
                BoardBasis = (int)Enum.Parse(typeof(Board), roomToBook.ProviderSpecificData["BoardCode"]),
                ContractID = Convert.ToInt32(roomToBook.ProviderSpecificData["ContractId"]),
                DepartureDate = roomToBook.CheckOutDate,
                HotelID = Convert.ToInt32(roomToBook.EstablishmentEdiCode) ,
                RoomID =  Convert.ToInt32(roomToBook.ProviderSpecificData["RoomId"]),
                RoomQuantity = request.ValuatedRooms.Count(),
                ClientList = GetClientList(request),
            };
            hlist.Add(h);
            return hlist.ToArray();
        }

        private ClientReferenceClient[] GetClientList(AccommodationProviderBookingRequest request)
        {
            List<ClientReferenceClient> cl = new List<ClientReferenceClient>();
            int adults = request.ValuatedRooms.Sum(r => r.Guests.AdultsCount);
            int children = request.ValuatedRooms.Sum(r => r.Guests.ChildrenCount);

            for (int cnt = 1; cnt <= adults; cnt++)
            {
                ClientReferenceClient c = new ClientReferenceClient
                {
                    Reference = "A" + cnt.ToString()
                };
                cl.Add(c);
            }
            for (int cnt = 1; cnt <= children; cnt++)
            {
                ClientReferenceClient c = new ClientReferenceClient
                {
                    Reference = "C" + cnt.ToString()
                };
                cl.Add(c);
            }
            
            return cl.ToArray();
        }

        public enum Board
        {
            RR = 1,
            BB = 2,
            HB = 3,
            FB = 4,
            AI = 5,
            UI = 6
        }
    }
}
