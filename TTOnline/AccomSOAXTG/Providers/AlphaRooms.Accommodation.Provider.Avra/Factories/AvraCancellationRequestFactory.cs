﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.CancelRequest;
using System;

namespace AlphaRooms.Accommodation.Provider.Avra.Factories
{
    public class AvraCancellationRequestFactory : IAvraCancellationRequestFactory
    {
        private const string AvraUsername = "AvraUsername";
        private const string AvraPassword = "AvraPassword";
        private const string AvraPrimaryLanguage = "AvraPrimaryLanguage";


        public ReservationCancellation CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new ReservationCancellation
            {
                Username = request.Provider.Parameters.GetParameterValue(AvraUsername),
                Password = request.Provider.Parameters.GetParameterValue(AvraPassword),
                ReservationID = Convert.ToInt32(request.ProviderBookingReference)
            };
            return supplierRequest;
        }

       
    }
}
