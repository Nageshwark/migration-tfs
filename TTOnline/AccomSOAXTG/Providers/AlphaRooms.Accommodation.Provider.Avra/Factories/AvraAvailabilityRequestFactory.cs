﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;

namespace AlphaRooms.Accommodation.Provider.Avra.Factories
{
    public class AvraAvailabilityRequestFactory : IAvraAvailabilityRequestFactory
    {
        private const string AvraUsername = "AvraUsername";
        private const string AvraPassword = "AvraPassword";
        private const string AvraPrimaryLanguage = "AvraPrimaryLanguage";

        
        public SearchHotelsRequest CreateSupplierAvailabilityRequest(
            AccommodationProviderAvailabilityRequest request)
        {

            var supplierRequest = new SearchHotelsRequest
            {
                Username = request.Provider.Parameters.GetParameterValue(AvraUsername),
                Password = request.Provider.Parameters.GetParameterValue(AvraPassword),
                DateFrom = Convert.ToDateTime(request.CheckInDate.ToString("yyyy-MM-dd")),
                DateTo = Convert.ToDateTime(request.CheckOutDate.ToString("yyyy-MM-dd")),
                Rooms = GetRooms(request),
                Language = request.Provider.Parameters.GetParameterValue(AvraPrimaryLanguage),
                PrefectureSpecified = request.DestinationCodes.Any() ? true : false,
                Prefecture =  request.DestinationCodes.Any() ? Convert.ToInt16(request.DestinationCodes[0]) : 0,
                HotelIDSpecified = request.EstablishmentCodes.Any() ? true : false,
                HotelID = request.EstablishmentCodes.Any() ? Convert.ToInt32(request.EstablishmentCodes[0]) : 0,
                UseTariff = true,
                UseTariffSpecified = true,
                IsOptimizedSpecified = true,
                IsOptimized = false
            };
            
            return supplierRequest;
        }

        private AlphaRooms.Accommodation.Provider.Avra.Request.RoomType[] GetRooms(AccommodationProviderAvailabilityRequest request)
        {
            List<AlphaRooms.Accommodation.Provider.Avra.Request.RoomType> rooms = new List<AlphaRooms.Accommodation.Provider.Avra.Request.RoomType>();
            foreach (AccommodationProviderAvailabilityRequestRoom rm in request.Rooms)
            {
                AlphaRooms.Accommodation.Provider.Avra.Request.RoomType room = new AlphaRooms.Accommodation.Provider.Avra.Request.RoomType();
                room.Adults = rm.Guests.AdultsCount;
                room.Children = rm.Guests.ChildrenAndInfantsCount;
                room.ChildrenAges = String.Join(",", rm.Guests.ChildAndInfantAges.Select(a => a.ToString())); 
                rooms.Add(room);
            }
            return rooms.ToArray();
        }

    }
}
