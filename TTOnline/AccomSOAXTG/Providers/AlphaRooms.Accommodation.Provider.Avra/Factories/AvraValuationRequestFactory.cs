﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;

namespace AlphaRooms.Accommodation.Provider.Avra.Factories
{
    public class AvraValuationRequestFactory : IAvraValuationRequestFactory
    {
        private const string AvraUsername = "AvraUsername";
        private const string AvraPassword = "AvraPassword";
        private const string AvraPrimaryLanguage = "AvraPrimaryLanguage";

        
        SearchHotelsRequest IAvraValuationRequestFactory.CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            var supplierRequest = new SearchHotelsRequest
            {
                Username = request.Provider.Parameters.GetParameterValue(AvraUsername),
                Password = request.Provider.Parameters.GetParameterValue(AvraPassword),
                DateFrom = Convert.ToDateTime(availabilityRequest.CheckInDate.ToString("yyyy-MM-dd")),
                DateTo = Convert.ToDateTime(availabilityRequest.CheckOutDate.ToString("yyyy-MM-dd")),
                Rooms = GetRooms(request),
                Language = request.Provider.Parameters.GetParameterValue(AvraPrimaryLanguage),
                HotelIDSpecified = true ,
                HotelID = Convert.ToInt32(availabilityRequest.EstablishmentCodes[0]),
                Board = request.SelectedRooms[0].AvailabilityResult.ProviderSpecificData["BoardCode"],
                //RoomName = request.SelectedRooms[0].AvailabilityResult.ProviderSpecificData["RoomName"],
                UseTariff = true,
                UseTariffSpecified = true,
                IsOptimizedSpecified = true,
                IsOptimized = false
            };

            return supplierRequest;
        }


        private Request.RoomType[] GetRooms(AccommodationProviderValuationRequest request)
        {
            var rooms = new List<Request.RoomType>();
            foreach (AccommodationProviderValuationRequestRoom rm in request.SelectedRooms)
            {
                Request.RoomType room = new Request.RoomType();
                 room.Adults = rm.Guests.AdultsCount;
                 room.Children = rm.Guests.ChildrenAndInfantsCount;
                 room.ChildrenAges = String.Join(",", rm.Guests.ChildAndInfantAges.Select(a => a.ToString())); 
                rooms.Add(room);
            }
            return rooms.ToArray();
        }
    }
}
