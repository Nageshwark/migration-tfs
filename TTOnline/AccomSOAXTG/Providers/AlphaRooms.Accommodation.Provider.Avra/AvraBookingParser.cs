﻿using System.Linq;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraBookingParser : IAccommodationBookingParser<AvraBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public AvraBookingParser(ILogger logger
                                        //,IAccommodationConfigurationManager configurationManager
                                        )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }



        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, AvraBookingResponse response)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Avra Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                AlphaRooms.Accommodation.Provider.Avra.BookRequest.ReservationSchema supplierRequest,
                                                AlphaRooms.Accommodation.Provider.Avra.BookResponse.ReservationSchema supplierResponse,
                                                ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse.Response != null
                               );

            if (hasResults )
            {
                if (supplierResponse.Response.Status == "Confirmed")
                {
                    var result = new AccommodationProviderBookingResult();
                    result.BookingStatus = BookingStatus.Confirmed;
                    result.ProviderBookingReference = supplierResponse.Response.ReservationID.ToString();
                    bookingResults.Enqueue(result);
                }
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("Avra Booking Parser: Error in Boooking.");

                if (supplierResponse != null && supplierResponse.ErrorList != null)
                {
                    sbMessage.AppendLine("Error Details:");
                    foreach (var e in supplierResponse.ErrorList.Errors)
                    {
                        sbMessage.AppendLine(string.Format("Description:{0}", e));
                    }
                }

                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }
    }
}
