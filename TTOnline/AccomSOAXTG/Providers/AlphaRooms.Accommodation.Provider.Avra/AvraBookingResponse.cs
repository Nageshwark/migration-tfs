﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that ECTravel accepts and returns XML strings.
    /// </summary>
    public class AvraBookingResponse
    {
        public AlphaRooms.Accommodation.Provider.Avra.BookRequest.ReservationSchema  SupplierRequest { get; set; }
        public AlphaRooms.Accommodation.Provider.Avra.BookResponse.ReservationSchema SupplierResponse { get; set; }
    }
}
