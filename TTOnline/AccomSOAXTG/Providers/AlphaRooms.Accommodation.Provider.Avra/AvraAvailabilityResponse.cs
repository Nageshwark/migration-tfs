﻿using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraAvailabilityResponse
    {
        public SearchHotelsRequest SupplierRequest { get; set; }
        public SearchHotels SupplierResponse { get; set; }

    }
}
