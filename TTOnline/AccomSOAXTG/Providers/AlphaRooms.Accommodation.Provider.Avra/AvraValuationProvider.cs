﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<AvraValuationResponse> automator;
        private readonly IAccommodationValuationParser<AvraValuationResponse> parser;

        public AvraValuationProvider(IAccommodationValuationAutomatorAsync<AvraValuationResponse> automator,
                                                IAccommodationValuationParser<AvraValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
