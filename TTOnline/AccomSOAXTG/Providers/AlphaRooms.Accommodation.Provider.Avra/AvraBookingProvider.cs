﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<AvraBookingResponse> automator;
        private readonly IAccommodationBookingParser<AvraBookingResponse> parser;

        public AvraBookingProvider(IAccommodationBookingAutomatorAsync<AvraBookingResponse> automator,
                                        IAccommodationBookingParser<AvraBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
