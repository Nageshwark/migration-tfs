﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra;

using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.Avra.Response;
using AlphaRooms.SOACommon.Interfaces;
//using AlphaRooms.SOACommon.Interfaces;


namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraAvailabilityParser:IAccommodationAvailabilityParser<AvraAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public AvraAvailabilityParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, AvraAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                if (request.Debugging)
                {
                    string serialisedResponse = response.SupplierResponse.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Avra Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, AvraAvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            
            bool hasResults = supplierResponse.SupplierResponse.Response != null && supplierResponse.SupplierResponse.Response.Hotels.Records > 0;

            if (hasResults)
            {
                foreach (var hotel in supplierResponse.SupplierResponse.Response.Hotels.Hotel)
                {
                    foreach (var room in hotel.Rooms.Rooms)
                    {
                        int roomNo = 1;
                        if (room.Occupancy.Count() != supplierResponse.SupplierResponse.Response.Hotels.NumberOfRooms)
                            continue;

                        foreach (var occupancy in room.Occupancy)
                        {
                            Board[] boards = GetBoards(occupancy);
                            foreach (Board board in boards)
                            {
                                var result = new AccommodationProviderAvailabilityResult();

                                result.RoomNumber = Convert.ToByte(roomNo);
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;

                                result.DestinationEdiCode = hotel.CityName;
                                result.EstablishmentEdiCode = hotel.ID.ToString();
                                result.EstablishmentName = hotel.Name;
                                result.RoomDescription = room.Name;
                                result.RoomCode = room.ID.ToString();
                                result.BoardCode = board.BoardCode;

                                result.CheckInDate = supplierResponse.SupplierResponse.DateFrom;
                                result.CheckOutDate = supplierResponse.SupplierResponse.DateTo;

                                result.Adults = request.Rooms[result.RoomNumber - 1].Guests.AdultsCount;
                                result.Children = (byte)(request.Rooms[result.RoomNumber - 1].Guests.ChildrenCount + request.Rooms[result.RoomNumber - 1].Guests.InfantsCount);

                                result.SalePrice = new Money()
                                {
                                    Amount = board.Price,
                                    CurrencyCode = hotel.Currency
                                };

                                result.CostPrice = result.SalePrice;

                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                result.IsOpaqueRate = false;
                                result.IsNonRefundable = Convert.ToBoolean(occupancy.NonRefundable);
                                result.NumberOfAvailableRooms = occupancy.Availability;

                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("BoardCode", board.BoardCode);
                                result.ProviderSpecificData.Add("RoomId", room.ID.ToString());
                                result.ProviderSpecificData.Add("RoomName", room.Name);

                                availabilityResults.Enqueue(result);
                            }
                            roomNo++;
                        }
                        
                    }
                }
            }
            else
            {
                

                if (supplierResponse != null && supplierResponse.SupplierResponse.ErrorList != null)
                {
                    var sbMessage = new StringBuilder();
                    sbMessage.AppendLine( "Avra Availability Parser: Availability Response cannot be parsed because it is null.");

                    foreach(var e in supplierResponse.SupplierResponse.ErrorList.Errors)
                    {
                        sbMessage.AppendLine(string.Format("Error Details: {0}", e));
                    }

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }
            
        }

        private Board[] GetBoards(SearchHotelsResponseHotelsHotelRoomsRoomOccupancy occ)
        {
            List<Board> boards = new List<Board>();
            if (!string.IsNullOrEmpty(occ.RR.Discount))
            {
                boards.Add(new Board("RR",occ.RR.Discount));
            }

            if (!string.IsNullOrEmpty(occ.BB.Discount))
            {
                boards.Add(new Board("BB", occ.BB.Discount));
            }

            if (!string.IsNullOrEmpty(occ.HB.Discount))
            {
                boards.Add(new Board("HB", occ.HB.Discount));
            }

            if (!string.IsNullOrEmpty(occ.FB.Discount))
            {
                boards.Add(new Board("FB", occ.FB.Discount));
            }

            if (!string.IsNullOrEmpty(occ.AI.Discount))
            {
                boards.Add(new Board("AI", occ.AI.Discount));
            }

            if (!string.IsNullOrEmpty(occ.UI.Discount))
            {
                boards.Add(new Board("UI", occ.UI.Discount));
            }


            return boards.ToArray();
        }
    
    }

    public class Board
    {
        private string _code;
        private decimal _price;
        public string BoardCode
        {
            get { return _code; }
        }
        public decimal Price
        {
            get { return _price; }
        }

        public Board(string code, string price){
            _code = code;
            _price = Convert.ToDecimal(price);
        }
    }
}
