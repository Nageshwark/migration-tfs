﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraCancellationParser : IAccommodationCancellationParserAsync<AvraCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public AvraCancellationParser(ILogger logger
            //, IAccommodationConfigurationManager configurationManagerECTravel
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }



        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(
            AccommodationProviderCancellationRequest request, AvraCancellationResponse response)
        {
            var cancellationResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();

            if (response.SupplierResponse.ErrorList != null)
            {
                foreach (var item in response.SupplierResponse.ErrorList.Errors)
                {
                    
                    cancellationResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the cancellation response from Avra.");
                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(request.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.XmlSerialize());

                    cancellationResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                       
                }
            }
            else
            {
                cancellationResult.CancellationStatus = CancellationStatus.Succeeded;
                cancellationResult.ProviderCancellationReference = response.SupplierResponse.Response.Reference;
                cancellationResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled."
                                             + response.SupplierResponse.Response.Reference;

                cancellationResults.Add(cancellationResult);
            }
            
            return cancellationResults;
        }
    }
}
