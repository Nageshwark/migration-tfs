﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=4.0.30319.33440.
// 

namespace AlphaRooms.Accommodation.Provider.Avra.Request
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SearchHotelsRequest
    {

        private string usernameField;

        private string passwordField;

        private System.DateTime dateFromField;

        private System.DateTime dateToField;

        private RoomType[] roomsField;

        private string languageField;

        private string boardField;

        private int hotelIDField;

        private bool hotelIDFieldSpecified;

        private string hotelNameField;

        private string roomNameField;

        private SearchHotelsRequestHoteltype hoteltypeField;

        private bool hoteltypeFieldSpecified;

        private int locationField;

        private bool locationFieldSpecified;

        private int ratingField;

        private bool ratingFieldSpecified;

        private string countryField;

        private int provinceField;

        private bool provinceFieldSpecified;

        private int prefectureField;

        private bool prefectureFieldSpecified;

        private int cityField;

        private bool cityFieldSpecified;

        private bool useTariffField;

        private bool useTariffFieldSpecified;

        private bool isOptimizedField;

        private bool isOptimizedFieldSpecified;

        public SearchHotelsRequest()
        {
            this.languageField = "en";
        }

        /// <remarks/>
        public string Username
        {
            get
            {
                return this.usernameField;
            }
            set
            {
                this.usernameField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DateFrom
        {
            get
            {
                return this.dateFromField;
            }
            set
            {
                this.dateFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DateTo
        {
            get
            {
                return this.dateToField;
            }
            set
            {
                this.dateToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Room", IsNullable = false)]
        public RoomType[] Rooms
        {
            get
            {
                return this.roomsField;
            }
            set
            {
                this.roomsField = value;
            }
        }

        /// <remarks/>
        public string Language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        public string Board
        {
            get
            {
                return this.boardField;
            }
            set
            {
                this.boardField = value;
            }
        }

        /// <remarks/>
        public int HotelID
        {
            get
            {
                return this.hotelIDField;
            }
            set
            {
                this.hotelIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HotelIDSpecified
        {
            get
            {
                return this.hotelIDFieldSpecified;
            }
            set
            {
                this.hotelIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string HotelName
        {
            get
            {
                return this.hotelNameField;
            }
            set
            {
                this.hotelNameField = value;
            }
        }

        /// <remarks/>
        public string RoomName
        {
            get
            {
                return this.roomNameField;
            }
            set
            {
                this.roomNameField = value;
            }
        }

        /// <remarks/>
        public SearchHotelsRequestHoteltype Hoteltype
        {
            get
            {
                return this.hoteltypeField;
            }
            set
            {
                this.hoteltypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HoteltypeSpecified
        {
            get
            {
                return this.hoteltypeFieldSpecified;
            }
            set
            {
                this.hoteltypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LocationSpecified
        {
            get
            {
                return this.locationFieldSpecified;
            }
            set
            {
                this.locationFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RatingSpecified
        {
            get
            {
                return this.ratingFieldSpecified;
            }
            set
            {
                this.ratingFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public int Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ProvinceSpecified
        {
            get
            {
                return this.provinceFieldSpecified;
            }
            set
            {
                this.provinceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Prefecture
        {
            get
            {
                return this.prefectureField;
            }
            set
            {
                this.prefectureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrefectureSpecified
        {
            get
            {
                return this.prefectureFieldSpecified;
            }
            set
            {
                this.prefectureFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CitySpecified
        {
            get
            {
                return this.cityFieldSpecified;
            }
            set
            {
                this.cityFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool UseTariff
        {
            get
            {
                return this.useTariffField;
            }
            set
            {
                this.useTariffField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UseTariffSpecified
        {
            get
            {
                return this.useTariffFieldSpecified;
            }
            set
            {
                this.useTariffFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsOptimized
        {
            get
            {
                return this.isOptimizedField;
            }
            set
            {
                this.isOptimizedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsOptimizedSpecified
        {
            get
            {
                return this.isOptimizedFieldSpecified;
            }
            set
            {
                this.isOptimizedFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class RoomType
    {

        private int adultsField;

        private int childrenField;

        private string childrenAgesField;

        public RoomType()
        {
            this.childrenField = 0;
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Adults
        {
            get
            {
                return this.adultsField;
            }
            set
            {
                this.adultsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int Children
        {
            get
            {
                return this.childrenField;
            }
            set
            {
                this.childrenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChildrenAges
        {
            get
            {
                return this.childrenAgesField;
            }
            set
            {
                this.childrenAgesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum SearchHotelsRequestHoteltype
    {

        /// <remarks/>
        H,

        /// <remarks/>
        A,

        /// <remarks/>
        V,

        /// <remarks/>
        U,
    }
}