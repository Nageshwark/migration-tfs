﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.Request;

namespace AlphaRooms.Accommodation.Provider.Avra.Interfaces
{
    public interface IAvraAvailabilityRequestFactory
    {
        SearchHotelsRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
        //PlaceSearchRequest CreateSupplierPlaceRequest(AccommodationProviderAvailabilityRequest request);

        //HotelListRequest CreateSupplierHotelRequest(AccommodationProviderAvailabilityRequest request);
        //LocationCategoriesRequest CreateSupplierLocationRequest(AccommodationProviderAvailabilityRequest request);
    }
}
