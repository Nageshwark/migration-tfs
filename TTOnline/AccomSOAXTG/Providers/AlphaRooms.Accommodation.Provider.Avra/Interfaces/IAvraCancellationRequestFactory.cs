﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.CancelRequest;

namespace AlphaRooms.Accommodation.Provider.Avra.Interfaces
{
    public interface IAvraCancellationRequestFactory
    {
        ReservationCancellation CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
