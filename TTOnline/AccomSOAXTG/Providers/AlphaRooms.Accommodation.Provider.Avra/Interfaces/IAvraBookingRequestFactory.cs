﻿using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Provider.Avra.Request;
using AlphaRooms.Accommodation.Provider.Avra.BookRequest;

namespace AlphaRooms.Accommodation.Provider.Avra.Interfaces
{
    public interface IAvraBookingRequestFactory
    {
        ReservationSchema CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
