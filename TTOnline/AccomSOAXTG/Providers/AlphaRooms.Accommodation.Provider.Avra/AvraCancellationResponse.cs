﻿using AlphaRooms.Accommodation.Provider.Avra.AvraService;
using AlphaRooms.Accommodation.Provider.Avra.CancelRequest;
using AlphaRooms.Accommodation.Provider.Avra.Request;

namespace AlphaRooms.Accommodation.Provider.Avra
{
    public class AvraCancellationResponse
    {
        public AlphaRooms.Accommodation.Provider.Avra.CancelRequest.ReservationCancellation SupplierRequest { get; set; }
        public AlphaRooms.Accommodation.Provider.Avra.CancelResponse.ReservationCancellation SupplierResponse { get; set; }
    }
}
