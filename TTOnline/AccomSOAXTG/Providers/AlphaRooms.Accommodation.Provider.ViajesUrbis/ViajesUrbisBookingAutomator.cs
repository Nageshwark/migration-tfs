﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisBookingAutomator :ViajesUrbisAutomaterBase, IAccommodationBookingAutomatorAsync<IEnumerable<ViajesUrbisBookingResponse>>
    {
        private const string ViajesUrbisBookingUrl = "ViajesUrbisBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesUrbisProviderBookingRequestFactory ViajesUrbisProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ViajesUrbisBookingAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesUrbisProviderBookingRequestFactory ViajesUrbisSupplierBookingRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.ViajesUrbisProviderBookingRequestFactory = ViajesUrbisSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<IEnumerable<ViajesUrbisBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ViajesUrbisBookingResponse> responses = new List<ViajesUrbisBookingResponse>();

            cavalHotelBookingConfirmRQ supplierRequest = null;
            ViajesUrbisBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this.ViajesUrbisProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.ViajesUrbisProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesUrbisBookingAutomator.GetBookingResponseAsync: ViajesUrbis supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<ViajesUrbisBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, cavalHotelBookingConfirmRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapCleint(request.Provider.Parameters);

            var response = client.confirmHotelBooking(supplierRequest);

            var supplierResponse = new ViajesUrbisBookingResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        
       
    }
}
