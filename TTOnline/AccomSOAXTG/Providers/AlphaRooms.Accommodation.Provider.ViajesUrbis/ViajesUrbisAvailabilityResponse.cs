﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisAvailabilityResponse
    {
        public cavalHotelAvailabilityRQ SupplierRequest { get; set; }
        public cavalHotelAvailabilityRS SupplierResponse { get; set; }

    }
}
