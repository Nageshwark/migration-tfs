﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces
{
    public interface IViajesUrbisProviderValuationRequestFactory
    {
        cavalHotelBookingValuationRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
