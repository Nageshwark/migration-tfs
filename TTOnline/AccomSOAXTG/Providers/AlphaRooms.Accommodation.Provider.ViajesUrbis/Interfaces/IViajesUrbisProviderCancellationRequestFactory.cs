﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.CancellationService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces
{
    public interface IViajesUrbisProviderCancellationRequestFactory
    {
        cavalCancelBookingRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
