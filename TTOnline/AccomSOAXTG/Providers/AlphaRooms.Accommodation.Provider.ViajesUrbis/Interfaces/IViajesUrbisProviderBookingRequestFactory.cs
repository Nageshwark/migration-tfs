﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces
{
    public interface IViajesUrbisProviderBookingRequestFactory
    {
        cavalHotelBookingConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request);
    }
}
