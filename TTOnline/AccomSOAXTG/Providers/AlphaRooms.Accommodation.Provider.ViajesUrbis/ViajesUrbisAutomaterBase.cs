﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisAutomaterBase
    {
        private const string ViajesUrbisAvailabilityUrl = "ViajesUrbisAvailabilityUrl";
        private const string ViajesUrbisValuationUrl = "ViajesUrbisAvailabilityUrl";
        private const string ViajesUrbisBookingUrl = "ViajesUrbisBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        protected HotelBookingServiceClient ConfigureSoapCleint(List<AccommodationProviderParameter> parameters)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(parameters.GetParameterValue(MaxReceivedMessageSize));
           
            EndpointAddress address = null;
            
            if (this.GetType() == typeof(ViajesUrbisAvailabilityAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesUrbisAvailabilityUrl)));
            else if (this.GetType() == typeof(ViajesUrbisValuationAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesUrbisValuationUrl)));
            else if (this.GetType() == typeof(ViajesUrbisBookingAutomator))
                address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesUrbisBookingUrl)));
            
            HotelBookingServiceClient client = new HotelBookingServiceClient(binding, address);

            return client;
        }
    }
}
