﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisValuationAutomator :ViajesUrbisAutomaterBase, IAccommodationValuationAutomatorAsync<ViajesUrbisValuationResponse>
    {
        private const string ViajesUrbisValuationUrl = "ViajesUrbisValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
  
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesUrbisProviderValuationRequestFactory ViajesUrbisProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ViajesUrbisValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesUrbisProviderValuationRequestFactory ViajesUrbisSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.ViajesUrbisProviderValuationRequestFactory = ViajesUrbisSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<ViajesUrbisValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ViajesUrbisValuationResponse response = null;

            try
            {
                var supplierRequest = this.ViajesUrbisProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViajesUrbisValuationAutomator.GetValuationResponseAsync: ViajesUrbis supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ViajesUrbisValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapCleint(request.Provider.Parameters);

            var response = client.getDetailedValuation(supplierRequest);

            var supplierResponse = new ViajesUrbisValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        
      
    
    }
}
