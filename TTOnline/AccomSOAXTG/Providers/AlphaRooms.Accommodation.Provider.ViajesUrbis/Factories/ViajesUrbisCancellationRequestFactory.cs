﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.CancellationService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Factories
{
    public class ViajesUrbisCancellationRequestFactory : IViajesUrbisProviderCancellationRequestFactory
    {
        private const string ViajesUrbisLogin = "ViajesUrbisLogin";
        private const string ViajesUrbisPassword = "ViajesUrbisPassword";
        private const string ViajesUrbisLanguage = "ViajesUrbisLanguage";
        private const string ViajesUrbisGzip = "ViajesUrbisGzip";
        private const string ViajesUrbisAgentId = "ViajesUrbisAgentId";

        public cavalCancelBookingRQ CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var supplierRequest = new cavalCancelBookingRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(ViajesUrbisGzip)),
                login = request.Provider.Parameters.GetParameterValue(ViajesUrbisLogin),
                password = request.Provider.Parameters.GetParameterValue(ViajesUrbisPassword),
                agentId = request.Provider.Parameters.GetParameterValue(ViajesUrbisAgentId),
                language = request.Provider.Parameters.GetParameterValue(ViajesUrbisLanguage),
                gzipResponseSpecified = true,
                locator = request.ProviderBookingReference,
                rqId = Guid.NewGuid().ToString()

            };

            return supplierRequest;
        }
    }
}
