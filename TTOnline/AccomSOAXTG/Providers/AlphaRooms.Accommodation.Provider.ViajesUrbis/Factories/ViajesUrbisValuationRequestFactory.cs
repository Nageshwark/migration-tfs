﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Factories
{
    public class ViajesUrbisValuationRequestFactory : IViajesUrbisProviderValuationRequestFactory
    {
        private const string ViajesUrbisLogin = "ViajesUrbisLogin";
        private const string ViajesUrbisPassword = "ViajesUrbisPassword";
        private const string ViajesUrbisLanguage = "ViajesUrbisLanguage";
        private const string ViajesUrbisGzip = "ViajesUrbisGzip";
        private const string ViajesUrbisAgentId = "ViajesUrbisAgentId";

        public cavalHotelBookingValuationRQ CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            
            var availabilityRequest = request.SelectedRooms.First().AvailabilityRequest;
            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            var supplierRequest = new cavalHotelBookingValuationRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(ViajesUrbisGzip)),
                login = request.Provider.Parameters.GetParameterValue(ViajesUrbisLogin),
                password = request.Provider.Parameters.GetParameterValue(ViajesUrbisPassword),
                agentId = request.Provider.Parameters.GetParameterValue(ViajesUrbisAgentId),
                language = request.Provider.Parameters.GetParameterValue(ViajesUrbisLanguage),
                checkIn = availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = availabilityRequest.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.SelectedRooms),
                establishmentId = availabilityResult.EstablishmentEdiCode,
                boardCode = availabilityResult.BoardCode
            };

           
            
            return supplierRequest;
        }


        private occupation[] CreateOccupancy(AccommodationProviderValuationRequestRoom[] rooms)
        {
            var availRqOccupations = new List<occupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new occupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenAndInfantsCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1,
                    roomCode = room.AvailabilityResult.RoomCode
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }
        private int?[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => (int?)guest.Age).ToArray();

        }
       
    }
}
