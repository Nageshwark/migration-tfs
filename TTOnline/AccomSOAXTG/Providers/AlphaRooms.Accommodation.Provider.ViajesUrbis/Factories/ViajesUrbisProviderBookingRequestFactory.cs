﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisProviderBookingRequestFactory : IViajesUrbisProviderBookingRequestFactory
    {
        private const string ViajesUrbisLogin = "ViajesUrbisLogin";
        private const string ViajesUrbisPassword = "ViajesUrbisPassword";
        private const string ViajesUrbisLanguage = "ViajesUrbisLanguage";
        private const string ViajesUrbisGzip = "ViajesUrbisGzip";
        private const string ViajesUrbisAgentId = "ViajesUrbisAgentId";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public cavalHotelBookingConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;
            var availabilityRequest = request.ValuatedRooms.First().AvailabilityRequest;
            
            var supplierRequest = new cavalHotelBookingConfirmRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(ViajesUrbisGzip)),
                login = request.Provider.Parameters.GetParameterValue(ViajesUrbisLogin),
                password = request.Provider.Parameters.GetParameterValue(ViajesUrbisPassword),
                agentId = request.Provider.Parameters.GetParameterValue(ViajesUrbisAgentId),
                language = request.Provider.Parameters.GetParameterValue(ViajesUrbisLanguage),
                checkIn = availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = availabilityRequest.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.ValuatedRooms),
                establishmentId = valuationResult.EstablishmentEdiCode,
                boardCode = valuationResult.BoardCode,
                titular = string.Format("{0} {1} {2}", request.Customer.TitleString, request.Customer.FirstName, request.Customer.Surname),
                agencyEmail = "",
                agencyReference = Guid.NewGuid().ToString(),    //Todo:Do we pass it in?
                commentForHotel = ""
              
            };

            return supplierRequest;
        }


        private occupation[] CreateOccupancy(AccommodationProviderBookingRequestRoom[] rooms)
        {
            var availRqOccupations = new List<occupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new occupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenAndInfantsCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1,
                    roomCode = room.ValuationResult.RoomCode
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }
        private int?[] GetChildAges(AccommodationProviderBookingRequestRoomGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => (int?)guest.Age).ToArray();

        }
        
    }
}
