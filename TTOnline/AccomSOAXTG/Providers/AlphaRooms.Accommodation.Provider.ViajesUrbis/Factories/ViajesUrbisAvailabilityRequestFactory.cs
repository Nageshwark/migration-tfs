﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis.Factories
{
    public class ViajesUrbisAvailabilityRequestFactory : IViajesUrbisAvailabilityRequestFactory
    {
        private const string ViajesUrbisLogin = "ViajesUrbisLogin";
        private const string ViajesUrbisPassword = "ViajesUrbisPassword";
        private const string ViajesUrbisLanguage = "ViajesUrbisLanguage";
        private const string ViajesUrbisGzip = "ViajesUrbisGzip";
        private const string ViajesUrbisAgentId = "ViajesUrbisAgentId";


        public cavalHotelAvailabilityRQ CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var supplierRequest = new cavalHotelAvailabilityRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(ViajesUrbisGzip)),
                login = request.Provider.Parameters.GetParameterValue(ViajesUrbisLogin),
                password = request.Provider.Parameters.GetParameterValue(ViajesUrbisPassword),
                agentId =  request.Provider.Parameters.GetParameterValue(ViajesUrbisAgentId),
                language = request.Provider.Parameters.GetParameterValue(ViajesUrbisLanguage),
                checkIn = request.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = request.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.Rooms),
            };

            if (request.DestinationCodes != null && request.DestinationCodes.Any())
                supplierRequest.cityIds = request.DestinationCodes;
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())
                supplierRequest.establishmentIds = request.EstablishmentCodes;
            else
            {
                throw new System.Exception("Either DestinationCodes or EstablishmentCodes needs to be passed in");
            }

            
            return supplierRequest;
        }

        private availRQOccupation[] CreateOccupancy(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            List<availRQOccupation> availRqOccupations = new List<availRQOccupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new availRQOccupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenAndInfantsCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }

        private int?[] GetChildAges(AccommodationProviderAvailabilityRequestGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => (int?)guest.Age).ToArray();

        }
        /*
         * <?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
	<SOAP-ENV:Header/>
	<SOAP-ENV:Body>
		<getAvailableHotels xmlns="http://caval.travel/20091127/hotelBooking">
			<rq xmlns="">
				<gzipResponse>false</gzipResponse>
				<login>xml</login>
				<password>xml</password>
				<agentId>1</agentId>
				<language>es</language>
				<checkIn>01/06/2012</checkIn>
				<checkOut>08/06/2012</checkOut>
				<establishmentIds>24</establishmentIds>
				<establishmentIds>115</establishmentIds>
				<establishmentIds>32</establishmentIds>
				<occupations>
					<adultsPerRoom>2</adultsPerRoom>
					<childrenPerRoom>0</childrenPerRoom>
					<numberOfRooms>1</numberOfRooms>
				</occupations>
			</rq>
		</getAvailableHotels>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>

         */


    }
}
