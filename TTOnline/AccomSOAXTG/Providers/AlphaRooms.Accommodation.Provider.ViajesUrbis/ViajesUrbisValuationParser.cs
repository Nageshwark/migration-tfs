﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{

    public class ViajesUrbisValuationParser : IAccommodationValuationParser<ViajesUrbisValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesUrbisValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, ViajesUrbisValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesUrbis Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRQ supplierRequest, cavalHotelBookingValuationRS supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.resultCode == 200);
            
            if (hasResults)
            {
                var availabilityResult = request.SelectedRooms.First().AvailabilityResult;
                int roomNumber = 1;
                
                foreach (var room in supplierResponse.occupations)
                {
                    var result = new AccommodationProviderValuationResult();

                    result.RoomNumber = (byte)roomNumber;
                    result.ProviderEdiCode = availabilityResult.ProviderEdiCode;
                    result.EstablishmentName = supplierResponse.establishmentName;
                    result.EstablishmentEdiCode = supplierResponse.establishmentId;
                    result.DestinationEdiCode = availabilityResult.DestinationEdiCode;

                    result.BoardDescription = availabilityResult.BoardDescription;
                    result.BoardCode = availabilityResult.BoardCode;
                    result.RoomCode = room.roomCode;

                    result.RoomDescription = room.roomName;
                    result.PaymentModel = PaymentModel.PostPayment;
                    result.RateType = RateType.NetStandard;
                    result.IsOpaqueRate = false;
                    result.IsNonRefundable = false;
                    result.NumberOfAvailableRooms = 0;

                    var amount = supplierResponse.netPrice.value / request.SelectedRooms.Length;

                    result.SalePrice = new Money
                    {
                        Amount = (decimal)amount,
                        CurrencyCode = supplierResponse.netPrice.currencyCode
                    };

                    result.CostPrice = result.SalePrice;
                    //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                    result.Adults = (byte)room.adultsPerRoom;
                    result.Children = (byte)room.childrenPerRoom;
                    result.Infants = 0;
                    result.CheckInDate = availabilityResult.CheckInDate;
                    result.CheckOutDate = availabilityResult.CheckOutDate;

                    result.CancellationPolicy = GetCancellationPolicy(supplierResponse.cancellationCosts);

                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("TotalPrice", supplierResponse.netPrice.value.ToString());
                    
                    roomNumber++;

                    availabilityResults.Enqueue(result);
                }
                

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("ViajesUrbis Availability Parser: Availability Response cannot be parsed because it is null.");

                if (supplierResponse != null && !string.IsNullOrEmpty(supplierResponse.message))
                    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));

                throw new SupplierApiDataException(sbMessage.ToString());
            }
           
        }

        private string[] GetCancellationPolicy(cancellationCost[] cancellationCosts)
        {
            return cancellationCosts.Select(cancellationCost => string.Format("From:{0}, Amount:{1}, Currency:{2}", cancellationCost.from, cancellationCost.netPrice.value, cancellationCost.netPrice.currencyCode)).ToArray();
        }
    }
}
