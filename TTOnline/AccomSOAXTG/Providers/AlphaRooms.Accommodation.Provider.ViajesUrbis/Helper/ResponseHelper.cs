﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.NTIncoming.NTIncomingHotelResService;

namespace AlphaRooms.Accommodation.Provider.NTIncoming.Helper
{
    public class ResponseHelper
    {
        public bool HasResResults(OTA_HotelResRS supplierResponse)
        {
            if (supplierResponse != null 
                && supplierResponse.Items != null 
                && supplierResponse.Items.Any() 
                && !GetItemByType(supplierResponse.Items, typeof(ErrorType[])).Any())
            {

                return true;
            }

            return false;
        }

        public IEnumerable<object> GetItemByType(object [] itemsArray, Type type)
        {
            return from item in itemsArray
                   where item.GetType() == type
                   select item;
        }
    }
}
