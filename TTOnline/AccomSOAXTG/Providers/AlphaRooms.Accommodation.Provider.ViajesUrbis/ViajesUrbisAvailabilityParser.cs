﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisAvailabilityParser:IAccommodationAvailabilityParser<ViajesUrbisAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public ViajesUrbisAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            ViajesUrbisAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, availabilityResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("ViajesUrbis Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, cavalHotelAvailabilityRQ supplierRequest, cavalHotelAvailabilityRS supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = ( supplierResponse != null && supplierResponse.resultCode == 200 &&
                                supplierResponse.availableEstablishments !=null);

            if (hasResults)
            {
                foreach (var establishment in supplierResponse.availableEstablishments)
                {
                    foreach (var price in establishment.prices)
                    {
                        int roomNumber = 1;
                        foreach (var room in price.rooms)
                        {
                            foreach (var board in price.boardPrices)
                            {
                                var result = new AccommodationProviderAvailabilityResult();

                                result.RoomNumber = (byte)roomNumber;
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.EstablishmentName = establishment.establishmentName;
                                result.EstablishmentEdiCode = establishment.establishmentId;
                                result.DestinationEdiCode = establishment.cityId;

                                result.BoardDescription = board.boardName;
                                result.BoardCode = board.boardCode;
                                result.RoomCode = room.roomCode;
                                result.RoomDescription = room.roomName;
                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                result.IsOpaqueRate = false;
                                result.IsNonRefundable = false;
                                result.NumberOfAvailableRooms = 0;

                                var amount = board.netPrice.value/request.Rooms.Length;

                                result.SalePrice = new Money
                                {
                                    Amount = (decimal) amount,
                                    CurrencyCode = board.netPrice.currencyCode
                                };

                                result.CostPrice = result.SalePrice;
                                //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                                result.Adults = (byte)room.adultsPerRoom;
                                result.Children = (byte)room.childrenPerRoom;
                                result.Infants = 0;
                                result.CheckInDate = request.CheckInDate;
                                result.CheckOutDate = request.CheckOutDate;

                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("TotalPrice", board.netPrice.value.ToString());

                                availabilityResults.Enqueue(result);
                            }

                            roomNumber++;
                        }
                    }
                    
                }
                

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("ViajesUrbis Availability Parser: Availability Response cannot be parsed because it is null.");

                if (supplierResponse!=null && !string.IsNullOrEmpty(supplierResponse.message))
                    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));
                

                throw new SupplierApiDataException(sbMessage.ToString());
            }
            
        }

        
    }
}
