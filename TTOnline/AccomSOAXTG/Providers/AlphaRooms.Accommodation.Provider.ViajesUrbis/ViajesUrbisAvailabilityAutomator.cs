﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;


namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisAvailabilityAutomator:ViajesUrbisAutomaterBase, IAccommodationAvailabilityAutomatorAsync<ViajesUrbisAvailabilityResponse>
    {
        private const string ViajesUrbisAvailabilityUrl = "ViajesUrbisAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UseProxy = "UseProxy";
        private const string ProxyUrl = "ProxyUrl";
        private const string ProxyPort = "ProxyPort";
        private const string ProxyUsername = "ProxyUsername";
        private const string ProxyPassword = "ProxyPassword";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IViajesUrbisAvailabilityRequestFactory _viajesUrbisSupplierAvailabilityRequestFactory;

        public ViajesUrbisAvailabilityAutomator(IViajesUrbisAvailabilityRequestFactory ViajesUrbisAvailabilityRequestFactory)
        {
            this._viajesUrbisSupplierAvailabilityRequestFactory = ViajesUrbisAvailabilityRequestFactory;
        }

        public async Task<ViajesUrbisAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            ViajesUrbisAvailabilityResponse response = null;

            try
            {
                ValidateRequest(request);

                cavalHotelAvailabilityRQ supplierRequest = this._viajesUrbisSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViajesUrbisAvailabilityAutomator.GetAvailabilityResponseAsync: ViajesUrbis supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<ViajesUrbisAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, cavalHotelAvailabilityRQ  supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            HotelBookingServiceClient client = ConfigureSoapCleint(request.Provider.Parameters);
            
          
            cavalHotelAvailabilityRS response = client.getAvailableHotels(supplierRequest);
            
            
            //supplierOutputLogger.LogAvailabilityResponse(supplierBase.Supplier, request.SearchId, request.DepartureAirportCode, request.DestinationAirportCode, request.DepartureDate, SupplierOutputFormats.Xml, supplierResponse);

            var supplierResponse = new ViajesUrbisAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

       

       

        private void ValidateRequest(AccommodationProviderAvailabilityRequest request)
        {
            //if (request.Rooms.Count() > 3)
            //    throw new ApplicationException("ViajesUrbis supports upto 3 Rooms");

            //foreach (var room in request.Rooms)
            //{
            //    if (room.Guests.AdultsCount > 6)
            //        throw new ApplicationException("ViajesUrbis supports upto 6 Adults");

            //    if (room.Guests.ChildrenCount  > 4)
            //        throw new ApplicationException("ViajesUrbis supports upto 4 Children");

            //    if (room.Guests.InfantsCount > 2)
            //        throw new ApplicationException("ViajesUrbis supports upto 2 Infants");
            //}
        }
    }
}
