﻿using AlphaRooms.Accommodation.Provider.ViajesUrbis;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViajesUrbis;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ViajesUrbisAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<ViajesUrbisAvailabilityResponse> parser;

        public ViajesUrbisAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ViajesUrbisAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ViajesUrbisAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
