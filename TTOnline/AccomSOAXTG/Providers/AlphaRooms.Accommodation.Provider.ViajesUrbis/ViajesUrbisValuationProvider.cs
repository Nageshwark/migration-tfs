﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViajesUrbis;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ViajesUrbisValuationResponse> automator;
        private readonly IAccommodationValuationParser<ViajesUrbisValuationResponse> parser;

        public ViajesUrbisValuationProvider(IAccommodationValuationAutomatorAsync<ViajesUrbisValuationResponse> automator,
                                                IAccommodationValuationParser<ViajesUrbisValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
