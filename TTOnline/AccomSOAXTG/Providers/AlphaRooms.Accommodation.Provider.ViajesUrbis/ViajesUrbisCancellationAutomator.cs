﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.CancellationService;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisCancellationAutomator : IAccommodationCancellationAutomatorAsync<ViajesUrbisCancellationResponse>
    {
        private const string ViajesUrbisCancellationUrl = "ViajesUrbisCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IViajesUrbisProviderCancellationRequestFactory _viajesUrbisProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public ViajesUrbisCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IViajesUrbisProviderCancellationRequestFactory ViajesUrbisSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._viajesUrbisProviderCancellationRequestFactory = ViajesUrbisSupplierCancellationRequestFactory;
            this.outputLogger = outputLogger;
        }
        public async Task<ViajesUrbisCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ViajesUrbisCancellationResponse response = null;

            try
            {
                var supplierRequest = _viajesUrbisProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                var client = ConfigureSoapCleint(request.Provider.Parameters);

                var supplierResponse = client.cancelBooking(supplierRequest);

                response = new ViajesUrbisCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViajesUrbisBookingAutomator.GetCancellationResponseAsync: ViajesUrbis supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        protected CommonsBookingServiceClient ConfigureSoapCleint(List<AccommodationProviderParameter> parameters)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(parameters.GetParameterValue(MaxReceivedMessageSize));

            EndpointAddress address = new EndpointAddress(new Uri(parameters.GetParameterValue(ViajesUrbisCancellationUrl)));

            var client = new CommonsBookingServiceClient(binding, address);

            return client;
        }

       
    }
}
