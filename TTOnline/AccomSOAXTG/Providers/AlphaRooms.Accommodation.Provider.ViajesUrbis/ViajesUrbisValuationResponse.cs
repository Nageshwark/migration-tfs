﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.ViajesUrbisService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisValuationResponse
    {
        public cavalHotelBookingValuationRQ SupplierRequest { get; set; }
        public cavalHotelBookingValuationRS SupplierResponse { get; set; }

    }
}
