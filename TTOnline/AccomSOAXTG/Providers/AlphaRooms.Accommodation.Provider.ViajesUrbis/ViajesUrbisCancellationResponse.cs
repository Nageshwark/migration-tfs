﻿using AlphaRooms.Accommodation.Provider.ViajesUrbis.CancellationService;

namespace AlphaRooms.Accommodation.Provider.ViajesUrbis
{
    public class ViajesUrbisCancellationResponse
    {
        public cavalCancelBookingRQ SupplierRequest { get; set; }
        public cavalCancelBookingRS SupplierResponse { get; set; }
    }
}
