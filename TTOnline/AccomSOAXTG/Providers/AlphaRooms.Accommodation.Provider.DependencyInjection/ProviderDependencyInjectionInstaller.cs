﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Accommodation.Provider.AriaContracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Contracts;
using AlphaRooms.Accommodation.Provider.AriaContracts.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Factories;
using AlphaRooms.Accommodation.Provider.HotelBeds;
using AlphaRooms.Accommodation.Provider.HotelBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U.Factories;
using AlphaRooms.Accommodation.Provider.Hotels4U.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotels4U;
using AlphaRooms.Accommodation.Provider.YouTravel;
using AlphaRooms.Utilities.EntityFramework;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.Avra;
using AlphaRooms.Accommodation.Provider.Avra.Factories;
using AlphaRooms.Accommodation.Provider.Avra.Interfaces;
using AlphaRooms.Accommodation.Provider.Expedia;
using AlphaRooms.Accommodation.Provider.Expedia.Factories;
using AlphaRooms.Accommodation.Provider.Expedia.Helper;
using AlphaRooms.Accommodation.Provider.Expedia.Interfaces;
using AlphaRooms.Accommodation.Provider.GTA;
using AlphaRooms.Accommodation.Provider.GTA.Factories;
using AlphaRooms.Accommodation.Provider.GTA.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelBeds.Factories;
using AlphaRooms.Accommodation.Provider.Hotels4U.Services;
using AlphaRooms.Accommodation.Provider.Hotusa;
using AlphaRooms.Accommodation.Provider.Hotusa.Factories;
using AlphaRooms.Accommodation.Provider.Hotusa.Interfaces;
using AlphaRooms.Accommodation.Provider.Jumbo;
using AlphaRooms.Accommodation.Provider.Jumbo.Factories;
using AlphaRooms.Accommodation.Provider.Jumbo.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Factories;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.MagicRoom;
using AlphaRooms.Accommodation.Provider.MagicRoom.Factories;
using AlphaRooms.Accommodation.Provider.MagicRoom.Interfaces;
using AlphaRooms.Accommodation.Provider.MTS;
using AlphaRooms.Accommodation.Provider.MTS.Factories;
using AlphaRooms.Accommodation.Provider.MTS.Interfaces;
using AlphaRooms.Accommodation.Provider.NTIncoming;
using AlphaRooms.Accommodation.Provider.NTIncoming.Factories;
using AlphaRooms.Accommodation.Provider.NTIncoming.Interfaces;
using AlphaRooms.Accommodation.Provider.Serhs;
using AlphaRooms.Accommodation.Provider.Serhs.Factories;
using AlphaRooms.Accommodation.Provider.Serhs.Interfaces;
using AlphaRooms.Accommodation.Provider.SunHotels;
using AlphaRooms.Accommodation.Provider.SunHotels.Factories;
using AlphaRooms.Accommodation.Provider.SunHotels.Interfaces;
using AlphaRooms.Accommodation.Provider.Tourico;
using AlphaRooms.Accommodation.Provider.Tourico.Factories;
using AlphaRooms.Accommodation.Provider.Tourico.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesUrbis;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Factories;
using AlphaRooms.Accommodation.Provider.ViajesUrbis.Interfaces;
using AlphaRooms.Accommodation.Provider.YouTravel.Factories;
using AlphaRooms.Accommodation.Provider.YouTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Services;
using AlphaRooms.Accommodation.Provider.Bonotel;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.Accommodation.Provider.ECTravel;
using AlphaRooms.Accommodation.Provider.ECTravel.Factories;
using AlphaRooms.Accommodation.Provider.ECTravel.Interfaces;
using AlphaRooms.Accommodation.Provider.Hotetec;
using AlphaRooms.Accommodation.Provider.Hotetec.Factories;
using AlphaRooms.Accommodation.Provider.Hotetec.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel;
using AlphaRooms.Accommodation.Provider.ItalCamel.Factories;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.OswaldArrigo;
using AlphaRooms.Accommodation.Provider.OswaldArrigo.Factories;
using AlphaRooms.Accommodation.Provider.RMI;
using AlphaRooms.Accommodation.Provider.RMI.Factories;
using AlphaRooms.Accommodation.Provider.RMI.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesOlympia;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Factories;
using AlphaRooms.Accommodation.Provider.ViajesOlympia.Interfaces;
using AlphaRooms.Accommodation.Provider.ViajesSideTours;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Factories;
using AlphaRooms.Accommodation.Provider.ViajesSideTours.Interfaces;
using AlphaRooms.Accommodation.Provider.ViatgesMagon;
using AlphaRooms.Accommodation.Provider.ViatgesMagon.Factories;
using AlphaRooms.Accommodation.Provider.Cosmos;
using AlphaRooms.Accommodation.Provider.Cosmos.Factories;
using AlphaRooms.Accommodation.Provider.Cosmos.Interfaces;
using AlphaRooms.Accommodation.Provider.HotelCompany;
using AlphaRooms.Accommodation.Provider.HotelCompany.Factories;
using AlphaRooms.Accommodation.Provider.HotelCompany.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate;
using AlphaRooms.Accommodation.Provider.TravelGate.Interfaces;
using AlphaRooms.Accommodation.Provider.TravelGate.Factories;

namespace AlphaRooms.Accommodation.Provider.DependencyInjection
{
    public class ProviderDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {

            #region Aria Contracts
            // Aria Contracts
            Bind<IDbContextActivator<AriaContractsAccommodationDbContext>>().To<NoProxyDbContextActivator<AriaContractsAccommodationDbContext>>().InSingletonScope();
            Bind<IAriaContractsRepository>().To<AriaContractsRepository>().InSingletonScope();
            Bind<IAccommodationAvailabilityAutomatorAsync<IEnumerable<AriaContractsAvailabilityResponse>>>().To<AriaContractsAvailabilityAutomator>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationAvailabilityParserAsync<IEnumerable<AriaContractsAvailabilityResponse>>>().To<AriaContractsAvailabilityParser>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationAvailabilityProvider>().To<AriaContractsAvailabilityProvider>().Named("Aria Contracts");
            Bind<IAccommodationValuationAutomatorAsync<IEnumerable<AriaContractsValuationResponse>>>().To<AriaContractsValuationAutomator>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationValuationParserAsync<IEnumerable<AriaContractsValuationResponse>>>().To<AriaContractsValuationParser>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationValuationProvider>().To<AriaContractsValuationProvider>().Named("Aria Contracts");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<AriaContractsBookingResponse>>>().To<AriaContractsBookingAutomator>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationBookingParserAsync<IEnumerable<AriaContractsBookingResponse>>>().To<AriaContractsBookingParser>().WhenAnyAncestorNamed("Aria Contracts");
            Bind<IAccommodationBookingProvider>().To<AriaContractsBookingProvider>().Named("Aria Contracts");
            Bind<IAriaContractsCancellationPolicyService>().To<AriaContractsCancellationPolicyService>().InSingletonScope();
            Bind<IAriaContractsEmailService>().To<AriaContractsEmailService>().InSingletonScope();
            Bind<IFaxService>().To<InterFaxService>().InSingletonScope();
            Bind<ISiteMinderService>().To<SiteMinderService>().InSingletonScope();
            #endregion

            #region HotelBeds
            // HotelBeds
            Bind<IAccommodationAvailabilityProvider>().To<HotelBedsAvailabilityProvider>().Named("HotelBeds");
            Bind<IAccommodationAvailabilityAutomatorAsync<IEnumerable<HotelBedsAvailabilityResponse>>>().To<HotelBedsAvailabilityAutomator>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IAccommodationAvailabilityParser<IEnumerable<HotelBedsAvailabilityResponse>>>().To<HotelBedsAvailabilityParser>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IHotelBedsProviderAvailabilityRequestFactory>().To<HotelBedsProviderAvailabilityRequestFactory>().WhenAnyAncestorNamed("HotelBeds");

            Bind<IAccommodationValuationProvider>().To<HotelBedsValuationProvider>().Named("HotelBeds");
            Bind<IAccommodationValuationAutomatorAsync<IEnumerable<HotelBedsValuationResponse>>>().To<HotelBedsValuationAutomator>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IAccommodationValuationParserAsync<IEnumerable<HotelBedsValuationResponse>>>().To<HotelBedsValuationParser>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IHotelBedsProviderValuationRequestFactory>().To<HotelBedsProviderValuationRequestFactory>().WhenAnyAncestorNamed("HotelBeds");

            Bind<IAccommodationBookingProvider>().To<HotelBedsBookingProvider>().Named("HotelBeds");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<HotelBedsBookingResponse>>>().To<HotelBedsBookingAutomator>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IAccommodationBookingParser<IEnumerable<HotelBedsBookingResponse>>>().To<HotelBedsBookingParser>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IHotelBedsProviderBookingRequestFactory>().To<HotelBedsProviderBookingRequestFactory>().WhenAnyAncestorNamed("HotelBeds");

            Bind<IAccommodationCancellationProvider>().To<HotelBedsCancellationProvider>().Named("HotelBeds");
            Bind<IAccommodationCancellationAutomatorAsync<HotelBedsCancellationResponse>>().To<HotelBedsCancellationAutomator>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IAccommodationCancellationParserAsync<HotelBedsCancellationResponse>>().To<HotelBedsCancellationParser>().WhenAnyAncestorNamed("HotelBeds");
            Bind<IHotelBedsProviderCancellationRequestFactory>().To<HotelBedsCancellationRequestFactory>().WhenAnyAncestorNamed("HotelBeds");

            Bind<IHotelBedsCancellationPolicyService>().To<HotelBedsCancellationPolicyService>().InSingletonScope();
            #endregion

            #region Bev5
            // Bev5
            Bind<IAccommodationAvailabilityProvider>().To<Bev5AvailabilityProvider>().Named("Bev5");
            Bind<IAccommodationAvailabilityAutomatorAsync<IEnumerable<AvailabilityRoomsResponse>>>().To<Bev5AvailabilityAutomator>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationAvailabilityParser<IEnumerable<AvailabilityRoomsResponse>>>().To<Bev5AvailabilityParser>().WhenAnyAncestorNamed("Bev5");
            Bind<IBev5RequestFactory>().To<Bev5RequestFactory>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationValuationProvider>().To<Bev5ValuationProvider>().Named("Bev5");
            Bind<IAccommodationValuationAutomatorAsync<ValuationRoomResponse>>().To<Bev5ValuationAutomator>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationValuationParser<ValuationRoomResponse>>().To<Bev5ValuationParser>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationBookingProvider>().To<Bev5BookingProvider>().Named("Bev5");
            Bind<IAccommodationBookingAutomatorAsync<BookRoomsResponse>>().To<Bev5BookingAutomator>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationBookingParser<BookRoomsResponse>>().To<Bev5BookingParser>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationCancellationProvider>().To<Bev5CancellationProvider>().Named("Bev5");
            Bind<IAccommodationCancellationAutomatorAsync<CancelAccommodationResponse>>().To<Bev5CancellationAutomator>().WhenAnyAncestorNamed("Bev5");
            Bind<IAccommodationCancellationParser<CancelAccommodationResponse>>().To<Bev5CancellationParser>().WhenAnyAncestorNamed("Bev5");
            Bind<IBev5BindingRateService>().To<Bev5BindingRateService>().InSingletonScope();
            Bind<IBev5RateTypeService>().To<Bev5RateTypeService>().InSingletonScope();
            Bind<IBev5SalePriceService>().To<Bev5SalePriceService>().InSingletonScope();
            Bind<IBev5EndpointService>().To<Bev5EndpointService>().InSingletonScope();
            #endregion

            #region  Hotels4u
            //Helper
            Bind<IHotels4UHelperService>().To<Hotels4UHelperService>().WhenAnyAncestorNamed("Hotels4U").InSingletonScope();

            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<Hotels4UAvailabilityProvider>().Named("Hotels4U");
            Bind<IAccommodationAvailabilityAutomatorAsync<Hotels4UAvailabilityResponse>>().To<Hotels4UAvailabilityAutomator>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IAccommodationAvailabilityParser<Hotels4UAvailabilityResponse>>().To<Hotels4UAvailabilityParser>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IHotels4UAvailabilityRequestFactory>().To<Hotels4UAvailabilityRequestFactory>().WhenAnyAncestorNamed("Hotels4U");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<Hotels4UValuationProvider>().Named("Hotels4U");
            Bind<IAccommodationValuationAutomatorAsync<Hotels4UValuationResponse>>().To<Hotels4UValuationAutomator>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IAccommodationValuationParser<Hotels4UValuationResponse>>().To<Hotels4UValuationParser>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IHotels4UProviderValuationRequestFactory>().To<Hotels4UValuationRequestFactory>().WhenAnyAncestorNamed("Hotels4U");

            //Booking
            Bind<IAccommodationBookingProvider>().To<Hotels4UBookingProvider>().Named("Hotels4U");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<Hotels4UBookingResponse>>>().To<Hotels4UBookingAutomator>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IAccommodationBookingParser<IEnumerable<Hotels4UBookingResponse>>>().To<Hotels4UBookingParser>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IHotels4UProviderBookingRequestFactory>().To<Hotels4UProviderBookingRequestFactory>().WhenAnyAncestorNamed("Hotels4U");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<Hotels4UCancellationProvider>().Named("Hotels4U");
            Bind<IAccommodationCancellationAutomatorAsync<Hotels4UCancellationResponse>>().To<Hotels4UCancellationAutomator>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IAccommodationCancellationParserAsync<Hotels4UCancellationResponse>>().To<Hotels4UCancellationParser>().WhenAnyAncestorNamed("Hotels4U");
            Bind<IHotels4UProviderCancellationRequestFactory>().To<Hotels4UCancellationRequestFactory>().WhenAnyAncestorNamed("Hotels4U");
            #endregion

            #region YouTravel
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<YouTravelAvailabilityProvider>().Named("YouTravel");
            Bind<IAccommodationAvailabilityAutomatorAsync<YouTravelAvailabilityResponse>>().To<YouTravelAvailabilityAutomator>().WhenAnyAncestorNamed("YouTravel");
            Bind<IAccommodationAvailabilityParser<YouTravelAvailabilityResponse>>().To<YouTravelAvailabilityParser>().WhenAnyAncestorNamed("YouTravel");
            Bind<IYouTravelAvailabilityRequestFactory>().To<YouTravelAvailabilityRequestFactory>().WhenAnyAncestorNamed("YouTravel");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<YouTravelValuationProvider>().Named("YouTravel");
            Bind<IAccommodationValuationAutomatorAsync<YouTravelAvailabilityResponse>>().To<YouTravelValuationAutomator>().WhenAnyAncestorNamed("YouTravel");
            Bind<IAccommodationValuationParser<YouTravelAvailabilityResponse>>().To<YouTravelValuationParser>().WhenAnyAncestorNamed("YouTravel");
            Bind<IYouTravelProviderValuationRequestFactory>().To<YouTravelValuationRequestFactory>().WhenAnyAncestorNamed("YouTravel");

            //Booking
            Bind<IAccommodationBookingProvider>().To<YouTravelBookingProvider>().Named("YouTravel");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<YouTravelBookingResponse>>>().To<YouTravelBookingAutomator>().WhenAnyAncestorNamed("YouTravel");
            Bind<IAccommodationBookingParser<IEnumerable<YouTravelBookingResponse>>>().To<YouTravelBookingParser>().WhenAnyAncestorNamed("YouTravel");
            Bind<IYouTravelProviderBookingRequestFactory>().To<YouTravelProviderBookingRequestFactory>().WhenAnyAncestorNamed("YouTravel");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<YouTravelCancellationProvider>().Named("YouTravel");
            Bind<IAccommodationCancellationAutomatorAsync<YouTravelCancellationResponse>>().To<YouTravelCancellationAutomator>().WhenAnyAncestorNamed("YouTravel");
            Bind<IAccommodationCancellationParserAsync<YouTravelCancellationResponse>>().To<YouTravelCancellationParser>().WhenAnyAncestorNamed("YouTravel");
            Bind<IYouTravelProviderCancellationRequestFactory>().To<YouTravelCancellationRequestFactory>().WhenAnyAncestorNamed("YouTravel");
            #endregion

            #region MagicRoom
            //Avail 
            Bind<IAccommodationAvailabilityProvider>().To<MagicRoomAvailabilityProvider>().Named("MagicRoom");
            Bind<IAccommodationAvailabilityAutomatorAsync<MagicRoomAvailabilityResponse>>().To<MagicRoomAvailabilityAutomator>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IAccommodationAvailabilityParser<MagicRoomAvailabilityResponse>>().To<MagicRoomAvailabilityParser>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IMagicRoomAvailabilityRequestFactory>().To<MagicRoomAvailabilityRequestFactory>().WhenAnyAncestorNamed("MagicRoom");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<MagicRoomValuationProvider>().Named("MagicRoom");
            Bind<IAccommodationValuationAutomatorAsync<MagicRoomValuationResponse>>().To<MagicRoomValuationAutomator>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IAccommodationValuationParser<MagicRoomValuationResponse>>().To<MagicRoomValuationParser>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IMagicRoomProviderValuationRequestFactory>().To<MagicRoomValuationRequestFactory>().WhenAnyAncestorNamed("MagicRoom");

            //Booking
            Bind<IAccommodationBookingProvider>().To<MagicRoomBookingProvider>().Named("MagicRoom");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<MagicRoomBookingResponse>>>().To<MagicRoomBookingAutomator>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IAccommodationBookingParser<IEnumerable<MagicRoomBookingResponse>>>().To<MagicRoomBookingParser>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IMagicRoomProviderBookingRequestFactory>().To<MagicRoomProviderBookingRequestFactory>().WhenAnyAncestorNamed("MagicRoom");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<MagicRoomCancellationProvider>().Named("MagicRoom");
            Bind<IAccommodationCancellationAutomatorAsync<MagicRoomCancellationResponse>>().To<MagicRoomCancellationAutomator>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IAccommodationCancellationParserAsync<MagicRoomCancellationResponse>>().To<MagicRoomCancellationParser>().WhenAnyAncestorNamed("MagicRoom");
            Bind<IMagicRoomProviderCancellationRequestFactory>().To<MagicRoomCancellationRequestFactory>().WhenAnyAncestorNamed("MagicRoom");

            #endregion

            #region Jumbo
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<JumboAvailabilityProvider>().Named("Jumbo");
            Bind<IAccommodationAvailabilityAutomatorAsync<JumboAvailabilityResponse>>().To<JumboAvailabilityAutomator>().WhenAnyAncestorNamed("Jumbo");
            Bind<IAccommodationAvailabilityParser<JumboAvailabilityResponse>>().To<JumboAvailabilityParser>().WhenAnyAncestorNamed("Jumbo");
            Bind<IJumboAvailabilityRequestFactory>().To<JumboAvailabilityRequestFactory>().WhenAnyAncestorNamed("Jumbo");

            //Valuation
            Bind<IAccommodationBookingProvider>().To<JumboBookingProvider>().Named("Jumbo");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<JumboBookingResponse>>>().To<JumboBookingAutomator>().WhenAnyAncestorNamed("Jumbo");
            Bind<IAccommodationBookingParser<IEnumerable<JumboBookingResponse>>>().To<JumboBookingParser>().WhenAnyAncestorNamed("Jumbo");
            Bind<IJumboProviderBookingRequestFactory>().To<JumboProviderBookingRequestFactory>().WhenAnyAncestorNamed("Jumbo");

            //Booking
            Bind<IAccommodationValuationProvider>().To<JumboValuationProvider>().Named("Jumbo");
            Bind<IAccommodationValuationAutomatorAsync<JumboValuationResponse>>().To<JumboValuationAutomator>().WhenAnyAncestorNamed("Jumbo");
            Bind<IAccommodationValuationParser<JumboValuationResponse>>().To<JumboValuationParser>().WhenAnyAncestorNamed("Jumbo");
            Bind<IJumboProviderValuationRequestFactory>().To<JumboValuationRequestFactory>().WhenAnyAncestorNamed("Jumbo");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<JumboCancellationProvider>().Named("Jumbo");
            Bind<IAccommodationCancellationAutomatorAsync<JumboCancellationResponse>>().To<JumboCancellationAutomator>().WhenAnyAncestorNamed("Jumbo");
            Bind<IAccommodationCancellationParserAsync<JumboCancellationResponse>>().To<JumboCancellationParser>().WhenAnyAncestorNamed("Jumbo");
            Bind<IJumboProviderCancellationRequestFactory>().To<JumboCancellationRequestFactory>().WhenAnyAncestorNamed("Jumbo");

            #endregion

            #region Cosmos
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<CosmosAvailabilityProvider>().Named("Cosmos");
            Bind<IAccommodationAvailabilityAutomatorAsync<CosmosAvailabilityResponse>>().To<CosmosAvailabilityAutomator>().WhenAnyAncestorNamed("Cosmos");
            Bind<IAccommodationAvailabilityParser<CosmosAvailabilityResponse>>().To<CosmosAvailabilityParser>().WhenAnyAncestorNamed("Cosmos");
            Bind<ICosmosAvailabilityRequestFactory>().To<CosmosAvailabilityRequestFactory>().WhenAnyAncestorNamed("Cosmos");

            //Valuation
            Bind<IAccommodationBookingProvider>().To<CosmosBookingProvider>().Named("Cosmos");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<CosmosBookingResponse>>>().To<CosmosBookingAutomator>().WhenAnyAncestorNamed("Cosmos");
            Bind<IAccommodationBookingParser<IEnumerable<CosmosBookingResponse>>>().To<CosmosBookingParser>().WhenAnyAncestorNamed("Cosmos");
            Bind<ICosmosBookingRequestFactory>().To<CosmosBookingRequestFactory>().WhenAnyAncestorNamed("Cosmos");

            //Booking
            Bind<IAccommodationValuationProvider>().To<CosmosValuationProvider>().Named("Cosmos");
            Bind<IAccommodationValuationAutomatorAsync<CosmosValuationResponse>>().To<CosmosValuationAutomator>().WhenAnyAncestorNamed("Cosmos");
            Bind<IAccommodationValuationParser<CosmosValuationResponse>>().To<CosmosValuationParser>().WhenAnyAncestorNamed("Cosmos");
            Bind<ICosmosValuationRequestFactory>().To<CosmosValuationRequestFactory>().WhenAnyAncestorNamed("Cosmos");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<CosmosCancellationProvider>().Named("Cosmos");
            Bind<IAccommodationCancellationAutomatorAsync<CosmosCancellationResponse>>().To<CosmosCancellationAutomator>().WhenAnyAncestorNamed("Cosmos");
            Bind<IAccommodationCancellationParserAsync<CosmosCancellationResponse>>().To<CosmosCancellationParser>().WhenAnyAncestorNamed("Cosmos");
            Bind<ICosmosCancellationRequestFactory>().To<CosmosCancellationRequestFactory>().WhenAnyAncestorNamed("Cosmos");

            #endregion

            #region LowCostBeds
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<LowCostBedsAvailabilityProvider>().Named("Lowcost Beds");
            Bind<IAccommodationAvailabilityAutomatorAsync<LowCostBedsAvailabilityResponse>>().To<LowCostBedsAvailabilityAutomator>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<IAccommodationAvailabilityParser<LowCostBedsAvailabilityResponse>>().To<LowCostBedsAvailabilityParser>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<ILowCostBedsProviderAvailabilityRequestFactory>().To<LowCostBedsProviderAvailabilityRequestFactory>().WhenAnyAncestorNamed("Lowcost Beds");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<LowCostBedsValuationProvider>().Named("Lowcost Beds");
            Bind<IAccommodationValuationAutomatorAsync<LowCostBedsValuationResponse>>().To<LowCostBedsValuationAutomator>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<IAccommodationValuationParser<LowCostBedsValuationResponse>>().To<LowCostBedsValuationParser>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<ILowCostBedsProviderValuationRequestFactory>().To<LowCostBedsProviderValuationRequestFactory>().WhenAnyAncestorNamed("Lowcost Beds");

            //Booking
            Bind<IAccommodationBookingProvider>().To<LowCostBedsBookingProvider>().Named("Lowcost Beds");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<LowCostBedsBookingResponse>>>().To<LowCostBedsBookingAutomator>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<IAccommodationBookingParser<IEnumerable<LowCostBedsBookingResponse>>>().To<LowCostBedsBookingParser>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<ILowCostBedsProviderBookingRequestFactory>().To<LowCostBedsProviderBookingRequestFactory>().WhenAnyAncestorNamed("Lowcost Beds");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<LowCostBedsCancellationProvider>().Named("Lowcost Beds");
            Bind<IAccommodationCancellationAutomatorAsync<LowCostBedsCancellationResponse>>().To<LowCostBedsCancellationAutomator>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<IAccommodationCancellationParserAsync<LowCostBedsCancellationResponse>>().To<LowCostBedsCancellationParser>().WhenAnyAncestorNamed("Lowcost Beds");
            Bind<ILowCostBedsProviderCancellationRequestFactory>().To<LowCostBedsCancellationRequestFactory>().WhenAnyAncestorNamed("Lowcost Beds");

            //Helper
            Bind<ILowCostBedsParameterService>().To<LowCostBedsParameterService>().WhenAnyAncestorNamed("Lowcost Beds").InSingletonScope();
            #endregion

            #region Expedia
            //Parameters
            Bind<IExpediaParameterService>().To<ExpediaParameterService>().WhenAnyAncestorNamed("Expedia");
            
            //Helpers
            Bind<IExpediaApiKeyHelper>().To<APIKeyHelper>().WhenAnyAncestorNamed("Expedia").InSingletonScope();

            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<ExpediaAvailabilityProvider>().Named("Expedia");
            Bind<IAccommodationAvailabilityAutomatorAsync<IEnumerable<ExpediaAvailabilityResponse>>>().To<ExpediaAvailabilityAutomator>().WhenAnyAncestorNamed("Expedia");
            Bind<IAccommodationAvailabilityParser<IEnumerable<ExpediaAvailabilityResponse>>>().To<ExpediaAvailabilityParser>().WhenAnyAncestorNamed("Expedia");
            Bind<IExpediaProviderAvailabilityRequestFactory>().To<ExpediaProviderAvailabilityRequestFactory>().WhenAnyAncestorNamed("Expedia");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ExpediaValuationProvider>().Named("Expedia");
            Bind<IAccommodationValuationAutomatorAsync<ExpediaValuationResponse>>().To<ExpediaValuationAutomator>().WhenAnyAncestorNamed("Expedia");
            Bind<IAccommodationValuationParser<ExpediaValuationResponse>>().To<ExpediaValuationParser>().WhenAnyAncestorNamed("Expedia");
            Bind<IExpediaProviderValuationRequestFactory>().To<ExpediaProviderValuationRequestFactory>().WhenAnyAncestorNamed("Expedia");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ExpediaBookingProvider>().Named("Expedia");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<ExpediaBookingResponse>>>().To<ExpediaBookingAutomator>().WhenAnyAncestorNamed("Expedia");
            Bind<IAccommodationBookingParser<IEnumerable<ExpediaBookingResponse>>>().To<ExpediaBookingParser>().WhenAnyAncestorNamed("Expedia");
            Bind<IExpediaProviderBookingRequestFactory>().To<ExpediaProviderBookingRequestFactory>().WhenAnyAncestorNamed("Expedia");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ExpediaCancellationProvider>().Named("Expedia");
            Bind<IAccommodationCancellationAutomatorAsync<ExpediaCancellationResponse>>().To<ExpediaCancellationAutomator>().WhenAnyAncestorNamed("Expedia");
            Bind<IAccommodationCancellationParserAsync<ExpediaCancellationResponse>>().To<ExpediaCancellationParser>().WhenAnyAncestorNamed("Expedia");
            Bind<IExpediaProviderCancellationRequestFactory>().To<ExpediaCancellationRequestFactory>().WhenAnyAncestorNamed("Expedia");

            #endregion

            #region NTIncoming
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<NTIncomingAvailabilityProvider>().Named("NTIncoming");
            Bind<IAccommodationAvailabilityAutomatorAsync<NTIncomingAvailabilityResponse>>().To<NTIncomingAvailabilityAutomator>().WhenAnyAncestorNamed("NTIncoming");
            Bind<IAccommodationAvailabilityParser<NTIncomingAvailabilityResponse>>().To<NTIncomingAvailabilityParser>().WhenAnyAncestorNamed("NTIncoming");
            Bind<INTIncomingAvailabilityRequestFactory>().To<NTIncomingAvailabilityRequestFactory>().WhenAnyAncestorNamed("NTIncoming");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<NTIncomingValuationProvider>().Named("NTIncoming");
            Bind<IAccommodationValuationAutomatorAsync<NTIncomingValuationResponse>>().To<NTIncomingValuationAutomator>().WhenAnyAncestorNamed("NTIncoming");
            Bind<IAccommodationValuationParser<NTIncomingValuationResponse>>().To<NTIncomingValuationParser>().WhenAnyAncestorNamed("NTIncoming");
            Bind<INTIncomingProviderValuationRequestFactory>().To<NTIncomingValuationRequestFactory>().WhenAnyAncestorNamed("NTIncoming");

            //Booking
            Bind<IAccommodationBookingProvider>().To<NTIncomingBookingProvider>().Named("NTIncoming");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<NTIncomingBookingResponse>>>().To<NTIncomingBookingAutomator>().WhenAnyAncestorNamed("NTIncoming");
            Bind<IAccommodationBookingParser<IEnumerable<NTIncomingBookingResponse>>>().To<NTIncomingBookingParser>().WhenAnyAncestorNamed("NTIncoming");
            Bind<INTIncomingProviderBookingRequestFactory>().To<NTIncomingProviderBookingRequestFactory>().WhenAnyAncestorNamed("NTIncoming");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<NTIncomingCancellationProvider>().Named("NTIncoming");
            Bind<IAccommodationCancellationAutomatorAsync<NTIncomingCancellationResponse>>().To<NTIncomingCancellationAutomator>().WhenAnyAncestorNamed("NTIncoming");
            Bind<IAccommodationCancellationParserAsync<NTIncomingCancellationResponse>>().To<NTIncomingCancellationParser>().WhenAnyAncestorNamed("NTIncoming");
            Bind<INTIncomingProviderCancellationRequestFactory>().To<NTIncomingCancellationRequestFactory>().WhenAnyAncestorNamed("NTIncoming");
            #endregion

            #region MTS - Open Travel Services
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<MTSAvailabilityProvider>().Named("Open Travel Services");
            Bind<IAccommodationAvailabilityAutomatorAsync<MTSAvailabilityResponse>>().To<MTSAvailabilityAutomator>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IAccommodationAvailabilityParser<MTSAvailabilityResponse>>().To<MTSAvailabilityParser>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IMTSAvailabilityRequestFactory>().To<MTSAvailabilityRequestFactory>().WhenAnyAncestorNamed("Open Travel Services");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<MTSValuationProvider>().Named("Open Travel Services");
            Bind<IAccommodationValuationAutomatorAsync<MTSValuationResponse>>().To<MTSValuationAutomator>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IAccommodationValuationParser<MTSValuationResponse>>().To<MTSValuationParser>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IMTSProviderValuationRequestFactory>().To<MTSProviderValuationRequestFactory>().WhenAnyAncestorNamed("Open Travel Services");

            //Booking
            Bind<IAccommodationBookingProvider>().To<MTSBookingProvider>().Named("Open Travel Services");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<MTSBookingResponse>>>().To<MTSBookingAutomator>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IAccommodationBookingParser<IEnumerable<MTSBookingResponse>>>().To<MTSBookingParser>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IMTSProviderBookingRequestFactory>().To<MTSProviderBookingRequestFactory>().WhenAnyAncestorNamed("Open Travel Services");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<MTSCancellationProvider>().Named("Open Travel Services");
            Bind<IAccommodationCancellationAutomatorAsync<MTSCancellationResponse>>().To<MTSCancellationAutomator>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IAccommodationCancellationParserAsync<MTSCancellationResponse>>().To<MTSCancellationParser>().WhenAnyAncestorNamed("Open Travel Services");
            Bind<IMTSProviderCancellationRequestFactory>().To<MTSCancellationRequestFactory>().WhenAnyAncestorNamed("Open Travel Services");
            #endregion

            #region GTA
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<GTAAvailabilityProvider>().Named("GTA");
            Bind<IAccommodationAvailabilityAutomatorAsync<GTAAvailabilityResponse>>().To<GTAAvailabilityAutomator>().WhenAnyAncestorNamed("GTA");
            Bind<IAccommodationAvailabilityParser<GTAAvailabilityResponse>>().To<GTAAvailabilityParser>().WhenAnyAncestorNamed("GTA");
            Bind<IGTAAvailabilityRequestFactory>().To<GTAAvailabilityRequestFactory>().WhenAnyAncestorNamed("GTA");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<GTAValuationProvider>().Named("GTA");
            Bind<IAccommodationValuationAutomatorAsync<GTAValuationResponse>>().To<GTAValuationAutomator>().WhenAnyAncestorNamed("GTA");
            Bind<IAccommodationValuationParser<GTAValuationResponse>>().To<GTAValuationParser>().WhenAnyAncestorNamed("GTA");
            Bind<IGTAProviderValuationRequestFactory>().To<GTAProviderValuationRequestFactory>().WhenAnyAncestorNamed("GTA");

            //Booking
            Bind<IAccommodationBookingProvider>().To<GTABookingProvider>().Named("GTA");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<GTABookingResponse>>>().To<GTABookingAutomator>().WhenAnyAncestorNamed("GTA");
            Bind<IAccommodationBookingParser<IEnumerable<GTABookingResponse>>>().To<GTABookingParser>().WhenAnyAncestorNamed("GTA");
            Bind<IGTAProviderBookingRequestFactory>().To<GTAProviderBookingRequestFactory>().WhenAnyAncestorNamed("GTA");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<GTACancellationProvider>().Named("GTA");
            Bind<IAccommodationCancellationAutomatorAsync<GTACancellationResponse>>().To<GTACancellationAutomator>().WhenAnyAncestorNamed("GTA");
            Bind<IAccommodationCancellationParserAsync<GTACancellationResponse>>().To<GTACancellationParser>().WhenAnyAncestorNamed("GTA");
            Bind<IGTAProviderCancellationRequestFactory>().To<GTACancellationRequestFactory>().WhenAnyAncestorNamed("GTA");

            #endregion

            #region Serhs
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<SerhsAvailabilityProvider>().Named("Serhs");
            Bind<IAccommodationAvailabilityAutomatorAsync<SerhsAvailabilityResponse>>().To<SerhsAvailabilityAutomator>().WhenAnyAncestorNamed("Serhs");
            Bind<IAccommodationAvailabilityParser<SerhsAvailabilityResponse>>().To<SerhsAvailabilityParser>().WhenAnyAncestorNamed("Serhs");
            Bind<ISerhsAvailabilityRequestFactory>().To<SerhsAvailabilityRequestFactory>().WhenAnyAncestorNamed("Serhs");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<SerhsValuationProvider>().Named("Serhs");
            Bind<IAccommodationValuationAutomatorAsync<SerhsValuationResponse>>().To<SerhsValuationAutomator>().WhenAnyAncestorNamed("Serhs");
            Bind<IAccommodationValuationParser<SerhsValuationResponse>>().To<SerhsValuationParser>().WhenAnyAncestorNamed("Serhs");
            Bind<ISerhsProviderValuationRequestFactory>().To<SerhsValuationRequestFactory>().WhenAnyAncestorNamed("Serhs");

            //Booking
            Bind<IAccommodationBookingProvider>().To<SerhsBookingProvider>().Named("Serhs");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<SerhsBookingResponse>>>().To<SerhsBookingAutomator>().WhenAnyAncestorNamed("Serhs");
            Bind<IAccommodationBookingParser<IEnumerable<SerhsBookingResponse>>>().To<SerhsBookingParser>().WhenAnyAncestorNamed("Serhs");
            Bind<ISerhsProviderBookingRequestFactory>().To<SerhsProviderBookingRequestFactory>().WhenAnyAncestorNamed("Serhs");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<SerhsCancellationProvider>().Named("Serhs");
            Bind<IAccommodationCancellationAutomatorAsync<SerhsCancellationResponse>>().To<SerhsCancellationAutomator>().WhenAnyAncestorNamed("Serhs");
            Bind<IAccommodationCancellationParserAsync<SerhsCancellationResponse>>().To<SerhsCancellationParser>().WhenAnyAncestorNamed("Serhs");
            Bind<ISerhsProviderCancellationRequestFactory>().To<SerhsCancellationRequestFactory>().WhenAnyAncestorNamed("Serhs");
            #endregion

            #region Viajes Urbis
            //Avail 
            Bind<IAccommodationAvailabilityProvider>().To<ViajesUrbisAvailabilityProvider>().Named("Viajes Urbis");
            Bind<IAccommodationAvailabilityAutomatorAsync<ViajesUrbisAvailabilityResponse>>().To<ViajesUrbisAvailabilityAutomator>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IAccommodationAvailabilityParser<ViajesUrbisAvailabilityResponse>>().To<ViajesUrbisAvailabilityParser>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IViajesUrbisAvailabilityRequestFactory>().To<ViajesUrbisAvailabilityRequestFactory>().WhenAnyAncestorNamed("Viajes Urbis");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ViajesUrbisValuationProvider>().Named("Viajes Urbis");
            Bind<IAccommodationValuationAutomatorAsync<ViajesUrbisValuationResponse>>().To<ViajesUrbisValuationAutomator>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IAccommodationValuationParser<ViajesUrbisValuationResponse>>().To<ViajesUrbisValuationParser>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IViajesUrbisProviderValuationRequestFactory>().To<ViajesUrbisValuationRequestFactory>().WhenAnyAncestorNamed("Viajes Urbis");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ViajesUrbisBookingProvider>().Named("Viajes Urbis");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<ViajesUrbisBookingResponse>>>().To<ViajesUrbisBookingAutomator>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IAccommodationBookingParser<IEnumerable<ViajesUrbisBookingResponse>>>().To<ViajesUrbisBookingParser>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IViajesUrbisProviderBookingRequestFactory>().To<ViajesUrbisProviderBookingRequestFactory>().WhenAnyAncestorNamed("Viajes Urbis");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ViajesUrbisCancellationProvider>().Named("Viajes Urbis");
            Bind<IAccommodationCancellationAutomatorAsync<ViajesUrbisCancellationResponse>>().To<ViajesUrbisCancellationAutomator>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IAccommodationCancellationParserAsync<ViajesUrbisCancellationResponse>>().To<ViajesUrbisCancellationParser>().WhenAnyAncestorNamed("Viajes Urbis");
            Bind<IViajesUrbisProviderCancellationRequestFactory>().To<ViajesUrbisCancellationRequestFactory>().WhenAnyAncestorNamed("Viajes Urbis");
            #endregion

            #region Tourico
            //Avail 
            Bind<IAccommodationAvailabilityProvider>().To<TouricoAvailabilityProvider>().Named("Tourico");
            Bind<IAccommodationAvailabilityAutomatorAsync<TouricoAvailabilityResponse>>().To<TouricoAvailabilityAutomator>().WhenAnyAncestorNamed("Tourico");
            Bind<IAccommodationAvailabilityParser<TouricoAvailabilityResponse>>().To<TouricoAvailabilityParser>().WhenAnyAncestorNamed("Tourico");
            Bind<ITouricoProviderAvailabilityRequestFactory>().To<TouricoProviderAvailabilityRequestFactory>().WhenAnyAncestorNamed("Tourico");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<TouricoValuationProvider>().Named("Tourico");
            Bind<IAccommodationValuationAutomatorAsync<TouricoValuationResponse>>().To<TouricoValuationAutomator>().WhenAnyAncestorNamed("Tourico");
            Bind<IAccommodationValuationParser<TouricoValuationResponse>>().To<TouricoValuationParser>().WhenAnyAncestorNamed("Tourico");
            Bind<ITouricoProviderValuationRequestFactory>().To<TouricoProviderValuationRequestFactory>().WhenAnyAncestorNamed("Tourico");

            //Booking
            Bind<IAccommodationBookingProvider>().To<TouricoBookingProvider>().Named("Tourico");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<TouricoBookingResponse>>>().To<TouricoBookingAutomator>().WhenAnyAncestorNamed("Tourico");
            Bind<IAccommodationBookingParser<IEnumerable<TouricoBookingResponse>>>().To<TouricoBookingParser>().WhenAnyAncestorNamed("Tourico");
            Bind<ITouricoProviderBookingRequestFactory>().To<TouricoProviderBookingRequestFactory>().WhenAnyAncestorNamed("Tourico");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<TouricoCancellationProvider>().Named("Tourico");
            Bind<IAccommodationCancellationAutomatorAsync<TouricoCancellationResponse>>().To<TouricoCancellationAutomator>().WhenAnyAncestorNamed("Tourico");
            Bind<IAccommodationCancellationParserAsync<TouricoCancellationResponse>>().To<TouricoCancellationParser>().WhenAnyAncestorNamed("Tourico");
            Bind<ITouricoProviderCancellationRequestFactory>().To<TouricoProviderCancellationlRequestFactory>().WhenAnyAncestorNamed("Tourico");
            #endregion

            #region ECTravel
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<ECTravelAvailabilityProvider>().Named("EC Travel");
            Bind<IAccommodationAvailabilityAutomatorAsync<ECTravelAvailabilityResponse>>().To<ECTravelAvailabilityAutomator>().WhenAnyAncestorNamed("EC Travel");
            Bind<IAccommodationAvailabilityParser<ECTravelAvailabilityResponse>>().To<ECTravelAvailabilityParser>().WhenAnyAncestorNamed("EC Travel");
            Bind<IECTravelAvailabilityRequestFactory>().To<ECTravelAvailabilityRequestFactory>().WhenAnyAncestorNamed("EC Travel");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ECTravelValuationProvider>().Named("EC Travel");
            Bind<IAccommodationValuationAutomatorAsync<ECTravelValuationResponse>>().To<ECTravelValuationAutomator>().WhenAnyAncestorNamed("EC Travel");
            Bind<IAccommodationValuationParser<ECTravelValuationResponse>>().To<ECTravelValuationParser>().WhenAnyAncestorNamed("EC Travel");
            Bind<IECTravelProviderValuationRequestFactory>().To<ECTravelValuationRequestFactory>().WhenAnyAncestorNamed("EC Travel");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ECTravelBookingProvider>().Named("EC Travel");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<ECTravelBookingResponse>>>().To<ECTravelBookingAutomator>().WhenAnyAncestorNamed("EC Travel");
            Bind<IAccommodationBookingParser<IEnumerable<ECTravelBookingResponse>>>().To<ECTravelBookingParser>().WhenAnyAncestorNamed("EC Travel");
            Bind<IECTravelProviderBookingRequestFactory>().To<ECTravelProviderBookingRequestFactory>().WhenAnyAncestorNamed("EC Travel");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ECTravelCancellationProvider>().Named("EC Travel");
            Bind<IAccommodationCancellationAutomatorAsync<ECTravelCancellationResponse>>().To<ECTravelCancellationAutomator>().WhenAnyAncestorNamed("EC Travel");
            Bind<IAccommodationCancellationParserAsync<ECTravelCancellationResponse>>().To<ECTravelCancellationParser>().WhenAnyAncestorNamed("EC Travel");
            Bind<IECTravelProviderCancellationRequestFactory>().To<ECTravelCancellationRequestFactory>().WhenAnyAncestorNamed("EC Travel");
            #endregion

            #region SunHotels
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<SunHotelsAvailabilityProvider>().Named("SunHotels");
            Bind<IAccommodationAvailabilityAutomatorAsync<SunHotelsAvailabilityResponse>>().To<SunHotelsAvailabilityAutomator>().WhenAnyAncestorNamed("SunHotels");
            Bind<IAccommodationAvailabilityParser<SunHotelsAvailabilityResponse>>().To<SunHotelsAvailabilityParser>().WhenAnyAncestorNamed("SunHotels");
            Bind<ISunHotelsAvailabilityRequestFactory>().To<SunHotelsAvailabilityRequestFactory>().WhenAnyAncestorNamed("SunHotels");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<SunHotelsValuationProvider>().Named("SunHotels");
            Bind<IAccommodationValuationAutomatorAsync<SunHotelsValuationResponse>>().To<SunHotelsValuationAutomator>().WhenAnyAncestorNamed("SunHotels");
            Bind<IAccommodationValuationParser<SunHotelsValuationResponse>>().To<SunHotelsValuationParser>().WhenAnyAncestorNamed("SunHotels");
            Bind<ISunHotelsProviderValuationRequestFactory>().To<SunHotelsProviderValuationRequestFactory>().WhenAnyAncestorNamed("SunHotels");

            //Booking
            Bind<IAccommodationBookingProvider>().To<SunHotelsBookingProvider>().Named("SunHotels");
            Bind<IAccommodationBookingAutomatorAsync<SunHotelsBookingResponse>>().To<SunHotelsBookingAutomator>().WhenAnyAncestorNamed("SunHotels");
            Bind<IAccommodationBookingParser<SunHotelsBookingResponse>>().To<SunHotelsBookingParser>().WhenAnyAncestorNamed("SunHotels");
            Bind<ISunHotelsBookingRequestFactory>().To<SunHotelsBookingRequestFactory>().WhenAnyAncestorNamed("SunHotels");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<SunHotelsCancellationProvider>().Named("SunHotels");
            Bind<IAccommodationCancellationAutomatorAsync<SunHotelsCancellationResponse>>().To<SunHotelCancellationAutomator>().WhenAnyAncestorNamed("SunHotels");
            Bind<IAccommodationCancellationParserAsync<SunHotelsCancellationResponse>>().To<SunHotelsCancellationlationParser>().WhenAnyAncestorNamed("SunHotels");
            Bind<ISunHotelsProviderCancellationRequestFactory>().To<SunHotelsCancellationRequestFactory>().WhenAnyAncestorNamed("SunHotels");
            #endregion

            #region Hotusa
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<HotusaAvailabilityProvider>().Named("Hotusa");
            Bind<IAccommodationAvailabilityAutomatorAsync<HotusaAvailabilityResponse>>().To<HotusaAvailabilityAutomator>().WhenAnyAncestorNamed("Hotusa");
            Bind<IAccommodationAvailabilityParser<HotusaAvailabilityResponse>>().To<HotusaAvailabilityParser>().WhenAnyAncestorNamed("Hotusa");
            Bind<IHotusaAvailabilityRequestFactory>().To<HotusaAvailabilityRequestFactory>().WhenAnyAncestorNamed("Hotusa");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<HotusaValuationProvider>().Named("Hotusa");
            Bind<IAccommodationValuationAutomatorAsync<HotusaValuationResponse>>().To<HotusaValuationAutomator>().WhenAnyAncestorNamed("Hotusa");
            Bind<IAccommodationValuationParser<HotusaValuationResponse>>().To<HotusaValuationParser>().WhenAnyAncestorNamed("Hotusa");
            Bind<IHotusaProviderValuationRequestFactory>().To<HotusaValuationRequestFactory>().WhenAnyAncestorNamed("Hotusa");

            //Booking
            Bind<IAccommodationBookingProvider>().To<HotusaBookingProvider>().Named("Hotusa");
            Bind<IAccommodationBookingAutomatorAsync<HotusaBookingResponse>>().To<HotusaBookingAutomator>().WhenAnyAncestorNamed("Hotusa");
            Bind<IAccommodationBookingParser<HotusaBookingResponse>>().To<HotusaBookingParser>().WhenAnyAncestorNamed("Hotusa");
            Bind<IHotusaBookingRequestFactory>().To<HotusaBookingRequestFactory>().WhenAnyAncestorNamed("Hotusa");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<HotusaCancellationProvider>().Named("Hotusa");
            Bind<IAccommodationCancellationAutomatorAsync<HotusaCancellationResponse>>().To<HotusaCancellationAutomator>().WhenAnyAncestorNamed("Hotusa");
            Bind<IAccommodationCancellationParserAsync<HotusaCancellationResponse>>().To<HotusaCancellationlationParser>().WhenAnyAncestorNamed("Hotusa");
            Bind<IHotusaProviderCancellationRequestFactory>().To<HotusaCancellationRequestFactory>().WhenAnyAncestorNamed("Hotusa");
            #endregion

            #region Avra
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<AvraAvailabilityProvider>().Named("Avra");
            Bind<IAccommodationAvailabilityAutomatorAsync<AvraAvailabilityResponse>>().To<AvraAvailabilityAutomator>().WhenAnyAncestorNamed("Avra");
            Bind<IAccommodationAvailabilityParser<AvraAvailabilityResponse>>().To<AvraAvailabilityParser>().WhenAnyAncestorNamed("Avra");
            Bind<IAvraAvailabilityRequestFactory>().To<AvraAvailabilityRequestFactory>().WhenAnyAncestorNamed("Avra");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<AvraValuationProvider>().Named("Avra");
            Bind<IAccommodationValuationAutomatorAsync<AvraValuationResponse>>().To<AvraValuationAutomator>().WhenAnyAncestorNamed("Avra");
            Bind<IAccommodationValuationParser<AvraValuationResponse>>().To<AvraValuationParser>().WhenAnyAncestorNamed("Avra");
            Bind<IAvraValuationRequestFactory>().To<AvraValuationRequestFactory>().WhenAnyAncestorNamed("Avra");

            //Booking
            Bind<IAccommodationBookingProvider>().To<AvraBookingProvider>().Named("Avra");
            Bind<IAccommodationBookingAutomatorAsync<AvraBookingResponse>>().To<AvraBookingAutomator>().WhenAnyAncestorNamed("Avra");
            Bind<IAccommodationBookingParser<AvraBookingResponse>>().To<AvraBookingParser>().WhenAnyAncestorNamed("Avra");
            Bind<IAvraBookingRequestFactory>().To<AvraBookingRequestFactory>().WhenAnyAncestorNamed("Avra");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<AvraCancellationProvider>().Named("Avra");
            Bind<IAccommodationCancellationAutomatorAsync<AvraCancellationResponse>>().To<AvraCancellationAutomator>().WhenAnyAncestorNamed("Avra");
            Bind<IAccommodationCancellationParserAsync<AvraCancellationResponse>>().To<AvraCancellationParser>().WhenAnyAncestorNamed("Avra");
            Bind<IAvraCancellationRequestFactory>().To<AvraCancellationRequestFactory>().WhenAnyAncestorNamed("Avra");
            #endregion

            #region Bonotel
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<BonotelAvailabilityProvider>().Named("Bonotel");
            Bind<IAccommodationAvailabilityAutomatorAsync<BonotelAvailabilityResponse>>().To<BonotelAvailabilityAutomator>().WhenAnyAncestorNamed("Bonotel");
            Bind<IAccommodationAvailabilityParser<BonotelAvailabilityResponse>>().To<BonotelAvailabilityParser>().WhenAnyAncestorNamed("Bonotel");
            Bind<IBonotelAvailabilityRequestFactory>().To<BonotelAvailabilityRequestFactory>().WhenAnyAncestorNamed("Bonotel");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<BonotelValuationProvider>().Named("Bonotel");
            Bind<IAccommodationValuationAutomatorAsync<BonotelValuationResponse>>().To<BonotelValuationAutomator>().WhenAnyAncestorNamed("Bonotel");
            Bind<IAccommodationValuationParser<BonotelValuationResponse>>().To<BonotelValuationParser>().WhenAnyAncestorNamed("Bonotel");
            Bind<IBonotelValuationRequestFactory>().To<BonotelValuationRequestFactory>().WhenAnyAncestorNamed("Bonotel");

            //Booking
            Bind<IAccommodationBookingProvider>().To<BonotelBookingProvider>().Named("Bonotel");
            Bind<IAccommodationBookingAutomatorAsync<BonotelBookingResponse>>().To<BonotelBookingAutomator>().WhenAnyAncestorNamed("Bonotel");
            Bind<IAccommodationBookingParser<BonotelBookingResponse>>().To<BonotelBookingParser>().WhenAnyAncestorNamed("Bonotel");
            Bind<IBonotelBookingRequestFactory>().To<BonotelBookingRequestFactory>().WhenAnyAncestorNamed("Bonotel");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<BonotelCancellationProvider>().Named("Bonotel");
            Bind<IAccommodationCancellationAutomatorAsync<BonotelCancellationResponse>>().To<BonotelCancellationAutomator>().WhenAnyAncestorNamed("Bonotel");
            Bind<IAccommodationCancellationParserAsync<BonotelCancellationResponse>>().To<BonotelCancellationlationParser>().WhenAnyAncestorNamed("Bonotel");
            Bind<IBonotelProviderCancellationRequestFactory>().To<BonotelCancellationRequestFactory>().WhenAnyAncestorNamed("Bonotel");
            #endregion

            #region ItalCamel
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<ItalCamelAvailabilityProvider>().Named("ItalCamel");
            Bind<IAccommodationAvailabilityAutomatorAsync<ItalCamelAvailabilityResponse>>().To<ItalCamelAvailabilityAutomator>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IAccommodationAvailabilityParser<ItalCamelAvailabilityResponse>>().To<ItalCamelAvailabilityParser>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IItalCamelAvailabilityRequestFactory>().To<ItalCamelAvailabilityRequestFactory>().WhenAnyAncestorNamed("ItalCamel");
            //Valuation
            Bind<IAccommodationValuationProvider>().To<ItalCamelValuationProvider>().Named("ItalCamel");
            Bind<IAccommodationValuationAutomatorAsync<ItalCamelValuationResponse>>().To<ItalCamelValuationAutomator>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IAccommodationValuationParser<ItalCamelValuationResponse>>().To<ItalCamelValuationParser>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IItalCamelProviderValuationRequestFactory>().To<ItalCamelProviderValuationRequestFactory>().WhenAnyAncestorNamed("ItalCamel");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ItalCamelBookingProvider>().Named("ItalCamel");
            Bind<IAccommodationBookingAutomatorAsync<ItalCamelBookingResponse>>().To<ItalCamelBookingAutomator>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IAccommodationBookingParser<ItalCamelBookingResponse>>().To<ItalCamelBookingParser>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IItalCamelBookingRequestFactory>().To<ItalCamelBookingRequestFactory>().WhenAnyAncestorNamed("ItalCamel");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ItalCamelCancellationProvider>().Named("ItalCamel");
            Bind<IAccommodationCancellationAutomatorAsync<ItalCamelCancellationResponse>>().To<ItalCamelCancellationAutomator>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IAccommodationCancellationParserAsync<ItalCamelCancellationResponse>>().To<ItalCamelCancellationlationParser>().WhenAnyAncestorNamed("ItalCamel");
            Bind<IItalCamelProviderCancellationRequestFactory>().To<ItalCamelCancellationRequestFactory>().WhenAnyAncestorNamed("ItalCamel");
            #endregion


            #region OswaldArrigo
            //Avail
            Bind<IAccommodationAvailabilityProvider>().To<OswaldArrigoAvailabilityProvider>().Named("OswaldArrigo");
            Bind<IAccommodationAvailabilityAutomatorAsync<OswaldArrigoAvailabilityResponse>>().To<OswaldArrigoAvailabilityAutomator>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<IAccommodationAvailabilityParser<OswaldArrigoAvailabilityResponse>>().To<OswaldArrigoAvailabilityParser>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<ICavalAvailabilityRequestFactory>().To<OswaldArrigoAvailabilityRequestFactory>().WhenAnyAncestorNamed("OswaldArrigo");
            //Valuation
            Bind<IAccommodationValuationProvider>().To<OswaldArrigoValuationProvider>().Named("OswaldArrigo");
            Bind<IAccommodationValuationAutomatorAsync<OswaldArrigoValuationResponse>>().To<OswaldArrigoValuationAutomator>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<IAccommodationValuationParser<OswaldArrigoValuationResponse>>().To<OswaldArrigoValuationParser>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<ICavalValuationRequestFactory>().To<OswaldArrigoValuationRequestFactory>().WhenAnyAncestorNamed("OswaldArrigo");

            //Booking
            Bind<IAccommodationBookingProvider>().To<OswaldArrigoBookingProvider>().Named("OswaldArrigo");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<OswaldArrigoBookingResponse>>>().To<OswaldArrigoBookingAutomator>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<IAccommodationBookingParser<IEnumerable<OswaldArrigoBookingResponse>>>().To<OswaldArrigoBookingParser>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<ICavalBookingRequestFactory>().To<OswaldArrigoProviderBookingRequestFactory>().WhenAnyAncestorNamed("OswaldArrigo");

            ////Cancellation
            Bind<IAccommodationCancellationProvider>().To<OswaldArrigoCancellationProvider>().Named("OswaldArrigo");
            Bind<IAccommodationCancellationAutomatorAsync<OswaldArrigoCancellationResponse>>().To<OswaldArrigoCancellationAutomator>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<IAccommodationCancellationParserAsync<OswaldArrigoCancellationResponse>>().To<OswaldArrigoCancellationParser>().WhenAnyAncestorNamed("OswaldArrigo");
            Bind<ICavalCancellationRequestFactory>().To<OswaldArrigoCancellationRequestFactory>().WhenAnyAncestorNamed("OswaldArrigo");
            #endregion

            #region Resort Marketing
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<RMIAvailabilityProvider>().Named("Resort Marketing");
            Bind<IAccommodationAvailabilityAutomatorAsync<RMIAvailabilityResponse>>().To<RMIAvailabilityAutomator>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IAccommodationAvailabilityParser<RMIAvailabilityResponse>>().To<RMIAvailabilityParser>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IRMIAvailabilityRequestFactory>().To<RMIAvailabilityRequestFactory>().WhenAnyAncestorNamed("Resort Marketing");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<RMIValuationProvider>().Named("Resort Marketing");
            Bind<IAccommodationValuationAutomatorAsync<RMIValuationResponse>>().To<RMIValuationAutomator>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IAccommodationValuationParser<RMIValuationResponse>>().To<RMIValuationParser>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IRMIValuationRequestFactory>().To<RMIValuationRequestFactory>().WhenAnyAncestorNamed("Resort Marketing");

            //Booking
            Bind<IAccommodationBookingProvider>().To<RMIBookingProvider>().Named("Resort Marketing");
            Bind<IAccommodationBookingAutomatorAsync<RMIBookingResponse>>().To<RMIBookingAutomator>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IAccommodationBookingParser<RMIBookingResponse>>().To<RMIBookingParser>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IRMIBookingRequestFactory>().To<RMIBookingRequestFactory>().WhenAnyAncestorNamed("Resort Marketing");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<RMICancellationProvider>().Named("Resort Marketing");
            Bind<IAccommodationCancellationAutomatorAsync<RMICancellationResponse>>().To<RMICancellationAutomator>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IAccommodationCancellationParserAsync<RMICancellationResponse>>().To<RMICancellationlationParser>().WhenAnyAncestorNamed("Resort Marketing");
            Bind<IRMIProviderCancellationRequestFactory>().To<RMICancellationRequestFactory>().WhenAnyAncestorNamed("Resort Marketing");
            #endregion

            #region ViajesOlympia
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<ViajesOlympiaAvailabilityProvider>().Named("Viajes Olympia");
            Bind<IAccommodationAvailabilityAutomatorAsync<ViajesOlympiaAvailabilityResponse>>().To<ViajesOlympiaAvailabilityAutomator>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IAccommodationAvailabilityParser<ViajesOlympiaAvailabilityResponse>>().To<ViajesOlympiaAvailabilityParser>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IViajesOlympiaAvailabilityRequestFactory>().To<ViajesOlympiaAvailabilityRequestFactory>().WhenAnyAncestorNamed("Viajes Olympia");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ViajesOlympiaValuationProvider>().Named("Viajes Olympia");
            Bind<IAccommodationValuationAutomatorAsync<ViajesOlympiaValuationResponse>>().To<ViajesOlympiaValuationAutomator>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IAccommodationValuationParser<ViajesOlympiaValuationResponse>>().To<ViajesOlympiaValuationParser>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IViajesOlympiaValuationRequestFactory>().To<ViajesOlympiaValuationRequestFactory>().WhenAnyAncestorNamed("Viajes Olympia");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ViajesOlympiaBookingProvider>().Named("Viajes Olympia");
            Bind<IAccommodationBookingAutomatorAsync<ViajesOlympiaBookingResponse>>().To<ViajesOlympiaBookingAutomator>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IAccommodationBookingParser<ViajesOlympiaBookingResponse>>().To<ViajesOlympiaBookingParser>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IViajesOlympiaBookingRequestFactory>().To<ViajesOlympiaBookingRequestFactory>().WhenAnyAncestorNamed("Viajes Olympia");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ViajesOlympiaCancellationProvider>().Named("Viajes Olympia");
            Bind<IAccommodationCancellationAutomatorAsync<ViajesOlympiaCancellationResponse>>().To<ViajesOlympiaCancellationAutomator>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IAccommodationCancellationParserAsync<ViajesOlympiaCancellationResponse>>().To<ViajesOlympiaCancellationParser>().WhenAnyAncestorNamed("Viajes Olympia");
            Bind<IViajesOlympiaProviderCancellationRequestFactory>().To<ViajesOlympiaCancellationRequestFactory>().WhenAnyAncestorNamed("Viajes Olympia");
            #endregion

            #region ViatgesMagon
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<ViatgesMagonAvailabilityProvider>().Named("Viatges Magon");
            Bind<IAccommodationAvailabilityAutomatorAsync<ViatgesMagonAvailabilityResponse>>().To<ViatgesMagonAvailabilityAutomator>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<IAccommodationAvailabilityParser<ViatgesMagonAvailabilityResponse>>().To<ViatgesMagonAvailabilityParser>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<ICavalAvailabilityRequestFactory>().To<ViatgesMagonAvailabilityRequestFactory>().WhenAnyAncestorNamed("Viatges Magon");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ViatgesMagonValuationProvider>().Named("Viatges Magon");
            Bind<IAccommodationValuationAutomatorAsync<ViatgesMagonValuationResponse>>().To<ViatgesMagonValuationAutomator>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<IAccommodationValuationParser<ViatgesMagonValuationResponse>>().To<ViatgesMagonValuationParser>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<ICavalValuationRequestFactory>().To<ViatgesMagonValuationRequestFactory>().WhenAnyAncestorNamed("Viatges Magon");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ViatgesMagonBookingProvider>().Named("Viatges Magon");
            Bind<IAccommodationBookingAutomatorAsync<IEnumerable<ViatgesMagonBookingResponse>>>().To<ViatgesMagonBookingAutomator>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<IAccommodationBookingParser<IEnumerable<ViatgesMagonBookingResponse>>>().To<ViatgesMagonBookingParser>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<ICavalBookingRequestFactory>().To<ViatgesMagonProviderBookingRequestFactory>().WhenAnyAncestorNamed("Viatges Magon");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ViatgesMagonCancellationProvider>().Named("Viatges Magon");
            Bind<IAccommodationCancellationAutomatorAsync<ViatgesMagonCancellationResponse>>().To<ViatgesMagonCancellationAutomator>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<IAccommodationCancellationParserAsync<ViatgesMagonCancellationResponse>>().To<ViatgesMagonCancellationParser>().WhenAnyAncestorNamed("Viatges Magon");
            Bind<ICavalCancellationRequestFactory>().To<ViatgesMagonCancellationRequestFactory>().WhenAnyAncestorNamed("Viatges Magon");
            #endregion

            #region ViajesSideTours
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<ViajesSideToursAvailabilityProvider>().Named("Viajes SideTours");
            Bind<IAccommodationAvailabilityAutomatorAsync<ViajesSideToursAvailabilityResponse>>().To<ViajesSideToursAvailabilityAutomator>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IAccommodationAvailabilityParser<ViajesSideToursAvailabilityResponse>>().To<ViajesSideToursAvailabilityParser>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IViajesSideToursAvailabilityRequestFactory>().To<ViajesSideToursAvailabilityRequestFactory>().WhenAnyAncestorNamed("Viajes SideTours");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<ViajesSideToursValuationProvider>().Named("Viajes SideTours");
            Bind<IAccommodationValuationAutomatorAsync<ViajesSideToursValuationResponse>>().To<ViajesSideToursValuationAutomator>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IAccommodationValuationParser<ViajesSideToursValuationResponse>>().To<ViajesSideToursValuationParser>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IViajesSideToursValuationRequestFactory>().To<ViajesSideToursValuationRequestFactory>().WhenAnyAncestorNamed("Viajes SideTours");

            //Booking
            Bind<IAccommodationBookingProvider>().To<ViajesSideToursBookingProvider>().Named("Viajes SideTours");
            Bind<IAccommodationBookingAutomatorAsync<ViajesSideToursBookingResponse>>().To<ViajesSideToursBookingAutomator>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IAccommodationBookingParser<ViajesSideToursBookingResponse>>().To<ViajesSideToursBookingParser>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IViajesSideToursBookingRequestFactory>().To<ViajesSideToursBookingRequestFactory>().WhenAnyAncestorNamed("Viajes SideTours");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<ViajesSideToursCancellationProvider>().Named("Viajes SideTours");
            Bind<IAccommodationCancellationAutomatorAsync<ViajesSideToursCancellationResponse>>().To<ViajesSideToursCancellationAutomator>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IAccommodationCancellationParserAsync<ViajesSideToursCancellationResponse>>().To<ViajesSideToursCancellationParser>().WhenAnyAncestorNamed("Viajes SideTours");
            Bind<IViajesSideToursCancellationRequestFactory>().To<ViajesSideToursCancellationRequestFactory>().WhenAnyAncestorNamed("Viajes SideTours");
            #endregion

            #region Hotetec
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<HotetecAvailabilityProvider>().Named("Hotetec");
            Bind<IAccommodationAvailabilityAutomatorAsync<HotetecAvailabilityResponse>>().To<HotetecAvailabilityAutomator>().WhenAnyAncestorNamed("Hotetec");
            Bind<IAccommodationAvailabilityParser<HotetecAvailabilityResponse>>().To<HotetecAvailabilityParser>().WhenAnyAncestorNamed("Hotetec");
            Bind<IHotetecAvailabilityRequestFactory>().To<HotetecAvailabilityRequestFactory>().WhenAnyAncestorNamed("Hotetec");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<HotetecValuationProvider>().Named("Hotetec");
            Bind<IAccommodationValuationAutomatorAsync<HotetecValuationResponse>>().To<HotetecValuationAutomator>().WhenAnyAncestorNamed("Hotetec");
            Bind<IAccommodationValuationParser<HotetecValuationResponse>>().To<HotetecValuationParser>().WhenAnyAncestorNamed("Hotetec");
            Bind<IHotetecValuationRequestFactory>().To<HotetecValuationRequestFactory>().WhenAnyAncestorNamed("Hotetec");

            //Booking
            Bind<IAccommodationBookingProvider>().To<HotetecBookingProvider>().Named("Hotetec");
            Bind<IAccommodationBookingAutomatorAsync<HotetecBookingResponse>>().To<HotetecBookingAutomator>().WhenAnyAncestorNamed("Hotetec");
            Bind<IAccommodationBookingParser<HotetecBookingResponse>>().To<HotetecBookingParser>().WhenAnyAncestorNamed("Hotetec");
            Bind<IHotetecBookingRequestFactory>().To<HotetecBookingRequestFactory>().WhenAnyAncestorNamed("Hotetec");

            //Cancellation
            Bind<IAccommodationCancellationProvider>().To<HotetecCancellationProvider>().Named("Hotetec");
            Bind<IAccommodationCancellationAutomatorAsync<HotetecCancellationResponse>>().To<HotetecCancellationAutomator>().WhenAnyAncestorNamed("Hotetec");
            Bind<IAccommodationCancellationParserAsync<HotetecCancellationResponse>>().To<HotetecCancellationParser>().WhenAnyAncestorNamed("Hotetec");
            Bind<IHotetecProviderCancellationRequestFactory>().To<HotetecCancellationRequestFactory>().WhenAnyAncestorNamed("Hotetec");
            #endregion

            #region HotelCompany
            // Avail
            Bind<IAccommodationAvailabilityProvider>().To<HotelCompanyAvailabilityProvider>().Named("HotelCompany");
            Bind<IAccommodationAvailabilityAutomatorAsync<HotelCompanyAvailabilityResponse>>().To<HotelCompanyAvailabilityAutomator>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IAccommodationAvailabilityParser<HotelCompanyAvailabilityResponse>>().To<HotelCompanyAvailabilityParser>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IHotelCompanyAvailabilityRequestFactory>().To<HotelCompanyAvailabilityRequestFactory>().WhenAnyAncestorNamed("HotelCompany");

            //Valuation
            Bind<IAccommodationValuationProvider>().To<HotelCompanyValuationProvider>().Named("HotelCompany");
            Bind<IAccommodationValuationAutomatorAsync<HotelCompanyValuationResponse>>().To<HotelCompanyValuationAutomator>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IAccommodationValuationParser<HotelCompanyValuationResponse>>().To<HotelCompanyValuationParser>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IHotelCompanyValuationRequestFactory>().To<HotelCompanyValuationRequestFactory>().WhenAnyAncestorNamed("HotelCompany");

            //Booking
            Bind<IAccommodationBookingProvider>().To<HotelCompanyBookingProvider>().Named("HotelCompany");
            Bind<IAccommodationBookingAutomatorAsync<HotelCompanyBookingResponse>>().To<HotelCompanyBookingAutomater>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IAccommodationBookingParser<HotelCompanyBookingResponse>>().To<HotelCompanyBookingParser>().WhenAnyAncestorNamed("HotelCompany");
            Bind<IHotelCompanyBookingRequestFactory>().To<HotelCompanyBookingRequestFactory>().WhenAnyAncestorNamed("HotelCompany");
            #endregion

            #region TravelGate
            // Avail

            Bind<IAccommodationAvailabilityProvider>().To<TravelGateAvailabilityProvider>().Named("TravelGate");
            Bind<IAccommodationAvailabilityAutomatorAsync<TravelGateAvailabilityResponse>>().To<TravelGateAvailabilityAutomator>().WhenAnyAncestorNamed("TravelGate");
            Bind<IAccommodationAvailabilityParser<TravelGateAvailabilityResponse>>().To<TravelGateAvailabilityParser>().WhenAnyAncestorNamed("TravelGate");
            Bind<ITravelGateProviderAvailabilityRequestFactory>().To<TravelGateProviderAvailabilityRequestFactory>().WhenAnyAncestorNamed("TravelGate");

            // Valuation
            Bind<IAccommodationValuationProvider>().To<TravelGateValuationProvider>().Named("TravelGate");
            Bind<IAccommodationValuationAutomatorAsync<TravelGateValuationResponse>>().To<TravelGateValuationAutomator>().WhenAnyAncestorNamed("TravelGate");
            Bind<IAccommodationValuationParserAsync<TravelGateValuationResponse>>().To<TravelGateValuationParser>().WhenAnyAncestorNamed("TravelGate");
            Bind<ITravelGateProviderValuationRequestFactory>().To<TravelGateProviderValuationRequestFactory>().WhenAnyAncestorNamed("TravelGate");

            //Booking
            Bind<IAccommodationBookingProvider>().To<TravelGateBookingProvider>().Named("TravelGate");
            Bind<IAccommodationBookingAutomatorAsync<TravelGateBookingResponse>>().To<TravelGateBookingAutomator>().WhenAnyAncestorNamed("TravelGate");
            Bind<IAccommodationBookingParser<TravelGateBookingResponse>>().To<TravelGateBookingParser>().WhenAnyAncestorNamed("TravelGate");
            Bind<ITravelGateProviderBookingRequestFactory>().To<TravelGateProviderBookingRequestFactory>().WhenAnyAncestorNamed("TravelGate");

            //Parameters
            Bind<ITravelGateParameterService>().To<TravelGateParameterService>().WhenAnyAncestorNamed("TravelGate");
            #endregion
        }
    }
}
