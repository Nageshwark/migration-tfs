﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider
{
    public abstract class AccommodationAvailabilitySearchBase : IAccommodationAvailabilityProvider
    {
        public abstract Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request);
    }
}
