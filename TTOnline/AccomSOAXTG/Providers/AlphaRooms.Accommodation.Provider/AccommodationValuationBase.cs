﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Provider
{
    public abstract class AccommodationValuationBase : IAccommodationValuationProvider
    {
        public abstract Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request);
    }
}
