﻿using System.Web.Services.Description;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<LowCostBedsAvailabilityResponse>
    {
        private const string LowCostBedsAvailabilityUrl = "LowCostBedsAvailabilityUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ILowCostBedsProviderAvailabilityRequestFactory lowCostBedsProviderAvailabilityRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public LowCostBedsAvailabilityAutomator(ILogger logger,
                                                //IAccommodationConfigurationManager configurationManager,
                                                ILowCostBedsProviderAvailabilityRequestFactory LowCostBedsProviderAvailabilityRequestFactory,
                                                IProviderOutputLogger outputLogger
                                                )
        {
            this.logger = logger;
           // this.configurationManager = configurationManager;
            this.lowCostBedsProviderAvailabilityRequestFactory = LowCostBedsProviderAvailabilityRequestFactory;
            this.outputLogger = outputLogger;
        }

        public async Task<LowCostBedsAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            LowCostBedsAvailabilityResponse response = null;
            
            try
            {
                var supplierRequest = this.lowCostBedsProviderAvailabilityRequestFactory.CreateProviderAvailabilityRequest(request);
                string requestXML = supplierRequest.XmlSerialize();

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    
                    if (response.SupplierResponse != null)
                    {
                        sb.AppendLine(response.XmlSerialize());
                    }
                    else
                    {
                        sb.AppendLine("Supplier Request Not Present!");
                    }

                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    
                    if (response.SupplierResponse !=null)
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    
                    sb.AppendLine();

                }

                throw new SupplierApiException(string.Format("LowCostBedsAvailabilityAutomator.GetAvailabilityResponseAsync: LowCostBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<LowCostBedsAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, SearchRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(LowCostBedsAvailabilityUrl);
            string serialisedRequest = supplierRequest.XmlSerialize();
            
            
            wbBook client = new wbBook();
            client.Url = url;

            SearchResponse response = client.Search(supplierRequest);

            // debug code
            if (request.Debugging)
            {
                string xmlResponse = response.XmlSerialize();
            }

            this.outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, response);

            LowCostBedsAvailabilityResponse supplierResponse = new LowCostBedsAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
