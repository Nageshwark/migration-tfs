﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<LowCostBedsBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<LowCostBedsBookingResponse>> parser;

        public LowCostBedsBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<LowCostBedsBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<LowCostBedsBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
