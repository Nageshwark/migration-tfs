﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.LowCostBeds;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<LowCostBedsValuationResponse> automator;
        private readonly IAccommodationValuationParser<LowCostBedsValuationResponse> parser;

        public LowCostBedsValuationProvider(IAccommodationValuationAutomatorAsync<LowCostBedsValuationResponse> automator,
                                                IAccommodationValuationParser<LowCostBedsValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
