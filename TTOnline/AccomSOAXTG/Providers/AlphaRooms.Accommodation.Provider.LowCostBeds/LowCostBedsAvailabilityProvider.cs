﻿//using AlphaRooms.Configuration;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;
using AlphaRooms.Utilities;
using Ninject;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<LowCostBedsAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<LowCostBedsAvailabilityResponse> parser;

        public LowCostBedsAvailabilityProvider(   IAccommodationAvailabilityAutomatorAsync<LowCostBedsAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<LowCostBedsAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
