﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsValuationAutomator : IAccommodationValuationAutomatorAsync<LowCostBedsValuationResponse>
    {
        private const string LowCostBedsAvailabilityUrl = "LowCostBedsAvailabilityUrl";
        //private const string LowCostBedsUsername = "LowCostBedsUsername";
        //private const string LowCostBedsPassword = "LowCostBedsPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ILowCostBedsProviderValuationRequestFactory lowCostBedsProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public LowCostBedsValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ILowCostBedsProviderValuationRequestFactory LowCostBedsSupplierValuationRequestFactory
            , IProviderOutputLogger outputLogger
            , IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.lowCostBedsProviderValuationRequestFactory = LowCostBedsSupplierValuationRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<LowCostBedsValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            PreBookRequest supplierRequest = null;
            LowCostBedsValuationResponse response = null;
            try
            {
                supplierRequest = this.lowCostBedsProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("LowCostBedsValuationAutomator.GetValuationResponseAsync: LowCostBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<LowCostBedsValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, PreBookRequest supplierRequest)
        {
            PreBookResponse response = null;

            string url = request.Provider.Parameters.GetParameterValue(LowCostBedsAvailabilityUrl);
            string serialisedRequest = supplierRequest.XmlSerialize();


            wbBook client = new wbBook();
            client.Url = url;

            this.loggerService.SetProviderRequest(request, supplierRequest);
            response = client.PreBook(supplierRequest);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, response);
            if (request.Debugging)
            {
                string xmlResponse = response.XmlSerialize();
            }

            LowCostBedsValuationResponse supplierResponse = new LowCostBedsValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

       
    
    }
}
