﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsProviderBookingRequestFactory : ILowCostBedsProviderBookingRequestFactory
    {
        private readonly ILowCostBedsParameterService parameterService;

        public LowCostBedsProviderBookingRequestFactory(ILowCostBedsParameterService parameterService)
        {
            this.parameterService = parameterService;
        }

        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookRequest CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            string userName, password;
            parameterService.GetCredentials(request.ChannelInfo, request.Provider, out userName, out password);
            string tradeReference = request.ItineraryId.ToString();

            
            var supplierRequest = new BookRequest();
            var bookingDetails = new BookingDetails();
            var leadGuest = request.Customer;

            bookingDetails.TradeReference = tradeReference;
            bookingDetails.ExpectedTotal = request.ValuatedRooms.First().ValuationResult.SalePrice.Amount;

            bookingDetails.ArrivalDate = request.ValuatedRooms.First().AvailabilityRequest.CheckInDate;
            bookingDetails.Duration = CalculateDuration(request.ValuatedRooms.First().AvailabilityRequest);
            bookingDetails.PropertyID = Convert.ToInt32(request.ValuatedRooms.First().ValuationResult.EstablishmentEdiCode);

            bookingDetails.LeadGuestTitle = leadGuest.Title.ToString();
            bookingDetails.LeadGuestFirstName = leadGuest.FirstName;
            bookingDetails.LeadGuestLastName = leadGuest.Surname;
            bookingDetails.LeadGuestEmail = leadGuest.EmailAddress;

            bookingDetails.RoomBookings = CreateRoomRequests(request.ValuatedRooms);

            if (request.ValuatedRooms.First().ValuationResult.PaymentModel == PaymentModel.CustomerPayDirect 
                    && request.ValuatedRooms.First().PaymentDetails != null)
            {
                var card  = request.ValuatedRooms.First().PaymentDetails;
                var surcharge = CalculateSurcharge(request.ValuatedRooms.First().ValuationResult.SalePrice.Amount, card.CardType);
                var amount = request.ValuatedRooms.First().ValuationResult.SalePrice.Amount;
                var currencyCode = request.ValuatedRooms.First().ValuationResult.SalePrice.CurrencyCode;

                supplierRequest.Payment = new Payment
                {
                    CardHoldersName = card.CardHolderName,
                    CreditCardTypeID = SetCardTypeId(card.CardType),
                    CardNumber = card.CardNumber,
                    SecurityCode = card.CardSecurityCode,
                    ExpireMonth = card.CardExpireDate.Month,
                    ExpireYear = card.CardExpireDate.Year,
                    Address1 = card.AddressLine1,
                    Address2 = card.AddressLine2,
                    TownCity = card.City,
                    Postcode = card.Postcode,
                    CurrencyCode = currencyCode,
                    Amount = amount,
                    TotalPayment = amount + surcharge,
                    Surcharge = surcharge
                };
            }

            supplierRequest.BookingDetails = bookingDetails;
            supplierRequest.LoginDetails = new LoginDetails()
            {
                Login = userName,
                Password = password
            };



            return supplierRequest;
        }

        public BookingRequest CreateBookingSearchRequest(AccommodationProviderBookingRequest request, string bookingReference)
        {
            var bookingRequest = new BookingRequest();
            string userName, password;
            parameterService.GetCredentials(request.ChannelInfo, request.Provider, out userName, out password);

            bookingRequest.LoginDetails = new LoginDetails()
            {
                Login = userName,
                Password = password
            };

            bookingRequest.BookingReference = bookingReference;

            return bookingRequest;
        }

        private decimal CalculateSurcharge(decimal amount, CardType cardType)
        {
            //TODO:Confir how to calcualte Surcharge.  Waiting for response from AlphaRooms Issue Number:50
            const decimal surchargePercent = 1;
           
            bool isCreditCard;

            switch (cardType)
            {
                case CardType.MastercardDebit:
                case CardType.VisaDebit:
                    isCreditCard = false;
                    break;

                default:
                    isCreditCard = true;
                    break;
                    
            }

            if (isCreditCard)
            {
                return Math.Round((amount*surchargePercent)/100, 2);
            }
            else
            {
                return 0;
            }

        }

        private int SetCardTypeId(CardType cardType)
        {
            switch (cardType)
            {
                case CardType.VisaCredit:
                    return 1;

                case CardType.MastercardDebit:
                    return 2;

                case CardType.VisaDebit:
                    return 3;

                case CardType.VisaDelta:
                    return 4;

                case CardType.VisaElectron:
                    return 5;

                case CardType.Switch:
                    return 6;

                case CardType.Solo:
                    return 7;

                case CardType.Maestro:
                    return 8;

                case CardType.MasterCard:
                    return 14;

                default:
                    throw new Exception("Card type not supported by this Supplier");
            }
            

            /*
             * 1                             Visa Credit
                2                             MasterCard Debit
                3                             Visa Debit
                4                             Visa Delta
                5                             Visa Electron
                6                             Switch
                7                             Solo
                8                             Maestro
                14                           Mastercard Credit
                16                           Elsie manual card
             */

            return 1;
        }

       

        private RoomBooking[] CreateRoomRequests(AccommodationProviderBookingRequestRoom[] rooms)
        {
            var supplieRoomRequests = new List<RoomBooking>();

            foreach (var room in rooms)
            {
                var supplierRoom = new RoomBooking()
                {
                    Adults = room.Guests.AdultsCount,
                    Children = room.Guests.ChildrenCount,
                    Infants = room.Guests.InfantsCount,
                    PropertyRoomTypeID = Convert.ToInt32(room.ValuationResult.RoomCode),
                    MealBasisID = Convert.ToInt32(room.ValuationResult.BoardCode),

                };

                List<Guest> guests = new List<Guest>();

                foreach (var guest in room.Guests)
                {
                    guests.Add(new Guest
                    {
                        Age = guest.Age,
                        FirstName = guest.FirstName,
                        LastName = guest.Surname,
                        Type = guest.Type.ToString(),
                        Title = guest.Title.ToString()
                    });
                }

                supplierRoom.Guests = guests.ToArray();

                supplieRoomRequests.Add(supplierRoom);
            }

            return supplieRoomRequests.ToArray();
        }
     

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int) duration.TotalDays;

        }

    }
}
