﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsProviderAvailabilityRequestFactory : ILowCostBedsProviderAvailabilityRequestFactory
    {
        private readonly ILowCostBedsParameterService parameterService;
        private readonly ILogger logger;

        /// <summary>
        /// Helper class used for grouping rooms together by number of adults, number of children, and child ages
        /// </summary>
        private class Room
        {
            public int Adults { get; set; }
            public int ChildrenAndInfants { get; set; }
            public int[] ChildAges { get; set; }
            public string ChildAgesCsv { get; set; }
        }

        // private readonly ISupplierService supplierService;

        public LowCostBedsProviderAvailabilityRequestFactory(ILowCostBedsParameterService parameterService, ILogger logger)
        {
            this.parameterService = parameterService;
            this.logger = logger;
            // this.supplierService = supplierService;
        }

        /// <summary>
        /// Note:  Limitations for HotelValuedAvail request:
        /// A maximum of 31 nights per hotel service can be requested.
        /// A maximum of 5 rooms per hotel service can be requested.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {

            string userName, password;
            //get the login credentials for the api based on the channel
            parameterService.GetCredentials(request.ChannelInfo, request.Provider, out userName, out password);

            SearchRequest supplierRequest = new SearchRequest();

            SearchDetails searchDetails = new SearchDetails();
            searchDetails.ArrivalDate = request.CheckInDate;
            searchDetails.Duration = CalculateDuration(request);

            //Search by Destination Codes
            if (request.DestinationCodes != null && request.DestinationCodes.Any())
            {
                //Search By Region ID only
                if (request.DestinationCodes.Length == 1 && !request.DestinationCodes.First().Contains(";"))
                {
                    searchDetails.RegionID = Convert.ToInt32(request.DestinationCodes.First());
                }
                else
                {
                    searchDetails.Resorts = ConvertToResort(request.DestinationCodes);

                    //Specify RegionID which is mandatory for Resort Search
                    if (searchDetails.Resorts != null && request.DestinationCodes != null &&
                        request.DestinationCodes.Any())
                        searchDetails.RegionID = Convert.ToInt32(request.DestinationCodes.First().Split(';')[0]);
                }
            }
            else if (request.EstablishmentCodes != null && request.EstablishmentCodes.Any())        //Search by Establishment Codes
            {
                //Since low cost beds expects establishment codes in integers, there may be old mappings in the system which were strings and 
                //they break while converting thereby not even creating the request with the succeeded conversions. So we are eliminating failed conversions.
                ConcurrentQueue<int> succeeded = new ConcurrentQueue<int>();
                ConcurrentQueue<string> failed = new ConcurrentQueue<string>();

                Parallel.ForEach(request.EstablishmentCodes, x =>
                {
                    int code;
                    if (int.TryParse(x, out code))
                    {
                        succeeded.Enqueue(code);
                    }
                    else
                    {
                        failed.Enqueue(x);
                    }
                });

                if (failed.Any())
                {
                    logger.Error("Establishment Codes could not be converted to int: {0}", string.Join(",", failed));
                }

                if (succeeded.Any())
                {
                    searchDetails.PropertyRequests = succeeded.Select(establishment => new PropertyRequest()
                    {
                        PropertyID = establishment
                    }).ToArray();
                }
                else
                {
                    //throw exception if there were no successfully converted establishment codes
                    throw new ArgumentException("Please set a Destionation or Establishment Code");
                }
            }
            else
            {
                throw new ArgumentException("Please set a Destionation or Establishment Code");
            }

            searchDetails.RoomRequests = CreateRoomRequests(request.Rooms);

            supplierRequest.LoginDetails = new LoginDetails()
            {
                Login = userName,
                Password = password
            };
            supplierRequest.SearchDetails = searchDetails;

            return supplierRequest;
        }

        private RoomRequest[] CreateRoomRequests(AccommodationProviderAvailabilityRequestRoom[] rooms)
        {
            List<RoomRequest> supplieRoomRequests = new List<RoomRequest>();
            int seq = 0;
            foreach (var room in rooms)
            {
                seq++;
                RoomRequest supplierRoom = new RoomRequest()
                {
                    Adults = room.Guests.AdultsCount,
                    Children = room.Guests.ChildrenCount,
                    Infants = room.Guests.InfantsCount,
                    Seq = seq,
                };


                var ages = from AccommodationProviderAvailabilityRequestGuest guest in room.Guests
                           where guest.Type == GuestType.Child
                           select new ChildAge()
                           {
                               Age = guest.Age
                           };


                if (room.Guests.ChildrenCount > 0 && ages.Any())
                    supplierRoom.ChildAges = ages.ToArray();

                supplieRoomRequests.Add(supplierRoom);
            }

            return supplieRoomRequests.ToArray();
        }

        private Resort[] ConvertToResort(string[] destinationCodes)
        {
            List<Resort> resorts = new List<Resort>();
            foreach (string destination in destinationCodes)
            {
                var resort = new Resort()
                {

                    ResortID = Convert.ToInt32(destination.Split(';')[1])
                };

                resorts.Add(resort);
            }

            return resorts.ToArray();
        }

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;


        }

    }
}
