﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsProviderValuationRequestFactory : ILowCostBedsProviderValuationRequestFactory
    {
        private readonly ILowCostBedsParameterService parameterService;
        private const string LowCostBedsDefaultLanguage = "LowCostBedsDefaultLanguage";

        public LowCostBedsProviderValuationRequestFactory(ILowCostBedsParameterService parameterService)
        {
            this.parameterService = parameterService;
        }

        /// <summary>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PreBookRequest CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {   
            string userName, password;
            parameterService.GetCredentials(request.ChannelInfo, request.Provider, out userName, out password);

            var availabilityResult = request.SelectedRooms.First().AvailabilityResult;

            
            PreBookRequest supplierRequest = new PreBookRequest();
           
            PreBookDetails preBookDetails = new PreBookDetails();
            preBookDetails.ArrivalDate = availabilityResult.CheckInDate;
            preBookDetails.Duration = CalculateDuration(request.SelectedRooms.First().AvailabilityRequest);
            preBookDetails.PropertyID = Convert.ToInt32(availabilityResult.EstablishmentEdiCode);

            
            preBookDetails.RoomBookings = CreateRoomRequests(request.SelectedRooms);
            
            supplierRequest.PreBookDetails = preBookDetails;
            supplierRequest.LoginDetails = new LoginDetails()
            {
                Login = userName,
                Password = password
            };
            
            

            return supplierRequest;
        }

        private PreBook_RoomBooking[] CreateRoomRequests(AccommodationProviderValuationRequestRoom[] rooms)
        {
            List<PreBook_RoomBooking> supplieRoomRequests = new List<PreBook_RoomBooking>();

            foreach (var room in rooms)
            {
                PreBook_RoomBooking supplierRoom = new PreBook_RoomBooking()
                {
                    Adults = room.Guests.AdultsCount,
                    Children = room.Guests.ChildrenCount,
                    Infants = room.Guests.InfantsCount,
                    PropertyRoomTypeID = Convert.ToInt32(room.AvailabilityResult.RoomCode),
                    MealBasisID = Convert.ToInt32(room.AvailabilityResult.BoardCode)

                };


                var ages = from AccommodationProviderAvailabilityRequestGuest guest in room.Guests
                           where guest.Type == GuestType.Child
                           select new ChildAge()
                           {
                               Age = guest.Age
                           };


                if (room.Guests.ChildrenCount > 0 && ages.Any())
                    supplierRoom.ChildAges = ages.ToArray();

                supplieRoomRequests.Add(supplierRoom);
            }

            return supplieRoomRequests.ToArray();
        }

        private Resort[] ConvertToResort(string[] destinationCodes)
        {
            List<Resort> resorts = new List<Resort>();
            foreach (string destination in destinationCodes)
            {
                var resort = new Resort()
                {

                    ResortID = Convert.ToInt32(destination.Split(';')[1])
                };

                resorts.Add(resort);
            }

            return resorts.ToArray();
        }

        private int CalculateDuration(AccommodationProviderAvailabilityRequest request)
        {
            TimeSpan duration = request.CheckOutDate - request.CheckInDate;

            return (int)duration.TotalDays;


        }
    }
}
