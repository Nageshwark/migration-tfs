﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds.Factories
{
    public class LowCostBedsCancellationRequestFactory : ILowCostBedsProviderCancellationRequestFactory
    {
        private readonly ILowCostBedsParameterService parameterService;
        

        public LowCostBedsCancellationRequestFactory(ILowCostBedsParameterService parameterService)
        {
            this.parameterService = parameterService;
        }

        public CancelRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            string login, password;
            parameterService.GetCredentials(request.ChannelInfo, request.Provider, out login, out password);

            var supplierRequest = new CancelRequest
            {
                LoginDetails = new LoginDetails
                {
                    Login = login,
                    Password = password
                },
                BookingReference = request.ProviderBookingReference
            };

            return supplierRequest;
        }
    }
}
