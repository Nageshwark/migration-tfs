﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsParameterService : ILowCostBedsParameterService
    {
        private const string LowCostBedsLogin_GBP = "LowCostBedsLogin_GBP";
        private const string LowCostBedsPassword_GBP = "LowCostBedsPassword_GBP";
        //private const string LowCostBedsLogin_EUR = "LowCostBedsLogin_EUR";
        //private const string LowCostBedsPassword_EUR = "LowCostBedsPassword_EUR";
        private const string LowCostBedsLogin_USD = "LowCostBedsLogin_USD";
        private const string LowCostBedsPassword_USD = "LowCostBedsPassword_USD";

        /// <summary>
        /// Gets currency based on the channel
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public string GetCurrency(Channel channel)
        {
            //No longer using EUR
            switch (channel)
            {
                case Channel.AlphaRoomsUS:
                case Channel.BetaBedsUS:
                    return "USD";
                default:
                    return "GBP";
            }
        }

        /// <summary>
        /// Gets API credentials based on channel
        /// </summary>
        /// <param name="channelInfo"></param>
        /// <param name="provider"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void GetCredentials(ChannelInfo channelInfo, AccommodationProvider provider, out string userName, out string password)
        {
            switch (channelInfo.Channel)
            {
                case Channel.AlphaRoomsUS:
                case Channel.BetaBedsUS:
                    userName = provider.Parameters.GetParameterValue(LowCostBedsLogin_USD);
                    password = provider.Parameters.GetParameterValue(LowCostBedsPassword_USD);
                    break;
                default://For IE and GBP we need to use GBP as well
                    userName = provider.Parameters.GetParameterValue(LowCostBedsLogin_GBP);
                    password = provider.Parameters.GetParameterValue(LowCostBedsPassword_GBP);
                    break;
            }
        }
    }
}