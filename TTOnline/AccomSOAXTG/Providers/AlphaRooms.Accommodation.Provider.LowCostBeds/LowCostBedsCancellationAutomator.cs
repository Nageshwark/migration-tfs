﻿using System;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsCancellationAutomator : IAccommodationCancellationAutomatorAsync<LowCostBedsCancellationResponse>
    {
        private const string LowCostBedsCancellationUrl = "LowCostBedsCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        //private const string LowCostBedsUsername = "LowCostBedsUsername";
        //private const string LowCostBedsPassword = "LowCostBedsPassword";

        
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ILowCostBedsProviderCancellationRequestFactory lowCostBedsProviderCancellationRequestFactory;
        //private readonly IProviderOutputLogger outputLogger;

        public LowCostBedsCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            ILowCostBedsProviderCancellationRequestFactory LowCostBedsSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.lowCostBedsProviderCancellationRequestFactory = LowCostBedsSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<LowCostBedsCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            LowCostBedsCancellationResponse response = null;

            try
            {
                var supplierRequest = lowCostBedsProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                
                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                string url = request.Provider.Parameters.GetParameterValue(LowCostBedsCancellationUrl);

                wbBook client = new wbBook();
                client.Url = url;
                

                CancelResponse supplierResponse = client.Cancel(supplierRequest);
            
                

                response = new LowCostBedsCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                return response;
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("LowCostBedsBookingAutomator.GetCancellationResponseAsync: LowCostBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),ex);
            }
        }

        

        
    }
}
