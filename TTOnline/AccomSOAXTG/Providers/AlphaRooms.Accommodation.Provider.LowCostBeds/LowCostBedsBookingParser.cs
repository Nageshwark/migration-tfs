﻿using System.Linq;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsBookingParser : IAccommodationBookingParser<IEnumerable<LowCostBedsBookingResponse>>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public LowCostBedsBookingParser(ILogger logger
            //IAccommodationConfigurationManager configurationManager
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, IEnumerable<LowCostBedsBookingResponse> responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                Sync.ParallelForEachIgnoreFailed(responses, (response) => CreateBookingResultsFromResponse(request, response, bookingResults));

                // DEBUG Code ONLY:
                //foreach (var response in responses)
                //{
                //    CreateBookingResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, bookingResults);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("LowCostBeds Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                        LowCostBedsBookingResponse bookingResponse,
                                                        ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (bookingResponse == null)
            {
                string message = string.Format("LowCostBeds Booking Parser: Booking Response cannot be parsed because it is null. Booking Request Id: {0}", request.BookingId);

                // Log the error to the event log
                logger.Error(message);

                // Throw an exception. This will be logged in the availability log in the db.
                throw new SupplierApiException(message);
            }



            // Debug Code
            if (request.Debugging)
            {
                string serialisedResponse = bookingResponse.XmlSerialize();
            }

            // Check that no error was included in the response - if an error is returned, log the error in the event log, but DO NOT throw an exception. Instead, return a failed booking.
            if (bookingResponse !=null && bookingResponse.SupplierResponse!=null && bookingResponse.SupplierResponse.ReturnStatus.Success)
            {
                // NOTE: This will process a single room booking ONLY. It is not designed to handle multiple room bookings.
                CreateAvailabilityResultFromResponse(request, bookingResponse, bookingResults);
            }
            else
            {
                
                var sb = new StringBuilder();
                sb.AppendLine("LowCostBeds Response Error.");
                sb.AppendLine("LowCostBeds returned one or more errors in their booking response:");
                sb.AppendLine("Booking Request:");
                sb.AppendLine(bookingResponse.XmlSerialize());
                sb.AppendLine();

                if (!string.IsNullOrEmpty(bookingResponse.SupplierResponse.ReturnStatus.Exception))
                {
                    sb.AppendFormat("Error:{0}", bookingResponse.SupplierResponse.ReturnStatus.Exception);
                    sb.AppendLine();
                }

                // Log the error to the event log
                if (logger != null)
                    logger.Error(sb.ToString());

                // Create a failed booking result
                var failedResult = new AccommodationProviderBookingResult()
                {
                    BookingStatus = BookingStatus.NotConfirmed,
                    Message = sb.ToString()
                };

                bookingResults.Enqueue(failedResult);

            }
        }

        /// <summary>
        /// Note that even though this method populates a list of AccommodationProviderBookingResult objects, it will actually only ever create a single result.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="bookingResponse"></param>
        /// <param name="bookingResults"></param>
        private void CreateAvailabilityResultFromResponse(AccommodationProviderBookingRequest request,
                                                            LowCostBedsBookingResponse bookingResponse,
                                                            ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            AccommodationProviderBookingResult result = new AccommodationProviderBookingResult();
            result.RoomNumber = request.ValuatedRooms[0].RoomNumber;
            try
            {
                
                result.ProviderBookingReference = bookingResponse.SupplierResponse.BookingReference;

                result.ProviderSpecificData = new Dictionary<string, string>();
                result.ProviderSpecificData.Add("TradeReference", bookingResponse.SupplierResponse.TradeReference);
                result.ProviderSpecificData.Add("TotalPrice", bookingResponse.SupplierResponse.TotalPrice.ToString());
                result.ProviderSpecificData.Add("TotalPaid", bookingResponse.SupplierResponse.TotalPaid.ToString());
                result.ProviderSpecificData.Add("TotalCommission", bookingResponse.SupplierResponse.TotalCommission.ToString());
                result.KeyCollectionInformation = bookingResponse.KeyCollectionInformation;

                result.BookingStatus = BookingStatus.Confirmed;
                 
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the booking response from LowCostBeds.");
                sb.AppendLine("Booking Request Id = " + request.BookingId);
                sb.AppendLine("Booking Response:");
                sb.AppendLine(bookingResponse.SupplierResponse.XmlSerialize());
                sb.AppendLine();
                

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Enqueue(result);
        }
    }
}
