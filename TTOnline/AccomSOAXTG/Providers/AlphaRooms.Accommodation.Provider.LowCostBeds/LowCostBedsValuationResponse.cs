﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsValuationResponse
    {
        public PreBookRequest SupplierRequest { get; set; }
        public PreBookResponse SupplierResponse { get; set; }
    }
}
