﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management.Instrumentation;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsAvailabilityParser : IAccommodationAvailabilityParser<LowCostBedsAvailabilityResponse>
    {

        private readonly ILogger logger;
        private readonly ILowCostBedsParameterService parameterService;
        private readonly IProviderNonRefundableService nonRefundableService;
        //private readonly IAccommodationConfigurationManager configurationManager;
        
        public LowCostBedsAvailabilityParser(ILogger logger, ILowCostBedsParameterService parameterService, IProviderNonRefundableService nonRefundableService)
            //IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.parameterService = parameterService;
            this.nonRefundableService = nonRefundableService;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request, LowCostBedsAvailabilityResponse responses)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                CreateAvailabilityResultsFromResponse(request, responses.SupplierResponse, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "LowCostBeds Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
            SearchResponse supplierResponse,
            ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.PropertyResults != null &&
                               supplierResponse.PropertyResults.Any());

            if (hasResults)
            {
                Parallel.ForEach(supplierResponse.PropertyResults, propertyList =>
                {
                    Sync.ParallelForEachIgnoreFailed(propertyList.RoomTypes, roomType =>
                    {
                        var result = new AccommodationProviderAvailabilityResult();

                        //result.Id = new Guid();
                        result.RoomNumber = (byte) roomType.Seq;

                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.SupplierEdiCode = request.Provider.EdiCode;
                        
                        result.PaymentModel = PaymentModel.PostPayment;

                        result.DestinationEdiCode = propertyList.Resort;

                        result.EstablishmentEdiCode = propertyList.PropertyID.ToString();
                        result.EstablishmentName = propertyList.PropertyName;

                        result.RoomDescription = roomType.RoomType1;

                        result.RoomCode = roomType.PropertyRoomTypeID.ToString();

                        result.CheckInDate = request.CheckInDate;
                        result.CheckOutDate = request.CheckOutDate;
                        result.Adults = request.Rooms[roomType.Seq - 1].Guests.AdultsCount;
                        result.Children = request.Rooms[roomType.Seq - 1].Guests.ChildrenCount;
                        result.Infants = request.Rooms[roomType.Seq - 1].Guests.InfantsCount;

                        result.BoardCode = roomType.MealBasisID.ToString();
                        result.BoardDescription = roomType.MealBasis;

                        result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(roomType.RoomType1, roomType.MealBasis, roomType.NonRefundableRates);


                        result.RateType  = RateType.NetStandard; 



                        result.SalePrice = new Money()
                        {
                            Amount = roomType.Total,
                            CurrencyCode = parameterService.GetCurrency(request.ChannelInfo.Channel)
                        };

                        //what should we populate here? there is only one price in H4U response.
                        result.CostPrice = result.SalePrice;


                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("Discount", roomType.Discount.ToString());
                       
                        if (!string.IsNullOrEmpty(roomType.RoomView))
                            result.ProviderSpecificData.Add("RoomView" , roomType.RoomView);

                        availabilityResults.Enqueue(result);

                    });
                });

                //int i = 0;




            }
            //else
            //{
                
            //    var sbError = new StringBuilder();

            //    sbError.AppendLine("LowCostBeds Availability Parser: Availability Response cannot be parsed because it is null.");

            //    if (supplierResponse!=null && supplierResponse.ReturnStatus !=null && !string.IsNullOrEmpty( supplierResponse.ReturnStatus.Exception))
            //    {
            //        sbError.AppendLine("Details:" + supplierResponse.ReturnStatus.Exception);
            //    }



            //    throw new SupplierApiDataException(sbError.ToString());
            //}



        }

    }
}
