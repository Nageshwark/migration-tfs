﻿using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsCancellationResponse
    {
        public CancelRequest SupplierRequest { get; set; }
        public CancelResponse SupplierResponse { get; set; }
    }
}
