﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    /// <summary>
    /// Combines the supplier request with the supplier response.
    /// Used to maintain the linkage between a request and its response between the automator and the parser.
    /// Note that LowCostBeds accepts and returns XML strings.
    /// </summary>
    public class LowCostBedsAvailabilityResponse
    {
        public SearchRequest SupplierRequest { get; set; }
        public List<AccommodationProviderAvailabilityRequestRoom> Rooms { get; set; }
        public SearchResponse SupplierResponse { get; set; }
    }
}
