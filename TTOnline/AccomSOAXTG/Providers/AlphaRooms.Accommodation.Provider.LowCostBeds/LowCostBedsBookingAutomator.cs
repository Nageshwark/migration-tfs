﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds;
using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsBookingAutomator : IAccommodationBookingAutomatorAsync<IEnumerable<LowCostBedsBookingResponse>>
    {
        private const string LowCostBedsBookingUrl = "LowCostBedsBookingUrl";
        //private const string LowCostBedsUsername = "LowCostBedsUsername";
        //private const string LowCostBedsPassword = "LowCostBedsPassword";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly ILowCostBedsProviderBookingRequestFactory lowCostBedsProviderBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;

        public LowCostBedsBookingAutomator(   ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ILowCostBedsProviderBookingRequestFactory lowCostBedsSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger,
                                            IProviderLoggerService loggerService
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this.lowCostBedsProviderBookingRequestFactory = lowCostBedsSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
        }

        public async Task<IEnumerable<LowCostBedsBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            var responses = new List<LowCostBedsBookingResponse>();
            LowCostBedsBookingResponse response = null;

          
            try
            {
                var supplierRequest = this.lowCostBedsProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingInitiateResponseAsync(request, supplierRequest, false);

                var bookingRequest = this.lowCostBedsProviderBookingRequestFactory.CreateBookingSearchRequest(request, response.SupplierResponse.BookingReference);
                string bookingDetails = await GetSupplierBookingDetailsAsync(request, bookingRequest);
                response.KeyCollectionInformation = bookingDetails;

                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.lowCostBedsProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingInitiateResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("LowCostBedsBookingAutomator.GetBookingResponseAsync: LowCostBeds supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }
                
            return responses;
        }

        private async Task<LowCostBedsBookingResponse> GetSupplierBookingInitiateResponseAsync(AccommodationProviderBookingRequest request, BookRequest supplierRequest, bool isCommit)
        {
            string url = request.Provider.Parameters.GetParameterValue(LowCostBedsBookingUrl);
            string serialisedRequest = supplierRequest.XmlSerialize();


            wbBook client = new wbBook();
            client.Url = url;

            this.loggerService.SetProviderRequest(request, supplierRequest);
            BookResponse response = client.Book(supplierRequest);
            this.loggerService.SetProviderResponse(request, response);
            this.outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, response);

            var supplierResponse = new LowCostBedsBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;

        }

        private async Task<string> GetSupplierBookingDetailsAsync(AccommodationProviderBookingRequest request, BookingRequest bookingRequest)
        {
            string bookingDetails = string.Empty;
            string url = request.Provider.Parameters.GetParameterValue(LowCostBedsBookingUrl);
            wbBook client = new wbBook();
            client.Url = url;
            this.loggerService.SetProviderRequest(request, bookingRequest);
            Booking response = client.BookingDetails(bookingRequest);

            if (response != null && response.BookingReference == bookingRequest.BookingReference)
            {
                var errata = response.Properties[0].Errata.ToList();
                bookingDetails = errata.Aggregate(bookingDetails, (current, erratum) => current + erratum.Subject + ": " + erratum.Description + ". ");

                //foreach (var erratum in errata)
                //{
                //    bookingDetails = bookingDetails + erratum.Subject + ": " + erratum.Description + ". ";
                //}
            }

            return bookingDetails;
        }
        
    }
}
