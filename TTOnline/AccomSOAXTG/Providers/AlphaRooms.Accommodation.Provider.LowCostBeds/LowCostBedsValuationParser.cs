﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.LowCostBeds;

using AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{

    public class LowCostBedsValuationParser : IAccommodationValuationParser<LowCostBedsValuationResponse>
    {
        private readonly ILogger logger;
        private readonly ILowCostBedsParameterService parameterService;
        private readonly IProviderNonRefundableService nonRefundableService;

        //private readonly IAccommodationConfigurationManager configurationManager;

        public LowCostBedsValuationParser(ILogger logger, ILowCostBedsParameterService parameterService, IProviderNonRefundableService nonRefundableService)
        {
            this.logger = logger;
            this.parameterService = parameterService;
            this.nonRefundableService = nonRefundableService;
            //this.configurationManager = configurationManager;
        }


        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, LowCostBedsValuationResponse responses)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                CreateValuationResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, valuationResults);
               
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("LowCostBeds Supplier Data exception in Valuation Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationResultsFromResponse(AccommodationProviderValuationRequest request, PreBookRequest supplierRequest, PreBookResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.RoomBookings != null && supplierResponse.RoomBookings.Any());
            //var commonHelper = new CommonHelper();

            if (hasResults)
            {
                var availResult = request.SelectedRooms.First().AvailabilityResult;

                //Sync.ParallelForEachIgnoreFailed(supplierResponse.RoomBookings, roomType =>
                foreach (var roomType in supplierResponse.RoomBookings)
                {
                    var result = new AccommodationProviderValuationResult();

                    //result.Id = new Guid();
                    result.RoomNumber = (byte) roomType.Sequence;

                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.SupplierEdiCode = request.Provider.EdiCode;

                    result.PaymentModel = PaymentModel.PostPayment;

                    result.DestinationEdiCode = availResult.DestinationEdiCode;
                    result.EstablishmentEdiCode = availResult.EstablishmentEdiCode;
                    result.EstablishmentName = availResult.EstablishmentName;

                    result.RoomDescription = roomType.RoomType;

                    result.RoomCode = roomType.PropertyRoomTypeID.ToString();

                    result.CheckInDate = availResult.CheckInDate;
                    result.CheckOutDate = availResult.CheckOutDate;
                    result.Adults = request.SelectedRooms[roomType.Sequence - 1].Guests.AdultsCount;
                    result.Children = request.SelectedRooms[roomType.Sequence - 1].Guests.ChildrenCount;
                    result.Infants = request.SelectedRooms[roomType.Sequence - 1].Guests.InfantsCount;

                    result.BoardCode = roomType.MealBasisID.ToString();
                    result.BoardDescription = roomType.MealBasis;

                    result.IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(roomType.RoomType, roomType.MealBasis, roomType.NonRefundableRates);
                    result.CommissionAmount = new Money
                    {
                        Amount = roomType.TotalCommission,
                        CurrencyCode = parameterService.GetCurrency(request.ChannelInfo.Channel)
                    };

                    result.RateType = RateType.NetStandard;

                    result.SalePrice = new Money()
                    {
                        Amount = roomType.TotalPrice,
                        CurrencyCode = parameterService.GetCurrency(request.ChannelInfo.Channel)
                    };

                    //what should we populate here? there is only one price in H4U response.
                    result.CostPrice = result.SalePrice;

                    result.CancellationPolicy = GetCancellationPolicy(request.ChannelInfo, supplierResponse.Cancellations);
                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("VATOnCommission", roomType.VATOnCommission.ToString());
                    result.ProviderSpecificData.Add("TotalPrice", supplierResponse.TotalPrice.ToString());

                    availabilityResults.Enqueue(result);

                    //});
                }
            }
            else
            {
                var  message = new StringBuilder();

                message.AppendLine("LowCostBeds Valuation Parser: Valuation Response cannot be parsed because it is null.");

                if (supplierResponse != null && supplierResponse.ReturnStatus != null)
                    message.AppendLine(string.Format("Details: {0}", supplierResponse.ReturnStatus.Exception));


                throw new SupplierApiDataException(message.ToString());
            }
        }

        private string[] GetCancellationPolicy(ChannelInfo channelInfo, Cancellation[] cancellations)
        {
            if (cancellations == null) return new string[0];
            return cancellations.Select(i => String.Format("From {0:dd/MM/yyyy} {1} penalty {2} {3}", i.StartDate
                , (i.EndDate < new DateTime(2099, 12, 31) ? String.Format("to {0:dd/MM/yyyy}", i.EndDate) : "onwards"), i.Penalty, channelInfo.CurrencyCode)).ToArray();
        }
    }
}
