﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    
    public class LowCostBedsBookingResponse
    {
        public BookRequest SupplierRequest { get; set; }
        public BookResponse SupplierResponse { get; set; }
        public string KeyCollectionInformation { get; set; }
    }
    
}
