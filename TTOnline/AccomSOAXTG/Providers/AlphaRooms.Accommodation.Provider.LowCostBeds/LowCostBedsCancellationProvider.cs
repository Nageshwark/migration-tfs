﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds
{
    public class LowCostBedsCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<LowCostBedsCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<LowCostBedsCancellationResponse> parser;

        public LowCostBedsCancellationProvider(IAccommodationCancellationAutomatorAsync<LowCostBedsCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<LowCostBedsCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
