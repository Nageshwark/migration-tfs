﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;


namespace AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces
{
    public interface ILowCostBedsProviderCancellationRequestFactory
    {
        CancelRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
