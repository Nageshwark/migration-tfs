﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces
{
    public interface ILowCostBedsParameterService
    {
        string GetCurrency(Channel channel);

        void GetCredentials(ChannelInfo channelInfo, AccommodationProvider provider, out string userName, out string password);
    }
}