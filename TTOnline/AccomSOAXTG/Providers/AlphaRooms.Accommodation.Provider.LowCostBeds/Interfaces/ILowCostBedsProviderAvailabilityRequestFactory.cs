﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.LowCostBeds.LCBService;

namespace AlphaRooms.Accommodation.Provider.LowCostBeds.Interfaces
{
    public interface ILowCostBedsProviderAvailabilityRequestFactory
    {
        SearchRequest CreateProviderAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
