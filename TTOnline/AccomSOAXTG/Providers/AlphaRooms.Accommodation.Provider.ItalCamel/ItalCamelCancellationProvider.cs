﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<ItalCamelCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<ItalCamelCancellationResponse> parser;

        public ItalCamelCancellationProvider(IAccommodationCancellationAutomatorAsync<ItalCamelCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<ItalCamelCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
}
