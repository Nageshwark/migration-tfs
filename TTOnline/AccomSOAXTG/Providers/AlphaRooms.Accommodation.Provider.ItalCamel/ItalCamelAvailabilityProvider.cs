﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ItalCamelAvailabilityResponse> _automator;
        private readonly IAccommodationAvailabilityParser<ItalCamelAvailabilityResponse> _parser;

        public ItalCamelAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ItalCamelAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ItalCamelAvailabilityResponse> parser)
        {
            this._automator = automator;
            this._parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await _automator.GetAvailabilityResponseAsync(request);
            return _parser.GetAvailabilityResults(request, responses);
        }
    }
}
