﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelCancellationlationParser : IAccommodationCancellationParserAsync<ItalCamelCancellationResponse>
    {
        public ItalCamelCancellationlationParser()
        {
            
        }


        public async System.Threading.Tasks.Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, ItalCamelCancellationResponse response)
        {
            var cancellationResults = new List<AccommodationProviderCancellationResult>();
            var cancellationResult = new AccommodationProviderCancellationResult();
            cancellationResult.CancellationStatus = response.SupplierResponse.STATUS == 0
                ? CancellationStatus.Succeeded
                : CancellationStatus.Failed;
           cancellationResults.Add(cancellationResult);
            return cancellationResults;
        }

    }

}
