﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ItalCamel;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync <ItalCamelValuationResponse> _automator;
        private readonly IAccommodationValuationParser<ItalCamelValuationResponse> _parser;

        public ItalCamelValuationProvider(IAccommodationValuationAutomatorAsync<ItalCamelValuationResponse> automator,
            IAccommodationValuationParser<ItalCamelValuationResponse> parser)
       {
            this._automator = automator;
            this._parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await _automator.GetValuationResponseAsync(request);
            return _parser.GetValuationResults(request, responses);
        }
    }
}
