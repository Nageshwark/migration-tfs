﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;
using System.ServiceModel;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelBookingAutomator : IAccommodationBookingAutomatorAsync<ItalCamelBookingResponse>
    {
        private const string ItalCamelBookingUrl = "ItalCamelBookingUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UserName = "ItalCamelUserName";
        private const string Password = "ItalCamelpassword";
        private const string LangUid = "ItalCamelLangUid";
        
        private readonly IItalCamelBookingRequestFactory _italCamelSupplierBookingRequestFactory;
        
        public ItalCamelBookingAutomator(IItalCamelBookingRequestFactory italCamelBookingRequestFactory)
        {
            this._italCamelSupplierBookingRequestFactory = italCamelBookingRequestFactory;
        }
        public async Task<ItalCamelBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {

            List<ItalCamelBookingResponse> responses = new List<ItalCamelBookingResponse>();
            ItalCamelBookingResponse response = null;
            try
            {
                XMLPackage40 supplierRequest = this._italCamelSupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
                response = await GetBookingResponseAsync(request, supplierRequest);
                responses.Add(response);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("italCamelBookingAutomator.GetAvailabilityResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        public async Task<ItalCamelBookingResponse> GetBookingResponseAsync(
            AccommodationProviderBookingRequest request, XMLPackage40 supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            Service_4_0SoapClient client = ConfigureSoapCleint(request);
            string username = request.Provider.Parameters.GetParameterValue(UserName);
            string password = request.Provider.Parameters.GetParameterValue(Password);
            string languageuid = request.Provider.Parameters.GetParameterValue(LangUid);
               
            XmlResponsePackage40 res = client.PACKAGEESTIMATE(username, password, languageuid, supplierRequest);
             var supplierResponse = new ItalCamelBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = res
            };
            return supplierResponse;
        
        }
        private Service_4_0SoapClient ConfigureSoapCleint(AccommodationProviderBookingRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ItalCamelBookingUrl)));
            Service_4_0SoapClient client = new Service_4_0SoapClient(binding, address);


            return client;
        }
      
        
    }
}
