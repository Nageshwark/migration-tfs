﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<ItalCamelBookingResponse> _automator;
        private readonly IAccommodationBookingParser<ItalCamelBookingResponse> _parser;

        public ItalCamelBookingProvider(IAccommodationBookingAutomatorAsync<ItalCamelBookingResponse> automator,
                                        IAccommodationBookingParser<ItalCamelBookingResponse> parser)
        {
            this._automator = automator;
            this._parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await _automator.GetBookingResponseAsync(request);
            return _parser.GetBookingResults(request, response);
        }
    }
}
