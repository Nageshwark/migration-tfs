﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;


namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelValuationAutomator : IAccommodationValuationAutomatorAsync<ItalCamelValuationResponse>
    {
        private const string ItalCamelValuationUrl = "ItalCamelValuationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private const string UserName = "ItalCamelUserName";
        private const string Password = "ItalCamelpassword";
        private const string LangUid = "ItalCamelLangUid";

        private readonly IItalCamelProviderValuationRequestFactory _italCamelProviderValuationRequestFactory;

        public ItalCamelValuationAutomator(IItalCamelProviderValuationRequestFactory italCamelSupplierValuationRequestFactory)
        {
            this._italCamelProviderValuationRequestFactory = italCamelSupplierValuationRequestFactory;
            
        }

        public async Task<ItalCamelValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ItalCamelValuationResponse response = null;

            try
            {
                XMLPackage40 supplierRequest = this._italCamelProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ItalCamelValuationAutomator.GetValuationResponseAsync: MTS supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ItalCamelValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, XMLPackage40 supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            Service_4_0SoapClient client = ConfigureSoapCleint(request);
            string username = request.Provider.Parameters.GetParameterValue(UserName);
            string password = request.Provider.Parameters.GetParameterValue(Password);
            string languageUid = request.Provider.Parameters.GetParameterValue(LangUid);

            XmlResponsePackage40 response = client.PACKAGEESTIMATE(username, password, languageUid, supplierRequest);

           

            var supplierResponse = new ItalCamelValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response

            };
            return supplierResponse;
          }


        private Service_4_0SoapClient ConfigureSoapCleint(AccommodationProviderValuationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));


            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ItalCamelValuationUrl)));
            Service_4_0SoapClient client = new Service_4_0SoapClient(binding, address);

            return client;
        }
      
    
    }
}
