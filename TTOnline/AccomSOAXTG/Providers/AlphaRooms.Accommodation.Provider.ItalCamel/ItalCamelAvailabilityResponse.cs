﻿using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelAvailabilityResponse
    {
        public XmlSearchInput40 SupplierRequest { get; set; }
        public ResponseSearch40 SupplierResponse { get; set; }
    }
}
