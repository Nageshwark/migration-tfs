﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces
{
    public interface IItalCamelBookingRequestFactory
    {
        XMLPackage40 CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
        
    }
}
