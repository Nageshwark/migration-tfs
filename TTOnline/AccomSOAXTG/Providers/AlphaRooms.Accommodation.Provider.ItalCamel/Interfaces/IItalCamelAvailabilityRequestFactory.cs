﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces
{
    public interface IItalCamelAvailabilityRequestFactory
    {
        XmlSearchInput40 CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
