﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
//using AlphaRooms.Accommodation.Provider.ItalCamel.Service.Request;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces
{
    public interface IItalCamelProviderCancellationRequestFactory
    {
        CancelBookingRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
