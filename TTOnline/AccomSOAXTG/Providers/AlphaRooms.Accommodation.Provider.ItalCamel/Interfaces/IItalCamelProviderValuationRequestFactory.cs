﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces
{
    public interface IItalCamelProviderValuationRequestFactory 
    {
        XMLPackage40 CreateSupplierValuationRequest(AccommodationProviderValuationRequest request);
    }
}
