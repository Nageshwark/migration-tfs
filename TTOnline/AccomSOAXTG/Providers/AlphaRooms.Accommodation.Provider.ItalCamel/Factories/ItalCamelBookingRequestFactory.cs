﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Factories
{
    public class ItalCamelBookingRequestFactory : IItalCamelBookingRequestFactory
    {
       
        public XMLPackage40 CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            var bookingrequest = new XMLPackage40
            {
                TYPE = "BOOKING",
                REFERENCENUMBER = request.BookingId.ToString(),
                BOOKING = GetbookingData(request)
            };

            return bookingrequest;
        }

        private  xmlBooking40 GetbookingData(AccommodationProviderBookingRequest request)
        {
            var valuatedRoom = request.ValuatedRooms.First();

            var bookingrequestdata = new xmlBooking40
            {
                TYPE = "BOOKING",
                NOTES = "",
                //AVAILABILITYTOKEN = request.Provider.,
                CHECKIN = valuatedRoom.ValuationResult.CheckInDate,
                CHECKOUT = valuatedRoom.ValuationResult.CheckOutDate,
                ACCOMMODATIONUID = valuatedRoom.ValuationResult.EstablishmentEdiCode,
                //REQUESTEDPRICE = valuatedRoom.ValuationResult.CostPrice.Amount,
                ROOMS = GetRoom(request)
            };
            return bookingrequestdata;
        }

        private xmlBookingRoom40[] GetRoom(AccommodationProviderBookingRequest roomRequest)
        {
            var lstroom = new List<xmlBookingRoom40>();
            var lstpax = new List<xmlBookingPax>();

            foreach (var room in roomRequest.ValuatedRooms)
            {
                var bookingRoom = new xmlBookingRoom40
                {
                    MASTERUID = room.ValuationResult.RoomCode,
                    ISTWIN = false,
                    PASSENGERS = GetPax(room),
                    BOARD = new xmlBookingBoard
                    {
                        UID = room.ValuationResult.ProviderSpecificData["BoardUid"]
                    }

                };
                lstroom.Add(bookingRoom);
            }

            return lstroom.ToArray();
        }

        private xmlBookingPax[] GetPax(AccommodationProviderBookingRequestRoom room)
        {
            var lstpax = new List<xmlBookingPax>();


            foreach (var guest in room.Guests)
            {
                var bookingpax = new xmlBookingPax
                {
                    NAME = "",
                    SURNAME = "",
                    BIRTHDATE = guest.Type == GuestType.Adult ? DateTime.MinValue : DateTime.Now.AddYears(-guest.Age)
                };
                lstpax.Add(bookingpax);
            }

            return lstpax.ToArray();
        }

    }
}
