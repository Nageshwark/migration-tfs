﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ItalCamel.Factories
{
    public class ItalCamelProviderValuationRequestFactory : IItalCamelProviderValuationRequestFactory 
    {
        private const string UserName = "ItalCamelUserName";
        private const string Password = "ItalCamelpassword";
        private const string LangUid = "ItalCamelLangUid";
        private const string incPriceBreakdown = "incPriceBreakdown";
        private const string incTransferInfo = "incTransferInfo";
        private const string immediateConfirmation = "immediateConfirmation";
        private const string includeNearlyCities = "includeNearlyCities";

        public XMLPackage40 CreateSupplierValuationRequest(AccommodationProviderValuationRequest request)
        {
            var availRequest = request.SelectedRooms.First().AvailabilityRequest;

            var supplierRequest = new XMLPackage40
            {
                TYPE = "ESTIMATE",
                REFERENCENUMBER = Guid.NewGuid().ToString(),
                BOOKING = GetbookingData(request)
            };

            return supplierRequest;
        }
      

        protected xmlBooking40 GetbookingData(AccommodationProviderValuationRequest request)
        {
            var valuatedRoom = request.SelectedRooms.First();

            var bookingrequestdata = new xmlBooking40
            {
                TYPE = "ESTIMATE",
                NOTES = Guid.NewGuid().ToString(),
                CHECKIN = valuatedRoom.AvailabilityResult.CheckInDate,
                CHECKOUT = valuatedRoom.AvailabilityResult.CheckOutDate,
                ACCOMMODATIONUID = valuatedRoom.AvailabilityResult.EstablishmentEdiCode,
                //REQUESTEDPRICE = request.SelectedRooms.Sum(x=>x.AvailabilityResult.SalePrice.Amount),
                ROOMS = GetRoom(request)
            };
            return bookingrequestdata;
        }

        private xmlBookingRoom40[] GetRoom(AccommodationProviderValuationRequest request)
        {
            var lstroom = new List<xmlBookingRoom40>();

            foreach (var room in request.SelectedRooms)
            {
                var bookingRoom = new xmlBookingRoom40
                {
                    MASTERUID = room.AvailabilityResult.RoomCode,
                    ISTWIN = false,
                    PASSENGERS = GetPax(room),
                    BOARD = new xmlBookingBoard
                    {
                        UID = room.AvailabilityResult.ProviderSpecificData["BoardUid"]
                    }

                };
                lstroom.Add(bookingRoom);
            }

            return lstroom.ToArray();
        }

        private xmlBookingPax[] GetPax(AccommodationProviderValuationRequestRoom room)
        {
            var lstpax = new List<xmlBookingPax>();


            foreach (var guest in room.Guests)
            {
                var bookingpax = new xmlBookingPax
                {
                    NAME = "",
                    SURNAME = "",
                    BIRTHDATE = guest.Type == GuestType.Adult ? DateTime.MinValue : DateTime.Now.AddYears(-guest.Age) 
                };
                lstpax.Add(bookingpax);
            }

            return lstpax.ToArray();
        }

    }
}
