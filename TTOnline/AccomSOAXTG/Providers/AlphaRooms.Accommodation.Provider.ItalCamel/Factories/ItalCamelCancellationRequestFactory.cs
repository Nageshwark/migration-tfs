﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
//using AlphaRooms.Accommodation.Provider.ItalCamel.Service.Request;


namespace AlphaRooms.Accommodation.Provider.ItalCamel.Factories
{
    public class ItalCamelCancellationRequestFactory : IItalCamelProviderCancellationRequestFactory
    {
        private const string UserName = "ItalCamelUserName";
        private const string Password = "ItalCamelpassword";
        private const string LangUid = "ItalCamelLangUid";
      
        public CancelBookingRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            var cancelBookingRequest = new CancelBookingRequest()
            {
                Username = request.Provider.Parameters.GetParameterValue(UserName),
                Password = request.Provider.Parameters.GetParameterValue(Password),
                Languageid = request.Provider.Parameters.GetParameterValue(LangUid),
                ItemUid = request.ProviderBookingReference
            };

            return cancelBookingRequest;
        }
    }
    
}

