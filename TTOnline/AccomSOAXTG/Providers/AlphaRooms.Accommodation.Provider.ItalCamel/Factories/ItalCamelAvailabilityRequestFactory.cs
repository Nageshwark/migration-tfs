﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using System.Collections.Generic;



namespace AlphaRooms.Accommodation.Provider.ItalCamel.Factories
{
    public class ItalCamelAvailabilityRequestFactory : IItalCamelAvailabilityRequestFactory
    {
        private const string ItalCamelUserName = "ItalCamelUserName";
        private const string ItalCamelPassword = "ItalCamelpassword";
        private const string ItalCamelLangUid = "ItalCamelLangUid";
        private const string ItalCamelTransferInfo = "ItalCamelTransferInfo";
        private const string ItalCamelImmediateConfirmation = "ItalCamelImmediateConfirmation";
        private const string ItalCamelIncludeNearlyCities = "ItalCamelIncludeNearlyCities";
        public XmlSearchInput40 CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {

            var availableRequest = new XmlSearchInput40
            {
                USERNAME = request.Provider.Parameters.GetParameterValue(ItalCamelUserName),
                PASSWORD = request.Provider.Parameters.GetParameterValue(ItalCamelPassword),
                LANGUAGEUID = request.Provider.Parameters.GetParameterValue(ItalCamelLangUid),
                CHECKIN = request.CheckInDate.ToUniversalTime().Date,
                CHECKOUT = request.CheckOutDate.ToUniversalTime().Date,
               
                //CITIES = request.DestinationCodes,
                ACCOMMODATIONS = request.EstablishmentCodes,
                ROOMS = GetPax(request),
                IMMEDIATE_CONFIRMATION_ONLY = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(ItalCamelImmediateConfirmation)),
                INCLUDE_TRANSFER_INFO = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(ItalCamelTransferInfo)),
                INCLUDE_NEARLY_CITIES = Convert.ToBoolean(request.Provider.Parameters.GetParameterValue(ItalCamelIncludeNearlyCities)),
                INCLUDE_PRICE_BREAKDOWN = true
            };
            if (request.DestinationCodes.Any())
            {
                availableRequest.CITIES = request.DestinationCodes;
            }
            else
            {
                availableRequest.ACCOMMODATIONS = request.EstablishmentCodes;
            }
            return availableRequest;
        }

        private xmlSearchInputRoom40[] GetPax(AccommodationProviderAvailabilityRequest request)
        {
            List<xmlSearchInputRoom40> lstroom=new List<xmlSearchInputRoom40>();
            foreach (var room in request.Rooms)
            {
                var roomRequest = new xmlSearchInputRoom40
                {
                    ADULTS = room.Guests.AdultsCount,
                    CHILDREN = room.Guests.ChildrenAndInfantsCount,
                    CHILDAGE1 = room.Guests.ChildAndInfantAges.FirstOrDefault(),
                    CHILDAGE2 = room.Guests.ChildrenAndInfantsCount>1 ? room.Guests.ChildAndInfantAges[1]:0
                };
                lstroom.Add(roomRequest);
            }

            return lstroom.ToArray();
        }


    }

}

