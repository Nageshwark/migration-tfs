﻿using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelBookingResponse
    {
        public XMLPackage40 SupplierRequest { get; set; }
        public XmlResponsePackage40 SupplierResponse { get; set; }
    }
}
