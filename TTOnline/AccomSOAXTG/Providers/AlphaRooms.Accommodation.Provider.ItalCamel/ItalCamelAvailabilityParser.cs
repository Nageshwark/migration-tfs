﻿using System;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using System.Collections.Concurrent;
using System.Linq;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelAvailabilityParser : IAccommodationAvailabilityParser<ItalCamelAvailabilityResponse>
    {

        public ItalCamelAvailabilityParser()
        {
            //this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(
            AccommodationProviderAvailabilityRequest request,
            ItalCamelAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse,
                    availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "ItalCamel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request,
            XmlSearchInput40 supplierRequest, ResponseSearch40 supplierResponse,
            ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {

            bool hasResults = supplierResponse.ACCOMMODATIONS.Any();

            if (hasResults)
            {
                foreach (var accommodation in supplierResponse.ACCOMMODATIONS)
                {

                    foreach (var room in accommodation.ROOMS.Where(r => r.AVAILABLE))
                    {
                        int roomNumber = 1;
                        foreach (var roomDetail in room.ROOMDETAILS)
                        {
                            foreach (var board in roomDetail.BOARDS)
                            {
                                var result = new AccommodationProviderAvailabilityResult();

                                result.RoomNumber = (byte) roomNumber;
                                result.ProviderEdiCode = request.Provider.EdiCode;
                                result.SupplierEdiCode = request.Provider.EdiCode;
                                result.EstablishmentName = accommodation.NAME;
                                result.EstablishmentEdiCode = accommodation.UID;
                                result.DestinationEdiCode = accommodation.CITY.UID;
                                result.BoardCode = board.ACRONYM;
                                result.BoardDescription = board.NAME;
                                result.RoomCode = room.MASTERUID;
                                result.RoomDescription = room.NAME;

                                result.SalePrice = new Money()
                                {
                                    Amount = board.AMOUNT,
                                    CurrencyCode = "EUR"

                                };
                                result.CostPrice = result.SalePrice;

                                result.PaymentModel = PaymentModel.PostPayment;
                                result.RateType = RateType.NetStandard;
                                
                                

                                result.Adults = (byte) supplierRequest.ROOMS[roomNumber - 1].ADULTS;
                                result.Children = (byte) supplierRequest.ROOMS[roomNumber - 1].CHILDREN;

                                result.CheckInDate = supplierRequest.CHECKIN;
                                result.CheckOutDate = supplierRequest.CHECKOUT;
                                result.IsNonRefundable = room.NOTREFUNDABLE;
                                result.NumberOfAvailableRooms = room.AVAILCOUNT;

                                result.ProviderSpecificData = new Dictionary<string, string>();
                                result.ProviderSpecificData.Add("BoardUid", board.UID);
                                
                                availabilityResults.Enqueue(result);

                            }
                            roomNumber++;
                        }
                    }
                }
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var error = supplierResponse.ERRORMESSAGE;

                sbMessage.AppendLine(
                    "ItalCamel Availability Parser: Availability Response cannot be parsed because it is null.");

                if (error != null)
                    sbMessage.AppendLine(string.Format("Error Details: {0}", error));


                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }
     }
}