﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;


namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelCancellationAutomator : IAccommodationCancellationAutomatorAsync<ItalCamelCancellationResponse>
    {
        private const string ItalCamelAvailabilityUrl = "ItalCamelAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";
        private readonly IItalCamelProviderCancellationRequestFactory _italCamelProviderCancellationRequestFactory;
        public ItalCamelCancellationAutomator(IItalCamelProviderCancellationRequestFactory ItalCamelSupplierCancellationRequestFactory)
        {
            
            this._italCamelProviderCancellationRequestFactory = ItalCamelSupplierCancellationRequestFactory;
            
        }
        public async Task<ItalCamelCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ItalCamelCancellationResponse SupplierResponse = new ItalCamelCancellationResponse();
            CancelBookingRequest cancelRequest = _italCamelProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

            if (request.Debugging)
            {
                var serialisedRequest = cancelRequest.XmlSerialize();
            }

            Service_4_0SoapClient client = ConfigureSoapCleint(request);
            
            var response = client.PACKAGEDELETE(cancelRequest.Username, cancelRequest.Password, cancelRequest.ItemUid, cancelRequest.Languageid);
            
            var supplierResponse = new ItalCamelCancellationResponse()
            {
                SupplierRequest = cancelRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

        private Service_4_0SoapClient ConfigureSoapCleint(AccommodationProviderCancellationRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ItalCamelAvailabilityUrl)));
            Service_4_0SoapClient client = new Service_4_0SoapClient(binding, address);

            return client;
        }

    }
}