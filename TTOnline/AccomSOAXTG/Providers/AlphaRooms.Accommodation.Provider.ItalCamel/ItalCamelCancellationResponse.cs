﻿using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelCancellationResponse
    {
        public CancelBookingRequest SupplierRequest { get; set; }
        public XmlResponsePackage40 SupplierResponse { get; set; }
    }

    public class CancelBookingRequest
    {
         public string Username { get; set; }
         public string Password { get; set; }
         public string Languageid { get; set; }
         public string ItemUid { get; set; }
        
    }
}
