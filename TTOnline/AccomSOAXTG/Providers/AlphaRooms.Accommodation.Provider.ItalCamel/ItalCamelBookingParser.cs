﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using System.Collections.Concurrent;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelBookingParser : IAccommodationBookingParser<ItalCamelBookingResponse>
    {
       private readonly IAccommodationConfigurationManager _configurationManager;

        public ItalCamelBookingParser()
        {
            this._configurationManager = _configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(
            AccommodationProviderBookingRequest request, ItalCamelBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();
            var result = new AccommodationProviderBookingResult();

            if (responses.SupplierResponse != null &&
                responses.SupplierResponse.STATUS == 0 &&
                responses.SupplierResponse.ERRORCODE == null &&
                responses.SupplierResponse.PACKAGE != null &&
                !string.IsNullOrEmpty(responses.SupplierResponse.PACKAGE.UID)
                )
            {
                result.BookingStatus = BookingStatus.Confirmed;

                result.ProviderBookingReference = responses.SupplierResponse.PACKAGE.UID;
                
            }
            else
            {
                var sbMessage = new StringBuilder();

                sbMessage.AppendLine("NTIncoming Booking Parser: Booking Response cannot be parsed because it is null.");

                if (responses.SupplierResponse != null && !string.IsNullOrEmpty(responses.SupplierResponse.ERRORMESSAGE))
                {
                    sbMessage.AppendLine("Error Details:");
                    sbMessage.AppendLine(string.Format("Code: {0}, Description: {1}",
                        responses.SupplierResponse.ERRORCODE, responses.SupplierResponse.ERRORMESSAGE));
                }

                result.Message = sbMessage.ToString();
                result.BookingStatus = BookingStatus.NotConfirmed;
                
            }

            bookingResults.Enqueue(result);
            return bookingResults;
        }

    }
}
