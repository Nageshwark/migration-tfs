﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using System.Text;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{

    public class ItalCamelValuationParser : IAccommodationValuationParser<ItalCamelValuationResponse>
    {

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(
            AccommodationProviderValuationRequest request, ItalCamelValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message =
                    string.Format(
                        "ItalCamel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine,
                        Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, XMLPackage40 supplierRequest, XmlResponsePackage40 supplierResponse,
            ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            if (request.Debugging)
            {
                string supplierResponseXml = supplierResponse.XmlSerialize();
            }

            bool hasResults = ( supplierResponse.PACKAGE != null &&
                                supplierResponse.PACKAGE.BOOKING !=null &&
                                supplierResponse.PACKAGE.BOOKING.ROOMS !=null &&
                                supplierResponse.PACKAGE.BOOKING.ROOMS.Any() && 
                                string.IsNullOrEmpty(supplierResponse.ERRORMESSAGE)
                               );


            if (hasResults)
            {
                var booking = supplierResponse.PACKAGE.BOOKING;


                foreach (var room in supplierResponse.PACKAGE.BOOKING.ROOMS)
                {
                    var selectedRoom = GetSelectedRoom(request.SelectedRooms, room.MASTERUID, room.BOARD.UID, room.TOTAL_AMOUNT);

                    if (selectedRoom == null)
                        throw new SupplierApiDataException("ItalCamel Valuation Parser: Unable to match the room type and Pax from  the response");

                    var result = new AccommodationProviderValuationResult();

                    result.RoomNumber = (byte) 1;
                    result.ProviderEdiCode = request.Provider.EdiCode;
                    result.SupplierEdiCode = request.Provider.EdiCode;
                    
                    //For some reason they return the Hotel name with Hotel Prefixed in the Valuation response.
                    //as such reading it from the Valuation response.
                    //result.EstablishmentName = booking.ACCOMMODATIONNAME;
                    result.EstablishmentName = selectedRoom.AvailabilityResult.EstablishmentName;

                    result.EstablishmentEdiCode = booking.ACCOMMODATIONUID;
                    result.DestinationEdiCode = booking.CITY.UID;
                    result.BoardCode = selectedRoom.AvailabilityResult.BoardCode;
                    result.BoardDescription = room.BOARD.NAME;
                    result.RoomCode = room.MASTERUID;
                    result.RoomDescription = room.NAME;

                    result.SalePrice = new Money()
                    {
                        Amount = room.TOTAL_AMOUNT,
                        CurrencyCode = request.SelectedRooms.First().AvailabilityResult.SalePrice.CurrencyCode
                    };

                    result.CostPrice = result.SalePrice;
                    //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                    result.Adults = selectedRoom.AvailabilityResult.Adults;
                    result.Children = selectedRoom.AvailabilityResult.Children;

                    result.PaymentModel = selectedRoom.AvailabilityResult.PaymentModel;
                    result.RateType = selectedRoom.AvailabilityResult.RateType;
                    result.IsOpaqueRate = selectedRoom.AvailabilityResult.IsBindingRate;
                    result.IsNonRefundable = selectedRoom.AvailabilityResult.IsBindingRate;

                    result.CheckInDate = booking.CHECKIN;
                    result.CheckOutDate = booking.CHECKOUT;

                    if (room.CANCELLATIONPOLICIES !=null && room.CANCELLATIONPOLICIES.Any())
                        result.CancellationPolicy = new[] { room.CANCELLATIONPOLICIES.First().DESCRIPTION };

                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("BoardUid", selectedRoom.AvailabilityResult.ProviderSpecificData["BoardUid"]);

                    availabilityResults.Enqueue(result);



                }


            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();

                var error = supplierResponse.ERRORMESSAGE;

                sbMessage.AppendLine("ItalCamel Valuation Parser: Valuation Response cannot be parsed because it is null.");

                if (error != null)
                    sbMessage.AppendLine(string.Format("Error Details: {0}", error));


                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }

        private AccommodationProviderValuationRequestRoom GetSelectedRoom(AccommodationProviderValuationRequestRoom[] selectedRooms, string roomCode, string boardCode, decimal amount)
        {
            var room = from r in selectedRooms
                       where r.AvailabilityResult.RoomCode == roomCode && 
                            r.AvailabilityResult.ProviderSpecificData["BoardUid"] == boardCode &&
                            r.AvailabilityResult.SalePrice.Amount == amount
                       select r;


            return room.FirstOrDefault();
        }
        private byte GetPax(GuestType guestType, AccommodationProviderValuationRequestRoom [] selectedRooms, string roomCode, string boardCode)
        {
            var room = from r in selectedRooms
                where r.AvailabilityResult.RoomCode == roomCode && r.AvailabilityResult.ProviderSpecificData["BoardUid"] == boardCode
                select r;

            if (room.Any())
            {
                if (guestType == GuestType.Adult)
                    return room.First().AvailabilityResult.Adults;
                else
                    return (byte)(room.First().AvailabilityResult.Children + room.First().AvailabilityResult.Infants);
            }
            else
                throw new SupplierApiDataException("Unable to calculate Pax details");
        }

        //private string GetCancellationPolicyString(xmlCancellationPolicy40[] cancellationpolicies)
        //{
        //    var sbCancellationPolicy = new StringBuilder();

        //    foreach (var cancellationpolicy in cancellationpolicies)
        //    {
        //        sbCancellationPolicy.AppendLine(string.Format("", cancellationpolicy.))
        //    }
        //}
    }
}