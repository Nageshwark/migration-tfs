﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ItalCamel.Interfaces;
using System.Web;
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;
using AlphaRooms.Utilities.ExtensionMethods;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelAvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<ItalCamelAvailabilityResponse>
    {
        private const string ItalCamelAvailabilityUrl = "ItalCamelAvailabilityUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";

        private readonly IItalCamelAvailabilityRequestFactory _italCamelAvailabilityRequestFactory;


        public ItalCamelAvailabilityAutomator(IItalCamelAvailabilityRequestFactory italCamelAvailabilityRequestFactory)
        {
            _italCamelAvailabilityRequestFactory = italCamelAvailabilityRequestFactory;
        }

        public async Task<ItalCamelAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            ItalCamelAvailabilityResponse response = null;

            try
            {
                //ValidateRequest(request);

                XmlSearchInput40 supplierRequest = this._italCamelAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
                
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("ItalCamelAvailabilityAutomator.GetAvailabilityResponseAsync: ItalCamel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);
                
            }


            return response;
        }

        private async Task<ItalCamelAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, XmlSearchInput40 supplierRequest) 
        {
            if (request.Debugging)
            {
                var serialisedRequest = supplierRequest.XmlSerialize();
            }

            Service_4_0SoapClient client = ConfigureSoapCleint(request);
            ResponseSearch40 response = client.GETAVAILABILITY(supplierRequest);

            var supplierResponse = new ItalCamelAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
 
            };
            return supplierResponse;
        }

        private Service_4_0SoapClient ConfigureSoapCleint(AccommodationProviderAvailabilityRequest request)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Convert.ToInt32(request.Provider.Parameters.GetParameterValue(MaxReceivedMessageSize));
            EndpointAddress address = new EndpointAddress(new Uri(request.Provider.Parameters.GetParameterValue(ItalCamelAvailabilityUrl)));
            Service_4_0SoapClient client = new Service_4_0SoapClient(binding, address);

        
            return client;
        }
      

    }
}
