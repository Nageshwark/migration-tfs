﻿
using AlphaRooms.Accommodation.Provider.ItalCamel.ItalCamelService;

namespace AlphaRooms.Accommodation.Provider.ItalCamel
{
    public class ItalCamelValuationResponse
    {
        public XMLPackage40 SupplierRequest { get; set; }
        public XmlResponsePackage40 SupplierResponse { get; set; }

    }
}
