﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<ViatgesMagonCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<ViatgesMagonCancellationResponse> parser;

        public ViatgesMagonCancellationProvider(IAccommodationCancellationAutomatorAsync<ViatgesMagonCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<ViatgesMagonCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
    
}
