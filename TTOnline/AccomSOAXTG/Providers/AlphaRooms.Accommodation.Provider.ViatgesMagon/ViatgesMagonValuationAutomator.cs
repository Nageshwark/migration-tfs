﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonValuationAutomator : CavalAutomaterBase, IAccommodationValuationAutomatorAsync<ViatgesMagonValuationResponse>
    {  
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalValuationRequestFactory _viatgesMagonProviderValuationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViatgesMagonValuationAutomator(  ILogger logger,
                                                IAccommodationConfigurationManager configurationManager,
                                                ICavalValuationRequestFactory viatgesMagonSupplierValuationRequestFactory,
                                                IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._viatgesMagonProviderValuationRequestFactory = viatgesMagonSupplierValuationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<ViatgesMagonValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            ViatgesMagonValuationResponse response = null;

            try
            {
                ValidateRequest(request);

                var supplierRequest = this._viatgesMagonProviderValuationRequestFactory.CreateSupplierValuationRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder allSupplierRequests = new StringBuilder();
                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}",Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest.XmlSerialize());
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse.XmlSerialize());
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViatgesMagonValuationAutomator.GetValuationResponseAsync: ViatgesMagon supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<ViatgesMagonValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Valuation);

            var response = client.getDetailedValuation(supplierRequest);

            var supplierResponse = new ViatgesMagonValuationResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, _outputLogger);

            return supplierResponse;
        }
    }
}
