﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;


namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonValuationResponse
    {
        public cavalHotelBookingValuationRQ SupplierRequest { get; set; }
        public cavalHotelBookingValuationRS SupplierResponse { get; set; }

    }
}
