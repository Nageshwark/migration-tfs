﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonCancellationParser : IAccommodationCancellationParserAsync<ViatgesMagonCancellationResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public ViatgesMagonCancellationParser(ILogger logger , IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }
        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, ViatgesMagonCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {

                bool hasResults = (response != null && response.SupplierResponse.resultCode == 200);

                if (hasResults)
                {
                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = response.SupplierResponse.message;
                }
                else
                {
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was a problem with the cancellation response from ViatgesMagon.");

                    if (response != null && response.SupplierResponse != null && !string.IsNullOrEmpty(response.SupplierResponse.message))
                        sb.AppendLine(string.Format("Error Details: {0}", response.SupplierResponse.message));

                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);

                    if (response != null)
                    {
                        sb.AppendLine("Cancellation Request:");
                        sb.AppendLine(response.SupplierRequest.XmlSerialize());
                        sb.AppendLine();
                        sb.AppendLine("Cancellation Response:");
                        sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    }

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from ViatgesMagon.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
       
    }
}
