﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViatgesMagon;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<ViatgesMagonValuationResponse> automator;
        private readonly IAccommodationValuationParser<ViatgesMagonValuationResponse> parser;

        public ViatgesMagonValuationProvider(IAccommodationValuationAutomatorAsync<ViatgesMagonValuationResponse> automator,
                                                IAccommodationValuationParser<ViatgesMagonValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
