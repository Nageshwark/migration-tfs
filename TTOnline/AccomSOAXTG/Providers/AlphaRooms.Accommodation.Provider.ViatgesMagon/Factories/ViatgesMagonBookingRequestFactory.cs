﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;
using System.Xml;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonProviderBookingRequestFactory : ICavalBookingRequestFactory
    {
        private const string CavalLogin = "CavalLogin";
        private const string CavalPassword = "CavalPassword";
        private const string CavalLanguage = "CavalLanguage";
        private const string CavalGzip = "CavalGzip";
        private const string CavalAgentId = "CavalAgentId";


        /// <summary>
        /// Note:  Each room to be booked will be handled by a separate PurchaseConfirmRQ request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public cavalHotelBookingConfirmRQ CreateSupplierBookingRequest(AccommodationProviderBookingRequest request)
        {
            var valuationResult = request.ValuatedRooms.First().ValuationResult;
            var availabilityRequest = request.ValuatedRooms.First().AvailabilityRequest;

            var supplierRequest = new cavalHotelBookingConfirmRQ
            {
                gzipResponse = bool.Parse(request.Provider.Parameters.GetParameterValue(CavalGzip)),
                login = request.Provider.Parameters.GetParameterValue(CavalLogin),
                password = request.Provider.Parameters.GetParameterValue(CavalPassword),
                agentId = request.Provider.Parameters.GetParameterValue(CavalAgentId),
                language = request.Provider.Parameters.GetParameterValue(CavalLanguage),
                checkIn = availabilityRequest.CheckInDate.ToString("dd/MM/yyyy"),
                checkOut = availabilityRequest.CheckOutDate.ToString("dd/MM/yyyy"),
                occupations = CreateOccupancy(request.ValuatedRooms),
                establishmentId = valuationResult.EstablishmentEdiCode,
                boardCode = valuationResult.BoardCode,
                titular = string.Format("{0} {1} {2}", request.Customer.TitleString, request.Customer.FirstName, request.Customer.Surname),
                agencyEmail = "",
                agencyReference = Guid.NewGuid().ToString(),    //Todo:Do we pass it in?
                commentForHotel = ""

            };

            return supplierRequest;
        }


        private occupation[] CreateOccupancy(AccommodationProviderBookingRequestRoom[] rooms)
        {
            var availRqOccupations = new List<occupation>();

            foreach (var room in rooms)
            {
                var availOccupation = new occupation
                {
                    adultsPerRoom = room.Guests.AdultsCount,
                    childrenPerRoom = room.Guests.ChildrenCount,
                    childAges = GetChildAges(room.Guests),
                    numberOfRooms = 1,
                    roomCode = room.ValuationResult.RoomCode
                };

                availRqOccupations.Add(availOccupation);
            }

            return availRqOccupations.ToArray();
        }
        private int?[] GetChildAges(AccommodationProviderBookingRequestRoomGuestCollection guests)
        {
            return guests.Where(g => g.Type == GuestType.Child).Select(guest => (int?)guest.Age).ToArray();

        }

    }
}

