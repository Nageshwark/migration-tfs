﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{

    public class ViatgesMagonValuationParser : CavalValuationParserBase, IAccommodationValuationParser<ViatgesMagonValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public ViatgesMagonValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, ViatgesMagonValuationResponse response)
        {
            var valuationResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    string xmlResponse = response.XmlSerialize();
                }

                CreateValuationyResultsFromResponse(request, response.SupplierRequest, response.SupplierResponse, valuationResults);
            }
            catch (System.Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("OswaldArrigo Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return valuationResults;
        }

        private void CreateValuationyResultsFromResponse(AccommodationProviderValuationRequest request, cavalHotelBookingValuationRQ supplierRequest, cavalHotelBookingValuationRS supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = (supplierResponse != null && supplierResponse.resultCode == 200);
            
            if (hasResults)
            {
                int roomNumber = 1;
                
                foreach (var room in supplierResponse.occupations)
                {
                    var result = FillRowDetails(request, supplierResponse, room, roomNumber);

                    roomNumber++;

                    availabilityResults.Enqueue(result);
                }
                

            }
            else
            {
                if (supplierResponse != null && !string.IsNullOrEmpty(supplierResponse.message))
                {
                    var sbMessage = new StringBuilder();
                    sbMessage.AppendLine("ViatgesMagon Availability Parser: Error in Valuation response.");

                    sbMessage.AppendLine(string.Format("Error Details: {0}", supplierResponse.message));

                    throw new SupplierApiDataException(sbMessage.ToString());
                }
            }
           
        }

        
    }
}
