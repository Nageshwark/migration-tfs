﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.ViatgesMagon;
using AlphaRooms.SOACommon.Interfaces;
using Ninject;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<IEnumerable<ViatgesMagonBookingResponse>> automator;
        private readonly IAccommodationBookingParser<IEnumerable<ViatgesMagonBookingResponse>> parser;

        public ViatgesMagonBookingProvider(IAccommodationBookingAutomatorAsync<IEnumerable<ViatgesMagonBookingResponse>> automator,
                                        IAccommodationBookingParser<IEnumerable<ViatgesMagonBookingResponse>> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
