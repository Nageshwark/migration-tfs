﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using Exception = System.Exception;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonBookingAutomator : CavalAutomaterBase, IAccommodationBookingAutomatorAsync<IEnumerable<ViatgesMagonBookingResponse>>
    {
    
        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalBookingRequestFactory _viatgesMagonProviderBookingRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViatgesMagonBookingAutomator(ILogger logger,
                                            IAccommodationConfigurationManager configurationManager,
                                            ICavalBookingRequestFactory viatgesMagonSupplierBookingRequestFactory,
                                            IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._viatgesMagonProviderBookingRequestFactory = viatgesMagonSupplierBookingRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<IEnumerable<ViatgesMagonBookingResponse>> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<ViatgesMagonBookingResponse> responses = new List<ViatgesMagonBookingResponse>();

            cavalHotelBookingConfirmRQ supplierRequest = null;
            ViatgesMagonBookingResponse response = null;

            try
            {
                // Currently the data model only supports one room per AccommodationProviderBookingRequest.
                supplierRequest = this._viatgesMagonProviderBookingRequestFactory.CreateSupplierBookingRequest(request);
                response = await GetSupplierBookingResponseAsync(request, supplierRequest);
                responses.Add(response);

                // Commented Code: if request supports multiple rooms
                //supplierRequests = Sync.ParallelForEachIgnoreFailed(request.SelectedRooms, (selectedRoom) => this.ViatgesMagonProviderBookingRequestFactory.CreateSupplierBookingRequest(request));
                //responses = await Async.ParallelForEachIgnoreFailed(supplierRequests, (supplierRequest) => GetSupplierBookingResponseAsync(request, supplierRequest));
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViatgesMagonBookingAutomator.GetBookingResponseAsync: ViatgesMagon supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return responses;
        }

        private async Task<ViatgesMagonBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, cavalHotelBookingConfirmRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Booking);

            var response = client.confirmHotelBooking(supplierRequest);

            var supplierResponse = new ViatgesMagonBookingResponse
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }

       
    }
}
