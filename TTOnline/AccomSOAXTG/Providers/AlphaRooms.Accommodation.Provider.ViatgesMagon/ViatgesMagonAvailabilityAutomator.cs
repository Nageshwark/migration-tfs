﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.CavalService;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using log4net.Repository.Hierarchy;


namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonAvailabilityAutomator : CavalAutomaterBase,
        IAccommodationAvailabilityAutomatorAsync<ViatgesMagonAvailabilityResponse>
    {
        private readonly ICavalAvailabilityRequestFactory _viatgesMagonSupplierAvailabilityRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViatgesMagonAvailabilityAutomator(ICavalAvailabilityRequestFactory viatgesMagonAvailabilityRequestFactory, IProviderOutputLogger outputLogger)
        {
            _viatgesMagonSupplierAvailabilityRequestFactory = viatgesMagonAvailabilityRequestFactory;
            _outputLogger = outputLogger;
        }

        public async Task<ViatgesMagonAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            ViatgesMagonAvailabilityResponse response = null;

            try
            {

                cavalHotelAvailabilityRQ supplierRequest = this._viatgesMagonSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);
            }
            catch (System.Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var  allSupplierRequests = new StringBuilder();

                if (response != null)
                {
                    allSupplierRequests.Append(string.Format("Request: {0}", Environment.NewLine));
                    allSupplierRequests.Append(response.SupplierRequest);
                    allSupplierRequests.Append(Environment.NewLine);

                    if (response.SupplierResponse != null)
                    {
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append("Response:");
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(response.SupplierResponse);
                        allSupplierRequests.Append(Environment.NewLine);
                        allSupplierRequests.Append(Environment.NewLine);
                    }
                }

                throw new SupplierApiException(string.Format("ViatgesMagonAvailabilityAutomator.GetAvailabilityResponseAsync: ViatgesMagon supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                        Environment.NewLine,
                        Environment.NewLine, errorMessage, Environment.NewLine, allSupplierRequests.ToString(),
                        Environment.NewLine),
                        ex);

            }


            return response;
        }

        private async Task<ViatgesMagonAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, cavalHotelAvailabilityRQ supplierRequest)
        {
            if (request.Debugging)
            {
                string serialisedRequest = supplierRequest.XmlSerialize();
            }

            var client = ConfigureSoapClient(request.Provider.Parameters, RequestType.Availability);


            cavalHotelAvailabilityRS response = client.getAvailableHotels(supplierRequest);

            var supplierResponse = new ViatgesMagonAvailabilityResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            _outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, supplierResponse);

            return supplierResponse;
        }
    }
}
