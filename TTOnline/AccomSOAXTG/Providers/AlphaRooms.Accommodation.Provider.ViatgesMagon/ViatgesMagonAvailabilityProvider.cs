﻿using AlphaRooms.Accommodation.Provider.ViatgesMagon;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.ViatgesMagon;
using AlphaRooms.Accommodation.Provider;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<ViatgesMagonAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<ViatgesMagonAvailabilityResponse> parser;

        public ViatgesMagonAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<ViatgesMagonAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<ViatgesMagonAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
