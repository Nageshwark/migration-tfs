﻿using System;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.CavalBase;
using AlphaRooms.Accommodation.Provider.CavalBase.Interfaces;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonCancellationAutomator : CavalAutomaterBase, IAccommodationCancellationAutomatorAsync<ViatgesMagonCancellationResponse>
    {
        private const string ViatgesMagonCancellationUrl = "ViatgesMagonCancellationUrl";
        private const string MaxReceivedMessageSize = "MaxReceivedMessageSize";


        private readonly ILogger _logger;
        private readonly IAccommodationConfigurationManager _configurationManager;
        private readonly ICavalCancellationRequestFactory _viatgesMagonProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger _outputLogger;

        public ViatgesMagonCancellationAutomator(   ILogger logger,
                                                    IAccommodationConfigurationManager configurationManager,
                                                    ICavalCancellationRequestFactory viatgesMagonSupplierCancellationRequestFactory,
                                                    IProviderOutputLogger outputLogger )
        {
            this._logger = logger;
            this._configurationManager = configurationManager;
            this._viatgesMagonProviderCancellationRequestFactory = viatgesMagonSupplierCancellationRequestFactory;
            this._outputLogger = outputLogger;
        }

        public async Task<ViatgesMagonCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            ViatgesMagonCancellationResponse response = null;

            try
            {
                var supplierRequest = _viatgesMagonProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);

                if (request.Debugging)
                {
                    string serialisedRequest = supplierRequest.XmlSerialize();
                }

                var client = ConfigureSoapClient(request);

                var supplierResponse = client.cancelBooking(supplierRequest);

                response = new ViatgesMagonCancellationResponse
                {
                    SupplierRequest = supplierRequest,
                    SupplierResponse = supplierResponse
                };

                
                return response;

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                var sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("ViatgesMagonBookingAutomator.GetCancellationResponseAsync: ViatgesMagon supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                        Environment.NewLine, Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(),
                        Environment.NewLine), ex);
            }
        }

    }
}
