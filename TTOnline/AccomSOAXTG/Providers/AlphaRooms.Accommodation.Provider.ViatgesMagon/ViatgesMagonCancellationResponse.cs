﻿
using AlphaRooms.Accommodation.Provider.CavalBase.CommonService;


namespace AlphaRooms.Accommodation.Provider.ViatgesMagon
{
    public class ViatgesMagonCancellationResponse
    {
        public cavalCancelBookingRQ SupplierRequest { get; set; }
        public cavalCancelBookingRS SupplierResponse { get; set; }
    }
}
