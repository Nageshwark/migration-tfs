﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelCancellationAutomator : BonotelAutomatorBase, IAccommodationCancellationAutomatorAsync<BonotelCancellationResponse>
    {
        private const string BonotelCancellationUrl = "BonotelCancellationUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IBonotelProviderCancellationRequestFactory BonotelProviderCancellationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public BonotelCancellationAutomator(ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IBonotelProviderCancellationRequestFactory BonotelSupplierCancellationRequestFactory
                                            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.BonotelProviderCancellationRequestFactory = BonotelSupplierCancellationRequestFactory;
            //this.outputLogger = outputLogger;
        }
        public async Task<BonotelCancellationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            List<BonotelCancellationResponse> responses = new List<BonotelCancellationResponse>();
            BonotelCancellationResponse response = null;
            try
            {
               cancellationRequest resRequest = BonotelProviderCancellationRequestFactory.CreateSupplierCancelRequest(request);
                response = await GetSupplierCancellationResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("BonotelCancellationAutomator.GetCancellationResponseAsync: Bonotel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }

        private async Task<BonotelCancellationResponse> GetSupplierCancellationResponseAsync(AccommodationProviderCancellationRequest request, cancellationRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(BonotelCancellationUrl);

            var responseXml = PostXmlRequest(url, supplierRequest.XmlSerialize());

            var response = responseXml.Result.XmlDeSerialize<cancellationResponse>();

            var supplierResponse = new BonotelCancellationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
