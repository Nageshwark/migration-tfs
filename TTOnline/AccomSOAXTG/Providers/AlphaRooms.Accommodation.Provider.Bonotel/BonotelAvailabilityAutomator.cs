﻿using System;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using System.Web;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelAvailabilityAutomator :BonotelAutomatorBase,  IAccommodationAvailabilityAutomatorAsync<BonotelAvailabilityResponse>
    {
        private const string BonotelAvailabilityUrl = "BonotelAvailabilityUrl";

        //private readonly IWebScrapeClient webScrapeClient;
        private readonly IBonotelAvailabilityRequestFactory _BonotelSupplierAvailabilityRequestFactory;


        public BonotelAvailabilityAutomator(IBonotelAvailabilityRequestFactory BonotelAvailabilityRequestFactory)
        {
            _BonotelSupplierAvailabilityRequestFactory = BonotelAvailabilityRequestFactory;
        }

        public async Task<BonotelAvailabilityResponse> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            BonotelAvailabilityResponse response = null;

            try
            {
                //ValidateRequest(request);

                availabilityRequest supplierRequest = this._BonotelSupplierAvailabilityRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierAvailabilityResponseAsync(request, supplierRequest);

            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("BonotelAvailabilityAutomator.GetAvailabilityResponseAsync: Bonotel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }


            return response;
        }

        private async Task<BonotelAvailabilityResponse> GetSupplierAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request, availabilityRequest supplierRequest)
        {
            var url = request.Provider.Parameters.GetParameterValue(BonotelAvailabilityUrl);

            var serialisedRequest = supplierRequest.XmlSerialize();
            var responseXml = PostXmlRequest(url, serialisedRequest);

            var response = responseXml.Result.XmlDeSerialize<availabilityResponse>();

            var supplierResponse = new BonotelAvailabilityResponse()
            {
                SupplierRequest=supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }

    }
}
