﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelBookingAutomator : BonotelAutomatorBase, IAccommodationBookingAutomatorAsync<BonotelBookingResponse>
    {
        private const string BonotelBookingUrl = "BonotelBookingUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IBonotelBookingRequestFactory _BonotelSupplierBookingRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public BonotelBookingAutomator(ILogger logger,
            // IAccommodationConfigurationManager configurationManager,
                                            IBonotelBookingRequestFactory BonotelSupplierBookingRequestFactory
            //IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
            this._BonotelSupplierBookingRequestFactory = BonotelSupplierBookingRequestFactory;
            this.outputLogger = outputLogger;
        }


        private async Task<BonotelBookingResponse> GetSupplierBookingResponseAsync(AccommodationProviderBookingRequest request, reservationRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(BonotelBookingUrl);

            var responseXml = PostXmlRequest(url, supplierRequest.XmlSerialize());

            var response = responseXml.Result.XmlDeSerialize<reservationResponse>();

            var supplierResponse = new BonotelBookingResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }


        public async Task<BonotelBookingResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            List<BonotelBookingResponse> responses = new List<BonotelBookingResponse>();
            BonotelBookingResponse response = null;
            try
            {
                reservationRequest resRequest = _BonotelSupplierBookingRequestFactory.CreateSupplierBookingInitiateRequest(request);
                response = await GetSupplierBookingResponseAsync(request, resRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();

                StringBuilder sb = new StringBuilder();

                if (response != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Request:");
                    sb.AppendLine(response.SupplierRequest.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Response:");
                    sb.AppendLine(response.SupplierResponse.XmlSerialize());
                    sb.AppendLine();
                }

                throw new SupplierApiException(string.Format("BonotelBookingAutomator.GetBookingResponseAsync: Bonotel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, sb.ToString(), Environment.NewLine),
                                                              ex);
            }

            return response;
        }
    }
}
