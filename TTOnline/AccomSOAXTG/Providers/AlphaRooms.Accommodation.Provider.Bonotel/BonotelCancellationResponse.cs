﻿using AlphaRooms.Accommodation.Provider.Bonotel.Service;


namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelCancellationResponse
    {
        public cancellationRequest SupplierRequest { get; set; }
        public cancellationResponse SupplierResponse { get; set; }
    }
}
