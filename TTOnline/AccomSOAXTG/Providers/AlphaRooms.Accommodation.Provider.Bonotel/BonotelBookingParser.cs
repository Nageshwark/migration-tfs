﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelBookingParser : IAccommodationBookingParser<BonotelBookingResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public BonotelBookingParser(ILogger logger)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, BonotelBookingResponse responses)
        {
            var bookingResults = new ConcurrentQueue<AccommodationProviderBookingResult>();

            try
            {
                CreateBookingResultsFromResponse(request, responses.SupplierRequest, responses.SupplierResponse, bookingResults);

            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Bonotel Supplier Data exception in Booking Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return bookingResults;
        }

        private void CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request,
                                                reservationRequest supplierRequest,
                                                reservationResponse supplierResponse,
                                                ConcurrentQueue<AccommodationProviderBookingResult> bookingResults)
        {
            if (request.Debugging)
            {
                string serialisedResponse = supplierResponse.XmlSerialize();
            }

            bool hasResults = (
                                supplierResponse != null
                               );

            if (hasResults && supplierResponse.status == "Y")
            {
                string rgId = supplierResponse.referenceNo;

                int roomNumber = 1;
                foreach (var res in supplierResponse.roomReferenceDetails)
                {
                    var result = new AccommodationProviderBookingResult();

                    result.BookingStatus = BookingStatus.Confirmed;
                    result.ProviderBookingReference = rgId.ToString();
                    result.Message = "";

                    result.ProviderSpecificData = new Dictionary<string, string>();
                    result.ProviderSpecificData.Add("BonotelRoomReservationId_" + roomNumber, res.Value.ToString());

                    result.RoomNumber = (byte)roomNumber;

                    bookingResults.Enqueue(result);

                    roomNumber++;
                }

            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("Bonotel Booking Parser: Error in Boooking.");

                if (supplierResponse != null && supplierResponse.errors != null)
                {
                    sbMessage.AppendLine("Error Details:");
                    sbMessage.AppendLine(string.Format("Code: {0}, Description:{1}", supplierResponse.errors.code, supplierResponse.errors.description));
                }


                throw new SupplierApiDataException(sbMessage.ToString());
            }

        }
    }
}
