﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelValuationAutomator : BonotelAutomatorBase, IAccommodationValuationAutomatorAsync<BonotelValuationResponse>
    {
        private const string BonotelAvailabilityUrl = "BonotelAvailabilityUrl";

        private readonly IWebScrapeClient webScrapeClient;
        private readonly ILogger logger;
       // private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IBonotelValuationRequestFactory BonotelProviderValuationRequestFactory;
        private readonly IProviderOutputLogger outputLogger;

        public BonotelValuationAutomator( ILogger logger,
                                            //IAccommodationConfigurationManager configurationManager,
                                            IBonotelValuationRequestFactory BonotelSupplierValuationRequestFactory
            //,
            //                                    IProviderOutputLogger outputLogger
            )
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
            this.BonotelProviderValuationRequestFactory = BonotelSupplierValuationRequestFactory;
            //this.outputLogger = outputLogger;
        }

        public async Task<BonotelValuationResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            BonotelValuationResponse response = null;
            try
            {
                availabilityRequest supplierRequest = this.BonotelProviderValuationRequestFactory.CreateSupplierAvailabilityRequest(request);

                response = await GetSupplierValuationResponseAsync(request, supplierRequest);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.GetDetailedMessageWithInnerExceptions();
                throw new SupplierApiException(string.Format("BonotelValuationAutomator.GetValuationResponseAsync: Bonotel supplier api exception.{0}Error Message: {1}{2}{3}{4}{5}",
                                                              Environment.NewLine,
                                                              Environment.NewLine, errorMessage, Environment.NewLine, errorMessage, Environment.NewLine),
                                                              ex);

            }

            return response;
        }

        private async Task<BonotelValuationResponse> GetSupplierValuationResponseAsync(AccommodationProviderValuationRequest request, availabilityRequest supplierRequest)
        {
            string url = request.Provider.Parameters.GetParameterValue(BonotelAvailabilityUrl);
            string serialisedRequest= supplierRequest.XmlSerialize();
            
            var responseXml = PostXmlRequest(url, serialisedRequest);

            var response = responseXml.Result.XmlDeSerialize<availabilityResponse>();

            var supplierResponse = new BonotelValuationResponse()
            {
                SupplierRequest = supplierRequest,
                SupplierResponse = response
            };

            return supplierResponse;
        }
    }
}
