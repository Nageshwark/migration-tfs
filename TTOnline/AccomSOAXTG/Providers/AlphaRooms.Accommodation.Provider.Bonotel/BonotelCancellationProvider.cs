﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelCancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<BonotelCancellationResponse> automator;
        private readonly IAccommodationCancellationParserAsync<BonotelCancellationResponse> parser;

        public BonotelCancellationProvider(IAccommodationCancellationAutomatorAsync<BonotelCancellationResponse> automator,
                                       IAccommodationCancellationParserAsync<BonotelCancellationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }
        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return await parser.GetCancellationResultsAsync(request, response);
        }
    }
}
