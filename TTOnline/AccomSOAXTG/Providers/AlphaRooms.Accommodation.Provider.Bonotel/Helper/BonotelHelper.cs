﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Factories;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Helper
{
    public class BonotelHelper : BonotelProviderFactoryBase
    {
                private const string BonotelRoomTypeCode = "BonotelRoomTypeCode";
        private const string BonotelBedTypeCode = "BonotelBedTypeCode";


        public  availabilityRequestRoomInfo[] GetRoomInfoArray(AccommodationProviderAvailabilityRequest request)
        {
            var roomInfo = new List<availabilityRequestRoomInfo>();
            foreach (var room in request.Rooms)
            {
                roomInfo.Add(new availabilityRequestRoomInfo()
                {
                    adultsNum = room.Guests.AdultsCount,
                    childNum = room.Guests.ChildrenAndInfantsCount,
                    bedTypeId = "0",
                    childAges = GetChildrenAges(room),
                    roomTypeId = "0"
                });
            }

            return roomInfo.ToArray();
        }

        public  availabilityRequestRoomInfo[] GetRoomInfoArray(AccommodationProviderValuationRequest request)
        {
            var roomInfo = new List<availabilityRequestRoomInfo>();
                        var avilRequest = request.SelectedRooms.First().AvailabilityRequest;
            foreach (var room in avilRequest.Rooms)
            {
                var avilResult = request.SelectedRooms[room.RoomNumber - 1].AvailabilityResult;
                roomInfo.Add(new availabilityRequestRoomInfo()
                {
                    adultsNum = room.Guests.AdultsCount,
                    childNum = room.Guests.ChildrenAndInfantsCount,
                    bedTypeId = (avilResult.ProviderSpecificData.ContainsKey(BonotelBedTypeCode) ? avilResult.ProviderSpecificData[BonotelBedTypeCode] : "0"),
                    childAges = GetChildrenAges(room),
                    roomTypeId = (avilResult.ProviderSpecificData.ContainsKey(BonotelRoomTypeCode) ? avilResult.ProviderSpecificData[BonotelRoomTypeCode] : "0")
                });
            }

            return roomInfo.ToArray();
        }

        public  string[] GetChildrenAges(AccommodationProviderAvailabilityRequestRoom room)
        {
            var lstChildAges = new List<string>();

            if (room.Guests.ChildrenAndInfantsCount > 0)
            {
                lstChildAges.AddRange(room.Guests.ChildAndInfantAges.Select(age => age.ToString()));
            }

            return lstChildAges.ToArray();
        }

        public  string[] GetHotelCodes(AccommodationProviderAvailabilityRequest request)
        {
            string[] hotelCodes = new string[0];
            int count = 0;
            if (request.EstablishmentCodes.Any())
            {
                hotelCodes = new string[request.EstablishmentCodes.Length];
                foreach (string code in request.EstablishmentCodes)
                {
                    hotelCodes[count] = code;
                    count++;
                }
            }

            return hotelCodes;
        }

        public  string GetDestination(AccommodationProviderAvailabilityRequest request)
        {
            if (request.DestinationCodes.Any())
            {
                return request.DestinationCodes[0];
            }
            return "";
        }
    }
}
