﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Provider;
using AlphaRooms.Accommodation.Provider.Bonotel;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelValuationProvider : AccommodationValuationBase
    {
        private readonly IAccommodationValuationAutomatorAsync<BonotelValuationResponse> automator;
        private readonly IAccommodationValuationParser<BonotelValuationResponse> parser;

        public BonotelValuationProvider(IAccommodationValuationAutomatorAsync<BonotelValuationResponse> automator,
                                                IAccommodationValuationParser<BonotelValuationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var responses = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, responses);
        }
    }
}
