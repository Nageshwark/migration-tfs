﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Threading.Tasks;
using AlphaRooms.Utilities.CustomExceptions;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
//using Ninject.Infrastructure.Language;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelCancellationlationParser : IAccommodationCancellationParserAsync<BonotelCancellationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public BonotelCancellationlationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }


        public async Task<IEnumerable<AccommodationProviderCancellationResult>> GetCancellationResultsAsync(AccommodationProviderCancellationRequest request, BonotelCancellationResponse response)
        {
            var bookingResults = new List<AccommodationProviderCancellationResult>();
            var bookingResult = new AccommodationProviderCancellationResult();

            try
            {
                if (response.SupplierResponse.status == "Y")
                {
                    bookingResult.CancellationStatus = CancellationStatus.Succeeded;
                    bookingResult.Message = "The booking: " + request.ProviderBookingReference + " has been cancelled.";

                }
                else
                {
                    bookingResult.CancellationStatus = CancellationStatus.Failed;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("There was an unknown problem with the cancellation response from Tourico.");
                    sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                    sb.AppendLine("Cancellation Request:");
                    sb.AppendLine(request.XmlSerialize());
                    sb.AppendLine();
                    sb.AppendLine("Cancellation Response:");
                    sb.AppendLine(response.XmlSerialize());

                    bookingResult.Message = sb.ToString();

                    logger.Error(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("There was a problem parsing the cancellation response from Tourico.");
                sb.AppendLine("Booking Request Id = " + request.ProviderBookingReference);
                sb.AppendLine("Cancellation Request:");
                sb.AppendLine(request.XmlSerialize());
                sb.AppendLine();
                sb.AppendLine("Cancellation Response:");
                sb.AppendLine(response.XmlSerialize());

                throw new SupplierApiException(sb.ToString());
            }

            bookingResults.Add(bookingResult);

            return bookingResults;
        }
      
    }
}
