﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;


namespace AlphaRooms.Accommodation.Provider.Bonotel.Factories
{
    public class BonotelCancellationRequestFactory : BonotelProviderFactoryBase, IBonotelProviderCancellationRequestFactory
    {
        public cancellationRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request)
        {
            cancellationRequest canRequest = new cancellationRequest()
            {
                control = CreateCancelAuthHeader(request.Provider.Parameters),
                supplierReferenceNo = request.ProviderBookingReference
            };

            return canRequest;
        }
    }
}
