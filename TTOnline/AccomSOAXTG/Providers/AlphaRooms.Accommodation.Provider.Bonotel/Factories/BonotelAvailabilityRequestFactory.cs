﻿using System;
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using System.Collections.Generic;
using AlphaRooms.Accommodation.Provider.Bonotel.Helper;


namespace AlphaRooms.Accommodation.Provider.Bonotel.Factories
{
    public class BonotelAvailabilityRequestFactory : BonotelProviderFactoryBase, IBonotelAvailabilityRequestFactory
    {
        private const string BonotelUserName = "BonotelUserName";
        private const string BonotelPassword = "BonotelPassword";

        public availabilityRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request)
        {
            var helper = new BonotelHelper();

            var availRequest = new availabilityRequest()
            {
                control = CreateAuthHeader(request.Provider.Parameters),
                checkIn = request.CheckInDate.ToString("dd-MMM-yyyy"),
                checkOut = request.CheckOutDate.ToString("dd-MMM-yyyy"),
                noOfRooms = request.Rooms.Count().ToString(),
                noOfNights = (request.CheckOutDate - request.CheckInDate).TotalDays.ToString(),
                hotelCodes = helper.GetHotelCodes(request),
                IATAAirportCode = helper.GetDestination(request),
                roomsInformation = helper.GetRoomInfoArray(request)
            };

            return availRequest;
        }


    }

}

