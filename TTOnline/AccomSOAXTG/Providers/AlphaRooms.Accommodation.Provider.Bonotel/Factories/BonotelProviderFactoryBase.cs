﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.WebScraping;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Factories
{
   public class BonotelProviderFactoryBase
    {
        private const string BonotelUserName = "BonotelUserName";
        private const string BonotelPassword = "BonotelPassword";

        public availabilityRequestControl CreateAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new availabilityRequestControl
            {
                userName = parameters.GetParameterValue(BonotelUserName),
                passWord = parameters.GetParameterValue(BonotelPassword)
            };
        }

        public reservationRequestControl CreateBookingAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new reservationRequestControl
            {
                userName = parameters.GetParameterValue(BonotelUserName),
                passWord = parameters.GetParameterValue(BonotelPassword)
            };
        }

        public cancellationRequestControl CreateCancelAuthHeader(List<AccommodationProviderParameter> parameters)
        {
            return new cancellationRequestControl
            {
                userName = parameters.GetParameterValue(BonotelUserName),
                passWord = parameters.GetParameterValue(BonotelPassword)
            };
        }

        
    }
}
