﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;



namespace AlphaRooms.Accommodation.Provider.Bonotel.Factories
{
    public class BonotelBookingRequestFactory : BonotelProviderFactoryBase, IBonotelBookingRequestFactory
    {
        private const string BonotelUserName = "BonotelUserName";
        private const string BonotelPassword = "BonotelPassword";
        private const string BonotelCurrencyCode = "BonotelCurrencyCode";

        public reservationRequest CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request)
        {
            reservationRequest supplierRequest = new reservationRequest()
            {
                control = CreateBookingAuthHeader(request.Provider.Parameters),
                reservationDetails = GetReservationDetails(request)
            };

            return supplierRequest;
        }


        private reservationRequestReservationDetails GetReservationDetails(AccommodationProviderBookingRequest request)
        {
            AccommodationProviderValuationResult roomToBook = request.ValuatedRooms.First().ValuationResult;

            var resDtls = new reservationRequestReservationDetails()
            {
                timeStamp = DateTime.Now.ToString(),
                confirmationType = "CON",
                tourOperatorOrderNumber = "0", //request.BookingId.ToString(),
                checkIn = roomToBook.CheckInDate.ToString("dd-MMM-yyyy"),
                checkOut = roomToBook.CheckOutDate.ToString("dd-MMM-yyyy"),
                noOfRooms = request.ValuatedRooms.Length.ToString(),
                noOfNights = (roomToBook.CheckOutDate - roomToBook.CheckInDate).TotalDays.ToString(),
                hotelCode = roomToBook.EstablishmentEdiCode,
                total = GetBookingPrice(roomToBook),
                totalTax = new reservationRequestReservationDetailsTotalTax() { currency = roomToBook.ProviderSpecificData[BonotelCurrencyCode], Value = 0 },
                roomData = GetReservationRoomDetails(request),
                comment = new reservationRequestReservationDetailsComment() { customer = "", hotel = "" }
            };

            return resDtls;
        }

        private reservationRequestReservationDetailsTotal GetBookingPrice(AccommodationProviderValuationResult roomToBook)
        {
            reservationRequestReservationDetailsTotal resTotal = new reservationRequestReservationDetailsTotal();
            resTotal.currency = roomToBook.ProviderSpecificData[BonotelCurrencyCode];
            resTotal.Value = Convert.ToDecimal(roomToBook.CostPrice.Amount);

            return resTotal;
        }

        private reservationRequestReservationDetailsRoomData[] GetReservationRoomDetails(AccommodationProviderBookingRequest request)
        {
            List<reservationRequestReservationDetailsRoomData> lstRoomDtls =
                new List<reservationRequestReservationDetailsRoomData>();

            foreach (var room in request.ValuatedRooms)
            {
                lstRoomDtls.Add(new reservationRequestReservationDetailsRoomData
                {
                    roomNo = room.RoomNumber.ToString(),
                    roomCode = room.ValuationResult.RoomCode,
                    roomTypeCode = room.ValuationResult.ProviderSpecificData["BonotelRoomTypeCode"],
                    bedTypeCode = room.ValuationResult.ProviderSpecificData["BonotelBedTypeCode"],
                    ratePlanCode = room.ValuationResult.BoardCode,
                    noOfAdults = room.Guests.AdultsCount,
                    noOfChildren = room.Guests.ChildrenAndInfantsCount,
                    occupancy = GetOccupancyDetails(room)
                });

            }

            return lstRoomDtls.ToArray();

        }

        private List<reservationRequestReservationDetailsRoomDataGuest> CreateChildList(AccommodationProviderBookingRequestRoom room)
        {
            var childList = new List<reservationRequestReservationDetailsRoomDataGuest>();

            foreach (var guest in room.Guests.Where(g => g.Type != GuestType.Adult))
            {
                childList.Add(new reservationRequestReservationDetailsRoomDataGuest
                {
                    age = guest.Age.ToString(),
                    firstName = guest.FirstName,
                    lastName = guest.Surname
                });
            }

            return childList;
        }

        private List<reservationRequestReservationDetailsRoomDataGuest> CreateAdultList(AccommodationProviderBookingRequestRoom room)
        {
            var adultList = new List<reservationRequestReservationDetailsRoomDataGuest>();

            foreach (var guest in room.Guests.Where(g => g.Type == GuestType.Adult))
            {
                adultList.Add(new reservationRequestReservationDetailsRoomDataGuest
                {
                    firstName = guest.FirstName,
                    lastName = guest.Surname
                });
            }

            return adultList;
        }

        private reservationRequestReservationDetailsRoomDataGuest[] GetOccupancyDetails(AccommodationProviderBookingRequestRoom room)
        {
            var adultList = new List<reservationRequestReservationDetailsRoomDataGuest>();

            return CreateAdultList(room).Concat(CreateChildList(room)).ToArray();
        }

    }
}
