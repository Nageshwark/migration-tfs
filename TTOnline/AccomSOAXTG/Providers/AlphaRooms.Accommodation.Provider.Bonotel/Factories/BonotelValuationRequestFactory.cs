﻿
using System.Linq;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using AlphaRooms.Accommodation.Provider.Bonotel.Helper;
namespace AlphaRooms.Accommodation.Provider.Bonotel.Factories
{
    public class BonotelValuationRequestFactory: BonotelProviderFactoryBase, IBonotelValuationRequestFactory
    {
        private const string BonotelUserName = "BonotelUserName";
        private const string BonotelPassword = "BonotelPassword";

        public availabilityRequest CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request)
        {
            var avilRequest = request.SelectedRooms.First().AvailabilityRequest;
            var hotelCode = request.SelectedRooms.First().AvailabilityResult.EstablishmentEdiCode;
            var helper = new BonotelHelper();

            var availRequest = new availabilityRequest()
            {
                control = CreateAuthHeader(request.Provider.Parameters),
                checkIn = avilRequest.CheckInDate.ToString("dd-MMM-yyyy"),
                checkOut = avilRequest.CheckOutDate.ToString("dd-MMM-yyyy"),
                noOfRooms = avilRequest.Rooms.Count().ToString(),
                noOfNights = (avilRequest.CheckOutDate - avilRequest.CheckInDate).TotalDays.ToString(),
                hotelCodes = new [] { hotelCode},
                roomsInformation = helper.GetRoomInfoArray(request)
            };

            

            return availRequest;
        }

    }
}
