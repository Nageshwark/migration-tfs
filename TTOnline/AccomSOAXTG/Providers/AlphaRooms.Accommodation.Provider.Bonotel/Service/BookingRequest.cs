﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=4.0.30319.33440.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class reservationRequest
{

    private reservationRequestControl controlField;

    private reservationRequestReservationDetails reservationDetailsField;

    /// <remarks/>
    public reservationRequestControl control
    {
        get
        {
            return this.controlField;
        }
        set
        {
            this.controlField = value;
        }
    }

    /// <remarks/>
    public reservationRequestReservationDetails reservationDetails
    {
        get
        {
            return this.reservationDetailsField;
        }
        set
        {
            this.reservationDetailsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestControl
{

    private string userNameField;

    private string passWordField;

    /// <remarks/>
    public string userName
    {
        get
        {
            return this.userNameField;
        }
        set
        {
            this.userNameField = value;
        }
    }

    /// <remarks/>
    public string passWord
    {
        get
        {
            return this.passWordField;
        }
        set
        {
            this.passWordField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetails
{

    private string confirmationTypeField;

    private string tourOperatorOrderNumberField;

    private string checkInField;

    private string checkOutField;

    private string noOfRoomsField;

    private string noOfNightsField;

    private string hotelCodeField;

    private reservationRequestReservationDetailsTotal totalField;

    private reservationRequestReservationDetailsTotalTax totalTaxField;

    private reservationRequestReservationDetailsRoomData[] roomDataField;

    private reservationRequestReservationDetailsComment commentField;

    private string timeStampField;

    /// <remarks/>
    public string confirmationType
    {
        get
        {
            return this.confirmationTypeField;
        }
        set
        {
            this.confirmationTypeField = value;
        }
    }

    /// <remarks/>
    public string tourOperatorOrderNumber
    {
        get
        {
            return this.tourOperatorOrderNumberField;
        }
        set
        {
            this.tourOperatorOrderNumberField = value;
        }
    }

    /// <remarks/>
    public string checkIn
    {
        get
        {
            return this.checkInField;
        }
        set
        {
            this.checkInField = value;
        }
    }

    /// <remarks/>
    public string checkOut
    {
        get
        {
            return this.checkOutField;
        }
        set
        {
            this.checkOutField = value;
        }
    }

    /// <remarks/>
    public string noOfRooms
    {
        get
        {
            return this.noOfRoomsField;
        }
        set
        {
            this.noOfRoomsField = value;
        }
    }

    /// <remarks/>
    public string noOfNights
    {
        get
        {
            return this.noOfNightsField;
        }
        set
        {
            this.noOfNightsField = value;
        }
    }

    /// <remarks/>
    public string hotelCode
    {
        get
        {
            return this.hotelCodeField;
        }
        set
        {
            this.hotelCodeField = value;
        }
    }

    /// <remarks/>
    public reservationRequestReservationDetailsTotal total
    {
        get
        {
            return this.totalField;
        }
        set
        {
            this.totalField = value;
        }
    }

    /// <remarks/>
    public reservationRequestReservationDetailsTotalTax totalTax
    {
        get
        {
            return this.totalTaxField;
        }
        set
        {
            this.totalTaxField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("roomData")]
    public reservationRequestReservationDetailsRoomData[] roomData
    {
        get
        {
            return this.roomDataField;
        }
        set
        {
            this.roomDataField = value;
        }
    }

    /// <remarks/>
    public reservationRequestReservationDetailsComment comment
    {
        get
        {
            return this.commentField;
        }
        set
        {
            this.commentField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string timeStamp
    {
        get
        {
            return this.timeStampField;
        }
        set
        {
            this.timeStampField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetailsTotal
{

    private string currencyField;

    private decimal valueField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string currency
    {
        get
        {
            return this.currencyField;
        }
        set
        {
            this.currencyField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTextAttribute()]
    public decimal Value
    {
        get
        {
            return this.valueField;
        }
        set
        {
            this.valueField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetailsTotalTax
{

    private string currencyField;

    private decimal valueField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string currency
    {
        get
        {
            return this.currencyField;
        }
        set
        {
            this.currencyField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTextAttribute()]
    public decimal Value
    {
        get
        {
            return this.valueField;
        }
        set
        {
            this.valueField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetailsRoomData
{

    private string roomNoField;

    private string roomCodeField;

    private string roomTypeCodeField;

    private string bedTypeCodeField;

    private string ratePlanCodeField;

    private byte noOfAdultsField;

    private byte noOfChildrenField;

    private reservationRequestReservationDetailsRoomDataGuest[] occupancyField;

    /// <remarks/>
    public string roomNo
    {
        get
        {
            return this.roomNoField;
        }
        set
        {
            this.roomNoField = value;
        }
    }

    /// <remarks/>
    public string roomCode
    {
        get
        {
            return this.roomCodeField;
        }
        set
        {
            this.roomCodeField = value;
        }
    }

    /// <remarks/>
    public string roomTypeCode
    {
        get
        {
            return this.roomTypeCodeField;
        }
        set
        {
            this.roomTypeCodeField = value;
        }
    }

    /// <remarks/>
    public string bedTypeCode
    {
        get
        {
            return this.bedTypeCodeField;
        }
        set
        {
            this.bedTypeCodeField = value;
        }
    }

    /// <remarks/>
    public string ratePlanCode
    {
        get
        {
            return this.ratePlanCodeField;
        }
        set
        {
            this.ratePlanCodeField = value;
        }
    }

    /// <remarks/>
    public byte noOfAdults
    {
        get
        {
            return this.noOfAdultsField;
        }
        set
        {
            this.noOfAdultsField = value;
        }
    }

    /// <remarks/>
    public byte noOfChildren
    {
        get
        {
            return this.noOfChildrenField;
        }
        set
        {
            this.noOfChildrenField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("guest", IsNullable = false)]
    public reservationRequestReservationDetailsRoomDataGuest[] occupancy
    {
        get
        {
            return this.occupancyField;
        }
        set
        {
            this.occupancyField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetailsRoomDataGuest
{

    private string titleField;

    private string firstNameField;

    private string lastNameField;

    private string ageField;

    /// <remarks/>
    public string title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    /// <remarks/>
    public string firstName
    {
        get
        {
            return this.firstNameField;
        }
        set
        {
            this.firstNameField = value;
        }
    }

    /// <remarks/>
    public string lastName
    {
        get
        {
            return this.lastNameField;
        }
        set
        {
            this.lastNameField = value;
        }
    }

    /// <remarks/>
    public string age
    {
        get
        {
            return this.ageField;
        }
        set
        {
            this.ageField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class reservationRequestReservationDetailsComment
{

    private string hotelField;

    private string customerField;

    /// <remarks/>
    public string hotel
    {
        get
        {
            return this.hotelField;
        }
        set
        {
            this.hotelField = value;
        }
    }

    /// <remarks/>
    public string customer
    {
        get
        {
            return this.customerField;
        }
        set
        {
            this.customerField = value;
        }
    }
}
