﻿using AlphaRooms.Accommodation.Provider.Bonotel.Service;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelAvailabilityResponse
    {
        public availabilityRequest SupplierRequest { get; set; }
        public availabilityResponse SupplierResponse { get; set; }
    }
}
