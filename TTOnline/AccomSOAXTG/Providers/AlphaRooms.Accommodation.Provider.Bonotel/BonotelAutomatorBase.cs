﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.WebScraping;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelAutomatorBase
    {
        public async Task<string> PostXmlRequest(string postUrl, string request)
        {
            var webScrapeClient = new WebScrapeClient() { UseCompression = true, Cookies = new WebScraperCookies(), ContentType = WebContentType.ApplicationXml };

            WebScrapeResponse response = await webScrapeClient.PostAsync(postUrl, request);

            return response.Value.ToString();
        }
    }
}
