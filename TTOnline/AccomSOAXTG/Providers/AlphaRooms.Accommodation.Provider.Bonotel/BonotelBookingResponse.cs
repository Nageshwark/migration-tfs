﻿using AlphaRooms.Accommodation.Provider.Bonotel.Service;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelBookingResponse
    {
        public reservationRequest SupplierRequest { get; set; }
        public reservationResponse SupplierResponse { get; set; }
    }
}
