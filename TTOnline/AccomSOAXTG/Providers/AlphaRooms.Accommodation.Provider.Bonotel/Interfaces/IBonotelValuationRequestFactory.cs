﻿using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Interfaces
{
    public interface IBonotelValuationRequestFactory
    {
        availabilityRequest CreateSupplierAvailabilityRequest(AccommodationProviderValuationRequest request);
    }
}
