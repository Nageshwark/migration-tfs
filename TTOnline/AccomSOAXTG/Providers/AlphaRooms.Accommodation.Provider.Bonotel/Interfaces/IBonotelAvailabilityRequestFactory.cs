﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Interfaces
{
    public interface IBonotelAvailabilityRequestFactory
    {
        availabilityRequest CreateSupplierAvailabilityRequest(AccommodationProviderAvailabilityRequest request);
    }
}
