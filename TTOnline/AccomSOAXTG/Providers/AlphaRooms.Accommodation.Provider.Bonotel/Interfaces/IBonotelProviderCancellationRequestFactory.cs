﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;

//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Interfaces
{
    public interface IBonotelProviderCancellationRequestFactory
    {
        cancellationRequest CreateSupplierCancelRequest(AccommodationProviderCancellationRequest request);
    }
}
