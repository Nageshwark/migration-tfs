﻿using AlphaRooms.Accommodation.Core.Provider.Contracts;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;

namespace AlphaRooms.Accommodation.Provider.Bonotel.Interfaces
{
    public interface IBonotelBookingRequestFactory
    {
        reservationRequest CreateSupplierBookingInitiateRequest(AccommodationProviderBookingRequest request);
    }
}
