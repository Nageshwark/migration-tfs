﻿using AlphaRooms.Accommodation.Provider.Bonotel.Service;


namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelValuationResponse
    {
        public availabilityRequest SupplierRequest { get; set; }
        public availabilityResponse SupplierResponse { get; set; }
    }
}
