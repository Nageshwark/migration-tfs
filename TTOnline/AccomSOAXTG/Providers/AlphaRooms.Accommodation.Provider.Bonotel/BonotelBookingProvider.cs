﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
//using Ninject;
//using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelBookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<BonotelBookingResponse> automator;
        private readonly IAccommodationBookingParser<BonotelBookingResponse> parser;

        public BonotelBookingProvider(IAccommodationBookingAutomatorAsync<BonotelBookingResponse> automator,
                                        IAccommodationBookingParser<BonotelBookingResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
