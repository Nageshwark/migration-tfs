﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.Bonotel;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{

    public class BonotelValuationParser : IAccommodationValuationParser<BonotelValuationResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public BonotelValuationParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, BonotelValuationResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderValuationResult>();

            try
            {
                // DEBUG CODE ONLY:
                if (request.Debugging)
                {
                    //string xmlResponse = response.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Bonotel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderValuationRequest request, BonotelValuationResponse supplierResponse, ConcurrentQueue<AccommodationProviderValuationResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                var hotelList = supplierResponse.SupplierResponse.hotelList;
                foreach (var hotel in hotelList)
                {
                    foreach (var roomInfo in hotel.roomInformation)
                    {
                        var result = new AccommodationProviderValuationResult();
                        var roomNo = int.Parse(roomInfo.roomNo);
                        var availabilityResult = request.SelectedRooms[roomNo - 1].AvailabilityResult;

                        result.RoomNumber = Convert.ToByte(roomNo);
                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.SupplierEdiCode = request.Provider.EdiCode;

                        result.DestinationEdiCode = hotel.city;
                        result.EstablishmentEdiCode = hotel.hotelCode.ToString();
                        result.EstablishmentName = hotel.name;

                        result.RoomDescription = roomInfo.roomDescription;
                        result.RoomCode = roomInfo.roomCode;

                        result.CheckInDate = request.SelectedRooms[0].AvailabilityRequest.CheckInDate;
                        result.CheckOutDate = request.SelectedRooms[0].AvailabilityRequest.CheckOutDate;

                        if (roomInfo.rateInformation.Any())
                        {
                            result.BoardCode = roomInfo.rateInformation[0].ratePlanCode;
                            result.BoardDescription = roomInfo.rateInformation[0].ratePlan;
                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.rateInformation[0].totalRate),
                                CurrencyCode = hotel.rateCurrencyCode
                            };
                        }

                        result.CostPrice = result.SalePrice;

                        result.Adults = request.SelectedRooms[roomNo - 1].Guests.AdultsCount;
                        result.Children = (byte)(request.SelectedRooms[result.RoomNumber - 1].Guests.ChildrenCount + request.SelectedRooms[result.RoomNumber - 1].Guests.InfantsCount);

                        result.PaymentModel = availabilityResult.PaymentModel;
                        result.RateType = availabilityResult.RateType;
                        result.IsNonRefundable = availabilityResult.IsNonRefundable;

                        result.NumberOfAvailableRooms = 0;
                        //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("BonotelRoomTypeCode", roomInfo.roomTypeCode);
                        result.ProviderSpecificData.Add("BonotelBedTypeCode", roomInfo.bedTypeCode);
                        result.ProviderSpecificData.Add("BonotelCurrencyCode", hotel.rateCurrencyCode);

                        availabilityResults.Enqueue(result);

                    }

                }
            }
        }

        private static bool HasResults(availabilityResponse supplierResponse)
        {

            if (supplierResponse != null && supplierResponse.status != "N")
            {
                var items = supplierResponse.hotelList;

                if (supplierResponse.hotelList.Any())
                    return true;
                else
                    return false;
            }

            return false;
        }
    }
}
