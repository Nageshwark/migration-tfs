﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Contracts;

namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelAvailabilityProvider : AccommodationAvailabilitySearchBase
    {
        private readonly IAccommodationAvailabilityAutomatorAsync<BonotelAvailabilityResponse> automator;
        private readonly IAccommodationAvailabilityParser<BonotelAvailabilityResponse> parser;

        public BonotelAvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<BonotelAvailabilityResponse> automator,
                                                IAccommodationAvailabilityParser<BonotelAvailabilityResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var responses = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, responses);
        }
    }
}
