﻿using System;
using System.Collections.Generic;
using System.Text;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System.Collections.Concurrent;
using System.Linq;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Request;
//using AlphaRooms.Accommodation.Provider.Bonotel.Service.Response;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Provider.Bonotel.Service;
namespace AlphaRooms.Accommodation.Provider.Bonotel
{
    public class BonotelAvailabilityParser : IAccommodationAvailabilityParser<BonotelAvailabilityResponse>
    {
        private readonly ILogger logger;
        //private readonly IAccommodationConfigurationManager configurationManager;

        public BonotelAvailabilityParser(ILogger logger)
        {
            this.logger = logger;
            //this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, BonotelAvailabilityResponse response)
        {
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();

            try
            {
                if (request.Debugging)
                {
                    string serialisedResponse = response.SupplierResponse.XmlSerialize();
                }

                CreateAvailabilityResultsFromResponse(request, response, availabilityResults);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;

                if (ex is AggregateException)
                {
                    AggregateException aggregateEx = ex as AggregateException;
                    errorMessage = aggregateEx.GetDetailedMessageWithInnerExceptions();
                }
                else
                {
                    errorMessage = ex.Message;
                }

                string message = string.Format("Bonotel Supplier Data exception in Availability Parser: {0} Exception Message: {1}{2}{3} Stack Trace: {4}{5}",
                                                Environment.NewLine,
                                                Environment.NewLine, errorMessage, Environment.NewLine,
                                                Environment.NewLine, ex.StackTrace);
                throw new SupplierApiDataException(message);
            }

            return availabilityResults;
        }

        private void CreateAvailabilityResultsFromResponse(AccommodationProviderAvailabilityRequest request, BonotelAvailabilityResponse supplierResponse, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            bool hasResults = HasResults(supplierResponse.SupplierResponse);

            if (hasResults)
            {
                foreach (var hotel in supplierResponse.SupplierResponse.hotelList)
                {
                    foreach (var roomInfo in hotel.roomInformation)
                    {
                        var result = new AccommodationProviderAvailabilityResult();

                        result.RoomNumber = Convert.ToByte(roomInfo.roomNo);
                        result.ProviderEdiCode = request.Provider.EdiCode;
                        result.SupplierEdiCode = request.Provider.EdiCode;

                        result.DestinationEdiCode = hotel.city;
                        result.EstablishmentEdiCode = hotel.hotelCode.ToString();
                        result.EstablishmentName = hotel.name;

                        result.RoomDescription = roomInfo.roomDescription;
                        result.RoomCode = roomInfo.roomCode;

                        result.CheckInDate = request.CheckInDate;
                        result.CheckOutDate = request.CheckOutDate;

                        if (roomInfo.rateInformation.Any())
                        {
                            result.BoardCode = roomInfo.rateInformation[0].ratePlanCode;
                            result.BoardDescription = roomInfo.rateInformation[0].ratePlan;
                            result.SalePrice = new Money()
                            {
                                Amount = Convert.ToDecimal(roomInfo.rateInformation[0].totalRate),
                                CurrencyCode = hotel.rateCurrencyCode
                            };
                        }

                        result.CostPrice = result.SalePrice;

                        result.Adults = request.Rooms[result.RoomNumber - 1].Guests.AdultsCount;
                        result.Children = (byte)(request.Rooms[result.RoomNumber - 1].Guests.ChildrenCount + request.Rooms[result.RoomNumber - 1].Guests.InfantsCount);
                        

                        result.PaymentModel = PaymentModel.PostPayment;
                        result.RateType = RateType.NetStandard;
                        result.IsOpaqueRate = false;
                        result.IsNonRefundable = false;
                        result.NumberOfAvailableRooms = 0;
                        //result.CommissionAmount = new Money { Amount = 0, CurrencyCode = "" };

                        result.ProviderSpecificData = new Dictionary<string, string>();
                        result.ProviderSpecificData.Add("BonotelRoomTypeCode", roomInfo.roomTypeCode);
                        result.ProviderSpecificData.Add("BonotelBedTypeCode", roomInfo.bedTypeCode);
                        result.ProviderSpecificData.Add("BonotelCurrencyCode", hotel.rateCurrencyCode);

                        availabilityResults.Enqueue(result);
                    }
                }
            }
        }

        private static bool HasResults(availabilityResponse supplierResponse)
        {

            if (supplierResponse != null && supplierResponse.status != "N")
            {
                if (supplierResponse.hotelList.Any())
                    return true;
                else
                    return false;
            }

            return false;
        }

    }
}
