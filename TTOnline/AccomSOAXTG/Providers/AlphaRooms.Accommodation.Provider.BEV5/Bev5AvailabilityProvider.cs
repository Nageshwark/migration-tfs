﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5AvailabilityProvider:AccommodationAvailabilitySearchBase
    {
        private IAccommodationAvailabilityAutomatorAsync<IEnumerable<AvailabilityRoomsResponse>> automator;
        private IAccommodationAvailabilityParser<IEnumerable<AvailabilityRoomsResponse>> parser;

        public Bev5AvailabilityProvider(IAccommodationAvailabilityAutomatorAsync<IEnumerable<AvailabilityRoomsResponse>> automator, IAccommodationAvailabilityParser<IEnumerable<AvailabilityRoomsResponse>>  parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderAvailabilityResult>> GetProviderAvailabilityAsync(AccommodationProviderAvailabilityRequest request)
        {
            var response = await automator.GetAvailabilityResponseAsync(request);
            return parser.GetAvailabilityResults(request, response);
        }
    }
}
