﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using Money = AlphaRooms.SOACommon.Contracts.Money;
using PaymentModel = AlphaRooms.SOACommon.Contracts.Enumerators.PaymentModel;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5AvailabilityParser : IAccommodationAvailabilityParser<IEnumerable<AvailabilityRoomsResponse>>
    {
        public const string Bev5BindingBindingRateSuppliers = "Bev5BindingCheckExcludedSuppliers";
        public const string Bev5SupplierNameTag = "Bev5SupplierNameTag";        
        private readonly IBev5BindingRateService bindingRateService;
        private readonly IProviderNonRefundableService nonRefundableService;
        private readonly IBev5SalePriceService salePriceService;
        private readonly IBev5RateTypeService rateTypeService;

        public Bev5AvailabilityParser(IBev5BindingRateService bindingRateService, IProviderNonRefundableService nonRefundableService, IBev5SalePriceService bev5SalePriceService
            , IBev5SalePriceService salePriceService, IBev5RateTypeService rateTypeService)
        {
            this.bindingRateService = bindingRateService;
            this.nonRefundableService = nonRefundableService;
            this.salePriceService = salePriceService;
            this.rateTypeService = rateTypeService;
        }

        public IEnumerable<AccommodationProviderAvailabilityResult> GetAvailabilityResults(AccommodationProviderAvailabilityRequest request, IEnumerable<AvailabilityRoomsResponse> responses)
        {
            if (responses == null) throw new SupplierApiDataException("BEV5 Availability Response is null");
            var availabilityResults = new ConcurrentQueue<AccommodationProviderAvailabilityResult>();
            try
            {
               
                var bev5Tag = request.Provider.Parameters.GetParameterValue(Bev5SupplierNameTag);
                var bindingRateSuppliers = request.Provider.Parameters.GetParameterValue(Bev5BindingBindingRateSuppliers).Split(';');
                Sync.ParallelForEachIgnoreFailed(responses, response => CreateAvailabilityResult(response, request, bindingRateSuppliers, bev5Tag, availabilityResults));
            }
            catch (Exception ex)
            {
                throw new SupplierApiDataException("BEV5 Availability Provider Data exception - " + ex.Message + " Stack: " + ex.StackTrace, ex);
            }
            return availabilityResults;
        }

        private void CreateAvailabilityResult(AvailabilityRoomsResponse bev5Response, AccommodationProviderAvailabilityRequest request, string[] bindingRateSuppliers
            , string bev5Tag, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            Sync.ParallelForEachIgnoreFailed(bev5Response.AvailabilityRooms, bev5Result => CreateBev5AvailabilityResult(bev5Result, request, bindingRateSuppliers, bev5Tag, availabilityResults));
        }

        private void CreateBev5AvailabilityResult(AvailabilityRoom bev5Result, AccommodationProviderAvailabilityRequest request, string[] bindingRateSuppliers, string bev5Tag, ConcurrentQueue<AccommodationProviderAvailabilityResult> availabilityResults)
        {
            availabilityResults.Enqueue(new AccommodationProviderAvailabilityResult
            {
                RoomNumber = (byte)bev5Result.RoomNumber
                , ProviderEdiCode = request.Provider.EdiCode
                , ProviderName = (string.IsNullOrEmpty(bev5Tag) ? bev5Result.Supplier : bev5Result.Supplier + bev5Tag)
                , ProviderFilterName = (string.IsNullOrEmpty(bev5Tag) ? bev5Result.Supplier : bev5Result.Supplier + bev5Tag)
                , SupplierEdiCode = bev5Result.EdiSource
                , DestinationEdiCode = bev5Result.DestinationID
                , EstablishmentEdiCode = bev5Result.EstablishmentKey.ToString()
                , ProviderEstablishmentCode = bev5Result.EdiCode
                , ProviderProviderCode = bev5Result.EdiSource
                , CheckInDate = request.CheckInDate
                , CheckOutDate = request.CheckOutDate
                , Adults = (byte)bev5Result.RoomAdults
                , Children = (byte)bev5Result.RoomChildren
                //Infants = TODO
                , BoardCode = bev5Result.BoardType.ToString() // This is the boardType from Bev5
                , BoardDescription = bev5Result.BoardDescription
                , RoomCode = bev5Result.RoomCode
                , RoomDescription = bev5Result.RoomDescription
                //NumberOfAvailableRooms = TODO
                , CostPrice = new Money(bev5Result.CostPrice, bev5Result.Currency)
                , SalePrice = salePriceService.GetAvailabilitySalePrice(bev5Result)
                , CommissionAmount = new Money(bev5Result.Margin, bev5Result.Currency)
                //TODO: Check if its Currency or SaleCurrency
                , IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(bev5Result.RoomDescription, bev5Result.BoardDescription, !bev5Result.IsRefundableRate)
                , IsBindingRate = bindingRateService.GetAvailabilityIsBindingRate(bev5Result, bindingRateSuppliers)
                , IsOpaqueRate = bev5Result.IsOpaqueRate
                , PaymentModel = (PaymentModel) bev5Result.PaymentModel
                , RateType = rateTypeService.GetAvailabilityRateType(bev5Result)
                , Contract = bev5Result.Contract
                , ProviderSpecificData = CreateProviderSpecificData(bev5Result)
            });
        }

        private Dictionary<string, string> CreateProviderSpecificData(AvailabilityRoom bev5Result)
        {
            var psd = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(bev5Result.IncomingContract))
            {
                psd.Add("IC", bev5Result.IncomingContract);
            }
            if (!String.IsNullOrEmpty(bev5Result.DestinationKey))
            {
                psd.Add("PDK", bev5Result.DestinationKey);
            }
            if (!String.IsNullOrEmpty(bev5Result.DestinationName))
            {
                psd.Add("DN", bev5Result.DestinationName);
            }
            if (!String.IsNullOrEmpty(bev5Result.BoardCode))
            {
                psd.Add("PBC", bev5Result.BoardCode);
            }
            if (!String.IsNullOrEmpty(bev5Result.Description))
            {
                psd.Add("PRD", bev5Result.Description);
            }
            if (!String.IsNullOrEmpty(bev5Result.RoomType))
            {
                psd.Add("RT", bev5Result.RoomType);
            }
            return psd;
        }
    }
}
