﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Utilities.ExtensionMethods;
using System.ServiceModel;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5CancellationAutomator : IAccommodationCancellationAutomatorAsync<CancelAccommodationResponse>
    {
        private const string Bev5MaxReceivedMessageSize = "Bev5MaxReceivedMessageSize";
        private readonly IBev5RequestFactory bev5RequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;
        private readonly IBev5EndpointService endpointService;

        public Bev5CancellationAutomator(IBev5RequestFactory bev5RequestFactory, IProviderOutputLogger outputLogger, IProviderLoggerService loggerService,
                        IBev5EndpointService endpointService)
        {
            this.bev5RequestFactory = bev5RequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
            this.endpointService = endpointService;
        }

        public async Task<CancelAccommodationResponse> GetCancellationResponseAsync(AccommodationProviderCancellationRequest request)
        {
            CancelAccommodationRequest bev5Request = null;
            try
            {
                var binding = new BasicHttpBinding() { MaxReceivedMessageSize = int.Parse(request.Provider.Parameters.GetParameterValue(Bev5MaxReceivedMessageSize)) };
                var address = new EndpointAddress(this.endpointService.GetEndpoint());
                var client = new Bev5SOAServiceClient(binding, address);
                bev5Request = bev5RequestFactory.CreateBev5CancellationRequest(request);
                this.loggerService.SetProviderRequest(request, bev5Request);
                var results = await client.CancelAccommodationBookingAsync(bev5Request);
                this.loggerService.SetProviderResponse(request, results);
                this.outputLogger.LogCancellationResponse(request, ProviderOutputFormat.Xml, results);
                return results;
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("BEV5 Cancellation Provider api exception. BEV5 Request - " + (bev5Request != null ? bev5Request.XmlSerialize() : null), ex);
            }
        }
    }
}
