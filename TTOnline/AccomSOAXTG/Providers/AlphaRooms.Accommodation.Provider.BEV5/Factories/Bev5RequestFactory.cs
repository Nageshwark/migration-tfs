﻿namespace AlphaRooms.Accommodation.Provider.BEV5.Factories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
    using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.Utilities;
    using Channel = AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService.Channel;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;

    public class Bev5RequestFactory:IBev5RequestFactory
    {
        private const string Bev5EdiProvidersToSearch = "Bev5EdiProvidersToSearch";
        private const string Bev5SearchesToSplit = "Bev5SearchesToSplit";
        private readonly IDestinationRepository destinationRepository;

        public Bev5RequestFactory(IDestinationRepository destinationRepository)
        {
            this.destinationRepository = destinationRepository;
        }

        public async Task<SearchRoomsRequest[]> CreateBev5SearchRequestAsync(AccommodationProviderAvailabilityRequest request)
        {
            var allBev5Suppliers = request.Provider.Parameters.GetParameterValue(Bev5EdiProvidersToSearch).Split(',');
            var bev5Suppliers= allBev5Suppliers.SplitToArrays(Convert.ToInt32(request.Provider.Parameters.GetParameterValue(Bev5SearchesToSplit)));
            var requests= await Async.ParallelForEach(bev5Suppliers, async(x)=> new SearchRoomsRequest
            {
                AvailabilityRequest = CreateBev5AvailabilityRequest(request, await GetBev5DestinationId(request)),
                IsProviderMode = true,
                SupplierInfo = CreateSupplierInfo(x.ToArray())
            });
            return requests.ToArray();
        }
   
        private async Task<string> GetBev5DestinationId(AccommodationProviderAvailabilityRequest request)
        {
            if (request.DestinationCodes.IsEmpty() && request.EstablishmentCodes.Any())
            {
                return (await destinationRepository.GetByEstablishmentIdAsync(Guid.Parse(request.EstablishmentCodes[0]))).DestinationId.ToString();
            }
            return request.DestinationCodes.FirstOrDefault();
        }

        private AccommodationSearchRequest CreateBev5AvailabilityRequest(AccommodationProviderAvailabilityRequest request, string bev5DestinationId)
        {
            return new AccommodationSearchRequest
            {
                Channel = (Channel)request.ChannelInfo.Channel
                , CheckinDate = request.CheckInDate
                , CheckoutDate = request.CheckOutDate
                , DestinationID = bev5DestinationId
                , ResortID = string.Empty
                , Rooms = CreateBev5RoomRequest(request.Rooms)
                , EstablishmentID = (request.EstablishmentCodes.Any() ? Guid.Parse(request.EstablishmentCodes.FirstOrDefault()) : Guid.Empty)
                , Site = Site.AlphaRooms
                , LocaleID = request.ChannelInfo.LocaleId
                , Currency = request.ChannelInfo.CurrencyCode // "GBP", //TODO: check this
                , AccomSearchResultsKey = request.AvailabilityId
                , IsPackageSearch = request.SearchType == SearchType.FlightAndHotel
            };
        }

        public EvaluateRoomsRequest CreateBev5ValuationRequest(AccommodationProviderValuationRequest request)
        {
            return new EvaluateRoomsRequest
            {
                AccommSearchResultsKey = Guid.NewGuid() //TODO 
                , AccomValuationResultsKey = Guid.NewGuid() // TODO 
                , ValuationRequests = CreateValuationRequests(request)
                , SelectedRooms = GetSelectedRooms(request.SelectedRooms)                
            };
        }

        public AccommodationBookingRequest CreateBev5BookingRequest(AccommodationProviderBookingRequest request)
        {
            var bookingRequest = new AccommodationBookingRequest();
            var leadPassenger = request.Customer;
            bookingRequest.Channel = (Channel) request.ChannelInfo.Channel;
            bookingRequest.Site = (Site)request.ChannelInfo.Site;
            bookingRequest.RetailIsoCurrency = "GBP"; // TODO
            bookingRequest.InIsoLanguage = "en"; 
            bookingRequest.InvoiceId = (int) request.ItineraryId;
            bookingRequest.ItineraryReference = request.ItineraryReference;
            bookingRequest.Rooms = request.ValuatedRooms.Select(valuatedRoom =>
            {
                if (valuatedRoom.Guests == null || valuatedRoom.Guests.IsEmpty())
                    throw new Exception("Could not find Guest details on Valuated Rooms");

                var valuationRoom = valuatedRoom.ValuationResult;
                return new BookRoomRequest
                {
                    Room = new Room
                    {
                        Supplier = valuationRoom.ProviderName,
                        SupplierEdiCode = valuationRoom.SupplierEdiCode,
                        CostPrice = GetPrice(valuationRoom.CostPrice.CurrencyCode,valuationRoom.CostPrice.Amount),
                        Margin = 0, //TODO 
                        IncomingContract = valuationRoom.ProviderSpecificData.ContainsKey("IC") ? valuationRoom.ProviderSpecificData["IC"] : string.Empty,
                        Contract = valuationRoom.Contract,
                        PaymentModel = (PaymentModel) valuationRoom.PaymentModel,
                        IsBindingRate = valuationRoom.IsBindingRate,
                        EstablishmentEdiCode = valuationRoom.ProviderEstablishmentCode,
                        EstablishmentKey = valuationRoom.EstablishmentEdiCode,
                        EstablishmentName = valuationRoom.EstablishmentName,
                        DestinationEdiCode = valuationRoom.ProviderSpecificData.ContainsKey("PDK") ? valuationRoom.ProviderSpecificData["PDK"] : string.Empty,
                        DestinationKey = valuationRoom.ProviderSpecificData.ContainsKey("PDK") ? valuationRoom.ProviderSpecificData["PDK"] : string.Empty,
                        Destination = valuationRoom.ProviderSpecificData.ContainsKey("DN") ? valuationRoom.ProviderSpecificData["DN"] : string.Empty,
                        RoomNumber = valuatedRoom.RoomNumber,
                        FromDate = valuationRoom.CheckInDate,
                        ToDate = valuationRoom.CheckOutDate,
                        NumberOfAdults = valuationRoom.Adults,
                        NumberOfChildren = valuationRoom.Children + valuationRoom.Infants,
                        BoardCode = valuationRoom.BoardCode,
                        BoardDescription = valuationRoom.BoardDescription,
                        RoomCode = valuationRoom.RoomCode,
                        RoomDescription = valuationRoom.RoomDescription,
                        RoomType = valuationRoom.ProviderSpecificData.ContainsKey("RT") ? valuationRoom.ProviderSpecificData["RT"] : string.Empty,
                        RoomVccPaymentTerms = valuationRoom.VirtualCreditCardPaymentTerms.Select(x => new RoomVccPaymentTerm
                        {
                            Amount = x.Price.Amount, Currency = x.Price.CurrencyCode, PaymentDate = x.PaymentDate
                        }).ToArray()
                        //ValuationFileNumber = TODO
                    },

                    RoomBookingDetails = new RoomBookingDetail
                    {
                        LeadPassengerTitle = (!string.IsNullOrEmpty(valuatedRoom.Guests[0].TitleString)? valuatedRoom.Guests[0].TitleString : leadPassenger.TitleString),
                        LeadPassengerFirstName = (!string.IsNullOrEmpty(valuatedRoom.Guests[0].FirstName)? valuatedRoom.Guests[0].FirstName: leadPassenger.FirstName),
                        LeadPassengerSurName = (!string.IsNullOrEmpty(valuatedRoom.Guests[0].Surname)? valuatedRoom.Guests[0].Surname:leadPassenger.Surname),
                        LeadPassengerContactNumber = leadPassenger.ContactNumber,
                        LateArrival = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.LateArrival,
                        Cot = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.CotRequired,
                        SeaViews = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.SeaViews,
                        AdjoiningRooms = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.AdjoiningRooms,
                        NonSmoking = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.NonSmoking,
                        DisabledFriendly = valuatedRoom.SpecialRequests != null && valuatedRoom.SpecialRequests.DisabledAccess,
                        OtherRequests = valuatedRoom.SpecialRequests != null ? valuatedRoom.SpecialRequests.OtherRequests : String.Empty
                    },

                    //TODO check if Age is always entered
                    Persons = valuatedRoom.Guests.Select(p => new Person
                    {
                        IsChild = (p.Type == GuestType.Child || p.Type == GuestType.Infant) ? true : false,
                        Id = p.Id,
                        Title = p.TitleString,
                        FirstName = p.FirstName,
                        Surname = p.Surname,
                        AgeAtTimeOfTravel = (short) p.Age
                    
                    }).ToArray(),

                    CardDetails = CreateCardDetails(request, valuatedRoom.PaymentDetails)
                };
            }).ToArray();
            
            return bookingRequest;
        }

        private CardDetails CreateCardDetails(AccommodationProviderBookingRequest request, AccommodationProviderBookingRequestRoomPaymentDetails paymentDetails)
        {
            if (paymentDetails == null)
            {
                return null;
            }

            return new CardDetails
            {
                CardTypek__BackingField = (CardType)paymentDetails.CardType,
                CardNumberk__BackingField = paymentDetails.CardNumber,
                // if this is a PayPal payment the CardExpireDate will be null
                ExpiryMonthk__BackingField = paymentDetails.CardExpireDate != null ? paymentDetails.CardExpireDate.Month : 1,
                ExpiryYeark__BackingField = paymentDetails.CardExpireDate != null ? paymentDetails.CardExpireDate.Year : 1,
                CardHolderNamek__BackingField = paymentDetails.CardHolderName,
                Cvck__BackingField = paymentDetails.CardSecurityCode,
                AddressLine1k__BackingField = paymentDetails.AddressLine1,
                AddressLine2k__BackingField = paymentDetails.AddressLine2,
                Cityk__BackingField = paymentDetails.City,
                Countryk__BackingField = paymentDetails.Country,
                PostalCodek__BackingField = paymentDetails.Postcode,
                EmailAddressk__BackingField = request.Customer.EmailAddress,
                CountryCodek__BackingField = request.ChannelInfo.CountryCode,
                Countyk__BackingField = paymentDetails.County
            };
        }

        public CancelAccommodationRequest CreateBev5CancellationRequest(AccommodationProviderCancellationRequest request)
        {
            return new CancelAccommodationRequest
            {
                ArrivalDatek__BackingField = request.CheckInDate,
                DepartureDatek__BackingField = request.CheckOutDate,
                BookingReferenceCodek__BackingField = request.ProviderBookingReference,
                SupplierPropertyCodek__BackingField = request.EstablishmentEdiCode,
                EdiProviderk__BackingField = request.Provider.EdiCode, // TODO : set the edisource code of the provider here
                ItineraryIdk__BackingField = request.ItineraryId,
                FirstNamek__BackingField = request.CustomerFirstName,
                LastNamek__BackingField = request.CustomerSurName
            };
        }

        private AvailabilityRoom[] GetSelectedRooms(AccommodationProviderValuationRequestRoom[] selectedRooms)
        {
            return selectedRooms.Select(i => CreateAvailabilityRoom(i.AvailabilityResult)).ToArray();
        }

        private AvailabilityRoom CreateAvailabilityRoom(AccommodationProviderAvailabilityResult selectedRoom)
        {
            return new AvailabilityRoom
            {
                EstablishmentKey = new Guid(selectedRoom.EstablishmentEdiCode),
                RoomId = selectedRoom.RoomNumber,
                RoomNumber = selectedRoom.RoomNumber,
                Supplier = selectedRoom.ProviderName,
                PaymentModel = (PaymentModel)selectedRoom.PaymentModel,
                EdiCode = selectedRoom.ProviderEstablishmentCode,
                EdiSource = selectedRoom.SupplierEdiCode,
                Description = selectedRoom.ProviderSpecificData.ContainsKey("PRD") ? selectedRoom.ProviderSpecificData["PRD"] : string.Empty,
                LongDescription = string.Empty,
                IncomingContract = selectedRoom.ProviderSpecificData.ContainsKey("IC") ? selectedRoom.ProviderSpecificData["IC"] : string.Empty,
                Contract = selectedRoom.Contract,
                RoomAdults = selectedRoom.Adults,
                RoomChildren = selectedRoom.Children + selectedRoom.Infants,
                DestinationID = selectedRoom.DestinationEdiCode,
                DestinationKey = selectedRoom.ProviderSpecificData.ContainsKey("PDK") ? selectedRoom.ProviderSpecificData["PDK"] : string.Empty,
                DestinationName = selectedRoom.ProviderSpecificData.ContainsKey("DN") ? selectedRoom.ProviderSpecificData["DN"] : string.Empty,
                ZoneKey = "", // TODO
                ZoneName = "", //TODO
                CategoryKey = "", // TODO
                CategoryDescription = "", //TODO
                BoardCode = selectedRoom.ProviderSpecificData.ContainsKey("PBC") ? selectedRoom.ProviderSpecificData["PBC"] : String.Empty,
                BoardType = GetBoardType(selectedRoom.BoardCode),
                BoardDescription = selectedRoom.BoardDescription,
                OriginalBoardDescription = selectedRoom.BoardDescription,
                RoomType = selectedRoom.ProviderSpecificData.ContainsKey("RT") ? selectedRoom.ProviderSpecificData["RT"] : string.Empty,
                RoomCode = selectedRoom.RoomCode,
                RoomDescription = selectedRoom.RoomDescription,
                OriginalRoomDescription = selectedRoom.RoomDescription,
                IsoLanguage = "", //TODO
                IsBindingRate = selectedRoom.IsBindingRate,
                IsRefundableRate = (bool)!(selectedRoom.IsNonRefundable),
                IsOpaqueRate = selectedRoom.IsOpaqueRate,
                //AmountType = TODO
                Currency = selectedRoom.CostPrice.CurrencyCode,
                SaleCurrency = selectedRoom.SalePrice.CurrencyCode,
                CostPrice = selectedRoom.CostPrice.Amount,
                TotalSaleAmount = selectedRoom.SalePrice.Amount,
                TotalDiscountAmount = 0,
                SelectedRoomCost = selectedRoom.CostPrice.Amount,
                Margin = 0, //TODO 
            };
        }

        private SearchSupplierInfo[] CreateSupplierInfo(string[] bev5EdiProvidersToSearch)
        {
            return bev5EdiProvidersToSearch.Select(providerCode => new SearchSupplierInfo {Code = providerCode}).ToArray();
        }

        private static RoomSearch[] CreateBev5RoomRequest(IEnumerable<AccommodationProviderAvailabilityRequestRoom> roomRequests)
        {
            return roomRequests.Select(GetBev5RoomSearch).ToArray();
        }

        private static RoomSearch GetBev5RoomSearch(AccommodationProviderAvailabilityRequestRoom roomRequest)
        {
            return new RoomSearch
            {
                People = roomRequest.Guests.Select(GetBev5Person).ToArray()
            };
        }

        private static Person GetBev5Person(AccommodationProviderAvailabilityRequestGuest guest)
        {
            return new Person
            {
                IsChild = guest.Type == GuestType.Child || guest.Type == GuestType.Infant
                , AgeAtTimeOfTravel = (short)guest.Age
            };
        }

        private AccommodationValuationRequest[] CreateValuationRequests(AccommodationProviderValuationRequest request)
        {
            return request.SelectedRooms.Select(i => CreateAccommodationValuationRequest(request, i)).ToArray();
        }

        private AccommodationValuationRequest CreateAccommodationValuationRequest(AccommodationProviderValuationRequest request, AccommodationProviderValuationRequestRoom room)
        {
            return new AccommodationValuationRequest
            {
                Channel = (Channel)request.ChannelInfo.Channel,
                EdiCode = room.AvailabilityResult.ProviderEstablishmentCode,
                EdiSource = room.AvailabilityResult.SupplierEdiCode,
                IncomingContract = room.AvailabilityResult.ProviderSpecificData.ContainsKey("IC") ? room.AvailabilityResult.ProviderSpecificData["IC"] : string.Empty,
                Contract = room.AvailabilityResult.Contract, 
                Currency = room.AvailabilityResult.SalePrice.CurrencyCode,
                Language = "EN", // TODO check this
                RoomNumber = room.RoomNumber,
                RoomId = room.RoomNumber,
                CheckinDate = room.AvailabilityResult.CheckInDate,
                CheckoutDate = room.AvailabilityResult.CheckOutDate,
                //ParentDestinationId = TODO
                DestinationId = room.AvailabilityResult.DestinationEdiCode,
                Resort = room.AvailabilityResult.ProviderSpecificData.ContainsKey("PDK") ? room.AvailabilityResult.ProviderSpecificData["PDK"] : string.Empty,
                RoomCode = room.AvailabilityResult.RoomCode,
                RoomDescription = room.AvailabilityResult.RoomDescription,
                OriginalRoomDescription = room.AvailabilityResult.RoomDescription,
                BoardCode = room.AvailabilityResult.ProviderSpecificData.ContainsKey("PBC") ? room.AvailabilityResult.ProviderSpecificData["PBC"] : string.Empty, //room.AvailabilityResult.BoardCode,
                BoardDescription = room.AvailabilityResult.BoardDescription,
                OriginalBoardDescription = room.AvailabilityResult.BoardDescription,
                NumberOfAdults = room.AvailabilityResult.Adults,
                NumberOfChildren = room.AvailabilityResult.Children + room.AvailabilityResult.Infants,
                ChildrenAges = room.Guests.ChildAndInfantAges.Select(i => (int)i).ToArray(),
                //IsBot = TODO
                EstablishmentKey = new Guid(room.AvailabilityResult.EstablishmentEdiCode) //TODO: is this the establishemnt id in alpha db?
            };
        }

        private Money GetPrice(string currency, decimal amount)
        {
            return new Money
            {
                Currency = currency
                , Value = amount
            };
        }

        private string GetSupplierSpecificSupplierData(string ediProvider, string ediDestinationId, string isoCountryCode, string isoLanguage)
        {
            // Set SupplierData to "isoCountryCode;ediDestinationId" for Zenacomm, Riu & Laterooms.
            if ((ediProvider == "14") || (ediProvider == "RU") || (ediProvider == "LR"))
            {
                return string.Format("{0};{1}", isoCountryCode, ediDestinationId);
            }

            if (ediProvider.ToUpper() == "Y")
            {
                // Set SupplierData to "isoCountryCode;ediDestinationId" for Jumbo when its searching in English for a Spanish destination.
                if (isoCountryCode.ToLower() == "es" && System.Threading.Thread.CurrentThread.CurrentCulture.LCID == 2057)
                {
                    return string.Format("{0};{1}", isoCountryCode, ediDestinationId);
                }

                // Set the SupplierData to "isoLanguage;ediDestinationId" for any other Jumbo results.
                return string.Format("{0};{1}", isoLanguage, ediDestinationId);
            }

            // Set SupplierData to "ediDestinationId" for any other supplier.
            return ediDestinationId;
        }

        private BoardType GetBoardType(string boardType)
        {
            if (boardType.Contains("RoomOnly") || boardType.Contains("RO")) return BoardType.RoomOnly;
            else if (boardType.Contains("SelfCatering") || boardType.Contains("SC")) return BoardType.SelfCatering;
            else if (boardType.Contains("Breakfast") || boardType.Contains("BB")) return BoardType.Breakfast;
            else if (boardType.Contains("HalfBoard") || boardType.Contains("HB")) return BoardType.HalfBoard;
            else if (boardType.Contains("FullBoard") || boardType.Contains("FB")) return BoardType.FullBoard;
            else if (boardType.Contains("AllInclusive") || boardType.Contains("AI")) return BoardType.AllInclusive;
            return BoardType.RoomOnly;
        }
    }
}
