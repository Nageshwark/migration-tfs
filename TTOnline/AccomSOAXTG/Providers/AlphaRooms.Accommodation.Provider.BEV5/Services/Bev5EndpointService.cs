﻿namespace AlphaRooms.Accommodation.Provider.BEV5.Services
{
    using System.Configuration;
    using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;

    public class Bev5EndpointService : IBev5EndpointService
    {
        public string GetEndpoint()
        {
            const string EndpointKey = "Bev5SOA_Endpoint";
            const string TemplateKey = "Bev5SOA_Endpoint_Template";

            var address = ConfigurationManager.AppSettings[EndpointKey];
            var template = ConfigurationManager.AppSettings[TemplateKey];

            string endpoint = null;

            if (!string.IsNullOrEmpty(address) && !string.IsNullOrEmpty(template))
            {
                endpoint = string.Format(template, address);
            }

            return endpoint;
        }
    }
}
