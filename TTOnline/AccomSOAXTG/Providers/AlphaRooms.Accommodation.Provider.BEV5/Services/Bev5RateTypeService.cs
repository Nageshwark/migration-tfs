﻿using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using RateType = AlphaRooms.SOACommon.Contracts.Enumerators.RateType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Services
{
    public class Bev5RateTypeService : IBev5RateTypeService
    {
        public SOACommon.Contracts.Enumerators.RateType GetAvailabilityRateType(Bev5SOAService.AvailabilityRoom bev5Result)
        {
            return GetRateType(bev5Result.PaymentModel, bev5Result.IsBindingRate, bev5Result.CostPrice, bev5Result.TotalSaleAmount);
        }
        
        public RateType GetValuationRateType(Bev5SOAService.ValuationRoomResult bev5Result)
        {
            return GetRateType(bev5Result.Room.PaymentModel, bev5Result.Room.IsBindingRate, bev5Result.Room.CostPrice, bev5Result.Room.TotalSaleAmount);
        }

        private RateType GetRateType(PaymentModel paymentModel, bool isBindingRate, decimal costPrice, decimal totalSaleAmount)
        {
            if (paymentModel == Bev5SOAService.PaymentModel.CustomerPayDirect) return RateType.GrossPayDirect;
            else if (costPrice == totalSaleAmount) return isBindingRate ? RateType.NetBinding : RateType.NetStandard;
            else if (costPrice != totalSaleAmount && totalSaleAmount == 0) return isBindingRate ? RateType.NetBinding : RateType.NetStandard;
            else if (costPrice != totalSaleAmount && totalSaleAmount > 0) return isBindingRate ? RateType.GrossStandard : RateType.GrossNonBinding;
            return RateType.NetStandard;
        }
    }
}
