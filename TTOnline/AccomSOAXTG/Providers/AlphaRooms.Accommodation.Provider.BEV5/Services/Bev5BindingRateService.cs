﻿using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Services
{
    public class Bev5BindingRateService : IBev5BindingRateService
    {
        public bool GetAvailabilityIsBindingRate(Bev5SOAService.AvailabilityRoom bev5Result, string[] bindingRateCheckExcludeSuppliers)
        {
            bev5Result.IsBindingRate= GetBindingRate(bev5Result.IsBindingRate, bev5Result.EdiSource, bev5Result.PaymentModel, bindingRateCheckExcludeSuppliers);
            return bev5Result.IsBindingRate;
        }

        public bool GetValuationIsBindingRate(Bev5SOAService.AvailabilityRoom availabilityRoom, string[] bindingRateCheckExcludeSuppliers)
        {
            availabilityRoom.IsBindingRate= GetBindingRate(availabilityRoom.IsBindingRate, availabilityRoom.EdiSource, availabilityRoom.PaymentModel, bindingRateCheckExcludeSuppliers);
            return availabilityRoom.IsBindingRate;
        }

        private bool GetBindingRate(bool isBindingRateFromProvider, string supplierEdiCode, Bev5SOAService.PaymentModel paymentModel, string[] bindingRateCheckExcludeSuppliers)
        {
            bool isBindingRate = false;
            if (paymentModel == Bev5SOAService.PaymentModel.CustomerPayDirect || supplierEdiCode=="EX")//Hack for Expedia through Bev5
                isBindingRate= true;
            else if (bindingRateCheckExcludeSuppliers.Contains(supplierEdiCode))
                isBindingRate = isBindingRateFromProvider;

            return isBindingRate;
        }
    }
}
