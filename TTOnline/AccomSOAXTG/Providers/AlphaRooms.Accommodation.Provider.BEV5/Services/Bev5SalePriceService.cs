﻿using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Services
{
    public class Bev5SalePriceService : IBev5SalePriceService
    {
        public Money GetAvailabilitySalePrice(Bev5SOAService.AvailabilityRoom bev5Result)
        {
            return GetSalePrice(bev5Result.CostPrice, bev5Result.Currency, bev5Result.TotalSaleAmount, bev5Result.SaleCurrency);
        }
        
        public Money GetValuationSalePrice(Core.Provider.Contracts.AccommodationProviderValuationRequestRoom roomRequest, Bev5SOAService.ValuationRoomResult roomResult)
        {
            return GetSalePrice(roomResult.Room.CostPrice, roomResult.Room.Currency, roomResult.Room.TotalSaleAmount, roomResult.Room.SaleCurrency);
        }

        private Money GetSalePrice(decimal costPrice, string currency, decimal totalSaleAmount, string saleCurrency)
        {
            if (totalSaleAmount > 0 && totalSaleAmount != costPrice && saleCurrency != null)
            {
                return new Money(totalSaleAmount, saleCurrency);
            }
            return new Money(costPrice, currency);
        }
    }
}
