﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Provider;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5ValuationProvider : AccommodationValuationBase
    {
        private IAccommodationValuationAutomatorAsync<ValuationRoomResponse> automator;
        private IAccommodationValuationParser<ValuationRoomResponse> parser;

        public Bev5ValuationProvider(IAccommodationValuationAutomatorAsync<ValuationRoomResponse> automator, IAccommodationValuationParser<ValuationRoomResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderValuationResult>> GetProviderValuationAsync(AccommodationProviderValuationRequest request)
        {
            var response = await automator.GetValuationResponseAsync(request);
            return parser.GetValuationResults(request, response);
        }
    }
}
