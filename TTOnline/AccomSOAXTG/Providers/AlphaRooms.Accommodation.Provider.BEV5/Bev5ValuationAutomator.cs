﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using System.ServiceModel;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5ValuationAutomator : IAccommodationValuationAutomatorAsync<ValuationRoomResponse>
    {
        private const string Bev5MaxReceivedMessageSize = "Bev5MaxReceivedMessageSize";
        private readonly IBev5RequestFactory bev5RequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;
        private readonly IBev5EndpointService endpointService;

        public Bev5ValuationAutomator(IBev5RequestFactory bev5RequestFactory, IProviderLoggerService loggerService, IProviderOutputLogger outputLogger, 
            IBev5EndpointService endpointService)
        {
            this.bev5RequestFactory = bev5RequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
            this.endpointService = endpointService;
        }

        public async Task<ValuationRoomResponse> GetValuationResponseAsync(AccommodationProviderValuationRequest request)
        {
            EvaluateRoomsRequest bev5Request = null;
            try
            {
                var binding = new BasicHttpBinding() { MaxReceivedMessageSize = int.Parse(request.Provider.Parameters.GetParameterValue(Bev5MaxReceivedMessageSize)) };
                var address = new EndpointAddress(this.endpointService.GetEndpoint());
                var client = new Bev5SOAServiceClient(binding, address);
                bev5Request = bev5RequestFactory.CreateBev5ValuationRequest(request);
                this.loggerService.SetProviderRequest(request, bev5Request);
                var results = await client.EvaluateRoomsAsync(bev5Request);
                this.loggerService.SetProviderResponse(request, results);
                this.outputLogger.LogValuationResponse(request, ProviderOutputFormat.Xml, results);
                // Check that the number of valuation results returned from Bev5 matches the number of rooms that the user has selected.
                // Bev5 may, sometimes, return an empty list of ValuationRooms in the event of a valuation failure.
                if (results.ValuationRooms.Count() != request.SelectedRooms.Count())
                {
                    throw new Exception("Bev5SOA Valuation Failure: Valuation results not found for one or more rooms.");
                }
                return results;
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("BEV5 Valuation Provider api exception. BEV5 Request - " + (bev5Request != null ? bev5Request.XmlSerialize() : null), ex);
            }
        }
    }
}
