﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Money = AlphaRooms.SOACommon.Contracts.Money;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5CancellationParser : IAccommodationCancellationParser<CancelAccommodationResponse>
    {
        public IEnumerable<AccommodationProviderCancellationResult> GetCancellationResults(AccommodationProviderCancellationRequest request, CancelAccommodationResponse response)
        {
            if (response == null) throw new SupplierApiDataException("BEV5 Cancellation Response is null");
            try 
            {
                return new [] { CreateAccommodationProviderCancellationResult(response) }; 
            }
            catch (Exception ex)
            {
                throw new SupplierApiDataException("BEV5 Valuation Provider Data exception - " + ex.Message + " Stack: " + ex.StackTrace, ex);
            }
        }

        private AccommodationProviderCancellationResult CreateAccommodationProviderCancellationResult(CancelAccommodationResponse response)
        {
            return new AccommodationProviderCancellationResult
            {
                CancellationStatus = response.CancellationStatusk__BackingField == true ? CancellationStatus.Succeeded : CancellationStatus.Failed
                , ProviderCancellationReference = response.CancellationReferenceCodek__BackingField
                , Message = response.SupplierCommentsk__BackingField
                , CancellationCost = new Money(response.CancellationCostk__BackingField, "GBP") // TODO: check the currency, as in Bev5 we are setting only the amount field.
            };
        }
    }
}
