﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5BookingParser : IAccommodationBookingParser<BookRoomsResponse>
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;

        public Bev5BookingParser(ILogger logger, IAccommodationConfigurationManager configurationManager)
        {
            this.logger = logger;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<AccommodationProviderBookingResult> GetBookingResults(AccommodationProviderBookingRequest request, BookRoomsResponse responses)
        {
            if (responses == null) throw new SupplierApiDataException("BEV5 Booking Response is null");
            try
            {
                return Sync.ParallelForEach(responses.RoomBookingResults, roomResult => CreateBookingResultsFromResponse(request, roomResult));
            }
            catch (Exception ex)
            {
                throw new SupplierApiDataException("BEV5 Valuation Provider Data exception - " + ex.Message + " Stack: " + ex.StackTrace, ex);
            }
        }

        private AccommodationProviderBookingResult CreateBookingResultsFromResponse(AccommodationProviderBookingRequest request, RoomBookingResult bookingResult)
        {
            return new AccommodationProviderBookingResult
            {
                RoomNumber = (byte) bookingResult.RoomNumber
                , BookingStatus = bookingResult.IsSuccessful == true ? BookingStatus.Confirmed : BookingStatus.NotConfirmed
                , ProviderBookingReference = bookingResult.SupplierBookingReference
                , KeyCollectionInformation = bookingResult.KeyCollectionInformation
            };
        }
    }
}
