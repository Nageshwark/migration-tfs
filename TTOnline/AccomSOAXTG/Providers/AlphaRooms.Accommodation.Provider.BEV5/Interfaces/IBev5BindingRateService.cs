﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Interfaces
{
    public interface IBev5BindingRateService
    {
        bool GetAvailabilityIsBindingRate(Bev5SOAService.AvailabilityRoom bev5Result, string[] bindingRateSuppliers);
        bool GetValuationIsBindingRate(Bev5SOAService.AvailabilityRoom availabilityRoom, string[] bindingRateSuppliers);
    }
}
