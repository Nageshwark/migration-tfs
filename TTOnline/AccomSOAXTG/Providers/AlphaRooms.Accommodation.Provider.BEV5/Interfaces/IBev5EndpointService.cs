﻿namespace AlphaRooms.Accommodation.Provider.BEV5.Interfaces
{
    public interface IBev5EndpointService
    {
        string GetEndpoint();
    }
}
