﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Interfaces
{
    public interface IBev5SalePriceService
    {
        SOACommon.Contracts.Money GetAvailabilitySalePrice(Bev5SOAService.AvailabilityRoom bev5Result);
        SOACommon.Contracts.Money GetValuationSalePrice(Core.Provider.Contracts.AccommodationProviderValuationRequestRoom roomRequest, Bev5SOAService.ValuationRoomResult roomResult);
    }
}
