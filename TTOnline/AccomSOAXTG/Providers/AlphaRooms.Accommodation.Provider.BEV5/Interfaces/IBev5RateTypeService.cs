﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Provider.BEV5.Interfaces
{
    public interface IBev5RateTypeService
    {
        SOACommon.Contracts.Enumerators.RateType GetAvailabilityRateType(Bev5SOAService.AvailabilityRoom bev5Result);
        SOACommon.Contracts.Enumerators.RateType GetValuationRateType(Bev5SOAService.ValuationRoomResult bev5Result);
    }
}
