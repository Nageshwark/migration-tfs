﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;

namespace AlphaRooms.Accommodation.Provider.BEV5.Interfaces
{
    public interface IBev5RequestFactory
    {
        Task<SearchRoomsRequest[]> CreateBev5SearchRequestAsync(AccommodationProviderAvailabilityRequest request);
        EvaluateRoomsRequest CreateBev5ValuationRequest(AccommodationProviderValuationRequest request);
        AccommodationBookingRequest CreateBev5BookingRequest(AccommodationProviderBookingRequest request);
        CancelAccommodationRequest CreateBev5CancellationRequest(AccommodationProviderCancellationRequest request);
    }
}
