﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using Money = AlphaRooms.SOACommon.Contracts.Money;
using PaymentModel = AlphaRooms.SOACommon.Contracts.Enumerators.PaymentModel;
using AlphaRooms.Utilities.CustomExceptions;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5ValuationParser : IAccommodationValuationParser<ValuationRoomResponse>
    {
        public const string Bev5SupplierNameTag = "Bev5SupplierNameTag";
        public const string Bev5BindingBindingRateSuppliers = "Bev5BindingCheckExcludedSuppliers";
        private readonly ILogger logger;
        private readonly IBev5BindingRateService bindingRateService;
        private readonly IProviderNonRefundableService nonRefundableService;
        private readonly IBev5SalePriceService salePriceService;
        private readonly IBev5RateTypeService rateTypeService;

        public Bev5ValuationParser(ILogger logger, IBev5BindingRateService bindingRateService, IProviderNonRefundableService nonRefundableService, IBev5SalePriceService bev5SalePriceService
            , IBev5SalePriceService salePriceService, IBev5RateTypeService rateTypeService)
        {
            this.logger = logger;
            this.bindingRateService = bindingRateService;
            this.nonRefundableService = nonRefundableService;
            this.salePriceService = salePriceService;
            this.rateTypeService = rateTypeService;
        }

        public IEnumerable<AccommodationProviderValuationResult> GetValuationResults(AccommodationProviderValuationRequest request, Bev5SOAService.ValuationRoomResponse response)
        {
            if (response == null) throw new SupplierApiDataException("BEV5 Valuation Response is null");
            try
            {
                var bev5Tag = request.Provider.Parameters.GetParameterValue(Bev5SupplierNameTag);
                var bindingRateSuppliers = request.Provider.Parameters.GetParameterValue(Bev5BindingBindingRateSuppliers).Split(';');
                return Sync.ParallelForEach(response.ValuationRooms, room => CreateAccommodationProviderValuationResult(request
                    , request.SelectedRooms.First(i => i.RoomNumber == room.Room.RoomNumber), room, bindingRateSuppliers, bev5Tag));
            }
            catch (Exception ex)
            {
                throw new SupplierApiDataException("BEV5 Valuation Provider Data exception - " + ex.Message + " Stack: " + ex.StackTrace, ex);
            }
        }

        private AccommodationProviderValuationResult CreateAccommodationProviderValuationResult(AccommodationProviderValuationRequest request, AccommodationProviderValuationRequestRoom roomRequest
            , ValuationRoomResult roomResult, string[] bindingRateSuppliers, string bev5Tag)
        {
           return new AccommodationProviderValuationResult
            {
                RoomNumber = (byte)roomResult.Room.RoomNumber
                , ProviderEdiCode = request.Provider.EdiCode
                , ProviderName = (string.IsNullOrEmpty(bev5Tag) ? roomResult.Room.Supplier : (!roomResult.Room.Supplier.Contains(bev5Tag) ? roomResult.Room.Supplier + bev5Tag : roomResult.Room.Supplier))
                , SupplierEdiCode = roomResult.Room.EdiSource
                , DestinationEdiCode = roomRequest.AvailabilityResult.DestinationEdiCode
                , EstablishmentEdiCode = roomRequest.AvailabilityResult.EstablishmentEdiCode
                , EstablishmentName = roomRequest.AvailabilityResult.EstablishmentName
                , ProviderEstablishmentCode = roomResult.Room.EdiCode
                , ProviderProviderCode = roomResult.Room.EdiSource
                , CheckInDate = roomRequest.AvailabilityRequest.CheckInDate
                , CheckOutDate = roomRequest.AvailabilityRequest.CheckOutDate
                , Adults = (byte)roomResult.Room.RoomAdults
                , Children = (byte)roomResult.Room.RoomChildren
                //Infants = TODO
                , BoardCode = roomResult.Room.BoardCode
                , BoardDescription = roomResult.Room.BoardDescription
                , RoomCode = roomResult.Room.RoomCode
                , RoomDescription = roomResult.Room.Description
                //NumberOfAvailableRooms = TODO
                , CostPrice = new Money(roomResult.Room.CostPrice, roomResult.Room.Currency)
                , SalePrice = salePriceService.GetValuationSalePrice(roomRequest, roomResult)
                //CommissionAmount = TODO 
                , IsNonRefundable = nonRefundableService.IsDescriptionNonRefundable(roomResult.Room.Description, roomResult.Room.BoardDescription, !roomResult.Room.IsRefundableRate), IsBindingRate = bindingRateService.GetValuationIsBindingRate(roomResult.Room, bindingRateSuppliers)
                , IsOpaqueRate = roomResult.Room.IsOpaqueRate
                , PaymentModel = (PaymentModel)roomResult.Room.PaymentModel
                , RateType = rateTypeService.GetValuationRateType(roomResult)
                , Contract = roomResult.Room.Contract
                //IsSpecialRate = TODO
                , ProviderSpecificData = CreateProviderSpecificData(roomResult)
                , CancellationPolicy = new[] { roomResult.CancellationPolicy }
            };
        }

        private Dictionary<string, string> CreateProviderSpecificData(ValuationRoomResult roomResult)
        {
            var psd = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(roomResult.Room.IncomingContract))
            {
                psd.Add("IC", roomResult.Room.IncomingContract);
            }
            if (!String.IsNullOrEmpty(roomResult.Room.DestinationKey))
            {
                psd.Add("PDK", roomResult.Room.DestinationKey);
            }
            if (!String.IsNullOrEmpty(roomResult.Room.DestinationName))
            {
                psd.Add("DN", roomResult.Room.DestinationName);
            }
            if (!String.IsNullOrEmpty(roomResult.Room.RoomType))
            {
                psd.Add("RT", roomResult.Room.RoomType);
            }
            return psd;
        }
    }
}
