﻿namespace AlphaRooms.Accommodation.Provider.BEV5
{
    using AlphaRooms.Accommodation.Core.Provider.Contracts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
    using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
    using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
    using AlphaRooms.Utilities.CustomExceptions;
    using AlphaRooms.Utilities.ExtensionMethods;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;

    public class Bev5AvailabilityAutomator : IAccommodationAvailabilityAutomatorAsync<IEnumerable<AvailabilityRoomsResponse>>
    {
        private const string Bev5MaxReceivedMessageSize = "Bev5MaxReceivedMessageSize";
        private const string Bev5InternalSearchTimeout = "Bev5InternalSearchTimeout";

        private readonly IBev5RequestFactory bev5RequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IBev5EndpointService endpointService;

        public Bev5AvailabilityAutomator(
            IBev5RequestFactory bev5RequestFactory,
            IProviderOutputLogger outputLogger,
            IBev5EndpointService endpointService)
        {
            this.bev5RequestFactory = bev5RequestFactory;
            this.outputLogger = outputLogger;
            this.endpointService = endpointService;
        }

        public async Task<IEnumerable<AvailabilityRoomsResponse>> GetAvailabilityResponseAsync(AccommodationProviderAvailabilityRequest request)
        {
            SearchRoomsRequest[] bev5Requests = null;

            List<AvailabilityRoomsResponse> availabilityRooms = new List<AvailabilityRoomsResponse>();
            try
            {
                Bev5SOAServiceClient client = this.CreateBev5Client(request);

                bev5Requests = await this.bev5RequestFactory.CreateBev5SearchRequestAsync(request);

                // start searchs
                Task<AvailabilityRoomsResponse>[] searchRequests = Bev5AvailabilityAutomator.StartSearches(bev5Requests, client);

                var allSearches = Task.WhenAll(searchRequests);

                await SearchCompletion(request, allSearches);

                if (searchRequests.All(x => x.Status != TaskStatus.RanToCompletion))
                {
                    // if all timed out
                    throw new AvailabilityTimeoutException(string.Format("Availability Timeout exception after {0} seconds", request.Provider.Parameters.GetParameterValue(Bev5InternalSearchTimeout)));
                }

                for (int i = 0; i < searchRequests.Length; i++)
                {
                    if (searchRequests[i].Status == TaskStatus.RanToCompletion)
                    {
                        availabilityRooms.Add(searchRequests[i].Result);
                    }
                }

                this.outputLogger.LogAvailabilityResponse(request, ProviderOutputFormat.Xml, availabilityRooms);
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("BEV5 Availability Provider api exception. BEV5 Request - " + (bev5Requests != null ? bev5Requests.XmlSerialize() : null), ex);
            }

            return availabilityRooms;
        }

        private Bev5SOAServiceClient CreateBev5Client(AccommodationProviderAvailabilityRequest request)
        {
            string parameterValue = request.Provider.Parameters.GetParameterValue(Bev5AvailabilityAutomator.Bev5MaxReceivedMessageSize);
            int bev5MaxReceived = int.Parse(parameterValue);

            BasicHttpBinding binding = new BasicHttpBinding
            {
                MaxReceivedMessageSize = bev5MaxReceived
            };

            string endpointUri = this.endpointService.GetEndpoint();
            var address = new EndpointAddress(endpointUri);
            var client = new Bev5SOAServiceClient(binding, address);

            return client;
        }

        private static Task<AvailabilityRoomsResponse>[] StartSearches(SearchRoomsRequest[] bev5Requests, Bev5SOAServiceClient client)
        {
            Task<AvailabilityRoomsResponse>[] searchRequests = new Task<AvailabilityRoomsResponse>[bev5Requests.Length];
            Parallel.For(0, bev5Requests.Length, i =>
            {
                searchRequests[i] = client.SearchRoomsAsync(bev5Requests[i]);
            });

            return searchRequests;
        }

        private static Task SearchCompletion(AccommodationProviderAvailabilityRequest request, Task<AvailabilityRoomsResponse[]> allSearches)
        {
            TimeSpan searchTimeout = Bev5AvailabilityAutomator.GetSearchTimeout(request);

            // wait for timeout or all searches complete
            return Task.WhenAny(allSearches, Task.Delay(searchTimeout));
        }

        private static TimeSpan GetSearchTimeout(AccommodationProviderAvailabilityRequest request)
        {
            string parameterValue = request.Provider.Parameters.GetParameterValue(Bev5InternalSearchTimeout);
            TimeSpan searchTimeout = TimeSpan.Parse(parameterValue);
            return searchTimeout;
        }
    }
}