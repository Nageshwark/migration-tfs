﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;
using AlphaRooms.Accommodation.Provider.BEV5.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.Utilities.CustomExceptions;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Enums;
using System.ServiceModel;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5BookingAutomator : IAccommodationBookingAutomatorAsync<BookRoomsResponse>
    {
        private const string Bev5MaxReceivedMessageSize = "Bev5MaxReceivedMessageSize";
        private readonly IBev5RequestFactory bev5RequestFactory;
        private readonly IProviderOutputLogger outputLogger;
        private readonly IProviderLoggerService loggerService;
        private readonly IBev5EndpointService endpointService;

        public Bev5BookingAutomator(IBev5RequestFactory bev5RequestFactory, IProviderOutputLogger outputLogger, IProviderLoggerService loggerService,
                        IBev5EndpointService endpointService)
        {
            this.bev5RequestFactory = bev5RequestFactory;
            this.outputLogger = outputLogger;
            this.loggerService = loggerService;
            this.endpointService = endpointService;
        }

        public async Task<BookRoomsResponse> GetBookingResponseAsync(AccommodationProviderBookingRequest request)
        {
            AccommodationBookingRequest bev5Request = null;
            try
            {
                var binding = new BasicHttpBinding() { MaxReceivedMessageSize = int.Parse(request.Provider.Parameters.GetParameterValue(Bev5MaxReceivedMessageSize)) };
                var address = new EndpointAddress(this.endpointService.GetEndpoint());
                var client = new Bev5SOAServiceClient(binding, address);
                bev5Request = bev5RequestFactory.CreateBev5BookingRequest(request);
                var results = await client.BookAccommodationAsync(bev5Request);
                // obscuring data for logs
                bev5Request.Rooms.ForEach(ObscureCardData);
                this.loggerService.SetProviderRequest(request, bev5Request);
                this.loggerService.SetProviderResponse(request, results);
                this.outputLogger.LogBookingResponse(request, ProviderOutputFormat.Xml, results);
                return results;
            }
            catch (Exception ex)
            {
                bev5Request?.Rooms.ForEach(ObscureCardData);
                var xmlRequest = bev5Request != null ? bev5Request.XmlSerialize() : "NULL";
                throw new SupplierApiException("BEV5 Booking Provider api exception. BEV5 Request - " + xmlRequest, ex);
            }
        }

        public void ObscureCardData(BookRoomRequest r)
        {
            if (r.CardDetails != null)
            {
                r.CardDetails.CardNumberk__BackingField = "";
                r.CardDetails.Cvck__BackingField = "";
            }
        }
    }
}
