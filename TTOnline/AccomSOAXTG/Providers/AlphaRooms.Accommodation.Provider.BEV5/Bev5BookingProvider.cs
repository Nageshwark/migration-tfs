﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5BookingProvider : AccommodationBookingBase
    {
        private readonly IAccommodationBookingAutomatorAsync<BookRoomsResponse> automator;
        private readonly IAccommodationBookingParser<BookRoomsResponse> parser;

        public Bev5BookingProvider(IAccommodationBookingAutomatorAsync<BookRoomsResponse> automator, IAccommodationBookingParser<BookRoomsResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderBookingResult>> MakeProviderBookingAsync(AccommodationProviderBookingRequest request)
        {
            var response = await automator.GetBookingResponseAsync(request);
            return parser.GetBookingResults(request, response);
        }
    }
}
