﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Provider.BEV5.Bev5SOAService;

namespace AlphaRooms.Accommodation.Provider.BEV5
{
    public class Bev5CancellationProvider : AccommodationCancellationBase
    {
        private readonly IAccommodationCancellationAutomatorAsync<CancelAccommodationResponse> automator;
        private readonly IAccommodationCancellationParser<CancelAccommodationResponse> parser;

        public Bev5CancellationProvider(IAccommodationCancellationAutomatorAsync<CancelAccommodationResponse> automator, IAccommodationCancellationParser<CancelAccommodationResponse> parser)
        {
            this.automator = automator;
            this.parser = parser;
        }

        public override async Task<IEnumerable<AccommodationProviderCancellationResult>> CancelProviderBookingAsync(AccommodationProviderCancellationRequest request)
        {
            var response = await automator.GetCancellationResponseAsync(request);
            return parser.GetCancellationResults(request, response);
        }
    }
}
