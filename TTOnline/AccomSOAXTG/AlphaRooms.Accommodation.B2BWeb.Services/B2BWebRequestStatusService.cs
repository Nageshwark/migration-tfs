﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebRequestStatusService : IB2BWebRequestStatusService
    {
        private readonly IB2BWebRequestStatusCaching requestStatusCaching;

        public B2BWebRequestStatusService(IB2BWebRequestStatusCaching requestStatusCaching)
        {
            this.requestStatusCaching = requestStatusCaching;
        }

        public AccommodationB2BWebRequestStatus CreateAvailabilityRequestStatus(AccommodationB2BWebAvailabilityRequest availabilityRequest)
        {
            var availabilityRequestStatus = CreateRequestStatus(availabilityRequest.AvailabilityId.Value);
            availabilityRequestStatus.AvailabilityRequest = availabilityRequest;
            availabilityRequestStatus.RequestType = RequestType.Availability;
            return availabilityRequestStatus;
        }

        public AccommodationB2BWebRequestStatus CreateValuationRequestStatus(AccommodationB2BWebValuationRequest valuationRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(valuationRequest.ValuationId.Value);
            valuationRequestStatus.ValuationRequest = valuationRequest;
            valuationRequestStatus.RequestType = RequestType.Valuation;
            return valuationRequestStatus;
        }

        public AccommodationB2BWebRequestStatus CreateBookingRequestStatus(AccommodationB2BWebBookingRequest bookingRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(bookingRequest.BookingId.Value);
            valuationRequestStatus.BookingRequest = bookingRequest;
            valuationRequestStatus.RequestType = RequestType.Booking;
            return valuationRequestStatus;
        }
        
        public AccommodationB2BWebRequestStatus CreateCancellationRequestStatus(AccommodationB2BWebCancellationRequest cancellationRequest)
        {
            var valuationRequestStatus = CreateRequestStatus(cancellationRequest.CancellationId.Value);
            valuationRequestStatus.CancellationRequest = cancellationRequest;
            valuationRequestStatus.RequestType = RequestType.Cancellation;
            return valuationRequestStatus;
        }

        private AccommodationB2BWebRequestStatus CreateRequestStatus(Guid Id)
        {
            return new AccommodationB2BWebRequestStatus()
            {
                Id = Id
                , StartDate = DateTime.Now
                , Status = Status.NotStarted
                , Message = "Not Started"
            };
        }
        
        public async Task<AccommodationB2BWebRequestStatus> GetRequestStatusOrNullByIdAsync(Guid id)
        {
            return await requestStatusCaching.GetByIdOrNullAsync(id);
        }

        public async Task<AccommodationB2BWebRequestStatus> GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(string availabilityKey)
        {
            return await requestStatusCaching.GetSuccessfulByAvailabilityKeyOrNullAsync(availabilityKey);
        }

        public DateTime CreateResultsCacheExpireDate()
        {
            return requestStatusCaching.CreateResultsCacheExpireDate();
        }

        public bool IsResultsExpireDateExpired(AccommodationB2BWebRequestStatus cachedRequestStatus)
        {
            return requestStatusCaching.IsResultsExpireDateExpired(cachedRequestStatus);
        }

        public async Task SaveRequestStatusAsync(AccommodationB2BWebRequestStatus requestStatus)
        {
            await requestStatusCaching.SaveAsync(requestStatus);
        }

        public async Task UpdateRequestStatus(AccommodationB2BWebRequestStatus requestStatus, Status status, string message)
        {
            requestStatus.Status = status;
            requestStatus.Message = message;
            await SaveRequestStatusAsync(requestStatus);
        }
    }
}
