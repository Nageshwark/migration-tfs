﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Accommodation.B2BWeb.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Services;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebValuationService : IB2BWebValuationService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BWebRequestStatusService requestStatusService;
        private readonly IB2BWebValuationLogger valuationLogger;
        private readonly ILogger logger;
        private readonly IB2BWebValuationRequestFactory requestFactory;
        private readonly IValuationService coreValuationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BWebValuationResultMappingService resultsMappingService;
        private readonly IB2BWebValuationRequestValidator requestValidator;
        private readonly IB2BUserRepository agentRepository;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;
        private readonly IB2BWebAvailabilityResultCaching availabilityResultCaching;
        private readonly IB2BWebAvailabilityResultMappingService availabilityResultMappingService;

        public B2BWebValuationService(IB2BWebRequestStatusService requestStatusService, ILogger logger, IB2BWebValuationRequestFactory requestFactory
            , IValuationService coreValuationService, IChannelInfoService channelInfoService, IB2BWebValuationResultMappingService resultsMappingService
            , IB2BWebValuationRequestValidator requestValidator, IB2BWebValuationLogger valuationLogger, IAccommodationConfigurationManager configurationManager
            , IB2BUserRepository agentRepository, AccommodationPromotionalDiscountService promotionalDiscountService, IB2BWebAvailabilityResultCaching availabilityResultCaching, IB2BWebAvailabilityResultMappingService availabilityResultMappingService)
        {
            this.requestStatusService = requestStatusService;
            this.logger = logger;
            this.requestFactory = requestFactory;
            this.coreValuationService = coreValuationService;
            this.channelInfoService = channelInfoService;
            this.resultsMappingService = resultsMappingService;
            this.requestValidator = requestValidator;
            this.valuationLogger = valuationLogger;
            this.configurationManager = configurationManager;
            this.agentRepository = agentRepository;
            this.promotionalDiscountService = promotionalDiscountService;
            this.availabilityResultCaching = availabilityResultCaching;
            this.availabilityResultMappingService = availabilityResultMappingService;
        }

        public async Task<Guid> StartValuationAsync(AccommodationB2BWebValuationRequest valuationRequest)
        {
            // create a valuation id if the caller didn't provide one
            if (valuationRequest.ValuationId == null) valuationRequest.ValuationId = Guid.NewGuid();

            // Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateValuationRequestStatus(valuationRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            // start the valuation process
            #pragma warning disable 4014
            Task.Run(async() => await StartValuationProcessAsync(valuationRequest, requestStatus));
            #pragma warning restore 4014
            return valuationRequest.ValuationId.Value;
        }

        public async Task<AccommodationB2BWebValuationResponse> GetValuationResponseAsync(Guid valuationId)
        {
            AccommodationB2BWebValuationRequest valuationRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try 
            {
                // get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(valuationId);
                                
                // throw exception when request status is not available
                if (requestStatus == null) new AccommodationB2BWebValuationResponse() { ValuationId = valuationId, ValuationStatus = Status.NotFound };

                // if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebValuationResponse() { ValuationId = valuationId, ValuationStatus = requestStatus.Status };

                // get valuation request
                valuationRequest = requestStatus.ValuationRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Valuation, valuationId.ToString(), "(accommodation)", valuationRequest.Debugging, logger))
                {
                    // perform a valuartion
                    var coreValuationResponse = await coreValuationService.GetValuationResponseAsync(valuationId);

                    // apply promotion discounts
                    promotionalDiscountService.ProcessDiscounts(coreValuationResponse.Result.Rooms, valuationRequest.PromotionalCode, valuationRequest.Channel);

                    // map the core results
                    var valuationResult = await resultsMappingService.MapFromCoreValuationResultsAsync(valuationRequest, coreValuationResponse.Result);

                    // convert to FlightValuationResponse and return
                    return new AccommodationB2BWebValuationResponse
                    {
                        ValuationId = valuationId
                        , ValuationStatus = requestStatus.Status
                        , ValuationResult = valuationResult
                        , PromotionalCode = valuationRequest.PromotionalCode
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetValuationResponse Error: search [{0}] {1}\n\n{2}", valuationId, ex.GetDetailedMessageWithInnerExceptions(), valuationRequest.ToIndentedJson());
                return new AccommodationB2BWebValuationResponse { ValuationId = valuationId, ValuationStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        private async Task StartValuationProcessAsync(AccommodationB2BWebValuationRequest valuationRequest, AccommodationB2BWebRequestStatus requestStatus)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Valuation, valuationRequest.ValuationId.ToString(), "(accommodation)", valuationRequest.Debugging, logger))
	        {
                var searchStarted = DateTime.Now;
                AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
                AccommodationValuationResponse coreValuationResponse = null; 
                Exception exception = null;
                B2BUser agent = null;
                try
                {
                    // get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(valuationRequest.Channel);

                    // get agent details
                    agent = await agentRepository.GetByIdAsync(valuationRequest.AgentId);

                    // update Mongo with inprogress status
                    await requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Valuation Started");
                    
                    // Validate Request
                    requestValidator.ValidateValuationRequest(valuationRequest);

                    // Get availability request from guid
                    var availabilityRequestStatus = await requestStatusService.GetRequestStatusOrNullByIdAsync(valuationRequest.AvailabilityId);
                    availabilityRequest = availabilityRequestStatus.AvailabilityRequest;

                    // set availability request in valuation request status
                    requestStatus.AvailabilityRequest = availabilityRequest;

                    // create a core valuation request
                    var coreValuationRequest = requestFactory.CreateValuationRequest(availabilityRequest, valuationRequest, channelInfo, agent);

                    // Get availability results
                    var availabilityB2BResults = await availabilityResultCaching.GetByRoomIdsAsync(valuationRequest.AvailabilityId, 
                                                                                                   availabilityRequestStatus.ResultsBaseDestinationId.Value,
                                                                                                   valuationRequest.SelectedRoomIds);

                    var coreAvailabilityResults = availabilityResultMappingService.MapToCoreAvailabilityResult(availabilityB2BResults);

                    // perform a valuation search
                    coreValuationResponse = await coreValuationService.ProcessValuationAsync(coreValuationRequest, coreAvailabilityResults);
                    
                    // check if valuation failed
                    if (coreValuationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        requestStatus.Status = Status.Successful;
                        requestStatus.Message = "Accommodation Valuation Completed";
                    }
                    else
                    {
                        requestStatus.Status = Status.Failed;
                        requestStatus.Message = "Accommodation Provider Valuation Failed";
                    }
                }
                catch (Exception ex)
                {
                    // update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation Valuation Error: search [{0}] {1}\n\n{2}", valuationRequest.ValuationId, ex.GetDetailedMessageWithInnerExceptions(), valuationRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                await valuationLogger.LogAsync(availabilityRequest, valuationRequest, requestStatus, coreValuationResponse, DateTime.Now - searchStarted, exception);
            }
        }
    }
}
