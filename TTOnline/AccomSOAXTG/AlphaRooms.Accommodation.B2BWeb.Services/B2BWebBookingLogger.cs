﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.DomainModels;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Repositories;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts.Enums;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AlphaRooms.WebScraping;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Accommodation.Core.Interfaces;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebBookingLogger : IB2BWebBookingLogger
    {
        private readonly ILogger logger;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BWebBookingLogRepository repository;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2BWebBookingLogger(ILogger logger, IAccommodationConfigurationManager configurationManager, IB2BWebBookingLogRepository repository, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService, IPerformanceLoggerService<PerformanceLog> performanceLoggerService)
        {
            this.logger = logger;
            this.repository = repository;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.performanceLoggerService = performanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(AccommodationB2BWebBookingRequest bookingRequest, AccommodationB2BWebRequestStatus requestStatus
            , AccommodationBookingResponse coreResponse, TimeSpan timeTaken, Exception exception)
        {

            CreatePerformanceLogs(bookingRequest, requestStatus, coreResponse, timeTaken);

            if (!this.configurationManager.AccommodationB2BWebBookingLoggerIsEnabled) return;
            try
            {
                B2BWebBookingLog[] bookingLogs;
                if (coreResponse == null)
                {
                    bookingLogs = new[] { new B2BWebBookingLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        , ItineraryReference = bookingRequest.ItineraryReference
                        , ValuationId = bookingRequest.ValuationId
                        , BookingId = bookingRequest.BookingId.Value
                        , BookingDate = requestStatus.StartDate
                        , Status = requestStatus.Status
                        , TimeTaken = (int)timeTaken.TotalMilliseconds
                        , Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        , ProviderRoomNumbers = Enumerable.Range(1, bookingRequest.ValuatedRooms.Length).ToString(',')
                    }};
                }
                else
                {
                    bookingLogs = coreResponse.ProcessResponseDetails.Select(i => new B2BWebBookingLog()
                    {
                        HostName = Environment.MachineName + this.configurationManager.AccommodationHostNameTag
                        ,
                        ItineraryReference = bookingRequest.ItineraryReference
                        ,
                        ValuationId = bookingRequest.ValuationId
                        ,
                        BookingId = bookingRequest.BookingId.Value
                        ,
                        BookingDate = requestStatus.StartDate
                        ,
                        Status = requestStatus.Status
                        ,
                        TimeTaken = (int)timeTaken.TotalMilliseconds
                        ,
                        Exceptions = (exception != null ? exception.GetDetailedMessageWithInnerExceptions() : null)
                        ,
                        ProviderId = i.Provider.Id
                        ,
                        ProviderRoomNumbers = i.BookingRequestRooms.Select(j => j.ValuationResultRoom.RoomNumber).ToString(',')
                        ,
                        ProviderRequests = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Request)
                        ,
                        ProviderResponses = CreateProviderRequests(i.ProviderRequests, ProviderRequestType.Response)
                        ,
                        ProviderStatus = (i.Results != null && i.Results.All(j => j.ProviderBookingResult.BookingStatus == BookingStatus.Confirmed && i.Exception == null) ? Status.Successful : Status.Failed)
                        ,
                        ProviderTimeTaken = (i.TimeTaken != null ? (int?)i.TimeTaken.Value.TotalMilliseconds : null)
                        ,
                        ResultsCount = i.ResultsCount
                        ,
                        ProviderException = (i.Exception != null ? i.Exception.GetDetailedMessageWithInnerExceptions() : null)
                    }).ToArray();
                }
                await this.repository.SaveRangeAsync(bookingLogs);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to log accommodation availability with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
            }
        }

        private void CreatePerformanceLogs(AccommodationB2BWebBookingRequest bookingRequest, AccommodationB2BWebRequestStatus requestStatus,
                                           AccommodationBookingResponse coreResponse, TimeSpan timeTaken)
        {
            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();

            var resultCount = coreResponse?.Result?.Rooms?.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Booking",
                RequestIdentifier = bookingRequest.AvailabilityId.ToString(),
                Channel = bookingRequest.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus.Status == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            var providerPerfLogs = coreResponse?.ProcessResponseDetails.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                m.IsSuccess = d.Exception == null;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }

        private string CreateProviderRequests(AccommodationProviderRequest[] providerRequests, ProviderRequestType type)
        {
            if (providerRequests == null) return null;
            StringBuilder builder = new StringBuilder();
            int nullCount = 0;
            foreach (var request in providerRequests.Where(i => i.Type == type))
            {
                if (request.Key != "default" && providerRequests.Length > 1)
                {
                    builder.Append(request.Key);
                    builder.Append(": ");
                }
                if (request.Object == null)
                {
                    builder.Append("<<NULL>>");
                    nullCount++;
                }
                else if (request.Object is string)
                {
                    builder.Append(request.Object);
                }
                else if (request.Object is WebScrapeResponse)
                {
                    builder.Append(((WebScrapeResponse)request.Object).Value);
                }
                else
                {
                    try
                    {
                        builder.Append(request.Object.XmlSerialize(request.Object.GetType()));
                    }
                    catch (Exception ex)
                    {
                        builder.Append("Unable to serialize: " + ex.GetDetailedMessageWithInnerExceptions());
                    }
                }
                builder.Append(";\r\n");
            }
            if (nullCount == providerRequests.Length) return null;
            if (builder.Length > 0) builder.Length -= 3;
            return builder.ToString();
        }
    }
}