﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityRequestValidator : IB2BWebAvailabilityRequestValidator
    {
        public void ValidateAvailabilityRequest(AccommodationB2BWebAvailabilityRequest availabilityRequest)
        {
            if (availabilityRequest.DestinationId == Guid.Empty && availabilityRequest.EstablishmentId == Guid.Empty)
                throw new ValidationException("ValidateAvailabilityRequest failed: the request must have either DestinationId or EstablishmentId.");
            if (availabilityRequest.CheckInDate == DateTime.MinValue)
                throw new ValidationException("ValidateAvailabilityRequest failed: CheckInDate is required.");
            if (availabilityRequest.CheckOutDate == DateTime.MinValue)
                throw new ValidationException("ValidateAvailabilityRequest failed: CheckOutDate is required.");
            if (availabilityRequest.Rooms == null || availabilityRequest.Rooms.Length < 1 || availabilityRequest.Rooms.Length > 5)
                throw new ValidationException("ValidateAvailabilityRequest failed: Room occupancy must be provided for between 1 and 5 rooms.");
        }
    }
}
