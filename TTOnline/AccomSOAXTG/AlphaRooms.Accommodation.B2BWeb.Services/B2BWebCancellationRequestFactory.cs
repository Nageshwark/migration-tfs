﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebCancellationRequestFactory : IB2BWebCancellationRequestFactory
    {
        static B2BWebCancellationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2BWebCancellationRequest, AccommodationCancellationRequest>();
                Mapper.CreateMap<AccommodationB2BWebCancellationRequestCustomer, AccommodationCancellationRequestCustomer>();
            }
        }
        
        public AccommodationCancellationRequest CreateCancellationRequest(AccommodationB2BWebCancellationRequest cancellationRequest, ChannelInfo channelInfo, B2BUser agent)
        {
            var request = Mapper.Map<AccommodationB2BWebCancellationRequest, AccommodationCancellationRequest>(cancellationRequest);
            request.ChannelInfo = channelInfo;
            request.Agent = agent;
            return request;
        }
    }
}
