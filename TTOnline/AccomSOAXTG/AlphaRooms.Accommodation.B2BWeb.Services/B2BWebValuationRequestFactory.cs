﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebValuationRequestFactory : IB2BWebValuationRequestFactory
    {
        static B2BWebValuationRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2BWebValuationRequest, AccommodationValuationRequest>();
            }
        }

        private readonly IB2BWebRoomIdService roomIdService;

        public B2BWebValuationRequestFactory(IB2BWebRoomIdService roomIdService)
        {
            this.roomIdService = roomIdService;
        }

        public AccommodationValuationRequest CreateValuationRequest(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationB2BWebValuationRequest valuationRequest, ChannelInfo channelInfo, B2BUser agent)
        {
            var request = Mapper.Map<AccommodationB2BWebValuationRequest, AccommodationValuationRequest>(valuationRequest);
            request.ChannelInfo = channelInfo;
            request.Agent = agent;
            request.SelectedRoomIds = valuationRequest.SelectedRoomIds.Select(i => roomIdService.ConvertToCoreRoomId(i)).ToArray();
            return request;
        }
    }
}
