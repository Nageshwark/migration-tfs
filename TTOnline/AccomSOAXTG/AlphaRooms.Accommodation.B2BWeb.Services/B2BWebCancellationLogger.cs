﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{

    public class B2BWebCancellationLogger : IB2BWebCancellationLogger
    {

        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService;
        private readonly IPerformanceLoggerService<PerformanceLog> performanceLoggerService;

        public B2BWebCancellationLogger(IPerformanceLoggerService<PerformanceLog> performanceLoggerService, IPerformanceLoggerService<ProviderPerformanceLog> providerPerformanceLoggerService, IAccommodationConfigurationManager configurationManager)
        {
            this.performanceLoggerService = performanceLoggerService;
            this.providerPerformanceLoggerService = providerPerformanceLoggerService;
            this.configurationManager = configurationManager;
        }

        public async Task LogAsync(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationB2BWebRequestStatus requestStatus, AccommodationCancellationResponse coreCancellationResponse
            , TimeSpan timeSpan, Exception exception)
        {
            CreatePerformanceLogs(cancellationRequest, requestStatus, coreCancellationResponse, timeSpan);

            // TODO Implement cancellation logger
        }

        private void CreatePerformanceLogs(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationB2BWebRequestStatus requestStatus, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeTaken)
        {
            var resultCount = coreCancellationResponse?.Result?.Rooms.Length ?? 0;
            var perfLog = new PerformanceLog
            {
                ProductType = SupplierTypes.Accommodation.ToString(),
                OperationType = "Cancellation",
                RequestIdentifier = cancellationRequest.ItineraryId.ToString(),
                Channel = cancellationRequest.Channel,
                TotalTime = (int)timeTaken.TotalSeconds,
                IsSuccess = requestStatus.Status == Status.Successful,
                ResultCount = resultCount,
                HostName = this.configurationManager.AccommodationHostNameTag
            };

            Mapper.CreateMap<PerformanceLog, ProviderPerformanceLog>();
            var providerPerfLogs = coreCancellationResponse?.ProcessResponseDetails.Select(d =>
            {
                var m = Mapper.Map<ProviderPerformanceLog>(perfLog);
                m.Provider = d.Provider.EdiCode;
                m.IsSuccess = d.Exception == null;
                m.TotalTime = d.TimeTaken != null ? (int)d.TimeTaken.Value.TotalSeconds : 0;
                m.ResultCount = d.ResultsCount ?? 0;
                return m;
            });

            performanceLoggerService.Report(perfLog);

            if (providerPerfLogs != null)
            {
                providerPerfLogs.ForEach(log => providerPerformanceLoggerService.Report(log));
            }
        }
    }
}
