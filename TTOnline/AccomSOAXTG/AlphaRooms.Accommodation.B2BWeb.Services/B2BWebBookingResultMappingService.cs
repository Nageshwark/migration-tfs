﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebBookingResultMappingService : IB2BWebBookingResultMappingService
    {
        static B2BWebBookingResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationBookingResult, AccommodationB2BWebBookingResult>();
                Mapper.CreateMap<AccommodationBookingResultRoom, AccommodationB2BWebBookingResultRoom>();
                Mapper.CreateMap<AccommodationProviderBookingResult, AccommodationB2BWebBookingResultRoom>();
                Mapper.CreateMap<AccommodationResultPriceItem, AccommodationB2BWebValuationResultRoomPriceItem>().ForMember(d => d.Type, f => f.MapFrom(s => s.Type));
            }
        }
        
        private readonly ILogger logger;

        public B2BWebBookingResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public AccommodationB2BWebBookingResult MapFromCoreBookingResults(AccommodationB2BWebBookingRequest bookingRequest, AccommodationBookingResult coreBookingResult)
        {
            AccommodationB2BWebBookingResult result = null;
            try
            {
                result = Mapper.Map<AccommodationBookingResult, AccommodationB2BWebBookingResult>(coreBookingResult);
                result.Rooms = coreBookingResult.Rooms.AsParallel().Select((i, j) => CreateAccommodationB2BWebBookingResultRoom(bookingRequest, i, bookingRequest.ValuatedRooms[j])).ToArray();
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreBookingResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreBookingResult.ToIndentedJson());
            }
            return result;
        }

        private AccommodationB2BWebBookingResultRoom CreateAccommodationB2BWebBookingResultRoom(AccommodationB2BWebBookingRequest bookingRequest, AccommodationBookingResultRoom coreBookingResultRoom
            , AccommodationB2BWebBookingRequestRoom accommodationB2CBookingRequestRoom)
        {

            var result = Mapper.Map<AccommodationProviderBookingResult, AccommodationB2BWebBookingResultRoom>(coreBookingResultRoom.ProviderBookingResult);
            result = Mapper.Map<AccommodationBookingResultRoom, AccommodationB2BWebBookingResultRoom>(coreBookingResultRoom, result);
            var priceBreakdown = Mapper.Map<AccommodationResultPriceItem[], AccommodationB2BWebValuationResultRoomPriceItem[]>(coreBookingResultRoom.ValuationResultRoom.PriceBreakdown);
            result.RoomId = accommodationB2CBookingRequestRoom.ValuatedRoomId;
            result.ProviderRoomCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RoomCode;
            result.ProviderName = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderName ?? coreBookingResultRoom.Provider.Name;
            result.ProviderEdiCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderProviderCode ?? coreBookingResultRoom.ValuationResultRoom.Provider.EdiCode;
            result.DestinationId = coreBookingResultRoom.ValuationResultRoom.DestinationId;
            result.DestinationName = ""; //coreBookingResultRoom.ValuationResultRoom.DestinationName;
            result.EstablishmentId = coreBookingResultRoom.ValuationResultRoom.EstablishmentId;
            result.EstablishmentName = ""; //coreBookingResultRoom.ValuationResultRoom.EstablishmentName;
            result.ProviderEstablishmentCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.ProviderEstablishmentCode ?? coreBookingResultRoom.ValuationResultRoom.ProviderResult.EstablishmentEdiCode;
            result.CheckInDate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CheckInDate;
            result.CheckOutDate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CheckOutDate;
            result.AdultsCount = coreBookingResultRoom.ValuationResultRoom.AdultsCount;
            result.ChildrensCount = coreBookingResultRoom.ValuationResultRoom.ChildrenCount;
            result.InfantsCount = coreBookingResultRoom.ValuationResultRoom.InfantsCount;
            result.RoomNumber = coreBookingResultRoom.RoomNumber;
            result.RoomDescription = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RoomDescription;
            result.BoardCode = coreBookingResultRoom.ValuationResultRoom.ProviderResult.BoardCode;
            result.BoardType = coreBookingResultRoom.ValuationResultRoom.BoardType;
            result.BoardDescription = coreBookingResultRoom.ValuationResultRoom.ProviderResult.BoardDescription;
               //, IsSpecialRate = result.IsSpecialRate
            result.PaymentModel = coreBookingResultRoom.ValuationResultRoom.ProviderResult.PaymentModel;
            result.IsBindingRate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsBindingRate;
            result.IsOpaqueRate = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsOpaqueRate;
            result.IsNonRefundable = coreBookingResultRoom.ValuationResultRoom.ProviderResult.IsNonRefundable;
            result.CancellationPolicy = coreBookingResultRoom.ValuationResultRoom.ProviderResult.CancellationPolicy.ToString("");
            result.Price = coreBookingResultRoom.ValuationResultRoom.Price;
            result.PriceBreakdown = priceBreakdown;
            result.ProviderPrice = coreBookingResultRoom.ValuationResultRoom.CostPrice;
            result.PaymentToTake = coreBookingResultRoom.ValuationResultRoom.PaymentToTake;
            result.IsCardChargeApplicable = (result.PaymentModel != PaymentModel.CustomerPayDirect);
            result.ExchangeRate = coreBookingResultRoom.ValuationResultRoom.ExchangeRate;
            result.Margin = coreBookingResultRoom.ValuationResultRoom.Margin;
            result.RateType = coreBookingResultRoom.ValuationResultRoom.ProviderResult.RateType;
            result.KeyCollectionInformation = coreBookingResultRoom.ProviderBookingResult.KeyCollectionInformation;
            return result;
        }
    }
}
