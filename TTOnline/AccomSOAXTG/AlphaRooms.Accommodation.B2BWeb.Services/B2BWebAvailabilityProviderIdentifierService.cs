﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityProviderIdentifierService : IB2BWebAvailabilityProviderIdentifierService
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAccommodationProviderRepository accommodationProviderRepository;
        private readonly IB2BUserRepository agentRepository;

        public B2BWebAvailabilityProviderIdentifierService(IAccommodationConfigurationManager configurationManager
            , IAccommodationProviderRepository accommodationProviderRepository, IB2BUserRepository agentRepository)
        {
            this.configurationManager = configurationManager;
            this.accommodationProviderRepository = accommodationProviderRepository;
            this.agentRepository = agentRepository;
        }

        public async Task<AccommodationProvider[]> GetActiveAvailabilityProvidersAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest)
        {
            if (!ArrayModule.IsNullOrEmpty(configurationManager.AccommodationProvidersToSearch))
            {
                return await this.accommodationProviderRepository.GetByIdsAsync(configurationManager.AccommodationProvidersToSearch);
            }
            return (await this.agentRepository.GetByIdAsync(availabilityRequest.AgentId)).Providers.Where(i => i.IsActive).ToArray();
        }
    }
}
