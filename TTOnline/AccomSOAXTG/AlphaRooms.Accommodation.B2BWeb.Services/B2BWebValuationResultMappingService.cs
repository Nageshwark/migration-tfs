﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebValuationResultMappingService : IB2BWebValuationResultMappingService
    {
        static B2BWebValuationResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationValuationResult, AccommodationB2BWebValuationResult>();
                Mapper.CreateMap<AccommodationValuationResultRoom, AccommodationB2BWebValuationResultRoom>();
                Mapper.CreateMap<AccommodationResultPriceItem, AccommodationB2BWebValuationResultRoomPriceItem>().ForMember(d=>d.Type,f=>f.MapFrom(s=>s.Type));
                Mapper.CreateMap<AccommodationProviderValuationResult, AccommodationB2BWebValuationResultRoom>();
                Mapper.CreateMap<AccommodationProviderValuationResultVccPaymentRule, AccommodationB2BWebValuationResultVccPaymentRule>();
            }
        }
        
        private readonly ILogger logger;
        private readonly IBoardRepository boardTypeRepository;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;

        public B2BWebValuationResultMappingService(ILogger logger, IEstablishmentDetailsRepository establishmentDetailsRepository, IBoardRepository boardTypeRepository)
        {
            this.logger = logger;
            this.boardTypeRepository = boardTypeRepository;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
        }

        public async Task<AccommodationB2BWebValuationResult> MapFromCoreValuationResultsAsync(AccommodationB2BWebValuationRequest valuationRequest, AccommodationValuationResult coreValuationResult)
        {
            var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreValuationResult.Rooms.First().DestinationId, valuationRequest.Channel);
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            AccommodationB2BWebValuationResult result = null;
            try
            {
                result = Mapper.Map<AccommodationValuationResult, AccommodationB2BWebValuationResult>(coreValuationResult);
                result.Rooms = coreValuationResult.Rooms.AsParallel().Select((i, j) => CreateAccommodationB2BWebValuationResultRoom(i, boardTypes, establishmentDetails
                    , valuationRequest.SelectedRoomIds[j])).ToArray();
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreValuationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreValuationResult.ToIndentedJson());
            }
            return result;
        }

        private AccommodationB2BWebValuationResultRoom CreateAccommodationB2BWebValuationResultRoom(AccommodationValuationResultRoom coreResult, Dictionary<BoardType, Board> boards
            , Dictionary<Guid, EstablishmentDetails> establishmentDetails, string roomId)
        {
            var establishmentDetail = establishmentDetails[coreResult.EstablishmentId];
            var room = Mapper.Map<AccommodationProviderValuationResult, AccommodationB2BWebValuationResultRoom>(coreResult.ProviderResult);
            room = Mapper.Map<AccommodationValuationResultRoom, AccommodationB2BWebValuationResultRoom>(coreResult, room);
            room.RoomId = roomId;
            room.ProviderEdiCode = coreResult.ProviderResult.ProviderProviderCode ?? coreResult.Provider.EdiCode;
            room.ProviderName = coreResult.ProviderResult.ProviderName ?? coreResult.Provider.Name;
            room.ProviderPrice = coreResult.CostPrice;
            room.ProviderRoomCode = coreResult.ProviderResult.RoomCode;
            room.DestinationName = establishmentDetail.DestinationName;
            room.EstablishmentName = establishmentDetail.EstablishmentName;
            room.ProviderEstablishmentCode = coreResult.ProviderResult.ProviderEstablishmentCode ?? coreResult.ProviderResult.EstablishmentEdiCode;
            room.BoardDescription = boards[room.BoardType].Name;
            room.ChildrensCount = coreResult.ChildrenCount;
            room.InfantsCount = coreResult.InfantsCount;
            room.IsCardChargeApplicable = (room.PaymentModel != PaymentModel.CustomerPayDirect);
            room.VccPaymentRules = coreResult.ProviderResult.VirtualCreditCardPaymentTerms.Select(i => CreateAccommodationB2BWebValuationResultRoomVccPaymentRules(i)).ToArray();
            var value = coreResult.SpecificData.GetValueOrDefault("AgentCommissionRate");
            room.AgentCommissionRate = (!string.IsNullOrEmpty(value) ? decimal.Parse(value) : (decimal?)null);
            if (!ArrayModule.IsNullOrEmpty(coreResult.CancellationPolicies))room.CancellationPolicy = coreResult.CancellationPolicies.Select(i => "<p>" + i + "</p>").ToString("");

            room.PriceHikeDetail = coreResult.PriceHikeDetail;
            return room;
        }

        private AccommodationB2BWebValuationResultVccPaymentRule CreateAccommodationB2BWebValuationResultRoomVccPaymentRules(AccommodationProviderValuationResultVccPaymentRule paymentRule)
        {
            return Mapper.Map<AccommodationProviderValuationResultVccPaymentRule, AccommodationB2BWebValuationResultVccPaymentRule>(paymentRule);
        }
    }
}
