﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebCancellationRequestValidator : IB2BWebCancellationRequestValidator
    {
        public void ValidateCancellationRequest(AccommodationB2BWebCancellationRequest cancellationRequest)
        {
            if (cancellationRequest.ItineraryId == Guid.Empty)
                throw new ValidationException("ValidateCancellationRequest failed: the request must have ItineraryId.");
        }
    }
}
