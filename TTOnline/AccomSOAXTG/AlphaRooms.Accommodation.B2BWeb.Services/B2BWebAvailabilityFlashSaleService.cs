﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityFlashSaleService : IAvailabilityFlashSaleService
    {
        private readonly IAccommodationFlashSaleRepository accommodationFlashSaleRepository;
        private readonly IB2BWebFlashSaleMappingFactory flashSaleMappingService;
        private Dictionary<int, Guid[]> agentEstablishmentMappings = null;
        public B2BWebAvailabilityFlashSaleService(IAccommodationFlashSaleRepository accommodationFlashSaleRepository, IB2BWebFlashSaleMappingFactory flashSaleMappingService)
        {
            this.accommodationFlashSaleRepository = accommodationFlashSaleRepository;
            this.flashSaleMappingService = flashSaleMappingService;
        }

        public async Task<IList<AccommodationAvailabilityResult>> FilterResultsForFlashSaleAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            // cache the mappings if not already cached
            if (agentEstablishmentMappings == null) await PopulateEstablishmentMappingsAsync();

            // if there are no mappings, return everything
            if (agentEstablishmentMappings == null) return results;

            // get establishment mappings for other agents
            var establishmentsExclusiveToOthers = agentEstablishmentMappings.Where(m => m.Key != availabilityRequest.Agent.Id).SelectMany(m => m.Value).ToArray();

            // remove the establishments which are not mapped with the agent but are mapped with any other agent(s)
            return results.AsParallel().Where(r => IsAvailableToAgent(r, availabilityRequest.Agent.Id) //establishment mapped to current agent
                                                    || IsNotExclusiveToOtherAgent(r, establishmentsExclusiveToOthers) //establishment mapped with just other agents
                                             ).ToList();
        }

        private async Task PopulateEstablishmentMappingsAsync()
        {
            agentEstablishmentMappings = new Dictionary<int, Guid[]>();

            //get all flash sale establishment mappings
            var flashSaleEstablishments = await accommodationFlashSaleRepository.GetAllAsync();

            //build agent mappings for all active mappings
            agentEstablishmentMappings = flashSaleMappingService.GetActiveAgentEstablishmentMappings(flashSaleEstablishments);
        }

        private bool IsNotExclusiveToOtherAgent(AccommodationAvailabilityResult result, Guid[] exclusiveEstablishments)
        {
            return !exclusiveEstablishments.Any() ||
                   !exclusiveEstablishments.Any(e => StringComparer.OrdinalIgnoreCase.Equals(e, result.EstablishmentId));
        }

        private bool IsAvailableToAgent(AccommodationAvailabilityResult accommodationAvailabilityResult, int agentId)
        {
            if (!agentEstablishmentMappings.ContainsKey(agentId)) return false;
            return agentEstablishmentMappings[agentId].Contains(accommodationAvailabilityResult.EstablishmentId);
        }
    }
}