﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityKeyGenerator : IB2BWebAvailabilityKeyGenerator
    {
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IAgeRuleService ageRuleService;

        public B2BWebAvailabilityKeyGenerator(IAccommodationConfigurationManager configurationManager, IAgeRuleService ageRuleService)
        {
            this.configurationManager = configurationManager;
            this.ageRuleService = ageRuleService;
        }

        public string CreateAvailabilityKey(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationProvider[] providers)
        {

            StringBuilder builder = new StringBuilder(((int)availabilityRequest.Channel).ToString("00"));
            builder.Append(ParseSearchType(availabilityRequest.SearchType));
            builder.Append(availabilityRequest.CheckInDate.ToString("yyyyMMdd"));
            builder.Append(((int)(availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate).TotalDays).ToString("000"));
            builder.Append(availabilityRequest.EstablishmentId == null ? 'D' + availabilityRequest.DestinationId.Value.ToShortString() : 'E' + availabilityRequest.EstablishmentId.Value.ToShortString());
            builder.Append((availabilityRequest.IgnoreConsolidation ? "1" : "0"));
            builder.Append("A" + availabilityRequest.AgentId);
            providers.ForEach(i => builder.Append(i.Id));
            builder.Append('R');
            builder.Append(availabilityRequest.PromotionalCode);
            foreach (var room in availabilityRequest.Rooms)
            {
                builder.Append(room.RoomNumber);
                var guestAges = Enumerable.Repeat(configurationManager.AccommodationAdultAge, room.Adults).Concat(room.ChildAges).ToArray();
                var providerAges = providers.Select(i => ageRuleService.CalculateProviderAges(i, guestAges)).ToArray();
                var pa = providerAges.Distinct(new CustomEqualityComparer<ProviderAge>((i, j) => i.Adults == j.Adults && i.Children == j.Children && i.Infants == j.Infants)).ToArray();
                foreach (var p in pa)
                {
                    builder.Append(p.Provider.Id);
                    builder.Append(p.Adults);
                    builder.Append(p.Children);
                    builder.Append(p.Infants);
                }
            }
            if (availabilityRequest.SuppliersToSearch != null && availabilityRequest.SuppliersToSearch.Any())
            {
                builder.Append(string.Join(string.Empty, availabilityRequest.SuppliersToSearch));
            }
            return builder.ToString();
        }

        private string ParseSearchType(SearchType searchType)
        {
            switch (searchType)
            {
                case SearchType.FlightAndHotel:
                    return "FHs";
                case SearchType.HotelOnly:
                    return "HOs";
                case SearchType.FlightOnly:
                    return "FOs";
                default:
                    throw new ArgumentOutOfRangeException(nameof(searchType), searchType, null);
            }
        }
    }
}
