﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Utilities;
using AutoMapper;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityMapResultMappingService : IB2BWebAvailabilityMapResultMappingService
    {
        static B2BWebAvailabilityMapResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2BWebAvailabilityResult, AccommodationB2BWebAvailabilityMapResult>();
            }
        }

        public IList<AccommodationB2BWebAvailabilityMapResult> MapFromCoreAvailabilityResultsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, IList<AccommodationB2BWebAvailabilityResult> results)
        {
            return results.Select(i => CreateAccommodationB2BWebAvailabilityResult(i)).ToArray();
        }

        private AccommodationB2BWebAvailabilityMapResult CreateAccommodationB2BWebAvailabilityResult(AccommodationB2BWebAvailabilityResult result)
        {
            return Mapper.Map<AccommodationB2BWebAvailabilityResult, AccommodationB2BWebAvailabilityMapResult>(result);
        }
    }
}
