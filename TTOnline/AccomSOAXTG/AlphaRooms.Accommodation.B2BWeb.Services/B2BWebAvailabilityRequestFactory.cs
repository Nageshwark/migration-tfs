﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityRequestFactory : IB2BWebAvailabilityRequestFactory
    {
        static B2BWebAvailabilityRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2BWebAvailabilityRequest, AccommodationAvailabilityRequest>()
                    .ForMember(i => i.Rooms, i => i.Ignore());
                Mapper.CreateMap<AccommodationB2BWebAvailabilityRequestRoom, AccommodationAvailabilityRequestRoom>()
                    .ForMember(i => i.Guests, i => i.Ignore());
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;

        public B2BWebAvailabilityRequestFactory(IAccommodationConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public AccommodationAvailabilityRequest CreateAvailabilityRequest(AccommodationB2BWebAvailabilityRequest availabilityRequest, ChannelInfo channelInfo, B2BUser agent
            , AccommodationProvider[] providers)
        {
            var request = Mapper.Map<AccommodationB2BWebAvailabilityRequest, AccommodationAvailabilityRequest>(availabilityRequest);
            request.ProviderCacheEnabled = configurationManager.AccommodationB2BWebEnableProviderCache;
            request.ChannelInfo = channelInfo;
            request.Agent = agent;
            request.Providers = providers;
            request.Rooms = availabilityRequest.Rooms.Select(i => CreateAvailabilityRequestRoom(i)).ToArray();
            request.Voucher = availabilityRequest.PromotionalCode;
            return request;
        }

        private AccommodationAvailabilityRequestRoom CreateAvailabilityRequestRoom(AccommodationB2BWebAvailabilityRequestRoom room)
        {
            var requestRoom = Mapper.Map<AccommodationB2BWebAvailabilityRequestRoom, AccommodationAvailabilityRequestRoom>(room);
            requestRoom.Guests = Enumerable.Repeat(CreateAvailabilityRequestRoomGuest(configurationManager.AccommodationAdultAge), room.Adults)
                .Concat(room.ChildAges.Select(i => CreateAvailabilityRequestRoomGuest(i))).ToArray();
            return requestRoom;
        }

        private AccommodationAvailabilityRequestRoomGuest CreateAvailabilityRequestRoomGuest(byte age)
        {
            return new AccommodationAvailabilityRequestRoomGuest() { Age = age };
        }
    }
}
