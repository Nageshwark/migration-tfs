﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebBookingRequestFactory : IB2BWebBookingRequestFactory
    {
        static B2BWebBookingRequestFactory()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationB2BWebBookingRequest, AccommodationBookingRequest>()
                    .ForMember(i => i.Customer, i => i.Ignore());
                Mapper.CreateMap<AccommodationB2BWebBookingRequestCustomer, AccommodationBookingRequestCustomer>();
                Mapper.CreateMap<AccommodationB2BWebBookingRequestRoom, AccommodationBookingRequestRoom>();
                Mapper.CreateMap<AccommodationB2BWebBookingRequestRoomSpecialRequest, AccommodationBookingRequestRoomSpecialRequest>();
            }
        }

        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BWebRoomIdService roomIdService;
        private readonly IAvailabilityResultRoomIdGenerator coreRoomIdService;

        public B2BWebBookingRequestFactory(IAccommodationConfigurationManager configurationManager, IB2BWebRoomIdService roomIdService
            , IAvailabilityResultRoomIdGenerator coreRoomIdService)
        {
            this.configurationManager = configurationManager;
            this.roomIdService = roomIdService;
            this.coreRoomIdService = coreRoomIdService;
        }

        public AccommodationBookingRequest CreateBookingRequest(AccommodationB2BWebBookingRequest bookingRequest, ChannelInfo channelInfo, B2BUser agent)
        {
            var request = Mapper.Map<AccommodationB2BWebBookingRequest, AccommodationBookingRequest>(bookingRequest);
            request.Customer = CreateBookingRequestCustomer(bookingRequest);
            request.ValuatedRooms = bookingRequest.ValuatedRooms.Select(i => CreateBookingRequestRoom(i)).ToArray();
            request.ChannelInfo = channelInfo;
            request.Agent = agent;
            return request;
        }

        private AccommodationBookingRequestCustomer CreateBookingRequestCustomer(AccommodationB2BWebBookingRequest bookingRequest)
        {
            var customer = Mapper.Map<AccommodationB2BWebBookingRequestCustomer, AccommodationBookingRequestCustomer>(bookingRequest.Customer);
            customer.ContactNumber = configurationManager.AccommodationB2BWebBookingCustomerContactNumber;
            customer.EmailAddress = configurationManager.AccommodationB2BWebBookingCustomerEmailAddress;
            customer.Age = bookingRequest.Customer.Age ?? configurationManager.AccommodationAdultAge;
            return customer;
        }

        private AccommodationBookingRequestRoom CreateBookingRequestRoom(AccommodationB2BWebBookingRequestRoom room)
        {
            var requestRoom = Mapper.Map<AccommodationB2BWebBookingRequestRoom, AccommodationBookingRequestRoom>(room);
            requestRoom.ValuatedRoomId = roomIdService.ConvertToCoreRoomId(room.ValuatedRoomId);
            requestRoom.Guests = this.coreRoomIdService.Parse(requestRoom.ValuatedRoomId).GuestAges.Select(i => CreateBookingRequestRoomGuest(i)).ToArray();
            return requestRoom;
        }

        private AccommodationBookingRequestRoomGuest CreateBookingRequestRoomGuest(byte guestAge)
        {
            return new AccommodationBookingRequestRoomGuest() { Age = guestAge };
        }
    }
}
