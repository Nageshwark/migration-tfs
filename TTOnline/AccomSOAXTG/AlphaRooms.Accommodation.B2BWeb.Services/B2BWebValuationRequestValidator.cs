﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebValuationRequestValidator : IB2BWebValuationRequestValidator
    {
        public void ValidateValuationRequest(AccommodationB2BWebValuationRequest valuationRequest)
        {
            if (valuationRequest.AvailabilityId == Guid.Empty)
                throw new ValidationException("ValidateValuationRequest failed: the request must have AvailabilityId.");
            if (ArrayModule.IsNullOrEmpty(valuationRequest.SelectedRoomIds))
                throw new ValidationException("ValidateValuationRequest failed: the request must have SelectedRoomIds.");
        }
    }
}
