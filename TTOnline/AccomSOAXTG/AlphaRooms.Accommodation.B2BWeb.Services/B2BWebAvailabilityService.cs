﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Accommodation.B2BWeb.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Services;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityService : IB2BWebAvailabilityService
    {
        private readonly ILogger logger;
        private readonly IB2BWebAvailabilityResultCaching resultsCaching;
        private readonly IB2BWebAvailabilityRequestValidator requestValidator;
        private readonly IB2BWebAvailabilityResultsFilterOptionsService resultsFilterOptionsService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IAvailabilityService coreAvailabilityService;
        private readonly IB2BWebAvailabilityRequestFactory requestFactory;
        private readonly IB2BWebAvailabilityResultMappingService resultsMappingService;
        private readonly IB2BWebRequestStatusService requestStatusService;
        private readonly IB2BWebAvailabilityKeyGenerator keyGenerator;
        private readonly IB2BWebAvailabilityLogger availabilityLogger;
        private readonly IB2BWebAvailabilityProviderIdentifierService providerIdentifierService;
        private readonly IAccommodationConfigurationManager configurationManager;
        private readonly IB2BWebAvailabilityMapResultMappingService mapResultMappingService;
        private readonly IB2BUserRepository agentRepository;
        private readonly IAvailabilityResultRoomIdGenerator roomIdGenerator;
        private readonly IB2BWebRoomIdService roomIdService;
        private readonly IB2BWebAvailabilityResultLowerPriceService b2bWebAvailabilityResultLowerPriceService;
        private readonly IAccommodationBusiness accommodationBusiness;

        public B2BWebAvailabilityService(ILogger logger, IB2BWebAvailabilityResultCaching resultsCaching, IB2BWebAvailabilityRequestValidator requestValidator
            , IB2BWebAvailabilityResultsFilterOptionsService resultsFilterOptionsService, IChannelInfoService channelInfoService, IAvailabilityService coreAvailabilityService
            , IB2BWebAvailabilityRequestFactory requestFactory, IB2BWebAvailabilityResultMappingService resultsMappingService, IB2BWebRequestStatusService requestStatusService
            , IB2BWebAvailabilityKeyGenerator keyGenerator, IB2BWebAvailabilityLogger availabilityLogger, IB2BWebAvailabilityProviderIdentifierService providerIdentifierService
            , IAccommodationConfigurationManager configurationManager, IB2BWebAvailabilityMapResultMappingService mapResultMappingService, IB2BWebRoomIdService roomIdService
            , IB2BUserRepository agentRepository, IAvailabilityResultRoomIdGenerator roomIdGenerator, IB2BWebAvailabilityResultLowerPriceService b2bWebAvailabilityResultLowerPriceService, IAccommodationBusiness accommodationBusiness)
        {
            this.logger = logger;
            this.resultsCaching = resultsCaching;
            this.requestValidator = requestValidator;
            this.resultsFilterOptionsService = resultsFilterOptionsService;
            this.channelInfoService = channelInfoService;
            this.coreAvailabilityService = coreAvailabilityService;
            this.requestFactory = requestFactory;
            this.resultsMappingService = resultsMappingService;
            this.requestStatusService = requestStatusService;
            this.keyGenerator = keyGenerator;
            this.availabilityLogger = availabilityLogger;
            this.providerIdentifierService = providerIdentifierService;
            this.configurationManager = configurationManager;
            this.mapResultMappingService = mapResultMappingService;
            this.agentRepository = agentRepository;
            this.roomIdGenerator = roomIdGenerator;
            this.roomIdService = roomIdService;
            this.b2bWebAvailabilityResultLowerPriceService = b2bWebAvailabilityResultLowerPriceService;
            this.accommodationBusiness = accommodationBusiness;
        }

        public async Task<Guid> StartAvailabilitySearchAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest)
        {
            //create a availabilityid if the caller didn't provide one
            bool isRestartSearch = false;
            if (availabilityRequest.AvailabilityId == null) availabilityRequest.AvailabilityId = Guid.NewGuid();
            else isRestartSearch = true;

            //Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateAvailabilityRequestStatus(availabilityRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            //start the availability process
#pragma warning disable 4014
            Task.Run(async () => await StartAvailabilityProcessAsync(availabilityRequest, requestStatus, isRestartSearch));
#pragma warning restore 4014
            return availabilityRequest.AvailabilityId.Value;
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria)
        {
            AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try
            {
                //get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

                //if we dont want any results back just return the status
                if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
                {
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
                }

                //if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

                //if we dont want any results back just return the status
                if (filterCriteria.NumberOfResults == 0) return new AccommodationB2BWebAvailabilityResponse()
                {
                    AvailabilityId = availabilityId,
                    AvailabilityStatus = requestStatus.Status
                    ,
                    AvailabilityResults = new AccommodationB2BWebAvailabilityResult[0]
                };

                //get the availability request
                availabilityRequest = requestStatus.AvailabilityRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
                {
                    //get the filtered and sorted accommodations
                    var availabilityResults = await resultsCaching.GetFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
                        , availabilityRequest.Rooms.Length, filterCriteria, sortCriteria, availabilityRequest.Debugging);

                    if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityResponse for search {0} returned {1} establishments after filtering.", availabilityId, availabilityResults.Results.Length);
                    // identify the lower price rooms
                    availabilityResults.Results = b2bWebAvailabilityResultLowerPriceService.ApplyAvailabilityLowerPrice(availabilityResults.Results);
                    //convert to AccommodationAvailabilityResponse and return
                    return new AccommodationB2BWebAvailabilityResponse
                    {
                        AvailabilityId = availabilityId
                        ,
                        AvailabilityStatus = requestStatus.Status
                        ,
                        AvailabilityResults = availabilityResults.Results
                        ,
                        AvailabilityTotalResultsCount = availabilityResults.ResultsTotalCount
                        ,
                        PromotionalCode = availabilityRequest.PromotionalCode
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                return new AccommodationB2BWebAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId, AccommodationB2BWebAvailabilityFilter filterCriteria, AccommodationB2BWebAvailabilitySort sortCriteria)
        {
            AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try
            {
                //get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

                //if we dont want any results back just return the status
                if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
                {
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
                }

                //if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

                //if we dont want any results back just return the status
                if (filterCriteria.NumberOfResults == 0) return new AccommodationB2BWebAvailabilityResponse()
                {
                    AvailabilityId = availabilityId,
                    AvailabilityStatus = requestStatus.Status
                    ,
                    AvailabilityResults = new AccommodationB2BWebAvailabilityResult[0]
                };

                //get the availability request
                availabilityRequest = requestStatus.AvailabilityRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
                {
                    //get the filtered and sorted accommodation
                    var availabilityResult = await resultsCaching.GetByEstablishmentIdFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
                        , establishmentId, filterCriteria, sortCriteria, availabilityRequest.Debugging);

                    if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityResponse for search {0} returned {1} rooms after filtering.", availabilityId, (availabilityResult != null ? availabilityResult.Rooms.Length : -1));

                    // identify the lower price rooms
                    availabilityResult = b2bWebAvailabilityResultLowerPriceService.ApplyAvailabilityLowerPrice(availabilityResult);

                    //convert to AccommodationAvailabilityResponse and return
                    return new AccommodationB2BWebAvailabilityResponse
                    {
                        AvailabilityId = availabilityId
                        ,
                        AvailabilityStatus = requestStatus.Status
                        ,
                        AvailabilityResults = (availabilityResult != null ? new[] { availabilityResult } : new AccommodationB2BWebAvailabilityResult[0])
                        ,
                        AvailabilityTotalResultsCount = (availabilityResult != null ? 1 : 0)
                        ,
                        PromotionalCode = availabilityRequest.PromotionalCode
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                return new AccommodationB2BWebAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        public async Task<AccommodationB2BWebAvailabilityResponse> GetAvailabilityResponseByRoomsAsync(Guid availabilityId, string[] roomIds)
        {
            AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try
            {
                //get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

                //if we dont want any results back just return the status
                if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
                {
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
                }

                //if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebAvailabilityResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

                //get the availability request
                availabilityRequest = requestStatus.AvailabilityRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
                {
                    //get the filtered and sorted accommodation
                    var availabilityResults = await resultsCaching.GetByRoomIdsAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value, roomIds);

                    if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityResponse for search {0} returned {1} rooms after filtering.", availabilityId, availabilityResults.Length);

                    //convert to AccommodationAvailabilityResponse and return
                    return new AccommodationB2BWebAvailabilityResponse
                    {
                        AvailabilityId = availabilityId
                        ,
                        AvailabilityStatus = requestStatus.Status
                        ,
                        AvailabilityResults = availabilityResults
                        ,
                        AvailabilityTotalResultsCount = availabilityResults.Length
                        ,
                        PromotionalCode = availabilityRequest.PromotionalCode
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                return new AccommodationB2BWebAvailabilityResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        public async Task<AccommodationB2BWebAvailabilityMapResponse> GetAvailabilityMapResponseAsync(Guid availabilityId, AccommodationB2BWebAvailabilityFilter filterCriteria)
        {
            AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try
            {
                //get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2BWebAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

                //if we dont want any results back just return the status
                if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
                {
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
                }

                //if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

                //if we dont want any results back just return the status
                if (filterCriteria.NumberOfResults == 0) return new AccommodationB2BWebAvailabilityMapResponse()
                {
                    AvailabilityId = availabilityId,
                    AvailabilityStatus = requestStatus.Status
                    ,
                    AvailabilityMapResults = new AccommodationB2BWebAvailabilityMapResult[0]
                };

                //get the availability request
                availabilityRequest = requestStatus.AvailabilityRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityMapResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
                {
                    //get the filtered and sorted accommodations
                    var availabilityResults = await resultsCaching.GetFilteredAndSortedAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value
                        , availabilityRequest.Rooms.Length, filterCriteria, new AccommodationB2BWebAvailabilitySort(), availabilityRequest.Debugging);

                    var mappedResults = mapResultMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, availabilityResults.Results);

                    if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityMapResponse for search {0} returned {1} rooms after filtering.", availabilityId, mappedResults.Count());

                    //convert to AccommodationAvailabilityResponse and return
                    return new AccommodationB2BWebAvailabilityMapResponse
                    {
                        AvailabilityId = availabilityId
                        ,
                        AvailabilityStatus = requestStatus.Status
                        ,
                        AvailabilityMapResults = mappedResults.ToArray()
                        ,
                        AvailabilityTotalResultsCount = availabilityResults.ResultsTotalCount
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityMapResponse Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                return new AccommodationB2BWebAvailabilityMapResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }


        public async Task<AccommodationB2BWebAvailabilityMapResponse> GetAvailabilityMapResponseByEstablishmentAsync(Guid availabilityId, Guid establishmentId)
        {
            AccommodationB2BWebAvailabilityRequest availabilityRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try
            {
                //get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return new AccommodationB2BWebAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = Status.NotFound };

                //if we dont want any results back just return the status
                if (requestStatus.Status == Status.Successful && requestStatusService.IsResultsExpireDateExpired(requestStatus))
                {
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.Expired, "Accommodation Availability StatusRequest Expired");
                }

                //if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebAvailabilityMapResponse() { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus.Status };

                //get the availability request
                availabilityRequest = requestStatus.AvailabilityRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.GetAvailabilityMapResults, availabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
                {
                    //get the filtered and sorted accommodations
                    var availabilityResult = await resultsCaching.GetByEstablishmentIdAsync(requestStatus.ResultsAvailabilityId.Value, requestStatus.ResultsBaseDestinationId.Value, establishmentId);

                    var mappedResults = mapResultMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, new[] { availabilityResult });

                    if (availabilityRequest.Debugging) logger.Debug("GetAvailabilityMapResponseByEstablishmentAsync for search {0} returned {1} rooms after filtering.", availabilityId, mappedResults.Count());

                    //convert to AccommodationAvailabilityResponse and return
                    return new AccommodationB2BWebAvailabilityMapResponse
                    {
                        AvailabilityId = availabilityId
                        ,
                        AvailabilityStatus = requestStatus.Status
                        ,
                        AvailabilityMapResults = mappedResults.ToArray()
                        ,
                        AvailabilityTotalResultsCount = mappedResults.Count
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityMapResponseByEstablishmentAsync Error: search [{0}] {1}\n\n{2}", availabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                return new AccommodationB2BWebAvailabilityMapResponse { AvailabilityId = availabilityId, AvailabilityStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        public async Task<AccommodationB2BWebAvailabilityRequest> GetAvailabilityRequestAsync(Guid availabilityId)
        {
            try
            {
                //get the request status
                var requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return null;

                return requestStatus.AvailabilityRequest;
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityRequestAsync Error: search [{0}] {1}", availabilityId, ex.GetDetailedMessageWithInnerExceptions());
                throw ex;
            }
        }

        public async Task<AccommodationB2BWebAvailabilityResultsFilterOptions> GetAvailabilityResultsFilterOptionsAsync(Guid availabilityId)
        {
            try
            {
                //get the request status
                var requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(availabilityId);

                //throw exception when request status is not available
                if (requestStatus == null) return null;

                return requestStatus.ResultsFilterOptions;
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetAvailabilityResultsFilterOptions Error: search [{0}] {1}", availabilityId, ex.GetDetailedMessageWithInnerExceptions());
                throw ex;
            }
        }

        private async Task StartAvailabilityProcessAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, AccommodationB2BWebRequestStatus requestStatus, bool isRestartSearch)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.AvailabilitySearching, availabilityRequest.AvailabilityId.ToString(), "(accommodation)", availabilityRequest.Debugging, logger))
            {
                AccommodationProvider[] providersToSearch = null;
                AccommodationProvider[] providers = null;
                AccommodationAvailabilityResponse coreAvailabilityResponse = null;
                AccommodationB2BWebAvailabilityResultsFilterOptions resultsFilterOptions = null;
                B2BUser agent = null;
                Guid resultsBaseDestinationId = Guid.Empty;
                Guid resultsAvailabilityId = Guid.Empty;
                Exception exception = null;
                var processStartDate = DateTime.Now;
                DateTime? postProcessStartDate = null;
                try
                {
                    //get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(availabilityRequest.Channel);

                    // get agent details
                    agent = await agentRepository.GetByIdAsync(availabilityRequest.AgentId);
                    availabilityRequest.SuppliersToSearch = agent.Suppliers.Select(x => x.EdiCode).ToArray();

                    //create a empty filter option 
                    resultsFilterOptions = resultsFilterOptionsService.CreateEmptyFilterOptions(availabilityRequest, channelInfo);

                    //update Mongo with inprogress status
                    await this.requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Search Started");

                    //Validate Request
                    requestValidator.ValidateAvailabilityRequest(availabilityRequest);

                    //get list of providers 
                    providersToSearch = await providerIdentifierService.GetActiveAvailabilityProvidersAsync(availabilityRequest);

                    //generate the search key
                    var availabilityKey = keyGenerator.CreateAvailabilityKey(availabilityRequest, providersToSearch);

                    //check if the same request was already recently performed
                    var cachedRequestStatus = await requestStatusService.GetSuccessfulRequestStatusByAvailabilityKeyOrNullAsync(availabilityKey);

                    //if results cache is available
                    DateTime resultsCacheExpireDate = requestStatusService.CreateResultsCacheExpireDate();
                    if (cachedRequestStatus != null && cachedRequestStatus.Status == Status.Successful && !requestStatusService.IsResultsExpireDateExpired(cachedRequestStatus)
                        && providersToSearch.All(i => i.IsCoreCacheActive))
                    {
                        //get parameters from the cached requests status
                        resultsCacheExpireDate = cachedRequestStatus.ResultsExpireDate.Value;
                        resultsFilterOptions = cachedRequestStatus.ResultsFilterOptions;
                        resultsBaseDestinationId = cachedRequestStatus.ResultsBaseDestinationId.Value;
                        resultsAvailabilityId = cachedRequestStatus.Id;
                    }
                    else
                    {
                        // set new search
                        resultsAvailabilityId = availabilityRequest.AvailabilityId.Value;

                        //get list of providers 
                        providers = providersToSearch;

                        //create a core availability request
                        var coreAvailabilityRequest = requestFactory.CreateAvailabilityRequest(availabilityRequest, channelInfo, agent, providers);

                        //perform a availability search
                        coreAvailabilityResponse = await coreAvailabilityService.GetAvailabilityResponseAsync(coreAvailabilityRequest);

                        //use the results expire date as a request status expire
                        resultsCacheExpireDate = coreAvailabilityResponse.ResultsCacheExpireDate;

                        //get post process start date
                        postProcessStartDate = coreAvailabilityResponse.PostProcessStartDate;

                        // get base destination
                        resultsBaseDestinationId = coreAvailabilityResponse.Results.Any() ? coreAvailabilityResponse.Results[0].BaseDestinationId : Guid.Empty;

                        //// b2b works only with post payment and net type
                        //var results = coreAvailabilityResponse.Results.Where(i => i.ProviderResult.PaymentModel == PaymentModel.PostPayment && i.ProviderResult.RateType == RateType.NetStandard).ToArray();

                        // remove possible old results from a restart search
                        if (isRestartSearch) await resultsCaching.DeleteRangeAsync(resultsBaseDestinationId, resultsAvailabilityId);

                        //post process only if the core returned results
                        if (coreAvailabilityResponse.Results.Any())
                        {
                            // execute post process for the results
                            accommodationBusiness.FilterBySupplier(availabilityRequest.SuppliersToSearch, agent, coreAvailabilityResponse);
                            var results = (AccommodationAvailabilityResult[])await accommodationBusiness.PostProcessAvailabilityResultsAsync(coreAvailabilityRequest, coreAvailabilityResponse.Results);

                            //map the core results
                            var mappedResults = await resultsMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, results);

                            //create filter and save results only if there are valid results
                            if (mappedResults.Any())
                            {
                                //create filter options based on the availability results
                                resultsFilterOptions = await resultsFilterOptionsService.CreateResultsFilterOptionsAsync(availabilityRequest, channelInfo, mappedResults);

                                //set the cache results to the same expire date
                                mappedResults.ForEach(i => i.CacheExpireDate = resultsCacheExpireDate);

                                //save results
                                await resultsCaching.SaveRangeAsync(resultsBaseDestinationId, mappedResults);
                            }
                        }
                    }
                    //update Mongo Status
                    requestStatus.AvailabilityKey = availabilityKey;
                    requestStatus.ResultsFilterOptions = resultsFilterOptions;
                    requestStatus.ResultsExpireDate = resultsCacheExpireDate;
                    requestStatus.ResultsBaseDestinationId = resultsBaseDestinationId;
                    requestStatus.ResultsAvailabilityId = resultsAvailabilityId;
                    requestStatus.Status = (coreAvailabilityResponse != null && coreAvailabilityResponse.ProcessResponseDetails.Any() && coreAvailabilityResponse.ProcessResponseDetails.All(i => i.Exception != null) ? Status.Failed : Status.Successful);
                    requestStatus.Message = "Accommodation Search Completed";
                }
                catch (Exception ex)
                {
                    //update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation StartSearchProcess Error: search [{0}] {1}\n\n{2}", availabilityRequest.AvailabilityId, ex.GetDetailedMessageWithInnerExceptions(), availabilityRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                if (availabilityRequest.Debugging && coreAvailabilityResponse != null && coreAvailabilityResponse.Results != null)
                {
                    logger.Debug("Accommodation availability search found {0} accommodations for search [{1}] \n\nRequest:\n\n{2}", coreAvailabilityResponse.Results.Count(), availabilityRequest.AvailabilityId, availabilityRequest.ToIndentedJson());
                }
                await availabilityLogger.LogAsync(availabilityRequest, requestStatus, providers, coreAvailabilityResponse
                    , (postProcessStartDate != null ? (TimeSpan?)(DateTime.Now - postProcessStartDate) : null), DateTime.Now - processStartDate, exception);
            }
        }
    }
}
