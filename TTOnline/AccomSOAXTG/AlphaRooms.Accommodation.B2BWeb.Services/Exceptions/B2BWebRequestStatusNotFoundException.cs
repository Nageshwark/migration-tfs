﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services.Exceptions
{
    public class B2BWebRequestStatusNotFoundException : Exception
    {
        private readonly Guid availabilityId;

        public B2BWebRequestStatusNotFoundException(Guid availabilityId) : base("Request status not found (id = '" + availabilityId + "').")
        {
            // TODO: Complete member initialization
            this.availabilityId = availabilityId;
        }

        public Guid AvailabilityId
        {
            get { return this.availabilityId; }
        }
    }
}
