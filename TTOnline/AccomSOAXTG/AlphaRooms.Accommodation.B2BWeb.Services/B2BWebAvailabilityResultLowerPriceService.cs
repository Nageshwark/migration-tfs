﻿using System.Linq;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityResultLowerPriceService: IB2BWebAvailabilityResultLowerPriceService
    {
        public AccommodationB2BWebAvailabilityResult[] ApplyAvailabilityLowerPrice(AccommodationB2BWebAvailabilityResult[] result)
        {
            result.ParallelForEach(r => ApplyAvailabilityLowerPrice(r));
            return result;
        }

        public AccommodationB2BWebAvailabilityResult ApplyAvailabilityLowerPrice(AccommodationB2BWebAvailabilityResult result)
        {
            result.Rooms.GroupBy(i => i, new CustomEqualityComparer<AccommodationB2BWebAvailabilityResultRoom>((i, j) => i.EstablishmentId == j.EstablishmentId
            && i.RoomNumber == j.RoomNumber && i.BoardType == j.BoardType)).AsParallel().Select(i => i.OrderBy(j => j.Price).First()).ForAll(i => i.IsLowestPrice = true);
            return result;
        }
    }
}