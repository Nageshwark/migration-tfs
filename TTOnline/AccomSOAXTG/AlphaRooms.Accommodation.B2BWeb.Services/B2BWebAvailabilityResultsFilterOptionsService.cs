﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityResultsFilterOptionsService : IB2BWebAvailabilityResultsFilterOptionsService
    {
        private readonly IB2BWebPaymentModelConverter paymentModelConverter;
        private readonly IBoardRepository boardRepository;
        private readonly IPaymentTypeRepository paymentTypeRepository;
        private readonly IStarRatingRepository starRatingRepository;
        
        public B2BWebAvailabilityResultsFilterOptionsService(IB2BWebPaymentModelConverter paymentModelConverter, IBoardRepository boardRepository, IPaymentTypeRepository paymentTypeRepository
            , IStarRatingRepository starRatingRepository)
        {
            this.paymentModelConverter = paymentModelConverter;
            this.boardRepository = boardRepository;
            this.paymentTypeRepository = paymentTypeRepository;
            this.starRatingRepository = starRatingRepository;
        }

        public AccommodationB2BWebAvailabilityResultsFilterOptions CreateEmptyFilterOptions(AccommodationB2BWebAvailabilityRequest availabilityRequest, ChannelInfo channelInfo)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptions()
            {
                PriceMin = null
                , PriceMax = null
                , Boards = new AccommodationB2BWebAvailabilityResultsFilterOptionBoardType[0]
                , Facilities = new AccommodationB2BWebAvailabilityResultsFilterOptionItem[0]
                , PaymentTypes = new AccommodationB2BWebAvailabilityResultsFilterOptionPaymentType[0]
                , Providers = new AccommodationB2BWebAvailabilityResultsFilterOptionProvider[0]
                , TravellerTypes = new AccommodationB2BWebAvailabilityResultsFilterOptionItem[0]
                , StarRatings = new AccommodationB2BWebAvailabilityResultsFilterOptionStarRating[0]
                , Districts = new AccommodationB2BWebAvailabilityResultsFilterOptionItem[0]
            };
        }

        public async Task<AccommodationB2BWebAvailabilityResultsFilterOptions> CreateResultsFilterOptionsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, ChannelInfo channelInfo
            , IList<AccommodationB2BWebAvailabilityResult> results)
        {
            var boards = await this.boardRepository.GetAllDictionaryAsync();
            var paymentTypes = await paymentTypeRepository.GetAllDictionaryAsync();
            var starRatings = await this.starRatingRepository.GetAllDictionaryAsync();
            var lowestPriceRooms = results.SelectMany(i => i.Rooms).Where(i => i.IsLowestPrice).ToArray();
            return new AccommodationB2BWebAvailabilityResultsFilterOptions()
            {
                PriceMin = lowestPriceRooms.Min(j => j.Price)
                , PriceMax = lowestPriceRooms.Max(j => j.Price)
                , Boards = lowestPriceRooms.Select(j => j.BoardType).Distinct().Select(i => CreateFilterOptionBoard(i, boards)).OrderBy(i => i.Name).ToArray()
                , Facilities = results.SelectMany(i => i.EstablishmentFacilities).DistinctBy(i => i.Name).Select(i => CreateFilterOptionFacility(i)).OrderBy(i => i.Name).ToArray()
                , PaymentTypes = lowestPriceRooms.Select(j => j.PaymentModel).Select(i => paymentModelConverter.ToPaymentType(i)).Distinct().Select(i => CreateFilterOptionPaymentType(i, paymentTypes)).OrderBy(i => i.Name).ToArray()
                , Providers = lowestPriceRooms.DistinctBy(i => i.ProviderEdiCode).Select(i => CreateFilterOptionProvider(i)).OrderBy(i => i.ProviderName).ToArray()
                , TravellerTypes = results.SelectMany(i => i.EstablishmentTravellerTypes).DistinctBy(i => i.Name).Select(i => CreateFilterOptionTravellerType(i)).OrderBy(i => i.Name).ToArray()
                , StarRatings = results.Select(i => i.EstablishmentStarRating).Distinct().Select(i => CreateFilterOptionStarRatings(i, starRatings)).OrderBy(i => i.Name).ToArray()
                , EstablishmentNames = results.Select(i => i.EstablishmentName).Distinct().OrderBy(i => i).ToArray()
                , Districts = results.Where(i => i.DestinationId != availabilityRequest.DestinationId && i.IsDestinationDistrict).DistinctBy(i => i.DestinationId).Select(i => CreateFilterOptionDistrict(i)).OrderBy(i => i.Name).ToArray()
            };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionBoardType CreateFilterOptionBoard(BoardType boardType, Dictionary<BoardType, Board> boards)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionBoardType() { BoardType = boardType, Name = boards[boardType].Name };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionPaymentType CreateFilterOptionPaymentType(PaymentTypeType paymentType, Dictionary<PaymentTypeType, PaymentType> paymentTypes)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionPaymentType() { PaymentType = paymentType, Name = paymentTypes[paymentType].Name};
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionStarRating CreateFilterOptionStarRatings(StarRatingType starRating, Dictionary<StarRatingType, StarRating> starRatings)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionStarRating() { StarRating = starRating, Name = starRatings[starRating].Name };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionItem CreateFilterOptionTravellerType(TravellerType travellerType)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionItem() { Id = travellerType.Id, Name = travellerType.Name };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionItem CreateFilterOptionFacility(Facility facility)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionItem() { Id = facility.Reference, Name = facility.Name };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionProvider CreateFilterOptionProvider(AccommodationB2BWebAvailabilityResultRoom roomResult)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionProvider() { ProvideEdiCode = roomResult.ProviderEdiCode, ProviderName = roomResult.ProviderName };
        }

        private AccommodationB2BWebAvailabilityResultsFilterOptionItem CreateFilterOptionDistrict(AccommodationB2BWebAvailabilityResult i)
        {
            return new AccommodationB2BWebAvailabilityResultsFilterOptionItem() { Id = i.DestinationReference, Name = i.DestinationName };
        }
    }
}