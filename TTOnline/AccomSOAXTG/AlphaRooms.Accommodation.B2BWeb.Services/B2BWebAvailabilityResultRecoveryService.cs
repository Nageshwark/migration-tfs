﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Interfaces.Caching;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityResultRecoveryService : IB2BWebAvailabilityResultRecoveryService
    {
        private readonly IAvailabilityResultRecoveryService availabilityResultRecoveryService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BWebAvailabilityProviderIdentifierService providerIdentifierService;
        private readonly IB2BWebAvailabilityRequestFactory requestFactory;
        private readonly IB2BWebAvailabilityResultMappingService resultsMappingService;
        private readonly IB2BWebAvailabilityResultCaching resultsCaching;
        private readonly IB2BUserRepository agentRepository;
        private readonly IB2BWebRoomIdService roomIdService;

        public B2BWebAvailabilityResultRecoveryService(IAvailabilityResultRecoveryService availabilityResultRecoveryService, IChannelInfoService channelInfoService
            , IB2BWebAvailabilityProviderIdentifierService providerIdentifierService, IB2BWebAvailabilityRequestFactory requestFactory, IB2BWebAvailabilityResultCaching resultsCaching
            , IB2BWebAvailabilityResultMappingService resultsMappingService, IB2BUserRepository agentRepository, IB2BWebRoomIdService roomIdService)
        {
            this.availabilityResultRecoveryService = availabilityResultRecoveryService;
            this.channelInfoService = channelInfoService;
            this.providerIdentifierService = providerIdentifierService;
            this.requestFactory = requestFactory;
            this.resultsMappingService = resultsMappingService;
            this.resultsCaching = resultsCaching;
            this.agentRepository = agentRepository;
            this.roomIdService = roomIdService;
        }

        public async Task<AccommodationB2BWebAvailabilityResult[]> RecoverRoomsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, string[] roomIds)
        {
            throw new NotImplementedException();
            //var coreRoomIds = roomIds.Select(i => roomIdService.ConvertToCoreRoomId(i)).ToArray();
            //var channelInfo = await channelInfoService.GetChannelInfo(availabilityRequest.Channel);
            //var agent = await agentRepository.GetByIdAsync(availabilityRequest.AgentId);
            //var providers = await providerIdentifierService.GetActiveAvailabilityProvidersAsync(availabilityRequest);
            //var coreAvailabilityRequest = requestFactory.CreateAvailabilityRequest(availabilityRequest, channelInfo, agent, providers);
            //var coreAvailabilityResults = await availabilityResultRecoveryService.ProcessAvailabilityRecoveryAsync(coreAvailabilityRequest, coreRoomIds);
            //var availabilityResults = await resultsMappingService.MapFromCoreAvailabilityResultsAsync(availabilityRequest, coreAvailabilityResults);
            //availabilityResults[0].Rooms.ForEach((i, j) => i.RoomId = roomIds[j]);
            //await resultsCaching.SaveRangeAsync(coreAvailabilityResults[0].BaseDestinationId, availabilityResults);
            //return availabilityResults.ToArray();
        }
    }
}
