﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Utilities;
using AutoMapper;
using Ninject.Extensions.Logging;
using AlphaRooms.Utilities.ExtensionMethods;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebCancellationResultMappingService : IB2BWebCancellationResultMappingService
    {
        static B2BWebCancellationResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationCancellationResult, AccommodationB2BWebCancellationResult>();
                //Mapper.CreateMap<AccommodationCancellationResultRoom, AccommodationB2BWebCancellationResultRoom>();
            }
        }
        
        private readonly ILogger logger;

        public B2BWebCancellationResultMappingService(ILogger logger)
        {
            this.logger = logger;
        }

        public AccommodationB2BWebCancellationResult MapFromCoreCancellationResults(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationCancellationResult coreCancellationResult)
        {
            AccommodationB2BWebCancellationResult result = null;
            try
            {
                result = Mapper.Map<AccommodationCancellationResult, AccommodationB2BWebCancellationResult>(coreCancellationResult);
                //result.Rooms = coreCancellationResult.Rooms.Select(i => CreateAccommodationB2BWebCancellationResultRoom(cancellationRequest, i)).ToArray();

            }
            catch (Exception ex)
            {
                logger.Error("Accommodation MapFromCoreCancellationResultsAsync Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreCancellationResult.ToIndentedJson());
            }
            return result;
        }

        //private AccommodationB2BWebCancellationResultRoom CreateAccommodationB2BWebCancellationResultRoom(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationCancellationResultRoom coreCancellationResultRoom)
        //{
        //    var result = Mapper.Map<AccommodationCancellationResultRoom, AccommodationB2BWebCancellationResultRoom>(coreCancellationResultRoom);
        //    return result;
        //}
    }
}
