﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Services;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.Providers;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAccommodationBusiness : IAccommodationBusiness
    {
        private readonly IAvailabilityResultLowerPriceService resultLowerPriceService;
        private readonly IAvailabilityResultConsolidationService resultConsolidationService;
        private readonly IAvailabilityResultMarkupService resultMarkupService;
        private readonly IAvailabilityFlashSaleService flashSaleService;
        private readonly AccommodationPromotionalDiscountService promotionalDiscountService;
        private readonly IPriceCalculatorService priceCalculatorService;

        public B2BWebAccommodationBusiness(IPriceCalculatorService priceCalculatorService, AccommodationPromotionalDiscountService promotionalDiscountService, IAvailabilityFlashSaleService flashSaleService, IAvailabilityResultMarkupService resultMarkupService, IAvailabilityResultConsolidationService resultConsolidationService, IAvailabilityResultLowerPriceService resultLowerPriceService)
        {
            this.priceCalculatorService = priceCalculatorService;
            this.promotionalDiscountService = promotionalDiscountService;
            this.flashSaleService = flashSaleService;
            this.resultMarkupService = resultMarkupService;
            this.resultConsolidationService = resultConsolidationService;
            this.resultLowerPriceService = resultLowerPriceService;
        }

        public async Task<IList<AccommodationAvailabilityResult>> PostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
        {
            // b2b works only with post payment and net type
            results = results.Where(i => i.ProviderResult.PaymentModel == PaymentModel.PostPayment && i.ProviderResult.RateType == RateType.NetStandard).ToArray();
            
            // filter payDirect nonRefundable - 
            //TODO: THIS IS A HACK. NEED TO REMOVE FROM HERE AND MOVE TO PROVIDERS
            results = results.Where(i => i.ProviderResult.PaymentModel != SOACommon.Contracts.Enumerators.PaymentModel.CustomerPayDirect
                                                    || i.ProviderResult.IsNonRefundable == false).ToArray();

            // filter results for flash sale
            var timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_FilterFlashSale");
            results = await flashSaleService.FilterResultsForFlashSaleAsync(availabilityRequest, results);
            timer.Record();

            // apply markup 
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Markups");
            results = await resultMarkupService.ApplyAvailabilityMarkupAsync(availabilityRequest, results);
            timer.Record();

            // process room switching
            //results = roomSwitchingService.ApplyAvailabilityRoomSwitching(availabilityRequest, results);

            // process price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Pricing");
            results = priceCalculatorService.ApplyAvailabilityPriceCalculation(availabilityRequest, results);
            timer.Record();

            //Apply promotional discounts
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Promotions");
            promotionalDiscountService.ProcessDiscounts(results, availabilityRequest.Voucher, availabilityRequest.ChannelInfo.Channel);
            timer.Record();

            // consolidate the accommodation results and highest price
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_Consolidate");
            results = resultConsolidationService.ConsolidateAvailabilityResults(availabilityRequest, results);
            timer.Record();

            // identify the lower price rooms
            timer = new SearchTimerService(availabilityRequest.AvailabilityId, "Search_Postprocess_LowerPriec");
            results = resultLowerPriceService.ApplyAvailabilityLowerPrice(results);
            timer.Record();

            return results.ToArray();
        }

        public void FilterBySupplier(string[] suppliersToSearch, B2BUser agent, AccommodationAvailabilityResponse coreAvailabilityResponse)
        {
            // If a provider is mapped against an agent, by default it takes all suppliers associated to it.
            // In the other hand if you want to restrict by supplier you have to map it by agent (especially for Aria Contracts).
            if (suppliersToSearch == null || !suppliersToSearch.Any())
            {
                return;
            }

            // Filter results by supplier
            var results = coreAvailabilityResponse.Results;
            var supplierResults = results.Where(r => r.Supplier.EdiCode != null && suppliersToSearch.Contains(r.Supplier.EdiCode)).ToArray();

            var providers = agent.Providers.Select(x => x.EdiCode).Distinct().ToArray();
            var ediProvidersFromSuppliers =
                agent.Providers.Where(x => agent.Suppliers.Select(y => y.ProviderId).Contains(x.Id))
                    .Select(z => z.EdiCode)
                    .Distinct()
                    .ToArray();

            var providersToSearch = providers.Except(ediProvidersFromSuppliers.Intersect(providers)).ToArray();

            var providerResults = results.Where(r => providersToSearch.Contains(r.Provider.EdiCode));

            coreAvailabilityResponse.Results = supplierResults.Union(providerResults).ToArray();
        }

		public IList<AccommodationAvailabilityResult> TravelGatePostProcessAvailabilityResultsAsync(AccommodationAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> results)
		{
			throw new NotImplementedException();
		}
	}
}
