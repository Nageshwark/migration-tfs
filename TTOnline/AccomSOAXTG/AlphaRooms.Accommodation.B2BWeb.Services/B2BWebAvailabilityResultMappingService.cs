﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Contracts.Enums;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using AutoMapper;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebAvailabilityResultMappingService : IB2BWebAvailabilityResultMappingService
    {
        static B2BWebAvailabilityResultMappingService()
        {
            lock (Automapper.MapperCreateMapLock)
            {
                Mapper.CreateMap<AccommodationProviderAvailabilityResult, AccommodationB2BWebAvailabilityResultRoom>();
                Mapper.CreateMap<AccommodationAvailabilityResult, AccommodationB2BWebAvailabilityResultRoom>();
                Mapper.CreateMap<AccommodationB2BWebAvailabilityResultRoom, AccommodationAvailabilityResult>();
            }
        }

        private readonly ILogger logger;
        private readonly IEstablishmentDetailsRepository establishmentDetailsRepository;
        private readonly IBoardRepository boardTypeRepository;
        private readonly ITravellerTypeRepository travellerTypeRepository;
        private readonly IFacilityRepository facilityRepository;
        private readonly IDestinationRepository destinationRepository;
        private readonly IB2BWebRoomIdService roomIdService;
        private readonly IAccommodationProviderRepository providerRepository;
        private readonly IAccommodationSupplierRepository supplierRepository;

        public B2BWebAvailabilityResultMappingService(ILogger logger, IEstablishmentDetailsRepository establishmentDetailsRepository, IBoardRepository boardTypeRepository
            , IFacilityRepository facilityRepository, ITravellerTypeRepository travellerTypeRepository, IDestinationRepository destinationRepository
            , IB2BWebRoomIdService roomIdService, IAccommodationSupplierRepository supplierRepository, IAccommodationProviderRepository providerRepository)
        {
            this.logger = logger;
            this.establishmentDetailsRepository = establishmentDetailsRepository;
            this.boardTypeRepository = boardTypeRepository;
            this.travellerTypeRepository = travellerTypeRepository;
            this.facilityRepository = facilityRepository;
            this.destinationRepository = destinationRepository;
            this.roomIdService = roomIdService;
            this.supplierRepository = supplierRepository;
            this.providerRepository = providerRepository;
        }

        public async Task<IList<AccommodationB2BWebAvailabilityResult>> MapFromCoreAvailabilityResultsAsync(AccommodationB2BWebAvailabilityRequest availabilityRequest, IList<AccommodationAvailabilityResult> coreResults)
        {
            if (!coreResults.Any()) return new AccommodationB2BWebAvailabilityResult[0];
            var establishmentDetails = await establishmentDetailsRepository.GetByDestinationIdDictionaryGroupEstablishmentIdAsync(coreResults[0].BaseDestinationId, availabilityRequest.Channel);
            var boardTypes = await boardTypeRepository.GetAllDictionaryAsync();
            var travellerTypes = await travellerTypeRepository.GetAllDictionaryAsync();
            var facilities = await facilityRepository.GetAllDictionaryAsync();

            var results = coreResults.GroupBy(i => i.EstablishmentId).AsParallel().Select(coreResult => 
            {
                AccommodationB2BWebAvailabilityResult result = null;
                try
                {
                    var establishmentResults = coreResult.ToArray();
                    var establishmentId = coreResult.Key;
                    var firstRoomResult = establishmentResults[0];
                    int duration = (int)(availabilityRequest.CheckOutDate - availabilityRequest.CheckInDate).TotalDays;
                    var establishmentDetail = establishmentDetails[establishmentId];
                    var cheapestRoom = establishmentResults.MinBy(i => i.Price);
                    var highestPrice = establishmentResults.Where(i => i.IsLowestPrice).Max(i => i.Price);

                    result = new AccommodationB2BWebAvailabilityResult()
                    {
                        AvailabilityId = availabilityRequest.AvailabilityId.Value
                        , DestinationId = firstRoomResult.DestinationId
                        , EstablishmentId = establishmentId
                        , EstablishmentFacilities = establishmentDetail.FacilityIds.Select(i => facilities[i]).ToArray()
                        , EstablishmentTravellerTypes = establishmentDetail.TravellerTypeIds.Select(i => travellerTypes[i]).ToArray()
                        , Rooms = establishmentResults.Select(i => CreateB2BWebAvailabilityResultRoom(i, boardTypes, duration)).ToArray()
                        , RoomsCount = establishmentResults.Length
                        , RoomsCheapestPrice = cheapestRoom.Price
                        , RoomsCheapestPricePerNight = cheapestRoom.PricePerNight
                        , RoomsHighestPrice = highestPrice
                        , CacheExpireDate = establishmentResults.Min(i => i.CacheExpireDate)

                        , DestinationReference = establishmentDetail.DestinationReference
                        , IsDestinationDistrict = establishmentDetail.IsDestinationDistrict
                        , CountrySeoFriendlyName = establishmentDetail.CountrySeoFriendlyName
                        , DestinationName = establishmentDetail.DestinationName
                        , DestinationSeoFriendlyName = establishmentDetail.DestinationSeoFriendlyName
                        , EstablishmentName = establishmentDetail.EstablishmentName
                        , EstablishmentSeoFriendlyName = establishmentDetail.EstablishmentSeoFriendlyName
                        , EstablishmentDescription = establishmentDetail.EstablishmentDescription
                        , EstablishmentType = (EstablishmentType)Enum.Parse(typeof(EstablishmentType), establishmentDetail.EstablishmentType)
                        , EstablishmentStarRating = firstRoomResult.EstablishmentStarRating
                        , EstablishmentAddressLine1 = establishmentDetail.EstablishmentAddressLine1
                        , EstablishmentAddressLine2 = establishmentDetail.EstablishmentAddressLine2
                        , EstablishmentAddressCity = establishmentDetail.EstablishmentAddressCity
                        , EstablishmentAddressPostCode = establishmentDetail.EstablishmentAddressPostCode
                        , EstablishmentTopImageUrl = establishmentDetail.EstablishmentTopImageUrl
                        , EstablishmentReviewCount = establishmentDetail.EstablishmentReviewCount
                        , EstablishmentReviewAverageScore = establishmentDetail.EstablishmentReviewAverageScore
                        , EstablishmentTripAdvisorAverageScore = establishmentDetail.EstablishmentTripAdvisorAverageScore
                        , EstablishmentTripAdvisorReviewCount = establishmentDetail.EstablishmentTripAdvisorReviewCount
                        , EstablishmentLongitude = (double?)establishmentDetail.EstablishmentLongitude
                        , EstablishmentLatitude = (double?)establishmentDetail.EstablishmentLatitude
                        , EstablishmentPopularity = establishmentDetail.EstablishmentPopularity
                    };
                }
                catch (Exception ex)
                {
                    logger.Error("Accommodation MapFromCoreAvailabilityResults Error: result [{0}] {1}\n\n{2}", "(unknown)", ex.Message, ex.StackTrace, coreResult.ToIndentedJson());
                }
                return result;
            }).Where(i => i != null).ToArray();
            uint id = 1;
            results.ForEach(i => i.Rooms.ForEach(j => { j.RoomIdAlpha2 = id++; roomIdService.ApplyAvailabilityB2BWebRoomId(j); }));
            return results;
        }

        public AccommodationAvailabilityResult[] MapToCoreAvailabilityResult(IList<AccommodationB2BWebAvailabilityResult> availabilityResults)
        {
            var accommodationAvailabilityResults = new List<AccommodationAvailabilityResult>();

            availabilityResults.ForEach(availabilityResult =>
            {
                availabilityResult.Rooms.ForEach(r =>
                {
                    var result = CreateAccommodationAvailabilityResult(r, availabilityResult);
                    if (result != null)
                    {
                        accommodationAvailabilityResults.Add(result);
                    }
                });
            });

            return accommodationAvailabilityResults.ToArray();
        }

        private AccommodationB2BWebAvailabilityResultRoom CreateB2BWebAvailabilityResultRoom(AccommodationAvailabilityResult coreResult, IDictionary<BoardType, Board> boards, int duration)
        {
            return new AccommodationB2BWebAvailabilityResultRoom()
            {
                Id = coreResult.Id,
                RoomId = coreResult.RoomId,
                DestinationId = coreResult.DestinationId,
                EstablishmentId = coreResult.EstablishmentId,
                ProviderEstablishmentCode = coreResult.ProviderResult.ProviderEstablishmentCode ?? coreResult.ProviderResult.EstablishmentEdiCode,
                ProviderName = coreResult.ProviderResult.ProviderName ?? coreResult.Provider.Name,
                ProviderEdiCode = coreResult.ProviderResult.ProviderProviderCode ?? coreResult.Provider.EdiCode,
                ProviderFilterName = coreResult.ProviderResult.ProviderFilterName ?? coreResult.Provider.Name,
                ProviderPrice = coreResult.ProviderResult.CostPrice,
                CheckInDate = coreResult.ProviderResult.CheckInDate,
                CheckOutDate = coreResult.ProviderResult.CheckOutDate,
                AdultsCount = coreResult.AdultsCount,
                ChildrenCount = coreResult.ChildrenCount,
                InfantsCount =  coreResult.InfantsCount,
                RoomDescription = coreResult.RoomDescription,
                RoomNumber = coreResult.RoomNumber,
                ProviderRoomCode = coreResult.ProviderResult.RoomCode,
                BoardDescription = boards[coreResult.BoardType].Name,
                BoardType = coreResult.BoardType,
                ExchangeRate = coreResult.ExchangeRate,
                HighestPrice = coreResult.HighestPrice,
                IsBindingRate = coreResult.ProviderResult.IsBindingRate,
                IsCardChargeApplicable = (coreResult.ProviderResult.PaymentModel != PaymentModel.CustomerPayDirect),
                IsLowestPrice = coreResult.IsLowestPrice,
                IsNonRefundable = coreResult.ProviderResult.IsNonRefundable,
                IsOpaqueRate = coreResult.ProviderResult.IsOpaqueRate,
                Margin = coreResult.Margin,
                PaymentModel = coreResult.ProviderResult.PaymentModel,
                PaymentToTake = coreResult.PaymentToTake,
                Price = coreResult.Price,
                PricePerNight = coreResult.PricePerNight,
                RateType = coreResult.ProviderResult.RateType,                         
                Discount = coreResult.DiscountPrice,
                ProviderSpecificData = coreResult.ProviderResult.ProviderSpecificData,
                SupplierEdiCode = coreResult.ProviderResult.SupplierEdiCode,
                ProviderDestinationCode = coreResult.ProviderResult.DestinationEdiCode,
                ProviderContract = coreResult.Provider.ContractDependencyMode == ContractDependencyMode.Dependent ? coreResult.ProviderResult.Contract : null,
                ProviderProviderCode = coreResult.ProviderResult.ProviderEdiCode,
                ProviderRoomDescription = coreResult.ProviderResult.RoomDescription
            };
        }
        
        private AccommodationAvailabilityResult CreateAccommodationAvailabilityResult(AccommodationB2BWebAvailabilityResultRoom room, AccommodationB2BWebAvailabilityResult result)
        {
            var providerTask = providerRepository.GetAllAsync();
            var supplierTask = supplierRepository.GetByEdiCodeAsync(room.SupplierEdiCode);

            Task.WhenAll(providerTask, supplierTask);

            var providers = providerTask.Result;
            var supplier = supplierTask.Result;
            var destinationId = result.DestinationId;
            var establishmentStarRating = result.EstablishmentStarRating;
            var checkIn =   room.CheckInDate;
            var checkOut = room.CheckOutDate;

            var resultRoom = Mapper.Map<AccommodationB2BWebAvailabilityResultRoom, AccommodationAvailabilityResult>(room);

            resultRoom.DiscountPrice = room.Discount;
            resultRoom.DestinationId = destinationId;
            resultRoom.EstablishmentStarRating = establishmentStarRating;
            resultRoom.RoomDescription = room.RoomDescription ?? room.ProviderRoomDescription;
            resultRoom.Provider = providers.FirstOrDefault(p => p.EdiCode == room.ProviderProviderCode);
            resultRoom.ProviderId = resultRoom.Provider?.Id ?? 0;
            resultRoom.Supplier = supplier;
            resultRoom.SalePrice = room.Price;
            resultRoom.Price = room.Price;
            resultRoom.CostPrice = room.ProviderPrice;

            resultRoom.ProviderResult = new AccommodationProviderAvailabilityResult
            {
                ProviderEstablishmentCode = room.ProviderEstablishmentCode,
                EstablishmentEdiCode = room.EstablishmentId.ToString(),
                ProviderName = room.ProviderName,
                ProviderEdiCode = room.ProviderEdiCode,
                ProviderProviderCode = room.ProviderProviderCode,
                ProviderFilterName = room.ProviderFilterName,
                CostPrice = room.ProviderPrice,
                SalePrice = room.ProviderPrice,
                IsBindingRate = room.IsBindingRate,
                IsNonRefundable = room.IsNonRefundable,
                IsOpaqueRate = room.IsOpaqueRate,
                PaymentModel = room.PaymentModel,
                RateType = room.RateType,
                ProviderSpecificData = room.ProviderSpecificData,
                SupplierEdiCode = room.SupplierEdiCode,
                DestinationEdiCode = room.ProviderDestinationCode,
                CheckInDate = checkIn,
                CheckOutDate = checkOut,
                Contract = room.ProviderContract,
                BoardCode = room.BoardType.ToString(),
                BoardDescription = room.BoardDescription,
                RoomNumber = (byte)room.RoomNumber,
                RoomDescription = room.ProviderRoomDescription,
                RoomCode = room.ProviderRoomCode,
                Adults = room.AdultsCount,
                Children = room.ChildrenCount,
                Infants = room.InfantsCount
            };

            return resultRoom;
        }
    }

}