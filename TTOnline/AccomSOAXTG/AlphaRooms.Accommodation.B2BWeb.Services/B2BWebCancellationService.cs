﻿using AlphaRooms.Accommodation.B2BWeb.Contracts;
using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using AlphaRooms.Accommodation.B2BWeb.Services.Exceptions;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebCancellationService : IB2BWebCancellationService
    {
        private readonly IB2BWebRequestStatusService requestStatusService;
        private readonly ILogger logger;
        private readonly IB2BWebCancellationRequestFactory requestFactory;
        private readonly ICancellationService coreCancellationService;
        private readonly IChannelInfoService channelInfoService;
        private readonly IB2BWebCancellationResultMappingService resultMappingService;
        private readonly IB2BWebCancellationRequestValidator requestValidator;
        private readonly IB2BWebCancellationLogger cancellationLogger;
        private readonly IB2BUserRepository agentRepository;

        public B2BWebCancellationService(IB2BWebRequestStatusService requestStatusService, ILogger logger, IB2BWebCancellationRequestFactory requestFactory
            , ICancellationService coreCancellationService, IChannelInfoService channelInfoService, IB2BWebCancellationResultMappingService resultMappingService
            , IB2BWebCancellationRequestValidator requestValidator, IB2BWebCancellationLogger cancellationLogger, IB2BUserRepository agentRepository)
        {
            this.requestStatusService = requestStatusService;
            this.logger = logger;
            this.requestFactory = requestFactory;
            this.coreCancellationService = coreCancellationService;
            this.channelInfoService = channelInfoService;
            this.resultMappingService = resultMappingService;
            this.requestValidator = requestValidator;
            this.cancellationLogger = cancellationLogger;
            this.agentRepository = agentRepository;
        }

        public async Task<Guid> StartCancellationProcessAsync(AccommodationB2BWebCancellationRequest cancellationRequest)
        {
            // create a booking id if the caller didn't provide one
            if (cancellationRequest.CancellationId == null) cancellationRequest.CancellationId = Guid.NewGuid();

            // Initialize new Accommodation Status and update Mongo with Request status Not started
            var requestStatus = requestStatusService.CreateCancellationRequestStatus(cancellationRequest);
            await requestStatusService.SaveRequestStatusAsync(requestStatus);

            // start the booking process
            #pragma warning disable 4014
            Task.Run(async() => await StartCancellationProcessAsync(cancellationRequest, requestStatus));
            #pragma warning restore 4014
            return cancellationRequest.CancellationId.Value;
        }

        public async Task<AccommodationB2BWebCancellationResponse> GetCancellationResponseAsync(Guid cancellationId)
        {
            AccommodationB2BWebCancellationRequest cancellationRequest = null;
            AccommodationB2BWebRequestStatus requestStatus = null;
            try 
            {
                // get the request status
                requestStatus = await this.requestStatusService.GetRequestStatusOrNullByIdAsync(cancellationId);
                                
                // throw exception when request status is not available
                if (requestStatus == null) new AccommodationB2BWebCancellationResponse() { CancellationId = cancellationId, CancellationStatus = Status.NotFound };

                // if search is not successful, don't continue just return with the status
                if (requestStatus.Status != Status.Successful) return new AccommodationB2BWebCancellationResponse() { CancellationId = cancellationId, CancellationStatus = requestStatus.Status };

                // get valuation request
                cancellationRequest = requestStatus.CancellationRequest;

                using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Booking, cancellationId.ToString(), "(accommodation)", cancellationRequest.Debugging, logger))
                {
                    // perform a cancellation search
                    var coreCancellationResponse = await coreCancellationService.GetCancellationResponseAsync(cancellationId);
                    
                    // map the core results
                    var cancellationResult = resultMappingService.MapFromCoreCancellationResults(cancellationRequest, coreCancellationResponse.Result);

                    // convert to FlightAvailabilityResponse and return
                    return new AccommodationB2BWebCancellationResponse
                    {
                        CancellationId = cancellationId
                        , CancellationStatus = requestStatus.Status
                        , CancellationResult = cancellationResult
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("Accommodation GetCancellationResponse Error: search [{0}] {1}\n\n{2}", cancellationId, ex.GetDetailedMessageWithInnerExceptions(), cancellationRequest.ToIndentedJson());
                return new AccommodationB2BWebCancellationResponse { CancellationId = cancellationId, CancellationStatus = requestStatus != null ? requestStatus.Status : Status.Failed };
            }
        }

        private async Task StartCancellationProcessAsync(AccommodationB2BWebCancellationRequest cancellationRequest, AccommodationB2BWebRequestStatus requestStatus)
        {
            using (new PerformanceAnalyser(SupplierTypes.Accommodation, PerformanceTimerType.Cancellation, cancellationRequest.CancellationId.ToString(), "(accommodation)", cancellationRequest.Debugging, logger))
	        {
                var searchStarted = DateTime.Now;
                AccommodationCancellationResponse coreCancellationResponse = null; 
                Exception exception = null;
                B2BUser agent = null;
                try
                {
                    // get channelinfo from request channel
                    var channelInfo = await channelInfoService.GetChannelInfo(cancellationRequest.Channel);

                    // get agent details
                    agent = await agentRepository.GetByIdAsync(cancellationRequest.AgentId);

                    // update Mongo with inprogress status
                    await requestStatusService.UpdateRequestStatus(requestStatus, Status.InProgress, "Accommodation Cancellation Started");
                    
                    // Validate Request
                    requestValidator.ValidateCancellationRequest(cancellationRequest);
                                        
                    // create a core cancellation request
                    var coreCancellationRequest = requestFactory.CreateCancellationRequest(cancellationRequest, channelInfo, agent);

                    // perform a availability search
                    coreCancellationResponse = await coreCancellationService.ProcessCancellationAsync(coreCancellationRequest);
                    
                    // check if valuation failed
                    if (coreCancellationResponse.ProcessResponseDetails.All(i => i.Exception == null))
                    {
                        // update Mongo Status
                        requestStatus.Status = Status.Successful;
                        requestStatus.Message = "Accommodation Cancellation Completed";
                    }
                    else
                    {
                        requestStatus.Status = Status.Failed;
                        requestStatus.Message = "Accommodation Provider Cancellation Failed";
                    }
                }
                catch (Exception ex)
                {
                    // update Mongo Status
                    requestStatus.Status = Status.Failed;
                    requestStatus.Message = "Error: " + ex.Message;
                    exception = ex;
                    logger.Error("Accommodation Cancellation Error: search [{0}] {1}\n\n{2}", cancellationRequest.CancellationId, ex.GetDetailedMessageWithInnerExceptions(), cancellationRequest.ToIndentedJson());
                }
                await requestStatusService.SaveRequestStatusAsync(requestStatus);
                await cancellationLogger.LogAsync(cancellationRequest, requestStatus, coreCancellationResponse, DateTime.Now - searchStarted, exception);
            }
        }
    }
}
