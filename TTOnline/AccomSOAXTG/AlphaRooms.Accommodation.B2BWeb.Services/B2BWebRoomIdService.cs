﻿using AlphaRooms.Accommodation.B2BWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BWeb.Contracts;

namespace AlphaRooms.Accommodation.B2BWeb.Services
{
    public class B2BWebRoomIdService : IB2BWebRoomIdService
    {
        public void ApplyAvailabilityB2BWebRoomId(AccommodationB2BWebAvailabilityResultRoom availabilityResult)
        {
            availabilityResult.RoomId = availabilityResult.RoomIdAlpha2.ToString("000000") + availabilityResult.RoomId;
        }

        public string ConvertToCoreRoomId(string b2bwebRoomId)
        {
            return b2bwebRoomId.Substring(6);
        }
    }
}
