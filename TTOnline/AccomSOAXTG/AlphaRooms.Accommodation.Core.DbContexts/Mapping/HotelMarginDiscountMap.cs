﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.Core.DbContexts.Mapping
{
    public class HotelMarginDiscountMap : EntityTypeConfiguration<HotelMarginDiscount>
    {
        public HotelMarginDiscountMap()
        {
            this.HasKey(t => t.pkID);
            this.Property(t => t.pkID).HasColumnName("pkID").IsRequired();
            this.Property(t => t.fkEdiProvider).HasColumnName("fkEdiProvider").IsRequired();
            this.Property(t => t.strEdiCode).HasColumnName("strEdiCode").IsRequired();
            this.Property(t => t.dtmStart).HasColumnName("dtmStart").IsRequired();
            this.Property(t => t.dtmEnd).HasColumnName("dtmEnd").IsRequired();
            this.Property(t => t.dclDiscount).HasColumnName("dclDiscount").IsRequired();
            this.Property(t => t.blnCommissionable).HasColumnName("blnCommissionable").IsRequired();
            this.Property(t => t.fkChannelID).HasColumnName("fkChannelID").IsRequired();
            this.Property(t => t.blnDoNotDelete).HasColumnName("blnDoNotDelete").IsRequired();
            this.Property(t => t.blnExcludeFromAdapative).HasColumnName("blnExcludeFromAdapative").IsRequired();
        }
    }
}
