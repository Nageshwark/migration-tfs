﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DomainModels;

namespace AlphaRooms.Accommodation.Core.DbContexts.Mapping
{
    class PriceHikeMap: EntityTypeConfiguration<PriceHikeThreshold>
    {
        public PriceHikeMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("AccommodationSupplierValuationThreshold");

            //this.Property(t => t.ProviderId).HasColumnName("ProviderId");
            //this.Property(t => t.SupplierId).HasColumnName("SupplierId");
            //this.Property(t => t.Threshold).HasColumnName("Threshold");
        }
    }
}
