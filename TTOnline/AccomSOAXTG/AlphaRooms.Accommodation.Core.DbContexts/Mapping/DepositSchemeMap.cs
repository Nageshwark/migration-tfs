﻿namespace AlphaRooms.Accommodation.Core.DbContexts.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using AlphaRooms.Accommodation.Core.DomainModels;

    public class DepositSchemeMap : EntityTypeConfiguration<DepositScheme>
    {
        public DepositSchemeMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("DepositScheme");

            this.Property(t => t.Id).HasColumnName("pkID");
            this.Property(t => t.IsEnabled).HasColumnName("blnIsEnabled");
            this.Property(t => t.IsPercentage).HasColumnName("blnIsPercentage");
            this.Property(t => t.DepositMinSpend).HasColumnName("decDepositMinSpend");
            this.Property(t => t.DepositMinWeeks).HasColumnName("intDepositMinWeeks");
            this.Property(t => t.DepositMinHotelSpend).HasColumnName("decDepositMinHotelSpend");
            this.Property(t => t.DepositDefaultValue).HasColumnName("decDepositDefaultValue");
            this.Property(t => t.BalanceDueWeeksBefore).HasColumnName("intBalanceDueWeeksBefore");
            this.Property(t => t.BalancePaymentMinSpend).HasColumnName("decBalancePaymentMinSpend");
            this.Property(t => t.StartDate).HasColumnName("dtmStart");

            this.Property(t => t.DepositDueWeeksAfter).HasColumnName("intDepositDueWeeksAfter");
            this.Property(t => t.DepositDueMinSpend).HasColumnName("decDepositDueMinSpend");
            this.Property(t => t.DepositDueIsPercentage).HasColumnName("blnDepositDueIsPercentage");
        }
    }
}