﻿

namespace AlphaRooms.Accommodation.Core.DbContexts.Mapping
{
    using System.Data.Entity.ModelConfiguration;

    using AlphaRooms.Accommodation.Core.DomainModels;

    public class DepositSchemeV1Map : EntityTypeConfiguration<DepositSchemeV1>
    {
        public DepositSchemeV1Map()
        {
            this.HasKey(t => t.Id);

            

            this.ToTable("DepositSchemeV1");
            this.Property(t => t.Id).HasColumnName("pkID");
            this.Property(t => t.IsEnabled).HasColumnName("blnEnabled");
            this.Property(t => t.DepositMinHotelSpend).HasColumnName("decDepositMinHotelSpend");
            this.Property(t => t.IsDepositAdminFeeApplicable).HasColumnName("blnDepositAdminFee");
            this.Property(t => t.AdminFee).HasColumnName("decAdminFee");
            this.Property(t => t.IsDepositTopUpPaymentEnabled).HasColumnName("blnDepositTopUpPaymentEnabled");
            this.Property(t => t.DepositValue).HasColumnName("decDepositValue");
            this.Property(t => t.DepositTopUpDueDaysAfterBooking).HasColumnName("intDepositTopUpDueDaysAfterBooking");
            this.Property(t => t.IsDepositTopUpPercentage).HasColumnName("blnDeposittopUpPercentage");
            this.Property(t => t.IsDepositTopUpAdminFee).HasColumnName("blnDepositTopUpAdminFee");
            this.Property(t => t.StartDate).HasColumnName("dtmStart");
            this.Property(t => t.BalancePaymentsMinAmt).HasColumnName("decBalancePaymentsMinAmt");
            this.Property(t => t.MaxInterimPayments).HasColumnName("intMaxInterimPayments");
            this.Property(t => t.InterimPaymentsSchedule).HasColumnName("strInterimPaymentsSchedule");
            this.Property(t => t.DaysBtwInterimPayments).HasColumnName("intDaysBTWInterimPayments");
            this.Property(t => t.FinalBalanceDueDays).HasColumnName("intFinalBalanceDueDays");
            this.Property(t => t.IsInterimAdminFeeApplicable).HasColumnName("blnInteirmAdminFee");
            this.Property(t => t.DepositTopUPValue).HasColumnName("decDepositTopUpValue");
            this.Property(t => t.IsInterimPaymentsApplicable).HasColumnName("blnInteirmPayments");
            this.Property(t => t.createdDate).HasColumnName("dtmCreated");
            this.Property(t => t.IsInterimPercentage).HasColumnName("blnInteirmPercentage");
            this.Property(t => t.InterimAmount).HasColumnName("intInterimAmt");


        }
    }
}
