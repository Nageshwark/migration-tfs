﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Utilities;
namespace AlphaRooms.Accommodation.Core.DbContexts
{
    public class MarkupServiceDbContext : DbContext
    {
        static MarkupServiceDbContext()
        {
            Database.SetInitializer<MarkupServiceDbContext>(null);
        }

        public MarkupServiceDbContext() : base("Name=AlphabedsEntities")
        {
        }

        public DbSet<HotelMarginDiscount> HotelMarginDiscounts { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new HotelMarginDiscountMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task<IEnumerable<HotelMarginDiscount>> GetHotelMarginDiscounts(Guid destinationId, int channel, string ediProvider)
        {
            return await this.ExecuteStoredProcedureAsync<HotelMarginDiscount>("HMD_GetByMasterDestination_And_EdiCode",
                new SqlParameter("MasterDestinationID", DataModule.DbNullIfNull(destinationId)),
                new SqlParameter("ChannelID", DataModule.DbNullIfNull(channel)),
                new SqlParameter("fkEdiProvider", DataModule.DbNullIfNull(ediProvider)));
        }
    }
}
