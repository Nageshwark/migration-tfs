﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.DbContexts
{
    public class DepositSchemeV1DbContext : DbContext
    {

        static DepositSchemeV1DbContext()
        { Database.SetInitializer<MarkupServiceDbContext>(null); }

        public DepositSchemeV1DbContext() : base("Name=AlphabedsEntities")
        {
        }

        public virtual DbSet<DepositSchemeV1> DepositSchemesV1 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DepositSchemeV1Map());
            base.OnModelCreating(modelBuilder);
        }
    }
}