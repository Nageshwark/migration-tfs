﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.DbContexts
{
    public class DepositSchemeDbContext : DbContext
    {

        static DepositSchemeDbContext()
        { Database.SetInitializer<MarkupServiceDbContext>(null); }

        public DepositSchemeDbContext() : base("Name=AlphabedsEntities")
        {
        }

        public virtual DbSet<DepositScheme> DepositSchemes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DepositSchemeMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}