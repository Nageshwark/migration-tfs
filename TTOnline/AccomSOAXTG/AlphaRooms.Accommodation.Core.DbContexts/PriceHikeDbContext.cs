﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.DbContexts.Mapping;
using AlphaRooms.Accommodation.Core.DomainModels;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.DbContexts
{
    public class PriceHikeDbContext : DbContext
    {
        static PriceHikeDbContext()
        {
            Database.SetInitializer<PriceHikeDbContext>(null);
        }

        public PriceHikeDbContext() : base("Name=AlphabedsEntities")
        {
        }

        public virtual DbSet<PriceHikeThreshold> PriceHikeThreshold { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PriceHikeMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
