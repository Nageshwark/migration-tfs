﻿using AlphaRooms.Accommodation.B2BXml.DbContexts;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Repositories
{
    public class B2BXmlValuationLogRepository : IB2BXmlValuationLogRepository
    {
        private readonly IDbContextActivator<B2BXmlLogDbContext> dbLogsContextActivator;

        public B2BXmlValuationLogRepository(IDbContextActivator<B2BXmlLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2BXmlValuationLog[] b2BXmlValuationLog)
        {
            using (var context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2BXmlValuationLogs.AddRange(b2BXmlValuationLog);
                await context.SaveChangesAsync();
            }
        }
    }
}
