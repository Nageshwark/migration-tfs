﻿using AlphaRooms.Accommodation.B2BXml.DbContexts;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Repositories
{
    public class B2BXmlBookingLogRepository : IB2BXmlBookingLogRepository
    {
        private readonly IDbContextActivator<B2BXmlLogDbContext> dbLogsContextActivator;

        public B2BXmlBookingLogRepository(IDbContextActivator<B2BXmlLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveRangeAsync(B2BXmlBookingLog[] b2BXmlValuationLog)
        {
            using (var context = this.dbLogsContextActivator.GetDbContext())
            {
                context.B2BXmlBookingLogs.AddRange(b2BXmlValuationLog);
                await context.SaveChangesAsync();
            }
        }
    }
}
