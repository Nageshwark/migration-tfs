﻿using AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories;
using AlphaRooms.Accommodation.B2BXml.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.DbContexts;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.Accommodation.B2BXml.Repositories
{
    public class B2BXmlAvailabilityLogRepository : IB2BXmlAvailabilityLogRepository
    {
        private readonly IDbContextActivator<B2BXmlLogDbContext> dbLogsContextActivator;

        public B2BXmlAvailabilityLogRepository(IDbContextActivator<B2BXmlLogDbContext> logContext)
        {
            this.dbLogsContextActivator = logContext;
        }

        public async Task SaveAsync(B2BXmlAvailabilityLog b2BXmlAvailabilityLog)
        {
            using (var context = this.dbLogsContextActivator.GetDbContext())
            {
                await context.B2BXmlAvailabilityLogInsertAsync(
                    b2BXmlAvailabilityLog.HostName, b2BXmlAvailabilityLog.AvailabilityId, b2BXmlAvailabilityLog.AvailabilityDate
                    , (byte)b2BXmlAvailabilityLog.AvailabilityType, (byte?)b2BXmlAvailabilityLog.Status, b2BXmlAvailabilityLog.Request, b2BXmlAvailabilityLog.ResultsCount
                    , b2BXmlAvailabilityLog.EstablishmentResultsCount, b2BXmlAvailabilityLog.Providers, b2BXmlAvailabilityLog.ProviderSearchTypes, b2BXmlAvailabilityLog.ProviderResultsCount
                    , b2BXmlAvailabilityLog.ProviderEstablishmentResultsCount, b2BXmlAvailabilityLog.ProviderTimeTaken, b2BXmlAvailabilityLog.ProviderExceptions
                    , b2BXmlAvailabilityLog.PostProcessTimeTaken, b2BXmlAvailabilityLog.TimeTaken, b2BXmlAvailabilityLog.Exceptions);
            }
        }
    }
}
