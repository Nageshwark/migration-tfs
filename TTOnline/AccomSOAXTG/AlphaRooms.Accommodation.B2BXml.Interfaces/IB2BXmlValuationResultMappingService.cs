﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlValuationResultMappingService
    {
        Task<OTA_HotelValRS> MapFromCoreValuationResultsAsync(AccommodationValuationRequest coreValuationRequest, AccommodationValuationResult coreResult, string valuationId);
    }
}
