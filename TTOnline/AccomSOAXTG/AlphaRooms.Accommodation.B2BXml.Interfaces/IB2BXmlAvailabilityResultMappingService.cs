﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlAvailabilityResultMappingService
    {
        Task<OTA_HotelAvailRS> MapFromCoreAvailabilityResultsAsync(AccommodationAvailabilityRequest coreAvailabilityRequest, IList<AccommodationAvailabilityResult> coreResult, string availabilityId);
    }
}
