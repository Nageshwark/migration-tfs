﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlBookingRequestFactory
    {
        AccommodationBookingRequest CreateBookingRequest(OTA_HotelResRQ otaHotelResRQ, Guid bookingId, ChannelInfo channelInfo, B2BUser agent, Guid valuationId, AccommodationValuationResult valuationResult);
    }
}
