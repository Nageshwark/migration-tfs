﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlAvailabilityRequestFactory
    {
        AccommodationAvailabilityRequest CreateAvailabilityRequest(OTA_HotelAvailRQ otaHotelAvailRQ, Guid availabilityId, ChannelInfo channelInfo, B2BUser agent, AccommodationProvider[] providers);
    }
}
