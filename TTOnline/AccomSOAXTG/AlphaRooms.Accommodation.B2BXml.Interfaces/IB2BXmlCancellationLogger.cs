﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlCancellationLogger
    {
        Task LogAsync(Guid cancellationId, DateTime processStartDate, string request, Status processStatus, AccommodationCancellationRequest coreCancellationRequest, AccommodationCancellationResponse coreCancellationResponse, TimeSpan timeSpan, Exception exception);
    }
}
