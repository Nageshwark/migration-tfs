﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlValuationLogger
    {
        Task LogAsync(Guid valuationId, DateTime valuationStartDate, string request, Status processStatus, AccommodationValuationRequest coreValuationRequest
            , AccommodationValuationResponse coreValuationResponse, TimeSpan timeSpan, Exception exception);
    }
}
