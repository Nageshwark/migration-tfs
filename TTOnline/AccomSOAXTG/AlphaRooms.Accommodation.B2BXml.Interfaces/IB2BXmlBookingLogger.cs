﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlBookingLogger
    {
        Task LogAsync(Guid bookingId, DateTime processStartDate, string request, Status processStatus, AccommodationBookingRequest coreBookingRequest, AccommodationBookingResponse coreBookingResponse
            , TimeSpan timeSpan, Exception exception);
    }
}
