﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlCancellationRequestValidator
    {
        void ValidateCancellationRequest(OTA_CancelRQ otaCancelRQ);
    }
}
