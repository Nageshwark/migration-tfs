﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlValuationRequestFactory
    {
        AccommodationValuationRequest CreateValuationRequest(OTA_HotelValRQ otaHotelValRQ, Guid valuationId, ChannelInfo channelInfo, B2BUser agent);
    }
}
