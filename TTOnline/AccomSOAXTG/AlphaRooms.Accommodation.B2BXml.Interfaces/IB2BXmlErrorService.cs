﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlErrorService
    {
        string CreateAvailabilityErrorXmlResponse(Exception ex);
        string CreateValuationErrorXmlResponse(Exception ex);        
        string CreateBookingErrorXmlResponse(Exception ex);
        string CreateCancellationErrorXmlResponse(Exception ex);
    }
}
