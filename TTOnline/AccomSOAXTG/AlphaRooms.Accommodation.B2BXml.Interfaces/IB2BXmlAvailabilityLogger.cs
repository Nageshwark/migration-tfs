﻿using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlAvailabilityLogger
    {
        Task LogAsync(AccommodationAvailabilityRequest coreAvailabilityRequest, Guid availabilityId, DateTime availabilityStartDate, Status availabilityStatus, string availabilityRequest, AccommodationProvider[] providers
            , AccommodationAvailabilityResponse coreResponse, TimeSpan? postProcessTimeTaken, TimeSpan timeTaken, Exception exception);
    }
}
