﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlApplyAgentConfigurationService
    {
        IList<AccommodationAvailabilityResult> ApplyAgentConfiguration(AccommodationAvailabilityRequest request, IList<AccommodationAvailabilityResult> results);
    }
}
