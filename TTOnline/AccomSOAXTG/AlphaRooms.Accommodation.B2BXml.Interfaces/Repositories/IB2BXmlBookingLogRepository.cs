﻿using AlphaRooms.Accommodation.B2BXml.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces.Repositories
{
    public interface IB2BXmlBookingLogRepository
    {
        Task SaveRangeAsync(B2BXmlBookingLog[] bookingLogs);
    }
}
