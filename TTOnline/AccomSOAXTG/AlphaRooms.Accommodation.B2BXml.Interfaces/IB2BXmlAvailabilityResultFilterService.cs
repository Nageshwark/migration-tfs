﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.Core.Contracts;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlAvailabilityResultFilterService
    {
        IList<AccommodationAvailabilityResult> FilterResults(OTA_HotelAvailRQ otaHotelAvailRQ, IList<AccommodationAvailabilityResult> results);
    }
}
