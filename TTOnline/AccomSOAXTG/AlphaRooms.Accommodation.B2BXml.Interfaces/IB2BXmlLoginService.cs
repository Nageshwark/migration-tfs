﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelVal;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlLoginService
    {
        Task<B2BUser> LoginAsync(OTA_HotelAvailRQ otaHotelAvailRQ);
        Task<B2BUser> LoginAsync(OTA_HotelValRQ otaHotelValRQ);
        Task<B2BUser> LoginAsync(OTA_HotelResRQ otaHotelAvailRQ);
        Task<B2BUser> LoginAsync(OTA_CancelRQ otaCancelRQ);
    }
}
