﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelAvail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlAvailabilityRequestValidator
    {
        void ValidateAvailabilityRequest(OTA_HotelAvailRQ otaHotelAvailRQ);
    }
}
