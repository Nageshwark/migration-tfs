﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlCancellationResultMappingService
    {
        OTA_CancelRS MapFromCoreCancellationResults(AccommodationCancellationRequest coreCancellationRequest, AccommodationCancellationResult result, string bookingReference);
    }
}
