﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.B2BXml.Contracts.OTACancel;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlCancellationRequestFactory
    {
        AccommodationCancellationRequest CreateCancellationRequest(OTA_CancelRQ otaCancelRQ, Guid cancellationId, ChannelInfo channelInfo, B2BUser clientProfile);
    }
}
