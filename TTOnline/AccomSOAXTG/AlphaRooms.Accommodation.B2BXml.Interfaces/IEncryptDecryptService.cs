﻿namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IEncryptDecryptService
    {
        string Encrypt(string toEncrypt);
        string Decrypt(string toDecrypt);
    }
}