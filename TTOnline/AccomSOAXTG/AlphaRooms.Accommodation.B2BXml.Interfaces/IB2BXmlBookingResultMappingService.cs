﻿using AlphaRooms.Accommodation.B2BXml.Contracts.OTAHotelRes;
using AlphaRooms.Accommodation.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.Interfaces
{
    public interface IB2BXmlBookingResultMappingService
    {
        OTA_HotelResRS MapFromCoreBookingResults(AccommodationBookingRequest coreBookingRequest, AccommodationBookingResult coreResult, AccommodationValuationResult ValuationResult, string bookingReference);
    }
}
