﻿using System;
using System.Configuration;
using System.Security.Policy;
using System.ServiceModel;
using System.Threading.Tasks;
using Alpharooms.Accommodation.B2BXml.TicketService.Infrastructure;
using Alpharooms.Accommodation.B2BXml.TicketService.TicketAdminServiceRef;
using AlphaRooms.SOACommon.Interfaces;

namespace Alpharooms.Accommodation.B2BXml.TicketService.Implementations
{
    public class CancellationTicketService : ICancellationTicketService
    {
        private readonly IEndpointConfiguration _cancellationTicketEndpointConfiguration;
        private const string StaffName = "Accounts Admin";
        private const string AcodeLocal = "Is locator cancelled with supplier = N, Has credit been raised to customer account = N , Has credit been raised on Vendor account = N";
        private const string NonAcode = "Is locator cancelled with supplier = N, Has credit been raised to customer account = N , Has credit been raised on Vendor account = N";

        public CancellationTicketService(IEndpointConfiguration cancellationTicketEndpointConfiguration)
        {
            _cancellationTicketEndpointConfiguration = cancellationTicketEndpointConfiguration;
        }
        public async Task<Ticket> GetCancellationResponseAsync(string itineraryId, string emailAddress, int channelId)
        {
            var cancellationTicket = CreateCancellationTicketRequest(itineraryId, emailAddress, channelId);

            //Send Request to Service
            var ticketService = GetClientFromEndpointAddress();

            var result = await ticketService.AddAndAssignTicketToStaffWithMessageAsync(cancellationTicket, StaffName,
                  TicketMessageTypes.Internal, true, AcodeLocal);

            return result;

        }

        private static Ticket CreateCancellationTicketRequest(string itineraryId, string emailAddress, int channelId)
        {
            return new Ticket()
            {
                ItineraryNumber = itineraryId,
                TicketStatus = TicketStatus.AccountsCancellation,
                TicketType = TicketTypes.CancelHotelBooking,
                EmailAddress = emailAddress,
                ChannelId = channelId
            };
        }
        private TicketingAdminServiceClient GetClientFromEndpointAddress()
        {
            var endpoint = this._cancellationTicketEndpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new TicketingAdminServiceClient("*")
                : new TicketingAdminServiceClient("*", endpoint);
            return client;
        }
    }
}
