﻿using System;
using System.Threading.Tasks;
using Alpharooms.Accommodation.B2BXml.TicketService.TicketAdminServiceRef;

namespace Alpharooms.Accommodation.B2BXml.TicketService.Infrastructure
{
    public interface ICancellationTicketService
    {
        Task<Ticket> GetCancellationResponseAsync(string itineraryId, string emailAddress, int channelId);
    }
}