﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using Ploeh.AutoFixture;

namespace Alpharooms.Accommodation.B2BXml.UnitTests.Helper
{
    public class TestDataCreateor
    {
        private Fixture fixture;

        private const string ProviderEdiCode = "B5";
        private const int ProviderId = 5;
        private const string SupplierEdiCode1 = "A";
        private const int SupplierId1 = 1;
        private const string SupplierEdiCode2 = "B";
        private const int SupplierId2 = 2;

        public TestDataCreateor()
        {
            fixture = fixture ?? new Fixture();
        }

        private AccommodationProviderAvailabilityResult GetProviderAvailabilityResult(RateType rateType, PaymentModel model)
        {

            var composer = fixture.Build<AccommodationProviderAvailabilityResult>()
                .With(i => i.RateType, rateType).With(i => i.PaymentModel, model);
            return composer.Create();
        }

        private AccommodationProvider GetProvider(string providerEdiCode, int providerId)
        {
            var composer = fixture.Build<AccommodationProvider>()
                .With(i => i.Id, providerId)
                .With(i => i.EdiCode, providerEdiCode);
            return composer.Create();
        }

        private AccommodationSupplier GetSupplier(string supplierEdiCode, int? supplierId, int providerId)
        {
            var composer = fixture.Build<AccommodationSupplier>()
                .With(i => i.EdiCode, supplierEdiCode)
                .With(i => i.Id, supplierId).With(i => i.ProviderId, providerId);
            return composer.Create();
        }


        public IList<AccommodationAvailabilityResult> GetAvailabilityResults()
        {
            //Add GrossStandard
            var composer =
                fixture.Build<AccommodationAvailabilityResult>()
                    .With(
                        i =>
                            i.ProviderResult,
                        GetProviderAvailabilityResult(RateType.GrossStandard, PaymentModel.PostPayment))
                    .With(i => i.Provider, GetProvider(ProviderEdiCode, ProviderId))
                    .With(i => i.Supplier, GetSupplier(SupplierEdiCode1, SupplierId1, ProviderId));
            var list = composer.CreateMany().ToList();

            //Add GrossPayDirect
            composer =
               fixture.Build<AccommodationAvailabilityResult>()
                   .With(
                       i =>
                           i.ProviderResult,
                       GetProviderAvailabilityResult(RateType.GrossPayDirect, PaymentModel.CustomerPayDirect))
                   .With(i => i.Provider, GetProvider(ProviderEdiCode, ProviderId)).With(i => i.Supplier, GetSupplier(SupplierEdiCode2, SupplierId2, ProviderId));

            list.AddRange(composer.CreateMany());
            return list;
        }

        private AccommodationAgentConfiguration GetAgentConfiguration(int providerId, int? supplierId, string providerEdiCode, string supplierEdiCode)
        {
            var composer =
                fixture.Build<AccommodationAgentConfiguration>()
                    .With(i => i.ProviderId, GetProvider(providerEdiCode, providerId).Id)
                    .With(i => i.SupplierId, GetSupplier(supplierEdiCode, supplierId, providerId).Id)
                    .With(i => i.GrossRatesEnabled, true);
                   // .Without(i => i.Agent);

            return composer.Create();
        }

        private B2BUser GetAgent()
        {
            var agentConfiguration1 = GetAgentConfiguration(ProviderId, SupplierId1, ProviderEdiCode, SupplierEdiCode1); // send back grossrates for A supplier
            var composer = fixture.Build<B2BUser>()
                .With(i => i.AgentConfiguration, new List<AccommodationAgentConfiguration>() { agentConfiguration1 })
                .Without(i => i.Company)
                .Without(i => i.BillToCompany)
                .Without(i => i.Customers);
            return composer.Create();
        }

        public AccommodationAvailabilityRequest GetAvailabilityRequest()
        {
            var composer = fixture.Build<AccommodationAvailabilityRequest>().With(i => i.Agent, GetAgent());
            return composer.Create();
        }

    }
}
