﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Configuration;
using System.Linq;
using Alpharooms.Accommodation.B2BXml.UnitTests.Helper;
using AlphaRooms.Accommodation.B2BXml.Services;
using AlphaRooms.Accommodation.Core.Contracts;
using AlphaRooms.Accommodation.Core.Provider.Contracts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;

namespace Alpharooms.Accommodation.B2BXml.UnitTests
{
    [TestClass]
    public class B2BXmlAgentConfigurationTests
    {
        public static TestDataCreateor DataSeeder;

        [ClassInitialize]
        public static void Start(TestContext context)
        {
            DataSeeder = new TestDataCreateor();
        }
        [TestMethod]
        public void TestAccommodationAgentsConfiguration()
        {

            var availResults = DataSeeder.GetAvailabilityResults();
            var availRequest = DataSeeder.GetAvailabilityRequest();

            
            var b2BAgentConfiguration = new B2BXmlApplyAgentConfigurationService();
            var results = b2BAgentConfiguration.ApplyAgentConfiguration(availRequest, availResults);

            Assert.AreEqual(results.Count,3);
        }


    }
}
