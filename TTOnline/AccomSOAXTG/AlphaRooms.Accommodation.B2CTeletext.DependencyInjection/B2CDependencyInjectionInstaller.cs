﻿using AlphaRooms.Accommodation.B2CTeletext.Caching;
using AlphaRooms.Accommodation.B2CTeletext.DbContexts;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Caching;
using AlphaRooms.Accommodation.B2CTeletext.Interfaces.Repositories;
using AlphaRooms.Accommodation.B2CTeletext.Repositories;
using AlphaRooms.Accommodation.B2CTeletext.Services;
using AlphaRooms.Utilities.EntityFramework;
using log4net.Config;
using Ninject;
using Ninject.Modules;
using System;
using AlphaRooms.Accommodation.B2CTeletext.MarkupServices;
using AlphaRooms.Cache.Interfaces;
using AlphaRooms.Accommodation.Core.Interfaces;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Accommodation.B2CTeletext.Contracts;
using AlphaRooms.Cache.Mongo;
using AlphaRooms.Cache.Mongo.Interfaces;
using System.Configuration;
using AlphaRooms.Configuration;

namespace AlphaRooms.Accommodation.B2CTeletext.DependencyInjection
{
    public class B2CDependencyInjectionInstaller : NinjectModule
    {
        public override void Load()
        {
            
            XmlConfigurator.Configure();
            bool IsNewDepositSchemaEnabled = ConfigurationManager.AppSettings["DepositSchemaVersion"] != null ? string.Compare(ConfigurationManager.AppSettings["DepositSchemaVersion"], "V1", true) == 0 : false;

            // databases
            Bind<IDbContextActivator<B2CLogDbContext>>().To<NoProxyDbContextActivator<B2CLogDbContext>>().InSingletonScope();
            Bind<IDbContextActivator<ContentContext>>().To<NoProxyDbContextActivator<ContentContext>>().InSingletonScope();

            // caching
            var configurationManager = Kernel.Get<IAccommodationConfigurationManager>();
            Bind<ICacheMultiDatabaseController<AccommodationB2CAvailabilityResult, Guid>>().To<B2CAvailabilityResultDatabaseController>().InSingletonScope();
            Bind<IMongoDbMultiDbCollection<AccommodationB2CAvailabilityResult, Guid>>().ToConstant(new MongoDbMultiDbCollection<AccommodationB2CAvailabilityResult, Guid>(
                new MongoDbMultiDbCollectionSettings(configurationManager.AccommodationB2CAvailabilityResultsDatabase
                    , configurationManager.AccommodationB2CAvailabilityResultsCollection
                    , configurationManager.AccommodationB2CAvailabilityResultsMultiDbCount
                    , configurationManager.AccommodationB2CAvailabilityResultsMultiDbIsEnabled)
                    , Kernel.Get<IMongoDbConnection>(), Kernel.Get<ICacheMultiDatabaseController<AccommodationB2CAvailabilityResult, Guid>>())).InSingletonScope();
            Bind<ICacheMultiDbQueryExpireAsync<AccommodationB2CAvailabilityResult, Guid>>().To<MongoDbMultiDbQuery<AccommodationB2CAvailabilityResult, Guid>>().InSingletonScope();
            Bind<ICacheMultiDbWriterExpireAsync<AccommodationB2CAvailabilityResult, Guid>>().To<MongoDbMultiDbWriter<AccommodationB2CAvailabilityResult, Guid>>().InSingletonScope();
            Bind<IB2CAvailabilityResultCaching>().To<B2CAvailabilityResultCaching>().InSingletonScope();
            Bind<IMongoDbCollection<AccommodationB2CRequestStatus>>().ToConstant(new MongoDbCollection<AccommodationB2CRequestStatus>(
                new MongoDbCollectionSettings(configurationManager.AccommodationB2CRequestStatusDatabase
                    , configurationManager.AccommodationB2CRequestStatusCollection), Kernel.Get<IMongoDbConnection>())).InSingletonScope();
            Bind<ICacheQueryExpireAsync<AccommodationB2CRequestStatus>>().To<MongoDbQuery<AccommodationB2CRequestStatus>>().InSingletonScope();
            Bind<ICacheWriterExpireAsync<AccommodationB2CRequestStatus>>().To<MongoDbWriter<AccommodationB2CRequestStatus>>().InSingletonScope();
            Bind<IB2CRequestStatusCaching>().To<B2CRequestStatusCaching>().InSingletonScope();

            // repositories
            Bind<IB2CAvailabilityLogRepository>().To<B2CAvailabilityLogRepository>().InSingletonScope();
            Bind<IB2CValuationLogRepository>().To<B2CValuationLogRepository>().InSingletonScope();
            Bind<IB2CBookingLogRepository>().To<B2CBookingLogRepository>().InSingletonScope();
            Bind<IAutoSuggestRepository>().To<AutoSuggestRepository>().InSingletonScope();

            // shared services
            Bind<IB2CPaymentModelConverter>().To<B2CPaymentModelConverter>().InSingletonScope();
            Bind<IB2CRequestStatusService>().To<B2CRequestStatusService>().InSingletonScope();
            Bind<IAccommodationBusiness>().To<B2CAccommodationBusiness>().InSingletonScope();
            Bind<IB2CItineraryBookingService>().To<B2CItineraryBookingService>().InSingletonScope();
            Bind<IB2CItineraryBookingFactory>().To<B2CItineraryBookingFactory>().InSingletonScope();
            Bind<IB2CItineraryBookingMapping>().To<B2CItineraryBookingMapping>().InSingletonScope();
            Bind<IDestinationService>().To<B2CDestinationService>().InSingletonScope();
            Bind<IB2COnlineBookabilityService>().To<OnlineBookabilityService>().InSingletonScope();
            Bind<IEndpointConfiguration>().To<EndpointConfiguration>().WhenInjectedInto<B2CItineraryBookingService>().InSingletonScope().WithConstructorArgument("service", "BookingService");

            // markup service
            if (Kernel.Get<IAccommodationConfigurationManager>().AccommodationB2BWebUseHmdForMarkup)
            {
                Bind<IAvailabilityResultMarkupService>().To<HmdService>().InSingletonScope();
                Bind<IValuationResultMarkupService>().To<HmdService>().InSingletonScope();
            }
            else
            {
                Bind<IAvailabilityResultMarkupService>().To<B2CMarkupService>().InSingletonScope();
                Bind<IValuationResultMarkupService>().To<B2CMarkupService>().InSingletonScope();
            }

            // availability services
            Bind<IB2CAvailabilityKeyGenerator>().To<B2CAvailabilityKeyGenerator>().InSingletonScope();
            Bind<IB2CAvailabilityLogger>().To<B2CAvailabilityLogger>().InSingletonScope();
            Bind<IB2CAvailabilityRequestFactory>().To<B2CAvailabilityRequestFactory>().InSingletonScope();
            Bind<IB2CAvailabilityRequestValidator>().To<B2CAvailabilityRequestValidator>().InSingletonScope();
            Bind<IB2CAvailabilityResultFilterOptionsService>().To<B2CAvailabilityResultFilterOptionsService>().InSingletonScope();
            Bind<IB2CAvailabilityResultMappingService>().To<B2CAvailabilityResultMappingService>().InSingletonScope();
            Bind<IB2CAvailabilityService>().To<B2CAvailabilityService>().InSingletonScope();
            Bind<IB2CAvailabilityProviderIdentifierService>().To<B2CAvailabilityProviderIdentifierService>().InSingletonScope();
            Bind<IB2CAvailabilityMapResultMappingService>().To<B2CAvailabilityMapResultMappingService>().InSingletonScope();
            Bind<IB2CAvailabilityFilterCriteriaFactory>().To<B2CAvailabilityFilterCriteriaFactory>();
            Bind<IAvailabilityResultConsolidationService>().To<B2CAvailabilityConsolidationService>();
            Bind<IAvailabilityResultLowerPriceService>().To<B2CAvailabilityResultLowerPriceService>().InSingletonScope();
            Bind<IAvailabilityFlashSaleService>().To<B2CAvailabilityFlashSaleService>().InSingletonScope();

            if (!IsNewDepositSchemaEnabled){
                Bind<IB2CAvailabilityDepositService>().To<B2CAvailabilityDepositService>().InSingletonScope();
            }else{
                Bind<IB2CAvailabilityDepositService>().To<B2CAvailabilityDepositServiceV1>().InSingletonScope();
            }
           

            // valuation services
            Bind<IB2CValuationService>().To<B2CValuationService>().InSingletonScope();
            Bind<IB2CValuationLogger>().To<B2CValuationLogger>().InSingletonScope();
            Bind<IB2CValuationRequestFactory>().To<B2CValuationRequestFactory>().InSingletonScope();
            Bind<IB2CValuationRequestValidator>().To<B2CValuationRequestValidator>().InSingletonScope();
            Bind<IB2CValuationResultMappingService>().To<B2CValuationResultMappingService>().InSingletonScope();

            // booking services
            Bind<IB2CBookingService>().To<B2CBookingService>().InSingletonScope();
            Bind<IB2CBookingLogger>().To<B2CBookingLogger>().InSingletonScope();
            Bind<IB2CBookingRequestFactory>().To<B2CBookingRequestFactory>().InSingletonScope();
            Bind<IB2CBookingRequestValidator>().To<B2CBookingRequestValidator>().InSingletonScope();
            Bind<IB2CBookingResultMappingService>().To<B2CBookingResultMappingService>().InSingletonScope();

            // cancellation services
            Bind<IB2CCancellationService>().To<B2CCancellationService>().InSingletonScope();
            Bind<IB2CCancellationLogger>().To<B2CCancellationLogger>().InSingletonScope();
            Bind<IB2CCancellationRequestFactory>().To<B2CCancellationRequestFactory>().InSingletonScope();
            Bind<IB2CCancellationRequestValidator>().To<B2CCancellationRequestValidator>().InSingletonScope();
            Bind<IB2CCancellationResultMappingService>().To<B2CCancellationResultMappingService>().InSingletonScope();

            //guidGeneration
            Bind<IB2CGuidService>().To<B2CGuidService>().InSingletonScope();
        }
    }
}
