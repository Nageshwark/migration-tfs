﻿using Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure;
using System.Threading.Tasks;

namespace Alpharooms.Accommodation.B2BXml.AuthenticationService.Infrastructure
{
    public interface IAuthenticateUserService
    {
        Task<IAuthenticateUserResponse> XmlAuthenticateAgentAsync(string username, string password, string agentIdentifier);

        Task<IAuthenticateUserResponse> AuthenticateAgentAsync(string username, string password);
    }
}