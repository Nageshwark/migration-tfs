﻿namespace Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure
{
    public interface IAuthenticateUserResponse
    {
        int? AuthenticatedUserId { get; set; }

        bool IsAuthenticated { get; set; }
    }
}
