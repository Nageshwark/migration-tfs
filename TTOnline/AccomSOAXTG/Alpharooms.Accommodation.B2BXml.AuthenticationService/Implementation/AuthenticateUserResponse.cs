﻿using Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure;
namespace Alpharooms.Accommodation.B2BXml.AuthenticationService.Implementation
{
    public class AuthenticateUserResponse : IAuthenticateUserResponse
    {
        public int? AuthenticatedUserId
        {
            get;set;
        }

        public bool IsAuthenticated
        {
            get;set;
        }
    }
}
