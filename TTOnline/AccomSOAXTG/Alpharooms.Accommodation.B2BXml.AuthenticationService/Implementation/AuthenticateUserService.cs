﻿using System;
using System.Threading.Tasks;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.AuthenticationServiceRef;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.Infrastructure;
using Alpharooms.Accommodation.B2BXml.AuthenticationService.infrastructure;
using AlphaRooms.SOACommon.Interfaces;

namespace Alpharooms.Accommodation.B2BXml.AuthenticationService.Implementation
{
    public class AuthenticateUserService : IAuthenticateUserService
    {
        private IEndpointConfiguration _endpointConfiguration { get; }

        public AuthenticateUserService(IEndpointConfiguration endpointConfiguration)
        {
            this._endpointConfiguration = endpointConfiguration;
        }

        public async Task<IAuthenticateUserResponse> XmlAuthenticateAgentAsync(string username, string password, string agentIdentifier)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(agentIdentifier))
            {
                throw new ArgumentNullException("Username/Password/Ident", "Username, password and agent identifier cannot be null");
            }

            var authenticateRequest = new XmlAuthenticateRequest();
            authenticateRequest.UserName = username;
            authenticateRequest.Password = password;
            authenticateRequest.UserIdent = agentIdentifier;

            var authenticationClient = GetClientFromEndpointAddress();
            var authenticationResponse = await authenticationClient.XmlAuthenticateAgentAsync(authenticateRequest);

            return new AuthenticateUserResponse { AuthenticatedUserId = authenticationResponse.B2BUser?.Id, IsAuthenticated = authenticationResponse.IsAuthenticated };
        }

        /// <summary>
        /// Added for backward compatibility
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<IAuthenticateUserResponse> AuthenticateAgentAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("Username/Password", "Username or password cannot be null");
            }
            var authenticateRequest = new AuthenticateRequest();
            authenticateRequest.UserName = username;
            authenticateRequest.Password = password;
          

            var authenticationClient = GetClientFromEndpointAddress();
            var authenticationResponse = await authenticationClient.AuthenticateAsync(authenticateRequest);

            return new AuthenticateUserResponse { AuthenticatedUserId = authenticationResponse.B2BUser?.Id, IsAuthenticated = authenticationResponse.IsAuthenticated };

        }

        private AuthenticationServiceClient GetClientFromEndpointAddress()
        {
            var endpoint = this._endpointConfiguration.EndpointAddress;
            var client = string.IsNullOrEmpty(endpoint)
                ? new AuthenticationServiceClient("*")
                : new AuthenticationServiceClient("*", endpoint);
            return client;
        }

    }
}
