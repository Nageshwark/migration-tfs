﻿using AlphaRooms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderBookingRequestRoomGuestCollection : IEnumerable<AccommodationProviderBookingRequestRoomGuest>
    {
        private readonly IList<AccommodationProviderBookingRequestRoomGuest> guests;
        private readonly byte adultsCount;
        private readonly byte childrenCount;
        private readonly byte infantsCount;
        private readonly byte childrenAndInfantsCount;
        private readonly byte[] childAndInfantAges;

        public AccommodationProviderBookingRequestRoomGuestCollection(IList<AccommodationProviderBookingRequestRoomGuest> guests)
        {
            this.guests = guests;
            this.adultsCount = this.guests.CountByte(i => i.Type == GuestType.Adult); 
            this.childrenCount = this.guests.CountByte(i => i.Type == GuestType.Child);
            this.infantsCount = this.guests.CountByte(i => i.Type == GuestType.Infant);
            this.childrenAndInfantsCount = this.guests.CountByte(i => i.Type == GuestType.Child || i.Type == GuestType.Infant);
            this.childAndInfantAges = this.guests.Where(i => i.Type == GuestType.Child || i.Type == GuestType.Infant).Select(i => i.Age).ToArray();
        }

        public IEnumerator<AccommodationProviderBookingRequestRoomGuest> GetEnumerator()
        {
            return (IEnumerator<AccommodationProviderBookingRequestRoomGuest>)this.guests.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.guests.GetEnumerator();
        }

        public AccommodationProviderBookingRequestRoomGuest this[int value] { get { return this.guests[value]; } }
        public byte AdultsCount { get { return this.adultsCount; } }
        public byte ChildrenCount { get { return this.childrenCount; } }
        public byte InfantsCount { get { return this.infantsCount; } }
        public byte ChildrenAndInfantsCount { get { return this.childrenAndInfantsCount; } }
        public byte[] ChildAndInfantAges { get { return this.childAndInfantAges; } }
    }
}
