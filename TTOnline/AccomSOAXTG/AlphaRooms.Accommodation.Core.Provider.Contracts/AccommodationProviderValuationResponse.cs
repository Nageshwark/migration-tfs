﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderValuationResponse
    {
        public IEnumerable<AccommodationProviderValuationResult> Results { get; set; }
    }
}
