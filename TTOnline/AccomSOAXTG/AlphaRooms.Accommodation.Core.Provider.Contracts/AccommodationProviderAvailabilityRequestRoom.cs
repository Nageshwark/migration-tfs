﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderAvailabilityRequestRoom
    {
        public const string RoomNumberFieldName = "RmN";
        public const string AvailabilityRequestIdFieldName = "ARI";
        public const string GuestsFieldName = "Gts";

        [BsonElement(RoomNumberFieldName)]
        public byte RoomNumber { get; set; }

        [BsonElement(AvailabilityRequestIdFieldName)]
        public Guid AvailabilityRequestId { get; set; }

        [BsonElement(GuestsFieldName)]
        public AccommodationProviderAvailabilityRequestGuestCollection Guests { get; set; }
    }
}
