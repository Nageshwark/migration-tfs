﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderAvailabilityResultProviderFilterItem
    {
        public bool IsProviderAndSupplierFilter { get; set; }

        public string ProviderFilterDisplay { get; set; }
    }
}
