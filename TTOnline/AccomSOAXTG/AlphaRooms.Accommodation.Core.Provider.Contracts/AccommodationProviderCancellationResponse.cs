﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderCancellationResponse
    {
        public IEnumerable<AccommodationProviderCancellationResult> Results { get; set; }
    }
}
