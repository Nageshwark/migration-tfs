﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderCancellationResult
    {
        public byte RoomNumber { get; set; }
        public CancellationStatus CancellationStatus { get; set; } 
        public string ProviderCancellationReference { get; set; }
        public string Message { get; set; } 
        public Money CancellationCost { get ; set; }
    }
}
