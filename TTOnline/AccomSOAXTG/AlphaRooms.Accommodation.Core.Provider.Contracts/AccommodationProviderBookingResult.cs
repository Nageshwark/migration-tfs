﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderBookingResult
    {
        public BookingStatus BookingStatus { get; set; }
        public string ProviderBookingReference { get; set; }
        public string Message { get; set; }
        public byte RoomNumber { get; set; }
        public Dictionary<string,string> ProviderSpecificData { get; set; }//Add Key Collection or other details in here
        public string KeyCollectionInformation { get; set; }
    }
}
