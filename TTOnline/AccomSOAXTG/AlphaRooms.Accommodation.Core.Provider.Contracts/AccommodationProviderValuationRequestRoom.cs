﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderValuationRequestRoom
    {
        public AccommodationProviderAvailabilityRequest AvailabilityRequest { get; set; }
        public byte RoomNumber { get; set; }
        public AccommodationProviderAvailabilityRequestGuestCollection Guests { get; set; }
        public AccommodationProviderAvailabilityResult AvailabilityResult { get; set; }
    }
}