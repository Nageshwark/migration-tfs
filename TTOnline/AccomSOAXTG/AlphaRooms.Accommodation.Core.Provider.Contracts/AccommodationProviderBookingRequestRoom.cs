﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderBookingRequestRoom
    {
        public AccommodationProviderAvailabilityRequest AvailabilityRequest { get; set; }
        public AccommodationProviderValuationResult ValuationResult { get; set; }
        public AccommodationProviderBookingRequestRoomGuestCollection Guests { get; set; }
        public AccommodationProviderBookingRequestRoomPaymentDetails PaymentDetails { get; set; }
        public AccommodationProviderBookingRequestRoomSpecialRequest SpecialRequests { get; set; }
        public byte RoomNumber { get; set; }
    }
}
