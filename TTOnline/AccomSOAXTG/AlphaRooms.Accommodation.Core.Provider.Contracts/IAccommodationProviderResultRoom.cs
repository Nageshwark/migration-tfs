﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public interface IAccommodationProviderResultRoom
    {
        DateTime CheckInDate { get; set; }

        DateTime CheckOutDate { get; set; }

        Money SalePrice { get; set; }

        Money CostPrice { get; set; }

        PaymentModel PaymentModel { get; set; }

        RateType RateType { get; set; }

        bool IsOpaqueRate { get; set; }

        bool IsNonRefundable { get; set; }

        bool IsBindingRate { get; set; }
    }
}
