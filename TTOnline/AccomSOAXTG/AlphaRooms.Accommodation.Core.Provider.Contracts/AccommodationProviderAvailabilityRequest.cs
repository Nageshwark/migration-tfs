﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderAvailabilityRequest
    {
        public const string AvailabilityIdFieldName = "AvI";
        public const string ChannelFieldName = "Chn";
        public const string ProcessIdFieldName = "PrI";
        public const string ChannelInfoFieldName = "CnI";
        public const string CheckInDateFieldName = "CIn";
        public const string CheckOutDateFieldName = "COt";
        public const string DestinationCodesFieldName = "DsC";
        public const string EstablishmentCodesFieldName = "EbC";
        public const string RoomsFieldName = "Rms";
        public const string DebuggingFieldName = "Dbg";
        public const string SearchTypeFieldName = "ScT";
        public const string MetaTokenFieldName = "Met";

        [BsonElement(AvailabilityIdFieldName)]
        public Guid AvailabilityId { get; set; }

        [BsonElement(ProcessIdFieldName)]
        public byte ProcessId { get; set; }

        /// <summary>
        /// Provider Domain Model. Provider contain AccommodationProviderParameters where all the parameters/settings required for the 
        /// Provider API call would be available. This would be preloaded from database when the Provider Domain model is created
        /// </summary>
        [BsonIgnore]
        public AccommodationProvider Provider { get; set; }

        public B2BUser Agent { get; set; }

        [BsonElement(ChannelFieldName)]
        public Channel Channel { get; set; }

        [BsonIgnore]
        public ChannelInfo ChannelInfo { get; set; }

        [BsonElement(CheckInDateFieldName)]
        public DateTime CheckInDate { get; set; }

        [BsonElement(MetaTokenFieldName)]
        public string MetaToken { get; set; }

        [BsonElement(CheckOutDateFieldName)]
        public DateTime CheckOutDate { get; set; }
        /// <summary>
        /// Provider DestinationCodes
        /// </summary>
        [BsonElement(DestinationCodesFieldName)]
        public string[] DestinationCodes { get; set; }

        /// <summary>
        /// Provider EstablishmentCodes
        /// </summary>
        [BsonElement(EstablishmentCodesFieldName)]
        public string[] EstablishmentCodes { get; set; }

        [BsonElement(RoomsFieldName)]
        public AccommodationProviderAvailabilityRequestRoom[] Rooms { get; set; }

        [BsonElement(DebuggingFieldName)]
        public bool Debugging { get; set; }
        /// <summary>
        /// Specifies if its a Hotel Only Search or its a Flight&Hotel Search. Would be required to search for Package rates
        /// if available with the accommodation provider
        /// </summary>

        [BsonElement(SearchTypeFieldName)]
        public SearchType SearchType { get; set; }
    }
}
