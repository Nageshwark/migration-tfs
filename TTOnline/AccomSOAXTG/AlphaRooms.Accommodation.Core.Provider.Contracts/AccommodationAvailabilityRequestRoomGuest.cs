﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationAvailabilityRequestRoomGuests
    {
        public List<byte> Age { get; set; }
        public byte RoomNumber { get; set; }

    }
}
