﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderValuationResult : IAccommodationProviderResultRoom
    {
        public const string RoomNumberFieldName = "Rmn";
        public const string ProviderEdiCodeFieldName = "PCd";
        public const string ProviderProviderCodeFieldName = "PPC";        
        public const string ProviderNameFieldName = "PNm";
        public const string ProviderAliasNameFieldName = "PANm";
        public const string SupplierEdiCodeFieldName = "SCd";
        public const string EstablishmentEdiCodeFieldName = "ECd";
        public const string DestinationEdiCodeFieldName = "DCd";
        public const string EstablishmentNameFieldName = "ENm";
        public const string ProviderEstablishmentCodeFieldName = "PEC";
        public const string AdultsFieldName = "Adt";
        public const string ChildrenFieldName = "Chd";
        public const string InfantsFieldName = "Inf";
        public const string CheckInDateFieldName = "CIn";
        public const string CheckOutDateFieldName = "COt";
        public const string BoardCodeFieldName = "BCd";
        public const string BoardDescriptionFieldName = "BDs";
        public const string RoomCodeFieldName = "RCd";
        public const string RoomDescriptionFieldName = "RDs";
        public const string SalePriceFieldName = "SPr";
        public const string CostPriceFieldName = "CPr";
        public const string CommissionAmountFieldName = "CAm";
        public const string CommissionPercentFieldName = "CPc";
        public const string PaymentModelFieldName = "PMd";
        public const string RateTypeFieldName = "RtT";
        public const string IsOpaqueRateFieldName = "IOp";
        public const string IsSpecialRateFieldName = "ISp";
        public const string IsNonRefundableFieldName = "INR";
        public const string IsBindingRateFieldName = "IBR";
        public const string ProviderSpecificDataFieldName = "PSp";
        public const string NumberOfAvailableRoomsFieldName = "NAv";
        public const string ContractFieldName = "Ctt";
        public const string CancellationPolicyFieldName = "CPl";
        public const string ProviderPaymentOptionsFieldName = "PPO";
        public const string VirtualCreditCardPaymentTermsFieldName = "VCC";

        [BsonElement(RoomNumberFieldName)]
        public byte RoomNumber { get; set; }

        [BsonElement(ProviderEdiCodeFieldName)]
        public string ProviderEdiCode { get; set; }
        
        [BsonElement(ProviderProviderCodeFieldName)]
        public string ProviderProviderCode { get; set; }

        [BsonElement(ProviderNameFieldName)]
        public string ProviderName { get; set; }

        [BsonElement(ProviderAliasNameFieldName)]
        public string ProviderAliasName { get; set; }

        [BsonElement(SupplierEdiCodeFieldName)]
        public string SupplierEdiCode { get; set; }

        [BsonElement(DestinationEdiCodeFieldName)]
        public string DestinationEdiCode { get; set; }

        [BsonElement(EstablishmentEdiCodeFieldName)]
        public string EstablishmentEdiCode { get; set; }

        [BsonElement(EstablishmentNameFieldName)]
        public string EstablishmentName { get; set; }

        [BsonElement(ProviderEstablishmentCodeFieldName)]
        public string ProviderEstablishmentCode { get; set; }

        [BsonElement(AdultsFieldName)]
        public byte Adults { get; set; }

        [BsonElement(ChildrenFieldName)]
        public byte Children { get; set; }

        [BsonElement(InfantsFieldName)]
        public byte Infants { get; set; }

        [BsonElement(CheckInDateFieldName)]
        public DateTime CheckInDate { get; set; }

        [BsonElement(CheckOutDateFieldName)]
        public DateTime CheckOutDate { get; set; }

        [BsonElement(BoardCodeFieldName)]
        public string BoardCode { get; set; }

        [BsonElement(BoardDescriptionFieldName)]
        public string BoardDescription { get; set; }

        [BsonElement(RoomCodeFieldName)]
        public string RoomCode { get; set; }

        [BsonElement(RoomDescriptionFieldName)]
        public string RoomDescription { get; set; }

        /// <summary>
        /// Set if Gross price is returned
        /// </summary>
        [BsonElement(SalePriceFieldName)]
        public Money SalePrice { get; set; }

        [BsonElement(CostPriceFieldName)]
        public Money CostPrice { get; set; }

        /// <summary>
        /// Set CommissionAmount and CommissionPercent if Gross Price is returned and the margin is returned in the response
        /// </summary>
        [BsonElement(CommissionAmountFieldName)]
        public Money CommissionAmount { get; set; }

        [BsonElement(CommissionPercentFieldName)]
        public decimal CommissionPercent { get; set; }

        [BsonElement(PaymentModelFieldName)]
        public PaymentModel PaymentModel { get; set; }

        [BsonElement(RateTypeFieldName)]
        public RateType RateType { get; set; }

        [BsonElement(IsOpaqueRateFieldName)]
        public bool IsOpaqueRate { get; set; }

        [BsonElement(IsSpecialRateFieldName)]
        public bool IsSpecialRate { get; set; }

        [BsonElement(IsNonRefundableFieldName)]
        public bool IsNonRefundable { get; set; }

        [BsonElement(IsBindingRateFieldName)]
        public bool IsBindingRate { get; set; }
        
        [BsonElement(ProviderSpecificDataFieldName)]
        public Dictionary<string, string> ProviderSpecificData { get; set; } //Replace Incoming Contract
        
        /// <summary>
        /// Set if returned from the provider or if 'X' rooms are requested and
        /// number of rooms returned is less than X. So if 3 (similar) rooms are requested and if the 
        /// provider only returns 2 set NumberOfAvailableRooms =2. This again varies with different providers 
        /// </summary>
        [BsonElement(NumberOfAvailableRoomsFieldName)]
        public int? NumberOfAvailableRooms { get; set; }
        
        [BsonElement(ContractFieldName)]
        public string Contract { get; set; }

        [BsonElement(CancellationPolicyFieldName)]
        public string[] CancellationPolicy { get; set; } //Set if returned from the Provider
        
        [BsonElement(ProviderPaymentOptionsFieldName)]
        public List<AccommodationProviderPaymentOption> ProviderPaymentOptions { get; set; }

        [BsonElement(VirtualCreditCardPaymentTermsFieldName)]
        public AccommodationProviderValuationResultVccPaymentRule[] VirtualCreditCardPaymentTerms { get; set; }
    }
}
