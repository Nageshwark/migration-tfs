﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderBookingRequestRoomGuest
    {
        public int Id { get; set; }
        public GuestTitle Title { get; set; }
        public string TitleString { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public GuestType Type { get; set; }
    }
}
