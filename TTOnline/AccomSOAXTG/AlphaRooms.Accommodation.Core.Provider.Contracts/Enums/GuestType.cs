﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public enum GuestType
    {
        Adult = 0, 
        Child = 1,
        Infant = 2
    }
}
