﻿using AlphaRooms.Accommodation.Core.Provider.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderRequest
    {
        private readonly ProviderRequestType type;
        private readonly object obj;
        private readonly string key;

        public AccommodationProviderRequest(ProviderRequestType type, string key, object obj)
        {
            this.type = type;
            this.key = key;
            this.obj = obj;
        }

        public ProviderRequestType Type { get { return this.type; } }
        public string Key { get { return this.key; } }
        public object Object { get { return this.obj; } }
    }
}
