﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderPaymentOption
    {
        public const string CardTypeFieldName = "CTp";
        public const string CardTypeTextFieldName = "CTT";
        public const string CostFieldName = "Cst";

        [BsonElement(CardTypeFieldName)]
        public CardType CardType { get; set; }

        [BsonElement(CardTypeTextFieldName)]
        public string CardTypeText { get; set; }

        [BsonElement(CostFieldName)]
        public Money Cost { get; set; }
    }
}
