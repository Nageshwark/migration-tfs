﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderValuationRequest
    {
        public Guid AvailabilityId { get; set; }
        public Guid ValuationId { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public B2BUser Agent { get; set; }
        public AccommodationProvider Provider { get; set; }
        public AccommodationProviderValuationRequestRoom[] SelectedRooms { get; set; }
        public bool Debugging { get; set; }
        public byte ProcessId { get; set; }
    }
}
