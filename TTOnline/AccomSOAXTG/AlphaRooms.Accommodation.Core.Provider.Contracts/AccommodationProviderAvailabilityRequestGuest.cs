﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderAvailabilityRequestGuest
    {
        public const string TypeFieldName = "Tpe";
        public const string AgeFieldName = "Age";

        [BsonElement(TypeFieldName)]
        public GuestType Type { get; set; }

        [BsonElement(AgeFieldName)]
        public byte Age { get; set; }
    }
}
