﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using MongoDB.Bson.Serialization.Attributes;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    [BsonIgnoreExtraElements]
    public class AccommodationProviderValuationResultVccPaymentRule
    {
        public const string PaymentDateFieldName = "PDt";
        public const string PriceFieldName = "Prc";
        public const string RoomCodeFieldName = "RCd";

        [BsonElement(PaymentDateFieldName)]
        public DateTime PaymentDate { get; set; }

        [BsonElement(PriceFieldName)]
        public Money Price { get; set; }

        [BsonElement(RoomCodeFieldName)]
        public string RoomCode { get; set; }
    }
}
