﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderCancellationRequest
    {
        public Guid CancellationId { get; set; }
        public int ItineraryId { get; set; }
        public B2BUser Agent { get; set; }
        public AccommodationProvider Provider { get; set; }
        public string ProviderBookingReference { get; set; }
        public Dictionary<string,string> ProviderSpecificData { get; set; }
        public bool Debugging { get; set; }
        public ChannelInfo ChannelInfo { get; set; }

        //Below fields are required for Pegasus
        public DateTime CheckInDate { get; set; } 
        public DateTime CheckOutDate { get; set; }
        public string EstablishmentEdiCode { get; set; } 
        public string CustomerFirstName { get; set; }
        public string CustomerSurName { get; set; }
    }    
}
