﻿
namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// All the properties here are to be set from the Provider response and the provider request
    /// </summary>
    [BsonIgnoreExtraElements]
    public class AccommodationProviderAvailabilityResult : IAccommodationProviderResultRoom, ICloneable
    {
        private const string RoomNumberFieldName = "Rmn";
        private const string ProviderEdiCodeFieldName = "PCd";
        private const string ProviderNameFieldName = "PNm";
        private const string ProviderAliasNameFieldName = "PANm";
        private const string ProviderProviderCodeFieldName = "PPC";
        private const string ProviderFilterItemFieldName = "PFI";
        private const string SupplierEdiCodeFieldName = "SCd";
        private const string ProviderFilterNameFieldName = "SFN";
        private const string EstablishmentEdiCodeFieldName = "ECd";
        private const string DestinationEdiCodeFieldName = "DCd";
        private const string EstablishmentNameFieldName = "ENm";
        private const string ProviderEstablishmentCodeFieldName = "PEC";
        private const string AdultsFieldName = "Adt";
        private const string ChildrenFieldName = "Chd";
        private const string InfantsFieldName = "Inf";
        private const string CheckInDateFieldName = "CIn";
        private const string CheckOutDateFieldName = "COt";
        private const string BoardCodeFieldName = "BCd";
        private const string BoardDescriptionFieldName = "BDs";
        private const string RoomCodeFieldName = "RCd";
        private const string RoomDescriptionFieldName = "RDs";
        private const string SalePriceFieldName = "SPr";
        private const string CostPriceFieldName = "CPr";
        private const string CommissionAmountFieldName = "CAm";
        private const string CommissionPercentFieldName = "CPc";
        private const string PaymentModelFieldName = "PMd";
        private const string RateTypeFieldName = "RtT";
        private const string IsOpaqueRateFieldName = "IOp";
        private const string IsSpecialRateFieldName = "ISp";
        private const string IsNonRefundableFieldName = "INR";
        private const string IsBindingRateFieldName = "IBR";
        private const string ContractFieldName = "Ctt";
        private const string ProviderSpecificDataFieldName = "PSp";
        private const string NumberOfAvailableRoomsFieldName = "NAv";

        /// <summary>
        /// A unique Guid for that room. This would be mainly for caching purpose
        /// </summary>
        [BsonElement(RoomNumberFieldName)]
        public byte RoomNumber { get; set; }

        [BsonElement(ProviderEdiCodeFieldName)]
        public string ProviderEdiCode { get; set; }

        [BsonElement(ProviderNameFieldName)]
        public string ProviderName { get; set; }

        [BsonElement(ProviderProviderCodeFieldName)]
        public string ProviderProviderCode { get; set; }

        [BsonElement(ProviderAliasNameFieldName)]
        public string ProviderAliasName { get; set; }
        
        [BsonElement(SupplierEdiCodeFieldName)]
        public string SupplierEdiCode { get; set; }

        [BsonElement(ProviderFilterNameFieldName)]
        public string ProviderFilterName { get; set; }

        [BsonElement(DestinationEdiCodeFieldName)]
        public string DestinationEdiCode { get; set; }

        [BsonElement(EstablishmentEdiCodeFieldName)]
        public string EstablishmentEdiCode { get; set; }

        [BsonElement(EstablishmentNameFieldName)]
        public string EstablishmentName { get; set; }

        [BsonElement(ProviderEstablishmentCodeFieldName)]
        public string ProviderEstablishmentCode { get; set; }

        [BsonElement(AdultsFieldName)]
        public byte Adults { get; set; }

        [BsonElement(ChildrenFieldName)]
        public byte Children { get; set; }

        [BsonElement(InfantsFieldName)]
        public byte Infants { get; set; }

        [BsonElement(CheckInDateFieldName)]
        public DateTime CheckInDate { get; set; }

        [BsonElement(CheckOutDateFieldName)]
        public DateTime CheckOutDate { get; set; }

        [BsonElement(BoardCodeFieldName)]
        public string BoardCode { get; set; }

        [BsonElement(BoardDescriptionFieldName)]
        public string BoardDescription { get; set; }

        [BsonElement(RoomCodeFieldName)]
        public string RoomCode { get; set; }

        [BsonElement(RoomDescriptionFieldName)]
        public string RoomDescription { get; set; }

        /// <summary>
        /// Set if Gross price is returned
        /// </summary>
        [BsonElement(SalePriceFieldName)]
        public Money SalePrice { get; set; }

        [BsonElement(CostPriceFieldName)]
        public Money CostPrice { get; set; }

        /// <summary>
        /// Set CommissionAmount if Gross Price is returned and the margin is returned in the response
        /// </summary>
        [BsonElement(CommissionAmountFieldName)]
        public Money CommissionAmount { get; set; }

        [BsonElement(CommissionPercentFieldName)]
        public decimal CommissionPercent { get; set; }

        [BsonElement(PaymentModelFieldName)]
        public PaymentModel PaymentModel { get; set; }

        [BsonElement(RateTypeFieldName)]
        public RateType RateType { get; set; }

        [BsonElement(IsOpaqueRateFieldName)]
        public bool IsOpaqueRate { get; set; }

        [BsonElement(IsNonRefundableFieldName)]
        public bool IsNonRefundable { get; set; }

        [BsonElement(IsBindingRateFieldName)]
        public bool IsBindingRate { get; set; }

        [BsonElement(ContractFieldName)]
        public string Contract { get; set; }

        [BsonElement(ProviderSpecificDataFieldName)]
  
        public Dictionary<string, string> ProviderSpecificData { get; set; }

        public List<AccommodationAvailabilityRequestRoomGuests> RoomGuests { get; set; }

        /// <summary>
        /// Set if returned from the provider or if 'X' rooms are requested and
        /// number of rooms returned is less than X. So if 3 (similar) rooms are requested and if the 
        /// provider only returns 2 set NumberOfAvailableRooms =2. This again varies with different providers 
        /// </summary>
        [BsonElement(NumberOfAvailableRoomsFieldName)]
        public int? NumberOfAvailableRooms { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
