﻿using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Contracts
{
    public class AccommodationProviderBookingRequest
    {
        public Guid AvailabilityId { get; set; }
        public Guid ValuationId { get; set; }
        public Guid BookingId { get; set; }
        public byte ProcessId { get; set; }
        public ChannelInfo ChannelInfo { get; set; }
        public B2BUser Agent { get; set; }
        public AccommodationProvider Provider { get; set; }
        public int ItineraryId { get; set; }
        public string ItineraryReference { get; set; }
        public AccommodationProviderBookingRequestCustomer Customer { get; set; }
        public AccommodationProviderBookingRequestRoom[] ValuatedRooms { get; set; }
        public bool Debugging { get; set; }
    }
}