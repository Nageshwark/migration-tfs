﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class DestinationRepository : IDestinationRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<Destination> cache;

        public DestinationRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<Destination> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("DST", async() => (await this.accommodationDbContextActivator.GetDbContext().GetDestinationsAsync()).ToArray());
        }

        public async Task<Destination> GetByDestinationIdAsync(Guid destinationId)
        {
            return (await this.cache.GetAllAsync()).First(i => i.DestinationId == destinationId);
        }

        public async Task<Destination> GetByEstablishmentIdAsync(Guid establishmentId)
        {
            return (await this.cache.GetAllAsync()).First(i => i.EstablishmentId == establishmentId);
        }
        public async Task<Destination> GetByEstablishmentIntIdAsync(int EstablishmentIntId)
        {
            return (await this.cache.GetAllAsync()).First(i => i.EstablishmentIntID == EstablishmentIntId);
        }
    }
}
