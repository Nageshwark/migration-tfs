﻿using System;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using System.Data.Entity;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class SupplierAliasRepository : ISupplierAliasRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<SupplierAlias> cache;

        public SupplierAliasRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<SupplierAlias> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("SAr", async () => await this.accommodationDbContextActivator.GetDbContext().SupplierAlias.ToArrayAsync());
        }

        public async Task<Dictionary<int, SupplierAlias>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("SAd", async () => (await this.cache.GetAllAsync()).ToDictionary(i => i.Id, i => i));
        }

        public async Task<SupplierAlias[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }

        public async Task<SupplierAlias[]> GetByIdsAsync(int[] ids)
        {
            return await this.cache.GetByKeysAsync(ids.Select(i => i.ToString()).ToArray());
        }

        public async Task<SupplierAlias> GetByIdAsync(int providerId)
        {
            return await this.cache.GetByKeyAsync(providerId.ToString());
        }
    }
}
