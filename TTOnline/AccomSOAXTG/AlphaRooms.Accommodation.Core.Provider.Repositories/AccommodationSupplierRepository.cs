﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Utilities;
using System.Data.Entity;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class AccommodationSupplierRepository : IAccommodationSupplierRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<AccommodationSupplier> cache;

        public AccommodationSupplierRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<AccommodationSupplier> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("ASr", async() => await this.accommodationDbContextActivator.GetDbContext().AccommodationSuppliers.ToArrayAsync()
                , async (i) => { int id = int.Parse(i); return await this.accommodationDbContextActivator.GetDbContext().AccommodationSuppliers.FirstAsync(j => j.Id == id); }
                , i => i.Id.ToString());
        }

        public async Task<Dictionary<int, AccommodationSupplier>> GetAllDictionaryGroupByIdAsync()
        {
            return await this.cache.GetOrSetAsync("Avd", async() => (await this.cache.GetAllAsync()).ToDictionary(i => i.Id, i => i));
        }

        public async Task<Dictionary<int, Dictionary<string, AccommodationSupplier>>> GetAllDictionaryGroupByProviderIdGroupByEdiCodeAsync()
        {
            return await this.cache.GetOrSetAsync("Pvd", async() => (await this.cache.GetAllAsync()).GroupBy(i => i.ProviderId)
                .ToDictionary(i => i.Key, i => i.GroupBy(j => j.EdiCode).ToDictionary(j => j.Key ?? "default", j => j.First())));
        }

        public async Task<AccommodationSupplier[]> GetByIdsAsync(int[] ids)
        {
            return await this.cache.GetByKeysAsync(ids.Select(i => i.ToString()).ToArray());
        }

        public async Task<AccommodationSupplier> GetByNameAsync(string name)
        {
            return (await this.cache.GetAllAsync()).FirstOrDefault(x => x.Name == name);
        }

        public async Task<AccommodationSupplier> GetByEdiCodeAsync(string ediCode)
        {
            return (await this.cache.GetAllAsync()).FirstOrDefault(x => x.EdiCode == ediCode);
        }

        public async Task<AccommodationSupplier[]> GetByProviderCodeAsync(int providerCode)
        {
            return (await this.cache.GetAllAsync()).Where(x => x.ProviderId == providerCode).ToArray();
        }
    }
}
