﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class MarkupRepository : IMarkupRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<Markup> cache;

        public MarkupRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<Markup> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("AMk", async() => (await this.accommodationDbContextActivator.GetDbContext().GetMarkupsAllAsync()).ToArray());
        }

        public async Task<Markup[]> GetActiveFilteredHierarchicalAsync(Channel channel, int? agentId)
        {
            return (await this.cache.GetAllAsync()).Where(i => i.IsActive 
                && (i.Channel == null || i.Channel == channel)
                && (i.IsB2B && agentId != null || !i.IsB2B && agentId == null)
                && (i.B2BAgentId == null || i.B2BAgentId == agentId)).ToArray();
        }
    }
}
