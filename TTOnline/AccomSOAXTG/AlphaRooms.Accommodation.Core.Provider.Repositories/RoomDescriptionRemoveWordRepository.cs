﻿namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DbContexts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Threading.Tasks;

    public class RoomDescriptionRemoveWordRepository : IRoomDescriptionRemoveWordRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<RoomDescriptionRemoveWord> cache;

        public RoomDescriptionRemoveWordRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<RoomDescriptionRemoveWord> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        // this is a Dictionary not a hashset. a Dictionary is guaranteed to support multiple readers concurrently, HashSet is not.
        public async Task<IReadOnlyDictionary<string, int>> GetAllAsync()
        {
            return await this.cache.GetOrSetAsync("DRW", (Func<Task<Dictionary<string, int>>>)this.GetFromRepositoryAsync);
        }

        public async Task<Dictionary<string, int>> GetFromRepositoryAsync()
        {
            var removeWords = this.accommodationDbContextActivator
                .GetDbContext()
                .RoomDescriptionRemoveWords;

            // Important needs to be case insensitive for key generation to work correctly
            return await removeWords.ToDictionaryAsync(x => x.Word, x => 0, StringComparer.OrdinalIgnoreCase);
        }

    }
}
