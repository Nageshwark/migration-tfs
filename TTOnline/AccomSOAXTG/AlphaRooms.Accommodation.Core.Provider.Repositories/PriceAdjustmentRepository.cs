﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Utilities;
using System.Data.Entity;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class PriceAdjustmentRepository : IPriceAdjustmentRepository
    {
        private readonly IDbContextActivator<CompetitorPricingDbContext> competitorPricingDbContextActivator;
        private readonly ICacheProviderAsync<PriceAdjustment> cache;

        public PriceAdjustmentRepository(IDbContextActivator<CompetitorPricingDbContext> competitorPricingDbContextActivator, ICacheProviderAsync<PriceAdjustment> cache)
        {
            this.competitorPricingDbContextActivator = competitorPricingDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<Guid, PriceAdjustment[]>> GetByDestinationIdAndDateDictionaryAsync(Guid baseDestinationId, DateTime checkInDate, DateTime checkOutDate)
        {
            return await this.cache.GetOrSetAsync("PAE" + baseDestinationId.ToShortString()+checkInDate.ToShortDateString()+checkOutDate.ToShortDateString(), async() =>
                (await this.competitorPricingDbContextActivator.GetDbContext().GetPriceAdjustmentsByDestinationIdAndDatesAsync(baseDestinationId, checkInDate, checkOutDate))
                    .GroupBy(i => i.EstablishmentId).ToDictionary(i => i.Key, i => i.ToArray()));
        }
        public async Task<Dictionary<Guid, PriceAdjustment[]>> GetByDestinationIdDictionaryAsync(Guid baseDestinationId)
        {
            return await this.cache.GetOrSetAsync("PAE" + baseDestinationId.ToShortString(), async () =>
                (await this.competitorPricingDbContextActivator.GetDbContext().GetPriceAdjustmentsByDestinationIdAsync(baseDestinationId))
                    .GroupBy(i => i.EstablishmentId).ToDictionary(i => i.Key, i => i.ToArray()));
        }
    }
}
