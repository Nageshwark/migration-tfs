﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class VirtualCreditCardRuleRepository : IVirtualCreditCardRuleRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<VirtualCreditCardRule> cache;

        public VirtualCreditCardRuleRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<VirtualCreditCardRule> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("VCC", async() => await this.accommodationDbContextActivator.GetDbContext().VirtualCreditCardRules.ToArrayAsync());
        }

        public async Task<VirtualCreditCardRule> GetDefaultRuleAsync()
        {
            return await this.cache.GetOrSetAsync("Dft", async() => (await this.cache.GetAllAsync()).First(i => i.SupplierId == null));
        }

        public async Task<VirtualCreditCardRule> GetProviderIdAndSupplierCodeAsync(int providerId, string supplierEdiCode)
        {
            return await this.cache.GetOrSetAsync("Av", async() => (await this.cache.GetAllAsync()).FirstOrDefault(i => i.Supplier != null 
                && i.Supplier.ProviderId == providerId && i.Supplier.EdiCode == supplierEdiCode));
        }

        public async Task<IEnumerable<VirtualCreditCardRule>> GetBySupplierIdAsync(int supplierId)
        {
            return (await this.cache.GetAllAsync()).Where(i => i.SupplierId == supplierId);
        }
    }
}
