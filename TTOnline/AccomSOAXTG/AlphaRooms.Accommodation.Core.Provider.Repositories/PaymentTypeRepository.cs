﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System.Data.Entity;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class PaymentTypeRepository : IPaymentTypeRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<PaymentType> cache;

        public PaymentTypeRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<PaymentType> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<PaymentTypeType, PaymentType>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("PTT", async() => await this.accommodationDbContextActivator.GetDbContext().PaymentTypes.ToDictionaryAsync(i => (PaymentTypeType)i.Id, i => i));
        }
    }
}
