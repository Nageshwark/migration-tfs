﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class TravellerTypeRepository : ITravellerTypeRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<TravellerType> cache;

        public TravellerTypeRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<TravellerType> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<int, TravellerType>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("DTN", async() => await this.accommodationDbContextActivator.GetDbContext().TravellerTypes.ToDictionaryAsync(i => i.Id, i => i));
        }
    }
}
