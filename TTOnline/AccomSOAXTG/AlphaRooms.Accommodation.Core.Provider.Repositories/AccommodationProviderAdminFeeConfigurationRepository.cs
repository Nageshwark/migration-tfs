﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Utilities;
using System.Data.Entity;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class AccommodationProviderAdminFeeConfigurationRepository : IAccommodationProviderAdminFeeConfigurationRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<AccommodationProviderAdminFeeConfiguration> cache;

        public AccommodationProviderAdminFeeConfigurationRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<AccommodationProviderAdminFeeConfiguration> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<int, AccommodationProviderAdminFeeConfiguration>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("Apafc", async () => await this.accommodationDbContextActivator.GetDbContext().AccommodationProviderAdminFeeConfiguration.ToDictionaryAsync(i => i.Id, i => i));
        }
    }
}
