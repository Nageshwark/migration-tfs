﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class RoomDescriptionMappingRepository : IRoomDescriptionMappingRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<RoomDescriptionMapping> cache;

        public RoomDescriptionMappingRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<RoomDescriptionMapping> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<RoomDescriptionMapping[]> GetByLocaleIdAsync(int localeId)
        {
            return await this.cache.GetOrSetAsync("Loc" + localeId.ToString(), async() => (await this.accommodationDbContextActivator.GetDbContext().GetRoomDescriptionMappingByLocaleIdAsync(localeId)).ToArray());
        }
    }
}