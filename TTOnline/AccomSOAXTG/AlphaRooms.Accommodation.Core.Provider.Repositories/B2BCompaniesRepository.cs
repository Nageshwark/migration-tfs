﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class B2BCompaniesRepository: IB2BCompaniesRepository
    {

        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<B2BCompany> cache;

        public B2BCompaniesRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<B2BCompany> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            // TODO put it back
            //this.cache.Register("B2BC", async () => await this.accommodationDbContextActivator.GetDbContext().B2BCompanies.Include(i => i.B2BUsers).ToArrayAsync()
            //    , async (i) => { int id = int.Parse(i); return await this.accommodationDbContextActivator.GetDbContext().B2BCompanies.Include(j => j.B2BUsers).FirstAsync(k => k.Id == id);},i=>i.Id.ToString());

        }
        public async Task<B2BCompany[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }

        public async Task<B2BCompany> GetByIdAsync(int Id)
        {
            return await this.cache.GetByKeyAsync(Id.ToString());
        }

        public async Task<B2BCompany[]> GetByParentIdAsync(int Id)
        {
            return await this.cache.GetOrSetAsync("B2BPC"+ Id.ToString("0000") , async() => (await this.cache.GetAllAsync()).Where(i=>i.ParentCompany.Id == Id).ToArray());
        }
    }
}