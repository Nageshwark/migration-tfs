﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class AccommodationProviderRepository : IAccommodationProviderRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<AccommodationProvider> cache;
        private readonly IProviderParameterUpdater<AccommodationProvider> supplierParameterUpdater;
        private readonly IAccommodationConfigurationManager configurationManager;

        public AccommodationProviderRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<AccommodationProvider> cache
            , IProviderParameterUpdater<AccommodationProvider> supplierParameterUpdater, IAccommodationConfigurationManager configurationManager)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.supplierParameterUpdater = supplierParameterUpdater;
            this.configurationManager = configurationManager;
            this.cache = cache;
            this.cache.Register("APr", async() => UpdateProperties(supplierParameterUpdater.UpdateSettings(await this.accommodationDbContextActivator.GetDbContext().AccommodationProviders.Include("Parameters").ToArrayAsync(), i => i.Parameters))
                , async(i) => { int id = int.Parse(i); return UpdateProperties(supplierParameterUpdater.UpdateSettings(await this.accommodationDbContextActivator.GetDbContext().AccommodationProviders.Include("Parameters").FirstAsync(j => j.Id == id), j => j.Parameters)); }
                , i => i.Id.ToString());
        }

        private AccommodationProvider[] UpdateProperties(AccommodationProvider[] providers)
        {
            providers.ForEach(i => UpdateProperties(i));
            return providers;
        }

        private AccommodationProvider UpdateProperties(AccommodationProvider provider)
        {
            provider.IsBookingEnabled = (configurationManager.AccommodationBookingDisableAll ? false : provider.IsBookingEnabled);
            return provider;
        }

        public async Task<AccommodationProvider[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }

        public async Task<Dictionary<int, AccommodationProvider>> GetAllDictionaryGroupByIdAsync()
        {
            return await this.cache.GetOrSetAsync("PEd", async() => (await this.cache.GetAllAsync()).ToDictionary(i => i.Id, i => i));
        }

        public async Task<AccommodationProvider[]> GetActiveAsync()
        {
            return await this.cache.GetOrSetAsync("Act", async() => (await this.cache.GetAllAsync()).Where(i => i.IsActive).ToArray());
        }

        public async Task<AccommodationProvider[]> GetByIdsAsync(int[] ids)
        {
            return await this.cache.GetByKeysAsync(ids.Select(i => i.ToString()).ToArray());
        }

        public async Task<AccommodationProvider> GetByIdAsync(int providerId)
        {
            return await this.cache.GetByKeyAsync(providerId.ToString());
        }
        
    }
}
