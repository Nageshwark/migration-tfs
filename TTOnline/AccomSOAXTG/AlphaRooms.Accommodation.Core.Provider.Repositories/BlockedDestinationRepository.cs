﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class BlockedDestinationRepository : IBlockedDestinationRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<BlockedDestination> cache;

        public BlockedDestinationRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<BlockedDestination> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("ABD", async() => await this.accommodationDbContextActivator.GetDbContext().BlockedDestinations.ToArrayAsync());
        }

        public async Task<BlockedDestination[]> GetAllAsync()
        {
            return await this.cache.GetOrSetAsync<BlockedDestination[]>(
                "AllBlockedDestinations",
                async () => await this.accommodationDbContextActivator.GetDbContext().BlockedDestinations.ToArrayAsync());
        }

        public async Task<BlockedDestination[]> GetByChannelAndDestinationIdHierarchicalAsync(Channel channel, Guid destinationId)
        {
            return await this.cache.GetOrSetAsync("CDt" + ((int)channel).ToString("00") + destinationId.ToShortString(), async() => (await this.cache.GetAllAsync()).Where(i => (i.Channel == null 
                || i.Channel == channel) && i.DestinationId == destinationId).ToArray());
        }
    }
}
