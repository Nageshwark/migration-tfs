﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class B2BUserRepository : IB2BUserRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<B2BUser> cache;

        public B2BUserRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<B2BUser> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("B2BU", async () => await this.accommodationDbContextActivator.GetDbContext().B2BUsers
                .Include("Company")
                .Include(u => u.BillToCompany)
                .Include(u => u.Customers)
                .Include(u=>u.AgentConfiguration)
                .Include("Company.ParentCompany")
                .Include("Providers")
                .Include("Suppliers")
                .Include("Providers.Parameters").ToArrayAsync()
            , async (i) => await this.accommodationDbContextActivator.GetDbContext().B2BUsers
                .Include("Company")
                .Include(u => u.BillToCompany)
                .Include(u => u.Customers)
                .Include(u => u.AgentConfiguration)
                .Include("Company.ParentCompany")
                .Include("Providers")
                .Include("Suppliers")
                .Include("Providers.Parameters").FirstAsync(j => j.UserName == i), i => i.UserName);
        }


        public async Task<B2BUser[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }

        public async Task<B2BUser> GetByIdAsync(int agentId)
        {
            return (await this.cache.GetAllAsync()).First(i => i.Id == agentId);
        }

        public async Task<B2BUser> GetByUserNameAsync(string name)
        {
            return await this.cache.GetByKeyAsync(name);
        }
    }
}

