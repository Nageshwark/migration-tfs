﻿namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    using AlphaRooms.Accommodation.Core.Provider.DbContexts;
    using AlphaRooms.Accommodation.Core.Provider.DomainModels;
    using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    public class RoomDescriptionReplacementRepository : IRoomDescriptionReplacementRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<RoomDescriptionWordReplacement> cache;

        public RoomDescriptionReplacementRepository(
            IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator,
            ICacheProviderAsync<RoomDescriptionWordReplacement> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        /// <summary>
        /// Gets all alias from the database in correct order, order is important
        /// </summary>
        /// <returns>all Alias in the order of the of priority</returns>
        public async Task<IReadOnlyList<Alias>> GetAllAliasAsync()
        {
            return await this.cache.GetOrSetAsync("DWR", (Func<Task<Alias[]>>)this.GetFromRepositoryAsync);
        }

        private async Task<Alias[]> GetFromRepositoryAsync()
        {
            var roomDescriptionReplacements = await this.accommodationDbContextActivator
                .GetDbContext()
                .RoomDescriptionWordReplacements
                // must be ordered! The order is relied upon when de duplicating
                .OrderBy(x => x.Order)
                .ToListAsync();

            return roomDescriptionReplacements
                .Select(x => new Alias(x.Word, x.Replacement))
                .ToArray();
        }
    }
}