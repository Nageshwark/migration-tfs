﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class DestinationSearchModeRepository : IDestinationSearchModeRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<DestinationSearchMode> cache;

        public DestinationSearchModeRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<DestinationSearchMode> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("DSM", async() => await this.accommodationDbContextActivator.GetDbContext().DestinationSearchModes.ToArrayAsync());
        }

        public async Task<DestinationSearchMode[]> GetByDestinationIdAsync(Guid destinationId)
        {
            return await this.cache.GetOrSetAsync("Dst" + destinationId.ToShortString(), async() => (await this.cache.GetAllAsync()).Where(i => i.DestinationId == destinationId).ToArray());
        }
    }
}
