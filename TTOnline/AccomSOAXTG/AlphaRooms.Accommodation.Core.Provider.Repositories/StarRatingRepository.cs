﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels.Enums;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using System.Data.Entity;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class StarRatingRepository : IStarRatingRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<StarRating> cache;

        public StarRatingRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<StarRating> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<StarRatingType, StarRating>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("STG", async() => await this.accommodationDbContextActivator.GetDbContext().StarRatings.ToDictionaryAsync(i => (StarRatingType)i.Id, i => i));
        }
    }
}
