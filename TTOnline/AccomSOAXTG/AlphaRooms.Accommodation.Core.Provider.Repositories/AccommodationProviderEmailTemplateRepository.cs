﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class AccommodationProviderEmailTemplateRepository : IAccommodationProviderEmailTemplateRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<AccommodationProviderEmailTemplate> cache;

        public AccommodationProviderEmailTemplateRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<AccommodationProviderEmailTemplate> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("PEP", async() => await this.accommodationDbContextActivator.GetDbContext().AccommodationProviderEmailTemplates.ToArrayAsync());
        }

        public async Task<IEnumerable<AccommodationProviderEmailTemplate>> GetByProviderIdAndTemplateTypeIdAsync(int providerId, int emailTemplateTypeId)
        {
            return await this.cache.GetOrSetAsync("PeC" + providerId.ToString("00") + emailTemplateTypeId.ToString("00"), async() => (await this.cache.GetAllAsync())
                .Where(i => i.ProviderId == providerId && i.EmailTemplateTypeId == emailTemplateTypeId).ToArray());
        }
    }
}
