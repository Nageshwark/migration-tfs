﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class RoomTypeRepository : IRoomTypeRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<RoomType> cache;

        public RoomTypeRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<RoomType> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("RTy", async() => await this.accommodationDbContextActivator.GetDbContext().RoomTypes.ToArrayAsync());
        }

        public async Task<RoomType[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }
    }
}