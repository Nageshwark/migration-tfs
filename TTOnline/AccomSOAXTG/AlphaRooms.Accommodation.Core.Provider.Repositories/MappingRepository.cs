﻿using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Utilities;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class MappingRepository : IMappingRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<Mapping> cache;

        public MappingRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<Mapping> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<string, ProviderMapping>> GetByBaseDestinationIdAsync(Guid baseDestinationId)
        {
            var mappingCache = await this.cache.GetOrSetAsync("MCH" + baseDestinationId.ToShortString(), async () => await GetByBaseDestinationIdAsyncInternal(baseDestinationId));
            return mappingCache.Destinations.ContainsKey(baseDestinationId) ? mappingCache.Destinations[baseDestinationId] : null;
        }

        public async Task<Dictionary<string, ProviderMapping>> GetByDestinationIdAsync(Guid baseDestinationId, Guid destinationId)
        {
            var mappingCache = await this.cache.GetOrSetAsync("MCH" + baseDestinationId.ToShortString(), async () => await GetByBaseDestinationIdAsyncInternal(baseDestinationId));
            if (mappingCache.Destinations.ContainsKey(destinationId))
            {
                return mappingCache.Destinations[destinationId];
            }
            return null;
        }

        public async Task<Dictionary<string, ProviderMapping>> GetByEstablishmentIdAsync(Guid baseDestinationId, Guid destinationId, Guid establishmentId)
        {
            var mappingCache = await this.cache.GetOrSetAsync("MCH" + baseDestinationId.ToShortString(), async () => await GetByBaseDestinationIdAsyncInternal(baseDestinationId));
            return mappingCache.Destinations.ContainsKey(destinationId) ? mappingCache.Destinations[destinationId].ToDictionary(i => i.Key, i => new ProviderMapping()
            {
                DestinationCodes = i.Value.DestinationCodes,
                EstablishmentMappings = i.Value.EstablishmentMappings.Where(j => j.Key == establishmentId).ToDictionary(j => j.Key, j => j.Value)
            }) : null;
        }
        private async Task<Mapping> GetByBaseDestinationIdAsyncInternal(Guid baseDestinationId)
        {
            List<EdiProviderDestinationEstablishmentMapping> ediProviderDestinationEstablishmentMappings;

            using (MultiResultsReader objectReader = await this.accommodationDbContextActivator.GetDbContext().GetAccommodationMappingByBaseDestinationIdAsync(baseDestinationId))
            {
                ediProviderDestinationEstablishmentMappings = objectReader.NextResult<EdiProviderDestinationEstablishmentMapping>().ToList();
            }

            var providerMappings = new Dictionary<Guid, Dictionary<string, ProviderMapping>>();

            // New Code 
            // Grouping all results by AlphaDestination
            foreach (var destGroup in ediProviderDestinationEstablishmentMappings.GroupBy(x => x.DestinationId))
            {
                var pmDict = new Dictionary<string, ProviderMapping>();
                //Grouping all results by Provider
                foreach (var provGroup in destGroup.GroupBy(y => y.ProviderEdiCode))
                {
                    // Data to hold:
                    //   All providerDestinations codes
                    //   All providerEstablishment mappings
                    ProviderMapping pm = new ProviderMapping();
                    pm.DestinationCodes = provGroup.Select(z => z.ProviderDestinationCode).Distinct().ToArray();
                    pm.EstablishmentMappings = new Dictionary<Guid, EstablishmentMapping[]>();
                    //Grouping by AlphaEstablishment (relationship AlphaEstablishment - ProviderEstablishment can be 1 - N)
                    foreach (var estGroup in provGroup.GroupBy(y => y.EstablishmentId))
                    {
                        pm.EstablishmentMappings.Add(estGroup.Key, estGroup.Select(item => new EstablishmentMapping()
                        {
							ResortDestinationId = item.ResortDestinationId,
                            DestinationId = item.DestinationId,
                            EstablishmentId = item.EstablishmentId,
                            ProviderEdiCode = item.ProviderEdiCode,
                            ProviderEstablishmentCode = item.ProviderEstablishmentCode
                        }).ToArray());
                    }
                    pmDict.Add(provGroup.Key, pm);
                }
                providerMappings.Add(destGroup.Key, pmDict);
            }
            return new Mapping() { Destinations = providerMappings };
        }
    }
}
