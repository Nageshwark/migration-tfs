﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class BoardDescriptionMappingRepository : IBoardDescriptionMappingRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<BoardDescriptionMapping> cache;

        public BoardDescriptionMappingRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<BoardDescriptionMapping> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<BoardDescriptionMapping[]> GetByLocaleIdAsync(int localeId)
        {
            return await this.cache.GetOrSetAsync("Loc" + localeId.ToString(), async() => (await this.accommodationDbContextActivator.GetDbContext().GetBoardDescriptionMappingByLocaleIdAsync(localeId)).ToArray());
        }
    }
}