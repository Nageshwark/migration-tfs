﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class EstablishmentDetailsRepository : IEstablishmentDetailsRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<EstablishmentDetails> cache;

        public EstablishmentDetailsRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<EstablishmentDetails> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<Guid, EstablishmentDetails>> GetByDestinationIdDictionaryGroupEstablishmentIdAsync(Guid destinationId, Channel channel)
        {
            return await this.cache.GetOrSetAsync("EsD" + destinationId.ToShortString() + "_" + ((int)channel).ToString(), async () => await GetByDestinationIdDictionaryGroupEstablishmentIdAsyncInternal(destinationId, channel));
        }

        private async Task<Dictionary<Guid, EstablishmentDetails>> GetByDestinationIdDictionaryGroupEstablishmentIdAsyncInternal(Guid destinationId, Channel channel)
        {
            EstablishmentDetails[] establishmentDetails;
            Dictionary<Guid, Guid[]> establishmentFacilities;
            Dictionary<Guid, int[]> establishmentTravellerTypes;

            using (MultiResultsReader objectReader = await this.accommodationDbContextActivator.GetDbContext().GetEstablishmentDetailsByDestinationIdAsync(destinationId, channel))
            {
                establishmentDetails = objectReader.NextResult<EstablishmentDetails>().ToArray();
                establishmentFacilities = objectReader.NextResult<EstablishmentFacility>().GroupBy(i => i.EstablishmentId).ToDictionary(i => i.Key, i => i.Select(j => j.FacilityId).ToArray());
                establishmentTravellerTypes = objectReader.NextResult<EstablishmentTravellerType>().GroupBy(i => i.EstablishmentId).ToDictionary(i => i.Key, i => i.Select(j => j.TravellerTypeId).ToArray());
            }

            Parallel.ForEach(establishmentDetails, establishment =>
            {
                establishment.FacilityIds = establishmentFacilities.GetValueOrDefault(establishment.EstablishmentId, new Guid[0]);
                establishment.TravellerTypeIds = establishmentTravellerTypes.GetValueOrDefault(establishment.EstablishmentId, new int[0]);
            });

            return establishmentDetails.ToDictionary(i => i.EstablishmentId, i => i);
        }
    }
}
