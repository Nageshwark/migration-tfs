﻿using System.Data.Entity;
using System.Threading.Tasks;
using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class AccommodationFlashSaleRepository : IAccommodationFlashSaleRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<AccommodationB2BFlashSaleEstablishment> cache;

        public AccommodationFlashSaleRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<AccommodationB2BFlashSaleEstablishment> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("AccommodationB2BFlashSaleEstablishments", async () => await this.accommodationDbContextActivator.GetDbContext().FlashSaleEstablishments.ToArrayAsync());
        }

        public async Task<AccommodationB2BFlashSaleEstablishment[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }
    }
}