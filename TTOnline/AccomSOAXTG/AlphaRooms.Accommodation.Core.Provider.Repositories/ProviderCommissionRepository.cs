﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class ProviderCommissionRepository : IProviderCommissionRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<ProviderCommission> cache;

        public ProviderCommissionRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<ProviderCommission> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }

        public async Task<Dictionary<string, ProviderCommission>> GetAllDictionaryAsync()
        {
            return await this.cache.GetOrSetAsync("PCI", async() => (await this.accommodationDbContextActivator.GetDbContext().ProviderCommissions.GroupBy(i => i.ProviderEdiCode + i.SupplierEdiCode)
                .ToDictionaryAsync(i => i.Key, i => i.First())));
        }
    }
}
