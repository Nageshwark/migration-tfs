﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class VolumeDiscountRepository : IVolumeDiscountRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<VolumeDiscount> cache;

        public VolumeDiscountRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<VolumeDiscount> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("VDc", async() => await this.accommodationDbContextActivator.GetDbContext().VolumeDiscounts.ToArrayAsync());
        }

        public async Task<VolumeDiscount[]> GetAllAsync()
        {
            return await this.cache.GetAllAsync();
        }
    }
}
