﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class SupplierExcludedOfDepositRepository : ISupplierExcludedOfDepositRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<string> cache;

        public SupplierExcludedOfDepositRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator,
            ICacheProviderAsync<string> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
        }
        public bool CheckIfSupplierExistsInExcludedList(string[] excludedSupplierList, string ediCode)
        {
            var isExist = this.accommodationDbContextActivator.GetDbContext().CheckIfSupplierExistsInExcludedList(excludedSupplierList, ediCode);
            return isExist;
        }

        public async Task<string[]> GetDepositExcludedListOFProvidersAsync()
        {
            return await this.cache.GetOrSetAsync("DESL", async () => await this.accommodationDbContextActivator.GetDbContext().GetDepositExcludedListOFProvidersAsync());
        }

    }
}
