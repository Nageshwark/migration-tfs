﻿using AlphaRooms.Accommodation.Core.Provider.DbContexts;
using AlphaRooms.Accommodation.Core.Provider.DomainModels;
using AlphaRooms.Accommodation.Core.Provider.Interfaces.Repositories;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.Core.Provider.Repositories
{
    public class CancellationPolicyRepository : ICancellationPolicyRepository
    {
        private readonly IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator;
        private readonly ICacheProviderAsync<CancellationPolicy> cache;

        public CancellationPolicyRepository(IDbContextActivator<AccommodationDbContext> accommodationDbContextActivator, ICacheProviderAsync<CancellationPolicy> cache)
        {
            this.accommodationDbContextActivator = accommodationDbContextActivator;
            this.cache = cache;
            this.cache.Register("ACP", async() => await this.accommodationDbContextActivator.GetDbContext().CancellationPolicies.ToArrayAsync());
        }

        public async Task<CancellationPolicy> GetByProviderIdAndProviderPolicyIdAsync(int? providerId, byte providerPolicyId, bool isB2B)
        {
            return await this.cache.GetOrSetAsync("PPP" + providerId.ToString() + providerPolicyId + isB2B.ToString(), async() => 
                (await this.cache.GetAllAsync()).Where(i => i.ProviderId == providerId  && i.ProviderPolicyId == providerPolicyId && (i.IsB2B == isB2B || i.IsB2B == null))
                .OrderBy(i => i.IsB2B).First()
            );
        }

        public async Task<CancellationPolicy> GetDefaultCancellationPolicyAsync(bool isB2B)
        {
            return await GetByProviderIdAndProviderPolicyIdAsync(null, 1, isB2B);
        }

        public async Task<CancellationPolicy> GetNonRefundableCancellationPolicyAsync(bool isB2B)
        {
            return await GetByProviderIdAndProviderPolicyIdAsync(null, 2, isB2B);
        }
    }
}
