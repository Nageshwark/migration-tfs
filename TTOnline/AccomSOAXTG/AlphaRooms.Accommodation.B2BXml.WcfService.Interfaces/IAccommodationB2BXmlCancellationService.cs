﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Accommodation.B2BXml.WcfService.Interfaces
{
    [ServiceContract]
    public interface IAccommodationB2BXmlCancellationService
    {
        [OperationContract]
        Task<string> GetCancellationResponseAsync(string request);
    }
}
