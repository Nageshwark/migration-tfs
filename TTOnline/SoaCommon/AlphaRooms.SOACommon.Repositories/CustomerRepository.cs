﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;

		public CustomerRepository(IDbContextActivator<SOACommonContext> context)
        {
            this.dbSOAContextActivator = context;
        }

        public async Task<PassengerContactDetails> GetCustomerByItineraryId(int id)
        {
            var dbContext = this.dbSOAContextActivator.GetDbContext();
            var passengerItinerary = await dbContext.Itineraries.FirstAsync(c => c.Id == id);
            return await dbContext.Customers.FirstAsync(c => c.Id == passengerItinerary.CustomerId);
        }
    }
}
