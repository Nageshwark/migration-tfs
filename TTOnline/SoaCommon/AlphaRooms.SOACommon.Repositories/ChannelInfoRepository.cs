﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities;

namespace AlphaRooms.SOACommon.Repositories
{
    public class ChannelInfoRepository : IChannelInfoRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;
        private readonly ICacheProviderAsync<ChannelInfo> cache;

        public ChannelInfoRepository(IDbContextActivator<SOACommonContext> context, ICacheProviderAsync<ChannelInfo> cache)
        {
			this.dbSOAContextActivator = context;
            this.cache = cache;
            this.cache.Register("ChI"
                , async() => await this.dbSOAContextActivator.GetDbContext().ChannelInfos.ToArrayAsync()
                , async(i) => 
                {
                    int id = int.Parse(i);
                    return await this.dbSOAContextActivator.GetDbContext().ChannelInfos.FirstAsync(j => j.ChannelId == id);
                }, i => i.ChannelId.ToString());
        }

        public async Task<ChannelInfo> GetChannelInfo(Channel channel)
        {
            return await this.cache.GetByKeyAsync(((int)channel).ToString());
        }
        
        public async Task<ChannelInfo> GetChannelInfoByLocaleId(int localeId)
        {
            return await this.cache.GetOrSetAsync("LId" + localeId.ToString(), async() => await this.dbSOAContextActivator.GetDbContext().ChannelInfos.FirstAsync(j => j.LocaleId == localeId));
        }

        public async Task<ChannelInfo> GetChannelInfoBySiteAndLanguageCodeAsync(Sites site, string languageCode, string countryCode)
        {
            return await this.cache.GetOrSetAsync("LgC" + site.ToString() + languageCode, async () => await this.dbSOAContextActivator.GetDbContext().ChannelInfos
                .SingleAsync(j => j.Site == site && j.LanguageCode == languageCode && j.CountryCode == countryCode));
        }
    }
}
