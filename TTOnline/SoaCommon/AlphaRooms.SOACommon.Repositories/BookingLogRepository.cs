﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class BookingLogRepository: IBookingLogRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;

        public BookingLogRepository(IDbContextActivator<SOACommonContext> context)
        {
			this.dbSOAContextActivator = context;
        }
        
        public async Task<bool> WriteLog(BookingLogItem log)
        {
            try
            {
				var bookingLogContext = dbSOAContextActivator.GetDbContext();
                bookingLogContext.BookingLogItems.Add(log);
                await bookingLogContext.SaveChangesAsync();
                return true;
            }
            catch (DbEntityValidationException)
            {
                return false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        // for integration testing
        public async Task<BookingLogItem> ReadLogbyBookingId(Guid bookingId, int iSequence)
        {
			return await dbSOAContextActivator.GetDbContext().BookingLogItems.FirstAsync(f => f.BookingId == bookingId && f.Sequence == iSequence);
        }
    }
}
