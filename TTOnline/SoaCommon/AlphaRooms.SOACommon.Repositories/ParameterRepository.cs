﻿namespace AlphaRooms.SOACommon.Repositories
{
    using AlphaRooms.SOACommon.DbContexts;
    using AlphaRooms.SOACommon.DomainModels;
    using AlphaRooms.SOACommon.Interfaces;
    using AlphaRooms.Utilities;
    using AlphaRooms.Utilities.EntityFramework;
    using System.Linq;

    public class ParameterRepository : IParameterRepository
    {
        private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;
        private readonly ICacheProvider<Parameter> cache;

        public ParameterRepository(IDbContextActivator<SOACommonContext> SOACommonContext, ICacheProvider<Parameter> cache)
        {
            this.dbSOAContextActivator = SOACommonContext;
            this.cache = cache;
            this.cache.Register(
                "PRp", 
                () => this.dbSOAContextActivator.GetDbContext().Parameters.ToArray(),
                (i) => this.dbSOAContextActivator.GetDbContext().Parameters.First(j => j.ParameterName == i),
                i => i.ParameterName);
        }

        public Parameter GetParameterByName(string paramName)
        {
            return this.cache.GetByKey(paramName);
        }
    }
}
