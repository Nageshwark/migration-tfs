﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces.Repositories;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class PromotionalDiscountRepository : IPromotionalDiscountRepository
    {
        private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;

        public PromotionalDiscountRepository(IDbContextActivator<SOACommonContext> dbSoaContextActivator)
        {
            dbSOAContextActivator = dbSoaContextActivator;
        }

        public PromotionalDiscount GetPromoCode(string voucherCode)
        {
            return this.dbSOAContextActivator.GetDbContext().PromotionalDiscounts.Include(i => i.AccommodationPromotion).FirstOrDefault(pc => pc.Voucher.Equals(voucherCode, StringComparison.OrdinalIgnoreCase));
        }
    }
}
