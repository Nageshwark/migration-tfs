﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class SupplierConnectivityLogRepository : ISupplierConnectivityLogRepository
    {
        private IDbContextActivator<SOACommonLogsContext> dbSOAContextActivator;

        public SupplierConnectivityLogRepository(IDbContextActivator<SOACommonLogsContext> context)
        {
			this.dbSOAContextActivator = context;
        }

        public async Task<SupplierConnectivityLog[]> GetSupplierConnectivityLogsAsync(DateTime startDate, DateTime endDate)
        {
			return await Task.FromResult<SupplierConnectivityLog[]>(dbSOAContextActivator.GetDbContext().SupplierConnectivityLogs.Where(i => i.LogDate >= startDate && i.LogDate <= endDate).ToArray());
        }

        public async Task SaveAsync(ISupplierConnectivityLogMonitor monitor)
        {
			await dbSOAContextActivator.GetDbContext().SupplierConnectivityLogsSaveAsync(monitor.HostName, (int)monitor.SupplierType, monitor.SupplierId, (int)monitor.ServiceType
                , monitor.StartDate.Value, (monitor.IsServiceSuccessful == true ? 1 : 0), (monitor.IsServiceSuccessful == true ? 0 : 1)
                , (int)(monitor.StopDate.Value - monitor.StartDate.Value).TotalMilliseconds
                , (monitor.ServiceException != null ? monitor.ServiceException.GetMessageWithInnerExceptions() : null));
        }
    }
}
