﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces.Repositories;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class PaymentDefaultCardFeeRepository : IPaymentDefaultCardFeeRepository
    {
        private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;
        private readonly ICacheProviderAsync<PaymentDefaultCardFee> cache;

        public PaymentDefaultCardFeeRepository(IDbContextActivator<SOACommonContext> context, ICacheProviderAsync<PaymentDefaultCardFee> cache)
        {
			this.dbSOAContextActivator = context;
            this.cache = cache;
            this.cache.Register(async() => await this.dbSOAContextActivator.GetDbContext().PaymentDefaultCardFees.ToArrayAsync());
        }

        public async Task<PaymentDefaultCardFee[]> GetByChannelId(DomainModels.Enumerators.Channel channel)
        {
            return await this.cache.GetOrSetAsync("DCC", async() => (await this.cache.GetAllAsync()).Where(i => i.Channel == channel).ToArray());
        }
    }
}
