﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.Utilities;

namespace AlphaRooms.SOACommon.Repositories
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;
        private readonly ICacheProviderAsync<ExchangeRate> cache;

        public ExchangeRateRepository(IDbContextActivator<SOACommonContext> context, ICacheProviderAsync<ExchangeRate> cache)
        {
			this.dbSOAContextActivator = context;
            this.cache = cache;
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(string currencyFrom, string currencyTo)
        {
            return await this.cache.GetOrSetAsync(currencyFrom + currencyTo, async() => await this.dbSOAContextActivator.GetDbContext()
                .ExchangeRates.FirstAsync(e => e.CurrencyFrom == currencyFrom && e.CurrencyTo == currencyTo));
        }
        
        public async Task<Dictionary<string, ExchangeRate>> GetExchangeRateAsync(string[] currencyFroms, string currencyTo)
        {
            return await this.cache.GetOrSetAsync(currencyFroms.ToString("") + ";" + currencyTo, async() => await this.dbSOAContextActivator.GetDbContext()
                .ExchangeRates.Where(e => currencyFroms.Any(j => e.CurrencyFrom == j) && e.CurrencyTo == currencyTo).ToDictionaryAsync(i => i.CurrencyFrom, i => i));
        }
    }
}
