﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class ItineraryRepository : IItineraryRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;

		public ItineraryRepository(IDbContextActivator<SOACommonContext> context)
        {
            this.dbSOAContextActivator = context;
        }
        
        public async Task<List<Passenger>> GetItineraryPersonByItineraryId(int itineraryId)
        {
            var dbcontext = this.dbSOAContextActivator.GetDbContext();
            return await dbcontext.ItineraryPersons.Where(x => x.ItineraryId == itineraryId).ToListAsync();
        }
    }
}
