﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities.EntityFramework;

namespace AlphaRooms.SOACommon.Repositories
{
    public class BookingItemRepository : IBookingItemRepository
    {
		private readonly IDbContextActivator<SOACommonContext> dbSOAContextActivator;

		public BookingItemRepository(IDbContextActivator<SOACommonContext> context)
        {
            this.dbSOAContextActivator = context;
        }
        
        public Task<List<ChargeItem>> GetItineraryBookingItems(int itineraryId)
        {
            var dbContext = this.dbSOAContextActivator.GetDbContext();

            var query = dbContext.Itineraries.Where(i => i.Id == itineraryId)
                .SelectMany(i => i.Bookings)
                .SelectMany(i => i.BookingItems)
                .SelectMany(i => i.BookingElements);

            return query.ToListAsync();
        }

        public List<Money> GetSupplierGross()
        {
            var dbContext = this.dbSOAContextActivator.GetDbContext();

			// 06/05/2014 - Commented out the original code that did not work - threw exception
			//return
			//	dbContext.BookingElements.Select(x => new Money(x.SupplierUnitCost.Amount * x.Quantity, x.SupplierUnitCost.CurrencyCode))
			//		.GroupBy(x => x.CurrencyCode)
			//		.Select(x => new Money(x.Sum(y => y.Amount), x.Key)).ToList();

			// Use anonymous types to avoid the error "Only parameterless constructors and initializers are supported in LINQ to Entities"
			var amountsGroupedByCurrencyCode = dbContext.BookingElements.Select(x => new {Amount = x.SupplierUnitCost.Amount * x.Quantity, CurrencyCode = x.SupplierUnitCost.CurrencyCode})
												.GroupBy(x => x.CurrencyCode);

			var summedTotalsByCurrencyCode = amountsGroupedByCurrencyCode.Select(x => new {Amount = (x.Sum(y => y.Amount)), CurrencyCode = x.Key});
			
			var supplierGross = summedTotalsByCurrencyCode.Select(z => new Money { Amount = z.Amount, CurrencyCode = z.CurrencyCode}).ToList();

			return supplierGross;
		}
    }
}
