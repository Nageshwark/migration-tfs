﻿using AlphaRooms.CacheInterfaces;
using AlphaRooms.SOACommon.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using m = MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace AlphaRooms.CacheCommands
{
    public class MongoCacheWriter : ICacheWriter
    {
        private readonly IDbConnection mongoDb;

        public MongoCacheWriter(IDbConnection mongoDb)
        {
            this.mongoDb = mongoDb;
        }
        
        public void Add<T>(string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire
        {
            value.CacheExpireDate = (DateTime.Now + cacheExpireTimeout).ToUniversalTime();
            mongoDb.GetMongoCollection<T>(collectionName).Insert(value);
        }

        public void AddIfDoesntExist<T>(string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire
        {
            value.CacheExpireDate = (DateTime.Now + cacheExpireTimeout).ToUniversalTime();
            try
            {
                mongoDb.GetMongoCollection<T>(collectionName).Insert(value, new MongoInsertOptions() { Flags = InsertFlags.ContinueOnError });
            }
            catch { }
        }

        public void AddIfDoesntExist<T>(string databaseKey, string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire
        {
            value.CacheExpireDate = (DateTime.Now + cacheExpireTimeout).ToUniversalTime();
            try
            {
                mongoDb.GetMongoCollection<T>(databaseKey, collectionName).Insert(value, new MongoInsertOptions() { Flags = InsertFlags.ContinueOnError });
            }
            catch { }
        }

        public void AddOrUpdate<T>(string collectionName, string keyName, object keyValue, T value)
        {
            mongoDb.GetMongoCollection<T>(collectionName).Update(Query.EQ(keyName, BsonValue.Create(keyValue)), m.Update.Replace<T>(value), UpdateFlags.Upsert);
        }

        public void AddRange<T>(string collectionName, IEnumerable<T> values, TimeSpan cacheExpireTimeout) where T : ICacheExpire
        {
            DateTime expireDate = (DateTime.Now + cacheExpireTimeout).ToUniversalTime();
            foreach (var value in values)
            {
                value.CacheExpireDate = expireDate;
            }
            mongoDb.GetMongoCollection<T>(collectionName).InsertBatch(values);

        }

        public void AddRange<T>(string databaseKey, string collectionName, IEnumerable<T> value)
        {
            mongoDb.GetMongoCollection<T>(databaseKey, collectionName).InsertBatch(value);
        }

        public void AddRangeIfDoesntExist<T>(string databaseKey, string collectionName, IEnumerable<T> value)
        {
            try
            {
                mongoDb.GetMongoCollection<T>(databaseKey, collectionName).InsertBatch(value, new MongoInsertOptions() { Flags = InsertFlags.ContinueOnError });
            }
            catch { }
        }

        public void AddRangeIfDoesntExist<T>(string collectionName, IEnumerable<T> value)
        {
            try
            {
                mongoDb.GetMongoCollection<T>(collectionName).InsertBatch(value, new MongoInsertOptions() { Flags = InsertFlags.ContinueOnError });
            }
            catch { }
        }

        public void AddOrUpdateRange<T>(string databaseKey, string collectionName, string keyName, Func<T, object> getKeyValue, IEnumerable<T> value)
        {
            var collection = this.mongoDb.GetMongoCollection<T>(databaseKey, collectionName);
            foreach (var item in value)
            {
                collection.Update(Query.EQ(keyName, BsonValue.Create(getKeyValue.Invoke(item))), m.Update.Replace<T>(item), UpdateFlags.Upsert);
            }
        }
        
        public void Update<T>(string databaseKey, string collectionName, string keyName, object keyValue, T value)
        {
            mongoDb.GetMongoCollection<T>(databaseKey, collectionName).Update(Query.EQ(keyName, BsonValue.Create(keyValue)), m.Update.Replace<T>(value));
        }
    }
}
