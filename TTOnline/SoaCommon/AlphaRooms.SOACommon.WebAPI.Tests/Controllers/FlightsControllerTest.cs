﻿using AlphaRooms.Flights.Contracts;
using AlphaRooms.Flights.Interfaces;
using AlphaRooms.Flights.Interfaces.Services;
using AlphaRooms.SOACommon.WebAPI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;

namespace AlphaRooms.SOACommon.WebAPI.Tests.Controllers
{
    [TestClass]
    public class FlightsControllerTest
    {
        
        [TestMethod]
        public async Task Search_Sould_Return_SearchId()
        {
            var request = new FlightAvailabilityRequest()
            {
                DepartureAirportCode = "LGW",
                DestinationAirportCode = "PMI",
                DepartureDate = DateTime.Parse("05 May 2014"),
                ReturnDate = DateTime.Parse("15 May 2014").AddDays(7),
                Duration = 10,
                Adults = 2,
                Children = 1,
                ChildAges = new[] { 7 },
                Channel = AlphaRooms.SOACommon.DomainModels.Enumerators.Channel.AlphaRoomsUK
                ,Debugging=true
            };
            
            var searchId = Guid.NewGuid();

            Mock<IAvailabilityService> mockAvailabilityService = new Mock<IAvailabilityService>();
            mockAvailabilityService.Setup(x=>x.StartAvailabilitySearch(request)).Returns(searchId);

            Mock<IValuationService> mockValuationService=new Mock<IValuationService>();

            FlightsController controller = new FlightsController(mockAvailabilityService.Object,mockValuationService.Object);

            controller.Request = new System.Net.Http.HttpRequestMessage();

            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            var response=await controller.Search(request) as OkNegotiatedContentResult<Guid>;

            Assert.IsNotNull(response);

            Assert.AreEqual(searchId, response.Content);
        }

        
    }
}
