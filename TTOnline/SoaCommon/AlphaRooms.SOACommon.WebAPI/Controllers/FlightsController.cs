﻿using AlphaRooms.Flights.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AlphaRooms.Flights.Contracts;
using AlphaRooms.Flights.DependencyInjection;
using AlphaRooms.Flights.Interfaces;
using AlphaRooms.Flights.Interfaces.Services;
using AlphaRooms.Flights.Services;
using MongoDB.Bson.Serialization.Serializers;
using Newtonsoft.Json;
using Ninject;

namespace AlphaRooms.SOACommon.WebAPI.Controllers
{
    [RoutePrefix("api/Flights")]
    public class FlightsController : ApiController
    {
        private readonly IAvailabilityService FlightAvailabilityService;
        private readonly IValuationService FlightValuationService;

        //public FlightsController()
        //{
        //    //this.FlightAvailabilityService = FlightAvailabilityService;
        //    //this.FlightValuationService = FlightValuationService;
        //}

        public FlightsController(IAvailabilityService FlightAvailabilityService, IValuationService FlightValuationService)
        {
            this.FlightAvailabilityService = FlightAvailabilityService;
            this.FlightValuationService = FlightValuationService;
        }

        [Route("Search")]
        [HttpPost]
        public async Task<IHttpActionResult> Search(FlightAvailabilityRequest availabilityRequest)
        {            
            try
            {
                if (availabilityRequest != null)
                {
                    var searchId = FlightAvailabilityService.StartAvailabilitySearch(availabilityRequest);
                    if (searchId != Guid.Empty)
                        return Ok(searchId);
                    else
                        throw new Exception("No SearchId Returned");
                }
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }

            return BadRequest("Invalid Request");
        }

        [Route("SearchResults")]
        [HttpPost]
        public async Task<IHttpActionResult> SearchResults(FlightAvailabilityResultsRequest availabilityResultRequest)
        {
            try
            {
                if (availabilityResultRequest != null)
                {
                    if (availabilityResultRequest.searchId != Guid.Empty)
                    {

                        var Response = await FlightAvailabilityService.GetFlightAvailability(availabilityResultRequest.searchId, availabilityResultRequest.filterCriteria, availabilityResultRequest.sortCriteria);
                        if (Response != null)
                            return Ok(Response);
                        else
                            throw new Exception("No Response Returned");
                    }
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return BadRequest("Inavalid Request");
        }

        [Route("Valuate")]
        [HttpGet]
        public IHttpActionResult Valuated([FromUri]Guid availablityRequestId, [FromUri]Guid flightId)
        {
            try
            {

                if (availablityRequestId != Guid.Empty && flightId != Guid.Empty)
                {
                    FlightValuationRequest valuationRequest = new FlightValuationRequest
                    {
                        AvailabilityRequestId = availablityRequestId,
                        SelectedFlightId = flightId
                    };
                    var response = FlightValuationService.StartFlightValuation(valuationRequest);
                    if (response != null)
                        return Ok(response);
                    else
                        throw new Exception("No Response Returned");
                }

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return BadRequest("Inavalid Request");
        }



        [Route("ValuationResults")]
        [HttpGet]
        public async Task<IHttpActionResult> ValuationResults([FromUri]Guid valuationId)
        {
            try
            {
                if (valuationId != Guid.Empty)
                {


                    var Response = await FlightValuationService.GetValuationResponse(valuationId);


                    if (Response != null)
                        return Ok(Response);
                    else
                        throw new Exception("No Response Returned");

                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return BadRequest("Inavalid Request");
        }
    }
}
