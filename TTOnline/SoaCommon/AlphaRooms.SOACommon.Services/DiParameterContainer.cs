﻿using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Services
{
    public class DiParameterContainer : IDiParameterContainer
    {
        private static readonly ConcurrentDictionary<object, object> Container = new ConcurrentDictionary<object, object>();

        public void Register<T>(object key, T value)
        {
            Container.TryAdd(key, value);
        }

        public T GetParameterValue<T>(object key)
        {
            object value;
            Container.TryGetValue(key, out value);
            return (T)value;
        }

        public void Unregister(object key)
        {
            object value;
            Container.TryRemove(key, out value);
        }
    }
}
