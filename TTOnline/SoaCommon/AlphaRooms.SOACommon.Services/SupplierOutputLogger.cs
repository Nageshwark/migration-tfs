﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SoaCommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.Utilities;
using AlphaRooms.Utilities.ExtensionMethods;
using Ninject.Extensions.Logging;

namespace AlphaRooms.SOACommon.Services
{
    public class SupplierOutputLogger : ISupplierOutputLogger
    {
        private readonly ISupplierOutputLoggerSettings settings;
        private readonly ILogger logger;
        
        public SupplierOutputLogger(ISupplierOutputLoggerSettings settings, ILogger logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        public void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, string departureAirportCode, string destinationAirportCode, DateTime departureDate, SupplierOutputFormats format, object response)
        {
            LogAvailabilityResponse(supplier, searchId, departureAirportCode, destinationAirportCode, departureDate, null, format, response);
        }

        public void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, string departureAirportCode, string destinationAirportCode, DateTime departureDate, string processName, SupplierOutputFormats format, object response)
        {
            if (!string.IsNullOrEmpty(settings.SupplierOutputPath))
            {
                try
                {
                    if (!Directory.Exists(Path.Combine(settings.SupplierOutputPath, searchId.ToString())))
                    {
                        Directory.CreateDirectory(Path.Combine(settings.SupplierOutputPath, searchId.ToString()));
                        File.WriteAllText(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), departureAirportCode + "_ " + destinationAirportCode + "_ " + departureDate.ToString("ddMMMyyyy") + ".txt"), "");
                    }
                    if (!Directory.Exists(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), supplier.Name)))
                    {
                        Directory.CreateDirectory(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), supplier.Name));
                    }
                    File.WriteAllText(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), supplier.Name, supplier.Code + "_Availability_" + departureAirportCode + "_" + destinationAirportCode
                        + "_ " + departureDate.Day.ToString("00") + "_" + departureDate.Month.ToString("00") + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response));
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier availability output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        public void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, RequestAirport[] requestDepartureAirport, RequestAirport[] requestDestinationAirport, DateTime departureDate
            , SupplierOutputFormats format, object response)
        {
            LogAvailabilityResponse(supplier, searchId, requestDepartureAirport, requestDestinationAirport, departureDate, null, format, response);
        }

        public void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, RequestAirport[] requestDepartureAirport, RequestAirport[] requestDestinationAirport, DateTime departureDate
            , string processName, SupplierOutputFormats format, object response)
        {
            if (!string.IsNullOrEmpty(settings.SupplierOutputPath))
            {
                LogAvailabilityResponse(supplier, searchId, requestDepartureAirport.Select(i => i.AirportCode).Aggregate((i, j) => i + j)
                    , requestDestinationAirport.Select(i => i.AirportCode).Aggregate((i, j) => i + j), departureDate, processName, format, response);
            }
        }

        public void LogResponse(ISupplierBase supplier, ServiceTypes serviceType, Guid searchId, Guid valuationId, SupplierOutputFormats format, object response)
        {
            LogResponse(supplier, serviceType, searchId, valuationId, null, format, response);
        }

        public void LogResponse(ISupplierBase supplier, ServiceTypes serviceType, Guid searchId, Guid valuationId, string processName, SupplierOutputFormats format, object response)
        {
            if (!string.IsNullOrEmpty(settings.SupplierOutputPath))
            {
                try
                {
                    if (!Directory.Exists(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), valuationId.ToString())))
                    {
                        Directory.CreateDirectory(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), valuationId.ToString()));
                    }
                    File.WriteAllText(Path.Combine(settings.SupplierOutputPath, searchId.ToString(), valuationId.ToString(), supplier.Code + "_" + ((int)serviceType).ToString() + serviceType.ToString()
                        + (processName != null ? "_" + processName : null) + "." + format.ToString().ToLower()), ObjectToString(response));
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to log supplier output with exceptions " + ex.GetDetailedMessageWithInnerExceptions());
                }
            }
        }

        private string ObjectToString(object response)
        {
            string responseString = response as string;
            if (responseString != null) return responseString;
            return response.XmlSerialize(response.GetType());
        }
    }
}
