﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.Utilities;

namespace AlphaRooms.SOACommon.Services.Factories
{
    public class PromotionalDiscountFactory : IPromotionalDiscountFactory
    {

        public string GetPromotionDisplayText(PromotionalDiscount promo, Money money)
        {
            var message = new StringBuilder();


            if (promo.DiscountType == DiscountType.Percentage)
            {
                message.AppendLine($"{promo.Amount}%");
                return message.ToString();
            }

            if (promo.DiscountType == DiscountType.FixedRepeatable && promo.Threshold.HasValue)
            {
                //message.AppendLine($"{Math.Abs(money.Amount)}{money.CurrencyCode.GetCurrencySymbol()} every {promo.Threshold.Value}{money.CurrencyCode.GetCurrencySymbol()} ");
                message.AppendLine($"{FormatMoney(money)} every {FormatMoney(new Money(promo.Threshold.Value, money.CurrencyCode))} ");
                return message.ToString();
            }

            //message.AppendLine($"{Math.Abs(money.Amount)}{money.CurrencyCode.GetCurrencySymbol()}");
            message.AppendLine($"{FormatMoney(money)}");
            
            return message.ToString();
        }

        public string GetPromotionDisplayText(PromotionalDiscount promo)
        {
            if (promo.Channel == null) throw new ArgumentNullException(nameof(promo.Channel));
            return GetPromotionDisplayText(promo, new Money(promo.Amount, GetChannelDefaultCurrency(promo.Channel.Value)));
        }

        private string GetChannelDefaultCurrency(Channel channel)
        {
            switch (channel)
            {
                case Channel.AlphaRoomsIE:
                case Channel.BetaBedsIE:
                    return "EUR".GetCurrencySymbol();
                case Channel.AlphaRoomsUK:
                case Channel.BetaBedsUK:
                    return "GBP".GetCurrencySymbol();
                case Channel.AlphaRoomsUS:
                case Channel.BetaBedsUS:
                    return "USD".GetCurrencySymbol();
                case Channel.TeletextIE:
                    return "EUR".GetCurrencySymbol();
                default:
                    return "";
            }
        }

        private string GetDisplayTextByProduct(PromotionalDiscount promo)
        {
            var message = new StringBuilder();
            //Accommodation specific messages
            if (promo.AccommodationPromotion != null)
            {
                message.AppendLine(promo.AccommodationPromotion.ApplicationType.GetDescription());

                if (promo.AccommodationPromotion.Establishment != null)
                {
                    message.AppendLine("Discount applies to selected hotels");
                }
            }

            //Other messages

            return message.ToString();
        }

        private string FormatMoney(Money money)
        {
            if (money.CurrencyCode == null)
                return null;

            NumberFormatInfo numberFormat = money.CurrencyCode.ToNumberFormat();

            if (numberFormat != null)
            {
                return string.Format(numberFormat, "{0:C}", Math.Abs(money.Amount));
            }

            return money.CurrencyCode + Math.Abs(money.Amount);
        }
    }

    public interface IPromotionalDiscountFactory
    {
        string GetPromotionDisplayText(PromotionalDiscount promo, Money money);
        string GetPromotionDisplayText(PromotionalDiscount promo);
    }
}
