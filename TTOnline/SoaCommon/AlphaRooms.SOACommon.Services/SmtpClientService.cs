﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Services
{
    public class SmtpClientService : ISmtpClientService
    {
        private readonly ISmtpServerSettings emailSettings;

        public SmtpClientService(ISmtpServerSettings emailSettings)
        {
            if (emailSettings == null)
            {
                throw new ArgumentNullException("EmailServerSettings");
            }

            this.emailSettings = emailSettings;
        }

        public bool SendEmail(MailMessage message)
        {
            using (var smtpClient = this.GetSmtpClient())
            {
                smtpClient.Send( message);
            }

            return true;
        }

        private SmtpClient GetSmtpClient()
        {
            var smtpClient = this.emailSettings.SmtpPort > 0
                ? new SmtpClient(this.emailSettings.SmtpHost, this.emailSettings.SmtpPort)
                : new SmtpClient(this.emailSettings.SmtpHost);

            if (!string.IsNullOrEmpty(this.emailSettings.SmtpUsername))
            {
                smtpClient.Credentials = new NetworkCredential(this.emailSettings.SmtpUsername, this.emailSettings.SmtpPassword);
            }

            smtpClient.EnableSsl = this.emailSettings.SmtpIsTlsOn;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            return smtpClient;
        }
    }
}
