﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Services
{
    public class BookingLoggerService : IBookingLoggerService
    {
        private readonly IBookingLogRepository repository;

        public BookingLoggerService(IBookingLogRepository repository)
        {
            this.repository = repository;
        }

        public async Task<bool> Log(BookingLogItem logItem)
        {
           return await repository.WriteLog(logItem);
        }

        public async Task<BookingLogItem> ReadLog(Guid bookingId, int iSequence)
        {
            return await repository.ReadLogbyBookingId(bookingId, iSequence);
        }
    }
}
