﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;

namespace AlphaRooms.SOACommon.Services
{
    public class ItineraryService : IItineraryService
    {
        private readonly IItineraryRepository repository;

		public ItineraryService(IItineraryRepository repository)
        {
            this.repository = repository;
        }

        public Task<List<Passenger>> GetItineraryPersonByItineraryId(int itineraryId)
        {
            return this.repository.GetItineraryPersonByItineraryId(itineraryId);
        }
    }
}
