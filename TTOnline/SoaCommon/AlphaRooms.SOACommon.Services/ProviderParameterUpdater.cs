﻿using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Services
{
    public class SupplierParameterUpdater<T> : IProviderParameterUpdater<T>
    {
        public T UpdateSettings(T supplier, Func<T, IEnumerable<IProviderParameter>> property)
        {
            foreach (var parameter in property.Invoke(supplier))
            {
                string value = ConfigurationManager.AppSettings[parameter.ParameterName];
                if (value != null) parameter.ParameterValue = value;
            }
            return supplier;
        }

        public T[] UpdateSettings(T[] suppliers, Func<T, IEnumerable<IProviderParameter>> property)
        {
            foreach (T supplier in suppliers)
            {
                this.UpdateSettings(supplier, property);
            }
            return suppliers;
        }
    }
}
