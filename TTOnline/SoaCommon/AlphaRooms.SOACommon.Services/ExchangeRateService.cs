﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        private IExchangeRateRepository repository;

        public ExchangeRateService(IExchangeRateRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Money> ConvertCurrencyAsync(Money amount, string currencyTo)
        {
            if (amount.CurrencyCode == currencyTo) return amount;
            var rate = await repository.GetExchangeRateAsync(amount.CurrencyCode, currencyTo);
            return new Money(amount.Amount * (decimal)rate.Rate, currencyTo);
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(string fromCurrency, string toCurrency)
        {
            return await repository.GetExchangeRateAsync(fromCurrency, toCurrency);
        }
    }
}
