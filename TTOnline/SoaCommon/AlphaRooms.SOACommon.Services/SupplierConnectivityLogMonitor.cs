﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Services
{
    public class SupplierConnectivityLogMonitor : ISupplierConnectivityLogMonitor
    {
        private readonly string hostName;
        private readonly SupplierTypes supplierType;
        private readonly int supplierId;
        private readonly ServiceTypes serviceType;
        private readonly Action<ISupplierConnectivityLogMonitor> callBackMethod;
        private DateTime? startDate;
        private DateTime? stopDate;
        private bool? isServiceSuccessful;
        private Exception serviceException;
        private bool isCompleted;

        internal SupplierConnectivityLogMonitor(string hostName, SupplierTypes supplierType, int supplierId, ServiceTypes serviceType, Action<ISupplierConnectivityLogMonitor> callBackMethod)
        {
            this.hostName = hostName;
            this.supplierType = supplierType;
            this.supplierId = supplierId;
            this.serviceType = serviceType;
            this.callBackMethod = callBackMethod;
        }

        public void Start()
        {
            this.startDate = DateTime.Now;
        }
        
        public void Stop()
        {
            this.StopProcess(true, null);
        }

        public void StopWithException(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");
            this.StopProcess(false, exception);
        }

        private void StopProcess(bool isSuccessful, Exception exception)
        {
            if (this.startDate == null) throw new Exception("Cannot stop when monitor is not running");
            this.stopDate = DateTime.Now;
            this.isServiceSuccessful = isSuccessful;
            this.serviceException = exception;
            this.isCompleted = true;
            this.callBackMethod.Invoke(this);
        }

        public string HostName
        {
            get { return this.hostName; }
        }

        public SupplierTypes SupplierType
        {
            get { return this.supplierType; }
        }

        public int SupplierId
        {
            get { return this.supplierId; }
        }

        public ServiceTypes ServiceType
        {
            get { return this.serviceType; }
        }

        public bool IsCompleted 
        {
            get { return this.isCompleted; }
        }

        public bool? IsServiceSuccessful
        {
            get { return this.isServiceSuccessful; }
        }

        public Exception ServiceException
        {
            get { return this.serviceException; }
        }

        public DateTime? StartDate
        {
            get { return this.startDate; }
        }

        public DateTime? StopDate
        {
            get { return this.stopDate; }
        }
    }
}
