﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Repositories;

namespace AlphaRooms.SOACommon.Services
{
    public class ChannelInfoService : IChannelInfoService
    {
        private readonly IChannelInfoRepository repository;

        public ChannelInfoService(IChannelInfoRepository repository)
        {
            this.repository = repository;
        }

        public Task<ChannelInfo> GetChannelInfo(Channel channel)
        {
            return repository.GetChannelInfo(channel);
        }

        public Task<ChannelInfo> GetChannelInfoByLocaleIdAsync(int localeId)
        {
            return repository.GetChannelInfoByLocaleId(localeId);
        }

        public Task<ChannelInfo> GetChannelInfoBySiteAndLanguageCodeAsync(Sites site, string languageCode)
        {
            return repository.GetChannelInfoBySiteAndLanguageCodeAsync(site, languageCode.Substring(0, 2), languageCode.Substring(3, 2));
        }
    }
}
