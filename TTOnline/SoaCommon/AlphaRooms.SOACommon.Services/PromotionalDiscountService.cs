﻿
using System;
using System.Text.RegularExpressions;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Repositories;
using AlphaRooms.SOACommon.Services.Factories;
using Ninject.Extensions.Logging;

namespace AlphaRooms.SOACommon.Services
{
    public abstract class PromotionalDiscountService : IPromotionalDiscountService
    {
        private readonly IPromotionalDiscountRepository repository;
        private readonly IPromotionalDiscountFactory factory;
        private readonly ILogger logger;

        protected PromotionalDiscountService(IPromotionalDiscountRepository repository, IPromotionalDiscountFactory factory, ILogger logger)
        {
            this.repository = repository;
            this.factory = factory;
            this.logger = logger;
        }
        
    #region Private
        private PromoCodeResponseStatus GetPromoCodeStatus(PromotionalDiscount promotionalDiscount)
        {
            if (IsPromoCodeNull(promotionalDiscount))
            {
                return PromoCodeResponseStatus.DoesNotExist;
            }

            if (!IsPromoCodeActive(promotionalDiscount))
            {
                return PromoCodeResponseStatus.Inactive;
            }

            if (!HasPromoCodeStarted(promotionalDiscount))
            {
                return PromoCodeResponseStatus.NotYetStarted;
            }

            if (IsPromoCodeExpired(promotionalDiscount))
            {
                return PromoCodeResponseStatus.Expired;
            }

            if (IsPromoCodeAlreadyClaimed(promotionalDiscount))
            {
                return PromoCodeResponseStatus.AlreadyClaimed;
            }

            return PromoCodeResponseStatus.Valid;
        }

        private bool IsPromoCodeNull(PromotionalDiscount promotionalDiscount)
        {
            return promotionalDiscount == null;
        }

        private bool IsPromoCodeActive(PromotionalDiscount promotionalDiscount)
        {
            return promotionalDiscount.Active;
        }

        private bool HasPromoCodeStarted(PromotionalDiscount promotionalDiscount)
        {
            return !(DateTime.Now.Date < promotionalDiscount.StartDate.Date);
        }

        private bool IsPromoCodeExpired(PromotionalDiscount promotionalDiscount)
        {
            return DateTime.Now.Date > promotionalDiscount.EndDate.Date;
        }

        private bool IsPromoCodeAlreadyClaimed(PromotionalDiscount promotionalDiscount)
        {
            return promotionalDiscount.ActualUses >= promotionalDiscount.MaxUses;
        }

        private Money CalculateFixedDiscount(Money currentPrice, decimal discount)
        {
            return new Money()
            {
                CurrencyCode = currentPrice.CurrencyCode,
                Amount = -discount
            };
        }

        private Money CalculatePercentageDiscount(Money currentPrice, decimal percentage)
        {
            return new Money()
            {
                CurrencyCode = currentPrice.CurrencyCode,
                Amount = Math.Round(-currentPrice.Amount * percentage / 100, 2)
            };
        }
    #endregion
        
        public PromotionalDiscountResult GetDiscount(Money amount, decimal promoAmount, PromotionalDiscount promotionalDiscount)
        {
            var promodescription = string.Empty;
            var costToCustomer = new Money();

            switch (promotionalDiscount.DiscountType)
            {
                case DiscountType.Fixed:
                    costToCustomer = CalculateFixedDiscount(amount, promoAmount);
                    promodescription = factory.GetPromotionDisplayText(promotionalDiscount, costToCustomer);
                    break;
                case DiscountType.FixedRepeatable:
                    var newPromoAmount = promoAmount * Math.Truncate(amount.Amount / promotionalDiscount.Threshold.Value);
                    costToCustomer = CalculateFixedDiscount(amount, newPromoAmount);
                    promodescription = factory.GetPromotionDisplayText(promotionalDiscount);
                    break;
                case DiscountType.Percentage:
                    costToCustomer = CalculatePercentageDiscount(amount, promoAmount);
                    promodescription = factory.GetPromotionDisplayText(promotionalDiscount, costToCustomer);
                    break;
            }

            return new PromotionalDiscountResult
            {
                Discount = costToCustomer,
                Description = promodescription,
                Voucher = promotionalDiscount.Voucher,
                DiscountType = promotionalDiscount.AccommodationPromotion.ApplicationType
            };
        }

        public bool IsAboveMinThreshold(PromotionalDiscount promotionalDiscount, Money salePrice)
        {
            return promotionalDiscount.Threshold == null || promotionalDiscount.Threshold <= salePrice.Amount;
        }

        public PromoCodeResponse GetPromoCode(string voucherCode, Channel? channel)
        {
            var promoCode = repository.GetPromoCode(voucherCode);

            if (channel != null) promoCode.Channel = (Channel)channel;

            return new PromoCodeResponse
            {
                Status = GetPromoCodeStatus(promoCode),
                PromotionalDiscount = promoCode
            };
        }
    }

    public class PromoCodeResponse
    {
        public PromotionalDiscount PromotionalDiscount { get; set; }
        public PromoCodeResponseStatus Status { get; set; }
    }

    public enum PromoCodeResponseStatus
    {
        Valid,
        DoesNotExist,
        Inactive,
        NotYetStarted,
        Expired,
        AlreadyClaimed
    }
}
