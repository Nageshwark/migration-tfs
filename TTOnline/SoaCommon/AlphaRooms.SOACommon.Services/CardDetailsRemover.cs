﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Services
{
    public class CardDetailsRemover : ICardDetailsRemover
    {
        public string BlankCreditCardDetails(string value, CardDetails cardDetails)
        {
            if (String.IsNullOrEmpty(value)) return value;
            StringBuilder builder = new StringBuilder(value);
            ObscureCardNumber(value, builder, cardDetails.CardNumber);
            ObscureCardCVC(value, builder, cardDetails.Cvc);
            return builder.ToString();
        }

        private void ObscureCardNumber(string value, StringBuilder builder, string cardNumber) 
        {
            MatchCollection matches = Regex.Matches(value, CreateCardNumberPattern(cardNumber));
            foreach(Match match in matches) 
            {
                ObscureText(builder, match.Index, 12);
            }
        }

        private void ObscureCardCVC(string value, StringBuilder builder, string cvc)
        {
            MatchCollection matches = Regex.Matches(value, '(' + cvc + ')');
            foreach (Match match in matches)
            {
                ObscureText(builder, match.Index, 3);
            }
        }

        private void ObscureText(StringBuilder builder, int index, int length)
        {
            length += index;
            while(index < length) 
            {
                builder[index] = '*';
                index++;
            }
        }

        private string CreateCardNumberPattern(string cardNumber)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("(");
            foreach (char n in cardNumber)
            {
                builder.Append(n);
                builder.Append(@"[ .;:\-|]*");
            }
            builder.Append(")");
            return builder.ToString();
        }
    }
}
