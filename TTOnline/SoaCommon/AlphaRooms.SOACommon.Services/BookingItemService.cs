﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;

namespace AlphaRooms.SOACommon.Services
{
    public class BookingItemService : IBookingItemService
    {
        private readonly IBookingItemRepository repository;

        public BookingItemService(IBookingItemRepository repository)
        {
            this.repository = repository;
        }

        public Task<List<ChargeItem>> GetItineraryBookingItems(int itineraryId)
        {
            return this.repository.GetItineraryBookingItems(itineraryId);
        }

        public List<Money> GetSupplierGross()
        {
            return this.repository.GetSupplierGross();
        }
    }
}
