﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Interfaces.Services;

namespace AlphaRooms.SOACommon.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository repository;

        public CustomerService(ICustomerRepository repository)
        {
            this.repository = repository;
        }

        public Task<PassengerContactDetails> GetCustomerByItineraryId(int id)
        {
            return this.repository.GetCustomerByItineraryId(id);
        }
    }
}
