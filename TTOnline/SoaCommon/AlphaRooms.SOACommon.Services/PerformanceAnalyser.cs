﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject.Extensions.Logging;

namespace AlphaRooms.SOACommon.Services
{
    public class PerformanceAnalyser : IDisposable
    {
        private readonly string description;
        private readonly ILogger logger;
        private readonly SupplierTypes supplierType;
        private readonly PerformanceTimerType timerType;
        private readonly string searchId;

        private Stopwatch timer;
        private bool isStopped;
        private bool isRunning;

        public PerformanceAnalyser(SupplierTypes supplierType, PerformanceTimerType timerType, string searchId, string description, bool activate, ILogger logger)
        {
            this.supplierType = supplierType;
            this.timerType = timerType;
            this.searchId = searchId;
            this.description = description;
            this.logger = logger;

            if (activate && logger != null)
            {
                Start();
            }
        }

        public void Start()
        {
            if (isRunning)
            {
                return;
            }

            timer = new Stopwatch();
            timer.Start();
            
            isRunning = true;
            isStopped = false;
        }

        public void Stop()
        {
            if (!isRunning)
            {
                return;
            }
            timer.Stop();
            isRunning = false;

            //logging code
            Task.Run(() => logger.Debug("[{0}]: {1}, {2} for search id {3} took {4} ms.", supplierType.ToString(), timerType.ToString(), description, searchId, timer.ElapsedMilliseconds));
        }

        public void Dispose()
        {
            if (!isStopped && isRunning)
            {
                isStopped = true;
                Stop();
            }
        }
    }
}
