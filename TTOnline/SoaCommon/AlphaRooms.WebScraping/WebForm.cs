﻿namespace AlphaRooms.WebScraping
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Web;

    using HtmlAgilityPack;
    using AlphaRooms.Utilities;

    /// <summary>
    /// Helper class for creating fake form posts.
    /// </summary>
    /// <remarks></remarks>
    public class WebForm : NameValueCollection
    {

        #region "Constructors"

        public WebForm()
        {
        }

        public WebForm(NameValueCollection clone)
            : base(clone)
        {
        }

        #endregion

        #region "Static Methods"
        public static WebForm FromHtmlNode(HtmlNode node)
        {

            var formPost = new WebForm();

            var htmlInputs = node.SelectNodes("//input");
            if (htmlInputs != null)
            {
                foreach (var htmlInput in htmlInputs)
                {
                    var htmlNameAttribute = htmlInput.Attributes["name"];
                    var name = "";
                    if (htmlNameAttribute == null)
                    {
                        if(htmlInput.Attributes["id"] != null) 
                            name = htmlInput.Attributes["id"].Value;
                    }
                    else
                    {
                        name = htmlNameAttribute.Value;
                    }
                    var value = "";
                    if (htmlInput.Attributes["value"] != null)
                        value = htmlInput.Attributes["value"].Value;
                    formPost.Add(name, value);
                }
            }

            htmlInputs = node.SelectNodes("//select");
            if (htmlInputs != null)
            {
                foreach (HtmlNode htmlInput in htmlInputs)
                {
                    if (!htmlInput.HasAttributes) continue;

                    var htmlNameAttribute = htmlInput.Attributes["name"];
                    var name = "";
                    if (htmlNameAttribute == null)
                    {
                        var idAttribute = htmlInput.Attributes["id"];

                        if (idAttribute == null) continue;

                        name = idAttribute.Value;
                    }
                    else
                    {
                        name = htmlNameAttribute.Value;
                    }

                    var value = "";
                    var htmlSelectedOptions = htmlInput.SelectNodes("option[@selected]");
                    //Choose last selected option as this is how browsers seem to operate.
                    if (htmlSelectedOptions != null && htmlSelectedOptions.Count > 0)
                    {
                        value = htmlSelectedOptions[htmlSelectedOptions.Count - 1].Attributes["value"].Value;
                    }
                    formPost.Add(name, value);
                }
            }

            return formPost;

        }
        #endregion

        #region "Public Methods"

        /// <summary>
        /// Remove all form post parameters except parameter with given name.
        /// </summary>
        /// <param name="exception">Name of the parameter to leave in the form post.</param>
        /// <remarks></remarks>
        public void ClearExcept(string exception)
        {
            this.ClearExcept(new string[] { exception });
        }

        /// <summary>
        /// Remove all form post parameters except parameters with given names.
        /// </summary>
        /// <param name="exceptions">Name of the parameters to leave in the form post.</param>
        /// <remarks></remarks>
        public void ClearExcept(string[] exceptions)
        {
            var objToRemove = new ArrayList();

            foreach (var key in this.AllKeys)
            {
                foreach (var sException in exceptions)
                {
                    if (key.ToLower() == sException.ToLower())
                    {
                        if (!objToRemove.Contains(key))
                        {
                            objToRemove.Add(key);
                        }
                    }
                }
            }

            foreach (string s in objToRemove)
            {
                this.Remove(s);
            }

        }

        /// <summary>
        /// Add a parameter to the form post.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <remarks></remarks>
        public override void Add(string name, string value)
        {
            if (this[name] != null)
            {
                this[name] = value;
            }
            else
            {
                base.Add(name, value);
            }
        }

        /// <summary>
        /// Get the querystring.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string ToString()
        {
            var parts = new string[this.Count];
            var keys = this.AllKeys;

            for (var i = 0; i <= keys.Length - 1; i++)
            {
                parts[i] = keys[i] + "=" + HttpUtility.UrlEncode(this[keys[i]]);
            }

            var formpost = String.Join("&", parts);
            formpost = formpost.Replace("!", "%21");

            return formpost;
        }
        
        #endregion

    }

}