﻿using System;
using System.Threading.Tasks;
using AlphaRooms.Utilities.CustomExceptions;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;

namespace AlphaRooms.WebScraping
{
    public class WebScrapeClient : IWebScrapeClient
    {
        private WebScraperCookies _cookies;
        public bool AllowAutoRedirect { get; set; }
        public bool UseCompression { get; set; }
        public IWebProxy Proxy { get; set; }
        public string Referer { get; set; }
        public WebContentType ContentType { get; set; }
        public bool KeepAlive { get; set; }

        public WebScrapeClient()
        {
            this.AllowAutoRedirect = true;
            this._cookies = new WebScraperCookies();
            this.ContentType = WebContentType.Default;
        }

        public WebScrapeResponse Get(string url)
        {
            return this.GetResponseSync(url, "", "GET", true, null);
        }

        public async Task<WebScrapeResponse> GetAsync(string url)
        {
            return await this.GetResponse(url, "", "GET", true, null);
        }

        public async Task<WebScrapeResponse> GetAsync(string url, WebHeaderCollection header)
        {
            return await this.GetResponse(url, "", "GET", true, header);
        }

        public WebScrapeResponse Post(string url, string body)
        {
            return this.GetResponseSync(url, body, "POST", true, null);
        }

        public async Task<WebScrapeResponse> PostAsync(string url, string body)
        {
            return await this.GetResponse(url, body, "POST", true, null);
        }

        public async Task<WebScrapeResponse> PostAsync(string url, string body, WebHeaderCollection header)
        {
            return await this.GetResponse(url, body, "POST", true, header);
        }

        public async Task<WebScrapeResponse> XTGPostAsync(string url, string body, WebHeaderCollection header)
        {
            return await this.GetXTGResponse(url, body, "POST", true, header);
        }

        public async Task<WebScrapeResponse> PostAsync(string url, string body, bool expect100Continue)
        {
            return await this.GetResponse(url, body, "POST", expect100Continue, null);
        }

        public async Task<WebScrapeResponse> PostAsync(string url, string body, WebHeaderCollection header, bool expect100Continue)
        {
            return await this.GetResponse(url, body, "POST", expect100Continue, header);
        }

        public WebScrapeResponse GetImage(string url)
        {
            return this.GetImageResponse(url, "", "GET", true, null);
        }

        public WebScraperCookies Cookies
        {
            get
            {
                return this._cookies;
            }
            set
            {
                this._cookies = value;
            }
        }

        private WebScrapeResponse GetResponseSync(string url, string body, string method, bool expect100Continue, WebHeaderCollection headers)
        {
            try
            {
                var httpRequest = this.GetHttpWebRequest(url, body, method, expect100Continue, headers);

                httpRequest.ServicePoint.ConnectionLimit = 100;

                var responseString = string.Empty;

                using (var response = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (var receiveStream = response.GetResponseStream())
                    {
                        var characterSet = "utf-8";

                        if (!string.IsNullOrEmpty(response.CharacterSet))
                        {
                            characterSet = response.CharacterSet.Replace("\"", "");
                        }

                        var encode = Encoding.GetEncoding(characterSet);

                        using (var reader = new StreamReader(receiveStream, encode))
                        {
                            responseString = reader.ReadToEnd();
                        }
                    }

                    this._cookies = GetUpdatedCookies(response, this._cookies);

                    return new WebScrapeResponse
                    {
                        Value = responseString,
                        Headers = response.Headers,
                        Status = response.StatusCode,
                        Uri = response.ResponseUri
                    };
                }
            }
            catch (Exception ex)
            {
                throw new SupplierApiException("Exception on Supplier Api call -" + url + " Request - " + body, ex);
            }
        }

        private async Task<WebScrapeResponse> GetXTGResponse(string url, string body, string method, bool expect100Continue,
                                             WebHeaderCollection headers)
        {
           
            try
            {
                var httpRequest = this.GetHttpWebRequest(url, body, method, expect100Continue, headers);

                httpRequest.ServicePoint.ConnectionLimit = 100;

                var responseString = string.Empty;

                using (var response = (HttpWebResponse)await httpRequest.GetResponseAsync())
                {
                    using (var receiveStream = response.GetResponseStream())
                    {
                        var characterSet = "utf-8";

                        if (!string.IsNullOrEmpty(response.CharacterSet))
                        {
                            characterSet = response.CharacterSet.Replace("\"", "");
                        }

                        var encode = Encoding.GetEncoding(characterSet);

                        using (var reader = new StreamReader(receiveStream, encode))
                        {
                            responseString = await reader.ReadToEndAsync();
                        }
                    }

                    this._cookies = GetUpdatedCookies(response, this._cookies);

                    return new WebScrapeResponse
                    {
                        Value = responseString,
                        Headers = response.Headers,
                        Status = response.StatusCode,
                        Uri = response.ResponseUri
                    };
                }
            }
            catch (WebException ex)
            {
                var _message = string.Empty;
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    _message = reader.ReadToEnd();
                   
                }  
                 throw new SupplierApiException(_message.ToString());
            }          
        }

        private async Task<WebScrapeResponse> GetResponse(string url, string body, string method, bool expect100Continue,
                                              WebHeaderCollection headers)
        {

            try
            {
                var httpRequest = this.GetHttpWebRequest(url, body, method, expect100Continue, headers);

                httpRequest.ServicePoint.ConnectionLimit = 100;

                var responseString = string.Empty;

                using (var response = (HttpWebResponse)await httpRequest.GetResponseAsync())
                {
                    using (var receiveStream = response.GetResponseStream())
                    {
                        var characterSet = "utf-8";

                        if (!string.IsNullOrEmpty(response.CharacterSet))
                        {
                            characterSet = response.CharacterSet.Replace("\"", "");
                        }

                        var encode = Encoding.GetEncoding(characterSet);

                        using (var reader = new StreamReader(receiveStream, encode))
                        {
                            responseString = await reader.ReadToEndAsync();
                        }
                    }

                    this._cookies = GetUpdatedCookies(response, this._cookies);

                    return new WebScrapeResponse
                    {
                        Value = responseString,
                        Headers = response.Headers,
                        Status = response.StatusCode,
                        Uri = response.ResponseUri
                    };
                }
            }
            catch (Exception ex)
            {               
                throw new SupplierApiException("Exception on Supplier Api call -" + url + " Request - " + body, ex);
            }            
        }

        private WebScrapeResponse GetImageResponse(string url, string body, string method, bool expect100Continue, WebHeaderCollection headers)
        {
            var httpRequest = this.GetHttpWebRequest(url, body, method, expect100Continue, headers);

            httpRequest.ServicePoint.ConnectionLimit = 100;

            byte[] responseBytes = null;

            using (var response = (HttpWebResponse)httpRequest.GetResponse())
            {
                using (var reader = new BinaryReader(response.GetResponseStream()))
                {
                    responseBytes = reader.ReadBytes(1024 * 1024); // 1MB limit.
                }

                this._cookies = GetUpdatedCookies(response, this._cookies);

                return new WebScrapeResponse
                {
                    ImageValue = responseBytes,
                    Headers = response.Headers,
                    Status = response.StatusCode,
                    Uri = response.ResponseUri
                };
            }
        }

        private HttpWebRequest GetHttpWebRequest(string url, string body, string method, bool expect100Continue, WebHeaderCollection headers)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;

            if (this.Proxy != null) request.Proxy = this.Proxy;

            if (this.UseCompression)
            {
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.Headers.Add("Accept-Encoding", "gzip");
            }

            var cookieContainer = new CookieContainer();
            foreach (var cookie in this._cookies.Cast<Cookie>())
            {
                cookieContainer.Add(cookie);
            }
            request.CookieContainer = cookieContainer;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    switch (header.ToString())
                    {
                        case "User-Agent":
                            request.UserAgent = headers[header.ToString()];
                            break;

                        case "Accept":
                            request.Accept = headers[header.ToString()];
                            break;

                        case "Host":
                            request.Host = headers[header.ToString()];
                            break;

                        case "Content-Type":
                            request.ContentType = headers[header.ToString()];
                            break;

                        default:
                            request.Headers.Add(header.ToString(), headers[header.ToString()]);
                            break;
                    }

                }
            }
            else
            {
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727;)";
                request.Headers.Add("Accept-Language", "en-gb");
            }

            request.ProtocolVersion = HttpVersion.Version11;
            request.AllowAutoRedirect = this.AllowAutoRedirect;
            request.KeepAlive = KeepAlive;

            if (KeepAlive)
            {
                var sp = request.ServicePoint;
                var prop = sp.GetType().GetProperty("HttpBehaviour", BindingFlags.Instance | BindingFlags.NonPublic);
                prop.SetValue(sp, (byte)0, null);
            }

            if (!string.IsNullOrEmpty(this.Referer))
                request.Referer = this.Referer;

            request.MaximumResponseHeadersLength = 800;

            if (method == "POST")
            {
                var encoding = new UTF8Encoding();
                var bytes = encoding.GetBytes(body);

                //request.ContinueTimeout = 30000;
                //request.ReadWriteTimeout = 30000;
                //request.Timeout = 30000;

                request.ServicePoint.Expect100Continue = expect100Continue;
                request.ContentType = GetContentType();
                request.ContentLength = bytes.Length;

                var stream = request.GetRequestStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Close();
            }

            return request;
        }

        private string GetContentType()
        {
            string result = "";
            switch (ContentType)
            {
                case WebContentType.Default:
                    result = "application/x-www-form-urlencoded";
                    break;
                case WebContentType.ApplicationJson:
                    result = "application/json";
                    break;
                case WebContentType.ApplicationXml:
                    result = "application/xml";
                    break;
                case WebContentType.TextXml:
                    result = "text/xml";
                    break;
            }
            return result;
        }

        private static WebScraperCookies GetUpdatedCookies(HttpWebResponse response, IEnumerable<Cookie> oldCookies)
        {
            var cookies = new WebScraperCookies();

            foreach (var cookie in response.Cookies.Cast<Cookie>())
            {
                cookies.Add(cookie);
            }

            foreach (var cookie in oldCookies.Where(cookie => cookies[cookie.Name] == null))
            {
                cookies.Add(cookie);
            }

            return cookies;
        }
    }
}