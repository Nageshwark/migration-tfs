using System.Threading.Tasks;
using System.Net;

namespace AlphaRooms.WebScraping
{
    public interface IWebScrapeClient
    {
        bool AllowAutoRedirect { get; set; }
        bool UseCompression { get; set; }
        string Referer { get; set; }
        bool KeepAlive { get; set; }
        WebContentType ContentType { get; set; }
        WebScraperCookies Cookies { get; set; }
        WebScrapeResponse Get(string url);
        Task<WebScrapeResponse> GetAsync(string url);
        WebScrapeResponse Post(string url, string body);
        Task<WebScrapeResponse> PostAsync(string url, string body);
        Task<WebScrapeResponse> PostAsync(string url, string body, bool expect100Continue);
        Task<WebScrapeResponse> PostAsync(string url, string body, WebHeaderCollection header);
        Task<WebScrapeResponse> XTGPostAsync(string url, string body, WebHeaderCollection header);
        Task<WebScrapeResponse> PostAsync(string url, string body, WebHeaderCollection header, bool expect100Continue);
       
    }
}