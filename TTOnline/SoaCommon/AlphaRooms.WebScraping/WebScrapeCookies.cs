﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.WebScraping
{
    public class WebScraperCookies : List<Cookie>
    {
        public Cookie this[string name]
        {
            get { return this.FirstOrDefault(i => string.Equals(i.Name, name, StringComparison.CurrentCultureIgnoreCase)); }
        }

        public void AddRangeFromParse(string value)
        {
            foreach (var cookie in value.Split(';'))
            {
                var cookieValue = cookie.Split(',');
                this.Add(new Cookie(cookieValue[0], cookieValue[1], cookieValue[2], cookieValue[3]));
            }
        }

        public void CopyToDomain(string path, string domain)
        {
            CopyFrom(this, path, domain);
        }

        public void CopyFrom(IEnumerable<Cookie> cookies, string path, string domain)
        {
            int count = cookies.Count();
            for (int i = 0; i < count; i++)
            {
                var cookie = cookies.ElementAt(i);
                this.Add(new Cookie(cookie.Name, cookie.Value, path, domain));
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            int count = this.Count;
            for (int i = 0; i < count; i++)
            {
                var cookie = this[i];
                builder.Append(cookie.Name);
                builder.Append(',');
                builder.Append(cookie.Value);
                builder.Append(',');
                builder.Append(cookie.Path);
                builder.Append(',');
                builder.Append(cookie.Domain);
                builder.Append(';');
            }
            if (builder.Length > 0) builder.Length--;
            return builder.ToString();
        }
    }
}
