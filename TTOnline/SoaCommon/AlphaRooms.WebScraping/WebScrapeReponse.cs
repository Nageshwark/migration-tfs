﻿namespace AlphaRooms.WebScraping
{
    using System;
    using System.Net;

    public class WebScrapeResponse
    { 
        public WebHeaderCollection Headers { get; set; }
        public string Value { get; set; }
        public byte[] ImageValue { get; set; }
        public HttpStatusCode Status { get; set; }
        public Uri Uri { get; set; } 
    }
}
