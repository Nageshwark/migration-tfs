﻿using HtmlAgilityPack;

namespace AlphaRooms.WebScraping
{
    public static class ExtensionMethods
    {
        public static HtmlDocument ToHtmlDocument(this string value)
        {
            var document = new HtmlDocument();
            document.LoadHtml(value);
            return document;
        }

        public static HtmlDocument ToHtmlDocument(this WebScrapeResponse response)
        {
            var document = new HtmlDocument();
            document.LoadHtml(response.Value);

            return document;
        }

        public static WebForm ToFormPost(this WebScrapeResponse response)
        {
            return WebForm.FromHtmlNode(response.ToHtmlDocument().DocumentNode);
        }

        public static WebForm ToFormPost(this HtmlDocument document)
        {
            return WebForm.FromHtmlNode(document.DocumentNode);
        }
    }
}
