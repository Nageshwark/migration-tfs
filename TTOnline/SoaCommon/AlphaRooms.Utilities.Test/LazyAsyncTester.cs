﻿﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.Test
{
    [TestClass]
    public class LazyAsyncTester
    {
        [TestMethod]
        public async Task ShouldCallObjectOnce()
        {
            int count = 0;
            LazyAsync<String> z = new LazyAsync<string>(() => { count++; return Task.FromResult<String>("Roby"); });

            Assert.AreEqual(0, count);

            string r = await z;

            Assert.AreEqual("Roby", r);
            Assert.AreEqual(1, count);

            string r2 = await z;

            Assert.AreEqual("Roby", r);
            Assert.AreEqual(1, count);
        }
    }
}
