﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AlphaRooms.Utilities.Test
{
    [TestClass]
    public class EnumExtensionsTester
    {
        private enum TestEnum
        {
            [System.ComponentModel.Description("Enum Item With Description")]
            ItemWithDescription = 1
            ,
            [System.ComponentModel.Description("Enum Item With Description2")]
            ItemWithDescription2 = 2
            ,
            ItemWithoutDescription = 3
        }

        [TestMethod]
        public void GetDescriptionShouldReturnStringForEnumWithDescription()
        {
            Assert.AreEqual("Enum Item With Description", TestEnum.ItemWithDescription.GetDescription());
        }

        [TestMethod]
        [ExpectedException(typeof(DescriptionNotFoundException))]
        public void GetDescriptionShouldThrowExceptionWhenThereIsNoDescription()
        {
            Assert.AreEqual(null, TestEnum.ItemWithoutDescription.GetDescription());
        }

        [TestMethod]
        public void GetDescriptionOrNullShouldReturnStringForEnumWithDescription()
        {
            Assert.AreEqual("Enum Item With Description", TestEnum.ItemWithDescription.GetDescriptionOrNull());
        }

        [TestMethod]
        public void GetDescriptionOrNullShouldReturnNullForEnumWithoutDescription()
        {
            Assert.AreEqual(null, TestEnum.ItemWithoutDescription.GetDescriptionOrNull());
        }

        [TestMethod]
        public void ToStringWithDescriptionShouldReturnStringForEnumWithDescription()
        {
            Assert.AreEqual("Enum Item With Description", TestEnum.ItemWithDescription.ToStringWithDescription());
        }

        [TestMethod]
        public void ToStringWithDescriptionShouldReturnEnumNameForEnumWithoutDescription()
        {
            Assert.AreEqual("ItemWithoutDescription", TestEnum.ItemWithoutDescription.ToStringWithDescription());
        }

        [TestMethod]
        public void ParseWithDescriptionShouldReturnEnumForDescription()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>("Enum Item With Description"));
        }

        [TestMethod]
        public void ParseWithDescriptionShouldReturnEnumForName()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>("ItemWithDescription"));
        }

        [TestMethod]
        public void ParseWithDescriptionShouldReturnEnumForValue()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>("1"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToStringWithDescriptionShouldThrowExceptionForNull()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>(null));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToStringWithDescriptionShouldThrowExceptionForEmptyString()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>(""));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ToStringWithDescriptionShouldThrowExceptionForInvalidString()
        {
            Assert.AreEqual(TestEnum.ItemWithDescription, EnumExtensions.ParseFromDescription<TestEnum>("Item"));
        }
    }
}
