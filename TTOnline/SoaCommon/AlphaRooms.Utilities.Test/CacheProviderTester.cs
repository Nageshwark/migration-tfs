﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.Test
{
    [TestClass]
    public class CacheProviderTester
    {
        private class Provider
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        private CacheProviderAsync<Provider> cache;
        private Provider[] providers;

        [TestInitialize]
        public void Initialize()
        {
            int length = 10; // 1000000;
            Provider[] providers = new Provider[length];
            for (int i = 0; i < length; i++)
            {
                providers[i] = new Provider() { Id = i + 1, Name = CreateRandomString() };
            }
            this.providers = providers;
            this.cache = new CacheProviderAsync<Provider>(true, TimeSpan.Parse("00:00:10"), TimeSpan.Parse("00:00:05"), TimeSpan.Parse("00:00:05"));
        }

        private string CreateRandomString()
        {
            Random rnd = new Random();
            int length = 255;
            char[] c = new char[length];
            for (int i = 0; i < length; i++)
            {
                c[i] = (char)rnd.Next('A', 'z');
            }
            return new string(c);
        }

        private TimeSpan WaitUntilAllExpire(CacheProviderAsync<Provider> cache)
        {
            //Debug.WriteLine("WaitUntilAllExpire ");
            DateTime start = DateTime.Now;
            do
            {
                Thread.Sleep(500);
            } while (cache.ContainsAll());
            return DateTime.Now - start;
        }

        private TimeSpan WaitUntilCustomKeyExpire(CacheProviderAsync<Provider> cache, string key)
        {
            //Debug.WriteLine("WaitUntilKeyExpire " + key);
            DateTime start = DateTime.Now;
            do
            {
                Thread.Sleep(500);
            } while (cache.ContainsCustomKey(key));
            return DateTime.Now - start;
        }
        private TimeSpan WaitUntilKeyExpire(CacheProviderAsync<Provider> cache, string key)
        {
            //Debug.WriteLine("WaitUntilKeyExpire " + key);
            DateTime start = DateTime.Now;
            do
            {
                Thread.Sleep(500);
            } while (cache.ContainsKey(key));
            return DateTime.Now - start;
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldReturnNewValue()
        {
            string key = "y";
            Provider value1 = providers[0];
            this.cache.InvalidateCustomKey(key);
            Provider result1 = await this.cache.GetOrSetAsync(key, async () => await Task.FromResult(value1));
            Assert.AreEqual(value1.Id, result1.Id);
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldReturnStoredValue()
        {
            string key = "y";
            Provider value1 = providers[0];
            Provider value2 = providers[1];
            this.cache.InvalidateCustomKey(key);
            Provider result1 = await this.cache.GetOrSetAsync(key, async () => await Task.FromResult(value1));
            Assert.AreEqual(value1.Id, result1.Id);
            Provider result2 = await this.cache.GetOrSetAsync(key, async () => await Task.FromResult(value2));
            Assert.AreEqual(value1.Id, result2.Id);
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldReturnNewValueAfterExpire()
        {
            string key = "y";
            Provider value1 = providers[0];
            Provider value2 = providers[1];
            this.cache.InvalidateCustomKey(key);
            DateTime p1 = DateTime.Now;
            Provider result1 = await this.cache.GetOrSetAsync(key, async () => await Task.FromResult(value1));
            DateTime p2 = DateTime.Now;
            Assert.AreEqual(value1.Id, result1.Id);
            this.WaitUntilCustomKeyExpire(cache, key);
            DateTime p3 = DateTime.Now;
            Provider result2 = await this.cache.GetOrSetAsync(key, async () => await Task.FromResult(value2));
            DateTime p4 = DateTime.Now;
            Assert.AreEqual(value2.Id, result2.Id);
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldNotCacheWhenCachingIsDisabled()
        {
            string key = "y";
            Provider value1 = providers[0];
            this.cache = new CacheProviderAsync<Provider>(false, TimeSpan.Parse("00:00:15"), TimeSpan.Parse("00:00:05"), TimeSpan.Parse("00:00:05"));
            this.cache.InvalidateCustomKey(key);
            int counter = 0;
            Provider result1 = await this.cache.GetOrSetAsync(key, async () => { counter++; return await Task.FromResult(value1); });
            Provider result2 = await this.cache.GetOrSetAsync(key, async () => { counter++; return await Task.FromResult(value1); });
            Assert.AreEqual(2, counter);
            Assert.AreEqual(value1.Id, result1.Id);
            Assert.AreEqual(value1.Id, result2.Id);
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldFailOnMultipleCalls()
        {
            string key = "y";
            int count = 0;
            Provider value1 = providers[0];
            this.cache.InvalidateCustomKey(key);
            Task<Provider>[] tasks = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 }.Select(i => this.cache.GetOrSetAsync("1", async () =>
            {
                count++;
                return await Task.FromResult(providers[0]);
            })).ToArray();
            await Task.WhenAll(tasks);
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public async Task GetOrSetAsyncShouldNotFailOnNullValues()
        {
            this.cache.InvalidateAll();
            var r = await this.cache.GetOrSetAsync("R", async () => await Task.FromResult((Provider)null));
            Assert.AreEqual(null, r);
        }

        [TestMethod]
        public async Task GetAllAsynclShouldReturnAllCachedItems()
        {
            this.cache.InvalidateAll();
            this.cache.Register(async () => await Task.FromResult(this.providers));
            Assert.AreEqual(this.providers.Length, (await this.cache.GetAllAsync()).Length);
        }

        [TestMethod]
        public async Task GetAllAsyncShouldNotRequestSource()
        {
            int counter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); });
            Assert.AreEqual(0, counter);
            Provider[] results1 = await this.cache.GetAllAsync();
            Assert.AreEqual(this.providers.Length, results1.Length);
            Assert.AreEqual(1, counter);
            Provider[] results2 = await this.cache.GetAllAsync();
            Assert.AreEqual(this.providers.Length, results2.Length);
            Assert.AreEqual(1, counter);
        }

        [TestMethod]
        public async Task GetAllAsyncShouldRequestSourceWhenExpired()
        {
            int counter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); });
            Provider[] results1 = await this.cache.GetAllAsync();
            Assert.AreEqual(this.providers.Length, results1.Length);
            this.WaitUntilAllExpire(cache);
            Provider[] results2 = await this.cache.GetAllAsync();
            Assert.AreEqual(this.providers.Length, results2.Length);
            Assert.AreEqual(2, counter);
        }

        [TestMethod]
        public async Task GetAllAsyncShouldNotCacheWhenCachingIsDisabled()
        {
            int counter = 0;
            this.cache = new CacheProviderAsync<Provider>(false, TimeSpan.Parse("00:00:15"), TimeSpan.Parse("00:00:05"), TimeSpan.Parse("00:00:05"));
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); });
            Provider[] results1 = await this.cache.GetAllAsync();
            Provider[] results2 = await this.cache.GetAllAsync();
            Assert.AreEqual(2, counter);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldReturnSpecificItem()
        {
            this.cache.InvalidateAll();
            this.cache.Register(async () => await Task.FromResult(this.providers)
                , async (i) => await Task.FromResult(this.providers[int.Parse(i)]), i => i.Id.ToString());
            Provider result = await this.cache.GetByKeyAsync("5");
            Assert.AreEqual(5, result.Id);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldNotFailOnNullValues()
        {
            this.cache.InvalidateAll();
            this.cache.Register(async () => await Task.FromResult(new Provider[0])
                , async (i) => await Task.FromResult((Provider)null), i => i.Id.ToString());
            Provider result = await this.cache.GetByKeyOrNullAsync("5");
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldNotRequestSource()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Provider result1 = await this.cache.GetByKeyAsync("5");
            Assert.AreEqual(1, counter);
            Assert.AreEqual(0, keyCounter);
            Provider result2 = await this.cache.GetByKeyAsync("5");
            Assert.AreEqual(1, counter);
            Assert.AreEqual(0, keyCounter);
            Assert.AreEqual(5, result1.Id);
            Assert.AreEqual(5, result2.Id);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldRequestSourceWhenExpired()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Provider result1 = await this.cache.GetByKeyAsync("5");
            this.WaitUntilKeyExpire(cache, "5");
            Provider result2 = await this.cache.GetByKeyAsync("5");
            Assert.AreEqual(2, counter);
            Assert.AreEqual(0, keyCounter);
            Assert.AreEqual(5, result1.Id);
            Assert.AreEqual(5, result2.Id);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldWaitAndGetReturnValidValue()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Task<Provider>[] tasks = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }.Select(i => this.cache.GetByKeyAsync((i + 1).ToString())).ToArray();
            Provider[] results = await Task<Provider>.WhenAll(tasks);
            Assert.AreEqual(1, counter);
            Assert.AreEqual(0, keyCounter);
            Assert.AreEqual(1, results[0].Id);
            Assert.AreEqual(2, results[1].Id);
            Assert.AreEqual(3, results[2].Id);
            Assert.AreEqual(4, results[3].Id);
            Assert.AreEqual(5, results[4].Id);
            Assert.AreEqual(6, results[5].Id);
            Assert.AreEqual(7, results[6].Id);
            Assert.AreEqual(8, results[7].Id);
            Assert.AreEqual(9, results[8].Id);
            Assert.AreEqual(10, results[9].Id);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldWaitUntilPopulateTimeoutAndRequestFromSourceItem()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; if (counter == 1) await Task.Delay(15000); return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i) - 1]); }, i => i.Id.ToString());
            Task<Provider>[] tasks = new int[] { 0, 1, 2 }.Select(i => this.cache.GetByKeyAsync((i + 1).ToString())).ToArray();
            Provider[] results = await Task<Provider>.WhenAll(tasks);
            Assert.AreEqual(1, counter);
            Assert.AreEqual(2, keyCounter);
            Assert.AreEqual(1, results[0].Id);
            Assert.AreEqual(2, results[1].Id);
            Assert.AreEqual(3, results[2].Id);
        }

        [TestMethod]
        public async Task GetByKeyOrNullAsyncShouldNotThrowExceptionWhenValueIsNull()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Provider result = await this.cache.GetByKeyOrNullAsync("100");
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public async Task GetByKeyAsyncShouldNotCacheWhenCachingIsDisabled()
        {
            int counter = 0, keyCounter = 0;
            this.cache = new CacheProviderAsync<Provider>(false, TimeSpan.Parse("00:00:15"), TimeSpan.Parse("00:00:05"), TimeSpan.Parse("00:00:05"));
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i) - 1]); }, i => i.Id.ToString());
            Task<Provider>[] tasks = new int[] { 0, 1, 2 }.Select(i => this.cache.GetByKeyAsync((1).ToString())).ToArray();
            Provider[] results = await Task<Provider>.WhenAll(tasks);
            Assert.AreEqual(0, counter);
            Assert.AreEqual(3, keyCounter);
            Assert.AreEqual(1, results[0].Id);
            Assert.AreEqual(1, results[1].Id);
            Assert.AreEqual(1, results[2].Id);
        }

        [TestMethod]
        public async Task GetByKeysAsyncShouldReturnSpecificItem()
        {
            this.cache.InvalidateAll();
            this.cache.Register(async () => await Task.FromResult(this.providers)
                , async (i) => await Task.FromResult(this.providers[int.Parse(i)]), i => i.Id.ToString());
            Provider[] results = await this.cache.GetByKeysAsync(new string[] { "4", "5" });
            Assert.AreEqual(4, results[0].Id);
            Assert.AreEqual(5, results[1].Id);
        }

        [TestMethod]
        public async Task GetByKeysAsyncShouldNotRequestSource()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Provider[] result1 = await this.cache.GetByKeysAsync(new string[] { "4", "5" });
            Assert.AreEqual(1, counter);
            Assert.AreEqual(0, keyCounter);
            Provider[] result2 = await this.cache.GetByKeysAsync(new string[] { "4", "5" });
            Assert.AreEqual(1, counter);
            Assert.AreEqual(0, keyCounter);
            Assert.AreEqual(4, result1[0].Id);
            Assert.AreEqual(5, result1[1].Id);
            Assert.AreEqual(4, result2[0].Id);
            Assert.AreEqual(5, result2[1].Id);
        }

        [TestMethod]
        public async Task GetByKeysAsyncShouldRequestSourceWhenExpired()
        {
            int counter = 0, keyCounter = 0;
            this.cache.InvalidateAll();
            this.cache.Register(async () => { counter++; return await Task.FromResult(this.providers); }
                , async (i) => { keyCounter++; return await Task.FromResult(this.providers[int.Parse(i)]); }, i => i.Id.ToString());
            Provider[] result1 = await this.cache.GetByKeysAsync(new string[] { "4", "5" });
            this.WaitUntilKeyExpire(cache, "5");
            Provider[] result2 = await this.cache.GetByKeysAsync(new string[] { "4", "5" });
            Assert.AreEqual(2, counter);
            Assert.AreEqual(0, keyCounter);
            Assert.AreEqual(4, result1[0].Id);
            Assert.AreEqual(5, result1[1].Id);
            Assert.AreEqual(4, result2[0].Id);
            Assert.AreEqual(5, result2[1].Id);
        }
    }
}
