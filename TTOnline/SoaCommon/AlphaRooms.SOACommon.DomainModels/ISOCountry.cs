﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class ISOCountry
    {
        public string IsoCode { get; set; }
        public string Country { get; set; }
        public bool Blocked { get; set; }
        public string ContinentName { get; set; }
        public string ImagePath { get; set; }
        public bool? IsVisible { get; set; }
        public int Id { get; set; }
        public int LocaleId { get; set; }
        public bool PossibleExtraChargeForHomeBookers { get; set; }
        public string SeoFriendlyName { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? RadiusToShowOnMap { get; set; }

        public virtual ICollection<ISOCountryLocalized> IsoCountryLocalizeds { get; set; }
    }
}