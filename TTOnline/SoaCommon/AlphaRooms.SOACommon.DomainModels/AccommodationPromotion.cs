﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    [Serializable]
    public class AccommodationPromotion
    {
        public int Id { get; set; }
        public Guid? Establishment { get; set; }
        public DiscountApplicationType ApplicationType { get; set; }
        public string BoardType { get; set; }
        public int? Rating { get; set; }
        public string CategoryType { get; set; }

        public PromotionalDiscount PromotionalDiscount { get; set; }

    }
}
