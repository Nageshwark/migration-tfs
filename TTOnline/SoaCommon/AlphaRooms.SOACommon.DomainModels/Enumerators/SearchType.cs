﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum SearchType
    {
        FlightOnly,
        FlightAndHotel,
        HotelOnly
    }
}
