﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    // Any change done here must also be done on AlphaRooms.Flights.DomainModels.Enumerators.MarkupTypes
    public enum CostTypes
    {
        Unknown = 0,
        AdultFare = 1,
        ChildFare = 2,
        InfantFare = 3,
        AdultTax = 4,
        ChildTax = 5,
        InfantTax = 6,
        AtolFee = 7,
        CardFee = 8,
        Baggage = 9,
        Meal = 10,
        SportsEquipment = 11,
        Equipment = 12,
        Fees = 13,
        TransferCharge = 14,
        RoomCharge = 15,
        ManagementCharge = 16,
        Champagne = 17,
        GolfEquipment = 18,
        SkisEquipment = 19,
        PriorityCheckIn = 20,
        PriorityBoarding = 21,
        AirportLounge = 22,
        Discount = 23
    }
}
