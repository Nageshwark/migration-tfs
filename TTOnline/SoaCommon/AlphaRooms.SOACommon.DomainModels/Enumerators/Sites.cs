﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    using System.ComponentModel;

    public enum Sites
    {
        Unknown = 0,

        [Description("Alpharooms.com")]
        AlphaRooms = 1,

        [Description("holexa.com")]
        Holexa = 2,

        [Description("vacenza.com")]
        Vacenza = 3,
        
        [Description("totalstay.vacenza.com")]
        TotalStayHost2Host = 4,

        [Description("BetaBeds.com")]
        BetaBeds = 5
    }
}
