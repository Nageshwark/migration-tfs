﻿using System.Runtime.Serialization;

namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum BookingStatus
    {
        NotConfirmed,
        BookingNotRequested,
        Confirmed,
        NotAttempted
    }
}
