﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum SearchResultSource
    {
        Unknown = 0,
        Cache = 1,
        Live = 2
    }
}
