﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum SortDirection
    {
        None,
        Ascending,
        Descending,
    }
}
