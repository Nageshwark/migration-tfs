﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    /// <summary>
    /// Defines Card Type
    /// </summary>
    public enum CardBrand
    {
        /// <summary>
        /// Visa
        /// </summary>
        VI,
        /// <summary>
        /// Mastercard
        /// </summary>
        MC,
        /// <summary>
        /// American Express
        /// </summary>
        AX,
        /// <summary>
        /// Solo/Switch, Maestro
        /// </summary>
        SW,
        /// <summary>
        /// International Maestro/Laser Card
        /// </summary>
        IM,
        /// <summary>
        /// Other / Not Set
        /// </summary>
        NotSet
    }
}
