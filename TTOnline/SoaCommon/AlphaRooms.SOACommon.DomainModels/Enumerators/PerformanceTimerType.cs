﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum PerformanceTimerType
    {
        AvailabilitySearching,
        SupplierCacheRetrieval,
        SupplierSearching,
        Processing,
        Processor,
        AvailabilityMarkup,
        ValuationMarkup,
        Consolidation,
        Valuation,
        PrebookingValuation,
        Booking,
        ValuationCacheRetrieval,
        BookingCacheRetrieval,
        GetAvailabilityResults,
        GetAvailabilityMapResults,
        Cancellation
    }
}
