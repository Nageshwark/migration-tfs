﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum PassengerTitle
    {
        Unknown,
        Mr,
        Mrs,
        Miss,
        Master,
        Ms,
        Dr,
        Rev
    }
}