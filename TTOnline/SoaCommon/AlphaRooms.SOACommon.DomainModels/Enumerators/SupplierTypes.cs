﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum SupplierTypes
    {
        Flight = 1,
        Transfer = 2,
        Product = 3,
        Accommodation = 4,
        CarHire = 5
    }
}
