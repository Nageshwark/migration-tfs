﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum Status
    {
        NotFound,

        NotStarted,

        InProgress,

        Successful,

        Failed,

        Timeout,

        Expired
    }
}
