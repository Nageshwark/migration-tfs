﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum BookingItemStatus
    {
        NotStarted = 1,
        Succeeded = 2,
        Failed = 3,
        ManualEntry = 4,
        Deferred = 5,
    }
}
