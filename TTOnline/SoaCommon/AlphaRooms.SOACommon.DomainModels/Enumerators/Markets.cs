﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum Markets
    {
        UK = 0,
        Spain = 1,
        Germany = 2,
        Ireland = 3,
        USA = 4,
        Canada = 5,
        Portugal = 6,
        Australia = 7,
        Austria = 8,
        Switzerland = 9,
        Italy = 10,
        Turkey = 11,
        Poland = 12
    }
}
