﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum ItineraryStatus
    {
        NotProcessed = 0,

        Success = 1,

        NotConfirmed = 2,

        ManualEntry = 3,

        PaymentFailed = 4,

        BookingFailed = 5,

        PaymentPending = 6,

        CardDeclinedBookingNotMade = 7,

        Deferred = 8
    }
}
