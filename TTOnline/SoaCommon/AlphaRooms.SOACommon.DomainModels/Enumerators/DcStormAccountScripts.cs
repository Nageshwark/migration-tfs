﻿using System.ComponentModel;
using System.Security.Cryptography;

namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum DcStormAccountScripts
    {
        [Description("t1.stormiq.com/dcv4/jslib/sid0705_4.003.js")]
        Alpharooms = 1, 
        
        [Description("t1.stormiq.com/dcv4/jslib/sid0706_4.003.js")]
        Vacenza = 2
    }
}
