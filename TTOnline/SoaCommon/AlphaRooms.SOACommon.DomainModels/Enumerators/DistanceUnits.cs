﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum DistanceUnits
    {
        Miles = 0, 
        Kilometers = 1
    }
}
