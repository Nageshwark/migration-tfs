﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum PassengerType
    {
        Unknown,
        Adult,
        Child,
        Infant
    }
}
