﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    [DataContract]
    public enum Channel
    {
        [Description("Alpharooms UK")]
        [EnumMember]
        AlphaRoomsUK = 1,

        [Description("Alpharooms IE")]
        [EnumMember]
        AlphaRoomsIE = 2,
        
        [Description("Alpharooms USA")]
        [EnumMember]
        AlphaRoomsUS = 31,

        [Description("BetaBeds UK")]
        [EnumMember]
        BetaBedsUK = 32,

        [Description("BetaBeds IE")]
        [EnumMember]
        BetaBedsIE = 33,

        [Description("BetaBeds USA")]
        [EnumMember]
        BetaBedsUS = 34,

        [Description("Teletext IE")]
        [EnumMember]
        TeletextIE = 35,

        [Description("Holidays On The Cheap UK")]
        [EnumMember]
        HolexaUK = 3,

        [Description("Vacenza UK")]
        [EnumMember]
        VacenzaUK = 4,

        [Description("Vacenza Spain")]
        [EnumMember]
        VacenzaSpain = 5,

        [Description("Vacenza Germany")]
        [EnumMember]
        VacenzaGermany = 6,

        [Description("Vacenza Ireland")]
        [EnumMember]
        VacenzaIreland = 7,
        [Description("Vacenza USA")]
        [EnumMember]
        VacenzaUSA = 8,

        [Description("Vacenza Canada")]
        [EnumMember]
        VacenzaCanada = 9,

        [Description("Vacenza Portugal")]
        [EnumMember]
        VacenzaPortugal = 10,

        [Description("Vacenza Australia")]
        [EnumMember]
        VacenzaAustralia = 11,

        [Description("Vacenza Austria")]
        [EnumMember]
        VacenzaAustria = 12,

        [Description("Vacenza Switzerland")]
        [EnumMember]
        VacenzaSwitzerland = 13,

        [Description("Vacenza Italy")]
        [EnumMember]
        VacenzaItaly = 14,

        [Description("Vacenza Turkey")]
        [EnumMember]
        VacenzaTurkey = 15,

        [Description("Vacenza Poland")]
        [EnumMember]
        VacenzaPoland = 16,

        [Description("TotalStay UK")]
        [EnumMember]
        TotalStayUK = 17,

        [Description("TotalStay Ireland")]
        [EnumMember]
        TotalStayIE = 18,

        [Description("TotalStay Spain")]
        [EnumMember]
        TotalStaySpain = 19,

        [Description("TotalStay Germany")]
        [EnumMember]
        TotalStayGermany = 20,

        [Description("TotalStay USA")]
        [EnumMember]
        TotalStayUSA = 21,

        [Description("TotalStay Canada")]
        [EnumMember]
        TotalStayCanada = 22,

        [Description("TotalStay Portugal")]
        [EnumMember]
        TotalStayPortugal = 23,

        [Description("TotalStay Australia")]
        [EnumMember]
        TotalStayAustralia = 25,

        [Description("TotalStay Austria")]
        [EnumMember]
        TotalStayAustria = 26,

        [Description("TotalStay Switzerland")]
        [EnumMember]
        TotalStaySwitzerland = 27,

        [Description("TotalStay Italy")]
        [EnumMember]
        TotalStayItaly = 28,

        [Description("TotalStay Turkey")]
        [EnumMember]
        TotalStayTurkey = 29,

        [Description("TotalStay Poland")]
        [EnumMember]
        TotalStayPoland = 30,

        [Description("Vacenza UI Testing")]
        [EnumMember]
        VacenzaUiTesting = 99,

        [Description("TeletextHolidays UK")]
        [EnumMember]
        TeletextHolidaysUK = 36
    }
}
