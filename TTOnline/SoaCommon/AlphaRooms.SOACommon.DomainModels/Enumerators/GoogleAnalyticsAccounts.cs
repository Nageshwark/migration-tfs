﻿using System.ComponentModel;

namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum GoogleAnalyticsAccounts
    {
        [Description("UA-20088-1")]
        Alpharooms = 1,
        [Description("UA-20088-14")]
        AlpharoomsMobile = 2,
        [Description("UA-20088-5")]
        Vacenza = 3,
        [Description("UA-20088-15")]
        VacenzaMobile = 4 
    }
}
