﻿
namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum RequestType
    {
        Availability,
        Valuation,
        PrebookingValuation,
        Prebooking,
        Booking,
        Cancellation
    }
}
