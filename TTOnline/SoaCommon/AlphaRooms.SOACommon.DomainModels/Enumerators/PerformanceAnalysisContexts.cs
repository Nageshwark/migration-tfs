﻿namespace AlphaRooms.SOACommon.DomainModels.Enumerators
{
    public enum PerformanceAnalysisContexts
    {
        Unknown,
        StartAvailabilitySearch,
        GetAvailabilityResults,
        StartValuation,
        GetValuationResult,
        StartBooking,
        GetBookingResult
    }
}
