﻿using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class PaymentDefaultCardFee
    {
        public int Id { get; set; }

        public Channel Channel { get; set; }

        public CardType CardType { get; set; }

        public string CardTypeText { get; set; }

        public decimal CardCostPerc { get; set; }

        public decimal? CardCostMin { get; set; }
    }
}
