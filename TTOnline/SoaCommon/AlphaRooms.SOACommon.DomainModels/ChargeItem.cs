﻿using System;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
	public class ChargeItem
    {
        public ChargeItem()
        {
            this.Cost = new Money();
            this.SupplierUnitCost = new Money();
            this.PaymentToTake=new Money();
        }

        public string SupplierRef { get; set; }
        public int Id { get; set; }
        public CostTypes CostType { get; set; }
        public Money Cost { get; set; }
        public Money SupplierUnitCost { get; set; }
        public Money PaymentToTake { get; set; }
        public bool IsPerPerson { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
    }
}
