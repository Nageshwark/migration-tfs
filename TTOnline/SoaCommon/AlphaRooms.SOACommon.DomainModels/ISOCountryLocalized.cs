﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class ISOCountryLocalized
    {
        public int Id { get; set; }
        public int LocaleId { get; set; }
        public string IsoCode { get; set; }
        public string Country { get; set; }
        public string ContinentName { get; set; }

        public virtual ISOCountry IsoCountry { get; set; }
    }
}