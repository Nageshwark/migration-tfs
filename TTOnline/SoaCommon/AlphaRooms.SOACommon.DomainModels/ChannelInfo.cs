﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    using System.Collections.Generic;
    using AlphaRooms.SOACommon.DomainModels.Enumerators;
    using AlphaRooms.Utilities;
    using MongoDB.Bson.Serialization.Attributes;

    public class ChannelInfo
    {
        private int channelId;

        public int ChannelId { get { return channelId; } set { this.channelId = value; this.Channel = (Channel)value; } }

        public Channel Channel { get; set; }

        public Sites Site { get; set; }

        public Markets Market { get; set; }

        public string CurrencyCode { get; set; }

        public string DisplayCurrencyCode { get; set; }

        public string CurrencySymbol { get; set; }
        
        public string InternalName { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public int LocaleId { get; set; }

        public string LanguageName { get; set; }

        public string LanguageCode { get; set; }

        public string CultureCode { get; set; }

        public bool AddressesHavePostCode { get; set; }

        public bool AddressesHaveCounty { get; set; }

        public bool Active { get; set; }

        public DistanceUnits DistanceUnit { get; set; }

        [Column("GoogleAnalyticsAccount")]
        public string GoogleAnalyticsAccountString { get; set; }
         
        [NotMapped]
        public GoogleAnalyticsAccounts GoogleAnalyticsAccount 
        {
            get { return EnumExtensions.ParseFromDescription<GoogleAnalyticsAccounts>(GoogleAnalyticsAccountString); }
            set { GoogleAnalyticsAccountString = value.ToStringWithDescription(); }
        }

        [Column("GoogleAnalyticsMobileAccount")]
        public string GoogleAnalyticsMobileAccountString { get; set; }

        [NotMapped]
        public GoogleAnalyticsAccounts GoogleAnalyticsMobileAccount
        {
            get { return EnumExtensions.ParseFromDescription<GoogleAnalyticsAccounts>(GoogleAnalyticsMobileAccountString); }
            set { GoogleAnalyticsMobileAccountString = value.ToStringWithDescription(); }
        }

        public int DcStormTrackingChannel { get; set; }

        [Column("DcStormAccountScript")]
        public string DcStormAccountScriptString { get; set; }
        
        [NotMapped]
        public DcStormAccountScripts DcStormAccountScript 
        {
            get { return EnumExtensions.ParseFromDescription<DcStormAccountScripts>(DcStormAccountScriptString); }
            set { DcStormAccountScriptString = value.ToStringWithDescription(); }
        }
        
        public int DcStormTrackingChannelMobile { get; set; }

        public bool ChargeCreditCardFee { get; set; }

        public string AffiliateUrl { get; set; }

        public string SafetechWebsiteName { get; set; }

        public string CriteoId { get; set; }

        public string CriteoIdPair { get; set; }

        [Column("ValidCardTypes")]
        public string ValidCardTypeString { get; set; }

        [NotMapped]
        [BsonIgnore]
        public List<CardType> ValidCardTypes
        {
            get { return ValidCardTypeString.Replace("CardType.", "").Split(';').Select(i => (CardType)Enum.Parse(typeof(CardType), i)).ToList(); }
            set { ValidCardTypeString = value.Select(i => i.ToString()).Aggregate((i,j) => i + ";" + j); }
        }
    }
}
