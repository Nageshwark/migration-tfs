﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AlphaRooms.SOACommon.Contracts.Enumerators;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class PromotionalDiscount
    {
        public int Id { get; set; }
        public string Voucher { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Amount { get; set; }
        public bool Active { get; set; }
        public int MaxUses { get; set; }
        public int ActualUses { get; set; }       
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public DiscountType DiscountType { get; set; }
        public Guid? Destination { get; set; }
        public decimal? Threshold { get; set; }
        public Channel? Channel { get; set; }

        public virtual AccommodationPromotion AccommodationPromotion { get; set; }
    }
}