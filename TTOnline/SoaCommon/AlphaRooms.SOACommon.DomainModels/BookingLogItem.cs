﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class BookingLogItem
    {
        public int Id { get; set; }
        public Guid BookingId { get; set; }
        public int Sequence { get; set; }
        public string LogData { get; set; }
        public int ItineraryId { get; set; }
        public int SupplierId { get; set; }
        public string Comment { get; set; }
        
    }
}
