﻿namespace AlphaRooms.SOACommon.DomainModels
{
    public class Parameter
    {
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public string Description { get; set; }
    }
}
