﻿using System;
using System.Collections.Generic;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class Itinerary
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public string Reference { get; set; }
		public ItineraryStatus Status { get; set; }
		public bool EmailSent { get; set; }
        public Channel Channel { get; set; }
        public bool IsImportedToGeneralLedger { get; set; }
        public bool IsTrustFund { get; set; }
        public string Notes { get; set; }
        public int? ItinSequenceNumber { get; set; }
        public Sites Site { get; set; }
        public bool IsPostProcessed { get; set; }
        public bool IsFullyPaid { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsRisky { get; set; }
        public PassengerContactDetails Customer { get; set; }
        public string StaffReference { get; set; }
        public DateTime? BalanceDueDate { get; set; }
        public string Referrer { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }

        public virtual ICollection<ItineraryPerson> ItineraryPersons { get; set; }
    }
}