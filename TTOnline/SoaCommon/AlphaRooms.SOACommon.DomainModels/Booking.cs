﻿using System.Collections.Generic;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class Booking
    {
        public int Id { get; set; }
        public int ItineraryId { get; set; }
        public BookingStatus Status { get; set; }
        public string ErrorString { get; set; }

        public virtual Itinerary Itinerary { get; set; }

        public virtual ICollection<BookingItem> BookingItems { get; set; }
    }
}
