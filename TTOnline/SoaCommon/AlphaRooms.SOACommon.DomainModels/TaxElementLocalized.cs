﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class TaxElementLocalized
    {
        public int Id { get; set; }
        public int TaxElementId { get; set; }
        public int LocaleId { get; set; }
        public string Name { get; set; }
        public string VacenzaDescription { get; set; }
    }
}