﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    [DataContract]
    public class Passenger
    {
        public int Id { get; set; }

        [Column("Title")]
        public string TitleAsString { get; set; }

        [NotMapped]
        public PassengerTitle Title
        {
            get { return (PassengerTitle) Enum.Parse(typeof(PassengerTitle), TitleAsString); }

            set { TitleAsString = value.ToStringWithDescription(); } 
        }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public Int16? Age { get; set; }

        public PassengerType Type { get; set; }

        public int ItineraryId { get; set; }

        public Gender Gender { get; set; }

        public bool IsLeadPassenger { get; set; }
    }
}