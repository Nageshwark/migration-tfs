﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class ItineraryPerson
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public short? AgeAtTimeOfTravel { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int ItineraryId { get; set; }
        public virtual Itinerary Itinerary { get; set; }
        public virtual ICollection<BookingItem> BookingItems { get; set; }
    }
}