﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class TaxElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal CommissionWeCharge { get; set; }
        public string VacenzaDescription { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsBaggage { get; set; }
        public bool IsMeal { get; set; }
    }
}