﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class BookingItem
    {
        public int Id { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
        public BookingItemStatus Status { get; set; }
        public string ErrorString { get; set; }
        public int BookingId { get; set; }
        public bool IsCancelled { get; set; }

        public virtual ICollection<ChargeItem> BookingElements { get; set; }
        public virtual Booking Booking { get; set; }
        public virtual ICollection<ItineraryPerson> ItineraryPersons { get; set; }

        public IList<Money> GetSupplierGross()
        {
            return
                this.BookingElements.Select(x => new Money(x.SupplierUnitCost.Amount * x.Quantity, x.SupplierUnitCost.CurrencyCode))
                    .GroupBy(x => x.CurrencyCode)
                    .Select(x => new Money(x.Sum(y => y.Amount), x.Key))
                    .ToList();
        }
    }
}
