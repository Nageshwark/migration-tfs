﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DomainModels
{
    public class SupplierConnectivityLog
    {
        public int Id { get; set; }
        public string HostName { get; set; }
        public SupplierTypes SupplierType { get; set; }
        public int SupplierId { get; set; }
        public ServiceTypes ServiceType { get; set; }
        public System.DateTime LogDate { get; set; }
        public int SuccessCount { get; set; }
        public int FailCount { get; set; }
        public int AverageTimeTaken { get; set; }
        public string Exceptions { get; set; }
    }
}
