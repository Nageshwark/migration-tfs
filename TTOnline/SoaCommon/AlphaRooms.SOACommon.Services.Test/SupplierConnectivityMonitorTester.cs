﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Services.Test
{
    /// <summary>
    /// Summary description for SupplierConnectivityMonitor
    /// </summary>
    [TestClass]
    public class SupplierConnectivityMonitorTester
    {
        private SupplierConnectivityLogMonitor monitor;
        private bool isCallBackCalled;

        [TestInitialize]
        public void Initialize()
        {
            this.monitor = new SupplierConnectivityLogMonitor("me", SupplierTypes.Flight, 1, ServiceTypes.Availability, (i) => { isCallBackCalled = true; });
            this.isCallBackCalled = false;
        }

        [TestMethod]
        public void ConstructorShouldPopulatePropertiesCorrectly()
        {
            Assert.AreEqual("me", monitor.HostName);
            Assert.AreEqual(SupplierTypes.Flight, monitor.SupplierType);
            Assert.AreEqual(1, monitor.SupplierId);
            Assert.AreEqual(ServiceTypes.Availability, monitor.ServiceType);
        }

        [TestMethod]
        public void StartShouldRecordDate()
        {
            monitor.Start();
            Assert.IsNotNull(monitor.StartDate);
        }

        [TestMethod]
        public void StopShouldRecordDateAndCallConnectivityService()
        {
            monitor.Start();
            monitor.Stop();
            Assert.IsNotNull(monitor.StopDate);
            Assert.AreEqual(true, monitor.IsServiceSuccessful);
            Assert.AreEqual(null, monitor.ServiceException);
            Assert.AreEqual(true, monitor.IsCompleted);
            Assert.AreEqual(true, isCallBackCalled);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void StopShouldThrowExceptionWhenStartIsNotCalled()
        {
            monitor.Stop();
        }

        [TestMethod]
        public void StopWithExceptionShouldRecordDateAndExceptionAndCallConnectivityService()
        {
            monitor.Start();
            Exception ex = new ArgumentException("Exception Test");
            monitor.StopWithException(ex);
            Assert.IsNotNull(monitor.StopDate);
            Assert.AreEqual(false, monitor.IsServiceSuccessful);
            Assert.AreEqual(ex, monitor.ServiceException);
            Assert.AreEqual(true, monitor.IsCompleted);
            Assert.AreEqual(true, isCallBackCalled);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StopWithExceptionShouldThrowExceptionWhenExceptionIsNull()
        {
            monitor.Start();
            monitor.StopWithException(null);
        }


        [TestMethod]
        public void IsCompletedShouldBeTrueAfterStopIsCalled()
        {
            Assert.AreEqual(false, monitor.IsCompleted);
            monitor.Start();
            Assert.AreEqual(false, monitor.IsCompleted);
            monitor.Stop();
            Assert.AreEqual(true, monitor.IsCompleted);
        }

        [TestMethod]
        public void IsCompletedShouldBeTrueAfterStopWithExceptionIsCalled()
        {
            Assert.AreEqual(false, monitor.IsCompleted);
            monitor.Start();
            Assert.AreEqual(false, monitor.IsCompleted);
            monitor.StopWithException(new ArgumentException());
            Assert.AreEqual(true, monitor.IsCompleted);
        }

        [TestMethod]
        public void IsServiceSuccessfulShouldBeTrueWhenStopIsCalled()
        {
            Assert.AreEqual(null, monitor.IsServiceSuccessful);
            monitor.Start();
            Assert.AreEqual(null, monitor.IsServiceSuccessful);
            monitor.Stop();
            Assert.AreEqual(true, monitor.IsServiceSuccessful);
        }

        [TestMethod]
        public void IsServiceSuccessfulShouldBeFalseWhenStopWithExceptionIsCalled()
        {
            Assert.AreEqual(null, monitor.IsServiceSuccessful);
            monitor.Start();
            Assert.AreEqual(null, monitor.IsServiceSuccessful);
            monitor.StopWithException(new ArgumentException());
            Assert.AreEqual(false, monitor.IsServiceSuccessful);
        }
    }
}
