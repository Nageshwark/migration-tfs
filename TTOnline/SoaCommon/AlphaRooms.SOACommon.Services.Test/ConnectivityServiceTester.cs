﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading;
using AlphaRooms.SOACommon.Services;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using Ninject.Extensions.Logging;

namespace AlphaRooms.SOACommon.Services.Test
{
    [TestClass]
    public class ConnectivityServiceTester
    {
        private Mock<ISupplierConnectivityLogRepository> repositoryMock;
        private Mock<IConnectivityLogServiceSettings> settingsMock;
        private Mock<ILogger> loggerMock;
        private ConnectivityLogService connectivityService;

        [TestInitialize]
        public void Initialize()
        {
            this.repositoryMock = new Mock<ISupplierConnectivityLogRepository>(MockBehavior.Strict);
            this.settingsMock = new Mock<IConnectivityLogServiceSettings>(MockBehavior.Strict);
            this.settingsMock.SetupGet(i => i.ConnectivityLogIsEnabled).Returns(true);
            this.loggerMock = new Mock<ILogger>(MockBehavior.Loose); 
            this.connectivityService = new ConnectivityLogService(settingsMock.Object, repositoryMock.Object, loggerMock.Object);
        }

        [TestMethod]
        public void CreateSupplierMonitorShouldReturnSupplierMonitor()
        {
            var monitor = connectivityService.CreateSupplierConnectivityMonitor(SupplierTypes.Flight, 1, ServiceTypes.Availability);
            Assert.IsNotNull(monitor);
            Assert.AreEqual(1, monitor.SupplierId);
            Assert.AreEqual(SupplierTypes.Flight, monitor.SupplierType);
            Assert.AreEqual(ServiceTypes.Availability, monitor.ServiceType);
        }

        [TestMethod]
        public void ServiceShouldCreateMonitorAndSendToDatabaseOnceMonitorIsStopped()
        {
            ISupplierConnectivityLogMonitor monitorCallback = null;
            repositoryMock.Setup(i => i.SaveAsync(It.IsAny<ISupplierConnectivityLogMonitor>())).Callback<ISupplierConnectivityLogMonitor>(i => monitorCallback = i);
            var monitor = connectivityService.CreateSupplierConnectivityMonitor(SupplierTypes.Flight, 1, ServiceTypes.Availability);
            monitor.Start();
            monitor.Stop();
            while (connectivityService.IsRunning)
            {
            }
            Assert.AreEqual(monitor, monitorCallback);
        }
    }
}
