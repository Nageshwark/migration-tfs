﻿using Moq;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.Utilities.EntityFramework;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Services.Test
{
    [TestClass]
    public class ChannelInfoServiceTester
    {
        private Mock<IChannelInfoRepository> repository;
        private ChannelInfoService service;
        private IDbContextActivator<SOACommonContext> context;

        [TestInitialize]
        public void Initialize()
        {
            this.repository = new Mock<IChannelInfoRepository>(MockBehavior.Strict);
            this.service = new ChannelInfoService(repository.Object);
            this.context = new DbContextActivator<SOACommonContext>();
        }

        [TestMethod]
        public void GetChannelInfoShouldSendChannelToDatabase()
        {
            var channel = Channel.AlphaRoomsUK;

            var channelInfo = new ChannelInfo();
            repository.Setup(i => i.GetChannelInfo(It.IsAny<Channel>())).Callback<Channel>(i => channel = i).ReturnsAsync(channelInfo);
            ChannelInfo result = service.GetChannelInfo(channel).Result;

            //IChannelInfoRepository channelRepository = new ChannelInfoRepository(context);
            //channelInfo = channelRepository.GetChannelInfo(channel).Result;
            
            Assert.AreEqual(Channel.AlphaRoomsUK, channel);

            Assert.AreEqual(channelInfo, result);
        }
    }
}
