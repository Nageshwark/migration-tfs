﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.Repositories;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.Utilities.EntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AlphaRooms.SOACommon.Services.Test
{
    [TestClass]
    public class BookingLoggerServiceTester
    {

        private IBookingLogRepository repositoryMock;
        private BookingLoggerService bookingLoggerService;
        private IDbContextActivator<SOACommonContext> context;
        [TestInitialize]
        public void Initialize()
        {
            this.context = new DbContextActivator<SOACommonContext>();
            this.repositoryMock = new BookingLogRepository(context);
            this.bookingLoggerService = new BookingLoggerService(repositoryMock);          
          
        }

        [TestMethod]
        public void LogTest()
        {
            var logitem = new BookingLogItem
            {
               
                BookingId = new Guid("751a2c8c-9a76-4f06-bee9-352f3d0c30b5"),
                Sequence = 1,
                LogData = "Test Log data",
                ItineraryId = 1,
                SupplierId = 1,
                Comment = null,
            };

            var result = bookingLoggerService.Log(logitem).Result;

            var logresult = bookingLoggerService.ReadLog(logitem.BookingId, logitem.Sequence);

            Assert.IsNotNull(logresult);

            // Assert.IsTrue(result);

        }

       
    }
}
