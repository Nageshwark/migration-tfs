﻿using System;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.DbContexts;
using AlphaRooms.Utilities.EntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.SOACommon.Services.Test
{
    [TestClass]
    public class ExchangeRateServiceTester
    {
        private Mock<IExchangeRateRepository> repository;
        private ExchangeRateService service;
        private IDbContextActivator<SOACommonContext> context;

        [TestInitialize]
        public void Initializer()
        {
            this.repository = new Mock<IExchangeRateRepository>(MockBehavior.Strict);
            this.service = new ExchangeRateService(repository.Object);
            this.context = new DbContextActivator<SOACommonContext>();
        }

        [TestMethod]
        public void GetExchangeRateFromDatabase()
        {
            //repository.Setup(i => i.ConvertCurrency(It.Is<ExchangeRate>(d => d.Rate == 0 && d.CurrencyFrom == "Pounds" && d.CurrencyTo == "Rupees")), Times.Never);

            repository.Setup(p => p.GetExchangeRate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns< string, string>(
                    (from, to) =>
                    {
                        var moqRate = new Mock<ExchangeRate>();
                        moqRate.SetupAllProperties();
                        moqRate.Object.CurrencyFrom = from;
                        moqRate.Object.CurrencyTo = to;
                        return moqRate.Object;
                    });

            //var amount = service.ConvertCurrency(new Money(100, "GBP"), "INR").GetAwaiter().GetResult();

            //Assert.AreEqual(Decimal.Zero, amount.Amount);
            //Assert.AreEqual("INR", amount.CurrencyCode);
        }
    }
}
