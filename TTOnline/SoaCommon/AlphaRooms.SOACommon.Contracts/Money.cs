﻿using System;
using System.Runtime.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Diagnostics;

namespace AlphaRooms.SOACommon.Contracts
{
	[DataContract]
    [DebuggerDisplay("{DebuggerDisplay}")]
    [BsonIgnoreExtraElements]
    [Serializable]
    public class Money : IComparable
    {
        public const string AmountFieldName = "Amt";
        public const string CurrencyCodeFieldName = "CrC";

        public Money()
        {
        }

        public Money(decimal amount)
        {
            this.Amount = amount;
        }
		
        public Money(decimal amount, string currencyCode)
        {
            this.Amount = amount;
            this.CurrencyCode = currencyCode;
        }

        public int CompareTo(object obj)
        {
            Money money = obj as Money;
            return decimal.Compare(this.Amount, money.Amount);
        }

		[DataMember]
        [BsonElement(CurrencyCodeFieldName)]
        public string CurrencyCode { get; set; }

		[DataMember]
		[BsonRepresentation(BsonType.Double, AllowTruncation=true)]
        [BsonElement(AmountFieldName)]
		public decimal Amount { get; set; }

        private string DebuggerDisplay 
        { 
            get { return this.CurrencyCode + this.Amount.ToString(); } 
        }

        public override string ToString()
        {
            return this.CurrencyCode + this.Amount.ToString();
        }
        
        public static Money Parse(string value)
        {
            return new Money(decimal.Parse(value.Substring(3)), value.Substring(0, 3));
        }
    }
}
