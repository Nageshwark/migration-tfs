﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.SoaCommon.Contracts
{
    public class RequestAirport
    {
        public string AirportCode { get; set; }
        public string[] ChildAirportCodes { get; set; }
    }
}
