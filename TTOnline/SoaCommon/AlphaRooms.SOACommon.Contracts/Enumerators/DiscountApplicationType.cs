﻿using System.ComponentModel;

namespace AlphaRooms.SOACommon.Contracts.Enumerators
{
    public enum DiscountApplicationType
    {
        [Description("Discount applied by booking")]
        PerBooking = 1,
        [Description("Discount applied by room")]
        PerRoom = 2
    }
}
