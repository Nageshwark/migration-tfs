﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Contracts.Enumerators
{
    public enum RateType
    {
        /// <summary>
        /// Net Non Binding Rate
        /// </summary>
        NetStandard,
        /// <summary>
        /// Net Rate for Pay Direct
        /// </summary>
        NetPayDirect,
        /// <summary>
        /// Net Binding Rate
        /// </summary>
        NetBinding,
        /// <summary>
        /// Gross Binding Rate
        /// </summary>
        GrossStandard,
        /// <summary>
        /// Gross Non Binding
        /// </summary>
        GrossNonBinding,
        /// <summary>
        /// Gross Rate for Pay Direct
        /// </summary>
        GrossPayDirect,

        NA
        
    }
}
