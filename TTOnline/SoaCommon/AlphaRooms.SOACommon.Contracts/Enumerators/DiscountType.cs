﻿namespace AlphaRooms.SOACommon.Contracts.Enumerators
{
    public enum DiscountType
    {
        Fixed = 1,
        Percentage = 2,
        FixedRepeatable = 3
    }
}
