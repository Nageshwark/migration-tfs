﻿using AlphaRooms.Utilities;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using AlphaRooms.Utilities.Enumerators;

namespace AlphaRooms.SOACommon.Contracts.Enumerators
{
    [DataContract]
    public enum CardType : int
    {
        /// <summary>
        /// The placeholder for unknown card types or invalid cards.
        /// </summary>
        [Description("Unknown")]
        [EnumMember]
		Unknown = 0,

        /// <summary>
        /// Represents a MasterCard credit card.
        /// </summary>
        [Description("Mastercard")]
		[EnumMember(Value = "Mastercard")]
        [PaymentType(CardPaymentType.Credit)]
		MasterCard = 1,

        /// <summary>
        /// Represents a Visa credit card.
        /// </summary>
        [Description("Visa")]
		[EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		VisaCredit = 2,

        /// <summary>
        /// Represents an American Express charge card.
        /// </summary>
        [Description("American Express")]
		[EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		AmericanExpress = 3,

        /// <summary>
        /// Represents a Diners Club or Carte Blanche charge card.
        /// </summary>
        [Description("Diners Club / Carte Blanche")]
		[EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		DinersCarteBlanche = 4,

        /// <summary>
        /// Represents a Discover credit card.
        /// </summary>
        [Description("Discover")]
        [EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		Discover = 5,

        /// <summary>
        /// Represents an Enroute charge card
        /// </summary>
        [Description("Enroute")]
        [EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		Enroute = 6,

        /// <summary>
        /// Represents a JCB charge card
        /// </summary>
        [Description("JCB")]
        [EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		JCB = 7,

        /// <summary>
        /// Represents a SWITCH credit card
        /// </summary>
        [Description("Switch")]
        [EnumMember]
        [PaymentType(CardPaymentType.Debit)]
		Switch = 8,

        /// <summary>
        /// Represents a DELTA credit card
        /// </summary>
        [Description("Delta")]
        [EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		Delta = 9,

        /// <summary>
        /// Represents a SOLO credit card
        /// </summary>
        [Description("Solo")]
        [EnumMember]
        [PaymentType(CardPaymentType.Credit)]
		Solo = 10,

        /// <summary>
        /// Represents a Maestro debit card
        /// </summary>
        [Description("Maestro")]
        [EnumMember]
        [PaymentType(CardPaymentType.Debit)]
		Maestro = 11,

        /// <summary>
        /// Represents a Laser debit card
        /// </summary>
        [Description("Laser")]
        [EnumMember]
        [PaymentType(CardPaymentType.Debit)]
		Laser = 12,

        /// <summary>
        /// Represents a prepaid master card
        /// </summary>
        [Description("Prepaid Master Card")]
        [PaymentType(CardPaymentType.Credit)]
		[EnumMember]
		PrepaidMasterCard = 13,

        /// <summary>
        /// Represents an international meastro card
        /// </summary>
        [Description("International Maestro")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		InternationalMaestro = 14,

        [Description("Visa Debit")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		VisaDebit = 15,

        [Description("Visa Delta")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		VisaDelta = 16,

        [Description("Visa Electron")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		VisaElectron = 17,

        [Description("Visa Purchasing")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		VisaPurchasing = 18,

        [Description("Mastercard Debit")]
        [PaymentType(CardPaymentType.Debit)]
		[EnumMember]
		MastercardDebit = 19,

        [Description("Eurocard")]
		[EnumMember]
		EuroCard = 20,

        [Description("Diners Club")]
		[EnumMember]
		DinersClub = 21,

        [Description("PayPal")]
        [EnumMember]
        PayPal = 22,
    }
}
