﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Contracts.Enumerators
{
    /// <summary>
    /// Defines Card Type
    /// </summary>
    [DataContract]
	public enum CardBrand
    {
        /// <summary>
        /// Visa
        /// </summary>
		[EnumMember]
		VI,

        /// <summary>
        /// Mastercard
        /// </summary>
		[EnumMember]
		MC,

        /// <summary>
        /// American Express
        /// </summary>
		[EnumMember]
		AX,

        /// <summary>
        /// Solo/Switch, Maestro
        /// </summary>
		[EnumMember]
		SW,

        /// <summary>
        /// International Maestro/Laser Card
        /// </summary>
		[EnumMember]
		IM,

        /// <summary>
        /// Other / Not Set
        /// </summary>
		[EnumMember]
		NotSet
    }
}
