﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Contracts
{
    [DataContract]
    [DebuggerDisplay("{Month}/{Year}")]
    [BsonIgnoreExtraElements]
    [Serializable]
    public class MonthYear
    {
        public const string MonthFieldName = "Mnt";
        public const string YearFieldName = "Yer";

        public MonthYear()
        {
        }

        public MonthYear(int month, int year)
        {
            this.Month = month;
            this.Year = year;
        }

        [DataMember]
        [BsonElement(MonthFieldName)]
        public int Month { get; set; }

        [DataMember]
        [BsonElement(YearFieldName)]
        public int Year { get; set; }
    }
}
