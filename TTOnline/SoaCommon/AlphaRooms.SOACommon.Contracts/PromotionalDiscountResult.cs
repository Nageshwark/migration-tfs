﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.SOACommon.Contracts
{
    [Serializable]
    [DataContract]
    public class PromotionalDiscountResult
    {
        [DataMember]
        public Money Discount { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public DiscountApplicationType DiscountType { get; set; }
    }
}
