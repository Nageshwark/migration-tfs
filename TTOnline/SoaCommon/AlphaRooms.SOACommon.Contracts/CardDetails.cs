﻿using System;
using System.Runtime.Serialization;
using System.Text;
using AlphaRooms.SOACommon.Contracts.Enumerators;

namespace AlphaRooms.SOACommon.Contracts
{
	[DataContract]
    public class CardDetails
    {
        [DataMember]
        public CardType CardType { get; set; }

        [DataMember]
        public string CardNumber { get; set; }

        [DataMember]
        public int ExpiryMonth { get; set; }

        [DataMember]
        public int ExpiryYear { get; set; }

        [DataMember]
        public int StartMonth { get; set; }

        [DataMember]
        public int StartYear { get; set; }

        [DataMember]
        public string IssueNumber { get; set; }

        [DataMember]
        public string CardHolderName { get; set; }

        [DataMember]
        public string Cvc { get; set; }

        // Added for commissionable booking
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string County { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public bool IsCreditCard { get; set; }

        /// <summary>
        /// Gets the orbital card type based on the card prefix
        /// </summary>
        /// <returns></returns>
        public CardBrand GetCardBrand()
        {
            CardBrand retVal;
            switch (CardType)
            {
                case CardType.Unknown:
                    retVal = CardBrand.NotSet;
                    break;
                case CardType.Laser:
                case CardType.InternationalMaestro:
                    retVal = CardBrand.IM;
                    break;
                case CardType.Switch:
                case CardType.Maestro:
                case CardType.Solo:
                    retVal = CardBrand.SW;
                    break;
                case CardType.MasterCard:
                case CardType.MastercardDebit:
                case CardType.PrepaidMasterCard:
                    retVal = CardBrand.MC;
                    break;
                case CardType.VisaCredit:
                case CardType.VisaDebit:
                case CardType.VisaDelta:
                case CardType.VisaElectron:
                case CardType.VisaPurchasing:
                    retVal = CardBrand.VI;
                    break;
                case CardType.AmericanExpress:
                    retVal = CardBrand.AX;
                    break;
                default:
                    retVal = CardBrand.NotSet;
                    break;
            }
            return retVal;
        }

        /// <summary>
        /// Gets the orbital card type based on the card prefix for Safetech.
        /// </summary>
        /// <returns>CardBrand</returns>
        public CardBrand GetCardBrandForSafetech()
        {
            CardBrand retVal = GetCardBrand();

            //Only set card Brand if not VI, MC, DI, AX or JCB.
            //It is only used for the more non credit card orders.            
            if (retVal == CardBrand.VI || retVal == CardBrand.MC || retVal == CardBrand.AX)
            {
                retVal = CardBrand.NotSet;
            }

            return retVal;
        }

        /// <summary>
        /// Formats and returns the Card Expiry Date for Chase.
        /// </summary>
        /// <returns>YYYYMM</returns>
        public string GetExpiryDate()
        {
            const string blank = "    ";

            //Format Month.
            string strMonth = Get2DigitMonth(ExpiryMonth);
            if (string.IsNullOrEmpty(strMonth))
            {
                return blank;
            }

            //Format Year.
            string strYear = Get4DigitYear(ExpiryYear);
            if (string.IsNullOrEmpty(strYear))
            {
                return blank;
            }

            //Put the year and month together.
            return strYear + strMonth;
        }

        /// <summary>
        /// Formats and returns the Card Expiry Month for Chase.
        /// </summary>
        /// <returns></returns>
        public string Get2DigitExpiryMonth()
        {
            return Get2DigitMonth(ExpiryMonth);
        }

        /// <summary>
        /// Formats and returns the Card Expiry Year for Chase.
        /// </summary>
        /// <returns></returns>
        public string Get2DigitExpiryYear()
        {
            return Get2DigitYear(ExpiryYear);
        }

        /// <summary>
        /// Formats and returns the Card Expiry Year for Chase.
        /// </summary>
        /// <returns></returns>
        public string Get4DigitExpiryYear()
        {
            return Get4DigitYear(ExpiryYear);
        }

        /// <summary>
        /// Formats and returns the Switch/Solo Card Start Date for Chase.
        /// </summary>
        /// <returns>MMYY</returns>
        public string GetSwitchSoloStartDate()
        {
            string blank = null;

            //Format Month.
            string strMonth = Get2DigitMonth(StartMonth);
            if (string.IsNullOrEmpty(strMonth))
            {
                return blank;
            }

            //Format Year.
            string strYear = Get2DigitYear(StartYear);
            if (string.IsNullOrEmpty(strYear))
            {
                return blank;
            }

            //Put the year and month together.
            return strMonth + strYear;
        }

        /// <summary>
        /// Get the Switch/Solo Issue Number.
        /// </summary>
        /// <returns></returns>
        public string GetSwitchSoloIssueNumber()
        {
            //If the Issue Number length is greater than 2 - this is a problem and return null instead?
            if (IssueNumber.Length > 2)
            {
                return null;
            }

            return IssueNumber;
        }

        /// <summary>
        /// Formats a month into a 2 digit string.
        /// </summary>
        /// <param name="intMonth"></param>
        /// <returns>MM</returns>
        private string Get2DigitMonth(int intMonth)
        {
            //If its a valid month...
            if (intMonth > 0 && intMonth <= 12)
            {
                //Format it to 2 digits.
                return String.Format("{0:00}", intMonth);
            }

            return string.Empty;
        }

        /// <summary>
        /// Formats a integer year into a 2 digit string.
        /// </summary>
        /// <param name="intYear"></param>
        /// <returns>YY</returns>
        private string Get2DigitYear(int intYear)
        {
            //If its a valid year...
            if (intYear > 0)
            {
                //If the year isn't in a 2 digit format...
                if (intYear > 99)
                {
                    //Divide by 100 and get the remainder.
                    intYear = intYear % 100;
                }

                //Format to 2 digits long.
                return String.Format("{0:00}", intYear);
            }

            return string.Empty;
        }

        /// <summary>
        /// Formats a integer year into a 4 digit string.
        /// </summary>
        /// <param name="intYear"></param>
        /// <returns>YYYY</returns>
        private string Get4DigitYear(int intYear)
        {
            //If its a valid year...
            if (intYear > 0)
            {
                //If the year isn't 4 digits long...
                if (intYear.ToString().Length < 4)
                {
                    //Add 2000 to make it 4 digits long.
                    intYear = intYear + 2000;
                }

                //Format to 4 digits long.
                return String.Format("{0:0000}", intYear);
            }

            return string.Empty;
        }

        /// <summary>
        /// Removes invalid charaters from a string for AVS.
        /// </summary>
        /// <returns></returns>
        public string GetCardHolderNameWithoutInvalidCharaters()
        {
            string str = CardHolderName.Replace("%", "");
            str = str.Replace("|", "");
            str = str.Replace("^", "");
            str = str.Replace("/", "");
            str = str.Replace("\\", "");

            return str;
        }

		public string GetPropertyWithoutInvalidCharacters(string property)
		{
			char[] invalidChars = { ',', '/', '\\', '-', '\'', '.' };
			var str = new StringBuilder(property);
			str = str.Replace(",", "");
			str = str.Replace("/", "");
			str = str.Replace("\\", "");
			str = str.Replace("-", "");
			str = str.Replace("\'", "");
			str = str.Replace(".", "");
			return str.ToString();
		}
    }
}
