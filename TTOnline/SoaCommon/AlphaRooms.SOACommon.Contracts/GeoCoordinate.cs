﻿using System;
using System.Runtime.Serialization;

namespace AlphaRooms.SOACommon.Contracts
{
	[DataContract]
	public class GeoCoordinate
    {
		[DataMember]
		public decimal Longitude { get; set; }

		[DataMember]
        public decimal Latitude { get; set; }
    }
}
