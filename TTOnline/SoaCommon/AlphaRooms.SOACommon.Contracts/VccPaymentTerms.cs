﻿using System;
using System.Runtime.Serialization;

namespace AlphaRooms.SOACommon.Contracts
{
    [DataContract]
    public class VccPaymentTerms
    {
        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public Money AmountToPay { get; set; }

        [DataMember]
        public bool PaymentCleared { get; set; }

        [DataMember]
        public int? VirtualCardId { get; set; }

        public bool CreateCardOnBooking { get { return this.PaymentDate <= DateTime.Now; } }
    }
}
