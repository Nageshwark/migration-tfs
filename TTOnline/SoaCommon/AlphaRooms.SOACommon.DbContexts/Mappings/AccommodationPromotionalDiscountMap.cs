﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class AccommodationPromotionalDiscountMap : EntityTypeConfiguration<AccommodationPromotion>
    {
        public AccommodationPromotionalDiscountMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("AccommodationPromotion");
            this.Property(t => t.Id).HasColumnName("PromoId");
            this.Property(t => t.ApplicationType).HasColumnName("ApplicationType");
            this.Property(t => t.BoardType).HasColumnName("BoardType");
            this.Property(t => t.CategoryType).HasColumnName("CategoryType");
            this.Property(t => t.Establishment).HasColumnName("Establishment");
            this.Property(t => t.Rating).HasColumnName("Rating");
        }
    }
}
