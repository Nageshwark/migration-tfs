﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ItineraryPersonMap : EntityTypeConfiguration<Passenger>
    {
        public ItineraryPersonMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("ItineraryPersons", "schmBooking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TitleAsString).HasColumnName("Title");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.Surname).HasColumnName("Surname");
            this.Property(t => t.Age).HasColumnName("AgeAtTimeOfTravel");
            this.Property(t => t.DateOfBirth).HasColumnName("DateOfBirth");
            this.Property(t => t.ItineraryId).HasColumnName("ItineraryId");

            this.Ignore(t => t.Type);
            this.Ignore(t => t.Gender);
        }
    }
}
