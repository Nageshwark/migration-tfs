﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ChannelInfoMap : EntityTypeConfiguration<ChannelInfo>
    {
        public ChannelInfoMap()
        {
            //Primary Key
            this.HasKey(a => a.ChannelId);

            // Table & Column Mappings
            this.ToTable("Channel");
            this.Property(a => a.ChannelId).HasColumnName("ChannelId");
            this.Ignore(a => a.Channel);
            this.Property(a => a.Site).HasColumnName("Site");
            this.Property(a => a.Market).HasColumnName("Market");
            this.Property(a => a.CurrencyCode).HasColumnName("CurrencyCode").IsUnicode(false);
            this.Property(a => a.DisplayCurrencyCode).HasColumnName("DisplayCurrencyCode").IsUnicode(false);
            this.Property(a => a.InternalName).HasColumnName("InternalName").IsUnicode(false);
            this.Property(a => a.CountryName).HasColumnName("CountryName").IsUnicode(false);
            this.Property(a => a.CountryCode).HasColumnName("CountryCode").IsUnicode(false);
            this.Property(a => a.LocaleId).HasColumnName("LocaleId");
            this.Property(a => a.LanguageName).HasColumnName("LanguageName").IsUnicode(false);
            this.Property(a => a.LanguageCode).HasColumnName("LanguageCode").IsUnicode(false);
            this.Property(a => a.CultureCode).HasColumnName("CultureCode").IsUnicode(false);
            this.Property(a => a.AddressesHavePostCode).HasColumnName("AddressesHavePostCode");
            this.Property(a => a.AddressesHaveCounty).HasColumnName("AddressesHaveCounty");
            this.Property(a => a.Active).HasColumnName("Active");
            this.Property(a => a.DistanceUnit).HasColumnName("DistanceUnit");
            this.Property(a => a.GoogleAnalyticsAccountString).HasColumnName("GoogleAnalyticsAccount").IsUnicode(false);
            this.Property(a => a.GoogleAnalyticsMobileAccountString).HasColumnName("GoogleAnalyticsMobileAccount").IsUnicode(false);
            this.Property(a => a.DcStormTrackingChannel).HasColumnName("DcStormTrackingChannel");
            this.Property(a => a.DcStormAccountScriptString).HasColumnName("DcStormAccountScript").IsUnicode(false);
            this.Property(a => a.DcStormTrackingChannelMobile).HasColumnName("DcStormTrackingChannelMobile");
            this.Property(a => a.ChargeCreditCardFee).HasColumnName("ChargeCreditCardFee");
            this.Property(a => a.AffiliateUrl).HasColumnName("AffiliateUrl").IsUnicode(false);
            this.Property(a => a.SafetechWebsiteName).HasColumnName("SafetechWebsiteName").IsUnicode(false);
            this.Property(a => a.CriteoId).HasColumnName("CriteoId").IsUnicode(false);
            this.Property(a => a.CriteoIdPair).HasColumnName("CriteoIdPair").IsUnicode(false);
            this.Property(a => a.ValidCardTypeString).HasColumnName("ValidCardTypes").IsUnicode(false);
            this.Property(a => a.CurrencySymbol).HasColumnName("CurrencySymbol").IsUnicode(false);
        }
    }
}
