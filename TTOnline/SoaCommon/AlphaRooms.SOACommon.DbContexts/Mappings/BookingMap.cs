﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class BookingMap : EntityTypeConfiguration<Booking>
    {
        public BookingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);


            // Properties
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.ErrorString)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Bookings", "schmBooking");

            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ItineraryId).HasColumnName("ItineraryId");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ErrorString).HasColumnName("ErrorString");
			//this.Ignore(t=>)

            this.HasRequired(t => t.Itinerary).WithMany(t => t.Bookings).HasForeignKey(t => t.ItineraryId);
        }
    }
}