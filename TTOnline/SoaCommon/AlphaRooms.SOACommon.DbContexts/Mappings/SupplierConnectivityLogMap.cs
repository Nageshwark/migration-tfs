﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class SupplierConnectivityLogMap : EntityTypeConfiguration<SupplierConnectivityLog>
    {
        public SupplierConnectivityLogMap()
        {
            //Primary Key
            this.HasKey(a => a.Id);

            // Table & Column Mappings
            this.ToTable("SupplierConnectivityLogs");
            this.Property(a => a.Id).HasColumnName("Id");
            this.Property(a => a.HostName).HasColumnName("HostName");
            this.Property(a => a.SupplierType).HasColumnName("SupplierType");
            this.Property(a => a.SupplierId).HasColumnName("SupplierId");
            this.Property(a => a.ServiceType).HasColumnName("ServiceType");
            this.Property(a => a.LogDate).HasColumnName("LogDate");
            this.Property(a => a.SuccessCount).HasColumnName("SuccessCount");
            this.Property(a => a.FailCount).HasColumnName("FailCount");
            this.Property(a => a.AverageTimeTaken).HasColumnName("AverageTimeTaken");
            this.Property(a => a.Exceptions).HasColumnName("Exceptions");
        }
    }
}
