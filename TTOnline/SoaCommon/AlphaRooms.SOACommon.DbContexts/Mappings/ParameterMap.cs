﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ParameterMap : EntityTypeConfiguration<Parameter>
    {
        public ParameterMap()
        {
            // Primary Key
            this.HasKey(x => x.ParameterName);

            //Tabl & Column mappings
            this.ToTable("Parameters");
            this.Property(x => x.ParameterName).HasColumnName("ParameterName").IsUnicode(false);
            this.Property(x => x.ParameterValue).HasColumnName("ParameterValue").IsUnicode(false);
            this.Property(x => x.Description).HasColumnName("Description").IsUnicode(false);
        }
    }
}
