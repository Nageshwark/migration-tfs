﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ExchangeRateMap : EntityTypeConfiguration<ExchangeRate>
    {
        public ExchangeRateMap()
        {
            //primary key
            this.HasKey(a => a.ID);

            this.ToTable("tblExchangeRate");
            this.Property(a => a.ID).HasColumnName("pkId");
            this.Property(a => a.CurrencyFrom).HasColumnName("strCurrencyFrom").IsUnicode(false);
            this.Property(a => a.CurrencyTo).HasColumnName("strCurrencyTo").IsUnicode(false);
            this.Property(a => a.Rate).HasColumnName("fltExchangeRate");
        }
    }
}
