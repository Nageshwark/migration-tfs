﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class PromotionalDiscountMap : EntityTypeConfiguration<PromotionalDiscount>
    {
        public PromotionalDiscountMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("Promotion");
            this.Property(t => t.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity); ;
            this.Property(t => t.Voucher).HasColumnName("Voucher").HasMaxLength(20);
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Active).HasColumnName("Used");
            this.Property(t => t.MaxUses).HasColumnName("MaxUses");
            this.Property(t => t.ActualUses).HasColumnName("ActualUses");
            this.Property(t => t.CheckInDate).HasColumnName("CheckInDate");
            this.Property(t => t.CheckOutDate).HasColumnName("CheckOutDate");
            this.Property(t => t.Destination).HasColumnName("Destination");
            this.Property(t => t.DiscountType).HasColumnName("DiscountType");
            this.Property(t => t.Threshold).HasColumnName("Threshold");

            this.Ignore(t => t.Channel);
        }
    }
}