﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ISOCountryMap : EntityTypeConfiguration<ISOCountry>
    {
        public ISOCountryMap()
        {
            // Primary Key
            this.HasKey(t => t.IsoCode);

            // Table & Column Mappings
            this.ToTable("tblISOCountry");

            this.Property(t => t.IsoCode).HasColumnName("strISOCode").IsRequired();
            this.Property(t => t.Country).HasColumnName("strCountry").IsRequired();
            this.Property(t => t.Blocked).HasColumnName("blnBlocked").IsRequired();
            this.Property(t => t.ContinentName).HasColumnName("strContinentName").IsRequired();
            this.Property(t => t.ImagePath).HasColumnName("strImagePath").IsRequired();
            this.Property(t => t.IsVisible).HasColumnName("blnVisible").IsOptional();
            this.Property(t => t.LocaleId).HasColumnName("fkLocaleID").IsRequired();
            this.Property(t => t.PossibleExtraChargeForHomeBookers).HasColumnName("blnPossibleExtraChargeForHomeBookers").IsRequired();
            this.Property(t => t.SeoFriendlyName).HasColumnName("strSeoFriendlyName").IsRequired();
            this.Property(t => t.Longitude).HasColumnName("dclLon").IsOptional();
            this.Property(t => t.Latitude).HasColumnName("dclLat").IsOptional();
            this.Property(t => t.RadiusToShowOnMap).HasColumnName("intRadiusToShowOnMap").IsOptional();
        }
    }
}
