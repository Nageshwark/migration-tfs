﻿using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class PaymentDefaultCardFeeMap : EntityTypeConfiguration<PaymentDefaultCardFee>
    {
        public PaymentDefaultCardFeeMap()
        {
            // Primary Key
            this.HasKey(x => x.Id);

            //Tabl & Column mappings
            this.ToTable("PaymentDefaultCardFees");
            this.Property(t => t.Channel).HasColumnName("ChannelId");
            this.Property(t => t.CardType).HasColumnName("CardTypeId");
            this.Property(t => t.CardTypeText).HasColumnName("CardTypeText");
            this.Property(t => t.CardCostMin).HasColumnName("CardCostMin").IsOptional();
            this.Property(t => t.CardCostPerc).HasColumnName("CardCostPerc");
        }
    }
}