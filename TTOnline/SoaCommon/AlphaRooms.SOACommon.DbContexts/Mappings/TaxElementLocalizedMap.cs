﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class TaxElementLocalizedMap : EntityTypeConfiguration<TaxElementLocalized>
    {
        public TaxElementLocalizedMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("TaxElementLocalized");

            this.Property(t => t.Id).HasColumnName("pkID").IsRequired();
            this.Property(t => t.TaxElementId).HasColumnName("fkTaxElementID").IsRequired();
            this.Property(t => t.LocaleId).HasColumnName("LocaleID").IsRequired();
            this.Property(t => t.Name).HasColumnName("strName").IsRequired();
            this.Property(t => t.VacenzaDescription).HasColumnName("strVacenzaDescription").IsRequired();

        }
    }
}