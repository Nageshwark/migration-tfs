﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class TaxElementMap: EntityTypeConfiguration<TaxElement>
    {
        public TaxElementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("TaxElement");

            this.Property(t => t.Id).HasColumnName("pkID").IsRequired();
            this.Property(t => t.Name).HasColumnName("strName").IsRequired();
            this.Property(t => t.CommissionWeCharge).HasColumnName("dclCommissionWeCharge").IsRequired();
            this.Property(t => t.VacenzaDescription).HasColumnName("strVacenzaDescription").IsRequired();
            this.Property(t => t.IsMandatory).HasColumnName("blnIsMandatory").IsRequired();
            this.Property(t => t.IsBaggage).HasColumnName("blnIsBaggage").IsRequired();
            this.Property(t => t.IsMeal).HasColumnName("blnIsMeal").IsRequired();
        }
    }
}