﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
   public class BookingLogMap : EntityTypeConfiguration<BookingLogItem>
    {
       public BookingLogMap()
       {
           // primay key
           this.HasKey(b => b.Id);

           this.ToTable("SOABookingLog");
           this.Property(b => b.Id ).HasColumnName("Id");
           this.Property(b => b.BookingId ).HasColumnName("BookingId");
           this.Property(b => b.Sequence).HasColumnName("Sequence");
           this.Property(b => b.LogData ).HasColumnName("LogData");
           this.Property(b => b.ItineraryId ).HasColumnName("ItineraryId");
           this.Property(b => b.SupplierId).HasColumnName("SupplierId");
           this.Property(b => b.Comment).HasColumnName("Comment");
       }
    }
}
