﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ISOCountryLocalizedMap : EntityTypeConfiguration<ISOCountryLocalized>
    {
        public ISOCountryLocalizedMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            // Table & Column Mappings
            this.ToTable("ISOCountryLocalized");
            this.Property(t => t.Id).HasColumnName("pkID").IsRequired();
            this.Property(t => t.LocaleId).HasColumnName("fkLocaleID").IsRequired();
            this.Property(t => t.IsoCode).HasColumnName("fkISOCode").IsRequired();
            this.Property(t => t.Country).HasColumnName("strCountry").IsRequired();
            this.Property(t => t.ContinentName).HasColumnName("strContinentName").IsRequired();

            this.HasRequired(x => x.IsoCountry).WithMany(x=>x.IsoCountryLocalizeds).HasForeignKey(x => x.IsoCode);

        }
    }
}
