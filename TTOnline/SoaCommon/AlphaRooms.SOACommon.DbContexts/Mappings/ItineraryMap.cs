﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class ItineraryMap : EntityTypeConfiguration<Itinerary>
    {
        public ItineraryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Itineraries", "schmBooking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CustomerId).HasColumnName("CustomerId");
            this.Property(t => t.DateTimeCreated).HasColumnName("DateTimeCreated");
            this.Property(t => t.Reference).HasColumnName("Reference");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.EmailSent).HasColumnName("EmailSent");
            this.Property(t => t.Channel).HasColumnName("Channel");
            this.Property(t => t.IsImportedToGeneralLedger).HasColumnName("IsImportedToGeneralLedger");
            this.Property(t => t.Notes).HasColumnName("Notes").IsUnicode(false);
            this.Property(t => t.ItinSequenceNumber).HasColumnName("ItinSequenceNumber");
            this.Property(t => t.Site).HasColumnName("Site");
            this.Property(t => t.IsPostProcessed).HasColumnName("IsPostProcessed");
            this.Property(t => t.IsFullyPaid).HasColumnName("IsFullyPaid");
            this.Property(t => t.IsCancelled).HasColumnName("IsCancelled");
            this.Property(t => t.IsRisky).HasColumnName("IsRisky");
            this.Property(t => t.StaffReference).HasColumnName("StaffReference");
            this.Property(t => t.BalanceDueDate).HasColumnName("BalanceDueDate");
            this.Property(t => t.Referrer).HasColumnName("Referrer");
            this.Property(t => t.IsTrustFund).HasColumnName("IsTrustFund");

           
        }
    }
}
