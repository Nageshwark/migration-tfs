﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class BookingItemMap : EntityTypeConfiguration<BookingItem>
    {
        public BookingItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.ErrorString)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("BookingItems", "schmBooking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ErrorString).HasColumnName("ErrorString");
            this.Property(t => t.BookingId).HasColumnName("BookingId");
            this.Property(t => t.Id).HasColumnName("Id");

            // Relationships
            this.HasMany(t => t.ItineraryPersons)
                .WithMany(t => t.BookingItems)
                .Map(m =>
                {
                    m.ToTable("BookingItemItineraryPerson", "schmBooking");
                    m.MapLeftKey("BookingItemId");
                    m.MapRightKey("ItineraryPersonId");
                });

            this.HasRequired(t => t.Booking)
                .WithMany(t => t.BookingItems)
                .HasForeignKey(d => d.BookingId);

        }
    }
}