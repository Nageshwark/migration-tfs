﻿using System.Data.Entity.ModelConfiguration;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts.Mappings
{
    public class BookingElementMap : EntityTypeConfiguration<ChargeItem>
    {
        public BookingElementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.SupplierUnitCost.CurrencyCode).IsRequired().HasMaxLength(3);
            
            this.Property(t => t.PaymentToTake.CurrencyCode).IsRequired().HasMaxLength(3);

            this.Property(t => t.Description).HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("BookingElements", "schmBooking");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Cost.Amount).HasColumnName("CostToCustomer");
            this.Property(t => t.Cost.CurrencyCode).HasColumnName("CostToCustomerCurrency").IsUnicode(false);
            //this.Property(t => t.Cost.CurrencySymbol).HasColumnName("CostToCustomerCurrencySymbol").IsUnicode(false);
            this.Property(t => t.SupplierUnitCost.Amount).HasColumnName("CostFromSupplier");
            this.Property(t => t.SupplierUnitCost.CurrencyCode).HasColumnName("CostFromSupplierCurrency").IsUnicode(false);
            //this.Property(t => t.SupplierUnitCost.CurrencySymbol).HasColumnName("CostFromSupplierCurrencySymbol").IsUnicode(false);
            this.Property(t => t.PaymentToTake.Amount).HasColumnName("PaymentToTake");
            this.Property(t => t.PaymentToTake.CurrencyCode).HasColumnName("PaymentToTakeCurrency");
            //this.Property(t => t.PaymentToTake.CurrencySymbol).HasColumnName("PaymentToTakeCurrencySymbol").IsUnicode(false);
            this.Property(t => t.Quantity).HasColumnName("Quantity");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.SupplierRef).HasColumnName("SupplierRef");

            this.Ignore(t => t.CostType);
            this.Ignore(t => t.IsPerPerson);
            
        }
    }
}