﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DbContexts.Mappings;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.DbContexts
{
    public class SOACommonContext : DbContext
    {
        static SOACommonContext()
        {
            Database.SetInitializer<SOACommonContext>(null);
        }

		public SOACommonContext() : base("Name=AlphabedsEntities")
        {   
        }

        public DbSet<Booking> Bookings { get; set; }
		public DbSet<ChargeItem> BookingElements { get; set; }
		public DbSet<BookingItem> BookingItems { get; set; }
		public DbSet<BookingLogItem> BookingLogItems { get; set; }
		public DbSet<ChannelInfo> ChannelInfos { get; set; }
		public DbSet<PassengerContactDetails> Customers { get; set; }
		public DbSet<ExchangeRate> ExchangeRates { get; set; }
		public DbSet<ISOCountry> IsoCountries { get; set; }
		public DbSet<ISOCountryLocalized> IsoCountriesLocalized { get; set; }
		public DbSet<Itinerary> Itineraries { get; set; }
        public DbSet<Passenger> ItineraryPersons { get; set; }
		public DbSet<Parameter> Parameters { get; set; }
		//public DbSet<Passenger> Passengers { get; set; }
		public DbSet<TaxElement> TaxElements { get; set; }
		public DbSet<TaxElementLocalized> TaxElementsLocalized { get; set; }
        public DbSet<PromotionalDiscount> PromotionalDiscounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BookingMap());
			modelBuilder.Configurations.Add(new BookingItemMap());
			modelBuilder.Configurations.Add(new BookingElementMap());
			modelBuilder.Configurations.Add(new BookingLogMap());
			modelBuilder.Configurations.Add(new ChannelInfoMap());
            modelBuilder.Configurations.Add(new CustomerMap());
			modelBuilder.Configurations.Add(new ExchangeRateMap());
			modelBuilder.Configurations.Add(new ISOCountryMap());
			modelBuilder.Configurations.Add(new ISOCountryLocalizedMap());
            modelBuilder.Configurations.Add(new ItineraryMap());
            modelBuilder.Configurations.Add(new ItineraryPersonMap());
			modelBuilder.Configurations.Add(new ParameterMap());
            modelBuilder.Configurations.Add(new TaxElementMap());
            modelBuilder.Configurations.Add(new TaxElementLocalizedMap());
            modelBuilder.Configurations.Add(new PromotionalDiscountMap());
            modelBuilder.Configurations.Add(new AccommodationPromotionalDiscountMap());

            //Marks accommodation promotion as optional (one-to-zero or one relationship)
            //Marks promotion property as required in accommodation promotion.
            modelBuilder.Entity<PromotionalDiscount>()
                .HasOptional(o => o.AccommodationPromotion)
                .WithRequired(r => r.PromotionalDiscount);

            base.OnModelCreating(modelBuilder);
        }
    }
}