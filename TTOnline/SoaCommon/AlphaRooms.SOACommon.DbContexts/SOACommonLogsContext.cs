﻿using AlphaRooms.SOACommon.DbContexts.Mappings;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.DbContexts
{
    public class SOACommonLogsContext : DbContext
    {
        static SOACommonLogsContext()
        {
            Database.SetInitializer<SOACommonLogsContext>(null);
        }

		public SOACommonLogsContext() : base("Name=AlphabedsLogsEntities")
        {   
        }

        public DbSet<SupplierConnectivityLog> SupplierConnectivityLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SupplierConnectivityLogMap());
            base.OnModelCreating(modelBuilder);
        }

        public async Task SupplierConnectivityLogsSaveAsync(string hostName, int supplierType, int supplierId, int serviceType, DateTime logDate
            , int successCount, int failCount, int averageTimeTaken, string exception)
        {
            await ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommandAsync("SupplierConnectivityLogsSave @HostName, @SupplierType, @SupplierId, @ServiceType, @LogDate, @SuccessCount, @FailCount, @AverageTimeTaken, @Exception"
                , new SqlParameter("HostName", hostName)
                , new SqlParameter("SupplierType", supplierType)
                , new SqlParameter("SupplierId", supplierId)
                , new SqlParameter("serviceType", serviceType)
                , new SqlParameter("LogDate", new DateTime(logDate.Year, logDate.Month, logDate.Day, logDate.Hour, logDate.Minute, 0))
                , new SqlParameter("SuccessCount", successCount)
                , new SqlParameter("FailCount", failCount)
                , new SqlParameter("AverageTimeTaken", averageTimeTaken)
                , new SqlParameter("Exception", (exception != null ? (object)exception : DBNull.Value))
                );
        }
    }
}
