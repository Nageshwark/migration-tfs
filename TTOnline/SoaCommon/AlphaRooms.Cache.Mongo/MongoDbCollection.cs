﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;

    public class MongoDbCollection<T> : IMongoDbCollection<T>
    {
        private readonly IMongoDbCollectionSettings settings;
        private readonly IMongoDbConnection connection;

        public MongoDbCollection(IMongoDbCollectionSettings settings, IMongoDbConnection dbConnection)
        {
            this.settings = settings;
            this.connection = dbConnection;
        }

        //public IMongoCollection<T> GetCollection()
        //{
        //    return this.collection;
        //}

        public IMongoCollection<T> GetCollection(bool isMeta = false)
        {
            return this.connection.GetCollection<T>(this.settings.MongoDatabaseName, this.settings.MongoCollectionName, isMeta);
        }

        public bool IsServerDateUTC { get { return this.connection.IsServerDateUTC; } }
    }
}
