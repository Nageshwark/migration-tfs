﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class MongoDbQuery<T> : ICacheQueryExpireAsync<T> where T : ICacheExpire
    {
        private readonly IMongoDbCollection<T> mongoDbCollection;
        private readonly IMongoCollection<T> collection;
        private readonly IMongoCollection<T> collectionForMeta;

        public MongoDbQuery(IMongoDbCollection<T> mongoDbCollection)
        {
            this.mongoDbCollection = mongoDbCollection;
            this.collection = mongoDbCollection.GetCollection();
            this.collectionForMeta = mongoDbCollection.GetCollection(true);
        }

        private IMongoCollection<T> GetCollection(bool isMeta)
        {
            if (isMeta)
            {
                return this.collectionForMeta;
            }
            else
            {
                return this.collection;
            }
        }

        public async Task<T> GetFirstItemOrNullAsync(Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await GetCollection(isMeta).Find(query).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstItemOrNullAsync(Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false)
        {
            var items = GetCollection(isMeta).Find(query);
            items = (sort.Order == CacheSortOrder.Asc ? items.SortBy(sort.Field) : items.SortByDescending(sort.Field));
            return await items.FirstOrDefaultAsync();
        }

        public async Task<T> GetItemAsync(Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await GetCollection(isMeta).Find(query).SingleAsync();
        }

        public async Task<T> GetItemOrNullAsync(Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await GetCollection(isMeta).Find(query).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await GetCollection(isMeta).Find(query).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false)
        {
            var items = GetCollection(isMeta).Find(query);
            items = (sort.Order == CacheSortOrder.Asc ? items.SortBy(sort.Field) : items.SortByDescending(sort.Field));
            return await items.ToListAsync();
        }

        public async Task<LimitedResults<T>> GetLimitedItemsAsync(Expression<Func<T, bool>> query, int startIndex, int count, bool isMeta = false)
        {
            var items = GetCollection(isMeta).Find(query);
            return new LimitedResults<T>(await items.Skip(startIndex).Limit(count).ToListAsync(), (int)await items.CountAsync());
        }

        public async Task<LimitedResults<T>> GetLimitedItemsAsync(Expression<Func<T, bool>> query, int startIndex, int count, ICacheSort<T> sort, bool isMeta = false)
        {
            var items = GetCollection(isMeta).Find(query);
            var sortedItems = (sort.Order == CacheSortOrder.Asc ? items.SortBy(sort.Field) : items.SortByDescending(sort.Field));
            return new LimitedResults<T>(await sortedItems.Skip(startIndex).Limit(count).ToListAsync(), (int)await items.CountAsync());
        }

        public DateTime GetCacheExpireDateThreshold()
        {
            return (this.mongoDbCollection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now);
        }

        public bool IsCacheExpireDateExpired(DateTime cacheExpireDate)
        {
            return (cacheExpireDate < (this.mongoDbCollection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now));
        }
    }
}
