﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.Cache.Mongo.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using MongoDB.Driver;

    public class MongoDbMultiDbQuery<T, K> : ICacheMultiDbQueryExpireAsync<T, K> where T : ICacheExpire
    {
        private readonly IMongoDbMultiDbCollection<T, K> collection;
 
        public MongoDbMultiDbQuery(IMongoDbMultiDbCollection<T, K> collection)
        {
            this.collection = collection;
        }

        private IMongoCollection<T> GetCollection(K databaseKey, bool isMeta = false)
        {
            return this.collection.GetCollection(databaseKey, isMeta);
        }

        public async Task<T> GetFirstItemOrNullAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await this.GetCollection(databaseKey, isMeta).Find(query).FirstOrDefaultAsync();
        }

        public async Task<T> GetItemAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await this.GetCollection(databaseKey, isMeta).Find(query).SingleAsync();
        }

        public async Task<T> GetItemOrNullAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await this.GetCollection(databaseKey, isMeta).Find(query).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false)
        {
            return await this.GetCollection(databaseKey, isMeta).Find(query).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync(K databaseKey, Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false)
        {
            var items = this.GetCollection(databaseKey, isMeta).Find(query);
            items = (sort.Order == CacheSortOrder.Asc ? items.SortBy(sort.Field) : items.SortByDescending(sort.Field));
            return await items.ToListAsync();
        }

        public async Task<LimitedResults<T>> GetLimitedItemsAsync(K databaseKey, Expression<Func<T, bool>> query, int startIndex, int count, bool isMeta = false)
        {
            var items = this.GetCollection(databaseKey, isMeta).Find(query);
            var itemsCount = (int)await items.CountAsync();
            return new LimitedResults<T>(await items.Skip(startIndex).Limit(count).ToListAsync(), itemsCount);
        }

        public async Task<LimitedResults<T>> GetLimitedItemsAsync(K databaseKey, Expression<Func<T, bool>> query, int startIndex, int count, ICacheSort<T> sort, bool isMeta = false)
        {
            var items = this.GetCollection(databaseKey, isMeta).Find(query);
            var sortedItems = (sort.Order == CacheSortOrder.Asc ? items.SortBy(sort.Field) : items.SortByDescending(sort.Field));
            var itemsCount = (int)await items.CountAsync();
            return new LimitedResults<T>(await sortedItems.Skip(startIndex).Limit(count).ToListAsync(), itemsCount);
        }

        public DateTime GetCacheExpireDateThreshold()
        {
            return (this.collection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now);
        }

        public bool IsCacheExpireDateExpired(DateTime cacheExpireDate)
        {
            return (cacheExpireDate < (this.collection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now));
        }
    }
}
