﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Interfaces;
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class MongoDbWriter<T> : ICacheWriterExpireAsync<T> where T : ICacheExpire
    {
        private readonly IMongoDbCollection<T> connection;
        private readonly IMongoCollection<T> collection;
        private readonly IMongoCollection<T> collectionForMeta;

        public MongoDbWriter(IMongoDbCollection<T> connection)
        {
            this.connection = connection;
            this.collection = connection.GetCollection();
            this.collectionForMeta = connection.GetCollection(true);
        }

        private IMongoCollection<T> GetCollection(bool isMeta)
        {
            if (isMeta)
            {
                return this.collectionForMeta;
            }
            else
            {
                return this.collection;
            }
        }

        public async Task AddAsync(T value, bool isMeta = false)
        {
            await GetCollection(isMeta).InsertOneAsync(value);
        }
        
        public async Task AddAsync(T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await GetCollection(isMeta).InsertOneAsync(value);
        }

        public async Task AddIfDoesntExistAsync(T value, bool isMeta = false)
        {
            try
            {
                await GetCollection(isMeta).InsertOneAsync(value);
            }
            catch
            {
            }
        }

        public async Task AddIfDoesntExistAsync(T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            try
            {
                await GetCollection(isMeta).InsertOneAsync(value);
            }
            catch
            {
            }
        }

        public async Task AddOrUpdateAsync(Expression<Func<T, bool>> filter, T value, bool isMeta = false)
        {
            await GetCollection(isMeta).ReplaceOneAsync(filter, value, new UpdateOptions() { IsUpsert = true });
        }

        public async Task AddOrUpdateAsync(Expression<Func<T, bool>> filter, T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await GetCollection(isMeta).ReplaceOneAsync(filter, value, new UpdateOptions() { IsUpsert = true });
        }

        public async Task AddRangeAsync(IEnumerable<T> values, bool isMeta = false)
        {
            await GetCollection(isMeta).InsertManyAsync(values);
        }

        public async Task AddRangeAsync(IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            foreach(var value in values) value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await GetCollection(isMeta).InsertManyAsync(values);
        }

        public async Task AddRangeIfDoesntExistAsync(IEnumerable<T> values, bool isMeta = false)
        {
            try
            {
                await GetCollection(isMeta).InsertManyAsync(values, new InsertManyOptions() { IsOrdered = false });
            }
            catch
            {
            }
        }

        public async Task AddRangeIfDoesntExistAsync(IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            foreach (var value in values) value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            try
            {
                await GetCollection(isMeta).InsertManyAsync(values, new InsertManyOptions() { IsOrdered = false });
            }
            catch
            {
            }
        }

        public async Task AddOrUpdateRangeAsync(Expression<Func<T, bool>> filter, IEnumerable<T> values, bool isMeta = false)
        {
            throw new NotImplementedException();
        }

        public async Task AddOrUpdateRangeAsync(Expression<Func<T, bool>> filter, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(Expression<Func<T, bool>> filter, bool isMeta = false)
        {
            await GetCollection(isMeta).DeleteManyAsync(filter);
        }

        public void SetCacheExpireTimeout(T value, TimeSpan cacheExpireTimeout)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
        }

        private DateTime GetCacheExpireDate(TimeSpan cacheExpireTimeout)
        {
            return ((this.connection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now) + cacheExpireTimeout);
        }
    }
}
