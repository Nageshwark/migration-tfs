﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;

    public class MongoDbConnection : IMongoDbConnection
    {
        private readonly IMongoDbSettings settings;
        private readonly IMongoClient server;
        private readonly IMongoClient metaserver;

        public MongoDbConnection(IMongoDbSettings settings)
        {
            this.settings = settings;
            this.server = new MongoClient(settings.MongoConnectionString);
            this.metaserver = new MongoClient(settings.MongoConnectionStringForMeta);
        }

        public IMongoCollection<T> GetCollection<T>(string mongoDatabaseName, string mongoCollectionName, bool isMeta = false)
        {
            if (!isMeta)
            {
                return this.server.GetDatabase(mongoDatabaseName).GetCollection<T>(mongoCollectionName);
            }
            else
            {
                return this.metaserver.GetDatabase(mongoDatabaseName).GetCollection<T>(mongoCollectionName);
            }
        }

        public bool IsServerDateUTC { get { return this.settings.CacheServerIsDateUTC; } }
    }
}
