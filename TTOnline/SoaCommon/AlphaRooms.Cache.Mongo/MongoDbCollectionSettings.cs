﻿using AlphaRooms.Cache.Mongo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Mongo
{
    public class MongoDbCollectionSettings : IMongoDbCollectionSettings
    {
        private readonly string mongoDatabaseName;
        private readonly string mongoCollectionName;

        public MongoDbCollectionSettings(string mongoDatabaseName, string mongoCollectionName)
        {
            this.mongoDatabaseName = mongoDatabaseName;
            this.mongoCollectionName = mongoCollectionName;
        }
        
        public string MongoDatabaseName { get { return this.mongoDatabaseName; } }

        public string MongoCollectionName { get { return this.mongoCollectionName; } }
    }
}
