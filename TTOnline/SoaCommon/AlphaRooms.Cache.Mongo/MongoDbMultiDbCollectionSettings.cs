﻿using AlphaRooms.Cache.Mongo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Mongo
{
    public class MongoDbMultiDbCollectionSettings : IMongoDbMultiDbCollectionSettings
    {
        private readonly string mongoDatabaseName;
        private readonly string mongoCollectionName;
        private readonly int multiDatabasesCount;
        private readonly bool multiDatabasesIsEnabled;

        public MongoDbMultiDbCollectionSettings(string mongoDatabaseName, string mongoCollectionName, int multiDatabasesCount, bool multiDatabasesIsEnabled)
        {
            this.mongoDatabaseName = mongoDatabaseName;
            this.mongoCollectionName = mongoCollectionName;
            this.multiDatabasesCount = multiDatabasesCount;
            this.multiDatabasesIsEnabled = multiDatabasesIsEnabled;
        }

        public string MongoDatabaseName { get { return this.mongoDatabaseName; } }

        public string MongoCollectionName { get { return this.mongoCollectionName; } }

        public int MultiDatabasesCount { get { return this.multiDatabasesCount; } }

        public bool MultiDatabasesIsEnabled { get { return this.multiDatabasesIsEnabled; } }
    }
}
