﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq.Expressions;
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;

    public class MongoDbMultiDbWriter<T, K> : ICacheMultiDbWriterExpireAsync<T, K> where T : ICacheExpire
    {
        private readonly IMongoDbMultiDbCollection<T, K> collection;

        public MongoDbMultiDbWriter(IMongoDbMultiDbCollection<T, K> collection)
        {
            this.collection = collection;
        }

        private IMongoCollection<T> GetCollection(K databaseKey, bool isMeta = false)
        {
            return this.collection.GetCollection(databaseKey, isMeta);
        }
        
        public async Task AddAsync(K databaseKey, T value, bool isMeta = false)
        {
            await this.GetCollection(databaseKey, isMeta).InsertOneAsync(value);
        }

        public async Task AddAsync(K databaseKey, T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await this.GetCollection(databaseKey, isMeta).InsertOneAsync(value);
        }

        public async Task AddIfDoesntExistAsync(K databaseKey, T value, bool isMeta = false)
        {
            try
            {
                await this.GetCollection(databaseKey, isMeta).InsertOneAsync(value);
            }
            catch
            {
            }
        }

        public async Task AddIfDoesntExistAsync(K databaseKey, T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            try
            {
                await this.GetCollection(databaseKey, isMeta).InsertOneAsync(value);
            }
            catch
            {
            }
        }

        public async Task AddOrUpdateAsync(K databaseKey, Expression<Func<T, bool>> filter, T value, bool isMeta = false)
        {
            await this.GetCollection(databaseKey, isMeta).ReplaceOneAsync(filter, value, new UpdateOptions() { IsUpsert = true });
        }

        public async Task AddOrUpdateAsync(K databaseKey, Expression<Func<T, bool>> filter, T value, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await this.GetCollection(databaseKey, isMeta).ReplaceOneAsync(filter, value, new UpdateOptions() { IsUpsert = true });
        }

        public async Task AddRangeAsync(K databaseKey, IEnumerable<T> values, bool isMeta = false)
        {
            await this.GetCollection(databaseKey, isMeta).InsertManyAsync(values);
        }

        public async Task AddRangeAsync(K databaseKey, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            foreach (var value in values) value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            await this.GetCollection(databaseKey, isMeta).InsertManyAsync(values);
        }

        public async Task AddRangeIfDoesntExistAsync(K databaseKey, IEnumerable<T> values, bool isMeta = false)
        {
            try
            {
                await this.GetCollection(databaseKey, isMeta).InsertManyAsync(values, new InsertManyOptions() { IsOrdered = false });
            }
            catch
            {
            }
        }

        public async Task AddRangeIfDoesntExistAsync(K databaseKey, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false)
        {
            foreach (var value in values) value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
            try
            {
                await this.GetCollection(databaseKey, isMeta).InsertManyAsync(values, new InsertManyOptions() { IsOrdered = false });
            }
            catch
            {
            }
        }

        public async Task AddOrUpdateRangeAsync(K databaseKey, Expression<Func<T, bool>> filter, IEnumerable<T> values)
        {
            throw new NotImplementedException();
        }

        public async Task AddOrUpdateRangeAsync(K databaseKey, Expression<Func<T, bool>> filter, IEnumerable<T> values, TimeSpan cacheExpireTimeout)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(K databaseKey, Expression<Func<T, bool>> filter, bool isMeta = false)
        {
            await this.collection.GetCollection(databaseKey, isMeta).DeleteManyAsync(filter);
        }

        public void SetCacheExpireTimeout(T value, TimeSpan cacheExpireTimeout)
        {
            value.CacheExpireDate = GetCacheExpireDate(cacheExpireTimeout);
        }

        private DateTime GetCacheExpireDate(TimeSpan cacheExpireTimeout)
        {
            return ((this.collection.IsServerDateUTC ? DateTime.UtcNow : DateTime.Now) + cacheExpireTimeout);
        }
    }
}
