﻿namespace AlphaRooms.Cache.Mongo
{
    using AlphaRooms.Cache.Mongo.Interfaces;
    using MongoDB.Driver;
    using AlphaRooms.Cache.Interfaces;

    public class MongoDbMultiDbCollection<T, K> : IMongoDbMultiDbCollection<T, K>
    {
        private readonly IMongoDbMultiDbCollectionSettings settings;
        private readonly IMongoDbConnection connection;
        private readonly IMongoCollection<T>[] collections;
        private readonly IMongoCollection<T>[] collectionsMeta;
        private readonly ICacheMultiDatabaseController<T, K> databaseController;

        public MongoDbMultiDbCollection(IMongoDbMultiDbCollectionSettings settings, IMongoDbConnection dbConnection, ICacheMultiDatabaseController<T, K> databaseController)
        {
            this.settings = settings;
            this.connection = dbConnection;
            this.databaseController = databaseController;
            this.collections = GetMongoCollections(settings);
            this.collectionsMeta = GetMongoCollections(settings, true);
        }

        private IMongoCollection<T>[] GetMongoCollections(IMongoDbMultiDbCollectionSettings settings, bool isMeta = false)
        {
            if (!settings.MultiDatabasesIsEnabled)
            {
                var singleCollections = new IMongoCollection<T>[1];
                singleCollections[0] = this.connection.GetCollection<T>(settings.MongoDatabaseName, settings.MongoCollectionName, isMeta);
                return singleCollections;
            }
            var collections = new IMongoCollection<T>[this.settings.MultiDatabasesCount];
            for (int i = 0; i < collections.Length; i++)
            {
                collections[i] = this.connection.GetCollection<T>(settings.MongoDatabaseName + "_" + (i + 1).ToString("00"), settings.MongoCollectionName, isMeta);
            }
            return collections;
        }

        public IMongoCollection<T> GetCollection(K databaseKey, bool isMeta = false)
        {
            var collections = (isMeta ? this.collectionsMeta : this.collections) ;

            if (!this.settings.MultiDatabasesIsEnabled)
            {
                return collections[0];
            }
            var i = this.databaseController.GetDatabaseIndexByKey(this.settings.MultiDatabasesCount, databaseKey);
            if (i < 0)
            {
                i = 0;
            }
            if (i >= this.settings.MultiDatabasesCount)
            {
                i = this.settings.MultiDatabasesCount - 1;
            }
            return collections[i];
        }

        public bool IsServerDateUTC { get { return this.connection.IsServerDateUTC; } }
    }
}
