﻿namespace AlphaRooms.Cache.Mongo.Interfaces
{
    using MongoDB.Driver;

    public interface IMongoDbCollection<T>
    {
        IMongoCollection<T> GetCollection(bool isAlternative = false);
        bool IsServerDateUTC { get; }
    }
}
