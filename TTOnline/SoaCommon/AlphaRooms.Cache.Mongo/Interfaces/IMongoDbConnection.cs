﻿namespace AlphaRooms.Cache.Mongo.Interfaces
{
    using MongoDB.Driver;

    public interface IMongoDbConnection
    {
        IMongoCollection<T> GetCollection<T>(string mongoDatabaseName, string mongoCollectionName, bool isAlternative = false);
        bool IsServerDateUTC { get; }
    }
}
