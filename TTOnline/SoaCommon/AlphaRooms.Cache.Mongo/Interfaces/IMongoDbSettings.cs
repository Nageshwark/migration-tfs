﻿namespace AlphaRooms.Cache.Mongo.Interfaces
{
    public interface IMongoDbSettings
    {
        bool CacheServerIsDateUTC { get; }
        string MongoConnectionString { get; }
        string MongoConnectionStringForMeta { get; }
        
    }
}
