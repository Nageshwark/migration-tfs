﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Mongo.Interfaces
{
    public interface IMongoDbMultiDbCollectionSettings : IMongoDbCollectionSettings
    {
        bool MultiDatabasesIsEnabled { get; }
        int MultiDatabasesCount { get; }
    }
}
