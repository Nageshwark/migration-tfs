﻿namespace AlphaRooms.Cache.Mongo.Interfaces
{
    using MongoDB.Driver;

    public interface IMongoDbMultiDbCollection<T,K>
    {
        IMongoCollection<T> GetCollection(K databaseKey, bool isMeta = false);
        bool IsServerDateUTC { get; }
    }
}
