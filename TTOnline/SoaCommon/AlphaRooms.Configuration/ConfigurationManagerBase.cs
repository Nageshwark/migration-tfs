﻿namespace AlphaRooms.Configuration
{
    using AlphaRooms.SOACommon.Interfaces;
    using Ninject.Extensions.Logging;
    using System;
    using System.Collections.Concurrent;
    using System.Configuration;

    public class ConfigurationManagerBase : IConfigurationManager
    {
        private readonly ConcurrentDictionary<string, ConfigurationItem> cachedSettings = new ConcurrentDictionary<string, ConfigurationItem>();

        private readonly ILogger logger;
        private readonly IParameterRepository parameterRepository;

        public ConfigurationManagerBase(ILogger logger, IParameterRepository parameterRepository)
        {
            this.logger = logger;
            this.parameterRepository = parameterRepository;
            this.TimeToLive = TimeSpan.FromMinutes(15);
        }

        public TimeSpan TimeToLive
        {
            get;
            set;
        }

        #region Dynamic Accessors

        public string this[string key]
        {
            get
            {
                ConfigurationItem item = null;

                this.cachedSettings.TryGetValue(key, out item);

                // check if it needs to be refreshed or needs to be loaded
                if (item == null || item.TimeStamp < DateTime.UtcNow - this.TimeToLive)
                {
                    item = this.LoadKey(key);
                    if (item == null)
                    {
                        this.logger.Info("Configuration parameter key [{0}] not found in the config file or the database.\n\n{1}", key, Environment.StackTrace);
                        return null;
                    }
                }

                return item.Value;
            }
        }

        public string this[string key, string defaultValue]
        {
            get
            {
                var value = this[key];

                // if not found anywhere, return the default
                if (value == null)
                {
                    this.logger.Info("Configuration parameter key [{0}] not found in the cache, default value of [{1}] returned.", key, defaultValue);
                    return defaultValue;
                }

                return value;
            }
        }

        #endregion

        #region Private Functions

        private ConfigurationItem LoadKey(string key)
        {
            // try the config first cos that overrides the database
            var item = this.LoadKeyFromConfig(key);

            // if not in the config, get it from the database
            if (item == null)
            {
                try
                {
                    var configValue = this.parameterRepository.GetParameterByName(key);

                    if (configValue != null)
                    {
                        item = new ConfigurationItem(configValue.ParameterValue);
                    }
                }
                catch (Exception ex)
                {
                    /*
                     * We need to swallow this exception as the AlphaRooms.Utilities.CacheProvider throwns an exception if it cannot find a value. 
                     * Not ideal, it also throws a raw exception not derivative bad.
                     */
                    this.logger.Error(ex.ToString());
                }
            }

            // add it to the collection
            if (item != null)
            {
                this.cachedSettings[key] = item;
            }

            return item;
        }

        private ConfigurationItem LoadKeyFromConfig(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (value != null)
            {
                return new ConfigurationItem(value);
            }

            return null;
        }

        #endregion

        private class ConfigurationItem
        {
            public ConfigurationItem(string value)
            {
                this.Value = value;
                this.TimeStamp = DateTime.UtcNow;
            }

            public string Value { get; private set; }
            public DateTime TimeStamp { get; private set; }
        }
    }
}