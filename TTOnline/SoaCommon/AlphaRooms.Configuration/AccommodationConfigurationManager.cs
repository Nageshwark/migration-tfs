﻿namespace AlphaRooms.Configuration
{
    using System;
    using System.Linq;
    using AlphaRooms.SOACommon.Interfaces;
    using Ninject.Extensions.Logging;
    using AlphaRooms.Cache.Mongo.Interfaces;

    public class AccommodationConfigurationManager : ConfigurationManagerBase, IAccommodationConfigurationManager, IMongoDbSettings, IProviderOutputLoggerSettings, ISmtpServerSettings

    {
        public AccommodationConfigurationManager(ILogger logger, IParameterRepository parameterRepository)
            : base(logger, parameterRepository)
        {

        }

        public string AccommodationCacheImplementation
        {
            get { return this["AccommodationCacheImplementation", "MongoDb"]; }
        }

        public int[] AccommodationProvidersToSearch
        {
            get
            {
                var providerIds = this["AccommodationProvidersToSearch", ""];
                if (string.IsNullOrEmpty(providerIds)) return null;
                return providerIds.Split(',').Select(i => int.Parse(i)).ToArray();
            }
        }

        public int[] TravelGateProvider
        {
            get
            {
                var providerIds = this["TravelGateProvider", "36"];
                return providerIds.Split(',').Select(i => int.Parse(i)).ToArray();
            }
        }

        public string MongoConnectionString
        {
            get { return this["AccommodationMongoConnectionString", "mongodb://localhost"]; }
        }

        public string MongoConnectionStringForMeta
        {
            get { return this["AccommodationMongoConnectionStringForMeta", "mongodb://localhost"]; }
        }

        public bool CacheServerIsDateUTC
        {
            get { return bool.Parse(this["AccommodationCacheServerIsDateUTC", "false"]); }
        }

        public int SmtpPort
        {
            get { return int.Parse(this["AccommodationProviderSMTPPort", "25"]); }
        }

        public string SmtpHost
        {
            get { return this["AccommodationProviderSMTPHost", "192.168.1.6"]; }
        }

        public string SmtpUsername
        {
            get { return this["AccommodationProviderSMTPUserName", "User@alpharooms.com"]; }
        }

        public int TTB2BUserId
        {
            get { return int.Parse(this["TTB2BUserId"]); }
        }

        public string SmtpPassword
        {
            get { return this["AccommodationProviderSMTPPassword", "Password"]; }
        }

        public bool SmtpIsTlsOn
        {
            get { return bool.Parse(this["AccommodationProviderSMTPTLSOn", "true"]); }
        }

        private string accommodationProviderOutputPath;

        public string ProviderOutputPath
        {
            get { return (accommodationProviderOutputPath != null ? accommodationProviderOutputPath : accommodationProviderOutputPath = this["AccommodationProviderOutputPath", string.Empty]); }
        }

        public string AccommodationHostNameTag
        {
            get { return this["AccommodationHostNameTag", null]; }
        }

        public decimal AccommodationVatPercentage
        {
            get { return decimal.Parse(this["AccommodationVatPercentage", "20"]); }
        }

		public string[] ExcludeSuppliersToApplyPriceMarkup
		{
			get
			{
				var excludeSuppliersToApplyPriceMarkup = this["ExcludeSuppliersToApplyPriceMarkup", ""];
				if (string.IsNullOrEmpty(excludeSuppliersToApplyPriceMarkup)) return null;
				return excludeSuppliersToApplyPriceMarkup.Split(',');
			}
		}

		public bool B2CEventLogInfoIsEnabled => bool.Parse(this["B2CEventLogInfoIsEnabled", "true"]);

		//  Core
		public decimal SupplierValuationThresholdDefault
        {
            get { return decimal.Parse(this["SupplierValuationThresholdDefault", "1"]); }
        }

        public byte AccommodationAdultAge
        {
            get { return byte.Parse(this["AccommodationAdultAge", "30"]); }
        }

        public byte AccommodationMaxInfantAge
        {
            get { return byte.Parse(this["AccommodationMaxInfantAge", "1"]); }
        }

        public byte AccommodationMaxChildAge
        {
            get { return byte.Parse(this["AccommodationMaxChildAge", "17"]); }
        }

        public byte AccommodationMaxRooms
        {
            get { return byte.Parse(this["AccommodationMaxRooms", "5"]); }
        }

        public string AccommodationAvailabilityCachedRequestCollection
        {
            get { return this["AccommodationAvailabilityCachedRequestCollection", "AccommodationCachedRequests"]; }
        }

        public string AccommodationAvailabilityResultsDatabase
        {
            get { return this["AccommodationAvailabilityResultsDatabase", "accommodationresults"]; }
        }

        public string AccommodationAvailabilityResultsCollection
        {
            get { return this["AccommodationAvailabilityResultsCollection", "AccommodationSearchResults"]; }
        }

        public int AccommodationAvailabilityResultsMultiDbCount
        {
            get { return int.Parse(this["AccommodationAvailabilityResultsMultiDbCount", "10"]); }
        }

        public bool AccommodationAvailabilityResultsMultiDbIsEnabled
        {
            get { return bool.Parse(this["AccommodationAvailabilityResultsMultiDbIsEnabled", "true"]); }
        }

        public TimeSpan AccommodationAvailabilitySearchTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationAvailabilitySearchTimeout", "00:00:30"]); }
        }

        public TimeSpan AccommodationAvailabilityEmptyResultsExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationAvailabilityEmptyResultsExpireTimeout", "00:05:00"]); }
        }

        public TimeSpan AccommodationValuationTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationValuationTimeout", "00:00:30"]); }
        }

        public bool AccommodationRepositoryIsCachingEnabled
        {
            get { return bool.Parse(this["AccommodationRepositoryIsCachingEnabled", "true"]); }
        }

        public TimeSpan AccommodationRepositoryCacheExpireTime
        {
            get { return TimeSpan.Parse(this["AccommodationRepositoryCacheExpireTime", "01:00:00"]); }
        }

        public TimeSpan AccommodationRepositoryCachingTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationRepositoryCachingTimeout", "00:00:05"]); }
        }

        public TimeSpan AccommodationRepositoryCommandTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationRepositoryCommandTimeout", "00:00:05"]); }
        }

        public string AccommodationValuationResultsDatabase
        {
            get { return this["AccommodationValuationResultsDatabase", "accommodationresults"]; }
        }

        public string AccommodationValuationResultsCollection
        {
            get { return this["AccommodationValuationResultsCollection", "AccommodationValuationResults"]; }
        }

        public TimeSpan AccommodationValuationResultsExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationValuationResultsExpireTimeout", "01:00:00"]); }
        }

        public string AccommodationBookingResultsDatabase
        {
            get { return this["AccommodationBookingResultsDatabase", "accommodationresults"]; }
        }

        public string AccommodationBookingResultsCollection
        {
            get { return this["AccommodationBookingResultsCollection", "AccommodationBookingResults"]; }
        }

        public TimeSpan AccommodationBookingResultsExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationBookingResultsExpireTimeout", "01:00:00"]); }
        }

        public decimal AccommodationCancellationFee_UK
        {
            get { return decimal.Parse(this["AccommodationCancellationFee_UK", "50"]); }
        }

        public decimal AccommodationCancellationFee_IE
        {
            get { return decimal.Parse(this["AccommodationCancellationFee_IE", "50"]); }
        }

        public decimal AccommodationCancellationFee_US
        {
            get { return decimal.Parse(this["AccommodationCancellationFee_US", "50"]); }
        }

        public int AccommodationNoRefundCancellationDaysBeforeDeparture
        {
            get { return int.Parse(this["AccommodationNoRefundCancellationDaysBeforeDeparture", "7"]); }
        }

        public string AccommodationCancellationResultsDatabase
        {
            get { return this["AccommodationCancellationResultsDatabase", "accommodationresults"]; }
        }

        public string AccommodationCancellationResultsCollection
        {
            get { return this["AccommodationCancellationResultsCollection", "AccommodationCancellationResults"]; }
        }

        public TimeSpan AccommodationCancellationResultsExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationCancellationResultsExpireTimeout", "01:00:00"]); }
        }

        public decimal AccommodationBaseDiscount
        {
            get { return decimal.Parse(this["AccommodationBaseDiscount", "6.66"]); }
        }

        public string AccommodationRoomDescriptionNonRefundableLabel
        {
            get { return this["AccommodationRoomDescriptionNonRefundableLabel", " - Non Refundable"]; }
        }

        public string[] AccommodationRoomDescriptionNonRefundableKeyword
        {
            get { return this["AccommodationRoomDescriptionNonRefundableKeyword", "non,refundable"].Split(','); }
        }

        public bool AccommodationPriceHikingIsEnabled
        {
            get { return bool.Parse(this["AccommodationPriceHikingIsEnabled", "true"]); }
        }

        public int[] AccommodationPriceHikingProviderIds
        {
            get { return this["AccommodationPriceHikingProviderIds", "1"].Split(',').Select(i => int.Parse(i)).ToArray(); }
        }

        public decimal AccommodationPriceHikingMaxHikingPercent
        {
            get { return decimal.Parse(this["AccommodationPriceHikingMaxHikingPercent", "20"]); }
        }

        public bool AccommodationRoomSwitchingIsEnabled
        {
            get { return bool.Parse(this["AccommodationRoomSwitchingIsEnabled", "true"]); }
        }

        public string AccommodationCachedRequestDatabase
        {
            get { return this["AccommodationRequestStatusDatabase", "accommodationresults"]; }
        }

        public string AccommodationCachedRequestCollection
        {
            get { return this["AccommodationRequestStatusCollection", " AccommodationCachedRequests"]; }
        }

        public TimeSpan AccommodationCachedRequestExpireThreshold
        {
            get { return TimeSpan.Parse(this["AccommodationCachedRequestExpireThreshold", "00:05:00"]); }
        }


        public bool AccommodationBookingDisableAll
        {
            get { return bool.Parse(this["AccommodationBookingDisableAll", "false"]); }
        }

        //  B2C

        public int AccommodationB2CAvailabilityResultsNumberOfRooms
        {
            get { return int.Parse(this["AccommodationB2CAvailabilityResultsNumberOfRooms", "5"]); }
        }

        public string AccommodationB2CRequestStatusDatabase
        {
            get { return this["AccommodationB2CRequestStatusDatabase", "accommodationb2cresults"]; }
        }

        public string AccommodationB2CRequestStatusCollection
        {
            get { return this["AccommodationB2CRequestStatusCollection", "AccommodationB2CRequestStatuses"]; }
        }

        public TimeSpan AccommodationB2CRequestStatusExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationB2CRequestStatusExpireTimeout", "02:00:00"]); }
        }

        public string AccommodationB2CAvailabilityResultsDatabase
        {
            get { return this["AccommodationB2CAvailabilityResultsDatabase", "accommodationb2cresults"]; }
        }

        public string AccommodationB2CAvailabilityResultsCollection
        {
            get { return this["AccommodationB2CAvailabilityResultsCollection", "AccommodationB2CSearchResults"]; }
        }

        public int AccommodationB2CAvailabilityResultsMultiDbCount
        {
            get { return int.Parse(this["AccommodationB2CAvailabilityResultsMultiDbCount", "10"]); }
        }

        public bool AccommodationB2CAvailabilityResultsMultiDbIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2CAvailabilityResultsMultiDbIsEnabled", "true"]); }
        }

        public bool AccommodationB2CAvailabilityLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2CAvailabilityLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2CUseHmdForMarkup
        {
            get { return bool.Parse(this["AccommodationB2CUseHmdForMarkup", "false"]); }
        }

        public bool AccommodationB2CValuationLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2CValuationLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2CBookingLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2CBookingLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2CConsolidationIncludePaymentModel
        {
            get { return bool.Parse(this["AccommodationB2CConsolidationIncludePaymentModel", "false"]); }
        }

        public TimeSpan AccommodationB2CRequestStatusExpireThreshold
        {
            get { return TimeSpan.Parse(this["AccommodationB2CRequestStatusExpireThreshold", "00:05:00"]); }
        }

        // B2B 

        //  B2BWeb

        public int AccommodationB2BWebAvailabilityResultsNumberOfRooms
        {
            get { return int.Parse(this["AccommodationB2BWebAvailabilityResultsNumberOfRooms", "5"]); }
        }

        public string AccommodationB2BWebRequestStatusDatabase
        {
            get { return this["AccommodationB2BWebRequestStatusDatabase", "accommodationb2bresults"]; }
        }

        public string AccommodationB2BWebRequestStatusCollection
        {
            get { return this["AccommodationB2BWebRequestStatusCollection", "AccommodationB2BWebRequestStatuses"]; }
        }

        public TimeSpan AccommodationB2BWebRequestStatusExpireTimeout
        {
            get { return TimeSpan.Parse(this["AccommodationB2BWebRequestStatusExpireTimeout", "02:00:00"]); }
        }

        public string AccommodationB2BWebAvailabilityResultsDatabase
        {
            get { return this["AccommodationB2BWebAvailabilityResultsDatabase", "accommodationb2bresults"]; }
        }

        public string AccommodationB2BWebAvailabilityResultsCollection
        {
            get { return this["AccommodationB2BWebAvailabilityResultsCollection", "AccommodationB2BWebSearchResults"]; }
        }

        public int AccommodationB2BWebAvailabilityResultsMultiDbCount
        {
            get { return int.Parse(this["AccommodationB2BWebAvailabilityResultsMultiDbCount", "10"]); }
        }

        public bool AccommodationB2BWebAvailabilityResultsMultiDbIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BWebAvailabilityResultsMultiDbIsEnabled", "true"]); }
        }

        public bool AccommodationB2BWebAvailabilityLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BWebAvailabilityLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2BWebUseHmdForMarkup
        {
            get { return bool.Parse(this["AccommodationB2BWebUseHmdForMarkup", "false"]); }
        }

        public bool AccommodationB2BWebValuationLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BWebValuationLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2BWebBookingLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BWebBookingLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2BWebConsolidationIncludePaymentModel
        {
            get { return bool.Parse(this["AccommodationB2BWebConsolidationIncludePaymentModel", "false"]); }
        }

        public TimeSpan AccommodationB2BWebRequestStatusExpireThreshold
        {
            get { return TimeSpan.Parse(this["AccommodationB2BWebRequestStatusExpireThreshold", "00:05:00"]); }
        }

        public string AccommodationB2BWebBookingCustomerContactNumber
        {
            get { return this["AccommodationB2BWebBookingCustomerContactNumber", "00441142515066"]; }
        }

        public string AccommodationB2BWebBookingCustomerEmailAddress
        {
            get { return this["AccommodationB2BWebBookingCustomerEmailAddress", "muhammad.a@alpharooms.com"]; }
        }

        // B2BXml
        
        public bool AccommodationB2BXmlAvailabilityLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BXmlAvailabilityLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2BXmlValuationLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BXmlValuationLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2BXmlBookingLoggerIsEnabled
        {
            get { return bool.Parse(this["AccommodationB2BXmlBookingLoggerIsEnabled", "true"]); }
        }

        public bool AccommodationB2CEnableProviderCache => bool.Parse(this["AccommodationB2CEnableProviderCache", "true"]);
        public bool AccommodationB2BWebEnableProviderCache => bool.Parse(this["AccommodationB2BWebEnableProviderCache", "true"]);
        public bool AccommodationB2BXmlEnableProviderCache => bool.Parse(this["AccommodationB2BXmlEnableProviderCache", "true"]);
        public int AccommodationB2CXMLGlobalLeadTime => int.Parse(this["AccommodationB2CXMLGlobalLeadTime","2"]);
        public int AccommodationB2BXMLGlobalLeadTime => int.Parse(this["AccommodationB2BXMLGlobalLeadTime", "2"]);

        public TimeSpan RefreshBookingAdjustmentsEvery
        {
            get { return TimeSpan.Parse(this["RefreshBookingAdjustmentsEvery", "00:20:00"]); }
        }

		public string[] AccommodationBev5VirtualCardSuppliers
		{
			get
			{
				var bev5VCCSuppliers = this["AccommodationBev5VirtualCardSuppliers", ""];
				if (string.IsNullOrEmpty(bev5VCCSuppliers)) return null;
				return bev5VCCSuppliers.Split(',');
			}
		}
        public bool EnableXTGLogging
        {
            get { return bool.Parse(this["EnableXTGLogging", "false"]); }
        }
    }    
}
