﻿
namespace AlphaRooms.Configuration
{
    using System;
    using System.Linq;

    using AlphaRooms.SOACommon.Interfaces;
    using Ninject.Extensions.Logging;

    public class ProductConfigurationManager : ConfigurationManagerBase, IProductConfigurationManager,IConnectivityLogServiceSettings
    {
        public ProductConfigurationManager(ILogger logger, IParameterRepository parameterRepository)
            : base(logger, parameterRepository)
        {

        }


        #region Named Accessors

        public int ProductAvailabilitySearchTimeout
        {
            get
            {
                return int.Parse(this["ProductAvailabilitySearchTimeout", "20"]);
            }
        }

        public int ProductCachePriceThreshold
        {
            get
            {
                return int.Parse(this["ProductCachePriceThreshold", "80"]);
            }
        }

        public int ProductCacheSearchTimeout
        {
            get
            {
                return int.Parse(this["ProductCacheSearchTimeout", "10"]);
            }
        }

        public int ProductCacheTimeToLive
        {
            get
            {
                return int.Parse(this["ProductCacheTTL", "60"]);
            }
        }

        public bool ProductCachingControl
        {
            get
            {
                return Convert.ToBoolean(this["ProductCachingControl", "true"]);
            }
        }

        public int ProductGroundTimeLimit
        {
            get
            {
                return int.Parse(this["ProductGroundTimeLimit", "5"]);
            }
        }

        public string ConnectionString
        {
            get
            {
                return Convert.ToString(this["ProductPersistenceConnectionString", "mongodb://localhost"]);
            }
        }

        public string ResultsDatabaseName
        {
            get
            {
                return Convert.ToString(this["ProductResultsDatabaseName", "ProductSearchResults"]);
            }
        }

        public string ProductRequestStatusCollection
        {
            get
            {
                return Convert.ToString(this["ProductRequestStatusCollection", "ProductRequestStatuses"]);
            }
        }

        public string InsuranceSearchResultsCollection
        {
            get
            {
                return Convert.ToString(this["ProductSearchResultsCollection", "InsuranceSearchResults"]);
            }
        }

        public string ProductCacheImplementation
        {
            get
            {
                return Convert.ToString(this["ProductCacheImplementation", "mongo"]);
            }
        }


        public int ProductSearchBoundaryDays
        {
            get
            {
                return int.Parse(this["ProductSearchBoundaryDays", "3"]);
            }
        }

        public int[] ProductSuppliersToSearch
        {
            get
            {
                //NOTE: this property must only be used within a config file; adding to the DB will limit ALL instances of the flights service
                //comma separated string of supplier id's
                var supplierIds = this["ProductSuppliersToSearch", ""];
                int[] ids = null;
                if (!string.IsNullOrEmpty(supplierIds))
                    ids = (from supplierId in supplierIds.Split(Convert.ToChar(",")) select int.Parse(supplierId)).ToArray();
                return ids;
            }
        }


        public string InsuranceValuationResultsCollection
        {
            get
            {
                return Convert.ToString(this["ProductValuationResultsCollection", "InsuranceValuationResults"]);
            }
        }


        public bool ProductRepositoryIsCachingEnabled
        {
            get
            {
                return bool.Parse(this["ProductRepositoryIsCachingEnabled", "true"]);
            }
        }

        public TimeSpan ProductRepositoryCacheExpireTime
        {
            get
            {
                return TimeSpan.Parse(this["ProductRepositoryCacheExpireTime", "01:00:00"]);
            }
        }


        public TimeSpan ProductRepositoryCachingTimeout
        {
            get
            {
                return TimeSpan.Parse(this["ProductRepositoryCachingTimeout", "00:00:05"]);
            }
        }


        public TimeSpan ProductRepositoryCommandTimeout
        {
            get
            {
                return TimeSpan.Parse(this["ProductRepositoryCommandTimeout", "00:00:05"]);
            }
        }

        public string InsuranceBookingResultsCollection
        {

            get
            {
                return Convert.ToString(this["ProductBookingResultsCollection", "InsuranceBookingResults"]);
            }


        }

        public double ProductInsurancePremiumTaxRate
        {
            get
            {
                return double.Parse(this["ProductInsurancePremiumTaxRate", "0.2"]);
            }
        }
        #endregion


        public string AttractionTicketSearchResultsCollection
        {
            get
            {
                return Convert.ToString(this["ProductSearchResultsCollection", "InsuranceSearchResults"]);
            }
        }

        public bool ConnectivityLogIsEnabled
        {
            get
            {
                return Convert.ToBoolean(this["ProductConnectivityLogIsEnabled", "true"]);
            }
        }

        public TimeSpan ProductAvailabilityResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["ProductAvailabilityResultCacheExpireTimeout", "02:00:00"]); }
        }

        public TimeSpan ProductValuationResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["ProductValuationResultCacheExpireTimeout", "02:00:00"]); }
        }

        public TimeSpan ProductBookingResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["ProductBookingResultCacheExpireTimeout", "02:00:00"]); }
        }

    }
}
