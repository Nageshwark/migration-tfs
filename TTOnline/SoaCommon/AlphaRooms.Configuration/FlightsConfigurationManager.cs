﻿using System;
using System.Linq;

using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Configuration
{
    public class FlightsConfigurationManager : ConfigurationManagerBase, IFlightsConfigurationManager,
        IRequestStatusServiceSettings, IConnectivityLogServiceSettings
    {
        public FlightsConfigurationManager(ILogger logger, IParameterRepository parameterRepository)
            : base(logger, parameterRepository)
        {

        }

        #region Named Accessors

        public int FlightAvailabilitySearchTimeout
        {
            get
            {
                //return int.Parse(this["FlightAvailabilitySearchTimeout", "20"]);
                return int.Parse(this["FlightAvailabilitySearchTimeout", "999999"]);
            }
        }

        public int FlightCachePriceThreshold
        {
            get { return int.Parse(this["FlightCachePriceThreshold", "80"]); }
        }

        public int FlightCacheSearchTimeout
        {
            get { return int.Parse(this["FlightCacheSearchTimeout", "10"]); }
        }

        public int FlightCacheTimeToLive
        {
            get { return int.Parse(this["FlightCacheTTL", "60"]); }
        }

        public bool FlightCachingControl
        {
            get { return Convert.ToBoolean(this["FlightCachingControl", "true"]); }
        }

        public int FlightsGroundTimeLimit
        {
            get { return int.Parse(this["FlightsGroundTimeLimit", "5"]); }
        }

        public int FlightSearchNextMinimumHrs
        {
            get { return int.Parse(this["FlightSearchNextMinimumHrs", "48"]); }
        }

        public string ConnectionString
        {
            get { return Convert.ToString(this["FlightsPersistenceConnectionString", "mongodb://localhost"]); }
        }

        public string ResultsDatabaseName
        {
            get { return Convert.ToString(this["FlightsResultsDatabaseName", "flightresults"]); }
        }

        public bool MultiDatabasesEnabled
        {
            get { return bool.Parse(this["FlightsMultiDatabasesEnabled", "true"]); }
        }

        public int MultiDatabasesCount
        {
            get { return int.Parse(this["FlightsMultiDatabasesCount", "10"]); }
        }

        public string FlightsSearchResultsCollection
        {
            get { return Convert.ToString(this["FlightsSearchResultsCollection", "FlightSearchResults"]); }
        }

        public string FlightsReplacedSearchResultsCollection
        {
            get { return Convert.ToString(this["FlightsReplacedResultsCollection", "FlightReplacedResults"]); }
        }

        public string FlightsCacheImplementation
        {
            get { return Convert.ToString(this["FlightsCacheImplementation", "mongo"]); }
        }

        public decimal FlightsAtolCostPerPerson
        {
            get { return Decimal.Parse(this["FlightsATOLCostPerPerson", "2.5"]); }
        }

        public int[] FlightSuppliersToSearch
        {
            get
            {
                //NOTE: this property must only be used within a config file; adding to the DB will limit ALL instances of the flights service
                //comma separated string of supplier id's
                var supplierIds = this["FlightSuppliersToSearch", ""];
                int[] ids = null;
                if (!string.IsNullOrEmpty(supplierIds))
                    ids =
                        (from supplierId in supplierIds.Split(Convert.ToChar(",")) select int.Parse(supplierId)).ToArray
                            ();
                return ids;
            }
        }

        public int FlightSearchBoundaryDays
        {
            get { return int.Parse(this["FlightSearchBoundaryDays", "3"]); }
        }

        public int FlightSearchBoundaryDuration
        {
            get { return int.Parse(this["FlightSearchBoundaryDuration", "7"]); }
        }

        public string FlightValuationResultsCollection
        {
            get { return this["FlightsValuationResultsCollection", "FlightValuationResults"]; }
        }

        public string FlightsBookingResultsCollection
        {
            get { return this["FlightsBookingResultsCollection", "FlightBookingResults"]; }
        }

        public string FlightAvailabilityRequestCollection
        {
            get { return this["FlightAvailabilityRequestCollection", "FlightSearchRequests"]; }
        }

        public bool FlightsReturnAllActiveSuppliersForSearch
        {
            get { return Convert.ToBoolean(this["FlightsReturnAllActiveSuppliersForSearch", "false"]); }
        }

        public bool FlightsDisableProcessors
        {
            get { return Convert.ToBoolean(this["FlightsDisableProcessors", "false"]); }
        }

        public bool FlightRepositoryIsCachingEnabled
        {
            get { return bool.Parse(this["FlightRepositoryIsCachingEnabled", "true"]); }
        }

        public TimeSpan FlightRepositoryCacheExpireTime
        {
            get { return TimeSpan.Parse(this["FlightRepositoryCacheExpireTime", "01:00:00"]); }
        }

        public TimeSpan FlightRepositoryCachingTimeout
        {
            get { return TimeSpan.Parse(this["FlightRepositoryCachingTimeout", "00:00:05"]); }
        }

        public TimeSpan FlightRepositoryCommandTimeout
        {
            get { return TimeSpan.Parse(this["FlightRepositoryCommandTimeout", "00:00:05"]); }
        }

        #endregion

        public TimeSpan FlightAvailabilityResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightAvailabilityResultCacheExpireTimeout", "02:00:00"]); }
        }

        public TimeSpan FlightValuationResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightValuationResultCacheExpireTimeout", "02:00:00"]); }
        }

        public TimeSpan FlightPrebookingResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightPrebookingResultCacheExpireTimeout", "02:00:00"]); }
        }

        public TimeSpan FlightBookingResultCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightBookingResultExpireTimeout", "02:00:00"]); }
        }

        public string RequestStatusCollection
        {
            get { return this["FlightsRequestStatusCollection", "FlightRequestStatuses"]; }
        }

        public TimeSpan RequestStatusCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightRequestStatusCacheExpireTimeout", "02:00:00"]); }
        }

        public bool ConnectivityLogIsEnabled
        {
            get { return bool.Parse(this["FlightConnectivityLogIsEnabled", "true"]); }
        }

        public bool FlightBookingLoggerIsEnabled
        {
            get { return Boolean.Parse(this["FlightBookingLoggerIsEnabled", "true"]); }
        }

        public bool FlightAvailabilityLoggerIsEnabled
        {
            get { return Boolean.Parse(this["FlightAvailabilityLoggerIsEnabled", "true"]); }
        }

        public int MaxInfantAgeForFlights
        {
            get { return int.Parse(this["FlightMaxInfantAge", "1"]); }
        }
 
        public int MaxChildAgeForFlights
        {
            get { return int.Parse(this["FlightMaxChildAge", "11"]); }
        }

        public decimal TravelportEasyjectApiFee
        {
            get { return decimal.Parse(this["TravelportEasyjetApiFee", "12.00"]); }
        }

        public SOACommon.DomainModels.Enumerators.FlightBookingLoggerModes FlightBookingLoggerMode
        {
            get
            {
                return
                    (SOACommon.DomainModels.Enumerators.FlightBookingLoggerModes)
                        Enum.Parse(typeof (SOACommon.DomainModels.Enumerators.FlightBookingLoggerModes)
                            , this["FlightBookingLoggerMode", "All"]);

            }
        }

        public int FlightsMaxNumofStops
        {
            get { return int.Parse(this["FlightsMaxNumofStops", "1"]); }
        }

        public string FlightSearchResultDetailsCollection
        {
            get { return this["FlightSearchResultDetailsCollection", "FlightSearchResultDetails"]; }
        }

        public TimeSpan FlightSearchResultDetailsCacheExpireTimeout
        {
            get { return TimeSpan.Parse(this["FlightSearchResultDetailsCacheExpireTimeout", "02:00:00"]); }
        }
    }
}
