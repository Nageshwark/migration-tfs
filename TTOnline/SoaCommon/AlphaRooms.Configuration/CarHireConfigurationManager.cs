﻿using System;
using System.Linq;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Configuration
{
    public class CarHireConfigurationManager : ConfigurationManagerBase, ICarHireConfigurationManager
    {
        public CarHireConfigurationManager(ILogger logger, IParameterRepository parameterRepository)
            : base(logger, parameterRepository)
        {

        }

        #region Named Accessor

        public int CarHireAvailabilitySearchTimeout
        {
            get { return int.Parse(this["CarHireAvailabilitySearchTimeout", "20"]); }
        }
        
        public bool CarHireCachingControl
        {
            get
            {
                return Convert.ToBoolean(this["CarHireCachingControl", "true"]);
            }
        }

        public int CarHireCacheSearchTimeout
        {
            get
            {
                return int.Parse(this["Flight" +
                                      "CacheSearchTimeout", "10"]);
            }
        }

        public string ConnectionString
        {
            get
            {
                return Convert.ToString(this["CarHirePersistenceConnectionString", "mongodb://localhost"]);
            }
        }

        public string ResultsDatabaseName
        {
            get
            {
                return Convert.ToString(this["CarHireResultsDatabaseName", "CarHireSearchResults"]);
            }
        }

        public string CarHiresRequestStatusCollection
        {
            get
            {
                return Convert.ToString(this["CarHireRequestStatusCollection", "CarHireRequestStatuses"]);
            }
        }
        
        public string CarHiresSearchResultsCollection
        {
            get
            {
                return Convert.ToString(this["CarHireSearchResultsCollection", "CarHireSearchResults"]);
            }
        }


        public int[] CarHireSuppliersToSearch
        {
            get
            {
                //NOTE: this property must only be used within a config file; adding to the DB will limit ALL instances of the carhires service
                //comma separated string of supplier id's
                var supplierIds = this["CarHireSuppliersToSearch", ""];
                int[] ids = null;
                if (!string.IsNullOrEmpty(supplierIds))
                    ids = (from supplierId in supplierIds.Split(Convert.ToChar(",")) select int.Parse(supplierId)).ToArray();
                return ids;
            }
        }

        public string CarHiresBookingResultsCollection
        {
            get
            {
                return this["CarHireBookingResultsCollection", "CarHireBookingResults"];
            }
        }

        public string CarHireCacheImplementation
        {
            get
            {
                return Convert.ToString(this["CarHireCacheImplementation", "mongo"]);
            }
        }

        #endregion
    }
}
