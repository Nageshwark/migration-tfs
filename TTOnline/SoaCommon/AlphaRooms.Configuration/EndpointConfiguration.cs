﻿namespace AlphaRooms.Configuration
{
    using System;

    using Ninject.Extensions.Logging;

    using AlphaRooms.SOACommon.Interfaces;

    public class EndpointConfiguration : ConfigurationManagerBase, IEndpointConfiguration
    {
        private readonly string endpoint;

        public string EndpointAddress
        {
            get
            {
                return this.endpoint;
            }
        }

        public EndpointConfiguration(string service, ILogger logger, IParameterRepository parameterRepository)
            : base(logger, parameterRepository)
        {
            if (string.IsNullOrEmpty(service))
            {
                throw new ArgumentException("Can not be null or empty.", "service");
            }

            this.endpoint = this.GetEndPoint(service);
        }

        private string GetEndPoint(string service)
        {
            const string EndpointKey = "_Endpoint";
            const string TemplateKey = "_Endpoint_Template";

            var address = this[service + EndpointKey];
            var template = this[service + TemplateKey];

            string foundEndpoint = null;

            if (!string.IsNullOrEmpty(address) && !string.IsNullOrEmpty(template))
            {
                foundEndpoint = string.Format(template, address);
            }

            return foundEndpoint;
        }
    }
}