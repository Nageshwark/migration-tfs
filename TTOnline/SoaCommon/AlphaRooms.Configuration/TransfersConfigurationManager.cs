﻿using System;
using System.Linq;
using AlphaRooms.SOACommon.Interfaces;
using Ninject.Extensions.Logging;

namespace AlphaRooms.Configuration
{
   public class TransfersConfigurationManager :  ConfigurationManagerBase, ITransfersConfigurationManager
    {

       public TransfersConfigurationManager(ILogger logger, IParameterRepository parameterRepository)
           : base(logger, parameterRepository)
       {

       }
        #region Named Accessors

       
       public int TransferAvailabilitySearchTimeout
       {
           get
           {
               return int.Parse(this["TransferAvailabilitySearchTimeout", "20"]);
           }
       }

       public string TransfersSearchResultsCollection
       {
           get
           {
               return Convert.ToString(this["TransfersSearchResultsCollection", "TransferSearchResults"]);
           }
       }

       public string TransfersRequestStatusCollection
       {
           get
           {
               return Convert.ToString(this["TransfersRequestStatusCollection", "TransferRequestStatuses"]);
           }
       }

       public bool TransferCachingControl
       {
           get
           {
               return Convert.ToBoolean(this["TransferCachingControl", "true"]);
           }
       }

       public string TransfersCacheImplementation
       {
           get
           {
               return Convert.ToString(this["TransfersCacheImplementation", "mongo"]);
           }
       }

       public string TransfersValuationResultsCollection
       {
           get
           {
               return Convert.ToString(this["TransfersValuationResultsCollection", "TransferValuationResults"]);
           }
       }

       public string TransfersBookingResultsCollection
       {
           get
           {
               return Convert.ToString(this["TransfersBookingResultsCollection", "TransferValuationResults"]);
           }
       }

       public int[] TransfersSuppliersToSearch
       {
           get
           {
               //NOTE: this property must only be used within a config file; adding to the DB will limit ALL instances of the flights service
               //comma separated string of supplier id's
               var supplierIds = this["TransferSuppliersToSearch", ""];
               int[] ids = null;
               if (!string.IsNullOrEmpty(supplierIds))
                   ids = (from supplierId in supplierIds.Split(Convert.ToChar(",")) select int.Parse(supplierId)).ToArray();
               return ids;
           }
       }

       public string ConnectionString
       {
           get
           {
               return Convert.ToString(this["TransfersPersistenceConnectionString", "mongodb://localhost"]);
           }
       }
       public string ResultsDatabaseName
       {
           get
           {
               return Convert.ToString(this["TransfersResultsDatabaseName", "TransferResults"]);
           }
       }
        #endregion
    }
}
