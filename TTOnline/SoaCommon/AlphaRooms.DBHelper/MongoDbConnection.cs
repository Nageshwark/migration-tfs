﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.DBHelper
{
    public class MongoDbConnection : IDbConnection
    {
        //private readonly IConfigurationManager configurationManager;
        //private readonly IDbMultiDatabaseController multiDatabaseController;
        //private MongoServer server;
        //private MongoDatabase[] databases;

        //public MongoDbConnection(IConfigurationManager configurationManager, IDbMultiDatabaseController multiDatabaseController)
        //{
        //    this.configurationManager = configurationManager;
        //    this.multiDatabaseController = multiDatabaseController;
        //}

        //private MongoServer GetMongoServer()
        //{
        //    if (server == null)
        //    {
        //        lock (this)
        //        {
        //            server = new MongoClient(configurationManager.ConnectionString).GetServer();
        //            if (databases == null)
        //            {
        //                if (configurationManager.MultiDatabasesEnabled) 
        //                {
        //                    databases = new MongoDatabase[configurationManager.MultiDatabasesCount + 1];
        //                    databases[0] = this.server.GetDatabase(configurationManager.ResultsDatabaseName);
        //                    for (int i = 1; i < databases.Length; i++)
        //                    {
        //                        databases[i] = this.server.GetDatabase(configurationManager.ResultsDatabaseName + "_" + i.ToString("00"));
        //                    }
        //                } 
        //                else
        //                {
        //                    databases = new MongoDatabase[] { this.server.GetDatabase(configurationManager.ResultsDatabaseName) };
        //                }
        //            }
        //        }
        //    }
        //    return server;
        //}

        //public MongoCollection<T> GetMongoCollection<T>(string collectionName)
        //{
        //    GetMongoServer();
        //    return databases[0].GetCollection<T>(collectionName);
        //}

        //public MongoCollection<T> GetMongoCollection<T>(string databaseKey, string collectionName)
        //{
        //    GetMongoServer();
        //    if (configurationManager.MultiDatabasesEnabled)
        //    {
        //        return databases[multiDatabaseController.GetDatabaseIndexByKey(databaseKey) + 1].GetCollection<T>(collectionName);
        //    }
        //    return databases[0].GetCollection<T>(collectionName);
        //}
        public MongoCollection<T> GetMongoCollection<T>(string collectionName)
        {
            throw new NotImplementedException();
        }

        public MongoCollection<T> GetMongoCollection<T>(string databaseKey, string collectionName)
        {
            throw new NotImplementedException();
        }
    }
}
