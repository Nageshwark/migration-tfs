﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IExchangeRateService
    {
        Task<Money> ConvertCurrencyAsync(Money amount, string currencyTo);

        Task<ExchangeRate> GetExchangeRateAsync(string fromCurrenct, string toCurrency);
    }
}
