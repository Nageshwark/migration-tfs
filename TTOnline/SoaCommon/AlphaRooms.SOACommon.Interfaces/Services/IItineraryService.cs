﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces.Services
{
    public interface IItineraryService
    {
        Task<List<Passenger>> GetItineraryPersonByItineraryId(int itineraryId);
    }
}
