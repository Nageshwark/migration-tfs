﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IChannelInfoService
    {
        Task<ChannelInfo> GetChannelInfo(Channel channel);
        Task<ChannelInfo> GetChannelInfoByLocaleIdAsync(int localeId);
        Task<ChannelInfo> GetChannelInfoBySiteAndLanguageCodeAsync(Sites site, string languageCode);
    }
}
