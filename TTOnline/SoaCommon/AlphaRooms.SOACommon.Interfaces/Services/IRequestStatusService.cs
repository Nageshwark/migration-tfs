﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
	public interface IRequestStatusService<T>
	{
		void UpdateDatabaseWithStatus(T status);
        T GetSearchStatus(Guid searchId);
	}
}
