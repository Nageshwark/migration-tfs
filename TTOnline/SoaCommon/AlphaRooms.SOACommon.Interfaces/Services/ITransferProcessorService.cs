﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
   public interface ITransferProcessorService
    {
       Task<List<TransferSupplierAvailabilityResult>> ProcessSupplierFlights(List<TransferSupplierAvailabilityResult> flights, TransferAvailabilityRequest request);
    }
}
