﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces
{
	public interface IBookingLoggerService
    {
        Task<bool> Log(BookingLogItem logItem);
        Task<BookingLogItem> ReadLog(Guid bookingId, int iSequence);
    }
}