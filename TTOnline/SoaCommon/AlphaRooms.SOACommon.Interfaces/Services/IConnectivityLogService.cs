﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IConnectivityLogService
    {
        ISupplierConnectivityLogMonitor CreateSupplierConnectivityMonitor(SupplierTypes supplierType, int supplierId, ServiceTypes serviceType);
    }
}
