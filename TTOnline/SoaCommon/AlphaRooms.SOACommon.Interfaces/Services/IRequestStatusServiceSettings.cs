﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IRequestStatusServiceSettings
    {
        TimeSpan RequestStatusCacheExpireTimeout { get; }

        string RequestStatusCollection { get; }
    }
}
