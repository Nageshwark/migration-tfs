﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IConnectivityLogServiceSettings
    {
        bool ConnectivityLogIsEnabled { get; }
    }
}
