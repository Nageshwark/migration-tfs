﻿using System;
using AlphaRooms.SOACommon.Contracts;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ICardDetailsRemover
    {
        string BlankCreditCardDetails(string value, CardDetails cardDetails);
    }
}
