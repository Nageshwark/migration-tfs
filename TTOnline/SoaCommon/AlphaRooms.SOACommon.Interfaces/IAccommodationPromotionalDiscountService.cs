﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IAccommodationPromotionalDiscountService<T> where T: class 
    {
        void ProcessDiscounts(IEnumerable<T> results, string code, Channel channel);
    }
}
