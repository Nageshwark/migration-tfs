﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IConnectivityLogMonitor
    {
        void Start();
        void Stop();
        void StopWithException(Exception ex);
        bool IsCompleted { get; }
        bool? IsServiceSuccessful { get; }
        string HostName { get; }
        SupplierTypes SupplierType { get; }
        ServiceTypes ServiceType { get; }
        DateTime? StartDate { get; }
        DateTime? StopDate { get; }
        Exception ServiceException { get; }
    }
}
