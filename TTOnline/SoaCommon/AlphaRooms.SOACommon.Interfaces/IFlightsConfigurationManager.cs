using System;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IFlightsConfigurationManager : IConfigurationManager
    {
        //named accessors
        int FlightAvailabilitySearchTimeout { get; }
        int FlightCachePriceThreshold { get; }
        int FlightCacheSearchTimeout { get; }
        int FlightCacheTimeToLive { get; }
        bool FlightCachingControl { get; }
        int FlightsGroundTimeLimit { get; }
        int FlightSearchNextMinimumHrs { get; }
        string FlightsSearchResultsCollection { get; }
        string FlightsReplacedSearchResultsCollection { get; }
        decimal FlightsAtolCostPerPerson { get; }
        int[] FlightSuppliersToSearch { get; }
        int FlightSearchBoundaryDays { get; }
        int FlightSearchBoundaryDuration { get; }
        string FlightValuationResultsCollection { get; }
        string FlightsBookingResultsCollection { get; }
        string FlightAvailabilityRequestCollection { get; }
        bool FlightsReturnAllActiveSuppliersForSearch { get; }
        bool FlightsDisableProcessors { get; }
        TimeSpan FlightAvailabilityResultCacheExpireTimeout { get; }
        TimeSpan FlightValuationResultCacheExpireTimeout { get; }
        TimeSpan FlightPrebookingResultCacheExpireTimeout { get; }
        TimeSpan FlightBookingResultCacheExpireTimeout { get; }
        bool FlightBookingLoggerIsEnabled { get; }
        FlightBookingLoggerModes FlightBookingLoggerMode { get; }
        bool FlightAvailabilityLoggerIsEnabled { get; }
        int MaxChildAgeForFlights { get; }
        decimal TravelportEasyjectApiFee { get; }
        int FlightsMaxNumofStops { get; }
        int MaxInfantAgeForFlights { get; }
        string FlightSearchResultDetailsCollection { get; }
        TimeSpan FlightSearchResultDetailsCacheExpireTimeout { get; }
    }
}
