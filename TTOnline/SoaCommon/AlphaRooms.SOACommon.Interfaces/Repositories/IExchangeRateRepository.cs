﻿using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using System.Collections.Generic;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IExchangeRateRepository
    {
        Task<ExchangeRate> GetExchangeRateAsync(string currencyFrom, string currencyTo);
        Task<Dictionary<string, ExchangeRate>> GetExchangeRateAsync(string[] currencyFroms, string currencyTo);
    }
}
