﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IBookingItemRepository
    {
        Task<List<ChargeItem>> GetItineraryBookingItems(int itineraryId);

        List<Money> GetSupplierGross();
    }
}
