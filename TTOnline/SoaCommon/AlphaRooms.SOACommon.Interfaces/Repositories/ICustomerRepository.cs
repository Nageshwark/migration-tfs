﻿using System;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ICustomerRepository
    {
        Task<PassengerContactDetails> GetCustomerByItineraryId(int id);
    }
}
