﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IItineraryRepository
    {
        Task<List<Passenger>> GetItineraryPersonByItineraryId(int itineraryId);
    }
}
