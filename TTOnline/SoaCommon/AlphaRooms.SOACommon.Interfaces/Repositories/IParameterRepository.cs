﻿namespace AlphaRooms.SOACommon.Interfaces
{
    using AlphaRooms.SOACommon.DomainModels;

    public interface IParameterRepository
    {
        Parameter GetParameterByName(string key);
    }
}