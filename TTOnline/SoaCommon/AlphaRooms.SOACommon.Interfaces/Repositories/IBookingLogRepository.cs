﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IBookingLogRepository
    {
        Task<bool> WriteLog(BookingLogItem log);
        Task<BookingLogItem> ReadLogbyBookingId(Guid bookingID, int iSequence);
    }
}
