﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IChannelInfoRepository
    {
        Task<ChannelInfo> GetChannelInfo(Channel channel);
        Task<ChannelInfo> GetChannelInfoByLocaleId(int localeId);
        Task<ChannelInfo> GetChannelInfoBySiteAndLanguageCodeAsync(Sites site, string languageCode, string countryCode);
    }
}
