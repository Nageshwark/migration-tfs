﻿using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces.Repositories
{
    public interface IPaymentDefaultCardFeeRepository
    {
        Task<PaymentDefaultCardFee[]> GetByChannelId(Channel channel);
    }
}
