﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;

namespace AlphaRooms.SOACommon.Interfaces.Repositories
{
    public interface IPromotionalDiscountRepository
    {
        PromotionalDiscount GetPromoCode(string voucherCode);
    }
}