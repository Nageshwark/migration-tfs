﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
	public interface IWindowsTimezoneRepository
    {
		TimeZoneInfo GetWindowsTimezoneByIdentifier(string windowsTimezoneIdentifier);
		bool IsTimezoneIdentifierValid(string windowsTimezoneIdentifier);
    }
}
