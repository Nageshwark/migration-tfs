﻿using AlphaRooms.SOACommon.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IProviderParameterUpdater<T>
    {
        T UpdateSettings(T suppliers, Func<T, IEnumerable<IProviderParameter>> property);
        T[] UpdateSettings(T[] suppliers, Func<T, IEnumerable<IProviderParameter>> property);
    }
}
