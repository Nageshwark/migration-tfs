﻿namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IEndpointConfiguration
    {
        string EndpointAddress
        {
            get;
        }
    }
}