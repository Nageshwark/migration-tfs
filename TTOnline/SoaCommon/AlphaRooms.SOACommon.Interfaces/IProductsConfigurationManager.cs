﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IProductConfigurationManager : IConfigurationManager
    {
        int ProductAvailabilitySearchTimeout { get; }
        int ProductCachePriceThreshold { get; }
        int ProductCacheSearchTimeout { get; }
        int ProductCacheTimeToLive { get; }
        bool ProductCachingControl { get; }
        int ProductGroundTimeLimit { get; }
        string ProductRequestStatusCollection { get; }
        int[] ProductSuppliersToSearch { get; }
        int ProductSearchBoundaryDays { get; }
        double ProductInsurancePremiumTaxRate { get; }

        string InsuranceSearchResultsCollection { get; }
        string InsuranceValuationResultsCollection { get; }
        string InsuranceBookingResultsCollection { get; }


        string AttractionTicketSearchResultsCollection { get; }
        //string InsuranceValuationResultsCollection { get; }
        //string InsuranceBookingResultsCollection { get; }
        TimeSpan ProductAvailabilityResultCacheExpireTimeout { get; }
        TimeSpan ProductValuationResultCacheExpireTimeout { get; }
        TimeSpan ProductBookingResultCacheExpireTimeout { get; }
    }
}
