﻿
namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ICarHireConfigurationManager : IConfigurationManager
    {
        int CarHireAvailabilitySearchTimeout { get; }
        int CarHireCacheSearchTimeout { get; }
        bool CarHireCachingControl { get; }
        string CarHiresRequestStatusCollection { get; }
        string CarHiresSearchResultsCollection { get; }
        int[] CarHireSuppliersToSearch { get; }
        string CarHiresBookingResultsCollection { get; }
        string CarHireCacheImplementation { get; }
    }
}
