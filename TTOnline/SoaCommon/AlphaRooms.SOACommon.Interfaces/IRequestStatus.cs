﻿using System;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
	public interface IRequestStatus
	{
		RequestType RequestType { get; set; }
		string RequestId { get; set; }
		Status SearchStatus { get; set; }
		DateTime SearchDate { get; set; }
		int SearchDuration { get; set; }
		string Message { get; set; }
        DateTime CacheExpireDate { get; set; }
	}
}
