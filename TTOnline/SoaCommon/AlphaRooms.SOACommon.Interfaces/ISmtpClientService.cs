﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ISmtpClientService
    {
        bool SendEmail(MailMessage message);
    }
}
