﻿using System;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IAccommodationConfigurationManager : IConfigurationManager
    {
        string AccommodationHostNameTag { get; }
        string AccommodationCacheImplementation { get; }
        int[] AccommodationProvidersToSearch { get; }
        int[] TravelGateProvider { get; }
      
        
        // soacommon
        bool AccommodationRepositoryIsCachingEnabled { get; }
        TimeSpan AccommodationRepositoryCacheExpireTime { get; }
        TimeSpan AccommodationRepositoryCachingTimeout { get; }
        TimeSpan AccommodationRepositoryCommandTimeout { get; }

        // B2C
        string AccommodationB2CAvailabilityResultsDatabase { get; }
        string AccommodationB2CAvailabilityResultsCollection { get; }
        int AccommodationB2CAvailabilityResultsMultiDbCount { get; }
        bool AccommodationB2CAvailabilityResultsMultiDbIsEnabled { get; }
        string AccommodationB2CRequestStatusDatabase { get; }
        string AccommodationB2CRequestStatusCollection { get; }
        TimeSpan AccommodationB2CRequestStatusExpireTimeout { get; }
        bool AccommodationB2CAvailabilityLoggerIsEnabled { get; }
        bool AccommodationB2CValuationLoggerIsEnabled { get; }
        int AccommodationB2CAvailabilityResultsNumberOfRooms { get; }
        bool AccommodationB2CBookingLoggerIsEnabled { get; }        
        bool AccommodationB2CUseHmdForMarkup { get; }
        bool AccommodationB2CConsolidationIncludePaymentModel { get; }
        TimeSpan AccommodationB2CRequestStatusExpireThreshold { get; }

        // B2BWeb
        string AccommodationB2BWebAvailabilityResultsDatabase { get; }
        string AccommodationB2BWebAvailabilityResultsCollection { get; }
        int AccommodationB2BWebAvailabilityResultsMultiDbCount { get; }
        bool AccommodationB2BWebAvailabilityResultsMultiDbIsEnabled { get; }
        string AccommodationB2BWebRequestStatusDatabase { get; }
        string AccommodationB2BWebRequestStatusCollection { get; }
        TimeSpan AccommodationB2BWebRequestStatusExpireTimeout { get; }
        bool AccommodationB2BWebAvailabilityLoggerIsEnabled { get; }
        int AccommodationB2BWebAvailabilityResultsNumberOfRooms { get; }
        bool AccommodationB2BWebUseHmdForMarkup { get; }
        bool AccommodationB2BWebValuationLoggerIsEnabled { get; }
        bool AccommodationB2BWebConsolidationIncludePaymentModel { get; }
        TimeSpan AccommodationB2BWebRequestStatusExpireThreshold { get; }
        bool AccommodationB2BWebBookingLoggerIsEnabled { get; }
        string AccommodationB2BWebBookingCustomerContactNumber { get; }
        string AccommodationB2BWebBookingCustomerEmailAddress { get; }

        // B2BXml
        bool AccommodationB2BXmlAvailabilityLoggerIsEnabled { get; }
        bool AccommodationB2BXmlValuationLoggerIsEnabled { get; }
        bool AccommodationB2BXmlBookingLoggerIsEnabled { get; }

        // Core
        decimal SupplierValuationThresholdDefault { get; }
        byte AccommodationAdultAge { get; }
        byte AccommodationMaxChildAge { get; }
        byte AccommodationMaxInfantAge { get; }
        byte AccommodationMaxRooms { get; }
        TimeSpan AccommodationAvailabilitySearchTimeout { get; }
        string AccommodationAvailabilityCachedRequestCollection { get; }
        string AccommodationAvailabilityResultsDatabase { get; }
        int AccommodationAvailabilityResultsMultiDbCount { get; }
        bool AccommodationAvailabilityResultsMultiDbIsEnabled { get; }
        string AccommodationAvailabilityResultsCollection { get; }
        TimeSpan AccommodationAvailabilityEmptyResultsExpireTimeout { get; }
        TimeSpan AccommodationValuationTimeout { get; }
        string AccommodationValuationResultsDatabase { get; }
        string AccommodationValuationResultsCollection { get; }
        TimeSpan AccommodationValuationResultsExpireTimeout { get; }
        string AccommodationBookingResultsDatabase { get; }
        string AccommodationBookingResultsCollection { get; }
        TimeSpan AccommodationBookingResultsExpireTimeout { get; }
        decimal AccommodationCancellationFee_UK { get; }
        decimal AccommodationCancellationFee_IE { get; }
        decimal AccommodationCancellationFee_US { get; }
        int AccommodationNoRefundCancellationDaysBeforeDeparture { get; }
        string AccommodationCancellationResultsDatabase { get; }
        string AccommodationCancellationResultsCollection { get; }
        TimeSpan AccommodationCancellationResultsExpireTimeout { get; }
        decimal AccommodationBaseDiscount { get; }
        string AccommodationRoomDescriptionNonRefundableLabel { get; }
        string[] AccommodationRoomDescriptionNonRefundableKeyword { get; }
        bool AccommodationPriceHikingIsEnabled { get; }
        int[] AccommodationPriceHikingProviderIds { get; }
        decimal AccommodationPriceHikingMaxHikingPercent { get; }
        bool AccommodationRoomSwitchingIsEnabled { get; }
        string AccommodationCachedRequestDatabase { get; }
        string AccommodationCachedRequestCollection { get; }
        TimeSpan AccommodationCachedRequestExpireThreshold { get; }
        bool AccommodationBookingDisableAll { get; }
        decimal AccommodationVatPercentage { get; }
		string[] ExcludeSuppliersToApplyPriceMarkup { get; }
		bool B2CEventLogInfoIsEnabled { get; }

		//Provider cache per service configuration
		bool AccommodationB2CEnableProviderCache { get; }
        bool AccommodationB2BWebEnableProviderCache { get; }
        bool AccommodationB2BXmlEnableProviderCache { get; }
        int AccommodationB2CXMLGlobalLeadTime { get; }
        int AccommodationB2BXMLGlobalLeadTime { get; }

        int TTB2BUserId { get; }

        // Aria Contracts availability
        TimeSpan RefreshBookingAdjustmentsEvery { get; }
        string[] AccommodationBev5VirtualCardSuppliers { get; }
        //XTG request and response config key 
        bool EnableXTGLogging { get; }
    }
}
