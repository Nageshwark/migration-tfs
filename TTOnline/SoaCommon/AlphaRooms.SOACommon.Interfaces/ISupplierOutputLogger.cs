﻿using AlphaRooms.SoaCommon.Contracts;
using AlphaRooms.SOACommon.DomainModels.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ISupplierOutputLogger
    {
        void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, string departureAirportCode, string destinationAirportCode, DateTime departureDate, SupplierOutputFormats format, object response);

        void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, string departureAirportCode, string destinationAirportCode, DateTime departureDate, string processName, SupplierOutputFormats format, object response);

        void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, RequestAirport[] requestDepartureAirport, RequestAirport[] requestDestinationAirport, DateTime departureDate, SupplierOutputFormats format, object response);

        void LogAvailabilityResponse(ISupplierBase supplier, Guid searchId, RequestAirport[] requestDepartureAirport, RequestAirport[] requestDestinationAirport, DateTime departureDate, string processName, SupplierOutputFormats format, object response);

        void LogResponse(ISupplierBase supplier, ServiceTypes serviceTypes, Guid searchId, Guid valuationId, SupplierOutputFormats format, object response);

        void LogResponse(ISupplierBase supplier, ServiceTypes serviceTypes, Guid searchId, Guid valuationId, string processName, SupplierOutputFormats format, object response);
    }
}
