﻿namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IConfigurationManager
    {
        string this[string key] { get; }
        string this[string key, string defaultValue] { get; }
    }
}