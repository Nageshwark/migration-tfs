﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IDiParameterContainer
    {
        void Register<T>(object key, T value);
        T GetParameterValue<T>(object key);
        void Unregister(object key);
    }
}
