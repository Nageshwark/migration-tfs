﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ISmtpServerSettings
    {
        int SmtpPort { get; }
        string SmtpHost { get; }
        string SmtpUsername { get; }
        string SmtpPassword { get; }
        bool SmtpIsTlsOn { get; }
    }
}
