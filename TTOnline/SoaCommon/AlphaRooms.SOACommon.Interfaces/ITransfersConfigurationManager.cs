﻿
namespace AlphaRooms.SOACommon.Interfaces
{
    public interface ITransfersConfigurationManager : IConfigurationManager
    {
        int TransferAvailabilitySearchTimeout { get; }
        string TransfersSearchResultsCollection { get; }
        string TransfersRequestStatusCollection { get; }
        bool TransferCachingControl { get; }
        string TransfersValuationResultsCollection { get; }
        string TransfersBookingResultsCollection { get; }
        int[] TransfersSuppliersToSearch { get; }
    }
}
