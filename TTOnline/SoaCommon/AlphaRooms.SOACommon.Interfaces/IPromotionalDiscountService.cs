﻿using AlphaRooms.SOACommon.Contracts;
using AlphaRooms.SOACommon.DomainModels;
using AlphaRooms.SOACommon.DomainModels.Enumerators;

namespace AlphaRooms.SOACommon.Interfaces
{
    public interface IPromotionalDiscountService
    {
        //PromoCodeResponse GetPromoCode(string voucherCode, Channel? channel);
        //string GetDescription(PromoCode promo);
        ////PriceElement GetDiscountElement(Money amount, decimal promoAmount, PromoCode promoCode);
        //bool IsAboveMinThreshold(PromoCode promo, Money salePrice);

        PromotionalDiscountResult GetDiscount(Money amount, decimal promoAmount, PromotionalDiscount promotionalDiscount);
    }
}