﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject.Extensions.Logging;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.SOACommon.DomainModels;
using System.Threading;
using System.Threading.Tasks;

namespace AlphaRooms.Configuration.Tests
{
    [TestClass]
    public class ConfigurationManagerBaseTests
    {
        [TestMethod]
        public void GetValueQueriesRepository()
        {
            const string key = "key";
            const string expected = "expected";

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Returns(
                new Parameter()
                {
                    ParameterName = key,
                    ParameterValue = expected
                });

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            string actual = config[key];

            Assert.AreEqual(expected, actual);
            parameterRepository.Verify(x => x.GetParameterByName(key), Times.Once);
        }

        [TestMethod]
        public void GetValueCachesValue()
        {
            const string key = "key";
            const string expected = "expected";

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Returns(
                new Parameter()
                {
                    ParameterName = key,
                    ParameterValue = expected
                });

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            // double get
            string actual = config[key];
            string actual2 = config[key];

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual2);

            // should be once as it should be cached
            parameterRepository.Verify(x => x.GetParameterByName(key), Times.Once);
        }

        [TestMethod]
        public void GetValueReturnsNull()
        {
            const string key = "key";
            const string expected = null;

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Returns(
                new Parameter()
                {
                    ParameterName = key,
                    ParameterValue = expected
                });

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            // double get
            string actual = config[key];
            string actual2 = config[key];

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual2);

            // should be once as it should be cached
            parameterRepository.Verify(x => x.GetParameterByName(key), Times.Once);
        }

        [TestMethod]
        public void GetValueCachedValueExpires()
        {
            const string key = "key";
            const string expected = "expected";

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Returns(
                new Parameter()
                {
                    ParameterName = key,
                    ParameterValue = expected
                });

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            // set ttl to small so it forces refesh
            config.TimeToLive = TimeSpan.FromMilliseconds(500);
            string actual = config[key];

            // wait for ttl expire
            Thread.Sleep(1000);
            string actual2 = config[key];


            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual2);

            // should be twice as ttl expired
            parameterRepository.Verify(x => x.GetParameterByName(key), Times.Exactly(2));
        }

        [TestMethod]
        public void GetValueCachedRefreshed()
        {
            const string key = "key";
            int parameterValue = 0;

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Returns((string x) =>
               new Parameter()
               {
                   ParameterName = key,
                   ParameterValue = (parameterValue++).ToString(),
               });

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            // set ttl to small so it forces refesh
            config.TimeToLive = TimeSpan.FromMilliseconds(500);
            string actual = config[key];

            // wait for ttl expire
            Thread.Sleep(1000);

            // should refresh the cache
            string actual2 = config[key];
            // should get the refreshed value 
            string actual3 = config[key];

            Assert.AreEqual("0", actual);
            Assert.AreEqual("1", actual2);
            Assert.AreEqual("1", actual3);

            // should be twice as ttl expired
            parameterRepository.Verify(x => x.GetParameterByName(key), Times.Exactly(2));
        }

        [TestMethod]
        public void GetValueDoesSwallowExceptions()
        {
            const string key = "key";

            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository.Setup(x => x.GetParameterByName(key)).Throws(
                new InvalidOperationException());

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            string actual = config[key];

            // if nothing is thrown it is a pass
        }

        [TestMethod]
        public void GetValueIsMultithreaded()
        {
            Mock<ILogger> logger = new Mock<ILogger>();

            Mock<IParameterRepository> parameterRepository = new Mock<IParameterRepository>();
            parameterRepository
                .Setup(x => x.GetParameterByName(It.IsAny<string>()))
                .Returns((Func<string, Parameter>)ConfigurationManagerBaseTests.Create);

            ConfigurationManagerBase config = new ConfigurationManagerBase(
                logger.Object,
                parameterRepository.Object);

            // to stop the test if it runs over an expected time
            CancellationTokenSource cancellationSource = new CancellationTokenSource(TimeSpan.FromSeconds(20));
            ParallelOptions parallelOptions = new ParallelOptions
            {
                CancellationToken = cancellationSource.Token,
            };

            // hammer the thing to make sure it works
            Parallel.For(0, 8000, parallelOptions, notUsed =>
            {
                for (int i = 0; i < 8000; i++)
                {
                    string key = i.ToString();
                    string actual = config[key];
                    Assert.AreEqual(key, actual);
                }
            });
        }

        private static Parameter Create(string key)
        {
            return new Parameter()
            {
                ParameterName = key,
                ParameterValue = key,
            };
        }
    }
}
