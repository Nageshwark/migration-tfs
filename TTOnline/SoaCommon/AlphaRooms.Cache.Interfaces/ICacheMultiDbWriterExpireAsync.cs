﻿
namespace AlphaRooms.Cache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface ICacheMultiDbWriterExpireAsync<T, K> : ICacheMultiDbWriterAsync<T, K> where T : ICacheExpire
    {
        Task AddAsync(K databaseKey, T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddIfDoesntExistAsync(K databaseKey, T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddOrUpdateAsync(K databaseKey, Expression<Func<T, bool>> filter, T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddRangeAsync(K databaseKey, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddRangeIfDoesntExistAsync(K databaseKey, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddOrUpdateRangeAsync(K databaseKey, Expression<Func<T, bool>> filter, IEnumerable<T> values, TimeSpan cacheExpireTimeout);
        Task DeleteAsync(K databaseKey, Expression<Func<T, bool>> filter, bool isMeta = false);
        void SetCacheExpireTimeout(T value, TimeSpan cacheExpireTimeout);        
    }
}
