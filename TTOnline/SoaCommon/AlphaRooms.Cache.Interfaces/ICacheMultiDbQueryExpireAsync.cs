﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheMultiDbQueryExpireAsync<T, K> : ICacheMultiDbQueryAsync<T, K> where T : ICacheExpire
    {
        DateTime GetCacheExpireDateThreshold();
        bool IsCacheExpireDateExpired(DateTime dateTime);
    }
}
