﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheQueryExpireAsync<T> : ICacheQueryAsync<T> where T : ICacheExpire
    {
        DateTime GetCacheExpireDateThreshold();
        bool IsCacheExpireDateExpired(DateTime dateTime);
    }
}
