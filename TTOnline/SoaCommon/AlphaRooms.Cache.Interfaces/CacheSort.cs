﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public class CacheSort<T> : ICacheSort<T>
    {
        public CacheSort(Expression<Func<T, object>> field, CacheSortOrder order)
        {
            this.Field = field;
            this.Order = order;
        }

        public Expression<Func<T, object>> Field { get; set; }
        public CacheSortOrder Order { get; set; }
    }
}
