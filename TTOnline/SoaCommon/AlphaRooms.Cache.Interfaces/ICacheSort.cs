﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheSort<T>
    {
        Expression<Func<T, object>> Field { get; set; }
        CacheSortOrder Order { get; set; }
    }
}
