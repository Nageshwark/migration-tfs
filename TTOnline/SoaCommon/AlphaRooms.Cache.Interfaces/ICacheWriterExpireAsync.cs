﻿namespace AlphaRooms.Cache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface ICacheWriterExpireAsync<T> : ICacheWriterAsync<T> where T : ICacheExpire
    {
        Task AddAsync(T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddIfDoesntExistAsync(T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddOrUpdateAsync(Expression<Func<T, bool>> filter, T value, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddRangeAsync(IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddRangeIfDoesntExistAsync(IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false);
        Task AddOrUpdateRangeAsync(Expression<Func<T, bool>> filter, IEnumerable<T> values, TimeSpan cacheExpireTimeout, bool isMeta = false);
        void SetCacheExpireTimeout(T value, TimeSpan cacheExpireTimeout);
    }
}
