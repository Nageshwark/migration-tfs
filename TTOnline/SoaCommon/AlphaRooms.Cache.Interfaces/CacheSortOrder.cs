﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Cache.Interfaces
{
    public enum CacheSortOrder
    {
        Asc = 0
        , Desc = 1
    }
}
