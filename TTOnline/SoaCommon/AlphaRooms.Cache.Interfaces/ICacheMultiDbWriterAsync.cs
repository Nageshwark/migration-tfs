﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheMultiDbWriterAsync<T, K>
    {
        Task AddAsync(K databaseKey, T value, bool isMeta = false);
        Task AddIfDoesntExistAsync(K databaseKey, T value, bool isMeta = false);
        Task AddOrUpdateAsync(K databaseKey, Expression<Func<T, bool>> filter, T value, bool isMeta = false);
        Task AddRangeAsync(K databaseKey, IEnumerable<T> values, bool isMeta = false);
        Task AddRangeIfDoesntExistAsync(K databaseKey, IEnumerable<T> values, bool isMeta = false);
        Task AddOrUpdateRangeAsync(K databaseKey, Expression<Func<T, bool>> filter, IEnumerable<T> values);
        Task DeleteAsync(K databaseKey, Expression<Func<T, bool>> filter, bool isMeta = false);
    }
}
