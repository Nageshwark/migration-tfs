﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheExpire
    {
        DateTime CacheExpireDate { get; set; }
    }
}
