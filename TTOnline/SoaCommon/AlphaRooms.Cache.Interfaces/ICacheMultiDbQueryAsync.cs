﻿namespace AlphaRooms.Cache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface ICacheMultiDbQueryAsync<T, K>
    {
        Task<T> GetItemAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false);
        Task<T> GetItemOrNullAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false);
        Task<T> GetFirstItemOrNullAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false);
        Task<IEnumerable<T>> GetItemsAsync(K databaseKey, Expression<Func<T, bool>> query, bool isMeta = false);
        Task<IEnumerable<T>> GetItemsAsync(K databaseKey, Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false);
        Task<LimitedResults<T>> GetLimitedItemsAsync(K databaseKey, Expression<Func<T, bool>> query, int startIndex, int count, bool isMeta = false);
        Task<LimitedResults<T>> GetLimitedItemsAsync(K databaseKey, Expression<Func<T, bool>> query, int startIndex, int count, ICacheSort<T> sort, bool isMeta = false);
    }
}
