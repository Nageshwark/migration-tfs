﻿namespace AlphaRooms.Cache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface ICacheQueryAsync<T>
    {
        Task<T> GetItemAsync(Expression<Func<T, bool>> query, bool isMeta = false);
        Task<T> GetItemOrNullAsync(Expression<Func<T, bool>> query, bool isMeta = false);
        Task<T> GetFirstItemOrNullAsync(Expression<Func<T, bool>> query, bool isMeta = false);
        Task<T> GetFirstItemOrNullAsync(Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false);
        Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> query, bool isMeta = false);
        Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> query, ICacheSort<T> sort, bool isMeta = false);
        Task<LimitedResults<T>> GetLimitedItemsAsync(Expression<Func<T, bool>> query, int startIndex, int count, bool isMeta = false);
        Task<LimitedResults<T>> GetLimitedItemsAsync(Expression<Func<T, bool>> query, int startIndex, int count, ICacheSort<T> sort, bool isMeta = false);
    }
}
