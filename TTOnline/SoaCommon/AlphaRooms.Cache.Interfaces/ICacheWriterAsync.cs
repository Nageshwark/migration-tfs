﻿namespace AlphaRooms.Cache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface ICacheWriterAsync<T>
    {
        Task AddAsync(T value, bool isMeta = false);
        Task AddIfDoesntExistAsync(T value, bool isMeta = false);
        Task AddOrUpdateAsync(Expression<Func<T, bool>> filter, T value, bool isMeta = false);
        Task AddRangeAsync(IEnumerable<T> values, bool isMeta = false);
        Task AddRangeIfDoesntExistAsync(IEnumerable<T> values, bool isMeta = false);
        Task AddOrUpdateRangeAsync(Expression<Func<T, bool>> filter, IEnumerable<T> values, bool isMeta = false);
        Task DeleteAsync(Expression<Func<T, bool>> filter, bool isMeta = false);
    }
}
