﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Cache.Interfaces
{
    public interface ICacheMultiDatabaseController<T, K>
    {
        int GetDatabaseIndexByKey(int multiDatabasesCount, K databaseKey);
    }
}
    
