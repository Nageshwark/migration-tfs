﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace AlphaRooms.Utilities.Ninject
{
    /// <summary>
    /// Used to instantiate apicontrollers using ninject container
    /// To get this working. Add the following on CreateKernel method right after the RegisterServices is executed
    /// GlobalConfiguration.Configuration.DependencyResolver = new NinjectMVC3DependencyResolver(kernel);
    /// </summary>
    public class NinjectMVC3DependencyResolver : NinjectMVC3DependencyScope, IDependencyResolver
    {
        private IKernel kernel;

        public NinjectMVC3DependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectMVC3DependencyScope(kernel.BeginBlock());
        }
    }
}
