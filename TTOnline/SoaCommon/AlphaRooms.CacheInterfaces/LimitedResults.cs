﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.CacheInterfaces
{
    public class LimitedResults<T>
    {
        private IEnumerable<T> results;
        private int totalResultsCount;

        public LimitedResults()
        {
        }

        public LimitedResults(IEnumerable<T> results, int totalResultsCount)
        {
            this.results = results;
            this.totalResultsCount = totalResultsCount;
        }

        public IEnumerable<T> Results { get { return this.results; } }
        public int TotalResultsCount { get { return this.totalResultsCount; }  }
    }
}
