﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.CacheInterfaces
{
    public interface ICacheQueryAsync
    {
        Task<T> GetItemAsync<T>(string collectionName, Expression<Func<T, bool>> query);

        Task<T> GetItemOrNullAsync<T>(string collectionName, Expression<Func<T, bool>> query);

        Task<T> GetFirstItemOrNullAsync<T>(string collectionName, Expression<Func<T, bool>> query);

        Task<IEnumerable<T>> GetItemsAsync<T>(string collectionName, Expression<Func<T, bool>> query);
        
        Task<LimitedResults<T>> GetLimitedItemsAsync<T>(string collectionName, Expression<Func<T, bool>> query, int startIndex, int count);

        bool IsCacheExpireDateExpired(DateTime dateTime);
    }
}
