﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace AlphaRooms.CacheInterfaces
{
    public interface ICacheDbHelper
    {
        MongoCollection<T1> GetMongoCollection<T1>(string collectionName);

        bool WriteDataToMongo<T>(T data, string collectionName, IMongoQuery query);
    }
}
