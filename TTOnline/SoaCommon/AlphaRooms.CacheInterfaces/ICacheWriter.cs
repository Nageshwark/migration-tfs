﻿using System;
using System.Collections.Generic;

namespace AlphaRooms.CacheInterfaces
{
    public interface ICacheWriter
    {
        void Add<T>(string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;

        void AddIfDoesntExist<T>(string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;

        void AddIfDoesntExist<T>(string databaseKey, string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;
        
        void AddOrUpdate<T>(string collectionName, string keyName, object keyValue, T value);

        void AddRange<T>(string collectionName, IEnumerable<T> values, TimeSpan cacheExpireTimeout) where T : ICacheExpire;

        void AddRange<T>(string databaseKey, string collectionName, IEnumerable<T> data);

        void AddRangeIfDoesntExist<T>(string collectionName, IEnumerable<T> data);

        void AddRangeIfDoesntExist<T>(string databaseKey, string collectionName, IEnumerable<T> data);

        void AddOrUpdateRange<T>(string databaseKey, string collectionName, string keyName, Func<T, object> getKeyValue, IEnumerable<T> data);

        void Update<T>(string databaseKey, string collectionName, string keyName, object keyValue, T value);
    }
}
