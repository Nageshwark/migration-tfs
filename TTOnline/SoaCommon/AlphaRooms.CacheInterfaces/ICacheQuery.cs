﻿using MongoDB.Driver;
using System.Collections.Generic;

namespace AlphaRooms.CacheInterfaces
{
    public interface ICacheQuery
    {
        T GetItem<T>(string collectionName, string keyName, object keyValue);

        T GetItem<T>(string collectionName, IEnumerable<IMongoQuery> query);

        T GetItem<T>(string databaseKey, string collectionName, string keyName, object keyValue);

        T GetItem<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query);

        IEnumerable<T> GetItems<T>(string collectionName, IEnumerable<IMongoQuery> query);

        IEnumerable<T> GetItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query);

        IEnumerable<T> GetItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, IMongoSortBy sort);

        LimitedResults<T> GetLimitedItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, int startIndex, int count);

        LimitedResults<T> GetLimitedItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, int startIndex, int count, IMongoSortBy sort);
    }
}