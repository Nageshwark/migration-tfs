﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.CacheInterfaces
{
    public interface ICacheWriterAsync
    {
        Task AddIfDoesntExistAsync<T>(string collectionName, T value);

        Task AddIfDoesntExistAsync<T>(string collectionName, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;

        Task AddOrUpdateAsync<T>(string collectionName, Expression<Func<T, bool>> filter, T value);

        Task AddOrUpdateAsync<T>(string collectionName, Expression<Func<T, bool>> filter, T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;
        
        Task AddRangeIfDoesntExistAsync<T>(string collectionName, IEnumerable<T> values);

        Task AddRangeIfDoesntExistAsync<T>(string collectionName, IEnumerable<T> values, TimeSpan cacheExpireTimeout) where T : ICacheExpire;

        void SetCacheExpireTimeout<T>(T value, TimeSpan cacheExpireTimeout) where T : ICacheExpire;
    }
}
