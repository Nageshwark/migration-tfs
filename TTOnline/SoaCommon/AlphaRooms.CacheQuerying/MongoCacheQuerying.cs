﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using AlphaRooms.SOACommon.Interfaces;
using AlphaRooms.CacheInterfaces;
using MongoDB.Bson;

namespace AlphaRooms.CacheQuerying
{
    public class MongoCacheQuerying : ICacheQuery 
    {
        private readonly IDbConnection dbHelper;
        
        public MongoCacheQuerying(IDbConnection dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public T GetItem<T>(string collectionName, string keyName, object keyValue)
        {
            return dbHelper.GetMongoCollection<T>(collectionName).FindOne(Query.EQ(keyName, BsonValue.Create(keyValue)));
        }

        public T GetItem<T>(string collectionName, IEnumerable<IMongoQuery> query)
        {
            return dbHelper.GetMongoCollection<T>(collectionName).FindOne(Query.And(query));
        }

        public T GetItem<T>(string databaseKey, string collectionName, string keyName, object keyValue)
        {
            return dbHelper.GetMongoCollection<T>(databaseKey, collectionName).FindOne(Query.EQ(keyName, BsonValue.Create(keyValue)));
        }

        public T GetItem<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query)
        {
            return dbHelper.GetMongoCollection<T>(databaseKey, collectionName).FindOne(Query.And(query));
        }

        public IEnumerable<T> GetItems<T>(string collectionName, IEnumerable<IMongoQuery> query)
        {
            return dbHelper.GetMongoCollection<T>(collectionName).Find(Query.And(query)).SetFlags(QueryFlags.Partial);
        }

        public IEnumerable<T> GetItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query)
        {
            return dbHelper.GetMongoCollection<T>(databaseKey, collectionName).Find(Query.And(query)).SetFlags(QueryFlags.Partial);
        }

        public IEnumerable<T> GetItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, IMongoSortBy sort)
        {
            return dbHelper.GetMongoCollection<T>(databaseKey, collectionName).Find(Query.And(query)).SetSortOrder(sort).SetFlags(QueryFlags.Partial);
        }

        public LimitedResults<T> GetLimitedItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, int startIndex, int count)
        {
            var items = dbHelper.GetMongoCollection<T>(databaseKey, collectionName).Find(Query.And(query)).SetSkip(startIndex).SetLimit(count).SetFlags(QueryFlags.Partial);
            return new LimitedResults<T>(items.ToArray(), (int)items.Count());
        }

        public LimitedResults<T> GetLimitedItems<T>(string databaseKey, string collectionName, IEnumerable<IMongoQuery> query, int startIndex, int count, IMongoSortBy sort)
        {
            var items = dbHelper.GetMongoCollection<T>(databaseKey, collectionName).Find(Query.And(query)).SetSortOrder(sort).SetSkip(startIndex).SetLimit(count).SetFlags(QueryFlags.Partial);
            return new LimitedResults<T>(items.ToArray(), (int)items.Count());
        }
    }
}
