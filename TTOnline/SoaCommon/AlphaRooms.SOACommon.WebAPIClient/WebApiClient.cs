﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.WebAPIClient
{
    public class WebApiClient : IWebApiClient
    {
       
        public async Task<T2> Post<T1,T2>(T1 Request, string baseUrl,string urlSuffix, MediaTypeFormatter mediaTypeFormatter)
        {

            try
            {

            
                //Initialize();
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(baseUrl);
                    var Response = await httpClient.PostAsync<T1>(urlSuffix, Request, mediaTypeFormatter);
                    //Response.EnsureSuccessStatusCode();
                    return await Response.Content.ReadAsAsync<T2>();
                }
            }
            catch (Exception ex)
            {

                return default(T2);
            }
        }

        public async Task<T1> Get<T1>(string baseUrl, string urlSuffix)
        {
            try
            {

                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(baseUrl);

                    var Response = await httpClient.GetAsync(urlSuffix);
                    //Response.EnsureSuccessStatusCode();
                    return await Response.Content.ReadAsAsync<T1>();
                }

            }
            catch (Exception ex)
            {

                return default(T1);
            }

        }
    }
}
