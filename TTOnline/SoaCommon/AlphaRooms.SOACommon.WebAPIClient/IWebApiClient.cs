﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.WebAPIClient
{
    public interface IWebApiClient
    {
        Task<T2> Post<T1, T2>(T1 Request, string baseUrl, string urlSuffix, MediaTypeFormatter mediaTypeFormatter);
        
        Task<T1> Get<T1>(string baseUrl, string urlSuffix);
    }
}
