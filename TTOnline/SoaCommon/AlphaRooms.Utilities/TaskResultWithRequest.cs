﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public class TaskWithRequest<TRequest, TResult> : Task<TResult>
    {
        private TRequest request;

        public TaskWithRequest(Func<TRequest, TResult> func, TRequest request) : base(() => func(request))
        {
            this.request = request;
        }

        public TRequest Request
        {
            get { return this.request; }
        }
    }
}
