﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class StringExtensions
    {
        public static string FirstOrDefaultPrice(this string value)
        {
            Match m = Regex.Match(value, @"[$€£]?\d{1,3}(([.,]\d{3})?){1,3}[.,]\d{2}");
            return (m != null ? m.Value : null);
        }

        public static string FirstOrDefaultNumber(this string value)
        {
            Match m = Regex.Match(value, @"\d{1,2}");
            return (m != null ? m.Value : null);
        }

        public static bool Contains(this string value, string v, StringComparison comparison)
        {
            return value.IndexOf(v, comparison) > -1;
        }

        public static string ToPascalCase(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            // under the hud this use unmanaged code and is really fast! Faster than char.ToLower
            string lowerValue = value.ToLowerInvariant();
            char[] chars = lowerValue.ToCharArray();

            // first letter is always captail
            bool upper = true;

            for (var i = 0; i < value.Length; i++)
            {
                if (upper)
                {
                    chars[i] = Char.ToUpperInvariant(chars[i]);
                }

                upper = !char.IsLetterOrDigit(chars[i]);
            }

            return new string(chars);
        }

        public static bool IsLeftEqual(this string value, string compare)
        {
            if (value.Length < compare.Length) return false;
            for (int i = 0; i < compare.Length; i++) { if (value[i] != compare[i]) return false; }
            return true;
        }

        public static bool IsRightEqual(this string value, string compare)
        {
            if (value.Length < compare.Length) return false;
            int j = value.Length - compare.Length;
            for (int i = 0; i < compare.Length; i++) { if (value[j + i] != compare[i]) return false; }
            return true;
        }

        public static int IndexOfFirstLetter(this string value)
        {
            for (int i = 0; i < value.Length; i++) { if (char.IsLetter(value[i])) return i; }
            return -1;
        }

        public static string Compress(this string value)
        {
            if (string.IsNullOrEmpty(value)) return value;

            byte[] buffer = System.Text.Encoding.Unicode.GetBytes(value);
            MemoryStream ms = new MemoryStream();
            using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return Convert.ToBase64String(gzBuffer);
        }

        public static string Decompress(this string value)
        {
            byte[] gzBuffer = Convert.FromBase64String(value);
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }

                return System.Text.Encoding.Unicode.GetString(buffer, 0, buffer.Length);
            }
        }
    }
}