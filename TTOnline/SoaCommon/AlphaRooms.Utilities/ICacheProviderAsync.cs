﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public interface ICacheProviderAsync<T>
    {
        void Register(Func<Task<T[]>> funcSourceAll);
        void Register(Func<Task<T[]>> funcSourceAll, Func<String, Task<T>> funcSourceKey, Func<T, String> funcGetKey);
        void Register(string keyPrefix, Func<Task<T[]>> funcSourceAll);
        void Register(string keyPrefix, Func<Task<T[]>> funcSourceAll, Func<String, Task<T>> funcSourceKey, Func<T, String> funcGetKey);
        Task<T[]> GetAllAsync();
        Task<T> GetByKeyAsync(string key);
        Task<T[]> GetByKeysAsync(string[] keys);
        Task<T> GetOrSetAsync(string key, Func<Task<T>> fetchFunction);
        Task<TSource> GetOrSetAsync<TSource>(string key, Func<Task<TSource>> fetchFunction);
    }
}
