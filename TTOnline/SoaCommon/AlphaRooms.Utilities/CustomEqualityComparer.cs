﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public class CustomEqualityComparer<T> : IEqualityComparer<T>
    {
        private Func<T, T, bool> func;

        public CustomEqualityComparer(Func<T, T, bool> func)
        {
            this.func = func;
        }

        public bool Equals(T x, T y)
        {
            return this.func.Invoke(x, y);
        }

        public int GetHashCode(T obj)
        {
            return this.GetHashCode();
        }
    }
}
