﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class Async
    {
        public static async Task<IEnumerable<R>> RunAllIgnoreFailed<R>(params Func<R>[] funcs)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<R>();
            await Task.WhenAll(funcs.Select(i => Task.Run(() =>
            {
                try
                {
                    R result = i.Invoke();
                    results.Enqueue(result);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            })));
            if (results.Count == 0 && exceptions.Count > 0) throw new AggregateException(exceptions);
            return results;
        }

        public static async Task<IEnumerable<R>> RunAllIgnoreFailed<R>(params Func<Task<R>>[] funcs)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<R>();
            await Task.WhenAll(funcs.Select(i => Task.Run(async() =>
            {
                try
                {
                    R result = await i.Invoke();
                    results.Enqueue(result);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            })));
            if (results.Count == 0 && exceptions.Count > 0) throw new AggregateException(exceptions);
            return results;
        }

        public static async Task ParallelForEach<T>(IEnumerable<T> items, Func<T, Task> func)
        {
            await Task.WhenAll(items.Select((i) => Task.Run(async() => await func.Invoke(i))));
        }

        public static async Task<IEnumerable<R>> ParallelForEach<T, R>(IEnumerable<T> items, Func<T, Task<R>> func)
        {
            return await Task.WhenAll(items.Select((i) => Task.Run(async() => await func.Invoke(i))));
        }

        public static async Task<IEnumerable<R>> ParallelForEachMany<T, R>(IEnumerable<T> items, Func<T, Task<IEnumerable<R>>> func)
        {
            return (await Task.WhenAll(items.Select((i) => Task.Run(async() => await func.Invoke(i))))).SelectMany(i => i);
        }

        public static async Task<IEnumerable<R>> ParallelForEachIgnoreFailed<T, R>(IEnumerable<T> items, Func<T, R> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<R>();
            await Task.WhenAll(items.Select(i => Task.Run(() =>
            {
                try
                {
                    R result = func.Invoke(i);
                    results.Enqueue(result);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            })));
            if (results.Count == 0 && exceptions.Count > 0) throw new AggregateException(exceptions);
            return results;
        }

        public static async Task<IEnumerable<R>> ParallelForEachIgnoreFailed<T, R>(IEnumerable<T> items, Func<T, Task<R>> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<R>();
            await Task.WhenAll(items.Select(i => Task.Run(async() =>
            {
                try
                {
                    R result = await func.Invoke(i);
                    results.Enqueue(result);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            })));
            if (results.Count == 0 && exceptions.Count > 0) throw new AggregateException(exceptions);
            return results;
        }
        
        public static async Task<IEnumerable<R>> ParallelForEachIgnoreFailedMany<T, R>(IEnumerable<T> items, Func<T, Task<IEnumerable<R>>> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<IEnumerable<R>>();
            await Task.WhenAll(items.Select(i => Task.Run(async() =>
            {
                try
                {
                    var result = await func.Invoke(i);
                    results.Enqueue(result);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            })));
            if (results.Count == 0 && exceptions.Count > 0) throw new AggregateException(exceptions);
            return results.SelectMany(i => i).ToArray();
        }
    }
}
