﻿
namespace AlphaRooms.Utilities
{
    /// <summary>
    /// Required for the AutoMapper Mapper.CreateMap to be thread safe
    /// </summary>
    public sealed class Automapper
    {
        public static object MapperCreateMapLock = new object();
    }
}
