﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading;

    /// <summary>
    /// Responsible for sharing objects in a round robin fashion.
    /// If called from different threads, the same object maybe shared between them.
    /// This class is thread safe.
    /// </summary>
    /// <typeparam name="T">the type that will be shared</typeparam>
    public class RoundRobin<T>
    {
        private readonly T[] values;
        private readonly uint valuesLength;

        // start on -1 so when it get incremented for the first time it goes to 0
        private int currentIndex = -1; // this is uint.MaxValue in binary

        /// <summary>
        /// Creates a round robin
        /// </summary>
        /// <param name="values">the values to enumerate through, these will be shallow copied</param>
        public RoundRobin(IEnumerable<T> values)
            : this(values, false)
        {
        }

        /// <summary>
        /// Creates a round robin
        /// </summary>
        /// <param name="values">the values to enumerate through, these will be shallow copied</param>
        /// <param name="shuffle">if true, the values will be shuffled to produce a random order</param>
        public RoundRobin(IEnumerable<T> values, bool shuffle)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            // this shallow copies, so we do not shuffle the passed array
            this.values = values.ToArray();
            this.valuesLength = (uint)this.values.Length;

            if (this.valuesLength == 0)
            {
                throw new ArgumentException("Must contain at least one element", "values");
            }

            if (shuffle)
            {
                RoundRobin<T>.Shuffle(this.values);
            }
        }

        public T GetValue()
        {
            // this does wrap correctly, so when we get to the end we start at 0 again
            uint localIndex = (uint)Interlocked.Increment(ref this.currentIndex);

            localIndex %= this.valuesLength;

            return this.values[localIndex];
        }

        private static void Shuffle(T[] array)
        {
            Random rng = RoundRobin<T>.CreateRandom();

            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }

        private static Random CreateRandom()
        {
            /*
             * We do not want to seed the random number based on time.
             * This leads to the same numbers being created, this happened.
             */

            byte[] seedArray = new byte[4];

            // GetBytes is a thread safe method see https://msdn.microsoft.com/en-us/library/wb9c8c67(v=vs.110).aspx
            new RNGCryptoServiceProvider().GetBytes(seedArray);

            int seed = BitConverter.ToInt32(seedArray, 0);

            // Seed the random number generator manually, this prevents it been based on time
            Random retVal = new Random(seed);

            return retVal;
        }
    }
}