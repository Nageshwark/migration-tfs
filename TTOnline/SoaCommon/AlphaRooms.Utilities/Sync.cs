﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class Sync
    {
        public static IEnumerable<TResult> ParallelForEach<TItem, TResult>(IEnumerable<TItem> items, Func<TItem, TResult> func)
        {
            var results = new ConcurrentQueue<TResult>();
            Parallel.ForEach(items, i => results.Enqueue(func.Invoke(i)));
            return results;
        }
        
        public static IEnumerable<TResult> ParallelForEachMany<TItem, TResult>(IEnumerable<TItem> items, Func<TItem, IEnumerable<TResult>> func)
        {
            var results = new ConcurrentQueue<IEnumerable<TResult>>();
            Parallel.ForEach(items, i => results.Enqueue(func.Invoke(i)));
            return results.SelectMany(i => i).ToArray();
        }

        public static void ParallelForEachIgnoreFailed<TItem>(IEnumerable<TItem> items, Action<TItem> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            Parallel.ForEach(items, i =>
            {
                try
                {
                    func.Invoke(i);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            });
            if (exceptions.Any() && exceptions.Count == items.Count()) throw new AggregateException(exceptions);
        }

        public static IEnumerable<TResult> ParallelForEachIgnoreFailed<TItem, TResult>(IEnumerable<TItem> items, Func<TItem, TResult> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<TResult>();
            Parallel.ForEach(items, i =>
            {
                try
                {
                    results.Enqueue(func.Invoke(i));
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            });
            if (exceptions.Any() && exceptions.Count == items.Count()) throw new AggregateException(exceptions);
            return results;
        }

        public static IEnumerable<TResult> ParallelForEachManyIgnoreFailed<TItem, TResult>(IEnumerable<TItem> items, Func<TItem, IEnumerable<TResult>> func)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var results = new ConcurrentQueue<IEnumerable<TResult>>();
            Parallel.ForEach(items, i =>
            {
                try
                {
                    results.Enqueue(func.Invoke(i));
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            });
            if (exceptions.Any() && exceptions.Count == items.Count()) throw new AggregateException(exceptions);
            return results.SelectMany(i => i).ToArray();
        }

        public static void ParallelForEachIgnoreFailed<TItem>(IEnumerable<TItem> items, Action<TItem> func, Action<IEnumerable<Tuple<TItem, Exception>>> exceptionHandler)
        {   
            var exceptions = new ConcurrentQueue<Tuple<TItem,Exception>>();
            Parallel.ForEach(items, i =>
            {
                try
                {
                    func.Invoke(i);
                }
                catch (Exception ex)
                {   
                    exceptions.Enqueue(Tuple.Create(i, ex));
                }
            });

            if (exceptions.Any())
            {
                exceptionHandler.Invoke(exceptions);
                if(exceptions.Count == items.Count()) throw new AggregateException(exceptions.Select(x => x.Item2));
            }
        }

        public static IEnumerable<T> RunMany<T>(params Func<IEnumerable<T>>[] functions)
        {
            var tasks = functions.Select(i => Task.Run(i)).ToArray();
            Task.WaitAll(tasks);
            return tasks.SelectMany(i => i.Result);
        }
    }
}
