﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Utilities;

namespace System
{
    public static class EnumExtensions
    {
        private static Dictionary<Type, Dictionary<string, object>> cache;

        /// <summary>
        /// Returns the enum item description or throw an exception when the item doesn't have one.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            string result = value.GetDescriptionOrNull();
            if (result == null) throw new DescriptionNotFoundException(value);
            return result;
        }

        /// <summary>
        /// Returns the enum item description or NULL when the item doesn't have one.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionOrNull(this Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            DescriptionAttribute descriptionAttribute = fieldInfo.GetCustomAttribute<DescriptionAttribute>(false);
            if (descriptionAttribute == null) return null;
            return descriptionAttribute.Description;
        }

        /// <summary>
        /// Returns the enum item description or the item name in case there is no description.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringWithDescription(this Enum value) {
            string description = value.GetDescriptionOrNull();
            if (description != null) return description;
            return value.ToString();
        }

        /// <summary>
        /// Converts the string representation of the name or numeric value of one or
        //  more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        /// <summary>
        /// Converts the string representation of the description or name or numeric value of one or
        //     more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="description"></param>
        /// <returns></returns>
        public static T ParseFromDescription<T>(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new ArgumentNullException("description");
            Type type = typeof(T);
            if (cache == null) cache = new Dictionary<Type, Dictionary<string, object>>();
            Dictionary<string, object> descriptions;
            cache.TryGetValue(type, out descriptions);
            if (descriptions == null)
            {
                descriptions = new Dictionary<string, object>();
                foreach (FieldInfo fieldInfo in type.GetFields())
                {
                    if (fieldInfo.Name != "value__")
                    {
                        DescriptionAttribute descriptionAttribute = fieldInfo.GetCustomAttribute<DescriptionAttribute>(false);
                        if (descriptionAttribute != null)
                        {
                            descriptions.Add(descriptionAttribute.Description.ToLower(), fieldInfo.GetValue(type));
                        }
                    }
                }
                cache.Add(type, descriptions);
            }
            else
            {
                descriptions = cache[type];
            }

            object item;
            descriptions.TryGetValue(value.ToLower(), out item);
            if (item == null) return EnumExtensions.Parse<T>(value);
            return (T)item;
        }

        /// <summary>
        /// return the attribute instance used for decorating an enum member
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name)
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }

        public static T ToEnumOrDefault<T>(this string value, T defaultValue) where T : struct, IConvertible
        {
            T enumValue;
            if (Enum.TryParse<T>(value, out enumValue)) return enumValue;
            return defaultValue;
        }
    }
}