﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    /*
     * ------- WARNING -------
     * This call is performance critical (speed & memory).
     * Minimum fields and allocation are used to ensure performance.
     * Needs to be thread safe, it will be called from multiple threads at the same time.
     * 
     * It is easy to introduce errors into this class.
     * When this first went live it contained an error where the store buffer was not flushed resulting in deadlocking.
     * This class is lockless and ordering of the instructions is important, every change must be carefully reviewed.
     *   
     *                         .//-:`
     *                        /yyyyos:
     *                      `oyyyyyyos+
     *                     .syyyyyyyysos`
     *                    :yyyyyyyyyyyyos-
     *                  `+yyyy:```.:yyyyos:
     *                 `oyyyys      +yyyyos+`
     *                -syyyyys      oyyyyysos.
     *               :yyyyyyyy      syyyyyyyos-
     *             `+yyyyyyyyy`     yyyyyyyyyos/
     *            .oyyyyyyyyyy-     yyyyyyyyyysoo`
     *           -syyyyyyyyyyy:    `yyyyyyyyyyysos.
     *          /yyyyyyyyyyyyy/    -yyyyyyyyyyyyyos:
     *        `+yyyyyyyyyyyyyyo    :yyyyyyyyyyyyyyos+
     *       .syyyyyyyyyyyyyyyy+--/syyyyyyyyyyyyyyysoo`
     *      :yyyyyyyyyyyyyyyyyys+++syyyyyyyyyyyyyyyys+s.
     *     /yyyyyyyyyyyyyyyyyy/`   `oyyyyyyyyyyyyyyyyyos:
     *   `oyyyyyyyyyyyyyyyyyyy:     +yyyyyyyyyyyyyyyyyyoo+
     *   oyyyyyyyyyyyyyyyyyyyyyo/:/oyyyyyyyyyyyyyyyyyyyy+s
     *   `:+ssssssssssssssssssssssssssssssssssssssssssssss
     */

    /// <summary>
    /// Represents a cache request that could be shared between threads
    /// </summary>
    internal class CacheRequest
    {
        /// <summary>
        /// This is private to stop other class from using this class.
        /// Used to record when an expectation is to be shared.
        /// It is wrapped in the class that we only have access to so it must be an expectation that needs to be thrown.
        /// </summary>
        private class ExceptionBox
        {
            private readonly Exception reportedException;

            public ExceptionBox(Exception reportedException)
            {
                this.reportedException = reportedException;
            }

            public Exception ReportedException
            {
                get
                {
                    return this.reportedException;
                }
            }
        }

        private readonly string key;
        private object result;

        // we do not dispose of this, as it is impossible to tell if other threads are still using this, Task<T> also has this behaviour
        private ManualResetEventSlim waitHandle;

        /// <summary>
        /// creates a CacheRequest
        /// </summary>
        /// <param name="key">the cache key for this request</param>
        public CacheRequest(string key)
        {
            this.key = key;
        }

        public string Key
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return this.key;
            }
        }

        /// <summary>
        /// Record an exception so that it is thrown by the other threads sharing this request.
        /// </summary>
        /// <param name="ex">the expection to be thrown by callers of GetResult</param>
        public void SetException(Exception ex)
        {
            if (ex == null)
            {
                ArgumentNullException argumentNullEx = new ArgumentNullException(nameof(ex));

                // we need to throw this to anyone waiting for results else they will be waiting forever
                this.SetException(argumentNullEx);

                // now throw on this thread
                throw argumentNullEx;
            }

            // wrap this in the box to ensure it is thrown later
            ExceptionBox exceptionBox = new ExceptionBox(ex);

            this.SetResult(exceptionBox);
        }

        /// <summary>
        /// Sets the result of the cache request, this will be shared to the other threads that call GetResult
        /// </summary>
        /// <param name="result">the object to share</param>
        public void SetResult(object result)
        {
            if (result == null)
            {
                ArgumentNullException ex = new ArgumentNullException(nameof(result));

                // we need to throw this to anyone waiting for results else they will be waiting forever
                this.SetException(ex);

                // now throw on this thread
                throw ex;
            }

            // save the result
            this.result = result;

            /*
             * This MemoryBarrier MUST be here. 
             * 1. It prevents reordering of the instructions, ensuring the order, so allowing this to be lock free.
             * 2. It flushs the Store Buffer for the processor, this page explains why https://msdn.microsoft.com/en-us/magazine/jj883956.aspx
             */
            Thread.MemoryBarrier();

            if (this.waitHandle != null)
            {
                // if the waitHandle is not null there is a waiting thread, we must singal them
                this.waitHandle.Set();
            }
        }

        /// <summary>
        /// Get the results or throws an exception from the thread performing the cache request
        /// </summary>
        /// <returns>the value from the cache request or throws the expection recived from the cache request</returns>
        public object GetResult()
        {
            if (this.result == null)
            {
                // result not yet ready

                // create handle
                ManualResetEventSlim newWaitHandle = new ManualResetEventSlim();

                /*
                 * 1. We could now be racing other threads so exchange only if still null, to ensure that we only use one waitHandle.
                 * 2. This is also a MemoryBarrier so instructions will not be reorder, this is important to ensure the order of instructions to be lock free.
                 * 3. It flushs the Store Buffer for the processor, so all threads see the same values
                 */
                ManualResetEventSlim currentWaitHandle = Interlocked.CompareExchange(ref this.waitHandle, newWaitHandle, null);

                if (currentWaitHandle == null)
                {
                    // we set the handle
                    currentWaitHandle = newWaitHandle;
                }
                else
                {
                    // we did not set it
                    newWaitHandle.Dispose();
                }

                // this last check is needed as the result could have been set, before we set the waitHandle
                if (this.result == null)
                {
                    // still not complete but because of the ordering, the setting thread will have to singal the handle
                    currentWaitHandle.Wait();
                }
            }

            // result will alway be set by this point
            this.ThrowIfResultException();

            return this.result;
        }

        private void ThrowIfResultException()
        {
            ExceptionBox exceptionBox = this.result as ExceptionBox;

            if (exceptionBox != null)
            {
                throw exceptionBox.ReportedException;
            }
        }
    }
}