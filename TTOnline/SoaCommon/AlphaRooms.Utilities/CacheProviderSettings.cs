﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public class CacheProviderSettings : ICacheProviderSettings
    {
        private string keyPrefix;
        private bool isCachingEnabled;
        private TimeSpan cacheExpireTime;
        private TimeSpan cachingTimeout;
        private TimeSpan commandTimeout;

        public CacheProviderSettings(string keyPrefix, bool isCachingEnabled, TimeSpan cacheExpireTime, TimeSpan cachingTimeout, TimeSpan commandTimeout)
        {
            this.keyPrefix = keyPrefix;
            this.isCachingEnabled = isCachingEnabled;
            this.cacheExpireTime = cacheExpireTime;
            this.cachingTimeout = cachingTimeout;
            this.commandTimeout = commandTimeout;
        }

        public string KeyPrefix { get { return this.keyPrefix; } }
        public bool IsCachingEnabled { get { return this.isCachingEnabled; } }
        public TimeSpan CacheExpireTime { get { return this.cacheExpireTime; } }
        public TimeSpan CachingTimeout { get { return this.cachingTimeout; } }
        public TimeSpan CommandTimeout { get { return this.commandTimeout; } }
    }
}
