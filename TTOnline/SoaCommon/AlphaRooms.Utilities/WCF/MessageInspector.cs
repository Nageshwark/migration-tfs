﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.WCF
{
    public class MessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        public List<KeyValuePair<string, object>> Headers { get; set; }
        public MessageProperties Properties { get; set; }
        public MessageInspector(List<KeyValuePair<string, object>> headers)
        {
            this.Headers = headers;
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            HttpRequestMessageProperty httpRequest;

            if (request.Properties.ContainsKey(HttpRequestMessageProperty.Name))
            {
                httpRequest = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            }
            else
            {
                httpRequest = new HttpRequestMessageProperty();
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequest);
            }

            if (httpRequest == null)
            {
                httpRequest = new HttpRequestMessageProperty();
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequest);
            }



            foreach (var header in this.Headers)
            {
                httpRequest.Headers.Add(header.Key, header.Value.ToString());
            }




            return null;

        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            this.Properties = reply.Properties;
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            OperationContext.Current.IncomingMessageProperties.Add("Context", request.Properties);

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {

        }
    }
}
