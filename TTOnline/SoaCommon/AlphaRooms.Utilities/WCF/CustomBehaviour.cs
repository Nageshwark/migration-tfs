﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;

namespace AlphaRooms.Utilities.WCF
{
    [AttributeUsage(AttributeTargets.All)]
    public class CustomBehavior : Attribute, IEndpointBehavior
    {
        public MessageInspector MessageInspector { get; set; }

        #region IEndpointBehavior Members

        public void Validate(ServiceEndpoint endpoint)
        {

        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {

        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            this.MessageInspector = new MessageInspector(this.Headers);

            clientRuntime.MessageInspectors.Add(MessageInspector);
        }


        #endregion

        #region Public Members



        public List<KeyValuePair<string, object>> Headers { get; set; }

        #endregion Public Members




    }
}
