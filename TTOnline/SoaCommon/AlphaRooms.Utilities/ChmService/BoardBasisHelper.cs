﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.ChmService
{
    public class BoardBasisHelper
    {
        public string GetBoardBasisDetails(string ratePlanType)
        {
            //var board = new BoardBasis();
            var boardDictionary = new Dictionary<string, string>
            {
                { "HA", "Room Only" },
                { "HD", "Bed and Breakfast"},
                {"MP", "Half Board"},
                {"PC", "Full Board"},
                {"TI", "All Inclusive"}
            };

            return boardDictionary[ratePlanType];

            //if (mealsIncluded ==null)
            //    return new BoardBasis { Code = "RO", Name = "Room Only" };

            //bool breakfast = Convert.ToBoolean(mealsIncluded.Breakfast);
            //bool lunch = Convert.ToBoolean(mealsIncluded.Lunch);
            //bool dinner = Convert.ToBoolean(mealsIncluded.Dinner);

            //if (breakfast && lunch && dinner)
            //    board = new BoardBasis {Code = "FB", Name = "Full Board"};

            //if (breakfast && lunch && !dinner)
            //    board = new BoardBasis {Code = "HB", Name = "Half Board"};

            //if (breakfast && !lunch && !dinner)
            //    board = new BoardBasis {Code = "BB", Name = "Bed and Breakfast"};

            //if (!breakfast && lunch && !dinner)
            //    board = new BoardBasis{Name = "Includes Lunch"};

            //if (!breakfast && !lunch && dinner)
            //    board = new BoardBasis { Name = "Includes Dinner" };

            //if (!breakfast && !lunch && !dinner)
            //    board = new BoardBasis {Code = "RO", Name = "Room Only"};

            //return board;
        }
    }

    //public class BoardBasis
    //{
    //    public string Name { get; set; }
    //    public string Code { get; set; }
    //}
}
