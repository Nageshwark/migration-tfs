﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class LinqExpression
    {
        private static readonly MethodInfo anyMethod = typeof(Enumerable).GetMethods().Where(m => m.Name == "Any" && m.GetParameters().Length == 2).Single();
        
        internal class SubstExpressionVisitor : System.Linq.Expressions.ExpressionVisitor
        {
            public Dictionary<Expression, Expression> subst = new Dictionary<Expression, Expression>();

            protected override Expression VisitParameter(ParameterExpression node)
            {
                Expression newValue;
                if (subst.TryGetValue(node, out newValue))
                {
                    return newValue;
                }
                return node;
            }
        }

        public static Expression<Func<T, bool>> Create<T>(Expression<Func<T, bool>> property)
        {
            return property;
        }

        public static Expression<Func<T, bool>> CreateTrue<T>()
        {
            return i => true;
        }

        public static Expression<Func<T, bool>> CreateFalse<T>()
        {
            return i => false;
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            ParameterExpression p = expr1.Parameters[0];
            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[expr2.Parameters[0]] = p;
            Expression body = Expression.AndAlso(expr1.Body, visitor.Visit(expr2.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            ParameterExpression p = expr1.Parameters[0];
            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[expr2.Parameters[0]] = p;
            Expression body = Expression.OrElse(expr1.Body, visitor.Visit(expr2.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }

        public static Expression<Func<TSource, bool>> AndAny<TSource, TChild>(this Expression<Func<TSource, bool>> value, Expression<Func<TSource, IEnumerable<TChild>>> property, Expression<Func<TChild, bool>> predicate)
        {
            var propertyParameter = property.Parameters[0];
            var propertyNemberInfo = (PropertyInfo)((MemberExpression)property.Body).Member;
            var anyExpression = Expression.Lambda<Func<TSource, bool>>(Expression.Call(anyMethod.MakeGenericMethod(typeof(TChild))
                , Expression.Property(propertyParameter, propertyNemberInfo), predicate), propertyParameter);
            return value.And(anyExpression);
        }
    }
}
