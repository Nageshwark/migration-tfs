﻿using AlphaRooms.Utilities.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Utilities.ExtensionMethods;
using Newtonsoft.Json.Serialization;

namespace AlphaRooms.Utilities
{
    public static class EntityFrameworkExtensions
    {
        public static async Task ExecuteSqlQueryAsync(this DbContext dbContext, string sqlQuery)
        {
            await dbContext.Database.ExecuteSqlCommandAsync(sqlQuery);
        }

        public static async Task ExecuteStoredProcedureAsync(this DbContext dbContext, string name, params DbParameter[] parameters)
        {

            dbContext.Database.Initialize(false);
            
            using (var conn = dbContext.Database.Connection)
            {
                await conn.OpenAsync();
                using (var command = conn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = name;
                    if (parameters != null && parameters.Any())
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    var foo = await command.ExecuteNonQueryAsync();
                }
            }
        }

        public static async Task<IEnumerable<T>> ExecuteStoredProcedureAsync<T>(this DbContext dbContext, string name, params DbParameter[] parameters)
        {

            var results = await ExecuteStoredProcedureMultiResultsAsync(dbContext, name, parameters);

            return results?.NextResult<T>();
        }

        public static async Task<MultiResultsReader> ExecuteStoredProcedureMultiResultsAsync(this DbContext dbContext, string name, params DbParameter[] parameters)
        {
            dbContext.Database.Initialize(false);
            var command = dbContext.Database.Connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = name;
            if (parameters != null && parameters.Any())
            {
                command.Parameters.AddRange(parameters);
            }
            dbContext.Database.Connection.Open();
            return new MultiResultsReader(dbContext, await command.ExecuteReaderAsync());
        }
    }
}
