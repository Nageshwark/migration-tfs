﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class StringModule
    {
        public static string NullIfEmpty(string value)
        {
            return (value != "" ? value : null);
        }

        public static bool EqualsIgnoreEmpty(string v1, string v2)
        {
            if (string.IsNullOrEmpty(v1) || string.IsNullOrEmpty(v2)) return true;
            return string.Equals(v1, v2);
        }
    }
}
