﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IQueryable extensions
    /// </summary>
    public static class IQueryableExtensions
    {
        #region Public Methods
        /// <summary>
        /// Sorts the elements of a sequence in ascending order according to a key. without the need for a strongly typed lambda expression
        /// </summary>
        /// <typeparam name="T">sequence type</typeparam>
        /// <param name="source">source sequence</param>
        /// <param name="keySelector">key selector expression</param>
        /// <returns>ordered sequence</returns>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, LambdaExpression keySelector)
        {
            return OrderBy<T>(source, keySelector, true);
        }

        /// <summary>
        /// Sorts the elements of a sequence in descending order according to a key. without the need for a strongly typed lambda expression
        /// </summary>
        /// <typeparam name="T">sequence type</typeparam>
        /// <param name="source">source sequence</param>
        /// <param name="keySelector">key selector expression</param>
        /// <returns>ordered sequence</returns>
        public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> source, LambdaExpression keySelector)
        {
            return OrderBy<T>(source, keySelector, false);
        }
        #endregion


        #region Private Methods
        /// <summary>
        /// Sorts the elements of a sequence according to a key. without the need for a strongly typed lambda expression
        /// </summary>
        /// <typeparam name="T">sequence type</typeparam>
        /// <param name="source">source sequence</param>
        /// <param name="keySelector">key selector expression</param>
        /// <param name="ascending">if true sort in ascending order otherwise descending order</param>
        /// <returns>ordered sequence</returns>
        private static IQueryable<T> OrderBy<T>(IQueryable<T> source, LambdaExpression keySelector, bool ascending)
        {
            Expression queryExpr = Expression.Call(
                typeof(Queryable), ascending ? "OrderBy" : "OrderByDescending",
                new Type[] { typeof(T), keySelector.ReturnType },
                source.Expression, keySelector);
            return source.Provider.CreateQuery<T>(queryExpr);
        }
        #endregion
    }
}
