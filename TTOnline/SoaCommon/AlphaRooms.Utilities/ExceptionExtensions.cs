﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class ExceptionExtensions
    {
        public static string GetMessageWithInnerExceptions(this Exception value)
        {
            if (value.InnerException == null) return value.Message;
            StringBuilder builder = new StringBuilder();
            WriteException(builder, value, false);
            return builder.ToString();
        }

        private static void WriteException(StringBuilder builder, Exception exception, bool isInnerException)
        {
            if (isInnerException) builder.Append("Inner Exception: ");
            builder.Append("[");
            builder.Append(exception.GetType().Name);
            builder.Append("]");
            builder.Append(exception.Message);
            builder.Append(" ");
            AggregateException aggregateException = exception as AggregateException;
            if (aggregateException != null)
            {
                foreach (Exception innerException in aggregateException.InnerExceptions)
                {
                    WriteException(builder, innerException, true);
                }
                return;
            }
            FaultException<ExceptionDetail> faultException = exception as FaultException<ExceptionDetail>;
            if (faultException != null)
            {
                if (faultException.Detail.InnerException != null)
                {
                    builder.Append(faultException.Detail.InnerException.Message);
                    builder.Append(" ");
                }
                return;
            }
            if (exception.InnerException != null)
            {
                WriteException(builder, exception.InnerException, true);
            }
        }

        public static string GetDetailedMessageWithInnerExceptions(this Exception value)
        {
            StringBuilder builder = new StringBuilder();
            WriteDetailedException(builder, value, false);
            return builder.ToString();
        }

        private static void WriteDetailedException(StringBuilder builder, Exception exception, bool isInnerException)
        {
            if (isInnerException) builder.Append("Inner Exception: ");
            else builder.Append("Exception: ");
            builder.Append(exception.Message);
            builder.Append("\r\nType: ");
            builder.Append(exception.GetType().FullName);
            builder.Append("\r\nStackTrack: ");
            builder.Append(exception.StackTrace);
            builder.Append("\r\n");
            builder.Append("\r\n");
            AggregateException aggregateException = exception as AggregateException;
            if (aggregateException != null)
            {
                foreach (Exception innerException in aggregateException.InnerExceptions)
                {
                    WriteDetailedException(builder, innerException, true);
                }
                return;
            }
            FaultException<ExceptionDetail> faultException = exception as FaultException<ExceptionDetail>;
            if (faultException != null)
            {
                builder.Append(faultException.Detail);
                builder.Append("\r\n");
                return;
            }
            if (exception.InnerException != null)
            {
                WriteDetailedException(builder, exception.InnerException, true);
            }
        }
    }
}
