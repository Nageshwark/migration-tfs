﻿using System;

namespace AlphaRooms.Utilities.Text
{
    public interface ICompressReader
    {
        bool ReadBoolean();
        bool? ReadBooleanOrNull();
        byte ReadByte(int length);
        DateTime ReadDate();
        T ReadEnum<T>(int length) where T : struct, IConvertible;
        Guid ReadGuid();
        int ReadInt(int length);
        string ReadString();
    }
}