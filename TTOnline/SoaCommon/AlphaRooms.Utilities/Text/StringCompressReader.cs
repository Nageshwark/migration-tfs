﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.Text
{
    public class StringCompressReader : ICompressReader
    {
        private string roomId;
        private int index = -1;

        public StringCompressReader(string roomId)
        {
            this.roomId = roomId;
            this.index = 0;
        }

        public int ReadInt(int length)
        {
            var value = int.Parse(this.roomId.Substring(this.index, length));
            this.index += length;
            return value;
        }

        public T ReadEnum<T>(int length) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
            var i = this.roomId.Substring(this.index, length);
            var value = (T)(object)int.Parse(i);
            this.index += length;
            return value;
        }

        public Guid ReadGuid()
        {
            var value = GuidModule.ParseFromShortString(this.roomId.Substring(this.index, GuidModule.MaxShortGuid));
            this.index += GuidModule.MaxShortGuid;
            return value;
        }

        public byte ReadByte(int length)
        {
            var value = byte.Parse(this.roomId.Substring(this.index, length));
            this.index += length;
            return value;
        }

        public DateTime ReadDate()
        {
            var value = DateTime.ParseExact(this.roomId.Substring(this.index, 8), "yyyyMMdd", CultureInfo.CurrentCulture);
            this.index += 8;
            return value;
        }

        public string ReadString()
        {
            var length = this.ReadInt(4);
            if (length == 9999) return null;
            var value = this.roomId.Substring(this.index, length);
            this.index += length;
            return value;
        }

        public bool ReadBoolean()
        {
            var value = this.roomId[this.index];
            this.index++;
            return value == '1';
        }

        public bool? ReadBooleanOrNull()
        {
            var value = this.roomId[this.index];
            this.index++;
            return (value == '1' ? true : (value == '3' ? null : (bool?)false));
        }
    }
}
