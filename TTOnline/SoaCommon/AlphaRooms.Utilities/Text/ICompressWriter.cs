﻿using System;

namespace AlphaRooms.Utilities.Text
{
    public interface ICompressWriter
    {
        void AppendBoolean(bool value);
        void AppendBooleanOrNull(bool? value);
        void AppendByte(byte value, int length);
        void AppendDate(DateTime value);
        void AppendEnum<T>(T value, int length) where T : struct, IConvertible;
        void AppendGuid(Guid value);
        void AppendInt(int value, int length);
        void AppendString(string value);
        string ToString();
    }
}