﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.Text
{
    public class StringCompressWriter : ICompressWriter
    {
        private static string[] numberFormat = new string[] {"", "0", "00", "000", "0000", "00000", "000000" };
        private readonly StringBuilder builder = new StringBuilder();

        public void AppendInt(int value, int length)
        {
            this.builder.Append(value.ToString(numberFormat[length]));
        }

        public void AppendEnum<T>(T value, int length) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
            this.builder.Append(value.ToInt32(CultureInfo.CurrentCulture).ToString(numberFormat[length]));
        }

        public void AppendDate(DateTime value)
        {
            this.builder.Append(value.ToString("yyyyMMdd"));
        }

        public void AppendByte(byte value, int length)
        {
            this.builder.Append(value.ToString(numberFormat[length]));
        }

        public void AppendString(string value)
        {
            this.AppendInt((value != null ? value.Length : 9999), 4);
            this.builder.Append(value);
        }

        public void AppendBoolean(bool value)
        {
            this.builder.Append(value ? '1' : '0');
        }

        public void AppendBooleanOrNull(bool? value)
        {
            this.builder.Append(value == null ? '3' : (value.Value ? '1' : '0'));
        }

        public void AppendGuid(Guid value)
        {
            this.builder.Append(value.ToShortString());
        }

        public override string ToString()
        {
            return this.builder.ToString();
        }
    }
}
