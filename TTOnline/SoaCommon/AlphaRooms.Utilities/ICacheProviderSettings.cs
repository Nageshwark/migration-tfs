﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Utilities
{
    public interface ICacheProviderSettings
    {
        string KeyPrefix { get; }
        bool IsCachingEnabled { get; }
        TimeSpan CacheExpireTime { get; }
        TimeSpan CachingTimeout { get; }
        TimeSpan CommandTimeout { get; }
    }
}
