﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class LinqExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> value, Action<T> action)
        {
            foreach (var item in value) action.Invoke(item);
        }

        public static void ForEach<T>(this IList<T> value, Action<T,int> action)
        {
            int index = 0;
            foreach (var item in value) action.Invoke(item, index++);
        }

        public static void ParallelForEach<T>(this IEnumerable<T> value, Action<T> action)
        {
            Parallel.ForEach(value, item => action.Invoke(item));
        }

        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector)
        {
            return value.MinBy(keySelector, Comparer<TKey>.Default);
        }

        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            using (IEnumerator<TSource> enumerator = value.GetEnumerator())
            {
                if (!enumerator.MoveNext()) throw new InvalidOperationException("Sequence was empty");
                TSource minElement = enumerator.Current;
                TKey minElementKey = keySelector.Invoke(minElement);
                while (enumerator.MoveNext())
                {
                    TSource element = enumerator.Current;
                    TKey elementKey = keySelector.Invoke(element);
                    if (comparer.Compare(elementKey, minElementKey) < 0)
                    {
                        minElement = element;
                        minElementKey = elementKey;
                    }
                }
                return minElement;
            }
        }

        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector)
        {
            return value.MaxBy(keySelector, Comparer<TKey>.Default);
        }

        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            using (IEnumerator<TSource> enumerator = value.GetEnumerator())
            {
                if (!enumerator.MoveNext()) throw new InvalidOperationException("Sequence was empty");
                TSource maxElement = enumerator.Current;
                TKey maxElementKey = keySelector.Invoke(maxElement);
                while (enumerator.MoveNext())
                {
                    TSource element = enumerator.Current;
                    TKey elementKey = keySelector.Invoke(element);
                    if (comparer.Compare(elementKey, maxElementKey) > 0)
                    {
                        maxElement = element;
                        maxElementKey = elementKey;
                    }
                }
                return maxElement;
            }
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector)
        {
            return value.DistinctBy(keySelector, EqualityComparer<TKey>.Default);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            var seenKeys = new HashSet<TKey>(comparer);
            foreach (TSource element in value) if (seenKeys.Add(keySelector(element))) yield return element;
        }

        public static string ToString<T>(this IEnumerable<T> value, char separator)
        {
            return ToString(value, separator.ToString());
        }

        public static string ToString<T>(this IEnumerable<T> value, string separator)
        {
            var builder = new StringBuilder();
            foreach (var item in value)
            {
                builder.Append(item);
                builder.Append(separator);
            }
            if (builder.Length > 0) builder.Length -= separator.Length;
            return builder.ToString();
        }

        public static IEnumerable<T[]> SplitToArrays<T>(this IEnumerable<T> value, int count)
        {
            var items = new T[count];
            var itemsIndex = 0;
            foreach (var item in value)
            {
                items[itemsIndex++] = item;
                if (itemsIndex == count)
                {
                    yield return items;
                    items = new T[count];
                    itemsIndex = 0;
                }
            }
            if (itemsIndex > 0) yield return items.SubArray(0, itemsIndex);
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> value, int count)
        {
            int valueCount = value.Count();
            for (int i = 0; i < valueCount; i += count) yield return value.Skip(i * count).Take(count);
        }

        public static IEnumerable<T> AsEnumerable<T>(this T item)
        {
            yield return item;
        }

        public static byte CountByte<T>(this IEnumerable<T> value, Func<T, bool> func)
        {
            byte c = 0;
            foreach (var item in value) if (func.Invoke(item)) c++;
            return c;
        }

        public static IEnumerable<IGrouping<int, TSource>> GroupDifferentBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector)
        {
            return value.GroupDifferentBy(keySelector, EqualityComparer<TKey>.Default);
        }

        public static IEnumerable<IGrouping<int, TSource>> GroupDifferentBy<TSource, TKey>(this IEnumerable<TSource> value, Func<TSource, TKey> keySelector
            , IEqualityComparer<TKey> comparer)
        {
            return value.GroupBy(keySelector, comparer).SelectMany(i => i.Select((j, index) => new { Index = index, Item = j })).GroupBy(i => i.Index, i => i.Item);
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }

        public static bool IsEmpty<T>(this IEnumerable<T> source)
        {
            return !source.Any();
        }

        // LGimenez: Try to avoid this please.
        public static IEnumerable<TResult> FullGroupJoin<TSource, TValue, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TValue> values, Func<TSource, TKey> sourceSelector, Func<TValue, TKey> valueSelector, Func<TKey, IEnumerable<TSource>, IEnumerable<TValue>, TResult> resultSelector)
        {
            var keys = source.Select(i => sourceSelector.Invoke(i)).Union(values.Select(j => valueSelector.Invoke(j))).ToArray();
            var sourceKeys = keys.GroupJoin(source, i => i, i => sourceSelector.Invoke(i), (i, j) => j);
            var valueKeys = keys.GroupJoin(values, i => i, i => valueSelector.Invoke(i), (i, j) => j);
            for (int i = 0; i < keys.Length; i++)
            {
                var item = keys[i];
                yield return resultSelector.Invoke(item, sourceKeys.ElementAt(i), valueKeys.ElementAt(i));
            }
        }

        public static Dictionary<TKey, TElement> ToDictionaryIgnoreDuplicated<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            var d = new Dictionary<TKey, TElement>();
            foreach (TSource element in source)
            {
                var key = keySelector(element);
                if (!d.Keys.Contains(key)) d.Add(key, elementSelector(element));
            }
            return d;
        }
    }
}
