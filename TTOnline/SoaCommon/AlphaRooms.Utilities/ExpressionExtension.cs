﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;

    using System.Collections;
    using System.Data.Entity;
    using System.Text.RegularExpressions;

    public static class ExpressionExtensions
    {
        /// <summary>
        /// Convert the type of the parameters to a lambda expression
        /// </summary>
        /// <typeparam name="F">from type</typeparam>
        /// <typeparam name="T">to type</typeparam>
        /// <param name="source">source expression</param>
        /// <returns>converted expression</returns>
        public static LambdaExpression ConvertParameters<F, T>(this LambdaExpression source)
        {
            var visitor = new ParameterTypeVisitor<F, T>(source);

            return visitor.Convert();
        }

        /// <summary>
        /// Rewrite expression with unary expressions of the specified type removed
        /// </summary>
        /// <param name="source">source expression</param>
        /// <param name="nodeType">expression type to remove or null to remove all unary expressions</param>
        /// <returns>converted expression</returns>
        public static LambdaExpression RemoveUnaryExpressions(this LambdaExpression source, ExpressionType? nodeType)
        {
            var visitor = new RemoveUnaryExpressionVisitor(source, nodeType);

            return visitor.Convert();
        }

        #region Nested Types

        /// <summary>
        /// Expression visitor to change parameter type taken from http://stackoverflow.com/questions/14437239/change-a-linq-expression-predicate-from-one-type-to-another
        /// </summary>
        /// <typeparam name="TFrom">from type</typeparam>
        /// <typeparam name="TTo">to type</typeparam>
        public class ParameterTypeVisitor<TFrom, TTo> : ExpressionVisitor
        {

            private readonly Dictionary<string, ParameterExpression> convertedParameters;
            private readonly LambdaExpression expression;

            public ParameterTypeVisitor(LambdaExpression expresionToConvert)
            {
                //for each parameter in the original expression creates a new parameter with the same name but with changed type 
                convertedParameters = expresionToConvert.Parameters
                    .ToDictionary(
                        x => x.Name,
                        x => Expression.Parameter(typeof(TTo), x.Name)
                    );

                expression = expresionToConvert;
            }

            public LambdaExpression Convert()
            {
                return (LambdaExpression)Visit(expression);
            }

            //handles Properties and Fields accessors 
            protected override Expression VisitMember(MemberExpression node)
            {
                //we want to replace only the nodes of type TFrom
                //so we can handle expressions of the form x=> x.Property.SubProperty
                //in the expression x=> x.Property1 == 6 && x.Property2 == 3
                //this replaces         ^^^^^^^^^^^         ^^^^^^^^^^^            
                if (node.Member.DeclaringType.IsAssignableFrom(typeof(TFrom)))
                {
                    return MakeMemberAlias(node.Member, this.Visit(node.Expression));
                }
                else
                {
                    return base.VisitMember(node);
                }
            }

            // this will be called where ever we have a reference to a parameter in the expression
            // for ex. in the expression x=> x.Property1 == 6 && x.Property2 == 3
            // this will be called twice     ^                   ^
            protected override Expression VisitParameter(ParameterExpression node)
            {
                var newParameter = convertedParameters[node.Name];
                return newParameter;
            }

            //this will be the first Visit method to be called
            //since we're converting LamdaExpressions
            protected override Expression VisitLambda<T>(Expression<T> node)
            {
                //visit the body of the lambda, this will Traverse the ExpressionTree 
                //and recursively replace parts of the expresion we for witch we have matching Visit methods 
                var newExp = Visit(node.Body);

                //this will create the new expression            
                return Expression.Lambda(newExp, convertedParameters.Select(x => x.Value));
            }

            private static Expression MakeMemberAlias(MemberInfo fromMember, Expression inner)
            {
                var bindAlias = fromMember.GetCustomAttribute<BindAliasAttribute>(true);

                if (bindAlias == null || !bindAlias.Aliases.Any())
                {
                    return MakeMemberAlias(inner, new[] { new MemberBinding(fromMember.Name) });
                }

                // One binding alias => simple property selctor.
                if (bindAlias.Aliases.Length == 1)
                {
                    return MakeMemberAlias(inner, MemberBinding.FromBindingAlias(bindAlias.Alias));
                }

                // Multiple binding aliases => addition of multiple simple property selctors.
                var expressions = bindAlias.Aliases.Select(alias => MakeMemberAlias(inner, MemberBinding.FromBindingAlias(alias))).ToArray();

                switch (bindAlias.AliasAggregationType)
                {
                    case BindAliasAttribute.AggregationType.Concatenate:
                        var aggregationMethod = typeof(string).GetMethod("Concat", new[] { typeof(string), typeof(string) });
                        return expressions.Aggregate((left, right) => Expression.Add(left, right, aggregationMethod));
                    case BindAliasAttribute.AggregationType.Sum:
                        return expressions.Aggregate(Expression.Add);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            private class MemberBinding
            {
                public static IEnumerable<MemberBinding> FromBindingAlias(string alias)
                {
                    var rawSplit = alias.Split('.');

                    var inFunciton = false;
                    var sb = new StringBuilder();
                    var regex = new Regex(@"^(.+?)\((.+)\)$");
                    foreach (var token in rawSplit)
                    {
                        if (inFunciton)
                        {
                            if (token.Contains(")"))
                            {
                                inFunciton = false;
                                var function = sb.Append(token).ToString();
                                var match = regex.Match(function);
                                if (!match.Success)
                                {
                                    throw new Exception("Badly formatted member binding alias: " + alias);
                                }
                                var name = match.Groups[1].Value;
                                var arguements = match.Groups[2].Value.Split(',').Select(x => x.Trim());
                                yield return new MemberBinding(name, true, arguements);
                            }
                            else
                            {
                                sb.Append(token).Append('.');
                            }
                        }
                        else if (token.Contains("()"))
                        {
                            yield return new MemberBinding(token.Replace("()", ""), true);
                        }
                        else if (token.Contains("("))
                        {
                            inFunciton = true;
                            sb.Clear().Append(token).Append('.');
                        }
                        else
                        {
                            yield return new MemberBinding(token);
                        }
                    }

                }

                public MemberBinding(string name, bool isMethod = false, IEnumerable<string> methodArguements = null)
                {
                    this.Name = name;
                    this.IsMethod = isMethod;
                    this.MethodArguements = methodArguements ?? Enumerable.Empty<string>();
                }

                public string Name { get; private set; }
                public bool IsMethod { get; private set; }
                public IEnumerable<string> MethodArguements { get; private set; }

            }

            private static Expression MakeMemberAlias(Expression inner, IEnumerable<MemberBinding> memberBindings)
            {
                var expression = inner;
                var type = typeof(TTo);

                foreach (var memberBinding in memberBindings)
                {
                    if (memberBinding.IsMethod && memberBinding.MethodArguements.Any())
                    {
                        // Currently supports Convert only.
                        if (memberBinding.Name == "Convert")
                        {
                            var convertTypeName = string.Join(", ", memberBinding.MethodArguements);
                            var convertType = Type.GetType(convertTypeName);
                            if (convertType == null)
                            {
                                throw new Exception("Can't get type: " + convertTypeName);
                            }
                            expression = Expression.Convert(expression, convertType);
                            type = convertType;
                            continue;
                        }
                    }

                    // Check for extension methods called on collections e.g. First(). Exclude strings becuase they're enumerable characters.
                    if (memberBinding.IsMethod && typeof(IEnumerable).IsAssignableFrom(type) && type != typeof(string))
                    {
                        var elementType = type.GetElementType() ?? type.GenericTypeArguments.First();
                        var method =
                            typeof(Enumerable).GetMember(memberBinding.Name, MemberTypes.Method, BindingFlags.Public | BindingFlags.Static)
                                .Cast<MethodInfo>()
                                .First(x => x.GetParameters().Count() == 1)
                                .MakeGenericMethod(elementType);
                        expression = Expression.Call(null, method, new[] { expression });
                        type = elementType;

                        continue;
                    }

                    if ((type == typeof(DateTime?) || type == typeof(DateTime)) && memberBinding.Name == "Date")
                    {
                        Expression<Func<DateTime?, DateTime?>> lambda = x => DbFunctions.TruncateTime(x);
                        var call = (MethodCallExpression)lambda.Body;

                        // Needs to be nullable to fit lambda proto
                        if (type == typeof(DateTime))
                        {
                            expression = Expression.Convert(expression, typeof(DateTime?));
                        }

                        return Expression.Call(null, call.Method, new[] { expression });
                    }

                    if ((type == typeof(DateTime?) || type == typeof(DateTime)) && memberBinding.Name == "Time")
                    {
                        Expression<Func<int?, int?, double?, TimeSpan?>> lambda = (h, m, s) => DbFunctions.CreateTime(h, m, s);
                        var call = (MethodCallExpression)lambda.Body;

                        // Needs to be primitive to call time component properties
                        if (type == typeof(DateTime?))
                        {
                            expression = Expression.Convert(expression, typeof(DateTime));
                        }

                        var hourExpression = Expression.Convert(ApplyMemberProperty(expression, (DateTime d) => d.Hour), typeof(int?));
                        var minuteExpression = Expression.Convert(ApplyMemberProperty(expression, (DateTime d) => d.Minute), typeof(int?));
                        var secondExpression = Expression.Convert(ApplyMemberProperty(expression, (DateTime d) => d.Second), typeof(double?));

                        return Expression.Call(null, call.Method, hourExpression, minuteExpression, secondExpression);
                    }

                    var toMember = type.GetMember(memberBinding.Name).First();

                    var propertyInfo = toMember as PropertyInfo;
                    if (propertyInfo != null)
                    {
                        type = propertyInfo.PropertyType;
                    }
                    else
                    {
                        var methodInfo = toMember as MethodInfo;
                        if (methodInfo != null)
                        {
                            type = methodInfo.ReturnType;
                        }
                    }

                    expression = Expression.MakeMemberAccess(expression, toMember);
                }

                return expression;
            }

            private static Expression ApplyMemberProperty<TObject, TProperty>(Expression baseExpression, Expression<Func<TObject, TProperty>> propertyExpression)
            {
                var member = propertyExpression.Body as MemberExpression;
                if (member == null)
                {
                    throw new ArgumentException("expression");
                }

                return Expression.MakeMemberAccess(baseExpression, member.Member);
            }
        }

        /// <summary>
        /// Expression visitor to remove unary expressions
        /// </summary>
        public class RemoveUnaryExpressionVisitor : ExpressionVisitor
        {
            private LambdaExpression expression;
            private ExpressionType? nodeType;

            public RemoveUnaryExpressionVisitor(LambdaExpression expression, ExpressionType? nodeType)
            {
                this.expression = expression;
                this.nodeType = nodeType;
            }

            public LambdaExpression Convert()
            {
                return Expression.Lambda(this.Visit(this.expression.Body), this.expression.Parameters);
            }

            protected override Expression VisitUnary(UnaryExpression node)
            {
                if (this.nodeType == null || node.NodeType == this.nodeType)
                {
                    return node.Operand;
                }

                return base.VisitUnary(node);
            }
        }
        #endregion

    }
}
