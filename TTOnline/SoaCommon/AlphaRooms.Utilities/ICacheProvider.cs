﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public interface ICacheProvider<T>
    {
        void Register(Func<T[]> funcSourceAll);
        void Register(Func<T[]> funcSourceAll, Func<String, T> funcSourceKey, Func<T, String> funcGetKey);
        void Register(string keyPrefix, Func<T[]> funcSourceAll);
        void Register(string keyPrefix, Func<T[]> funcSourceAll, Func<String, T> funcSourceKey, Func<T, String> funcGetKey);
        T[] GetAll();
        T GetByKey(string key);
        T[] GetByKeys(string[] keys);
        T GetOrSet(string key, Func<T> fetchFunction);
    }
}
