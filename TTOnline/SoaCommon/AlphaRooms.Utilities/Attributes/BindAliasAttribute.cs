﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.ComponentModel;

    //See http://ole.michelsen.dk/blog/bind-a-model-property-to-a-different-named-query-string-field/
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class BindAliasAttribute : Attribute
    {
        /// <summary>
        /// Creating a binding alias of parameters that will be added together (concat for strings, addition for numerics).
        /// </summary>
        /// <param name="aliases"></param>
        public BindAliasAttribute(params string[] aliases)
        {
            Aliases = aliases;
            AliasAggregationType = AggregationType.Concatenate;
        }

        public string Alias
        {
            get
            {
                return string.Join(" + ", Aliases);
            }
        }

        public string[] Aliases { get; private set; }

        public AggregationType AliasAggregationType { get; set; }

        public override object TypeId
        {
            get { return Alias; }
        }

        public enum AggregationType
        {
            Concatenate,
            Sum
        }

        public sealed class AliasedPropertyDescriptor : PropertyDescriptor
        {
            public PropertyDescriptor Inner { get; private set; }

            public AliasedPropertyDescriptor(string alias, PropertyDescriptor inner)
                : base(alias, null)
            {
                Inner = inner;
            }

            public override bool CanResetValue(object component)
            {
                return Inner.CanResetValue(component);
            }

            public override Type ComponentType
            {
                get { return Inner.ComponentType; }
            }

            public override object GetValue(object component)
            {
                return Inner.GetValue(component);
            }

            public override bool IsReadOnly
            {
                get { return Inner.IsReadOnly; }
            }

            public override Type PropertyType
            {
                get { return Inner.PropertyType; }
            }

            public override void ResetValue(object component)
            {
                Inner.ResetValue(component);
            }

            public override void SetValue(object component, object value)
            {
                Inner.SetValue(component, value);
            }

            public override bool ShouldSerializeValue(object component)
            {
                return Inner.ShouldSerializeValue(component);
            }
        }
    }
}

