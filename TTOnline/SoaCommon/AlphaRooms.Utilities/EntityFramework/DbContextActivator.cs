﻿using System.Data.Entity;

namespace AlphaRooms.Utilities.EntityFramework
{
    /// <summary>
    /// DbContextActivator creates a new DbContext every time the GetDbContext method is called.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DbContextActivator<T> : IDbContextActivator<T>
        where T : DbContext, new()
    {
        public T GetDbContext()
        {
            return new T();
        }
    }
}
