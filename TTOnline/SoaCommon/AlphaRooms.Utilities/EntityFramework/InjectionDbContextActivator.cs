﻿using System.Data.Entity;

namespace AlphaRooms.Utilities.EntityFramework
{
    /// <summary>
    /// InjectionDbContextActivator gives the same DbContext which was given in the constructor every time the GetDbContext method is called.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InjectionDbContextActivator<T> : IDbContextActivator<T>
        where T : DbContext, new()
    {
        private readonly T dbContext;

        public InjectionDbContextActivator(T dbContext)
        {
            this.dbContext = dbContext;
        }

        public T GetDbContext()
        {
            return this.dbContext;
        }
    }
}
