using System.Data.Entity;

namespace AlphaRooms.Utilities.EntityFramework
{
    public interface IDbContextActivator<out T>
        where T : DbContext, new()
    {
        T GetDbContext();
    }
}