﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;

namespace AlphaRooms.Utilities.EntityFramework
{
    public class MultiResultsReader : IDisposable
    {
        private readonly IObjectContextAdapter objectContextAdapter;
        private readonly DbDataReader dbDataReader;
        private bool firstRecord;

        public MultiResultsReader(IObjectContextAdapter objectContextAdapter, DbDataReader dbDataReader)
        {
            this.objectContextAdapter = objectContextAdapter;
            this.dbDataReader = dbDataReader;
            this.firstRecord = true;
        }

        public IEnumerable<T> NextResult<T>()
        {
            if (!this.firstRecord) this.dbDataReader.NextResult();
            else this.firstRecord = false;
            return this.objectContextAdapter.ObjectContext.Translate<T>(this.dbDataReader); // , typeof(T).Name, MergeOption.AppendOnly
        }

        public void Dispose()
        {
            this.dbDataReader.Dispose();

        }

        public void Close()
        {
            this.dbDataReader.Close();
        }
    }
}
