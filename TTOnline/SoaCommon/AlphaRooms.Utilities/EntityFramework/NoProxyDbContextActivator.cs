﻿using System.Data.Entity;

namespace AlphaRooms.Utilities.EntityFramework
{
    /// <summary>
    /// NoProxyDbContextActivator creates a new DbContext with Proxy Creation Disabled every time the GetDbContext method is called.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NoProxyDbContextActivator<T> : IDbContextActivator<T>
        where T : DbContext, new()
    {
        public T GetDbContext()
        {
            var dbContext = new T();

            dbContext.Configuration.ProxyCreationEnabled = false;

            return dbContext;
        }
    }
}
