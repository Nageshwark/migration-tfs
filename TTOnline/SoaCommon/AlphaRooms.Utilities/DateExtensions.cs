﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class DateExtensions
    {
        public static double ToUnixTimeStamp(this DateTime value)
        {
            TimeSpan ts = (value - new DateTime(1970, 1, 1, 0, 0, 0));
            return ts.TotalSeconds;
        }

        public static int GetNumberOfDays(DateTime startDate, DateTime endDate)
        {
            long tickDiff = endDate.Ticks - startDate.Ticks;
            tickDiff = tickDiff / 10000000;
            int numDays = (int)(tickDiff / 86400);
            return numDays;
        }

        /// <summary>
        /// Checks whether Range #1 contains Range #2
        /// </summary>
        /// <param name="range1Start">Range #1 start date</param>
        /// <param name="range1End">Range #1 end date</param>
        /// <param name="range2Start">Range #2 start date</param>
        /// <param name="range2End">Range #2 end date</param>
        /// <returns></returns>
        public static bool DateRangeContainsDateRange(DateTime range1Start, DateTime range1End, DateTime range2Start, DateTime range2End)
        {
            var range1 = new Range(range1Start, range1End);
            var range2 = new Range(range2Start, range2End);
            return range1.Contains(range2);
        }

        class Range
        {
            public DateTime Begin { get; private set; }
            public DateTime End { get; private set; }
            public Range(DateTime begin, DateTime end)
            {
                Begin = begin;
                End = end;
            }

            public bool Contains(Range range)
            {
                return range.Begin >= Begin && range.End <= End;
            }
        }
    }
}
