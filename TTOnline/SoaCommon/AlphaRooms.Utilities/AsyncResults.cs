﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Utilities
{
    public class AsyncResults
    {
        private readonly bool hasTimeout;
        private readonly AggregateException[] exceptions;
        private AggregateException parentExceptions;

        public AsyncResults(bool HasTimeout, AggregateException[] exceptions, AggregateException parentExceptions)
        {
            this.hasTimeout = HasTimeout;
            this.exceptions = exceptions;
            this.parentExceptions = parentExceptions;
        }

        public bool IsSuccessful
        {
            get { return (!this.hasTimeout && this.exceptions.Length == 0); }
        }

        public bool HasTimedOut
        {
            get { return this.hasTimeout; }
        }

        public AggregateException[] Exceptions
        {
            get { return this.exceptions; }
        }

        public AggregateException ParentException
        {
            get { return this.parentExceptions; }
        }
    }
}
