﻿namespace AlphaRooms.Utilities.ExtensionMethods
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;

    public static class ExtensionMethods
    {
        public static string ToIndentedJson(this object obj)
        {
            string json = JsonConvert.SerializeObject(
                obj, 
                Newtonsoft.Json.Formatting.Indented, 
                new JsonSerializerSettings { DateFormatString = "dd MMM yyyy HH:mm" });

            return json; 
        }

        public static string ToIndentedJson(this string obj)
        {
            var parsedJson = JsonConvert.DeserializeObject(obj);
            return JsonConvert.SerializeObject(parsedJson, Newtonsoft.Json.Formatting.Indented);
        }

        public static string ToJson(this Object obj)
        {
            string json = JsonConvert.SerializeObject(obj);
            return json;
        }

        private static ConcurrentDictionary<Type, XmlSerializer> xmlSerializers = new ConcurrentDictionary<Type, XmlSerializer>();

        private static XmlSerializer GetSerializer(Type type)
        {
            XmlSerializer xmlSerializer;

            if (!xmlSerializers.TryGetValue(type, out xmlSerializer))
            {
                xmlSerializer = new XmlSerializer(type);
                xmlSerializers.TryAdd(type, xmlSerializer);
            }

            return xmlSerializer;
        }

        public static string ToIndentedXml(this string obj)
        {
            return XElement.Parse(obj).ToString();
        }

        public static string XmlSerialize(this object objectToSerialize, Type type)
        {
            XmlSerializer xmlSerializer = GetSerializer(type);

            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, objectToSerialize);
                return textWriter.ToString();
            }
        }

        public static string XmlSerializeFormatted(this object objectToSerialize, Type type)
        {
            XmlSerializer xmlSerializer = GetSerializer(type);

            using (var textWriter = new StringWriter())
            {
                using (var xmlTextWriter = XmlTextWriter.Create(textWriter, new XmlWriterSettings() { Indent = true }))
                {
                    xmlSerializer.Serialize(xmlTextWriter, objectToSerialize);

                    xmlTextWriter.Close();
                    textWriter.Close();

                    return textWriter.ToString();
                }
            }
        }

        public static string XmlSerialize<T>(this T objectToSerialize)
        {
            XmlSerializer xmlSerializer = GetSerializer(typeof(T));

            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, objectToSerialize);
                return textWriter.ToString();
            }
        }

        public static T XmlDeSerialize<T>(this string objectToDeSerialize)
        {
            XmlSerializer xmlSerializer = GetSerializer(typeof(T));

            using (var textWriter = new StringReader(objectToDeSerialize))
            {
                return (T)xmlSerializer.Deserialize(textWriter);
            }
        }

        // Return the left most n characters of a string.
        // If the string is less than n charcaters long, the entire string will be returned.
        public static string Left(this string value, int length)
        {
            string result = string.Empty;

            if (string.IsNullOrEmpty(value))
            {
                result = value;
            }
            else
            {
                length = Math.Abs(length);

                if (value.Length <= length)
                {
                    result = value;
                }
                else
                {
                    result = value.Substring(0, length);
                }
            }

            return result;
        }

        public static bool IsEmpty(this Guid? g)
        {
            return g.HasValue && g.Value == Guid.Empty;
        }

        public static bool IsNullOrEmpty(this Guid? g)
        {
            return !g.HasValue || g.Value == Guid.Empty;
        }
    }
}
