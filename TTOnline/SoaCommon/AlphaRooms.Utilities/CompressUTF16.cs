﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class CompressUTF16
    {        
        public static string CompressNumericString(string numerics)
        {
            var chars = new char[(int)Math.Ceiling(Decimal.Divide(numerics.Length, 4))];
            int index = 0;
            for(int i = 0; i < numerics.Length; i += 4)
            {
                chars[index++] = CompressNumeric((byte)(numerics[i] - '0'), (numerics.Length > i + 1 ? (byte)(numerics[i + 1] - '0') : (byte)0), (numerics.Length > i + 2 ? (byte)(numerics[i + 2] - '0') : (byte)0)
                    , (numerics.Length > i + 3 ? (byte)(numerics[i + 3] - '0') : (byte)0));
            }
            return new string(chars);
        }

        public static char CompressNumeric(byte n1, byte n2, byte n3, byte n4)
        {
            return (char)(n4 + (n3 << 4) + (n2 << 8) + (n1 << 12));
        }

        public static char CompressDate(DateTime dateTime)
        {
            return (char)(dateTime.Day + (dateTime.Month << 5) + ((dateTime.Year % 100) << 9));
        }

        public static string CompressGuid(Guid guid)
        {
            return Convert.ToBase64String(guid.ToByteArray()).Substring(0, 22);
        }

        public static string CompressString(string text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            var chars = new char[(int)Math.Ceiling(Decimal.Divide(text.Length, 2))];
            int index = 0;
            for(int i = 0;i < text.Length;i += 2)
            {
                chars[index++] = CompressChar(text[i], (text.Length > i + 1 ? text[i + 1] : (char)0));
            }
            return new string(chars);
        }
        
        public static char CompressChar(char c1, char c2)
        {
            return (char)((c2 & 255) + ((c1 & 255) << 8));
        }

        public static string DecompressString(string text)
        {
            return new String(text.SelectMany(i => DecompressStringChar(i)).ToArray());
        }

        public static char[] DecompressStringChar(char c)
        {
            var c1 = (char)((c & 65280) >> 8);
            var c2 = (char)(c & 255);
            return (c2 != 0 ? new char[] { c1,  c2} : new char[] { c1 });
        }

        public static char[] DecompressChar(char c)
        {
            return new char[] { (char)((c & 65280) >> 8), (char)(c & 255)};
        }

        public static byte[] DecompressNumeric(char x)
        {
            return new byte[] { (byte)((x & 61440) >> 12), (byte)((x & 3840) >> 8), (byte)((x & 240) >> 4), (byte)(x & 15) };
        }

        public static DateTime DecompressDate(char x)
        {
            return new DateTime(2000 + (byte)((x & 65024) >> 9), (byte)((x & 480) >> 5), (byte)(x & 31));
        }

        public static Guid DecompressGuid(string text)
        {
            return new Guid(Convert.FromBase64String(text.Substring(0, 22) + "=="));
        }
    }
}
