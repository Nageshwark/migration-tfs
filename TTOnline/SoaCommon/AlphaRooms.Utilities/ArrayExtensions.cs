﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class ArrayExtensions
    {
        public static T[] SubArray<T>(this T[] data, int index, int count)
        {
            var result = new T[count];
            Array.Copy(data, index, result, 0, count);
            return result;
        }
    }

    public static class ArrayModule
    {
        public static bool IsNullOrEmpty<T>(T[] value)
        {
            return (value == null || !value.Any());
        }

        public static T[] CreateWithValues<T>(int count, Func<int,T> func)
        {
            var array = new T[count];
            for(int i = 0;i < count;i++) array[i] = func.Invoke(i);
            return array;
        }
    }
}
