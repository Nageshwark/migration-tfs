﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class DictionaryExtensions
    {
        public static R GetValueOrDefault<R, V>(this Dictionary<V, R> dictionary, V value, R defaultValue)
        {
            R mapping;
            if (dictionary.TryGetValue(value, out mapping)) return mapping;
            return defaultValue;
        }

        public static R GetValueOrDefault<R, V>(this Dictionary<V, R> dictionary, V value)
        {
            return dictionary.GetValueOrDefault(value, default(R));
        }
    }
}
