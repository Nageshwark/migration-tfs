﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaRooms.Utilities
{
    public class DescriptionNotFoundException : Exception
    {
        private Enum value;

        public DescriptionNotFoundException(Enum value)
        {
            this.value = value;
        }

        public override string Message
        {
            get
            {
                return string.Format("Enum item {0} description was not found.", this.value);
            }
        }
    }
}
