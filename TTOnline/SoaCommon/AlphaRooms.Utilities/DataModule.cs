﻿using System;

namespace AlphaRooms.Utilities
{
    public static class DataModule
    {
        public static object DbNullIfNull(object obj)
        {
            return (obj ?? DBNull.Value);
        }       
    }
}
