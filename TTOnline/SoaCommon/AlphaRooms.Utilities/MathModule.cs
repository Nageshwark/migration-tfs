﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class MathModule
    {
        public static decimal DivZero(decimal value, decimal div) 
        {
            return (div != 0 ? value / div : 0);
        }
    }
}
