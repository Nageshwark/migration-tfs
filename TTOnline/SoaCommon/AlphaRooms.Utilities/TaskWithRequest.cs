﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public class TaskWithRequest<TRequest> : Task
    {
        private TRequest request;

        public TaskWithRequest(Action<TRequest> action, TRequest request) : base(() => action(request))
        {
            this.request = request;
        }

        public TRequest Request
        {
            get { return this.request; }
        }
    }
}
