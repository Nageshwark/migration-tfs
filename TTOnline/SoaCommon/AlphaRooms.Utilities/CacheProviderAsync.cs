﻿namespace AlphaRooms.Utilities
{
    using System;
    using System.Runtime.Caching;
    using System.Threading.Tasks;

    public class CacheProviderAsync<T> : ICacheProviderAsync<T>
    {
        private class CacheStatus
        {
            public CacheStatuses Status;
            public CacheItemPolicy Policy;
        }

        private enum CacheStatuses
        {
            Unavailable
            , BeingPopulated
        }

        private readonly ObjectCache cache;
        private Func<Task<T[]>> funcSourceAllItems;
        private Func<String, Task<T>> funcSourceKey;
        private Func<T, String> funcGetKey;
        private string keyGetAll;
        private string keyGetByKey;
        private string keyPrefix;
        private string keyIsRunning;
        private readonly bool isCachingEnabled;
        private readonly TimeSpan cacheExpireTime;
        private readonly TimeSpan cachingTimeout;
        private readonly TimeSpan commandTimeout;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMemoryCacheProvider" /> class.
        /// </summary>
        /// <param name="cacheItemPolicyFactory">The cache item policy factory.</param>
        /// <param name="cache">The cache.</param>
        public CacheProviderAsync(TimeSpan cacheExpireTime)
            : this(true, cacheExpireTime, TimeSpan.Parse("00:00:05"), TimeSpan.Parse("00:00:05"))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMemoryCacheProvider" /> class.
        /// </summary>
        /// <param name="cacheItemPolicyFactory">The cache item policy factory.</param>
        /// <param name="cache">The cache.</param>
        public CacheProviderAsync(ICacheProviderSettings settings)
            : this(settings.IsCachingEnabled, settings.CacheExpireTime, settings.CachingTimeout, settings.CommandTimeout)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMemoryCacheProvider" /> class.
        /// </summary>
        /// <param name="cacheItemPolicyFactory">The cache item policy factory.</param>
        /// <param name="cache">The cache.</param>
        public CacheProviderAsync(bool isCachingEnabled, TimeSpan cacheExpireTime, TimeSpan cachingTimeoutTimeSpan, TimeSpan commandTimeoutSpan)
        {
            this.cache = MemoryCache.Default;
            this.isCachingEnabled = isCachingEnabled;
            this.cacheExpireTime = cacheExpireTime;
            this.cachingTimeout = cachingTimeoutTimeSpan;
            this.commandTimeout = commandTimeoutSpan;
            this.SetKeyPrefix(typeof(T).Name);
        }

        /// <summary>
        /// Populate cache with a collection of objects and their specific key
        /// </summary>
        /// <param name="items"></param>
        /// <param name="funcGetKey"></param>
        public void Register(Func<Task<T[]>> funcGetAllItems)
        {
            Register(null, funcGetAllItems, null, null);
        }

        /// <summary>
        /// Populate cache with a collection of objects and their specific key
        /// </summary>
        /// <param name="items"></param>
        /// <param name="funcGetKey"></param>
        public void Register(Func<Task<T[]>> funcGetAllItems, Func<String, Task<T>> funcSourceKey, Func<T, String> funcGetKey)
        {
            Register(null, funcGetAllItems, funcSourceKey, funcGetKey);
        }


        /// <summary>
        /// Populate cache with a collection of objects and their specific key
        /// </summary>
        /// <param name="items"></param>
        /// <param name="funcGetKey"></param>
        public void Register(string keyPrefix, Func<Task<T[]>> funcGetAllItems)
        {
            Register(keyPrefix, funcGetAllItems, null, null);
        }

        /// <summary>
        /// Populate cache with a collection of objects and their specific key
        /// </summary>
        /// <param name="items"></param>
        /// <param name="funcGetKey"></param>
        public void Register(string keyPrefix, Func<Task<T[]>> funcSourceAllItems, Func<String, Task<T>> funcSourceKey, Func<T, String> funcGetKey)
        {
            if (funcSourceAllItems == null) throw new ArgumentNullException("funcSourceAllItems");
            if (!string.IsNullOrEmpty(keyPrefix)) this.SetKeyPrefix(keyPrefix);
            this.funcSourceAllItems = funcSourceAllItems;
            this.funcSourceKey = funcSourceKey;
            this.funcGetKey = funcGetKey;
        }

        private void SetKeyPrefix(string keyPrefix)
        {
            this.keyPrefix = keyPrefix;
            this.keyGetAll = this.keyPrefix + "All";
            this.keyGetByKey = this.keyPrefix + "Key";
            this.keyIsRunning = this.keyPrefix + "IsR";
        }

        /// <summary>
        /// Gets a list of all cached objects. 
        /// </summary>
        /// <returns></returns>
        public async Task<T[]> GetAllAsync()
        {
            return await this.GetByKeyInternal<T[]>(this.keyGetAll, null);
        }

        /// <summary>
        /// Gets a cached object by key.
        /// </summary>
        /// <returns></returns>
        public async Task<T> GetByKeyAsync(string key)
        {
            return await this.GetByKeyInternal<T>(this.keyGetByKey + key, key);
        }

        /// <summary>
        /// Gets a list of cached object by keys.
        /// </summary>
        /// <returns></returns>
        public async Task<T[]> GetByKeysAsync(string[] keys)
        {
            T[] values = new T[keys.Length];

            for (int i = 0; i < keys.Length; i++)
            {
                values[i] = await this.GetByKeyInternal<T>(this.keyGetByKey + keys[i], keys[i]);
            }

            return values;
        }

        private async Task<TParam> GetByKeyInternal<TParam>(string key, string value)
        {
            TParam result = default(TParam);
            if (!this.isCachingEnabled)
            {
                //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Not Cached Get", DateTime.Now, this.keyPrefix, key));
                if (this.funcSourceAllItems == null) throw new Exception("Source not registered.");
                if (key != this.keyGetAll && this.funcGetKey == null) throw new Exception("Key not registered.");
                if (key == this.keyGetAll)
                {
                    result = (TParam)(object)await this.funcSourceAllItems.Invoke();
                    if (result == null) throw new Exception("Source cannot return null.");
                    return result;
                }
                result = (TParam)(object)await this.funcSourceKey.Invoke(value);
                if (result == null) throw new Exception(string.Format("Key {0} not found.", value));
                return result;
            }
            result = (TParam)this.cache.Get(key);
            if (result != null) return result;
            CacheStatus status;
            lock (this.cache)
            {
                result = (TParam)this.cache.Get(key);
                //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Get '{3}'", DateTime.Now, this.keyPrefix, key, result));
                if (result != null) return result;
                status = this.GetCacheStatus();
                //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Status {3}", DateTime.Now, this.keyPrefix, key, status.Status));
            }
            if (this.funcSourceAllItems == null) throw new Exception("Source not registered.");
            if (key != this.keyGetAll && this.funcGetKey == null) throw new Exception("Key not registered.");
            if (status.Status == CacheStatuses.Unavailable)
            {
                try
                {
                    CacheItemPolicy policy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.Add(this.cacheExpireTime) };
                    //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Populate Started", DateTime.Now, this.keyPrefix, key));
                    T[] items = await this.funcSourceAllItems.Invoke();
                    if (items == null) throw new Exception("Source cannot return null.");
                    if (this.funcGetKey != null)
                    {
                        foreach (T item in items)
                        {
                            string itemKey = this.keyGetByKey + this.funcGetKey.Invoke(item);
                            if (key == itemKey) result = (TParam)(object)item;
                            if (item != null) this.cache.Add(itemKey, item, policy);
                            //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Stored {3} {4} {5}", DateTime.Now, this.keyPrefix, key, itemKey, item, this.cache.Contains(itemKey)));
                        }
                    }
                    this.cache.Add(this.keyGetAll, items, policy);
                    if (key == this.keyGetAll) result = (TParam)(object)items;
                }
                finally
                {
                    this.cache.Remove(this.keyIsRunning);
                    //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Populate Completed", DateTime.Now, this.keyPrefix, key));
                }
                //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Populate Get '{3}'", DateTime.Now, this.keyPrefix, key, result));
                if (result == null)
                {
                    if (key == this.keyGetAll) throw new Exception("Source cannot return null.");
                    throw new Exception(string.Format("Key {0} not found.", value));
                }
                return result;
            }
            //else if (status.Status == CacheStatuses.BeingPopulated)
            //{
            DateTime start = DateTime.Now;
            DateTime expire = start + this.commandTimeout;
            expire = (status.Policy.AbsoluteExpiration.DateTime < expire ? status.Policy.AbsoluteExpiration.DateTime : expire);
            //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Waiting {3}", DateTime.Now, this.keyPrefix, key, expire));
            while (DateTime.Now < expire)
            {
                await Task.Delay(100);
                TParam item = (TParam)this.cache.Get(key);
                if (item != null)
                {
                    result = item;
                    //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Waiting Done", DateTime.Now, this.keyPrefix, key));
                    //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Waiting Get '{3}'", DateTime.Now, this.keyPrefix, key, result));
                    return result;
                }
            }
            //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Waiting Expired", DateTime.Now, this.keyPrefix, key));
            if (key == this.keyGetAll) result = (TParam)(object)await this.funcSourceAllItems.Invoke();
            else result = (TParam)(object)await this.funcSourceKey.Invoke(value);
            if (result == null)
            {
                if (key == this.keyGetAll) throw new Exception("Source cannot return null.");
                throw new Exception(string.Format("Key {0} not found.", value));
            }
            this.cache.Add(key, result, new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.Add(cacheExpireTime) });
            //}
            //Debug.WriteLine(string.Format("{0:dd MMM yyyy hh:mm:ss.ffff} {1} {2} Force Get '{3}'", DateTime.Now, this.keyPrefix, key, result));
            return result;
        }

        private CacheStatus GetCacheStatus()
        {
            CacheItemPolicy policy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.Add(this.cachingTimeout) };
            CacheItemPolicy current = (CacheItemPolicy)this.cache.AddOrGetExisting(this.keyIsRunning, policy, policy);
            return new CacheStatus() { Status = (current == null ? CacheStatuses.Unavailable : CacheStatuses.BeingPopulated), Policy = current };
        }

        /// <summary>
        /// Gets the or set a cached object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="fetchFunction">The fetch function.</param>
        /// <param name="itemType">Type of the item.</param>
        /// <returns></returns>
        public Task<T> GetOrSetAsync(string key, Func<Task<T>> fetchFunction)
        {
            return this.GetOrSetAsyncInternal(this.keyPrefix + key, fetchFunction);
        }

        /// <summary>
        /// Gets the or set a cached object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="fetchFunction">The fetch function.</param>
        /// <param name="itemType">Type of the item.</param>
        /// <returns></returns>
        public Task<TSource> GetOrSetAsync<TSource>(string key, Func<Task<TSource>> fetchFunction)
        {
            return this.GetOrSetAsyncInternal(this.keyPrefix + key, fetchFunction);
        }

        private Task<TParam> GetOrSetAsyncInternal<TParam>(string key, Func<Task<TParam>> fetchFunction)
        {
            if (fetchFunction == null)
            {
                throw new ArgumentNullException(nameof(fetchFunction));
            }

            if (!this.isCachingEnabled)
            {
                return fetchFunction.Invoke();
            }

            // create the object the will allow use to share the request across threads
            CacheRequest cacheRequest = new CacheRequest(key);

            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.Add(cacheExpireTime) };
            object itemFromCache = this.cache.AddOrGetExisting(key, cacheRequest, cacheItemPolicy);

            Task<TParam> fectchTask;

            if (itemFromCache == null)
            {
                /*
                 * If we null we have inserted our CacheRequest into the cache.
                 * This means other threads after use will be waiting for use to complete this request.
                 * It is our responsibility to perform the fetching.
                 */
                try
                {
                    // call the fetch method, this task should now be running
                    fectchTask = fetchFunction.Invoke();

                    if (fectchTask == null)
                    {
                        throw new InvalidOperationException($"{nameof(fetchFunction)} can not return a null Task.");
                    }

                    /*
                     * add our endding task onto the end.
                     * This task updates the cache with the result of the fetch task.
                     * We need to do this to have constant null result handling across shared requests.
                     */

                    fectchTask = fectchTask.ContinueWith(
                        this.FectchTaskComplete,
                        key,
                        // run synchronously to avoid overhead of it going back to task queue as the method will be very quick
                        TaskContinuationOptions.ExecuteSynchronously);

                    // set the result in the CacheRequest for threads that are already waiting
                    cacheRequest.SetResult(fectchTask);
                }
                catch (Exception ex)
                {
                    // we must always remove our CacheRequest from the cache, or the execption will be cached for ever
                    this.cache.Remove(key);

                    // set the exception so other pooled request receive this.
                    cacheRequest.SetException(ex);

                    // throw for this thread
                    throw;
                }
            }
            else
            {
                // something was already in the cache, have we got somebody CacheRequest
                cacheRequest = itemFromCache as CacheRequest;

                if (cacheRequest != null)
                {
                    // get the result of the already in progress request
                    fectchTask = (Task<TParam>)cacheRequest.GetResult();
                }
                else
                {
                    // must be the value saved in the cache, return this
                    fectchTask = Task.FromResult((TParam)itemFromCache);
                }
            }

            return fectchTask;
        }

        /// <summary>
        /// This method is called after the fetching task is complete.
        /// It is our responsible for updating the cache with the result of the fetch task.
        /// </summary>
        /// <typeparam name="TParm">the type returned by the task</typeparam>
        /// <param name="fectchTask">the completed task</param>
        /// <param name="state">the cache key</param>
        /// <returns></returns>
        private TParm FectchTaskComplete<TParm>(Task<TParm> fectchTask, object state)
        {
            string cacheKey = (string)state;

            TParm valueToCache;

            try
            {
                // this will throw if there was a problem with the task
                valueToCache = fectchTask.Result;

                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.Add(cacheExpireTime) };

                this.cache.Set(cacheKey, valueToCache, cacheItemPolicy);
            }
            catch (Exception)
            {
                // we must always remove our CacheRequest from the cache, or the execption will be cached for ever
                this.cache.Remove(cacheKey);

                throw;
            }

            return valueToCache;
        }
    }
}