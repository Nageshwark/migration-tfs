﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class GuidExtensions
    {
        public static string ToShortString(this Guid value)
        {
            var builder = new StringBuilder(Convert.ToBase64String(value.ToByteArray()));
            builder.Replace('/', '_');
            builder.Replace('+', '-');
            builder.Length = GuidModule.MaxShortGuid;
            return builder.ToString();
        }
    }
}
