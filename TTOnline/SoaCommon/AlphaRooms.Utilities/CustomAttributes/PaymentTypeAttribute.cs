﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Utilities.Enumerators;

namespace AlphaRooms.Utilities
{
    public class PaymentTypeAttribute:Attribute
    {
        private CardPaymentType paymentType;

        public PaymentTypeAttribute(CardPaymentType paymentType)
        {
            this.paymentType = paymentType;
        }

        public CardPaymentType PaymentType
        {
            get
            {
                return paymentType;
            }
        }
    }
}
