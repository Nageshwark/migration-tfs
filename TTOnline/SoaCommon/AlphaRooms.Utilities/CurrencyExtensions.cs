﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class CurrencyExtensions
    {
        /// <summary>
        /// Method used to return a currency symbol.  
        /// It receive as a parameter a currency code (3 digits).  
        /// </summary>  
        /// <param name="code">3 digits code. Samples GBP, BRL, USD, etc.</param>  
        public static string GetCurrencySymbol(this string code)
        {
            RegionInfo regionInfo = (from culture in CultureInfo.GetCultures(CultureTypes.InstalledWin32Cultures)
                                                          where culture.Name.Length > 0 && !culture.IsNeutralCulture
                                                          let region = new RegionInfo(culture.LCID)
                                                          where String.Equals(region.ISOCurrencySymbol, code, StringComparison.InvariantCultureIgnoreCase)
                                                          select region).First();

            return regionInfo.CurrencySymbol;
        }

        public static NumberFormatInfo ToNumberFormat(this string currency)
        {
            if (currency == null)
                return null;

            switch (currency)
            {
                case "AUD":
                    return new CultureInfo(3081).NumberFormat;
                case "CHF":
                    return new CultureInfo(2055).NumberFormat;
                case "EUR":
                    return new CultureInfo(3082).NumberFormat;
                case "GBP":
                    return new CultureInfo(2057).NumberFormat;
                case "USD":
                    return new CultureInfo(1033).NumberFormat;
                case "PLN":
                    return new CultureInfo(1045).NumberFormat;
                case "TRY":
                    return new CultureInfo(1055).NumberFormat;

                default:
                    return null;
            }
        }
    }
}
