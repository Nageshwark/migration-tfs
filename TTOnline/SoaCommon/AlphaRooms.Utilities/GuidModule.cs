﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities
{
    public static class GuidModule
    {
        public const int MaxShortGuid = 22;

        public static bool IsValidGuid(string value)
        {
            Guid guid;
            return Guid.TryParse(value, out guid);
        }

        public static Guid ParseFromShortString(string value)
        {
            var builder = new StringBuilder(value);
            builder.Replace('_', '/');
            builder.Replace('-', '+');
            return new Guid(Convert.FromBase64String(builder.ToString() + "=="));
        }
    }
}
