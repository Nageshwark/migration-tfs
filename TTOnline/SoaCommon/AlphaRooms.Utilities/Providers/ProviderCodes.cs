﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.Utilities.Providers
{
    /// <summary>
    /// Contains provider edi codes.
    /// </summary>
    public static class ProviderCodes
    {
        // Add here integrated providers only
        public const string AriaContracts = "A";
        public const string Avra = "AV";
        public const string B5 = "B5";
        public const string Expedia = "EX";
        public const string GTA = "GT";
        public const string HotelBeds = "B";
        public const string HotelCompany = "HO";
        public const string Hotusa = "H";
        public const string Hotetec = "HC";
        public const string Hotels4U = "19";
        public const string Jumbo = "Y";
        public const string ViajesOlympia = "2";
        public const string ViajesUrbis = "V";
        public const string YouTravel = "1";
    }
}
