﻿using System;

namespace AlphaRooms.Utilities.CustomExceptions
{
    public class AvailabilityTimeoutException : Exception
    {
        public AvailabilityTimeoutException()
        {
        }

        public AvailabilityTimeoutException(string message) : base(message)
        {
        }

        public AvailabilityTimeoutException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
