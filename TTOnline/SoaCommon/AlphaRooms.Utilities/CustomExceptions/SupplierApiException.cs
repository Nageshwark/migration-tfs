﻿using System;

namespace AlphaRooms.Utilities.CustomExceptions
{
    public class SupplierApiException : Exception
    {
        public SupplierApiException()
        {
        }

        public SupplierApiException(string message)
            : base(message)
        {
        }

        public SupplierApiException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
