﻿using System;

namespace AlphaRooms.Utilities.CustomExceptions
{
    public class SupplierApiTimeoutException : Exception
    {
        public SupplierApiTimeoutException()
        {
        }

        public SupplierApiTimeoutException(string message) : base(message)
        {
        }

        public SupplierApiTimeoutException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
