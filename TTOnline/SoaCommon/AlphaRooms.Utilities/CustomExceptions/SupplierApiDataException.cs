﻿using System;

namespace AlphaRooms.Utilities.CustomExceptions
{
    public class SupplierApiDataException : Exception
    {
        public SupplierApiDataException()
        {
        }

        public SupplierApiDataException(string message) : base(message)
        {
        }

        public SupplierApiDataException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
