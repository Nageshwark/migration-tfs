﻿using System;

namespace AlphaRooms.Utilities.CustomExceptions
{
    public class SupplierAvailabilityTimeoutException : Exception
    {
        private readonly int searchTimeout;

         public SupplierAvailabilityTimeoutException(int searchTimeout)
        {
            this.searchTimeout = searchTimeout;
        }

         public SupplierAvailabilityTimeoutException(int searchTimeout, string message)
             : base(message)
        {
            this.searchTimeout = searchTimeout;
        }

        public SupplierAvailabilityTimeoutException(int searchTimeout, string message, Exception inner)
            : base(message, inner)
        {
            this.searchTimeout = searchTimeout;
        }

        public int SearchTimeout
        {
            get { return this.searchTimeout; }
        }
    }
}
