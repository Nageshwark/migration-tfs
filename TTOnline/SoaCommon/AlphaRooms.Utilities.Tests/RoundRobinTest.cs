﻿namespace AlphaRooms.Utilities.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    [TestClass]
    public class RoundRobinTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullValuesThrows()
        {
            new RoundRobin<string>(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EmtpyValuesThrows()
        {
            new RoundRobin<string>(new string[0]);
        }

        [TestMethod]
        public void SignalValueTest()
        {
            RoundRobin<int> roundRobin = new RoundRobin<int>(new int[] { 1 });

            Assert.AreEqual(1, roundRobin.GetValue());
            Assert.AreEqual(1, roundRobin.GetValue());
        }

        [TestMethod]
        public void ShuffleTest()
        {
            // must shuffle items random

            const int testSize = 1000;

            string[] results = new string[testSize];

            string[] testStrings = new string[11];
            for (int i = 0; i < testStrings.Length; i++)
            {
                testStrings[i] = i.ToString();
            }

            Parallel.For(0, testSize, i =>
            {
                RoundRobin<string> subject = new RoundRobin<string>(testStrings, true);

                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < testStrings.Length; j++)
                {
                    string value = subject.GetValue();
                    sb.Append(value);
                }

                results[i] = sb.ToString();
            });

            HashSet<string> dup = new HashSet<string>();
            for (int i = 0; i < results.Length; i++)
            {
                Assert.IsTrue(dup.Add(results[i]));
            }
        }

        [TestMethod]
        public void RoundRobinTest()
        {
            // do it actualy Round Robin
            const int testSize = 1000000;

            int[] testStrings = new int[10];
            for (int i = 0; i < testStrings.Length; i++)
            {
                testStrings[i] = i;
            }

            RoundRobin<int> subject = new RoundRobin<int>(testStrings);

            for (int i = 0; i < testSize; i++)
            {
                Assert.AreEqual(i % testStrings.Length, subject.GetValue());
            }
        }

        [TestMethod]
        public void EvenDistributionTest()
        {
            const long testSize = 1000000;

            int[] testStrings = new int[10];
            for (int i = 0; i < testStrings.Length; i++)
            {
                testStrings[i] = i;
            }

            RoundRobin<int> subject = new RoundRobin<int>(testStrings);
            int[] testResults = new int[testStrings.Length];

            Parallel.For(0, testSize, i =>
            {
                int value = subject.GetValue();

                Interlocked.Increment(ref testResults[value]);
            });

            int numberDistribution = testResults[0];

            for (int i = 0; i < testResults.Length; i++)
            {
                Assert.AreEqual(numberDistribution, testResults[i]);
            }
        }
    }
}
