﻿namespace AlphaRooms.Utilities.Tests
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// A helper class for the tests
    /// </summary>
    internal class CacheTester
    {
        private static readonly object expected = new object();
        private int numberOfTimesCalled;

        public static object Expected
        {
            get
            {
                return expected;
            }
        }

        public bool Throw { get; set; }

        public int SleepFor { get; set; }

        public int NumberOfTimesCalled
        {
            get
            {
                return this.numberOfTimesCalled;
            }
        }

        public object ObjectMethod()
        {
            Interlocked.Increment(ref this.numberOfTimesCalled);

            if (this.SleepFor > 0)
            {
                Thread.Sleep(this.SleepFor);
            }

            if (this.Throw)
            {
                throw new NotImplementedException();
            }

            return CacheTester.Expected;
        }

        public async Task<object> ObjectMethodAsync()
        {
            Interlocked.Increment(ref this.numberOfTimesCalled);

            if (this.SleepFor > 0)
            {
                Thread.Sleep(this.SleepFor);
            }

            if (this.Throw)
            {
                throw new NotImplementedException();
            }

            return await Task.Run(() =>
            {
                return CacheTester.Expected;
            });
        }

        public async Task<object> TaskExceptionMethodAsync()
        {
            Interlocked.Increment(ref this.numberOfTimesCalled);

            return await Task.Run(() =>
            {
                if (this.SleepFor > 0)
                {
                    Thread.Sleep(this.SleepFor);
                }

                if (this.Throw)
                {
                    throw new NotImplementedException();
                }

                return CacheTester.Expected;
            });
        }

        public int IntMethod()
        {
            int localTimesCalled = Interlocked.Increment(ref this.numberOfTimesCalled);

            if (this.SleepFor > 0)
            {
                Thread.Sleep(this.SleepFor);
            }

            if (this.Throw)
            {
                throw new NotImplementedException();
            }

            return localTimesCalled;
        }

        public async Task<int> IntMethodAsync()
        {
            int localTimesCalled = Interlocked.Increment(ref this.numberOfTimesCalled);

            if (this.SleepFor > 0)
            {
                Thread.Sleep(this.SleepFor);
            }

            if (this.Throw)
            {
                throw new NotImplementedException();
            }

            return await Task.Run(() =>
            {
                return localTimesCalled;
            });
        }
    }
}