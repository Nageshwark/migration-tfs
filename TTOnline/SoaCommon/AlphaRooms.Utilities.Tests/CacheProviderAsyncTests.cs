﻿namespace AlphaRooms.Utilities.Tests
{
    using AlphaRooms.Utilities;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    [TestClass]
    public class CacheProviderAsyncTests
    {
        [TestMethod]
        public void CanCacheValues()
        {
            /*
             * make sure it can cache int & structs
             */
            CacheProviderAsync<int> cacheProvider = new CacheProviderAsync<int>(TimeSpan.FromMinutes(1));

            CacheTester mockObj = new CacheTester();
            string cacheKey = Guid.NewGuid().ToString();

            Task<int> cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.IntMethodAsync);
            Assert.AreEqual(1, cacheTask.Result);

            // result is turned from cache
            cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.IntMethodAsync);
            Assert.AreEqual(1, cacheTask.Result);

            // should be called once only as the second should can from cache
            Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
        }

        [TestMethod]
        public void CanCacheObjects()
        {
            /*
             * make sure it can cache class and objects
             */
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            CacheTester mockObj = new CacheTester();
            string cacheKey = Guid.NewGuid().ToString();

            Task<object> cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Assert.AreEqual(CacheTester.Expected, cacheTask.Result);

            // result is turned from cache
            cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Assert.AreEqual(CacheTester.Expected, cacheTask.Result);

            // should be called once only as the second should can from cache
            Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowsOnNullTask()
        {
            // The cache should not cache a null task
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            cacheProvider.GetOrSetAsync("NullKey", () => (Task<object>)null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DoesNotCacheNullTaskResult()
        {
            /*
             * The cache can not cache a null object - more of a limition than required behavour
             */

            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            Task<object> returnTask = new Task<object>(() =>
            {
                return null;
            });
            returnTask.Start();

            try
            {
                cacheProvider
                    .GetOrSetAsync("NullKey", () => returnTask)
                    .Wait();
            }
            catch (AggregateException ex)
            {
                if (ex.InnerExceptions.Count == 1)
                {
                    throw ex.InnerExceptions[0];
                }

                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void DoesNotCacheCancelledTask()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.Cancel();

            Task<object> cancelledTask = new Task<object>(() => new object(), cts.Token);

            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            // should throw
            object obj = cacheProvider.GetOrSetAsync("CancelledTask", () => cancelledTask).Result;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetOrSetThrowsOnNullFetchFunction()
        {
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            // bad usage, should throw
            cacheProvider.GetOrSetAsync("NullKey", null).Wait();
        }

        [TestMethod]
        public void DoesNotCacheFetchFunctionExceptions()
        {
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            string cacheKey = Guid.NewGuid().ToString();

            CacheTester mockObj = new CacheTester();
            mockObj.Throw = true;

            Task shouldThrow = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Task.WaitAny(shouldThrow);
            Assert.AreEqual(TaskStatus.Faulted, shouldThrow.Status, "Task should have faulted");

            mockObj.Throw = false;
            Task shouldNotThrow = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Task.WaitAny(shouldNotThrow);

            Assert.AreEqual(TaskStatus.RanToCompletion, shouldNotThrow.Status, "Task should have not faulted. The exception has been cached.");
        }

        [TestMethod]
        public void DoesNotCacheExceptionsThrownByTasks()
        {
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            string cacheKey = Guid.NewGuid().ToString();

            CacheTester mockObj = new CacheTester();
            mockObj.Throw = true;

            Task shouldThrow = cacheProvider.GetOrSetAsync(cacheKey, mockObj.TaskExceptionMethodAsync);
            Task.WaitAny(shouldThrow);
            Assert.AreEqual(TaskStatus.Faulted, shouldThrow.Status, "Task should have faulted");

            mockObj.Throw = false;
            Task shouldNotThrow = cacheProvider.GetOrSetAsync(cacheKey, mockObj.TaskExceptionMethodAsync);
            Task.WaitAny(shouldNotThrow);

            Assert.AreEqual(TaskStatus.RanToCompletion, shouldNotThrow.Status, "Task should have not faulted. The exception has been cached.");
        }

        [TestMethod]
        public void DoesShareFetchFunctionExceptions()
        {
            /*
             * if multipul requests have been pool and it throws, the expection need to be share across all pooled requests
             */
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            string cacheKey = Guid.NewGuid().ToString();

            CacheTester mockObj = new CacheTester();
            mockObj.Throw = true;

            // add a sleep to make sure the requests get pooled
            mockObj.SleepFor = 1000;

            Task[] tasks = new Task[500];

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() =>
                    {
                        try
                        {
                            var cacheItem = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync).Result;
                            Assert.Fail("Task should have faulted");
                        }
                        catch (AggregateException ex)
                        {
                            if (ex.InnerExceptions.Count != 1 || !(ex.InnerExceptions[0].InnerException is NotImplementedException))
                            {
                                throw;
                            }
                        }
                    },
                TaskCreationOptions.LongRunning); // use long running so tasks are not delayed in starting 

                tasks[i].Start();
            }

            Task.WaitAll(tasks);

            // should only be called once and exception shared
            Assert.AreEqual(1, mockObj.NumberOfTimesCalled);

            /*
             * In dev we found a problem where the CacheRequest was not removed from the in memory cache.
             * This lead to excetion being return when it should not
             * The below bit of the test is to make sure this has not happened
             */

            // should not throw now 
            mockObj.Throw = false;
            mockObj.SleepFor = 0;

            object actual = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync).Result;
            Assert.AreEqual(CacheTester.Expected, actual);
            Assert.AreEqual(2, mockObj.NumberOfTimesCalled);
        }

        [TestMethod]
        public void DoesShareTaskExceptions()
        {
            /*
             * if multipul requests have been pool and it throws, the expection need to be share across all pooled requests
             */
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));

            string cacheKey = Guid.NewGuid().ToString();

            CacheTester mockObj = new CacheTester();
            mockObj.Throw = true;

            // add a sleep to make sure the requests get pooled
            mockObj.SleepFor = 1000;

            Task[] tasks = new Task[500];

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() =>
                {
                    try
                    {
                        cacheProvider.GetOrSetAsync(cacheKey, mockObj.TaskExceptionMethodAsync).Wait();
                        Assert.Fail("Task should have faulted");
                    }
                    catch (AggregateException ex)
                    {
                        if (ex.InnerExceptions.Count != 1 || !(ex.InnerExceptions[0].InnerException is NotImplementedException))
                        {
                            throw;
                        }
                    }
                },
                TaskCreationOptions.LongRunning); // use long running so tasks are not delayed in starting 

                tasks[i].Start();
            }

            Task.WaitAll(tasks);

            // should only be called once and exception shared
            Assert.AreEqual(1, mockObj.NumberOfTimesCalled);

            /*
             * In dev we found a problem where the CacheRequest was not removed from the in memory cache.
             * This lead to excetion being return when it should not
             * The below bit of the test is to make sure this has not happened
             */

            // should not throw now 
            mockObj.Throw = false;
            mockObj.SleepFor = 0;

            object actual = cacheProvider.GetOrSetAsync(cacheKey, mockObj.TaskExceptionMethodAsync).Result;
            Assert.AreEqual(CacheTester.Expected, actual);
            Assert.AreEqual(2, mockObj.NumberOfTimesCalled);
        }

        [TestMethod]
        public void AllowsOnlyOneExecution()
        {
            /*
             * In multi thread conditions the cache should be thread safe and only allow one execution as the request are pooled.
             * Should always return the cached result.
             */

            const int Iterations = 20;

            Parallel.For(0, Iterations, i =>
            {
                CacheTester mockObj = new CacheTester();

                string cacheKey = Guid.NewGuid().ToString();
                CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMinutes(1));


                // hammer the thing from multipul threads
                Parallel.For(0, Iterations * 5, j =>
                {
                    Task<object> cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
                    object actual = cacheTask.Result;
                    Assert.AreEqual(CacheTester.Expected, actual);
                });

                Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
            });
        }

        [TestMethod]
        public void CacheItemsExpire()
        {
            // make sure it can cache class and objects
            CacheProviderAsync<object> cacheProvider = new CacheProviderAsync<object>(TimeSpan.FromMilliseconds(1));

            CacheTester mockObj = new CacheTester();
            string cacheKey = Guid.NewGuid().ToString();

            Task<object> cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Assert.AreEqual(CacheTester.Expected, cacheTask.Result);

            // to make sure item expired from cache
            Thread.Sleep(250);

            cacheTask = cacheProvider.GetOrSetAsync(cacheKey, mockObj.ObjectMethodAsync);
            Assert.AreEqual(CacheTester.Expected, cacheTask.Result);

            // should be called twice
            Assert.AreEqual(2, mockObj.NumberOfTimesCalled);
        }
    }
}