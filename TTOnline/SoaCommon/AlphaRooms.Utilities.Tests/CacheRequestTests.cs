﻿namespace AlphaRooms.Utilities.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Threading.Tasks;
    using System.Threading;

    [TestClass]
    public class CacheRequestTests
    {
        [TestMethod]
        public void CacheRequestStoresKey()
        {
            string expected = "key";
            CacheRequest cacheRequest = new CacheRequest(expected);
            Assert.AreEqual(expected, cacheRequest.Key);
        }

        #region Value handling

        [TestMethod]
        public void CacheRequestStoresValue()
        {
            object expected = new object();

            CacheRequest cacheRequest = new CacheRequest("key");

            cacheRequest.SetResult(expected);

            object actual = cacheRequest.GetResult();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CacheRequestStoresExceptionValue()
        {
            Exception expected = new Exception();

            CacheRequest cacheRequest = new CacheRequest("key");

            cacheRequest.SetResult(expected);

            object actual = cacheRequest.GetResult();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CacheRequestThrowsOnNullValue()
        {
            // you can not cache null objects
            object value = null;

            CacheRequest cacheRequest = new CacheRequest("key");

            cacheRequest.SetResult(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CacheRequestResultNullArgIsShared()
        {
            // if a null value is attemped to be set we need to share this with the other threads waiting for a result else they maybe waiting forever

            // you can not cache null objects
            object value = null;

            CacheRequest cacheRequest = new CacheRequest("key");

            try
            {
                cacheRequest.SetResult(value);
            }
            catch (ArgumentNullException)
            {
            }

            cacheRequest.GetResult();
        }

        #endregion

        #region Exception rethrowning

        [TestMethod]
        public void CacheRequestRethrowsException()
        {
            Exception expected = new Exception();

            CacheRequest cacheRequest = new CacheRequest("key");

            cacheRequest.SetException(expected);

            try
            {
                cacheRequest.GetResult();
                Assert.Fail("Test shioud have thrown");
            }
            catch (Exception actual)
            {
                Assert.AreEqual(expected, actual);
            }
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CacheRequestExNullArgIsShared()
        {
            // if a null value is attemped to be set we need to share this with the other threads waiting for a result else they maybe waiting forever

            // you can not cache null objects
            Exception value = null;

            CacheRequest cacheRequest = new CacheRequest("key");

            try
            {
                cacheRequest.SetException(value);
            }
            catch (ArgumentNullException)
            {
            }

            cacheRequest.GetResult();
        }

        #endregion

        #region thread blocking

        [TestMethod]
        public void CacheRequestBlocksUntilResultsSet()
        {
            object expected = new object();

            CacheRequest cacheRequest = new CacheRequest("key");

            // use lots of tasks to make sure it is thread safe
            Task<object>[] getTasks = new Task<object>[500];

            Parallel.For(0, getTasks.Length, i =>
            {
                getTasks[i] = Task.Factory.StartNew(
                    cacheRequest.GetResult,
                    TaskCreationOptions.LongRunning); // long running to skip the task queue
            });

            for (int i = 0; i < getTasks.Length; i++)
            {
                Assert.AreEqual(TaskStatus.Running, getTasks[i].Status, "Task should be running waiting for result");
            }

            cacheRequest.SetResult(expected);

            for (int i = 0; i < getTasks.Length; i++)
            {
                // tasks should be completing now
                getTasks[i].Wait(500);
                Assert.AreEqual(TaskStatus.RanToCompletion, getTasks[i].Status, "Task should now complete");
            }
        }

        [TestMethod]
        public void CacheRequestBlocksUntilExceptionSet()
        {
            Exception expected = new Exception();

            CacheRequest cacheRequest = new CacheRequest("key");

            // use lots of tasks to make sure it is thread safe
            Task<object>[] getTasks = new Task<object>[500];

            Parallel.For(0, getTasks.Length, i =>
            {
                getTasks[i] = Task.Factory.StartNew(
                    cacheRequest.GetResult,
                    TaskCreationOptions.LongRunning); // long running to skip the task queue
            });

            for (int i = 0; i < getTasks.Length; i++)
            {
                Assert.AreEqual(TaskStatus.Running, getTasks[i].Status, "Task should be running waiting for result");
            }

            cacheRequest.SetException(expected);

            try
            {
                Task.WaitAll(getTasks, 500);
            }
            catch (AggregateException)
            {
            }

            for (int i = 0; i < getTasks.Length; i++)
            {
                // tasks should be completing now
                Assert.AreEqual(TaskStatus.Faulted, getTasks[i].Status, "Task should now faulted");
            }
        }

        [TestMethod]
        public void CacheRequestNoNotBlockTest()
        {
            /*
             * This test tries to create race condictions within the CacheRequest class.
             * The race condiction result in threads waiting for the result to be set even though when it had already been set 
             * If the test is hanging and not completing we have a problem
             */

            for (int i = 0; i < 5000; i++)
            {
                Parallel.For(0, 100, j =>
                {
                    CacheRequest cacheRequest = new CacheRequest("key");

                    Task[] tasks = new Task[]
                    {
                        new Task(() => Parallel.For( 0, 5, notUsed => cacheRequest.GetResult())),
                        new Task(() =>  cacheRequest.SetResult(1)),
                    };

                    Parallel.ForEach(tasks, t => t.Start());
                    Task.WaitAll(tasks);
                });
            }
        }
        #endregion
    }
}