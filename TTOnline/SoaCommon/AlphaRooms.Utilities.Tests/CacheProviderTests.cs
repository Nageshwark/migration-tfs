﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Threading;

namespace AlphaRooms.Utilities.Tests
{
    [TestClass]
    public class CacheProviderTests
    {
        //[TestMethod]
        //public void CanCacheValues()
        //{
        //    /*
        //     * make sure it can cache int & structs
        //     */
        //    CacheProvider<int> cacheProvider = new CacheProvider<int>(TimeSpan.FromMinutes(1));

        //    CacheTester mockObj = new CacheTester();
        //    string cacheKey = Guid.NewGuid().ToString();

        //    int cacheValue = cacheProvider.GetOrSet(cacheKey, mockObj.IntMethod);
        //    Assert.AreEqual(1, cacheValue);

        //    // result is turned from cache
        //    cacheValue = cacheProvider.GetOrSet(cacheKey, mockObj.IntMethod);
        //    Assert.AreEqual(1, cacheValue);

        //    // should be called once only as the second should can from cache
        //    Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
        //}

        //[TestMethod]
        //public void CanCacheObjects()
        //{
        //    /*
        //     * make sure it can cache class and objects
        //     */
        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //    CacheTester mockObj = new CacheTester();
        //    string cacheKey = Guid.NewGuid().ToString();

        //    object cacheValue = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //    Assert.AreEqual(CacheTester.Expected, cacheValue);

        //    // result is turned from cache
        //    cacheValue = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //    Assert.AreEqual(CacheTester.Expected, cacheValue);

        //    // should be called once only as the second should can from cache
        //    Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public void DoesNotCacheNull()
        //{
        //    /*
        //     * The cache can not cache a null object - more of a limition than required behavour
        //     */

        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //    cacheProvider.GetOrSet("NullKey", () => null);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public void GetOrSetThrowsOnNullFetchFunction()
        //{
        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //    // bad usage, should throw
        //    cacheProvider.GetOrSet("NullKey", null);
        //}

        //[TestMethod]
        //public void DoesNotCacheExceptions()
        //{
        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //    string cacheKey = Guid.NewGuid().ToString();

        //    CacheTester mockObj = new CacheTester();
        //    mockObj.Throw = true;

        //    try
        //    {
        //        cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //        Assert.Fail("Task should have faulted");
        //    }
        //    catch (NotImplementedException)
        //    {
        //    }

        //    mockObj.Throw = false;

        //    // should not throw
        //    object actual = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);

        //    Assert.AreEqual(CacheTester.Expected, actual);
        //}

        //[TestMethod]
        //public void DoesShareExceptions()
        //{
        //    /*
        //     * if multipul requests have been pool and it throws, the expection need to be share across all pooled requests
        //     */
        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //    string cacheKey = Guid.NewGuid().ToString();

        //    CacheTester mockObj = new CacheTester();
        //    mockObj.Throw = true;

        //    // add a sleep to make sure the requests get pooled
        //    mockObj.SleepFor = 1000;

        //    Task[] tasks = new Task[500];

        //    for (int i = 0; i < tasks.Length; i++)
        //    {
        //        tasks[i] = new Task(() =>
        //        {
        //            try
        //            {
        //                cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //                Assert.Fail("Task should have faulted");
        //            }
        //            catch (NotImplementedException)
        //            {
        //            }
        //        },
        //        TaskCreationOptions.LongRunning); // use long running so tasks are not delayed in starting 

        //        tasks[i].Start();
        //    }

        //    Task.WaitAll(tasks);

        //    // should only be called once and exception shared
        //    Assert.AreEqual(1, mockObj.NumberOfTimesCalled);

        //    /*
        //     * In dev we found a problem where the CacheRequest was not removed from the in memory cache.
        //     * This lead to excetion being return when it should not
        //     * The below bit of the test is to make sure this has not happened
        //     */

        //    // should not throw now 
        //    mockObj.Throw = false;
        //    mockObj.SleepFor = 0;

        //    object actual = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //    Assert.AreEqual(CacheTester.Expected, actual);
        //    Assert.AreEqual(2, mockObj.NumberOfTimesCalled);
        //}

        //// the below test is commented out has it fails as Dan took it out as it was caching exceptions
        //[TestMethod]
        //public void AllowsOnlyOneExecution()
        //{
        //    /*
        //     * In multi thread conditions the cache should be thread safe and only allow one execution as the request are pooled.
        //     * Should always return the cached result.
        //     */

        //    const int Iterations = 20;

        //    Parallel.For(0, Iterations, i =>
        //    {
        //        CacheTester mockObj = new CacheTester();

        //        string cacheKey = Guid.NewGuid().ToString();
        //        CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMinutes(1));

        //        // hammer the thing from multipul threads
        //        Parallel.For(0, Iterations * 5, j =>
        //            {
        //                var actual = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //                Assert.AreEqual(CacheTester.Expected, actual);
        //            });

        //        Assert.AreEqual(1, mockObj.NumberOfTimesCalled);
        //    });
        //}

        //[TestMethod]
        //public void CacheItemsExpire()
        //{
        //    // make sure it can cache class and objects
        //    CacheProvider<object> cacheProvider = new CacheProvider<object>(TimeSpan.FromMilliseconds(1));

        //    CacheTester mockObj = new CacheTester();
        //    string cacheKey = Guid.NewGuid().ToString();

        //    object actual = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //    Assert.AreEqual(CacheTester.Expected, actual);

        //    // to make sure item expired from cache
        //    Thread.Sleep(250);

        //    actual = cacheProvider.GetOrSet(cacheKey, mockObj.ObjectMethod);
        //    Assert.AreEqual(CacheTester.Expected, actual);

        //    // should be called twice
        //    Assert.AreEqual(2, mockObj.NumberOfTimesCalled);
        //}
    }
}