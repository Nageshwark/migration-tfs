﻿namespace AlphaRooms.Utilities.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.IO;

    [TestClass]
    public class StringExtensionsTests
    {

        #region ToPascalCase

        [TestMethod]
        public void ToPascalCaseStartLetterIsUpperCase()
        {
            string test = "hello";
            string actual = test.ToPascalCase();
            string expected = "Hello";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToPascalCaseLettersAreLowerCase()
        {
            string test = "HELLO";
            string actual = test.ToPascalCase();
            string expected = "Hello";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToPascalCaseNewWordLetterIsUpperCase()
        {
            string test = "Hello world";
            string actual = test.ToPascalCase();
            string expected = "Hello World";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToPascalCaseNewWordLettersIsLowerCase()
        {
            string test = "Hello WORLD";
            string actual = test.ToPascalCase();
            string expected = "Hello World";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToPascalCaseUpperAfterNonLetter()
        {
            string test = "hello.world";
            string actual = test.ToPascalCase();
            string expected = "Hello.World";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToPascalCaseIdenticalResultsAsOriginal()
        {
            /*
             * It was found that the Original ToPascalCase had very bad performance, it took up most of the CPU from a search.
             * It was rewrote to have better performance. This test is to make sure non of the behaviour was altered. 
             */

            // this is real data taken from a live search
            string[] roomDescriptions = File.ReadAllLines("MallorcaSearchRoomDescriptions.txt");

            for (int i = 0; i < roomDescriptions.Length; i++)
            {
                string expected = OriginalToPascalCase(roomDescriptions[i]);
                string actual = roomDescriptions[i].ToPascalCase();

                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        /// this was the Original ToPascalCase it was found to have very bad performance and was most of the CPU from a search
        /// </summary>
        private static string OriginalToPascalCase(string value)
        {
            if (string.IsNullOrEmpty(value)) return value;

            var chars = value.ToLower().ToCharArray();
            chars[0] = char.ToUpper(value[0]);

            if (value.Length == 1) return new string(chars);

            for (var i = 1; i < value.Length; i++)
                if (i > 0 && value[i - 1] == ' ' || !char.IsLetterOrDigit(value[i - 1]))
                    chars[i] = char.ToUpper(value[i]);

            return new string(chars);
        }

        /*
         * if looking at the speed of the ToPascalCase looking at the text in MallorcaSearchRoomDescriptions.txt 
         * there are a lot repeated. It might be quicker to use a case insensitive dictionary to store results, 
         * lookup might be quicker and memory overhead reduced as string instances will be shared.
         */

        ////[TestMethod]
        ////public void ToPascalCaseSpeedTest()
        ////{
        ////    const int iterations = 800;

        ////    // this is real data taken from a live search
        ////    string[] roomDescriptions = File.ReadAllLines("MallorcaSearchRoomDescriptions.txt");

        ////    // warm up
        ////    for (int i = 0; i < iterations / 100; i++)
        ////    {
        ////        for (int j = 0; j < roomDescriptions.Length; j++)
        ////        {
        ////            roomDescriptions[j].ToPascalCase();
        ////            OldToPascalCase(roomDescriptions[j]);
        ////        }
        ////    }

        ////    for (int t = 0; t < 3; t++)
        ////    {
        ////        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        ////        for (int i = 0; i < iterations; i++)
        ////        {
        ////            for (int j = 0; j < roomDescriptions.Length; j++)
        ////            {
        ////                roomDescriptions[j].ToPascalCase();
        ////            }
        ////        }
        ////        sw.Stop();
        ////        System.Console.WriteLine("ToPascalCase " + sw.Elapsed);

        ////        sw = System.Diagnostics.Stopwatch.StartNew();
        ////        for (int i = 0; i < iterations; i++)
        ////        {
        ////            for (int j = 0; j < roomDescriptions.Length; j++)
        ////            {
        ////                OldToPascalCase(roomDescriptions[j]);
        ////            }
        ////        }
        ////        sw.Stop();
        ////        System.Console.WriteLine("Old " + sw.Elapsed);
        ////    }
        ////}

        ////private static string OldToPascalCase(string value)
        ////{
        ////    if (string.IsNullOrEmpty(value)) return value;

        ////    var chars = value.ToLower().ToCharArray();
        ////    chars[0] = char.ToUpper(value[0]);

        ////    if (value.Length == 1) return new string(chars);

        ////    for (var i = 1; i < value.Length; i++)
        ////        if (i > 0 && value[i - 1] == ' ' || !char.IsLetterOrDigit(value[i - 1]))
        ////            chars[i] = char.ToUpper(value[i]);

        ////    return new string(chars);
        ////}

        #endregion
    }
}