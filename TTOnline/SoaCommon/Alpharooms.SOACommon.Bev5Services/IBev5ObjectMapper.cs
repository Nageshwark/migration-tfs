﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Bev5Services
{
    public interface IBev5ObjectMapper<source,destination>
        where source :class
        where destination :class 
    {
        destination Map(source sourceObject);
    }
}
