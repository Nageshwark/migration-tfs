﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Bev5Services
{
    public interface IBev5CommonClientService<Request,Response> 
        where Request :class 
        where Response:class
    {
        Response GetResponseFromBev5(Request request); 
    }
}
