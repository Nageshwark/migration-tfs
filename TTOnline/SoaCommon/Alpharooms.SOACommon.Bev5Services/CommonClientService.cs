﻿using AlphaRooms.Bev5.Contracts.Request.Search;
using AlphaRooms.Bev5.Contracts.Response.Search;
using AlphaRooms.Bev5.Contracts.ServiceInterfaces;
using AlphaRooms.Common.Helpers;
using AlphaRooms.SOACommon.Bev5Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlphaRooms.Bev5.Contracts.Request.Evaluate;

namespace AlphaRooms.SOACommon.Bev5Services
{
    public sealed class CommonClientService<Request, Response> : IBev5CommonClientService<Request, Response>
        where Request : class
        where Response : class
    {
        private readonly IBev5Configuration Configuration;

        public CommonClientService(IBev5Configuration Configuration)
        {
            this.Configuration = Configuration;
        }

        public Response GetResponseFromBev5(Request request)
        {
            if (request != null)
            {
                if (typeof(Request) == typeof(SearchFlightsRequest))
                {
                    var searchResponse =
                    WcfHelper.UseServiceClient((IAvailabilityService client) => client.SearchFlights(request as SearchFlightsRequest),
                    Configuration.Url);
                    
                    return (Response)Convert.ChangeType(searchResponse, typeof(Response));
                }
                if (typeof(Request) == typeof(ValuationFlightsRequest))
                {
                    var searchResponse =
                    WcfHelper.UseServiceClient((IAvailabilityService client) => client.ValuateFlights (request as ValuationFlightsRequest),
                    Configuration.Url);

                    return (Response)Convert.ChangeType(new AlphaRooms.Bev5.Contracts.Response.Evaluate.ValuationFlightsResponse(), typeof(Response));
                }
            }
            return null;
        }
    }
}
