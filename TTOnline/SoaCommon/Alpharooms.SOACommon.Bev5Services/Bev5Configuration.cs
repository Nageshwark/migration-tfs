﻿using System;
using AlphaRooms.Configuration;
using AlphaRooms.SOACommon.Interfaces;

namespace AlphaRooms.SOACommon.Bev5Services
{
    public class Bev5Configuration:ConfigurationManagerBase,IBev5Configuration 
    {
        public Bev5Configuration(Ninject.Extensions.Logging.ILogger logger, IParameterRepository parameterRepository) : base(logger, parameterRepository) { }

        public string Url
        {
            get { return this["Bev5Url", "http://localhost:8000/AlphaRooms.Bev5.WcfInterface/service"]; }
            
        }
        
        public int GetProviderId(string ProviderCode)
        {
            
            return Convert.ToInt32(this[ProviderCode, "1"]);
            
        }
    }
}
