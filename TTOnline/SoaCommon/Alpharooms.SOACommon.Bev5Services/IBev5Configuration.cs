﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaRooms.SOACommon.Bev5Services
{
    public interface IBev5Configuration
    {
        int GetProviderId(string ProviderCode);

        string Url { get; }
        
    }
}
