﻿using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.Misc
{


    public class ESRShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class ESRHits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<object> hits { get; set; }
    }

    public class ESRMinPrice
    {
        public double value { get; set; }
    }

    public class ESRJourney
    {
        public DateTime outboundArrivalDate { get; set; }
        public DateTime inboundDepartureDate { get; set; }
        public DateTime departureDate { get; set; }
        public DateTime returnDate { get; set; }
        public int duration { get; set; }
        public string departure { get; set; }
        public string gatewayName { get; set; }
        public string flightOperator { get; set; }
        public string destination { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public string gatewayCode { get; set; }
        public bool inboundFlightDirect { get; set; }
        public bool outboundFlightDirect { get; set; }
    }

    public class ESRAccommodation
    {
        public string boardTypeCode { get; set; }
        public string boardTypeId { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int rating { get; set; }
        public DateTime updated { get; set; }
    }
    public class ESRRating
    {
        public string tripAdvisorId { get; set; }
        public float averageRating { get; set; }
        public int reviewCount { get; set; }
    }

    public class ESRHotel
    {
        public string iff { get; set; }
        public List<string> images { get; set; }
        public object mobileimages { get; set; }
        public string description { get; set; }
        public List<object> features { get; set; }
        public string name { get; set; }
        public ESRRating rating { get; set; }
        public string latlon { get; set; }
        public string starRating { get; set; }
    }

    public class ESRSource
    {
        public string id { get; set; }
        public string quoteRef { get; set; }
        public int price { get; set; }
        public bool isPreferential { get; set; }
        public string phone { get; set; }
        public ESRJourney journey { get; set; }
        public ESRAccommodation accommodation { get; set; }
        public ESRHotel hotel { get; set; }
        public string hotelOperator { get; set; }
        public string contentId { get; set; }
        public int tradingNameId { get; set; }
        public bool isPreferredByUser { get; set; }
        public int airportCode { get; set; }
        public int airportCollectionId { get; set; }
        public int regionId { get; set; }
        public int labelId { get; set; }
    }

    public class ESRHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public ESRSource _source { get; set; }
        public List<int> sort { get; set; }
    }

    public class ESRTPHits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public List<ESRHit> hits { get; set; }
    }

    public class ESTopPrice
    {
        public ESRTPHits hits { get; set; }
    }

    public class ESRBucket
    {
        public string key { get; set; }
        public int doc_count { get; set; }
        public ESRMinPrice min_price { get; set; }
        public ESTopPrice top_price { get; set; }
    }

    public class ESRHotelIff
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<ESRBucket> buckets { get; set; }
    }
    public class ESRResorts
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<ESRResortsBucket> buckets { get; set; }

    }
    public class Region_Source
    {
        public string regionName { get; set; }
    }
    public class Hits_Region_Hits
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public int _ttl { get; set; }
        public Region_Source _source { get; set; }
        public List<int> sort { get; set; }
    }
    public class Region_Hits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public List<Hits_Region_Hits> hits { get; set; }
    }
    public class Region_TopPrice
    {
        public Region_Hits hits { get; set; }
    }

    public class ESRResortsBucket
    {
        public double key { get; set; }
        public int doc_count { get; set; }
        public Region_TopPrice top_price { get; set; }
        public Region_RegionId region_id { get; set; }
    }
    public class Region_RegionId
    {
        public double value { get; set; }
    }
    public class ESRPriceMin
    {
        public object value { get; set; }
    }
    public class ESRPriceMax
    {
        public object value { get; set; }
    }
    public class ESRAirportBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }

    }
    public class HoteliffValue
    {
        public double value { get; set; }
    }
    public class ESRAirports
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<ESRAirportBucket> buckets { get; set; }
    }
    public class HotelTop_Price
    {
        public HitsHotel hits { get; set; }
    }
    public class HitsHotel
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public List<HitsHotelHits> hits { get; set; }
    }
    public class SourceHotel
    {
        public int regionId { get; set; }
        public HotelName hotel { get; set; }
    }
    public class HotelName
    {
        public string name { get; set; }
    }
    public class HitsHotelHits
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public int _ttl { get; set; }
        public SourceHotel _source { get; set; }
        public List<int> sort { get; set; }
    }
    public class ESRAggHotelBucket
    {
        public double key { get; set; }
        public int doc_count { get; set; }
        public HoteliffValue hoteliff { get; set; }
        public HotelTop_Price top_price { get; set; }
    }
    public class ESRAggHotel
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<ESRAggHotelBucket> buckets { get; set; }
    }
    public class ESRAggregations
    {
        public ESRHotelIff hotel_iff { get; set; }
        public ESRPriceMin price_Min { get; set; }
        public ESRPriceMax price_Max { get; set; }
        public ESRResorts resorts { get; set; }
        public ESRAirports airports { get; set; }
        public ESRAggHotel hotel { get; set; }
    }

    public class ElasticsearchResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public ESRShards _shards { get; set; }
        public ESRHits hits { get; set; }
        public ESRAggregations aggregations { get; set; }
    }


    // ********************** Search Response ****************//
    public class ESSearchResponseShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class ESSearchResponseSource
    {
        public int adults { get; set; }
        //public string boardTypeId { get; set; }
        public int children { get; set; }
        public string dateMax { get; set; }
        public string dateMin { get; set; }
        //public string departureDate { get; set; }
        public int departureIds { get; set; }
        public int destinationIds { get; set; }
        public int duration { get; set; }
        //public int priceMax { get; set; }
        //public int priceMin { get; set; }
        //public string ratings { get; set; }
        public long responseId { get; set; }
        public int ttssCount { get; set; }
        public List<string> regionIds { get; set; }
        public int parentDepartureIds { get; set; }
        public bool isCahceHit { get; set; }
        public double ttssResponseTime { get; set; }
    }

    public class ESSearchResponseHitResults
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public int _ttl { get; set; }
        public ESSearchResponseSource _source { get; set; }
    }

    public class ESSearchResponseHits
    {
        public int total { get; set; }
        public double? max_score { get; set; }
        public List<ESSearchResponseHitResults> hits { get; set; }
    }

    public class ESSearchResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public ESSearchResponseShards _shards { get; set; }
        public ESSearchResponseHits hits { get; set; }
    }
    //***************** Insertion Response ***************//
    public class ESNewSearchInsertionShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class ESNewSearchInsertion
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public ESNewSearchInsertionShards _shards { get; set; }
        public bool created { get; set; }
    }


    //********************Response Object ***************************//
    public class SearchResponse
    {
        public long responseId { get; set; }
        public int ttssCount { get; set; }
    }
    //****************** Diversity Response ***********************//

    public class DiversityShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class DiversityHits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<object> hits { get; set; }
    }

    public class DiversityMinPrice
    {
        public double value { get; set; }
    }

    public class DiversityJourney
    {
        public string departureDate { get; set; }
    }

    public class Source
    {
        public DiversityJourney journey { get; set; }
        public int price { get; set; }
        public string quoteRef { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public Source _source { get; set; }
        public List<int> sort { get; set; }
    }

    public class TopPriceHits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public List<Hit> hits { get; set; }
    }

    public class TopPrice
    {
        public TopPriceHits hits { get; set; }
    }

    public class BoardTypeBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public DiversityMinPrice min_price { get; set; }
        public TopPrice top_price { get; set; }
    }

    public class GroupByCategory
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<BoardTypeBucket> buckets { get; set; }
    }

    public class IFFBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public GroupByCategory group_by_category { get; set; }
    }

    public class GroupByIff
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<IFFBucket> buckets { get; set; }
    }

    public class DiversityAggregations
    {
        public GroupByIff group_by_iff { get; set; }
    }

    public class DiversityResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public DiversityShards _shards { get; set; }
        public DiversityHits hits { get; set; }
        public DiversityAggregations aggregations { get; set; }
    }
    //******************Facets Response********************//

    public class FacetsResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public _Shards _shards { get; set; }
        public Hits hits { get; set; }
        public FacetsAggregations aggregations { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public object[] hits { get; set; }
    }

    public class FacetsAggregations
    {
        public FacetsHotel hotel { get; set; }
        public FacetsResorts resorts { get; set; }
        public FacetsPrice_Min price_min { get; set; }
        public FacetsPrice_Max price_max { get; set; }
        public FacetsAirports airports { get; set; }
    }

    public class FacetsHotel
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public FacetsHotelBucket[] buckets { get; set; }
    }

    public class FacetsHotelBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
    }

    public class FacetsResorts
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public FacetsResortsBucket[] buckets { get; set; }
    }

    public class FacetsResortsBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public NoOfHotels NoOfHotels { get; set; }
    }
    public class NoOfHotels
    {
        public int value { get; set; }
    }

    public class FacetsPrice_Min
    {
        public double? value { get; set; }
    }

    public class FacetsPrice_Max
    {
        public double? value { get; set; }
    }

    public class FacetsAirports
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public FacetsAirportsBucket[] buckets { get; set; }
    }

    public class FacetsAirportsBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
    }
    //***********************Diversity Search Response ****************

    public class DSRShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class DSRSource
    {
        public int adults { get; set; }
        public int children { get; set; }
        public string dateMax { get; set; }
        public string dateMin { get; set; }
        public int departureIds { get; set; }
        public string destinationIds { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int ttssCount { get; set; }
        public long responseId { get; set; }
        public double ttssResponseTime { get; set; }
        public bool isCahceHit { get; set; }
        public List<string> ratings { get; set; }
        public string timeStamp { get; set; }
        public List<string> tradingNameId { get; set; }
        public string ttssURL { get; set; }
        public int offersFound { get; set; }
        public int offersDecorated { get; set; }
        public int offersInserted { get; set; }
        public List<string> hotelKeys { get; set; }
    }

    public class DSRHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public int _ttl { get; set; }
        public DSRSource _source { get; set; }
    }

    public class DSRHits
    {
        public int total { get; set; }
        public double? max_score { get; set; }
        public List<DSRHit> hits { get; set; }
    }

    public class DiversitySearchResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public DSRShards _shards { get; set; }
        public DSRHits hits { get; set; }
    }

}
