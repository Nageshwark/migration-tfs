﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class DestinationInfo
    {
        public string destinationCode { get; set; }
        public string destinationName { get; set; }
        public string parentDestinationCode { get; set; }
        public string parentDestinationName { get; set; }
    }
    public class AlphaDestinationTTRegion
    {
        public Guid destinationCode { get; set; }
        public int regionId { get; set; }
    }
}
