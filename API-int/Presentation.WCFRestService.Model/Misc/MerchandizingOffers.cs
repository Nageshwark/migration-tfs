﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class MerchandizingOffers
    {
        public string sectionId { get; set; }
        public string queryId { get; set; }
        public int minPrice { get; set; }
        public string lastUpdated { get; set; }
        public string quoteRef { get; set; }
        public string image { get; set; }
        public string hotelLocation { get; set; }
        public string starRating { get; set; }
        public string tripAdvisorRating { get; set; }
        public string reviewCount { get; set; }
        public string boardBasis { get; set; }
        public string displayParentRegion { get; set; }
        public string displaySectionName { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public MinPriceToolOfferQuery query { get; set; }
        public bool? isImageOverride { get; set; }
        public bool? isFromDesktop { get; set; }
        public bool? isFromMobile { get; set; }
        public string phoneNumber { get; set; }
    }

    public class MinPriceToolOfferQuery
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string baseDepartureDate { get; set; }
        public List<int> destinationIds { get; set; }
        public List<int> departureIds { get; set; }
        public List<int> boardType { get; set; }
        public int duration { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public List<int> ratings { get; set; }
        public string channelId { get; set; }
        public bool flexibleDates { get; set; }
        public int dateOffset { get; set; }
        public string tradingNameId { get; set; }
        public string iff { get; set; }
        public string channelName { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public string destinationName { get; set; }
        public string departureName { get; set; }
        public string websiteUrl { get; set; }
        public string artirixUrl { get; set; }
        public bool isHotelLandingPageAvailable { get; set; }
        public string hotelLandingPageUrl { get; set; }
        public bool isDynamic { get; set; }
        public string dynamicUrl { get; set; }
    }
}
