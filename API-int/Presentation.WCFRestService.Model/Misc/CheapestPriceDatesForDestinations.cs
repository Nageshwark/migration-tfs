﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class CheapestPriceDatesForDestinations
    {
        public string destinationId { get; set; }
        public string destLabel { get; set; }
        public string channelId { get; set; }
        public string boardType { get; set; }
        public string cheapestDepartureDate { get; set; }
        public int duration { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
    }
}
