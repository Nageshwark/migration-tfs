﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class TravelAlert
    {
        public Guid travelAlertId { get; set; }
        public string travelAlertMessage { get; set; }
        public List<Website> travelAlertWebConfiguration { get; set; }
        public string updatedBy { get; set; }
        public string updatedDate { get; set; }
        public string createdBy { get; set; }
        public string createdDate { get; set; }
    }

    public class Website
    {
        public string website { get; set; }
        public bool showInHomePage { get; set; }
        public bool showInAllPages { get; set; }
    }

    public class OperationResponse
    {
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
    }

    public class TravelAlertOperationResponse
    {
        public List<TravelAlert> travelAlerts { get; set; }
        public OperationResponse operationResponse { get; set; }
    }
    public class TravelAlertWeb
    {
        public Guid travelAlertId { get; set; }
        public string travelAlertMessage { get; set; }
        public string updatedBy { get; set; }
        public string updatedDate { get; set; }
        public string createdBy { get; set; }
        public string createdDate { get; set; }
    }
}
