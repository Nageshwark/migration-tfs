﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class TTFileInfo
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileContent { get; set; }
        public string BucketName { get; set; }
    }
}
