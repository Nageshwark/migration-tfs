﻿using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.Artirix
{
    public class Holidays
    {
        public int total { get; set; }
        public List<Offer> offers { get; set; }
        public Facets facets { get; set; }
        public int totalHotels { get; set; }


    }

    public class Facets
    {
        public List<FacetHotel> hotels { get; set; }
        public List<Airport> airports { get; set; }
        public Pricerange priceRange { get; set; }
        public List<Resort> resorts { get; set; }
        public int totalHotels { get; set; }
    }

    public class Pricerange
    {
        public int min { get; set; }
        public int max { get; set; }
    }

    public class FacetHotel
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public string hotelKey { get; set; }
        public int count { get; set; }
    }

    public class Airport
    {
        public string name { get; set; }
        public int departureId { get; set; }
        public int count { get; set; }
    }

    public class Resort
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public int count { get; set; }
    }

    public class Offer
    {
        public string id { get; set; }
        public string quoteRef { get; set; }
        public int price { get; set; }
        public bool isPreferential { get; set; }
        public string phone { get; set; }
        public Journey journey { get; set; }
        public Accommodation accommodation { get; set; }
        public OfferHotel hotel { get; set; }
        public string hotelOperator { get; set; }
        public string contentId { get; set; }
        public string tradingNameId { get; set; }
        public object score { get; set; }
        public bool isPreferredByUser { get; set; }
    }

    public class Journey
    {
        public DateTime outboundArrivalDate { get; set; }
        public DateTime inboundDepartureDate { get; set; }
        public DateTime departureDate { get; set; }
        public DateTime returnDate { get; set; }
        public int duration { get; set; }
        public string departure { get; set; }
        public string gatewayName { get; set; }
        public string flightOperator { get; set; }
        public string destination { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public string gatewayCode { get; set; }
        public bool inboundFlightDirect { get; set; }
        public bool outboundFlightDirect { get; set; }
    }

    public class Accommodation
    {
        public string boardTypeCode { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int rating { get; set; }
        public DateTime updated { get; set; }
    }
    
    public class OfferHotel
    {
        public string iff { get; set; }
        public string latlon { get; set; }
        public string starRating { get; set; }
        public List<string> images { get; set; }
        public List<string> mobileimages { get; set; }
        public List<string> thumbnailimages { get; set; }
        public string description { get; set; }
        public List<object> features { get; set; }
        public string name { get; set; }
        public Rating rating { get; set; }
    }

    public class Rating
    {
        public string tripAdvisorId { get; set; }
        public float averageRating { get; set; }
        public int reviewCount { get; set; }
    }

}
