﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Presentation.WCFRestService.Model.AdvancePassenger
{
    [DataContract]
    public class TopDogAdvPassengers
    {
        private List<Passenger> passengers;
        private string surname;
        private string departureDate;
        private string bookingReference;
        private string countrySite;

        [DataMember]
        public string CountrySite
        {
            get { return countrySite; }
            set { countrySite = value; }
        }

        [DataMember]
        public string BookingReference
        {
            get { return bookingReference; }
            set { bookingReference = value; }
        }
        [DataMember]
        public string Surname
        {
            get
            {
                return surname;
            }
            set { surname = value; }
        }
        [DataMember]
        public string DepartureDate
        {
            set
            {
                departureDate = value;
            }
            get { return departureDate; }
        }
        [DataMember]
        public List<Passenger> Passengers
        {
            get
            {
                return passengers;
            }
            set { passengers = value; }
        }
    }

    [DataContract]
    public class Passenger
    {
        private string passengerFullName;
        private string dateOfBirth;
        private string passportNumber;
        private string passportIssuedDate;
        private string placeOfIssue;
        private string redressNumber;
        private string passportExpiryDate;
        private string nationality;
        private bool isLeadPassenger;

        [DataMember]
        public string PassengerFullName
        {
            get { return passengerFullName; }
            set { passengerFullName = value; }
        }
        [DataMember]
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }
        [DataMember]
        public string PassportNumber
        {
            get { return passportNumber; }
            set { passportNumber = value; }
        }
        [DataMember]
        public string PassportIssuedDate
        {
            get
            {
                return passportIssuedDate;
            }
            set { passportIssuedDate = value; }
        }
        [DataMember]
        public string PlaceOfIssue
        {
            get { return placeOfIssue; }
            set { placeOfIssue = value; }
        }
        [DataMember]
        public string RedressNumber
        {
            get
            {
                return redressNumber;
            }
            set { redressNumber = value; }
        }
        [DataMember]
        public string PassportExpiryDate
        {
            get
            {
                return passportExpiryDate;
            }
            set { passportExpiryDate = value; }
        }
        [DataMember]
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        [DataMember]
        public bool IsLeadPassenger
        {
            get { return isLeadPassenger; }
            set { isLeadPassenger = value; }
        }
    }
}
