﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TeleappliantReport
{
    public class SearchObject
    {
        public string SessionId { get; set; }
        public string Platform { get; set; }
        public string SrchSite { get; set; }
        public string SrchDate { get; set; }
        public string SrchVisitorID { get; set; }
        public string SrchDepDate { get; set; }
        public string SrchBoardBasis { get; set; }
        public string SrchRating { get; set; }
        public string SrchDepPoint { get; set; }
        public string SrchNoOfDays { get; set; }
        public string SrchAdults { get; set; }
        public string SrchChildren { get; set; }
        public string SrchChannel { get; set; }
        public string SrchFlex { get; set; }
        public string SrchDest { get; set; }
        public string SrchDestCode { get; set; }
        public int SrchHotels { get; set; }
        public string SearchType { get; set; }
        public string Source { get; set; }
        public string ReferringUrl { get; set; }
        public string ReferringUrlFamily { get; set; }
        public string Type { get; set; }
        public string MobilePlatformOS { get; set; }
        public string MobileBrand { get; set; }
        public string AppVersion { get; set; }
        public string utm_source { get; set; }
        public string utm_medium { get; set; }
        public string utm_campaign { get; set; }
        public string utm_term { get; set; }
        public string utm_content { get; set; }
        public Geolocation Geolocation { get; set; }
    }

    public class Geolocation
    {
        public int lat { get; set; }
        public int log { get; set; }
    }
}
