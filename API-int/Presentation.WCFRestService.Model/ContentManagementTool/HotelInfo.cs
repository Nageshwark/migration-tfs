﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ContentManagementTool
{
    public class MHIDLocation
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }

    public class MHIDAddress
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string AddressLineFour { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }

    public class MHIDContact
    {
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string EMailId { get; set; }
    }

    public class MHIDRegion
    {
        public int RegionId { get; set; }
        public int ResortId { get; set; }
        public string Regname { get; set; }
        public int Reglevel { get; set; }
        public string AirportCodes { get; set; }
        public int ParentRegionId { get; set; }
        public string ParentRegionName { get; set; }
        public string ParentRegionLink { get; set; }
        public int GrandRegionId { get; set; }
        public string GrandRegionName { get; set; }
        public string GrandRegionLink { get; set; }
    }

    //public class Images
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    //public class MobileImages
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    //public class ThumbnailImages
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    public class MHIDSource
    {
        public int MasterHotelId { get; set; }
        public string BuildingName { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
        public int MasterResortId { get; set; }
        public string MasterResortName { get; set; }
        public bool PopularHotel { get; set; }
        public int RatingLevel { get; set; }
        public int Rating { get; set; }
        public string GIATAID { get; set; }
        public string FriendlyUrl { get; set; }
        public int RegionTTSSLabelD { get; set; }
        public MHIDLocation Location { get; set; }
        public MHIDAddress Address { get; set; }
        public MHIDContact Contact { get; set; }
        public MHIDRegion Region { get; set; }
        public List<object> Features { get; set; }
        public int ImageCount { get; set; }
        public string Description { get; set; }
    }

    public class TripAdvisor
    {
        public int TAID { get; set; }
        public int reviewsCount { get; set; }
        public double TARating { get; set; }
        public int noOfRooms { get; set; }
        //public List<string> hotelStyle { get; set; }
    }

    public class MHIDStaticInfo
    {
        public MHIDSource _source { get; set; }
        //public string insertedTime { get; set; }
        public TripAdvisor TripAdvisor { get; set; }
        //public bool Override { get; set; }
        //public bool IsDirty { get; set; }
        //public string ETag { get; set; }
    }
}
