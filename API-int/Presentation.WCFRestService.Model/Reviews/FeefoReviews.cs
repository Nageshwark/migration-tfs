﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class FeefoReviews
    {
        public List<string> products_purchased { get; set; }
        public Merchant merchant { get; set; }

        public FeefoCustomer customer { get; set; }
        public Social social { get; set; }
        public Service service { get; set; }
        public string last_updated_date { get; set; }
        public string last_modified_at { get; set; }
        public string last_modified_date { get; set; }
        public string url { get; set; }
        public bool isPublished { get; set; }
    }

    public class Merchant
    {
        public string identifier { get; set; }
    }

    public class FeefoCustomer
    {
        public string display_name { get; set; }
        public string display_location { get; set; }
    }

    public class Social
    {
        public string twitter { get; set; }
        public string facebook { get; set; }
        public string google_plus { get; set; }
    }

    public class Rating
    {
        public int min { get; set; }
        public int max { get; set; }
        public int rating { get; set; }
    }

    public class Service
    {
        public string id { get; set; }
        public string helpful_votes { get; set; }
        public string title { get; set; }
        public string created_at { get; set; }
        public Rating rating { get; set; }
        public string review { get; set; }
    }
}
