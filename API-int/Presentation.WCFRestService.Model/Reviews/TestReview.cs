﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class TestReview
    {
        public string reviewId { get; set; }
        public string reviewTitle { get; set; }
        public string description { get; set; }
        public int reviewRating { get; set; }
        public CustomerInfo1 customer { get; set; }
        public string postedOn { get; set; }
        public string reviewProviderName { get; set; }
        public bool isPublished { get; set; }
        public string mdCheckSum { get; set; }
    }

    public class CustomerInfo1
    {
        public string postedBy { get; set; }
    }
}
