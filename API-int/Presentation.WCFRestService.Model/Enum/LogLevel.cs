﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Enum
{
    public enum LogLevel
    {
        Critical, //Value: 1. Indicates logs for a critical alert.
        Error,        //Value: 2. Indicates logs for an error.
        Information,        //Value: 4. Indicates logs for an informational message.
        Undefined,        //Value: 0. Indicates logs at all levels.
        Verbose,        //Value: 5. Indicates logs at all levels.
        Warning,        //Value: 3. Indicates logs for a warning.
        ElasticSearch,    // Testing the correctness of elasticseach
        ElasticSearchDiversity,
        MissingElasticSearchMappings,
        RoutingApi,
        RoutingApiError,
        GetRoutingApiError,
        CustomerReviews,
        ElasticSearchAvailability,
        PaySafeStandaloneCredit,
        LogCheapBoardBasis,
        ElasticSearchAvailabilityTesting,
        ElasticSearchMissingAlphaPkIdToIntIdMapping
    };
}
