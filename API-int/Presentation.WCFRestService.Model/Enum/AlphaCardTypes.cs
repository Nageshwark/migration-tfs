﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Enum
{    
    public enum AlphaCardTypes : int
    {
        /// <summary>
        /// The placeholder for unknown card types or invalid cards.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Represents a MasterCard credit card.
        /// </summary>
        [Description("Master Card")]
        MasterCard = 1,

        /// <summary>
        /// Represents a Visa credit card.
        /// </summary>
        [Description("Visa Credit")]
        VisaCredit = 2,

        /// <summary>
        /// Represents an American Express charge card.
        /// </summary>
        [Description("American Express")]
        AmericanExpress = 3,

        /// <summary>
        /// Represents a Diners Club or Carte Blanche charge card.
        /// </summary>
        [Description("Diners Club / Carte Blanche")]
        DinersCarteBlanche = 4,

        /// <summary>
        /// Represents a Discover credit card.
        /// </summary>
        Discover = 5,

        /// <summary>
        /// Represents an Enroute charge card
        /// </summary>
        Enroute = 6,

        /// <summary>
        /// Represents a JCB charge card
        /// </summary>
        JCB = 7,

        /// <summary>
        /// Represents a SWITCH credit card
        /// </summary>
        Switch = 8,

        /// <summary>
        /// Represents a DELTA credit card
        /// </summary>
        Delta = 9,

        /// <summary>
        /// Represents a SOLO credit card
        /// </summary>
        Solo = 10,

        /// <summary>
        /// Represents a Maestro debit card
        /// </summary>
        Maestro = 11,

        /// <summary>
        /// Represents a Laser debit card
        /// </summary>
        Laser = 12,

        /// <summary>
        /// Represents a prepaid master card
        /// </summary>
        [Description("Prepaid Master Card")]
        PrepaidMasterCard = 13,

        /// <summary>
        /// Represents an international meastro card
        /// </summary>
        [Description("International Maestro")]
        InternationalMaestro = 14,

        [Description("Visa Debit")]
        VisaDebit = 15,

        [Description("Visa Delta")]
        VisaDelta = 16,

        [Description("Visa Electron")]
        VisaElectron = 17,

        [Description("Visa Purchasing")]
        VisaPurchasing = 18,

        [Description("Master Card Debit")]
        MastercardDebit = 19,

        [Description("Eurocard")]
        EuroCard = 20,

        [Description("Diners Club")]
        DinersClub = 21,

        [Description("PayPal")]
        PayPal = 22,
    }
}
