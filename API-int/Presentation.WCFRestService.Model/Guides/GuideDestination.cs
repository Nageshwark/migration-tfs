﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Guides
{

    public class GuideDestination
    {
        [JsonProperty("bgimage")]
        public string bgimage { get; set; }        
        public string bgimage_m { get; set; }        
        public string bgimage_t { get; set; }
        public string bgimage_sd { get; set; }
        public string headerImgAltTag { get; set; }
        public string headerImgTitleTag { get; set; }
        [JsonProperty("ptitle")]
        public string ptitle { get; set; }
        [JsonProperty("pmetadesc")]
        public string pmetadesc { get; set; }
        public string sectionid { get; set; }
        [JsonProperty("pmetakeyword")]
        public string pmetakeyword { get; set; }
        [JsonProperty("regid")]
        public string regid { get; set; }
        [JsonProperty("regname")]
        public string regname { get; set; }
        [JsonProperty("reglevel")]
        public string reglevel { get; set; }
        [JsonProperty("showGuideOnMobile")]
        public bool showGuideOnMobile { get; set; }
        [JsonProperty("showLastMinuteDeals")]
        public bool showLastMinuteDeals { get; set; }
        [JsonProperty("recommendedhotels")]
        public List<RecommendedHotel> recommendedhotels { get; set; }
        [JsonProperty("parent_regid")]
        public string parent_regid { get; set; }
        [JsonProperty("parent_regname")]
        public string parent_regname { get; set; }
        [JsonProperty("parent_link")]
        public string parent_link { get; set; }
        [JsonProperty("grand_regid")]
        public string grand_regid { get; set; }
        [JsonProperty("grand_regname")]
        public string grand_regname { get; set; }
        [JsonProperty("grand_reglink")]
        public string grand_reglink { get; set; }
        [JsonProperty("resortid")]
        public string resortid { get; set; }
        [JsonProperty("destcode")]
        public string destcode { get; set; }
        [JsonProperty("link_summary")]
        public List<link_summary> link_summary { get; set; }
        [JsonProperty("sections")]
        public List<section> sections { get; set; }
        [JsonProperty("weather")]
        public List<weather> weather { get; set; }
        [JsonProperty("otherguides")]
        public List<OtherGuides> otherguides { get; set; }
        public List<ExpertView> expertviews { get; set; }
        public List<MetaTag> metatags { get; set; }
        [JsonProperty("highSeasonalAverage")]
        public string highSeasonalAverage { get; set; }
        public string dropDownLabelName { get; set; }
        public List<GuideVideos> guideVideos { get; set; }
    }
    public class OtherGuides
    {
        [JsonProperty("gname")]
        public string gname { get; set; }
        [JsonProperty("gimg")]
        public string gimg { get; set; }
        [JsonProperty("glink")]
        public string glink { get; set; }
    }
    public class weather
    {
        [JsonProperty("temperature")]
        List<Temperature> temparature { get; set; }
        [JsonProperty("rain")]
        List<Rain> rain { get; set; }
    }
    public class Temperature
    {
        [JsonProperty("month")]
        public string month { get; set; }
        [JsonProperty("destinationValue")]
        public string destinationValue { get; set; }
        [JsonProperty("homeValue")]
        public string homeValue { get; set; }
    }
    public class Rain
    {
        [JsonProperty("month")]
        public string month { get; set; }
        [JsonProperty("destinationValue")]
        public string destinationValue { get; set; }
        [JsonProperty("homeValue")]
        public string homeValue { get; set; }
    }
    public class section
    {
        [JsonProperty("sname")]
        public string sname { get; set; }
        [JsonProperty("sheader")]
        public string sheader { get; set; }
        [JsonProperty("surl")]
        public string surl { get; set; }
        [JsonProperty("shortdesc")]
        public string shortdesc { get; set; }
        [JsonProperty("shortdescIE")]
        public string shortdescIE { get; set; }
        [JsonProperty("atglance")]
        public List<atglance> atglance { get; set; }
        [JsonProperty("subsections")]
        public List<subsections> subsections { get; set; }

        [JsonProperty("lat")]
        public string lat { get; set; }
        [JsonProperty("lon")]
        public string lon { get; set; }
        [JsonProperty("zoom")]
        public string zoom { get; set; }
    }

    public class subsections
    {
        [JsonProperty("subheader")]
        public string subheader { get; set; }
        [JsonProperty("thumbnailImageurl")]
        public string thumbnailImageurl { get; set; }
        [JsonProperty("imageurl")]
        public string imageurl { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
        public string subsurl { get; set; }
        [JsonProperty("transport")]
        public List<transport> transport { get; set; }

    }
    public class atglance
    {
        [JsonProperty("Key")]
        public string Key { get; set; }
        [JsonProperty("Value")]
        public string Value { get; set; }
    }
    public class transport
    {
        [JsonProperty("Mode")]
        public string Mode { get; set; }
        [JsonProperty("Content")]
        public string Content { get; set; }
    }
    public class link_summary
    {
        [JsonProperty("lname")]
        public string lname { get; set; }
        [JsonProperty("guideId")]
        public string guideId { get; set; }
        public string bgimage_m { get; set; }
        [JsonProperty("lurl")]
        public string lurl { get; set; }
        public string sectionid { get; set; }
        [JsonProperty("placestogo")]
        public section PlacesToGo { get; set; }
        [JsonProperty("recommendedhotels")]
        public List<RecommendedHotel> recommendedhotels { get; set; }
    }

    public class CalledDestination
    {
        public string id { get; set; }
        public string name { get; set; }
        public string linkurl { get; set; }
        public string destcodes { get; set; }
        public string resortcode { get; set; }
    }

    public class CalledRootDestination
    {
        public List<CalledDestination> destinations { get; set; }
    }

    public class RecommendedHotel
    {
        public string queryid { get; set; }
        public string tripAdvisorrating { get; set; }
        public int reviewscount { get; set; }
        public bool isTAoverride { get; set; }
        public string callouttext { get; set; }
        public string discount { get; set; }
    }

    public class GuideVideos
    {
        public string VideoUrl { get; set; }
        public string VideoThumbnail { get; set; }
        public string VideoDesc { get; set; }
        public string VideoTitle { get; set; }
    }

    public class ExpertView
    {
        public string title { get; set; }
        public string opinion { get; set; }
        public string name { get; set; }
        public string designation { get; set; }
        public string dateposted { get; set; }
    }

    public class MetaTag
    {
        public string name { get; set; }
        public string content { get; set; }
    }
}
