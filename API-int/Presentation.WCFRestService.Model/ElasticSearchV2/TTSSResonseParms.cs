﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class TTSSResonseParms
    {
        public string Status { get; set; }
        public int Count { get; set; }
        public List<Result> Results { get; set; }
        //public Search Search { get; set; }
    }
    public class Result
    {
        public int destinationId { get; set; }
        public string tradingNameId { get; set; }
        public int duration { get; set; }
        public string departureId { get; set; }
        public int boardTypeId { get; set; }
        public string boardTypeCode { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string rating { get; set; }
        public int price { get; set; }
        public string accommodation { get; set; }
        public string updated { get; set; }
        public string accommodationUpdateTime { get; set; }
        public string rank { get; set; }
        public string outboundFlightDirect { get; set; }
        public string inboundFlightDirect { get; set; }
        public string destination { get; set; }
        public string glong { get; set; }
        public string glat { get; set; }
        public string hotelOperator { get; set; }
        public string contentId { get; set; }
        public string hotelKey { get; set; }
        public string quoteRef { get; set; }
        public string id { get; set; }
        public string outboundDepartureDate { get; set; }
        public string outboundArrivalDate { get; set; }
        public string outboundDepartureAirportName { get; set; }
        public string outboundDepartureAirportCode { get; set; }
        public string outboundArrivalAirportName { get; set; }
        public string outboundArrivalAirportCode { get; set; }
        public string outboundLegCount { get; set; }
        public string outboundFlightDuration { get; set; }
        public string outboundFlightNumber { get; set; }
        public string outboundAirlineICAO { get; set; }
        public string inboundDepartureDate { get; set; }
        public string inboundArrivalDate { get; set; }
        public string inboundDepartureAirportName { get; set; }
        public string inboundDepartureAirportCode { get; set; }
        public string inboundArrivalAirportName { get; set; }
        public string inboundArrivalAirportCode { get; set; }
        public string inboundLegCount { get; set; }
        public string inboundFlightDuration { get; set; }
        public string inboundFlightNumber { get; set; }
        public string inboundAirlineICAO { get; set; }
        public string adultBagPrice { get; set; }
        public string childBagPrice { get; set; }
        public int alternativeTotalPrice { get; set; }
        public string alternativeOutboundDepartureDate { get; set; }
        public string alternativeOutboundArrivalDate { get; set; }
        public string alternativeOutboundDepartureAirportName { get; set; }
        public string alternativeOutboundDepartureAirportCode { get; set; }
        public string alternativeOutboundArrivalAirportName { get; set; }
        public string alternativeOutboundArrivalAirportCode { get; set; }
        public string alternativeOutboundLegCount { get; set; }
        public string alternativeOutboundFlightDuration { get; set; }
        public string alternativeOutboundFlightNumber { get; set; }
        public string alternativeOutboundAirlineICAO { get; set; }
        public string alternativeInboundDepartureDate { get; set; }
        public string alternativeInboundArrivalDate { get; set; }
        public string alternativeInboundDepartureAirportName { get; set; }
        public string alternativeInboundDepartureAirportCode { get; set; }
        public string alternativeInboundArrivalAirportName { get; set; }
        public string alternativeInboundArrivalAirportCode { get; set; }
        public string alternativeInboundLegCount { get; set; }
        public string alternativeInboundFlightDuration { get; set; }
        public string alternativeInboundFlightNumber { get; set; }
        public string alternativeInboundAirlineICAO { get; set; }
        public string alternativeAdultBagPrice { get; set; }
        public string alternativechildBagPrice { get; set; }
    }

    public class Search
    {
        public string tradingNameId { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public string durationMin { get; set; }
        public string durationMax { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string destinationId { get; set; }
        public double timeout { get; set; }
        public int boardBasisId { get; set; }
        public string departureId { get; set; }
    }
    public class TTSSResponseStream
    {
        public string ttssResponse { get; set; }
        public double ttssResponseTime { get; set; }

    }
}
