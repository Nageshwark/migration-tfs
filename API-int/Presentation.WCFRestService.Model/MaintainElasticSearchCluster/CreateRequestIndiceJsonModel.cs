﻿namespace Presentation.WCFRestService.Model.MaintainElasticSearchCluster
{
    public class CreateRequestIndiceJsonModel
    {
        public string ElasticSearchUserName { get; set; }
        public string ElasticSearchPassword { get; set; }
        public string IntEndPoint { get; set; }
        public string ProdEndPoint { get; set; }
        public bool SendStatusEmailForInt { get; set; }
        public bool SendStatusEmailForProd { get; set; }
        public string RequestBody { get; set; }
        public string IndicePrefix { get; set; }
        public string IndexPattern { get; set; }
        public string ToEmail { get; set; }
        public string FromEmail { get; set; }
        public string SuccessfulSubjectForInt { get; set; }
        public string FailureSubjectForInt { get; set; }
        public string SuccessfulSubjectForProd { get; set; }
        public string FailureSubjectForProd { get; set; }
        public int ConnectTimeout { get; set; }
        public int ReadTimeout { get; set; }
        public string IndiceName { get; set; }
    }
}
