﻿using Newtonsoft.Json;

namespace Presentation.WCFRestService.Model.MaintainElasticSearchCluster
{
    public class IndiceModel
    {
        [JsonProperty(PropertyName = "health")]
        public string Health { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "index")]
        public string Index { get; set; }
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }
        [JsonProperty(PropertyName = "pri")]
        public string Primary { get; set; }
        [JsonProperty(PropertyName = "rep")]
        public string ReplicaShards { get; set; }
        [JsonProperty(PropertyName = "docs.count")]
        public string DocsCount { get; set; }
        [JsonProperty(PropertyName = "docs.deleted")]
        public string DocsDeleted { get; set; }
        [JsonProperty(PropertyName = "store.size")]
        public string StoreSize { get; set; }
        [JsonProperty(PropertyName = "pri.store.size")]
        public string PrimaryStoreSize { get; set; }
    }
}
