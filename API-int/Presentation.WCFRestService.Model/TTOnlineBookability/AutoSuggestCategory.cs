﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public enum AutoSuggestCategory
    {
        Destinations = 1,
        Countries = 2,
        Hotels = 3,
        Airports = 4,
        DepartureAirports = 5,
        AirportsWithParking = 6
    }
}
