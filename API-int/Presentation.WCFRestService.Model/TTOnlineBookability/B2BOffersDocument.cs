﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class B2BOffersDocument
    {
        public string hotelCode { get; set; }
        public int hotelId { get; set; }
        public string hotelCity { get; set; }
        public string hotelName { get; set; }
        public string defaultAirportCode { get; set; }
        public string destinationCode { get; set; }
        public int destinationId { get; set; }
        public string destinationName { get; set; }
        public int parentDestinationId { get; set; }
        public string parentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string countryName { get; set; }
        public int rank { get; set; }
        public int mhid { get; set; }
        public int starRating { get; set; }
        public string tripAdvisorId { get; set; }
        public float averageRating { get; set; }
        public int reviewCount { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string roomStayCandidateRPH { get; set; }
        public string roomId { get; set; }
        public string boardType { get; set; }
        public string boardTypeCode { get; set; }
        public string roomDescription { get; set; }
        public double basePrice { get; set; }
        public double commission { get; set; }
        public double vat { get; set; }
        public double totalPrice { get; set; }
        public string currency { get; set; }
        public bool isBidingRate { get; set; }
        public bool isDirectPayment { get; set; }
        public bool IsOpaqueRate { get; set; }
        public bool IsRefundableRate { get; set; }
        public bool IsSpecialRate { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public string timeStamp { get; set; }
    }
}
