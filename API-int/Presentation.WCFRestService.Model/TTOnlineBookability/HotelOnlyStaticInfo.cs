﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class StaticEstablishmentParams
    {
        public Guid establishmentCode { get; set; }
        public int establishmentIntId { get; set; }
        public string establishmentName { get; set; }
        public string defaultAirportCode { get; set; }
        public int destinationId { get; set; }
        public int parentDestinationId { get; set; }
        public string parentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string countryName { get; set; }
        public string tripAdvisorId { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string description { get; set; }
        public Dictionary<string,List<string>> establishmentFacilities { get; set; }
        public Dictionary<string, List<string>> roomFacilities { get; set; }
        public List<NearestAttaraction> nearestAttractions { get; set; }
        public AirportInformation nearestAirport { get; set; }
    }
    public class Facility
    {
        public string facilityType { get; set; }
        public string categoryName { get; set; }
        public object skills { get; set; }
    }
    public class AirportInformation
    {
        public string duration { get; set; }
        public string distance { get; set; }
        public string airportName { get; set; }
    }
    public class NearestAttaraction
    {
        public string attractionName { get; set; }
        public string distanceToAttraction { get; set; }
    }

    public class DestinationLevelEssentialInformation
    {
        public string destinationCode { get; set; }
        public int destinationId { get; set; }
        public string destinationName { get; set; }
        public string timezone { get; set; }
        public string plugType { get; set; }
        public string currencySymbol { get; set; }
        public string currencyName { get; set; }
    }

    public class EssentialInformation
    {
        public DestinationLevelEssentialInformation destinationLevelEssentialInfo { get; set; }
        public List<NearestAttaraction> nearestAttractions { get; set; }
        public AirportInformation nearestAirport { get; set; }
    }
}
