﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    [XmlRoot(ElementName = "RequestorID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RequestorID
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlAttribute(AttributeName = "MessagePassword")]
        public string MessagePassword { get; set; }
    }

    [XmlRoot(ElementName = "Source", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class SourceObj
    {
        [XmlElement(ElementName = "RequestorID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RequestorID RequestorID { get; set; }
    }

    [XmlRoot(ElementName = "POS", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class POS
    {
        [XmlElement(ElementName = "Source", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public SourceObj Source { get; set; }
    }

    [XmlRoot(ElementName = "HotelRef", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelRef
    {
        [XmlAttribute(AttributeName = "AreaID")]
        public string AreaID { get; set; }
        [XmlAttribute(AttributeName = "HotelCode")]
        public string HotelCode { get; set; }
    }

    [XmlRoot(ElementName = "StayDateRange", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class StayDateRange
    {
        [XmlAttribute(AttributeName = "Start")]
        public string Start { get; set; }
        [XmlAttribute(AttributeName = "End")]
        public string End { get; set; }
    }

    [XmlRoot(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCount
    {
        [XmlAttribute(AttributeName = "AgeQualifyingCode")]
        public string AgeQualifyingCode { get; set; }
        [XmlAttribute(AttributeName = "Count")]
        public string Count { get; set; }
        [XmlAttribute(AttributeName = "Age")]
        public string Age { get; set; }
    }

    [XmlRoot(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCounts
    {
        [XmlElement(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<GuestCount> GuestCount { get; set; }
    }

    [XmlRoot(ElementName = "RoomStayCandidate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStayCandidate
    {
        [XmlElement(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuestCounts GuestCounts { get; set; }
        [XmlAttribute(AttributeName = "RPH")]
        public string RPH { get; set; }
    }

    [XmlRoot(ElementName = "RoomStayCandidates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStayCandidates
    {
        [XmlElement(ElementName = "RoomStayCandidate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<RoomStayCandidate> RoomStayCandidate { get; set; }
    }

    [XmlRoot(ElementName = "HotelAvailRQAdditionalData", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelAvailRQAdditionalData
    {
        [XmlElement(ElementName = "IsDirectPayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsDirectPayment { get; set; }
        [XmlElement(ElementName = "IsBidingRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsBidingRate { get; set; }
        [XmlElement(ElementName = "IsOpaqueRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsOpaqueRate { get; set; }
        [XmlElement(ElementName = "IsRefundableRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsRefundableRate { get; set; }
        [XmlElement(ElementName = "IsSpecialRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsSpecialRate { get; set; }
    }

    [XmlRoot(ElementName = "TPA_Extensions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TPA_Extensions
    {
        [XmlElement(ElementName = "HotelAvailRQAdditionalData", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public HotelAvailRQAdditionalData HotelAvailRQAdditionalData { get; set; }
    }

    [XmlRoot(ElementName = "Criterion", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Criterion
    {
        [XmlElement(ElementName = "HotelRef", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<HotelRef> HotelRef { get; set; }
        [XmlElement(ElementName = "StayDateRange", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public StayDateRange StayDateRange { get; set; }
        [XmlElement(ElementName = "RoomStayCandidates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomStayCandidates RoomStayCandidates { get; set; }
        [XmlElement(ElementName = "TPA_Extensions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TPA_Extensions TPA_Extensions { get; set; }
    }

    [XmlRoot(ElementName = "HotelSearchCriteria", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelSearchCriteria
    {
        [XmlElement(ElementName = "Criterion", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Criterion Criterion { get; set; }
    }

    [XmlRoot(ElementName = "AvailRequestSegment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AvailRequestSegment
    {
        [XmlElement(ElementName = "HotelSearchCriteria", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public HotelSearchCriteria HotelSearchCriteria { get; set; }
    }

    [XmlRoot(ElementName = "AvailRequestSegments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AvailRequestSegments
    {
        [XmlElement(ElementName = "AvailRequestSegment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AvailRequestSegment AvailRequestSegment { get; set; }
    }

    [XmlRoot(ElementName = "OTA_HotelAvailRQ", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class OTA_HotelAvailRQ
    {
        [XmlElement(ElementName = "POS", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public POS POS { get; set; }
        [XmlElement(ElementName = "AvailRequestSegments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AvailRequestSegments AvailRequestSegments { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "Version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "PrimaryLangID")]
        public string PrimaryLangID { get; set; }
        [XmlAttribute(AttributeName = "Target")]
        public string Target { get; set; }
    }
}