﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class B2CAvailabilitySearchRequestAvailability
    {
        public int availabilityOffersCount { get; set; }
        public Guid availabilityId { get; set; }
    }
}
