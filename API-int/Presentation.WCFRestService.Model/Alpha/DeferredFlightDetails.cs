﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Alpha
{
    public class DeferredFlightDetails
    {
        public string ItineraryReference { get; set; }
        public string EmailAddress { get; set; }
        public string OutBoundFlightNumber { get; set; }
        public string OutboundFlightArrivalHour { get; set; }
        public string OutboundFlightArrivalMin { get; set; }
        public string InboundFlightNumber { get; set; }
        public string InboundFlightDepartureHour { get; set; }
        public string InboundFlightDepartureMin { get; set; }
        public string Channel { get; set; }
    }
}
