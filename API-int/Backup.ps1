Copy-Item D:\websites\Dummy\bin D:\websites\NewBackup\bin -Recurse -force
Copy-Item D:\websites\Dummy\data D:\websites\NewBackup\data -Recurse -force
Copy-Item D:\websites\Dummy\Web.config D:\websites\NewBackup\Web.config -Recurse -force
Copy-Item D:\websites\Dummy\Global.asax D:\websites\NewBackup\Global.asax -Recurse -force

Copy-Item D:\websites\Test1\Presentation.Web.WCFRestServices\bin D:\websites\Dummy\bin -Recurse -force
Copy-Item D:\websites\Test1\Presentation.Web.WCFRestServices\data D:\websites\Dummy\data -Recurse -force
Copy-Item D:\websites\Test1\Presentation.Web.WCFRestServices\Web.config  D:\websites\Dummy\Web.config -Recurse -force
Copy-Item D:\websites\Test1\Presentation.Web.WCFRestServices\Global.asax  D:\websites\Dummy\Global.asax -Recurse -force