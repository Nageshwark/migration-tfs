﻿using Newtonsoft.Json;
using NUnit.Framework;
using Presentation.WCFRestService.Model.Misc;
using Presentation.Web.WCFRestServices;
using System.Net;
using System.ServiceModel.Web;

namespace Presentation.Web.WCFRestService.Tests.Core_Tests
{
    /// <summary>
    /// Core Tests
    /// </summary>
    [TestFixture]
    public class BusinessLogicTests
    {
        // Opted for Email
        [TestCase(1, "{\"firstName\":\"Teletext Test 1\",\"surName\":null,\"emailAddress\":\"test1@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":null,\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":true,\"optForSMS\":false,\"optForPost\":false,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email & SMS
        [TestCase(2, "{\"firstName\":\"Teletext Test 2\",\"surName\":null,\"emailAddress\":\"test2@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":true,\"preferredAirport\":\"Any London\",\"contactNumber\":\"+44-9659845456\",\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":false,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email, SMS & Post
        [TestCase(3, "{\"firstName\":\"Teletext Test 3\",\"surName\":null,\"emailAddress\":\"test3@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":\"+44-9659845456\",\"boardBasis\":null,\"addressLineOne\":\"Test Address Line One\",\"town\":\"Test Town\",\"city\":\"Test City\",\"county\":\"Test County\",\"postCode\":\"Test Post Code\",\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":true,\"channel\":\"uk\", \"source\":\"website\"}")]
        public void MarketService_PostNewsletterSubscriptionTest(int testcase, string newsletterSubscription)
        {
            NewsletterSubscriptionModel newsletterSubscriptionObj = JsonConvert.DeserializeObject<NewsletterSubscriptionModel>(newsletterSubscription);
            Assert.IsTrue(MarketingService.SaveNewsletterSubscriptionToS3(newsletterSubscriptionObj));
        }

        // All values are null
        [TestCase(arg1: 1, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":null,\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":null,\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":false,\"optForSMS\":false,\"optForPost\":false,\"channel\":null, \"source\":null}")]
        // First Name is available
        [TestCase(arg1: 2, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":null,\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":null,\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":false,\"optForSMS\":false,\"optForPost\":false,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email -> but emailAddress is NULL
        [TestCase(arg1: 3, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":null,\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":null,\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":true,\"optForSMS\":false,\"optForPost\":false,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email & SMS -> but SMS is NULL
        [TestCase(arg1: 4, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":\"test@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":null,\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":false,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email, SMS & Post -> but Address details are NULL
        [TestCase(arg1: 5, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":\"test@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":\"+44-9659845456\",\"boardBasis\":null,\"addressLineOne\":null,\"town\":null,\"city\":null,\"county\":null,\"postCode\":null,\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":true,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email, SMS & Post
        [TestCase(arg1: 6, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":\"test@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":\"+44-9659845456\",\"boardBasis\":null,\"addressLineOne\":\"Test Address Line One\",\"town\":\"Test Town\",\"city\":\"Test City\",\"county\":\"Test County\",\"postCode\":\"Test Post Code\",\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":true,\"channel\":\"uk\", \"source\":\"website\"}")]
        // Opted for Email, SMS & Post -> but channel is null
        [TestCase(arg1: 7, arg2: "{\"firstName\":\"Teletext Test\",\"surName\":null,\"emailAddress\":\"test@teletext-holidays.co.uk\",\"optedPersonalisedDeals\":false,\"preferredAirport\":null,\"contactNumber\":\"+44-9659845456\",\"boardBasis\":null,\"addressLineOne\":\"Test Address Line One\",\"town\":\"Test Town\",\"city\":\"Test City\",\"county\":\"Test County\",\"postCode\":\"Test Post Code\",\"optForEmail\":true,\"optForSMS\":true,\"optForPost\":true,\"channel\":null, \"source\":\"website\"}")]
        public void MarketService_MandatoryParameterCheckForNewsletterSubscription_Test(int testcase, string newsletterSubscription)
        {
            NewsletterSubscriptionModel newsletterSubscriptionObj = JsonConvert.DeserializeObject<NewsletterSubscriptionModel>(newsletterSubscription);

            // Unit test the First Name
            //Assert.IsFalse(string.IsNullOrWhiteSpace(newsletterSubscriptionObj.firstName));
            WebFaultException<string> ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("First Name cannot be Null or Empty.", HttpStatusCode.BadRequest); });
            Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(ex.Detail, Is.EqualTo("First Name cannot be Null or Empty."));

            ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Channel cannot be Null or Empty.", HttpStatusCode.BadRequest); });
            Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(ex.Detail, Is.EqualTo("Channel cannot be Null or Empty."));

            ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Source cannot be Null or Empty.", HttpStatusCode.BadRequest); });
            Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(ex.Detail, Is.EqualTo("Source cannot be Null or Empty."));

            // Unit test the scenario where Email is selected as the subcription Model
            if (newsletterSubscriptionObj.optForEmail)
            {
                //Assert.IsFalse(string.IsNullOrWhiteSpace(newsletterSubscriptionObj.emailAddress));
                ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Email Address cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                Assert.That(ex.Detail, Is.EqualTo("Email Address cannot be Null or Empty."));
            }
            if (newsletterSubscriptionObj.optForSMS)
            {
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.contactNumber))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Contact Number cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("Contact Number cannot be Null or Empty."));
                }
            }
            if (newsletterSubscriptionObj.optForPost)
            {
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.addressLineOne))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Address cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("Address cannot be Null or Empty."));
                }
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.town))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Town cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("Town cannot be Null or Empty."));
                }
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.city))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("City cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("City cannot be Null or Empty."));
                }
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.county))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("County cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("County cannot be Null or Empty."));
                }
                if (string.IsNullOrWhiteSpace(newsletterSubscriptionObj.postCode))
                {
                    ex = Assert.Throws<WebFaultException<string>>(() => { throw new WebFaultException<string>("Post Code cannot be Null or Empty.", HttpStatusCode.BadRequest); });
                    Assert.That(ex.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
                    Assert.That(ex.Detail, Is.EqualTo("Post Code cannot be Null or Empty."));
                }
            }
        }
    }
}
