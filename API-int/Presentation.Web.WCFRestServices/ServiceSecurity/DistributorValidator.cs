﻿using Jose;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Configuration;
using System.Web;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class DistributorValidator : ServiceAuthorizationManager
    {
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            var action = OperationContext.Current.IncomingMessageProperties["HttpOperationName"] as string;
            if (!string.IsNullOrEmpty(action))
            {
                if (!RestrictAccess.RestrictedMethods().Any(q => q.Equals(action, StringComparison.InvariantCultureIgnoreCase)))
                    return true;
            }
            var token = WebOperationContext.Current.IncomingRequest.Headers["Accept"];
            if (action.Equals("SendAPPLink", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!token.Equals("*/*", StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        var domain = HttpContext.Current.Request.UrlReferrer.Authority;
                        ErrorLogger.Log(string.Format("{0}{1}", "Domain", domain), WCFRestService.Model.Enum.LogLevel.Information);
                        var allowedDomains = ConfigurationManager.AppSettings["allowedDomains"].Split(',');
                        var isDomanMapped = (from d in allowedDomains where d.Contains(domain) select d).Any();
                        ErrorLogger.Log(string.Format("{0}{1}", "Url Referrer",DateTime.Now.ToString()), WCFRestService.Model.Enum.LogLevel.Information);
                        ErrorLogger.Log(string.Join(",", allowedDomains), WCFRestService.Model.Enum.LogLevel.Information);
                        if (isDomanMapped && ValidateDomainToken(token))
                        {
                            ErrorLogger.Log(string.Format("{0}{1}", "Valid Domain", isDomanMapped), WCFRestService.Model.Enum.LogLevel.Information);
                            return true;
                        }
                        else
                        {
                            ErrorLogger.Log(string.Format("{0}","Not a White Listed Domain"), WCFRestService.Model.Enum.LogLevel.Information);
                            return true;
                        }
                    }
                    catch (Exception exception)
                    {
                        var trace = exception.Message;
                        ErrorLogger.Log(string.Format("{0}", trace), WCFRestService.Model.Enum.LogLevel.Information);
                        return true;
                    }
                }
                else
                {
                    ErrorLogger.Log(string.Format("{0}{1}", "Token Equals to */*","Not a valid token"), WCFRestService.Model.Enum.LogLevel.Information);
                    return true;
                }
            }
            else
            {
                if ((token != null) && (token != string.Empty))
                {
                    if (!token.Equals("*/*", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var svcCredentials = string.Empty;
                        byte[] secretKey = Common.Base64UrlDecode(Common.SignedKey);
                        try
                        {
                            svcCredentials = JWT.Decode(token, secretKey);
                        }
                        catch (Exception ex) { var trace = ex.Message; }
                        if (!string.IsNullOrEmpty(svcCredentials))
                        {
                            var info = JsonConvert.DeserializeObject<Payload>(svcCredentials);
                            var dw = new DwRepository();
                            if (info != null)
                            {
                                if (dw.CheckUser(info.user)) return true;
                                else
                                    return true;
                            }
                            else
                                return true;
                        }
                        else
                            return true;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
        }

        private bool ValidateDomainToken(string token)
        {
            byte[] secretKey = Common.Base64UrlDecode(Common.SignedKey);
            var validateToken = JWT.Decode(token, secretKey);
            if (!string.IsNullOrEmpty(validateToken))
            {
                var info = JsonConvert.DeserializeObject<Payload>(validateToken);
                var dw = new DwRepository();
                if (info != null)
                {
                    if (dw.IsTokenExpired(info.iat))
                    {
                        ErrorLogger.Log(string.Format("{0}","Token was not expired" ), WCFRestService.Model.Enum.LogLevel.Information);
                        return true;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        }
    }
}