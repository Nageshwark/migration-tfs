﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class StaticInfoBuilder
    {
        public static List<NearestAttaraction> GetNearestHotelAttractions(string hotelCode)
        {
            List<NearestAttaraction> nearestAttractions = new List<NearestAttaraction>();
            StaticEstablishmentParams staticHotelInfo = new StaticEstablishmentParams();

            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId;

            string redisStaticInfo = string.Empty;

            if(alphaPkIdToIntId.ContainsKey(hotelCode.ToUpper()))
            {
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                     ConfigurationManager.AppSettings["RedisPort"]))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
                        redisStaticInfo = redisClient.GetValue(alphaPkIdToIntId[hotelCode.ToUpper()].ToString());

                        if (!string.IsNullOrWhiteSpace(redisStaticInfo))
                        {
                            staticHotelInfo = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo);
                            if(staticHotelInfo.nearestAttractions != null)
                            {
                                nearestAttractions = staticHotelInfo.nearestAttractions;
                            }                            
                        }
                    }
                }
            }
            return nearestAttractions;
        }

        public static DestinationLevelEssentialInformation GetDestinationEssentialInformation(string destinationCode)
        {
            DestinationLevelEssentialInformation essentialInfo = new DestinationLevelEssentialInformation();
            string redisStaticInfo = string.Empty;

            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                     ConfigurationManager.AppSettings["RedisPort"]))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDestinationLevelEssentialStaticInfoDb"]);
                    redisStaticInfo = redisClient.GetValue(destinationCode);

                    if (!string.IsNullOrWhiteSpace(redisStaticInfo))
                    {
                        essentialInfo = JsonConvert.DeserializeObject<DestinationLevelEssentialInformation>(redisStaticInfo);
                    }
                }
            }

            return essentialInfo;
        }

        public static AirportInformation GetNearestAirportInformation(string hotelCode)
        {
            AirportInformation airportInfo = new AirportInformation();
            StaticEstablishmentParams staticHotelInfo = new StaticEstablishmentParams();
            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId;

            string redisStaticInfo = string.Empty;

            if (alphaPkIdToIntId.ContainsKey(hotelCode.ToUpper()))
            {
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                     ConfigurationManager.AppSettings["RedisPort"]))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
                        redisStaticInfo = redisClient.GetValue(alphaPkIdToIntId[hotelCode.ToUpper()].ToString());

                        if (!string.IsNullOrWhiteSpace(redisStaticInfo))
                        {
                            staticHotelInfo = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo);
                            if (staticHotelInfo.nearestAttractions != null)
                            {
                                airportInfo = staticHotelInfo.nearestAirport;
                            }
                        }
                    }
                }
            }
            return airportInfo;
        }
    }
}