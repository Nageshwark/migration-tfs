﻿using Presentation.WCFRestService.Model;
using System.Collections.Generic;

namespace Presentation.Web.WCFRestServices
{
    public class Teletext
    {
        public static List<Slider> GetHomepageSlider()
        {
            return TeletextBL.GetHomepageSlider();
        }

        public static OTP GetCustomerAuthOTP(string mobileNo)
        {
            return TeletextBL.GetCustomerAuthOTP(mobileNo);
        }

        public static AuthStatus AuthCustomerOTP(string mobileNo, string otpr)
        {
            return TeletextBL.AuthCustomerOTP(mobileNo, otpr);
        }
    }
}