﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.DynamicSEO
{
    public class Category
    {
        public int flexibilityDuration { get; set; }

        public int dateRolling { get; set; }

        public string categoryTitle { get; set; }

        public string categoryId { get; set; }

        public string boardBasis { get; set; }

        public int duration { get; set; }

        public string starRating { get; set; }

        public string airport { get; set; }

        public int adults { get; set; }

        public string channel { get; set; }

        public int children { get; set; }

        public bool isDirty { get; set; }

        public string destLabel { get; set; }

        public List<FooterLink> footerLinks { get; set; }
    }
}
