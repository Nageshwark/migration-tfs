﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model;

namespace Presentation.Web.WCFRestServices
{
    interface IMarketingService
    {
        NewsLetterSubscriptionStatus GetNewsLetterSubscription(string name, string email, string phoneNumber, string airport);
        string GetNewsLetterSubscriberByEmail(string email);
    }
}


