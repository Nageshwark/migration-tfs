﻿using Presentation.WCFRestService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices
{
    public class SmartFocusServices : IMarketingService
    {
        public NewsLetterSubscriptionStatus GetNewsLetterSubscription(string name, string email, string phoneNumber, string airport)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            SmartFocusServicesBL smartFocus = new SmartFocusServicesBL();
            smartFocus.OpenConnection();
            string[] namePieces = name.Split(' ');
            status.JobId = smartFocus.InsertUpdateMemberSubscription(namePieces[0], name.Remove(0, namePieces[0].Length).Trim(), email, phoneNumber, airport);
            //status.Status = smartFocus.GetJobStatus(status.JobId);
            status.Status = true;
            smartFocus.CloseConnection();
            return status;
        }

        public string GetNewsLetterSubscriberByEmail(string email)
        {
            SmartFocusServicesBL smartFocus = new SmartFocusServicesBL();
            smartFocus.OpenConnection();
            string result = smartFocus.GetMemberByEmail(email);
            smartFocus.CloseConnection();
            return result;
        }
    }   
}