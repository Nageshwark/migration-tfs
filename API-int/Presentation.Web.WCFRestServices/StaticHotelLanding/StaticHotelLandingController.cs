﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.Misc;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using Presentation.WCFRestService.Model.Enum;
using System.ServiceModel.Web;
using Presentation.WCFRestService.Model.StaticHotelLanding;
using System.Text.RegularExpressions;
using Presentation.WCFRestService.Model;
using ServiceStack.Redis;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using System.Globalization;
using Amazon.S3;
using Presentation.Web.WCFRestServices.ElasticSearchV2;
using Presentation.WCFRestService.Model.Guides;

namespace Presentation.Web.WCFRestServices.StaticHotelLanding
{
    public class StaticHotelLandingController
    {
        public static StaticMasterHotelInfo GetHotelInfoByUrl(string url)
        {
            try
            {
                string masterHotelId = string.Empty;
                string cacheTime = string.Empty;
                MHIDStaticInfo mhidStaticInfo = null;
                StaticMasterHotelInfo staticMHInfo = null;
                int SHORT_DESCRIPTION_MAX_LENGTH = 200;
                List<OverseasDestination> overseasDestinationList = Global.overseasDestinationList;
                Dictionary<string, string> destinationIdToPeakSeason = Global.destinationIdToPeakSeason;
                string redisStaticInfo = null;
                StaticResponseParms staticResponseParms = new StaticResponseParms();
                urlToDestination urlToDestinationObj = new urlToDestination();
                //var staticHotelTTSSCachedPrice = string.Empty;

                // Make the Redis call here
                System.Diagnostics.Stopwatch totalRedisFetchTimer = new System.Diagnostics.Stopwatch();
                totalRedisFetchTimer.Start();
                
                string redisStaticMappingInfo = RedisDataAccessHelper.RedisDataHelper.GetRedisValueByKey(url, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelLandingMapping"]));
                if (!string.IsNullOrWhiteSpace(redisStaticMappingInfo))
                {
                    urlToDestinationObj = JsonConvert.DeserializeObject<urlToDestination>(redisStaticMappingInfo);
                    masterHotelId = urlToDestinationObj.MHID;                    
                    redisStaticInfo = RedisDataAccessHelper.RedisDataHelper.GetRedisValueByKey(masterHotelId, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
                    if (!string.IsNullOrWhiteSpace(redisStaticMappingInfo))
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo);
                    }
                }

                totalRedisFetchTimer.Stop();

                #region binding the hotel data
                // Use MHID to get the static hotel info
                if (!string.IsNullOrWhiteSpace(masterHotelId))
                {                    
                    staticMHInfo = new StaticMasterHotelInfo();
                    staticMHInfo.timetakenForRedisFetching = totalRedisFetchTimer.Elapsed.TotalMilliseconds;

                    System.Diagnostics.Stopwatch GetMasterHotelInfoResponseTimer = new System.Diagnostics.Stopwatch();
                    GetMasterHotelInfoResponseTimer.Start();
                                        
                    mhidStaticInfo = Utilities.GetMasterHotelInfo(masterHotelId.ToString(), out cacheTime);

                    GetMasterHotelInfoResponseTimer.Stop();
                    staticMHInfo.timetakenForGetMasterHotelInfoCall = GetMasterHotelInfoResponseTimer.Elapsed.TotalMilliseconds;
                    TTSSMasterHotelCheapPriceData staticHotelTTSSCachedPrice = GetHotelLandingOfferController(masterHotelId);

                    if (!string.IsNullOrEmpty(staticHotelTTSSCachedPrice.MasterHotelId))
                    {
                        staticMHInfo.staticHotelCachedPrices = staticHotelTTSSCachedPrice;
                    }
                    if (mhidStaticInfo._source != null)
                    {
                        MasterHotelSource _source = new MasterHotelSource();
                        _source.BuildingName = mhidStaticInfo._source.BuildingName;
                        _source.Description = mhidStaticInfo._source.Description;
                        _source.DestinationCode = mhidStaticInfo._source.DestinationCode;
                        _source.DestinationName = mhidStaticInfo._source.DestinationName;
                        _source.Features = mhidStaticInfo._source.Features;
                        _source.Location = mhidStaticInfo._source.Location;
                        _source.MasterHotelId = mhidStaticInfo._source.MasterHotelId;
                        _source.MasterResortId = mhidStaticInfo._source.MasterResortId;
                        _source.MasterResortName = mhidStaticInfo._source.MasterResortName;
                        _source.Rating = mhidStaticInfo._source.Rating;
                        _source.RatingLevel = mhidStaticInfo._source.RatingLevel;

                        if (mhidStaticInfo._source.Region != null)
                        {
                            MHIDRegion region = new MHIDRegion();
                            region.AirportCodes = mhidStaticInfo._source.Region.AirportCodes;
                            region.GrandRegionId = mhidStaticInfo._source.Region.GrandRegionId;
                            region.GrandRegionName = mhidStaticInfo._source.Region.GrandRegionName.Trim();
                            region.ParentRegionId = mhidStaticInfo._source.Region.ParentRegionId;
                            region.ParentRegionName = mhidStaticInfo._source.Region.ParentRegionName.Trim();
                            region.RegionId = mhidStaticInfo._source.Region.RegionId;
                            region.Reglevel = mhidStaticInfo._source.Region.Reglevel;
                            region.Regname = mhidStaticInfo._source.Region.Regname.Trim();
                            region.ResortId = mhidStaticInfo._source.Region.ResortId;

                            // Adding Guides link for the link url for the various level of regions
                            if(region.RegionId > 0)
                            {
                                region.RegionLink = Guides.Guides.GetGuideLinkUrl(region.RegionId);
                               
                            }
                            if(region.ParentRegionId > 0)
                            {
                                region.ParentRegionLink = Guides.Guides.GetGuideLinkUrl(region.ParentRegionId);
                            }
                            if(region.GrandRegionId > 0)
                            {
                                region.GrandRegionLink = Guides.Guides.GetGuideLinkUrl(region.GrandRegionId);
                            }

                            _source.Region = region;

                            string phoneNumber = OffersBuilder.GetTelephoneNumber(region.RegionId, "192", 0, "");
                            
                            if(string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                if(region.ParentRegionId != 0)
                                {
                                    phoneNumber = OffersBuilder.GetTelephoneNumber(region.ParentRegionId, "192", 0, "");

                                    if(string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        if (region.GrandRegionId != 0)
                                        {
                                            phoneNumber = OffersBuilder.GetTelephoneNumber(region.GrandRegionId, "192", 0, "");
                                        }
                                        else
                                        {
                                            phoneNumber = string.Empty;
                                        }
                                    }
                                }
                                else
                                {
                                    phoneNumber = string.Empty;
                                }
                            }
                            _source.phoneNumber = phoneNumber;
                        }

                        if (!string.IsNullOrWhiteSpace(mhidStaticInfo._source.Description))
                        {
                            string tempDesc = Regex.Replace(mhidStaticInfo._source.Description, "<h3>.*?</h3>", String.Empty);
                            tempDesc = Regex.Replace(tempDesc, "<.*?>", String.Empty).Trim();
                            if (tempDesc.Length > SHORT_DESCRIPTION_MAX_LENGTH)
                            {
                                _source.shortDescription = tempDesc.Substring(0, SHORT_DESCRIPTION_MAX_LENGTH);
                            }
                            else
                            {
                                _source.shortDescription = tempDesc.Substring(0, tempDesc.Length);
                            }
                        }
                        else
                        {
                            _source.shortDescription = string.Empty;
                        }

                        if (mhidStaticInfo._source.Address != null)
                        {
                            MHIDAddress address = new MHIDAddress();
                            address.AddressLineOne = mhidStaticInfo._source.Address.AddressLineOne;
                            address.AddressLineTwo = mhidStaticInfo._source.Address.AddressLineTwo;
                            address.AddressLineThree = mhidStaticInfo._source.Address.AddressLineThree;
                            address.AddressLineFour = mhidStaticInfo._source.Address.AddressLineFour;
                            address.City = mhidStaticInfo._source.Address.City;
                            address.PostCode = mhidStaticInfo._source.Address.PostCode;

                            _source.Address = address;
                        }

                        staticMHInfo._source = _source;
                    }

                    TripAdvisor taInfo = new TripAdvisor();
                    taInfo.hotelStyle = mhidStaticInfo.TripAdvisor.hotelStyle;
                    taInfo.noOfRooms = mhidStaticInfo.TripAdvisor.noOfRooms;

                    // For TA Reviews Count, TA ID and Average TA Rating
                    if (staticResponseParms != null && !string.IsNullOrWhiteSpace(staticResponseParms.tripAdvisorId))
                    {
                        taInfo.reviewsCount = staticResponseParms.reviewCount;
                        taInfo.TAID = int.Parse(staticResponseParms.tripAdvisorId);
                        taInfo.TARating = staticResponseParms.averageRating;

                        staticMHInfo.TripAdvisor = taInfo;
                    }
                    else if (mhidStaticInfo.TripAdvisor != null)
                    {
                        taInfo.reviewsCount = mhidStaticInfo.TripAdvisor.reviewsCount;
                        taInfo.TAID = mhidStaticInfo.TripAdvisor.TAID;
                        taInfo.TARating = mhidStaticInfo.TripAdvisor.TARating;

                        staticMHInfo.TripAdvisor = taInfo;
                    }
                    // Supply the Search Query Parameters
                    SearchQueryParameters searchQueryParams = new SearchQueryParameters();
                    searchQueryParams.adults = ConfigurationManager.AppSettings["adults"];
                    searchQueryParams.boardType = ConfigurationManager.AppSettings["boardType"];
                    searchQueryParams.channelId = ConfigurationManager.AppSettings["channelId"];
                    searchQueryParams.children = ConfigurationManager.AppSettings["children"];
                    searchQueryParams.departureIds = ConfigurationManager.AppSettings["departureIds"];
                    searchQueryParams.duration = ConfigurationManager.AppSettings["duration"];
                    searchQueryParams.ratings = ConfigurationManager.AppSettings["ratings"];

                    if (urlToDestinationObj != null)
                    {
                        searchQueryParams.labelName = urlToDestinationObj.destLabel;
                        searchQueryParams.labelId = urlToDestinationObj.destinationLabelId;
                        searchQueryParams.channelId = urlToDestinationObj.channelId;
                    }
                    else
                    {
                        searchQueryParams.labelName = string.Empty;
                        searchQueryParams.labelId = string.Empty;
                        searchQueryParams.channelId = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(searchQueryParams.labelName))
                    {
                        // Incorporate the logic for peak seasons
                        if (!string.IsNullOrWhiteSpace(searchQueryParams.labelId) && destinationIdToPeakSeason.ContainsKey(searchQueryParams.labelId))
                        {
                            int[] dateRollingNFlexibilityInfo = GetDateRollingForDestination(searchQueryParams.labelId, destinationIdToPeakSeason);
                            searchQueryParams.dateRolling = dateRollingNFlexibilityInfo[0];
                            searchQueryParams.flexibilityDuration = dateRollingNFlexibilityInfo[1];
                        }
                        else
                        {
                            searchQueryParams.flexibilityDuration = Int32.Parse(ConfigurationManager.AppSettings["flexibility"]);
                            searchQueryParams.dateRolling = Int32.Parse(ConfigurationManager.AppSettings["dateRolling"]);
                        }
                    }
                    else
                    {
                        searchQueryParams.flexibilityDuration = Int32.Parse(ConfigurationManager.AppSettings["flexibility"]);
                        searchQueryParams.dateRolling = Int32.Parse(ConfigurationManager.AppSettings["dateRolling"]);
                    }

                    staticMHInfo.searchParams = searchQueryParams;
                    // Meta Tags for the static hotel landing page
                    staticMHInfo.metaDescription = ConfigurationManager.AppSettings["StaticHotelLandingMetaDescription"];
                    staticMHInfo.title = ConfigurationManager.AppSettings["StaticHotelLandingMetaTitle"];

                    // Fetching the image urls for the Hotel
                    staticMHInfo.images = new List<string>();
                    staticMHInfo.mobileimages = new List<string>();
                    staticMHInfo.thumbnailimages = new List<string>();

                    if (mhidStaticInfo.errataData != null)
                    {
                        staticMHInfo.errataData = mhidStaticInfo.errataData;
                    }
                    else
                    {
                        staticMHInfo.errataData = new List<ErrataInfo>();
                    }

                    if (Global.mhidtoImageCountList.ContainsKey(mhidStaticInfo._source.MasterHotelId.ToString()))
                    {
                        int hoteliffCount = Global.mhidtoImageCountList[mhidStaticInfo._source.MasterHotelId.ToString()];
                        string imgSourceURLFormat = Global.imgSourceURLFormat;
                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                        {
                            staticMHInfo.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                        }
                    }

                    if (mhidStaticInfo == null)
                    {
                        throw new WebFaultException<string>("GetMasterHotelInfo: MasterHotelId Does not exist:" + masterHotelId.ToString(), System.Net.HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("GetHotelInfoByUrl: No mapped MasterHotelId found for the URL:" + url, System.Net.HttpStatusCode.InternalServerError);
                }
                #endregion
                return staticMHInfo;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        private static int[] GetDateRollingForDestination(string parentDestinationId, Dictionary<string, string> destinationIdToPeakSeason)
        {
            Dictionary<string, int> months = new Dictionary<string, int>
                {
                    { "January", 1 },{ "February", 2 },{ "March", 3 },{ "April", 4 }, { "May", 5 },{ "June", 6 },
                    { "July", 7 }, { "August", 8 },{ "September", 9 },{ "October", 10 },{ "November", 11 },{ "December", 12 }
                };

            int[] dateRollingNFlexibility = new int[2];
            int currentMonth = Int32.Parse(DateTime.UtcNow.Month.ToString());
            int minPeakSelectedMonth = -1;
            int maxPeakSelectedMonth = -1;

            string[] destinationPeakMonths = destinationIdToPeakSeason[parentDestinationId].Split(',');
            int[] peakMonths = new int[13] { -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            foreach (string month in destinationPeakMonths)
            {
                peakMonths[months[month.Trim()]] = 1;
            }

            // iterate the 12 months (next year same month in worst case) in circular array fashion to get the next peak season month if any
            int maxIteration = 12;
            int index = currentMonth;
            while (maxIteration > 0)
            {
                if (index < peakMonths.Length)
                {
                    if (peakMonths[index] == 1)
                    {
                        // got the next peak season, set the dateRolling
                        if (minPeakSelectedMonth != -1)
                            maxPeakSelectedMonth = index;
                        else
                            minPeakSelectedMonth = index;
                    }
                    else if (minPeakSelectedMonth != -1)
                    {
                        if (maxPeakSelectedMonth == -1)
                        {
                            maxPeakSelectedMonth = minPeakSelectedMonth;
                        }
                        else
                        {
                            break;
                        }
                    }
                    maxIteration--;
                    index++;
                }
                else
                {
                    index = 1;
                }
            }

            if (minPeakSelectedMonth != -1 && maxPeakSelectedMonth != -1)
            {
                int length = 12;
                //int differenceInMinMaxPeakMonths = -1;
                int middleMonthPeakSeason = midPoint(minPeakSelectedMonth, maxPeakSelectedMonth, length);

                if (middleMonthPeakSeason == 0)
                    middleMonthPeakSeason = 1;

                if (middleMonthPeakSeason <= currentMonth)
                {
                    // increment the date rolling by an year
                    DateTime currentDate = DateTime.UtcNow;
                    DateTime meanpeakSelectedDate = new DateTime(DateTime.UtcNow.Year + 1, middleMonthPeakSeason, 15);
                    dateRollingNFlexibility[0] = Convert.ToInt32((meanpeakSelectedDate - currentDate).TotalDays);
                }
                else
                {
                    // increment the date rolling
                    DateTime currentDate = DateTime.UtcNow;
                    DateTime meanpeakSelectedDate = new DateTime(DateTime.UtcNow.Year, middleMonthPeakSeason, 15);
                    dateRollingNFlexibility[0] = Convert.ToInt32((meanpeakSelectedDate - currentDate).TotalDays);
                }

                // Generate the dynamic flexibity range of dates
                dateRollingNFlexibility[1] = int.Parse(ConfigurationManager.AppSettings["flexibility"]);
                //if (minPeakSelectedMonth <= maxPeakSelectedMonth)
                //{
                //    differenceInMinMaxPeakMonths = maxPeakSelectedMonth - minPeakSelectedMonth + 1;
                //}
                //else
                //{
                //    differenceInMinMaxPeakMonths = 12 - (minPeakSelectedMonth - maxPeakSelectedMonth);
                //}
                //dateRollingNFlexibility[1] = (differenceInMinMaxPeakMonths / 2) * 30;
            }
            return dateRollingNFlexibility;
        }

        private static int midPoint(int low, int high, int length)
        {
            int mid = -1;
            low %= length;
            high %= length;
            while (low < 0)
            {
                low += length;
            }
            while (high < low)
            {
                high += length;
            }

            mid = (low + high) / 2;
            mid %= length;
            return mid;
        }

        private static string GetMHIDByUrl(string url)
        {
            try
            {
                //List<MHIDBuildingNameMapping> mhidBuildingNameMapping = new List<MHIDBuildingNameMapping>();
                Dictionary<string, string> mhidBuildingNameMapping = new Dictionary<string, string>();
                string mhidBuildingNameMappingResponse = string.Empty;
                string Mhid = string.Empty;

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForMhidData(ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFileBucketName"], ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFilePath"])))
                {
                    mhidBuildingNameMappingResponse = reader.ReadToEnd();
                }
                mhidBuildingNameMapping = JsonConvert.DeserializeObject<Dictionary<string, string>>(mhidBuildingNameMappingResponse);

                // Search for the Url to get the MHID
                url = url.Replace("/hotels", "");

                if (mhidBuildingNameMapping.TryGetValue(url.ToLower().Trim(), out Mhid))
                {

                }
                return Mhid;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        /*
         * 
         *  Get the Offer from the TTSS Cheapest Price Log File for each day for each Master Hotel
         * 
         */
        public static TTSSMasterHotelCheapPriceData GetHotelLandingOfferController(string mhid)
        {
            TTSSMasterHotelCheapPriceData hotelLandingOfferInfo = new TTSSMasterHotelCheapPriceData();
            Dictionary<string, string> boardBasisMapping = new Dictionary<string, string>();

            boardBasisMapping.Add("RO", "Room Only");
            boardBasisMapping.Add("BB", "Bed & Breakfast");
            boardBasisMapping.Add("SC", "Self Catering");
            boardBasisMapping.Add("HB", "Half Board");
            boardBasisMapping.Add("FB", "Full Board");
            boardBasisMapping.Add("AI", "All Inclusive");

            // Read the file from S3
            try
            {
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["TTSSHotelCheapestPriceBucket"], ConfigurationManager.AppSettings["TTSSHotelCheapestPriceKeyName"] + mhid + ".json")))
                {
                    hotelLandingOfferInfo = JsonConvert.DeserializeObject<TTSSMasterHotelCheapPriceData>(reader.ReadToEnd());
                    if (hotelLandingOfferInfo != null)
                    {
                        hotelLandingOfferInfo.CheapestDay = DateTime.Parse(hotelLandingOfferInfo.CheapestDay.Trim()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                        hotelLandingOfferInfo.CheapestBoard = boardBasisMapping[hotelLandingOfferInfo.CheapestBoard];
                        hotelLandingOfferInfo.PhoneNumber = OffersBuilder.GetTelephoneNumber(int.Parse(hotelLandingOfferInfo.DestinationId), ConfigurationManager.AppSettings["DefaultTradingNameIds"], 0, "");
                        hotelLandingOfferInfo.CheapestPrice = Math.Round(Double.Parse(hotelLandingOfferInfo.CheapestPrice), MidpointRounding.AwayFromZero).ToString();
                        for (var i = 0; i < hotelLandingOfferInfo.CheapestPerMonth.Count; i++)
                        {
                            hotelLandingOfferInfo.CheapestPerMonth[i].CheapestPrice = Math.Round(Double.Parse(hotelLandingOfferInfo.CheapestPerMonth[i].CheapestPrice), MidpointRounding.AwayFromZero).ToString();
                            hotelLandingOfferInfo.CheapestPerMonth[i].CheapestDay = DateTime.Parse(hotelLandingOfferInfo.CheapestPerMonth[i].CheapestDay.Trim()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                        }
                    }
                }           
            }
            catch(AmazonS3Exception awsexp)
            {
                ErrorLogger.Log(string.Format("AWSS3Exception Occurred : Message: {0} & StackTrace; {1}", awsexp.Message, awsexp.StackTrace), LogLevel.Error);
                return hotelLandingOfferInfo;
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(string.Format("Error Occurred : Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                return hotelLandingOfferInfo;
            }

            return hotelLandingOfferInfo;
        }

    }
}