﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Presentation.WCFRestService.Model.Enum;
using System.Configuration;
using MySql.Data.MySqlClient;
using Presentation.WCFRestService.Model.LambdaHits;
using System.IO;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Guides;
using System.Threading.Tasks;
using System.ServiceModel.Web;
using Amazon.S3;
using Amazon.S3.Model;

namespace Presentation.Web.WCFRestServices.LambdaHits
{
    public class GetRecentBlogsBL
    {
        static MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["BlogsConnString"]);
        public static void GenerateRecentBlogsFromDBController(string procedureName)
        {
            List<RecentBlog> recentBlogs = new List<RecentBlog>();
            string queryParameter = string.Empty;
            string awsBucket = string.Empty;
            string awsKeyName = string.Empty;

            try
            {
                connection.Open();
                recentBlogs = makeStoredProcedureCall(procedureName, queryParameter);
                connection.Close();

                // Write the recent blogs to S3
                awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                awsKeyName = ConfigurationManager.AppSettings["RecentBlogsFilePath"];
                string Etag = Utilities.PutObjectToS3(awsBucket, awsKeyName, recentBlogs);
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        ErrorLogger.Log("Cannot connect to server.  Contact administrator", LogLevel.Error);
                        break;

                    case 1045:
                        ErrorLogger.Log("Invalid username/password, please try again", LogLevel.Error);
                        break;

                    default:
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                        throw new WebFaultException<string>("Error in doing Database operation", System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public static List<RecentBlog> GetRecentBlogsWebController(string pageName)
        {
            List<RecentBlog> recentBlogs = new List<RecentBlog>();
            try
            {
                var recentBlogsStream = "";
                string awsBucket = string.Empty;
                string awsKeyName = string.Empty;

                if (string.IsNullOrWhiteSpace(pageName)) // For Blogs on HP
                {
                    awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                    awsKeyName = ConfigurationManager.AppSettings["RecentBlogsFilePath"];
                }
                else if(!string.IsNullOrWhiteSpace(pageName)) // For Blogs on Other than HP
                {
                    awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                    awsKeyName = ConfigurationManager.AppSettings["PageSpecificBlogsFilePath"] + pageName.Replace(" ", "-").ToLower().Trim() + ".json";
                }
                else
                {
                    throw new WebFaultException<string>("pageName parameter values are missing", System.Net.HttpStatusCode.BadRequest);
                }

                try
                {
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucket, awsKeyName)))
                    {
                        recentBlogsStream = reader.ReadToEnd();
                    }
                    recentBlogs = JsonConvert.DeserializeObject<List<RecentBlog>>(recentBlogsStream);
                }
                catch (Exception ex)
                {
                    return recentBlogs;
                }
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & Details: {1} & StackTrace; {2}", ex.Message, ex.Data, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
            return recentBlogs;
        }

        public static void GenerateTagSpecificRecentBlogsFromDB(string procedureName)
        {
            try
            {
                List<Destination> guideDestinationsList = new List<Destination>();
                List<BespokePage> bespokePages = new List<BespokePage>();

                var bespokePagesJson = "";

                // Fetch the list of Guides Destinations
                string destinationGuidesJson = Utilities.ExecuteGetWebRequest("https://contentservice.teletextholidays.co.uk/GetJsonService.svc/GetGuidesIndexListFromCloudFront");

                var destinationGuides = JsonConvert.DeserializeObject<DestinationGuides>(destinationGuidesJson);
                guideDestinationsList = destinationGuides.destinations;

                // Fetch the lisf of Bespoke pages
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["BlogsBucket"], ConfigurationManager.AppSettings["bespokePageListFileName"])))
                {
                    bespokePagesJson = reader.ReadToEnd();
                }

                bespokePages = JsonConvert.DeserializeObject<List<BespokePage>>(bespokePagesJson);

                // If there are any pages for which recent blogs have to be generated
                if(bespokePages.Count > 0 || guideDestinationsList.Count > 0)
                {
                    try
                    {
                        connection.Open();

                        string queryParameter = string.Empty;
                        string awsBucket = string.Empty;
                        string awsKeyName = string.Empty;

                        // Iterate over the bespoke page list and generate the recent blogs for each bespoke page if available in DB
                        foreach (BespokePage bespokePage in bespokePages)
                        {
                            string tagStr = string.Empty;

                            // If tags are available
                            if(bespokePage.tags.Count > 0)
                            {
                                tagStr = string.Join(" ", bespokePage.tags.ToArray());
                            }

                            queryParameter = ("'" + bespokePage.pageName.Replace("Holidays", "").Trim() + " " + tagStr + "'").Trim();

                            List<RecentBlog> recentBlogs = makeStoredProcedureCall(procedureName, queryParameter);

                            // Write the recent blogs to S3
                            if (recentBlogs.Count > 0)
                            {
                                awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                                awsKeyName = bespokePage.pageName.Replace(" ", "-").ToLower() + ".json";
                                string Etag = Utilities.PutObjectToS3(awsBucket, awsKeyName, recentBlogs, ConfigurationManager.AppSettings["PageSpecificBlogsFilePath"]);
                            }
                        }

                        // Iterate over the guides Destination list and generate the recent blogs for each destination if available in DB
                        foreach (Destination destination in guideDestinationsList)
                        {
                            queryParameter = ("'" + destination.name.Replace("Islands", "") + "'").Trim();

                            List<RecentBlog> recentBlogs = makeStoredProcedureCall(procedureName, queryParameter);

                            connection.Close();

                            // Write the recent blogs to S3
                            if (recentBlogs.Count > 0)
                            {
                                awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                                string fileName = destination.linkurl.Split('/')[Convert.ToInt32(destination.reglevel) + 1];
                                awsKeyName = fileName + ".json";
                                string Etag = Utilities.PutObjectToS3(awsBucket, awsKeyName, recentBlogs, ConfigurationManager.AppSettings["PageSpecificBlogsFilePath"]);
                            }
                        }
                    }
                    catch(MySqlException ex)
                    {
                        switch (ex.Number)
                        {
                            case 0:
                                ErrorLogger.Log("Cannot connect to server.  Contact administrator", LogLevel.Error);
                                break;

                            case 1045:
                                ErrorLogger.Log("Invalid username/password, please try again", LogLevel.Error);
                                break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
            }
        }

        private static List<RecentBlog> makeStoredProcedureCall(string storedProcedureName, string queryParameters = "")
        {
            string queryString = "Call " + storedProcedureName + "(" + queryParameters + ")";

            List<RecentBlog> recentBlogs = new List<RecentBlog>();
            MySqlCommand cmd = new MySqlCommand(queryString, connection);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dtable = ds.Tables[0];

            for (int i = 0; i < dtable.Rows.Count; i++)
            {
                RecentBlog blog = new RecentBlog();
                blog.nodeId = Convert.ToInt32(dtable.Rows[i]["nid"]);
                blog.nameTag = dtable.Rows[i]["name"].ToString();
                blog.teaserTextOne = dtable.Rows[i]["field_teaser_text_1_value"].ToString();
                blog.teaserTextTwo = dtable.Rows[i]["field_teaser_text_2_value"].ToString();
                blog.imageUrl = dtable.Rows[i]["imageUrl"].ToString();
                blog.aliasUrl = dtable.Rows[i]["aliasUrl"].ToString();
                blog.bodySummary = dtable.Rows[i]["body_summary"].ToString();

                recentBlogs.Add(blog);
            }
            return recentBlogs;
        }
    }
}