﻿using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2BAvailabilityRequestExecution
    {
        public static B2CAvailabilityResultsParams GetAvailabilityResponse(string availabilityRequestStr, AvailabilitySearchRequest availabilityRequest)
        {
            B2CAvailabilityResultsParams b2bAvailabilityParams = new B2CAvailabilityResultsParams();
            //Calling CreateSOAPWebRequest method
            HttpWebRequest request = CreateSOAPWebRequest();

            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request
            SOAPReqBody.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:GetAvailabilityResponse>
         <!--Optional:-->
         <tem:request><![CDATA[" + availabilityRequestStr + "]]></tem:request></tem:GetAvailabilityResponse></soapenv:Body></soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request    
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    //reading stream    
                    var ServiceResult = rd.ReadToEnd();
                    //writting stream result on console    
                    XmlDocument docResponse = new XmlDocument();
                    docResponse.LoadXml(ServiceResult);
                    XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
                    Envelope responseEnvelope = (Envelope)serializer.Deserialize(new StringReader(ServiceResult));

                    var availabilityResponseResultXml = responseEnvelope.Body.GetAvailabilityResponseResponse.GetAvailabilityResponseResult;
                    XmlSerializer serializeResult = new XmlSerializer(typeof(B2BResponseAvailabilityOffers));
                    B2BResponseAvailabilityOffers availabilityResultSet = (B2BResponseAvailabilityOffers)serializeResult.Deserialize(new StringReader(availabilityResponseResultXml));

                    if (availabilityResultSet.Success != null)
                    {
                        ErrorLogger.Log("Got the Response from B2B Endpoint", LogLevel.ElasticSearchAvailability);

                        b2bAvailabilityParams = B2BESDocumentsBuilder.B2BESOfferBuilder(availabilityResultSet, availabilityRequest);
                    }
                    else
                    {
                        ErrorLogger.Log(string.Format("Error response from B2B Endpoint with message: {0} & Code: {1}", availabilityResultSet.Errors.Error.ShortText, availabilityResultSet.Errors.Error.Code), LogLevel.ElasticSearchAvailability);
                    }                    
                }
            }
            return b2bAvailabilityParams;
        }

        public static HttpWebRequest CreateSOAPWebRequest()
        {
            //Making Web Request    
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["B2BAccommodationXMLService"]);
            //SOAPAction    
            Req.Headers.Add(@"SOAPAction:http://tempuri.org/IAccommodationB2BXmlAvailabilityService/GetAvailabilityResponse");
            //Content_type    
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method    
            Req.Method = "POST";
            //return HttpWebRequest    
            return Req;
        }
    }
}