﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CBookingRequest
    {
        public static AccommodationService.AccommodationB2CBookingRequest BuildBookingObject(BookingRequest bookingRequest)
        {
            AccommodationService.AccommodationB2CBookingRequest accommodationB2CBookingRequest = new AccommodationService.AccommodationB2CBookingRequest();

            accommodationB2CBookingRequest.AvailabilityId = Guid.Parse(bookingRequest.AvailabilityId);// System.Guid.Parse("09ec9715-fd8d-46ba-853f-37b16d5fffa7"); //availabilityId;            
            accommodationB2CBookingRequest.Customer = new AccommodationService.AccommodationB2CBookingRequestCustomer();            
            accommodationB2CBookingRequest.Customer.ContactNumber = bookingRequest.Customer.ContactNumber;
            accommodationB2CBookingRequest.Customer.EmailAddress = bookingRequest.Customer.EmailAddress;
            accommodationB2CBookingRequest.Customer.FirstName = bookingRequest.Customer.FirstName;
            accommodationB2CBookingRequest.Customer.Id = 0;
            accommodationB2CBookingRequest.Customer.Surname = bookingRequest.Customer.Surname;
            accommodationB2CBookingRequest.Customer.Site = bookingRequest.Customer.Site;
            accommodationB2CBookingRequest.Customer.Title = bookingRequest.Customer.Title;
            accommodationB2CBookingRequest.Customer.AddressLine1 = bookingRequest.Customer.AddressLine1;
            accommodationB2CBookingRequest.Customer.AddressLine2 = bookingRequest.Customer.AddressLine2;
            accommodationB2CBookingRequest.Customer.Town = bookingRequest.Customer.Town;
            accommodationB2CBookingRequest.Customer.Country = bookingRequest.Customer.Country;
            accommodationB2CBookingRequest.Customer.County = bookingRequest.Customer.County;
            accommodationB2CBookingRequest.Customer.PostCode = bookingRequest.Customer.PostCode;
            accommodationB2CBookingRequest.Customer.UserId = null;
            accommodationB2CBookingRequest.Customer.Channel = AccommodationService.Channel.TeletextHolidaysUK;
            accommodationB2CBookingRequest.Customer.LanguageId = bookingRequest.Customer.LanguageId;


            accommodationB2CBookingRequest.paymentDetails = new AccommodationService.AccommodationB2CBookingRequestRoomPaymentDetails();
            accommodationB2CBookingRequest.paymentDetails.AddressLine1 = bookingRequest.PaymentDetails.AddressLine1;
            accommodationB2CBookingRequest.paymentDetails.AddressLine2 = bookingRequest.PaymentDetails.AddressLine1;
            accommodationB2CBookingRequest.paymentDetails.CardExpireDate = new AccommodationService.MonthYear();
            accommodationB2CBookingRequest.paymentDetails.CardExpireDate.Month = bookingRequest.PaymentDetails.CardExpireDate.Month;
            accommodationB2CBookingRequest.paymentDetails.CardExpireDate.Year = bookingRequest.PaymentDetails.CardExpireDate.Year;

            accommodationB2CBookingRequest.paymentDetails.CardHolderName = bookingRequest.PaymentDetails.CardHolderName;
            accommodationB2CBookingRequest.paymentDetails.CardNumber = bookingRequest.PaymentDetails.CardNumber;
            accommodationB2CBookingRequest.paymentDetails.CardSecurityCode = bookingRequest.PaymentDetails.CardSecurityCode;
            accommodationB2CBookingRequest.paymentDetails.CardType = (AccommodationService.CardType)bookingRequest.PaymentDetails.CardType;
            accommodationB2CBookingRequest.paymentDetails.City = bookingRequest.PaymentDetails.City;
            accommodationB2CBookingRequest.paymentDetails.Country = bookingRequest.PaymentDetails.Country;
            accommodationB2CBookingRequest.paymentDetails.County = bookingRequest.PaymentDetails.County;
            accommodationB2CBookingRequest.paymentDetails.Postcode = bookingRequest.PaymentDetails.Postcode;
            accommodationB2CBookingRequest.paymentDetails.PaySafeVPSAuthCode = bookingRequest.PaymentDetails.PaySafeVPSAuthCode;
            accommodationB2CBookingRequest.paymentDetails.MerchantRefNum = bookingRequest.PaymentDetails.PaysafeAuthorisationCode;

            var valuatedRoom = new AccommodationService.AccommodationB2CBookingRequestRoom();
            accommodationB2CBookingRequest.ValuatedRooms = new AccommodationService.AccommodationB2CBookingRequestRoom[bookingRequest.ValuatedRooms.Count()];


            for (int i = 0; i < bookingRequest.ValuatedRooms.Count(); i++)
            {
                accommodationB2CBookingRequest.ValuatedRooms[i] = new AccommodationService.AccommodationB2CBookingRequestRoom();
                accommodationB2CBookingRequest.ValuatedRooms[i].Guests = new AccommodationService.AccommodationB2CBookingRequestRoomGuest[bookingRequest.ValuatedRooms[i].Guests.Count()];
                for (int j = 0; j < bookingRequest.ValuatedRooms[i].Guests.Count(); j++)
                {
                    var guest = new AccommodationService.AccommodationB2CBookingRequestRoomGuest();
                    guest.Age = bookingRequest.ValuatedRooms[i].Guests[j].Age.HasValue ? (byte?)(bookingRequest.ValuatedRooms[i].Guests[j].Age) : null;// null;
                    guest.FirstName = bookingRequest.ValuatedRooms[i].Guests[j].FirstName;//"Test";
                    guest.Id = bookingRequest.ValuatedRooms[i].Guests[j].Id;//0;
                    guest.Surname = bookingRequest.ValuatedRooms[i].Guests[j].Surname;//"Test";
                    guest.TitleString = bookingRequest.ValuatedRooms[i].Guests[j].TitleString;//"Mr";
                    accommodationB2CBookingRequest.ValuatedRooms[i].Guests[j] = guest;
                }

                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails = new AccommodationService.AccommodationB2CBookingRequestRoomPaymentDetails();
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.AddressLine1 = bookingRequest.ValuatedRooms[i].PaymentDetails.AddressLine1;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.AddressLine2 = bookingRequest.ValuatedRooms[i].PaymentDetails.AddressLine2;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardExpireDate = new AccommodationService.MonthYear();
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardExpireDate.Month = int.Parse(bookingRequest.ValuatedRooms[i].PaymentDetails.CardExpireMonth);
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardExpireDate.Year = int.Parse(bookingRequest.ValuatedRooms[i].PaymentDetails.CardExpireYear);

                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardHolderName = bookingRequest.ValuatedRooms[i].PaymentDetails.CardHolderName;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardNumber = bookingRequest.ValuatedRooms[i].PaymentDetails.CardNumber;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardSecurityCode = bookingRequest.ValuatedRooms[i].PaymentDetails.CardSecurityCode;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.CardType = GetCardType(bookingRequest.ValuatedRooms[i].PaymentDetails.CardType);// bookingRequest.ValuatedRooms[i].PaymentDetails.CardType; AccommodationService.CardType.vis.Mastercard;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.City = bookingRequest.ValuatedRooms[i].PaymentDetails.City;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.Country = bookingRequest.ValuatedRooms[i].PaymentDetails.Country;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.County = bookingRequest.ValuatedRooms[i].PaymentDetails.County;
                //accommodationB2CBookingRequest.ValuatedRooms[i].PaymentDetails.Postcode = bookingRequest.ValuatedRooms[i].PaymentDetails.Postcode;

                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests = new AccommodationService.AccommodationB2CBookingRequestRoomSpecialRequest();
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.AdjoiningRooms = bookingRequest.ValuatedRooms[i].SpecialRequests.AdjoiningRooms;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.CotRequired = bookingRequest.ValuatedRooms[i].SpecialRequests.CotRequired;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.DisabledAccess = bookingRequest.ValuatedRooms[i].SpecialRequests.DisabledAccess;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.LateArrival = bookingRequest.ValuatedRooms[i].SpecialRequests.LateArrival;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.NonSmoking = bookingRequest.ValuatedRooms[i].SpecialRequests.NonSmoking;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.OtherRequests = bookingRequest.ValuatedRooms[i].SpecialRequests.OtherRequests != null ? bookingRequest.ValuatedRooms[i].SpecialRequests.OtherRequests.ToString() : null;
                accommodationB2CBookingRequest.ValuatedRooms[i].SpecialRequests.SeaViews = bookingRequest.ValuatedRooms[i].SpecialRequests.SeaViews;

                accommodationB2CBookingRequest.ValuatedRooms[i].ValuatedRoomId = bookingRequest.ValuatedRooms[i].ValuatedRoomId;// "120001201BGwMmPGVLEKehkhiskUuAQ2018050901681230300010SUSDT2000000242 Bedroom Standard Suite0002AI0013All Inclusive000010010EUR1298.014tX2h1ktzEem5zN3sx7ASw";// response.ValuationResult.Rooms[0].RoomId;                
            }
            accommodationB2CBookingRequest.ValuationId = Guid.Parse(bookingRequest.ValuationId); //System.Guid.Parse("e3451af8-1023-4b49-b238-c225cf4ed756");  //response.ValuationId;

            accommodationB2CBookingRequest.Channel = AccommodationService.Channel.TeletextHolidaysUK;           

            return accommodationB2CBookingRequest;
        }


        private static AccommodationService.CardType GetCardType(int cardType)
        {
            switch (cardType)
            {
                case 1:
                    return AccommodationService.CardType.Mastercard;
                case 2:
                    return AccommodationService.CardType.VisaCredit;
                case 3:
                    return AccommodationService.CardType.AmericanExpress;
                case 4:
                    return AccommodationService.CardType.DinersCarteBlanche;
                case 5:
                    return AccommodationService.CardType.Discover;
                case 6:
                    return AccommodationService.CardType.Enroute;
                case 7:
                    return AccommodationService.CardType.JCB;
                case 8:
                    return AccommodationService.CardType.Switch;
                case 9:
                    return AccommodationService.CardType.Delta;
                case 10:
                    return AccommodationService.CardType.Solo;
                case 11:
                    return AccommodationService.CardType.Maestro;
                case 12:
                    return AccommodationService.CardType.Laser;
                case 13:
                    return AccommodationService.CardType.PrepaidMasterCard;
                case 14:
                    return AccommodationService.CardType.InternationalMaestro;
                case 15:
                    return AccommodationService.CardType.VisaDebit;
                case 16:
                    return AccommodationService.CardType.VisaDelta;
                case 17:
                    return AccommodationService.CardType.VisaElectron;
                case 18:
                    return AccommodationService.CardType.VisaPurchasing;
                case 19:
                    return AccommodationService.CardType.MastercardDebit;
                case 20:
                    return AccommodationService.CardType.EuroCard;
                case 21:
                    return AccommodationService.CardType.DinersClub;
                case 22:
                    return AccommodationService.CardType.PayPal;
                default:
                    return AccommodationService.CardType.Unknown;
            }

        }
    }
}