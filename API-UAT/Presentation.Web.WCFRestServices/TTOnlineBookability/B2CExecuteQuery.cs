﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CExecuteQuery
    {
        public static B2CAvailabilitySearchRequestAvailability CheckRequestInAvailabilityIndice(AvailabilitySearchRequest availabilityRequest)
        {
            B2CAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = new B2CAvailabilitySearchRequestAvailability();
            
            string searchRequestQuery = B2CQueryBuilder.GetAvailabilityRequestSearchQuery(availabilityRequest);
            string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "availability" + "/_search", searchRequestQuery);
            SearchResponseParams searchRequestResponse = new SearchResponseParams();
            try
            {
                searchRequestResponse = JsonConvert.DeserializeObject<SearchResponseParams>(searchResponse);
                if (searchRequestResponse.hits.total != 0)
                {
                    b2bAvailabilitySearchRequestAvailability.availabilityOffersCount = searchRequestResponse.hits.hits[0]._source.availabilityOffersCount;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error Deserializing the es request response.Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                //errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace));
            }
            return b2bAvailabilitySearchRequestAvailability;
        }
        public static B2CAvailabilitySearchRequestAvailability CheckRequestInAvailabilityIndiceV2(AvailabilitySearchRequest availabilityRequest)
        {
            B2CAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = new B2CAvailabilitySearchRequestAvailability();

            string searchRequestQuery = B2CQueryBuilder.GetAvailabilityRequestSearchQueryV2(availabilityRequest);
            string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["AvailabilityIndiceType"] + "/_search", searchRequestQuery);
            SearchResponseParams searchRequestResponse = new SearchResponseParams();
            try
            {
                searchRequestResponse = JsonConvert.DeserializeObject<SearchResponseParams>(searchResponse);
                if (searchRequestResponse.hits.total != 0)
                {
                    b2bAvailabilitySearchRequestAvailability.availabilityOffersCount = searchRequestResponse.hits.hits[0]._source.availabilityOffersCount;
                    b2bAvailabilitySearchRequestAvailability.availabilityId = searchRequestResponse.hits.hits[0]._source.availabilityId;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error Deserializing the es request response.Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                //errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace));
            }
            return b2bAvailabilitySearchRequestAvailability;
        }

        public static B2CAvailabilityResultsParams HitB2CAvailability(AvailabilitySearchRequest availabilityRequest, string filename)
        {
            // Make a B2C Availability Request and insert into ES
            string b2cAvailabilitySearchQuery = B2CQueryBuilder.GetB2CAvailabilitySearchQueryV2(availabilityRequest);
            return B2CAvailabilityRequestExecution.GetAvailabilityResponse(b2cAvailabilitySearchQuery, availabilityRequest, filename);
        }
    }
}