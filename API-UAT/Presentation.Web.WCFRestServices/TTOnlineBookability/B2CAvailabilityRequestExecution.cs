﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CAvailabilityRequestExecution
    {
        public static B2CAvailabilityResultsParams GetAvailabilityResponse(string availabilityRequestStr, AvailabilitySearchRequest availabilityRequest, string filename)
        {
            System.Diagnostics.Stopwatch accomSoaResponseTimer = new System.Diagnostics.Stopwatch();
            double accomodationSoaResponseTime = 0.0;

            System.Diagnostics.Stopwatch accomSoaResponseTimer2 = new System.Diagnostics.Stopwatch();
            double accomodationSoaResponseTime2 = 0.0;
            //accomSoaResponseTimer.Start();
            B2CAvailabilityResultsParams b2cAvailabilityResultsParams = new B2CAvailabilityResultsParams();
            B2CAvailabilityResponse b2cAvailabilityResponse = new B2CAvailabilityResponse();
            
            int pageSize = int.Parse(ConfigurationManager.AppSettings["OffersCountToFetchFromB2C"]);
            try
            {                
                AccommodationService.AccommodationAvailabilityServiceClient accomdationService = new AccommodationService.AccommodationAvailabilityServiceClient();
                AccommodationService.AccommodationB2CAvailabilityRequest accommodationB2CAvailabilityRequest = new AccommodationService.AccommodationB2CAvailabilityRequest();
                //accommodationB2CAvailabilityRequest = JsonConvert.DeserializeObject<AccommodationService.AccommodationB2CAvailabilityRequest>("{\"AvailabilityId\":null,\"Channel\":1,\"CheckInDate\":\"2018-07-27T00:00:00\",\"CheckOutDate\":\"2018-08-03T00:00:00\",\"ClientIpAddress\":\"127.0.0.1\",\"Debugging\":false,\"DestinationId\":\"fdcecbb6-b92e-4495-baa4-700f5d046687\",\"EstablishmentId\":null,\"IgnoreConsolidation\":false,\"PromotionalCode\":null,\"ProvidersToSearch\":null,\"Rooms\":[{\"Adults\":2,\"ChildAges\":\"\",\"Children\":0,\"RoomNumber\":1}],\"SearchType\":2,\"SuppliersToSearch\":null,\"TC4Origin\":false}");
                accommodationB2CAvailabilityRequest = JsonConvert.DeserializeObject<AccommodationService.AccommodationB2CAvailabilityRequest>(availabilityRequestStr);

                ErrorLogger.Log("Availability Request query sent to B2C Accommodation Service : " + availabilityRequestStr, LogLevel.ElasticSearchAvailability);

                // Direct FOD or Meta Traffic
                if(availabilityRequest.availabilityId == Guid.Empty && !string.IsNullOrWhiteSpace(availabilityRequest.hotelCode))
                {
                    if (availabilityRequest.searchRequestType == "fresh")
                    {
                        accomSoaResponseTimer.Start();
                        var result1 = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = 0 },
                                                                           new AccommodationService.AccommodationB2CAvailabilitySort(), false);

                        if (result1.AvailabilityStatus == AccommodationService.Status.Successful)
                        {
                            accommodationB2CAvailabilityRequest.AvailabilityId = result1.AvailabilityId;
                            var result2 = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = pageSize, PageNumber = 1 }, new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                            var result3 = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = result2.AvailabilityTotalResultsCount },
                                                                               new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                            b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result3));
                            accomSoaResponseTimer.Stop();
                            accomodationSoaResponseTime = accomSoaResponseTimer.Elapsed.TotalMilliseconds;
                            //ErrorLogger.Log("\nTime Taken to fetch the availability results from B2C : " + accomodationSoaResponseTime, filename);
                            b2cAvailabilityResponse.AvailabilityResults = b2cAvailabilityResponse.AvailabilityResults.Where(p => p.EstablishmentId == System.Guid.Parse(availabilityRequest.hotelCode)).ToList(); //.AvailabilityResults.Where(p => p.EstablishmentId == System.Guid.Parse(availabilityRequest.hotelCode));
                            b2cAvailabilityResponse.AvailabilityTotalResultsCount = b2cAvailabilityResponse.AvailabilityResults.Count;
                            //b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(finalResult));
                        }
                        else
                        {
                            ErrorLogger.Log("B2C First status call not Successful.", LogLevel.ElasticSearchAvailability);
                        }
                    }
                }
                else
                {
                    if (availabilityRequest.searchRequestType == "fresh")
                    {
                        accomSoaResponseTimer.Start();
                        var result1 = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = 0 },
                        new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                        //var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = pageSize, PageNumber = 1 }, new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                        accomSoaResponseTimer.Stop();
                        accomodationSoaResponseTime = accomSoaResponseTimer.Elapsed.TotalMilliseconds;
                        //ErrorLogger.Log("\nTime Taken to fetch the availability results from B2C 1 : " + accomodationSoaResponseTime, filename);

                        if (result1.AvailabilityStatus == AccommodationService.Status.Successful)
                        {
                            accommodationB2CAvailabilityRequest.AvailabilityId = result1.AvailabilityId;
                            accomSoaResponseTimer2.Start();
                            var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = pageSize, PageNumber = 1 }, new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                            accomSoaResponseTimer2.Stop();
                            accomodationSoaResponseTime2 = accomSoaResponseTimer2.Elapsed.TotalMilliseconds;
                            //ErrorLogger.Log("\nTime Taken to fetch the availability results from B2C 2 : " + accomodationSoaResponseTime2, filename);
                            b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                        }
                        else
                        {
                            ErrorLogger.Log("B2C First status call not Successful.", LogLevel.ElasticSearchAvailability);
                        }

                    }
                    else if (availabilityRequest.searchRequestType == "continued")
                    {
                        accomSoaResponseTimer.Start();
                        accommodationB2CAvailabilityRequest.AvailabilityId = availabilityRequest.availabilityId;
                        var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = availabilityRequest.AvailabilityTotalResultsCount },
                                                                               new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                        accomSoaResponseTimer.Stop();
                        accomodationSoaResponseTime = accomSoaResponseTimer.Elapsed.TotalMilliseconds;
                        //ErrorLogger.Log("\nTime Taken to fetch the availability results from B2C : " + accomodationSoaResponseTime, filename);
                        b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                        b2cAvailabilityResponse.AvailabilityResults = b2cAvailabilityResponse.AvailabilityResults.Skip(int.Parse(ConfigurationManager.AppSettings["OffersCountToFetchFromB2C"])).ToList();
                    }
                }                

                if (b2cAvailabilityResponse.AvailabilityResults.Count > 0)
                {
                    ErrorLogger.Log(availabilityRequest.searchRequestType + " : " +"Got the Response from B2C Endpoint. Total Number of offers available = " + b2cAvailabilityResponse.AvailabilityTotalResultsCount, LogLevel.ElasticSearchAvailability);
                    b2cAvailabilityResultsParams = B2CESOfferDocBuilder.B2CESOfferBuilder(b2cAvailabilityResponse, availabilityRequest, filename);
                    b2cAvailabilityResultsParams.availabilityId = b2cAvailabilityResponse.AvailabilityId;

                    if(availabilityRequest.searchRequestType == "fresh")
                    {
                        Task<AccommodationService.AccommodationB2CAvailabilityResultsFilterOptions> filterOptions = accomdationService.GetAccommodationAvailabilityResultsFilterOptionsAsync(b2cAvailabilityResultsParams.availabilityId);
                        filterOptions.Wait();
                        AccommodationService.AccommodationB2CAvailabilityResultsFilterOptions filterOptionResults = filterOptions.Result;
                        DateTime startDate = Convert.ToDateTime(availabilityRequest.startDate);
                        DateTime endDate = Convert.ToDateTime(availabilityRequest.endDate);
                        b2cAvailabilityResultsParams.priceMin = Convert.ToInt32(filterOptionResults.PricePerNightMin.Amount);
                        b2cAvailabilityResultsParams.priceMax = Convert.ToInt32(filterOptionResults.PricePerNightMax.Amount);
                    }
                }
                else
                {
                    ErrorLogger.Log("No Results from B2C Service or Error response from B2C Service.", LogLevel.ElasticSearchAvailability);
                }
                b2cAvailabilityResultsParams.accomodationSoaResponseTime = accomodationSoaResponseTime;                
            }
            catch(Exception ex)
            {
                ErrorLogger.Log("Exception from B2C Endpoint. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
            }
            return b2cAvailabilityResultsParams;
        }
    }
}