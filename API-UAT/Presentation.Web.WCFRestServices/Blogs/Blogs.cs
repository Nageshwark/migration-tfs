﻿using Amazon.S3;
using Amazon.S3.Model;
using MySql.Data.MySqlClient;
using Presentation.WCFRestService.Model.Blogs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Presentation.Web.WCFRestServices.Blogs
{
    public class Blogs
    {
        private static readonly Random random = new Random();
        public static string GetBlogs()
        {
            DataSet ds = ExecuteQuery("Get_Blog_SiteMap");
            DataRowCollection rows = ds.Tables[0].Rows;
            urlset urlSet = new urlset();
            urlSet.Urls = new List<url>();
            if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int index = ds.Tables[0].Rows.Count - 1; index >= 0; index--)
                {
                    url url = new url();
                    url.loc = "https://www.teletextholidays.co.uk/" + (string)ds.Tables[0].Rows[index]["aliasUrl"];
                    url.changefreq = "weekly";
                    Random r = new Random();
                    url.priority = Math.Round(RandomNumberBetween(0.5, 0.8),2).ToString().Substring(0,3);
                    urlSet.Urls.Add(url);
                }
            }

            XmlSerializer xsSubmit = new XmlSerializer(typeof(urlset));            
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, urlSet);
                    xml = sww.ToString();
                }
            }

            return xml.Replace("utf-16", "utf-8").Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"","").Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "").Replace("<Urls>","").Replace("</Urls>","");
        }
        private static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }
        private static DataSet ExecuteQuery(string procName, List<MySqlParameter> procParams = null)
        {
            DataSet ds = null;
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ttBlog"].ToString()))
            {
                con.Open();

                MySqlCommand com = new MySqlCommand();
                com.Connection = con;

                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                if (procParams != null && procParams.Count != 0)
                {
                    foreach (MySqlParameter param in procParams)
                    {
                        com.Parameters.Add(param);
                    }
                }

                MySqlDataAdapter adap = new MySqlDataAdapter(com);
                ds = new DataSet();

                try
                {
                    adap.Fill(ds);
                }
                catch (Exception ex)
                {
                    con.Close();

                    throw ex;
                }

                con.Close();
            }
            return ds;
        }
        public static string CompressBlogImages()
        {
            int index = 0;
            try
            {
                IAmazonS3 client;
                string awsBucketName = "ttext-drupal-prod";
                string imageFolderPath = "drupal/images/blogs/s3fs-public/";
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                ListObjectsRequest listObjectsRequest = new ListObjectsRequest
                {
                    BucketName = awsBucketName,
                    Prefix = imageFolderPath,
                    Delimiter = "/"
                };
                do
                {
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        ListObjectsResponse listObjectsResponse = client.ListObjects(listObjectsRequest);
                        if (listObjectsResponse.S3Objects == null || listObjectsResponse.S3Objects.Count() == 1)
                        {
                            return null;
                        }

                        var images = listObjectsResponse.S3Objects.Where(p => p.LastModified > DateTime.Now.AddDays(-1).AddHours(2) && p.Key.EndsWith(".png")).ToList();
                        //var images = listObjectsResponse.S3Objects.Where(p => p.LastModified > DateTime.Now.AddHours(-.5) && p.Key.EndsWith(".png")).ToList();
                        foreach (var image1 in images)
                        {
                            index++;

                            var tempImageName = image1.Key;
                            var jpg = tempImageName.Replace("drupal/images/blogs/s3fs-public/", "");
                            string desImagePath = "drupal/images/blogs/s3fs-public/";
                            Stream imageStream = new MemoryStream();
                            GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = tempImageName };
                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                response.ResponseStream.CopyTo(imageStream);
                            }
                            imageStream.Position = 0;
                            var copyImageStream = new MemoryStream();
                            imageStream.CopyTo(copyImageStream);
                            copyImageStream.Position = 0;
                            PutObjectToS3("ttext-drupal-prod", tempImageName.Replace("drupal/images/blogs/s3fs-public/", ""), imageStream, "drupal/images/blogs/s3fs-public/BlogImages-Backup/");
                            imgProcess(100, desImagePath, copyImageStream, tempImageName.Replace("drupal/images/blogs/s3fs-public/", ""));
                        }

                        images = listObjectsResponse.S3Objects.Where(p => p.LastModified > DateTime.Now.AddDays(-1).AddHours(2) && (p.Key.EndsWith(".jpg") || p.Key.EndsWith(".jpeg") || p.Key.EndsWith(".JPG"))).ToList();
                        //images = listObjectsResponse.S3Objects.Where(p => p.LastModified > DateTime.Now.AddHours(-.5) && (p.Key.EndsWith(".jpg") || p.Key.EndsWith(".jpeg") || p.Key.EndsWith(".JPG"))).ToList();
                        foreach (var image1 in images)
                        {
                            index++;

                            var tempImageName = image1.Key;
                            var jpg = tempImageName.Replace("drupal/images/blogs/s3fs-public/", "");
                            string desImagePath = "drupal/images/blogs/s3fs-public/";
                            Stream imageStream = new MemoryStream();
                            GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = tempImageName };
                            using (GetObjectResponse response = client.GetObject(request))
                            {
                                response.ResponseStream.CopyTo(imageStream);
                            }

                            imageStream.Position = 0;
                            var copyImageStream = new MemoryStream();
                            imageStream.CopyTo(copyImageStream);
                            copyImageStream.Position = 0;
                            PutObjectToS3("ttext-drupal-prod", tempImageName.Replace("drupal/images/blogs/s3fs-public/", ""), imageStream, "drupal/images/blogs/s3fs-public/BlogImages-Backup/");
                            //imageStream.Position = 0;
                            imgProcess(80, desImagePath, copyImageStream, tempImageName.Replace("drupal/images/blogs/s3fs-public/", ""));
                        }

                        if (listObjectsResponse.IsTruncated)
                        {
                            listObjectsRequest.Marker = listObjectsResponse.NextMarker;
                        }
                        else
                        {
                            listObjectsRequest = null;
                        }
                    }

                } while (listObjectsRequest != null);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return "Failed";
            }

            return "Success";
        }
        static void imgProcess(Int32 quality, string desImagePath, Stream imageStream, string fileName)
        {
            try
            {
                using (Bitmap image = (Bitmap)System.Drawing.Image.FromStream(imageStream))
                {
                    ImageCodecInfo jpgInfo = ImageCodecInfo.GetImageEncoders()
                             .Where(codecInfo =>
                             codecInfo.MimeType == "image/jpeg").First();
                    Image finalImage = image;
                    Bitmap bitmap = null;
                    try
                    {
                        Int32 height = image.Height;
                        Int32 width = image.Width;
                        int left = 0;
                        int top = 0;
                        int srcWidth = width;
                        int srcHeight = height;
                        bitmap = new Bitmap(width, height);
                        double croppedHeightToWidth = (double)height / width;
                        double croppedWidthToHeight = (double)width / height;

                        if (image.Width > image.Height)
                        {
                            srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
                            if (srcWidth < image.Width)
                            {
                                srcHeight = image.Height;
                                left = (image.Width - srcWidth) / 2;
                            }
                            else
                            {
                                srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
                                srcWidth = image.Width;
                                top = (image.Height - srcHeight) / 2;
                            }
                        }
                        else
                        {
                            srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
                            if (srcHeight < image.Height)
                            {
                                srcWidth = image.Width;
                                top = (image.Height - srcHeight) / 2;
                            }
                            else
                            {
                                srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
                                srcHeight = image.Height;
                                left = (image.Width - srcWidth) / 2;
                            }
                        }
                        using (Graphics g = Graphics.FromImage(bitmap))
                        {
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            g.CompositingQuality = CompositingQuality.HighQuality;
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                            new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
                        }
                        finalImage = bitmap;
                    }
                    catch
                    {

                    }
                    try
                    {
                        using (EncoderParameters encParams = new EncoderParameters(1))
                        {
                            encParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                            var stream = new System.IO.MemoryStream();
                            finalImage.Save(stream, jpgInfo, encParams);
                            PutObjectToS3("ttext-drupal-prod", fileName, stream, desImagePath);
                        }
                    }
                    catch (Exception Exception)
                    {
                        Console.WriteLine(Exception.Message);
                    }
                    if (bitmap != null)
                    {
                        bitmap.Dispose();
                    }
                }
            }
            catch (Exception e)
            {

            }
        }
        public static string PutObjectToS3(string awsBucketName, string keyName, Stream stream, string subfolderPath = "")
        {
            IAmazonS3 client;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];            
            string s3Etag = string.Empty;
            try
            {

                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", subfolderPath, keyName)

                    };
                    request.InputStream = stream;
                    PutObjectResponse response2 = client.PutObject(request);
                    s3Etag = response2.ETag;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return s3Etag;
        }
    }
}