﻿using Amazon.S3;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace Presentation.Web.WCFRestServices.Tools
{
    public class ToolsController
    {
        public static TravelAlertOperationResponse SaveOrUpdateTravelAlertsController(TravelAlert travelAlert, string userId)
        {
            string responseMessage = string.Empty;
            List<TravelAlert> travelAlerts = new List<TravelAlert>();
            TravelAlertOperationResponse travelAlertOperationResponse = new TravelAlertOperationResponse();

            string awsBucketName = ConfigurationManager.AppSettings["TravelAlertToolBucket"];
            string awsBucketSubFolderPath = ConfigurationManager.AppSettings["TravelAlertToolSubFolderPath"];
            string travelAlertsConfigFileName = ConfigurationManager.AppSettings["TravelAlertToolFileName"];

            travelAlert.updatedBy = userId;
            travelAlert.updatedDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");

            // Save new Travel Alerts
            if (travelAlert.travelAlertId == Guid.Empty)
            {
                travelAlert.travelAlertId = Guid.NewGuid();

                travelAlert.createdBy = userId;
                travelAlert.createdDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");

                try
                {
                    // Get the existing Travel Alerts from the JSON and append the new Travel Alert to it
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, awsBucketSubFolderPath + travelAlertsConfigFileName)))
                    {
                        travelAlerts = JsonConvert.DeserializeObject<List<TravelAlert>>(reader.ReadToEnd());

                        // append the new travel alert to the existing travel alerts
                        travelAlerts.Add(travelAlert);

                        string eTag = Utilities.PutObjectToS3(awsBucketName, travelAlertsConfigFileName, travelAlerts, awsBucketSubFolderPath);

                        if (!string.IsNullOrWhiteSpace(eTag))
                        {
                            travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                            travelAlertOperationResponse.operationResponse = new OperationResponse();
                            travelAlertOperationResponse.operationResponse.responseCode = 0;
                            travelAlertOperationResponse.operationResponse.responseMessage = "Travel Alerts have been saved SUCCESSFULLY.";
                            ErrorLogger.Log(string.Format("Travel Alerts Tool -- Travel Alert Id - {0} successfully saved by the user - {1}", travelAlerts.Last().travelAlertId, userId), LogLevel.Information);
                        }
                        else
                        {
                            travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                            travelAlertOperationResponse.operationResponse = new OperationResponse();
                            travelAlertOperationResponse.operationResponse.responseCode = -1;
                            responseMessage = "Some Error Occurred while saving the travel alerts. Please try after sometime.";
                            ErrorLogger.Log("Some Error Occurred while saving the travel alerts.Please try after sometime. Travel Alert JSON : " + JsonConvert.SerializeObject(travelAlert), LogLevel.Information);
                        }
                    }
                }
                catch (AmazonS3Exception s3Exception)
                {
                    ErrorLogger.Log("AWS S3 Exception Occurred when saving the travel alerts. Message: " + s3Exception.Message + ". & StackTrace: " + s3Exception.StackTrace, LogLevel.Error);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("An Exception Occurred when saving the travel alerts. Message: " + ex.Message + ". & StackTrace: " + ex.StackTrace, LogLevel.Error);
                }
            }
            else
            {
                // Update the existing Travel Alerts
                try
                {
                    // Get the existing Travel Alerts from the JSON and append the new Travel Alert to it
                    
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, awsBucketSubFolderPath + travelAlertsConfigFileName)))
                    {
                        travelAlerts = JsonConvert.DeserializeObject<List<TravelAlert>>(reader.ReadToEnd());

                        TravelAlert oldTravelAlert = travelAlerts.Select(alert => alert).Where(alert => alert.travelAlertId == travelAlert.travelAlertId).FirstOrDefault();

                        // update all the fields in the old alert to the new alert
                        oldTravelAlert.travelAlertMessage = travelAlert.travelAlertMessage;

                        List<Website> newWebsitesConfig = new List<Website>();
                        foreach (Website newWebsiteConfig in travelAlert.travelAlertWebConfiguration)
                        {
                            foreach (Website oldWebsiteConfig in oldTravelAlert.travelAlertWebConfiguration)
                            {
                                if (oldWebsiteConfig.website == newWebsiteConfig.website)
                                {
                                    oldWebsiteConfig.showInHomePage = newWebsiteConfig.showInHomePage;
                                    oldWebsiteConfig.showInAllPages = newWebsiteConfig.showInAllPages;
                                    break;
                                }
                            }
                        }

                        string eTag = Utilities.PutObjectToS3(awsBucketName, travelAlertsConfigFileName, travelAlerts, awsBucketSubFolderPath);

                        if (!string.IsNullOrWhiteSpace(eTag))
                        {
                            travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                            travelAlertOperationResponse.operationResponse = new OperationResponse();
                            travelAlertOperationResponse.operationResponse.responseCode = 0;
                            travelAlertOperationResponse.operationResponse.responseMessage = "Travel Alerts have been updated SUCCESSFULLY.";
                            ErrorLogger.Log(string.Format("Travel Alerts Tool -- Travel Alert Id - {0} successfully updated by the user - {1}", travelAlert.travelAlertId, userId), LogLevel.Information);
                        }
                        else
                        {
                            travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                            travelAlertOperationResponse.operationResponse = new OperationResponse();
                            travelAlertOperationResponse.operationResponse.responseCode = -1;
                            responseMessage = "Some Error Occurred while updating the travel alerts. Please try after sometime.";
                            ErrorLogger.Log("Some Error Occurred while updating the travel alerts.Please try after sometime. Travel Alert JSON : " + JsonConvert.SerializeObject(travelAlert), LogLevel.Information);
                        }
                    }
                }
                catch (AmazonS3Exception s3Exception)
                {
                    ErrorLogger.Log("AWS S3 Exception Occurred when updating the travel alerts. Message: " + s3Exception.Message + ". & StackTrace: " + s3Exception.StackTrace, LogLevel.Error);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("An Exception Occurred when saving the travel alerts. Message: " + ex.Message + ". & StackTrace: " + ex.StackTrace, LogLevel.Error);
                }
            }
            return travelAlertOperationResponse;
        }

        public static List<TravelAlert> GetTravelAlertsController()
        {
            List<TravelAlert> travelAlerts = new List<TravelAlert>();

            string awsBucketName = ConfigurationManager.AppSettings["TravelAlertToolBucket"];
            string awsBucketSubFolderPath = ConfigurationManager.AppSettings["TravelAlertToolSubFolderPath"];
            string travelAlertsConfigFileName = ConfigurationManager.AppSettings["TravelAlertToolFileName"];

            try
            {
                // Get the existing Travel Alerts from the JSON and append the new Travel Alert to it
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, awsBucketSubFolderPath + travelAlertsConfigFileName)))
                {
                    travelAlerts = JsonConvert.DeserializeObject<List<TravelAlert>>(reader.ReadToEnd());
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                ErrorLogger.Log("AWS S3 Exception Occurred when saving the travel alerts. Message: " + s3Exception.Message + ". & StackTrace: " + s3Exception.StackTrace, LogLevel.Error);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("An Exception Occurred when saving the travel alerts. Message: " + ex.Message + ". & StackTrace: " + ex.StackTrace, LogLevel.Error);
            }

            return travelAlerts;
        }

        public static TravelAlertOperationResponse DeleteTravelAlertController(Guid travelAlertId, string userId)
        {
            List<TravelAlert> travelAlerts = new List<TravelAlert>();
            TravelAlertOperationResponse travelAlertOperationResponse = new TravelAlertOperationResponse();

            string awsBucketName = ConfigurationManager.AppSettings["TravelAlertToolBucket"];
            string awsBucketSubFolderPath = ConfigurationManager.AppSettings["TravelAlertToolSubFolderPath"];
            string travelAlertsConfigFileName = ConfigurationManager.AppSettings["TravelAlertToolFileName"];

            try
            {
                // Get the existing Travel Alerts from the JSON and append the new Travel Alert to it
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, awsBucketSubFolderPath + travelAlertsConfigFileName)))
                {
                    travelAlerts = JsonConvert.DeserializeObject<List<TravelAlert>>(reader.ReadToEnd());

                    if(travelAlerts.Count > 0)
                    {
                        bool istravelAlertAvailable = (travelAlerts.Select(alert => alert).Where(alert => alert.travelAlertId == travelAlertId).FirstOrDefault()) != null ? true : false;

                        if(istravelAlertAvailable)
                        {
                            travelAlerts.RemoveAll(travelAlert => travelAlert.travelAlertId == travelAlertId);

                            string eTag = Utilities.PutObjectToS3(awsBucketName, travelAlertsConfigFileName, travelAlerts, awsBucketSubFolderPath);

                            if (!string.IsNullOrWhiteSpace(eTag))
                            {
                                travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                                travelAlertOperationResponse.operationResponse = new OperationResponse();
                                travelAlertOperationResponse.operationResponse.responseCode = 0;
                                travelAlertOperationResponse.operationResponse.responseMessage = "Travel Alert has been deleted SUCCESSFULLY.";
                                ErrorLogger.Log(string.Format("Travel Alerts Tool -- Travel Alert Id - {0} successfully deleted by the user - {1}", travelAlertId, userId), LogLevel.Information);
                            }
                            else
                            {
                                travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                                travelAlertOperationResponse.operationResponse = new OperationResponse();
                                travelAlertOperationResponse.operationResponse.responseCode = -1;
                                travelAlertOperationResponse.operationResponse.responseMessage = "Travel Alert CANNOT BE DELETED due to some technical issue. Please try after sometime.";
                                ErrorLogger.Log("Travel Alert CANNOT BE DELETED due to some technical issue. Please try after sometime.", LogLevel.Information);
                            }
                        }
                        else
                        {
                            travelAlertOperationResponse.travelAlerts = new List<TravelAlert>(travelAlerts);
                            travelAlertOperationResponse.operationResponse = new OperationResponse();
                            travelAlertOperationResponse.operationResponse.responseCode = -1;
                            travelAlertOperationResponse.operationResponse.responseMessage = "Travel Alert CANNOT BE DELETED as the Travel Alert ID provided is incorrect. Please try again.";
                            ErrorLogger.Log("Travel Alert CANNOT BE DELETED as the Travel Alert ID provided is incorrect. Please try again.", LogLevel.Information);
                        }
                    }                    
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                ErrorLogger.Log("AWS S3 Exception Occurred when saving the travel alerts. Message: " + s3Exception.Message + ". & StackTrace: " + s3Exception.StackTrace, LogLevel.Error);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("An Exception Occurred when saving the travel alerts. Message: " + ex.Message + ". & StackTrace: " + ex.StackTrace, LogLevel.Error);
            }

            return travelAlertOperationResponse;
        }

        public static List<TravelAlertWeb> GetTravelAlertWebController(string website, string isHP)
        {
            List<TravelAlertWeb> travelAlertsOnWeb = new List<TravelAlertWeb>();
            List<TravelAlert> travelAlerts = new List<TravelAlert>();

            string awsBucketName = ConfigurationManager.AppSettings["TravelAlertToolBucket"];
            string awsBucketSubFolderPath = ConfigurationManager.AppSettings["TravelAlertToolSubFolderPath"];
            string travelAlertsConfigFileName = ConfigurationManager.AppSettings["TravelAlertToolFileName"];

            try
            {
                // Get the existing Travel Alerts from the JSON and append the new Travel Alert to it
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, awsBucketSubFolderPath + travelAlertsConfigFileName)))
                {
                    travelAlerts = JsonConvert.DeserializeObject<List<TravelAlert>>(reader.ReadToEnd());

                    if (travelAlerts.Count > 0)
                    {
                        if (website.ToLower() == "uk")
                        {
                            if (Convert.ToBoolean(isHP)) // UK Home Page Alerts
                            {
                                foreach (TravelAlert travelAlert in travelAlerts)
                                {
                                    bool isUKtravelAlertAvailableForHP = (travelAlert.travelAlertWebConfiguration.Select(alert => alert).Where(alert => alert.website.ToLower() == website.ToLower() && alert.showInHomePage).FirstOrDefault()) != null ? true : false;
                                    if (isUKtravelAlertAvailableForHP)
                                    {
                                        TravelAlertWeb travelAlertOnWeb = new TravelAlertWeb();
                                        travelAlertOnWeb.travelAlertId = travelAlert.travelAlertId;
                                        travelAlertOnWeb.travelAlertMessage = travelAlert.travelAlertMessage;
                                        travelAlertOnWeb.createdBy = travelAlert.createdBy;
                                        travelAlertOnWeb.createdDate = travelAlert.createdDate;
                                        travelAlertOnWeb.updatedBy = travelAlert.updatedBy;
                                        travelAlertOnWeb.updatedDate = travelAlert.updatedDate;

                                        travelAlertsOnWeb.Add(travelAlertOnWeb);
                                    }
                                }
                            }
                            else // UK Other than HP Alerts
                            {
                                foreach (TravelAlert travelAlert in travelAlerts)
                                {
                                    bool isUKtravelAlertAvailableForOtherPages = (travelAlert.travelAlertWebConfiguration.Select(alert => alert).Where(alert => alert.website.ToLower() == website.ToLower() && alert.showInAllPages).FirstOrDefault()) != null ? true : false;
                                    if (isUKtravelAlertAvailableForOtherPages)
                                    {
                                        TravelAlertWeb travelAlertOnWeb = new TravelAlertWeb();
                                        travelAlertOnWeb.travelAlertId = travelAlert.travelAlertId;
                                        travelAlertOnWeb.travelAlertMessage = travelAlert.travelAlertMessage;
                                        travelAlertOnWeb.createdBy = travelAlert.createdBy;
                                        travelAlertOnWeb.createdDate = travelAlert.createdDate;
                                        travelAlertOnWeb.updatedBy = travelAlert.updatedBy;
                                        travelAlertOnWeb.updatedDate = travelAlert.updatedDate;

                                        travelAlertsOnWeb.Add(travelAlertOnWeb);
                                    }
                                }
                            }
                        }
                        else if (website.ToLower() == "ie")
                        {
                            if (Convert.ToBoolean(isHP)) // IE Home Page Alerts
                            {
                                foreach (TravelAlert travelAlert in travelAlerts)
                                {
                                    bool isIEtravelAlertAvailableForHP = (travelAlert.travelAlertWebConfiguration.Select(alert => alert).Where(alert => alert.website.ToLower() == website.ToLower() && alert.showInHomePage).FirstOrDefault()) != null ? true : false;
                                    if (isIEtravelAlertAvailableForHP)
                                    {
                                        TravelAlertWeb travelAlertOnWeb = new TravelAlertWeb();
                                        travelAlertOnWeb.travelAlertId = travelAlert.travelAlertId;
                                        travelAlertOnWeb.travelAlertMessage = travelAlert.travelAlertMessage;
                                        travelAlertOnWeb.createdBy = travelAlert.createdBy;
                                        travelAlertOnWeb.createdDate = travelAlert.createdDate;
                                        travelAlertOnWeb.updatedBy = travelAlert.updatedBy;
                                        travelAlertOnWeb.updatedDate = travelAlert.updatedDate;

                                        travelAlertsOnWeb.Add(travelAlertOnWeb);
                                    }
                                }
                            }
                            else // IE Other than HP Alerts
                            {
                                foreach (TravelAlert travelAlert in travelAlerts)
                                {
                                    bool isIEtravelAlertAvailableForOtherPages = (travelAlert.travelAlertWebConfiguration.Select(alert => alert).Where(alert => alert.website.ToLower() == website.ToLower() && alert.showInAllPages).FirstOrDefault()) != null ? true : false;
                                    if (isIEtravelAlertAvailableForOtherPages)
                                    {
                                        TravelAlertWeb travelAlertOnWeb = new TravelAlertWeb();
                                        travelAlertOnWeb.travelAlertId = travelAlert.travelAlertId;
                                        travelAlertOnWeb.travelAlertMessage = travelAlert.travelAlertMessage;
                                        travelAlertOnWeb.createdBy = travelAlert.createdBy;
                                        travelAlertOnWeb.createdDate = travelAlert.createdDate;
                                        travelAlertOnWeb.updatedBy = travelAlert.updatedBy;
                                        travelAlertOnWeb.updatedDate = travelAlert.updatedDate;

                                        travelAlertsOnWeb.Add(travelAlertOnWeb);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                ErrorLogger.Log("AWS S3 Exception Occurred when saving the travel alerts. Message: " + s3Exception.Message + ". & StackTrace: " + s3Exception.StackTrace, LogLevel.Error);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("An Exception Occurred when saving the travel alerts. Message: " + ex.Message + ". & StackTrace: " + ex.StackTrace, LogLevel.Error);
            }

            return travelAlertsOnWeb;
        }

        /*
         *  Below region has the implementation related to the SEO Template Tool for
         *  BeSpoke Pages
         */
        #region SEO Bespoke Template Tool
        public static PageAlias GetSEOTemplatePageIdFromDBController(string aliasurl)
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["UKDrupalRDSConnectionStr"].ConnectionString.ToString();
            string storedProcedureName = ConfigurationManager.AppSettings["GetDrupalPageIdForSEOTemplateSPName"];

            MySqlConnection connection = new MySqlConnection(connectionStr);
            PageAlias pageAliasInfo = new PageAlias();

            try
            {
                string queryString = "Call " + storedProcedureName + "('" + aliasurl + "')";

                MySqlCommand cmd = new MySqlCommand(queryString, connection);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                DataTable dtable = ds.Tables[0];

                for (int i = 0; i < dtable.Rows.Count; i++)
                {
                    pageAliasInfo.pageId = dtable.Rows[i]["source"].ToString();
                }
            }
            catch (MySqlException ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("Error while performing Database operation", System.Net.HttpStatusCode.InternalServerError);
            }
            return pageAliasInfo;
        }

        public static object PostPageAliasInDBController(PageAlias pageAlias, string userId)
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["UKDrupalRDSConnectionStr"].ConnectionString.ToString();
            string storedProcedureName = ConfigurationManager.AppSettings["PostDrupalPageIdNAliasUrlForSEOTemplateSPName"];

            MySqlConnection connection = new MySqlConnection(connectionStr);
            PageAlias pageAliasInfo = new PageAlias();

            try
            {
                string queryString = "Call " + storedProcedureName + "('" + pageAlias.pageId + "','" + pageAlias.aliasUrl + "')";

                MySqlCommand cmd = new MySqlCommand(queryString, connection);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                pageAliasInfo.pageId = pageAlias.pageId;
                pageAliasInfo.aliasUrl = pageAlias.aliasUrl;
            }
            catch (MySqlException ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("Error while performing Database operation", System.Net.HttpStatusCode.InternalServerError);
            }
            return pageAliasInfo;
        }
        #endregion
    }
}