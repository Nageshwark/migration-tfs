﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.Routing
{
    public class RoutingConfigValueHelper
    {
        public static string GetRoutingApiS3BucketName(string countrySite)
        {
            string configkey = "RoutingApiS3BucketName";

            if (countrySite != Global.COUNTRY_SITE_UK)
            {
                configkey += countrySite.ToUpper();
            }
            return ConfigurationManager.AppSettings[configkey];
        }


        public static int GetRedisDbId(string countrySite, string ukConfigKey)
        {
            int dbId;

            if (countrySite != Global.COUNTRY_SITE_UK)
            {
                ukConfigKey += countrySite.ToUpper();
            }

            Int32.TryParse(ConfigurationManager.AppSettings[ukConfigKey], out dbId);
            return dbId;
        }

    }
}