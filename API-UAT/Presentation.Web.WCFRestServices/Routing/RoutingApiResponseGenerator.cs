﻿using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Routing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Amazon.ElasticLoadBalancing.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.Web.WCFRestServices.Routing;
using ServiceStack;

namespace Presentation.Web.WCFRestServices
{
    public class RoutingApiResponseGenerator
    {
        public static RoutingResponse GetRoutingData(string destinationId, string departureId, string dates)
        {
            RoutingResponse response = new RoutingResponse();

            if (!string.IsNullOrEmpty(destinationId) && string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                response = GetRoutingDataByDestinationId(destinationId);
            }
            else if (string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                response = GetRoutingDataByDepartureId(departureId);
            }
            else if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                response = GetRoutingDataByDestinationAndDepartureId(destinationId, departureId);
            }
            else if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && !string.IsNullOrEmpty(dates))
            {
                response = GetRoutingDataByDestinationAndDepartureIdAndDates(destinationId, departureId, dates);
            }
            return response;
        }

        private static RoutingResponse GetRoutingDataByDestinationId(string destinationId)
        {
            RoutingResponse response = new RoutingResponse { valid = true };

            List<string> resortIds = GetResortIdsbyDestinationId(destinationId);

            try
            {
                if (resortIds != null && resortIds.Count > 0)
                {
                    response = RoutingRedisDataHelper.GetRoutingDataByResortIds(resortIds);
                    response.destinationIds.Add(Convert.ToInt32(destinationId));

                    response.total = response.departureIds.Count + response.dates.Count + response.destinationIds.Count +
                                     response.durations.Count;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), WCFRestService.Model.Enum.LogLevel.GetRoutingApiError);
            }
            return response;
        }

        private static List<string> GetResortIdsbyDestinationId(string destinationId)
        {
            List<string> result = new List<string>();

            if (!Global.DestinationResortMap.TryGetValue(destinationId, out result))
                return null;

            return result;
        }

        private static RoutingResponse GetRoutingDataByDepartureId(string departureId)
        {

            RoutingResponse response = new RoutingResponse();

            List<string> airportIds = new List<string>();

            if (Global.AirportGroupIdOrAirportIdToAirportsMapping.ContainsKey(departureId))
            {
                airportIds = Global.AirportGroupIdOrAirportIdToAirportsMapping[departureId];
            }

            try
            {
                if (airportIds != null && airportIds.Count > 0)
                {
                    response = RoutingRedisDataHelper.GetRoutingDataByAirportIds(airportIds);

                    response.valid = true;
                    response.departureIds.Add(Convert.ToInt32(departureId));

                    response.total = response.departureIds.Count + response.dates.Count + response.destinationIds.Count +
                                     response.durations.Count;
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("\nException in RoutingApiResponseGenerator.GetRoutingDataByDepartureId.\nMessage: " + exception.Message + ".\nStackTrace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                throw;
            }
            return response;
        }

        private static RoutingResponse GetRoutingDataByDestinationAndDepartureId(string destinationId, string departureId)
        {
            RoutingResponse response = new RoutingResponse();

            List<string> airportIds = new List<string>();

            if (Global.AirportGroupIdOrAirportIdToAirportsMapping.ContainsKey(departureId))
            {
                airportIds = Global.AirportGroupIdOrAirportIdToAirportsMapping[departureId];
            }

            List<string> resortIds = GetResortIdsbyDestinationId(destinationId);

            try
            {
                if (resortIds != null && resortIds.Count > 0 && airportIds != null && airportIds.Count > 0)
                {
                    response = RoutingRedisDataHelper.GetRoutingDataByResortIdsAndAirportIds(resortIds, airportIds);

                    response.valid = true;
                    response.destinationIds.Add(Convert.ToInt32(destinationId));
                    response.departureIds.Add(Convert.ToInt32(departureId));
                    response.total = response.departureIds.Count + response.dates.Count + response.destinationIds.Count +
                                     response.durations.Count;
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("\nException in RoutingApiResponseGenerator.GetRoutingDataByDestinationAndDepartureId.\nMessage: " + exception.Message + ".\nStackTrace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
            }

            return response;
        }

        private static RoutingResponse GetRoutingDataByDestinationAndDepartureIdAndDates(string destinationId, string departureId, string dates)
        {
            RoutingResponse response = new RoutingResponse();

            List<string> airportIds = new List<string>();

            if (Global.AirportGroupIdOrAirportIdToAirportsMapping.ContainsKey(departureId))
            {
                airportIds = Global.AirportGroupIdOrAirportIdToAirportsMapping[departureId];
            }

            List<string> resortIds = GetResortIdsbyDestinationId(destinationId);

            try
            {
                List<string> datesList = dates.IndexOf(',') > 0 ? dates.Split(',').ToList() : new List<string>() { dates };

                response = RoutingRedisDataHelper.GetRoutingDataByResortIdsAndAirportIdsAndDates(resortIds, airportIds,
                    datesList);

                response.destinationIds.Add(Convert.ToInt32(destinationId));
                response.departureIds.Add(Convert.ToInt32(departureId));
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("\nException in RoutingApiResponseGenerator.GetRoutingDataByDestinationAndDepartureIdAndDates.\nMessage: " + exception.Message + ".\nStackTrace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
            }

            return response;
        }
    }
}