﻿using System;
using System.ServiceModel.Channels;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class TeletextUsers
    {
        public string KeepMeDeals { get; set; }
        public string PhoneCode { get; set; }
        public string Provider { get; set; }
        public DateTime? SignUpDate { get; set; }
        public string Telephone { get; set; }
        public string Platform { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string AppVersion { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string DepartureAirport { get; set; }
    }

    public class Payload
    {
        public string user { get; set; }
        public string iat { get; set; }
        public string domain { get; set; }
    }

    public class JsonContentTypeMapper : WebContentTypeMapper
    {
        public override WebContentFormat
                   GetMessageFormatForContentType(string contentType)
        {
            if (contentType == "text/javascript")
            {
                return WebContentFormat.Json;
            }
            else
            {
                return WebContentFormat.Default;
            }
        }
    }
}