﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class RestrictAccess
    {
        public static List<string> RestrictedMethods()
        {
            var xmlPath = (HttpRuntime.AppDomainAppPath + @"data\RestrictMethods.xml");

            var xml = XDocument.Load(xmlPath);

            var results = (from n in xml.Descendants("method") select n.Value).ToList();

            return results;
        }
    }
}