﻿using AutoMapper;
using System.Data;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class DataTableProfile : Profile
    {
        public DataTableProfile()
        {
            IMappingExpression<DataRow, TeletextUsers> mappingExpression;

            mappingExpression = CreateMap<DataRow, TeletextUsers>(MemberList.Destination);

            mappingExpression.ForMember(d => d.KeepMeDeals, o => o.MapFrom(map => map["Keep_me_deals"]));
            mappingExpression.ForMember(d => d.AppVersion, o => o.MapFrom(map => map["AppVersion"]));
            mappingExpression.ForMember(d => d.CreatedOn, o => o.MapFrom(map => map["CreatedOn"]));
            mappingExpression.ForMember(d => d.DepartureAirport, o => o.MapFrom(map => map["Departure_airport"]));
            mappingExpression.ForMember(d => d.Email, o => o.MapFrom(map => map["Email"]));
            mappingExpression.ForMember(d => d.DisplayName, o => o.MapFrom(map => map["DisplayName"]));
            mappingExpression.ForMember(d => d.PhoneCode, o => o.MapFrom(map => map["Phone_Code"]));
            mappingExpression.ForMember(d => d.Provider, o => o.MapFrom(map => map["Provider"]));
            mappingExpression.ForMember(d => d.SignUpDate, o => o.MapFrom(map => map["SignUpDate"]));
            mappingExpression.ForMember(d => d.Telephone, o => o.MapFrom(map => map["Telephone"]));
            mappingExpression.ForMember(d => d.Platform, o => o.MapFrom(map => map["Platform"]));
        }
    }
}