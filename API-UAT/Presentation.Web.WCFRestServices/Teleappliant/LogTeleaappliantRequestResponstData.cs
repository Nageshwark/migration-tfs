﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.Teleappliant
{
    public class LogTeleaappliantRequestResponstData
    {
        public int Id { get; set; }
        public string RequestTime { get; set; }
        public string RequestSessionId { get; set; }
        public string ResponseSessionId { get; set; }        
        public string DeviceType { get; set; }
        public string IPAddress { get; set; }
        public string CountryCode { get; set; }
        public int RequestIngressNumber { get; set; }
        public int RequestDestinationId { get; set; }
        public int ResponseIngressNumber { get; set; }
        public int ResponseDestinationId { get; set; }
        public string ResponsePhoneNumber { get; set; }
        
    }
}