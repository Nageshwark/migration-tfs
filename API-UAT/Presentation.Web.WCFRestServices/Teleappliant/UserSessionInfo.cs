﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Presentation.Web.WCFRestServices.Teleappliant
{    
    public class UserSessionInfo
    {
        [JsonProperty("session")]
        public String session { get; set; }
        [JsonProperty("locality")]
        public Locality locality { get; set; }
        [DataMember]
        public Dictionary<string, value> quotes { get; set; }
    }

    public class Locality
    {
        [JsonProperty("ip")]
        public String ip { get; set; }
        [JsonProperty("isoCode")]
        public String isoCode { get; set; }
    }
    
    [Serializable]
    public class value
    {
        [DataMember]
        public String ingress { get; set; }
        [DataMember]
        public String expiry { get; set; }
    }
}