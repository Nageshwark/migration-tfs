﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.CustomerSupport;
using System.ServiceModel.Web;
using Presentation.WCFRestService.Model.Enum;

namespace Presentation.Web.WCFRestServices.CustomerSupport
{
    public class CustomerSupportController
    {
        public static string submitCustomerSupportRequest(CustomerSupportForm customerSupportDetails)
        {
            try
            {
                if (isCustomerDetailsValid(customerSupportDetails))
                {
                    // Create a ticket in the vision API and make an async Ajax call
                    return "SUCCESS";
                }
                else
                {
                    throw new WebFaultException<string>("CustomerSupportRequest: User input details are empty.", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        private static bool isCustomerDetailsValid(CustomerSupportForm customerSupportDetails)
        {
            if (customerSupportDetails == null || string.IsNullOrWhiteSpace(customerSupportDetails.leadPassengerName) || string.IsNullOrWhiteSpace(customerSupportDetails.emailAddress) || string.IsNullOrWhiteSpace(customerSupportDetails.bookingRef) || string.IsNullOrWhiteSpace(customerSupportDetails.departureDate) || string.IsNullOrWhiteSpace(customerSupportDetails.requestType) || string.IsNullOrWhiteSpace(customerSupportDetails.message))
                return false;
            return true;
        }
    }
}