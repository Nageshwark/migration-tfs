﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace Presentation.Web.WCFRestServices
{


    public class GogLocation
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class GogGeometry
    {
        public GogLocation location { get; set; }
        public Viewport viewport { get; set; }
    }

    public class OpeningHours
    {
        public bool open_now { get; set; }
        public List<object> weekday_text { get; set; }
    }

    public class Photo
    {
        public int height { get; set; }
        public List<string> html_attributions { get; set; }
        public string photo_reference { get; set; }
        public int width { get; set; }
    }

    public class GogResult
    {
        public GogGeometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public OpeningHours opening_hours { get; set; }
        public List<Photo> photos { get; set; }
        public string place_id { get; set; }
        public double rating { get; set; }
        public string reference { get; set; }
        public string scope { get; set; }
        public List<string> types { get; set; }
        public string vicinity { get; set; }
    }

    public class GoogleResponse
    {
        public List<object> html_attributions { get; set; }
        public string next_page_token { get; set; }
        public List<GogResult> results { get; set; }
        public string status { get; set; }
    }

    public class Location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
    }
    public class Result
    {
        public Geometry geometry { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string rating { get; set; }
        public string vicinity { get; set; }
    }

    public class GoogleBARObject
    {
        public List<Result> results { get; set; }
    }


    public class GoogleAirport
    {
        public List<AirportResult> results { get; set; }
        public string status { get; set; }
    }

    public class AirportResult
    {
        public AirportGeometry geometry { get; set; }
        public string name { get; set; }
        public string[] types { get; set; }
    }

    public class AirportGeometry
    {
        public AirportLocation location { get; set; }
    }

    public class AirportLocation
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }

}