﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class Images
    {
        public string mobile { get; set; }
        public string desktop { get; set; }
    }

    public class CarouselItem
    {
        public string imageUrl { get; set; }
        public Images images { get; set; }
        public string heading { get; set; }
        public string description { get; set; }
    }

    public class TemplatePageDesc
    {
        public string title { get; set; }
        public string breadCrumTitle { get; set; }
        public string content { get; set; }
        public List<CarouselItem> carouselItems { get; set; }
    }

    public class BottomOffersSectionIds
    {
        public string breadCrumTitle { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string MerchBoxes { get; set; }
    }

    public class HolidayOffersList
    {
        public string breadCrumTitle { get; set; }
        public string mainTitle { get; set; }
        public string tagLine { get; set; }
        public string Description { get; set; }
        public List<string> MerchBoxes { get; set; }
    }

    public class UserReview
    {
        public string reviewTitle { get; set; }
        public string reviewDescription { get; set; }
        public string customerName { get; set; }
        public string customerCountry { get; set; }
        public string userIcon { get; set; }
    }

    public class Images2
    {
        public string mobile { get; set; }
        public string desktop { get; set; }
    }

    public class BestDestination
    {
        public string title { get; set; }
        public Images2 images { get; set; }
        public string content { get; set; }
    }

    public class Faq
    {
        public string title { get; set; }
        public string description { get; set; }
    }

    public class AllInclusiveTemplateData
    {
        public string pageTitle { get; set; }
        public string pageUrl { get; set; }
        public string breadcrumb { get; set; }
        public bool isPublished { get; set; }
        public string metaDesc { get; set; }
        public string ogURL { get; set; }
        public string h1Tag { get; set; }
        public string h1CaptionText { get; set; }
        public string headerImgsize1920 { get; set; }
        public string headerImgsize1440 { get; set; }
        public string headerImgsize1280 { get; set; }
        public string headerImgsize1024 { get; set; }
        public string headerImgsize768 { get; set; }
        public string headerImgsize767 { get; set; }
        public string ovrVwbrdCrumTitle { get; set; }
        public TemplatePageDesc templatePageDesc { get; set; }
        public string topoffersSectionIds { get; set; }
        public BottomOffersSectionIds bottomOffersSectionIds { get; set; }
        public List<HolidayOffersList> holidayOffersList { get; set; }
        public string reviewBrdCrumTitle { get; set; }
        public List<UserReview> userReviews { get; set; }
        public string bestDestBrdCrumTitle { get; set; }
        public List<BestDestination> bestDestinations { get; set; }
        public string faqBrdCrumTitle { get; set; }
        public List<Faq> faqs { get; set; }
    }
}
