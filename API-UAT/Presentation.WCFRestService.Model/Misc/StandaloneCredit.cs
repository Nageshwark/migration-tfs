﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class CardExpiry
    {
        public int month { get; set; }
        public int year { get; set; }
    }

    public class _Card
    {
        public string cardNum { get; set; }
        public CardExpiry cardExpiry { get; set; }
    }

    public class _Profile
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
    }

    public class BillingDetails
    {
        public string street { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
    }

    public class StandaloneCredit
    {
        public string merchantRefNum { get; set; }
        public int amount { get; set; }
        public _Card card { get; set; }
        public _Profile profile { get; set; }
        public BillingDetails billingDetails { get; set; }
        public string customerIp { get; set; }
        public string description { get; set; }
    }
}
