﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineMinPriceTool
{
    public class HotelOnlyMinPriceToolQuery
    {
        public string id { get; set; }
        public string establishmentName { get; set; }
        public string description { get; set; }
        public string checkInDate { get; set; }
        public string destinationCode { get; set; }
        public List<int> boardType { get; set; }
        public int duration { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public List<int> ratings { get; set; }
        public int dateOffset { get; set; }
        public string establishmentCode { get; set; }
        public string destinationName { get; set; }
        public string websiteUrl { get; set; }
    }
    public class TTOMinPriceTool
    {
        public int roomId { get; set; }
        public string sectionId { get; set; }
        public string queryId { get; set; }
        public double totalMinPrice { get; set; }
        public double perNightMinPrice { get; set; }
        public string lastUpdated { get; set; }
        public string image { get; set; }
        public string hotelLocation { get; set; }
        public string starRating { get; set; }
        public string tripAdvisorRating { get; set; }
        public string reviewCount { get; set; }
        public string boardBasis { get; set; }
        public string displayParentRegion { get; set; }
        public string displaySectionName { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public HotelOnlyMinPriceToolQuery query { get; set; }
        public string updatedBy { get; set; }
    }

    public class HotelOnlyMinPriceOffer
    {
        public int roomId { get; set; }
        public string adults { get; set; }
        public string children { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public string starRatings { get; set; }
        public string duration { get; set; }
        public string destinationCode { get; set; }
        public string destinationName { get; set; }
        public string priceMin { get; set; }
        public string priceMax { get; set; }
        public string sort { get; set; }
        public string establishmentCode { get; set; }
        public int establishmentId { get; set; }
        public int mhid { get; set; }
        public string boardType { get; set; }
        public string queryId { get; set; }
        public string sectionId { get; set; }
        public string dateOffSet { get; set; }
        public string totalMinPrice { get; set; }
        public string perNightMinPrice { get; set; }
        public string description { get; set; }
        public string establishmentName { get; set; }
        public string ID { get; set; }
        public string displaySectionName { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
    }
}
