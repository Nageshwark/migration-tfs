﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{

    [XmlRoot(ElementName = "Fee", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Fee
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "Fees", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Fees
    {
        [XmlElement(ElementName = "Fee", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Fee> Fee { get; set; }
    }

    [XmlRoot(ElementName = "Rate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Rate
    {
        [XmlElement(ElementName = "Fees", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Fees Fees { get; set; }
    }

    [XmlRoot(ElementName = "Rates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Rates
    {
        [XmlElement(ElementName = "Rate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Rate Rate { get; set; }
    }

    [XmlRoot(ElementName = "RoomRateDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRateDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "RoomRateAdditionalData", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRateAdditionalData
    {
        [XmlElement(ElementName = "IsBidingRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsBidingRate { get; set; }
        [XmlElement(ElementName = "IsDirectPayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsDirectPayment { get; set; }
        [XmlElement(ElementName = "IsOpaqueRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsOpaqueRate { get; set; }
        [XmlElement(ElementName = "IsRefundableRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsRefundableRate { get; set; }
        [XmlElement(ElementName = "IsSpecialRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string IsSpecialRate { get; set; }
    }

    [XmlRoot(ElementName = "TPA_Extensions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TPA_ExtensionsResponse
    {
        [XmlElement(ElementName = "RoomRateAdditionalData", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomRateAdditionalData RoomRateAdditionalData { get; set; }
    }

    [XmlRoot(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Total
    {
        [XmlElement(ElementName = "TPA_Extensions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TPA_Extensions TPA_Extensions { get; set; }
        [XmlAttribute(AttributeName = "AmountAfterTax")]
        public string AmountAfterTax { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "RoomRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRate
    {
        [XmlElement(ElementName = "Rates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Rates Rates { get; set; }
        [XmlElement(ElementName = "RoomRateDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomRateDescription RoomRateDescription { get; set; }
        [XmlElement(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Total Total { get; set; }
        [XmlAttribute(AttributeName = "RoomTypeCode")]
        public string RoomTypeCode { get; set; }
        [XmlAttribute(AttributeName = "NumberOfUnits")]
        public string NumberOfUnits { get; set; }
        [XmlAttribute(AttributeName = "RoomID")]
        public string RoomID { get; set; }
    }

    [XmlRoot(ElementName = "RoomRates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRates
    {
        [XmlElement(ElementName = "RoomRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<RoomRate> RoomRate { get; set; }
    }

    [XmlRoot(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCountResponse
    {
        [XmlAttribute(AttributeName = "AgeQualifyingCode")]
        public string AgeQualifyingCode { get; set; }
        [XmlAttribute(AttributeName = "Age")]
        public string Age { get; set; }
        [XmlAttribute(AttributeName = "Count")]
        public string Count { get; set; }
    }

    [XmlRoot(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCountsResponse
    {
        [XmlElement(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<GuestCount> GuestCount { get; set; }
    }

    [XmlRoot(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class B2BTimeSpan
    {
        [XmlAttribute(AttributeName = "Start")]
        public string Start { get; set; }
        [XmlAttribute(AttributeName = "End")]
        public string End { get; set; }
    }

    [XmlRoot(ElementName = "HotelAmenity", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelAmenity
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "BasicPropertyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class BasicPropertyInfo
    {
        [XmlElement(ElementName = "HotelAmenity", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<HotelAmenity> HotelAmenity { get; set; }
        [XmlAttribute(AttributeName = "HotelCode")]
        public string HotelCode { get; set; }
        [XmlAttribute(AttributeName = "HotelCityCode")]
        public string HotelCityCode { get; set; }
        [XmlAttribute(AttributeName = "HotelName")]
        public string HotelName { get; set; }
        [XmlAttribute(AttributeName = "AreaID")]
        public string AreaID { get; set; }
    }

    [XmlRoot(ElementName = "RoomStay", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStay
    {
        [XmlElement(ElementName = "RoomRates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomRates RoomRates { get; set; }
        [XmlElement(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuestCounts GuestCounts { get; set; }
        [XmlElement(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public B2BTimeSpan TimeSpan { get; set; }
        [XmlElement(ElementName = "BasicPropertyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public BasicPropertyInfo BasicPropertyInfo { get; set; }
        [XmlAttribute(AttributeName = "RoomStayCandidateRPH")]
        public string RoomStayCandidateRPH { get; set; }
    }

    [XmlRoot(ElementName = "RoomStays", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStays
    {
        [XmlElement(ElementName = "RoomStay", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<RoomStay> RoomStay { get; set; }
    }
    [XmlRoot(ElementName = "Error", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Error
    {
        [XmlAttribute(AttributeName = "ShortText")]
        public string ShortText { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "Errors", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Errors
    {
        [XmlElement(ElementName = "Error", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Error Error { get; set; }
    }

    [XmlRoot(ElementName = "OTA_HotelAvailRS", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class B2BResponseAvailabilityOffers
    {
        [XmlElement(ElementName = "Success", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Success { get; set; }
        [XmlElement(ElementName = "Errors", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Errors Errors { get; set; }
        [XmlElement(ElementName = "RoomStays", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomStays RoomStays { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "Version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
