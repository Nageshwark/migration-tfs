﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class ReviewNPayParams
    {
        public string id { get; set; }
        public string value { get; set; }
    }

    public class ReviewNPayDocHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public ReviewNPayParams _source { get; set; }
    }

    public class Hits2
    {
        public int total { get; set; }
        public string max_score { get; set; }
        public List<ReviewNPayDocHit> hits { get; set; }
    }

    public class ReviewNPayInfoESDoc
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits2 hits { get; set; }
    }
    public class Status
    {
        public string status { get; set; }
    }

}
