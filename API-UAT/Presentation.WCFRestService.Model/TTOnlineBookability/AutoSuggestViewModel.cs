﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class AutoSuggestViewModel
    {   
        public Guid? locationId { get; set; }
        
        public string name { get; set; }
        
        public string category { get; set; }
        
        public string destinationId { get; set; }
    }
}
