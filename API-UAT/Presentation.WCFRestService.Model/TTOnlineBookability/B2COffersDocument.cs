﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class B2COffersDocument
    {
        public Guid availabilityId { get; set; }
        public Guid hotelCode { get; set; }
        public int hotelId { get; set; }
        public string hotelName { get; set; }
        public string defaultAirportCode { get; set; }
        public Guid destinationCode { get; set; }
        public int TTRegionId { get; set; }
        public int TTDestinationId { get; set; }
        public string destinationName { get; set; }
        public string parentDestinationCode { get; set; }
        public string parentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string countryName { get; set; }
        public List<object> hotelFacilities { get; set; }
        public List<object> roomFacilities { get; set; }
        public List<object> hotelGeneralInfo { get; set; }
        public int rank { get; set; }
        public int mhid { get; set; }
        public int starRating { get; set; }
        public string tripAdvisorId { get; set; }
        public double averageRating { get; set; }
        public int reviewCount { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public int roomNumber { get; set; }
        public int roomId { get; set; }
        public int boardType { get; set; }
        public string boardDescription { get; set; }
        public string roomDescription { get; set; }
        public double totalPrice { get; set; }
        public double perNightPrice { get; set; }
        public double highestPrice { get; set; }
        public double perNightHighestPrice { get; set; }
        public string currencyCode { get; set; }
        public bool isBidingRate { get; set; }
        public bool IsActiveAccommodationAdminFee { get; set; }
        public bool IsOpaqueRate { get; set; }
        public bool IsCardChargeApplicable { get; set; }
        public bool IsDuplicateRate { get; set; }
        public bool IsEligibleForDeposit { get; set; }
        public bool IsNonRefundable { get; set; }
        public bool IsSpecialRate { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public string timeStamp { get; set; }
        public string providerEdiCode { get; set; }
        public string providerEstablishmentCode { get; set; }
        public string providerName { get; set; }
        public double providerPrice { get; set; }
        public string providerCurrencyCode { get; set; }
        public string providerRoomCode { get; set; }
        public string hotelSEOFriendlyName { get; set; }
        public string destinationSEOFriendlyName { get; set; }
        public string guestInfoSearchRequest { get; set; }
        public string roomTypeSearch { get; set; }
        public bool isDirectFODOffer { get; set; }
    }
}
