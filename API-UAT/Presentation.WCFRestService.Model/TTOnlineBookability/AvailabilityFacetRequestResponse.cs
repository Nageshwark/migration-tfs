﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class FacetsRequestResponseShards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class FacetsRequestResponseHits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<object> hits { get; set; }
    }

    public class NoOfHotels
    {
        public int value { get; set; }
    }

    public class FRRHotelBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public NoOfHotels NoOfHotels { get; set; }
    }

    public class FRRHotel
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<FRRHotelBucket> buckets { get; set; }
    }

    public class NoOfResorts
    {
        public int value { get; set; }
    }

    public class FRRResortsBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public NoOfResorts NoOfResorts { get; set; }
    }

    public class FRRResorts
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<FRRResortsBucket> buckets { get; set; }
    }

    public class FRRPriceMin
    {
        public double? value { get; set; }
    }

    public class FRRPriceMax
    {
        public double? value { get; set; }
    }

    public class NoOfAirports
    {
        public int value { get; set; }
    }

    public class FRRAirportBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public NoOfAirports NoOfAirports { get; set; }
    }

    public class FRRAirports
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<FRRAirportBucket> buckets { get; set; }
    }
    public class FRRStarRatingBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public NoOfResorts NoOfStarHotels { get; set; }
    }
    public class FRRTripAdvisorRatingBucket
    {
        public float key { get; set; }
        public int doc_count { get; set; }
        public NoOfResorts NoOfTARatedHotels { get; set; }
    }
    public class FRRStarRating
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<FRRStarRatingBucket> buckets { get; set; }
    }
    public class FRRTripAdvisorRating
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<FRRTripAdvisorRatingBucket> buckets { get; set; }
    }

    public class FacetsRequestResponseAggregations
    {
        public FRRHotel hotel { get; set; }
        public FRRResorts resorts { get; set; }
        public FRRPriceMin price_min { get; set; }
        public FRRPriceMax price_max { get; set; }
        public FRRAirports airports { get; set; }
        public FRRStarRating rating { get; set; }
        public FRRTripAdvisorRating tripAdvisorRating { get; set; }
    }

    public class AvailabilityFacetsRequestResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public FacetsRequestResponseShards _shards { get; set; }
        public FacetsRequestResponseHits hits { get; set; }
        public FacetsRequestResponseAggregations aggregations { get; set; }
    }
}
