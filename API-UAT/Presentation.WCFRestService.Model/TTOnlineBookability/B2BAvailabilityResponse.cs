﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    [XmlRoot(ElementName = "GetAvailabilityResponseResponse", Namespace = "http://tempuri.org/")]
    public class GetAvailabilityResponseResponse
    {
        [XmlElement(ElementName = "GetAvailabilityResponseResult", Namespace = "http://tempuri.org/")]
        public string GetAvailabilityResponseResult { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ResponseBody
    {
        [XmlElement(ElementName = "GetAvailabilityResponseResponse", Namespace = "http://tempuri.org/")]
        public GetAvailabilityResponseResponse GetAvailabilityResponseResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public ResponseBody Body { get; set; }
        [XmlAttribute(AttributeName = "s", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S { get; set; }
    }
}
