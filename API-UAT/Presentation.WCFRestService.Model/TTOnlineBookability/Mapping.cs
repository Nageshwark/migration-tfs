﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class AlphaTTDestination
    {
        public Guid AlphaDestinationCode { get; set; }
        public int TTextDestinationID { get; set; }
    }
}
