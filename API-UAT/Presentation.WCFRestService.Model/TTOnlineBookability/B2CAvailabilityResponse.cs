﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class RoomOccupancy
    {
        public int AdultsCount { get; set; }
        public int ChildrenAndInfantsCount { get; set; }
        public int RoomNumber { get; set; }
    }

    public class AccommodationAdminFee
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class PaymentToTake
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class Price
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class ProviderPrice
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class _Room
    {
        public AccommodationAdminFee AccommodationAdminFee { get; set; }
        public string BoardDescription { get; set; }
        public int BoardType { get; set; }
        public object Discount { get; set; }
        public double ExchangeRate { get; set; }
        public HighestPrice HighestPrice { get; set; }
        public bool IsActiveAccommodationAdminFee { get; set; }
        public bool IsBindingRate { get; set; }
        public bool IsCardChargeApplicable { get; set; }
        public bool IsDuplicateRate { get; set; }
        public bool IsEligibleForDeposit { get; set; }
        public bool IsLowestPrice { get; set; }
        public bool IsNonRefundable { get; set; }
        public bool IsOpaqueRate { get; set; }
        public double Margin { get; set; }
        public int PaymentModel { get; set; }
        public PaymentToTake PaymentToTake { get; set; }
        public Price Price { get; set; }
        public string ProviderEdiCode { get; set; }
        public string ProviderEstablishmentCode { get; set; }
        public string ProviderName { get; set; }
        public ProviderPrice ProviderPrice { get; set; }
        public string ProviderRoomCode { get; set; }
        public int RateType { get; set; }
        public string RoomDescription { get; set; }
        public int RoomIdAlpha2 { get; set; }
        public int RoomNumber { get; set; }
    }

    public class HighestPrice
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class RoomsCheapestPrice
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class RoomsCheapestPricePerNight
    {
        public double Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class EstablishmentFacility
    {
        public Guid Id { get; set; }
        public bool IsShortListed { get; set; }
        public string Name { get; set; }
        public int Reference { get; set; }
    }
    public class AvailabilityResult
    {
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public Guid DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string DestinationSeoFriendlyName { get; set; }
        public string EstablishmentDescription { get; set; }
        public List<EstablishmentFacility> EstablishmentFacilities { get; set; }
        public Guid EstablishmentId { get; set; }
        public string EstablishmentName { get; set; }
        public int EstablishmentReviewAverageScore { get; set; }
        public int EstablishmentReviewCount { get; set; }
        public string EstablishmentSeoFriendlyName { get; set; }
        public int EstablishmentStarRating { get; set; }
        public string EstablishmentTopImageUrl { get; set; }
        public double EstablishmentTripAdvisorAverageScore { get; set; }
        public int EstablishmentTripAdvisorReviewCount { get; set; }
        public int EstablishmentType { get; set; }
        public bool IsEligibleForOnePoundDeposit { get; set; }
        public List<RoomOccupancy> RoomOccupancies { get; set; }
        public List<_Room> Rooms { get; set; }
        public RoomsCheapestPrice RoomsCheapestPrice { get; set; }
        public RoomsCheapestPricePerNight RoomsCheapestPricePerNight { get; set; }
        public int RoomsCount { get; set; }
    }

    public class B2CAvailabilityResponse
    {
        public object AvailabilityFilterDetails { get; set; }
        public Guid AvailabilityId { get; set; }
        public List<AvailabilityResult> AvailabilityResults { get; set; }
        public int AvailabilityStatus { get; set; }
        public int AvailabilityTotalResultsCount { get; set; }
        public object PromotionalCode { get; set; }
        public DateTime StartDate { get; set; }
    }
}
