﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Online.RoomValuation
{
    public class Fee
    {
        public string Code { get; set; }
        public string Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class Fees
    {
        public List<Fee> Fee { get; set; }
    }

    public class Rate
    {
        public Fees Fees { get; set; }
    }

    public class Rates
    {
        public Rate Rate { get; set; }
    }

    public class RoomRateDescription
    {
        public string Text { get; set; }
    }

    public class RoomRateAdditionalData
    {
        public string IsBidingRate { get; set; }
        public string IsDirectPayment { get; set; }
        public string IsOpaqueRate { get; set; }
        public string IsRefundableRate { get; set; }
        public string IsSpecialRate { get; set; }
    }

    public class TPA_Extensions
    {
        public RoomRateAdditionalData RoomRateAdditionalData { get; set; }
    }

    public class Total
    {
        public TPA_Extensions TPA_Extensions { get; set; }
        public string AmountAfterTax { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class RoomRate
    {
        public Rates Rates { get; set; }
        public RoomRateDescription RoomRateDescription { get; set; }
        public Total Total { get; set; }
        public string RoomTypeCode { get; set; }
        public string NumberOfUnits { get; set; }
        public string RoomID { get; set; }
    }

    public class RoomRates
    {
        public RoomRate RoomRate { get; set; }
    }

    public class GuestCount
    {
        public string AgeQualifyingCode { get; set; }
        public string Age { get; set; }
        public string Count { get; set; }
    }
    public class GuestCounts
    {
        public List<GuestCount> GuestCount { get; set; }
    }
    
    public class TimeSpan
    {        
        public string Start { get; set; }
        public string End { get; set; }
    }
    
    public class PenaltyDescription
    {        
        public string Text { get; set; }
    }
    
    public class CancelPenalty
    {       
        public PenaltyDescription PenaltyDescription { get; set; }
    }
    
    public class CancelPenalties
    {        
        public List<CancelPenalty> CancelPenalty { get; set; }
    }
    
    public class HotelAmenity
    {        
        public string Code { get; set; }
    }
    
    public class BasicPropertyInfo
    {       
        public List<HotelAmenity> HotelAmenity { get; set; }        
        public string HotelCode { get; set; }        
        public string HotelCityCode { get; set; }        
        public string HotelName { get; set; }        
        public string AreaID { get; set; }
    }
    
    public class RoomStay
    {        
        public List<RoomRates> RoomRates { get; set; }        
        public GuestCounts GuestCounts { get; set; }        
        public TimeSpan TimeSpan { get; set; }
        public CancelPenalties CancelPenalties { get; set; }        
        public BasicPropertyInfo BasicPropertyInfo { get; set; }        
        public string RoomStayCandidateRPH { get; set; }
    }
    
    public class RoomStays
    {        
        public List<RoomStay> RoomStay { get; set; }

        public string RoomUniqueId { get; set; }
    }
}
