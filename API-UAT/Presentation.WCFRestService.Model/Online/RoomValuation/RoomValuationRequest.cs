﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Online.RoomValuation
{
    public class RoomValuationRequest
    {
        public string RoomID { get; set; }
    }
}
