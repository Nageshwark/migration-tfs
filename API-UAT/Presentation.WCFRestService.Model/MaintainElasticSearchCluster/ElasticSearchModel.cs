﻿using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.MaintainElasticSearchCluster
{
    public class ElasticSearchShardsModel
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class ElasticSearchSourceModel
    {
        public int adults { get; set; }
        public IList<string> boardType { get; set; }
        public int children { get; set; }
        public DateTime? departureDate { get; set; }
        public object dateMin { get; set; }
        public object dateMax { get; set; }
        public int departureIds { get; set; }
        public IList<string> airportIds { get; set; }
        public IList<string> regionIds { get; set; }
        public int destinationIds { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int ttssCount { get; set; }
        public int parentDepartureIds { get; set; }
        public int priceMax { get; set; }
        public int priceMin { get; set; }
        public double ttssResponseTime { get; set; }
        public string destinationType { get; set; }
        public bool isRegion { get; set; }
        public int taRating { get; set; }
        public int channelId { get; set; }
        public int labelId { get; set; }
        public bool isCacheHit { get; set; }
        public IList<string> ratings { get; set; }
        public IList<string> tradingNameIds { get; set; }
        public DateTime timeStamp { get; set; }
        public string ttssURL { get; set; }
        public double indexingTime { get; set; }
        public double decoratingTime { get; set; }
        public int offersFound { get; set; }
        public int offersDecorated { get; set; }
        public int offersInserted { get; set; }
        public IList<string> hotelKeys { get; set; }
        public IList<object> usersPreferredHotelKeys { get; set; }
        public bool skipFacets { get; set; }
        public string endPoint { get; set; }
        public IList<object> hotelKeysToExclude { get; set; }
        public object sort { get; set; }
        public bool isHotelKeys { get; set; }
        public int facetsTrails { get; set; }
        public int offersReturned { get; set; }
        public string searchURL { get; set; }
    }

    public class ElasticSearchHitModel
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public float _score { get; set; }
        public ElasticSearchSourceModel _source { get; set; }
    }

    public class ElasticSearchHitsModel
    {
        public int total { get; set; }
        public float max_score { get; set; }
        public IList<ElasticSearchHitModel> hits { get; set; }
    }

    public class ElasticSearchModel
    {
        public string _scroll_id { get; set; }
        public int took { get; set; }
        public bool timed_out { get; set; }
        public ElasticSearchShardsModel _shards { get; set; }
        public ElasticSearchHitsModel hits { get; set; }
    }
}
