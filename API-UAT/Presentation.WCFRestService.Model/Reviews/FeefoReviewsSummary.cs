﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class FeefoMerchant
    {
        public string identifier { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string logo { get; set; }
        public string review_url { get; set; }
    }

    public class Meta
    {
        public int count { get; set; }
        public int pages { get; set; }
        public int page_size { get; set; }
    }

    public class FeefoService
    {
        public int count { get; set; }
        public int one_star_count { get; set; }
        public int two_star_count { get; set; }
        public int three_star_count { get; set; }
        public int four_star_count { get; set; }
        public int five_star_count { get; set; }
    }

    public class FeefoRating
    {
        public double min { get; set; }
        public double max { get; set; }
        public double rating { get; set; }
        public FeefoService service { get; set; }
    }

    public class FeefoReviewsSummary
    {
        public FeefoMerchant merchant { get; set; }
        public Meta meta { get; set; }
        public FeefoRating rating { get; set; }
    }
}
