﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class TTSSResultsParams
    {
        public int ttssCount { get; set; }
        public double ttssResponseTime { get; set; }
        public string ttssURL { get; set; }
        public double indexingTime { get; set; }
        public double decoratingTime { get; set; }
        public int offersFound { get; set; }
        public int offersDecorated { get; set; }
        public int offersInserted { get; set; }
        public int facetsTrials { get; set; }
        public bool isBulkInsertionOnRetry { get; set; }
        public int resetStage { get; set; }
    }
}
