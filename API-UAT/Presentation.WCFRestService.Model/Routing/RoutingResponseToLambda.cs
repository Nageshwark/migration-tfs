﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Routing
{
    public class RoutingResponseToLambda
    {
        public int Count { get; set; }
        public string ErrorMessage { get; set; }

        public RoutingResponseToLambda()
        {
            this.Count = 0;
            this.ErrorMessage = String.Empty;
        }
    }
}
