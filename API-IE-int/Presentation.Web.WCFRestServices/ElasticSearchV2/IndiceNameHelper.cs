﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public static class IndiceNameHelper
    {
        public static string GetOffersIndex(APISearchRequest apiSearchRequest)
        {
            string esOffersIndiceKey= "OffersIndice";
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                esOffersIndiceKey += apiSearchRequest.countrySite.ToUpper();
            }
            return ConfigurationManager.AppSettings[esOffersIndiceKey];
        }

        public static string GetRequestIndex(APISearchRequest apiSearchRequest)
        {
            string esRequestIndiceKey = "RequestIndice";
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                esRequestIndiceKey += apiSearchRequest.countrySite.ToUpper();
            }
            return ConfigurationManager.AppSettings[esRequestIndiceKey];
        }


        public static string GetOffersIndex(string countrySite)
        {
            string esOffersIndiceKey = "OffersIndice";
            if (countrySite != Global.COUNTRY_SITE_UK)
            {
                esOffersIndiceKey += countrySite.ToUpper();
            }
            return ConfigurationManager.AppSettings[esOffersIndiceKey];
        }

        public static string GetRequestIndex(string countrySite)
        {
            string esRequestIndiceKey = "RequestIndice";
            if (countrySite != Global.COUNTRY_SITE_UK)
            {
                esRequestIndiceKey += countrySite.ToUpper();
            }
            return ConfigurationManager.AppSettings[esRequestIndiceKey];
        }

    }
}