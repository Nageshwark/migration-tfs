﻿using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.Web.WCFRestServices.ElasticSearchV2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Presentation.Web.WCFRestServices.Directional_Selling_Chat
{
    public class DirectionalSelling
    {
        public static bool InsertMessagesInES(ChatMessage chatMessage)
        {
            string updateChatQuery = QueryBuilder.GetUpdateDirectionalSellingChatMessageQuery(chatMessage);
            try
            {
                HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-Chat-ClusterUrl"] + ConfigurationManager.AppSettings["ChatIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ChatIndiceDataType"] + "/" + chatMessage.sessionId + "/" + "_update");
                httpWebSearchRequest.ContentType = "application/json";
                httpWebSearchRequest.Method = "POST";
                httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                {
                    string searchRequestParamsResponse = updateChatQuery;
                    streamWriter.Write(searchRequestParamsResponse);
                    streamWriter.Flush();
                }
                var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                if (httpSearchResponse != null)
                {
                    httpSearchResponse.Close();
                    httpSearchResponse.Dispose();
                }
            }
            catch (WebException ex)
            {
                ErrorLogger.Log(string.Format("Web exception occured while inserting Directional Selling Message into ES. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                return false;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Exception occured while inserting Directional Selling Message into ES. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                return false;
            }
            return true;
        }
    }
}