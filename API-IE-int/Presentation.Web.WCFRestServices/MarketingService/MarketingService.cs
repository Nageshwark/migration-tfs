﻿using Presentation.WCFRestService.Model;

namespace Presentation.Web.WCFRestServices
{
    public class MarketingService
    {
        public static NewsLetterSubscriptionStatus GetNewsLetterSubscription(string name, string email, string phone, string airport)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            IMarketingService marketingService = new SmartFocusServices();
            status = marketingService.GetNewsLetterSubscription(name, email, phone, airport);            
            return status;
        }

        public static string GetNewsLetterSubscriberByEmail(string email)
        {
            IMarketingService marketingService = new SmartFocusServices();
            return marketingService.GetNewsLetterSubscriberByEmail(email);
        }
    }
}