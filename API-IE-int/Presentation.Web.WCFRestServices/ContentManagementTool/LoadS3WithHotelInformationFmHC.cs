﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.ContentManagementTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;

namespace Presentation.Web.WCFRestServices.ContentManagementTool
{
    public class LoadS3WithHotelInformationFmHC
    {
        public static string LoadS3WithHotelInformation(string mhids) {
            // Get list of mhids already already present on S3
            List<string> mhidsInS3 = new List<string>();

            IAmazonS3 client;
            string accessKeyID = "AKIAIOLBY2H5PCCKK6DA";
            string secretAccessKeyID = "fBgn5M/a0Vh4FXrOTIGj2j0ZSPw5ZhGS6UcKtoXE";
            string bucketName = ConfigurationManager.AppSettings["MasterHotelAWSBucket"];
            List<string> mhidsForWhichDataToBeDownloaded = new List<string>();
            //List<string> mhidsNotAvailableOnHCEndpoint = new List<string>();

            List<S3Object> s3Objects = new List<S3Object>();
        
            using(client = new AmazonS3Client(accessKeyID, secretAccessKeyID, Amazon.RegionEndpoint.EUWest1))
            {
                ListObjectsRequest request = new ListObjectsRequest();
                request.BucketName = bucketName;
                request.Prefix = ConfigurationManager.AppSettings["MasterHotelFolderPath"] +  "/";
                do
                {
                    ListObjectsResponse response = client.ListObjects(request);

                    s3Objects = response.S3Objects;

                    foreach(S3Object obj in s3Objects)
                    {
                        mhidsInS3.Add(obj.Key.Replace(request.Prefix, "").Replace("/","").Replace(".json","").Split('/')[0]);
                    }
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }
                } while (request != null);
            }
            // Check if the mhids entered are already in S3.
            // If YES, Do not Download it, else download Hotel Inforamtion for it.
            var testingMhids = mhids.Split(',');
            foreach(string mhid in testingMhids)
            {
                string result = mhidsInS3.FirstOrDefault(s => s == mhid);
                if(string.IsNullOrWhiteSpace(result))
                {
                    mhidsForWhichDataToBeDownloaded.Add(mhid);
                }
            }

            Dictionary<string, HCHotelInformation> hotelInformationFromHCEndpoint =  DownloadFromHCEndpoint(mhidsForWhichDataToBeDownloaded);
            var mhidsNotAvailableOnHCEndpoint = from x in hotelInformationFromHCEndpoint where (x.Value.HotelinfoResult != null && x.Value.HotelinfoResult.HotelId == 0) select x.Key;
            Task.Run(() => UploadToS3(hotelInformationFromHCEndpoint));

            return string.Join(",", mhidsNotAvailableOnHCEndpoint);
        }

        private static Dictionary<string, HCHotelInformation> DownloadFromHCEndpoint(List<string> mhidsForWhichDataToBeDownloaded)
        {
            Dictionary<string, HCHotelInformation> hotelInformation = new Dictionary<string, HCHotelInformation>();
            foreach (string mhid in mhidsForWhichDataToBeDownloaded)
            {
                var hotelInfoResponse = Utilities.ExecuteGetWebRequest("http://contenthub.hotelcompany.co/HotelService.svc/Hotelinfo/" + mhid);
                var hcResp = JsonConvert.DeserializeObject<HCHotelInformation>(hotelInfoResponse);
                
                if(!hotelInformation.ContainsKey(mhid))
                {
                    hotelInformation.Add(mhid, hcResp);
                }
            }
            return hotelInformation;
        }

        private static void UploadToS3(Dictionary<string, HCHotelInformation> hotelInformationFromHCEndpoint)
        {
            var imgCount = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/MHIDtoImgCount.json"));

            string bucketName = ConfigurationManager.AppSettings["MasterHotelAWSBucket"];

            foreach (KeyValuePair<string, HCHotelInformation> hotelInfo in hotelInformationFromHCEndpoint)
            {
                HCHotelInformation hcResp = hotelInfo.Value;
                string mhid = hotelInfo.Key;

                if(hcResp != null && hcResp.HotelinfoResult != null && hcResp.HotelinfoResult.HotelId != 0)
                {
                    string latit = string.Empty;
                    string longi = string.Empty;
                    MHIDStaticInfo mhInfo = new MHIDStaticInfo();
                    TripAdvisor ta = new TripAdvisor();
                    mhInfo.TripAdvisor = ta;
                    MHIDSource source = new MHIDSource();
                    source.MasterHotelId = int.Parse(mhid);
                    source.BuildingName = hcResp.HotelinfoResult.BuildingName;
                    source.DestinationCode = hcResp.HotelinfoResult.DestinationCode;
                    source.DestinationName = hcResp.HotelinfoResult.DestinationName;
                    source.PopularHotel = hcResp.HotelinfoResult.PopularHotel;
                    source.RatingLevel = hcResp.HotelinfoResult.RatingLevel;
                    source.Rating = hcResp.HotelinfoResult.Rating;
                    source.GIATAID = string.Empty;
                    source.FriendlyUrl = hcResp.HotelinfoResult.FriendlyUrl;
                    source.RegionTTSSLabelD = hcResp.HotelinfoResult.RegionTTSSLabelD;
                    MHIDLocation loc = new MHIDLocation();
                    if (hcResp.HotelinfoResult.Location != null)
                    {
                        loc.lat = hcResp.HotelinfoResult.Location.lat;
                        latit = hcResp.HotelinfoResult.Location.lat.ToString();
                        loc.lon = hcResp.HotelinfoResult.Location.lon;
                        longi = hcResp.HotelinfoResult.Location.lon.ToString();
                    }
                    source.Location = loc;
                    MHIDAddress add = new MHIDAddress();
                    add.AddressLineOne = hcResp.HotelinfoResult.Address.AddressLineOne;
                    add.AddressLineTwo = hcResp.HotelinfoResult.Address.AddressLineTwo;
                    add.AddressLineThree = hcResp.HotelinfoResult.Address.AddressLineThree;
                    add.AddressLineFour = hcResp.HotelinfoResult.Address.AddressLineFour;
                    add.City = hcResp.HotelinfoResult.Address.City;
                    add.PostCode = hcResp.HotelinfoResult.Address.PostCode;
                    source.Address = add;
                    MHIDContact con = new MHIDContact();
                    con.EMailId = hcResp.HotelinfoResult.Contact.EMailId;
                    con.Telephone = hcResp.HotelinfoResult.Contact.Telephone;
                    con.Fax = hcResp.HotelinfoResult.Contact.Fax;
                    source.Contact = con;
                    MHIDRegion reg = new MHIDRegion();
                    if (hcResp.HotelinfoResult.Region != null)
                    {
                        reg.AirportCodes = hcResp.HotelinfoResult.Region.AirportCodes;
                        reg.GrandRegionId = hcResp.HotelinfoResult.Region.GrandRegionId;
                        reg.GrandRegionLink = hcResp.HotelinfoResult.Region.GrandRegionLink;
                        reg.GrandRegionName = hcResp.HotelinfoResult.Region.GrandRegionName;
                        reg.ParentRegionId = hcResp.HotelinfoResult.Region.ParentRegionId;
                        reg.ParentRegionLink = hcResp.HotelinfoResult.Region.ParentRegionLink;
                        reg.ParentRegionName = hcResp.HotelinfoResult.Region.ParentRegionName;
                        reg.RegionId = hcResp.HotelinfoResult.Region.RegionId;
                        reg.Reglevel = hcResp.HotelinfoResult.Region.Reglevel;
                        reg.Regname = hcResp.HotelinfoResult.Region.Regname;
                        reg.ResortId = hcResp.HotelinfoResult.Region.ResortId;
                        source.MasterResortId = hcResp.HotelinfoResult.Region.ResortId;
                        source.MasterResortName = hcResp.HotelinfoResult.Region.Regname;
                    }
                    source.Region = reg;
                    if (imgCount.ContainsKey(mhid))
                    {
                        source.ImageCount = imgCount[mhid];
                    }
                    else
                    {
                        source.ImageCount = 0;
                    }
                    source.Description = string.Empty;
                    if (hcResp.HotelinfoResult.Descriptions != null)
                    {
                        foreach (HCDescription dec in hcResp.HotelinfoResult.Descriptions)
                        {
                            source.Description = source.Description + "<h3>" + dec.DescriptionHeader + "</h3><p>" + dec.DescriptionText + "</p>";
                        }
                    }
                    source.Features = new List<object>();
                    foreach (HCFeature fec in hcResp.HotelinfoResult.Features)
                    {
                        for (int i = 0; i < fec.FeaturesList.Count; i++)
                        {
                            source.Features.Add(fec.FeaturesList[i]);
                        }
                    }
                    mhInfo._source = source;

                    Utilities.PutObjectToS3(bucketName, mhid + ".json", mhInfo, ConfigurationManager.AppSettings["MasterHotelFolderPath"] + "/");
                }
            }
        }
    }
}