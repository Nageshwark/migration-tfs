﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Artirix;
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.ServiceModel.Web;
using System.Threading.Tasks;

namespace Presentation.Web.WCFRestServices
{
    public class ArtirixImageSearch
    {
        public static Holidays GetAtxImgSrc(string artirixSearchUrl, string platform)
        {
            Holidays holidays = new Holidays();            

            try
            {
                Int32 minCountOfImage = Global.minCountOfImage;
                Dictionary<string, int> iffToImageCountList = Global.iffToImageCountList;
                var artirixRespStr = string.Empty;
                if (platform.ToLower() == "mobile")
                {
                    artirixRespStr = Utilities.ExecuteMobileArtirixGetWebRequest(artirixSearchUrl);
                }
                else
                {
                    artirixRespStr = Utilities.ExecuteArtirixGetWebRequest(artirixSearchUrl);
                }
                holidays = JsonConvert.DeserializeObject<Holidays>(artirixRespStr);
                string imgSourceURLFormat = Global.imgSourceURLFormat;
                string artxImgSourceURLFormat = Global.artxImgSourceURLFormat;
                ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };

                if (holidays == null || holidays.offers == null)
                {
                    throw new WebFaultException<string>("Artirix returned an empty response", System.Net.HttpStatusCode.InternalServerError);
                }

                 foreach (Offer offr in holidays.offers)
                //Parallel.ForEach(holidays.offers, parrOptions, offr =>
                {
                   
                    if (offr == null || offr.hotel == null)
                    {
                        continue;
                    }
                    offr.hotel.mobileimages = new List<string>();
                    offr.hotel.thumbnailimages = new List<string>();

                    int hoteliffCount = 0;

                    if (Global.iffToMHIDMap.ContainsKey(offr.hotel.iff) && Global.iffToMHIDMap[offr.hotel.iff].ImageCount >= minCountOfImage)
                    {
                        hoteliffCount = Global.iffToMHIDMap[offr.hotel.iff].ImageCount;
                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                        {
                            offr.hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + Global.iffToMHIDMap[offr.hotel.iff].MasterHotelID + "/" + hotelIndex + ".jpg"));
                            offr.hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + Global.iffToMHIDMap[offr.hotel.iff].MasterHotelID + "/" + hotelIndex + ".jpg"));
                        }
                    }                    
                    else
                    {
                        if (offr.hotel.images == null || offr.hotel.images.Count == 0)
                        {
                            offr.hotel.mobileimages = null;
                            continue;
                        }                        
                        for (int item = 0; item < offr.hotel.images.Count; item++)
                        {
                            offr.hotel.mobileimages.Add(string.Format(artxImgSourceURLFormat, offr.hotel.images[item]));
                            offr.hotel.thumbnailimages.Add(string.Format(ConfigurationManager.AppSettings["ArtirixThumbnailPath"], offr.hotel.images[item]));
                        }                        
                    }
                };

                return holidays;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetAtxImgSrc:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }
        public static Holidays GetAsyncAtxImgSrc(string artirixSearchUrl)
        {
            Holidays holidays = new Holidays();
            try
            {
                Int32 minCountOfImage = Global.minCountOfImage;
                Dictionary<string, int> iffToImageCountList = Global.iffToImageCountList;
               // var artirixRespStr = string.Empty;
                //artirixRespStr = Utilities.ExecuteArtirixGetWebRequest(artirixSearchUrl);
                holidays = JsonConvert.DeserializeObject<Holidays>(artirixSearchUrl);
                string imgSourceURLFormat = Global.imgSourceURLFormat;
                string artxImgSourceURLFormat = Global.artxImgSourceURLFormat;
                ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };

                if (holidays == null || holidays.offers == null)
                {
                    throw new WebFaultException<string>("Artirix returned an empty response", System.Net.HttpStatusCode.InternalServerError);
                }

                foreach (Offer offr in holidays.offers)
                //Parallel.ForEach(holidays.offers, parrOptions, offr =>
                {
                    if (offr == null || offr.hotel == null)
                    {
                        continue;
                    }
                    offr.hotel.mobileimages = new List<string>();
                    offr.hotel.thumbnailimages = new List<string>();

                    int hoteliffCount = 0;

                    if (Global.iffToMHIDMap.ContainsKey(offr.hotel.iff) && Global.iffToMHIDMap[offr.hotel.iff].ImageCount >= minCountOfImage)
                    {
                        hoteliffCount = Global.iffToMHIDMap[offr.hotel.iff].ImageCount;
                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                        {
                            offr.hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + Global.iffToMHIDMap[offr.hotel.iff].MasterHotelID + "/" + hotelIndex + ".jpg"));
                            offr.hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + Global.iffToMHIDMap[offr.hotel.iff].MasterHotelID + "/" + hotelIndex + ".jpg"));
                        }
                    }
                    else
                    {
                        if (offr.hotel.images == null || offr.hotel.images.Count == 0)
                        {
                            offr.hotel.mobileimages = null;
                            continue;
                        }
                        for (int item = 0; item < offr.hotel.images.Count; item++)
                        {
                            offr.hotel.mobileimages.Add(string.Format(artxImgSourceURLFormat, offr.hotel.images[item]));
                            offr.hotel.thumbnailimages.Add(string.Format(ConfigurationManager.AppSettings["ArtirixThumbnailPath"], offr.hotel.images[item]));
                        }
                    }
                };

                return holidays;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetAtxImgSrc:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }
        
    }
    


}