﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Artirix;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Guides;
using Presentation.WCFRestService.Model.Misc;
using Presentation.Web.WCFRestServices.MyBooking;
using Presentation.Web.WCFRestServices.TopDog;
using Presentation.WCFRestService.Model.Reviews;
using Presentation.Web.WCFRestServices.Reviews;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Globalization;
using Presentation.Web.WCFRestServices.RedisDataAccessHelper;
using System.Threading.Tasks;
using Presentation.WCFRestService.Model.Flight.Artirix;
using System.Threading;
using System.Net.Http;
using System.ServiceModel;
using Presentation.Web.WCFRestServices.Google;

using Presentation.WCFRestService.Model.DeepLinks;
using Presentation.Web.WCFRestServices.LambdaHits;
using Presentation.Web.WCFRestServices.StaticHotelLanding;
using Amazon.CloudFront.Model;

using Authentication = Paysafe.ThreeDSecure.Authentications;
using EnrollmentChecks = Paysafe.ThreeDSecure.EnrollmentChecks;
using Authorization = Paysafe.CardPayments.Authorization;
using Paysafe;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.Web.WCFRestServices.ElasticSearchV2;
using Offer = Presentation.WCFRestService.Model.Artirix.Offer;
using static Presentation.WCFRestService.Model.ElasticSearchV2.SearchResponseParams;
using Destination = Presentation.WCFRestService.Model.Guides.Destination;
using Paysafe.CustomerVault;
using Presentation.WCFRestService.Model.CustomerSupport;
using Presentation.WCFRestService.Model.Utilities;
using Presentation.Web.WCFRestServices.CustomerSupport;
using Presentation.WCFRestService.Model.MaintainElasticSearchCluster;
using Amazon.SimpleEmail.Model;
using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon;
using Akamai.EdgeGrid.Auth;
using System.Text.RegularExpressions;
using Presentation.WCFRestService.Model.Utilities;
using Paysafe.CustomerVault;
using System.Data.SqlClient;
using System.Data;
using ServiceStack.Common;
using Newtonsoft.Json.Linq;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using Presentation.Web.WCFRestServices.TTOnlineBookability;
using System.Xml;
using System.Xml.Serialization;
using Presentation.Web.WCFRestServices.ValuationService;
using Presentation.Web.WCFRestServices.ContentManagementTool;
using Presentation.WCFRestService.Model.TTOnlineMinPriceTool;
using Presentation.Web.WCFRestServices.TTOnlineMinPriceTool;
using System.Net.Http.Headers;
using Presentation.Web.WCFRestServices.Directional_Selling_Chat;
using Presentation.WCFRestService.Model.TeleappliantReport;
using System.Net.Http.Headers;

namespace Presentation.Web.WCFRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GetJsonService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select GetJsonService.svc or GetJsonService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class GetJsonService : IGetJsonService
    {
        static string[] documentCodes = { "Admin.PaymentsSummary", "Admin.ATOL.Receipt", "Admin.RSI.Transfer", "Admin.RSI.Accom", "Admin.RSI.AdditionalExtras" };
        #region API Calls for Maintaining Elastic Search Cluster

        public void CreateRequestIndice()
        {
            Task.Factory.StartNew(BeginCreateRequestIndice);
        }

        private void BeginCreateRequestIndice()
        {
            CreateRequestIndiceJsonModel createRequestIndiceJsonModel = GetCreateRequestIndiceJsonModel();

            if (createRequestIndiceJsonModel == null)
            {
                createRequestIndiceJsonModel = new CreateRequestIndiceJsonModel();

                int connectTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ConnectTimeout"], out connectTimeout);

                createRequestIndiceJsonModel.ConnectTimeout = connectTimeout;
                createRequestIndiceJsonModel.ElasticSearchPassword = ConfigurationManager.AppSettings["esPassword"];

                createRequestIndiceJsonModel.ElasticSearchUserName = ConfigurationManager.AppSettings["esUserName"];
                createRequestIndiceJsonModel.FailureSubjectForInt = ConfigurationManager.AppSettings["FailureSubjectForInt"];
                createRequestIndiceJsonModel.FailureSubjectForProd = ConfigurationManager.AppSettings["FailureSubjectForProd"];
                createRequestIndiceJsonModel.FromEmail = ConfigurationManager.AppSettings["FromEmail"];
                createRequestIndiceJsonModel.IndexPattern = ConfigurationManager.AppSettings["IndexPattern"];
                createRequestIndiceJsonModel.IndicePrefix = ConfigurationManager.AppSettings["IndicePrefix"];
                createRequestIndiceJsonModel.IntEndPoint = ConfigurationManager.AppSettings["IntEndPointForCreateRequestIndice"];
                createRequestIndiceJsonModel.ProdEndPoint = ConfigurationManager.AppSettings["ProdEndPointCreateRequestIndice"];

                int readTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ReadTimeout"], out readTimeout);
                createRequestIndiceJsonModel.ReadTimeout = readTimeout;

                createRequestIndiceJsonModel.RequestBody = ConfigurationManager.AppSettings["RequestBodyForCreateRequestIndice"];
                createRequestIndiceJsonModel.SendStatusEmailForInt = Boolean.Parse(ConfigurationManager.AppSettings["SendEmailForInt"]);
                createRequestIndiceJsonModel.SendStatusEmailForProd = Boolean.Parse(ConfigurationManager.AppSettings["SendEmailForProd"]);

                createRequestIndiceJsonModel.ToEmail = ConfigurationManager.AppSettings["ToEmail"];

                createRequestIndiceJsonModel.SuccessfulSubjectForInt = ConfigurationManager.AppSettings["SuccessfulSubjectForInt"];
                createRequestIndiceJsonModel.SuccessfulSubjectForProd = ConfigurationManager.AppSettings["SuccessfulSubjectForProd"];
            }

            string currentDateStringFormat = DateTime.UtcNow.AddDays(1).ToString(createRequestIndiceJsonModel.IndexPattern);
            createRequestIndiceJsonModel.IndiceName = createRequestIndiceJsonModel.IndicePrefix + currentDateStringFormat;

            string intUrl = createRequestIndiceJsonModel.IntEndPoint + "/" + createRequestIndiceJsonModel.IndiceName;

            this.CreateRequestIndice(intUrl, true, false, createRequestIndiceJsonModel);

            string prodUrl = createRequestIndiceJsonModel.ProdEndPoint + "/" + createRequestIndiceJsonModel.IndiceName;

            this.CreateRequestIndice(prodUrl, false, true, createRequestIndiceJsonModel);
        }

        private void SendStatusEmail(string emailBody, bool isSuccessful, bool isInt, bool isProd, CreateRequestIndiceJsonModel createRequestIndiceJsonModel)
        {
            Amazon.SimpleEmail.Model.Destination destination = new Amazon.SimpleEmail.Model.Destination(createRequestIndiceJsonModel.ToEmail.Split(',').ToList());

            Content subjectContent = null;

            if (isSuccessful)
            {
                if (isInt)
                {
                    subjectContent = new Content(createRequestIndiceJsonModel.SuccessfulSubjectForInt);
                }
                else if (isProd)
                {
                    subjectContent = new Content(createRequestIndiceJsonModel.SuccessfulSubjectForProd);
                }
            }
            else
            {
                if (isInt)
                {
                    subjectContent = new Content(createRequestIndiceJsonModel.FailureSubjectForInt);
                }
                else if (isProd)
                {
                    subjectContent = new Content(createRequestIndiceJsonModel.FailureSubjectForProd);
                }
            }

            Content textBody = new Content(emailBody);

            Body body = new Body(textBody);

            Amazon.SimpleEmail.Model.Message message = new Message(subjectContent, body);
            SendEmailRequest request = new SendEmailRequest(createRequestIndiceJsonModel.FromEmail, destination, message);

            BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSAccessKeyIdForMaitainingESCluster"],
                        ConfigurationManager.AppSettings["AWSSecretAccessKeyForMaitainingESCluster"]);

            Amazon.SimpleEmail.AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(basicAwsCredentials, RegionEndpoint.EUWest1);

            client.SendEmail(request);
        }

        private void CreateRequestIndice(string url, bool isInt, bool isProd, CreateRequestIndiceJsonModel createRequestIndiceJsonModel)
        {
            string response = String.Empty;
            bool isSuccessful = false;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Credentials = new NetworkCredential(createRequestIndiceJsonModel.ElasticSearchUserName, createRequestIndiceJsonModel.ElasticSearchPassword);
            request.Timeout = createRequestIndiceJsonModel.ConnectTimeout;
            request.Method = "PUT";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            HttpWebResponse httpWebResponse = null;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(createRequestIndiceJsonModel.RequestBody);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                httpWebResponse = (HttpWebResponse)request.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    isSuccessful = true;
                    if (isInt)
                    {
                        response = "Successfully created '" + createRequestIndiceJsonModel.IndiceName + "' in INT!";
                    }
                    else if (isProd)
                    {
                        response = "Successfully created '" + createRequestIndiceJsonModel.IndiceName + "' in PROD!";
                    }
                }
                else
                {
                    if (isInt)
                    {
                        response = "Failed to create '" + createRequestIndiceJsonModel.IndiceName + "' in INT!";
                    }
                    else if (isProd)
                    {
                        response = "Failed to create '" + createRequestIndiceJsonModel.IndiceName + "' in PROD!";
                    }
                }
            }
            catch (WebException ex)
            {
                //ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, createRequestIndiceJsonModel.RequestBody, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                using (Stream responseStream = ex.Response.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream))
                    {
                        response = "Failed to create '" + createRequestIndiceJsonModel.IndiceName +
                                   "'.\nError Message: " + responseStreamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, createRequestIndiceJsonModel.RequestBody, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            if (isInt && createRequestIndiceJsonModel.SendStatusEmailForInt)
            {
                SendStatusEmail(response, isSuccessful, isInt, isProd, createRequestIndiceJsonModel);
            }
            else if (isProd && createRequestIndiceJsonModel.SendStatusEmailForProd)
            {
                SendStatusEmail(response, isSuccessful, isInt, isProd, createRequestIndiceJsonModel);
            }
        }

        private CreateRequestIndiceJsonModel GetCreateRequestIndiceJsonModel()
        {
            CreateRequestIndiceJsonModel createRequestIndiceJsonModel = new CreateRequestIndiceJsonModel();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSAccessKeyIdForMaitainingESCluster"],
                        ConfigurationManager.AppSettings["AWSSecretAccessKeyForMaitainingESCluster"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest getObjectRequest = new GetObjectRequest
                    {
                        BucketName = ConfigurationManager.AppSettings["MaintainElasticSearchClusterBucketName"],
                        Key = ConfigurationManager.AppSettings["CreateRequestIndiceConfigFileName"]
                    };

                    using (GetObjectResponse getObjectResponse = s3Client.GetObject(getObjectRequest))
                    {
                        if (getObjectResponse != null && getObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            getObjectResponse.ContentLength > 0)
                        {
                            string createRequestIndiceInS3;

                            using (Stream stream = getObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                createRequestIndiceInS3 = streamReader.ReadToEnd();
                            }

                            if (!string.IsNullOrEmpty(createRequestIndiceInS3) &&
                                !string.IsNullOrWhiteSpace(createRequestIndiceInS3))
                            {
                                createRequestIndiceJsonModel =
                                    JsonConvert.DeserializeObject<CreateRequestIndiceJsonModel>(createRequestIndiceInS3);
                            }
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("Exception occurred while GetJsonService.GetCreateRequestIndiceJsonModel.\nException is: " + exception.Message + "\n.Stack Trace: " + exception.StackTrace, LogLevel.Critical);
                return null;
            }

            return createRequestIndiceJsonModel;
        }

        public void DeleteQueryByOffer()
        {
            Task.Factory.StartNew(BeginDeleteQueryByOffer);
        }

        private void BeginDeleteQueryByOffer()
        {
            DeleteQueryByOffersModel deleteQueryByOffersModel = this.GetDeleteByQueryOffersModel();

            if (deleteQueryByOffersModel == null)
            {
                deleteQueryByOffersModel = new DeleteQueryByOffersModel();

                deleteQueryByOffersModel.RequestBody =
                    ConfigurationManager.AppSettings["RequestBodyForDeleteQueryByOffers"];

                int connectTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ConnectTimeout"], out connectTimeout);

                deleteQueryByOffersModel.ConnectTimeout = connectTimeout;

                deleteQueryByOffersModel.ElasticSearchPassword = ConfigurationManager.AppSettings["esPassword"];
                deleteQueryByOffersModel.ElasticSearchUserName = ConfigurationManager.AppSettings["esUserName"];

                deleteQueryByOffersModel.IntEndPoint = ConfigurationManager.AppSettings["IntEndPointForDeleteQueryByOffers"];
                deleteQueryByOffersModel.ProdEndPoint = ConfigurationManager.AppSettings["ProdEndPointForDeleteQueryByOffers"];

                int readTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ReadTimeout"], out readTimeout);
                deleteQueryByOffersModel.ReadTimeout = readTimeout;
            }

            string timeToBeDeleted = DateTime.UtcNow.AddMinutes(-30).ToString("s");

            deleteQueryByOffersModel.RequestBody = deleteQueryByOffersModel.RequestBody.Replace("TO_DATE_TIMESTAMP", timeToBeDeleted);

            DeleteQueryByOffersFromElasticSearch(deleteQueryByOffersModel.IntEndPoint, true, false, deleteQueryByOffersModel);

            DeleteQueryByOffersFromElasticSearch(deleteQueryByOffersModel.ProdEndPoint, false, true, deleteQueryByOffersModel);

        }

        private DeleteQueryByOffersModel GetDeleteByQueryOffersModel()
        {
            DeleteQueryByOffersModel deleteQueryByOffersModel = new DeleteQueryByOffersModel();
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSAccessKeyIdForMaitainingESCluster"],
                        ConfigurationManager.AppSettings["AWSSecretAccessKeyForMaitainingESCluster"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest getObjectRequest = new GetObjectRequest
                    {
                        BucketName = ConfigurationManager.AppSettings["MaintainElasticSearchClusterBucketName"],
                        Key = ConfigurationManager.AppSettings["DeleteQueryByOffersFileName"]
                    };

                    using (GetObjectResponse getObjectResponse = s3Client.GetObject(getObjectRequest))
                    {
                        if (getObjectResponse != null && getObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            getObjectResponse.ContentLength > 0)
                        {
                            string deleteQueryByOffersS3;

                            using (Stream stream = getObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                deleteQueryByOffersS3 = streamReader.ReadToEnd();
                            }

                            if (!string.IsNullOrEmpty(deleteQueryByOffersS3) &&
                                !string.IsNullOrWhiteSpace(deleteQueryByOffersS3))
                            {
                                deleteQueryByOffersModel =
                                    JsonConvert.DeserializeObject<DeleteQueryByOffersModel>(deleteQueryByOffersS3);
                            }
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("Exception occurred while GetJsonService.GetDeleteByQueryOffersModel.\nException is: " + exception.Message + "\n.Stack Trace: " + exception.StackTrace, LogLevel.Critical);
                return null;
            }
            return deleteQueryByOffersModel;
        }

        private void DeleteQueryByOffersFromElasticSearch(string url, bool isInt, bool isProd,
            DeleteQueryByOffersModel deleteQueryByOffersModel)
        {
            string response = String.Empty;
            bool isSuccessful = false;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Credentials = new NetworkCredential(deleteQueryByOffersModel.ElasticSearchUserName, deleteQueryByOffersModel.ElasticSearchPassword);
            request.Timeout = deleteQueryByOffersModel.ConnectTimeout;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            HttpWebResponse httpWebResponse = null;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(deleteQueryByOffersModel.RequestBody);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                httpWebResponse = (HttpWebResponse)request.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    isSuccessful = true;
                    if (isInt)
                    {
                        response = "Delete Query by offers successful in INT!";
                    }
                    else if (isProd)
                    {
                        response = "Delete Query by offers successful in PROD!";
                    }
                }
                else
                {
                    if (isInt)
                    {
                        response = "Delete Query by offers failed in INT!";
                    }
                    else if (isProd)
                    {
                        response = "Delete Query by offers failed in PROD!";
                    }
                }
            }
            catch (WebException ex)
            {
                //ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, createRequestIndiceJsonModel.RequestBody, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                using (Stream responseStream = ex.Response.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream))
                    {
                        response = "Failed to Delete Query by offers '" + deleteQueryByOffersModel.RequestBody +
                                   "'.\nError Message: " + responseStreamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, deleteQueryByOffersModel.RequestBody, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            if (!isSuccessful)
            {
                ErrorLogger.Log(response, LogLevel.Critical);
            }
        }

        public void ExportAndDeleteIndices()
        {
            Task.Factory.StartNew(BeginExportAndDeleteIndices);
        }

        private void BeginExportAndDeleteIndices()
        {
            ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel =
                GetExportAndDeleteRequestIndicesConfigModelFromS3();

            if (exportAndDeleteRequestIndicesConfigModel == null)
            {
                exportAndDeleteRequestIndicesConfigModel = new ExportAndDeleteRequestIndicesConfigModel();


                int connectTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ConnectTimeout"], out connectTimeout);

                exportAndDeleteRequestIndicesConfigModel.ConnectTimeout = connectTimeout;

                exportAndDeleteRequestIndicesConfigModel.ElasticSearchPassword = ConfigurationManager.AppSettings["esPassword"];
                exportAndDeleteRequestIndicesConfigModel.ElasticSearchUserName = ConfigurationManager.AppSettings["esUserName"];

                exportAndDeleteRequestIndicesConfigModel.ProdListOfIndicesUrl = ConfigurationManager.AppSettings["ProdListOfIndicesUrl"];

                int readTimeout = 0;
                Int32.TryParse(ConfigurationManager.AppSettings["ReadTimeout"], out readTimeout);

                exportAndDeleteRequestIndicesConfigModel.ReadTimeout = readTimeout;
            }

            List<IndiceModel> listOfIndicesModels = GetListOfIndices(exportAndDeleteRequestIndicesConfigModel);

            if (listOfIndicesModels == null)
            {
                return;
            }

            string requestIndiceNameToBeExportedToS3 = exportAndDeleteRequestIndicesConfigModel.IndicePrefix +
                DateTime.UtcNow.AddDays(exportAndDeleteRequestIndicesConfigModel.NumberOfDaysBackToExportIndices).ToString(exportAndDeleteRequestIndicesConfigModel.IndexPattern);

            string requestIndiceNameToBeDeleted = exportAndDeleteRequestIndicesConfigModel.IndicePrefix +
                DateTime.UtcNow.AddDays(exportAndDeleteRequestIndicesConfigModel.NumberOfDaysBackToDeleteIndices).ToString(exportAndDeleteRequestIndicesConfigModel.IndexPattern);

            IndiceModel requestIndiceModelToBeDeleted = (from indiceModel in listOfIndicesModels
                                                         where indiceModel.Index.Equals(requestIndiceNameToBeDeleted)
                                                         select indiceModel).FirstOrDefault();

            if (requestIndiceModelToBeDeleted != null)
            {
                DeleteRequestIndice(requestIndiceNameToBeDeleted, exportAndDeleteRequestIndicesConfigModel);
            }

            IndiceModel requestIndiceToBeExported = (from indiceModel in listOfIndicesModels
                                                     where indiceModel.Index.Equals(requestIndiceNameToBeExportedToS3)
                                                     select indiceModel).FirstOrDefault();

            if (requestIndiceToBeExported != null)
            {
                ExportRequestIndiceToS3(requestIndiceToBeExported.Index, exportAndDeleteRequestIndicesConfigModel);
            }
        }

        private static bool DeleteRequestIndice(string indiceName, ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel)
        {
            bool isSuccessful = false;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(exportAndDeleteRequestIndicesConfigModel.ProdUrlToDeleteIndice + indiceName);
            request.Credentials = new NetworkCredential(exportAndDeleteRequestIndicesConfigModel.ElasticSearchUserName, exportAndDeleteRequestIndicesConfigModel.ElasticSearchPassword);
            request.Timeout = exportAndDeleteRequestIndicesConfigModel.ConnectTimeout;
            request.Method = "DELETE";
            HttpWebResponse httpWebResponse = null;
            try
            {
                httpWebResponse = (HttpWebResponse)request.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    isSuccessful = true;
                }
            }
            catch (WebException ex)
            {
                using (Stream responseStream = ex.Response.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream))
                    {
                        ErrorLogger.Log($"DeleteRequestIndice. WebException. Message: {responseStreamReader.ReadToEnd()}", LogLevel.Critical);
                        isSuccessful = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log($"Error occurred in DeleteRequestIndice.\nException Message: {exception.Message}.\nStack Trace: {exception.StackTrace}", LogLevel.Critical);
                isSuccessful = false;
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            return isSuccessful;
        }

        private static void ExportRequestIndiceToS3(string indiceName,
            ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel)
        {
            List<string> scrollIds = new List<string>();

            List<ElasticSearchSourceModel> sources = new List<ElasticSearchSourceModel>();

            ElasticSearchModel elasticSearchModelForScrollId = GetScrollId(indiceName,
                exportAndDeleteRequestIndicesConfigModel);

            foreach (ElasticSearchHitModel hit in elasticSearchModelForScrollId.hits.hits)
            {
                sources.Add(hit._source);
            }

            WriteSourcesToS3(indiceName, "_00", sources);

            sources.Clear();

            if (elasticSearchModelForScrollId == null)
            {
                return;
            }

            int count = 0;

            string scrollId = elasticSearchModelForScrollId._scroll_id;

            scrollIds.Add(scrollId);

            int quotient = 0;
            int numberOfLoops = Math.DivRem(elasticSearchModelForScrollId.hits.total, 10000, out quotient);

            numberOfLoops = quotient == 0 ? numberOfLoops : numberOfLoops + 1;


            for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
            {
                ElasticSearchModel elasticSearchModel =
                    GetRequestIndiceDataForSubsequentData(exportAndDeleteRequestIndicesConfigModel,
                        scrollId);

                if (elasticSearchModel != null)
                {
                    scrollId = elasticSearchModel._scroll_id;
                    scrollIds.Add(scrollId);

                    foreach (ElasticSearchHitModel hit in elasticSearchModel.hits.hits)
                    {
                        sources.Add(hit._source);
                    }

                    count++;

                    string suffix = String.Empty;
                    suffix = (count < 10) ? "_0" + count : "_" + count;
                    WriteSourcesToS3(indiceName, suffix, sources);
                    sources.Clear();
                }
            }
        }

        private static void WriteSourcesToS3(string indiceName, string suffix, List<ElasticSearchSourceModel> sources)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSAccessKeyIdForMaitainingESCluster"],
                                        ConfigurationManager.AppSettings["AWSSecretAccessKeyForMaitainingESCluster"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    PutObjectRequest putObjectRequest = new PutObjectRequest();
                    putObjectRequest.BucketName = ConfigurationManager.AppSettings["MaintainElasticSearchClusterBucketName"];
                    putObjectRequest.Key = ConfigurationManager.AppSettings["FolderPathToWriteRequestIndice"] + "/" + indiceName + "/" + (indiceName + suffix) + ".csv";
                    putObjectRequest.ContentBody = ServiceStack.Text.CsvSerializer.SerializeToCsv(sources);
                    putObjectRequest.ContentType = "text/plain";

                    PutObjectResponse putObjectResponse = s3Client.PutObject(putObjectRequest);

                    if (putObjectResponse != null)
                    {
                        ErrorLogger.Log($"WriteSourcesToS3. Successfully Written {indiceName + "/" + (indiceName + suffix) + ".csv"}.", LogLevel.Information);
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log($"WriteSourcesToS3. Failed to write {indiceName + "/" + (indiceName + suffix) + ".csv"}.\nException Message: {exception.Message}.\nStack Trace: {exception.StackTrace}", LogLevel.Information);
            }
        }

        private static ElasticSearchModel GetRequestIndiceDataForSubsequentData(
            ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel, string scollId)
        {
            ElasticSearchModel elasticSearchModel = new ElasticSearchModel();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(exportAndDeleteRequestIndicesConfigModel.ProdUrlToRetrieveSubsequentRecords);
            request.Credentials = new NetworkCredential(exportAndDeleteRequestIndicesConfigModel.ElasticSearchUserName, exportAndDeleteRequestIndicesConfigModel.ElasticSearchPassword);
            request.Timeout = exportAndDeleteRequestIndicesConfigModel.ConnectTimeout;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            HttpWebResponse httpWebResponse = null;

            try
            {
                string requestBody =
                    exportAndDeleteRequestIndicesConfigModel.RequestBodyToRetrieveSubsequentRecords.Replace("NEW_SCROLL_ID", scollId);
                byte[] bytes = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                httpWebResponse = (HttpWebResponse)request.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader responseStreamReader = new StreamReader(responseStream))
                        {
                            string response = responseStreamReader.ReadToEnd();

                            elasticSearchModel = JsonConvert.DeserializeObject<ElasticSearchModel>(response);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                using (Stream responseStream = ex.Response.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream))
                    {
                        ErrorLogger.Log($"GetRequestIndiceDataForSubsequentData. WebException. Message: {responseStreamReader.ReadToEnd()}", LogLevel.Critical);
                        return null;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log($"Error occurred in GetRequestIndiceDataForSubsequentData.\nException Message: {exception.Message}.\nStack Trace: {exception.StackTrace}", LogLevel.Critical);
                return null;
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            return elasticSearchModel;
        }

        private static ElasticSearchModel GetScrollId(string indiceName,
            ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel)
        {
            ElasticSearchModel elasticSearchModel = new ElasticSearchModel();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format(exportAndDeleteRequestIndicesConfigModel.ProdUrlToRetrieveScrollId, indiceName));
            request.Credentials = new NetworkCredential(exportAndDeleteRequestIndicesConfigModel.ElasticSearchUserName, exportAndDeleteRequestIndicesConfigModel.ElasticSearchPassword);
            request.Timeout = exportAndDeleteRequestIndicesConfigModel.ConnectTimeout;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            HttpWebResponse httpWebResponse = null;

            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(exportAndDeleteRequestIndicesConfigModel.RequestBodyToRetrieveScrollId);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                httpWebResponse = (HttpWebResponse)request.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader responseStreamReader = new StreamReader(responseStream))
                        {
                            string response = responseStreamReader.ReadToEnd();

                            elasticSearchModel = JsonConvert.DeserializeObject<ElasticSearchModel>(response);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                using (Stream responseStream = ex.Response.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream))
                    {
                        ErrorLogger.Log($"GetScrollId. WebException. Message: {responseStreamReader.ReadToEnd()}", LogLevel.Critical);
                        return null;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log($"GetScrollId. WebException.\nException Message: {exception.Message}\nStack Trace: {exception.StackTrace}", LogLevel.Critical);
                return null;
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            return elasticSearchModel;
        }

        private static List<IndiceModel> GetListOfIndices(ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel)
        {
            List<IndiceModel> listOfIndicesModels = new List<IndiceModel>();


            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(exportAndDeleteRequestIndicesConfigModel.ProdListOfIndicesUrl);
                request.Credentials = new NetworkCredential(exportAndDeleteRequestIndicesConfigModel.ElasticSearchUserName, exportAndDeleteRequestIndicesConfigModel.ElasticSearchPassword);
                request.Timeout = exportAndDeleteRequestIndicesConfigModel.ConnectTimeout;
                request.Method = "GET";
                request.Accept = "application/json";
                HttpWebResponse httpWebResponse = null;

                try
                {
                    httpWebResponse = (HttpWebResponse)request.GetResponse();

                    if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = httpWebResponse.GetResponseStream())
                        {
                            using (StreamReader responseStreamReader = new StreamReader(responseStream))
                            {
                                string listOfIndices = responseStreamReader.ReadToEnd();

                                listOfIndicesModels = JsonConvert.DeserializeObject<List<IndiceModel>>(listOfIndices);
                            }
                        }
                    }
                }
                catch (WebException ex)
                {
                    using (Stream responseStream = ex.Response.GetResponseStream())
                    {
                        using (StreamReader responseStreamReader = new StreamReader(responseStream))
                        {
                            ErrorLogger.Log($"GetListOfIndices. WebException. Message: {responseStreamReader.ReadToEnd()}", LogLevel.Critical);
                            return null;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ErrorLogger.Log($"GetListOfIndices. WebException.\nException Message: {exception.Message}\nStack Trace: {exception.StackTrace}", LogLevel.Critical);
                    return null;
                }
                finally
                {
                    if (httpWebResponse != null)
                    {
                        httpWebResponse.Close();
                        httpWebResponse.Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                return null;
            }

            return listOfIndicesModels;
        }

        private static ExportAndDeleteRequestIndicesConfigModel GetExportAndDeleteRequestIndicesConfigModelFromS3()
        {
            ExportAndDeleteRequestIndicesConfigModel exportAndDeleteRequestIndicesConfigModel =
                new ExportAndDeleteRequestIndicesConfigModel();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                            new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSAccessKeyIdForMaitainingESCluster"],
                                ConfigurationManager.AppSettings["AWSSecretAccessKeyForMaitainingESCluster"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest getObjectRequest = new GetObjectRequest
                    {
                        BucketName = ConfigurationManager.AppSettings["MaintainElasticSearchClusterBucketName"],
                        Key = ConfigurationManager.AppSettings["ExportAndDeleteRequestIndicesFileName"]
                    };

                    using (GetObjectResponse getObjectResponse = s3Client.GetObject(getObjectRequest))
                    {
                        if (getObjectResponse != null && getObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            getObjectResponse.ContentLength > 0)
                        {
                            string exportAndDeleteRequestIndicesS3;
                            using (Stream stream = getObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                exportAndDeleteRequestIndicesS3 = streamReader.ReadToEnd();
                            }

                            if (!string.IsNullOrEmpty(exportAndDeleteRequestIndicesS3) &&
                                !string.IsNullOrWhiteSpace(exportAndDeleteRequestIndicesS3))
                            {
                                exportAndDeleteRequestIndicesConfigModel =
                                    JsonConvert.DeserializeObject<ExportAndDeleteRequestIndicesConfigModel>(exportAndDeleteRequestIndicesS3);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log($"GetExportAndDeleteRequestIndicesConfigModelFromS3.\nException Message: {exception.Message}\nStack Trace: {exception.StackTrace}", LogLevel.Critical);
                return null;
            }

            return exportAndDeleteRequestIndicesConfigModel;
        }

        #endregion

        #region Teletext Web API

        public Stream AuthenticateCustomerOTP(string mobileNo, string otpr)
        {
            AuthStatus otps = new AuthStatus();
            otps = Teletext.AuthCustomerOTP(mobileNo, otpr);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(otps)));
        }

        public Stream GetCustomerAuthenticationOTP(string mobileNo)
        {
            OTP otp = new OTP();
            otp = Teletext.GetCustomerAuthOTP(mobileNo);
            string country = "0";
            string authKey = "99211ABjkMgJiIwQ56683c98";
            string mobileNumber = mobileNo;
            string senderId = "TelTxt";
            string message = HttpUtility.UrlEncode("Hi your OTP is " + otp.OTPValue);
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authKey);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", senderId);
            sbPostData.AppendFormat("&route={0}", "TelTxt");

            try
            {
                string sendSMSUri = "https://control.msg91.com/api/sendhttp.php";
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (SystemException ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(otp)));
        }

        public Stream GetGuidesIndex()
        {
            string fileName = fileName = HttpRuntime.AppDomainAppPath + @"data\guideindex.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetDestinations()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinations-overseas.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelList(string destinationCode, string hotelName, string resortId)
        {
            List<AutoSuggestHotel> selectedHotels = new List<AutoSuggestHotel>();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\hotellist.json";

            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            if (!string.IsNullOrEmpty(json))
            {
                GuideHotel guideHotel = JsonConvert.DeserializeObject<GuideHotel>(json);
                var elements = destinationCode.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
                foreach (string dest in elements)
                {
                    List<AutoSuggestHotel> filterHotels = null;
                    if (resortId == "0")
                    {
                        filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                    }
                    else
                    {
                        if (resortId.IndexOf(",") > -1)
                        {
                            List<string> resIds = resortId.Split(',').ToList<string>();
                            filterHotels = guideHotel.Hotels.FindAll(
                                                                        h => h.DestinationCode.ToLower() == dest.ToLower() &&
                                                                        h.HotelName.ToLower().Contains(hotelName.ToLower()))
                                                                    .Where(x => resIds.Any(y => int.Parse(y) == x.ResortID)).ToList();
                        }
                        else
                        {
                            filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.ResortID == int.Parse(resortId) && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                        }
                    }

                    foreach (AutoSuggestHotel hotel in filterHotels)
                    {
                        if (!selectedHotels.Exists(a => a.FriendlyURL == hotel.FriendlyURL))
                            selectedHotels.Add(hotel);
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(selectedHotels)));
        }

        public Stream GetHotelListForAutoSuggest(string regionId, string hotelName)
        {
            string destinationCode = string.Empty;
            string resortId = string.Empty;
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.id.Equals(regionId))
                    {
                        destinationCode = region.destcodes;
                        resortId = region.resortcode;
                        break;
                    }
                }
            }

            List<AutoSuggestHotel> selectedHotels = new List<AutoSuggestHotel>();
            fileName = HttpRuntime.AppDomainAppPath + @"data\hotellist.json";

            if (!string.IsNullOrEmpty(destinationCode) && !string.IsNullOrEmpty(resortId))
            {
                if (System.IO.File.Exists(fileName))
                {
                    using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        json = sr.ReadToEnd();
                    }
                }
                else
                {
                    json = "{}";
                }

                if (!string.IsNullOrEmpty(json))
                {
                    GuideHotel guideHotel = JsonConvert.DeserializeObject<GuideHotel>(json);
                    var elements = destinationCode.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
                    foreach (string dest in elements)
                    {
                        List<AutoSuggestHotel> filterHotels = null;
                        if (resortId == "0")
                        {
                            filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                        }
                        else
                        {
                            if (resortId.IndexOf(",") > -1)
                            {
                                List<string> resIds = resortId.Split(',').ToList<string>();
                                filterHotels = guideHotel.Hotels.FindAll(
                                                                            h => h.DestinationCode.ToLower() == dest.ToLower() &&
                                                                            h.HotelName.ToLower().Contains(hotelName.ToLower()))
                                                                        .Where(x => resIds.Any(y => int.Parse(y) == x.ResortID)).ToList();
                            }
                            else
                            {
                                filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.ResortID == int.Parse(resortId) && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                            }
                        }

                        foreach (AutoSuggestHotel hotel in filterHotels)
                        {
                            if (!selectedHotels.Exists(a => a.FriendlyURL == hotel.FriendlyURL))
                                selectedHotels.Add(hotel);
                        }
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(selectedHotels)));
        }

        public Stream GetGuidesIndexList()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetGuide(string RegionId)
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\" + @"region" + RegionId + ".json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetRegionById(string RegionId)
        {
            RequestedRegion reqRegion = new RequestedRegion();
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.id.Equals(RegionId))
                    {
                        reqRegion.regid = region.id;
                        reqRegion.regUrl = region.linkurl;
                        reqRegion.regname = region.name;
                        reqRegion.reglevel = region.reglevel;
                        reqRegion.parent_regid = region.parent_regid != null ? region.parent_regid : string.Empty;
                        reqRegion.parent_regname = region.parent_regname != null ? region.parent_regname : string.Empty;
                        reqRegion.parent_link = region.parent_link != null ? region.parent_link : string.Empty;
                        reqRegion.grand_regid = region.grand_regid != null ? region.grand_regid : string.Empty;
                        reqRegion.grand_regname = region.grand_regname != null ? region.grand_regname : string.Empty;
                        reqRegion.grand_reglink = region.grand_reglink != null ? region.grand_reglink : string.Empty;
                        break;
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reqRegion)));
        }

        public Stream GetRegionByURL(string url)
        {
            RequestedRegion reqRegion = new RequestedRegion();
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.linkurl.ToLower().Equals(url.ToLower()))
                    {
                        reqRegion.regid = region.id;
                        reqRegion.regUrl = region.linkurl;
                        reqRegion.regname = region.name;
                        reqRegion.reglevel = region.reglevel;
                        reqRegion.parent_regid = region.parent_regid != null ? region.parent_regid : string.Empty;
                        reqRegion.parent_regname = region.parent_regname != null ? region.parent_regname : string.Empty;
                        reqRegion.parent_link = region.parent_link != null ? region.parent_link : string.Empty;
                        reqRegion.grand_regid = region.grand_regid != null ? region.grand_regid : string.Empty;
                        reqRegion.grand_regname = region.grand_regname != null ? region.grand_regname : string.Empty;
                        reqRegion.grand_reglink = region.grand_reglink != null ? region.grand_reglink : string.Empty;
                        break;
                    }
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reqRegion)));
        }

        public Stream GetOverseasDestination(string destinationName)
        {
            List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(destinationName);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(overseasList)));
        }

        public Stream GetOvrseaseDestinationLabelRegions()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "";
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetMasterHotelIdByURL(string url)
        {
            string masterHotelId = string.Empty;
            IHotelService hotelService = new HotelService();
            masterHotelId = hotelService.GetMasterHotelIdByURL(url);
            var masterHotel = new { MasterHotelId = masterHotelId };
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterHotel)));
        }

        public Stream GetHotelDataByMasterHotelId(string masterHotelId)
        {
            string json = string.Empty;
            IHotelService hotelService = new HotelService();
            json = hotelService.GetHotelDataByMasterHotelId(masterHotelId);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel)
        {
            IHotelService hotelService = new HotelService();
            bool result = hotelService.SetMasterHotelData(hotel);
            string json = "{" + "\"" + "Status" + "\"" + ":" + "\"" + (result ? "Success" : "Fail") + "\"" + "}";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream SaveGuideInformation(Guide guide, string userId)
        {
            try
            {
                GuideDestination orgGuideDestination = new GuideDestination();

                if (!string.IsNullOrWhiteSpace(guide.regid))
                {
                    //If Guide Exists, get the JSON file from S3.
                    orgGuideDestination = Guides.Guides.GetGuideDestinationInfo(guide.regid);
                    if (guide == null)
                    {
                        throw new NullReferenceException(string.Format("Guide- {0} does not exist.", guide.regid));
                    }
                    orgGuideDestination.regid = guide.regid;
                    //Remove existing Places TO GO/Things To Do and Overview sections from the JSON, as we are going to dump new data.
                    orgGuideDestination.sections.RemoveAll(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("overview", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("essential info", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                }
                else
                {
                    //Get the new GuideId from the GuideIndexList JSON file.
                    orgGuideDestination.regid = Guides.Guides.getNextRegID();
                    if (string.IsNullOrWhiteSpace(orgGuideDestination.regid))
                    {
                        throw new NullReferenceException("Next RegionId does not exist.");
                    }
                    guide.regid = orgGuideDestination.regid;
                    orgGuideDestination.sections = new List<section>();
                }

                //Dump the new Data.
                orgGuideDestination.bgimage = guide.bgimage;
                orgGuideDestination.ptitle = guide.ptitle;
                orgGuideDestination.bgimage_m = guide.bgimage_m;
                orgGuideDestination.bgimage_t = guide.bgimage_t;
                orgGuideDestination.bgimage_sd = guide.bgimage_sd;
                orgGuideDestination.headerImgAltTag = guide.headerImgAltTag;
                orgGuideDestination.headerImgTitleTag = guide.headerImgTitleTag;
                orgGuideDestination.pmetadesc = guide.pmetadesc;
                orgGuideDestination.sectionid = guide.sectionid;
                orgGuideDestination.pmetakeyword = guide.pmetakeyword;
                orgGuideDestination.showGuideOnMobile = guide.showGuideOnMobile;

                orgGuideDestination.regname = guide.regname;
                orgGuideDestination.reglevel = guide.reglevel;
                orgGuideDestination.recommendedhotels = guide.recommendedhotels;
                orgGuideDestination.parent_regid = guide.parent_regid;
                orgGuideDestination.parent_regname = guide.parent_regname;
                orgGuideDestination.parent_link = guide.parent_link;
                orgGuideDestination.grand_regid = guide.grand_regid;
                orgGuideDestination.grand_regname = guide.grand_regname;
                orgGuideDestination.grand_reglink = guide.grand_reglink;
                orgGuideDestination.highSeasonalAverage = guide.highSeasonalAverage;

                orgGuideDestination.sections.AddRange(guide.sections);
                orgGuideDestination.expertviews = guide.expertviews;
                orgGuideDestination.metatags = guide.metatags;
                orgGuideDestination.dropDownLabelName = guide.dropDownLabelName;
                orgGuideDestination.guideVideos = guide.guideVideos;

                //Once the data is dumped to the Object, Upload it to S3.
                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["AWSGuideBucketName"], guide.regid + ".json", orgGuideDestination);

                var isGuideIndexListUpdated = Guides.Guides.AddRecordguidesindexlist(orgGuideDestination);
                ErrorLogger.Log(string.Format("Guide - {0} successfully updated By the User - {1}", guide.regid, userId), LogLevel.Information);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("SaveGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideInformation(string regionId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null");
                }

                var guide = Guides.Guides.GetGuideInfo(regionId);
                if (guide == null)
                {
                    throw new NullReferenceException(string.Format("Guide- {0} does not exist.", regionId));
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guide.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guide.showLastMinuteDeals = true;
                }

                if (guide.recommendedhotels == null)
                {
                    guide.recommendedhotels = new List<RecommendedHotel>();
                }

                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideInformationByUrl(string url)
        {
            try
            {
                var regionId = Guides.Guides.GetDestinationIdByUrl(url.ToLower().Replace("/guidesv2", "/guides"));
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or empty");
                }

                var guide = Guides.Guides.GetGuideInfo(regionId);

                if (guide == null)
                {
                    throw new NullReferenceException(string.Format("Guide- {0} does not exist.", regionId));
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guide.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guide.showLastMinuteDeals = true;
                }
                if (guide.recommendedhotels == null)
                {
                    guide.recommendedhotels = new List<RecommendedHotel>();
                }

                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformationByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideLandingPageInfo(string regionId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or Empty");
                }

                var guideDestination = Guides.Guides.GetGuide(regionId);

                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guideDestination)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideLandingPageInfo returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideLandingPageInfoByUrl(string url)
        {
            try
            {
                var regionId = Guides.Guides.GetDestinationIdByUrl(url.ToLower().Replace("/guidesv2", "/guides").Replace("/p/", "/guides/"));
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or Empty");
                }
                //Get Region From S3.                
                GuideDestination guideDestination = Guides.Guides.GetGuideDestinationInfo(regionId); //JsonConvert.DeserializeObject<GuideDestination>(json);
                if (guideDestination == null)
                {
                    throw new NullReferenceException("Guide object does not exist");
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guideDestination.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guideDestination.showLastMinuteDeals = true;
                }
                //Get GuideIndexList JSON File.
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                //Get Child Regions based on the Region ID                
                IEnumerable<WCFRestService.Model.Guides.Destination> guideChilds = destinationGuides.destinations.Where(p => p.parent_regid == regionId);

                //Append ChildRegion Info to the ParentRegion if Exists
                if (guideChilds != null && guideChilds.Count() > 0)
                {
                    guideDestination.link_summary = new List<link_summary>();
                    GuideDestination guideChildInfo = null;
                    link_summary linkSummary = null;
                    for (var guideChildIndex = 0; guideChildIndex < guideChilds.Count(); guideChildIndex++)
                    {
                        var guideChild = guideChilds.ToList()[guideChildIndex];
                        //Get Child Regions from S3.                        
                        guideChildInfo = Guides.Guides.GetGuideDestinationInfo(guideChild.id);
                        if (guideChildInfo == null)
                        {
                            continue;
                        }

                        linkSummary = new link_summary();
                        linkSummary.lname = Guides.Guides.GetDestinationName(guideChild.id);
                        linkSummary.guideId = guideChild.id;
                        linkSummary.bgimage_m = guideChildInfo.bgimage_m;
                        linkSummary.lurl = Guides.Guides.GetDestinationUrl(guideChild.id).ToLower();
                        linkSummary.sectionid = guideChildInfo.sectionid;
                        linkSummary.recommendedhotels = guideChildInfo.recommendedhotels == null ? new List<RecommendedHotel>() : guideChildInfo.recommendedhotels;

                        //Add Child Regions Places To Go/Things To Do to the Parent Region.
                        var childGuidePlacesToGo = guideChildInfo.sections.Where(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                        if (childGuidePlacesToGo != null && childGuidePlacesToGo.Count() > 0 && guideDestination.reglevel != "1")
                        {
                            linkSummary.PlacesToGo = childGuidePlacesToGo.FirstOrDefault();
                        }

                        guideDestination.link_summary.Add(linkSummary);
                    }
                }

                if (guideDestination.recommendedhotels == null)
                {
                    guideDestination.recommendedhotels = new List<RecommendedHotel>();
                }

                if (guideDestination.reglevel == "1")
                {
                    var placesToGo = guideDestination.sections.Where(p => p.sname.ToLower() == "places to go");
                    if (placesToGo != null && placesToGo.Count() > 0)
                    {
                        guideDestination.sections.Remove(placesToGo.FirstOrDefault());
                    }
                }

                guideDestination.ptitle = guideDestination.ptitle.Replace("Holidays |", "Holidays 2017/2018 |").Replace("Holidays –", "Holidays 2017/2018 –");
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guideDestination)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideLandingPageInfoByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetPTGGuideLandingPageInfoByUrl(string url)
        {
            try
            {
                var regionId = Guides.Guides.GetDestinationIdByUrl(url.ToLower().Replace("/guidesv2", "/guides").Replace("/p/", "/guides/"));
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or Empty");
                }
                //Get Region From S3.                
                GuideDestination guideDestination = Guides.Guides.GetGuideDestinationInfo(regionId); //JsonConvert.DeserializeObject<GuideDestination>(json);
                if (guideDestination == null)
                {
                    throw new NullReferenceException("Guide object does not exist");
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guideDestination.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guideDestination.showLastMinuteDeals = true;
                }
                //Get GuideIndexList JSON File.
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                //Get Child Regions based on the Region ID                
                IEnumerable<WCFRestService.Model.Guides.Destination> guideChilds = destinationGuides.destinations.Where(p => p.parent_regid == regionId);

                //Append ChildRegion Info to the ParentRegion if Exists
                if (guideChilds != null && guideChilds.Count() > 0)
                {
                    guideDestination.link_summary = new List<link_summary>();
                    GuideDestination guideChildInfo = null;
                    link_summary linkSummary = null;
                    for (var guideChildIndex = 0; guideChildIndex < guideChilds.Count(); guideChildIndex++)
                    {
                        var guideChild = guideChilds.ToList()[guideChildIndex];
                        //Get Child Regions from S3.                        
                        guideChildInfo = Guides.Guides.GetGuideDestinationInfo(guideChild.id);
                        if (guideChildInfo == null)
                        {
                            continue;
                        }

                        linkSummary = new link_summary();
                        linkSummary.lname = Guides.Guides.GetDestinationName(guideChild.id);
                        linkSummary.guideId = guideChild.id;
                        linkSummary.bgimage_m = guideChildInfo.bgimage_m;
                        linkSummary.lurl = Guides.Guides.GetDestinationUrl(guideChild.id).ToLower();
                        linkSummary.sectionid = guideChildInfo.sectionid;
                        linkSummary.recommendedhotels = guideChildInfo.recommendedhotels == null ? new List<RecommendedHotel>() : guideChildInfo.recommendedhotels;

                        //Add Child Regions Places To Go/Things To Do to the Parent Region.
                        var childGuidePlacesToGo = guideChildInfo.sections.Where(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                        if (childGuidePlacesToGo != null && childGuidePlacesToGo.Count() > 0)
                        {
                            linkSummary.PlacesToGo = childGuidePlacesToGo.FirstOrDefault();
                        }

                        guideDestination.link_summary.Add(linkSummary);
                    }
                }

                //if (guideDestination.recommendedhotels == null)
                //{
                //    guideDestination.recommendedhotels = new List<RecommendedHotel>();
                //}

                guideDestination.ptitle = guideDestination.ptitle.Replace("Holidays |", "Holidays 2017/2018 |").Replace("Holidays –", "Holidays 2017/2018 –");
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guideDestination.link_summary)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetPTGGuideLandingPageInfoByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuidesIndexListFromCloudFront()
        {
            try
            {
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(destinationGuides)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformationByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetTeletextRouting(string destinationId, string departureId, string dates)
        {
            int number;
            if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && !string.IsNullOrEmpty(dates))
            {
                return null;
            }
            else if (string.IsNullOrEmpty(destinationId) && string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                throw new WebFaultException<string>("all parameters are empty.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(destinationId) && !int.TryParse(destinationId, out number))
            {
                throw new WebFaultException<string>("Invalid destinationId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(departureId) && !int.TryParse(departureId, out number))
            {
                throw new WebFaultException<string>("Invalid departureId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }

            RoutingResponse routingResult = RoutingApiResponseGenerator.GetRoutingData(destinationId, departureId, dates);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(routingResult)));
        }

        //IE Code
        public Stream GetTeletextRoutingIE(string destinationId, string departureId, string dates, string countrySite)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetTeletextRouting(destinationId, departureId, dates);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            int number;
            if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && !string.IsNullOrEmpty(dates))
            {
                return null;
            }
            else if (string.IsNullOrEmpty(destinationId) && string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                throw new WebFaultException<string>("all parameters are empty.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(destinationId) && !int.TryParse(destinationId, out number))
            {
                throw new WebFaultException<string>("Invalid destinationId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(departureId) && !int.TryParse(departureId, out number))
            {
                throw new WebFaultException<string>("Invalid departureId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }

            RoutingResponse routingResult = RoutingApiResponseGeneratorMT.GetRoutingData(destinationId, departureId, dates, countrySite);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(routingResult)));
        }

        #endregion

        #region Flight Stats API

        public Stream GetFlightByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightStats flightStats = new FlightStats();

            FlightStatusResponse flight = flightStats.GetFlightByFlightNumberAndFlightDate(flightNumber, day, month, year);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(flight)));

        }

        public Stream GetFlightScheduleByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightStats flightStats = new FlightStats();

            FlightScheduleResponse flight = flightStats.GetFlightScheduleByFlightNumberAndFlightDate(flightNumber, day, month, year);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(flight)));

        }
        #endregion

        #region TopDog API IE

        public Stream GetMobileBasketIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetMobileBasket(BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            MobileBasket mobileBasket = new MobileBasket();
            mobileBasket.Airlines = new Dictionary<string, string>();
            mobileBasket.Airports = new Dictionary<string, string>();
            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManagerMT.GetBasket(countrySite,BookingReference, Surname, DepartureDate, true, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }
        
        public Stream GetBasketIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetBasket(BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManagerMT.GetBasket(countrySite,BookingReference, Surname, DepartureDate, true, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetTOPDOGBasketIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetTOPDOGBasketIE(countrySite, BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManagerMT.GetBasket(countrySite,BookingReference, Surname, DepartureDate, false, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetBookingDetailsFromTOPDOGIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetBookingDetailsFromTOPDOGIE(countrySite, BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManagerMT.GetBasket(countrySite,BookingReference, Surname, DepartureDate, true, true);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetQuickQuoteDetailsIE(string countrySite, string BookingReference, string DepartureDate)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetQuickQuoteDetails(BookingReference, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            try
            {
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManagerMT.GetQuickQuoteDetails(countrySite,BookingReference, DepartureDate);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetPaymentPlanIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetPaymentPlan(BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;
            try
            {
                BookingReference = BookingReference.ToUpper();
                var paymentPlan = TopDogManagerMT.GetPayments(countrySite,BookingReference, Surname, DepartureDate);

                if (paymentPlan != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentPlan)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetDocumentsIE(string countrySite, string BookingReference, string Surname, string DepartureDate)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetDocuments(BookingReference, Surname, DepartureDate);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            var json = String.Empty;

            try
            {

                MyDocuments myBookingDocuments = null;

                try
                {
                    //TODO: This section always throws an error since there is no invoice document. Can be removed if there will never be an invoice document
                    BookingReference = BookingReference.ToUpper();
                    Stream data = TopDogManagerMT.DownloadS3Object(countrySite,Utilities.GetWebConfigValueForMPT(countrySite, "BookingAWSBucketName"), string.Format("{0}-invoice.pdf", BookingReference.ToLower()));
                    StreamReader reader = new StreamReader(data);
                    Task.Run(() => TopDogManagerMT.GetDocuments(countrySite, BookingReference, Surname, DepartureDate));
                    myBookingDocuments = new MyDocuments();
                    DocumentType docType = new DocumentType();
                    DocumentList docList = new DocumentList();
                    List<DocumentList> dl = new List<DocumentList>();
                    docType.Description = "Consolidated Document";
                    docList.DocumentType = new DocumentType();
                    docList.DocumentType = docType;
                    docList.Category = (int)Category.Generic;
                    docList.DocumentUrl = Utilities.GetWebConfigValueForMPT(countrySite, "S3TeletextBookingUrl").ToString() + "/" + string.Format("{0}-invoice.pdf", BookingReference.ToLower());
                    docList.DocumentFormat = "PDF";
                    dl.Add(docList);
                    myBookingDocuments.DocumentList = dl;
                }
                catch (Exception ex)
                {
                    BookingReference = BookingReference.ToUpper();
                    var bookingDocuments = TopDogManagerMT.GetDocuments(countrySite, BookingReference, Surname, DepartureDate);

                    if (bookingDocuments != null)
                    {
                        myBookingDocuments = new MyDocuments();
                        List<DocumentList> dl = new List<DocumentList>();
                        myBookingDocuments.BookingReference = bookingDocuments[0].BookingReference;
                        myBookingDocuments.Comment = bookingDocuments[0].Comment;
                        foreach (var document in bookingDocuments[0].DocumentList)
                        {
                            DocumentType docType = new DocumentType();
                            docType.Code = document.DocumentType.Code;
                            docType.Description = document.DocumentType.Description;

                            DocumentList docList = new DocumentList();

                            if (documentCodes.Contains(document.DocumentType.Code))
                            {
                                docList.Category = (int)Category.Payments;
                            }
                            else if (document.DocumentType.Code == "Admin.ImportantInformation")
                            {
                                docList.Category = (int)Category.Important_Information;
                            }
                            else if (document.DocumentType.Code == "ATOL-CERTIFICATE")
                            {
                                docList.Category = (int)Category.ATOL_Certificate;
                            }
                            else if (document.DocumentType.Code == "Admin.E-TicketsDetails.Combined")
                            {
                                docList.Category = (int)Category.Flights;
                            }
                            else if (document.DocumentType.Code == "Admin.Accommodation.Voucher.Supplier")
                            {
                                docList.Category = (int)Category.Accomodation;
                            }
                            else if (document.DocumentType.Code == "Admin.TransferVoucher")
                            {
                                docList.Category = (int)Category.Transfer;
                            }
                            docList.CreationDateTime = document.CreationDateTime;
                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentFormat = document.DocumentFormat;

                            docList.DocumentUrl = document.DocumentUrl.Replace("10.0.0.190", ConfigurationManager.AppSettings["TopDogDocumentsURL"]);

                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentType = docType;
                            docList.UserName = document.UserName;
                            dl.Add(docList);
                        }
                        myBookingDocuments.DocumentList = dl;
                    }
                }

                if (myBookingDocuments != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDocuments)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }
                
        #endregion

        #region TopDog API

        public Stream GetMobileBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;
            MobileBasket mobileBasket = new MobileBasket();
            mobileBasket.Airlines = new Dictionary<string, string>();
            mobileBasket.Airports = new Dictionary<string, string>();
            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, true, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, true, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetTOPDOGBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, false, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetBookingDetailsFromTOPDOG(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, true, true);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetQuickQuoteDetails(string BookingReference, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetQuickQuoteDetails(BookingReference, DepartureDate);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetDocuments(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {

                MyDocuments myBookingDocuments = null;

                try
                {
                    BookingReference = BookingReference.ToUpper();
                    Stream data = Utilities.DownloadS3Object(ConfigurationManager.AppSettings["BookingAWSBucketName"], string.Format("{0}-invoice.pdf", BookingReference.ToLower()));
                    StreamReader reader = new StreamReader(data);
                    Task.Run(() => TopDogManager.GetDocuments(BookingReference, Surname, DepartureDate));
                    myBookingDocuments = new MyDocuments();
                    DocumentType docType = new DocumentType();
                    DocumentList docList = new DocumentList();
                    List<DocumentList> dl = new List<DocumentList>();
                    docType.Description = "Consolidated Document";
                    docList.DocumentType = new DocumentType();
                    docList.DocumentType = docType;
                    docList.Category = (int)Category.Generic;
                    docList.DocumentUrl = ConfigurationManager.AppSettings["S3TeletextBookingUrl"].ToString() + "/" + string.Format("{0}-invoice.pdf", BookingReference.ToLower());
                    docList.DocumentFormat = "PDF";
                    dl.Add(docList);
                    myBookingDocuments.DocumentList = dl;
                }
                catch (Exception ex)
                {
                    var bookingDocuments = TopDogManager.GetDocuments(BookingReference, Surname, DepartureDate);

                    if (bookingDocuments != null)
                    {
                        myBookingDocuments = new MyDocuments();
                        List<DocumentList> dl = new List<DocumentList>();
                        myBookingDocuments.BookingReference = bookingDocuments[0].BookingReference;
                        myBookingDocuments.Comment = bookingDocuments[0].Comment;
                        foreach (var document in bookingDocuments[0].DocumentList)
                        {
                            DocumentType docType = new DocumentType();
                            docType.Code = document.DocumentType.Code;
                            docType.Description = document.DocumentType.Description;

                            DocumentList docList = new DocumentList();

                            if (documentCodes.Contains(document.DocumentType.Code))
                            {
                                docList.Category = (int)Category.Payments;
                            }
                            else if (document.DocumentType.Code == "Admin.ImportantInformation")
                            {
                                docList.Category = (int)Category.Important_Information;
                            }
                            else if (document.DocumentType.Code == "ATOL-CERTIFICATE")
                            {
                                docList.Category = (int)Category.ATOL_Certificate;
                            }
                            else if (document.DocumentType.Code == "Admin.E-TicketsDetails.Combined")
                            {
                                docList.Category = (int)Category.Flights;
                            }
                            else if (document.DocumentType.Code == "Admin.Accommodation.Voucher.Supplier")
                            {
                                docList.Category = (int)Category.Accomodation;
                            }
                            else if (document.DocumentType.Code == "Admin.TransferVoucher")
                            {
                                docList.Category = (int)Category.Transfer;
                            }
                            docList.CreationDateTime = document.CreationDateTime;
                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentFormat = document.DocumentFormat;
                            docList.DocumentUrl = document.DocumentUrl.Replace("10.0.0.190", ConfigurationManager.AppSettings["TopDogDocumentsURL"]);
                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentType = docType;
                            docList.UserName = document.UserName;
                            dl.Add(docList);
                        }
                        myBookingDocuments.DocumentList = dl;
                    }
                }
                //else
                //{
                //    myBookingDocuments = new MyDocuments();
                //    DocumentType docType = new DocumentType();
                //    DocumentList docList = new DocumentList();
                //    List<DocumentList> dl = new List<DocumentList>();
                //    docType.Description = "Consolidated Document";
                //    docList.Category = (int)Category.Generic;
                //    docList.DocumentUrl = ConfigurationManager.AppSettings["S3TeletextBookingUrl"].ToString() + "/" + string.Format("{0}-invoice.pdf", BookingReference.ToLower());
                //    docList.DocumentFormat = "PDF";
                //    dl.Add(docList);
                //    myBookingDocuments.DocumentList = dl;
                //}


                if (myBookingDocuments != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDocuments)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetPaymentPlan(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;
            try
            {
                BookingReference = BookingReference.ToUpper();
                var paymentPlan = TopDogManager.GetPayments(BookingReference, Surname, DepartureDate);

                if (paymentPlan != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentPlan)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream SendBookingConfirmationEmail(string BookingReference, string Surname, string DepartureDate, string EmailAddress)
        {
            var json = String.Empty;
            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                TopDogManager.SendBookingConfirmationEmail(BookingReference, Surname, DepartureDate, EmailAddress);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes("success"));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes("Failed"));
            }
        }

        #endregion

        # region Mobile App API

        public Stream GetHotelsByDestinationCode(string destCode)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelsByDestCodeURL"], destCode);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelByID(string hotelId)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelByIdURL"], hotelId);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelReviewByID(string hotelId)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelReviewByIdURL"], hotelId);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHolidaySearch(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                        string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorRating,
                                        string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType,
                                        string labelId, string preferredHotelKeys, string skipFacets, string destinationType, string platform,string isShortDescriptionRequired, string os)
        {
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixImgUrl(
                                adults, children, ratings, departureDate, dateMin, dateMax,
                                durationMin, durationMax, destinationIds, priceMin, priceMax,
                                departureIds, tripadvisorRating, sort, hotelKeysToExclude,
                                channelId, hotelKeys, tradingNameIds, boardType, labelId, preferredHotelKeys, skipFacets, destinationType);

            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            if (string.IsNullOrWhiteSpace(ArtirixServiceUrlFmt))
            {
                throw new WebFaultException<string>("Service url not found in config.", System.Net.HttpStatusCode.BadRequest);
            }

            string artirixSearchUrl = ArtirixServiceUrlFmt + "search?" + paramsString;
            if (string.IsNullOrWhiteSpace(platform))
            {
                platform = "desktop";
            }
            try
            {
                Holidays holidays = new Holidays();
                holidays = ArtirixImageSearch.GetAtxImgSrc(artirixSearchUrl, platform);
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["ArtirixCacheTime"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidays)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>(ex.Message + ". " + ex.StackTrace, System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetHolidaySearchV_2(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                        string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorRating,
                                        string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType,
                                        string labelId, string preferredHotelKeys, string skipFacets, string destinationType)
        {
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixImgUrl(
                                adults, children, ratings, departureDate, dateMin, dateMax,
                                durationMin, durationMax, destinationIds, priceMin, priceMax,
                                departureIds, tripadvisorRating, sort, hotelKeysToExclude,
                                channelId, hotelKeys, tradingNameIds, boardType, labelId, preferredHotelKeys, skipFacets, destinationType);
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            if (string.IsNullOrWhiteSpace(ArtirixServiceUrlFmt))
            {
                throw new WebFaultException<string>("Service url not found in config.", System.Net.HttpStatusCode.BadRequest);
            }
            string artirixSearchUrl = ArtirixServiceUrlFmt + "search?" + paramsString;
            string json = string.Empty;
            Boolean isPresentInRedis = false;
            try
            {
                json = RedisDataHelper.GetValueByKey(
                    artirixSearchUrl,
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["ArtirixRedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));

                isPresentInRedis = true;
            }
            catch (System.ArgumentNullException ex)
            {
                isPresentInRedis = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!isPresentInRedis)
            {
                ThreadPool.SetMaxThreads(int.Parse(ConfigurationManager.AppSettings["ArtirixMaxThreads"]), 0);
                ThreadPool.SetMinThreads(int.Parse(ConfigurationManager.AppSettings["ArtirixMinThreads"]), 0);
                ThreadPool.QueueUserWorkItem(HolidaysAsync, new object[] { artirixSearchUrl });
            }
            if (!string.IsNullOrWhiteSpace(json))
            {
                try
                {
                    Holidays holidays = new Holidays();
                    holidays = ArtirixImageSearch.GetAsyncAtxImgSrc(json);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    HolidaysAsyncResult result = new HolidaysAsyncResult()
                    {
                        isCompleted = true,
                        facets = holidays.facets,
                        offers = holidays.offers,
                        total = holidays.total,
                        totalHotels = holidays.totalHotels
                    };
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                    throw new WebFaultException<string>(ex.Message + ". " + ex.StackTrace, System.Net.HttpStatusCode.BadRequest);
                }
            }
            else
            {
                HolidaysAsyncResult result = new HolidaysAsyncResult()
                {
                    isCompleted = false
                };
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));
            }
        }

        private void HolidaysAsync(object state)
        {
            object[] artirixValue = state as object[];
            string artirixUrl = artirixValue[0].ToString();
            string artirixRespStr = Utilities.ExecuteMobileArtirixGetWebRequest(artirixUrl);
            RedisDataHelper.InsertTAValue(artirixUrl, artirixRespStr, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["ArtirixRedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
            //return artirixRespStr;
        }

        public Stream GetHolidays(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");

            if (!string.IsNullOrEmpty(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrEmpty(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrEmpty(ratings))
            {
                searchParams.Append("ratings=" + ratings + "&");
            }

            if (!string.IsNullOrEmpty(departureDate))
            {
                searchParams.Append("departureDate=" + departureDate + "&");
            }

            if (!string.IsNullOrEmpty(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrEmpty(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrEmpty(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrEmpty(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrEmpty(destinationIds))
            {
                searchParams.Append("destinationIds=" + destinationIds + "&");
            }

            if (!string.IsNullOrEmpty(priceMin))
            {
                searchParams.Append("priceMin=" + priceMin + "&");
            }

            if (!string.IsNullOrEmpty(priceMax))
            {
                searchParams.Append("priceMax=" + priceMax + "&");
            }

            if (!string.IsNullOrEmpty(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrEmpty(tripadvisorrating))
            {
                searchParams.Append("tripadvisorrating=" + tripadvisorrating + "&");
            }

            if (!string.IsNullOrEmpty(sort))
            {
                searchParams.Append("sort=" + sort + "&");
            }

            if (!string.IsNullOrEmpty(hotelKeysToExclude))
            {
                searchParams.Append("hotelKeysToExclude=" + hotelKeysToExclude + "&");
            }

            if (!string.IsNullOrEmpty(channelId))
            {
                searchParams.Append("channelId=" + channelId + "&");
            }

            if (!string.IsNullOrEmpty(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrEmpty(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "&");
            }

            if (!string.IsNullOrEmpty(boardType))
            {
                searchParams.Append("boardType=" + boardType + "&");
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            json = Utilities.GetJson<string>(url);
            Holidays holidays = JsonConvert.DeserializeObject<Holidays>(json);
            if (holidays != null && holidays.offers != null)
            {
                foreach (Offer offr in holidays.offers)
                {
                    if (offr != null && offr.hotel != null && offr.hotel.images != null)
                    {
                        int count = offr.hotel.images.Count;
                        for (int i = 0; i < count; i++)
                        {
                            offr.hotel.images[i] = string.Format(ConfigurationManager.AppSettings["ArtirixImgPath"], offr.hotel.images[i]);
                        }
                    }
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidays)));
        }

        public Stream GetMapByLocation(string lat, string lon)
        {
            string url = string.Format(ConfigurationManager.AppSettings["GoogleMapURL"], lat, lon, lat, lon);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.Proxy = null;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return responseStream;
        }

        public Stream GetHomePageSlider()
        {
            List<Slider> sliderList = Teletext.GetHomepageSlider();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sliderList)));
        }

        public Stream GetOffersAndDealsSlider()
        {
            var json = String.Empty;
            string fileName = ConfigurationManager.AppSettings["OffersandDealsS3URL"];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileName);
            request.Proxy = null;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            json = reader.ReadToEnd();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }


        public Stream GetPubsAndRestruantsByGoogleAPI(string lat, string lon, string type, string hotelId)
        {
            int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
            var s3Response = String.Empty;
            string googleKey = type + hotelId;
            bool googleResponse = false;
            //googleResponse = GoogleAPI.GetBarsAndRestaurants(lat, lon, type, googleKey, key1);                       

            while (!googleResponse && keyCount > 0)
            {
                string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                googleResponse = GoogleAPI.GetBarsAndRestaurants(lat, lon, type, googleKey, googleAPIKey);
                keyCount--;
            }
            try
            {
                string s3URL = "https://teletext-googleapi.s3.amazonaws.com/BarsandRestaurants/" + googleKey + ".json";
                s3Response = Utilities.ExecuteGoogleGetWebRequest(s3URL);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("S3 and Google returned empty response", System.Net.HttpStatusCode.BadRequest);
            }
            if (!string.IsNullOrWhiteSpace(s3Response))
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(s3Response));
        }

        public Stream GetArtirixHotelDescription(string hotelOperator, string contentId)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();
            if (!string.IsNullOrEmpty(hotelOperator) && !string.IsNullOrEmpty(contentId))
            {
                searchParams.Append("hotelOperator=" + hotelOperator + "&");
                searchParams.Append("contentId=" + contentId);
            }
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "description?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }


        //IE Code
        public Stream GetArtirixRoutingIE(string countrySite, string destinationId, string departureId, string dates)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetArtirixRouting(destinationId, departureId, dates);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && !string.IsNullOrEmpty(dates))
            {
                RoutingResponse routingResponse = new RoutingResponse();
                routingResponse.valid = true;
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(routingResponse)));

            }
            string useAritirixRouting = ConfigurationManager.AppSettings["UseAritirixRouting"];

            if (!String.IsNullOrEmpty(useAritirixRouting) && !String.IsNullOrWhiteSpace(useAritirixRouting) &&
                useAritirixRouting.ToLower().Equals("false"))
            {
                return this.GetTeletextRoutingIE(destinationId, departureId, dates, countrySite);
            }


            //TODO :Code analysis not done for the remaining part of method. Not sure if ArtirixServiceUrlFmt supports routing for countries other than UK
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();

            if (!string.IsNullOrEmpty(destinationId))
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }

            if (!string.IsNullOrEmpty(departureId))
            {
                searchParams.Append("departureId=" + departureId + "&");
            }

            if (!string.IsNullOrEmpty(dates))
            {
                searchParams.Append("dates=" + dates);
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "routing?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetArtirixRouting(string destinationId, string departureId, string dates)
        {
            if (!string.IsNullOrEmpty(destinationId) && !string.IsNullOrEmpty(departureId) && !string.IsNullOrEmpty(dates))
            {
                RoutingResponse routingResponse = new RoutingResponse();
                routingResponse.valid = true;
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(routingResponse)));

            }
            string useAritirixRouting = ConfigurationManager.AppSettings["UseAritirixRouting"];

            if (!String.IsNullOrEmpty(useAritirixRouting) && !String.IsNullOrWhiteSpace(useAritirixRouting) &&
                useAritirixRouting.ToLower().Equals("false"))
            {
                return this.GetTeletextRouting(destinationId, departureId, dates);
            }

            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();

            if (!string.IsNullOrEmpty(destinationId))
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }

            if (!string.IsNullOrEmpty(departureId))
            {
                searchParams.Append("departureId=" + departureId + "&");
            }

            if (!string.IsNullOrEmpty(dates))
            {
                searchParams.Append("dates=" + dates);
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "routing?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetArtirixDiversity(string destinationId, string hotelKeys, string departureIds, string dateMin, string dateMax, string children, string adults, string durationMax, string durationMin, string ratings)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();

            if (!string.IsNullOrEmpty(destinationId))
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }
            if (!string.IsNullOrEmpty(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrEmpty(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrEmpty(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrEmpty(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrEmpty(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrEmpty(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrEmpty(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrEmpty(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrEmpty(ratings))
            {
                searchParams.Append("ratings=" + ratings);
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "diversity?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelContent(string hotelOperator, string contentId)
        {
            var json = String.Empty;
            string url = ConfigurationManager.AppSettings["ArtirixContentDescription"] + "hotelOperator=" + hotelOperator + "&contentId=" + contentId;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetGoogleAirportInfo(string airport)
        {
            int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
            string airportkey = airport.ToLower();
            var s3Response = String.Empty;
            bool googleResponse = false;
            while (!googleResponse && keyCount > 0)
            {
                string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                googleResponse = GoogleAPI.GetAirportInfo(airport, googleAPIKey);
                keyCount--;
            }
            try
            {
                string s3URL = "https://teletext-googleapi.s3.amazonaws.com/Airports/" + airportkey + ".json";
                s3Response = Utilities.ExecuteGoogleGetWebRequest(s3URL);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("S3 and Google returned empty response", System.Net.HttpStatusCode.BadRequest);
            }
            if (!string.IsNullOrWhiteSpace(s3Response))
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(s3Response));
        }

        public Stream GetGoogleDistance(string oLat, string oLng, string dLat, string dLng, string mode)
        {
            var json = string.Empty;
            string url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + oLat + "," + oLng + "&destinations=" + dLat + "," + dLng + "&transit_mode=" + mode;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        #endregion

        # region PostCode API

        public Stream GetAddressesByPostCode(string postCode)
        {
            IPostCodeAddresses postCodeAddresses = new OpenPostCode();

            PostCodeResponse resp = postCodeAddresses.GetAddressesByPostCode(postCode);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(resp)));
        }

        #endregion

        # region SmartFocus API

        public Stream GetNewsLetterSubscription(string name, string email, string phone, string airport)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            status = MarketingService.GetNewsLetterSubscription(name, Uri.EscapeUriString(email), phone, airport);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status)));
        }
        public Stream GetSubscription(emailSubscription emailSubcription)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            status = MarketingService.GetNewsLetterSubscription(emailSubcription.name, emailSubcription.email, emailSubcription.phone, emailSubcription.airport);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status)));
        }

        public Stream GetNewsLetterSubscriberByEmail(string email)
        {
            string status = MarketingService.GetNewsLetterSubscriberByEmail(email);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(status));
        }
        #endregion


        #region SmartFocus API IE 
        //IE CODE
        public Stream GetNewsLetterSubscriptionIE(string countrySite, string name, string email, string phone, string airport)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetNewsLetterSubscription(name, email, phone, airport);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            //TODO: Currentlty defaulted to true, since smartfocus subscription is not available for IE yet
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            status.Status = true;
            status.JobId = 100000001;
            //status = MarketingService.GetNewsLetterSubscription(name, Uri.EscapeUriString(email), phone, airport);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status)));
        }
        #endregion


        #region MinPriceTool IE
        public Stream GetArtirixMinPriceNewIE(string countrySite, string adults, string children, string ratings, string departureDate, string dateMin, string dateMax,
                                       string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                       string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                       string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                       string boardType, string dateOffSet, string flexDates, string isES)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetArtirixMinPriceNew(adults, children, ratings, departureDate, dateMin, dateMax,
                           durationMin, durationMax, destinationIds, destinationName, priceMin,
                           priceMax, departureIds, departureName, tripadvisorrating, sort,
                           hotelKeysToExclude, channelId, channelName, hotelKeys, tradingNameIds,
                           boardType, dateOffSet, flexDates, isES);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            var preUrl = String.Empty;
            if (isES == "true")
            {
                preUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteSearch-ES-URL") + "/" + channelName + "/" + destinationName;
            }
            else
            {
                preUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteSearchURL") + "/" + channelName + "/" + destinationName;
            }

            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(departureName, boardType, durationMax, adults, children, ratings, priceMin, priceMax,
                                                    hotelKeys, tradingNameIds, dateOffSet, flexDates);
            try
            {
                var paramsString = string.Empty;
                paramsString = Utilities.GetArtirixUrl(adults, children, ratings, departureDate, dateMin, dateMax,
                                                        durationMin, durationMax, destinationIds, priceMin, priceMax,
                                                        departureIds, tripadvisorrating, sort, hotelKeysToExclude,
                                                        channelId, hotelKeys, tradingNameIds, boardType);
                var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
                string artirixSearchUrl = string.Empty;

                if (isES == "true")
                {
                    artirixSearchUrl = Utilities.GetWebConfigValueForMPT(countrySite, "ESHoldaySearchUrlFmt") + "ESHolidaySearch" + "/" + countrySite + "?" + paramsString;
                }
                else
                {
                    //TODO: This will neeed to modified for IE if  Artirix is being used
                    artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
                }

                MinPriceView minPriceView = new MinPriceView();
                minPriceView = MinPriceTool.MinPriceResponseViewNew(artirixSearchUrl, preUrl, postUrl, departureDate, flexDates);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));


            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }


        }



        public Stream EditorUpdateIE(string countrySite, editUpdate editUpdate, string userId, string isES)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return EditorUpdate(editUpdate, userId, isES);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;
            string outputUrl = string.Empty;

            if (isES == "true")
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
                outputUrl = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceOutput-ES");
            }
            else
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
                outputUrl = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceOutput");
            }

            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            string jsonObjectInput = Utilities.ExecuteGetWebRequest(outputUrl);
            minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);
            int countIn = minPriceToolInputObject.Count;
            int flag = 0;
            for (int test = 0; test < countIn; test++)
            {
                if (minPriceToolInputObject[test].sectionId.ToLower() == editUpdate.sectionId.ToLower() && minPriceToolInputObject[test].query.id.ToLower() == editUpdate.ID.ToLower())
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                        break;
                    }
                    else
                    {
                        throw new WebFaultException<string>("The combination of Section Id and ID already exists ", System.Net.HttpStatusCode.BadRequest);
                    }
                }
            }
            if (flag == 0)
            {
                for (int test = 0; test < countIn; test++)
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                    }
                }
            }
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName,
                    CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName,
                    CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully EditorUpdate By the User - {1}", editUpdate.queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));

        }

        public Stream GetMinPriceHomePageIE(string countrySite, string isEs)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetMinPriceHomePage(isEs);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            string inputURL = string.Empty;
            string outputURL = string.Empty;

            if (isEs == "true")
            {
                inputURL = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceInput-ES");
                outputURL = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceOutput-ES");
            }
            else
            {
                inputURL = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceInput");
                outputURL = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceoutput");
            }

            //string inputFileResponse = Utilities.ExecuteGetWebRequest(inputURL);
            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            //List<MinPriceToolObject> minPriceToolInput;
            List<MinPriceToolObject> minPriceToolOutput;
            //minPriceToolInput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(inputFileResponse);
            minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse);
            //int countIn = minPriceToolInput.Count;
            int countOut = minPriceToolOutput.Count;
            try
            {
                if (countOut != 0)
                {
                    //for (int count = 0; count < countOut; count++)
                    //{
                    //    minPriceToolInput[count].lastUpdated = minPriceToolOutput[count].lastUpdated;
                    //    minPriceToolInput[count].minPrice = minPriceToolOutput[count].minPrice;
                    //    minPriceToolInput[count].quoteRef = minPriceToolOutput[count].quoteRef;
                    //}
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetMinPriceHomePage Fetching:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }

        public Stream DeleteObjectIE(string countrySite, string queryId, string userId, string isES)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return DeleteObject(queryId, userId, isES);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
            }
            else
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
            }

            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolOutputObject = new List<MinPriceToolObject>();
            minPriceToolInputObject = MinPriceTool.DeleteInputObjectMT(countrySite, queryId, isES);
            minPriceToolOutputObject = MinPriceTool.DeleteOutputObjectMT(countrySite, queryId, isES);
            //ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully deleted By the User - {1}", queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }


        public Stream SaveorUpdateMinPriceNewIE(string countrySite, saveUpdateMinPrice saveUpdateMinPrice, string userId, string isES)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return SaveorUpdateMinPriceNew(saveUpdateMinPrice, userId, isES);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
            }
            else
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
            }

            IAmazonS3 client;

            // Read the Etag for the MinPriceOutput for reduce concurrency issues
            string oldMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

            var preUrl = String.Empty;
            if (isES == "true")
            {
                preUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteSearch-ES-URL") + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }
            else
            {
                preUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteSearchURL") + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }

            //preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(saveUpdateMinPrice.departureName, saveUpdateMinPrice.boardType, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.adults,
                                                    saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings, saveUpdateMinPrice.priceMin, saveUpdateMinPrice.priceMax,
                                                    saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds, saveUpdateMinPrice.dateOffSet, saveUpdateMinPrice.flexDates);
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixUrl(saveUpdateMinPrice.adults, saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings,
                                                    saveUpdateMinPrice.departureDate, saveUpdateMinPrice.dateMin, saveUpdateMinPrice.dateMax,
                                saveUpdateMinPrice.durationMin, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.destinationIds, saveUpdateMinPrice.priceMin,
                                saveUpdateMinPrice.priceMax, saveUpdateMinPrice.departureIds, saveUpdateMinPrice.tripadvisorrating, saveUpdateMinPrice.sort,
                                saveUpdateMinPrice.hotelKeysToExclude, saveUpdateMinPrice.channelId, saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds,
                                saveUpdateMinPrice.boardType);
            if (saveUpdateMinPrice.isDynamic)
            {
                saveUpdateMinPrice.dynamicUrl = "sample url"; //TODO: Generate url dynamically.
            }
            else
            {
                saveUpdateMinPrice.dynamicUrl = string.Empty;
            }
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            string artirixSearchUrl = string.Empty;

            if (isES == "true")
            {
                //TODO: refactor countrysite below to work for MT. 
                artirixSearchUrl = Utilities.GetWebConfigValueForMPT(countrySite, "ESHoldaySearchUrlFmt") + "ESHolidaySearch" + "/" + countrySite + "?" + paramsString;
            }
            else
            {
                //TODO:Change needed if Artirix is used of IE and other countries
                artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            }

            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.InsertNewObjectToS3MT(countrySite, artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName,
                            CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName,
                            CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.UpdateOldObjectToS3MT(countrySite, artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);
                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName,
                            CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName,
                            CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (WebFaultException<string> webExp)
            {
                if (webExp.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    ErrorLogger.Log("Lambda is updating prices so denied saving/updating from MPT", LogLevel.Information);
                    throw webExp;
                }
                else
                {
                    ErrorLogger.Log(webExp.ToString(), LogLevel.Information);
                    throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null. Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
                }

            }
            //catch (Exception ex)
            //{
            //    ErrorLogger.Log(ex.ToString(), LogLevel.Information);
            //    throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null \n Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
            //}
        }

        public Stream SetQueryImageUrlIE(string countrySite, string queryId, string sectionId, string imageUrl, string userId, string isES)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return SetQueryImageUrl(queryId, sectionId, imageUrl, userId, isES);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            List<MinPriceToolObject> minPriceQueryUrl = new List<MinPriceToolObject>();
            minPriceQueryUrl = MinPriceTool.SetQueryImageUrlMT(countrySite, queryId, sectionId, imageUrl, isES);
            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
            }
            else
            {
                awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
            }
            //string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName,
                    CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName,
                    CannedACL = S3CannedACL.PublicRead //TODO: IE - UK code appears to work without this setting. Check with that team. Any AWS account config needed
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0}, section - {1}, imageUrl - {2} successfully set the image url by the User - {3}", queryId, sectionId, imageUrl, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }

        public Stream GetSectionObjectsIE(string countrySite, string sectionIds, string platform)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetSectionObjects(sectionIds, platform);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            List<MerchandizingOffers> minPriceToolSectionObject = new List<MerchandizingOffers>();
            minPriceToolSectionObject = MinPriceTool.SectionObjectMT(countrySite, sectionIds, platform);
            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
                }
                else
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
                }
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
                }
                else
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
                }
            }
            else
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-BucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-InputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "MinPriceTool-ES-OutputFolderName");
                }
                else
                {
                    awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
                    awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
                    awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
                }
                //awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                //awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                //awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }


            //awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                if (cacheTime < 1)
                {
                    cacheTime = 900;
                }
            }

            List<MerchandizingOffers> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.id).ToList();
            }

            // Hotel landing pages url to be added
            foreach (MerchandizingOffers mptObj in result)
            {
                if (!string.IsNullOrWhiteSpace(mptObj.query.iff) && Global.mhidToLandingPageURL.ContainsKey(mptObj.query.iff))
                {
                    string queryParamsUrl = (!string.IsNullOrWhiteSpace(mptObj.query.websiteUrl)) ? mptObj.query.websiteUrl.Substring(mptObj.query.websiteUrl.IndexOf('#')) : string.Empty;
                    mptObj.query.isHotelLandingPageAvailable = true;
                    mptObj.query.hotelLandingPageUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteDomain") + "/hotels" + Global.mhidToLandingPageURL[mptObj.query.iff] + queryParamsUrl;
                }
            }


            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }


        public Stream GetSectionObjectsV2IE(string countrySite, string sectionIds, string platform)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetSectionObjectsV2(sectionIds, platform);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            List<MerchandizingOffers> minPriceToolSectionObject = new List<MerchandizingOffers>();
            minPriceToolSectionObject = MinPriceTool.SectionObjectV2MT(countrySite, sectionIds);
            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            //string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];

            List<MerchandizingOffers> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.isDynamic).ToList();
            }

            // Hotel landing pages url to be added
            foreach (MerchandizingOffers mptObj in result)
            {
                if (!string.IsNullOrWhiteSpace(mptObj.query.iff) && Global.mhidToLandingPageURL.ContainsKey(mptObj.query.iff))
                {
                    string queryParamsUrl = (!string.IsNullOrWhiteSpace(mptObj.query.websiteUrl)) ? mptObj.query.websiteUrl.Substring(mptObj.query.websiteUrl.IndexOf('#')) : string.Empty;
                    mptObj.query.isHotelLandingPageAvailable = true;
                    mptObj.query.hotelLandingPageUrl = Utilities.GetWebConfigValueForMPT(countrySite, "WebsiteDomain") + "/hotels" + Global.mhidToLandingPageURL[mptObj.query.iff] + queryParamsUrl;
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }


        public Stream GetQueryIdObjectsIE(string countrySite, string queryId, string sectionId)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return GetQueryIdObjects(queryId, sectionId);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            MinPriceToolObject minPriceToolSectionObject = new MinPriceToolObject();
            minPriceToolSectionObject = MinPriceTool.queryIdMT(countrySite, queryId, sectionId);
            string awsAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSAcessKeyId");
            string awsSceretAccessKeyId = Utilities.GetWebConfigValueForMPT(countrySite, "AWSSceretAccessKeyId");
            string awsBucketName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSBucketName");
            string awsInputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSInputFolderName");
            string awsOutputFolderKeyName = Utilities.GetWebConfigValueForMPT(countrySite, "AWSOutputFolderName");
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = lastCreated.Subtract(DateTime.UtcNow).TotalSeconds;
                // lastCreated = objRequest.ModifiedSinceDate;
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolSectionObject)));

        }

        #endregion


        #region MinPriceTool

        public Stream GetArtirixMinPrice(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates)
        {
            var preUrl = String.Empty;
            preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + channelName + "/" + destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrl(departureName, boardType, durationMax, adults, children, ratings, priceMin, priceMax,
                                                    hotelKeys, tradingNameIds, dateOffSet, flexDates);
            try
            {
                var paramsString = string.Empty;
                paramsString = Utilities.GetArtirixUrl(adults, children, ratings, departureDate, dateMin, dateMax,
                                                        durationMin, durationMax, destinationIds, priceMin, priceMax,
                                                        departureIds, tripadvisorrating, sort, hotelKeysToExclude,
                                                        channelId, hotelKeys, tradingNameIds, boardType);
                var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
                string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
                MinPriceView minPriceView = new MinPriceView();
                minPriceView = MinPriceTool.MinPriceResponseView(artirixSearchUrl, preUrl, postUrl);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));


            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }


        }


        public Stream GetMinPriceHomePage(string isEs)
        {
            string inputURL = string.Empty;
            string outputURL = string.Empty;

            if (isEs == "true")
            {
                inputURL = ConfigurationManager.AppSettings["MinPriceInput-ES"];
                outputURL = ConfigurationManager.AppSettings["MinPriceOutput-ES"];
            }
            else
            {
                inputURL = ConfigurationManager.AppSettings["MinPriceInput"];
                outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
            }

            //string inputFileResponse = Utilities.ExecuteGetWebRequest(inputURL);
            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            //List<MinPriceToolObject> minPriceToolInput;
            List<MinPriceToolObject> minPriceToolOutput;
            //minPriceToolInput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(inputFileResponse);
            minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse);
            //int countIn = minPriceToolInput.Count;
            int countOut = minPriceToolOutput.Count;
            try
            {
                if (countOut != 0)
                {
                    //for (int count = 0; count < countOut; count++)
                    //{
                    //    minPriceToolInput[count].lastUpdated = minPriceToolOutput[count].lastUpdated;
                    //    minPriceToolInput[count].minPrice = minPriceToolOutput[count].minPrice;
                    //    minPriceToolInput[count].quoteRef = minPriceToolOutput[count].quoteRef;
                    //}
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetMinPriceHomePage Fetching:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }

        public Stream DeleteObject(string queryId, string userId, string isES)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
            }
            else
            {
                awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }

            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolOutputObject = new List<MinPriceToolObject>();
            minPriceToolInputObject = MinPriceTool.DeleteInputObject(queryId, isES);
            minPriceToolOutputObject = MinPriceTool.DeleteOutputObject(queryId, isES);
            //ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully deleted By the User - {1}", queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }
        public Stream GetSectionObjects(string sectionIds, string platform)
        {
            List<MerchandizingOffers> minPriceToolSectionObject = new List<MerchandizingOffers>();
            minPriceToolSectionObject = MinPriceTool.SectionObject(sectionIds, platform);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
                }
                else
                {
                    awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                }
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
                }
                else
                {
                    awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                }
            }
            else
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
                }
                else
                {
                    awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                    awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                }
                //awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                //awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                //awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }


            //awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                if (cacheTime < 1)
                {
                    cacheTime = 900;
                }
            }

            List<MerchandizingOffers> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.id).ToList();
            }

            // Hotel landing pages url to be added
            foreach (MerchandizingOffers mptObj in result)
            {
                if (!string.IsNullOrWhiteSpace(mptObj.query.iff) && Global.mhidToLandingPageURL.ContainsKey(mptObj.query.iff))
                {
                    string queryParamsUrl = (!string.IsNullOrWhiteSpace(mptObj.query.websiteUrl)) ? mptObj.query.websiteUrl.Substring(mptObj.query.websiteUrl.IndexOf('#')) : string.Empty;
                    mptObj.query.isHotelLandingPageAvailable = true;
                    mptObj.query.hotelLandingPageUrl = ConfigurationManager.AppSettings["WebsiteDomain"] + "/hotels" + Global.mhidToLandingPageURL[mptObj.query.iff] + queryParamsUrl;
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }
        public Stream GetQueryIdObjects(string queryId, string sectionId)
        {
            MinPriceToolObject minPriceToolSectionObject = new MinPriceToolObject();
            minPriceToolSectionObject = MinPriceTool.queryId(queryId, sectionId);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = lastCreated.Subtract(DateTime.UtcNow).TotalSeconds;
                // lastCreated = objRequest.ModifiedSinceDate;
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolSectionObject)));

        }

        public Stream SetQueryImageUrl(string queryId, string sectionId, string imageUrl, string userId, string isES)
        {
            List<MinPriceToolObject> minPriceQueryUrl = new List<MinPriceToolObject>();
            minPriceQueryUrl = MinPriceTool.SetQueryImageUrl(queryId, sectionId, imageUrl, isES);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
            }
            else
            {
                awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }
            //string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0}, section - {1}, imageUrl - {2} successfully set the image url by the User - {3}", queryId, sectionId, imageUrl, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }

        public Stream GetDistinctSectionId(string sectionIdStartWith, string platform)
        {
            string outputURL = string.Empty;

            if (platform != null && platform.ToLower().Equals("mobile"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceOutput-ES"];
                }
                else
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
                }
            }
            else if (platform != null && platform.ToLower().Equals("desktop"))
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceOutput-ES"];
                }
                else
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
                }
            }
            else
            {
                if (ConfigurationManager.AppSettings["GetOffersFromNewMinPrice-ES"].Equals("true"))
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceOutput-ES"];
                }
                else
                {
                    outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
                }
            }

            //string outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
            Dictionary<string, SectionInfo> sectionInfoDictionary = new Dictionary<string, SectionInfo>();

            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            List<MinPriceToolObject> minPriceToolOutput;

            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse).Where(x => x.isFromDesktop == true).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse).Where(x => x.isFromMobile == true).ToList();
            }
            else
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse);
            }

            SectionInfo sectionInfo;
            int countIn = minPriceToolOutput.Count;
            for (int count = 0; count < countIn; count++)
            {
                if (!string.IsNullOrEmpty(sectionIdStartWith))
                {
                    var secId = minPriceToolOutput[count].sectionId.ToLower();
                    if (!sectionInfoDictionary.ContainsKey(secId) && (secId.StartsWith(sectionIdStartWith.ToLower()) || string.IsNullOrWhiteSpace(secId)))
                    {
                        sectionInfo = new SectionInfo();
                        sectionInfo.displaySectionName = minPriceToolOutput[count].displaySectionName;
                        sectionInfo.image = minPriceToolOutput[count].image;
                        sectionInfo.sectionId = minPriceToolOutput[count].sectionId;
                        sectionInfoDictionary.Add(sectionInfo.sectionId, sectionInfo);
                    }
                }
                else
                {
                    sectionInfo = new SectionInfo();
                    sectionInfo.displaySectionName = minPriceToolOutput[count].displaySectionName;
                    sectionInfo.image = minPriceToolOutput[count].image;
                    sectionInfo.sectionId = minPriceToolOutput[count].sectionId;
                    if (!sectionInfoDictionary.ContainsKey(sectionInfo.sectionId))
                    {
                        sectionInfoDictionary.Add(sectionInfo.sectionId, sectionInfo);
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sectionInfoDictionary.Values)));
        }

        public Stream EditorUpdate(editUpdate editUpdate, string userId, string isES)
        {

            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;
            string outputUrl = string.Empty;

            if (isES == "true")
            {
                awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
                outputUrl = ConfigurationManager.AppSettings["MinPriceOutput-ES"];
            }
            else
            {
                awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                outputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
            }

            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            string jsonObjectInput = Utilities.ExecuteGetWebRequest(outputUrl);
            minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);
            int countIn = minPriceToolInputObject.Count;
            int flag = 0;
            for (int test = 0; test < countIn; test++)
            {
                if (minPriceToolInputObject[test].sectionId.ToLower() == editUpdate.sectionId.ToLower() && minPriceToolInputObject[test].query.id.ToLower() == editUpdate.ID.ToLower())
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                        break;
                    }
                    else
                    {
                        throw new WebFaultException<string>("The combination of Section Id and ID already exists ", System.Net.HttpStatusCode.BadRequest);
                    }
                }
            }
            if (flag == 0)
            {
                for (int test = 0; test < countIn; test++)
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                    }
                }
            }
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully EditorUpdate By the User - {1}", editUpdate.queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));

        }

        public Stream SaveorUpdateMinPrice(saveUpdateMinPrice saveUpdateMinPrice, string userId, string isES)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
            }
            else
            {
                awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }

            IAmazonS3 client;
            var preUrl = String.Empty;

            if (isES == "true")
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearch-ES-URL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }
            else
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }

            //preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrl(saveUpdateMinPrice.departureName, saveUpdateMinPrice.boardType, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.adults,
                                                    saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings, saveUpdateMinPrice.priceMin, saveUpdateMinPrice.priceMax,
                                                    saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds, saveUpdateMinPrice.dateOffSet, saveUpdateMinPrice.flexDates);
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixUrl(saveUpdateMinPrice.adults, saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings,
                                                    saveUpdateMinPrice.departureDate, saveUpdateMinPrice.dateMin, saveUpdateMinPrice.dateMax,
                                saveUpdateMinPrice.durationMin, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.destinationIds, saveUpdateMinPrice.priceMin,
                                saveUpdateMinPrice.priceMax, saveUpdateMinPrice.departureIds, saveUpdateMinPrice.tripadvisorrating, saveUpdateMinPrice.sort,
                                saveUpdateMinPrice.hotelKeysToExclude, saveUpdateMinPrice.channelId, saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds,
                                saveUpdateMinPrice.boardType);
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            string artirixSearchUrl = string.Empty;

            if (isES == "true")
            {
                artirixSearchUrl = ConfigurationManager.AppSettings["ESHoldaySearchUrlFmt"] + "ESHolidaySearch?" + paramsString;
            }
            else
            {
                artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            }

            //string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            if (saveUpdateMinPrice.isDynamic)
            {
                saveUpdateMinPrice.dynamicUrl = "sample url"; //TODO: Generate url dynamically.
            }
            else
            {
                saveUpdateMinPrice.dynamicUrl = string.Empty;
            }
            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.InsertNewObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.UpdateOldObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);


                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Information);
                throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null \n Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetArtirixMinPriceNew(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates, string isES)
        {
            var preUrl = String.Empty;
            if (isES == "true")
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearch-ES-URL"] + "/" + channelName + "/" + destinationName;
            }
            else
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + channelName + "/" + destinationName;
            }

            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(departureName, boardType, durationMax, adults, children, ratings, priceMin, priceMax,
                                                    hotelKeys, tradingNameIds, dateOffSet, flexDates);
            try
            {
                var paramsString = string.Empty;
                paramsString = Utilities.GetArtirixUrl(adults, children, ratings, departureDate, dateMin, dateMax,
                                                        durationMin, durationMax, destinationIds, priceMin, priceMax,
                                                        departureIds, tripadvisorrating, sort, hotelKeysToExclude,
                                                        channelId, hotelKeys, tradingNameIds, boardType);
                var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
                string artirixSearchUrl = string.Empty;

                if (isES == "true")
                {
                    artirixSearchUrl = ConfigurationManager.AppSettings["ESHoldaySearchUrlFmt"] + "ESHolidaySearch?" + paramsString;
                }
                else
                {
                    artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
                }

                MinPriceView minPriceView = new MinPriceView();
                minPriceView = MinPriceTool.MinPriceResponseViewNew(artirixSearchUrl, preUrl, postUrl, departureDate, flexDates);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));


            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }


        }

        public Stream SaveorUpdateMinPriceNew(saveUpdateMinPrice saveUpdateMinPrice, string userId, string isES)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            if (isES == "true")
            {
                awsBucketName = ConfigurationManager.AppSettings["MinPriceTool-ES-BucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-InputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["MinPriceTool-ES-OutputFolderName"];
            }
            else
            {
                awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            }

            IAmazonS3 client;

            // Read the Etag for the MinPriceOutput for reduce concurrency issues
            string oldMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

            var preUrl = String.Empty;
            if (isES == "true")
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearch-ES-URL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }
            else
            {
                preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            }

            //preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(saveUpdateMinPrice.departureName, saveUpdateMinPrice.boardType, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.adults,
                                                    saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings, saveUpdateMinPrice.priceMin, saveUpdateMinPrice.priceMax,
                                                    saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds, saveUpdateMinPrice.dateOffSet, saveUpdateMinPrice.flexDates);
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixUrl(saveUpdateMinPrice.adults, saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings,
                                                    saveUpdateMinPrice.departureDate, saveUpdateMinPrice.dateMin, saveUpdateMinPrice.dateMax,
                                saveUpdateMinPrice.durationMin, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.destinationIds, saveUpdateMinPrice.priceMin,
                                saveUpdateMinPrice.priceMax, saveUpdateMinPrice.departureIds, saveUpdateMinPrice.tripadvisorrating, saveUpdateMinPrice.sort,
                                saveUpdateMinPrice.hotelKeysToExclude, saveUpdateMinPrice.channelId, saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds,
                                saveUpdateMinPrice.boardType);
            if (saveUpdateMinPrice.isDynamic)
            {
                saveUpdateMinPrice.dynamicUrl = "sample url"; //TODO: Generate url dynamically.
            }
            else
            {
                saveUpdateMinPrice.dynamicUrl = string.Empty;
            }
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            string artirixSearchUrl = string.Empty;

            if (isES == "true")
            {
                artirixSearchUrl = ConfigurationManager.AppSettings["ESHoldaySearchUrlFmt"] + "ESHolidaySearch?" + paramsString;
            }
            else
            {
                artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            }

            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.InsertNewObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.UpdateOldObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl, isES);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);
                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (WebFaultException<string> webExp)
            {
                if (webExp.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    ErrorLogger.Log("Lambda is updating prices so denied saving/updating from MPT", LogLevel.Information);
                    throw webExp;
                }
                else
                {
                    ErrorLogger.Log(webExp.ToString(), LogLevel.Information);
                    throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null. Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
                }

            }
            //catch (Exception ex)
            //{
            //    ErrorLogger.Log(ex.ToString(), LogLevel.Information);
            //    throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null \n Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
            //}
        }

        public Stream GetSectionObjectsV2(string sectionIds, string platform)
        {
            List<MerchandizingOffers> minPriceToolSectionObject = new List<MerchandizingOffers>();
            minPriceToolSectionObject = MinPriceTool.SectionObjectV2(sectionIds);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            //string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            //string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            //string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];

            List<MerchandizingOffers> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.isDynamic).ToList();
            }

            // Hotel landing pages url to be added
            foreach (MerchandizingOffers mptObj in result)
            {
                if (!string.IsNullOrWhiteSpace(mptObj.query.iff) && Global.mhidToLandingPageURL.ContainsKey(mptObj.query.iff))
                {
                    string queryParamsUrl = (!string.IsNullOrWhiteSpace(mptObj.query.websiteUrl)) ? mptObj.query.websiteUrl.Substring(mptObj.query.websiteUrl.IndexOf('#')) : string.Empty;
                    mptObj.query.isHotelLandingPageAvailable = true;
                    mptObj.query.hotelLandingPageUrl = ConfigurationManager.AppSettings["WebsiteDomain"] + "/hotels" + Global.mhidToLandingPageURL[mptObj.query.iff] + queryParamsUrl;
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }

        #endregion


        #region ElasticSearch IE
        public Stream ESHolidaySearchIE(string countrySite, int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string isShortDescriptionRequired, string platform,
                                       string os, int timeout, bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, string utmSource, string isDirectionalSelling="false")
        {
            bool isDirectionalSellingValue = false;
            Dictionary<int, string> MhidCommentsDic = new Dictionary<int, string>();

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ESHolidaySearch(destinationIds, departureIds, boardType, departureDate, dateMin,
                                       dateMax, durationMin, durationMax, adults, children, priceMin,
                                        priceMax, ratings, tradingNameIds, destinationType, hotelKeys,
                                        tripAdvisorRating, hotelKeysToExclude, sort, channelId, labelId,
                                       usersPreferredHotelKeys, skipFacets, isShortDescriptionRequired, platform,
                                       os, timeout, longDateRangeQuery, cacheHotTime, navigatedPage, pageSource, utmSource, isDirectionalSelling);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            StringBuilder errorLogger = new StringBuilder();
            #region Mandatory params check 
            //Destination Check
            if (destinationIds == 0)
            {
                throw new WebFaultException<string>("Destination cannot be Null", HttpStatusCode.BadRequest);
            }
            //Departure Check
            if (departureIds == 0)
            {
                throw new WebFaultException<string>("Departure cannot be Null", HttpStatusCode.BadRequest);
            }
            //DateMin Check 
            if (string.IsNullOrWhiteSpace(dateMin))
            {
                throw new WebFaultException<string>("DateMin cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMin, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMin cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            //DateMax Check
            if (string.IsNullOrWhiteSpace(dateMax))
            {
                throw new WebFaultException<string>("DateMax cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMax, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMax cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            #endregion
            #region Defaults
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                List<string> HotelKeys = hotelKeys.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(HotelKeys).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
                // If the hotel Key exist, send rating as 1,2,3,4,5
                //ratings = "1,2,3,4,5";
            }
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                List<string> hKTE = hotelKeysToExclude.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(hKTE).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
            }
            //HotelOffersGeneration
            isDirectionalSellingValue = (isDirectionalSelling.EqualsIgnoreCase("true") ? true : false);
            if (isDirectionalSellingValue)
            {
                var resp = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["HotelsDisplayedOnFodDirectionalSelling"]);
                List<HotelRankingsInfo> ListOfOffers = JsonConvert.DeserializeObject<List<HotelRankingsInfo>>(resp);
                var MhidWithComments = from i in ListOfOffers
                                       where int.Parse(i.destinationID) == destinationIds && i.toDisplayOnFod
                                       select new
                                       {
                                           DirectionalSellingMhids = i.mhid,
                                           Comment = i.comment
                                       };
                MhidCommentsDic = MhidWithComments.ToDictionary(x => x.DirectionalSellingMhids, x => x.Comment);
                hotelKeys = string.Join(",", MhidCommentsDic.Select(x => x.Key));

            }
            #endregion
            #region Building the API Search Request Object
            APISearchRequest searchRequest = new APISearchRequest(countrySite, destinationIds, departureIds, boardType, departureDate, dateMin,
                                       dateMax, durationMin, durationMax, adults, children, priceMin,
                                       priceMax, ratings, tradingNameIds, destinationType, hotelKeys,
                                       tripAdvisorRating, hotelKeysToExclude, sort, channelId, labelId,
                                       usersPreferredHotelKeys, skipFacets, isShortDescriptionRequired, platform, os,
                                       timeout, longDateRangeQuery, cacheHotTime, navigatedPage, pageSource, utmSource, MhidCommentsDic, isDirectionalSellingValue);

            #endregion

            HolidayOffers holidayOffers = new HolidayOffers();
            holidayOffers.isOfferFound = false;
            holidayOffers.resetStage = -1;
            int previousStage = -1;

            int numberOfLoops = 0;

            // In case of Directional Selling call, send empty response to user
            if (isDirectionalSellingValue && MhidCommentsDic.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
            }

            do
            {
                previousStage = holidayOffers.resetStage;
                if (holidayOffers.resetStage != -1)
                {
                    searchRequest = TTSSExecution.ResetSearchRequestParams(searchRequest, holidayOffers.resetStage);
                }
                SearchRequestAvailability searchRequestAvailability = ExecuteQuery.CheckRequestInSearchIndice(searchRequest, errorLogger);
                holidayOffers = OffersBuilder.GenerateOffers(searchRequestAvailability, searchRequest, errorLogger, utmSource);

                //Added to prevent infinite loop, which will result in 1000's of requests to TTSS
                //Infinite loop will occur if reset stage logic works. code is here until Resetstage can be properly tested for IE
                numberOfLoops = numberOfLoops + 1;
                if (numberOfLoops > 5)
                {
                    ErrorLogger.Log(string.Format("ESHolidaySearchIE: do-while loop count is greater than 5. please inspect the request. DestinationId={0}, DepartureId={1} ", destinationIds.ToString(), departureIds.ToString()), LogLevel.Information);
                    break;
                }
            }
            while (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && searchRequest.hotelKeys.Count == 0 && holidayOffers.totalHotels == 0 && holidayOffers.resetStage <= 4 && !holidayOffers.isOfferFound && previousStage != 4);

            if (!string.IsNullOrWhiteSpace(sort))
            {
                if (sort.Contains("tripadvisorratingdesc"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                }
                else if (sort.Contains("ratingdesc"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                }
            }
            //if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            //{
            //    Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            //}
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
        }

        public Stream ESDiversityIE(string countrySite, int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                           int children, string ratings, string hotelKeys, string tradingNameIds, string platform, string os)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ESDiversity(destinationId, departureIds, dateMin, dateMax, durationMin, durationMax, adults,
                            children, ratings, hotelKeys, tradingNameIds, platform, os);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            StringBuilder errorLogger = new StringBuilder();
            APIDiversityRequest apiDiversityRequest = new APIDiversityRequest();
            #region Defaults

            apiDiversityRequest.countrySite = countrySite;

            if (adults == 0)
            {
                apiDiversityRequest.adults = 2;
            }
            else
            {
                apiDiversityRequest.adults = adults;
            }
            apiDiversityRequest.children = children;
            apiDiversityRequest.departureIds = departureIds;
            apiDiversityRequest.dateMax = dateMax;
            apiDiversityRequest.dateMin = dateMin;
            if (durationMax == 0)
            {
                apiDiversityRequest.durationMax = 7;
                apiDiversityRequest.durationMin = 7;
            }
            else
            {
                apiDiversityRequest.durationMax = durationMax;
                apiDiversityRequest.durationMin = durationMin;
            }
            if (string.IsNullOrWhiteSpace(ratings))
            {
                apiDiversityRequest.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                apiDiversityRequest.ratings = ratings.Split(',').ToList();
            }
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                //apiDiversityRequest.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();

                string tradingIdWebConfigKey = "DefaultTradingNameIds";
                if (apiDiversityRequest.countrySite == Global.COUNTRY_SITE_IE)
                {
                    tradingIdWebConfigKey += apiDiversityRequest.countrySite.ToUpper();
                }

                apiDiversityRequest.tradingNameIds = ConfigurationManager.AppSettings[tradingIdWebConfigKey].Split(',').ToList();

            }
            else
            {
                apiDiversityRequest.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            if (destinationId == 0)
            {
                DiversityError error = new DiversityError();
                error.error = "A destinationId is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.destinationId = destinationId;
            }
            if (string.IsNullOrWhiteSpace(hotelKeys))
            {
                DiversityError error = new DiversityError();
                error.error = "At least 1 hotelKey is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.hotelKeys = hotelKeys.Split(',').ToList();
            }
            if (string.IsNullOrWhiteSpace(platform))
            {
                apiDiversityRequest.platform = "desktop";
            }
            else
            {
                apiDiversityRequest.platform = platform;
            }
            if (string.IsNullOrWhiteSpace(os))
            {
                apiDiversityRequest.os = "windows";
            }
            else
            {
                apiDiversityRequest.os = os;
            }
            #endregion
            Diversity diversity = new Diversity();
            diversity = OffersBuilder.GenerateDiversityOffers(apiDiversityRequest, errorLogger);
            //if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            //{
            //    Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            //}
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(diversity)));
        }
        #endregion


        #region ElasticSearch

        public Stream ESHolidaySearch(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string isShortDescriptionRequired, string platform,
                                       string os, int timeout, bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, string utmSource, string isDirectionalSelling)
        {
            StringBuilder errorLogger = new StringBuilder();
            bool isDirectionalSellingValue = false;
            Dictionary<int, string> MhidCommentsDic = new Dictionary<int, string>();
            #region Mandatory params check 
            //Destination Check
            if (destinationIds == 0)
            {
                throw new WebFaultException<string>("Destination cannot be Null", HttpStatusCode.BadRequest);
            }
            //Departure Check
            if (departureIds == 0)
            {
                throw new WebFaultException<string>("Departure cannot be Null", HttpStatusCode.BadRequest);
            }
            //DateMin Check 
            if (string.IsNullOrWhiteSpace(dateMin))
            {
                throw new WebFaultException<string>("DateMin cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMin, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMin cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            
            //DateMax Check
            if (string.IsNullOrWhiteSpace(dateMax))
            {
                throw new WebFaultException<string>("DateMax cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMax, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMax cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
         
            #endregion
            #region Defaults
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                List<string> HotelKeys = hotelKeys.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(HotelKeys).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
                // If the hotel Key exist, send rating as 1,2,3,4,5
                //ratings = "1,2,3,4,5";
            }
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                List<string> hKTE = hotelKeysToExclude.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(hKTE).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
            }
            //HotelOffersGeneration
            isDirectionalSellingValue = (isDirectionalSelling.EqualsIgnoreCase("true") ? true : false);
            if (isDirectionalSellingValue)
            {
                var resp = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["HotelsDisplayedOnFodDirectionalSelling"]);
                List<HotelRankingsInfo> ListOfOffers = JsonConvert.DeserializeObject<List<HotelRankingsInfo>>(resp);
                var MhidWithComments = from i in ListOfOffers where int.Parse(i.destinationID) == destinationIds && i.toDisplayOnFod
                                       select new
                                       {
                                           DirectionalSellingMhids = i.mhid,
                                           Comment = i.comment
                                       } ;
                MhidCommentsDic = MhidWithComments.ToDictionary(x => x.DirectionalSellingMhids, x => x.Comment);
                hotelKeys = string.Join(",", MhidCommentsDic.Select(x => x.Key));

            }
            #endregion
            #region Building the API Search Request Object
            APISearchRequest searchRequest = new APISearchRequest(destinationIds, departureIds, boardType, departureDate, dateMin,
                                       dateMax, durationMin, durationMax, adults, children, priceMin,
                                       priceMax, ratings, tradingNameIds, destinationType, hotelKeys,
                                       tripAdvisorRating, hotelKeysToExclude, sort, channelId, labelId,
                                       usersPreferredHotelKeys, skipFacets, isShortDescriptionRequired, platform, os,
                                       timeout, longDateRangeQuery, cacheHotTime, navigatedPage, pageSource, utmSource , MhidCommentsDic, isDirectionalSellingValue);

            #endregion

            
            HolidayOffers holidayOffers = new HolidayOffers();
            holidayOffers.isOfferFound = false;
            holidayOffers.resetStage = -1;
            int previousStage = -1;

            // In case of Directional Selling call, send empty response to user
            if(isDirectionalSellingValue && MhidCommentsDic.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
            }

            do
            {
                previousStage = holidayOffers.resetStage;
                if (holidayOffers.resetStage != -1)
                {
                    searchRequest = TTSSExecution.ResetSearchRequestParams(searchRequest, holidayOffers.resetStage);
                }
                SearchRequestAvailability searchRequestAvailability = ExecuteQuery.CheckRequestInSearchIndice(searchRequest, errorLogger);
                holidayOffers = OffersBuilder.GenerateOffers(searchRequestAvailability, searchRequest, errorLogger, utmSource);
            }
            while (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && searchRequest.hotelKeys.Count == 0 && holidayOffers.totalHotels == 0 && holidayOffers.resetStage <= 4 && !holidayOffers.isOfferFound && previousStage != 4);

            if (!string.IsNullOrWhiteSpace(sort))
            {
                if (sort.Contains("tripadvisorratingdesc"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                }
                else if (sort.Contains("ratingdesc"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                }
            }
            //if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            //{
            //    Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            //}
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
        }

        public Stream ESDiversity(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                           int children, string ratings, string hotelKeys, string tradingNameIds, string platform, string os)
        {
            StringBuilder errorLogger = new StringBuilder();
            APIDiversityRequest apiDiversityRequest = new APIDiversityRequest();
            #region Defaults
            if (adults == 0)
            {
                apiDiversityRequest.adults = 2;
            }
            else
            {
                apiDiversityRequest.adults = adults;
            }
            apiDiversityRequest.children = children;
            apiDiversityRequest.departureIds = departureIds;
            apiDiversityRequest.dateMax = dateMax;
            apiDiversityRequest.dateMin = dateMin;
            if (durationMax == 0)
            {
                apiDiversityRequest.durationMax = 7;
                apiDiversityRequest.durationMin = 7;
            }
            else
            {
                apiDiversityRequest.durationMax = durationMax;
                apiDiversityRequest.durationMin = durationMin;
            }
            if (string.IsNullOrWhiteSpace(ratings))
            {
                apiDiversityRequest.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                apiDiversityRequest.ratings = ratings.Split(',').ToList();
            }
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                apiDiversityRequest.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();
            }
            else
            {
                apiDiversityRequest.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            if (destinationId == 0)
            {
                DiversityError error = new DiversityError();
                error.error = "A destinationId is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.destinationId = destinationId;
            }
            if (string.IsNullOrWhiteSpace(hotelKeys))
            {
                DiversityError error = new DiversityError();
                error.error = "At least 1 hotelKey is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.hotelKeys = hotelKeys.Split(',').ToList();
            }
            if (string.IsNullOrWhiteSpace(platform))
            {
                apiDiversityRequest.platform = "desktop";
            }
            else
            {
                apiDiversityRequest.platform = platform;
            }
            if (string.IsNullOrWhiteSpace(os))
            {
                apiDiversityRequest.os = "windows";
            }
            else
            {
                apiDiversityRequest.os = os;
            }
            #endregion
            Diversity diversity = new Diversity();
            diversity = OffersBuilder.GenerateDiversityOffers(apiDiversityRequest, errorLogger);
            //if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            //{
            //    Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            //}
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(diversity)));
        }

        public Stream TAReviews(string taid)
        {
            string taReviews = string.Empty;
            string tsReviewsInsertionTime = string.Empty;
            string cacheTime = string.Empty;
            TAResponse taResponse = new TAResponse();
            TAReviews reviews = new TAReviews();
            ReviewsData reviewsData;
            tsReviewsInsertionTime = RedisDataHelper.GetRedisValueByKey(taid + "-Time", Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
            if (!string.IsNullOrWhiteSpace(tsReviewsInsertionTime))
            {
                if (DateTime.UtcNow < DateTime.Parse(tsReviewsInsertionTime.Substring(1, tsReviewsInsertionTime.Length - 2)).AddDays(Convert.ToInt32(ConfigurationManager.AppSettings["TAReviewsLiveTime"])))
                {
                    taReviews = RedisDataHelper.GetRedisValueByKey(taid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
                    taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                    reviews.data = new List<ReviewsData>();
                    foreach (Datum data in taResponse.data)
                    {
                        reviewsData = new ReviewsData();
                        reviewsData.published_date = data.published_date;
                        reviewsData.rating = data.rating;
                        reviewsData.title = data.title;
                        reviewsData.text = data.text;
                        reviewsData.subratings = data.subratings;
                        reviewsData.user = data.user;
                        reviewsData.user.username = data.user.username;
                        reviewsData.user.user_location.name = data.user.user_location.name;
                        reviewsData.url = data.url;
                        reviews.data.Add(reviewsData);
                    }
                    cacheTime = (DateTime.Parse(tsReviewsInsertionTime.Substring(1, tsReviewsInsertionTime.Length - 2)).AddDays(1) - DateTime.UtcNow).TotalSeconds.ToString();
                }
                else
                {
                    string taURL = ConfigurationManager.AppSettings["TAURL"] + taid + "/reviews?key=" + ConfigurationManager.AppSettings["TAKey"] + "&isTA=true";
                    var response = Utilities.ExecuteTAGetWebRequest(taURL);
                    if (!string.IsNullOrWhiteSpace(response))
                    {
                        taResponse = JsonConvert.DeserializeObject<TAResponse>(response);
                        reviews.data = new List<ReviewsData>();
                        foreach (Datum data in taResponse.data)
                        {
                            reviewsData = new ReviewsData();
                            reviewsData.published_date = data.published_date;
                            reviewsData.rating = data.rating;
                            reviewsData.title = data.title;
                            reviewsData.text = data.text;
                            reviewsData.subratings = data.subratings;
                            reviewsData.user = data.user;
                            reviewsData.user.username = data.user.username;
                            reviewsData.user.user_location.name = data.user.user_location.name;
                            reviewsData.url = data.url;
                            reviews.data.Add(reviewsData);
                        }
                        RedisDataHelper.UpdateTAValue(taid, response, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]),
                        (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                        RedisDataHelper.SetTAReviewsTime(taid + "-Time", DateTime.UtcNow.ToString("s"), ConfigurationManager.AppSettings["RedisDBForTAReviews"]);
                        cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                    }
                    else
                    {
                        taReviews = RedisDataHelper.GetRedisValueByKey(taid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
                        taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                        reviews.data = new List<ReviewsData>();
                        foreach (Datum data in taResponse.data)
                        {
                            reviewsData = new ReviewsData();
                            reviewsData.published_date = data.published_date;
                            reviewsData.rating = data.rating;
                            reviewsData.title = data.title;
                            reviewsData.text = data.text;
                            reviewsData.subratings = data.subratings;
                            reviewsData.user = data.user;
                            reviewsData.user.username = data.user.username;
                            reviewsData.user.user_location.name = data.user.user_location.name;
                            reviewsData.url = data.url;
                            reviews.data.Add(reviewsData);
                        }
                    }
                }
            }
            else
            {
                string taURL = ConfigurationManager.AppSettings["TAURL"] + taid + "/reviews?key=" + ConfigurationManager.AppSettings["TAKey"] + "&isTA=true";
                taReviews = Utilities.ExecuteTAGetWebRequest(taURL);
                if (!string.IsNullOrWhiteSpace(taReviews))
                {
                    taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                    reviews.data = new List<ReviewsData>();
                    foreach (Datum data in taResponse.data)
                    {
                        reviewsData = new ReviewsData();
                        reviewsData.published_date = data.published_date;
                        reviewsData.rating = data.rating;
                        reviewsData.title = data.title;
                        reviewsData.text = data.text;
                        reviewsData.subratings = data.subratings;
                        reviewsData.user = data.user;
                        reviewsData.user.username = data.user.username;
                        reviewsData.user.user_location.name = data.user.user_location.name;
                        reviewsData.url = data.url;
                        reviews.data.Add(reviewsData);
                    }
                    cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                    RedisDataHelper.InsertTAValue(taid, taReviews, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                    RedisDataHelper.SetTAReviewsTime(taid + "-Time", DateTime.UtcNow.ToString("s"), ConfigurationManager.AppSettings["RedisDBForTAReviews"]);
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
        }

        public Stream UpdateTAInfo()
        {
            string configUrl = ConfigurationManager.AppSettings["TAConfigURL"];
            string configResponse = Utilities.ExecuteGetWebRequest(configUrl);
            TAConfig taConfig = JsonConvert.DeserializeObject<TAConfig>(configResponse);
            string taUpdateUrl = ConfigurationManager.AppSettings["TAUpdateURL"];
            string taUpdateResponse = Utilities.ExecuteGetWebRequest(taUpdateUrl);
            List<TA> taInputModelResponse = JsonConvert.DeserializeObject<List<TA>>(taUpdateResponse);
            TAUpdateList taList = new TAUpdateList();
            taList.updatedMHUIDs = new List<int>();
            taList.updatedMHUIDs = (from taInputModel in taInputModelResponse
                                    where taInputModel.lastModifiedDate.Equals(taConfig.dateTimeLastUpdated)
                                    select taInputModel.masterHotelId).ToList();
            taList.count = taList.updatedMHUIDs.Count;
            MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            TAInfo taInfo = new TAInfo();
            Task.Factory.StartNew(() =>
            {
                for (int Count = 0; Count < taInputModelResponse.Count; Count++)
                {
                    try
                    {
                        if (taConfig.dateTimeLastUpdated == taInputModelResponse[Count].lastModifiedDate)
                        {
                            string hotelInfo = RedisDataHelper.GetRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));
                            if (!string.IsNullOrWhiteSpace(hotelInfo))
                            {
                                mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(hotelInfo);
                                var mhidTAUrl = ConfigurationManager.AppSettings["MHIDTAInfo"] + taInputModelResponse[Count].masterHotelId + ".json";
                                var mhidTAInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUrl);
                                if (!string.IsNullOrWhiteSpace(mhidTAInfoResponse))
                                {
                                    taInfo = JsonConvert.DeserializeObject<TAInfo>(mhidTAInfoResponse);
                                    mhidStaticInfo.TripAdvisor.reviewsCount = !string.IsNullOrEmpty(taInfo.numberOfReviews) ? int.Parse(taInfo.numberOfReviews) : 0;
                                    mhidStaticInfo.TripAdvisor.TARating = !string.IsNullOrEmpty(taInfo.rating) ? double.Parse(taInfo.rating) : 0;
                                    mhidStaticInfo.insertedTime = DateTime.UtcNow.ToString("s");
                                }
                                RedisDataHelper.SetHotelInfoRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), mhidStaticInfo, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));
                            }
                            string staticHotelInfo = RedisDataHelper.GetRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
                            if (!string.IsNullOrWhiteSpace(staticHotelInfo))
                            {
                                staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(staticHotelInfo);
                                var mhidTAUrl = ConfigurationManager.AppSettings["MHIDTAInfo"] + taInputModelResponse[Count].masterHotelId + ".json";
                                var mhidTAInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUrl);
                                if (!string.IsNullOrWhiteSpace(mhidTAInfoResponse))
                                {
                                    taInfo = JsonConvert.DeserializeObject<TAInfo>(mhidTAInfoResponse);
                                    staticResponseParms.reviewCount = !string.IsNullOrEmpty(taInfo.numberOfReviews) ? int.Parse(taInfo.numberOfReviews) : 0;
                                    staticResponseParms.averageRating = !string.IsNullOrEmpty(taInfo.rating) ? float.Parse(taInfo.rating) : 0;
                                }
                                RedisDataHelper.SetTAInfoRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), staticResponseParms, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log(ex.Message + "---1", LogLevel.ElasticSearch);
                    }
                }
            });
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(taList)));
        }
        public Stream AkamaiFastPurge(string urls)
        {
            // Mandatory Parameter Check
            if (string.IsNullOrWhiteSpace(urls))
            {
                throw new WebFaultException<string>("Urls cannot be Empty or NULL. Please check the urls and try again.", System.Net.HttpStatusCode.BadRequest);
            }

            List<string> urlsToPurge = new List<string>();
            urlsToPurge = urls.Split(',').ToList();

            // Trim the Urls
            for (int urlIndexer = 0; urlIndexer < urlsToPurge.Count; urlIndexer++)
            {
                urlsToPurge[urlIndexer] = urlsToPurge[urlIndexer].Trim();

                Regex regex = new Regex(@"^http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$");
                Match match = regex.Match(urlsToPurge[urlIndexer]);

                if (!match.Success)
                {
                    throw new WebFaultException<string>("One of more of the urls are not in correct format.", System.Net.HttpStatusCode.BadRequest);
                }
            }

            AkamaiPurgeRequest akamaiPurgeRequest = new AkamaiPurgeRequest();
            akamaiPurgeRequest.objects = urlsToPurge;

            var signer = new EdgeGridV1Signer();
            var credential = new ClientCredential(ConfigurationManager.AppSettings["AkamaiClientToken"], ConfigurationManager.AppSettings["AkamaiAccessToken"], ConfigurationManager.AppSettings["AkamaiClientSecretKey"]);
            var uri = new Uri(ConfigurationManager.AppSettings["AkamaiFastPurgeHost"] + ConfigurationManager.AppSettings["FastPurgeAPIURI"]);

            string purgePostRequestObj = JsonConvert.SerializeObject(akamaiPurgeRequest);

            try
            {
                byte[] byteArray = Encoding.ASCII.GetBytes(purgePostRequestObj);
                MemoryStream stream = new MemoryStream(byteArray);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";

                using (var result = signer.Execute(request, credential, stream))
                {
                    using (result)
                    {
                        using (var reader = new StreamReader(result))
                        {
                            string value = reader.ReadToEnd();
                            AkamaiPurgeResponse akamaiPurgeResponse = new AkamaiPurgeResponse();

                            akamaiPurgeResponse = JsonConvert.DeserializeObject<AkamaiPurgeResponse>(value);

                            if (akamaiPurgeResponse.httpStatus == 201)
                            {
                                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                                return new MemoryStream(Encoding.UTF8.GetBytes("Fast Purge will be completed in " + akamaiPurgeResponse.estimatedSeconds + " seconds."));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("AkamaiFastPurge returning Error.", System.Net.HttpStatusCode.BadRequest);
            }
            return null;
        }

        public Stream GetMasterHotelInfo(string mhid)
        {
            string cacheTime = string.Empty;
            MHIDStaticInfo mhidStaticInfo = Utilities.GetMasterHotelInfo(mhid, out cacheTime);
            if (mhidStaticInfo == null)
            {
                throw new WebFaultException<string>("GetMasterHotelInfo: MasterHotelId Does not exist:" + mhid, System.Net.HttpStatusCode.BadRequest);
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneDay"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhidStaticInfo)));
        }

        public Stream PostMasterHotelInfo(MHIDStaticInfo mhidStaticInfo, string userId)
        {
            //try
            //{            
            string cacheTime = string.Empty;
            var existingMasterHotelObject = Utilities.GetMasterHotelInfo(mhidStaticInfo._source.MasterHotelId.ToString(), out cacheTime);
            if (existingMasterHotelObject == null)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotelId Does not exist:" + mhidStaticInfo._source.MasterHotelId, System.Net.HttpStatusCode.BadRequest);
            }

            if (mhidStaticInfo.ETag != existingMasterHotelObject.ETag)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
            }

            var isEtagUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], mhidStaticInfo._source.MasterHotelId + ".json", mhidStaticInfo, string.Format("{0}/", ConfigurationManager.AppSettings["MasterHotelFolderPath"]));
            if (string.IsNullOrEmpty(isEtagUpdated))
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel unable to update.", System.Net.HttpStatusCode.BadRequest);
            }

            mhidStaticInfo.ETag = isEtagUpdated;
            mhidStaticInfo.insertedTime = DateTime.UtcNow.ToString("s");
            var masterHotelDataUpdated = RedisDataHelper.SetHotelInfoRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), mhidStaticInfo, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));

            if (!masterHotelDataUpdated)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
            }

            MHDescription mhDescription = new MHDescription();
            mhDescription.insertedTime = DateTime.UtcNow.ToString("s");
            mhDescription.description = mhidStaticInfo._source.Description;
            var masterHotelDescUpdated = RedisDataHelper.SetHotelDescriptionRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), mhDescription, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));

            if (!masterHotelDescUpdated)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel Desc not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
            }

            string staticHotelInfo = RedisDataHelper.GetRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
            if (!string.IsNullOrWhiteSpace(staticHotelInfo))
            {
                var staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(staticHotelInfo);
                staticResponseParms.features = mhidStaticInfo._source.Features;
                staticResponseParms.name = mhidStaticInfo._source.BuildingName;
                staticResponseParms.lat = mhidStaticInfo._source.Location.lat.ToString();
                staticResponseParms.lng = mhidStaticInfo._source.Location.lon.ToString();
                var staticHotelInfoUpdated = RedisDataHelper.SetStaticHotelInfoByKey(mhidStaticInfo._source.MasterHotelId.ToString(), staticResponseParms, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));

                if (!staticHotelInfoUpdated)
                {
                    throw new WebFaultException<string>("PostMasterHotelInfo: Static Hotel Info not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
                }
            }

            ErrorLogger.Log(string.Format("PostMasterHotelInfo - {0} successfully updated By the User - {1}", mhidStaticInfo._source.MasterHotelId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhidStaticInfo)));
            //}
            //catch (Exception ex)
            //{
            //    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
            //    throw new WebFaultException<string>("SaveGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            //}
        }

        public Stream ProcessMasterHotelImages(string MHID, string MasterHotelImages, string userId, string MHETag)
        {
            string cacheTime = string.Empty;
            var existingMasterHotelObject = Utilities.GetMasterHotelInfo(MHID, out cacheTime); //Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
            if (!existingMasterHotelObject.ETag.Contains(MHETag))
            {
                throw new WebFaultException<string>("ProcessMasterHotelImages: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
            }

            List<string> mHotelImages = MasterHotelImages.Split(',').ToList();
            if (mHotelImages == null || mHotelImages.Count() == 0)
            {
                throw new WebFaultException<string>("ProcessMasterHotelImages: Images are empty", System.Net.HttpStatusCode.BadRequest);
            }

            //Delete Object from S3
            foreach (var mHotelImage in mHotelImages)
            {
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID));
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID));
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID));
            }

            //Rename the Images.
            var imageCount = Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID));
            var thumbnailImages = Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID));
            Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID));
            //Update the images count in MasterHotel JSON object.            
            MHIDStaticInfo mhidStaticInfo = Utilities.GetMasterHotelInfo(MHID, out cacheTime);
            //            var mhidStaticInfo = GetMasterHotelInfoTest(MHID);// Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
            mhidStaticInfo._source.ImageCount = imageCount;
            //Update the JSON into Redis
            ErrorLogger.Log(string.Format("ProcessMasterHotelImages - {0} successfully updated By the User - {1}", MHID, userId), LogLevel.Information);
            return PostMasterHotelInfo(mhidStaticInfo, userId);
        }

        public Stream GetMasterHotelDescription(string mhid, string fromBot)
        {
            MHDescription mhDescription = new MHDescription();
            MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
            string cacheTime = string.Empty;
            string description = string.Empty;
            description = RedisDataHelper.GetRedisValueByKey(mhid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));
            //mhDescription = JsonConvert.DeserializeObject<MHDescription>(description);
            //var insertionDate = (DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime));
            if (string.IsNullOrWhiteSpace(description) || ((DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime)).AddDays(1) - DateTime.UtcNow).TotalSeconds < 0)
            {
                var mhidURL = ConfigurationManager.AppSettings["MHIDStaticInfo"] + mhid + ".json";
                var mhidStaticInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidURL);
                if (!string.IsNullOrEmpty(mhidStaticInfoResponse))
                {
                    mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(mhidStaticInfoResponse);
                    mhDescription.description = mhidStaticInfo._source.Description;
                    mhDescription.insertedTime = DateTime.UtcNow.ToString("s");
                    mhDescription.MHID = mhid;
                    RedisDataHelper.SetHotelDescriptionRedisValueByKey(mhid, mhDescription, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));
                    cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                }
            }
            else
            {
                mhDescription = JsonConvert.DeserializeObject<MHDescription>(description);
                var insertionDate = (DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime));
                if ((insertionDate.AddDays(1) - DateTime.UtcNow).TotalSeconds > 0)
                {
                    cacheTime = ((insertionDate.AddDays(1) - DateTime.UtcNow).TotalSeconds).ToString();
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            if (!string.IsNullOrEmpty(mhDescription.description) && !string.IsNullOrEmpty(fromBot) && fromBot.ToLower() == "true")
            {
                mhDescription.description = HTMLToText.ConvertHTMLToText(mhDescription.description);
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhDescription)));
        }

        public string AddMasterHotelTag(string tagName, string userId)
        {
            var masterHotelTagsJSON = string.Empty;
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelTagsMasterJSON"])))
            {
                masterHotelTagsJSON = reader.ReadToEnd();
            }
            List<string> masterHotelTags = JsonConvert.DeserializeObject<List<string>>(masterHotelTagsJSON);
            var isTagExists = masterHotelTags.Where(p => p == tagName);
            if (isTagExists != null && isTagExists.Count() > 0)
            {
                return tagName;
            }

            masterHotelTags.Add(tagName);
            var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelTagsMasterJSON"], masterHotelTags.OrderBy(p => p));
            ErrorLogger.Log(string.Format("AddMasterHotelTag - {0} successfully added By the User - {1}", tagName, userId), LogLevel.Information);
            return tagName;
        }

        public string AddMasterHotelFacilities(string facilityName, string userId)
        {
            var masterHotelFacilityJSON = string.Empty;
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelFacililtiesMasterJSON"])))
            {
                masterHotelFacilityJSON = reader.ReadToEnd();
            }
            List<string> masterHotelFacilities = JsonConvert.DeserializeObject<List<string>>(masterHotelFacilityJSON);
            var isTagExists = masterHotelFacilities.Where(p => p == facilityName);
            if (isTagExists != null && isTagExists.Count() > 0)
            {
                return facilityName;
            }

            masterHotelFacilities.Add(facilityName);
            var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelFacililtiesMasterJSON"], masterHotelFacilities.OrderBy(p => p));
            ErrorLogger.Log(string.Format("AddMasterHotelFacilities - {0} successfully added By the User - {1}", facilityName, userId), LogLevel.Information);
            return facilityName;
        }

        public Stream SwapMasterHotelImages(string MHID, string destImage, string MHETag, string userId)
        {
            MHIDStaticInfo existingMasterHotelObject = null;
            try
            {
                string cacheTime = string.Empty;
                existingMasterHotelObject = Utilities.GetMasterHotelInfo(MHID, out cacheTime);
                //existingMasterHotelObject =  Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
                if (!existingMasterHotelObject.ETag.Contains(MHETag))
                {
                    throw new WebFaultException<string>("ProcessMasterHotelImages: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
                }
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID), destImage);
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID), destImage);
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID), destImage);
                ErrorLogger.Log(string.Format("ProcessMasterHotelImages - {0} successfully updated By the User - {1}", MHID, userId), LogLevel.Information);
            }
            catch (Exception ex)
            { }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(existingMasterHotelObject)));
        }

        public void CreateTimeBasedIndices()
        {
            string searchIndice = ConfigurationManager.AppSettings["SearchIndice"];
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string diversityIndice = ConfigurationManager.AppSettings["DiversityIndice"];
            string searchDataType = ConfigurationManager.AppSettings["SearchIndiceDataType"];
            string diversityDataType = ConfigurationManager.AppSettings["DiversityIndiceDataType"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            int t = 0;
            if (Utilities.ExecuteGetIndiceWebRequest(searchIndice, searchDataType))
            {
                t++;
            }
            if (Utilities.ExecuteGetIndiceWebRequest(resultsIndice, resultsDataType))
            {
                t++;
            }
            if (Utilities.ExecuteGetIndiceWebRequest(diversityIndice, diversityDataType))
            {
                t++;
            }
            Utilities.TriggerIndiceInfoMail(t);
        }

        public void DeleteTimeBasedIndices()
        {
            string searchIndice = ConfigurationManager.AppSettings["SearchIndice"];
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string diversityIndice = ConfigurationManager.AppSettings["DiversityIndice"];
            string searchDataType = ConfigurationManager.AppSettings["SearchIndiceDataType"];
            string diversityDataType = ConfigurationManager.AppSettings["DiversityIndiceDataType"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            Utilities.DeleteTimeBasedIndice(searchIndice, searchDataType, -7);
            Utilities.DeleteTimeBasedIndice(resultsIndice, resultsDataType, -1);
            Utilities.DeleteTimeBasedIndice(diversityIndice, diversityDataType, -1);
        }
        public void DeleteResultsIndexResults()
        {
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            string clusterURL = ConfigurationManager.AppSettings["ES-ClusterUrl"];
            string webReqURL = clusterURL + resultsIndice + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + resultsDataType + "/_delete_by_query?conflicts=proceed";
            string postData = "{\"query\":{\"bool\":{\"must\":[{\"range\":{\"timeStamp\":{\"to\":\"" + DateTime.UtcNow.AddHours(-1).ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}}}";
            var result = Utilities.ExecutePostJsonWebRequest(webReqURL, postData);

        }

        #endregion

        #region Dynamic SEO

        public Stream GetCategoryList()
        {
            try
            {
                var categoryList = Utilities.GetCategoryList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(categoryList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetCategoryList returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream ProcessCategoryList(List<DynamicSEO.Category> categoryList, string userId)
        {
            try
            {
                var masterCategoryList = Utilities.GetCategoryList();
                foreach (var category in categoryList)
                {
                    var baseCategory = masterCategoryList.Where(p => p.categoryId == category.categoryId);
                    if (baseCategory != null && baseCategory.Count() > 0)
                    {
                        masterCategoryList.Remove(baseCategory.FirstOrDefault());
                    }

                    masterCategoryList.Add(category);
                }

                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterCategoryJSON"], masterCategoryList);
                //var isFileUpdated = Utilities.PutObjectToS3("teletext-deploy", "MasterCategory.json", masterCategoryList);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterCategoryList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("ProcessCategoryList returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream GetTemplateList()
        {
            try
            {
                var templateList = Utilities.GetTemplateList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(templateList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetTemplateList returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream ProcessTemplateList(List<DynamicSEO.Template> templateList, string userId)
        {
            try
            {
                var masterTemplateList = Utilities.GetTemplateList();
                foreach (var template in templateList)
                {
                    var baseTemplate = masterTemplateList.Where(p => p.templateId == template.templateId);
                    if (baseTemplate != null && baseTemplate.Count() > 0)
                    {
                        masterTemplateList.Remove(baseTemplate.FirstOrDefault());
                    }

                    masterTemplateList.Add(template);
                }

                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterTemplateJSON"], masterTemplateList);
                //var isFileUpdated = Utilities.PutObjectToS3("teletext-deploy", "MasterTemplate.json", masterTemplateList);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterTemplateList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("ProcessTemplateList returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream GetHoliday(string fileName)
        {
            try
            {
                var holiday = Utilities.GetHoliday(fileName.Replace("/holidays/", ""));

                // Check if the regionid exists in the guideindexlist file
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                bool doesGuideExists = false;

                List<WCFRestService.Model.Guides.Destination> destinationGuidesList = destinationGuides.destinations;

                foreach (WCFRestService.Model.Guides.Destination destination in destinationGuidesList)
                {
                    if (destination.id.Equals(holiday.regionId))
                    {
                        doesGuideExists = true;
                    }
                }

                if (holiday != null && doesGuideExists)
                {
                    var guide = Guides.Guides.GetGuide(holiday.regionId);
                    holiday.bgImage = guide.bgimage;
                    List<DynamicSEO.PlacesToGo> guideSubSections = new List<DynamicSEO.PlacesToGo>();
                    if (guide.reglevel == "1")
                    {
                        if (guide.link_summary != null)
                        {
                            guideSubSections = guide.link_summary[0].PlacesToGo.subsections.Take(3).Select(mn => new DynamicSEO.PlacesToGo { subheader = mn.subheader, thumbnailImageurl = mn.thumbnailImageurl }).ToList();
                        }
                    }
                    else
                    {
                        guideSubSections = guide.sections.Where(p => p.surl == "#places").FirstOrDefault().subsections.Take(3).Select(mn => new DynamicSEO.PlacesToGo { subheader = mn.subheader, thumbnailImageurl = mn.thumbnailImageurl }).ToList();
                    }

                    if (guideSubSections.Count() > 0)
                    {
                        holiday.placesToGo = guideSubSections;
                    }
                    else
                    {
                        holiday.placesToGo = null;
                    }

                }
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneDay"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holiday)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetHoliday returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region GoogleAPI
        public Stream GetGoogleNearBy(float lati, float logi, string patternType, string type, string radius)
        {
            string lat = lati.ToString("0.00");
            string lon = logi.ToString("0.00");
            GoogleResponse googleResponse = new GoogleResponse();
            Result result;
            GoogleBARObject resultgoogleobject = new GoogleBARObject();
            if (string.IsNullOrWhiteSpace(radius))
            {
                radius = ConfigurationManager.AppSettings["radius"];
            }
            patternType = "Pattern";
            if (!string.IsNullOrWhiteSpace(type))
            {
                if (type.Contains(","))
                {
                    DiversityError error = new DiversityError();
                    error.error = "Please check the fields";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
                }
            }
            var googleNearBy = string.Empty;
            if (GoogleAPI.GetGoogleNearBy(lat, lon, patternType, type, radius) == true)
            {
                string attracts = type;
                if (String.IsNullOrWhiteSpace(type))
                {
                    attracts = "Attractions";
                }
                var googleS3URl = "https://s3-eu-west-1.amazonaws.com/teletext-googleapi/" + patternType.ToUpper() + "/" + attracts.ToUpper() + "/" + lat + "tt" + lon + ".json";
                var googleS3URLResponse = Utilities.ExecuteGetWebRequest(googleS3URl);
                googleResponse = JsonConvert.DeserializeObject<GoogleResponse>(googleS3URLResponse);
                resultgoogleobject.results = new List<Result>();
                foreach (GogResult rst in googleResponse.results)
                {
                    result = new Result();
                    result.geometry = new Geometry();
                    result.geometry.location = new Location();
                    result.geometry.location.lat = rst.geometry.location.lat.ToString();
                    result.geometry.location.lng = rst.geometry.location.lng.ToString();
                    result.id = rst.id;
                    result.name = rst.name;
                    result.rating = rst.rating.ToString();
                    result.vicinity = rst.vicinity;
                    resultgoogleobject.results.Add(result);
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneMonth"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(resultgoogleobject)));
            }
            else
            {
                DiversityError error = new DiversityError();
                error.error = "Google didn't respond. Please try after sometime";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
        }

        public Stream GetImage(string lat, string lng, string zoom)
        {
            bool isS3Present = true;
            bool isExpiry = false;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            if (string.IsNullOrWhiteSpace(zoom))
            {
                zoom = "6";
            }
            string key = "staticmaps/" + lat + "tt" + lng + "z" + zoom + ".png";
            IAmazonS3 client;
            var result = string.Empty;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                try
                {
                    GetObjectRequest objRequest = new GetObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = key
                    };
                    GetObjectResponse objResponse = client.GetObject(objRequest);
                    if (objResponse.LastModified.AddMonths(1).Subtract(DateTime.UtcNow).TotalSeconds > 0)
                    {
                        WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + objResponse.LastModified.AddMonths(1).Subtract(DateTime.UtcNow).TotalSeconds);
                        WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                        return objResponse.ResponseStream;
                    }
                    else
                    {
                        isS3Present = true;
                        isExpiry = true;
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    isS3Present = false;
                }

            }
            if (!isS3Present || isExpiry)
            {
                try
                {
                    var webClient = new WebClient();
                    byte[] imageBytes = webClient.DownloadData("https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=" + zoom + "&size=640x304&maptype=roadmap&markers=color:red|label:.|" + lat + "," + lng + "&key=" + ConfigurationManager.AppSettings["GoogleStaticMapKey"]);
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = key
                        };
                        request.InputStream = new MemoryStream(imageBytes);
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneMonth"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                    return new MemoryStream(imageBytes);
                }
                catch (WebException ex)
                {
                    if (isExpiry)
                    {
                        Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = key
                        };
                        GetObjectResponse objResponse = client.GetObject(objRequest);
                        WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                        return objResponse.ResponseStream;
                    }
                    else
                    {
                        DiversityError error = new DiversityError();
                        error.error = "Google didn't respond. Please try after sometime";
                        return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
                    }
                }
            }
            else
            {
                DiversityError error = new DiversityError();
                error.error = "Google didn't respond. Please try after sometime";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }

        }


        public void InvalidateUATCloudFront(string urlPaths, string isAMP)
        {
            string distributionId = string.Empty;

            if (isAMP == "true")
            {
                distributionId = ConfigurationManager.AppSettings["UATAMPCloudFrontDistributionId"];
            }
            else
            {
                distributionId = ConfigurationManager.AppSettings["UATMainCloudFrontDistributionId"];
            }
            List<string> paths = new List<string>();
            var invalidatePaths = urlPaths.Split(',');
            for (int i = 0; i < invalidatePaths.Length; i++)
            {
                paths.Add(invalidatePaths[i]);
            }
            Amazon.CloudFront.AmazonCloudFrontClient client = new Amazon.CloudFront.AmazonCloudFrontClient("AKIAJURLJ7MPSQG5JINQ", "5E26AjCm94YRaC42HL9Z31+hKakuoCkz7mBtPkv9", Amazon.RegionEndpoint.EUWest1);
            CreateInvalidationRequest request = new CreateInvalidationRequest();
            request.DistributionId = distributionId;
            request.InvalidationBatch = new InvalidationBatch
            {
                CallerReference = DateTime.UtcNow.Ticks.ToString(),
                Paths = new Paths
                {
                    Items = paths,
                    Quantity = paths.Count
                }

            };
            CreateInvalidationResponse response = client.CreateInvalidation(request);
            client.Dispose();
        }

        public void InvalidatePRODCloudFront(string urlPaths)
        {
            List<string> paths = new List<string>();
            var invalidatePaths = urlPaths.Split(',');
            for (int i = 0; i < invalidatePaths.Length; i++)
            {
                paths.Add(invalidatePaths[i]);
            }
            Amazon.CloudFront.AmazonCloudFrontClient client = new Amazon.CloudFront.AmazonCloudFrontClient("AKIAIOLBY2H5PCCKK6DA", "fBgn5M/a0Vh4FXrOTIGj2j0ZSPw5ZhGS6UcKtoXE", Amazon.RegionEndpoint.EUWest1);
            CreateInvalidationRequest request = new CreateInvalidationRequest();
            request.DistributionId = "EYDRTIRGYAOZ";
            request.InvalidationBatch = new InvalidationBatch
            {
                CallerReference = DateTime.UtcNow.Ticks.ToString(),
                Paths = new Paths
                {
                    Items = paths,
                    Quantity = paths.Count
                }

            };
            CreateInvalidationResponse response = client.CreateInvalidation(request);
            client.Dispose();
        }


        #endregion

        #region Reviews        
        public Stream GetAllPublishedReviews(int limit, int startIndex, string reviewType)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(reviewType))
                {
                    reviewType = "all";
                }
                var reviews = ReviewController.GetAllPublishedReviews(limit, startIndex, reviewType);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetReviews returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream GetReviews(string limit, string startIndex, string reviewProvider, string showPublished, string showRatings)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(limit))
                {
                    limit = "30";
                }
                if (string.IsNullOrWhiteSpace(startIndex))
                {
                    startIndex = "0";
                }
                if (string.IsNullOrWhiteSpace(reviewProvider))
                {
                    reviewProvider = "all";
                }
                if (string.IsNullOrWhiteSpace(showPublished))
                {
                    showPublished = "all";
                }
                if (string.IsNullOrWhiteSpace(showRatings))
                {
                    showRatings = "1,2,3,4,5";
                }
                var reviews = ReviewController.GetReviews(limit, startIndex, reviewProvider, showPublished, showRatings);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetReviews returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream PublishorUnpublishReviews(ReviewInformation review, string userId)
        {
            ResponseSummary response = new ResponseSummary();
            string operationDone = string.Empty;

            if (review.isPublished)
            {
                // review is published previously, and we are unpublishing it
                response = ReviewController.UnpublishReview(review);
                operationDone = "unpublished";
            }
            else
            {
                // review is unpublished previsouly, and we are publishing it

                response = ReviewController.PublishReview(review);
                operationDone = "published";
            }
            ErrorLogger.Log(string.Format("Reviews Modulation Tool -- Review Id - {0} - Review Title - {1} - successfully {2} by the User - {3}", review.reviewId, review.reviewTitle, operationDone, userId), LogLevel.CustomerReviews);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }
        #endregion

        #region HomePage
        public Stream GetNearestAirportGroup(string Latitude, string Longitude)
        {
            try
            {
                var airportGroupName = Utilities.GetNearestAirportGroup(double.Parse(Latitude), double.Parse(Longitude));
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(airportGroupName));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetNearestAirportGroup returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region DeepLinks

        public void GenerateDeepLinks()
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            #region QuickQuotes
            string qqURL = "https://teletext-mobileapp-mybookings.s3.amazonaws.com/QuickQuotes/Test" + DateTime.UtcNow.ToString("yyyyMMdd") + ".csv";
            string fileNameQQ = "Quotes-" + DateTime.UtcNow.ToString("yyyyMMdd") + ".json";
            Dictionary<string, DLStatus> quoteStatus = new Dictionary<string, DLStatus>();
            quoteStatus = JsonConvert.DeserializeObject<Dictionary<string, DLStatus>>(Utilities.ExecuteDeepLinkGetWebRequest("https://teletext-mobileapp-mybookings.s3.amazonaws.com/DeepLinkStatus/QuickQuotes/" + fileNameQQ));
            try
            {
                List<QuickQuotesDL> qqDl = new List<QuickQuotesDL>();
                List<Nexmo> nexmo = new List<Nexmo>();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(qqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    while (!reader.EndOfStream)
                    {
                        var result = reader.ReadLine().Split(',');

                        if (result[15].ToLower() == "valid")
                        {
                            if (!quoteStatus.ContainsKey(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')))
                            {

                                try
                                {
                                    string MobileNumber = result[10].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    if (MobileNumber.Length == 12)
                                    {
                                        QuickQuotesDL qqdl = new QuickQuotesDL();
                                        DLStatus dlStatus = new DLStatus();
                                        dlStatus.status = false;
                                        qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                        qqdl.campaign = "MyBooking_Live";
                                        qqdl.channel = "sms";
                                        DataDL dataDL = new DataDL();
                                        dlStatus.surname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dlStatus.deptDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dlStatus.mobileNumber = MobileNumber;
                                        dlStatus.reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                        dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                        dataDL.Source = "Quote";
                                        //dataDL.ElastSearchSourceModel = "MyBooking";
                                        qqdl.data = dataDL;
                                        string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                        Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                        var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + MobileNumber + "&from=Teletext&text=Hello Mr.Yadav, Thanks for your enquiry with Teletext Holidays. Pls download our mobile app here:" + branch.url + ", to view your Quote Details and avail great holiday offers");
                                        if (nexmoResponse.Contains("\"status\": \"0\""))
                                        {
                                            dlStatus.status = true;
                                            quoteStatus.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                        else
                                        {
                                            dlStatus.status = false;
                                            quoteStatus.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                                }
                            }
                            else
                            {
                                if (quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')].status == false)
                                {
                                    var dlStatus = quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')];
                                    QuickQuotesDL qqdl = new QuickQuotesDL();
                                    qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                    qqdl.campaign = "MyBooking_Live";
                                    qqdl.channel = "sms";
                                    DataDL dataDL = new DataDL();
                                    dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                    dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                    dataDL.Source = "Quote";
                                    qqdl.data = dataDL;
                                    string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                    Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                    var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + dlStatus.mobileNumber + "&from=Teletext&text=Hello Mr.Yadav, Thanks for your enquiry with Teletext Holidays. Pls download our mobile app here:" + branch.url + ", to view your Quote Details and avail great holiday offers&client-ref=internal-QQ-Testing");
                                    if (nexmoResponse.Contains("\"status\": \"0\""))
                                    {
                                        dlStatus.status = true;
                                        quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                    else
                                    {
                                        dlStatus.status = false;
                                        quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                }
                            }
                        }
                    }
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = "teletext-mobileapp-mybookings",
                            Key = "DeepLinkStatus/QuickQuotes/" + fileNameQQ
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(quoteStatus)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region MyBooking
            string mbURL = "https://teletext-mobileapp-mybookings.s3.amazonaws.com/ConfirmedBookings/MobileAppFeed_TT_" + DateTime.UtcNow.ToString("yyyyMMdd") + ".csv";
            string fileNameMB = "MyBooking-" + DateTime.UtcNow.ToString("yyyyMMdd") + ".json";
            Dictionary<string, DLStatus> quoteStatusMB = new Dictionary<string, DLStatus>();
            quoteStatusMB = JsonConvert.DeserializeObject<Dictionary<string, DLStatus>>(Utilities.ExecuteDeepLinkGetWebRequest("https://teletext-mobileapp-mybookings.s3.amazonaws.com/DeepLinkStatus/MyBookings/" + fileNameMB));
            try
            {
                List<QuickQuotesDL> qqDl = new List<QuickQuotesDL>();
                List<Nexmo> nexmo = new List<Nexmo>();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(mbURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    while (!reader.EndOfStream)
                    {
                        var result = reader.ReadLine().Split(',');

                        if (result[15].ToLower() == "valid")
                        {
                            if (!quoteStatusMB.ContainsKey(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')))
                            {

                                try
                                {
                                    string MobileNumber = result[10].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    if (MobileNumber.Length == 12)
                                    {
                                        QuickQuotesDL qqdl = new QuickQuotesDL();
                                        DLStatus dlStatus = new DLStatus();
                                        dlStatus.status = false;
                                        qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                        qqdl.campaign = "MyBooking_Live";
                                        qqdl.channel = "sms";
                                        DataDL dataDL = new DataDL();
                                        dlStatus.surname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dlStatus.deptDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dlStatus.mobileNumber = MobileNumber;
                                        dlStatus.reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                        dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                        dataDL.Source = "MyBooking";
                                        qqdl.data = dataDL;
                                        string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                        Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                        var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + MobileNumber + "&from=Teletext&text=" + branch.url + "&client-ref=internal-TT-Testing");
                                        if (nexmoResponse.Contains("\"status\": \"0\""))
                                        {
                                            dlStatus.status = true;
                                            quoteStatusMB.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                        else
                                        {
                                            dlStatus.status = false;
                                            quoteStatusMB.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                                }
                            }
                            else
                            {
                                if (quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')].status == false)
                                {
                                    var dlStatus = quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')];
                                    QuickQuotesDL qqdl = new QuickQuotesDL();
                                    qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                    qqdl.campaign = "MyBooking_Live";
                                    qqdl.channel = "sms";
                                    DataDL dataDL = new DataDL();
                                    dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                    dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                    dataDL.Source = "Quote";
                                    qqdl.data = dataDL;
                                    string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                    Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                    var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + dlStatus.mobileNumber + "&from=Teletext&text=" + branch.url);
                                    if (nexmoResponse.Contains("\"status\": \"0\""))
                                    {
                                        dlStatus.status = true;
                                        quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                    else
                                    {
                                        dlStatus.status = false;
                                        quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                }
                            }
                        }
                    }
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = "teletext-mobileapp-mybookings",
                            Key = "DeepLinkStatus/MyBookings/" + fileNameMB
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(quoteStatusMB)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        public Stream SendAPPLink(APPLink applink)
        {
            string nexmoURL = string.Format(ConfigurationManager.AppSettings["NexmoUrl"], applink.mobileNumber, applink.from, applink.subject, applink.clientRef);
            var nexmoResponse = Utilities.ExecuteGetWebRequest(nexmoURL);
            return new MemoryStream(Encoding.UTF8.GetBytes(nexmoResponse));
        }
        #endregion

        #region LambdaHits
        public void GenerateRecentBlogsFromDB(string procedureName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(procedureName))
                {
                    throw new WebFaultException<string>("procedureName parameter cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                GetRecentBlogsBL.GenerateRecentBlogsFromDBController(procedureName);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GenerateRecentBlogsFromDB returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public void GenerateBlogsByTagsFromDB(string procedureName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(procedureName))
                {
                    throw new WebFaultException<string>("procedureName parameter cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                GetRecentBlogsBL.GenerateTagSpecificRecentBlogsFromDB(procedureName);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GenerateBlogsByTagsFromDB returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Blogs
        public Stream GetRecentBlogs(string pageName)
        {
            try
            {
                var recentBlogs = GetRecentBlogsBL.GetRecentBlogsWebController(pageName);
                double cacheTime = 0;

                if (recentBlogs.Count > 0)
                {
                    string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                    string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                    string awsBucket = string.Empty;
                    string awsKeyName = string.Empty;

                    DateTime lastCreated = DateTime.UtcNow;
                    IAmazonS3 client;

                    if (string.IsNullOrWhiteSpace(pageName))
                    {
                        awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                        awsKeyName = ConfigurationManager.AppSettings["RecentBlogsFilePath"];

                    }
                    else if (!string.IsNullOrWhiteSpace(pageName))
                    {
                        awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                        awsKeyName = ConfigurationManager.AppSettings["PageSpecificBlogsFilePath"] + pageName.Replace(" ", "-").ToLower().Trim() + ".json";
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                        {
                            BucketName = awsBucket,
                            Key = awsKeyName
                        };
                        Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                        lastCreated = objResponse.LastModified.AddHours(6);
                        cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                        if (cacheTime < 0)
                        {
                            cacheTime = 3600;
                        }
                    }
                }

                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime.ToString());
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(recentBlogs)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & Details: {1} & StackTrace; {2}", ex.Message, ex.Data, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }
        public Stream GetBlogs()
        {
            try
            {
                IAmazonS3 client;
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                string s3Etag = string.Empty;
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = "teletext-robots.prd.tt.artirix.com",
                        Key = string.Format("{0}{1}", "", ConfigurationManager.AppSettings["BlogSitemap"])

                    };
                    request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(Blogs.Blogs.GetBlogs()));
                    PutObjectResponse response2 = client.PutObject(request);
                    s3Etag = response2.ETag;
                }
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject("Success")));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & Details: {1} & StackTrace; {2}", ex.Message, ex.Data, ex.StackTrace), LogLevel.Error);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject("Failure")));
            }
        }

        public Stream CompressBlogImages()
        {
            try
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(Blogs.Blogs.CompressBlogImages())));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & Details: {1} & StackTrace; {2}", ex.Message, ex.Data, ex.StackTrace), LogLevel.Error);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject("Failure")));
            }
        }
        #endregion

        #region HotelLandingPage
        public Stream GetMasterHotelInfoByUrl(string url)
        {
            try
            {


                if (string.IsNullOrWhiteSpace(url))
                {
                    throw new WebFaultException<string>("Parameter url cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                var hotelInfo = StaticHotelLandingController.GetHotelInfoByUrl(url);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                // WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(hotelInfo)));
            }
            catch (WebFaultException wex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", wex.Message, wex.StackTrace), LogLevel.Error);
                throw wex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetMasterHotelInfoByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Payments IE
        
        public Stream ProcessPaymentIE(string countrySite, MakePayment paymentDetails)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessPayment(paymentDetails);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            try
            {
                var json = String.Empty;
                
                string apiKey = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeApiKey");
                string apiSecret = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeApiSecret");
                string accountNumber = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeAccountNumber");

                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);
                PaysafeApiClient client = null;
                if (Utilities.GetWebConfigValueForMPT(countrySite, "PaySafeEnv").ToUpper() == Global.ENV_LIVE)
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.LIVE, accountNumber);
                }
                else
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);
                }

                EnrollmentChecks enrollmentChecks = EnrollmentChecks.Builder()
                       .merchantRefNum(paymentDetails.RefNo + "-" + System.Guid.NewGuid().ToString())
                       .amount(Convert.ToInt32(TopDogManagerMT.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber) * currencyBaseUnitsMultiplier))
                       .currency(Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeCurrencyCode"))
                       .customerIp(string.IsNullOrEmpty(paymentDetails.customerIp) ? Utilities.GetWebConfigValueForMPT(countrySite, "PaySafeCustomerIP") : paymentDetails.customerIp)
                       .userAgent(paymentDetails.userAgent)
                       .card()
                           .cardNum(paymentDetails.CardNumber)
                           .cvv(paymentDetails.CCVNumber)
                           .cardExpiry()
                               .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                               .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                               .Done()
                           .Done()
                       .acceptHeader(paymentDetails.acceptHeader)
                       .merchantUrl(paymentDetails.merchantUrl)
                       .Build();

                EnrollmentChecks response = client.threeDSecureService().submit(enrollmentChecks);

                if (response.threeDEnrollment() == "N" || response.threeDEnrollment() == "U")
                {

                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                    .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                    .locale("en_US")
                                                                    .firstName(paymentDetails.FirstName)
                                                                    .lastName(paymentDetails.LastName)
                                                                    .email(paymentDetails.Email)
                                                                    .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());
                    decimal finalAmount = 0;
                    if (response.card() == null || string.IsNullOrEmpty(response.card().cardType()))
                    {
                        finalAmount = TopDogManagerMT.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber);
                    }
                    else
                    {
                        paymentDetails.SelectedCardType = response.card().cardType();
                        finalAmount = TopDogManagerMT.GetFinalAmount(paymentDetails.SelectedCardType, paymentDetails.Amount.ToString("0.00"));
                    }

                    Authorization auth = client.cardPaymentService().authorize(Authorization.Builder()
                        .customerIp(paymentDetails.customerIp)
                        .merchantRefNum(response.merchantRefNum())
                        .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                        .settleWithAuth(true)
                        .card()
                            .cvv(paymentDetails.CCVNumber)
                            .paymentToken(card.paymentToken())
                            .Done()
                        .billingDetails()
                            .street(paymentDetails.Address1 + paymentDetails.Address2)
                            .city(paymentDetails.City)
                            .zip(paymentDetails.PostCode)
                            .phone(paymentDetails.Phone)
                            .Done()
                        .Build());

                    paymentDetails.IsThreeDSecure = false;

                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                else
                {
                    paymentDetails.SelectedCardType = response.card().cardType();
                    paymentDetails.IsThreeDSecure = true;
                    paymentDetails.AcsUrl = response.acsURL();
                    paymentDetails.PaReq = response.paReq();
                }

                paymentDetails.enrollmentId = response.id();
                paymentDetails.merchantRefNum = response.merchantRefNum();
                paymentDetails = TopDogManagerMT.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }
        
        public Stream ProcessThreeDSecurePaymentIE(string countrySite, MakePayment paymentDetails)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessThreeDSecurePayment(paymentDetails);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            try
            {
                var json = String.Empty;

                string apiKey = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeApiKey");
                string apiSecret = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeApiSecret");
                string accountNumber = Utilities.GetWebConfigValueForMPT(countrySite, "PaysafeAccountNumber");

                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);

                PaysafeApiClient client = null;
                if (System.Configuration.ConfigurationManager.AppSettings["PaySafeEnv"].ToUpper() == Global.ENV_LIVE)
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.LIVE, accountNumber);
                }
                else
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);
                }

                Authentication authentications = Authentication.Builder()
                      .merchantRefNum(paymentDetails.merchantRefNum)
                      .paResp(paymentDetails.paResp)
                      .enrollmentId(paymentDetails.enrollmentId)
                      .Build();

                Authentication response_authentication = client.threeDSecureService().submit(authentications);
                string payment_id = string.Empty;

                if (response_authentication.threeDResult() == "Y" && response_authentication.status() == "COMPLETED")
                {
                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                   .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                   .locale("en_US")
                                                                   .firstName(paymentDetails.FirstName)
                                                                   .lastName(paymentDetails.LastName)
                                                                   .email(paymentDetails.Email)
                                                                   .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());

                    decimal finalAmount = TopDogManager.GetFinalAmount(paymentDetails.SelectedCardType, paymentDetails.Amount.ToString("0.00"));

                    var auth = client.cardPaymentService().authorize(Authorization.Builder()
                           .customerIp(paymentDetails.customerIp)
                           .merchantRefNum(paymentDetails.merchantRefNum)
                           .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                           .settleWithAuth(true)
                            .card()
                                .cvv(paymentDetails.CCVNumber)
                                .paymentToken(card.paymentToken())
                            .Done()
                              .authentication()
                                .eci(response_authentication.eci())
                                .cavv(response_authentication.cavv())
                                .xid(response_authentication.xid())
                                .threeDEnrollment("Y")
                                .threeDResult(response_authentication.threeDResult())
                                .signatureStatus(response_authentication.signatureStatus())
                            .Done()
                             .billingDetails()
                                    .street(paymentDetails.Address1 + paymentDetails.Address2)
                                    .city(paymentDetails.City)
                                    .zip(paymentDetails.PostCode)
                                    .phone(paymentDetails.Phone)
                            .Done()
                           .Build());


                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                paymentDetails = TopDogManager.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }

        public void MakePaymentProcessIE(string countrySite, MakePayment paymentDetails)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
               MakePaymentProcess(paymentDetails);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            try
            {
                if (paymentDetails.PaysafeAuthorisationCode != null && paymentDetails.IsSuccessful)
                {
                    decimal cardCharge = 0;
                    bool isPercentType = false;
                    if (paymentDetails.CardType == paymentDetails.SelectedCardType)
                    {
                        TopDogManagerMT.GetCardPercentage(paymentDetails.CardType, paymentDetails.CardNumber, ref cardCharge, ref isPercentType);
                    }
                    else
                    {
                        TopDogManagerMT.GetCardPercentage(paymentDetails.SelectedCardType, ref cardCharge, ref isPercentType);
                    }
                    paymentDetails.CardCharges = cardCharge;
                    paymentDetails.CardId = TopDogManagerMT.GetCardId(TopDogManagerMT.FindCardType(paymentDetails.CardNumber, paymentDetails.SelectedCardType));
                    paymentDetails.PaymentDate = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    paymentDetails.TopDogError = "MakePaymentProcess- Reached Code";
                    TopDogManagerMT.MakePaymentProcess(paymentDetails);
                    bool mailSent = false;
                    /******SUCCESSFULL EMAIL**************/ //TODO
                    Guid outResult = System.Guid.NewGuid();
                    if (!System.Guid.TryParse(paymentDetails.RefNo, out outResult))
                        mailSent = TopDogManagerMT.SendTakePaymentConfirmationEmail(countrySite, paymentDetails.Email, "Your payment has been received: " + paymentDetails.RefNo, paymentDetails.merchantRefNum, paymentDetails);
                    /******SUCCESSFULL EMAIL**************/

                    try
                    {
                        TopDogManagerMT mybookingManager = new TopDogManagerMT();
                        TopDogManagerMT.TopDogPaymentPost topDogPaymentPost = new TopDogManagerMT.TopDogPaymentPost(mybookingManager.AddPayment);
                        topDogPaymentPost.BeginInvoke(countrySite,paymentDetails, new AsyncCallback(TaskCompleted), paymentDetails.merchantRefNum);
                    }
                    catch (Exception ex)
                    {
                        paymentDetails.IsTopDogPostingSuccess = false;
                        string errstr = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
                        paymentDetails.TopDogError = errstr;
                        TopDogManagerMT.MakePaymentProcess(paymentDetails);
                    }
                }
                else
                {
                    decimal cardCharge = 0;
                    bool isPercentType = false;
                    if (paymentDetails.CardType == paymentDetails.SelectedCardType)
                    {
                        TopDogManagerMT.GetCardPercentage(paymentDetails.CardType, paymentDetails.CardNumber, ref cardCharge, ref isPercentType);
                    }
                    else
                    {
                        TopDogManagerMT.GetCardPercentage(paymentDetails.SelectedCardType, ref cardCharge, ref isPercentType);
                    }
                    paymentDetails.CardCharges = cardCharge;
                    paymentDetails.CardId = TopDogManagerMT.GetCardId(TopDogManagerMT.FindCardType(paymentDetails.CardNumber, paymentDetails.SelectedCardType));
                    paymentDetails.PaymentDate = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    paymentDetails.TopDogError = "MakePaymentProcess- Reached Code 1";
                    TopDogManagerMT.MakePaymentProcess(paymentDetails);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = string.Empty;
                if (ex.InnerException != null)
                {
                    errorMessage = ex.InnerException.Message;
                }
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    errorMessage = errorMessage + "-------" + ex.Message;
                }
                if (!string.IsNullOrEmpty(ex.StackTrace))
                {
                    errorMessage = errorMessage + "-------" + ex.StackTrace;
                }
                paymentDetails.paymentError = errorMessage;
                TopDogManagerMT.MakePaymentProcess(paymentDetails);
            }
        }
        
        #endregion Payments IE


        #region Payments

        public Stream ProcessPaymentTesting(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = "67710-Teleappliant";//System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                string apiSecret = "B-qa2-0-5850184a-0-302c02140d56b0c3c1ba0e4c7714e674aaaaba71f4aaa3a802147a43a44e36c011084c4d9af8a5450eae7129da39";// System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                string accountNumber = "1001077320";// System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);
                PaysafeApiClient client = null;

                client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);

                EnrollmentChecks enrollmentChecks = EnrollmentChecks.Builder()
                       .merchantRefNum(paymentDetails.RefNo)
                       .amount(Convert.ToInt32(paymentDetails.Amount * currencyBaseUnitsMultiplier))
                       .currency("GBP")
                       .customerIp(string.IsNullOrEmpty(paymentDetails.customerIp) ? System.Configuration.ConfigurationManager.AppSettings["PaySafeCustomerIP"] : paymentDetails.customerIp)
                       .userAgent(paymentDetails.userAgent)
                       .card()
                           .cardNum(paymentDetails.CardNumber)
                           .cvv(paymentDetails.CCVNumber)
                           .cardExpiry()
                               .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                               .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                               .Done()
                           .Done()
                       .acceptHeader(paymentDetails.acceptHeader)
                       .merchantUrl(paymentDetails.merchantUrl)
                       .Build();

                EnrollmentChecks response = client.threeDSecureService().submit(enrollmentChecks);

                if (response.threeDEnrollment() == "N" || response.threeDEnrollment() == "U")
                {

                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                    .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                    .locale("en_US")
                                                                    .firstName(paymentDetails.FirstName)
                                                                    .lastName(paymentDetails.LastName)
                                                                    .email(paymentDetails.Email)
                                                                    .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());
                    decimal finalAmount = 0;
                    if (response.card() == null || string.IsNullOrEmpty(response.card().cardType()))
                    {
                        finalAmount = paymentDetails.Amount;
                    }
                    else
                    {
                        paymentDetails.SelectedCardType = response.card().cardType();
                        finalAmount = paymentDetails.Amount;
                    }

                    Authorization auth = client.cardPaymentService().authorize(Authorization.Builder()
                        .customerIp(paymentDetails.customerIp)
                        .merchantRefNum(response.merchantRefNum())
                        .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                        .settleWithAuth(true)
                        .card()
                            .cvv(paymentDetails.CCVNumber)
                            .paymentToken(card.paymentToken())
                            .Done()
                        .billingDetails()
                            .street(paymentDetails.Address1 + paymentDetails.Address2)
                            .city(paymentDetails.City)
                            .zip(paymentDetails.PostCode)
                            .phone(paymentDetails.Phone)
                            .Done()
                        .Build());

                    paymentDetails.IsThreeDSecure = false;

                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                else
                {
                    paymentDetails.SelectedCardType = response.card().cardType();
                    paymentDetails.IsThreeDSecure = true;
                    paymentDetails.AcsUrl = response.acsURL();
                    paymentDetails.PaReq = response.paReq();
                }

                paymentDetails.enrollmentId = response.id();
                paymentDetails.merchantRefNum = response.merchantRefNum();
                paymentDetails = TopDogManager.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }

        public Stream ProcessPayment(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                string apiSecret = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                string accountNumber = System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);
                PaysafeApiClient client = null;
                if (System.Configuration.ConfigurationManager.AppSettings["PaySafeEnv"].ToLower() == "live")
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.LIVE, accountNumber);
                }
                else
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);
                }

                EnrollmentChecks enrollmentChecks = EnrollmentChecks.Builder()
                       .merchantRefNum(paymentDetails.RefNo + "-" + System.Guid.NewGuid().ToString())
                       .amount(Convert.ToInt32(TopDogManager.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber) * currencyBaseUnitsMultiplier))
                       .currency("GBP")
                       .customerIp(string.IsNullOrEmpty(paymentDetails.customerIp) ? System.Configuration.ConfigurationManager.AppSettings["PaySafeCustomerIP"] : paymentDetails.customerIp)
                       .userAgent(paymentDetails.userAgent)
                       .card()
                           .cardNum(paymentDetails.CardNumber)
                           .cvv(paymentDetails.CCVNumber)
                           .cardExpiry()
                               .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                               .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                               .Done()
                           .Done()
                       .acceptHeader(paymentDetails.acceptHeader)
                       .merchantUrl(paymentDetails.merchantUrl)
                       .Build();

                EnrollmentChecks response = client.threeDSecureService().submit(enrollmentChecks);

                if (response.threeDEnrollment() == "N" || response.threeDEnrollment() == "U")
                {

                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                    .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                    .locale("en_US")
                                                                    .firstName(paymentDetails.FirstName)
                                                                    .lastName(paymentDetails.LastName)
                                                                    .email(paymentDetails.Email)
                                                                    .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());
                    decimal finalAmount = 0;
                    if (response.card() == null || string.IsNullOrEmpty(response.card().cardType()))
                    {
                        finalAmount = TopDogManager.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber);
                    }
                    else
                    {
                        paymentDetails.SelectedCardType = response.card().cardType();
                        finalAmount = TopDogManager.GetFinalAmount(paymentDetails.SelectedCardType, paymentDetails.Amount.ToString("0.00"));
                    }

                    Authorization auth = client.cardPaymentService().authorize(Authorization.Builder()
                        .customerIp(paymentDetails.customerIp)
                        .merchantRefNum(response.merchantRefNum())
                        .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                        .settleWithAuth(true)
                        .card()
                            .cvv(paymentDetails.CCVNumber)
                            .paymentToken(card.paymentToken())
                            .Done()
                        .billingDetails()
                            .street(paymentDetails.Address1 + paymentDetails.Address2)
                            .city(paymentDetails.City)
                            .zip(paymentDetails.PostCode)
                            .phone(paymentDetails.Phone)
                            .Done()
                        .Build());

                    paymentDetails.IsThreeDSecure = false;

                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                else
                {
                    paymentDetails.SelectedCardType = response.card().cardType();
                    paymentDetails.IsThreeDSecure = true;
                    paymentDetails.AcsUrl = response.acsURL();
                    paymentDetails.PaReq = response.paReq();
                }

                paymentDetails.enrollmentId = response.id();
                paymentDetails.merchantRefNum = response.merchantRefNum();
                paymentDetails = TopDogManager.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }

        public Stream ProcessThreeDSecurePaymentTesting(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = "67710-Teleappliant";//System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                string apiSecret = "B-qa2-0-5850184a-0-302c02140d56b0c3c1ba0e4c7714e674aaaaba71f4aaa3a802147a43a44e36c011084c4d9af8a5450eae7129da39";// System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                string accountNumber = "1001077320";// System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);

                //string apiKey = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                //string apiSecret = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                //string accountNumber = System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                //int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);

                PaysafeApiClient client = null;                

                client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);

                Authentication authentications = Authentication.Builder()
                      .merchantRefNum(paymentDetails.merchantRefNum)
                      .paResp(paymentDetails.paResp)
                      .enrollmentId(paymentDetails.enrollmentId)
                      .Build();

                Authentication response_authentication = client.threeDSecureService().submit(authentications);
                string payment_id = string.Empty;

                if (response_authentication.threeDResult() == "Y" && response_authentication.status() == "COMPLETED")
                {
                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                   .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                   .locale("en_US")
                                                                   .firstName(paymentDetails.FirstName)
                                                                   .lastName(paymentDetails.LastName)
                                                                   .email(paymentDetails.Email)
                                                                   .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());

                    decimal finalAmount = paymentDetails.Amount;

                    var auth = client.cardPaymentService().authorize(Authorization.Builder()
                           .customerIp(paymentDetails.customerIp)
                           .merchantRefNum(paymentDetails.merchantRefNum)
                           .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                           .settleWithAuth(true)
                            .card()
                                .cvv(paymentDetails.CCVNumber)
                                .paymentToken(card.paymentToken())
                            .Done()
                              .authentication()
                                .eci(response_authentication.eci())
                                .cavv(response_authentication.cavv())
                                .xid(response_authentication.xid())
                                .threeDEnrollment("Y")
                                .threeDResult(response_authentication.threeDResult())
                                .signatureStatus(response_authentication.signatureStatus())
                            .Done()
                             .billingDetails()
                                    .street(paymentDetails.Address1 + paymentDetails.Address2)
                                    .city(paymentDetails.City)
                                    .zip(paymentDetails.PostCode)
                                    .phone(paymentDetails.Phone)
                            .Done()
                           .Build());


                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                paymentDetails = TopDogManager.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }

        public Stream ProcessThreeDSecurePayment(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                string apiSecret = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                string accountNumber = System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);

                PaysafeApiClient client = null;
                if (System.Configuration.ConfigurationManager.AppSettings["PaySafeEnv"].ToLower() == "live")
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.LIVE, accountNumber);
                }
                else
                {
                    client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);
                }

                Authentication authentications = Authentication.Builder()
                      .merchantRefNum(paymentDetails.merchantRefNum)
                      .paResp(paymentDetails.paResp)
                      .enrollmentId(paymentDetails.enrollmentId)
                      .Build();

                Authentication response_authentication = client.threeDSecureService().submit(authentications);
                string payment_id = string.Empty;

                if (response_authentication.threeDResult() == "Y" && response_authentication.status() == "COMPLETED")
                {
                    Profile profile = client.customerVaultService().create(Profile.Builder()
                                                                   .merchantCustomerId(System.Guid.NewGuid().ToString())
                                                                   .locale("en_US")
                                                                   .firstName(paymentDetails.FirstName)
                                                                   .lastName(paymentDetails.LastName)
                                                                   .email(paymentDetails.Email)
                                                                   .Build());

                    Card card = client.customerVaultService().create(Card.Builder()
                                                            .cardNum(paymentDetails.CardNumber)
                                                            .cardExpiry()
                                                                .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                                                .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                                                .Done()
                                                            .profileId(profile.id())
                                                            .Build());

                    decimal finalAmount = TopDogManager.GetFinalAmount(paymentDetails.SelectedCardType, paymentDetails.Amount.ToString("0.00"));

                    var auth = client.cardPaymentService().authorize(Authorization.Builder()
                           .customerIp(paymentDetails.customerIp)
                           .merchantRefNum(paymentDetails.merchantRefNum)
                           .amount(Convert.ToInt32(finalAmount * currencyBaseUnitsMultiplier))
                           .settleWithAuth(true)
                            .card()
                                .cvv(paymentDetails.CCVNumber)
                                .paymentToken(card.paymentToken())
                            .Done()
                              .authentication()
                                .eci(response_authentication.eci())
                                .cavv(response_authentication.cavv())
                                .xid(response_authentication.xid())
                                .threeDEnrollment("Y")
                                .threeDResult(response_authentication.threeDResult())
                                .signatureStatus(response_authentication.signatureStatus())
                            .Done()
                             .billingDetails()
                                    .street(paymentDetails.Address1 + paymentDetails.Address2)
                                    .city(paymentDetails.City)
                                    .zip(paymentDetails.PostCode)
                                    .phone(paymentDetails.Phone)
                            .Done()
                           .Build());


                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = auth.authCode();
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                paymentDetails = TopDogManager.MakePaymentProcess(paymentDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                paymentDetails.ErrorMessages = ex.Message;
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
        }

        public void MakePaymentProcess(MakePayment paymentDetails)
        {
            try
            {
                if (paymentDetails.PaysafeAuthorisationCode != null && paymentDetails.IsSuccessful)
                {
                    decimal cardCharge = 0;
                    bool isPercentType = false;
                    if (paymentDetails.CardType == paymentDetails.SelectedCardType)
                    {
                        TopDogManager.GetCardPercentage(paymentDetails.CardType, paymentDetails.CardNumber, ref cardCharge, ref isPercentType);
                    }
                    else
                    {
                        TopDogManager.GetCardPercentage(paymentDetails.SelectedCardType, ref cardCharge, ref isPercentType);
                    }
                    paymentDetails.CardCharges = cardCharge;
                    paymentDetails.CardId = TopDogManager.GetCardId(TopDogManager.FindCardType(paymentDetails.CardNumber, paymentDetails.SelectedCardType));
                    paymentDetails.PaymentDate = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    paymentDetails.TopDogError = "MakePaymentProcess- Reached Code";
                    TopDogManager.MakePaymentProcess(paymentDetails);
                    bool mailSent = false;
                    /******SUCCESSFULL EMAIL**************/ //TODO
                    mailSent = TopDogManager.SendTakePaymentConfirmationEmail(paymentDetails.Email, "Your payment has been received: " + paymentDetails.RefNo, paymentDetails.merchantRefNum, paymentDetails);
                    /******SUCCESSFULL EMAIL**************/

                    try
                    {
                        TopDogManager mybookingManager = new TopDogManager();
                        TopDogManager.TopDogPaymentPost topDogPaymentPost = new TopDogManager.TopDogPaymentPost(mybookingManager.AddPayment);
                        topDogPaymentPost.BeginInvoke(paymentDetails, new AsyncCallback(TaskCompleted), paymentDetails.merchantRefNum);
                    }
                    catch (Exception ex)
                    {
                        paymentDetails.IsTopDogPostingSuccess = false;
                        string errstr = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
                        paymentDetails.TopDogError = errstr;
                        TopDogManager.MakePaymentProcess(paymentDetails);
                    }
                }
                else
                {
                    decimal cardCharge = 0;
                    bool isPercentType = false;
                    if (paymentDetails.CardType == paymentDetails.SelectedCardType)
                    {
                        TopDogManager.GetCardPercentage(paymentDetails.CardType, paymentDetails.CardNumber, ref cardCharge, ref isPercentType);
                    }
                    else
                    {
                        TopDogManager.GetCardPercentage(paymentDetails.SelectedCardType, ref cardCharge, ref isPercentType);
                    }
                    paymentDetails.CardCharges = cardCharge;
                    paymentDetails.CardId = TopDogManager.GetCardId(TopDogManager.FindCardType(paymentDetails.CardNumber, paymentDetails.SelectedCardType));
                    paymentDetails.PaymentDate = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    paymentDetails.TopDogError = "MakePaymentProcess- Reached Code 1";
                    TopDogManager.MakePaymentProcess(paymentDetails);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = string.Empty;
                if (ex.InnerException != null)
                {
                    errorMessage = ex.InnerException.Message;
                }
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    errorMessage = errorMessage + "-------" + ex.Message;
                }
                if (!string.IsNullOrEmpty(ex.StackTrace))
                {
                    errorMessage = errorMessage + "-------" + ex.StackTrace;
                }
                paymentDetails.paymentError = errorMessage;
                TopDogManager.MakePaymentProcess(paymentDetails);
            }
        }

        public void TaskCompleted(IAsyncResult asyncResult)
        {
            string transactionId = (string)asyncResult.AsyncState;
            if (asyncResult.IsCompleted)
            {
                //MakePayment objPay = TopDogManager.GetPaymentDetails(transactionId);
            }
        }

        public Stream GetCreditCardCharges()
        {
            try
            {
                //MakePayment makepayment = new MakePayment();

                //makepayment.RefNo = "TT1234567";
                //makepayment.FirstName = "Chandrakanth_Modified3";
                //makepayment.LastName = "Eega3";
                //makepayment.Email = "Email3";
                //makepayment.Amount = 1000;
                //makepayment.TotalDueAmount = 12003;
                //makepayment.CardCharges = 123;
                //makepayment.IsSuccessful = true;
                //makepayment.PaymentDate = System.DateTime.Now;
                //makepayment.DepartureDate = System.DateTime.Now.AddDays(45);
                //makepayment.PaymentDueDate = System.DateTime.Now.AddDays(-45);
                //makepayment.TransactionID = System.Guid.NewGuid().ToString();
                //makepayment.PaysafeAuthorisationCode = System.Guid.NewGuid().ToString();
                //makepayment.paymentError = "Payment Error";
                //makepayment.TopDogError = "TopDogError";
                //makepayment.IsTopDogPostingSuccess = true;
                //makepayment.CardId = 1;
                //makepayment.PaymentId = 3;
                //TopDogManager.MakePaymentProcess(makepayment);

                var creditCardCharges = TopDogManager.GetCreditCardCharges();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(creditCardCharges)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetCreditCardCharges returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream GetCreditCardChargesTesting()
        {
            try
            {
                var creditCardCharges = TopDogManager.GetCreditCardChargesTesting();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(creditCardCharges)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetCreditCardCharges returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        #endregion Payments

        #region CustomerSupport

        public Stream TeleappliantCustomerSupportForm(TeleappliantCustomerSupportForm customerSupportDetails)
        {
            try
            {

                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["CustomerSupportBucket"], System.Guid.NewGuid() + ".json", customerSupportDetails);
                return new MemoryStream(Encoding.UTF8.GetBytes("Success"));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("TeleappliantCustomerSupportForm returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream CustomerSupportRequest(CustomerSupportForm customerSupportDetails)
        {
            try
            {
                if (customerSupportDetails == null)
                {
                    throw new WebFaultException<string>("Post call object cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                var queryResponse = CustomerSupportController.submitCustomerSupportRequest(customerSupportDetails);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(queryResponse)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("CustomerSupportRequest returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Teleappliant

        public Stream GetTeleappliantNumbers(string userSessionInfo)
        {
            string contents = string.Empty;
            try
            {
                JObject jObject = JObject.Parse(userSessionInfo);
                string name = (string)jObject["session"];

                using (var httpClient = new System.Net.Http.HttpClient())
                {
                    var url = "http://tt-api.voipoffice.org/api/numbers/assign";
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer dppqTHcO6PfNUahAXRpYUzB1yo1MHPaM7I6IABwY");
                    httpClient.DefaultRequestHeaders.Accept
          .Add(new MediaTypeWithQualityHeaderValue("application/x.numberapi.v2+json"));//ACCEPT header               

                    var formContent = new StringContent(userSessionInfo,
                                        Encoding.UTF8,
                                        "application/json");//CONTENT-TYPE header

                    var response = httpClient.PostAsync(url, formContent).Result;
                    contents = response.Content.ReadAsStringAsync().Result;
                }

                if (ConfigurationManager.AppSettings["EnableTeleappliantLogging"].ToLower() == "true")
                {
                    var message = "Request:" + "\r\n\r\n" + userSessionInfo + "\r\n\r\n\r\n\r\n\r\n" + "Response:" + "\r\n\r\n" + contents;
                    string logPath = HttpRuntime.AppDomainAppPath + @"Log\" + "TeleappliantReport\\" + name + "\\";
                    string filepath = logPath + name + "-" + System.DateTime.Now.ToString("yyyyMMddTHHmmss") + ".txt";

                    if (!Directory.Exists(logPath))
                    {
                        Directory.CreateDirectory(logPath);
                    }

                    if (File.Exists(filepath))
                    {
                        using (StreamWriter writer = new StreamWriter(filepath, true))
                        {
                            writer.WriteLine(message);
                        }
                    }
                    else
                    {
                        StreamWriter writer = File.CreateText(filepath);
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(contents));
        }

        #endregion Teleappliant

        #region Email 
        public void SendEmail(Email email)
        {
            try
            {
                Utilities.SendEmail(email.Subject, email.Body, email.EmailTo);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("SendEmail returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public string CreateVisionTicket(VisionTicket visionTicket)
        {
            string result = string.Empty;
            try
            {
                result = CreateVisioTicket(visionTicket).Result;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("CreateVisionTicket returned an error response", System.Net.HttpStatusCode.BadRequest);
            }

            return result;
        }

        public string SubmitCustomerDetails()
        {
            string result = string.Empty;
            try
            {
                var httpContext = HttpContext.Current;
                var httpResponse = httpContext.Response;
                var httpRequest = httpContext.Request;
                result = CreateVisioTicket(httpRequest).Result;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("SubmitCustomerDetails returned an error response", System.Net.HttpStatusCode.BadRequest);
            }

            return result;
        }

        #endregion Email

        private async Task<string> CreateVisioTicket(VisionTicket visionTicket)
        {
            HttpClient httpClient = new HttpClient();
            MultipartFormDataContent form = new MultipartFormDataContent();

            form.Add(new StringContent(visionTicket.vis_txtusername), "vis_txtusername");
            form.Add(new StringContent(visionTicket.vis_txttoken), "vis_txttoken");
            form.Add(new StringContent(visionTicket.vis_module), "vis_module");
            form.Add(new StringContent(visionTicket.vis_operation), "vis_operation");
            form.Add(new StringContent(visionTicket.vis_domain), "vis_domain");
            form.Add(new StringContent(visionTicket.vis_department), "vis_department");
            form.Add(new StringContent(visionTicket.vis_status), "vis_status");
            form.Add(new StringContent(visionTicket.vis_priority), "vis_priority");
            form.Add(new StringContent(visionTicket.vis_type), "vis_type");
            form.Add(new StringContent(visionTicket.vis_send_message), "vis_send_message");
            form.Add(new StringContent(visionTicket.vis_private), "vis_private");
            form.Add(new StringContent(visionTicket.vis_sendmail), "vis_sendmail");
            form.Add(new StringContent(visionTicket.vis_as_client), "vis_as_client");
            form.Add(new StringContent(visionTicket.vis_email), "vis_email");
            form.Add(new StringContent(visionTicket.vis_from), "vis_from");
            form.Add(new StringContent(visionTicket.vis_subject), "vis_subject");
            form.Add(new StringContent(visionTicket.vis_ticket_post), "vis_ticket_post");
            form.Add(new StringContent(visionTicket.vis_category), "vis_category");
            form.Add(new StringContent(visionTicket.vis_label), "vis_label");
            form.Add(new StringContent(visionTicket.vis_channel), "vis_channel");

            HttpResponseMessage response = await httpClient.PostAsync("http://support.teletext.co.uk/api/index.php", form);

            response.EnsureSuccessStatusCode();
            httpClient.Dispose();
            string sd = response.Content.ReadAsStringAsync().Result;
            return sd;
        }


        private async Task<string> CreateVisioTicket(HttpRequest httpRequest)
        {
            HttpClient httpClient = new HttpClient();
            MultipartFormDataContent form = new MultipartFormDataContent();

            form.Add(new StringContent(httpRequest.Form["vis_txttoken"].ToString()), "vis_txttoken");
            form.Add(new StringContent(httpRequest.Form["vis_module"].ToString()), "vis_module");
            form.Add(new StringContent(httpRequest.Form["vis_operation"].ToString()), "vis_operation");
            form.Add(new StringContent(httpRequest.Form["vis_domain"].ToString()), "vis_domain");
            form.Add(new StringContent(httpRequest.Form["vis_department"].ToString()), "vis_department");
            form.Add(new StringContent(httpRequest.Form["vis_status"].ToString()), "vis_status");
            form.Add(new StringContent(httpRequest.Form["vis_priority"].ToString()), "vis_priority");
            form.Add(new StringContent(httpRequest.Form["vis_subject"].ToString()), "vis_subject");
            form.Add(new StringContent(httpRequest.Form["vis_type"].ToString()), "vis_type");
            form.Add(new StringContent(httpRequest.Form["vis_email"].ToString()), "vis_email");
            form.Add(new StringContent(httpRequest.Form["vis_ticket_post"].ToString()), "vis_ticket_post");
            form.Add(new StringContent(httpRequest.Form["vis_from"].ToString()), "vis_from");
            form.Add(new StringContent(httpRequest.Form["vis_send_message"].ToString()), "vis_send_message");
            form.Add(new StringContent(httpRequest.Form["vis_private"].ToString()), "vis_private");
            form.Add(new StringContent(httpRequest.Form["vis_sendmail"].ToString()), "vis_sendmail");
            form.Add(new StringContent(httpRequest.Form["vis_as_client"].ToString()), "vis_as_client");
            form.Add(new StringContent(httpRequest.Form["vis_owner_list"].ToString()), "vis_owner_list");
            form.Add(new StringContent(httpRequest.Form["vis_channel"].ToString()), "vis_channel");


            for (int i = 0; i < httpRequest.Files.Count; i++)
            {
                MemoryStream target = new MemoryStream();
                httpRequest.Files[i].InputStream.CopyTo(target);
                byte[] data = target.ToArray();
                form.Add(new ByteArrayContent(data), "vis_attachment" + i, httpRequest.Files[i].FileName);
            }

            HttpResponseMessage response = await httpClient.PostAsync("http://support.teletext.co.uk/api/index.php", form);

            response.EnsureSuccessStatusCode();
            httpClient.Dispose();
            string sd = response.Content.ReadAsStringAsync().Result;
            return sd;

        }


        public string SubmitData()
        {
            var httpContext = HttpContext.Current;
            var httpRequest = httpContext.Request;
            var fileName = httpRequest.Form["FileName"].ToString();
            var filePath = httpRequest.Form["FilePath"].ToString();
            var fileConent = httpRequest.Form["FileContent"].ToString();
            var bucketName = httpRequest.Form["BucketName"].ToString();

            var isFileUpdated = Utilities.PutObjectToS3(bucketName, fileName, fileConent, true, filePath);
            return isFileUpdated;
        }

        #region refreshalternatecofig

        public Stream RefreshAlternateConfigFile()
        {
            try
            {
                string ReloadStatus = Utilities.LoadAlternateConfigFromS3(ConfigurationManager.AppSettings["AlternateConfigBucket"], ConfigurationManager.AppSettings["AlternateConfigFileName"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(ReloadStatus));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("RefreshAlternateConfigFile returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }

        }
        #endregion refreshalternateconfig

        #region updatedictionary
        public Stream UpdateDictionary(string DictionaryName)
        {
            try
            {
                string ReloadStatus = Utilities.UpdateDictionary(DictionaryName);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(ReloadStatus));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("UpdateDictionary returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }

        }

        #endregion updatedictionary

        #region RetrieveAll
        public Stream RetrieveAll(string action)
        {
            List<OverrideTable> OverrideRankDataModels = new List<OverrideTable>();
            Overrideerror er = new Overrideerror();
            if (action.ToLower().IsNullOrEmpty())
            {
                ErrorLogger.Log("Query parameter is empty. Please enter appropriate query parameter.", LogLevel.Error);
                throw new WebFaultException<string>("Query parameter is empty. Please enter appropriate query parameter.", HttpStatusCode.BadRequest);
            }
            if (action.ToLower() != "retrieveall")
            {
                ErrorLogger.Log("Query Parameter `action` is invalid. Please provide the action as 'retrieveall'.", LogLevel.Error);
                throw new WebFaultException<string>("Query Parameter `action` is invalid. Please provide the action as 'retrieveall'.", HttpStatusCode.BadRequest);
            }
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();

                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["OverrideRankStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@Action", SqlDbType.VarChar, 50)).Value = action.ToLower();
                    SqlParameter errorval = new SqlParameter("@ErrorValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    sqlSelectCommand.Parameters.Add(errorval);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);

                    DataSet resultDataSet = new DataSet();

                    sqlDataAdapter.Fill(resultDataSet);

                    if (resultDataSet.Tables.Count > 0)
                    {
                        if (resultDataSet.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                            {
                                OverrideTable OverrideRankDataModel = new OverrideTable();
                                OverrideRankDataModel.mhid = Convert.ToInt32(dataRow["MHID"]);
                                OverrideRankDataModel.hotelName = dataRow["Hotelname"].ToString();
                                OverrideRankDataModel.destination = dataRow["Destination"].ToString();
                                OverrideRankDataModel.destinationID = dataRow["DestinationID"].ToString();
                                OverrideRankDataModel.rank = Convert.ToInt32(dataRow["Rank"]);
                                OverrideRankDataModel.toDisplayOnFod = Convert.ToBoolean(dataRow["IsActive"]);
                                OverrideRankDataModel.comment = dataRow["Comment"].ToString();
                                OverrideRankDataModels.Add(OverrideRankDataModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("RetrieveAll api returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(OverrideRankDataModels)));
        }
        #endregion RetrieveAll

        #region Retrieve
        public Stream Retrieve(string action, string mhid)
        {
            OverrideTable OverrideRankDataModel = new OverrideTable();
            Overrideerror er = new WCFRestService.Model.Misc.Overrideerror();
            if (action.IsNullOrEmpty() || mhid.IsNullOrEmpty())
            {
                ErrorLogger.Log("Query parameter is empty. Please enter appropriate query parameter.", LogLevel.Error);
                throw new WebFaultException<string>("Query parameter is empty. Please enter appropriate query parameter.", HttpStatusCode.BadRequest);
            }
            if (action.ToLower() != "retrieve")
            {
                ErrorLogger.Log("Query Parameter `action` is invalid. Please provide the action as 'retrieve'.", LogLevel.Error);
                throw new WebFaultException<string>("Query Parameter `action` is invalid. Please provide the action as 'retrieve'.", HttpStatusCode.BadRequest);
            }
            string errval;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();

                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["OverrideRankStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@Action", SqlDbType.VarChar, 50)).Value = action.ToLower();
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@MHID", SqlDbType.Int)).Value = Convert.ToInt32(mhid);
                    SqlParameter errorval = new SqlParameter("@ErrorValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    sqlSelectCommand.Parameters.Add(errorval);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);
                    DataSet resultDataSet = new DataSet();
                    sqlDataAdapter.Fill(resultDataSet);
                    errval = errorval.Value.ToString();
                    if (errorval.Value.ToString() == "0")
                    {
                        DataRow dataRow = resultDataSet.Tables[0].Rows[0];
                        OverrideRankDataModel.mhid = Convert.ToInt32(dataRow["MHID"]);
                        OverrideRankDataModel.hotelName = dataRow["Hotelname"].ToString();
                        OverrideRankDataModel.destination = dataRow["Destination"].ToString();
                        OverrideRankDataModel.destinationID = dataRow["DestinationID"].ToString();
                        OverrideRankDataModel.rank = Convert.ToInt32(dataRow["Rank"]);
                        OverrideRankDataModel.toDisplayOnFod = Convert.ToBoolean(dataRow["IsActive"]);
                        OverrideRankDataModel.comment = dataRow["Comment"].ToString();
                    }

               }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("Retrieve Api returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
            if (errval == "1")
            {
                ErrorLogger.Log("MasterHotelID doesn't exist in the Master Ranking Table.", LogLevel.Error);
                throw new WebFaultException<string>("MasterHotelID doesn't exist in the Master Ranking Table.", HttpStatusCode.BadRequest);
            }
            else if (errval == "2")
            {
                ErrorLogger.Log("Rank for the MasterHotelID has already been overridden. Please delete the existing record and then retrieve again.", LogLevel.Error);
                throw new WebFaultException<string>("Rank for the MasterHotelID has already been overridden. Please delete the existing record and then retrieve again.", HttpStatusCode.BadRequest);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(OverrideRankDataModel)));
        }
        #endregion Retrieve

        #region Delete
        public Stream Delete(string action, string mhid)
        {
            OverrideTable OverrideRankDataModel = new OverrideTable();
            Overrideerror er = new WCFRestService.Model.Misc.Overrideerror();
            if (action.IsNullOrEmpty() || mhid.IsNullOrEmpty())
            {
                er.errorCode = 6;
                er.errorMsg = "Query parameter is empty. Please enter appropriate query parameter.";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
            }
            if (action.ToLower() != "delete")
            {
                er.errorCode = 5;
                er.errorMsg = "Query Parameter `action` is invalid. Please provide the action as 'delete'.";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
            }

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();

                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["OverrideRankStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@Action", SqlDbType.VarChar, 50)).Value = action.ToLower();
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@MHID", SqlDbType.Int)).Value = Convert.ToInt32(mhid);
                    SqlParameter errorval = new SqlParameter("@ErrorValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    sqlSelectCommand.Parameters.Add(errorval);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);

                    DataSet resultDataSet = new DataSet();

                    sqlDataAdapter.Fill(resultDataSet);

                }
                er.errorCode = 0;
                er.errorMsg = "Record has been deleted Successfully.";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));

            }
            catch (Exception ex)
            {

                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("Delete operation failed. It throwed exception : ", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion delete

        #region OverrideRank
        public Stream OverrideRank(string action, string mhid, string newRank, string toDisplayOnFod, string comment)
        {
            Overrideerror er = new WCFRestService.Model.Misc.Overrideerror();
            if (action.IsNullOrEmpty() || mhid.IsNullOrEmpty() || newRank.IsNullOrEmpty())
            {
                er.errorCode = 6;
                er.errorMsg = "Query parameter is empty. Please enter appropriate query parameter.";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
            }
            if (action.ToLower() != "override")
            {
                er.errorCode = 5;
                er.errorMsg = "Query Parameter `action` is invalid. Please provide the action as 'override'.";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
            }
            if (Convert.ToInt32(newRank) < 1 || Convert.ToInt32(newRank) > 1000)
            {
                er.errorCode = 4;
                er.errorMsg = "Rank should be in the range of 1 to 1000";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
            }
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();
                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["OverrideRankStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@Action", SqlDbType.VarChar, 50)).Value = action.ToLower();
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@MHID", SqlDbType.Int)).Value = Convert.ToInt32(mhid);
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@NewRank", SqlDbType.Int)).Value = Convert.ToInt32(newRank);
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.Bit)).Value = (toDisplayOnFod.Equals("true")) ? 1 : 0;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@Comment", SqlDbType.VarChar,5000)).Value = comment.ToString();
                    SqlParameter errorval = new SqlParameter("@ErrorValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    sqlSelectCommand.Parameters.Add(errorval);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);

                    DataSet resultDataSet = new DataSet();

                    sqlDataAdapter.Fill(resultDataSet);
                    if (errorval.Value.ToString() == "0")
                    {
                        er.errorCode = 0;
                        er.errorMsg = "Hotel Rank has been successfully overridden.";
                    }
                    else if (errorval.Value.ToString() == "3")
                    {
                        er.errorCode = 3;
                        er.errorMsg = "The new rank already exists for some other mhid";
                    }
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(er)));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("OverrideRank returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion OverrideRank

        #region WebsiteSignUp IE
        public void WebsiteSignUpIE(string countrySite, SignUpDetails signUpDetails)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                WebsiteSignUp(signUpDetails);
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionStringForWebsiteSignUp"].ConnectionString))
                {
                    sqlConnection.Open();

                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["WebsiteSignUpStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pDeparture_airport", SqlDbType.VarChar, 50)).Value = signUpDetails.Departure_airport;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pDisplayName", SqlDbType.VarChar, 100)).Value = signUpDetails.DisplayName;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar, 50)).Value = signUpDetails.Email;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pKeep_me_deals", SqlDbType.VarChar, 15)).Value = signUpDetails.Keep_me_deals;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pPhone_Code", SqlDbType.VarChar, 20)).Value = signUpDetails.Phone_Code;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pProvider", SqlDbType.VarChar, 30)).Value = signUpDetails.Provider;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pSignUpDate", SqlDbType.Date)).Value = signUpDetails.SignUpDate;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pTelephone", SqlDbType.VarChar, 50)).Value = signUpDetails.Telephone;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pBrand", SqlDbType.VarChar, 25)).Value = countrySite.Equals(Global.COUNTRY_SITE_IE)?"Teletext Ireland": "Teletext";

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);
                    DataSet resultDataSet = new DataSet();
                    sqlDataAdapter.Fill(resultDataSet);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Failed in adding the website sign up details to stored procedure", LogLevel.Error);
                throw new WebFaultException<string>("WebsiteSignUp Api returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion WebsiteSignUp IE

        #region WebsiteSignUp
        public void WebsiteSignUp(SignUpDetails signUpDetails)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionStringForWebsiteSignUp"].ConnectionString))
                {
                    sqlConnection.Open();

                    SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["WebsiteSignUpStoredProcedure"], sqlConnection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pDeparture_airport", SqlDbType.VarChar, 50)).Value = signUpDetails.Departure_airport;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pDisplayName", SqlDbType.VarChar, 100)).Value = signUpDetails.DisplayName;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar, 50)).Value = signUpDetails.Email;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pKeep_me_deals", SqlDbType.VarChar, 15)).Value = signUpDetails.Keep_me_deals;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pPhone_Code", SqlDbType.VarChar, 20)).Value = signUpDetails.Phone_Code;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pProvider", SqlDbType.VarChar, 30)).Value = signUpDetails.Provider;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pSignUpDate", SqlDbType.Date)).Value = signUpDetails.SignUpDate;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pTelephone", SqlDbType.VarChar, 50)).Value = signUpDetails.Telephone;
                    sqlSelectCommand.Parameters.Add(new SqlParameter("@pBrand", SqlDbType.VarChar, 25)).Value = "Teletext UK";
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);
                    DataSet resultDataSet = new DataSet();
                    sqlDataAdapter.Fill(resultDataSet);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Failed in adding the website sign up details to stored procedure", LogLevel.Error);
                throw new WebFaultException<string>("WebsiteSignUp Api returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion WebsiteSignUp

        #region TTOnlineBookability
        public Stream ESAvailabilitySearch(AvailabilitySearchRequest availabilityRequest)
        {
            #region Mandatory Param Check
            /* Mandatory Parameter Check */
            if (string.IsNullOrWhiteSpace(availabilityRequest.destinationCode))
            {
                throw new WebFaultException<string>("Destination cannot be Null", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrWhiteSpace(availabilityRequest.startDate))
            {
                throw new WebFaultException<string>("Check-in date cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(availabilityRequest.startDate, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("Check-in date cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            if (string.IsNullOrWhiteSpace(availabilityRequest.endDate))
            {
                throw new WebFaultException<string>("Check-out date cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(availabilityRequest.endDate, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("Check-out date cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }

            /* End of Mandatory Param check */
            #endregion
            #region Default Values
            if (String.IsNullOrWhiteSpace(availabilityRequest.rating))
            {
                availabilityRequest.rating = "1,2,3,4,5";
            }
            if (availabilityRequest.boardType == null)
            {
                availabilityRequest.boardType = ConfigurationManager.AppSettings["TTOnlineDefaultBoardTypes"].Split(',').ToList();
            }
            availabilityRequest.destinationCode = availabilityRequest.destinationCode;
            availabilityRequest.searchRequestType = "fresh";
            if (availabilityRequest.roomList.Count > 1)
            {
                availabilityRequest.roomSearchType = "multiroom";
            }
            else
            {
                availabilityRequest.roomSearchType = "singleroom";
            }
            StringBuilder guestInfo = new StringBuilder();
            guestInfo.Append("");
            int roomNumber = 1;
            foreach (GuestsPerRoom guest in availabilityRequest.roomList)
            {
                if (guestInfo.Length > 1)
                {
                    guestInfo.Append("," + roomNumber.ToString() + "," + guest.adults.ToString() + "," + guest.children.ToString());
                }
                else
                {
                    guestInfo.Append(roomNumber.ToString() + "," + guest.adults.ToString() + "," + guest.children.ToString());
                }
                roomNumber++;
            }
            availabilityRequest.guestInfoSearchRequest = guestInfo.ToString();
            #endregion
            #region Building the API Search Request Object
            ////if (!String.IsNullOrWhiteSpace(availabilityRequest.destinationCode))
            ////{
            ////    List<int> destinationsList = new List<int>();
            ////    StringBuilder destinationStr = new StringBuilder();
            ////    destinationStr.Append("");

            ////    if(Global.destinationAlphaNumericIdToDestinationId.ContainsKey(availabilityRequest.destinationCode.ToUpper()))
            ////    {
            ////        destinationStr.Append(Global.destinationAlphaNumericIdToDestinationId[availabilityRequest.destinationCode.ToUpper()].ToString());
            ////        if (Global.parentChildDestinationHierarchy.ContainsKey(destinationStr.ToString()))
            ////        {
            ////            destinationStr.Append("," + Global.parentChildDestinationHierarchy[destinationStr.ToString()]);
            ////        }
            ////    }
            ////    availabilityRequest.destinationIds = destinationStr.ToString();                
            ////}

            if (availabilityRequest.boardType.Contains<string>("-1") || availabilityRequest.boardType.Count == 0 || availabilityRequest.boardType == null)
            {
                availabilityRequest.boardType = ConfigurationManager.AppSettings["TTOnlineDefaultBoardTypes"].Split(',').ToList();
            }
            #endregion
            ErrorLogger.Log("\n\nRequest Object: " + JsonConvert.SerializeObject(availabilityRequest), LogLevel.ElasticSearchAvailability);
            ErrorLogger.Log("Checking if the Availability Search Request is available in the request indice.", LogLevel.ElasticSearchAvailability);
            //B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = B2CExecuteQuery.CheckRequestInAvailabilityIndice(availabilityRequest);
            B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = B2CExecuteQuery.CheckRequestInAvailabilityIndiceV2(availabilityRequest);
            //ErrorLogger.Log("Number of offers available from prior search : " + b2bAvailabilitySearchRequestAvailability.availabilityOffersCount, LogLevel.ElasticSearchAvailability);

            AvailabilityOffers holidayOffers = new AvailabilityOffers();
            //holidayOffers = B2COffersBuilder.GenerateOffers(b2bAvailabilitySearchRequestAvailability, availabilityRequest);
            holidayOffers = B2COffersBuilder.GenerateOffersV2(b2bAvailabilitySearchRequestAvailability, availabilityRequest);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            // WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
        }

        public Stream InvokeRoomValuationService(string availabilityId, string roomIds, string promotionalCode, string valuationId)
        {
            var json = String.Empty;

            try
            {
                AccommodationService.AccommodationValuationServiceClient localValuationService = new AccommodationService.AccommodationValuationServiceClient();
                AccommodationService.AccommodationB2CValuationRequest localAccommodationB2CAvailabilityRequest = new AccommodationService.AccommodationB2CValuationRequest();
                localAccommodationB2CAvailabilityRequest = new AccommodationService.AccommodationB2CValuationRequest();
                localAccommodationB2CAvailabilityRequest.AvailabilityId = System.Guid.Parse(availabilityId);
                localAccommodationB2CAvailabilityRequest.Channel = AccommodationService.Channel.TeletextHolidaysUK;
                localAccommodationB2CAvailabilityRequest.Debugging = false;
                localAccommodationB2CAvailabilityRequest.PromotionalCode = promotionalCode;
                localAccommodationB2CAvailabilityRequest.SelectedRoomIds = (roomIds != null && roomIds.Split(',').Count() > 0) ? roomIds.Split(',').Select(s => uint.Parse(s)).ToArray() : new uint[0];
                localAccommodationB2CAvailabilityRequest.ValuationId = null;
                var accommodationB2CValuationResponse = localValuationService.StartAndGetAccommodationValuationProcess(localAccommodationB2CAvailabilityRequest);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(accommodationB2CValuationResponse)));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream BookingService(BookingRequest bookingRequest)
        {
            var json = String.Empty;

            string contents = string.Empty;
            try
            {
                AccommodationService.AccommodationBookingServiceClient localBookingService = new AccommodationService.AccommodationBookingServiceClient();
                AccommodationService.AccommodationB2CBookingRequest accommodationB2CBookingRequest = new AccommodationService.AccommodationB2CBookingRequest();
                AccommodationService.AccommodationB2CBookingResponse accommodationB2CValuationResponse = new AccommodationService.AccommodationB2CBookingResponse();

                if (ConfigurationManager.AppSettings["IsOnlineBookabilityLive"] == "true")
                {
                    accommodationB2CBookingRequest = B2CBookingRequest.BuildBookingObject(bookingRequest);
                    accommodationB2CValuationResponse = localBookingService.StartAndGetAccommodationBookingProcess(accommodationB2CBookingRequest);
                }
                else
                {
                    accommodationB2CValuationResponse.BookingStatus = AccommodationService.Status.Successful;
                }

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(accommodationB2CValuationResponse)));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetEssentialInfo(string hotelCode, string destinationCode)
        {
            EssentialInformation essentialInfo = new EssentialInformation();

            if (string.IsNullOrWhiteSpace(hotelCode) && string.IsNullOrWhiteSpace(destinationCode))
            {
                throw new WebFaultException<string>("Parameters cannot be null", HttpStatusCode.BadRequest);
            }

            if (!string.IsNullOrWhiteSpace(hotelCode) && !string.IsNullOrWhiteSpace(destinationCode))
            {
                // Get Both Nearest Attractions and Essential Destination Info
                List<NearestAttaraction> nearestAttractions = StaticInfoBuilder.GetNearestHotelAttractions(hotelCode);
                AirportInformation aiportInfo = StaticInfoBuilder.GetNearestAirportInformation(hotelCode);
                DestinationLevelEssentialInformation essentialInformation = StaticInfoBuilder.GetDestinationEssentialInformation(destinationCode);              

                essentialInfo.nearestAttractions = nearestAttractions;
                essentialInfo.destinationLevelEssentialInfo = essentialInformation;
                essentialInfo.nearestAirport = aiportInfo;
            }
            else if (!string.IsNullOrWhiteSpace(hotelCode))
            {
                // Get Nearest Attractions to hotel
                List<NearestAttaraction> nearestAttractions = StaticInfoBuilder.GetNearestHotelAttractions(hotelCode);
                AirportInformation aiportInfo = StaticInfoBuilder.GetNearestAirportInformation(hotelCode);

                essentialInfo.nearestAttractions = nearestAttractions;
                essentialInfo.nearestAirport = aiportInfo;
            }
            else if (!string.IsNullOrWhiteSpace(destinationCode))
            {
                // Get Essential Destination Info
                DestinationLevelEssentialInformation essentialInformation = StaticInfoBuilder.GetDestinationEssentialInformation(destinationCode);
                essentialInfo.destinationLevelEssentialInfo = essentialInformation;
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(essentialInfo)));
        }
        public Stream TestingB2CAccommodationService(AvailabilitySearchRequest availabilityRequest)
        {
            ErrorLogger.Log("Actual request : " + JsonConvert.SerializeObject(availabilityRequest), LogLevel.ElasticSearchAvailabilityTesting);
            string availabilityRequestStr = B2CQueryBuilder.GetB2CAvailabilitySearchQuery(availabilityRequest);

            availabilityRequest.searchRequestType = "fresh";
            // MAke a call to B2C Accommodation SOA
            B2CAvailabilityResponse b2cAvailabilityResponse = new B2CAvailabilityResponse();
            try
            {
                AccommodationService.AccommodationAvailabilityServiceClient accomdationService = new AccommodationService.AccommodationAvailabilityServiceClient();
                AccommodationService.AccommodationB2CAvailabilityRequest accommodationB2CAvailabilityRequest = new AccommodationService.AccommodationB2CAvailabilityRequest();
                accommodationB2CAvailabilityRequest = JsonConvert.DeserializeObject<AccommodationService.AccommodationB2CAvailabilityRequest>(availabilityRequestStr);

                ErrorLogger.Log("Request sent to B2C AccommodationSOA Service : " + JsonConvert.SerializeObject(accommodationB2CAvailabilityRequest), LogLevel.ElasticSearchAvailabilityTesting);

                if (availabilityRequest.searchRequestType == "fresh")
                {
                    var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = 0, PageNumber = 1 }, new AccommodationService.AccommodationB2CAvailabilitySort(), false);

                    b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                }
                else if (availabilityRequest.searchRequestType == "continued")
                {
                    var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { PageNumber = 2 },
                                                                           new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                    b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Exception from B2C Endpoint Testing. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(b2cAvailabilityResponse)));
        }

        public Stream GetDestinationsAndHotels(string SearchTerm)
        {
            var json = String.Empty;
            try
            {
                AccommodationService.OnlineBookabilityServiceClient onlineBookabilityServiceClient = new AccommodationService.OnlineBookabilityServiceClient();
                var result = onlineBookabilityServiceClient.GetAutoCompleteHotelAndDestinationNames(SearchTerm, AccommodationService.Channel.TeletextHolidaysUK);
                var suggestions = new Dictionary<AutoSuggestCategory, List<AutoSuggestViewModel>>();
                if (result.DestinationAutoSuggest != null && result.DestinationAutoSuggest.Count() > 0)
                {
                    suggestions.Add(AutoSuggestCategory.Destinations, result.DestinationAutoSuggest.Select(m => new AutoSuggestViewModel
                    {
                        category = m.Category,
                        name = m.Name,
                        destinationId = m.DestinationId,
                        locationId = m.LocationId
                    }).ToList());
                }
                if (result.HotelAutoSuggest != null && result.HotelAutoSuggest.Count() > 0)
                {
                    suggestions.Add(AutoSuggestCategory.Hotels, result.HotelAutoSuggest.Select(m => new AutoSuggestViewModel
                    {
                        category = m.Category,
                        name = m.Name,
                        destinationId = m.DestinationId,
                        locationId = m.LocationId
                    }).ToList());
                }
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(suggestions)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream SaveReviewNPayInformation(ReviewNPayParams reviewNPayParam)
        {
            Status saveStatus = new Status();
            try
            {
                #region Mandatory Param Check
                if (string.IsNullOrEmpty(reviewNPayParam.id) && string.IsNullOrEmpty(reviewNPayParam.value))
                {
                    throw new WebFaultException<string>("Id & Value cannot be Null", HttpStatusCode.BadRequest);
                }
                #endregion

                // insert the object into ES
                try
                {
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "ReviewNPayInfo");
                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(reviewNPayParam);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    saveStatus.status = "Error occured while posting data.";
                    ErrorLogger.Log(string.Format("Exception occured while inserting ReviewNPay Object call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearchAvailability);
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(saveStatus)));
                }

                saveStatus.status = "Value has been successfully saved.";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearchAvailability);
                throw new WebFaultException<string>("Save Review and Pay Information returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(saveStatus)));
        }

        public Stream GetReviewNPayInformation(string id)
        {
            ReviewNPayParams reviewNPayParams = new ReviewNPayParams();
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new WebFaultException<string>("Id cannot be Null", HttpStatusCode.BadRequest);
                }

                try
                {
                    string query = B2CQueryBuilder.GetReviewNPayESQuery(id);
                    string reviewNPayInfoResponseFromES = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/_search", query);
                    ReviewNPayInfoESDoc response = JsonConvert.DeserializeObject<ReviewNPayInfoESDoc>(reviewNPayInfoResponseFromES);
                    if (response != null && response.hits.hits.Count > 0)
                    {
                        List<ReviewNPayDocHit> hs = new List<ReviewNPayDocHit>(response.hits.hits);
                        reviewNPayParams = response.hits.hits[0]._source;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting ReviewNPay Object call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearchAvailability);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearchAvailability);
                throw new WebFaultException<string>("GetReviewAndPay Information returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviewNPayParams)));
        }
        #endregion

        #region HotelOnlyOffers MPT
        public Stream GetHotelOnlyMinPriceHomePage()
        {
            string outputURL = string.Empty;
            outputURL = ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"];

            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            List<TTOMinPriceTool> minPriceToolOutput;
            minPriceToolOutput = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(outputFileResponse);
            int countOut = minPriceToolOutput.Count;
            try
            {
                if (countOut != 0)
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetHotelOnlyMinPriceHomePage Fetching:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }

        public Stream GetB2CMinPrice(string adults, string children, string checkInDate, string checkOutDate, string duration, string destinationCode, string destinationName, string priceMin,
                                           string priceMax, string establishmentCode, string boardType, string dateOffSet, string flexDates, string ratings, string tripAdvisorRating)
        {
            try
            {
                AvailabilitySearchRequest availabilityRequest = new AvailabilitySearchRequest();
                availabilityRequest.destinationCode = destinationCode;
                availabilityRequest.hotelCode = establishmentCode;
                availabilityRequest.startDate = checkInDate;
                availabilityRequest.endDate = checkOutDate;
                availabilityRequest.rating = ratings;

                List<GuestsPerRoom> guestsInfo = new List<GuestsPerRoom>();
                GuestsPerRoom guests = new GuestsPerRoom();
                guests.adults = int.Parse(adults);
                guests.children = int.Parse(children);
                guests.ages = new List<byte>();
                guestsInfo.Add(guests);

                availabilityRequest.roomList = guestsInfo;
                if (!string.IsNullOrWhiteSpace(tripAdvisorRating))
                {
                    availabilityRequest.tripAdvisorRating = float.Parse(tripAdvisorRating);
                }
                availabilityRequest.priceMin = int.Parse(priceMin);
                availabilityRequest.priceMax = int.Parse(priceMax);
                availabilityRequest.boardType = boardType.Split(',').ToList();
                availabilityRequest.sort = "totalPrice";

                string postJsonQueryData = JsonConvert.SerializeObject(availabilityRequest);
                // Make a call to esAvailability API and get the response
                AvailabilityOffers availabilityOffers = JsonConvert.DeserializeObject<AvailabilityOffers>(Utilities.ExecutePostJsonWebRequest(ConfigurationManager.AppSettings["ESAvailabilitySearchUrl"], postJsonQueryData));

                TTOMinPriceView minPriceView = new TTOMinPriceView();
                minPriceView = TTOnlineMinPriceTool.TTOnlineMinPriceTool.GetMinPriceViewResponse(availabilityOffers);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream DeleteQueryObject(string queryId, string userId)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            awsBucketName = ConfigurationManager.AppSettings["HotelOnlyMinPriceToolFileBucket"];
            awsInputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTInputFileName"];
            awsOutputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTOutputFileName"];


            List<TTOMinPriceTool> minPriceToolInputObject = new List<TTOMinPriceTool>();
            List<TTOMinPriceTool> minPriceToolOutputObject = new List<TTOMinPriceTool>();

            // Delete the query from the Input File
            minPriceToolInputObject = TTOnlineMinPriceTool.TTOnlineMinPriceTool.DeleteQuery(queryId, ConfigurationManager.AppSettings["HotelOnlyMinPriceInput"], awsBucketName, awsInputFolderKeyName);

            // Delete the query from the Output File
            minPriceToolOutputObject = TTOnlineMinPriceTool.TTOnlineMinPriceTool.DeleteQuery(queryId, ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"], awsBucketName, awsOutputFolderKeyName);

            ErrorLogger.Log(string.Format("Hotel Only Min Price Tool -- QueryId - {0} successfully deleted By the User - {1}", queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }

        public Stream SaveOrUpdateHotelOnlyMinPriceOffers(HotelOnlyMinPriceOffer saveUpdateMinPrice, string userId)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            awsBucketName = ConfigurationManager.AppSettings["HotelOnlyMinPriceToolFileBucket"];
            awsInputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTInputFileName"];
            awsOutputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTOutputFileName"];

            IAmazonS3 client;

            // Read the Etag for the MinPriceOutput for reduce concurrency issues
            string oldMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<TTOMinPriceTool> minPriceToolInputObject = new List<TTOMinPriceTool>();
                    minPriceToolInputObject = TTOnlineMinPriceTool.TTOnlineMinPriceTool.InsertHotelOnlyMPTOfferToS3(saveUpdateMinPrice, userId);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);

                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Hotel Only Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<TTOMinPriceTool> minPriceToolInputObject = new List<TTOMinPriceTool>();
                    minPriceToolInputObject = TTOnlineMinPriceTool.TTOnlineMinPriceTool.UpdateHotelOnlyMPTOfferToS3(saveUpdateMinPrice, userId);

                    // Check for change in Etag
                    string newMPTOutputEtag = Utilities.GetEtag(awsBucketName, awsOutputFolderKeyName);
                    if (!(oldMPTOutputEtag.Equals(newMPTOutputEtag)))
                    {
                        throw new WebFaultException<string>("Offers are being currently updated. Please try after 10-20 minutes. Inconvenience is deeply regreted.", System.Net.HttpStatusCode.Forbidden);
                    }

                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Hotel Only Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (WebFaultException<string> webExp)
            {
                if (webExp.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    ErrorLogger.Log("Lambda is updating prices so denied saving/updating from MPT", LogLevel.Information);
                    throw webExp;
                }
                else
                {
                    ErrorLogger.Log(webExp.ToString(), LogLevel.Information);
                    throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null. Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
                }

            }
        }

        public Stream UpdateHotelOnlyMinPriceOfferFields(editUpdate editUpdate, string userId)
        {

            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;
            string outputUrl = string.Empty;

            awsBucketName = ConfigurationManager.AppSettings["HotelOnlyMinPriceToolFileBucket"];
            awsInputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTInputFileName"];
            awsOutputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTOutputFileName"];
            outputUrl = ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"];

            List<TTOMinPriceTool> minPriceToolOutputObject = new List<TTOMinPriceTool>();
            string jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
            minPriceToolOutputObject = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(jsonObjectOutput);
            int countOut = minPriceToolOutputObject.Count;
            int flag = 0;
            for (int test = 0; test < countOut; test++)
            {
                if (minPriceToolOutputObject[test].sectionId.ToLower() == editUpdate.sectionId.ToLower() && minPriceToolOutputObject[test].query.id.ToLower() == editUpdate.ID.ToLower())
                {
                    if (minPriceToolOutputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolOutputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolOutputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolOutputObject[test].query.id = editUpdate.ID;
                        minPriceToolOutputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                        break;
                    }
                    else
                    {
                        throw new WebFaultException<string>("The combination of Section Id and ID already exists ", System.Net.HttpStatusCode.BadRequest);
                    }
                }
            }
            if (flag == 0)
            {
                for (int test = 0; test < countOut; test++)
                {
                    if (minPriceToolOutputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolOutputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolOutputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolOutputObject[test].query.id = editUpdate.ID;
                        minPriceToolOutputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                    }
                }
            }
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Hotel Only Min Price Tool -- QueryId - {0} successfully updated HotelOnlyMinPriceUpdateOfferFields By the User - {1}", editUpdate.queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));

        }

        public Stream GetMerchandizingOffers(string sectionIds)
        {
            List<TTOMinPriceTool> minPriceToolSectionObject = new List<TTOMinPriceTool>();
            minPriceToolSectionObject = TTOnlineMinPriceTool.TTOnlineMinPriceTool.MerchandizingOffers(sectionIds);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            string awsOutputFolderKeyName = string.Empty;

            awsBucketName = ConfigurationManager.AppSettings["HotelOnlyMinPriceToolFileBucket"];
            awsInputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTInputFileName"];
            awsOutputFolderKeyName = ConfigurationManager.AppSettings["HotelOnlyMPTOutputFileName"];

            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                if (cacheTime < 1)
                {
                    cacheTime = 900;
                }
            }

            List<TTOMinPriceTool> result = null;
            result = minPriceToolSectionObject.OrderBy(o => o.query.id).ToList();

            //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }
        #endregion

        #region Tool APIs
        public Stream LoadS3WithHotelInformation(string mhids)
        {
            try
            {
                string responseStr = string.Empty;
                var mhidsNotAvailableOnHCEndpoint = LoadS3WithHotelInformationFmHC.LoadS3WithHotelInformation(mhids);
                if (!string.IsNullOrWhiteSpace(mhidsNotAvailableOnHCEndpoint))
                {
                    responseStr = "List of MHIDs which doesn't have content on HC Exposed Endpoint : " + mhidsNotAvailableOnHCEndpoint + ". Rest for all MHIDs data is available on LIVE System.";
                }
                else
                {
                    responseStr = "Content available on LIVE System. Please check again.";
                }
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(responseStr)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("LoadS3WithHotelInformation returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream ProcessStandaloneCredit(StandaloneCredit standaloneCredit, string userId)
        {
            try
            {
                // check if all fields are available
                #region Mandatory fields check
                if (string.IsNullOrWhiteSpace(standaloneCredit.merchantRefNum))
                {
                    throw new WebFaultException<string>("Merchant Ref Number cannot be Null", HttpStatusCode.BadRequest);
                }
                if (standaloneCredit.amount < 0)
                {
                    throw new WebFaultException<string>("Amount cannot be Negative", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.card.cardNum))
                {
                    throw new WebFaultException<string>("Card Number cannot be Null", HttpStatusCode.BadRequest);
                }
                if (standaloneCredit.card.cardExpiry.month < 0)
                {
                    throw new WebFaultException<string>("Card Expiry Month cannot be Negative", HttpStatusCode.BadRequest);
                }
                if (standaloneCredit.card.cardExpiry.year < 0)
                {
                    throw new WebFaultException<string>("Card Expiry Year cannot be Negative", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.profile.firstName) || string.IsNullOrWhiteSpace(standaloneCredit.profile.lastName))
                {
                    throw new WebFaultException<string>("First Name and/or Last Name cannot be Null", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.profile.email))
                {
                    throw new WebFaultException<string>("Email Address cannot be Null", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.billingDetails.street))
                {
                    throw new WebFaultException<string>("Street Address cannot be Null", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.billingDetails.city))
                {
                    throw new WebFaultException<string>("Card Number cannot be Null", HttpStatusCode.BadRequest);
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.billingDetails.zip))
                {
                    throw new WebFaultException<string>("Zip or Postal Code cannot be Null", HttpStatusCode.BadRequest);
                }
                #endregion

                #region Default Values
                if (string.IsNullOrWhiteSpace(standaloneCredit.customerIp))
                {
                    standaloneCredit.customerIp = ConfigurationManager.AppSettings["PaySafeCustomerIP"];
                }
                if (string.IsNullOrWhiteSpace(standaloneCredit.billingDetails.country))
                {
                    standaloneCredit.billingDetails.country = ConfigurationManager.AppSettings["PaySafeStandaloneCreditCountry"];
                }

                #endregion
                StandaloneCreditResponse responseStr = new StandaloneCreditResponse();

                // Make Post API Call
                string url = ConfigurationManager.AppSettings["PaySafeStandaloneCreditAPIURL"].Replace("{{account_no}}", ConfigurationManager.AppSettings["PaySafeStandaloneCreditAccountNumber"]);
                string apiKey = ConfigurationManager.AppSettings["PaySafeStandaloneCreditAPIKey"];
                responseStr = Utilities.ExecutePostWebRequestWithBasicAuthorization(url, JsonConvert.SerializeObject(standaloneCredit), apiKey);
                ErrorLogger.Log(JsonConvert.SerializeObject(responseStr), LogLevel.PaySafeStandaloneCredit);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(responseStr.postCallStatusInfo)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.PaySafeStandaloneCredit);
                throw new WebFaultException<string>("ProcessStandaloneCredit returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public void LogCheapestBoard(string data)
        {
            try
            {
                var logData = data.Split('@');
                foreach (string line in logData)
                {
                    ErrorLogger.Log(line, LogLevel.LogCheapBoardBasis);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.PaySafeStandaloneCredit);
                throw new WebFaultException<string>("LogCheapestBoard returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }        

        //public void WebsiteUserVisitsSerpReport(SearchObject signUpDetails)
        //{
        //    try
        //    {
        //        using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionStringForWebsiteSignUp"].ConnectionString))
        //        {
        //            sqlConnection.Open();

        //            SqlCommand sqlSelectCommand = new SqlCommand(ConfigurationManager.AppSettings["WebsiteSignUpStoredProcedure"], sqlConnection)
        //            {
        //                CommandType = System.Data.CommandType.StoredProcedure
        //            };
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pDeparture_airport", SqlDbType.VarChar, 50)).Value = signUpDetails.Departure_airport;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pDisplayName", SqlDbType.VarChar, 100)).Value = signUpDetails.DisplayName;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar, 50)).Value = signUpDetails.Email;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pKeep_me_deals", SqlDbType.VarChar, 15)).Value = signUpDetails.Keep_me_deals;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pPhone_Code", SqlDbType.VarChar, 20)).Value = signUpDetails.DisplayName;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pProvider", SqlDbType.VarChar, 30)).Value = signUpDetails.Provider;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pSignUpDate", SqlDbType.Date)).Value = signUpDetails.SignUpDate;
        //            sqlSelectCommand.Parameters.Add(new SqlParameter("@pTelephone", SqlDbType.VarChar, 50)).Value = signUpDetails.Telephone;
        //            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlSelectCommand);
        //            DataSet resultDataSet = new DataSet();
        //            sqlDataAdapter.Fill(resultDataSet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogger.Log("Failed in adding the website sign up details to stored procedure", LogLevel.Error);
        //        throw new WebFaultException<string>("WebsiteSignUp Api returned an error response", System.Net.HttpStatusCode.InternalServerError);
        //    }
        //}
        #endregion

        #region Diresctional Selling Chat Feature
        public Stream PostChatMessages(ChatMessage chatMessage)
        {
            #region Mandatory Params Check
            if(chatMessage.sessionId == null)
            {
                throw new WebFaultException<string>("Session Id cannot be Null", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrWhiteSpace(chatMessage.message))
            {
                throw new WebFaultException<string>("Message cannot be Empty", HttpStatusCode.BadRequest);
            }
            else if (chatMessage.message.Contains("#") && chatMessage.message.Contains("/hotel="))
            {
                chatMessage.message = ConfigurationManager.AppSettings["WebsiteSearch-Domain-URL"] + ConfigurationManager.AppSettings["DirectionalFodURL"] + chatMessage.message.Substring((chatMessage.message.IndexOf("#") + 1));
            }
            #endregion
            Overrideerror response = new Overrideerror();
            if (DirectionalSelling.InsertMessagesInES(chatMessage))
            {
                response.errorCode = 0;
                response.errorMsg = "Message successfully posted.";
            }
            else
            {
                response.errorCode = 1;
                response.errorMsg = "Message failed to update. Please try again.";
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }
            #endregion
        }
}