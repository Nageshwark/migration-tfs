﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.Misc;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using Presentation.WCFRestService.Model.Enum;
using System.ServiceModel.Web;
using Presentation.WCFRestService.Model.StaticHotelLanding;
using System.Text.RegularExpressions;
using Presentation.WCFRestService.Model;
using ServiceStack.Redis;
using Presentation.WCFRestService.Model.ElasticSearchV2;

namespace Presentation.Web.WCFRestServices.StaticHotelLanding
{
    public class StaticHotelLandingController
    {
        public static object GetHotelInfoByUrl(string url)
        {
            try
            {
                // Make the Redis call here
                string redisStaticMappingInfo = null;
                urlToDestination urlToDestinationObj = new urlToDestination();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelLandingMapping"]);
                        redisStaticMappingInfo = redisClient.GetValue(url);
                        if (redisStaticMappingInfo != null)
                        {
                            urlToDestinationObj = JsonConvert.DeserializeObject<urlToDestination>(redisStaticMappingInfo);
                        }
                    }
                }

                string Mhid = urlToDestinationObj.MHID;
                string cacheTime = string.Empty;
                MHIDStaticInfo mhidStaticInfo = null;
                StaticMasterHotelInfo staticMHInfo = null;
                int SHORT_DESCRIPTION_MAX_LENGTH = 200;
                List<OverseasDestination> overseasDestinationList = Global.overseasDestinationList;
                Dictionary<string, string> destinationIdToPeakSeason = Global.destinationIdToPeakSeason;
                //List<urlToDestination> urlToDestinationMappingList = new List<urlToDestination>();
                string redisStaticInfo = null;
                StaticResponseParms staticResponseParms = new StaticResponseParms();

                //var json = String.Empty;
                //using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["HotelLandingPageURLToDestinationMappingFilePath"])))
                //{
                //    json = reader.ReadToEnd();
                //}
                //urlToDestinationMappingList = JsonConvert.DeserializeObject<List<urlToDestination>>(json);

                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValue(Mhid);
                        if(redisStaticInfo != null)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo);
                        }
                    }
                }
                // Use MHID to get the static hotel info
                if (!string.IsNullOrWhiteSpace(Mhid))
                {
                    staticMHInfo = new StaticMasterHotelInfo();
                    mhidStaticInfo = Utilities.GetMasterHotelInfo(Mhid.ToString(), out cacheTime);
                    if(mhidStaticInfo._source != null)
                    {
                        MasterHotelSource _source = new MasterHotelSource();
                        _source.BuildingName = mhidStaticInfo._source.BuildingName;
                        _source.Description = mhidStaticInfo._source.Description;
                        _source.DestinationCode = mhidStaticInfo._source.DestinationCode;
                        _source.DestinationName = mhidStaticInfo._source.DestinationName;
                        _source.Features = mhidStaticInfo._source.Features;
                        _source.GIATAID = mhidStaticInfo._source.GIATAID;
                        _source.Location = mhidStaticInfo._source.Location;
                        _source.MasterHotelId = mhidStaticInfo._source.MasterHotelId;
                        _source.MasterResortId = mhidStaticInfo._source.MasterResortId;
                        _source.MasterResortName = mhidStaticInfo._source.MasterResortName;
                        _source.Rating = mhidStaticInfo._source.Rating;
                        _source.RatingLevel = mhidStaticInfo._source.RatingLevel;
                        _source.RegionTTSSLabelD = mhidStaticInfo._source.RegionTTSSLabelD;

                        if(mhidStaticInfo._source.Region != null)
                        {
                            MHIDRegion region = new MHIDRegion();
                            region.AirportCodes = mhidStaticInfo._source.Region.AirportCodes;
                            region.GrandRegionId = mhidStaticInfo._source.Region.GrandRegionId;
                            region.GrandRegionLink = mhidStaticInfo._source.Region.GrandRegionLink;
                            region.GrandRegionName = mhidStaticInfo._source.Region.GrandRegionName.Trim();
                            region.ParentRegionId = mhidStaticInfo._source.Region.ParentRegionId;
                            region.ParentRegionLink = mhidStaticInfo._source.Region.ParentRegionLink;
                            region.ParentRegionName = mhidStaticInfo._source.Region.ParentRegionName.Trim();
                            region.RegionId = mhidStaticInfo._source.Region.RegionId;
                            region.Reglevel = mhidStaticInfo._source.Region.Reglevel;
                            region.Regname = mhidStaticInfo._source.Region.Regname.Trim();
                            region.ResortId = mhidStaticInfo._source.Region.ResortId;
                            _source.Region = region;
                        }

                        if(!string.IsNullOrWhiteSpace(mhidStaticInfo._source.Description))
                        {
                            string tempDesc = Regex.Replace(mhidStaticInfo._source.Description, "<h3>.*?</h3>", String.Empty);
                            tempDesc = Regex.Replace(tempDesc, "<.*?>", String.Empty).Trim();
                            if(tempDesc.Length > SHORT_DESCRIPTION_MAX_LENGTH)
                            {
                                _source.shortDescription = tempDesc.Substring(0, SHORT_DESCRIPTION_MAX_LENGTH);
                            }
                            else
                            {
                                _source.shortDescription = tempDesc.Substring(0, tempDesc.Length);
                            }
                        }
                        else
                        {
                            _source.shortDescription = string.Empty;
                        }

                        if (mhidStaticInfo._source.Address != null)
                        {
                            MHIDAddress address = new MHIDAddress();
                            address.AddressLineOne = mhidStaticInfo._source.Address.AddressLineOne;
                            address.AddressLineTwo = mhidStaticInfo._source.Address.AddressLineTwo;
                            address.AddressLineThree = mhidStaticInfo._source.Address.AddressLineThree;
                            address.AddressLineFour = mhidStaticInfo._source.Address.AddressLineFour;
                            address.City = mhidStaticInfo._source.Address.City;
                            address.PostCode = mhidStaticInfo._source.Address.PostCode;

                            _source.Address = address;
                        }                       

                        staticMHInfo._source = _source;
                    }

                    TripAdvisor taInfo = new TripAdvisor();
                    taInfo.hotelStyle = mhidStaticInfo.TripAdvisor.hotelStyle;
                    taInfo.noOfRooms = mhidStaticInfo.TripAdvisor.noOfRooms;

                    // For TA Reviews Count, TA ID and Average TA Rating
                    if (staticResponseParms != null && !string.IsNullOrWhiteSpace(staticResponseParms.tripAdvisorId))
                    {
                        taInfo.reviewsCount = staticResponseParms.reviewCount;
                        taInfo.TAID = int.Parse(staticResponseParms.tripAdvisorId);
                        taInfo.TARating = staticResponseParms.averageRating;

                        staticMHInfo.TripAdvisor = taInfo;
                    }
                    else if(mhidStaticInfo.TripAdvisor != null)
                    {
                        taInfo.reviewsCount = mhidStaticInfo.TripAdvisor.reviewsCount;
                        taInfo.TAID = mhidStaticInfo.TripAdvisor.TAID;
                        taInfo.TARating = mhidStaticInfo.TripAdvisor.TARating;

                        staticMHInfo.TripAdvisor = taInfo;
                    }
                    // Supply the Search Query Parameters
                    SearchQueryParameters searchQueryParams = new SearchQueryParameters();
                    searchQueryParams.adults = ConfigurationManager.AppSettings["adults"];
                    searchQueryParams.boardType = ConfigurationManager.AppSettings["boardType"];
                    searchQueryParams.channelId = ConfigurationManager.AppSettings["channelId"];
                    searchQueryParams.children = ConfigurationManager.AppSettings["children"];
                    searchQueryParams.departureIds = ConfigurationManager.AppSettings["departureIds"];
                    searchQueryParams.duration = ConfigurationManager.AppSettings["duration"];
                    searchQueryParams.ratings = ConfigurationManager.AppSettings["ratings"];

                    //urlToDestination urlToDestinationObject = null;
                    //urlToDestinationObject = urlToDestinationMappingList.Find(r => r.url.Equals(url));
                    
                    if(urlToDestinationObj != null)
                    {
                        searchQueryParams.labelName = urlToDestinationObj.destLabel;
                        searchQueryParams.labelId = urlToDestinationObj.destinationLabelId;
                        searchQueryParams.channelId = urlToDestinationObj.channelId;
                    }
                    else
                    {
                        searchQueryParams.labelName = string.Empty;
                        searchQueryParams.labelId = string.Empty;
                        searchQueryParams.channelId = string.Empty;
                    }
                    //getMasterHotelInfoByUrlTimer.Start();
                    if (!string.IsNullOrWhiteSpace(searchQueryParams.labelName))
                    {
                        // Incorporate the logic for peak seasons
                        if (!string.IsNullOrWhiteSpace(searchQueryParams.labelId) && destinationIdToPeakSeason.ContainsKey(searchQueryParams.labelId))
                        {
                            int[] dateRollingNFlexibilityInfo = GetDateRollingForDestination(searchQueryParams.labelId, destinationIdToPeakSeason);
                            searchQueryParams.dateRolling = dateRollingNFlexibilityInfo[0];
                            searchQueryParams.flexibilityDuration = dateRollingNFlexibilityInfo[1];
                        }
                        else
                        {
                            searchQueryParams.flexibilityDuration = Int32.Parse(ConfigurationManager.AppSettings["flexibility"]);
                            searchQueryParams.dateRolling = Int32.Parse(ConfigurationManager.AppSettings["dateRolling"]);
                        }
                    }
                    else
                    {
                        searchQueryParams.flexibilityDuration = Int32.Parse(ConfigurationManager.AppSettings["flexibility"]);
                        searchQueryParams.dateRolling = Int32.Parse(ConfigurationManager.AppSettings["dateRolling"]);
                    }

                    staticMHInfo.searchParams = searchQueryParams;
                    //getMasterHotelInfoByUrlTimer.Stop();
                    // Meta Tags for the static hotel landing page
                    staticMHInfo.metaDescription = ConfigurationManager.AppSettings["StaticHotelLandingMetaDescription"];
                    staticMHInfo.title = ConfigurationManager.AppSettings["StaticHotelLandingMetaTitle"];

                    // Fetching the image urls for the Hotel
                    staticMHInfo.images = new List<string>();
                    staticMHInfo.mobileimages = new List<string>();
                    staticMHInfo.thumbnailimages = new List<string>();

                    if (Global.mhidtoImageCountList.ContainsKey(mhidStaticInfo._source.MasterHotelId.ToString()))
                    {
                        int hoteliffCount = Global.mhidtoImageCountList[mhidStaticInfo._source.MasterHotelId.ToString()];
                        string imgSourceURLFormat = Global.imgSourceURLFormat;
                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                        {
                            staticMHInfo.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                        }
                    }

                    if (mhidStaticInfo == null)
                    {
                        throw new WebFaultException<string>("GetMasterHotelInfo: MasterHotelId Does not exist:" + Mhid.ToString(), System.Net.HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("GetHotelInfoByUrl: No mapped MasterHotelId found for the URL:" + url, System.Net.HttpStatusCode.InternalServerError);
                }
                return staticMHInfo;
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        private static int[] GetDateRollingForDestination(string parentDestinationId, Dictionary<string, string> destinationIdToPeakSeason)
        {
            Dictionary<string, int> months = new Dictionary<string, int>
                {
                    { "January", 1 },{ "February", 2 },{ "March", 3 },{ "April", 4 }, { "May", 5 },{ "June", 6 },
                    { "July", 7 }, { "August", 8 },{ "September", 9 },{ "October", 10 },{ "November", 11 },{ "December", 12 }
                };

            int []dateRollingNFlexibility = new int[2];
            int currentMonth = Int32.Parse(DateTime.UtcNow.Month.ToString());
            int minPeakSelectedMonth = -1;
            int maxPeakSelectedMonth = -1;

            string[] destinationPeakMonths = destinationIdToPeakSeason[parentDestinationId].Split(',');
            int[] peakMonths = new int[13] { -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            foreach (string month in destinationPeakMonths)
            {
                peakMonths[months[month.Trim()]] = 1;
            }

            // iterate the 12 months (next year same month in worst case) in circular array fashion to get the next peak season month if any
            int maxIteration = 12;
            int index = currentMonth;
            while (maxIteration > 0)
            {
                if (index < peakMonths.Length)
                {
                    if (peakMonths[index] == 1)
                    {
                        // got the next peak season, set the dateRolling
                        if (minPeakSelectedMonth != -1)
                            maxPeakSelectedMonth = index;
                        else
                            minPeakSelectedMonth = index;
                    }
                    else if (minPeakSelectedMonth != -1)
                    {
                        if (maxPeakSelectedMonth == -1)
                        {
                            maxPeakSelectedMonth = minPeakSelectedMonth;
                        }
                        else
                        {
                            break;
                        }
                    }
                    maxIteration--;
                    index++;
                }
                else
                {
                    index = 1;
                }
            }
            
            if(minPeakSelectedMonth != -1 && maxPeakSelectedMonth != -1)
            {
                int length = 12;
                //int differenceInMinMaxPeakMonths = -1;
                int middleMonthPeakSeason = midPoint(minPeakSelectedMonth, maxPeakSelectedMonth, length);

                if (middleMonthPeakSeason == 0)
                    middleMonthPeakSeason = 1;

                if (middleMonthPeakSeason <= currentMonth)
                {
                    // increment the date rolling by an year
                    DateTime currentDate = DateTime.UtcNow;
                    DateTime meanpeakSelectedDate = new DateTime(DateTime.UtcNow.Year + 1, middleMonthPeakSeason, 15);
                    dateRollingNFlexibility[0] = Convert.ToInt32((meanpeakSelectedDate - currentDate).TotalDays);
                }
                else
                {
                    // increment the date rolling
                    DateTime currentDate = DateTime.UtcNow;
                    DateTime meanpeakSelectedDate = new DateTime(DateTime.UtcNow.Year, middleMonthPeakSeason, 15);
                    dateRollingNFlexibility[0] = Convert.ToInt32((meanpeakSelectedDate - currentDate).TotalDays);
                }

                // Generate the dynamic flexibity range of dates
                dateRollingNFlexibility[1] = int.Parse(ConfigurationManager.AppSettings["flexibility"]);
                //if (minPeakSelectedMonth <= maxPeakSelectedMonth)
                //{
                //    differenceInMinMaxPeakMonths = maxPeakSelectedMonth - minPeakSelectedMonth + 1;
                //}
                //else
                //{
                //    differenceInMinMaxPeakMonths = 12 - (minPeakSelectedMonth - maxPeakSelectedMonth);
                //}
                //dateRollingNFlexibility[1] = (differenceInMinMaxPeakMonths / 2) * 30;
            }
            return dateRollingNFlexibility;
        }

        private static int midPoint(int low, int high, int length)
        {
            int mid = -1;
            low %= length;
            high %= length;
            while(low < 0)
            {
                low += length;
            }
            while(high < low)
            {
                high += length;
            }

            mid = (low + high) / 2;
            mid %= length;
            return mid;
        }

        private static string GetMHIDByUrl(string url)
        {
            try
            {
                //List<MHIDBuildingNameMapping> mhidBuildingNameMapping = new List<MHIDBuildingNameMapping>();
                Dictionary<string, string> mhidBuildingNameMapping = new Dictionary<string, string>();
                string mhidBuildingNameMappingResponse = string.Empty;
                string Mhid = string.Empty;

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForMhidData(ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFileBucketName"], ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFilePath"])))
                {
                    mhidBuildingNameMappingResponse = reader.ReadToEnd();
                }
                mhidBuildingNameMapping = JsonConvert.DeserializeObject<Dictionary<string, string>>(mhidBuildingNameMappingResponse);

                // Search for the Url to get the MHID
                url = url.Replace("/hotels","");

                if (mhidBuildingNameMapping.TryGetValue(url.ToLower().Trim(), out Mhid))
                {

                }
                return Mhid;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }
    }
}