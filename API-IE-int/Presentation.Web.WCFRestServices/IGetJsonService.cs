﻿using Presentation.WCFRestService.Model.CustomerSupport;
using Presentation.WCFRestService.Model.DeepLinks;
using Presentation.WCFRestService.Model.Guides;
using Presentation.WCFRestService.Model.Misc;
using Presentation.WCFRestService.Model.Reviews;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using Presentation.Web.WCFRestServices.Teleappliant;
using Presentation.WCFRestService.Model.Utilities;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using Presentation.WCFRestService.Model.TTOnlineMinPriceTool;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.TeleappliantReport;

namespace Presentation.Web.WCFRestServices
{
    [ServiceContract]
    [ScriptService]
    public interface IGetJsonService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "CreateRequestIndice")]

        void CreateRequestIndice();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "DeleteQueryByOffer")]

        void DeleteQueryByOffer();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ExportAndDeleteIndices")]

        void ExportAndDeleteIndices();
        # region Teletext Web API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "AuthenticateCustomerOTP/{mobileNo}/{otpr}")]
        Stream AuthenticateCustomerOTP(string mobileNo, string otpr);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCustomerAuthenticationOTP/{mobileNo}")]
        Stream GetCustomerAuthenticationOTP(string mobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuidesIndex")]
        Stream GetGuidesIndex();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuidesIndexList")]
        Stream GetGuidesIndexList();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDestinations")]
        Stream GetDestinations();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuide/{RegionId}")]
        Stream GetGuide(string RegionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetRegionById/{RegionId}")]
        Stream GetRegionById(string RegionId);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetRegionByURL?url={url}")]
        Stream GetRegionByURL(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOverseasDestination/{destinationName}")]
        Stream GetOverseasDestination(string destinationName);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOvrseaseDestinationLabelRegions")]
        Stream GetOvrseaseDestinationLabelRegions();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelList/{destinationCode}/{hotelName}/{resortId}")]
        Stream GetHotelList(string destinationCode, string hotelName, string resortId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelListForAutoSuggest/{regionId}/{hotelName}")]
        Stream GetHotelListForAutoSuggest(string regionId, string hotelName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetMasterHotelIdByURL?url={url}")]
        Stream GetMasterHotelIdByURL(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelDataByMasterHotelId/{masterHotelId}")]
        Stream GetHotelDataByMasterHotelId(string masterHotelId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                   RequestFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "SetMasterHotelData")]
        Stream SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideLandingPageInfo/{RegionId}")]
        Stream GetGuideLandingPageInfo(string RegionId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SaveGuideInformation?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream SaveGuideInformation(Guide guide, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideInformation/{RegionId}")]
        Stream GetGuideInformation(string regionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideInformationByUrl?url={url}")]
        Stream GetGuideInformationByUrl(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideLandingPageInfoByUrl?url={url}")]
        Stream GetGuideLandingPageInfoByUrl(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetPTGGuideLandingPageInfoByUrl?url={url}")]
        Stream GetPTGGuideLandingPageInfoByUrl(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetGuidesIndexListFromCloudFront")]
        Stream GetGuidesIndexListFromCloudFront();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                           BodyStyle = WebMessageBodyStyle.Bare,
                           UriTemplate = "GetTeletextRouting?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetTeletextRouting(string destinationId, string departureId, string dates);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   BodyStyle = WebMessageBodyStyle.Bare,
                   UriTemplate = "GetTeletextRouting/{countrySite=UK}?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetTeletextRoutingIE(string countrySite, string destinationId, string departureId, string dates);//IE CODE


        #endregion

        #region Flight Stats API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetFlightByFlightNumberAndFlightDate?flightNumber={flightNumber}&day={day}&month={month}&year={year}")]
        Stream GetFlightByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetFlightScheduleByFlightNumberAndFlightDate?flightNumber={flightNumber}&day={day}&month={month}&year={year}")]
        Stream GetFlightScheduleByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year);

        #endregion

        # region TopDog API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetMobileBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetMobileBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetTOPDOGBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetTOPDOGBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetBookingDetailsFromTOPDOG/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetBookingDetailsFromTOPDOG(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetQuickQuoteDetails/{BookingReference}/{DepartureDate}")]
        Stream GetQuickQuoteDetails(string BookingReference, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDocuments/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetDocuments(string BookingReference, string Surname, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetPaymentPlan/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetPaymentPlan(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "SendBookingConfirmationEmail/{BookingReference}/{Surname}/{DepartureDate}/{EmailAddress}")]
        Stream SendBookingConfirmationEmail(string BookingReference, string Surname, string DepartureDate, string EmailAddress);
        #endregion

        #region TopDog API IE

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetMobileBasket/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetMobileBasketIE(string countrySite,string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetBasket/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetBasketIE(string countrySite, string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetTOPDOGBasket/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetTOPDOGBasketIE(string countrySite, string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetBookingDetailsFromTOPDOG/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetBookingDetailsFromTOPDOGIE(string countrySite, string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetQuickQuoteDetails/{countrySite=UK}/{BookingReference}/{DepartureDate}")]
        Stream GetQuickQuoteDetailsIE(string countrySite, string BookingReference, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDocuments/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetDocumentsIE(string countrySite, string BookingReference, string Surname, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetPaymentPlan/{countrySite=UK}/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetPaymentPlanIE(string countrySite, string BookingReference, string Surname, string DepartureDate);

        #endregion

        #region Mobile App API


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelsByDestinationCode/{destCode}")]
        Stream GetHotelsByDestinationCode(string destCode);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelByID/{hotelId}")]
        Stream GetHotelByID(string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelReviewByID/{hotelId}")]
        Stream GetHotelReviewByID(string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHolidaySearch?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&destinationType={destinationType}&platform={platform}&isShortDescriptionRequired={isShortDescriptionRequired}&os={os}")]
        Stream GetHolidaySearch(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType, string labelId, string preferredHotelKeys, string skipFacets, string destinationType, string platform, string isShortDescriptionRequired, string os);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHolidaySearchV_2?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&destinationType={destinationType}")]
        Stream GetHolidaySearchV_2(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType, string labelId, string preferredHotelKeys, string skipFacets, string destinationType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHolidays?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}")]
        Stream GetHolidays(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType);


        [OperationContract]
        [WebGet(UriTemplate = "GetMapByLocation?lat={lat}&lon={lon}",
                                RequestFormat = WebMessageFormat.Xml,
                                ResponseFormat = WebMessageFormat.Xml,
                                BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetMapByLocation(string lat, string lon);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHomePageSlider")]
        Stream GetHomePageSlider();


        [OperationContract]

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOffersAndDealsSlider")]
        Stream GetOffersAndDealsSlider();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPubsAndRestruantsByGoogleAPI/{lat}/{lon}/{type}/{hotelId}",
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetPubsAndRestruantsByGoogleAPI(string lat, string lon, string type, string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixHotelDescription?hotelOperator={hotelOperator}&contentId={contentId}")]
        Stream GetArtirixHotelDescription(string hotelOperator, string contentId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixRouting?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetArtirixRouting(string destinationId, string departureId, string dates);

        //IE CODE
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                           BodyStyle = WebMessageBodyStyle.Bare,
                           UriTemplate = "GetArtirixRouting/{countrySite=UK}?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetArtirixRoutingIE(string countrySite, string destinationId, string departureId, string dates);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixDiversity?destinationId={destinationId}&hotelKeys={hotelKeys}&departureIds={departureIds}&dateMin={dateMin}&dateMax={dateMax}&children={children}&adults={adults}&durationMax={durationMax}&durationMin={durationMin}&ratings={ratings}")]
        Stream GetArtirixDiversity(string destinationId, string hotelKeys, string departureIds, string dateMin, string dateMax, string children, string adults, string durationMax, string durationMin, string ratings);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHotelContent?hotelOperator={hotelOperator}&contentId={contentId}")]
        Stream GetHotelContent(string hotelOperator, string contentId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetGoogleAirportInfo/{airport}")]
        Stream GetGoogleAirportInfo(string airport);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetGoogleDistance/{oLat}/{oLng}/{dLat}/{dLng}/{mode}")]
        Stream GetGoogleDistance(string oLat, string oLng, string dLat, string dLng, string mode);



        #endregion

        # region PostCode API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAddressesByPostCode/{postCode}")]
        Stream GetAddressesByPostCode(string postCode);

        #endregion

        # region SmartFocus API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetNewsLetterSubscription?name={name}&email={email}&phone={phone}&airport={airport}")]
        Stream GetNewsLetterSubscription(string name, string email, string phone, string airport);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "EmailSubscription")]
        Stream GetSubscription(emailSubscription emailSubscription);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetNewsLetterSubscriberByEmail/{email}")]
        Stream GetNewsLetterSubscriberByEmail(string email);


        #endregion


        #region SmartFocus API IE

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetNewsLetterSubscription/{countrySite=UK}?name={name}&email={email}&phone={phone}&airport={airport}")]
        Stream GetNewsLetterSubscriptionIE(string countrySite, string name, string email, string phone, string airport);

        #endregion

        #region MinPriceTool for IE


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetQueryIdObjects/{countrySite=UK}?queryId={queryId}&sectionId={sectionId}")]
        Stream GetQueryIdObjectsIE(string countrySite, string queryId, string sectionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetSectionObjects/{countrySite=UK}?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjectsIE(string countrySite, string sectionIds, string platform);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                         BodyStyle = WebMessageBodyStyle.Bare,
                         UriTemplate = "GetMinPriceHomePage/{countrySite=UK}?isEs={isES}")]
        Stream GetMinPriceHomePageIE(string countrySite, string isEs);

        [OperationContract]
        [WebInvoke(Method = "POST",
                        RequestFormat = WebMessageFormat.Json,
                        ResponseFormat = WebMessageFormat.Json,
                        BodyStyle = WebMessageBodyStyle.Bare,
                        UriTemplate = "SaveorUpdateMinPriceNew/{countrySite=UK}?userId={userId}&isEs={isEs}")]
        Stream SaveorUpdateMinPriceNewIE(string countrySite, saveUpdateMinPrice saveUpdateMinPrice, string userId, string isEs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                        BodyStyle = WebMessageBodyStyle.Bare,
                        UriTemplate = "GetArtirixMinPriceNew/{countrySite=UK}?adults={adults}&children={children}&starRatings={starRatings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&departureName={departureName}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&channelName={channelName}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}&isEs={isES}")]
        Stream GetArtirixMinPriceNewIE(string countrySite, string adults, string children, string starRatings, string departureDate, string dateMin, string dateMax,
                                       string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                       string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                       string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                       string boardType, string dateOffSet, string flexDates, string isEs);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "DeleteObject/{countrySite=UK}?queryId={queryId}&userId={userId}&isEs={isES}")]
        Stream DeleteObjectIE(string countrySite, string queryId, string userId, string isES);


        [OperationContract]
        [WebInvoke(Method = "POST",
                        RequestFormat = WebMessageFormat.Json,
                        ResponseFormat = WebMessageFormat.Json,
                        BodyStyle = WebMessageBodyStyle.Bare,
                        UriTemplate = "EditorUpdate/{countrySite=UK}?userId={userId}&isEs={isEs}")]
        Stream EditorUpdateIE(string countrySite, editUpdate editUpdate, string userId, string isEs);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                        BodyStyle = WebMessageBodyStyle.Bare,
                        UriTemplate = "SetQueryImageUrl/{countrySite=UK}?queryId={queryId}&sectionId={sectionId}&imageUrl={imageUrl}&userId={userId}&isEs={isEs}")]
        Stream SetQueryImageUrlIE(string countrySite, string queryId, string sectionId, string imageUrl, string userId, string isES);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                        BodyStyle = WebMessageBodyStyle.Bare,
                        UriTemplate = "GetSectionObjectsV2/{countrySite=UK}?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjectsV2IE(string countrySite, string sectionIds, string platform);
        #endregion


        #region MinPriceTool
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetArtirixMinPrice?adults={adults}&children={children}&starRatings={starRatings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&departureName={departureName}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&channelName={channelName}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}")]
        Stream GetArtirixMinPrice(string adults, string children, string starRatings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetMinPriceHomePage?isEs={isES}")]
        Stream GetMinPriceHomePage(string isEs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "DeleteObject?queryId={queryId}&userId={userId}&isEs={isES}")]
        Stream DeleteObject(string queryId, string userId, string isES);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetSectionObjects?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjects(string sectionIds, string platform);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetQueryIdObjects?queryId={queryId}&sectionId={sectionId}")]
        Stream GetQueryIdObjects(string queryId, string sectionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SetQueryImageUrl?queryId={queryId}&sectionId={sectionId}&imageUrl={imageUrl}&userId={userId}&isEs={isEs}")]
        Stream SetQueryImageUrl(string queryId, string sectionId, string imageUrl, string userId, string isEs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetDistinctSectionId?sectionIdStartWith={sectionIdStartWith}&platform={platform}")]
        Stream GetDistinctSectionId(string sectionIdStartWith, string platform);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "EditorUpdate?userId={userId}&isEs={isEs}")]
        Stream EditorUpdate(editUpdate editUpdate, string userId, string isEs);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SaveorUpdateMinPrice?userId={userId}&isEs={isEs}")]
        Stream SaveorUpdateMinPrice(saveUpdateMinPrice saveUpdateMinPrice, string userId,string isEs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetArtirixMinPriceNew?adults={adults}&children={children}&starRatings={starRatings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&departureName={departureName}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&channelName={channelName}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}&isEs={isES}")]
        Stream GetArtirixMinPriceNew(string adults, string children, string starRatings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates, string isEs);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SaveorUpdateMinPriceNew?userId={userId}&isEs={isEs}")]
        Stream SaveorUpdateMinPriceNew(saveUpdateMinPrice saveUpdateMinPrice, string userId, string isEs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetSectionObjectsV2?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjectsV2(string sectionIds, string platform);

        #endregion

        #region ElasticSearch IE
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "ESHolidaySearch/{countrySite=uk}?destinationIds={destinationIds}&departureIds={departureIds}&boardType={boardType}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&priceMin={priceMin}&priceMax={priceMax}&ratings={ratings}&tradingNameIds={tradingNameIds}&destinationType={destinationType}&hotelKeys={hotelKeys}&tripAdvisorRating={tripAdvisorRating}&hotelKeysToExclude={hotelKeysToExclude}&sort={sort}&channelId={channelId}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&isShortDescriptionRequired={isShortDescriptionRequired}&platform={platform}&os={os}&timeout={timeout}&longDateRangeQuery={longDateRangeQuery}&cacheHotTime={cacheHotTime}&navigatedPage={navigatedPage}&pageSource={pageSource}&utmSource={utmSource}&isDirectionalSelling={isDirectionalSelling}")]
        Stream ESHolidaySearchIE(string countrysite, int destinationIds, int departureIds, string boardType, string departureDate, string dateMin, string dateMax, int durationMin, int durationMax,
                                 int adults, int children, int priceMin, int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                 int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId, string preferredHotelKeys, string skipFacets,
                                 string isShortDescriptionRequired, string platform, string os, int timeout, bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, string utmSource, string isDirectionalSelling);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "ESDiversity/{countrySite=uk}?destinationId={destinationId}&departureIds={departureIds}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&ratings={ratings}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&platform={platform}&os={os}")]
        Stream ESDiversityIE(string countrysite, int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                            int children, string ratings, string hotelKeys, string tradingNameIds, string platform, string os);

        #endregion


        #region ElasticSearch
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "ESHolidaySearch?destinationIds={destinationIds}&departureIds={departureIds}&boardType={boardType}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&priceMin={priceMin}&priceMax={priceMax}&ratings={ratings}&tradingNameIds={tradingNameIds}&destinationType={destinationType}&hotelKeys={hotelKeys}&tripAdvisorRating={tripAdvisorRating}&hotelKeysToExclude={hotelKeysToExclude}&sort={sort}&channelId={channelId}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&isShortDescriptionRequired={isShortDescriptionRequired}&platform={platform}&os={os}&timeout={timeout}&longDateRangeQuery={longDateRangeQuery}&cacheHotTime={cacheHotTime}&navigatedPage={navigatedPage}&pageSource={pageSource}&utmSource={utmSource}&isDirectionalSelling={isDirectionalSelling}")]
        Stream ESHolidaySearch(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin, string dateMax, int durationMin, int durationMax,
                                 int adults, int children, int priceMin, int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                 int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId, string preferredHotelKeys, string skipFacets,
                                 string isShortDescriptionRequired, string platform, string os, int timeout, bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, string utmSource, string isDirectionalSelling);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "ESDiversity?destinationId={destinationId}&departureIds={departureIds}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&ratings={ratings}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&platform={platform}&os={os}")]
        Stream ESDiversity(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                            int children, string ratings, string hotelKeys, string tradingNameIds, string platform, string os);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "TAReviews?TAID={TAID}")]
        Stream TAReviews(string taid);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "UpdateTAInfo")]
        Stream UpdateTAInfo();

        [OperationContract]
        [WebInvoke(Method = "GET",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "AkamaiFastPurge?urls={urls}")]
        Stream AkamaiFastPurge(string urls);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "GetMasterHotelInfo?MHID={MHID}")]
        Stream GetMasterHotelInfo(string MHID);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PostMasterHotelInfo?MHID={MHID}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream PostMasterHotelInfo(MHIDStaticInfo mhidStaticInfo, string MHID);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "GetMasterHotelDescription?MHID={MHID}&fromBot={fromBot}")]
        Stream GetMasterHotelDescription(string MHID, string fromBot);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "ProcessMasterHotelImages?MHID={MHID}&MasterHotelImages={MasterHotelImages}&userId={userId}&MHETag={MHETag}")]
        Stream ProcessMasterHotelImages(string MHID, string MasterHotelImages, string userId, string MHETag);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddMasterHotelTag?tagName={tagName}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        string AddMasterHotelTag(string tagName, string userId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddMasterHotelFacilities?facilityName={facilityName}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        string AddMasterHotelFacilities(string facilityName, string userId);


        [OperationContract]
        [WebInvoke(UriTemplate = "/SwapMasterHotelImages?MHID={MHID}&destImage={destImage}&MHETag={MHETag}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream SwapMasterHotelImages(string MHID, string destImage, string MHETag, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "CreateTimeBasedIndices")]
        void CreateTimeBasedIndices();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "DeleteTimeBasedIndices")]
        void DeleteTimeBasedIndices();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "DeleteResultsIndexResults")]
        void DeleteResultsIndexResults();


        #endregion

        #region Dynamic SEO
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetCategoryList")]
        Stream GetCategoryList();

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessCategoryList?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessCategoryList(List<DynamicSEO.Category> categoryList, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetTemplateList")]
        Stream GetTemplateList();

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessTemplateList?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessTemplateList(List<DynamicSEO.Template> templateList, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHoliday?fileName={fileName}")]
        Stream GetHoliday(string fileName);

        #endregion

        #region GoogleAPI
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetGoogleNearBy?lat={lat}&lon={lon}&patternType={patternType}&type={type}&radius={radius}",
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetGoogleNearBy(float lat, float lon, string patternType, string type, string radius);

        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetImage?lat={lat}&lng={lng}&zoom={zoom}")]
        Stream GetImage(string lat, string lng, string zoom);


        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "InvalidateUATCloudFront?urlPaths={urlPaths}&isAMP={isAMP}")]
        void InvalidateUATCloudFront(string urlPaths, string isAMP);

        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "InvalidatePRODCloudFront?urlPaths={urlPaths}")]
        void InvalidatePRODCloudFront(string urlPaths);

        #endregion

        #region Reviews

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetAllPublishedReviews?limit={limit}&startIndex={startIndex}&reviewType={reviewType}")]
        Stream GetAllPublishedReviews(int limit, int startIndex, string reviewType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetReviews?limit={limit}&startIndex={startIndex}&reviewProvider={reviewProvider}&showPublished={showPublished}&showRatings={showRatings}")]
        Stream GetReviews(string limit, string startIndex, string reviewProvider, string showPublished, string showRatings);


        [OperationContract]
        [WebInvoke(Method = "POST",
                               RequestFormat = WebMessageFormat.Json,
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "PublishorUnpublishReviews?userId={userId}")]
        Stream PublishorUnpublishReviews(ReviewInformation review, string userId);


        #endregion

        #region HomePage
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetNearestAirportGroup/{Latitude}/{Longitude}")]
        Stream GetNearestAirportGroup(string Latitude, string Longitude);
        #endregion

        #region DeepLinks

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GenerateDeepLinks")]
        void GenerateDeepLinks();

        [OperationContract]
        [WebInvoke(Method = "POST",
                               RequestFormat = WebMessageFormat.Json,
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "SendAPPLink")]
        Stream SendAPPLink(APPLink applink);


        #endregion

        #region LambdaHits
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GenerateRecentBlogsFromDB?procedureName={procedureName}")]
        void GenerateRecentBlogsFromDB(string procedureName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GenerateBlogsByTagsFromDB?procedureName={procedureName}")]
        void GenerateBlogsByTagsFromDB(string procedureName);
        #endregion

        #region Blogs
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetRecentBlogs?pageName={pageName}")]
        Stream GetRecentBlogs(string pageName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetBlogs")]
        Stream GetBlogs();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "CompressBlogImages")]
        Stream CompressBlogImages();
        #endregion

        #region HotelLandingPage
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetMasterHotelInfoByUrl?url={url}")]
        Stream GetMasterHotelInfoByUrl(string url);
        #endregion


        #region Payments IE

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessPayment/{countrySite=UK}",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        Stream ProcessPaymentIE(string countrySite, MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessThreeDSecurePayment/{countrySite=UK}",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessThreeDSecurePaymentIE(string countrySite, MakePayment paymentDetails);


        [OperationContract]
        [WebInvoke(UriTemplate = "/MakePaymentProcess/{countrySite=UK}",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        void MakePaymentProcessIE(string countrySite, MakePayment paymentDetails);
        #endregion Payments


        #region Payments

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessPaymentTesting",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        Stream ProcessPaymentTesting(MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessPayment",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        Stream ProcessPayment(MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessThreeDSecurePayment",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessThreeDSecurePayment(MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessThreeDSecurePaymentTesting",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessThreeDSecurePaymentTesting(MakePayment paymentDetails);        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetCreditCardCharges")]
        Stream GetCreditCardCharges();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetCreditCardChargesTesting")]
        Stream GetCreditCardChargesTesting();

        [OperationContract]
        [WebInvoke(UriTemplate = "/MakePaymentProcess",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        void MakePaymentProcess(MakePayment paymentDetails);
        #endregion Payments

        #region CustomerSupport
        [OperationContract]
        [WebInvoke(UriTemplate = "/CustomerSupportRequest",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream CustomerSupportRequest(CustomerSupportForm customerSupportDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/TeleappliantCustomerSupportForm",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream TeleappliantCustomerSupportForm(TeleappliantCustomerSupportForm customerSupportDetails);        

        [OperationContract]
        [WebInvoke(Method = "POST",
             UriTemplate = "SubmitCustomerDetails/",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string SubmitCustomerDetails();
        #endregion


        [OperationContract]
        [WebInvoke(UriTemplate = "/GetTeleappliantNumbers?userSessionInfo={userSessionInfo}",
                   Method = "POST",                   
                   ResponseFormat = WebMessageFormat.Json)]
        Stream GetTeleappliantNumbers(string userSessionInfo);

        #region Email

        [OperationContract]
        [WebInvoke(UriTemplate = "/SendEmail",
                   Method = "POST")]
        void SendEmail(Email email);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateVisionTicket",
                  Method = "POST")]
        string CreateVisionTicket(VisionTicket visionTicket);


        #endregion Email

        #region rankoverride tool
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "RefreshAlternateConfigFile")]
        Stream RefreshAlternateConfigFile();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "UpdateDictionary?dictionaryName={dictionaryName}")]
        Stream UpdateDictionary(string dictionaryName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "RetrieveAll?action={action}")]
        Stream RetrieveAll(string action);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Retrieve?action={action}&mhid={mhid}")]
        Stream Retrieve(string action, string mhid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "Delete?action={action}&mhid={mhid}")]
        Stream Delete(string action, string mhid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "OverrideRank?action={action}&mhid={mhid}&newrank={newrank}&toDisplayOnFod={toDisplayOnFod}&comment={comment}")]
        Stream OverrideRank(string action, string mhid, string newrank, string toDisplayOnFod, string comment);

        #endregion rankoverride tool

        #region website signup IE

        [OperationContract]
        [WebInvoke(UriTemplate = "/WebsiteSignUp/{countrySite=UK}",
                   Method = "POST",
                   RequestFormat = WebMessageFormat.Json,
                   ResponseFormat = WebMessageFormat.Json,
                   BodyStyle = WebMessageBodyStyle.Bare)]
        void WebsiteSignUpIE(string countrySite, SignUpDetails signUpDetails);

        #endregion website signup IE

        #region website signup

        [OperationContract]
        [WebInvoke(UriTemplate = "/WebsiteSignUp",
                   Method = "POST",
                   RequestFormat = WebMessageFormat.Json,
                   ResponseFormat = WebMessageFormat.Json,
                   BodyStyle = WebMessageBodyStyle.Bare)]
        void WebsiteSignUp(SignUpDetails signUpDetails);

        #endregion website signup

        #region TTOnlineBookability
        [OperationContract]
        [WebInvoke(UriTemplate = "/ESAvailabilitySearch",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json)]
        Stream ESAvailabilitySearch(AvailabilitySearchRequest availabilityRequest);



        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                           BodyStyle = WebMessageBodyStyle.Bare,
                                          UriTemplate = "/InvokeRoomValuationService?availabilityId={availabilityId}&roomIds={roomIds}&promotionalCode={promotionalCode}&valuationId={valuationId}")]
        Stream InvokeRoomValuationService(string availabilityId, string roomIds, string promotionalCode, string valuationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/BookingService",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json)]
        Stream BookingService(BookingRequest bookingRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
             UriTemplate = "SubmitData/",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string SubmitData();

        [OperationContract]
        [WebInvoke(UriTemplate = "/TestingB2CAccommodationService",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json)]
        Stream TestingB2CAccommodationService(AvailabilitySearchRequest availabilityRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                           BodyStyle = WebMessageBodyStyle.Bare,
                                          UriTemplate = "/GetEssentialInfo?hotelCode={hotelCode}&destinationCode={destinationCode}")]
        Stream GetEssentialInfo(string hotelCode, string destinationCode);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetDestinationsAndHotels/{SearchTerm}")]
        Stream GetDestinationsAndHotels(string SearchTerm);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  RequestFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "SaveReviewNPayInformation")]
        Stream SaveReviewNPayInformation(ReviewNPayParams reviewNPayParam);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetReviewNPayInformation?id={id}")]
        Stream GetReviewNPayInformation(string id);

        #endregion

        #region HotelOnlyOffers MPT
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetHotelOnlyMinPriceHomePage")]
        Stream GetHotelOnlyMinPriceHomePage();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                            BodyStyle = WebMessageBodyStyle.Bare,
                            UriTemplate = "GetB2CMinPrice?adults={adults}&children={children}&checkInDate={checkInDate}&checkOutDate={checkOutDate}&duration={duration}&destinationCode={destinationCode}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&establishmentCode={establishmentCode}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}&ratings={ratings}&tripAdvisorRating={tripAdvisorRating}")]
        Stream GetB2CMinPrice(string adults, string children, string checkInDate, string checkOutDate, string duration, string destinationCode, string destinationName, string priceMin,
                                           string priceMax, string establishmentCode, string boardType, string dateOffSet, string flexDates, string ratings, string tripAdvisorRating);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "DeleteQueryObject?queryId={queryId}&userId={userId}")]
        Stream DeleteQueryObject(string queryId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SaveOrUpdateHotelOnlyMinPriceOffers?userId={userId}")]
        Stream SaveOrUpdateHotelOnlyMinPriceOffers(HotelOnlyMinPriceOffer saveUpdateMinPrice, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "UpdateHotelOnlyMinPriceOfferFields?userId={userId}")]
        Stream UpdateHotelOnlyMinPriceOfferFields(editUpdate editUpdate, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetMerchandizingOffers?sectionIds={sectionIds}")]
        Stream GetMerchandizingOffers(string sectionIds);
        #endregion

        #region Tool APIs
        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadS3WithHotelInformation?MHIDs={mhids}",
                   Method = "GET",
                   ResponseFormat = WebMessageFormat.Json)]
        Stream LoadS3WithHotelInformation(string mhids);

        [OperationContract]
        [WebInvoke(UriTemplate = "ProcessStandaloneCredit?userId={userId}",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            Method = "POST")]
        Stream ProcessStandaloneCredit(StandaloneCredit standaloneCredit, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "LogCheapestBoard?data={data}")]

        void LogCheapestBoard(string data);
        #endregion

        #region Directional Selling Chat Feature
        [OperationContract]
        [WebInvoke(UriTemplate = "/PostChatMessages",
                   Method = "POST",
                   ResponseFormat = WebMessageFormat.Json)]
        Stream PostChatMessages(ChatMessage chatMessage);
        #endregion
    }
}
