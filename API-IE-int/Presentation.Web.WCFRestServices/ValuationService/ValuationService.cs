﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Configuration;

namespace Presentation.Web.WCFRestServices.ValuationService
{
    public class ValuationService
    {
        public static HttpWebRequest CreateValuationSOAPWebRequest()
        {

            //Making Web Request    
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["B2BAccommodationXMLService"]);
            //SOAPAction    
            Req.Headers.Add(@"SOAPAction:http://tempuri.org/IAccommodationB2BXmlValuationService/GetValuationResponse");
            //Content_type    
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method    
            Req.Method = "POST";
            //return HttpWebRequest    
            return Req;
        }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public RoomValuationBody Body { get; set; }
        [XmlAttribute(AttributeName = "s", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class RoomValuationBody
    {
        [XmlElement(ElementName = "GetValuationResponseResponse", Namespace = "http://tempuri.org/")]
        public GetValuationResponseResponse GetValuationResponseResponse { get; set; }
    }

    [XmlRoot(ElementName = "GetValuationResponseResponse", Namespace = "http://tempuri.org/")]
    public class GetValuationResponseResponse
    {
        [XmlElement(ElementName = "GetValuationResponseResult", Namespace = "http://tempuri.org/")]
        public string GetValuationResponseResult { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}