﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Guides;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.Guides
{
    public static class Guides
    {
        public static string AddRecordguidesindexlist(GuideDestination vGuideDestination)
        {
            string isFileUploaded = string.Empty;
            try
            {
                //fetch guidesindexlist.json
                DestinationGuides destinationGuides = GetGuideIndexList();
                if (destinationGuides == null)
                {
                    return string.Empty;
                }

                var duprec = destinationGuides.destinations.SingleOrDefault(p => p.id == vGuideDestination.regid);

                if (duprec == null)
                {
                    destinationGuides.destinations.Add(new Destination
                    {
                        id = vGuideDestination.regid,
                        grand_regid = vGuideDestination.grand_regid,
                        destcodes = vGuideDestination.destcode,
                        grand_reglink = vGuideDestination.grand_reglink,
                        grand_regname = vGuideDestination.grand_regname,
                        parent_link = vGuideDestination.parent_link,
                        name = vGuideDestination.regname,
                        parent_regid = vGuideDestination.parent_regid,
                        parent_regname = vGuideDestination.parent_regname,
                        reglevel = vGuideDestination.reglevel,
                        resortcode = vGuideDestination.resortid
                    });
                }
                else
                {
                    duprec.name = vGuideDestination.regname;
                    duprec.reglevel = vGuideDestination.reglevel;
                    duprec.parent_regid = vGuideDestination.parent_regid;
                }

                isFileUploaded = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["AWSGuideBucketName"], ConfigurationManager.AppSettings["GuideIndexListFileName"], destinationGuides);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isFileUploaded;
        }

        public static DestinationGuides GetGuideIndexList()
        {
            try
            {
                var guideJson = "";

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["AWSGuideBucketName"], ConfigurationManager.AppSettings["GuideIndexListFileName"])))
                {
                    guideJson = reader.ReadToEnd();
                }
                DestinationGuides destinationGuides = JsonConvert.DeserializeObject<DestinationGuides>(guideJson);

                return destinationGuides;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        public static string getNextRegID()
        {
            try
            {
                DestinationGuides destinationGuides = GetGuideIndexList();
                var maxregid = (from d in destinationGuides.destinations
                                orderby d.id.Length, d.id
                                select d.id).ToList();
                Int64 nextregid = Convert.ToInt64(maxregid[maxregid.Count - 1]) + 1;

                return nextregid.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetDestinationUrl(string destinationId)
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\" + ConfigurationManager.AppSettings["GuideIndexListFileName"];
            var json = String.Empty;

            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        json = sr.ReadToEnd();
                        DestinationGuides destinationGuides = JsonConvert.DeserializeObject<DestinationGuides>(json);
                        return destinationGuides.destinations.Where(p => p.id == destinationId).FirstOrDefault().linkurl;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return string.Empty;
        }

        public static string GetDestinationName(string destinationId)
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\" + ConfigurationManager.AppSettings["GuideIndexListFileName"];
            var json = String.Empty;

            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        json = sr.ReadToEnd();
                        DestinationGuides destinationGuides = JsonConvert.DeserializeObject<DestinationGuides>(json);
                        return destinationGuides.destinations.Where(p => p.id == destinationId).FirstOrDefault().name;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return string.Empty;
        }

        public static string GetDestinationIdByUrl(string guideUrl)
        {
            try
            {
                DestinationGuides destinationGuides = GetGuideIndexList();
                return destinationGuides.destinations.Where(p => p.linkurl == guideUrl).FirstOrDefault().id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Empty;
        }

        public static GuideDestination GetGuideDestinationInfo(string regionId)
        {
            try
            {
                // Get the file From S3 based on the RegionId.
                string bucketName = ConfigurationManager.AppSettings["AWSGuideBucketName"];
                Stream data = Utilities.DownloadS3Object(bucketName, regionId + ".json");
                StreamReader reader = new StreamReader(data);
                string json = reader.ReadToEnd();
                GuideDestination guideDestination = JsonConvert.DeserializeObject<GuideDestination>(json);
                return guideDestination;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Guide GetGuideInfo(string regionId)
        {
            // Get the file From S3 based on the RegionId.
            string bucketName = ConfigurationManager.AppSettings["AWSGuideBucketName"];
            Stream data = Utilities.DownloadS3Object(bucketName, regionId + ".json");
            StreamReader reader = new StreamReader(data);
            string json = reader.ReadToEnd();
            Guide guide = JsonConvert.DeserializeObject<Guide>(json);
            //Remove all the sections the existing the JSON file Places To GO/Things to Do and Overview.
            if (guide.sections != null && guide.sections.Count > 0)
            {
                guide.sections.RemoveAll(s => !s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) && !s.sname.Equals("overview", StringComparison.OrdinalIgnoreCase) && !s.sname.Equals("essential info", StringComparison.OrdinalIgnoreCase) && !s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
            }

            if (guide.metatags == null)
            {
                guide.metatags = new List<MetaTag>();
            }

            return guide;
        }

        public static List<OverseasDestination> GetOverseasDestination(string destinationName)
        {
            List<OverseasDestination> overseasList = new List<OverseasDestination>();
            OverseasDestination overdest = new OverseasDestination();
            OverseasDestination overdest1 = new OverseasDestination();
            OverseasDestination overdest2 = new OverseasDestination();
            More overmore = new More();
            var json = String.Empty;
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinations-overseas.json";

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                List<OverseasDestination> destRegionList = JsonConvert.DeserializeObject<List<OverseasDestination>>(json);

                if (destinationName.ToLower().Equals("all"))
                {
                    overseasList = destRegionList;
                }
                else
                {
                    foreach (OverseasDestination dest in destRegionList)
                    {
                        if (dest.DestinationName.ToLower().Equals(destinationName.ToLower()))
                        {
                            overdest.DestinationId = dest.DestinationId;
                            overdest.DestinationName = dest.DestinationName;
                            overdest.More = dest.More;
                            break;
                        }

                        if (dest.DestinationName.ToLower().IndexOf(destinationName.ToLower()) != -1)
                        {
                            if (!string.IsNullOrEmpty(overdest1.DestinationId))
                            {
                                overdest1.DestinationId = dest.DestinationId;
                                overdest1.DestinationName = dest.DestinationName;
                                overdest1.More = dest.More;
                            }

                            if (!string.IsNullOrEmpty(overdest.DestinationId))
                                break;
                        }

                        if (dest.More != null)
                        {
                            foreach (More more in dest.More)
                            {
                                if (more.DestinationName.ToString().ToLower().IndexOf(destinationName.ToLower()) != -1)
                                {
                                    overdest2.DestinationId = more.DestinationId;
                                    overdest2.DestinationName = more.DestinationName;
                                    overdest2.ParentDestinationId = dest.DestinationId;
                                    overdest2.ParentDestinationName = dest.DestinationName;
                                    break;
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(overdest.DestinationId))
                        overseasList.Add(overdest);
                    else if (!string.IsNullOrEmpty(overdest1.DestinationId))
                        overseasList.Add(overdest1);
                    else if (!string.IsNullOrEmpty(overdest2.DestinationId))
                        overseasList.Add(overdest2);

                }
            }

            return overseasList;
        }

        public static GuideDestination GetGuide(string regionId)
        {

            //Get Region From S3.                
            GuideDestination guideDestination = GetGuideDestinationInfo(regionId);

            if (guideDestination == null)
            {
                throw new NullReferenceException("Guide object does not exist");
            }
            List<OverseasDestination> overseasList = GetOverseasDestination(guideDestination.regname);
            if (overseasList != null && overseasList.Count > 0)
            {
                guideDestination.showLastMinuteDeals = true;
            }
            //Get GuideIndexList JSON File.
            DestinationGuides guideIndex = GetGuideIndexList();

            //Get Child Regions based on the Region ID                
            IEnumerable<Destination> guideChilds = guideIndex.destinations.Where(p => p.parent_regid == regionId);

            //Append ChildRegion Info to the ParentRegion if Exists
            if (guideChilds != null && guideChilds.Count() > 0)
            {
                guideDestination.link_summary = new List<link_summary>();
                GuideDestination guideChildInfo = null;
                link_summary linkSummary = null;
                for (var guideChildIndex = 0; guideChildIndex < guideChilds.Count(); guideChildIndex++)
                {
                    var guideChild = guideChilds.ToList()[guideChildIndex];
                    //Get Child Regions from S3.                        
                    guideChildInfo = GetGuideDestinationInfo(guideChild.id);
                    if (guideChildInfo == null)
                    {
                        continue;
                    }

                    linkSummary = new link_summary();
                    linkSummary.lname = GetDestinationName(guideChild.id);
                    linkSummary.guideId = guideChild.id;
                    linkSummary.bgimage_m = guideChildInfo.bgimage_m;
                    linkSummary.lurl = GetDestinationUrl(guideChild.id).ToLower();
                    linkSummary.sectionid = guideChildInfo.sectionid;
                    linkSummary.recommendedhotels = guideChildInfo.recommendedhotels == null ? new List<RecommendedHotel>() : guideChildInfo.recommendedhotels;

                    //Add Child Regions Places To Go/Things To Do to the Parent Region.
                    var childGuidePlacesToGo = guideChildInfo.sections.Where(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                    if (childGuidePlacesToGo != null && childGuidePlacesToGo.Count() > 0)
                    {
                        linkSummary.PlacesToGo = childGuidePlacesToGo.FirstOrDefault();
                    }

                    guideDestination.link_summary.Add(linkSummary);
                }
            }

            if (guideDestination.recommendedhotels == null)
            {
                guideDestination.recommendedhotels = new List<RecommendedHotel>();
            }

            return guideDestination;
        }
    }
}