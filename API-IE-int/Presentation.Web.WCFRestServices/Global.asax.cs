﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Presentation.WCFRestService.Model;
using System.Web;
using Presentation.WCFRestService.Model.Routing;
using Presentation.WCFRestService.Model.Misc;
using System.Net;

namespace Presentation.Web.WCFRestServices
{
    enum Category { Payments, Flights, Accomodation, Transfer, ATOL_Certificate, Important_Information, Generic };
    public class Global : System.Web.HttpApplication
    {
        #region IE Code
        //TODO: Use Dict to hold list of valid country sites, and use that in IE api calls
        public const string COUNTRY_SITE_UK = "UK";
        public const string COUNTRY_SITE_IE = "IE";
        public const string ENV_TEST = "TEST";
        public const string ENV_LIVE = "LIVE";

        public static string airportIdsMT { get; set; }
        public static string reverseAirportIdsMT { get; set; }
        public static string parentAirportIdsMT { get; set; }
        public static string childAirportIdsMT { get; set; }
        public static Dictionary<string, Dictionary<string, string>> airportCodesMT { get; set; }
        public static Dictionary<string, Dictionary<string, int>> reverseAirportCodesMT { get; set; }
        public static Dictionary<string, Dictionary<int, int>> parentAirportCodesMT { get; set; }

        public static string parentAirportIdsMultiMT { get; set; }
        public static Dictionary<string, Dictionary<int, List<int>>> parentAirportCodesMultiMT { get; set; }

        public static Dictionary<string, Dictionary<int, string>> childAirportCodesMT { get; set; }
        public static string DataServicesUrlRoot { get; set; }

        public static string RedisHostForRoutingApiIE;
        public static string RedisPortForRoutingApiIE;
        public static string RedisPasswordForRoutingApiIE;
        public static string RedisConnectionStringForRoutingApiIE;
        #endregion

        public static string iffToImageCountResult { get; set; }
        public static string mhidToImageCountResult { get; set; }
        public static string mhidToLandingPageResults { get; set; }
        public static string labelRegionMapping { get; set; }
        public static string iffvsMasterId { get; set; }
        public static string airportIds { get; set; }
        public static string reverseAirportIds { get; set; }
        public static string parentAirportIds { get; set; }
        public static string childAirportIds { get; set; }
        
        public static string ArtirixServiceUrlFmt { get; set; }
        public static Int32 minCountOfImage { get; set; }
        public static Dictionary<string, int> iffToImageCountList { get; set; }
        public static Dictionary<string, int> mhidtoImageCountList { get; set; }
        public static Dictionary<string, string> mhidToLandingPageURL { get; set; }
        public static Dictionary<string, string> labelRegionMappingList { get; set; }
        public static Dictionary<string, int> iffvsMasterIdMapping { get; set; }
        public static Dictionary<string, string> airportCodes { get; set; }
        public static Dictionary<string, int> reverseAirportCodes { get; set; }
        public static Dictionary<int, int> parentAirportCodes { get; set; }
        public static Dictionary<string, TelePhoneSet> TelePhoneSetsMT { get; set; }
        public static Dictionary<string, TelePhoneSet> TelePhoneSetsForAppMT { get; set; }
        public static Dictionary<string, List<DestinationTelePhoneNumber>> DestinationTelePhoneNumbersMT { get; set; }
        public static Dictionary<string, List<DestinationTelePhoneNumber>> DestinationTelePhoneNumbersForAppMT { get; set; }

        public static Dictionary<int, string> childAirportCodes { get; set; }
        public static Dictionary<string,string> ICAOAirlinesMapping { get; set; }
        public static string imgSourceURLFormat { get; set; }
        public static string artxImgSourceURLFormat { get; set; }
        public static Int32 MaxDegreeOfParallelism { get; set; }
        public static List<HotelURL> AllHotelURLMap { get; set; }
        public static Dictionary<string, MasterHotelImagesMap> iffToMHIDMap { get; set; }
        public static Dictionary<string, string> MHIDtoMHNMap { get; set; }
        public static Dictionary<string, int> MHIDtoRegIDMap { get; set; }
        public static Dictionary<int, string> RegIDtoRegNameMap { get; set; }
        public static Dictionary<string, string> GuidetoGuideBoundaries { get; set; }
        public static Dictionary<string, TelePhoneSet> TelePhoneSets { get; set; }
        public static Dictionary<string, TelePhoneSet> TelePhoneSetsForApp { get; set; }
        public static Dictionary<string, List<DestinationTelePhoneNumber>> DestinationTelePhoneNumbers { get; set; }
        public static Dictionary<string, List<DestinationTelePhoneNumber>> DestinationTelePhoneNumbersForApp { get; set; }
        public static List<OverseasDestination> overseasDestinationList { get; set; }
        public static Dictionary<string, string> destinationIdToPeakSeason { get; set; }
        public static List<string> destinationAirportCodesForFlightTriangulation { get; set; }
        public static AlternateWebConfig AlternateWebConfigObj = new AlternateWebConfig();

        #region Declaring Static Variables for TT Online Bookability
        public static Dictionary<string, int> alphaPkIdToIntId { get; set; }
        public static Dictionary<string, int> destinationAlphaNumericIdToDestinationId { get; set; }
        public static Dictionary<string, string> parentChildDestinationHierarchy { get; set; }
        public static Dictionary<int, int> parentDestinations { get; set; }
        public static Dictionary<string, int> establishmentIdtoImageCountList { get; set; }
        public static string TTOnlineImgSourceURLFormat { get; set; }
        public static List<DestinationInfo> destinationInformationList { get; set; }
        public static List<AlphaDestinationTTRegion> alphaDestinationTTRegionMapping { get; set; }
        #endregion

        #region Declaring Static Variables For Routing Api

        public static Dictionary<string, List<string>> DestinationResortMap = new Dictionary<string, List<string>>();

        public static readonly Dictionary<string, List<int>> AirportCodeToMultipleDepartureIdsMapping = new Dictionary<string, List<int>>();

        public static Dictionary<string, List<string>> AirportGroupIdOrAirportIdToAirportsMapping = new Dictionary<string, List<string>>();

        public static Dictionary<string, int> AirportCodeToDepartureIdMapping = new Dictionary<string, int>();

        //IE Code
        public static Dictionary<string, int> AirportCodeToDepartureIdMappingMT = new Dictionary<string, int>();
        public static readonly Dictionary<string, List<int>> AirportCodeToMultipleDepartureIdsMappingMT = new Dictionary<string, List<int>>();
        public static Dictionary<string, List<string>> AirportGroupIdOrAirportIdToAirportsMappingMT = new Dictionary<string, List<string>>();


        public static string RoutingApiS3BucketName;

        public static string JsonFilesPathForRoutingDataByAirportCodeAndResortId;

        public static string FirstSetSuccessfullyWrittenPath;
        public static string FirstSetSuccessfullyWrittenFileName;

        public static string SecondSetSuccessfullyWrittenPath;
        public static string SecondSetSuccessfullyWrittenFileName;

        public static string ThirdSetSuccessfullyWrittenPath;
        public static string ThirdSetSuccessfullyWrittenFileName;

        public static string FourthSetSuccessfullyWrittenPath;
        public static string FourthSetSuccessfullyWrittenFileName;

        public static string JsonFilesPathForRoutingDataByAirportCode;
        public static string AirportCodesSuccessfullyWrittenPath;
        public static string AirportCodesSuccessfullyWrittenFileName;

        public static string JsonFilesPathForRoutingDataByResortId;
        public static string ResortIdsSuccessfullyWrittenPath;
        public static string ResortIdsSuccessfullyWrittenFileName;

        public static string RedisHostForRoutingApi;
        public static string RedisPortForRoutingApi;
        public static string RedisPasswordForRoutingApi;

        public static int RedisDbForResortIdAndAirportCode;
        public static int RedisDbForAirportCode;
        public static int RedisDbForResortId;

        public static string RedisConnectionStringForRoutingApi;
        #endregion

        protected void Application_Start(object sender, EventArgs e)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//For Paysafe API
            Utilities.LoadAlternateConfigFromS3(ConfigurationManager.AppSettings["AlternateConfigBucket"], ConfigurationManager.AppSettings["AlternateConfigFileName"]);
            string fileName = ConfigurationManager.AppSettings["GetHolidaySearchURL"];
            string mhidFile = ConfigurationManager.AppSettings["MHIDtoIMGCount"];
            ICAOAirlinesMapping = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ICAOAirlines"]));
            iffToImageCountResult = Utilities.ExecuteGetWebRequest(fileName);
            mhidToImageCountResult = Utilities.ExecuteGetWebRequest(mhidFile);
            string regionLabelMapping = ConfigurationManager.AppSettings["RegionLablelMapping"];
            labelRegionMapping = Utilities.ExecuteGetWebRequest(regionLabelMapping);
            iffToImageCountList = JsonConvert.DeserializeObject<Dictionary<string, int>>(iffToImageCountResult);
            mhidtoImageCountList = JsonConvert.DeserializeObject<Dictionary<string, int>>(mhidToImageCountResult);
            mhidToLandingPageResults = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["MHIDtoLandingPageURLs"]);
            mhidToLandingPageURL =  JsonConvert.DeserializeObject<Dictionary<string, string>>(mhidToLandingPageResults);
            labelRegionMappingList = JsonConvert.DeserializeObject<Dictionary<string, string>>(labelRegionMapping);
            ArtirixServiceUrlFmt = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"];
            DataServicesUrlRoot = ConfigurationManager.AppSettings["DataServicesUrlRoot"];//IE CODE
            minCountOfImage = Int32.Parse(ConfigurationManager.AppSettings["MinCountofImages"]);
            imgSourceURLFormat = ConfigurationManager.AppSettings["TeletextImgPath"];
            artxImgSourceURLFormat = ConfigurationManager.AppSettings["ArtirixImgPath"];
            MaxDegreeOfParallelism = Int32.Parse(ConfigurationManager.AppSettings["MaxDegreeOfParallelism"]);
            AllHotelURLMap = Utilities.FillAllHotelsURL();
            iffvsMasterId = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ifftomasterHotelIdMapping"]);
            iffvsMasterIdMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(iffvsMasterId);
            airportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportDictionary"]);
            airportCodes = JsonConvert.DeserializeObject<Dictionary<string, string>>(airportIds);
            reverseAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportToAirportRegion"]);
            reverseAirportCodes = JsonConvert.DeserializeObject<Dictionary<string, int>>(reverseAirportIds);
            parentAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ParentAirport"]);
            parentAirportCodes = JsonConvert.DeserializeObject<Dictionary<int, int>>(parentAirportIds);
            childAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ChildAirport"]);
            childAirportCodes = JsonConvert.DeserializeObject<Dictionary<int, string>>(childAirportIds);

            //IE Code
            airportIdsMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportDictionaryMT"]);
            airportCodesMT = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(airportIdsMT);

            reverseAirportIdsMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportToAirportRegionMT"]);
            reverseAirportCodesMT = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(reverseAirportIdsMT);

            parentAirportIdsMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ParentAirportMT"]);
            parentAirportCodesMT = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<int, int>>>(parentAirportIdsMT);

            //The purpose of Multiple parent lookup below is to allow "request-departureid-parent" to match "result departure id parent"
            // With single parent lookup (parentAirportCodesMT), the request and result parents may not match , which will result in
            //relevant offer documents being filtered out in ES search. This causes board basis information not to show up in UI
            //Multi-parent lookups are applied for only result departure ids in TTSSExecution class
            parentAirportIdsMultiMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ParentAirportMultiMT"]);
            parentAirportCodesMultiMT = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<int, List<int>>>>(parentAirportIdsMultiMT);


            childAirportIdsMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ChildAirportMT"]);
            childAirportCodesMT = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<int, string>>>(childAirportIdsMT);


            TelePhoneSetsMT = JsonConvert.DeserializeObject<Dictionary<string, TelePhoneSet>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["TelephoneSets-IE"]));
            TelePhoneSetsForAppMT = JsonConvert.DeserializeObject<Dictionary<string, TelePhoneSet>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["TelephoneSetsForApp-IE"]));
            var destinationTelephonesetsJSONMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["DestinationTelephonesets-IE"]);
            DestinationTelePhoneNumbersMT = Utilities.GetDestinationTelephoneNumberMappings(destinationTelephonesetsJSONMT, TelePhoneSetsMT);
            DestinationTelePhoneNumbersForAppMT = Utilities.GetDestinationTelephoneNumberMappings(destinationTelephonesetsJSONMT, TelePhoneSetsForAppMT);
            // End IE Code
            iffToMHIDMap = JsonConvert.DeserializeObject<Dictionary<string, MasterHotelImagesMap>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["IffCodeToMHID"]));
            MHIDtoMHNMap = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["MHIDtoMHN"]));
            MHIDtoRegIDMap = JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["MHIDtoRegID"]));
            RegIDtoRegNameMap = JsonConvert.DeserializeObject<Dictionary<int, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["RegIDtoRegName"]));
            GuidetoGuideBoundaries = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["GuidetoGuideBoundaries"]));
            TelePhoneSets = JsonConvert.DeserializeObject<Dictionary<string, TelePhoneSet>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["TelephoneSets"]));
            TelePhoneSetsForApp = JsonConvert.DeserializeObject<Dictionary<string, TelePhoneSet>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["TelephoneSetsForApp"]));
            var destinationTelephonesetsJSON = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["DestinationTelephonesets"]);
            DestinationTelePhoneNumbers = Utilities.GetDestinationTelephoneNumberMappings(destinationTelephonesetsJSON, TelePhoneSets);
            DestinationTelePhoneNumbersForApp = Utilities.GetDestinationTelephoneNumberMappings(destinationTelephonesetsJSON, TelePhoneSetsForApp);

            #region
            destinationAirportCodesForFlightTriangulation = JsonConvert.DeserializeObject<List<string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["InitialDestinationAirportsForFlightTriangulation"]));
            #endregion

            #region Initializing the Static variables related to Routing Api

            DestinationResortMap = Utilities.GenerateDestinationResortMap(labelRegionMappingList);

            RoutingApiS3BucketName = ConfigurationManager.AppSettings["RoutingApiS3BucketName"];

            JsonFilesPathForRoutingDataByAirportCodeAndResortId = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByResortIdAndAirportCode"];

            FirstSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["FirstSetSuccessfullyWrittenFileName"];
            FirstSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["FirstSetSuccessfullyWrittenPath"];

            SecondSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["SecondSetSuccessfullyWrittenFileName"];
            SecondSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["SecondSetSuccessfullyWrittenPath"];

            ThirdSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["ThirdSetSuccessfullyWrittenFileName"];
            ThirdSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["ThirdSetSuccessfullyWrittenPath"];

            FourthSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["FourthSetSuccessfullyWrittenFileName"];
            FourthSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["FourthSetSuccessfullyWrittenPath"];

            JsonFilesPathForRoutingDataByAirportCode = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByAirportCode"];
            AirportCodesSuccessfullyWrittenPath = ConfigurationManager.AppSettings["AirportCodesSuccessfullyWrittenPath"];
            AirportCodesSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["AirportCodesSuccessfullyWrittenFileName"];

            JsonFilesPathForRoutingDataByResortId = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByResortId"];
            ResortIdsSuccessfullyWrittenPath = ConfigurationManager.AppSettings["ResortIdsSuccessfullyWrittenPath"];
            ResortIdsSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["ResortIdsSuccessfullyWrittenFileName"];

            RedisHostForRoutingApi = ConfigurationManager.AppSettings["RedisHostForRoutingApi"];
            RedisPortForRoutingApi = ConfigurationManager.AppSettings["RedisPortForRoutingApi"];
            RedisPasswordForRoutingApi = ConfigurationManager.AppSettings["RedisPasswordForRoutingApi"];

            RedisConnectionStringForRoutingApi = Utilities.GetRedisConnectionStringForRoutingApi();

            RedisHostForRoutingApiIE = ConfigurationManager.AppSettings["RedisHostForRoutingApiIE"];
            RedisPortForRoutingApiIE = ConfigurationManager.AppSettings["RedisPortForRoutingApiIE"];
            RedisPasswordForRoutingApiIE = ConfigurationManager.AppSettings["RedisPasswordForRoutingApiIE"];

            RedisConnectionStringForRoutingApiIE = Utilities.GetRedisConnectionStringForRoutingApiIE();

            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForResortIdAndAirportCode"], out RedisDbForResortIdAndAirportCode);
            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForAirportCode"], out RedisDbForAirportCode);
            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForResortId"], out RedisDbForResortId);

            //System.Diagnostics.Debugger.Break();

            string airportsXmlData = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["RoutingAirports"]);

            List<AirportGroup> airportGroups = Utilities.GetAirportsFromXmlString(airportsXmlData);

            //IE Code
            string airportsXmlDataMT = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["RoutingAirportsMT"]);
            List<AirportGroup> airportGroupsMT = Utilities.GetAirportsFromXmlStringMT(airportsXmlDataMT);


            //if (airportGroups != null && airportGroups.Count > 0)
            //{
            //    AirportCodeToMultipleDepartureIdsMapping = Utilities.GenerateAirportCodeToDepartureIdMapping(airportGroups);
            //}

            #endregion

            #region Load the destination Overseas Json
            overseasDestinationList = Utilities.GetOverseasDestination();
            #endregion

            #region Destination to their peak Seasons data
            destinationIdToPeakSeason = Utilities.GetDestinationPeakSeasons();
            #endregion

            #region TT Online Bookability
            alphaPkIdToIntId = JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphapkdIdToIntId.json"));
            destinationAlphaNumericIdToDestinationId = JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/destinationAlphaNumericIdToDestinationId.json"));
            parentChildDestinationHierarchy = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/parent-child-destinationcode-hierarchy.json"));
            parentDestinations = JsonConvert.DeserializeObject<Dictionary<int, int>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/parentDestination.json"));
            establishmentIdtoImageCountList = JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["EstablishmentIdToImgCount"]));
            TTOnlineImgSourceURLFormat = ConfigurationManager.AppSettings["TeletextOnlineImgPath"];
            destinationInformationList = JsonConvert.DeserializeObject<List<DestinationInfo>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/parent-destinationCode-mapping.json"));
            alphaDestinationTTRegionMapping = JsonConvert.DeserializeObject<List<AlphaDestinationTTRegion>>(Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/AlphaDestinationTTRegionMapping.json"));
            #endregion
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST, PUT, DELETE");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}