﻿using Presentation.WCFRestService.Model.TTOnlineMinPriceTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System.ServiceModel.Web;
using Presentation.WCFRestService.Model.Enum;
using System.Configuration;
using Newtonsoft.Json;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using System.Text;
using Presentation.Web.WCFRestServices.TTOnlineBookability;
using Presentation.WCFRestService.Model.Misc;

namespace Presentation.Web.WCFRestServices.TTOnlineMinPriceTool
{
    public class TTOnlineMinPriceTool
    {
        public static TTOMinPriceView GetMinPriceViewResponse(AvailabilityOffers availabilityOffers)
        {
            TTOMinPriceView minPriceViewResponse = new TTOMinPriceView();

            if (availabilityOffers == null || availabilityOffers.offers == null)
            {
                throw new WebFaultException<string>("ESAvailability returned an empty response", System.Net.HttpStatusCode.InternalServerError);
            }
            try
            {
                foreach (AOffers offer in availabilityOffers.offers)
                {
                    minPriceViewResponse.minimumPrice = double.Parse(offer.rooms[0].roomPrices[0].totalPrice);
                    minPriceViewResponse.roomId = offer.rooms[0].roomPrices[0].roomId;
                    minPriceViewResponse.checkInDate = Convert.ToDateTime(offer.checkInDate).ToString("yyyy-MM-dd");
                    minPriceViewResponse.checkOutDate = Convert.ToDateTime(offer.checkOutDate).ToString("yyyy-MM-dd");
                    minPriceViewResponse.boardBasis = offer.rooms[0].roomPrices[0].boardType;
                    minPriceViewResponse.starRating = offer.rating;
                    minPriceViewResponse.establishmentCode = offer.hotelCode;
                    minPriceViewResponse.establishmentId = offer.hotelId;
                    minPriceViewResponse.establishmentName = offer.hotelName;
                    minPriceViewResponse.mhid = offer.mhid;
                    minPriceViewResponse.description = offer.description;
                    minPriceViewResponse.glat = offer.location.lat.ToString();
                    minPriceViewResponse.glong = offer.location.lon.ToString();
                    minPriceViewResponse.destinationCode = offer.destinationCode;
                    minPriceViewResponse.destinationName = offer.destinationName;
                    break;
                }
            }
            catch(Exception ex)
            {
                ErrorLogger.Log("Error at GetMinPriceViewResponse:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return minPriceViewResponse;
        }

        public static List<TTOMinPriceTool> DeleteQuery(string queryId, string fileUrl, string bucketName, string keyName)
        {
            List<TTOMinPriceTool> minPriceToolOffersList = null;
            try
            {
                var jsonObjectOutput = String.Empty;
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(fileUrl);

                minPriceToolOffersList = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(jsonObjectOutput);
                minPriceToolOffersList.RemoveAll(r => r.queryId.ToLower() == queryId.ToLower());
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];

                IAmazonS3 client;
                using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {
                    Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = bucketName,
                        Key = keyName
                    };
                    request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOffersList)));
                    PutObjectResponse response2 = client.PutObject(request);
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Object Not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                return null;
            }
            return minPriceToolOffersList;
        }

        public static List<TTOMinPriceTool> InsertHotelOnlyMPTOfferToS3(HotelOnlyMinPriceOffer saveUpdateMinPrice, string insertedByUser)
        {
            string outputUrl = string.Empty;
            outputUrl = ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"];
            string outputFileName = Utilities.ExecuteGetWebRequest(outputUrl);

            string esAvailabilitySearchRequest = B2CQueryBuilder.GetESAvailabilitySearchRequest(saveUpdateMinPrice);

            AvailabilityOffers availabilityOffers = JsonConvert.DeserializeObject<AvailabilityOffers>(Utilities.ExecutePostJsonWebRequest(ConfigurationManager.AppSettings["ESAvailabilitySearchUrl"], esAvailabilitySearchRequest));
            
            TTOMinPriceTool minPriceToolOffer = null;
            List<TTOMinPriceTool> minPriceToolOutput = new List<TTOMinPriceTool>();
            minPriceToolOutput = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(outputFileName);
            var sectionIdQueryIdChecker = minPriceToolOutput.FindAll(r => r.sectionId.ToLower() == saveUpdateMinPrice.sectionId.ToLower() && r.query.id.ToLower() == saveUpdateMinPrice.ID.ToLower());
            if (sectionIdQueryIdChecker.Count != 0)
            {
                throw new WebFaultException<string>("The combination of Section Id and ID exists ", System.Net.HttpStatusCode.BadRequest);
            }
            try
            {
                if (saveUpdateMinPrice.sectionId == null || saveUpdateMinPrice.ID == null)
                {
                    throw new WebFaultException<string>("Section Id or Id cannot be null \n Please check the fields again", System.Net.HttpStatusCode.BadRequest);
                }
                minPriceToolOffer = new TTOMinPriceTool();
                minPriceToolOffer.updatedBy = insertedByUser;
                minPriceToolOffer.roomId = saveUpdateMinPrice.roomId;
                minPriceToolOffer.query = new HotelOnlyMinPriceToolQuery();
                minPriceToolOffer.queryId = System.Guid.NewGuid().ToString();
                minPriceToolOffer.sectionId = saveUpdateMinPrice.sectionId;
                var dt = DateTime.UtcNow;
                minPriceToolOffer.minPrice = double.Parse(saveUpdateMinPrice.minPrice);
                minPriceToolOffer.lastUpdated = String.Format("{0:s}", dt);

                minPriceToolOffer.hotelLocation = Utilities.RemoveDiacritics(availabilityOffers.offers[0].destinationName);
                minPriceToolOffer.starRating = availabilityOffers.offers[0].rating.ToString();
                minPriceToolOffer.reviewCount = availabilityOffers.offers[0].taInfo.reviewCount.ToString();
                if (!string.IsNullOrWhiteSpace(availabilityOffers.offers[0].parentDestinationName))
                {
                    minPriceToolOffer.displayParentRegion = Utilities.RemoveDiacritics(availabilityOffers.offers[0].parentDestinationName);
                }
                else
                {
                    foreach(DestinationInfo destinationInfoObj in Global.destinationInformationList)
                    {
                        if(new Guid(destinationInfoObj.destinationCode) == availabilityOffers.offers[0].destinationCode)
                        {
                            minPriceToolOffer.displayParentRegion = destinationInfoObj.destinationName;
                        }
                    }
                }

                minPriceToolOffer.tripAdvisorRating = availabilityOffers.offers[0].taInfo.averageRating.ToString();
                minPriceToolOffer.image = availabilityOffers.offers[0].images[0].Replace("h460w840", "h205w286");

                minPriceToolOffer.displaySectionName = System.Web.HttpUtility.HtmlEncode(saveUpdateMinPrice.displaySectionName);
                if (saveUpdateMinPrice.boardType == "1")
                {
                    minPriceToolOffer.boardBasis = "Room Only";
                }
                else if (saveUpdateMinPrice.boardType == "2")
                {
                    minPriceToolOffer.boardBasis = "Self Catering";
                }
                else if (saveUpdateMinPrice.boardType == "3")
                {
                    minPriceToolOffer.boardBasis = "Bed & Breakfast";
                }
                else if (saveUpdateMinPrice.boardType == "4")
                {
                    minPriceToolOffer.boardBasis = "Half Board";
                }
                else if (saveUpdateMinPrice.boardType == "5")
                {
                    minPriceToolOffer.boardBasis = "Full Board";
                }
                else if (saveUpdateMinPrice.boardType == "6")
                {
                    minPriceToolOffer.boardBasis = "All Inclusive";
                }
                else
                {
                    minPriceToolOffer.boardBasis = "Any Board";
                }

                minPriceToolOffer.query.websiteUrl = createWebSiteUrl(saveUpdateMinPrice);
                minPriceToolOffer.query.checkInDate = saveUpdateMinPrice.checkInDate;
                minPriceToolOffer.query.adults = int.Parse(saveUpdateMinPrice.adults);
                minPriceToolOffer.query.boardType = saveUpdateMinPrice.boardType.Split(',').Select(int.Parse).ToList();
                minPriceToolOffer.query.children = int.Parse(saveUpdateMinPrice.children);
                minPriceToolOffer.query.description = Utilities.RemoveDiacritics(saveUpdateMinPrice.description);
                minPriceToolOffer.query.destinationCode = saveUpdateMinPrice.destinationCode;
                minPriceToolOffer.query.duration = int.Parse(saveUpdateMinPrice.duration);
                if (saveUpdateMinPrice.establishmentCode != null)
                {
                    minPriceToolOffer.query.establishmentCode = saveUpdateMinPrice.establishmentCode;
                }
                else
                {
                    minPriceToolOffer.query.establishmentCode = string.Empty;
                }
                minPriceToolOffer.query.dateOffset = int.Parse(saveUpdateMinPrice.dateOffSet);
                minPriceToolOffer.query.establishmentName = Utilities.RemoveDiacritics(saveUpdateMinPrice.establishmentName);
                minPriceToolOffer.query.id = saveUpdateMinPrice.ID;
                minPriceToolOffer.query.destinationName = Utilities.RemoveDiacritics(saveUpdateMinPrice.destinationName);
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMax))
                {
                    minPriceToolOffer.query.priceMax = int.Parse(saveUpdateMinPrice.priceMax);
                }
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMin))
                {
                    minPriceToolOffer.query.priceMin = int.Parse(saveUpdateMinPrice.priceMin);
                }
                minPriceToolOffer.query.ratings = saveUpdateMinPrice.starRatings.Split(',').Select(int.Parse).ToList();
                minPriceToolOffer.glat = saveUpdateMinPrice.glat;
                minPriceToolOffer.glong = saveUpdateMinPrice.glong;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at HotelOnly MinPrice offer insertion:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            if (!minPriceToolOutput.Contains(minPriceToolOffer))
            {
                minPriceToolOutput.Add(minPriceToolOffer);
            }
            return minPriceToolOutput;
        }

        private static string createWebSiteUrl(HotelOnlyMinPriceOffer saveUpdateMinPrice)
        {
            string websiteUrl = string.Empty;
            string preUrl = ConfigurationManager.AppSettings["preWebsiteUrl"];
            string postUrl = string.Empty;

            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.destinationName))
            {
                postUrl += "destination=" + saveUpdateMinPrice.destinationName.Replace(" ", "_");
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.checkInDate))
            {
                postUrl += "&checkindate=" + saveUpdateMinPrice.checkInDate;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.duration))
            {
                postUrl += "&duration=" + saveUpdateMinPrice.duration;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.establishmentCode))
            {
                postUrl += "&hotelcode=" + saveUpdateMinPrice.establishmentCode;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMin))
            {
                postUrl += "&minprice=" + saveUpdateMinPrice.priceMin;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMax))
            {
                postUrl += "&maxprice=" + saveUpdateMinPrice.priceMax;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.starRatings))
            {
                postUrl += "&rating=" + saveUpdateMinPrice.starRatings;
            }
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.boardType))
            {
                string boardDescription = string.Empty;

                if (saveUpdateMinPrice.boardType == "1")
                {
                    boardDescription = "Room Only";
                }
                else if (saveUpdateMinPrice.boardType == "2")
                {
                    boardDescription = "Self Catering";
                }
                else if (saveUpdateMinPrice.boardType == "3")
                {
                    boardDescription = "Bed N Breakfast";
                }
                else if (saveUpdateMinPrice.boardType == "4")
                {
                    boardDescription = "Half Board";
                }
                else if (saveUpdateMinPrice.boardType == "5")
                {
                    boardDescription = "Full Board";
                }
                else if (saveUpdateMinPrice.boardType == "6")
                {
                    boardDescription = "All Inclusive";
                }
                else
                {
                    boardDescription = "Any Board";
                }

                postUrl += "&board=" + boardDescription.Replace(" ", "_");
            }
            // single room
            postUrl += "&rooms=1"; 
            if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.adults) && !string.IsNullOrWhiteSpace(saveUpdateMinPrice.children))
            {
                postUrl += "&room1=" + saveUpdateMinPrice.adults + "," + saveUpdateMinPrice.children;
            }
            //if(!string.IsNullOrWhiteSpace(saveUpdateMinPrice.adults))
            //{
            //    postUrl += "&adults=" + saveUpdateMinPrice.adults;
            //}
            //if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.children))
            //{
            //    postUrl += "&children=" + saveUpdateMinPrice.children;
            //}

            websiteUrl = preUrl + postUrl;
            return websiteUrl;
        }

        public static List<TTOMinPriceTool> UpdateHotelOnlyMPTOfferToS3(HotelOnlyMinPriceOffer saveUpdateMinPrice, string updatedByUser)
        {
            List<TTOMinPriceTool> minPriceToolOutput = new List<TTOMinPriceTool>();
            try
            {
                string outputUrl = string.Empty;
                outputUrl = ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"];
                string outputFileName = Utilities.ExecuteGetWebRequest(outputUrl);

                string esAvailabilitySearchRequest = B2CQueryBuilder.GetESAvailabilitySearchRequest(saveUpdateMinPrice);

                AvailabilityOffers availabilityOffers = JsonConvert.DeserializeObject<AvailabilityOffers>(Utilities.ExecutePostJsonWebRequest(ConfigurationManager.AppSettings["ESAvailabilitySearchUrl"], esAvailabilitySearchRequest));
              
                minPriceToolOutput = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(outputFileName);
                var existingHotelOnlyMinPriceOffer = minPriceToolOutput.Find(r => r.sectionId.ToLower() == saveUpdateMinPrice.sectionId.ToLower() && r.query.id.ToLower() == saveUpdateMinPrice.ID.ToLower());

                if (existingHotelOnlyMinPriceOffer == null)
                {
                    throw new WebFaultException<string>("The combination of Section Id and ID not found ", System.Net.HttpStatusCode.BadRequest);
                }

                var dt = DateTime.Now;
                existingHotelOnlyMinPriceOffer.updatedBy = updatedByUser;
                existingHotelOnlyMinPriceOffer.minPrice = double.Parse(saveUpdateMinPrice.minPrice);
                existingHotelOnlyMinPriceOffer.lastUpdated = String.Format("{0:s}", dt);
                existingHotelOnlyMinPriceOffer.sectionId = saveUpdateMinPrice.sectionId;
                existingHotelOnlyMinPriceOffer.hotelLocation = Utilities.RemoveDiacritics(availabilityOffers.offers[0].destinationName);
                existingHotelOnlyMinPriceOffer.starRating = availabilityOffers.offers[0].rating.ToString();
                existingHotelOnlyMinPriceOffer.reviewCount = availabilityOffers.offers[0].taInfo.reviewCount.ToString();

                existingHotelOnlyMinPriceOffer.displaySectionName = System.Web.HttpUtility.HtmlEncode(saveUpdateMinPrice.displaySectionName);
                if (saveUpdateMinPrice.boardType == "1")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Room Only";
                }
                else if (saveUpdateMinPrice.boardType == "2")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Self Catering";
                }
                else if (saveUpdateMinPrice.boardType == "3")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Bed & Breakfast";
                }
                else if (saveUpdateMinPrice.boardType == "4")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Half Board";
                }
                else if (saveUpdateMinPrice.boardType == "5")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Full Board";
                }
                else if (saveUpdateMinPrice.boardType == "6")
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "All Inclusive";
                }
                else
                {
                    existingHotelOnlyMinPriceOffer.boardBasis = "Any Board";
                }

                if (availabilityOffers.offers[0].parentDestinationName != null)
                {
                    existingHotelOnlyMinPriceOffer.displayParentRegion = Utilities.RemoveDiacritics(availabilityOffers.offers[0].parentDestinationName);
                }
                else
                {
                    foreach (DestinationInfo destinationInfoObj in Global.destinationInformationList)
                    {
                        if (new Guid(destinationInfoObj.destinationCode) == availabilityOffers.offers[0].destinationCode)
                        {
                            existingHotelOnlyMinPriceOffer.displayParentRegion = destinationInfoObj.destinationName;
                        }
                    }
                }

                existingHotelOnlyMinPriceOffer.tripAdvisorRating = availabilityOffers.offers[0].taInfo.averageRating.ToString();
                existingHotelOnlyMinPriceOffer.image = availabilityOffers.offers[0].images[0].Replace("h460w840", "h205w286");

                existingHotelOnlyMinPriceOffer.query.websiteUrl = createWebSiteUrl(saveUpdateMinPrice);
                existingHotelOnlyMinPriceOffer.query.adults = int.Parse(saveUpdateMinPrice.adults);
                existingHotelOnlyMinPriceOffer.query.checkInDate = saveUpdateMinPrice.checkInDate;
                existingHotelOnlyMinPriceOffer.query.boardType = saveUpdateMinPrice.boardType.Split(',').Select(int.Parse).ToList();
                existingHotelOnlyMinPriceOffer.query.children = int.Parse(saveUpdateMinPrice.children);
                existingHotelOnlyMinPriceOffer.query.dateOffset = int.Parse(saveUpdateMinPrice.dateOffSet);

                existingHotelOnlyMinPriceOffer.query.description = Utilities.RemoveDiacritics(saveUpdateMinPrice.description);
                existingHotelOnlyMinPriceOffer.query.destinationCode = saveUpdateMinPrice.destinationCode;
                existingHotelOnlyMinPriceOffer.query.duration = int.Parse(saveUpdateMinPrice.duration);
                existingHotelOnlyMinPriceOffer.query.id = saveUpdateMinPrice.ID;
                if (saveUpdateMinPrice.establishmentCode != null)
                {
                    existingHotelOnlyMinPriceOffer.query.establishmentCode = saveUpdateMinPrice.establishmentCode;
                }
                else
                {
                    existingHotelOnlyMinPriceOffer.query.establishmentCode = string.Empty;
                }
                existingHotelOnlyMinPriceOffer.query.establishmentName = Utilities.RemoveDiacritics(saveUpdateMinPrice.establishmentName);
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMax))
                {
                    existingHotelOnlyMinPriceOffer.query.priceMax = int.Parse(saveUpdateMinPrice.priceMax);
                }
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMin))
                {
                    existingHotelOnlyMinPriceOffer.query.priceMin = int.Parse(saveUpdateMinPrice.priceMin);
                }
                existingHotelOnlyMinPriceOffer.query.ratings = saveUpdateMinPrice.starRatings.Split(',').Select(int.Parse).ToList();
                existingHotelOnlyMinPriceOffer.query.destinationName = Utilities.RemoveDiacritics(saveUpdateMinPrice.destinationName);
                existingHotelOnlyMinPriceOffer.glat = saveUpdateMinPrice.glat;
                existingHotelOnlyMinPriceOffer.glong = saveUpdateMinPrice.glong;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at Hotel Only MinPrice offer updation:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return minPriceToolOutput;
        }

        public static List<TTOMinPriceTool> MerchandizingOffers(string sectionIds)
        {
            List<TTOMinPriceTool> minPriceToolOutputObject = new List<TTOMinPriceTool>();
            List<TTOMinPriceTool> minPriceToolSectionIdObject = new List<TTOMinPriceTool>();
            try
            {
                var jsonObjectOutput = String.Empty;
                string outputUrl = string.Empty;
                
                outputUrl = ConfigurationManager.AppSettings["HotelOnlyMinPriceOutput"];
                
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
                minPriceToolOutputObject = JsonConvert.DeserializeObject<List<TTOMinPriceTool>>(jsonObjectOutput);
                if (string.IsNullOrEmpty(sectionIds))
                {
                    return minPriceToolOutputObject;
                }
                var sectionSplit = sectionIds.Split(',');
                for (int i = 0; i < sectionSplit.Length; i++)
                {
                    minPriceToolSectionIdObject.AddRange(minPriceToolOutputObject.FindAll(r => r.sectionId.ToLower() == sectionSplit[i].ToLower()));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("SectionId not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }

            return minPriceToolSectionIdObject;

        }
    }
}