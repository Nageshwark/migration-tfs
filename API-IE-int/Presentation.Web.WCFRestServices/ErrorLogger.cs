﻿using Presentation.WCFRestService.Model.Enum;
using System;
using System.IO;
using System.Web;

namespace Presentation.Web.WCFRestServices
{
    public static class ErrorLogger
    {
        public static void Log(string message, LogLevel logLevel)
        {
            string LogPath = HttpRuntime.AppDomainAppPath + @"Log\";
            string filename = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

            if (logLevel == LogLevel.Information)
            {
                filename = "InformationLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearch)
            {
                filename = "Elasticsearch_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchDiversity)
            {
                filename = "ElasticSearchDiversity_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.MissingElasticSearchMappings)
            {
                filename = "MissingElasticSearchMappings_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.RoutingApi)
            {
                filename = "RoutingApi_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.RoutingApiError)
            {
                filename = "RoutingApiError_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.GetRoutingApiError)
            {
                filename = "GetRoutingApiError_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.CustomerReviews)
            {
                filename = "CustomerReviews_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchAvailability)
            {
                filename = "ElasticSearchAvailability_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if(logLevel == LogLevel.PaySafeStandaloneCredit)
            {
                filename = "PaySafeStandaloneCredit_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.LogCheapBoardBasis)
            {
                filename = "LogCheapBoardBasis_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchAvailabilityTesting)
            {
                filename = "ElasticSearchAvailabilityTesting_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchMissingAlphaPkIdToIntIdMapping)
            {
                filename = "ElasticSearchMissingAlphaPkIdToIntIdMapping_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }

            string filepath = LogPath + filename;
            try
            {
                if (File.Exists(filepath))
                {
                    using (StreamWriter writer = new StreamWriter(filepath, true))
                    {
                        writer.WriteLine(DateTime.Now + "=> " + message);
                    }
                }
                else
                {
                    StreamWriter writer = File.CreateText(filepath);
                    writer.WriteLine(DateTime.Now + "=> " + message);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message, LogLevel.Error);
            }
        }

    }
}