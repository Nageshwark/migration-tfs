﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CExecuteQuery
    {
        public static B2BAvailabilitySearchRequestAvailability CheckRequestInAvailabilityIndice(AvailabilitySearchRequest availabilityRequest)
        {
            B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = new B2BAvailabilitySearchRequestAvailability();
            
            string searchRequestQuery = B2CQueryBuilder.GetAvailabilityRequestSearchQuery(availabilityRequest);
            string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "availability" + "/_search", searchRequestQuery);
            SearchResponseParams searchRequestResponse = new SearchResponseParams();
            try
            {
                searchRequestResponse = JsonConvert.DeserializeObject<SearchResponseParams>(searchResponse);
                if (searchRequestResponse.hits.total != 0)
                {
                    b2bAvailabilitySearchRequestAvailability.availabilityOffersCount = searchRequestResponse.hits.hits[0]._source.availabilityOffersCount;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error Deserializing the es request response.Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                //errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace));
            }
            return b2bAvailabilitySearchRequestAvailability;
        }
        public static B2BAvailabilitySearchRequestAvailability CheckRequestInAvailabilityIndiceV2(AvailabilitySearchRequest availabilityRequest)
        {
            B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability = new B2BAvailabilitySearchRequestAvailability();

            string searchRequestQuery = B2CQueryBuilder.GetAvailabilityRequestSearchQueryV2(availabilityRequest);
            string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "availability" + "/_search", searchRequestQuery);
            SearchResponseParams searchRequestResponse = new SearchResponseParams();
            try
            {
                searchRequestResponse = JsonConvert.DeserializeObject<SearchResponseParams>(searchResponse);
                if (searchRequestResponse.hits.total != 0)
                {
                    b2bAvailabilitySearchRequestAvailability.availabilityOffersCount = searchRequestResponse.hits.hits[0]._source.availabilityOffersCount;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error Deserializing the es request response.Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                //errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace));
            }
            return b2bAvailabilitySearchRequestAvailability;
        }

        public static B2BAvailabilityResultsParams HitB2CAvailability(AvailabilitySearchRequest availabilityRequest)
        {
            // Make a B2b Availability Request and insert into ES
            //string b2bAvailabilitySearchQuery = B2BQueryBuilder.GetB2bAvailabilitySearchQuery(availabilityRequest);
            //return B2BAvailabilityRequestExecution.GetAvailabilityResponse(b2bAvailabilitySearchQuery, availabilityRequest);
            //string b2cAvailabilitySearchQuery = B2CQueryBuilder.GetB2CAvailabilitySearchQuery(availabilityRequest);
            string b2cAvailabilitySearchQuery = B2CQueryBuilder.GetB2CAvailabilitySearchQueryV2(availabilityRequest);
            return B2CAvailabilityRequestExecution.GetAvailabilityResponse(b2cAvailabilitySearchQuery, availabilityRequest);
        }
    }
}