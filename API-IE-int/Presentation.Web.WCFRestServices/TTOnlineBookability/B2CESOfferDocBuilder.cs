﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CESOfferDocBuilder
    {
        public static B2BAvailabilityResultsParams B2CESOfferBuilder(B2CAvailabilityResponse availabilityResultSet, AvailabilitySearchRequest availabilityRequest)
        {
            System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
            offersDecorationTimer.Start();
            B2BAvailabilityResultsParams b2bAvailabilityParams = new B2BAvailabilityResultsParams();
            b2bAvailabilityParams.isBulkInsertionOnRetry = true;
            ErrorLogger.Log("Building the ES Query for bulk insert into ES.", LogLevel.ElasticSearchAvailability);
            /* Load the Alpha pkId to AccomId Dictionary - start here */
            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId;

            /* Load the Alpha pkId to AccomId Dictionary - start here */

            /* Load AlphaId to MHID Mapping - Start Here */
            var alphaToMhidMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphaHoCoMapping.json");
            Dictionary<string, int> alphaToMhidMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaToMhidMappingResponse);
            ErrorLogger.Log("Downloaded alphaToMhidMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load AlphaId to MHID Mapping - End Here */

            /* Load MHID to Rank Mapping - Start Here */
            var mhidToRankMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/MHIDRankMapping.json");
            Dictionary<string, int> mhidToRankMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(mhidToRankMappingResponse);
            ErrorLogger.Log("Downloaded mhidToRankMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load MHID to Rank Mapping - Ends Here */

            /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */
            Dictionary<string, int> destinationMapping = Global.destinationAlphaNumericIdToDestinationId;
            /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */

            /* Load Alpha Destination to TT Region Mapping - Start Here */
            List<AlphaDestinationTTRegion> alphaDestinationTTRegionMapping = Global.alphaDestinationTTRegionMapping;
            /* Load Alpha Destination to TT Region Mapping - End Here */

            /* Dictionary for storing the Hotel Static Content against hotelId - Starts Here */
            Dictionary<int, StaticMHResponse> mhStaticInfo = new Dictionary<int, StaticMHResponse>();

            /* Dictionary for storing the Hotel Static Content against hotelId - Ends Here */

            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            int availabilityOfferCount = 0;
            int uniqueHotelsInserted = 0;
            int offersInserted = 0;
            double offerDecorationTimeElapsed = 0.0;
            string offersESBulkInsertionQuery = string.Empty;
            string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
            int bulkInsertionRetryCount = 1;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);

            StaticEstablishmentParams staticResponseParms = new StaticEstablishmentParams();
            Dictionary<int, StaticEstablishmentParams> staticInfo = new Dictionary<int, StaticEstablishmentParams>();

            StaticResponseParms ttextStaticResponseParms = new StaticResponseParms();
            Dictionary<int, StaticResponseParms> ttextStaticInfo = new Dictionary<int, StaticResponseParms>();

            List<Guid> distinctHotelCodes = (from b2cAvailabilityResult in availabilityResultSet.AvailabilityResults select b2cAvailabilityResult.EstablishmentId).Distinct().ToList();
            availabilityOfferCount = availabilityResultSet.AvailabilityTotalResultsCount;
            List<string> distinctHotelIds = new List<string>();
            List<string> redisStaticInfo = null;
            List<string> ttextHotelsAvailable = new List<string>();

            foreach (Guid distinctHotelCode in distinctHotelCodes)
            {
                if (alphaPkIdToIntId.ContainsKey(distinctHotelCode.ToString("D").ToUpper()))
                {
                    distinctHotelIds.Add(alphaPkIdToIntId[distinctHotelCode.ToString("D").ToUpper()].ToString());
                }
            }

            // Check how many hotels are present in the TText Side
            foreach (Guid distinctHotelCode in distinctHotelCodes)
            {
                if (alphaToMhidMapping.ContainsKey(distinctHotelCode.ToString("D")))
                {
                    ttextHotelsAvailable.Add(alphaToMhidMapping[distinctHotelCode.ToString("D")].ToString());
                }
            }

            ErrorLogger.Log("Fetching the Static Content from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);
            // Single Content Hub Content
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                     ConfigurationManager.AppSettings["RedisPort"]))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
                    redisStaticInfo = redisClient.GetValues(distinctHotelIds);
                    // offersDecorated = redisStaticInfo.Count;
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo[mhidCount]);
                        if (!staticInfo.ContainsKey(staticResponseParms.establishmentIntId))
                        {
                            staticInfo.Add(staticResponseParms.establishmentIntId, staticResponseParms);
                        }
                    }
                }
            }

            // TText Static Content
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                    redisStaticInfo = redisClient.GetValues(ttextHotelsAvailable);
                    // offersDecorated = redisStaticInfo.Count;
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        ttextStaticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                        if (!ttextStaticInfo.ContainsKey(ttextStaticResponseParms.mhid))
                        {
                            ttextStaticInfo.Add(ttextStaticResponseParms.mhid, ttextStaticResponseParms);
                        }
                    }
                }
            }
            ErrorLogger.Log("DONE ::: Fetching the Static Contetn from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);

            /* offer builder */
            try
            {
                if (availabilityResultSet != null && availabilityResultSet.AvailabilityResults.Count > 0)
                {
                    foreach (AvailabilityResult availabilityresult in availabilityResultSet.AvailabilityResults)
                    {
                        if (alphaPkIdToIntId.ContainsKey(availabilityresult.EstablishmentId.ToString("D").ToUpper()) && staticInfo.ContainsKey(alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()]))
                        {
                            uniqueHotelsInserted++;
                            List<_Room> rooms = new List<_Room>();
                            rooms = availabilityresult.Rooms;
                            Dictionary<int, RoomConfiguration> roomConfigDict = new Dictionary<int, RoomConfiguration>();

                            /* create a room occupancy dict */
                            foreach (RoomOccupancy roomOccupancy in availabilityresult.RoomOccupancies)
                            {
                                RoomConfiguration currentRoomConfig = new RoomConfiguration();
                                currentRoomConfig.adults = roomOccupancy.AdultsCount;
                                currentRoomConfig.childrenAndInfants = roomOccupancy.ChildrenAndInfantsCount;

                                roomConfigDict.Add(roomOccupancy.RoomNumber, currentRoomConfig);
                            }

                            foreach (_Room room in rooms)
                            {
                                B2COffersDocument offers = new B2COffersDocument();
                                staticResponseParms = staticInfo[alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()]];

                                RoomConfiguration roomConfig = roomConfigDict[room.RoomNumber];
                                offers.adults = roomConfig.adults;
                                offers.children = roomConfig.childrenAndInfants;
                                offers.guestInfoSearchRequest = availabilityRequest.guestInfoSearchRequest;
                                offers.roomTypeSearch = availabilityRequest.roomSearchType;

                                offers.availabilityId = availabilityResultSet.AvailabilityId;
                                offers.roomId = room.RoomIdAlpha2;
                                offers.roomDescription = room.RoomDescription;
                                offers.roomNumber = room.RoomNumber;
                                offers.boardType = room.BoardType;
                                offers.boardDescription = room.BoardDescription;
                                offers.providerEdiCode = room.ProviderEdiCode;
                                offers.providerEstablishmentCode = room.ProviderEstablishmentCode;
                                offers.providerName = room.ProviderName;
                                offers.providerPrice = room.ProviderPrice.Amount;
                                offers.providerCurrencyCode = room.ProviderPrice.CurrencyCode;
                                offers.providerRoomCode = room.ProviderRoomCode;
                                offers.totalPrice = room.Price.Amount;
                                offers.highestPrice = (room.HighestPrice != null) ? room.HighestPrice.Amount : 0;
                                offers.currencyCode = room.Price.CurrencyCode;
                                offers.IsActiveAccommodationAdminFee = room.IsActiveAccommodationAdminFee;
                                offers.isBidingRate = room.IsBindingRate;
                                offers.IsCardChargeApplicable = room.IsCardChargeApplicable;
                                offers.IsDuplicateRate = room.IsDuplicateRate;
                                offers.IsEligibleForDeposit = room.IsEligibleForDeposit;
                                offers.IsNonRefundable = room.IsNonRefundable;
                                offers.IsOpaqueRate = room.IsOpaqueRate;


                                /* Hotel Static Info from Single Content Hub */
                                offers.hotelCode = availabilityresult.EstablishmentId;
                                offers.hotelId = (alphaPkIdToIntId.ContainsKey(availabilityresult.EstablishmentId.ToString("D").ToUpper()) ? alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()] : 0);
                                offers.hotelSEOFriendlyName = availabilityresult.EstablishmentSeoFriendlyName;
                                offers.defaultAirportCode = staticResponseParms.defaultAirportCode;
                                offers.checkInDate = availabilityresult.CheckInDate.ToString("yyyy-MM-dd");
                                offers.checkOutDate = availabilityresult.CheckOutDate.ToString("yyyy-MM-dd");
                                offers.destinationCode = availabilityresult.DestinationId;

                                var alphaDestinationRegionMapping = alphaDestinationTTRegionMapping.Find(r => r.destinationCode == offers.destinationCode);
                                if (alphaDestinationRegionMapping != null)
                                {
                                    offers.TTRegionId = alphaDestinationRegionMapping.regionId;
                                }
                                else
                                {
                                    offers.TTRegionId = 0;
                                }


                                //offers.destinationId = staticResponseParms.destinationId;
                                offers.destinationName = availabilityresult.DestinationName;
                                offers.destinationSEOFriendlyName = availabilityresult.DestinationSeoFriendlyName;

                                if (offers.destinationCode != null)
                                {
                                    var destinationInfo = Global.destinationInformationList.Find(r => r.destinationCode == offers.destinationCode.ToString().ToUpper());
                                    if (destinationInfo != null)
                                    {
                                        offers.parentDestinationCode = destinationInfo.parentDestinationCode;
                                        offers.parentDestinationName = destinationInfo.parentDestinationName;
                                    }
                                }
                                offers.ISOCountryCode = staticResponseParms.ISOCountryCode;
                                offers.countryName = staticResponseParms.countryName;
                                offers.rank = getRank(alphaToMhidMapping, mhidToRankMapping, availabilityresult.EstablishmentId);
                                offers.mhid = alphaToMhidMapping.ContainsKey(availabilityresult.EstablishmentId.ToString("D")) ? alphaToMhidMapping[availabilityresult.EstablishmentId.ToString("D")] : 0;
                                offers.starRating = availabilityresult.EstablishmentStarRating;
                                offers.hotelFacilities = new List<object>();
                                offers.roomFacilities = new List<object>();
                                offers.hotelGeneralInfo = new List<object>();

                                // hotel general Info
                                if (staticResponseParms.establishmentFacilities.Count > 0 && staticResponseParms.establishmentFacilities.ContainsKey("General Info"))
                                {
                                    offers.hotelGeneralInfo.AddRange(staticResponseParms.establishmentFacilities["General Info"]);
                                }

                                if (offers.mhid > 0 && ttextStaticInfo.ContainsKey(offers.mhid))
                                {
                                    // Show TText Static Content
                                    ttextStaticResponseParms = ttextStaticInfo[offers.mhid];
                                    offers.hotelName = ttextStaticResponseParms.name;
                                    offers.tripAdvisorId = ttextStaticResponseParms.tripAdvisorId;
                                    offers.averageRating = ttextStaticResponseParms.averageRating;
                                    offers.reviewCount = ttextStaticResponseParms.reviewCount;
                                    offers.lat = ttextStaticResponseParms.lat;
                                    offers.lon = ttextStaticResponseParms.lng;

                                    // hotel facilities
                                    if (ttextStaticResponseParms.features.Count > 0)
                                    {
                                        offers.hotelFacilities = ttextStaticResponseParms.features;
                                    }
                                    else
                                    {
                                        foreach (KeyValuePair<string, List<string>> hotelFacility in staticResponseParms.establishmentFacilities)
                                        {
                                            if (hotelFacility.Key != "General Info")
                                            {
                                                offers.hotelFacilities.AddRange(hotelFacility.Value);
                                            }

                                        }
                                    }

                                    // room facilities
                                    foreach (KeyValuePair<string, List<string>> roomFacility in staticResponseParms.roomFacilities)
                                    {
                                        offers.roomFacilities.AddRange(roomFacility.Value);
                                    }
                                }
                                else
                                {
                                    // Show Alpha Static Content
                                    offers.hotelName = staticResponseParms.establishmentName;
                                    offers.tripAdvisorId = staticResponseParms.tripAdvisorId;
                                    offers.averageRating = availabilityresult.EstablishmentTripAdvisorAverageScore;
                                    offers.reviewCount = availabilityresult.EstablishmentTripAdvisorReviewCount;
                                    offers.lat = staticResponseParms.lat.ToString();
                                    offers.lon = staticResponseParms.lon.ToString();

                                    // hotel facilities
                                    foreach (KeyValuePair<string, List<string>> hotelFacility in staticResponseParms.establishmentFacilities)
                                    {
                                        if (hotelFacility.Key != "General Info")
                                        {
                                            offers.hotelFacilities.AddRange(hotelFacility.Value);
                                        }

                                    }

                                    // room facilities
                                    foreach (KeyValuePair<string, List<string>> roomFacility in staticResponseParms.roomFacilities)
                                    {
                                        offers.roomFacilities.AddRange(roomFacility.Value);
                                    }
                                }

                                //////////// hotel General Info
                                //////////foreach (Facility facility in staticResponseParms.establishmentFacilities)
                                //////////{
                                //////////    if (facility.categoryName == "General Info")
                                //////////    {
                                //////////        offers.hotelGeneralInfo = facility.skills.ToString().Split(',').ToList().ConvertAll(o => (object)o);
                                //////////    }
                                //////////}

                                //////if (offers.mhid > 0 && ttextStaticInfo.ContainsKey(offers.mhid))
                                //////{
                                //////    // Show TText Static Content
                                //////    ttextStaticResponseParms = ttextStaticInfo[offers.mhid];
                                //////    offers.hotelName = ttextStaticResponseParms.name;
                                //////    offers.tripAdvisorId = ttextStaticResponseParms.tripAdvisorId;
                                //////    offers.averageRating = ttextStaticResponseParms.averageRating;
                                //////    offers.reviewCount = ttextStaticResponseParms.reviewCount;
                                //////    offers.lat = ttextStaticResponseParms.lat;
                                //////    offers.lon = ttextStaticResponseParms.lng;
                                //////    //offers.hotelFacilities = new List<object>();
                                //////    //offers.roomFacilities = new List<object>();
                                //////    //offers.hotelGeneralInfo = new List<object>();

                                //////    //offers.hotelGeneralInfo = staticResponseParms.establishmentFacilities.Where(r => r.categoryName == "General Info").Select(r => r.skills).ToString().Split(',').ToList().ConvertAll(o => (object)o);

                                //////    // hotel facilities
                                //////    if(ttextStaticResponseParms.features.Count > 0)
                                //////    {
                                //////        offers.hotelFacilities = ttextStaticResponseParms.features;
                                //////    }
                                //////    else
                                //////    {
                                //////        foreach (Facility facility in staticResponseParms.establishmentFacilities)
                                //////        {
                                //////            List<object> hotelFacilities = facility.skills.ToString().Split(',').ToList().ConvertAll(o => (object)o);
                                //////            offers.hotelFacilities.AddRange(hotelFacilities);
                                //////        }
                                //////    }

                                //////    // room facilities
                                //////    foreach (Facility facility in staticResponseParms.roomFacilities)
                                //////    {
                                //////        List<object> roomFacilities = facility.skills.ToString().Split(',').ToList().ConvertAll(o => (object)o);
                                //////        offers.roomFacilities.AddRange(roomFacilities);
                                //////    }

                                //////    //offers.hotelFacilities = (ttextStaticResponseParms.features.Count > 0) ? ttextStaticResponseParms.features : staticResponseParms.establishmentFacilities;
                                //////    //offers.roomFacilities = staticResponseParms.roomFacilities.Select(r => r.skills).ToString().Split(',').ToList();
                                //////}
                                //////else
                                //////{
                                //////    // Show Alpha Static Content
                                //////    offers.hotelName = staticResponseParms.establishmentName;
                                //////    offers.tripAdvisorId = staticResponseParms.tripAdvisorId;
                                //////    offers.averageRating = availabilityresult.EstablishmentTripAdvisorAverageScore;
                                //////    offers.reviewCount = availabilityresult.EstablishmentTripAdvisorReviewCount;
                                //////    offers.lat = staticResponseParms.lat.ToString();
                                //////    offers.lon = staticResponseParms.lon.ToString();
                                //////    //offers.hotelFacilities = staticResponseParms.establishmentFacilities;
                                //////    //offers.roomFacilities = staticResponseParms.roomFacilities;

                                //////    // hotel facilities
                                //////    foreach (Facility facility in staticResponseParms.establishmentFacilities)
                                //////    {
                                //////        List<object> hotelFacilities = facility.skills.ToString().Split(',').ToList().ConvertAll(o => (object)o);
                                //////        offers.hotelFacilities.AddRange(hotelFacilities);
                                //////    }

                                //////    // room facilities
                                //////    foreach (Facility facility in staticResponseParms.roomFacilities)
                                //////    {
                                //////        List<object> roomFacilities = facility.skills.ToString().Split(',').ToList().ConvertAll(o => (object)o);
                                //////        offers.roomFacilities.AddRange(roomFacilities);
                                //////    }
                                //////}
                                offers.timeStamp = timeStamp;

                                searchParams.Append("{\"index\" : {\"_index\" : \"" + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "\", \"_type\" : \"availability\" }}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + "\n");
                                offersInserted++;
                            }
                        }
                        else
                        {
                            // Alpha pkId to intId mapping is missing
                            ErrorLogger.Log("Following Establishment Id is missing the int Id Mapping : " + availabilityresult.EstablishmentId, LogLevel.ElasticSearchMissingAlphaPkIdToIntIdMapping);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Exception while decorating offers for insertion into ES. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
            }


            offersDecorationTimer.Stop();
            offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;

            offersESBulkInsertionQuery = searchParams.ToString();
            ErrorLogger.Log(string.Format("Number of Offers to be inserted in the Offers indice : {0}", offersInserted), LogLevel.ElasticSearchAvailability);
            ErrorLogger.Log("Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);

            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            if (distinctHotelIds.Count != 0)
            {
                while (b2bAvailabilityParams.isBulkInsertionOnRetry && bulkInsertionRetryCount >= 0)
                {
                    try
                    {
                        string bulkOffers = offersESBulkInsertionQuery;
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "_bulk");
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(bulkOffers);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        if (httpResponse != null)
                        {
                            httpResponse.Close();
                            httpResponse.Dispose();
                        }
                        b2bAvailabilityParams.isBulkInsertionOnRetry = false;
                    }
                    catch (WebException webexp)
                    {
                        ErrorLogger.Log("Web exception occured while Bulk call. Message - " + webexp.Message + ", Stack Trace - " + webexp.StackTrace, LogLevel.ElasticSearchAvailability);
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Web exception occured while Bulk call. Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                }

                // Make facets call to check if more than 80% of the offers are getting fetched or not
                string facetsResponse = string.Empty;
                AvailabilityFacetsRequestResponse facetsRequestResponse = new AvailabilityFacetsRequestResponse();
                string facetsQuery = B2CQueryBuilder.GetFacetsQuery(availabilityRequest);
                try
                {
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
                    while (facetsTrials > 0)
                    {
                        if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                        {
                            break;
                        }
                        else
                        {
                            Thread.Sleep(500);
                            facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
                            facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
                            facetsTrials--;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log((string.Format("Exception occured while checking the facets. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace)), LogLevel.ElasticSearchAvailability);
                }
            }
            offersIndexingTimer.Stop();

            ErrorLogger.Log("DONE ::: Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);

            b2bAvailabilityParams.availabilityOffersCount = availabilityOfferCount;
            b2bAvailabilityParams.uniqueHotelsInserted = uniqueHotelsInserted;
            b2bAvailabilityParams.offersInserted = offersInserted;
            b2bAvailabilityParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            b2bAvailabilityParams.decoratingTime = offerDecorationTimeElapsed;
            b2bAvailabilityParams.facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]) - facetsTrials;

            return b2bAvailabilityParams;
        }

        ////public static B2BAvailabilityResultsParams B2CESOfferBuilderV2(B2CAvailabilityResponse availabilityResultSet, AvailabilitySearchRequest availabilityRequest)
        ////{
        ////    System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
        ////    offersDecorationTimer.Start();
        ////    B2BAvailabilityResultsParams b2bAvailabilityParams = new B2BAvailabilityResultsParams();
        ////    b2bAvailabilityParams.isBulkInsertionOnRetry = true;
        ////    ErrorLogger.Log("Building the ES Query for bulk insert into ES.", LogLevel.ElasticSearchAvailability);
        ////    /* Load the Alpha pkId to AccomId Dictionary - start here */
        ////    //var alphaPkIdToIntIdResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphapkdIdToIntId.json");
        ////    Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId; //JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaPkIdToIntIdResponse);

        ////    /* Load the Alpha pkId to AccomId Dictionary - start here */

        ////    /* Load AlphaId to MHID Mapping - Start Here */
        ////    var alphaToMhidMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphaHoCoMapping.json");
        ////    Dictionary<string, int> alphaToMhidMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaToMhidMappingResponse);
        ////    ErrorLogger.Log("Downloaded alphaToMhidMapping file.", LogLevel.ElasticSearchAvailability);
        ////    /* Load AlphaId to MHID Mapping - End Here */

        ////    /* Load MHID to Rank Mapping - Start Here */
        ////    var mhidToRankMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/MHIDRankMapping.json");
        ////    Dictionary<string, int> mhidToRankMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(mhidToRankMappingResponse);
        ////    ErrorLogger.Log("Downloaded mhidToRankMapping file.", LogLevel.ElasticSearchAvailability);
        ////    /* Load MHID to Rank Mapping - Ends Here */

        ////    /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */
        ////    Dictionary<string, int> destinationMapping = Global.destinationAlphaNumericIdToDestinationId;
        ////    /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */

        ////    /* Dictionary for storing the Hotel Static Content against hotelId - Starts Here */
        ////    Dictionary<int, StaticMHResponse> mhStaticInfo = new Dictionary<int, StaticMHResponse>();

        ////    /* Dictionary for storing the Hotel Static Content against hotelId - Ends Here */

        ////    StringBuilder searchParams = new StringBuilder();
        ////    searchParams.Append("");
        ////    int availabilityOfferCount = 0;
        ////    int uniqueHotelsInserted = 0;
        ////    int offersInserted = 0;
        ////    double offerDecorationTimeElapsed = 0.0;
        ////    string offersESBulkInsertionQuery = string.Empty;
        ////    string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
        ////    int bulkInsertionRetryCount = 1;
        ////    int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);

        ////    StaticEstablishmentParams staticResponseParms = new StaticEstablishmentParams();
        ////    Dictionary<int, StaticEstablishmentParams> staticInfo = new Dictionary<int, StaticEstablishmentParams>();

        ////    StaticResponseParms ttextStaticResponseParms = new StaticResponseParms();
        ////    Dictionary<int, StaticResponseParms> ttextStaticInfo = new Dictionary<int, StaticResponseParms>();

        ////    List<Guid> distinctHotelCodes = (from b2cAvailabilityResult in availabilityResultSet.AvailabilityResults select b2cAvailabilityResult.EstablishmentId).Distinct().ToList();
        ////    availabilityOfferCount = availabilityResultSet.AvailabilityTotalResultsCount;
        ////    List<string> distinctHotelIds = new List<string>();
        ////    List<string> redisStaticInfo = null;
        ////    List<string> ttextHotelsAvailable = new List<string>();

        ////    foreach (Guid distinctHotelCode in distinctHotelCodes)
        ////    {
        ////        if (alphaPkIdToIntId.ContainsKey(distinctHotelCode.ToString("D").ToUpper()))
        ////        {
        ////            distinctHotelIds.Add(alphaPkIdToIntId[distinctHotelCode.ToString("D").ToUpper()].ToString());
        ////        }
        ////    }

        ////    // Check how many hotels are present in the TText Side
        ////    foreach (Guid distinctHotelCode in distinctHotelCodes)
        ////    {
        ////        if (alphaToMhidMapping.ContainsKey(distinctHotelCode.ToString("D")))
        ////        {
        ////            ttextHotelsAvailable.Add(alphaToMhidMapping[distinctHotelCode.ToString("D")].ToString());
        ////        }
        ////    }

        ////    ErrorLogger.Log("Fetching the Static Content from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);
        ////    // Single Content Hub Content
        ////    using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
        ////                                             ConfigurationManager.AppSettings["RedisPort"]))
        ////    {
        ////        using (IRedisClient redisClient = pooledClientManager.GetClient())
        ////        {
        ////            redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
        ////            redisStaticInfo = redisClient.GetValues(distinctHotelIds);
        ////            // offersDecorated = redisStaticInfo.Count;
        ////            for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
        ////            {
        ////                staticResponseParms = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo[mhidCount]);
        ////                if (!staticInfo.ContainsKey(staticResponseParms.establishmentIntId))
        ////                {
        ////                    staticInfo.Add(staticResponseParms.establishmentIntId, staticResponseParms);
        ////                }
        ////            }
        ////        }
        ////    }

        ////    // TText Static Content
        ////    using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
        ////    {
        ////        using (IRedisClient redisClient = pooledClientManager.GetClient())
        ////        {
        ////            redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
        ////            redisStaticInfo = redisClient.GetValues(ttextHotelsAvailable);
        ////            // offersDecorated = redisStaticInfo.Count;
        ////            for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
        ////            {
        ////                ttextStaticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
        ////                if (!ttextStaticInfo.ContainsKey(ttextStaticResponseParms.mhid))
        ////                {
        ////                    ttextStaticInfo.Add(ttextStaticResponseParms.mhid, ttextStaticResponseParms);
        ////                }
        ////            }
        ////        }
        ////    }
        ////    ErrorLogger.Log("DONE ::: Fetching the Static Contetn from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);

        ////    /* offer builder */
        ////    try
        ////    {
        ////        if (availabilityResultSet != null && availabilityResultSet.AvailabilityResults.Count > 0)
        ////        {
        ////            foreach (AvailabilityResult availabilityresult in availabilityResultSet.AvailabilityResults)
        ////            {
        ////                if (alphaPkIdToIntId.ContainsKey(availabilityresult.EstablishmentId.ToString("D").ToUpper()) && staticInfo.ContainsKey(alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()]))
        ////                {
        ////                    uniqueHotelsInserted++;
        ////                    List<_Room> rooms = new List<_Room>();
        ////                    rooms = availabilityresult.Rooms;
        ////                    Dictionary<int, RoomConfiguration> roomConfigDict = new Dictionary<int, RoomConfiguration>();

        ////                    /* create a room occupancy dict */
        ////                    foreach (RoomOccupancy roomOccupancy in availabilityresult.RoomOccupancies)
        ////                    {
        ////                        RoomConfiguration currentRoomConfig = new RoomConfiguration();
        ////                        currentRoomConfig.adults = roomOccupancy.AdultsCount;
        ////                        currentRoomConfig.childrenAndInfants = roomOccupancy.ChildrenAndInfantsCount;

        ////                        roomConfigDict.Add(roomOccupancy.RoomNumber, currentRoomConfig);
        ////                    }

        ////                    RoomConfiguration roomConfig = roomConfigDict[room.RoomNumber];
        ////                    offers.adults = roomConfig.adults;
        ////                    offers.children = roomConfig.childrenAndInfants;

        ////                    foreach (_Room room in rooms)
        ////                    {
        ////                        B2COffersDocumentV2 offers = new B2COffersDocumentV2();
        ////                        staticResponseParms = staticInfo[alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()]];

        ////                        offers.availabilityId = availabilityResultSet.AvailabilityId;
        ////                        offers.roomId = room.RoomIdAlpha2;
        ////                        offers.roomDescription = room.RoomDescription;
        ////                        offers.roomNumber = room.RoomNumber;
        ////                        offers.boardType = room.BoardType;
        ////                        offers.boardDescription = room.BoardDescription;
        ////                        offers.providerEdiCode = room.ProviderEdiCode;
        ////                        offers.providerEstablishmentCode = room.ProviderEstablishmentCode;
        ////                        offers.providerName = room.ProviderName;
        ////                        offers.providerPrice = room.ProviderPrice.Amount;
        ////                        offers.providerCurrencyCode = room.ProviderPrice.CurrencyCode;
        ////                        offers.providerRoomCode = room.ProviderRoomCode;
        ////                        offers.totalPrice = room.Price.Amount;
        ////                        offers.highestPrice = (room.HighestPrice != null) ? room.HighestPrice.Amount : 0;
        ////                        offers.currencyCode = room.Price.CurrencyCode;
        ////                        offers.IsActiveAccommodationAdminFee = room.IsActiveAccommodationAdminFee;
        ////                        offers.isBidingRate = room.IsBindingRate;
        ////                        offers.IsCardChargeApplicable = room.IsCardChargeApplicable;
        ////                        offers.IsDuplicateRate = room.IsDuplicateRate;
        ////                        offers.IsEligibleForDeposit = room.IsEligibleForDeposit;
        ////                        offers.IsNonRefundable = room.IsNonRefundable;
        ////                        offers.IsOpaqueRate = room.IsOpaqueRate;


        ////                        /* Hotel Static Info from Single Content Hub */
        ////                        offers.hotelCode = availabilityresult.EstablishmentId;
        ////                        offers.hotelId = (alphaPkIdToIntId.ContainsKey(availabilityresult.EstablishmentId.ToString("D").ToUpper()) ? alphaPkIdToIntId[availabilityresult.EstablishmentId.ToString("D").ToUpper()] : 0);
        ////                        offers.hotelSEOFriendlyName = availabilityresult.EstablishmentSeoFriendlyName;
        ////                        offers.defaultAirportCode = staticResponseParms.defaultAirportCode;
        ////                        offers.checkInDate = availabilityresult.CheckInDate.ToString("yyyy-MM-dd");
        ////                        offers.checkOutDate = availabilityresult.CheckOutDate.ToString("yyyy-MM-dd");
        ////                        offers.destinationCode = availabilityresult.DestinationId;
        ////                        offers.destinationId = staticResponseParms.destinationId;
        ////                        offers.destinationName = availabilityresult.DestinationName;
        ////                        offers.destinationSEOFriendlyName = availabilityresult.DestinationSeoFriendlyName;
        ////                        offers.parentDestinationId = staticResponseParms.parentDestinationId;
        ////                        offers.parentDestinationName = staticResponseParms.parentDestinationName;
        ////                        offers.ISOCountryCode = staticResponseParms.ISOCountryCode;
        ////                        offers.countryName = staticResponseParms.countryName;
        ////                        offers.rank = getRank(alphaToMhidMapping, mhidToRankMapping, availabilityresult.EstablishmentId);
        ////                        offers.mhid = alphaToMhidMapping.ContainsKey(availabilityresult.EstablishmentId.ToString("D")) ? alphaToMhidMapping[availabilityresult.EstablishmentId.ToString("D")] : 0;
        ////                        offers.starRating = availabilityresult.EstablishmentStarRating;

        ////                        if (offers.mhid > 0 && ttextStaticInfo.ContainsKey(offers.mhid))
        ////                        {
        ////                            // Show TText Static Content
        ////                            ttextStaticResponseParms = ttextStaticInfo[offers.mhid];
        ////                            offers.hotelName = ttextStaticResponseParms.name;
        ////                            offers.tripAdvisorId = ttextStaticResponseParms.tripAdvisorId;
        ////                            offers.averageRating = ttextStaticResponseParms.averageRating;
        ////                            offers.reviewCount = ttextStaticResponseParms.reviewCount;
        ////                            offers.lat = ttextStaticResponseParms.lat;
        ////                            offers.lon = ttextStaticResponseParms.lng;
        ////                            offers.hotelFacilities = (ttextStaticResponseParms.features.Count > 0) ? ttextStaticResponseParms.features : staticResponseParms.establishmentFacilities;
        ////                            offers.roomFacilities = staticResponseParms.roomFacilities;
        ////                        }
        ////                        else
        ////                        {
        ////                            // Show Alpha Static Content
        ////                            offers.hotelName = staticResponseParms.establishmentName;
        ////                            offers.tripAdvisorId = staticResponseParms.tripAdvisorId;
        ////                            offers.averageRating = availabilityresult.EstablishmentTripAdvisorAverageScore;
        ////                            offers.reviewCount = availabilityresult.EstablishmentTripAdvisorReviewCount;
        ////                            offers.lat = staticResponseParms.lat.ToString();
        ////                            offers.lon = staticResponseParms.lon.ToString();
        ////                            offers.hotelFacilities = staticResponseParms.establishmentFacilities;
        ////                            offers.roomFacilities = staticResponseParms.roomFacilities;
        ////                        }
        ////                        offers.timeStamp = timeStamp;

        ////                        searchParams.Append("{\"index\" : {\"_index\" : \"" + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "\", \"_type\" : \"availability\" }}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + "\n");
        ////                        offersInserted++;
        ////                    }
        ////                }
        ////                else
        ////                {
        ////                    // Alpha pkId to intId mapping is missing
        ////                    ErrorLogger.Log("Following Establishment Id is missing the int Id Mapping : " + availabilityresult.EstablishmentId, LogLevel.ElasticSearchMissingAlphaPkIdToIntIdMapping);
        ////                }

        ////            }
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        ErrorLogger.Log("Exception while decorating offers for insertion into ES. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
        ////    }


        ////    offersDecorationTimer.Stop();
        ////    offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;

        ////    offersESBulkInsertionQuery = searchParams.ToString();
        ////    ErrorLogger.Log(string.Format("Number of Offers to be inserted in the Offers indice : {0}", offersInserted), LogLevel.ElasticSearchAvailability);
        ////    ErrorLogger.Log("Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);
        ////    //ErrorLogger.Log(string.Format("Offers Query for bulk insertion : {0}", offersESBulkInsertionQuery), LogLevel.ElasticSearchAvailability);

        ////    System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
        ////    offersIndexingTimer.Start();
        ////    if (distinctHotelIds.Count != 0)
        ////    {
        ////        while (b2bAvailabilityParams.isBulkInsertionOnRetry && bulkInsertionRetryCount >= 0)
        ////        {
        ////            try
        ////            {
        ////                string bulkOffers = offersESBulkInsertionQuery;
        ////                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "_bulk");
        ////                httpWebRequest.ContentType = "application/json";
        ////                httpWebRequest.Method = "POST";
        ////                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        ////                {
        ////                    streamWriter.Write(bulkOffers);
        ////                    streamWriter.Flush();
        ////                    streamWriter.Close();
        ////                }
        ////                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        ////                if (httpResponse != null)
        ////                {
        ////                    httpResponse.Close();
        ////                    httpResponse.Dispose();
        ////                }
        ////                b2bAvailabilityParams.isBulkInsertionOnRetry = false;
        ////            }
        ////            catch (WebException webexp)
        ////            {
        ////                ErrorLogger.Log("Web exception occured while Bulk call. Message - " + webexp.Message + ", Stack Trace - " + webexp.StackTrace, LogLevel.ElasticSearchAvailability);
        ////                Thread.Sleep(500);
        ////                bulkInsertionRetryCount--;
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                ErrorLogger.Log("Web exception occured while Bulk call. Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
        ////                Thread.Sleep(500);
        ////                bulkInsertionRetryCount--;
        ////            }
        ////        }

        ////        // Make facets call to check if more than 80% of the offers are getting fetched or not
        ////        string facetsResponse = string.Empty;
        ////        AvailabilityFacetsRequestResponse facetsRequestResponse = new AvailabilityFacetsRequestResponse();
        ////        string facetsQuery = B2CQueryBuilder.GetFacetsQuery(availabilityRequest);
        ////        try
        ////        {
        ////            facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
        ////            facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
        ////            while (facetsTrials > 0)
        ////            {
        ////                if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
        ////                {
        ////                    break;
        ////                }
        ////                else
        ////                {
        ////                    Thread.Sleep(500);
        ////                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
        ////                    facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
        ////                    facetsTrials--;
        ////                }
        ////            }
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            ErrorLogger.Log((string.Format("Exception occured while checking the facets. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace)), LogLevel.ElasticSearchAvailability);
        ////        }
        ////    }
        ////    offersIndexingTimer.Stop();

        ////    ErrorLogger.Log("DONE ::: Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);

        ////    b2bAvailabilityParams.availabilityOffersCount = availabilityOfferCount;
        ////    b2bAvailabilityParams.uniqueHotelsInserted = uniqueHotelsInserted;
        ////    b2bAvailabilityParams.offersInserted = offersInserted;
        ////    b2bAvailabilityParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
        ////    b2bAvailabilityParams.decoratingTime = offerDecorationTimeElapsed;
        ////    b2bAvailabilityParams.facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]) - facetsTrials;

        ////    return b2bAvailabilityParams;
        ////}

        private static int getRank(Dictionary<string, int> alphaToMhidMapping, Dictionary<string, int> mhidToRankMapping, Guid hotelCode)
        {
            int rank = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultRank"]);

            if (alphaToMhidMapping.ContainsKey(hotelCode.ToString("D")))
            {
                string mhid = alphaToMhidMapping[hotelCode.ToString("D")].ToString();

                if (mhidToRankMapping.ContainsKey(mhid))
                {
                    return mhidToRankMapping[mhid];
                }
            }
            return rank; // send default rank i.e., 999999
        }
    }

    public class RoomConfiguration
    {
        public int adults { get; set; }
        public int childrenAndInfants { get; set; }
    }
}