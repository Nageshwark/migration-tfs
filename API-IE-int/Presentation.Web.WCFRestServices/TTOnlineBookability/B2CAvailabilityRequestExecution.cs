﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CAvailabilityRequestExecution
    {
        public static B2BAvailabilityResultsParams GetAvailabilityResponse(string availabilityRequestStr, AvailabilitySearchRequest availabilityRequest)
        {
            B2BAvailabilityResultsParams b2cAvailabilityResultsParams = new B2BAvailabilityResultsParams();
            B2CAvailabilityResponse b2cAvailabilityResponse = new B2CAvailabilityResponse();
            System.Diagnostics.Stopwatch accomSoaResponseTimer = new System.Diagnostics.Stopwatch();
            double accomodationSoaResponseTime = 0.0;
            int pageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
            try
            {                
                AccommodationService.AccommodationAvailabilityServiceClient accomdationService = new AccommodationService.AccommodationAvailabilityServiceClient();
                AccommodationService.AccommodationB2CAvailabilityRequest accommodationB2CAvailabilityRequest = new AccommodationService.AccommodationB2CAvailabilityRequest();
                //accommodationB2CAvailabilityRequest = JsonConvert.DeserializeObject<AccommodationService.AccommodationB2CAvailabilityRequest>("{\"AvailabilityId\":null,\"Channel\":1,\"CheckInDate\":\"2018-07-27T00:00:00\",\"CheckOutDate\":\"2018-08-03T00:00:00\",\"ClientIpAddress\":\"127.0.0.1\",\"Debugging\":false,\"DestinationId\":\"fdcecbb6-b92e-4495-baa4-700f5d046687\",\"EstablishmentId\":null,\"IgnoreConsolidation\":false,\"PromotionalCode\":null,\"ProvidersToSearch\":null,\"Rooms\":[{\"Adults\":2,\"ChildAges\":\"\",\"Children\":0,\"RoomNumber\":1}],\"SearchType\":2,\"SuppliersToSearch\":null,\"TC4Origin\":false}");
                accommodationB2CAvailabilityRequest = JsonConvert.DeserializeObject<AccommodationService.AccommodationB2CAvailabilityRequest>(availabilityRequestStr);

                ErrorLogger.Log("Availability Request query sent to B2C Accommodation Service : " + availabilityRequestStr, LogLevel.ElasticSearchAvailability);
                
                if (availabilityRequest.searchRequestType == "fresh")
                {
                    accomSoaResponseTimer.Start();
                    var result1 = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = 0 },
                                                                       new AccommodationService.AccommodationB2CAvailabilitySort(), false);

                    if (result1.AvailabilityStatus == AccommodationService.Status.Successful)
                    {
                        accommodationB2CAvailabilityRequest.AvailabilityId = result1.AvailabilityId;
                        var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = pageSize, PageNumber = 1 }, new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                        accomSoaResponseTimer.Stop();
                        accomodationSoaResponseTime = accomSoaResponseTimer.Elapsed.TotalMilliseconds;
                        b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                    }
                    else
                    {
                        ErrorLogger.Log("B2C First call other than Success.", LogLevel.ElasticSearchAvailability);
                    }
                    
                }
                else if (availabilityRequest.searchRequestType == "continued")
                {
                    accomSoaResponseTimer.Start();
                    accommodationB2CAvailabilityRequest.AvailabilityId = availabilityRequest.availabilityId;
                    var result = accomdationService.StartAndGetAccommodationAvailabilityResponse(accommodationB2CAvailabilityRequest, new AccommodationService.AccommodationB2CAvailabilityFilter() { NumberOfResults = availabilityRequest.AvailabilityTotalResultsCount },
                                                                           new AccommodationService.AccommodationB2CAvailabilitySort(), false);
                    accomSoaResponseTimer.Stop();
                    accomodationSoaResponseTime = accomSoaResponseTimer.Elapsed.TotalMilliseconds;
                    b2cAvailabilityResponse = JsonConvert.DeserializeObject<B2CAvailabilityResponse>(JsonConvert.SerializeObject(result));
                    b2cAvailabilityResponse.AvailabilityResults = b2cAvailabilityResponse.AvailabilityResults.Skip(20).ToList();
                }

                if (b2cAvailabilityResponse.AvailabilityResults.Count > 0)
                {
                    ErrorLogger.Log("Got the Response from B2C Endpoint. Total Number of offers available = " + b2cAvailabilityResponse.AvailabilityTotalResultsCount, LogLevel.ElasticSearchAvailability);
                    b2cAvailabilityResultsParams = B2CESOfferDocBuilder.B2CESOfferBuilder(b2cAvailabilityResponse, availabilityRequest);
                    b2cAvailabilityResultsParams.availabilityId = b2cAvailabilityResponse.AvailabilityId;

                    if(availabilityRequest.searchRequestType == "fresh")
                    {
                        Task<AccommodationService.AccommodationB2CAvailabilityResultsFilterOptions> filterOptions = accomdationService.GetAccommodationAvailabilityResultsFilterOptionsAsync(b2cAvailabilityResultsParams.availabilityId);
                        filterOptions.Wait();
                        AccommodationService.AccommodationB2CAvailabilityResultsFilterOptions filterOptionResults = filterOptions.Result;
                        DateTime startDate = Convert.ToDateTime(availabilityRequest.startDate);
                        DateTime endDate = Convert.ToDateTime(availabilityRequest.endDate);
                        b2cAvailabilityResultsParams.priceMin = Convert.ToInt32(filterOptionResults.PricePerNightMin.Amount) * (endDate.Subtract(startDate).Days);
                        b2cAvailabilityResultsParams.priceMax = Convert.ToInt32(filterOptionResults.PricePerNightMax.Amount) * (endDate.Subtract(startDate).Days);
                    }
                }
                else
                {
                    ErrorLogger.Log("Error response from B2C Endpoint.", LogLevel.ElasticSearchAvailability);
                }
                b2cAvailabilityResultsParams.accomodationSoaResponseTime = accomodationSoaResponseTime;                
            }
            catch(Exception ex)
            {
                ErrorLogger.Log("Exception from B2C Endpoint. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
            }
            return b2cAvailabilityResultsParams;
        }
    }
}