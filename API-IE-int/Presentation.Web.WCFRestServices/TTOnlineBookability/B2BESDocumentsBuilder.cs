﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2BESDocumentsBuilder
    {
        public static B2BAvailabilityResultsParams B2BESOfferBuilder(B2BResponseAvailabilityOffers availabilityResultSet, AvailabilitySearchRequest availabilityRequest)
        {
            System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
            offersDecorationTimer.Start();
            B2BAvailabilityResultsParams b2bAvailabilityParams = new B2BAvailabilityResultsParams();
            b2bAvailabilityParams.isBulkInsertionOnRetry = true;
            ErrorLogger.Log("Building the ES Query for bulk insert into ES.", LogLevel.ElasticSearchAvailability);
            /* Load the Alpha pkId to AccomId Dictionary - start here */
            //var alphaPkIdToIntIdResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphapkdIdToIntId.json");
            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId; //JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaPkIdToIntIdResponse);

            /* Load the Alpha pkId to AccomId Dictionary - start here */

            /* Load AlphaId to MHID Mapping - Start Here */
            var alphaToMhidMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphaHoCoMapping.json");
            Dictionary<string, int> alphaToMhidMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaToMhidMappingResponse);
            ErrorLogger.Log("Downloaded alphaToMhidMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load AlphaId to MHID Mapping - End Here */

            /* Load MHID to Rank Mapping - Start Here */
            var mhidToRankMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/MHIDRankMapping.json");
            Dictionary<string, int> mhidToRankMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(mhidToRankMappingResponse);
            ErrorLogger.Log("Downloaded mhidToRankMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load MHID to Rank Mapping - Ends Here */

            /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */
            Dictionary<string, int> destinationMapping = Global.destinationAlphaNumericIdToDestinationId;
            /* Load Destination AlphaNumericId to Destination Id Mapping - Start Here */

            /* Dictionary for storing the Hotel Static Content against hotelId - Starts Here */
            Dictionary<int, StaticMHResponse> mhStaticInfo = new Dictionary<int, StaticMHResponse>();

            /* Dictionary for storing the Hotel Static Content against hotelId - Ends Here */

            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            int availabilityOfferCount = 0;
            int offersDecorated = 0;
            int offersInserted = 0;
            double offerDecorationTimeElapsed = 0.0;
            string offersESBulkInsertionQuery = string.Empty;
            string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
            int bulkInsertionRetryCount = 1;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);

            Dictionary<string, string> boardtypeToboardCode = new Dictionary<string, string>();
            boardtypeToboardCode.Add("HalfBoard", "3");
            boardtypeToboardCode.Add("Breakfast", "8");
            boardtypeToboardCode.Add("AllInclusive", "5");
            boardtypeToboardCode.Add("RoomOnly", "9");
            boardtypeToboardCode.Add("SelfCatering", "2");
            boardtypeToboardCode.Add("FullBoard", "4");

            StaticMHResponse staticResponseParms = new StaticMHResponse();
            Dictionary<int, StaticMHResponse> staticInfo = new Dictionary<int, StaticMHResponse>();
            List<string> distinctHotelCodes = (from ttssResult in availabilityResultSet.RoomStays.RoomStay select ttssResult.BasicPropertyInfo.HotelCode).Distinct().ToList();
            availabilityOfferCount = availabilityResultSet.RoomStays.RoomStay.Count;
            List<string> distinctHotelIds = new List<string>();
            List<string> redisStaticInfo = null;

            foreach (string distinctHotelCode in distinctHotelCodes)
            {
                if (alphaPkIdToIntId.ContainsKey(distinctHotelCode.ToUpper()))
                {
                    distinctHotelIds.Add(alphaPkIdToIntId[distinctHotelCode.ToUpper()].ToString());
                }
            }

            ErrorLogger.Log("Fetching the Static Content from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                     ConfigurationManager.AppSettings["RedisPort"]))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticContentDb"]);
                    redisStaticInfo = redisClient.GetValues(distinctHotelIds);
                    offersDecorated = redisStaticInfo.Count;
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticMHResponse>(redisStaticInfo[mhidCount]);
                        if (!staticInfo.ContainsKey(int.Parse(staticResponseParms._source.HotelId)))
                        {
                            staticInfo.Add(int.Parse(staticResponseParms._source.HotelId), staticResponseParms);
                        }
                    }
                }
            }
            ErrorLogger.Log("DONE ::: Fetching the Static Contetn from REDIS for distinct hotel ids.", LogLevel.ElasticSearchAvailability);

            if (availabilityResultSet.RoomStays != null)
            {
                foreach (RoomStay room in availabilityResultSet.RoomStays.RoomStay)
                {
                    /* Room rates for same hotel for different boardtypes and suppliers */
                    if (staticInfo.ContainsKey(alphaPkIdToIntId[room.BasicPropertyInfo.HotelCode.ToUpper()]) && room.RoomRates != null)
                    {
                        List<RoomRate> roomRates = room.RoomRates.RoomRate;

                        foreach (RoomRate roomRate in roomRates)
                        {
                            B2BOffersDocument offers = new B2BOffersDocument();

                            /* Guest information */
                            int adults = 0, children = 0;
                            foreach (GuestCount guest in room.GuestCounts.GuestCount)
                            {
                                if (Convert.ToInt32(guest.AgeQualifyingCode) == int.Parse(ConfigurationManager.AppSettings["AgeQualifyingCodeForAdults"]))
                                {
                                    adults += Convert.ToInt32(guest.Count);
                                }
                                else if (Convert.ToInt32(guest.AgeQualifyingCode) == int.Parse(ConfigurationManager.AppSettings["AgeQualifyingCodeForChildren"]))
                                {
                                    children += Convert.ToInt32(guest.Count);
                                }
                            }

                            /* Populating the hotel information */
                            staticResponseParms = staticInfo[alphaPkIdToIntId[room.BasicPropertyInfo.HotelCode.ToUpper()]];

                            offers.adults = adults;
                            offers.children = children;
                            offers.hotelCode = room.BasicPropertyInfo.HotelCode.ToUpper();
                            offers.hotelId = (alphaPkIdToIntId.ContainsKey(room.BasicPropertyInfo.HotelCode.ToUpper()) ? alphaPkIdToIntId[room.BasicPropertyInfo.HotelCode.ToUpper()] : 0);
                            offers.hotelName = staticResponseParms._source.BuildingName;
                            offers.hotelCity = room.BasicPropertyInfo.HotelCityCode;
                            offers.defaultAirportCode = staticResponseParms._source.DefaultAirportCode;
                            offers.destinationCode = room.BasicPropertyInfo.AreaID.ToUpper();
                            offers.destinationId = staticResponseParms._source.DestinationId;
                            offers.destinationName = staticResponseParms._source.DestinationName;
                            offers.parentDestinationId = staticResponseParms._source.ParentDestinationId;
                            offers.parentDestinationName = staticResponseParms._source.ParentDestinationName;
                            offers.ISOCountryCode = staticResponseParms._source.ISOCountryCode;
                            offers.countryName = staticResponseParms._source.CountryName;
                            offers.rank = getRank(alphaToMhidMapping, mhidToRankMapping, room.BasicPropertyInfo.HotelCode);
                            offers.mhid = alphaToMhidMapping.ContainsKey(room.BasicPropertyInfo.HotelCode) ? alphaToMhidMapping[room.BasicPropertyInfo.HotelCode] : 0;
                            offers.starRating = staticResponseParms._source.Rating;
                            offers.tripAdvisorId = staticResponseParms.TripAdvisor.TAID;
                            offers.averageRating = staticResponseParms.TripAdvisor.TARating;
                            offers.reviewCount = staticResponseParms.TripAdvisor.ReviewsCount;
                            offers.lat = staticResponseParms._source.Location.Lat.ToString();
                            offers.lon = staticResponseParms._source.Location.Lon.ToString();
                            offers.checkInDate = room.TimeSpan.Start;
                            offers.checkOutDate = room.TimeSpan.End;
                            offers.timeStamp = timeStamp;

                            offers.roomId = roomRate.RoomID;
                            offers.boardType = roomRate.RoomTypeCode;
                            offers.roomStayCandidateRPH = room.RoomStayCandidateRPH;
                            offers.boardTypeCode = boardtypeToboardCode[roomRate.RoomTypeCode.Trim()];
                            offers.roomDescription = roomRate.RoomRateDescription.Text;
                            var basePrice = roomRate.Rates.Rate.Fees.Fee.Find(r => r.Code.ToLower() == "base");
                            offers.basePrice = Double.Parse(basePrice.Amount);
                            var commission = roomRate.Rates.Rate.Fees.Fee.Find(r => r.Code.ToLower() == "commission");
                            offers.commission = Double.Parse(commission.Amount);
                            var vat = roomRate.Rates.Rate.Fees.Fee.Find(r => r.Code.ToLower() == "vat");
                            offers.vat = Double.Parse(vat.Amount);
                            offers.currency = roomRate.Total.CurrencyCode;
                            offers.totalPrice = Double.Parse(roomRate.Total.AmountAfterTax);
                            offers.isBidingRate = (roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData != null && roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData.IsBidingRate == "Y") ? true : false;
                            offers.isDirectPayment = (roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData != null && roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData.IsDirectPayment == "Y") ? true : false;
                            offers.IsOpaqueRate = (roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData != null && roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData.IsOpaqueRate == "Y") ? true : false;
                            offers.IsRefundableRate = (roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData != null && roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData.IsRefundableRate == "Y") ? true : false;
                            offers.IsSpecialRate = (roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData != null && roomRate.Total.TPA_Extensions.HotelAvailRQAdditionalData.IsSpecialRate == "Y") ? true : false;
                            
                            searchParams.Append("{\"index\" : {\"_index\" : \"b2b-availability-response-offers\", \"_type\" : \"availability\" }}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + "\n");

                            offersInserted++;
                        }
                    }
                }
            }
            offersDecorationTimer.Stop();
            offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;

            offersESBulkInsertionQuery = searchParams.ToString();
            ErrorLogger.Log(string.Format("Number of Offers to be inserted in the Offers indice : {0}", offersInserted), LogLevel.ElasticSearchAvailability);
            ErrorLogger.Log("Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);
            //ErrorLogger.Log(string.Format("Offers Query for bulk insertion : {0}", offersESBulkInsertionQuery), LogLevel.ElasticSearchAvailability);

            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            if (distinctHotelIds.Count != 0)
            {
                //InsertIntoES(offersESBulkInsertionQuery);
                while(b2bAvailabilityParams.isBulkInsertionOnRetry && bulkInsertionRetryCount >= 0)
                {
                    try
                    {
                        string bulkOffers = offersESBulkInsertionQuery;
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "_bulk");
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(bulkOffers);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        if (httpResponse != null)
                        {
                            httpResponse.Close();
                            httpResponse.Dispose();
                        }
                        b2bAvailabilityParams.isBulkInsertionOnRetry = false;
                    }
                    catch (WebException webexp)
                    {
                        ErrorLogger.Log("Web exception occured while Bulk call. Message - " + webexp.Message + ", Stack Trace - " + webexp.StackTrace, LogLevel.ElasticSearchAvailability);
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                    catch(Exception ex)
                    {
                        ErrorLogger.Log("Web exception occured while Bulk call. Message - " + ex.Message + ", Stack Trace - " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                }

                // Make facets call to check if more than 80% of the offers are getting fetched or not
                string facetsResponse = string.Empty;
                AvailabilityFacetsRequestResponse facetsRequestResponse = new AvailabilityFacetsRequestResponse();
                string facetsQuery = B2CQueryBuilder.GetFacetsQuery(availabilityRequest);
                try
                {
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
                    while (facetsTrials > 0)
                    {
                        if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                        {
                            break;
                        }
                        else
                        {
                            Thread.Sleep(500);
                            facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search", facetsQuery);
                            facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);
                            facetsTrials--;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log((string.Format("Exception occured while checking the facets. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace)), LogLevel.ElasticSearchAvailability);
                }
            }
            offersIndexingTimer.Stop();

            ErrorLogger.Log("DONE ::: Bulk inserting the offers in the ES indice", LogLevel.ElasticSearchAvailability);

            b2bAvailabilityParams.availabilityOffersCount = availabilityOfferCount;
            b2bAvailabilityParams.uniqueHotelsInserted = offersDecorated;
            b2bAvailabilityParams.offersInserted = offersInserted;
            b2bAvailabilityParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            b2bAvailabilityParams.decoratingTime = offerDecorationTimeElapsed;
            b2bAvailabilityParams.facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]) - facetsTrials;

            return b2bAvailabilityParams;
        }

        private static void InsertIntoES(string offersESBulkInsertionQuery)
        {
            try
            {
                string bulkOffers = offersESBulkInsertionQuery;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "_bulk");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(bulkOffers);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse != null)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                }
            }
            catch (WebException webexp)
            {
                ErrorLogger.Log("Web exception occured while Bulk call. Message - " + webexp.Message + ", Stack Trace - " + webexp.StackTrace, LogLevel.ElasticSearchAvailability);
                // errorLogger.AppendLine((string.Format("Web exception occured while Bulk call. Message - {1}, Stack Trace - {2}, ErrorMessage-4", webexp.Message, webexp.StackTrace)));
            }
        }
        private static int getRank(Dictionary<string, int> alphaToMhidMapping, Dictionary<string, int> mhidToRankMapping, string hotelCode)
        {
            int rank = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultRank"]);

            if (alphaToMhidMapping.ContainsKey(hotelCode))
            {
                string mhid = alphaToMhidMapping[hotelCode].ToString();

                if (mhidToRankMapping.ContainsKey(mhid))
                {
                    return mhidToRankMapping[mhid];
                }
            }
            return rank; // send default rank i.e., 999999
        }
    }
}