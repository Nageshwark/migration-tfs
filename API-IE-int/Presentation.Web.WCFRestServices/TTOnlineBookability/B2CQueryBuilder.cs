﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Configuration;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.TTOnlineMinPriceTool;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2CQueryBuilder
    {
        public static string GetESAvailabilitySearchRequest(HotelOnlyMinPriceOffer hotelOnlyAvailabilitySearchRequest)
        {
            AvailabilitySearchRequest availabilityRequest = new AvailabilitySearchRequest();
            availabilityRequest.destinationCode = hotelOnlyAvailabilitySearchRequest.destinationCode;
            availabilityRequest.hotelCode = hotelOnlyAvailabilitySearchRequest.establishmentCode;
            availabilityRequest.startDate = Convert.ToDateTime(hotelOnlyAvailabilitySearchRequest.checkInDate).ToString("yyyy-MM-dd");
            availabilityRequest.endDate = Convert.ToDateTime(hotelOnlyAvailabilitySearchRequest.checkOutDate).ToString("yyyy-MM-dd");
            availabilityRequest.rating = hotelOnlyAvailabilitySearchRequest.starRatings;

            List<GuestsPerRoom> guestsInfo = new List<GuestsPerRoom>();
            GuestsPerRoom guests = new GuestsPerRoom();
            guests.adults = int.Parse(hotelOnlyAvailabilitySearchRequest.adults);
            guests.children = int.Parse(hotelOnlyAvailabilitySearchRequest.children);
            guests.ages = new List<byte>();
            guestsInfo.Add(guests);

            availabilityRequest.roomList = guestsInfo;

            availabilityRequest.sort = hotelOnlyAvailabilitySearchRequest.sort;
            availabilityRequest.priceMin = int.Parse(hotelOnlyAvailabilitySearchRequest.priceMin);
            availabilityRequest.priceMax = int.Parse(hotelOnlyAvailabilitySearchRequest.priceMax);
            availabilityRequest.boardType = hotelOnlyAvailabilitySearchRequest.boardType.Select(c => c.ToString()).ToList();

            return JsonConvert.SerializeObject(availabilityRequest);
        }

        public static string GetAvailabilityRequestSearchQueryV2(AvailabilitySearchRequest availabilityRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            int guestConfigurations = availabilityRequest.roomList.Count;

            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"guestInfo\":{\"query\": \"" + availabilityRequest.guestInfoSearchRequest + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"term\":{\"roomSearchType\":{\"value\":\"" + availabilityRequest.roomSearchType + "\"}}},");
            searchParams.Append("{\"match\":{\"destinationCode\":{\"query\":\"" + availabilityRequest.destinationCode + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"boardType\":{\"query\":\"" + string.Join(" ", availabilityRequest.boardType) + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"startDate\":{\"query\":\"" + availabilityRequest.startDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"endDate\":{\"query\":\"" + availabilityRequest.endDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"isCacheHit\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("\"sort\":[{\"_score\":{\"order\":\"asc\"}}]}");

            return searchParams.ToString();
        }

        public static string GetAvailabilityRequestSearchQuery(AvailabilitySearchRequest availabilityRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            int guestConfigurations = availabilityRequest.roomList.Count;

            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            for (int i = 0; i < guestConfigurations; i++)
            {
                if (i == 0)
                {
                    searchParams.Append("{\"bool\":{\"must\":[{\"term\":{\"adults\":{\"value\":" + availabilityRequest.roomList[i].adults + "}}},{\"term\":{\"children\":{\"value\":" + availabilityRequest.roomList[i].children + "}}}]}}");
                }
                else
                {
                    searchParams.Append(",{\"bool\":{\"must\":[{\"term\":{\"adults\":{\"value\":" + availabilityRequest.roomList[i].adults + "}}},{\"term\":{\"children\":{\"value\":" + availabilityRequest.roomList[i].children + "}}}]}}");
                }
            }
            searchParams.Append("],\"must\":[");
            searchParams.Append("{\"match\":{\"destinationCode\":{\"query\":\"" + availabilityRequest.destinationCode + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"boardType\":{\"query\":\"" + string.Join(" ", availabilityRequest.boardType) + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"startDate\":{\"query\":\"" + availabilityRequest.startDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"endDate\":{\"query\":\"" + availabilityRequest.endDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"isCacheHit\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("\"sort\":[{\"_score\":{\"order\":\"asc\"}}]}");

            return searchParams.ToString();
        }

        public static string GetB2CAvailabilitySearchQueryV2(AvailabilitySearchRequest availabilityRequest)
        {
            B2CAvailabilityRequest b2bAvailabilityRequest = new B2CAvailabilityRequest();
            b2bAvailabilityRequest.AvailabilityId = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["AvailabilityId"]) ? null : ConfigurationManager.AppSettings["AvailabilityId"]);
            b2bAvailabilityRequest.Channel = int.Parse(ConfigurationManager.AppSettings["Channel"]);
            b2bAvailabilityRequest.CheckInDate = Convert.ToDateTime(availabilityRequest.startDate);
            b2bAvailabilityRequest.CheckOutDate = Convert.ToDateTime(availabilityRequest.endDate);
            b2bAvailabilityRequest.ClientIpAddress = ConfigurationManager.AppSettings["ClientIpAddress"];
            b2bAvailabilityRequest.Debugging = (ConfigurationManager.AppSettings["Debugging"] == "true" ? true : false);
            b2bAvailabilityRequest.DestinationId = availabilityRequest.destinationCode.ToLower();
            b2bAvailabilityRequest.EstablishmentId = string.IsNullOrWhiteSpace(availabilityRequest.hotelCode) ? null : availabilityRequest.hotelCode;
            b2bAvailabilityRequest.IgnoreConsolidation = (ConfigurationManager.AppSettings["IgnoreConsolidation"] == "true" ? true : false);
            b2bAvailabilityRequest.PromotionalCode = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["PromotionalCode"]) ? null : ConfigurationManager.AppSettings["PromotionalCode"]);
            b2bAvailabilityRequest.ProvidersToSearch = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["ProvidersToSearch"]) ? null : ConfigurationManager.AppSettings["ProvidersToSearch"]);
            List<_Room_> roomList = new List<_Room_>();

            int roomNumber = 0;
            foreach (GuestsPerRoom room in availabilityRequest.roomList)
            {
                roomNumber++;
                _Room_ roomRequest = new _Room_();
                roomRequest.Adults = room.adults;
                roomRequest.ChildAges = room.ages;
                roomRequest.Children = room.children;
                roomRequest.RoomNumber = roomNumber;

                roomList.Add(roomRequest);
            }
            b2bAvailabilityRequest.Rooms = roomList;
            b2bAvailabilityRequest.SearchType = int.Parse(ConfigurationManager.AppSettings["SearchType"]);
            b2bAvailabilityRequest.SuppliersToSearch = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SuppliersToSearch"]) ? null : ConfigurationManager.AppSettings["SuppliersToSearch"]);
            b2bAvailabilityRequest.TC4Origin = (ConfigurationManager.AppSettings["TC4Origin"] == "true" ? true : false);

            return JsonConvert.SerializeObject(b2bAvailabilityRequest);
        }

        public static string GetAvailabilityOffersQuery(AvailabilitySearchRequest availabilityRequest, int offersCount)
        {
            DateTime toDate = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);

            StringBuilder searchParams = new StringBuilder();

            searchParams.Append("");

            string sortParameter = string.Empty;
            string sort = string.Empty;
            if (String.IsNullOrWhiteSpace(availabilityRequest.sort))
            {
                sort = "totalPrice";
                sortParameter = "asc";
            }
            else
            {
                if (availabilityRequest.sort.Contains("tripadvisorratingdesc"))
                {
                    sort = "averageRating";
                    sortParameter = "desc";
                }
                else if (availabilityRequest.sort.Contains("ratingdesc"))
                {
                    sort = "starRating";
                    sortParameter = "desc";
                }
                else if (availabilityRequest.sort.Contains("rank"))
                {
                    sort = "rank";
                    sortParameter = "asc";
                }
                else
                {
                    sort = "totalPrice";
                    sortParameter = "asc";
                }
            }
            StringBuilder destinationStr = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(availabilityRequest.destinationCode))
            {

                destinationStr.Append("");

                destinationStr.Append(availabilityRequest.destinationCode);
                if (Global.parentChildDestinationHierarchy.ContainsKey(availabilityRequest.destinationCode.ToUpper()))
                {
                    destinationStr.Append("," + Global.parentChildDestinationHierarchy[availabilityRequest.destinationCode.ToUpper()]);
                }
            }

            ////if (availabilityRequest.roomList.Count > 1)
            ////{
            ////    searchParams.Append("{\"query\":{ \"bool\":{ \"should\":[");
            ////}
            ////else
            ////{
            ////    searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            ////}

            ////for (int i = 0; i < availabilityRequest.roomList.Count; i++)
            ////{
            ////    if (i == 0)
            ////    {
            ////        searchParams.Append("{\"bool\":{\"must\":[{\"term\":{\"adults\":{\"value\":" + availabilityRequest.roomList[i].adults + "}}},{\"term\":{\"children\":{\"value\":" + availabilityRequest.roomList[i].children + "}}}]}}");
            ////    }
            ////    else
            ////    {
            ////        searchParams.Append(",{\"bool\":{\"must\":[{\"term\":{\"adults\":{\"value\":" + availabilityRequest.roomList[i].adults + "}}},{\"term\":{\"children\":{\"value\":" + availabilityRequest.roomList[i].children + "}}}]}}");
            ////    }
            ////}
            ////searchParams.Append("],\"must\":[");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"guestInfoSearchRequest\":{\"query\": \"" + availabilityRequest.guestInfoSearchRequest + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"term\":{\"roomTypeSearch\":{\"value\":\"" + availabilityRequest.roomSearchType + "\"}}},");
            if (!availabilityRequest.boardType.Contains<string>("-1"))
            {
                searchParams.Append("{\"terms\":{\"boardType\":[\"" + string.Join("\",\"", availabilityRequest.boardType) + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"checkInDate\":{\"query\":\"" + availabilityRequest.startDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"checkOutDate\":{\"query\":\"" + availabilityRequest.endDate + "\",\"operator\":\"and\"}}},");
            //searchParams.Append("{\"terms\":{\"destinationId\":[" + availabilityRequest.destinationIds + "]}},");
            if (!string.IsNullOrWhiteSpace(destinationStr.ToString()))
            {
                searchParams.Append("{\"match\":{\"destinationCode\":{\"query\":\"" + destinationStr.ToString() + "\",\"operator\":\"or\"}}},");
            }
            searchParams.Append("{\"terms\":{\"starRating\": [" + availabilityRequest.rating + "]}},");
            if (availabilityRequest.tripAdvisorRating != 0)
            {
                searchParams.Append("{\"range\":{\"averageRating\":{\"gte\": " + availabilityRequest.tripAdvisorRating + "}}},");
            }
            if (availabilityRequest.priceMax != 0)
            {
                searchParams.Append("{\"range\":{\"totalPrice\":{\"from\":" + availabilityRequest.priceMin + ",\"to\":" + availabilityRequest.priceMax + "}}},");
            }
            else
            {
                searchParams.Append("{\"range\":{\"totalPrice\":{\"from\":" + availabilityRequest.priceMin + "}}},");
            }
            if (!string.IsNullOrWhiteSpace(availabilityRequest.hotelCode))
            {
                searchParams.Append("{\"match\":{\"hotelCode\":{\"query\":\"" + availabilityRequest.hotelCode + "\",\"operator\":\"or\"}}},");
            }
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]");
            if (!string.IsNullOrWhiteSpace(availabilityRequest.hotelCodesToExclude))
            {
                var hotelCodesToExclude = availabilityRequest.hotelCodesToExclude.Split(',');
                int noOfHotelToExclude = hotelCodesToExclude.Count();
                searchParams.Append(",\"must_not\":[");
                for (int i = 0; i < noOfHotelToExclude; i++)
                {
                    searchParams.Append("{\"match\":{\"hotelCode\":{\"query\":\"" + hotelCodesToExclude[i].ToString() + "\",\"operator\":\"or\"}}}");
                    if (i != noOfHotelToExclude - 1)
                    {
                        searchParams.Append(",");
                    }
                }
                searchParams.Append("]}},");
            }
            else
            {
                searchParams.Append("}},");
            }
            searchParams.Append("\"size\": 0,");
            searchParams.Append("\"aggs\":{\"group_by_hotelId\":{\"terms\":{\"field\":\"hotelId\",\"size\":" + offersCount + ",\"order\":{\"sort_parameter\":\"" + sortParameter + "\"}},\"aggs\":{\"sort_parameter\":{\"min\":{\"field\":\"" + sort + "\"}},\"top_price\":{\"top_hits\":{\"size\":500,\"sort\":{\"totalPrice\":{\"order\":\"asc\"}}}}}}}}");

            return searchParams.ToString();
        }

        public static string GetFacetsQuery(AvailabilitySearchRequest availabilityRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);

            StringBuilder searchParams = new StringBuilder();

            searchParams.Append("");

            StringBuilder destinationStr = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(availabilityRequest.destinationCode))
            {

                destinationStr.Append("");

                destinationStr.Append(availabilityRequest.destinationCode);
                if (Global.parentChildDestinationHierarchy.ContainsKey(availabilityRequest.destinationCode.ToUpper()))
                {
                    destinationStr.Append("," + Global.parentChildDestinationHierarchy[availabilityRequest.destinationCode.ToUpper()]);
                }
            }
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"guestInfoSearchRequest\":{\"query\": \"" + availabilityRequest.guestInfoSearchRequest + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"term\":{\"roomTypeSearch\":{\"value\":\"" + availabilityRequest.roomSearchType + "\"}}},");
            if (!availabilityRequest.boardType.Contains<string>("-1"))
            {
                searchParams.Append("{\"terms\":{\"boardType\":[\"" + string.Join("\",\"", availabilityRequest.boardType) + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"checkInDate\":{\"query\":\"" + availabilityRequest.startDate + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"checkOutDate\":{\"query\":\"" + availabilityRequest.endDate + "\",\"operator\":\"and\"}}},");
            if (availabilityRequest.priceMax != 0)
            {
                searchParams.Append("{\"range\":{\"totalPrice\":{\"from\":" + availabilityRequest.priceMin + ",\"to\":" + availabilityRequest.priceMax + "}}},");
            }
            else
            {
                searchParams.Append("{\"range\":{\"totalPrice\":{\"from\":" + availabilityRequest.priceMin + "}}},");
            }
            if (!string.IsNullOrWhiteSpace(destinationStr.ToString()))
            {
                searchParams.Append("{\"match\":{\"destinationCode\":{\"query\":\"" + destinationStr.ToString() + "\",\"operator\":\"or\"}}},");
            }
            searchParams.Append("{\"terms\":{\"starRating\": [" + availabilityRequest.rating + "]}},");
            searchParams.Append("{\"range\":{\"averageRating\":{\"gte\": " + availabilityRequest.tripAdvisorRating + "}}},");
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("\"size\":0,\"aggs\":{");
            searchParams.Append("\"price_min\":{\"min\":{\"field\":\"totalPrice\"}},");
            searchParams.Append("\"price_max\":{\"max\":{\"field\":\"totalPrice\"}},");
            searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotelId\",\"size\":5000},\"aggs\":{\"NoOfHotels\":{\"cardinality\":{\"field\":\"hotelId\"}}}},");
            searchParams.Append("\"rating\":{\"terms\":{\"field\":\"starRating\",\"size\":10},\"aggs\":{\"NoOfStarHotels\":{\"cardinality\":{\"field\":\"hotelId\"}}}},");
            searchParams.Append("\"tripAdvisorRating\":{\"terms\":{\"field\":\"averageRating\",\"size\":10},\"aggs\":{\"NoOfTARatedHotels\":{\"cardinality\":{\"field\":\"hotelId\"}}}}}}");

            return searchParams.ToString();
        }

        public static string GetB2CAvailabilitySearchQuery(AvailabilitySearchRequest availabilityRequest)
        {
            B2CAvailabilityRequest b2bAvailabilityRequest = new B2CAvailabilityRequest();
            b2bAvailabilityRequest.AvailabilityId = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["AvailabilityId"]) ? null : ConfigurationManager.AppSettings["AvailabilityId"]);
            b2bAvailabilityRequest.Channel = int.Parse(ConfigurationManager.AppSettings["Channel"]);
            b2bAvailabilityRequest.CheckInDate = Convert.ToDateTime(availabilityRequest.startDate);
            b2bAvailabilityRequest.CheckOutDate = Convert.ToDateTime(availabilityRequest.endDate);
            b2bAvailabilityRequest.ClientIpAddress = ConfigurationManager.AppSettings["ClientIpAddress"];
            b2bAvailabilityRequest.Debugging = (ConfigurationManager.AppSettings["Debugging"] == "true" ? true : false);
            b2bAvailabilityRequest.DestinationId = availabilityRequest.destinationCode.ToLower();
            b2bAvailabilityRequest.EstablishmentId = string.IsNullOrWhiteSpace(availabilityRequest.hotelCode) ? null : availabilityRequest.hotelCode;
            b2bAvailabilityRequest.IgnoreConsolidation = (ConfigurationManager.AppSettings["IgnoreConsolidation"] == "true" ? true : false);
            b2bAvailabilityRequest.PromotionalCode = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["PromotionalCode"]) ? null : ConfigurationManager.AppSettings["PromotionalCode"]);
            b2bAvailabilityRequest.ProvidersToSearch = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["ProvidersToSearch"]) ? null : ConfigurationManager.AppSettings["ProvidersToSearch"]);
            List<_Room_> roomList = new List<_Room_>();

            int roomNumber = 0;
            foreach (GuestsPerRoom room in availabilityRequest.roomList)
            {
                roomNumber++;
                _Room_ roomRequest = new _Room_();
                roomRequest.Adults = room.adults;
                roomRequest.ChildAges = room.ages;
                roomRequest.Children = room.children;
                roomRequest.RoomNumber = roomNumber;

                roomList.Add(roomRequest);
            }
            b2bAvailabilityRequest.Rooms = roomList;
            b2bAvailabilityRequest.SearchType = int.Parse(ConfigurationManager.AppSettings["SearchType"]);
            b2bAvailabilityRequest.SuppliersToSearch = (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SuppliersToSearch"]) ? null : ConfigurationManager.AppSettings["SuppliersToSearch"]);
            b2bAvailabilityRequest.TC4Origin = (ConfigurationManager.AppSettings["TC4Origin"] == "true" ? true : false);

            return JsonConvert.SerializeObject(b2bAvailabilityRequest);
        }

        public static string GetReviewNPayESQuery(string id)
        {
            StringBuilder searchParams = new StringBuilder();

            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"id\":\"" + id + "\"}}]}}}");

            return searchParams.ToString();
        }
             
    }
}