﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.TTOnlineBookability;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Configuration;
using System.IO;
using Presentation.WCFRestService.Model.Enum;
using ServiceStack.Redis;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.WCFRestService.Model.Misc;
using System.Text;
using Presentation.Web.WCFRestServices.ElasticSearchV2;

namespace Presentation.Web.WCFRestServices.TTOnlineBookability
{
    public class B2COffersBuilder
    {
        public static AvailabilityOffers GenerateOffers(B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability, AvailabilitySearchRequest availabilityRequest)
        {
            AvailabilityOffers availabilityOffers = new AvailabilityOffers();
            availabilityOffers.offers = new List<AOffers>();
            B2BAvailabilityResultsParams b2bAvailabilityParams = new B2BAvailabilityResultsParams();
            b2bAvailabilityParams.isResultFromB2C = false;
            int pageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
            // Search whether the request is available in the ES availability request indice
            ////ErrorLogger.Log("Checking if the Availability Search Request is available in the request indice.", LogLevel.ElasticSearchAvailability);
            ////bool isSearchAvailable = B2BExecuteQuery.CheckRequestInAvailabilityIndice(availabilityRequest);
            ////ErrorLogger.Log("Is Availability Search Request available in the request indice : " + isSearchAvailable.ToString(), LogLevel.ElasticSearchAvailability);

            if (b2bAvailabilitySearchRequestAvailability.availabilityOffersCount == 0)
            {
                ErrorLogger.Log("Getting the data by hiting the B2C endpoint", LogLevel.ElasticSearchAvailability);
                b2bAvailabilityParams = B2CExecuteQuery.HitB2CAvailability(availabilityRequest);
                b2bAvailabilityParams.isResultFromB2C = true;
                ErrorLogger.Log("Completed getting data from B2C endpoint.", LogLevel.ElasticSearchAvailability);
            }

            ErrorLogger.Log("Fetching Offers from the Availability Offers indice.", LogLevel.ElasticSearchAvailability);
            // Fetch Offers from ES
            string availabilityOffersQuery = B2CQueryBuilder.GetAvailabilityOffersQuery(availabilityRequest, pageSize);
            ErrorLogger.Log(string.Format("GetAvailabilityOffersQuery : {0}", availabilityOffersQuery), LogLevel.ElasticSearchAvailability);
            string availabilityOffersResponseFromES = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", availabilityOffersQuery);
            ErrorLogger.Log(string.Format("availabilityOffersResponseFromES : {0}", availabilityOffersResponseFromES), LogLevel.ElasticSearchAvailability);

            AvailabilityOffersResponse availabilityOffersResp = JsonConvert.DeserializeObject<AvailabilityOffersResponse>(availabilityOffersResponseFromES);
            ErrorLogger.Log(string.Format("availabilityOffersResp : {0}", JsonConvert.SerializeObject(availabilityOffersResp)), LogLevel.ElasticSearchAvailability);

            //string currentHotelCode = string.Empty;

            /* Get the Short Descriptions for the distinct hotel codes*/
            StaticEstablishmentParams staticResponseParms = new StaticEstablishmentParams();
            Dictionary<int, string> hotelStaticDescription = new Dictionary<int, string>();

            StaticResponseParms ttextStaticResponseParms = new StaticResponseParms();
            Dictionary<string, string> ttextStaticMHDescription = new Dictionary<string, string>();

            /* Load AlphaId to MHID Mapping - Start Here */
            var alphaToMhidMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphaHoCoMapping.json");
            Dictionary<string, int> alphaToMhidMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaToMhidMappingResponse);
            ErrorLogger.Log("Downloaded alphaToMhidMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load AlphaId to MHID Mapping - End Here */

            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId;
            List<Guid> distinctHotelCodes = (from bucket in availabilityOffersResp.aggregations.group_by_hotelId.buckets select bucket.top_price.hits.hits[0]._source.hotelCode).Distinct().ToList();
            List<string> distinctHotelIds = new List<string>();
            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            string imgSourceURLFormat = Global.imgSourceURLFormat;
            string ttOnlineImgSourceUrlFormat = Global.TTOnlineImgSourceURLFormat;
            List<string> ttextHotelsAvailable = new List<string>();

            if (distinctHotelCodes.Count != 0)
            {
                foreach (Guid distinctHotelCode in distinctHotelCodes)
                {
                    if (alphaPkIdToIntId.ContainsKey(distinctHotelCode.ToString("D").ToUpper()))
                    {
                        distinctHotelIds.Add(alphaPkIdToIntId[distinctHotelCode.ToString("D").ToUpper()].ToString());
                    }
                }

                // Check how many hotels are present in the TText Side
                foreach (Guid distinctHotelCode in distinctHotelCodes)
                {
                    if (alphaToMhidMapping.ContainsKey(distinctHotelCode.ToString("D")))
                    {
                        ttextHotelsAvailable.Add(alphaToMhidMapping[distinctHotelCode.ToString("D")].ToString());
                    }
                }

                ErrorLogger.Log("Getting the Hotel Static Info from REDIS.", LogLevel.ElasticSearchAvailability);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                         ConfigurationManager.AppSettings["RedisPort"]))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelIds);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo[mhidCount]);
                            if (!hotelStaticDescription.ContainsKey(staticResponseParms.establishmentIntId))
                            {
                                hotelStaticDescription.Add(staticResponseParms.establishmentIntId, staticResponseParms.description);
                            }
                        }
                    }
                }

                ErrorLogger.Log("Getting the TText Hotel Static Info from REDIS.", LogLevel.ElasticSearchAvailability);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]);
                        List<string> descriptionList = redisClient.GetValues(ttextHotelsAvailable);
                        for (int descriptionIndex = 0; descriptionIndex < descriptionList.Count(); descriptionIndex++)
                        {
                            MHDescription mhDescription = JsonConvert.DeserializeObject<MHDescription>(descriptionList[descriptionIndex]);

                            if (!string.IsNullOrWhiteSpace(mhDescription.MHID))
                            {
                                if (!ttextStaticMHDescription.ContainsKey(mhDescription.MHID))
                                {
                                    ttextStaticMHDescription.Add(mhDescription.MHID, mhDescription.description);
                                }
                            }
                        }
                    }
                }

                ErrorLogger.Log("Started building Offers.", LogLevel.ElasticSearchAvailability);
                try
                {
                    foreach (AvailabilityOfferBucket bucket in availabilityOffersResp.aggregations.group_by_hotelId.buckets)
                    {
                        AOffers offer = new AOffers();
                        offer.availabilityId = bucket.top_price.hits.hits[0]._source.availabilityId;
                        offer.hotelCode = bucket.top_price.hits.hits[0]._source.hotelCode;
                        offer.hotelId = bucket.top_price.hits.hits[0]._source.hotelId;
                        offer.hotelName = bucket.top_price.hits.hits[0]._source.hotelName;
                        offer.defaultAirportCode = bucket.top_price.hits.hits[0]._source.defaultAirportCode;
                        offer.destinationCode = bucket.top_price.hits.hits[0]._source.destinationCode;
                        //offer.destinationId = bucket.top_price.hits.hits[0]._source.destinationId;
                        offer.destinationName = bucket.top_price.hits.hits[0]._source.destinationName;
                        offer.parentDestinationCode = bucket.top_price.hits.hits[0]._source.parentDestinationCode;
                        offer.parentDestinationName = bucket.top_price.hits.hits[0]._source.parentDestinationName;
                        offer.ISOCountryCode = bucket.top_price.hits.hits[0]._source.ISOCountryCode;
                        offer.countryName = bucket.top_price.hits.hits[0]._source.countryName;
                        offer.rank = bucket.top_price.hits.hits[0]._source.rank;
                        offer.mhid = bucket.top_price.hits.hits[0]._source.mhid;
                        offer.checkInDate = bucket.top_price.hits.hits[0]._source.checkInDate;
                        offer.checkOutDate = bucket.top_price.hits.hits[0]._source.checkOutDate;
                        // offer.description = (offer.mhid > 0) ? (ttextStaticMHDescription.ContainsKey(offer.mhid.ToString()) ? ttextStaticMHDescription[offer.mhid.ToString()] : string.Empty) : (hotelStaticDescription.ContainsKey(offer.hotelId) ? hotelStaticDescription[offer.hotelId] : string.Empty);
                        offer.hotelFacilities = bucket.top_price.hits.hits[0]._source.hotelFacilities;
                        offer.roomFacilities = bucket.top_price.hits.hits[0]._source.roomFacilities;
                        offer.images = new List<string>();
                        offer.thumbnailimages = new List<string>();

                        // Hotel Description if mhid is available, serve from TText (if not available on TText serve from Alpha), else serve from Alpha
                        if (offer.mhid > 0)
                        {
                            if (ttextStaticMHDescription.ContainsKey(offer.mhid.ToString()))
                            {
                                offer.description = ttextStaticMHDescription[offer.mhid.ToString()];
                            }
                            else
                            {
                                if (hotelStaticDescription.ContainsKey(offer.hotelId))
                                {
                                    offer.description = hotelStaticDescription[offer.hotelId];
                                }
                                else
                                {
                                    offer.description = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            if (hotelStaticDescription.ContainsKey(offer.hotelId))
                            {
                                offer.description = hotelStaticDescription[offer.hotelId];
                            }
                            else
                            {
                                offer.description = string.Empty;
                            }
                        }

                        // If MHID is available, then serve the TText MasterHotel Images and Description, else serve Alpha Images
                        int hoteliffCount = 0;
                        if (offer.mhid > 0 && isTTImgAvailable(offer.mhid))
                        {
                            offer.isTTImg = true;
                            offer.isAlphaImg = false;
                            hoteliffCount = Global.mhidtoImageCountList[offer.mhid.ToString()];
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                offer.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + offer.mhid.ToString() + "/" + hotelIndex + ".jpg"));
                                offer.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + offer.mhid.ToString() + "/" + hotelIndex + ".jpg"));
                            }
                        }
                        else if (isAlphaImgAvailable(offer.hotelId))
                        {
                            offer.isTTImg = false;
                            offer.isAlphaImg = true;
                            hoteliffCount = Global.establishmentIdtoImageCountList[offer.hotelId.ToString()];
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                offer.images.Add(string.Format(ttOnlineImgSourceUrlFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + offer.hotelId.ToString() + "/" + hotelIndex + ".jpg"));
                                offer.thumbnailimages.Add(string.Format(ttOnlineImgSourceUrlFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + offer.hotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            }
                        }
                        else
                        {
                            offer.isTTImg = false;
                            offer.isAlphaImg = false;
                            offer.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            offer.images.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }

                        offer.rating = bucket.top_price.hits.hits[0]._source.starRating;

                        /* TA Ratings - Starts Here */
                        TADetails taDetails = new TADetails();
                        taDetails.tripAdvisorId = bucket.top_price.hits.hits[0]._source.tripAdvisorId;
                        taDetails.averageRating = bucket.top_price.hits.hits[0]._source.averageRating;
                        taDetails.reviewCount = bucket.top_price.hits.hits[0]._source.reviewCount;

                        offer.taInfo = taDetails;
                        /* TA Ratings - Ends Here */

                        TTLocation location = new TTLocation();
                        location.lat = (!string.IsNullOrWhiteSpace(bucket.top_price.hits.hits[0]._source.lat)) ? Double.Parse(bucket.top_price.hits.hits[0]._source.lat) : 0.0;
                        location.lon = (!string.IsNullOrWhiteSpace(bucket.top_price.hits.hits[0]._source.lon)) ? Double.Parse(bucket.top_price.hits.hits[0]._source.lon) : 0.0;

                        offer.location = location;

                        List<Room> rooms = new List<Room>();

                        if (bucket.top_price.hits.hits.Count > 0)
                        {
                            foreach (AvailabilitySearchHit hit in bucket.top_price.hits.hits)
                            {

                                var roomStayCandidateList = rooms.Find(r => r.roomNumber == hit._source.roomNumber);

                                if (roomStayCandidateList == null)
                                {
                                    Room room = new Room();
                                    room.roomNumber = hit._source.roomNumber;
                                    room.adults = hit._source.adults;
                                    room.children = hit._source.children;

                                    List<RoomPrice> roomPrices = new List<RoomPrice>();
                                    RoomPrice roomPrice = new RoomPrice();
                                    roomPrice.roomId = hit._source.roomId;
                                    roomPrice.boardTypeCode = hit._source.boardType;
                                    roomPrice.boardType = hit._source.boardDescription;
                                    roomPrice.roomDescription = hit._source.roomDescription;
                                    roomPrice.totalPrice = String.Format("{0:F2}", hit._source.totalPrice);
                                    roomPrice.highestPrice = String.Format("{0:F2}", hit._source.highestPrice);

                                    roomPrices.Add(roomPrice);

                                    room.roomPrices = roomPrices;
                                    rooms.Add(room);
                                }
                                else
                                {
                                    RoomPrice roomPrice = new RoomPrice();
                                    roomPrice.roomId = hit._source.roomId;
                                    roomPrice.boardTypeCode = hit._source.boardType;
                                    roomPrice.boardType = hit._source.boardDescription;
                                    roomPrice.roomDescription = hit._source.roomDescription;
                                    roomPrice.totalPrice = String.Format("{0:F2}", hit._source.totalPrice);
                                    roomPrice.highestPrice = String.Format("{0:F2}", hit._source.highestPrice);

                                    roomStayCandidateList.roomPrices.Add(roomPrice);
                                }
                            }
                        }

                        offer.rooms = rooms;
                        availabilityOffers.offers.Add(offer);
                    }
                }
                catch(Exception ex)
                {
                    ErrorLogger.Log("Exception while buildingoffers. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                }

                ErrorLogger.Log("Completed building Offers.", LogLevel.ElasticSearchAvailability);
                ErrorLogger.Log("Making the Facets Call.", LogLevel.ElasticSearchAvailability);
                #region Facets
                string facetsQuery = B2CQueryBuilder.GetFacetsQuery(availabilityRequest);
                string facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
                AvailabilityFacetsRequestResponse facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);

                /* Testing Purpose - delete later */
                availabilityOffers.isCacheHit = !b2bAvailabilityParams.isResultFromB2C;
                availabilityOffers.B2CAvailabilityTotalHotelCount = b2bAvailabilityParams.availabilityOffersCount;
                /* Testing Purpose - delete later */

                availabilityOffers.totalHotels = (b2bAvailabilityParams.isResultFromB2C) ? b2bAvailabilityParams.availabilityOffersCount : facetsRequestResponse.aggregations.hotel.buckets.Count;
                availabilityOffers.facets = new AvailabilityFacets();
                availabilityOffers.facets.priceRange = new WCFRestService.Model.TTOnlineBookability.Pricerange();
                availabilityOffers.facets.priceRange.min = Convert.ToInt32(facetsRequestResponse.aggregations.price_min.value);
                availabilityOffers.facets.priceRange.max = Convert.ToInt32(facetsRequestResponse.aggregations.price_max.value);

                /* Star Rating Cardinality - Starts Here */
                Dictionary<string, int> starRatingCardinality = new Dictionary<string, int>();
                foreach(FRRStarRatingBucket starRatingElement in facetsRequestResponse.aggregations.rating.buckets)
                {
                    if(starRatingCardinality.ContainsKey(starRatingElement.key.ToString()))
                    {
                        starRatingCardinality[starRatingElement.key.ToString()] = starRatingElement.NoOfStarHotels.value;
                    }
                    else
                    {
                        starRatingCardinality.Add(starRatingElement.key.ToString(), starRatingElement.NoOfStarHotels.value);
                    }
                }
                availabilityOffers.facets.starRatingCardinality = starRatingCardinality;
                /* Star Rating Cardinality - Ends Here */
                /* TripAdvisor Rating Cardinality - Starts Here */
                Dictionary<string, int> taRatingCardinality = new Dictionary<string, int>();
                availabilityOffers.facets.tripAdvisorCardinality = new Dictionary<string, int>();
                foreach (FRRTripAdvisorRatingBucket taRatingElement in facetsRequestResponse.aggregations.tripAdvisorRating.buckets)
                {
                    if (taRatingCardinality.ContainsKey(taRatingElement.key.ToString()))
                    {
                        taRatingCardinality[taRatingElement.key.ToString()] = taRatingElement.NoOfTARatedHotels.value;
                    }
                    else
                    {
                        taRatingCardinality.Add(taRatingElement.key.ToString(), taRatingElement.NoOfTARatedHotels.value);
                    }
                }
                for(int rating = 5; rating >= 3; rating--)
                {
                    if(rating != 5)
                    {
                        availabilityOffers.facets.tripAdvisorCardinality.Add(rating.ToString() + "+", ((taRatingCardinality.ContainsKey(rating.ToString())) ? taRatingCardinality[rating.ToString()] : 0) + ((taRatingCardinality.ContainsKey((rating + 0.5).ToString())) ? taRatingCardinality[(rating + 0.5).ToString()] : 0) + availabilityOffers.facets.tripAdvisorCardinality[(rating + 1).ToString() + "+"]);
                    }
                    else
                    {
                        availabilityOffers.facets.tripAdvisorCardinality.Add(rating.ToString() + "+", (taRatingCardinality.ContainsKey(rating.ToString())) ? taRatingCardinality[rating.ToString()] : 0);
                    }
                }
                
                /* TripAdvisor Rating Cardinality - Ends Here */
                #endregion
                ErrorLogger.Log("End of Facets Call.", LogLevel.ElasticSearchAvailability);
            }
            ErrorLogger.Log("Storing the search request in the indice by making Async call.", LogLevel.ElasticSearchAvailability);
            #region  Async_call_to_insert_in_request_index
            Task.Factory.StartNew(() =>
            {
                foreach (GuestsPerRoom guest in availabilityRequest.roomList)
                {
                    AvailabilitySearchRequestParams searchRequestParams = new AvailabilitySearchRequestParams();
                    searchRequestParams.destinationCode = availabilityRequest.destinationCode;
                    searchRequestParams.hotelCode = availabilityRequest.hotelCode;
                    searchRequestParams.startDate = availabilityRequest.startDate;
                    searchRequestParams.endDate = availabilityRequest.endDate;
                    //searchRequestParams.adults = guest.adults;
                    //searchRequestParams.children = guest.children;
                    searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                    searchRequestParams.boardType = availabilityRequest.boardType;

                    searchRequestParams.availabilityOffersCount = b2bAvailabilityParams.availabilityOffersCount;
                    searchRequestParams.uniqueHotelsInserted = b2bAvailabilityParams.uniqueHotelsInserted;
                    searchRequestParams.offersInserted = b2bAvailabilityParams.offersInserted;
                    searchRequestParams.indexingTime = b2bAvailabilityParams.indexingTime;
                    searchRequestParams.decoratingTime = b2bAvailabilityParams.decoratingTime;
                    searchRequestParams.accommodationSoaResponseTime = b2bAvailabilityParams.accomodationSoaResponseTime;
                    if (b2bAvailabilityParams.isResultFromB2C)
                    {
                        searchRequestParams.isCacheHit = false;
                    }
                    else
                    {
                        searchRequestParams.isCacheHit = true;
                    }

                    try
                    {
                        HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "availability");
                        httpWebSearchRequest.ContentType = "application/json";
                        httpWebSearchRequest.Method = "POST";
                        httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                        httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                        using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                        {
                            string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                            streamWriter.Write(searchRequestParamsResponse);
                            streamWriter.Flush();
                        }
                        var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                        if (httpSearchResponse != null)
                        {
                            httpSearchResponse.Close();
                            httpSearchResponse.Dispose();
                        }
                    }
                    catch (WebException ex)
                    {
                        ErrorLogger.Log(string.Format("Web exception occured while inserting Availability call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log(string.Format("Exception occured while inserting Availability call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                    }
                }
            });
            #endregion

            /* Make Async Call to Insert Data into ES for the current request - Starts Here */
            #region  Async_call_to_insert_remaining_offers_in_offers_index
            Task.Factory.StartNew(() =>
            {
                if (b2bAvailabilityParams.availabilityOffersCount > pageSize)
                {
                    availabilityRequest.searchRequestType = "continued";
                    availabilityRequest.availabilityId = b2bAvailabilityParams.availabilityId;
                    b2bAvailabilityParams = B2CExecuteQuery.HitB2CAvailability(availabilityRequest);
                }
            });
            #endregion
            /* Make Async Call to Insert Data into ES for the current request - Ends Here */
            return availabilityOffers;
        }

        private static bool isAlphaImgAvailable(int hotelId)
        {
            if(Global.establishmentIdtoImageCountList.ContainsKey(hotelId.ToString()) && Global.establishmentIdtoImageCountList[hotelId.ToString()] > 0)
            {
                return true;
            }
            return false;
        }

        private static bool isTTImgAvailable(int mhid)
        {
            if (Global.mhidtoImageCountList.ContainsKey(mhid.ToString()) && Global.mhidtoImageCountList[mhid.ToString()] > 0)
            {
                return true;
            }
            return false;
        }

        public static AvailabilityOffers GenerateOffersV2(B2BAvailabilitySearchRequestAvailability b2bAvailabilitySearchRequestAvailability, AvailabilitySearchRequest availabilityRequest)
        {
            AvailabilityOffers availabilityOffers = new AvailabilityOffers();
            availabilityOffers.offers = new List<AOffers>();
            B2BAvailabilityResultsParams b2bAvailabilityParams = new B2BAvailabilityResultsParams();
            b2bAvailabilityParams.isResultFromB2C = false;
            // Search whether the request is available in the ES availability request indice
            ////ErrorLogger.Log("Checking if the Availability Search Request is available in the request indice.", LogLevel.ElasticSearchAvailability);
            ////bool isSearchAvailable = B2BExecuteQuery.CheckRequestInAvailabilityIndice(availabilityRequest);
            ////ErrorLogger.Log("Is Availability Search Request available in the request indice : " + isSearchAvailable.ToString(), LogLevel.ElasticSearchAvailability);


            if (b2bAvailabilitySearchRequestAvailability.availabilityOffersCount == 0)
            {
                ErrorLogger.Log("Getting the data by hiting the B2C endpoint", LogLevel.ElasticSearchAvailability);
                b2bAvailabilityParams = B2CExecuteQuery.HitB2CAvailability(availabilityRequest);
                b2bAvailabilityParams.isResultFromB2C = true;
                ErrorLogger.Log("Completed getting data from B2C endpoint.", LogLevel.ElasticSearchAvailability);
            }

            ErrorLogger.Log("Fetching Offers from the Availability Offers indice.", LogLevel.ElasticSearchAvailability);
            // Fetch Offers from ES
            string availabilityOffersQuery = B2CQueryBuilder.GetAvailabilityOffersQuery(availabilityRequest, 20);
            ErrorLogger.Log(string.Format("GetAvailabilityOffersQuery : {0}", availabilityOffersQuery), LogLevel.ElasticSearchAvailability);
            string availabilityOffersResponseFromES = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", availabilityOffersQuery);
            ErrorLogger.Log(string.Format("availabilityOffersResponseFromES : {0}", availabilityOffersResponseFromES), LogLevel.ElasticSearchAvailability);

            AvailabilityOffersResponse availabilityOffersResp = JsonConvert.DeserializeObject<AvailabilityOffersResponse>(availabilityOffersResponseFromES);
            ErrorLogger.Log(string.Format("availabilityOffersResp : {0}", JsonConvert.SerializeObject(availabilityOffersResp)), LogLevel.ElasticSearchAvailability);

            //string currentHotelCode = string.Empty;

            /* Get the Short Descriptions for the distinct hotel codes*/
            StaticEstablishmentParams staticResponseParms = new StaticEstablishmentParams();
            Dictionary<int, string> hotelStaticDescription = new Dictionary<int, string>();

            StaticResponseParms ttextStaticResponseParms = new StaticResponseParms();
            Dictionary<string, string> ttextStaticMHDescription = new Dictionary<string, string>();

            /* Load AlphaId to MHID Mapping - Start Here */
            var alphaToMhidMappingResponse = Utilities.ExecuteGetWebRequest("http://teletext-deploy.s3.amazonaws.com/webapp/js/data/alphaHoCoMapping.json");
            Dictionary<string, int> alphaToMhidMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(alphaToMhidMappingResponse);
            ErrorLogger.Log("Downloaded alphaToMhidMapping file.", LogLevel.ElasticSearchAvailability);
            /* Load AlphaId to MHID Mapping - End Here */

            Dictionary<string, int> alphaPkIdToIntId = Global.alphaPkIdToIntId;
            List<Guid> distinctHotelCodes = (from bucket in availabilityOffersResp.aggregations.group_by_hotelId.buckets select bucket.top_price.hits.hits[0]._source.hotelCode).Distinct().ToList();
            List<string> distinctHotelIds = new List<string>();
            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            string imgSourceURLFormat = Global.imgSourceURLFormat;
            string ttOnlineImgSourceUrlFormat = Global.TTOnlineImgSourceURLFormat;
            List<string> ttextHotelsAvailable = new List<string>();

            if (distinctHotelCodes.Count != 0)
            {
                foreach (Guid distinctHotelCode in distinctHotelCodes)
                {
                    if (alphaPkIdToIntId.ContainsKey(distinctHotelCode.ToString("D").ToUpper()))
                    {
                        distinctHotelIds.Add(alphaPkIdToIntId[distinctHotelCode.ToString("D").ToUpper()].ToString());
                    }
                }

                // Check how many hotels are present in the TText Side
                foreach (Guid distinctHotelCode in distinctHotelCodes)
                {
                    if (alphaToMhidMapping.ContainsKey(distinctHotelCode.ToString("D")))
                    {
                        ttextHotelsAvailable.Add(alphaToMhidMapping[distinctHotelCode.ToString("D")].ToString());
                    }
                }

                ErrorLogger.Log("Getting the Hotel Static Info from REDIS.", LogLevel.ElasticSearchAvailability);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(ConfigurationManager.AppSettings["RedisPassword"] + "@" + ConfigurationManager.AppSettings["RedisHostForHotelJson"] + ":" +
                                                         ConfigurationManager.AppSettings["RedisPort"]))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisHotelStaticInfoDb"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelIds);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticEstablishmentParams>(redisStaticInfo[mhidCount]);
                            if (!hotelStaticDescription.ContainsKey(staticResponseParms.establishmentIntId))
                            {
                                hotelStaticDescription.Add(staticResponseParms.establishmentIntId, staticResponseParms.description);
                            }
                        }
                    }
                }

                ErrorLogger.Log("Getting the TText Hotel Static Info from REDIS.", LogLevel.ElasticSearchAvailability);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]);
                        List<string> descriptionList = redisClient.GetValues(ttextHotelsAvailable);
                        for (int descriptionIndex = 0; descriptionIndex < descriptionList.Count(); descriptionIndex++)
                        {
                            MHDescription mhDescription = JsonConvert.DeserializeObject<MHDescription>(descriptionList[descriptionIndex]);

                            if (!string.IsNullOrWhiteSpace(mhDescription.MHID))
                            {
                                if (!ttextStaticMHDescription.ContainsKey(mhDescription.MHID))
                                {
                                    ttextStaticMHDescription.Add(mhDescription.MHID, mhDescription.description);
                                }
                            }
                        }
                    }
                }

                ErrorLogger.Log("Started building Offers.", LogLevel.ElasticSearchAvailability);
                try
                {
                    foreach (AvailabilityOfferBucket bucket in availabilityOffersResp.aggregations.group_by_hotelId.buckets)
                    {
                        AOffers offer = new AOffers();
                        offer.availabilityId = bucket.top_price.hits.hits[0]._source.availabilityId;
                        offer.hotelCode = bucket.top_price.hits.hits[0]._source.hotelCode;
                        offer.hotelId = bucket.top_price.hits.hits[0]._source.hotelId;
                        offer.hotelName = bucket.top_price.hits.hits[0]._source.hotelName;
                        offer.defaultAirportCode = bucket.top_price.hits.hits[0]._source.defaultAirportCode;
                        offer.destinationCode = bucket.top_price.hits.hits[0]._source.destinationCode;
                        offer.TTRegionId = bucket.top_price.hits.hits[0]._source.TTRegionId;
                        //offer.destinationId = bucket.top_price.hits.hits[0]._source.destinationId;
                        offer.destinationName = bucket.top_price.hits.hits[0]._source.destinationName;
                        offer.parentDestinationCode = bucket.top_price.hits.hits[0]._source.parentDestinationCode;
                        offer.parentDestinationName = bucket.top_price.hits.hits[0]._source.parentDestinationName;
                        offer.ISOCountryCode = bucket.top_price.hits.hits[0]._source.ISOCountryCode;
                        offer.countryName = bucket.top_price.hits.hits[0]._source.countryName;
                        offer.rank = bucket.top_price.hits.hits[0]._source.rank;
                        offer.mhid = bucket.top_price.hits.hits[0]._source.mhid;
                        offer.checkInDate = bucket.top_price.hits.hits[0]._source.checkInDate;
                        offer.checkOutDate = bucket.top_price.hits.hits[0]._source.checkOutDate;
                        // offer.description = (offer.mhid > 0) ? (ttextStaticMHDescription.ContainsKey(offer.mhid.ToString()) ? ttextStaticMHDescription[offer.mhid.ToString()] : string.Empty) : (hotelStaticDescription.ContainsKey(offer.hotelId) ? hotelStaticDescription[offer.hotelId] : string.Empty);
                        offer.hotelFacilities = bucket.top_price.hits.hits[0]._source.hotelFacilities;
                        offer.roomFacilities = bucket.top_price.hits.hits[0]._source.roomFacilities;
                        offer.hotelGeneralInfo = bucket.top_price.hits.hits[0]._source.hotelGeneralInfo;
                        offer.images = new List<string>();
                        offer.thumbnailimages = new List<string>();

                        // Hotel Description if mhid is available, serve from TText (if not available on TText serve from Alpha), else serve from Alpha
                        if (offer.mhid > 0)
                        {
                            if (ttextStaticMHDescription.ContainsKey(offer.mhid.ToString()))
                            {
                                offer.description = ttextStaticMHDescription[offer.mhid.ToString()];
                            }
                            else
                            {
                                if (hotelStaticDescription.ContainsKey(offer.hotelId))
                                {
                                    offer.description = hotelStaticDescription[offer.hotelId];
                                }
                                else
                                {
                                    offer.description = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            if (hotelStaticDescription.ContainsKey(offer.hotelId))
                            {
                                offer.description = hotelStaticDescription[offer.hotelId];
                            }
                            else
                            {
                                offer.description = string.Empty;
                            }
                        }

                        // If MHID is available, then serve the TText MasterHotel Images and Description, else serve Alpha Images
                        int hoteliffCount = 0;
                        if (offer.mhid > 0 && isTTImgAvailable(offer.mhid))
                        {
                            offer.isTTImg = true;
                            offer.isAlphaImg = false;
                            hoteliffCount = Global.mhidtoImageCountList[offer.mhid.ToString()];
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                offer.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + offer.mhid.ToString() + "/" + hotelIndex + ".jpg"));
                                offer.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + offer.mhid.ToString() + "/" + hotelIndex + ".jpg"));
                            }
                        }
                        else if (isAlphaImgAvailable(offer.hotelId))
                        {
                            offer.isTTImg = false;
                            offer.isAlphaImg = true;
                            hoteliffCount = Global.establishmentIdtoImageCountList[offer.hotelId.ToString()];
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                offer.images.Add(string.Format(ttOnlineImgSourceUrlFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + offer.hotelId.ToString() + "/" + hotelIndex + ".jpg"));
                                offer.thumbnailimages.Add(string.Format(ttOnlineImgSourceUrlFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + offer.hotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            }
                        }
                        else
                        {
                            offer.isTTImg = false;
                            offer.isAlphaImg = false;
                            offer.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            offer.images.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }

                        offer.rating = bucket.top_price.hits.hits[0]._source.starRating;

                        /* TA Ratings - Starts Here */
                        TADetails taDetails = new TADetails();
                        taDetails.tripAdvisorId = bucket.top_price.hits.hits[0]._source.tripAdvisorId;
                        taDetails.averageRating = bucket.top_price.hits.hits[0]._source.averageRating;
                        taDetails.reviewCount = bucket.top_price.hits.hits[0]._source.reviewCount;

                        offer.taInfo = taDetails;
                        /* TA Ratings - Ends Here */

                        TTLocation location = new TTLocation();
                        location.lat = (!string.IsNullOrWhiteSpace(bucket.top_price.hits.hits[0]._source.lat)) ? Double.Parse(bucket.top_price.hits.hits[0]._source.lat) : 0.0;
                        location.lon = (!string.IsNullOrWhiteSpace(bucket.top_price.hits.hits[0]._source.lon)) ? Double.Parse(bucket.top_price.hits.hits[0]._source.lon) : 0.0;

                        offer.location = location;

                        List<Room> rooms = new List<Room>();

                        if (bucket.top_price.hits.hits.Count > 0)
                        {
                            foreach (AvailabilitySearchHit hit in bucket.top_price.hits.hits)
                            {

                                var roomStayCandidateList = rooms.Find(r => r.roomNumber == hit._source.roomNumber);

                                
                                string phoneNumber = string.Empty;
                                if(offer.TTRegionId != 0)
                                {
                                    phoneNumber = OffersBuilder.GetTelephoneNumber(offer.TTRegionId, "192", Convert.ToInt32(hit._source.totalPrice), "desktop");
                                }
                                offer.phoneNumber = phoneNumber;

                                if (roomStayCandidateList == null)
                                {
                                    Room room = new Room();
                                    room.roomNumber = hit._source.roomNumber;
                                    room.adults = hit._source.adults;
                                    room.children = hit._source.children;

                                    List<RoomPrice> roomPrices = new List<RoomPrice>();
                                    RoomPrice roomPrice = new RoomPrice();
                                    roomPrice.roomId = hit._source.roomId;
                                    roomPrice.boardTypeCode = hit._source.boardType;
                                    roomPrice.boardType = hit._source.boardDescription;
                                    roomPrice.roomDescription = hit._source.roomDescription;
                                    roomPrice.totalPrice = String.Format("{0:F2}", hit._source.totalPrice);
                                    roomPrice.highestPrice = String.Format("{0:F2}", hit._source.highestPrice);

                                    roomPrices.Add(roomPrice);

                                    room.roomPrices = roomPrices;
                                    rooms.Add(room);
                                }
                                else
                                {
                                    RoomPrice roomPrice = new RoomPrice();
                                    roomPrice.roomId = hit._source.roomId;
                                    roomPrice.boardTypeCode = hit._source.boardType;
                                    roomPrice.boardType = hit._source.boardDescription;
                                    roomPrice.roomDescription = hit._source.roomDescription;
                                    roomPrice.totalPrice = String.Format("{0:F2}", hit._source.totalPrice);
                                    roomPrice.highestPrice = String.Format("{0:F2}", hit._source.highestPrice);

                                    roomStayCandidateList.roomPrices.Add(roomPrice);
                                }
                            }
                        }

                        offer.rooms = rooms;
                        availabilityOffers.offers.Add(offer);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("Exception while buildingoffers. Message: " + ex.Message + ". StackTrace: " + ex.StackTrace, LogLevel.ElasticSearchAvailability);
                }

                ErrorLogger.Log("Completed building Offers.", LogLevel.ElasticSearchAvailability);
                ErrorLogger.Log("Making the Facets Call.", LogLevel.ElasticSearchAvailability);

                #region Facets
                string facetsQuery = B2CQueryBuilder.GetFacetsQuery(availabilityRequest);
                string facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityOffersIndice"] + "/_search", facetsQuery);
                AvailabilityFacetsRequestResponse facetsRequestResponse = JsonConvert.DeserializeObject<AvailabilityFacetsRequestResponse>(facetsResponse);

                /* Testing Purpose - delete later */
                availabilityOffers.isCacheHit = !b2bAvailabilityParams.isResultFromB2C;
                availabilityOffers.B2CAvailabilityTotalHotelCount = b2bAvailabilityParams.availabilityOffersCount;
                /* Testing Purpose - delete later */

                availabilityOffers.totalHotels = (b2bAvailabilityParams.isResultFromB2C) ? b2bAvailabilityParams.availabilityOffersCount : facetsRequestResponse.aggregations.hotel.buckets.Count;
                availabilityOffers.facets = new AvailabilityFacets();
                availabilityOffers.facets.priceRange = new WCFRestService.Model.TTOnlineBookability.Pricerange();
                availabilityOffers.facets.priceRange.min = (b2bAvailabilityParams.isResultFromB2C) ? Convert.ToInt32(b2bAvailabilityParams.priceMin) : Convert.ToInt32(facetsRequestResponse.aggregations.price_min.value);
                availabilityOffers.facets.priceRange.max = (b2bAvailabilityParams.isResultFromB2C) ? Convert.ToInt32(b2bAvailabilityParams.priceMax) : Convert.ToInt32(facetsRequestResponse.aggregations.price_max.value);

                /* Star Rating Cardinality - Starts Here */
                Dictionary<string, int> starRatingCardinality = new Dictionary<string, int>();
                foreach (FRRStarRatingBucket starRatingElement in facetsRequestResponse.aggregations.rating.buckets)
                {
                    if (starRatingCardinality.ContainsKey(starRatingElement.key.ToString()))
                    {
                        starRatingCardinality[starRatingElement.key.ToString()] = starRatingElement.NoOfStarHotels.value;
                    }
                    else
                    {
                        starRatingCardinality.Add(starRatingElement.key.ToString(), starRatingElement.NoOfStarHotels.value);
                    }
                }
                availabilityOffers.facets.starRatingCardinality = starRatingCardinality;
                /* Star Rating Cardinality - Ends Here */
                /* TripAdvisor Rating Cardinality - Starts Here */
                Dictionary<string, int> taRatingCardinality = new Dictionary<string, int>();
                availabilityOffers.facets.tripAdvisorCardinality = new Dictionary<string, int>();
                foreach (FRRTripAdvisorRatingBucket taRatingElement in facetsRequestResponse.aggregations.tripAdvisorRating.buckets)
                {
                    if (taRatingCardinality.ContainsKey(taRatingElement.key.ToString()))
                    {
                        taRatingCardinality[taRatingElement.key.ToString()] = taRatingElement.NoOfTARatedHotels.value;
                    }
                    else
                    {
                        taRatingCardinality.Add(taRatingElement.key.ToString(), taRatingElement.NoOfTARatedHotels.value);
                    }
                }
                for (int rating = 5; rating >= 3; rating--)
                {
                    if (rating != 5)
                    {
                        availabilityOffers.facets.tripAdvisorCardinality.Add(rating.ToString() + "+", ((taRatingCardinality.ContainsKey(rating.ToString())) ? taRatingCardinality[rating.ToString()] : 0) + ((taRatingCardinality.ContainsKey((rating + 0.5).ToString())) ? taRatingCardinality[(rating + 0.5).ToString()] : 0) + availabilityOffers.facets.tripAdvisorCardinality[(rating + 1).ToString() + "+"]);
                    }
                    else
                    {
                        availabilityOffers.facets.tripAdvisorCardinality.Add(rating.ToString() + "+", (taRatingCardinality.ContainsKey(rating.ToString())) ? taRatingCardinality[rating.ToString()] : 0);
                    }
                }

                /* TripAdvisor Rating Cardinality - Ends Here */
                #endregion
                ErrorLogger.Log("End of Facets Call.", LogLevel.ElasticSearchAvailability);
            }
            ErrorLogger.Log("Storing the search request in the indice by making Async call.", LogLevel.ElasticSearchAvailability);
            #region  Async_call_to_insert_in_request_index
            Task.Factory.StartNew(() =>
            {
                AvailabilitySearchRequestParams searchRequestParams = new AvailabilitySearchRequestParams();
                searchRequestParams.guestInfo = availabilityRequest.guestInfoSearchRequest;
                searchRequestParams.roomSearchType = availabilityRequest.roomSearchType;
                searchRequestParams.destinationCode = availabilityRequest.destinationCode;
                searchRequestParams.hotelCode = availabilityRequest.hotelCode;
                searchRequestParams.startDate = availabilityRequest.startDate;
                searchRequestParams.endDate = availabilityRequest.endDate;
                //searchRequestParams.adults = guest.adults;
                //searchRequestParams.children = guest.children;
                searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.boardType = availabilityRequest.boardType;

                searchRequestParams.availabilityOffersCount = b2bAvailabilityParams.availabilityOffersCount;
                searchRequestParams.uniqueHotelsInserted = b2bAvailabilityParams.uniqueHotelsInserted;
                searchRequestParams.offersInserted = b2bAvailabilityParams.offersInserted;
                searchRequestParams.indexingTime = b2bAvailabilityParams.indexingTime;
                searchRequestParams.decoratingTime = b2bAvailabilityParams.decoratingTime;
                searchRequestParams.accommodationSoaResponseTime = b2bAvailabilityParams.accomodationSoaResponseTime;
                if (b2bAvailabilityParams.isResultFromB2C)
                {
                    searchRequestParams.isCacheHit = false;
                }
                else
                {
                    searchRequestParams.isCacheHit = true;
                }

                try
                {
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["AvailabilityRequestsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + "availability");
                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLogger.Log(string.Format("Web exception occured while inserting Availability call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting Availability call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
            });
            #endregion

            /* Make Async Call to Insert Data into ES for the current request - Starts Here */
            #region  Async_call_to_insert_remaining_offers_in_offers_index
            Task.Factory.StartNew(() =>
            {
                if (b2bAvailabilityParams.availabilityOffersCount > 20)
                {
                    availabilityRequest.searchRequestType = "continued";
                    availabilityRequest.availabilityId = b2bAvailabilityParams.availabilityId;
                    availabilityRequest.AvailabilityTotalResultsCount = b2bAvailabilityParams.availabilityOffersCount;
                    b2bAvailabilityParams = B2CExecuteQuery.HitB2CAvailability(availabilityRequest);
                }
            });
            #endregion
            /* Make Async Call to Insert Data into ES for the current request - Ends Here */
            return availabilityOffers;
        }
    }
}