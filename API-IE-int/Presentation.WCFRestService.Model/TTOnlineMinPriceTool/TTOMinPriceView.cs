﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineMinPriceTool
{
    public class TTOMinPriceView
    {
        public double minimumPrice { get; set; }
        public int roomId { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public string boardBasis { get; set; }
        public float starRating { get; set; }
        public Guid establishmentCode { get; set; }
        public int establishmentId { get; set; }
        public string establishmentName { get; set; }
        public int mhid { get; set; }        
        public string description { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public Guid destinationCode { get; set; }
        public string destinationName { get; set; }
    }
}
