﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class QuoteRefResponseParams
    {
        public int took { get; set; }
        public bool errors { get; set; }
        public List<Item> items { get; set; }
    }
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Index
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public string result { get; set; }
        public Shards _shards { get; set; }
        public bool created { get; set; }
        public int status { get; set; }
    }

    public class Item
    {
        public Index index { get; set; }
    }
}
