﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Presentation.WCFRestService.Model.Misc;

namespace Presentation.WCFRestService.Model.StaticHotelLanding
{
    public class StaticMasterHotelInfo
    {
        public MasterHotelSource _source { get; set; }
        public TripAdvisor TripAdvisor { get; set; }
        public SearchQueryParameters searchParams { get; set; }
        public List<string> images { get; set; }
        public List<string> mobileimages { get; set; }
        public List<string> thumbnailimages { get; set; }
        public string title { get; set; }
        public string metaDescription { get; set; }
    }

    public class MasterHotelSource
    {
        public int MasterHotelId { get; set; }
        public string BuildingName { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
        public int MasterResortId { get; set; }
        public string MasterResortName { get; set; }
        public int RatingLevel { get; set; }
        public int Rating { get; set; }
        public string GIATAID { get; set; }
        public int RegionTTSSLabelD { get; set; }
        public MHIDLocation Location { get; set; }
        public MHIDAddress Address { get; set; }
        public MHIDRegion Region { get; set; }
        public List<object> Features { get; set; }
        public string Description { get; set; }
        public string shortDescription { get; set; }
    }

    public class urlToDestination
    {
        public string url { get; set; }
        public string destLabel { get; set; }
        public string destinationLabelId { get; set; }
        public string MHID { get; set; }
        public string channelId { get; set; }
    }
}
