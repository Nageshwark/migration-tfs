﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class TrustPilotLink
    {
        public string href { get; set; }
        public string method { get; set; }
        public string rel { get; set; }
    }

    public class Name
    {
        public string identifying { get; set; }
        public List<string> referring { get; set; }
    }

    public class NumberOfReviews
    {
        public int total { get; set; }
        public int oneStar { get; set; }
        public int twoStars { get; set; }
        public int threeStars { get; set; }
        public int fourStars { get; set; }
        public int fiveStars { get; set; }
    }

    public class TrustPilotReviewsSummary
    {
        public List<TrustPilotLink> links { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public Name name { get; set; }
        public string websiteUrl { get; set; }
        public double trustScore { get; set; }
        public int stars { get; set; }
        public string country { get; set; }
        public NumberOfReviews numberOfReviews { get; set; }
        public string status { get; set; }
    }
}
