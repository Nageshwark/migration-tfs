﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class SignUpDetails
    {
        public string Departure_airport;
        public string DisplayName;
        public string Email;
        public string Keep_me_deals;
        public string Phone_Code;
        public string Provider;
        public string SignUpDate;
        public string Telephone;
    }
}
