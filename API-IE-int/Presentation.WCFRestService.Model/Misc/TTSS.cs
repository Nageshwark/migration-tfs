﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Presentation.WCFRestService.Model.Misc
{
    [XmlRoot(ElementName = "Result")]
    public class Result
    {
        [XmlAttribute(AttributeName = "accommodationUpdateTime")]
        public string accommodationUpdateTime { get; set; }
        [XmlAttribute(AttributeName = "rank")]
        public string rank { get; set; }
        [XmlAttribute(AttributeName = "destinationId")]
        public string DestinationId { get; set; }
        [XmlAttribute(AttributeName = "tradingNameId")]
        public string TradingNameId { get; set; }
        [XmlAttribute(AttributeName = "duration")]
        public string Duration { get; set; }
        [XmlAttribute(AttributeName = "departureId")]
        public string DepartureId { get; set; }
        [XmlAttribute(AttributeName = "boardTypeId")]
        public string BoardTypeId { get; set; }
        [XmlAttribute(AttributeName = "boardTypeCode")]
        public string BoardTypeCode { get; set; }
        [XmlAttribute(AttributeName = "adults")]
        public string Adults { get; set; }
        [XmlAttribute(AttributeName = "children")]
        public string Children { get; set; }
        [XmlAttribute(AttributeName = "rating")]
        public string Rating { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string Price { get; set; }
        [XmlAttribute(AttributeName = "accommodation")]
        public string Accommodation { get; set; }
        [XmlAttribute(AttributeName = "updated")]
        public string Updated { get; set; }
        [XmlAttribute(AttributeName = "outboundFlightDirect")]
        public string OutboundFlightDirect { get; set; }
        [XmlAttribute(AttributeName = "inboundFlightDirect")]
        public string InboundFlightDirect { get; set; }
        [XmlAttribute(AttributeName = "destination")]
        public string Destination { get; set; }
        [XmlAttribute(AttributeName = "glong")]
        public string Glong { get; set; }
        [XmlAttribute(AttributeName = "glat")]
        public string Glat { get; set; }
        [XmlAttribute(AttributeName = "hotelOperator")]
        public string HotelOperator { get; set; }
        [XmlAttribute(AttributeName = "contentId")]
        public string ContentId { get; set; }
        [XmlAttribute(AttributeName = "hotelKey")]
        public string HotelKey { get; set; }
        [XmlAttribute(AttributeName = "quoteRef")]
        public string QuoteRef { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "outboundDepartureDate")]
        public string OutboundDepartureDate { get; set; }
        [XmlAttribute(AttributeName = "outboundArrivalDate")]
        public string OutboundArrivalDate { get; set; }
        [XmlAttribute(AttributeName = "outboundDepartureAirportName")]
        public string OutboundDepartureAirportName { get; set; }
        [XmlAttribute(AttributeName = "outboundDepartureAirportCode")]
        public string OutboundDepartureAirportCode { get; set; }
        [XmlAttribute(AttributeName = "outboundArrivalAirportName")]
        public string OutboundArrivalAirportName { get; set; }
        [XmlAttribute(AttributeName = "outboundArrivalAirportCode")]
        public string OutboundArrivalAirportCode { get; set; }
        [XmlAttribute(AttributeName = "outboundLegCount")]
        public string OutboundLegCount { get; set; }
        [XmlAttribute(AttributeName = "outboundFlightDuration")]
        public string OutboundFlightDuration { get; set; }
        [XmlAttribute(AttributeName = "outboundFlightNumber")]
        public string OutboundFlightNumber { get; set; }
        [XmlAttribute(AttributeName = "outboundAirlineICAO")]
        public string OutboundAirlineICAO { get; set; }
        [XmlAttribute(AttributeName = "inboundDepartureDate")]
        public string InboundDepartureDate { get; set; }
        [XmlAttribute(AttributeName = "inboundArrivalDate")]
        public string InboundArrivalDate { get; set; }
        [XmlAttribute(AttributeName = "inboundDepartureAirportName")]
        public string InboundDepartureAirportName { get; set; }
        [XmlAttribute(AttributeName = "inboundDepartureAirportCode")]
        public string InboundDepartureAirportCode { get; set; }
        [XmlAttribute(AttributeName = "inboundArrivalAirportName")]
        public string InboundArrivalAirportName { get; set; }
        [XmlAttribute(AttributeName = "inboundArrivalAirportCode")]
        public string InboundArrivalAirportCode { get; set; }
        [XmlAttribute(AttributeName = "inboundLegCount")]
        public string InboundLegCount { get; set; }
        [XmlAttribute(AttributeName = "inboundFlightDuration")]
        public string InboundFlightDuration { get; set; }
        [XmlAttribute(AttributeName = "inboundFlightNumber")]
        public string InboundFlightNumber { get; set; }
        [XmlAttribute(AttributeName = "inboundAirlineICAO")]
        public string InboundAirlineICAO { get; set; }
        [XmlAttribute(AttributeName = "adultBagPrice")]
        public string AdultBagPrice { get; set; }
        [XmlAttribute(AttributeName = "childBagPrice")]
        public string ChildBagPrice { get; set; }
        [XmlAttribute(AttributeName = "alternativeTotalPrice")]
        public string AlternativeTotalPrice { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundDepartureDate")]
        public string AlternativeOutboundDepartureDate { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundArrivalDate")]
        public string AlternativeOutboundArrivalDate { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundDepartureAirportName")]
        public string AlternativeOutboundDepartureAirportName { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundDepartureAirportCode")]
        public string AlternativeOutboundDepartureAirportCode { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundArrivalAirportName")]
        public string AlternativeOutboundArrivalAirportName { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundArrivalAirportCode")]
        public string AlternativeOutboundArrivalAirportCode { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundLegCount")]
        public string AlternativeOutboundLegCount { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundFlightDuration")]
        public string AlternativeOutboundFlightDuration { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundFlightNumber")]
        public string AlternativeOutboundFlightNumber { get; set; }
        [XmlAttribute(AttributeName = "alternativeOutboundAirlineICAO")]
        public string AlternativeOutboundAirlineICAO { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundDepartureDate")]
        public string AlternativeInboundDepartureDate { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundArrivalDate")]
        public string AlternativeInboundArrivalDate { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundDepartureAirportName")]
        public string AlternativeInboundDepartureAirportName { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundDepartureAirportCode")]
        public string AlternativeInboundDepartureAirportCode { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundArrivalAirportName")]
        public string AlternativeInboundArrivalAirportName { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundArrivalAirportCode")]
        public string AlternativeInboundArrivalAirportCode { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundLegCount")]
        public string AlternativeInboundLegCount { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundFlightDuration")]
        public string AlternativeInboundFlightDuration { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundFlightNumber")]
        public string AlternativeInboundFlightNumber { get; set; }
        [XmlAttribute(AttributeName = "alternativeInboundAirlineICAO")]
        public string AlternativeInboundAirlineICAO { get; set; }
        [XmlAttribute(AttributeName = "alternativeAdultBagPrice")]
        public string AlternativeAdultBagPrice { get; set; }
        [XmlAttribute(AttributeName = "alternativechildBagPrice")]
        public string AlternativechildBagPrice { get; set; }
    }
    [XmlRoot(ElementName = "Results")]
    public class Results
    {
        [XmlElement(ElementName = "Result")]
        public List<Result> Result { get; set; }
        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
    }

    [XmlRoot(ElementName = "Search")]
    public class Search
    {
        [XmlAttribute(AttributeName = "tradingNameId")]
        public string TradingNameId { get; set; }
        [XmlAttribute(AttributeName = "dateMin")]
        public string DateMin { get; set; }
        [XmlAttribute(AttributeName = "dateMax")]
        public string DateMax { get; set; }
        [XmlAttribute(AttributeName = "durationMin")]
        public string DurationMin { get; set; }
        [XmlAttribute(AttributeName = "durationMax")]
        public string DurationMax { get; set; }
        [XmlAttribute(AttributeName = "adults")]
        public string Adults { get; set; }
        [XmlAttribute(AttributeName = "children")]
        public string Children { get; set; }
        [XmlAttribute(AttributeName = "destinationIds")]
        public string DestinationIds { get; set; }
        [XmlAttribute(AttributeName = "timeout")]
        public string Timeout { get; set; }
        [XmlAttribute(AttributeName = "boardBasisId")]
        public string BoardBasisId { get; set; }
        [XmlAttribute(AttributeName = "departureId")]
        public string DepartureId { get; set; }
    }

    [XmlRoot(ElementName = "Container")]
    public class Container
    {
        [XmlElement(ElementName = "Results")]
        public Results Results { get; set; }
        //[XmlElement(ElementName = "Search")]
        //public Search Search { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
    }

}
