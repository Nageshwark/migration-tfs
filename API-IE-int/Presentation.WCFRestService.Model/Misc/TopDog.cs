﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{      
    public class TopDogBookingDataParam
    {
        public List<TopDogBooking> TopDogBookingDataParamList { get; set; }
    }

    public class TopDogBooking
    {
        public string BookingRef { get; set; }
        public string LastName { get; set; }
        public string DOJ { get; set; }
    }
}
