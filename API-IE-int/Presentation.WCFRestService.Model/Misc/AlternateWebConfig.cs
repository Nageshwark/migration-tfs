﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class AlternateWebConfig
    {
        public Dictionary<string, string> AlternateConfigKeys = new Dictionary<string, string>();
    }
}
