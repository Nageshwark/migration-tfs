﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc.TTModel
{   
        public enum OfferCase
        {
            FreeNights = 1,
            EarlyBirdDiscount = 2,
            ComplimentaryBreakFast = 3
        }

        public enum PropertyCase
        {
            Hotel = 1,
            Hostel = 2,
            Apartment = 3
        }

        public class TTHotel
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public int _version { get; set; }
            public bool found { get; set; }
            public _Source _source { get; set; }
            public TTInfo _TTInfo { get; set; }
        }

        public class _Source
        {
            public int HotelId { get; set; }
            public string BuildingName { get; set; }
            public string DestinationCode { get; set; }
            public string DestinationName { get; set; }
            public string CountryCode { get; set; }
            public string CountryName { get; set; }
            public int MasterResortId { get; set; }
            public string MasterResortName { get; set; }
            public float CustomerRating { get; set; }
            public bool PopularHotel { get; set; }
            public int RatingLevel { get; set; }
            public string RatingName { get; set; }
            public int Rating { get; set; }
            public string Hotelkey { get; set; }
            public string ClosestAirport { get; set; }
            public string FriendlyUrl { get; set; }
            public bool HotelCompanyData { get; set; }
            public int RegionTTSSLabelD { get; set; }
            public Location Location { get; set; }
            public Address Address { get; set; }
            public Contact Contact { get; set; }
            public Region Region { get; set; }
            public List<Feature> Features { get; set; }
            public List<Image> Images { get; set; }
            public List<Description> Descriptions { get; set; }
            public List<Policy> Policies { get; set; }
        }

        public class Location
        {
            public float lat { get; set; }
            public float lon { get; set; }
        }

        public class Address
        {
            public string AddressLineOne { get; set; }
            public string AddressLineTwo { get; set; }
            public string AddressLineThree { get; set; }
            public string AddressLineFour { get; set; }
            public string City { get; set; }
            public string PostCode { get; set; }
        }

        public class Contact
        {
            public string Telephone { get; set; }
            public string Fax { get; set; }
            public string EMailId { get; set; }
        }

        public class Region
        {
            public int RegionId { get; set; }
            public int ResortId { get; set; }
            public string Regname { get; set; }
            public int Reglevel { get; set; }
            public string AirportCodes { get; set; }
            public int ParentRegionId { get; set; }
            public string ParentRegionName { get; set; }
            public string ParentRegionLink { get; set; }
            public int GrandRegionId { get; set; }
            public string GrandRegionName { get; set; }
            public string GrandRegionLink { get; set; }
        }

        public class Feature
        {
            public string FeatureName { get; set; }
            public List<string> FeaturesList { get; set; }
        }

        public class Image
        {
            public string ImageId { get; set; }
            public string ImageURL { get; set; }
            public string ImageDescription { get; set; }
            public bool DefaultImage { get; set; }
            public string ImageType { get; set; }
        }

        public class Description
        {
            public string DescriptionHeader { get; set; }
            public string DescriptionText { get; set; }
        }

        public class Policy
        {
            public string PolicyName { get; set; }
            public string PolicyDetail { get; set; }
        }

        public class TTInfo
        {
            public List<string> HotelClassifications { get; set; }

            public List<Rating> Ratings { get; set; }

            public List<string> LocationAndNeighbourhood { get; set; }

            public string PropertyType { get; set; }

            public List<OfferTag> OfferTags { get; set; }

            public int NoOfRooms { get; set; }

            public string TARating { get; set; }

            public List<TAReview> Reviews { get; set; }

            public string GIATA { get; set; }

            public string TAID { get; set; }

            public string UpdatePermissionFromHC { get; set; }

            public string TripAdvisorRating { get; set; }

            public string TripAdvisorReviewCount { get; set; }

            public DateTime TripAdvisorCacheCreateDate { get; set; }

    }

        public class Rating
        {
            public string RatingName { get; set; }
            public string RatingValue { get; set; }
        }

        public class OfferTag
        {
            public string Offer { get; set; }
        }

        public class TAReview
        {

        }

        public class TripAdvisorAPIresponseClass
        {
            [Newtonsoft.Json.JsonProperty("masterHotelId")]
            public string MasterHotelID { get; set; }

            [Newtonsoft.Json.JsonProperty("rating")]
            public string rating { get; set; }

            [Newtonsoft.Json.JsonProperty("numberOfReviews")]
            public string reviews_count { get; set; }

            [Newtonsoft.Json.JsonProperty("tripAdvisorId")]
            public string TAID { get; set; }
        }


       
}
