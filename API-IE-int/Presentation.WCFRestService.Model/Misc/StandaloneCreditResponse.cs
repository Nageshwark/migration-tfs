﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class Link
    {
        public string rel { get; set; }
        public string href { get; set; }
    }

    public class _CardExpiry
    {
        public int month { get; set; }
        public int year { get; set; }
    }

    public class _CardResp
    {
        public string type { get; set; }
        public string lastDigits { get; set; }
        public _CardExpiry cardExpiry { get; set; }
    }

    public class _ProfileResp
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
    }

    public class _BillingDetails
    {
        public string street { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
    }

    public class StandaloneCreditResponse
    {
        public List<Link> links { get; set; }
        public string id { get; set; }
        public string merchantRefNum { get; set; }
        public DateTime txnTime { get; set; }
        public string status { get; set; }
        public int amount { get; set; }
        public _CardResp card { get; set; }
        public string authCode { get; set; }
        public _ProfileResp profile { get; set; }
        public _BillingDetails billingDetails { get; set; }
        public string customerIp { get; set; }
        public string description { get; set; }
        public string currencyCode { get; set; }
        public PostCallStatusInfo postCallStatusInfo { get; set; }
    }
}
