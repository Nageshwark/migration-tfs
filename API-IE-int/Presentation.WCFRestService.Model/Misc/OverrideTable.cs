﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class OverrideTable
    {
        public int mhid { get; set; }
        public string hotelName { get; set; }
        public string destination { get; set; }
        public string destinationID { get; set; }
        public int rank  { get; set; }
        public bool toDisplayOnFod { get; set; }
        public string comment { get; set; }

    }
    public class HotelRankingsInfo
    {
        public int mhid { get; set; }
        public string hotelName { get; set; }
        public string destination { get; set; }
        public string destinationID { get; set; }
        public int rank { get; set; }
        public bool toDisplayOnFod { get; set; }
        public string comment { get; set; }
    }
}
