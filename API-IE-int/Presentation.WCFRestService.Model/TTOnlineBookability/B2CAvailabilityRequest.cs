﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class _Room_
    {
        public int Adults { get; set; }
        public List<byte> ChildAges { get; set; }
        public int Children { get; set; }
        public int RoomNumber { get; set; }
    }

    public class B2CAvailabilityRequest
    {
        public object AvailabilityId { get; set; }
        public int Channel { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string ClientIpAddress { get; set; }
        public bool Debugging { get; set; }
        public string DestinationId { get; set; }
        public object EstablishmentId { get; set; }
        public bool IgnoreConsolidation { get; set; }
        public object PromotionalCode { get; set; }
        public object ProvidersToSearch { get; set; }
        public List<_Room_> Rooms { get; set; }
        public int SearchType { get; set; }
        public object SuppliersToSearch { get; set; }
        public bool TC4Origin { get; set; }
    }
}
