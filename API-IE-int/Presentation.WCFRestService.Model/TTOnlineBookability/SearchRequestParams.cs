﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class AvailabilitySearchRequestParams
    {
        public string destinationCode { get; set; }
        public string hotelCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string guestInfo { get; set; } // Format: roomNumber1,adults1,children1,roomNumber2,adults2,children2,......
        //public int adults { get; set; }
        //public int children { get; set; }
        public string timeStamp { get; set; }
        public int uniqueHotelsInserted { get; set; }
        public int availabilityOffersCount { get; set; }
        public int offersInserted { get; set; }
        public double indexingTime { get; set; }
        public double decoratingTime { get; set; }
        public double accommodationSoaResponseTime { get; set; }
        public List<string> boardType { get; set; }
        public bool isCacheHit { get; set; }
        public string roomSearchType { get; set; }
    }
}
