﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class StaticMHResponse
    {
        public StaticSource _source { get; set; }
        public TripAdvisor TripAdvisor { get; set; }
        public DateTime insertionTime { get; set; }
    }
    public class Location
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }

    public class Address
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string CityTown { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
    }

    public class Contact
    {
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string EmailId { get; set; }
    }

    public class StaticSource
    {
        public string HotelId { get; set; }
        public string BuildingName { get; set; }
        public string DefaultAirportCode { get; set; }
        public int DestinationId { get; set; }
        public string DestinationName { get; set; }
        public int ParentDestinationId { get; set; }
        public string ParentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string CountryName { get; set; }
        public int Rating { get; set; }
        public Location Location { get; set; }
        public Address Address { get; set; }
        public Contact Contact { get; set; }
        public string ShortDescription { get; set; }
        public string SEOFriendlyName { get; set; }
    }

    public class TripAdvisor
    {
        public string TAID { get; set; }
        public int ReviewsCount { get; set; }
        public float TARating { get; set; }
    }
}
