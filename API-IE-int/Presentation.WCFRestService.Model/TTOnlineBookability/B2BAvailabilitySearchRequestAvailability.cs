﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class B2BAvailabilitySearchRequestAvailability
    {
        public int availabilityOffersCount { get; set; }
    }
}
