﻿/*
 *  This class describes the object format in which the response is sent back to FE 
 */ 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class AvailabilityOffers
    {
        public List<AOffers> offers { get; set; }
        public AvailabilityFacets facets { get; set; }
        public  int totalHotels { get; set; }
        public bool isCacheHit { get; set; }
        public int B2CAvailabilityTotalHotelCount { get; set; }
    }
    public class AvailabilityFacets
    {
        public Pricerange priceRange { get; set; }
        public Dictionary<string,int> starRatingCardinality { get; set; }
        public Dictionary<string,int> tripAdvisorCardinality { get; set; }
    }
    public class Pricerange
    {
        public int min { get; set; }
        public int max { get; set; }
    }
    public class AOffers
    {
        public Guid availabilityId { get; set; }
        public Guid hotelCode { get; set; }
        public int hotelId { get; set; }
        public string hotelCity { get; set; }
        public string hotelName { get; set; }
        public Guid destinationCode { get; set; }
        public int TTRegionId { get; set; }
        public string phoneNumber { get; set; }
        //public int destinationId { get; set; }
        public string destinationName { get; set; }
        public string parentDestinationCode { get; set; }
        public string parentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string countryName { get; set; }
        public string defaultAirportCode { get; set; }
        public int rank { get; set; }
        public int mhid { get; set; }
        public TTLocation location { get; set; }
        public List<object> hotelFacilities { get; set; }
        public List<object> roomFacilities { get; set; }
        public List<object> hotelGeneralInfo { get; set; }
        public string description { get; set; }
        // public string shortDescription { get; set; }
        public int rating { get; set; }
        public List<string> images { get; set; }
        public List<string> thumbnailimages { get; set; }
        public TADetails taInfo { get; set; }
        public List<Room> rooms { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public bool isTTImg { get; set; }
        public bool isAlphaImg { get; set; }
    }

    public class Room
    {
        public int roomNumber { get; set; }
        public List<RoomPrice> roomPrices { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
    }
    public class RoomPrice
    {
        public int roomId { get; set; }
        public int boardTypeCode { get; set; }
        public string boardType { get; set; }
        public string roomDescription { get; set; }
        public string totalPrice { get; set; }
        public string highestPrice { get; set; }
    }
    public class TADetails
    {
        public string tripAdvisorId { get; set; }
        public double averageRating { get; set; }
        public int reviewCount { get; set; }
    }
    public class TTLocation
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
