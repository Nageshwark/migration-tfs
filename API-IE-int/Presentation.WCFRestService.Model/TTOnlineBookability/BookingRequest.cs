﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class Customer
    {
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public int Id { get; set; }
        public string Surname { get; set; }
        public int Site { get; set; }
        public string Title { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public int? UserId { get; set; }
        public int Channel { get; set; }
        public int LanguageId { get; set; }
        public int? B2BUserId { get; set; }
    }

    public class Guest
    {
        public int? Age { get; set; }
        public string FirstName { get; set; }
        public int Id { get; set; }
        public string Surname { get; set; }
        public string TitleString { get; set; }
    }

    public class CardExpireDate
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class PaymentDetails
    {
        public int CardType { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public CardExpireDate CardExpireDate { get; set; }
        public string CardSecurityCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string County { get; set; }
    }

    public class SpecialRequests
    {
        public bool AdjoiningRooms { get; set; }
        public bool CotRequired { get; set; }
        public bool DisabledAccess { get; set; }
        public bool LateArrival { get; set; }
        public bool NonSmoking { get; set; }
        public object OtherRequests { get; set; }
        public bool SeaViews { get; set; }
    }

    public class ValuatedRoom
    {
        public List<Guest> Guests { get; set; }
        public PaymentDetails PaymentDetails { get; set; }
        public SpecialRequests SpecialRequests { get; set; }
        public string ValuatedRoomId { get; set; }
    }

    public class BookingRequest
    {
        public string AvailabilityId { get; set; }
        public object BookingId { get; set; }
        public int Channel { get; set; }
        public Customer Customer { get; set; }
        public bool Debugging { get; set; }
        public PaymentDetails PaymentDetails { get; set; }
        public List<ValuatedRoom> ValuatedRooms { get; set; }
        public string ValuationId { get; set; }
    }
}
