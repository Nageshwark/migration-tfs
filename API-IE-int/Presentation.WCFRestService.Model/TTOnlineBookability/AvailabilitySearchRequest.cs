﻿/*
 *  This is the class which
 *  will deserialize the object received from WebSite to build the availability
 *  Request Object 
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class AvailabilitySearchRequest
    {
        public string destinationCode { get; set; }
        public string destinationIds { get; set; }
        public string hotelCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public List<GuestsPerRoom> roomList { get; set; }
        public string rating { get; set; }
        public float tripAdvisorRating { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public string sort { get; set; }
        public List<string> boardType { get; set; }
        public string hotelCodesToExclude { get; set; }
        public string searchRequestType { get; set; }
        public Guid availabilityId { get; set; }
        public string roomSearchType { get; set; }
        public string guestInfoSearchRequest { get; set; }
        public int AvailabilityTotalResultsCount { get; set; }
    }

    public class GuestsPerRoom
    {
        public int adults { get; set; }
        public int children { get; set; }
        public List<byte> ages { get; set; }
    }
}
