﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }

    public class _Source
    {
        public string destinationCode { get; set; }
        public string hotelCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public DateTime timeStamp { get; set; }
        public int offersDecorated { get; set; }
        public int availabilityOffersCount { get; set; }
        public int offersInserted { get; set; }
        public double indexingTime { get; set; }
        public double decoratingTime { get; set; }
        public List<string> boardType { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public _Source _source { get; set; }
    }

    public class _Hits
    {
        public int total { get; set; }
        public double? max_score { get; set; }
        public List<Hit> hits { get; set; }
    }

    public class SearchResponseParams
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public _Hits hits { get; set; }
    }
}
