﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }

    public class Source
    {
        public Guid availabilityId { get; set; }
        public Guid hotelCode { get; set; }
        public int hotelId { get; set; }
        public string hotelName { get; set; }
        public string defaultAirportCode { get; set; }
        public Guid destinationCode { get; set; }
        public int TTRegionId { get; set; }
        //public int destinationId { get; set; }
        public string destinationName { get; set; }
        public string parentDestinationCode { get; set; }
        public string parentDestinationName { get; set; }
        public string ISOCountryCode { get; set; }
        public string countryName { get; set; }
        public List<object> hotelFacilities { get; set; }
        public List<object> roomFacilities { get; set; }
        public List<object> hotelGeneralInfo { get; set; }
        public int mhid { get; set; }
        public int rank { get; set; }
        public int starRating { get; set; }
        public string tripAdvisorId { get; set; }
        public double averageRating { get; set; }
        public int reviewCount { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public int roomNumber { get; set; }
        public int roomId { get; set; }
        public int boardType { get; set; }
        public string boardDescription { get; set; }
        public string roomDescription { get; set; }
        public double totalPrice { get; set; }
        public double highestPrice { get; set; }
        public string currencyCode { get; set; }
        public bool isBidingRate { get; set; }
        public bool IsActiveAccommodationAdminFee { get; set; }
        public bool IsOpaqueRate { get; set; }
        public bool IsCardChargeApplicable { get; set; }
        public bool IsDuplicateRate { get; set; }
        public bool IsEligibleForDeposit { get; set; }
        public bool IsNonRefundable { get; set; }
        public bool IsSpecialRate { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public DateTime timeStamp { get; set; }
        public string providerEdiCode { get; set; }
        public string providerEstablishmentCode { get; set; }
        public string providerName { get; set; }
        public double providerPrice { get; set; }
        public string providerCurrencyCode { get; set; }
        public string providerRoomCode { get; set; }
        public string hotelSEOFriendlyName { get; set; }
        public string destinationSEOFriendlyName { get; set; }
    }

    public class AvailabilitySearchHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int? _score { get; set; }
        public Source _source { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public double? max_score { get; set; }
        public List<AvailabilitySearchHit> hits { get; set; }
    }

    public class RootObject
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
    }
    ////public class Shards
    ////{
    ////    public int total { get; set; }
    ////    public int successful { get; set; }
    ////    public int skipped { get; set; }
    ////    public int failed { get; set; }
    ////}

    ////public class Hits
    ////{
    ////    public int total { get; set; }
    ////    public double? max_score { get; set; }
    ////    public List<AvailabilitySearchHit> hits { get; set; }
    ////}

    ////public class Source
    ////{
    ////    public string hotelCode { get; set; }
    ////    public int hotelId { get; set; }
    ////    public string hotelCity { get; set; }
    ////    public string hotelName { get; set; }
    ////    public string defaultAirportCode { get; set; }
    ////    public string destinationCode { get; set; }
    ////    public int destinationId { get; set; }
    ////    public string destinationName { get; set; }
    ////    public int parentDestinationId { get; set; }
    ////    public string parentDestinationName { get; set; }
    ////    public string ISOCountryCode { get; set; }
    ////    public string countryName { get; set; }
    ////    public int rank { get; set; }
    ////    public int mhid { get; set; }
    ////    public int starRating { get; set; }
    ////    public string tripAdvisorId { get; set; }
    ////    public float averageRating { get; set; }
    ////    public int reviewCount { get; set; }
    ////    public string lat { get; set; }
    ////    public string lon { get; set; }
    ////    public string roomId { get; set; }
    ////    public string boardType { get; set; }
    ////    public string boardTypeCode { get; set; }
    ////    public string roomStayCandidateRPH { get; set; }
    ////    public string roomDescription { get; set; }
    ////    public double basePrice { get; set; }
    ////    public int commission { get; set; }
    ////    public int vat { get; set; }
    ////    public double totalPrice { get; set; }
    ////    public string currency { get; set; }
    ////    public bool isBidingRate { get; set; }
    ////    public bool isDirectPayment { get; set; }
    ////    public bool IsOpaqueRate { get; set; }
    ////    public bool IsRefundableRate { get; set; }
    ////    public bool IsSpecialRate { get; set; }
    ////    public int adults { get; set; }
    ////    public int children { get; set; }
    ////    public string checkInDate { get; set; }
    ////    public string checkOutDate { get; set; }
    ////    public DateTime timeStamp { get; set; }
    ////    public int offersDecorated { get; set; }
    ////    public int availabilityOffersCount { get; set; }
    ////    public int offersInserted { get; set; }
    ////    public double indexingTime { get; set; }
    ////    public double decoratingTime { get; set; }
    ////}

    ////public class AvailabilitySearchHit
    ////{
    ////    public string _index { get; set; }
    ////    public string _type { get; set; }
    ////    public string _id { get; set; }
    ////    public object _score { get; set; }
    ////    public Source _source { get; set; }
    ////    public List<double> sort { get; set; }
    ////}

    public class AggrHits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public List<AvailabilitySearchHit> hits { get; set; }
    }

    public class TopPrice
    {
        public AggrHits hits { get; set; }
    }

    public class MinRank
    {
        public double value { get; set; }
    }

    public class AvailabilityOfferBucket
    {
        public int key { get; set; }
        public int doc_count { get; set; }
        public TopPrice top_price { get; set; }
        public MinRank min_rank { get; set; }
    }

    public class GroupByHotelId
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<AvailabilityOfferBucket> buckets { get; set; }
    }

    public class Aggregations
    {
        public GroupByHotelId group_by_hotelId { get; set; }
    }

    public class AvailabilityOffersResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
    }
}
