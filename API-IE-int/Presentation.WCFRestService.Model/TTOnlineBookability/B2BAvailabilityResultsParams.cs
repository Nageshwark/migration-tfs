﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.TTOnlineBookability
{
    public class B2BAvailabilityResultsParams
    {
        public int availabilityOffersCount { get; set; }
        public int uniqueHotelsInserted { get; set; }
        public int offersInserted { get; set; }
        public double indexingTime { get; set; }
        public double decoratingTime { get; set; }
        public double accomodationSoaResponseTime { get; set; }
        public bool isBulkInsertionOnRetry { get; set; }
        public int facetsTrials { get; set; }
        public bool isResultFromB2C { get; set; }
        public Guid availabilityId { get; set; }
        public double priceMin { get; set; }
        public double priceMax { get; set;}
    }
}
