﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ContentManagementTool
{
    public class HCAddress
    {
        public string AddressLineFour { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineThree { get; set; }
        public string AddressLineTwo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }

    public class HCContact
    {
        public string EMailId { get; set; }
        public string Fax { get; set; }
        public string Telephone { get; set; }
    }

    public class HCDescription
    {
        public string DescriptionHeader { get; set; }
        public string DescriptionText { get; set; }
    }

    public class HCFeature
    {
        public string FeatureName { get; set; }
        public List<object> FeaturesList { get; set; }
    }

    public class HCImage
    {
        public bool DefaultImage { get; set; }
        public string ImageDescription { get; set; }
        public string ImageId { get; set; }
        public string ImageType { get; set; }
        public string ImageURL { get; set; }
    }

    public class HCLocation
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }

    public class HCRegion
    {
        public string AirportCodes { get; set; }
        public int GrandRegionId { get; set; }
        public string GrandRegionLink { get; set; }
        public string GrandRegionName { get; set; }
        public int ParentRegionId { get; set; }
        public string ParentRegionLink { get; set; }
        public string ParentRegionName { get; set; }
        public int RegionId { get; set; }
        public int Reglevel { get; set; }
        public string Regname { get; set; }
        public int ResortId { get; set; }
    }

    public class HCSupplierInfo
    {
        public string SupplierHotelCode { get; set; }
        public string SupplierName { get; set; }
    }

    public class HCHotelinfoResult
    {
        public HCAddress Address { get; set; }
        public string BuildingName { get; set; }
        public object ChainCode { get; set; }
        public string ClosestAirport { get; set; }
        public HCContact Contact { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public double CustomerRating { get; set; }
        public List<HCDescription> Descriptions { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
        public List<HCFeature> Features { get; set; }
        public string FriendlyUrl { get; set; }
        public bool HotelCompanyData { get; set; }
        public int HotelId { get; set; }
        public string Hotelkey { get; set; }
        public List<HCImage> Images { get; set; }
        public HCLocation Location { get; set; }
        public int MasterResortId { get; set; }
        public string MasterResortName { get; set; }
        public object Policies { get; set; }
        public bool PopularHotel { get; set; }
        public int Rating { get; set; }
        public int RatingLevel { get; set; }
        public string RatingName { get; set; }
        public HCRegion Region { get; set; }
        public int RegionTTSSLabelD { get; set; }
        public List<HCSupplierInfo> SupplierInfo { get; set; }
    }

    public class HCHotelInformation
    {
        public HCHotelinfoResult HotelinfoResult { get; set; }
    }
}
