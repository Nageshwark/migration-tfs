﻿using MySql.Data.MySqlClient;
using Presentation.WCFRestService.Model.Blogs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Presentation.Web.WCFRestServices.Blogs
{
    public class Blogs
    {
        private static readonly Random random = new Random();
        public static string GetBlogs()
        {
            DataSet ds = ExecuteQuery("Get_Blog_SiteMap");
            DataRowCollection rows = ds.Tables[0].Rows;
            urlset urlSet = new urlset();
            urlSet.Urls = new List<url>();
            if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int index = ds.Tables[0].Rows.Count - 1; index >= 0; index--)
                {
                    url url = new url();
                    url.loc = "https://www.teletextholidays.co.uk/" + (string)ds.Tables[0].Rows[index]["aliasUrl"];
                    url.changefreq = "weekly";
                    Random r = new Random();
                    url.priority = Math.Round(RandomNumberBetween(0.5, 0.8),2).ToString().Substring(0,3);
                    urlSet.Urls.Add(url);
                }
            }

            XmlSerializer xsSubmit = new XmlSerializer(typeof(urlset));            
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, urlSet);
                    xml = sww.ToString();
                }
            }

            return xml.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"","").Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "").Replace("<Urls>","").Replace("</Urls>","");
        }
        private static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }
        private static DataSet ExecuteQuery(string procName, List<MySqlParameter> procParams = null)
        {
            DataSet ds = null;
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ttBlog"].ToString()))
            {
                con.Open();

                MySqlCommand com = new MySqlCommand();
                com.Connection = con;

                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                if (procParams != null && procParams.Count != 0)
                {
                    foreach (MySqlParameter param in procParams)
                    {
                        com.Parameters.Add(param);
                    }
                }

                MySqlDataAdapter adap = new MySqlDataAdapter(com);
                ds = new DataSet();

                try
                {
                    adap.Fill(ds);
                }
                catch (Exception ex)
                {
                    con.Close();

                    throw ex;
                }

                con.Close();
            }
            return ds;
        }
    }
}