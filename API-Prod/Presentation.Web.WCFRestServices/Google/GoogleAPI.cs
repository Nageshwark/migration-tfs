﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace Presentation.Web.WCFRestServices.Google
{
    public class GoogleAPI
    {
        public static bool GetAirportInfo(string airport, string key)
        {
            GoogleAirport googleAirport = new GoogleAirport();
            string airportkey = airport.ToLower();
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            string awsInputFolderKeyName = "Airports/";
            int resultsCount = 0;
            bool isS3Insertion = false;
            IAmazonS3 client;
            string airportUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=airports+in+" + airport + "&key=" + key;
            var airportResponse = Utilities.ExecuteGetWebRequest(airportUrl);

            AirportResult airportResult;
            try
            {
                GoogleAirport googleobject = JsonConvert.DeserializeObject<GoogleAirport>(airportResponse);
                resultsCount = googleobject.results.Count;
                googleAirport.status = googleobject.status;
                googleAirport.results = new List<AirportResult>();
                foreach (AirportResult airResults in googleobject.results)
                {
                    airportResult = new AirportResult();
                    airportResult.geometry = airResults.geometry;
                    airportResult.geometry.location.lat = airResults.geometry.location.lat;
                    airportResult.geometry.location.lng = airResults.geometry.location.lng;
                    airportResult.name = airResults.name;
                    airportResult.types = airResults.types;
                    googleAirport.results.Add(airportResult);
                }
                if (resultsCount != 0)
                {
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName + airportkey + ".json"
                        };
                        request.InputStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(googleAirport)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    isS3Insertion = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Google Airport Response Error " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return isS3Insertion;
        }
        public static bool SetGoogleAttractions(string lat, string lon, string patternType, string attractions, string radius, string googleKey, string apiKey)
        {            
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            bool isS3Insertion = false;
            int resultsCount = 0;
            IAmazonS3 client;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            var s3Response = String.Empty;
            searchParams.Append("location=" + lat + "," + lon + "&");
            if (attractions == "attractions")
            {
                searchParams.Append("query=" + attractions + "&");
            }
            else
            {
                searchParams.Append("type=" + attractions + "&");
            }
            searchParams.Append("key=" + apiKey + "&");
            searchParams.Append("radius=" + radius + "&");
            string paramsString = searchParams.ToString();
            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            string url = string.Empty;
            if(attractions == "attractions")
            {
                url = ConfigurationManager.AppSettings["PubsAndRestruantsGoogleURL"] + paramsString;
            }
            else
            {
                url = ConfigurationManager.AppSettings["PubsAndRestruantsGoogleTextURL"] + paramsString;
            }
            var googleResponse = Utilities.ExecuteGetWebRequest(url);
            GoogleBARObject resultgoogleobject = new GoogleBARObject();
            try
            {
                GoogleBARObject googleobject = JsonConvert.DeserializeObject<GoogleBARObject>(googleResponse);
                resultsCount = googleobject.results.Count;
                if (resultsCount != 0)
                {
                    if (attractions.Contains(","))
                    {
                        attractions = "Attractions";
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = patternType.ToUpper() + "/" + attractions.ToUpper() + "/" + lat + "tt" + lon + ".json"
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(googleResponse));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    isS3Insertion = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return isS3Insertion;
        }
        public static bool GetBarsAndRestaurants(string lat, string lng, string type, string googleKey, string apiKey)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            string awsInputFolderKeyName = "BarsandRestaurants/";
            bool isS3Insertion = false;
            int resultsCount = 0;
            IAmazonS3 client;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            string radius = ConfigurationManager.AppSettings["radius"].ToString();
            var s3Response = String.Empty;

            if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lng))
            {
                searchParams.Append("location=" + lat + "," + lng + "&");
            }

            if (!string.IsNullOrEmpty(type))
            {
                searchParams.Append("types=" + type + "&");
            }
            searchParams.Append("key=" + apiKey + "&");
            if (!string.IsNullOrEmpty(radius))
            {
                searchParams.Append("radius=" + radius + "&");
            }
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            string url = ConfigurationManager.AppSettings["PubsAndRestruantsGoogleURL"] + paramsString;
            var googleResponse = Utilities.ExecuteGetWebRequest(url);
            GoogleBARObject resultgoogleobject = new GoogleBARObject();
            Result result;
            try
            {
                GoogleBARObject googleobject = JsonConvert.DeserializeObject<GoogleBARObject>(googleResponse);
                resultsCount = googleobject.results.Count;
                resultgoogleobject.results = new List<Result>();
                foreach (Result rst in googleobject.results)
                {
                    result = new Result();
                    result.geometry = rst.geometry;
                    result.geometry.location.lat = rst.geometry.location.lat;
                    result.geometry.location.lng = rst.geometry.location.lng;
                    result.id = rst.id;
                    result.name = rst.name;
                    result.rating = rst.rating;
                    result.vicinity = rst.vicinity;
                    resultgoogleobject.results.Add(result);
                }
                if (resultsCount != 0)
                {
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName + googleKey + ".json"
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(resultgoogleobject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    isS3Insertion = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return isS3Insertion;
        }
        public static bool GetGoogleNearBy(string lat, string lon, string patternType, string type, string radius)
        {
            bool isS3Insertion = false;
            bool isS3Present = true;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            string attractions = type;
            if (string.IsNullOrWhiteSpace(type))
            {
                attractions = "attractions";
            }
            string key = patternType + "/" + attractions.ToUpper() + "/" + lat+"tt"+ lon+ ".json";
            IAmazonS3 client;
            var result = string.Empty;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                try
                {
                    Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = key
                    };
                    Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                    var lastCreated = objResponse.LastModified;
                    if (DateTime.UtcNow.Subtract(lastCreated.AddDays(int.Parse(ConfigurationManager.AppSettings["GoogleAPIExpiry"]))).TotalMilliseconds > 0)
                    {
                        int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
                        var s3Response = String.Empty;
                        string googleKey = key;
                        bool googleResponse = false;
                        while (!googleResponse && keyCount > 0)
                        {
                            string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                            string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                            googleResponse = GoogleAPI.SetGoogleAttractions(lat, lon, patternType,  attractions, radius, googleKey, googleAPIKey);
                            keyCount--;
                        }
                    }
                    isS3Insertion = true;
                }
                catch (AmazonS3Exception ex)
                {
                    isS3Present = false;
                }

            }
            if (!isS3Present)
            {
                int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
                var s3Response = String.Empty;
                string googleKey = key;
                bool googleResponse = false;
                while (!googleResponse && keyCount > 0)
                {
                    string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                    string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                    googleResponse = GoogleAPI.SetGoogleAttractions(lat, lon, patternType,  attractions, radius, googleKey, googleAPIKey);
                    keyCount--;
                }
                return googleResponse;
            }
            return isS3Insertion;
        }

    }
}