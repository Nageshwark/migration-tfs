﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using Presentation.Web.WCFRestServices.RedisDataAccessHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Presentation.WCFRestService.Model.Routing;
using Airport = Presentation.WCFRestService.Model.Routing.Airport;
using Newtonsoft.Json.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Device.Location;
using System.Web;

namespace Presentation.Web.WCFRestServices
{
    public static class Utilities
    {
        private static Random random;
        private static object syncObj = new object();
        private static void InitRandomNumber(int seed)
        {
            random = new Random(seed);
        }

        public static int GenerateRandomNumber(int max)
        {
            lock (syncObj)
            {
                if (random == null)
                    random = new Random(); // Or exception...
                return random.Next(max);
            }
        }


        //IE CODE
        /// <summary>
        /// Get parent departure id - handles departures with multiple parent ids
        /// </summary>
        /// <param name="countrySite"></param>
        /// <param name="resultDepartureId"></param>
        /// <param name="requestDepartureId"></param>
        /// <returns></returns>
        public static int GetParentDepartureId(string countrySite, int resultDepartureId, int requestDepartureId)
        {
            List<int> departureCodes = Global.parentAirportCodesMultiMT[countrySite.ToLower()][resultDepartureId];

            //if requestDepartureId is a parent, match result to the same parent
            if (requestDepartureId < 0)
            {
                if (departureCodes.Contains(requestDepartureId))
                {
                    return requestDepartureId;
                }
            }

            //all other cases - list should have single value, so return first value
            return departureCodes.First();
        }

        //IE CODE
        public static string GetWebConfigValueForMPT(string countrySite, string ukConfigKey)
        {
            if (countrySite != Global.COUNTRY_SITE_UK)
            {
                ukConfigKey += "-" + countrySite.ToUpper();
            }

            return ConfigurationManager.AppSettings[ukConfigKey];
        }

        public static string GetRedisConnectionStringForRoutingApiIE()
        {
            string redisConnectionString = String.Empty;

            if (!String.IsNullOrEmpty(Global.RedisPasswordForRoutingApiIE))
            {
                redisConnectionString = Global.RedisPasswordForRoutingApiIE + "@" + Global.RedisHostForRoutingApiIE + ":" +
                                                     Global.RedisPortForRoutingApiIE;
            }
            else
            {
                redisConnectionString = Global.RedisHostForRoutingApiIE + ":" + Global.RedisPortForRoutingApiIE;
            }

            return redisConnectionString;
        }

        public static string GetRedisConnectionStringForRoutingApi()
        {
            string redisConnectionString = String.Empty;

            if (!String.IsNullOrEmpty(Global.RedisPasswordForRoutingApi))
            {
                redisConnectionString = Global.RedisPasswordForRoutingApi + "@" + Global.RedisHostForRoutingApi + ":" +
                                                     Global.RedisPortForRoutingApi;
            }
            else
            {
                redisConnectionString = Global.RedisHostForRoutingApi + ":" + Global.RedisPortForRoutingApi;
            }

            return redisConnectionString;
        }

        public static List<AirportGroup> GetAirportsFromXmlString(string airportXmlData)
        {
            List<AirportGroup> airportGroups = new List<AirportGroup>();

            XmlDocument airportXmlDocument = new XmlDocument();
            airportXmlDocument.LoadXml(airportXmlData);

            XmlNodeList airportGroupNodeList = airportXmlDocument.SelectNodes("descendant::Airports/AirportGroup");

            if (airportGroupNodeList != null && airportGroupNodeList.Count > 0)
            {
                foreach (XmlNode airportGroupNode in airportGroupNodeList)
                {
                    AirportGroup airportGroup = new AirportGroup();

                    string airportGroupId = ((XmlElement)airportGroupNode).GetAttribute("Id");

                    airportGroup.Id = Int32.Parse(airportGroupId);
                    airportGroup.Value = ((XmlElement)airportGroupNode).GetAttribute("Value");

                    if (!Global.AirportGroupIdOrAirportIdToAirportsMapping.ContainsKey(airportGroup.Id.ToString()))
                    {
                        Global.AirportGroupIdOrAirportIdToAirportsMapping.Add(airportGroup.Id.ToString(), airportGroup.Airports.Select(a => a.Id.ToString()).ToList());
                    }

                    XmlNodeList airportNodeList = airportGroupNode.SelectNodes("descendant::Airport");

                    if (airportNodeList != null)
                    {
                        foreach (XmlNode airportNode in airportNodeList)
                        {
                            Airport airport = new Airport();

                            string airportId = ((XmlElement)airportNode).GetAttribute("Id");
                            airport.Id = Int32.Parse(airportId);

                            airport.Code = ((XmlElement)airportNode).GetAttribute("Code");
                            airport.Value = ((XmlElement)airportNode).GetAttribute("Value");

                            airportGroup.Airports.Add(airport);

                            if (Global.AirportCodeToMultipleDepartureIdsMapping.ContainsKey(airport.Code))
                            {
                                List<int> existingDepartureIds = Global.AirportCodeToMultipleDepartureIdsMapping[airport.Code];
                                existingDepartureIds.Add(airportGroup.Id);
                            }
                            else
                            {
                                List<int> departureIds = new List<int> { airport.Id, airportGroup.Id };
                                Global.AirportCodeToMultipleDepartureIdsMapping.Add(airport.Code, departureIds);
                            }

                            if (!Global.AirportCodeToDepartureIdMapping.ContainsKey(airport.Code))
                            {
                                Global.AirportCodeToDepartureIdMapping.Add(airport.Code, airport.Id);
                            }

                            if (!Global.AirportGroupIdOrAirportIdToAirportsMapping.ContainsKey(airport.Id.ToString()))
                            {
                                Global.AirportGroupIdOrAirportIdToAirportsMapping.Add(airport.Id.ToString(), new List<string>() { airport.Id.ToString() });
                            }

                            List<string> existingAirportsForAirportGroup =
                                Global.AirportGroupIdOrAirportIdToAirportsMapping[airportGroupId];
                            existingAirportsForAirportGroup.Add(airport.Id.ToString());
                            Global.AirportGroupIdOrAirportIdToAirportsMapping.Remove(airportGroupId);
                            Global.AirportGroupIdOrAirportIdToAirportsMapping.Add(airportGroupId, existingAirportsForAirportGroup);

                        }
                    }
                    airportGroups.Add(airportGroup);
                }
            }

            return airportGroups;
        }


        //IE CODE
        public static List<AirportGroup> GetAirportsFromXmlStringMT(string airportXmlData)
        {
            List<AirportGroup> airportGroups = new List<AirportGroup>();

            XmlDocument airportXmlDocument = new XmlDocument();
            airportXmlDocument.LoadXml(airportXmlData);

            XmlNodeList airportGroupNodeList = airportXmlDocument.SelectNodes("descendant::Airports/AirportGroup");

            if (airportGroupNodeList != null && airportGroupNodeList.Count > 0)
            {
                foreach (XmlNode airportGroupNode in airportGroupNodeList)
                {
                    AirportGroup airportGroup = new AirportGroup();

                    string airportGroupId = ((XmlElement)airportGroupNode).GetAttribute("Id");

                    airportGroup.Id = Int32.Parse(airportGroupId);
                    airportGroup.Value = ((XmlElement)airportGroupNode).GetAttribute("Value");

                    if (!Global.AirportGroupIdOrAirportIdToAirportsMappingMT.ContainsKey(airportGroup.Id.ToString()))
                    {
                        Global.AirportGroupIdOrAirportIdToAirportsMappingMT.Add(airportGroup.Id.ToString(), airportGroup.Airports.Select(a => a.Id.ToString()).ToList());
                    }

                    XmlNodeList airportNodeList = airportGroupNode.SelectNodes("descendant::Airport");

                    if (airportNodeList != null)
                    {
                        foreach (XmlNode airportNode in airportNodeList)
                        {
                            Airport airport = new Airport();

                            string airportId = ((XmlElement)airportNode).GetAttribute("Id");
                            airport.Id = Int32.Parse(airportId);

                            airport.Code = ((XmlElement)airportNode).GetAttribute("Code");
                            airport.Value = ((XmlElement)airportNode).GetAttribute("Value");

                            airportGroup.Airports.Add(airport);

                            if (Global.AirportCodeToMultipleDepartureIdsMappingMT.ContainsKey(airport.Code))
                            {
                                List<int> existingDepartureIds = Global.AirportCodeToMultipleDepartureIdsMappingMT[airport.Code];
                                existingDepartureIds.Add(airportGroup.Id);
                            }
                            else
                            {
                                List<int> departureIds = new List<int> { airport.Id, airportGroup.Id };
                                Global.AirportCodeToMultipleDepartureIdsMappingMT.Add(airport.Code, departureIds);
                            }

                            if (!Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airport.Code))
                            {
                                Global.AirportCodeToDepartureIdMappingMT.Add(airport.Code, airport.Id);
                            }

                            if (!Global.AirportGroupIdOrAirportIdToAirportsMappingMT.ContainsKey(airport.Id.ToString()))
                            {
                                Global.AirportGroupIdOrAirportIdToAirportsMappingMT.Add(airport.Id.ToString(), new List<string>() { airport.Id.ToString() });
                            }

                            List<string> existingAirportsForAirportGroup =
                                Global.AirportGroupIdOrAirportIdToAirportsMappingMT[airportGroupId];
                            existingAirportsForAirportGroup.Add(airport.Id.ToString());
                            Global.AirportGroupIdOrAirportIdToAirportsMappingMT.Remove(airportGroupId);
                            Global.AirportGroupIdOrAirportIdToAirportsMappingMT.Add(airportGroupId, existingAirportsForAirportGroup);

                        }
                    }
                    airportGroups.Add(airportGroup);
                }
            }

            return airportGroups;
        }

        public static string GetJson<T>(string url)
        {
            string json = null;
            try
            {
                string cache_key = typeof(T).FullName + ":" + url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                request.Proxy = null;
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    json = reader.ReadToEnd();
                }
                return json;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("url=" + url + "-Description=" + ex.ToString(), LogLevel.Error);
                return "";
            }
        }

        public static List<ArtirixDataItem> GetArtirixHomepageData()
        {
            List<ArtirixDataItem> artirixData = null;
            try
            {
                string webReqURL = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"].ToString() + "minprices/all";
                string json = string.Empty;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ArtirixUserId"].ToString()))
                {
                    string username = ConfigurationManager.AppSettings["ArtirixUserId"].ToString();
                    string password = ConfigurationManager.AppSettings["ArtirixUserPassword"].ToString();
                    CredentialCache mycache = new CredentialCache();
                    mycache.Add(new Uri(webReqURL), "Basic", new NetworkCredential(username, password));
                    httpWebRequest.Credentials = mycache;
                }
                httpWebRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Accept = "application/json";
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    json = reader.ReadToEnd();
                    artirixData = JsonConvert.DeserializeObject<List<ArtirixDataItem>>(json);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return artirixData;
        }

        public static string ExecutePostWebRequest(string url, string postData)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.Accept = "application/xml";
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            var result = reader.ReadToEnd();
            stream.Dispose();
            reader.Dispose();
            return result;
        }

        public static StandaloneCreditResponse ExecutePostWebRequestWithBasicAuthorization(string url, string postData, string apiKey)
        {
            StandaloneCreditResponse standaloneCreditResp = new StandaloneCreditResponse();
            PostCallStatusInfo postCallStatusInfo = new PostCallStatusInfo();
            var result = string.Empty;
            string statusCode = string.Empty;
            string statusDescription = string.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                string base64Encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(apiKey));
                request.Headers.Add("Authorization", "Basic " + base64Encoded);
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();
                standaloneCreditResp = JsonConvert.DeserializeObject<StandaloneCreditResponse>(result);
                postCallStatusInfo.statusCode = "200";
                postCallStatusInfo.statusDescription = ConfigurationManager.AppSettings["PaysafeSuccessMsg"];

                standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
            }
            catch(WebException ex)
            {                
                if(ex.Message.Contains("409"))
                {
                    if(ex.Response.Headers.GetValues("X-Application-Status-Code")[0].Equals("5031"))
                    {
                        statusCode = "409";
                        statusDescription = "The transaction you have submitted has already been processed.";
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1} & StatusCode: {2} & StatusDescription: {3} & X-Application-Status-Code: {4}", ex.Message, ex.StackTrace, statusCode, statusDescription, ex.Response.Headers.GetValues("X-Application-Status-Code")[0]), LogLevel.PaySafeStandaloneCredit);
                        postCallStatusInfo.statusCode = statusCode;
                        postCallStatusInfo.statusDescription = statusDescription;
                        postCallStatusInfo.X_Application_Status_Code = ex.Response.Headers.GetValues("X-Application-Status-Code")[0];

                        standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
                    }
                }
                else if(ex.Message.Contains("400"))
                {
                    if (ex.Response.Headers.GetValues("X-Application-Status-Code")[0].Equals("5042"))
                    {
                        statusCode = "400";
                        statusDescription = "The merchant reference number is missing or invalid or it exceeds the maximum permissible length.";
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1} & StatusCode: {2} & StatusDescription: {3} & X-Application-Status-Code: {4}", ex.Message, ex.StackTrace, statusCode, statusDescription, ex.Response.Headers.GetValues("X-Application-Status-Code")[0]), LogLevel.PaySafeStandaloneCredit);
                        standaloneCreditResp.postCallStatusInfo.statusCode = statusCode;
                        standaloneCreditResp.postCallStatusInfo.statusDescription = statusDescription;
                        standaloneCreditResp.postCallStatusInfo.X_Application_Status_Code = ex.Response.Headers.GetValues("X-Application-Status-Code")[0];

                        standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
                    }
                    else if(ex.Response.Headers.GetValues("X-Application-Status-Code")[0].Equals("5068"))
                    {
                        statusCode = "400";
                        statusDescription = "Either you submitted a request that is missing a mandatory field or the value of a field does not match the format expected.";
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1} & StatusCode: {2} & StatusDescription: {3} & X-Application-Status-Code: {4}", ex.Message, ex.StackTrace, statusCode, statusDescription, ex.Response.Headers.GetValues("X-Application-Status-Code")[0]), LogLevel.PaySafeStandaloneCredit);
                        standaloneCreditResp.postCallStatusInfo.statusCode = statusCode;
                        standaloneCreditResp.postCallStatusInfo.statusDescription = statusDescription;
                        standaloneCreditResp.postCallStatusInfo.X_Application_Status_Code = ex.Response.Headers.GetValues("X-Application-Status-Code")[0];

                        standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
                    }
                    else
                    {
                        statusCode = "400";
                        statusDescription = "Error Occured while submiting the data. Please check the information you provided and re-try again.";
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1} & StatusCode: {2} & StatusDescription: {3} & X-Application-Status-Code: {4}", ex.Message, ex.StackTrace, statusCode, statusDescription, ex.Response.Headers.GetValues("X-Application-Status-Code")[0]), LogLevel.PaySafeStandaloneCredit);
                        standaloneCreditResp.postCallStatusInfo.statusCode = statusCode;
                        standaloneCreditResp.postCallStatusInfo.statusDescription = statusDescription;
                        standaloneCreditResp.postCallStatusInfo.X_Application_Status_Code = ex.Response.Headers.GetValues("X-Application-Status-Code")[0];

                        standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
                    }                    
                }
                else
                {
                    statusCode = "5XX";
                    statusDescription = "Internal Server has occured. Please try after some time.";
                    ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1} & StatusCode: {2} & StatusDescription: {3} & X-Application-Status-Code: {4}", ex.Message, ex.StackTrace, statusCode, statusDescription, ex.Response.Headers.GetValues("X-Application-Status-Code")[0]), LogLevel.PaySafeStandaloneCredit);
                    result = "Internal Server has occured. Please try after some time.";
                    standaloneCreditResp.postCallStatusInfo.statusCode = statusCode;
                    standaloneCreditResp.postCallStatusInfo.statusDescription = statusDescription;
                    standaloneCreditResp.postCallStatusInfo.X_Application_Status_Code = ex.Response.Headers.GetValues("X-Application-Status-Code")[0];

                    standaloneCreditResp.postCallStatusInfo = postCallStatusInfo;
                }
            }
            return standaloneCreditResp;
        }

        public static string ExecuteGetWebRequest(string webReqURL)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public static TAEtagResponse ExecuteGetWebRequestReturnEtag(string webReqURL)
        {
            string result = string.Empty;
            TAEtagResponse resp = new TAEtagResponse();
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    resp.content = reader.ReadToEnd();
                    resp.etag = response.Headers.GetValues("ETag")[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }
        public static string ExecuteDeepLinkGetWebRequest(string webReqURL)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return "{}";
            }
            return result;
        }
        public static string ExecuteTAGetWebRequest(string webReqURL)
        {
            ErrorLogger.Log("\nUtilities.ExecuteTAGetWebRequest: " + webReqURL, LogLevel.Information);
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return result;
        }
        public static string ExecuteGoogleGetWebRequest(string webReqURL)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return result;
        }
        public static string ExecuteArtirixGetWebRequest(string webReqURL)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["ArtirixTimeOut"]);
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public static string ExecuteMobileArtirixGetWebRequest(string webReqURL)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["ArtirixMobileTimeOut"]);
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public static ttssResponseStream ExecuteTTSSGetWebRequest(string webReqURL)
        {
            ttssResponseStream ttssRes = new ttssResponseStream();
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Headers.Add("Accept-Encoding", "gzip,deflate");
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
                timer.Start();
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                timer.Stop();
                ttssRes.ttssResponseTime = timer.Elapsed.TotalMilliseconds;
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    ttssRes.ttssResponse = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ttssRes;
        }

        public static string CacheTimeForTheService(Int32 cacheTime = 5)
        {
            string noOfMin = ConfigurationManager.AppSettings["CacheTimeOutOneHour"];
            Int32 cacheTimeService = (Int32.Parse(noOfMin) * (cacheTime));
            return (cacheTimeService).ToString();
        }
        public static string GetArtirixImgUrl(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                            string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorrating,
                                            string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType,
                                            string labelId, string preferredHotelKeys, string skipFacets, string destinationType)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");

            if (!string.IsNullOrWhiteSpace(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrWhiteSpace(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("ratings=" + ratings + "&");
            }

            if (!string.IsNullOrWhiteSpace(departureDate))
            {
                searchParams.Append("departureDate=" + departureDate + "&");
            }
            if (!string.IsNullOrWhiteSpace(labelId))
            {
                searchParams.Append("labelId=" + labelId + "&");
            }
            if (!string.IsNullOrWhiteSpace(preferredHotelKeys))
            {
                searchParams.Append("preferredHotelKeys=" + preferredHotelKeys + "&");
            }
            if (!string.IsNullOrWhiteSpace(skipFacets))
            {
                searchParams.Append("skipFacets=" + skipFacets + "&");
            }
            if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("destinationType=" + destinationType + "&");
            }
            if (!string.IsNullOrWhiteSpace(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(destinationIds))
            {
                searchParams.Append("destinationIds=" + destinationIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(priceMin))
            {
                searchParams.Append("priceMin=" + priceMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(priceMax))
            {
                searchParams.Append("priceMax=" + priceMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(tripadvisorrating))
            {
                searchParams.Append("tripadvisorrating=" + tripadvisorrating + "&");
            }

            if (!string.IsNullOrWhiteSpace(sort))
            {
                searchParams.Append("sort=" + sort + "&");
            }

            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                searchParams.Append("hotelKeysToExclude=" + hotelKeysToExclude + "&");
            }

            if (!string.IsNullOrWhiteSpace(channelId))
            {
                searchParams.Append("channelId=" + channelId + "&");
            }

            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrWhiteSpace(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(boardType))
            {
                searchParams.Append("boardType=" + boardType + "&");
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string GetArtirixUrl(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                            string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorrating,
                                            string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");

            if (!string.IsNullOrWhiteSpace(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrWhiteSpace(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("ratings=" + ratings + "&");
            }

            if (!string.IsNullOrWhiteSpace(departureDate))
            {
                searchParams.Append("departureDate=" + departureDate + "&");
            }
            if (!string.IsNullOrWhiteSpace(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(destinationIds))
            {
                searchParams.Append("destinationIds=" + destinationIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(priceMin) && priceMin != "0")
            {
                searchParams.Append("priceMin=" + priceMin + "&");
            }

            if (!string.IsNullOrWhiteSpace(priceMax) && priceMax != "0")
            {
                searchParams.Append("priceMax=" + priceMax + "&");
            }

            if (!string.IsNullOrWhiteSpace(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(tripadvisorrating))
            {
                searchParams.Append("tripadvisorrating=" + tripadvisorrating + "&");
            }

            if (!string.IsNullOrWhiteSpace(sort))
            {
                searchParams.Append("sort=" + sort + "&");
            }

            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                searchParams.Append("hotelKeysToExclude=" + hotelKeysToExclude + "&");
            }

            if (!string.IsNullOrWhiteSpace(channelId))
            {
                searchParams.Append("channelId=" + channelId + "&");
            }

            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrWhiteSpace(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "&");
            }

            if (!string.IsNullOrWhiteSpace(boardType))
            {
                searchParams.Append("boardType=" + boardType + "&");
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }

        public static List<HotelURL> FillAllHotelsURL()
        {
            ///first try to fetch from redis server
            ///if available then fill data
            ///if not available fetch from s3
            ///Fill in Redis
            ///if redis insert is success then deserialize json and return the object

            List<HotelURL> hotels = null;
            string json = string.Empty;
            bool result = false;
            try
            {
                json = RedisDataHelper.GetValueByKey(
                        "AllHotels",
                        ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                        (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                result = true;
            }
            catch (System.ArgumentNullException ex) /// if data is not available in redis
            {
                try
                {
                    json = ExecuteGetWebRequest(string.Format(ConfigurationManager.AppSettings["TTFilePath"].ToString(), "AllHotels.json"));
                    if (!string.IsNullOrEmpty(json))
                    {
                        result = RedisDataHelper.InsertValue(
                            "AllHotels",
                            json,
                            ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                            Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                            Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                            (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                        result = true;
                    }
                }
                catch
                {
                    ///data is not available in S3, or redis insert is fail
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }

            if (result == true && !string.IsNullOrEmpty(json))
            {
                try
                {
                    hotels = JsonConvert.DeserializeObject<List<HotelURL>>(json);
                }
                catch
                {

                }
            }
            return hotels;
        }

        public static string GetWebsitePostUrl(string departureName, string boardType, string durationMax, string adults, string children, string ratings, string priceMin, string priceMax, string hotelKeys, string tradingNameIds, string dateOffSet, string flexDates)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            if (!string.IsNullOrWhiteSpace(departureName))
            {
                searchParams.Append(departureName + "/");
            }
            if (!string.IsNullOrWhiteSpace(boardType) && boardType == "8")
            {
                searchParams.Append("boardtype=" + "bedandbreakfast" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "9")
            {
                searchParams.Append("boardtype=" + "roomonly" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "2")
            {
                searchParams.Append("boardtype=" + "selfcatering" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "3")
            {
                searchParams.Append("boardtype=" + "halfboard" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "4")
            {
                searchParams.Append("boardtype=" + "fullboard" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "5")
            {
                searchParams.Append("boardtype=" + "allinclusive" + "/");
            }
            else if (!string.IsNullOrWhiteSpace(boardType) && boardType == "9,2,8,3,4,5")
            {
                searchParams.Append("boardtype=" + "anyboard" + "/");
            }
            if (!string.IsNullOrWhiteSpace(durationMax))
            {
                searchParams.Append("nights=" + durationMax + "/");
            }
            if (flexDates != "yes" && !string.IsNullOrWhiteSpace(flexDates))
            {
                searchParams.Append("flexdates=" + flexDates + "/");
            }
            if (!string.IsNullOrWhiteSpace(adults))
            {
                searchParams.Append("adults=" + adults + "/");
            }
            if (!string.IsNullOrWhiteSpace(children))
            {
                searchParams.Append("children=" + children + "/");
            }
            if (!string.IsNullOrWhiteSpace(ratings) && ratings != "any")
            {
                string[] ratingValues = ratings.Split(',');
                if (ratingValues.Length == 1)
                {
                    searchParams.Append("minstars=" + "5" + "/");
                }
                else if (ratingValues.Length == 2)
                {
                    searchParams.Append("minstars=" + "4" + "/");
                }
                else if (ratingValues.Length == 3)
                {
                    searchParams.Append("minstars=" + "3" + "/");
                }
                else if (ratingValues.Length == 4)
                {
                    searchParams.Append("minstars=" + "2" + "/");
                }
                else if (ratingValues.Length == 5)
                {
                    searchParams.Append("minstars=" + "1" + "/");
                }
            }
            if (!string.IsNullOrWhiteSpace(priceMax) && priceMax != "0")
            {
                searchParams.Append("price=" + priceMin + "-" + priceMax + "/");
            }
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotel=" + hotelKeys + "/");
            }
            if (!string.IsNullOrWhiteSpace(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "/");
            }
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("/"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }

        public static string GetWebsitePostUrlNew(string departureName, string boardType, string durationMax, string adults, string children, string ratings, string priceMin, string priceMax, string hotelKeys, string tradingNameIds, string dateOffSet, string flexDates)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            if (!string.IsNullOrWhiteSpace(departureName))
            {
                searchParams.Append(departureName + "/");
            }
            if (boardType == "8")
            {
                searchParams.Append("boardtype=" + "bedandbreakfast" + "/");
            }
            else if (boardType == "9")
            {
                searchParams.Append("boardtype=" + "roomonly" + "/");
            }
            else if (boardType == "2")
            {
                searchParams.Append("boardtype=" + "selfcatering" + "/");
            }
            else if (boardType == "3")
            {
                searchParams.Append("boardtype=" + "halfboard" + "/");
            }
            else if (boardType == "4")
            {
                searchParams.Append("boardtype=" + "fullboard" + "/");
            }
            else if (boardType == "5")
            {
                searchParams.Append("boardtype=" + "allinclusive" + "/");
            }
            else
            {
                searchParams.Append("boardtype=" + "anyboard" + "/");
            }
            if (!string.IsNullOrWhiteSpace(durationMax))
            {
                searchParams.Append("nights=" + durationMax + "/");
            }
            if (flexDates == "no")
            {
                searchParams.Append("flexdates=" + flexDates + "/");
            }
            if (!string.IsNullOrWhiteSpace(adults))
            {
                searchParams.Append("adults=" + adults + "/");
            }
            if (!string.IsNullOrWhiteSpace(children))
            {
                searchParams.Append("children=" + children + "/");
            }
            if (!string.IsNullOrWhiteSpace(ratings) && ratings != "any")
            {
                string[] ratingValues = ratings.Split(',');
                if (ratingValues.Length == 1)
                {
                    searchParams.Append("minstars=" + "5" + "/");
                }
                if (ratingValues.Length == 2)
                {
                    searchParams.Append("minstars=" + "4" + "/");
                }
                if (ratingValues.Length == 3)
                {
                    searchParams.Append("minstars=" + "3" + "/");
                }
                if (ratingValues.Length == 4)
                {
                    searchParams.Append("minstars=" + "2" + "/");
                }
                if (ratingValues.Length == 5)
                {
                    searchParams.Append("minstars=" + "1" + "/");
                }
            }
            if (!string.IsNullOrWhiteSpace(priceMax) && priceMax != "0")
            {
                searchParams.Append("price=" + priceMin + "-" + priceMax + "/");
            }
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotel=" + hotelKeys + "/");
            }
            if (!string.IsNullOrWhiteSpace(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "/");
            }
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("/"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }

        public static string GetTTSSUrl(int destinationIds, int departureId,
                                         string datemin, string datemax, int durationMin, int durationMax, int adults, int children,
                                        int priceMin, string tradingNameId, string destinationType,
                                        string hotelKey, string taRating)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            if (destinationIds != 0)
            {
                searchParams.Append("destinationId=" + destinationIds + "&");
            }

            if (departureId != 0)
            {
                searchParams.Append("departureId=" + departureId + "&");
            }
            if (!string.IsNullOrWhiteSpace(datemin))
            {
                searchParams.Append("dateMin=" + datemin + "&");
            }
            if (!string.IsNullOrWhiteSpace(datemax))
            {
                searchParams.Append("dateMax=" + datemax + "&");
            }

            if (durationMin != 0)
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (durationMax != 0)
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (adults != 0)
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (children != 0)
            {
                searchParams.Append("children=" + children + "&");
            }
            if (priceMin != 0 && priceMin < 80)
            {
                priceMin = 80;
            }
            //if (priceMin != 0)
            //{
            //    searchParams.Append("priceMin=" + priceMin + "&");
            //}
            //searchParams.Append("priceMin=0&");
            searchParams.Append("rating=2345&");
            if (!string.IsNullOrWhiteSpace(tradingNameId))
            {
                searchParams.Append("tradingNameId=" + tradingNameId + "&");
            }
            else
            {
                searchParams.Append("tradingNameId=192,576" + "&");
            }

            if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("destinationType=" + destinationType + "&");
            }
            else
            {
                searchParams.Append("destinationType=label&");
            }
            if (!string.IsNullOrWhiteSpace(hotelKey))
            {
                searchParams.Append("hotelKey=" + hotelKey + "&");
            }
            if (!string.IsNullOrWhiteSpace(taRating))
            {
                searchParams.Append("taRating=" + taRating + "&");
            }
            searchParams.Append("maxResults=" + ConfigurationManager.AppSettings["TTSSMaxSearchResults"] + "&");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string GetTTSSUrlV_2(string destinationIds, int departureId, string datemin, string datemax, int durationMin, int durationMax, int adults, int children,
                                           int priceMin, string tradingNameId, string destinationType, string hotelKeys, int tripAdvisorRating, string ratings, string boardType)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            if (!string.IsNullOrWhiteSpace(destinationIds))
            {
                searchParams.Append("destinationId=" + destinationIds + "&");
            }

            if (departureId != 0)
            {
                searchParams.Append("departureId=" + departureId + "&");
            }
            if (!string.IsNullOrWhiteSpace(boardType))
            {
                if (boardType == "9,2,8,3,4,5")
                {
                    searchParams.Append("boardType=-1&");
                }
                else
                {
                    searchParams.Append("boardType=" + boardType + "&");
                }
            }
            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("rating=" + ratings.Replace(",", "") + "&");
            }
            if (!string.IsNullOrWhiteSpace(datemin))
            {
                searchParams.Append("dateMin=" + datemin + "&");
            }
            if (!string.IsNullOrWhiteSpace(datemax))
            {
                searchParams.Append("dateMax=" + datemax + "&");
            }

            if (durationMin != 0)
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (durationMax != 0)
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (adults != 0)
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (children != 0)
            {
                searchParams.Append("children=" + children + "&");
            }
            if (priceMin != 0 && priceMin < 80)
            {
                priceMin = 80;
            }
            if (!string.IsNullOrWhiteSpace(tradingNameId))
            {
                searchParams.Append("tradingNameId=" + tradingNameId + "&");
            }
            else
            {
                searchParams.Append("tradingNameId=192,576" + "&");
            }

            if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("destinationType=" + destinationType + "&");
            }
            else
            {
                searchParams.Append("destinationType=Label&");
            }

            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotelKey=" + hotelKeys + "&");
            }

            if (tripAdvisorRating != 0)
            {
                searchParams.Append("taRating=" + tripAdvisorRating + "&");
            }
            searchParams.Append("maxResults=" + ConfigurationManager.AppSettings["TTSSMaxSearchResults"] + "&");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }

        public static string GetDiversityTTSSUrl(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                           int children, string ratings, string hotelKeys, string tradingNameId)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            if (destinationId != 0)
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }

            if (departureIds != 0)
            {
                searchParams.Append("departureId=" + departureIds + "&");
            }
            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("rating=" + ratings.Replace(",", "") + "&");
            }
            if (!string.IsNullOrWhiteSpace(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }
            if (!string.IsNullOrWhiteSpace(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }
            if (durationMin != 0)
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (durationMax != 0)
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (adults != 0)
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (children != 0)
            {
                searchParams.Append("children=" + children + "&");
            }
            if (!string.IsNullOrWhiteSpace(tradingNameId))
            {
                searchParams.Append("tradingNameId=" + tradingNameId + "&");
            }
            else
            {
                searchParams.Append("tradingNameId=192,576" + "&");
            }
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("hotelKey=" + hotelKeys + "&");
            }
            searchParams.Append("maxResults=" + ConfigurationManager.AppSettings["TTSSMaxSearchResults"] + "&");
            searchParams.Append("destinationType=Label");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string GetESQuery(long responseId, int departureIds, string boardtypeId, string ratings, string hotelKeysToExclude, string sortParameter, int priceMin, int pricemax)
        {
            string sort = string.Empty;
            if (string.IsNullOrWhiteSpace(sortParameter))
            {
                sortParameter = "price";
                sort = "asc";
            }
            else
            {
                sort = "desc";
            }
            string esQuery = null;
            string preQuery = "{\"query\":{ \"bool\":{ \"must\":[";
            string esAirportId = string.Empty;
            if (departureIds < 0)
            {
                esAirportId = "{\"match\":{\"airportCollectionId\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},";
            }
            else
            {
                esAirportId = "{\"match\":{\"airportCode\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},";
            }
            string esBoardType = string.Empty;
            if (!string.IsNullOrWhiteSpace(boardtypeId))
            {
                esBoardType = "{\"match\":{\"accommodation.boardTypeId\":{\"query\":\"" + boardtypeId + "\",\"operator\":\"and\"}}},";
            }
            string esRating = "{\"terms\":{\"accommodation.rating\":[\"" + ratings.Replace(",", "\",\"") + "\"]}},";
            string esPriceRange = "{\"range\":{\"price\":{\"from\":" + priceMin + ",\"to\":" + pricemax + "}}},";
            string esResponseId = "{\"terms\":{\"responseId\":[\"" + responseId.ToString().Replace(",", "\",\"") + "\"]}}]"; //"{\"match\":{\"responseId\":{\"query\":\"" + responseId + "\",\"operator\":\"and\"}}}]";
            string esHotelKeysToExclude = string.Empty;
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                esHotelKeysToExclude = ",\"must_not\":[{\"terms\":{\"hotel.iff\":[\"" + hotelKeysToExclude.Replace(",", "\",\"") + "\"]}}]}},";
            }
            else
            {
                esHotelKeysToExclude = "}},";
            }

            string esSize = "\"size\": 0,";
            string esAggs = "\"aggs\":{\"hotel_iff\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":20,\"order\":{\"min_price\":\"" + sort + "\"}},\"aggs\":{\"min_price\":{\"min\":{\"field\":\"" + sortParameter + "\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}}}}}},";
            string esPriceAggs = "\"price_min\":{\"min\":{\"field\":\"price\"}},\"price_max\":{\"max\":{\"field\":\"price\"}},";
            string esHotelAggs = "\"hotel\":{\"terms\":{\"field\":\"hotel.name\",\"size\":500,\"order\":{\"hoteliff\":\"asc\",\"region_id\": \"asc\"}},\"aggs\":{\"hoteliff\":{\"min\":{\"field\":\"hotel.iff\"}},\"region_id\":{\"min\":{\"field\":\"regionId\"}}}},";
            string esAirportAggs = "\"airports\":{\"terms\":{\"field\":\"airportCode\",\"size\":500}},";
            string esResortAggs = "\"resorts\":{\"terms\":{\"field\":\"journey.destination\",\"size\":500,\"order\":{\"resort_name\":\"desc\"}},\"aggs\":{\"resort_name\":{\"min\":{\"field\":\"regionId\"}}}}}}";
            //string esSortOrder = "\"sort\":[{\"" + sortParameter + "\":{\"order\":\"asc\"}}]}";
            esQuery = preQuery + esAirportId + esBoardType + esRating + esPriceRange + esResponseId + esHotelKeysToExclude + esSize + esAggs + esPriceAggs + esHotelAggs + esAirportAggs + esResortAggs;
            return esQuery;
        }
        public static string GetESQueryV_2(int departureIds, string boardtypeId, string ratings, int adults, int children, int durationMin, int durationMax, string destinationIds,
                                            string departureDate, string hotelKeysToExclude, string sortParameter, int priceMin, int priceMax, string dateMin,
                                            string dateMax, string hotelKey, string destinationType, string labelId, int tripAdvisorRating, string usersPreferredHotelKeys, string tradingNameId, int tradingNameIdCount)
        {
            DateTime dt1 = DateTime.UtcNow;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            System.TimeSpan tSpan = new System.TimeSpan(0, 0, 30, 0);
            DateTime dt2 = dt1.Subtract(tSpan);
            //Dictionary<int, string> parentRegionIds = Global.labelRegionMappingList;
            //var regionDestinationIds = parentRegionIds[destinationIds];
            string sort = string.Empty;
            if (string.IsNullOrWhiteSpace(sortParameter))
            {
                sortParameter = "price";
                sort = "asc";
            }
            else
            {
                sort = "desc";
                if (sortParameter.StartsWith("trip"))
                {
                    sortParameter = "hotel.rating.averageRating";
                }
                else
                {
                    sortParameter = "accommodation.rating";
                }
            }
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            string esAirportId = string.Empty;
            if (departureIds < 0)
            {
                searchParams.Append("{\"match\":{\"airportCollectionId\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"match\":{\"airportCode\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            }
            string esAdults = string.Empty;
            if (adults != 0)
            {
                searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}},");
            }
            if (!string.IsNullOrWhiteSpace(tradingNameId))
            {
                searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + tradingNameId.Replace(",", "\",\"") + "\"]}},");
            }
            if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + usersPreferredHotelKeys.Replace(",", "\",\"") + "\"]}},");
            }
            string esBoardType = string.Empty;
            if (!string.IsNullOrWhiteSpace(boardtypeId))
            {
                searchParams.Append("{\"terms\":{\"accommodation.boardTypeId\":[\"" + boardtypeId.Replace(",", "\",\"") + "\"]}},");
            }
            string esDuration = string.Empty;
            if (durationMin == durationMax)
            {
                searchParams.Append("{\"match\":{\"journey.duration\":{\"query\":\"" + durationMin + "\",\"operator\":\"and\"}}},");
            }
            else if (durationMin != 0 && durationMax != 0)
            {
                searchParams.Append("{\"range\":{\"journey.duration\":{\"gte\":" + durationMin + ",\"lte\":" + durationMax + ",\"operation\":\"and\"}}},");
            }
            string esDestination = string.Empty;
            if (string.IsNullOrWhiteSpace(destinationType) && !string.IsNullOrWhiteSpace(destinationIds))
            {
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                var regionDestinationIds = parentRegionIds[destinationIds];
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + regionDestinationIds.Replace(",", "\",\"") + "\"]}},");
            }
            else if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + destinationIds.Replace(",", "\",\"") + "\"]}},");
            }
            string esChildren = string.Empty;
            //if (children != 0)
            // {
            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}},");
            // }
            string esDateRange = string.Empty;
            if (dateMin == dateMax)
            {
                searchParams.Append("{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00\",\"to\":\"" + dateMax + "T23:59:59\"}}},");
            }
            else
            {
                searchParams.Append("{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00\",\"to\":\"" + dateMax + "T23:59:59\"}}},");
            }
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + dt2.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + dt1.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}},");
            string esHotelKey = string.Empty;
            if (!string.IsNullOrWhiteSpace(hotelKey))
            {
                searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + hotelKey.Replace(",", "\",\"") + "\"]}},");
            }
            string esRating = string.Empty;
            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + ratings.Replace(",", "\",\"") + "\"]}},");
            }
            if (tripAdvisorRating != 0)
            {
                searchParams.Append("{\"range\":{\"hotel.rating.averageRating\":{\"gte\":" + (double)tripAdvisorRating + "}}},");
            }
            string esPriceRange = string.Empty;
            if (priceMax != 0)
            {
                searchParams.Append("{\"range\":{\"price\":{\"from\":" + priceMin + ",\"to\":" + priceMax + "}}}]");
            }
            else
            {
                searchParams.Append("{\"range\":{\"price\":{\"from\":" + priceMin + "}}}]");
            }
            string esHotelKeysToExclude = string.Empty;
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                searchParams.Append(",\"must_not\":[{\"terms\":{\"hotel.iff\":[\"" + hotelKeysToExclude.Replace(",", "\",\"") + "\"]}}]}},");
            }
            else
            {
                searchParams.Append("}},");
            }

            searchParams.Append("\"size\": 0,");
            searchParams.Append("\"aggs\":{\"hotel_iff\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":" + tradingNameIdCount + ",\"order\":{\"min_price\":\"" + sort + "\"}},\"aggs\":{\"min_price\":{\"min\":{\"field\":\"" + sortParameter + "\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}}}}}}}}");
            //searchParams.Append("\"price_min\":{\"min\":{\"field\":\"price\"}},\"price_max\":{\"max\":{\"field\":\"price\"}},");
            //searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":500}},");
            //searchParams.Append("\"airports\":{\"terms\":{\"field\":\"airportCode\",\"size\":500}},");
            //searchParams.Append("\"resorts\":{\"terms\":{\"field\":\"regionId\",\"size\":500}}}}");
            /*
            searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":500,\"order\":{\"hoteliff\":\"asc\"}},\"aggs\":{\"hoteliff\":{\"min\":{\"field\":\"price\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}},\"_source\":{\"include\":[\"hotel.name\",\"regionId\"]}}}}},");
            searchParams.Append("\"airports\":{\"terms\":{\"field\":\"airportCode\",\"size\":500}},");
            searchParams.Append("\"resorts\":{\"terms\":{\"field\":\"regionId\",\"size\":500,\"order\":{\"region_id\":\"desc\"}},\"aggs\":{\"region_id\":{\"min\":{\"field\":\"price\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}},\"_source\":{\"include\":[\"regionName\"]}}}}}}}");
            */
            string paramsString = searchParams.ToString();
            //esQuery = preQuery + esAirportId + esAdults +esBoardType + esDuration + esDestination + esChildren + esDateRange + esHotelKey + esRating + esPriceRange+
            //esHotelKeysToExclude + esSize + esAggs + esPriceAggs + esHotelAggs + esAirportAggs + esResortAggs;
            return paramsString;
        }
        public static string GetESFacetsQuery(int departureIds, string boardtypeId, string ratings, int adults, int children, int durationMin, int durationMax, string destinationIds,
                                            string departureDate, string hotelKeysToExclude, string sortParameter, int priceMin, int priceMax, string dateMin,
                                            string dateMax, string hotelKey, string destinationType, string labelId, int tripAdvisorRating, string usersPreferredHotelKeys)
        {
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            string esAirportId = string.Empty;
            if (departureIds < 0)
            {
                searchParams.Append("{\"match\":{\"airportCollectionId\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"match\":{\"airportCode\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            }
            string esAdults = string.Empty;
            if (adults != 0)
            {
                searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}},");
            }
            string esBoardType = string.Empty;
            if (!string.IsNullOrWhiteSpace(boardtypeId))
            {
                searchParams.Append("{\"terms\":{\"accommodation.boardTypeId\":[\"" + boardtypeId.Replace(",", "\",\"") + "\"]}},");
            }
            if (!string.IsNullOrWhiteSpace(hotelKey))
            {
                searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + hotelKey.Replace(",", "\",\"") + "\"]}},");
            }
            string esDuration = string.Empty;
            if (durationMin == durationMax)
            {
                searchParams.Append("{\"match\":{\"journey.duration\":{\"query\":\"" + durationMin + "\",\"operator\":\"and\"}}},");
            }
            else if (durationMin != 0 && durationMax != 0)
            {
                searchParams.Append("{\"range\":{\"journey.duration\":{\"gte\":" + durationMin + ",\"lte\":" + durationMax + ",\"operation\":\"and\"}}},");
            }
            string esDestination = string.Empty;
            if (string.IsNullOrWhiteSpace(destinationType) && !string.IsNullOrWhiteSpace(destinationIds))
            {
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                var regionDestinationIds = parentRegionIds[destinationIds];
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + regionDestinationIds.Replace(",", "\",\"") + "\"]}},");
            }
            else if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + destinationIds.Replace(",", "\",\"") + "\"]}},");
            }
            string esChildren = string.Empty;
            //if (children != 0)
            // {
            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}},");
            // }
            string esDateRange = string.Empty;
            if (dateMin == dateMax)
            {
                searchParams.Append("{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00\",\"to\":\"" + dateMax + "T23:59:59\"}}},");
            }
            else
            {
                searchParams.Append("{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00\",\"to\":\"" + dateMax + "T23:59:59\"}}},");
            }
            if (tripAdvisorRating != 0)
            {
                searchParams.Append("{\"range\":{\"hotel.rating.averageRating\":{\"gte\":" + (double)tripAdvisorRating + "}}},");
            }
            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + ratings.Replace(",", "\",\"") + "\"]}}]}},");
            }
            searchParams.Append("\"size\": 0,");
            searchParams.Append("\"aggs\":{\"price_min\":{\"min\":{\"field\":\"price\"}},\"price_max\":{\"max\":{\"field\":\"price\"}},");
            searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":5000}},");
            searchParams.Append("\"airports\":{\"terms\":{\"field\":\"airportCode\",\"size\":5000}},");
            searchParams.Append("\"resorts\":{\"terms\":{\"field\":\"regionId\",\"size\":5000},\"aggs\":{\"NoOfHotels\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}}}}");
            string paramsString = searchParams.ToString();
            return paramsString;
        }

        public static string GetESSearchQuery(string destinationIds, int departureIds, string departureDate, string dateMin, string dateMax, int durationMin,
                                                int durationMax, int adults, int children, string boardType, string ratings, string destinationType, string labelId,
                                                string hotelKeys, string tradingNameIds)
        {
            DateTime dt1 = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime dt2 = dt1.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}},");
            //string esSearchChildren = string.Empty;
            //if (children != 0)
            //{
            searchParams.Append("{\"match\":{\"children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}},");
            // }
            searchParams.Append("{\"match\":{\"departureIds\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            if (string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("{\"match\":{\"isHotelKeys\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            }
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + dt2.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + dt1.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}},");
            if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"match\":{\"destinationIds\":{\"query\":\"" + labelId + "\",\"operator\":\"and\"}}},");
            }
            else if (string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"match\":{\"destinationIds\":{\"query\":\"" + destinationIds + "\",\"operator\":\"and\"}}},");
            }
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                var taIds = "576,192";
                searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + taIds.Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + tradingNameIds.Replace(",", "\",\"") + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"ratings\":{\"query\":\"" + ratings.Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"boardType\":{\"query\":\"" + boardType.Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"dateMax\":{\"gte\":\"" + dateMin + "T00:00:00\"}}},");
            searchParams.Append("{\"range\":{\"dateMin\":{\"lte\":\"" + dateMax + "T23:59:59\"}}},");
            searchParams.Append("{\"match\":{\"isCahceHit\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            if (durationMin != 0)
            {
                searchParams.Append("{\"match\":{\"durationMin\":{\"query\":\"" + durationMin + "\",\"operator\":\"and\"}}},");
            }
            if (durationMax != 0)
            {
                searchParams.Append("{\"match\":{\"durationMax\":{\"query\":\"" + durationMax + "\",\"operator\":\"and\"}}}");
            }
            searchParams.Append("]}}}");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals(","))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string GetParentESSearchQuery(string destinationIds, int departureIds, string departureDate, string dateMin, string dateMax,
                                                    int durationMin, int durationMax, int adults, int children, string boardType, string ratings,
                                                     string destinationType, string labelId, string hotelKeys)
        {
            string esSearchQuery = null;
            DateTime dt1 = DateTime.UtcNow;
            //StringBuilder searchParams = new StringBuilder();
            //searchParams.Append("");
            System.TimeSpan tSpan = new System.TimeSpan(0, 0, 30, 0);
            DateTime dt2 = dt1.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}},");
            //string esSearchBoardTypeId = "{\"match\":{\"boardTypeId\":{\"query\":\"" + boardType + "\",\"operator\":\"and\"}}},";
            string esSearchChildren = string.Empty;
            if (children != 0)
            {
                searchParams.Append("{\"match\":{\"children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}},");
            }
            searchParams.Append("{\"match\":{\"parentDepartureIds\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},");
            if (string.IsNullOrWhiteSpace(hotelKeys))
            {
                searchParams.Append("{\"match\":{\"isHotelKeys\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            }
            string esSearchDestinationIds = string.Empty;
            if (!string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"match\":{\"regionIds\":{\"query\":\"" + labelId + "\",\"operator\":\"and\"}}},");
            }
            else if (string.IsNullOrWhiteSpace(destinationType))
            {
                searchParams.Append("{\"match\":{\"regionIds\":{\"query\":\"" + destinationIds + "\",\"operator\":\"and\"}}},");
            }
            searchParams.Append("{\"match\":{\"ratings\":{\"query\":\"" + ratings.Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"boardType\":{\"query\":\"" + boardType.Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + dt2.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + dt1.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}},");
            //string esSearchDuration = "{\"match\":{\"duration\":{\"query\":\"" + duration + "\",\"operator\":\"and\"}}},";
            // string esSearchPriceMax = "{\"match\":{\"priceMax\":{\"query\":\"" + priceMax + "\",\"operator\":\"and\"}}},";
            // string esSearchPriceMin = "{\"match\":{\"priceMin\":{\"query\":\"" + priceMin + "\",\"operator\":\"and\"}}},";
            //string esSearchRatings = "{\"match\":{\"ratings\":{\"query\":\"" + ratings + "\",\"operator\":\"and\"}}},";
            string esSearchDateMax = string.Empty;
            string esSearchDateMin = string.Empty;
            searchParams.Append("{\"range\":{\"dateMax\":{\"gte\":\"" + dateMin + "T00:00:00\"}}},");
            searchParams.Append("{\"range\":{\"dateMin\":{\"lte\":\"" + dateMax + "T23:59:59\"}}},");
            //if (dateMin == dateMax)
            //{
            //    searchParams.Append("{\"range\":{\"dateMax\":{\"gte\":\"" + dateMax + "\"}}},");
            //    searchParams.Append("{\"range\":{\"dateMin\":{\"lte\":\"" + dateMin + "\"}}},");
            //}
            //else
            //{
            //    searchParams.Append("{\"match\":{\"dateMax\":{\"query\":\"" + dateMax + "\",\"operator\":\"and\"}}},");
            //    searchParams.Append("{\"match\":{\"dateMin\":{\"query\":\"" + dateMin + "\",\"operator\":\"and\"}}},");
            //}
            searchParams.Append("{\"match\":{\"isCahceHit\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            string esSearchDurationMin = string.Empty;
            string esSearchDurationMax = string.Empty;
            if (durationMin != 0)
            {
                searchParams.Append("{\"match\":{\"durationMin\":{\"query\":\"" + durationMin + "\",\"operator\":\"and\"}}},");
            }
            if (durationMax != 0)
            {
                searchParams.Append("{\"match\":{\"durationMax\":{\"query\":\"" + durationMax + "\",\"operator\":\"and\"}}}");
            }
            //string esSearchDuration = "{\"match\":{\"duration\":{\"query\":\"" + duration + "\",\"operator\":\"and\"}}}]}}}";
            //string esSearchDepartureDate = "{\"match\":{\"departureDate\":{\"query\":\"" + departureDate + "\",\"operator\":\"and\"}}}]}}}";
            //esSearchQuery = preQuery + esSearchAdults + esSearchChildren + esSearchDepartureIds + esSearchDestinationIds + esSearchRatings +esSearchBoardType+
            //esSearchDateMax + esSearchDateMin + esCacheHit + esSearchDurationMin+esSearchDurationMax ;
            //esSearchQuery = esSearchQuery.Remove(esSearchQuery.Length - 1) + "]}}}";
            //return esSearchQuery;
            searchParams.Append("]}}}");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals(","))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string DiversityQuery(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                             int children, string ratings, string hotelKeys, string tradingNameId)
        {
            string diversityQuery = string.Empty;
            string preQuery = "{\"query\":{ \"bool\":{ \"must\":[";
            string esAdults = string.Empty;
            if (adults != 0)
            {
                esAdults = "{\"match\":{\"accommodation.adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}},";
            }
            string esDestination = "{\"match\":{\"labelId\":{\"query\":\"" + destinationId + "\",\"operator\":\"and\"}}},";
            string esDeparture = string.Empty;
            if (departureIds < 0 && departureIds != 0)
            {
                esDeparture = "{\"match\":{\"airportCollectionId\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},";
            }
            if (departureIds > 0 && departureIds != 0)
            {
                esDeparture = "{\"match\":{\"airportCode\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}},";
            }
            string esDuration = string.Empty;
            if (durationMin != 0 && durationMax != 0)
            {
                esDuration = "{\"range\":{\"journey.duration\":{\"gte\":" + durationMin + ",\"lte\":" + durationMax + "}}},";
            }
            else if (durationMin == 0 && durationMax != 0)
            {
                esDuration = "{\"range\":{\"journey.duration\":{\"lte\":" + durationMax + "}}},";
            }
            else if (durationMin != 0 && durationMax == 0)
            {
                esDuration = "{\"range\":{\"journey.duration\":{\"gte\":" + durationMin + "}}},";
            }
            string esTradingNameId = string.Empty;
            if (string.IsNullOrWhiteSpace(tradingNameId))
            {
                var taIds = "576,192";
                esTradingNameId = "{\"terms\":{\"tradingNameId\":[\"" + taIds.Replace(",", "\",\"") + "\"]}},";
            }
            else
            {
                esTradingNameId = "{\"terms\":{\"tradingNameId\":[\"" + tradingNameId.Replace(",", "\",\"") + "\"]}},";
            }
            string esChilds = string.Empty;
            //if (children != 0)
            // {
            esChilds = "{\"match\":{\"accommodation.children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}},";
            // }
            string esRating = string.Empty;
            if (string.IsNullOrEmpty(ratings))
            {
                esRating = "{\"terms\":{\"accommodation.rating\":[\"" + ratings.Replace(",", "\",\"") + "\"]}},";
            }
            string esHotelKeys = "{\"terms\":{\"hotel.iff\":[\"" + hotelKeys.Replace(",", "\",\"") + "\"]}},";
            string esDepartureDate = string.Empty;
            if (string.IsNullOrWhiteSpace(dateMax))
            {
                esDepartureDate = "{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00" + "\"}}}]}},";
            }
            else if (string.IsNullOrWhiteSpace(dateMin))
            {
                esDepartureDate = "{\"range\":{\"journey.departureDate\":{\"to\":\"" + dateMax + "T23:55:55" + "\"}}}]}},";
            }
            else if (!string.IsNullOrWhiteSpace(dateMin) && !string.IsNullOrWhiteSpace(dateMax))
            {
                esDepartureDate = "{\"range\":{\"journey.departureDate\":{\"from\":\"" + dateMin + "T00:00:00" + "\",\"to\":\"" + dateMax + "T23:55:55" + "\"}}}]}},";
            }
            else
            {
                esDepartureDate = "]}},";
            }
            //string esFrom = "\"from\":0,";
            string esSize = "\"size\": 0,";
            //string esSortOrder = "\"sort\":[{\"hotel.iff\":{\"order\":\"asc\"}},{\"price\":{\"order\":\"desc\"}}]}";
            string esAggs = "\"aggs\":{\"group_by_iff\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":20},\"aggs\":{\"group_by_category\":{\"terms\":{\"field\":\"accommodation.boardTypeId\",\"order\":{\"min_price\":\"asc\"}},\"aggs\":{\"min_price\":{\"min\":{\"field\":\"price\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}},\"_source\":{\"includes\":[\"quoteRef\",\"price\",\"journey.departureDate\"]}}}}}}}}}";
            diversityQuery = preQuery + esAdults + esDestination + esDeparture + esTradingNameId + esChilds + esDuration + esRating + esHotelKeys + esDepartureDate + esSize + esAggs;
            return diversityQuery;
        }
        public static string DiversitySearchQuery(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                             int children, string ratings, string hotelKeys, string tradingNameId)
        {
            StringBuilder searchParams = new StringBuilder();
            DateTime dt1 = DateTime.UtcNow;
            System.TimeSpan tSpan = new System.TimeSpan(0, 0, 30, 0);
            DateTime dt2 = dt1.Subtract(tSpan);
            searchParams.Append("");
            searchParams.Append("{\"query\":{\"bool\":{\"must\":[");
            searchParams.Append("{\"match\":{\"destinationIds\":{\"query\":\"" + destinationId + "\",\"operator\":\"and\"}}}");
            if (adults != 0)
            {
                searchParams.Append(",{\"match\":{\"adults\":{\"query\":\"" + adults + "\",\"operator\":\"and\"}}}");
            }
            //if (children != 0)
            // {
            searchParams.Append(",{\"match\":{\"children\":{\"query\":\"" + children + "\",\"operator\":\"and\"}}}");
            // }
            if (departureIds != 0)
            {
                searchParams.Append(",{\"match\":{\"departureIds\":{\"query\":\"" + departureIds + "\",\"operator\":\"and\"}}}");
            }
            searchParams.Append(",{\"match\":{\"hotelKeys\":{\"query\":\"" + hotelKeys.Replace(",", " ") + "\",\"operator\":\"and\"}}}");
            if (!string.IsNullOrWhiteSpace(ratings))
            {
                searchParams.Append(",{\"terms\":{\"ratings\":[\"" + ratings.Replace(",", "\",\"") + "\"]}}");
            }
            searchParams.Append(",{\"range\":{\"timeStamp\":{\"from\":\"" + dt2.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + dt1.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}");
            searchParams.Append(",{\"range\":{\"dateMax\":{\"gte\":\"" + dateMin + "T00:00:00\"}}}");
            searchParams.Append(",{\"range\":{\"dateMin\":{\"lte\":\"" + dateMax + "T23:59:59\"}}}");
            searchParams.Append(",{\"match\":{\"isCahceHit\":{\"query\":\"false\",\"operator\":\"and\"}}}");
            if (!string.IsNullOrWhiteSpace(tradingNameId))
            {
                searchParams.Append(",{\"terms\":{\"tradingNameId\":[\"" + tradingNameId.Replace(",", "\",\"") + "\"]}}");
            }
            if (durationMin != 0)
            {
                searchParams.Append(",{\"match\":{\"durationMin\":{\"query\":\"" + durationMin + "\",\"operator\":\"and\"}}}");
            }
            if (durationMax != 0)
            {
                searchParams.Append(",{\"match\":{\"durationMax\":{\"query\":\"" + durationMax + "\",\"operator\":\"and\"}}}");
            }
            searchParams.Append("]}}}");
            return searchParams.ToString();
        }
        public static string ExecutePostJsonWebRequest(string url, string postJson)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            byte[] bytes = Encoding.UTF8.GetBytes(postJson);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            var result = reader.ReadToEnd();
            stream.Dispose();
            reader.Dispose();
            return result;
        }
        public static string ExecuteESPostJsonWebRequest(string url, string postJson)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
            request.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            WebResponse response = null;
            Stream stream = null;
            StreamReader reader = null;
            string result = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(postJson);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                response = request.GetResponse();
                stream = response.GetResponseStream();
                reader = new StreamReader(stream);
                result = reader.ReadToEnd();
            }
            catch (WebException ex)
            {
                ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, postJson, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Web exception occured while making HTTP call. Url - {0}, PostJson - {1}, Message - {2}, Stack Trace - {3}", url, postJson, ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
                if (reader != null)
                {
                    reader.Dispose();
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }
            }
            return result;
        }

        public static Stream DownloadS3Object(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                    }

                    imageStream.Position = 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return imageStream;
        }

        /* Written this method for downloading the Mhid->url mapping json */
        public static Stream DownloadS3ObjectForMhidData(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSDevUserAWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSDevUserAWSSecretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                    }

                    imageStream.Position = 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return imageStream;
        }

        /* written this method for Reviews API */

        public static Stream DownloadS3ObjectForReviewsData(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSSecretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                    }

                    imageStream.Position = 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return imageStream;
        }

        /* written this method to mainly test on INT because other dependent folders are not available on INT S3 */

        public static Stream DownloadS3ObjectForTripAdvisorData(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                    }

                    imageStream.Position = 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return imageStream;
        }

        public static string PutObjectToS3(string awsBucketName, string keyName, object objectData, string subfolderPath = "")
        {
            IAmazonS3 client;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string s3Etag = string.Empty;
            try
            {

                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", subfolderPath, keyName)

                    };
                    request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(objectData)));
                    PutObjectResponse response2 = client.PutObject(request);
                    s3Etag = response2.ETag;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return s3Etag;
        }

        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static Dictionary<string, List<string>> GenerateDestinationResortMap(Dictionary<string, string> labelRegionMappingList)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            foreach (KeyValuePair<string, string> map in labelRegionMappingList)
            {
                if (!string.IsNullOrEmpty(map.Value))
                {
                    result.Add(map.Key, map.Value.Split(',').ToList());
                }
            }
            return result;
        }

        public static Dictionary<string, TradingNameDestinationMapping> GetTradingNameDestinationMappings(Dictionary<string, TelePhoneSet> telePhoneSets, string destinationTelephonesetsJSON)
        {
            Dictionary<string, List<DestinationTelePhoneSet>> tradingDestinationMap = new Dictionary<string, List<DestinationTelePhoneSet>>();
            Dictionary<string, TradingNameDestinationMapping> tradingNameDestinationMappings = JsonConvert.DeserializeObject<Dictionary<string, TradingNameDestinationMapping>>(destinationTelephonesetsJSON);
            foreach (var tradingNameDestinationMapping in tradingNameDestinationMappings)
            {
                tradingDestinationMap = ReadTradingDestinationMapping(destinationTelephonesetsJSON, tradingNameDestinationMapping.Key);
                tradingNameDestinationMappings[tradingNameDestinationMapping.Key].TradingNameDestination = tradingDestinationMap;
            }

            return tradingNameDestinationMappings;
        }

        public static Dictionary<string, List<DestinationTelePhoneSet>> ReadTradingDestinationMapping(string destinationTelephonesetJSON, string tradingNameId)
        {
            byte[] destinationTelephonesetBytes = Encoding.ASCII.GetBytes(destinationTelephonesetJSON);
            using (MemoryStream stm = new MemoryStream(destinationTelephonesetBytes, 0, destinationTelephonesetBytes.Length))
            using (TextReader tr = new StreamReader(stm))
            using (JsonTextReader reader = new JsonTextReader(tr))
            {
                JObject destinationTelephonesetObject = (JObject)JToken.ReadFrom(reader);
                var uri = destinationTelephonesetObject[tradingNameId];
                var tradingDestinationMapping = JsonConvert.DeserializeObject<Dictionary<string, List<DestinationTelePhoneSet>>>(uri.ToString());
                return tradingDestinationMapping;
            }
        }

        public static Dictionary<string, List<DestinationTelePhoneNumber>> GetDestinationTelephoneNumberMappings(string destinationTelephonesetsJSON, Dictionary<string, TelePhoneSet> telePhoneSets)
        {
            Dictionary<string, TradingNameDestinationMapping> tradingNameDestinationMappings = GetTradingNameDestinationMappings(telePhoneSets, destinationTelephonesetsJSON);
            Dictionary<string, List<DestinationTelePhoneNumber>> destinationTelePhoneNumbers = new Dictionary<string, List<DestinationTelePhoneNumber>>();
            foreach (var tradingNameDestinationMapping in tradingNameDestinationMappings)
            {
                foreach (var tradingNameDestination in tradingNameDestinationMapping.Value.TradingNameDestination)
                {
                    List<DestinationTelePhoneNumber> telePhoneSetNumbers = new List<DestinationTelePhoneNumber>();
                    DestinationTelePhoneNumber telePhoneSetNumber = new DestinationTelePhoneNumber();
                    foreach (var destinationTelephoneSets in tradingNameDestination.Value)
                    {
                        telePhoneSetNumber = new DestinationTelePhoneNumber();
                        telePhoneSetNumber.Price = destinationTelephoneSets.Price;
                        telePhoneSetNumber.TelephoneSet = destinationTelephoneSets.TelephoneSetId.ToString();
                        telePhoneSetNumbers.Add(telePhoneSetNumber);
                    }

                    destinationTelePhoneNumbers.Add(tradingNameDestinationMapping.Key + "|" + tradingNameDestination.Key, telePhoneSetNumbers);
                }
            }

            return destinationTelePhoneNumbers;
        }

        public static bool DeleteObjectToS3(string awsBucketName, string keyName, string subfolderPath)
        {
            IAmazonS3 client;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            bool objectPostingSuccess = false;
            try
            {

                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", subfolderPath, keyName)

                    };

                    client.DeleteObject(deleteObjectRequest);
                }

                objectPostingSuccess = true;
            }
            catch (Exception ex)
            {
                throw;
            }

            return objectPostingSuccess;
        }

        public static int RenameMasterHotelImages(string awsBucketName, string imageFolderPath)
        {

            int index = 0;
            try
            {
                IAmazonS3 client;
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                ListObjectsRequest listObjectsRequest = new ListObjectsRequest
                {
                    BucketName = awsBucketName,
                    Prefix = imageFolderPath,
                };
                do
                {
                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        ListObjectsResponse listObjectsResponse = client.ListObjects(listObjectsRequest);
                        if (listObjectsResponse.S3Objects == null || listObjectsResponse.S3Objects.Count() == 1)
                        {
                            return index;
                        }

                        var images = listObjectsResponse.S3Objects.Select(p => p.Key.Replace(imageFolderPath, "")).Where(p => !string.IsNullOrEmpty(p)).Select(p => p.Replace(".jpg", "")).OrderBy(p => int.Parse(p));
                        foreach (var image in images)
                        {
                            index++;
                            if (index.ToString() == image)
                            {
                                continue;
                            }

                            CopyObjectRequest copyObjectRequest = new CopyObjectRequest()
                            {
                                SourceBucket = awsBucketName,
                                DestinationBucket = awsBucketName,
                                SourceKey = string.Format("{0}{1}.jpg", imageFolderPath, image),
                                DestinationKey = string.Format("{0}{1}.jpg", imageFolderPath, index)
                            };

                            CopyObjectResponse copyObectResponse = client.CopyObject(copyObjectRequest);

                            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = string.Format("{0}{1}", imageFolderPath, image + ".jpg")

                            };

                            client.DeleteObject(deleteObjectRequest);
                        }
                        if (listObjectsResponse.IsTruncated)
                        {
                            listObjectsRequest.Marker = listObjectsResponse.NextMarker;
                        }
                        else
                        {
                            listObjectsRequest = null;
                        }
                    }

                } while (listObjectsRequest != null);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {

            }

            return index;
        }


        public static int SwapingMasterHotelImages(string awsBucketName, string imageFolderPath, string imageId)
        {

            int index = 0;
            try
            {
                IAmazonS3 client;
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                ListObjectsRequest listObjectsRequest = new ListObjectsRequest
                {
                    BucketName = awsBucketName,
                    Prefix = imageFolderPath,
                };

                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {
                    var uniqueId = System.Guid.NewGuid().ToString().Substring(0, 10);
                    CopyObjectRequest copyObjectRequest = new CopyObjectRequest()
                    {
                        SourceBucket = awsBucketName,
                        DestinationBucket = awsBucketName,
                        SourceKey = string.Format("{0}{1}.jpg", imageFolderPath, imageId),
                        DestinationKey = string.Format("{0}{1}.jpg", imageFolderPath, uniqueId)
                    };

                    CopyObjectResponse copyObectResponse = client.CopyObject(copyObjectRequest);

                    DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", imageFolderPath, imageId + ".jpg")

                    };
                    client.DeleteObject(deleteObjectRequest);

                    copyObjectRequest = new CopyObjectRequest()
                    {
                        SourceBucket = awsBucketName,
                        DestinationBucket = awsBucketName,
                        SourceKey = string.Format("{0}{1}.jpg", imageFolderPath, 1),
                        DestinationKey = string.Format("{0}{1}.jpg", imageFolderPath, imageId)
                    };

                    copyObectResponse = client.CopyObject(copyObjectRequest);
                    deleteObjectRequest = new DeleteObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", imageFolderPath, 1 + ".jpg")

                    };
                    client.DeleteObject(deleteObjectRequest);

                    copyObjectRequest = new CopyObjectRequest()
                    {
                        SourceBucket = awsBucketName,
                        DestinationBucket = awsBucketName,
                        SourceKey = string.Format("{0}{1}.jpg", imageFolderPath, uniqueId),
                        DestinationKey = string.Format("{0}{1}.jpg", imageFolderPath, 1)
                    };

                    copyObectResponse = client.CopyObject(copyObjectRequest);

                    deleteObjectRequest = new DeleteObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = string.Format("{0}{1}", imageFolderPath, uniqueId + ".jpg")

                    };

                    client.DeleteObject(deleteObjectRequest);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {

            }

            return index;
        }

        //public static List<string> GetMasterHotelTags(string awsBucketName, string imageFolderPath)
        //{
        //    List<string> HotelStyles = new List<string>();
        //    int index = 0;
        //    try
        //    {
        //        IAmazonS3 client;
        //        string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
        //        string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
        //        ListObjectsRequest listObjectsRequest = new ListObjectsRequest
        //        {
        //            BucketName = awsBucketName,
        //            Prefix = imageFolderPath,
        //        };

        //        do
        //        {
        //            using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
        //            {
        //                ListObjectsResponse listObjectsResponse = client.ListObjects(listObjectsRequest);
        //                var objects = listObjectsResponse.S3Objects;
        //                foreach (var mhObject in objects)
        //                {
        //                    var imageStream = DownloadS3Object(awsBucketName, mhObject.Key);
        //                    StreamReader reader = new StreamReader(imageStream);
        //                    string json = reader.ReadToEnd();
        //                    MHHotelTags guideDestination = JsonConvert.DeserializeObject<MHHotelTags>(json);
        //                    if (guideDestination.hotelStyle != null && guideDestination.hotelStyle.Count() > 0)
        //                    {
        //                        HotelStyles.AddRange(guideDestination.hotelStyle);
        //                    }
        //                }

        //                if (listObjectsResponse.IsTruncated)
        //                {
        //                    listObjectsRequest.Marker = listObjectsResponse.NextMarker;
        //                }
        //                else
        //                {
        //                    listObjectsRequest = null;
        //                }
        //            }
        //        } while (listObjectsRequest != null);
        //    }
        //    catch (AmazonS3Exception amazonS3Exception)
        //    {

        //    }

        //    return HotelStyles;
        //}

        //public class MHHotelTags
        //{
        //    public List<string> hotelStyle { get; set; }
        //}

        public static MHIDStaticInfo GetMasterHotelInfo(string mhid, out string cacheTime)
        {
            MHIDStaticInfo mhidStaticInfo = null;
            cacheTime = string.Empty;

            try
            {
                string hotelInfo = string.Empty;
                TAInfo taInfo = new TAInfo();
                TAFeatures taFeatures = new TAFeatures();
                hotelInfo = RedisDataHelper.GetRedisValueByKey(mhid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));

                if (!string.IsNullOrWhiteSpace(hotelInfo) && (DateTime.Parse(JsonConvert.DeserializeObject<MHIDStaticInfo>(hotelInfo).insertedTime).AddDays(int.Parse(ConfigurationManager.AppSettings["MHDescRedisCacheTime"])) - DateTime.UtcNow).TotalSeconds > 0)
                {
                    mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(hotelInfo);
                    cacheTime = (DateTime.Parse(mhidStaticInfo.insertedTime).AddDays(int.Parse(ConfigurationManager.AppSettings["MHDescRedisCacheTime"])) - DateTime.UtcNow).TotalSeconds.ToString();
                }
                else
                {
                    var mhidURL = ConfigurationManager.AppSettings["MHIDStaticInfo"] + mhid + ".json";
                    ErrorLogger.Log("\n MasterHotelId Url: " + mhidURL, LogLevel.Information);
                    mhidStaticInfo = Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], mhid));
                    if (mhidStaticInfo != null)
                    {
                        var mhidTAUrl = ConfigurationManager.AppSettings["MHIDTAInfo"] + mhid + ".json";
                        var mhidTAInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUrl);
                        mhidStaticInfo.insertedTime = DateTime.Now.AddDays(-int.Parse(ConfigurationManager.AppSettings["MHDescRedisCacheTime"])).ToString("s"); // "2016-09-01T14:09:47";
                        if (!string.IsNullOrWhiteSpace(mhidTAInfoResponse))
                        {
                            taInfo = JsonConvert.DeserializeObject<TAInfo>(mhidTAInfoResponse);
                            mhidStaticInfo.TripAdvisor.TAID = taInfo.tripAdvisorId;
                            mhidStaticInfo.TripAdvisor.reviewsCount = !string.IsNullOrEmpty(taInfo.numberOfReviews) ? int.Parse(taInfo.numberOfReviews) : 0;
                            mhidStaticInfo.TripAdvisor.TARating = !string.IsNullOrEmpty(taInfo.rating) ? double.Parse(taInfo.rating) : 0;
                            mhidStaticInfo.insertedTime = DateTime.UtcNow.ToString("s");
                            cacheTime = "259200";
                        }
                        var mhidTAUtilUrl = ConfigurationManager.AppSettings["MHIDTAUtilInfo"] + mhid + ".json";
                        string mhidTAUtilResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUtilUrl);
                        if (!string.IsNullOrWhiteSpace(mhidTAUtilResponse))
                        {
                            taFeatures = JsonConvert.DeserializeObject<TAFeatures>(mhidTAUtilResponse);
                            if (taFeatures.noOfRooms != 0)
                            {
                                mhidStaticInfo.TripAdvisor.noOfRooms = taFeatures.noOfRooms;
                            }
                            if (taFeatures.hotelStyle != null)
                            {
                                mhidStaticInfo.TripAdvisor.hotelStyle = taFeatures.hotelStyle;
                            }
                        }
                        RedisDataHelper.SetHotelInfoRedisValueByKey(mhid, mhidStaticInfo, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));
                        MHDescription mhDescription = new MHDescription();
                        mhDescription.insertedTime = DateTime.UtcNow.ToString("s");
                        mhDescription.description = mhidStaticInfo._source.Description;
                        RedisDataHelper.SetHotelDescriptionRedisValueByKey(mhid, mhDescription, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), WCFRestService.Model.Enum.LogLevel.Critical);
            }
            if (Global.mhidtoImageCountList.ContainsKey(mhid))
            {
                mhidStaticInfo._source.ImageCount = Global.mhidtoImageCountList[mhid];
            }
            return mhidStaticInfo;
        }

        public static MHIDStaticInfo DownloadS3ObjectForMasterHotel(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    var etag = string.Empty;
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                        etag = response.ETag;
                    }

                    StreamReader reader = new StreamReader(imageStream, Encoding.UTF8, true);
                    imageStream.Position = 0;
                    var result = reader.ReadToEnd();
                    var mhIDStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(result);
                    mhIDStaticInfo.ETag = etag;
                    return mhIDStaticInfo;
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }

        public static string GetEtag(string awsBucketName, string keyName)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {

                    GetObjectRequest request = new GetObjectRequest { BucketName = awsBucketName, Key = keyName };
                    var etag = string.Empty;
                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        response.ResponseStream.CopyTo(imageStream);
                        etag = response.ETag;
                    }

                    StreamReader reader = new StreamReader(imageStream, Encoding.UTF8, true);
                    imageStream.Position = 0;
                    return etag;
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }

        public static void TriggerMail(string url, int value, string email)
        {

            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.cloudzimail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("dev.du1@teletext-holidays.co.uk", "TextDays@11");
            string body = string.Empty;
            if (value == 1)
            {
                body = "Request for purging the " + url + " has been accepted and will process in 4 Min";
            }
            else
            {
                body = "Sorry you cannot make the request\nKindly register your email Id";
            }
            MailMessage mm1 = new MailMessage("yes-reply@teletext-holidays.co.uk", email, "Purge-Request", body);
            mm1.BodyEncoding = UTF8Encoding.UTF8;
            mm1.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.Send(mm1);

        }

        public static bool SendEmail(string subject, string emailBody, string email)
        {
            string fromName = ConfigurationManager.AppSettings["EmailFromName"];
            string fromEmail = ConfigurationManager.AppSettings["EmailFromEmail"];
            string port = ConfigurationManager.AppSettings["SMTPPort"];
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            string userName = ConfigurationManager.AppSettings["SMTPUserName"];
            string password = ConfigurationManager.AppSettings["EmailPassword"];

            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(port);
                client.Host = smtpServer;// "smtp.mailgun.org";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(userName, password);
                MailMessage mm1 = new MailMessage();
                foreach (var toEmail in email.Split(';'))
                {
                    mm1.To.Add(toEmail);
                }
                mm1.From = new MailAddress(fromEmail);
                mm1.Subject = subject;
                mm1.Body = emailBody;
                mm1.IsBodyHtml = true;
                mm1.BodyEncoding = UTF8Encoding.UTF8;
                mm1.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.Send(mm1);

            }
            catch (Exception ex)
            {
                //Log.Error("Unable to send reject email", ex, this);
                return false;
            }

            return true;
        }

        public static List<DynamicSEO.Category> GetCategoryList()
        {
            try
            {
                var categoryListJSON = "";

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterCategoryJSON"])))
                {
                    categoryListJSON = reader.ReadToEnd();
                }
                var categoryList = JsonConvert.DeserializeObject<List<DynamicSEO.Category>>(categoryListJSON);

                return categoryList;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        public static DynamicSEO.Holidays GetHoliday(string fileName)
        {
            try
            {
                var holidayJSON = string.Empty;
                string awsBucketName = string.Empty;
                string awsInputFilePath = string.Empty;

                bool isDynamicSEOPage = fileName.Contains("-in-");

                if (isDynamicSEOPage)
                {
                    awsBucketName = ConfigurationManager.AppSettings["DynamicSEOBucket"];
                    awsInputFilePath = ConfigurationManager.AppSettings["HolidaysJSON"];
                }
                else
                {
                    awsBucketName = ConfigurationManager.AppSettings["DynamicSEOBucket"];
                    awsInputFilePath = ConfigurationManager.AppSettings["HotelListingJSON"];
                }
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(awsBucketName, string.Format(awsInputFilePath, fileName))))
                {
                    holidayJSON = reader.ReadToEnd();
                }
                var holiday = JsonConvert.DeserializeObject<DynamicSEO.Holidays>(holidayJSON);

                return holiday;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }


        public static List<DynamicSEO.Template> GetTemplateList()
        {
            try
            {
                var templateListJSON = "";

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterTemplateJSON"])))
                {
                    templateListJSON = reader.ReadToEnd();
                }
                var templateList = JsonConvert.DeserializeObject<List<DynamicSEO.Template>>(templateListJSON);

                return templateList;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }


        public static string BuildHashCodeForObject(Object obj)
        {
            if (obj != null)
            {
                string objectString = JsonConvert.SerializeObject(obj);
                if (!string.IsNullOrWhiteSpace(objectString))
                {
                    byte[] tmpHash = new MD5CryptoServiceProvider().ComputeHash(UTF8Encoding.UTF8.GetBytes(objectString));

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < tmpHash.Length; i++)
                    {
                        sb.Append(tmpHash[i].ToString("X2"));
                    }
                    return sb.ToString();
                }
            }
            return null;
        }

        public static string GetNearestAirportGroup(double userLat, double userLong)
        {
            var airportGroups = GetAirportGroup();
            double distance = 999999999;
            string airportName = string.Empty;

            foreach (var airportGroup in airportGroups)
            {
                var dist = GetDistance(userLat, userLong, airportGroup.latitude, airportGroup.longitude) / 1000;
                if (dist < distance)
                {
                    distance = dist;
                    airportName = airportGroup.Name;
                }
            }
            return airportName;
        }

        public static List<Presentation.WCFRestService.Model.HomePage.AirportGroup> GetAirportGroup()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpRuntime.AppDomainAppPath + @"data\AirportGroupsCentroid.xml");
            XmlNodeList airportNodesList = xml.SelectNodes("/AirportGroups/AirportGroup");
            List<Presentation.WCFRestService.Model.HomePage.AirportGroup> airportGroups = new List<Presentation.WCFRestService.Model.HomePage.AirportGroup>();
            foreach (XmlNode airportNode1 in airportNodesList)
            {
                var airportGroup = new Presentation.WCFRestService.Model.HomePage.AirportGroup();
                airportGroup.Id = airportNode1.Attributes["Id"].Value;
                airportGroup.Name = airportNode1.Attributes["Value"].Value;
                airportGroup.latitude = double.Parse(airportNode1.Attributes["Latitude"].Value);
                airportGroup.longitude = double.Parse(airportNode1.Attributes["Longitude"].Value);
                airportGroups.Add(airportGroup);
            }

            return airportGroups;
        }

        public static double GetDistance(double sLatitude, double sLongitude, double eLatitude, double eLongitude) //working
        {
            var sCoord = new GeoCoordinate(sLatitude, sLongitude);
            var eCoord = new GeoCoordinate(eLatitude, eLongitude);

            return sCoord.GetDistanceTo(eCoord);
        }

        public static bool ExecuteGetIndiceWebRequest(string indice, string dataType)
        {
            string date = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
            string clusterURL = ConfigurationManager.AppSettings["ES-ClusterUrl"];
            string webReqURL = clusterURL + indice + "-" + date;
            string result = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    result = reader.ReadToEnd();
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (WebException ex)
            {
                string postData = "{\"settings\":{\"number_of_shards\":5,\"number_of_replicas\":1},\"mappings\":{\"" + dataType + "\":{}}}";
                var resp = Utilities.ExecuteIndicePostWebRequest(webReqURL, postData);
                if (!string.IsNullOrWhiteSpace(resp))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        public static string ExecuteIndicePostWebRequest(string url, string postData)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                var result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                return null;
            }
        }
        public static void TriggerIndiceInfoMail(int t)
        {
            string exceptionBody = string.Empty;
            if (t < 3)
            {
                exceptionBody = "Please check the Indices\n They are not created properly =>" + DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
            }
            else
            {
                exceptionBody = "All Indices Created Succesfully for the day =>" + DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
            }
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.cloudzimail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["email"], ConfigurationManager.AppSettings["password-es"]);
            MailMessage mm1 = new MailMessage();
            mm1.To.Add(ConfigurationManager.AppSettings["ToList"]);
            mm1.From = new MailAddress("Elasticsearch-Indices@teletext-holidays.co.uk");
            mm1.Subject = "Indices-States";
            mm1.Body = exceptionBody;
            mm1.CC.Add(ConfigurationManager.AppSettings["CCList"]);
            mm1.BodyEncoding = UTF8Encoding.UTF8;
            mm1.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.Send(mm1);

        }

        public static void DeleteTimeBasedIndice(string indice, string dataType, int days)
        {
            string date = DateTime.UtcNow.AddDays(days).ToString("yyyy-MM-dd");
            string clusterURL = ConfigurationManager.AppSettings["ES-ClusterUrl"];
            string webReqURL = clusterURL + indice + "-" + date;
            string postData = "{\"settings\":{\"number_of_shards\":5,\"number_of_replicas\":1},\"mappings\":{\"" + dataType + "\":{}}}";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webReqURL);
                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                var result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();
            }
            catch (WebException ex)
            {

            }
        }

        public static List<OverseasDestination> GetOverseasDestination()
        {
            List<OverseasDestination> overseasList = new List<OverseasDestination>();
            var json = String.Empty;
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object("teletext-deploy", "webapp/js/data/destinations-overseas.json")))
            {
                json = reader.ReadToEnd();
            }
            overseasList = JsonConvert.DeserializeObject<List<OverseasDestination>>(json);

            return overseasList;
        }
        public static string GetDestinationId(string parentDestinationName)
        {
            string destinationId = string.Empty;
            List<OverseasDestination> overseasDestinationList = Global.overseasDestinationList;

            foreach (OverseasDestination destination in overseasDestinationList)
            {
                if (destination.DestinationName.ToLower() == parentDestinationName.ToLower())
                {
                    destinationId = destination.DestinationId;
                    break;
                }
                if (destination.More != null && destination.More.Length > 0)
                {
                    foreach (More moreDestination in destination.More)
                    {
                        if (moreDestination.DestinationName.ToLower() == parentDestinationName.ToLower())
                        {
                            destinationId = moreDestination.DestinationId;
                            break;
                        }
                    }
                }
            }
            return destinationId;
        }

        public static Dictionary<string, string> GetDestinationPeakSeasons()
        {
            Dictionary<string, string> destinationIdToPeakSeason = new Dictionary<string, string>();
            var peakseasonsJson = "";

            // Get the peak month information from Json
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFileBucketName"], ConfigurationManager.AppSettings["DestinationPeakSeasonJSON"])))
            {
                peakseasonsJson = reader.ReadToEnd();
            }

            destinationIdToPeakSeason = JsonConvert.DeserializeObject<Dictionary<string, string>>(peakseasonsJson);

            return destinationIdToPeakSeason;
        }
    }
}




