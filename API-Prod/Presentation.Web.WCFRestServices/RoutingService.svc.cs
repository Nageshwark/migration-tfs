﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Routing;
using ServiceStack.Redis;
using Presentation.Web.WCFRestServices.Routing;

namespace Presentation.Web.WCFRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RoutingService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RoutingService.svc or RoutingService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class RoutingService : IRoutingService
    {
        #region ProcessS3RecordsByAirportCodeAndResortId for First Set
        public Stream ProcessS3RecordsByAirportCodeAndResortIdFirstSet()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest getFirstSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.FirstSetSuccessfullyWrittenPath + "/" +
                              Global.FirstSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse firstSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(getFirstSetSuccessfullyWrittenObjectRequest))
                    {

                        if (firstSetSuccessfullyWrittenObjectResponse != null &&
                            firstSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            firstSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten;

                            using (Stream stream = firstSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForFirstSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop;
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForFirstSet(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForFirstSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdFirstSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForFirstSet(List<string> fileNames)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForResortIdAndAirportCode;
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    
                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMapping.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMapping[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForFirstSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }
        #endregion
        
        #region ProcessS3RecordsByAirportCodeAndResortId for Second Set

        public Stream ProcessS3RecordsByAirportCodeAndResortIdSecondSet()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest secondSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.SecondSetSuccessfullyWrittenPath + "/" +
                              Global.SecondSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse secondSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(secondSetSuccessfullyWrittenObjectRequest))
                    {

                        if (secondSetSuccessfullyWrittenObjectResponse != null &&
                            secondSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            secondSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = secondSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForSecondSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForSecondSet(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForSecondSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdSecondSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }


            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }


        private static void DownloadS3FileAndInsertIntoRedisForSecondSet(List<string> fileNames)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForResortIdAndAirportCode;
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    
                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMapping.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMapping[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForSecondSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion

        #region Process S3 Records By Airport Code and Resort Id for the Third Set
        public Stream ProcessS3RecordsByAirportCodeAndResortIdThirdSet()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest thirdSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.ThirdSetSuccessfullyWrittenPath + "/" +
                              Global.ThirdSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse thirdSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(thirdSetSuccessfullyWrittenObjectRequest))
                    {

                        if (thirdSetSuccessfullyWrittenObjectResponse != null &&
                            thirdSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            thirdSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = thirdSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForThirdSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForThirdSet(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForThirdSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdThirdSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForThirdSet(List<string> fileNames)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForResortIdAndAirportCode;
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMapping.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMapping[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    ErrorLogger.Log(
                                        "\nError occurred while inserting record for: " + fileName + ".\nException is: " +
                                        exception.Message + ".\nStackTrace: " + exception.StackTrace,
                                        LogLevel.RoutingApiError);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForThirdSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }
        #endregion End of Process S3 Records By Airport Code and Resort Id for the Third Set

        #region Process S3 Records By Airport Code and Resort Id for the Fourth Set
        public Stream ProcessS3RecordsByAirportCodeAndResortIdFourthSet()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest getFirstSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.FourthSetSuccessfullyWrittenPath + "/" +
                              Global.FourthSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse fourthSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(getFirstSetSuccessfullyWrittenObjectRequest))
                    {

                        if (fourthSetSuccessfullyWrittenObjectResponse != null &&
                            fourthSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            fourthSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = fourthSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForFourthSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForFourthSet(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForFourthSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdFourthSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForFourthSet(List<string> fileNames)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForResortIdAndAirportCode;
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream)
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    
                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMapping.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMapping[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    ErrorLogger.Log(
                                        "\nError occurred while inserting record for: " + fileName + ".\nException is: " +
                                        exception.Message + ".\nStackTrace: " + exception.StackTrace,
                                        LogLevel.RoutingApiError);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForFirstSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion End of Process S3 Records By Airport Code and Resort Id for the Fourth Set
        
        #region Process S3 Records By Airport Code
        public Stream ProcessS3RecordsByAirportCode()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest airprotCodesSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.AirportCodesSuccessfullyWrittenPath + "/" +
                              Global.AirportCodesSuccessfullyWrittenFileName
                    };

                    using (
                        GetObjectResponse airportCodesSuccessfullyWrittenObjectResponse =
                            s3Client.GetObject(airprotCodesSuccessfullyWrittenObjectRequest))
                    {
                        if (airportCodesSuccessfullyWrittenObjectResponse != null &&
                            airportCodesSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            airportCodesSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string airportCodesSuccessfullyWritten = String.Empty;

                            using (Stream stream = airportCodesSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);

                                airportCodesSuccessfullyWritten = streamReader.ReadToEnd();
                            }

                            if (!String.IsNullOrEmpty(airportCodesSuccessfullyWritten))
                            {
                                string[] airportCodes = airportCodesSuccessfullyWritten.Split(',');

                                response.Count = airportCodes.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(airportCodes.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForInsertingAirportCodes = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            airportCodes.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            airportCodes.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForAirportCodes(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForInsertingAirportCodes.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }

                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCode. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForAirportCodes(List<string> airportCodes)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest routingDataByAirportCodeObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForAirportCode;
                            foreach (string fileName in airportCodes)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCode + "/" + fileName;

                                RoutingDataByAirportCode routingDataByAirportCode = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8,
                                                    false);

                                                routingDataByAirportCode =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCode>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCode != null)
                                                {
                                                    if (
                                                        Global.AirportCodeToDepartureIdMapping.ContainsKey(
                                                            routingDataByAirportCode.airportCode))
                                                    {
                                                        routingDataByAirportCode.departureId =
                                                            Global.AirportCodeToDepartureIdMapping[
                                                                routingDataByAirportCode.airportCode];
                                                    }

                                                    routingDataByAirportCode.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                string airportCode = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                if (Global.AirportCodeToDepartureIdMapping.ContainsKey(airportCode))
                                {
                                    int departureId = Global.AirportCodeToDepartureIdMapping[airportCode];

                                    if (routingDataByAirportCode != null)
                                    {
                                        redisClient.Set(departureId.ToString(), routingDataByAirportCode);
                                    }
                                }
                                else
                                {
                                    if (routingDataByAirportCode != null)
                                    {
                                        redisClient.Set(airportCode, routingDataByAirportCode);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForFirstSet. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion End of Processing S3 Records by Airport Code

        #region Processing S3 Records By Resort Id
        public Stream ProcessS3RecordsByResortId()
        {
            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest resortIdsSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName,
                        Key = Global.ResortIdsSuccessfullyWrittenPath + "/" +
                              Global.ResortIdsSuccessfullyWrittenFileName
                    };

                    using (
                        GetObjectResponse resortIdsSuccessfullyWrittenObjectResponse =
                            s3Client.GetObject(resortIdsSuccessfullyWrittenObjectRequest))
                    {
                        if (resortIdsSuccessfullyWrittenObjectResponse != null &&
                            resortIdsSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            resortIdsSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string resortIdsSuccessfullyWritten = String.Empty;

                            using (Stream stream = resortIdsSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);

                                resortIdsSuccessfullyWritten = streamReader.ReadToEnd();
                            }

                            if (!String.IsNullOrEmpty(resortIdsSuccessfullyWritten))
                            {
                                string[] resortIds = resortIdsSuccessfullyWritten.Split(',');

                                response.Count = resortIds.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(resortIds.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForInsertingResortIds = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {

                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            resortIds.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            resortIds.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForResortIds(fileNamesToProcessInThisLoop);
                                    });

                                    tasksCreatedForInsertingResortIds.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }

                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByResortIds. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForResortIds(List<string> resortIdsToBeProccessed)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = Global.RedisDbForResortId;
                            foreach (string fileName in resortIdsToBeProccessed)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByResortId + "/" + fileName;

                                RoutingDataByResortId routingDataByResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8,
                                                    false);

                                                routingDataByResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByResortId != null)
                                                {
                                                    routingDataByResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }

                                                if (routingDataByResortId != null &&
                                                    routingDataByResortId.airportCodes != null &&
                                                    routingDataByResortId.airportCodes.Count > 0)
                                                {
                                                    foreach (string airportCode in routingDataByResortId.airportCodes)
                                                    {
                                                        List<int> departureIds = new List<int>();

                                                        Global.AirportCodeToMultipleDepartureIdsMapping.TryGetValue(
                                                            airportCode,
                                                            out departureIds);

                                                        if (departureIds != null)
                                                        {
                                                            routingDataByResortId.departureIds.AddRange(
                                                                departureIds.Except(routingDataByResortId.departureIds)
                                                                    .ToList());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                if (routingDataByResortId != null && fileName.IndexOf('.') > -1)
                                {
                                    redisClient.Set(fileName.Split('.')[0], routingDataByResortId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForResortId. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }

        }

        #endregion End of Processing S3 Records By Resort Id

        #region Routing methods for IE

        #region ProcessS3RecordsByAirportCodeAndResortId for First Set MT
        public Stream ProcessS3RecordsByAirportCodeAndResortIdFirstSetMT(string countrySite)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByAirportCodeAndResortIdFirstSet();
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest getFirstSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.FirstSetSuccessfullyWrittenPath + "/" +
                              Global.FirstSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse firstSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(getFirstSetSuccessfullyWrittenObjectRequest))
                    {

                        if (firstSetSuccessfullyWrittenObjectResponse != null &&
                            firstSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            firstSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten;

                            using (Stream stream = firstSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForFirstSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop;
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForFirstSetMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForFirstSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdFirstSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForFirstSetMT(List<string> fileNames, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite)
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForResortIdAndAirportCode");
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMappingMT[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForFirstSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }
        #endregion

        #region ProcessS3RecordsByAirportCodeAndResortId for Second Set MT

        public Stream ProcessS3RecordsByAirportCodeAndResortIdSecondSetMT(string countrySite)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByAirportCodeAndResortIdSecondSet();
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest secondSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.SecondSetSuccessfullyWrittenPath + "/" +
                              Global.SecondSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse secondSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(secondSetSuccessfullyWrittenObjectRequest))
                    {

                        if (secondSetSuccessfullyWrittenObjectResponse != null &&
                            secondSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            secondSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = secondSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForSecondSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForSecondSetMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForSecondSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdSecondSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }


            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }


        private static void DownloadS3FileAndInsertIntoRedisForSecondSetMT(List<string> fileNames, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite)
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForResortIdAndAirportCode");
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMappingMT[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForSecondSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion

        #region Process S3 Records By Airport Code and Resort Id for the Third Set MT
        public Stream ProcessS3RecordsByAirportCodeAndResortIdThirdSetMT(string countrySite)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByAirportCodeAndResortIdThirdSet();
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest thirdSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.ThirdSetSuccessfullyWrittenPath + "/" +
                              Global.ThirdSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse thirdSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(thirdSetSuccessfullyWrittenObjectRequest))
                    {

                        if (thirdSetSuccessfullyWrittenObjectResponse != null &&
                            thirdSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            thirdSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = thirdSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForThirdSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForThirdSetMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForThirdSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdThirdSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForThirdSetMT(List<string> fileNames, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = Global.RoutingApiS3BucketName
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForResortIdAndAirportCode");
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMappingMT[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    ErrorLogger.Log(
                                        "\nError occurred while inserting record for: " + fileName + ".\nException is: " +
                                        exception.Message + ".\nStackTrace: " + exception.StackTrace,
                                        LogLevel.RoutingApiError);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForThirdSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }
        #endregion End of Process S3 Records By Airport Code and Resort Id for the Third Set

        #region Process S3 Records By Airport Code and Resort Id for the Fourth Set MT
        public Stream ProcessS3RecordsByAirportCodeAndResortIdFourthSetMT(string countrySite)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByAirportCodeAndResortIdFourthSet();
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }

            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest getFirstSetSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.FourthSetSuccessfullyWrittenPath + "/" +
                              Global.FourthSetSuccessfullyWrittenFileName
                    };

                    using (GetObjectResponse fourthSetSuccessfullyWrittenObjectResponse = s3Client.GetObject(getFirstSetSuccessfullyWrittenObjectRequest))
                    {

                        if (fourthSetSuccessfullyWrittenObjectResponse != null &&
                            fourthSetSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            fourthSetSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string fileNamesSuccessfullyWritten = String.Empty;

                            using (Stream stream = fourthSetSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);
                                fileNamesSuccessfullyWritten = streamReader.ReadToEnd();
                            }
                            if (!String.IsNullOrEmpty(fileNamesSuccessfullyWritten))
                            {
                                string[] fileNames = fileNamesSuccessfullyWritten.Split(',');

                                response.Count = fileNames.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(fileNames.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForFourthSet = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            fileNames.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForFourthSetMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForFourthSet.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeAndResortIdFourthSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        private static void DownloadS3FileAndInsertIntoRedisForFourthSetMT(List<string> fileNames, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByAirportCodeAndResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite)
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForResortIdAndAirportCode");
                            foreach (string fileName in fileNames)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeAndResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCodeAndResortId + "/" + fileName;

                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeAndResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream)
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true);

                                                routingDataByAirportCodeAndResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCodeAndResortId != null)
                                                {
                                                    routingDataByAirportCodeAndResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                try
                                {
                                    string temp = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                    string airportCode = temp.IndexOf('_') > 0 ? temp.Split('_')[0] : temp;
                                    string resortId = temp.IndexOf('_') > 0 ? temp.Split('_')[1] : temp;

                                    string redisFileName = String.Empty;

                                    if (Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airportCode))
                                    {
                                        int departureId = Global.AirportCodeToDepartureIdMappingMT[airportCode];
                                        redisFileName = departureId + "_" + resortId;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                    else
                                    {
                                        redisFileName = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;
                                        redisClient.Set(redisFileName, routingDataByAirportCodeAndResortId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    ErrorLogger.Log(
                                        "\nError occurred while inserting record for: " + fileName + ".\nException is: " +
                                        exception.Message + ".\nStackTrace: " + exception.StackTrace,
                                        LogLevel.RoutingApiError);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForFourthSetMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion End of Process S3 Records By Airport Code and Resort Id for the Fourth Set

        #region Process S3 Records By Airport Code MT

        /// This method is invoked after processing TTSS file (by writing contents to S3 based on AWS lambda RoutingApi_InsertBy_AirportCode_ResortIdIndividually) 
        ///  which have AirportCode and corresponding ResortIds, Dates and few other details.This method will read the S3 contents(all airport specific Jsons) and writes/updates the same data in Redis
        /// If method is successful, the airport code count will be returned else specific error related to the issue in processing will be returned
        public Stream ProcessS3RecordsByAirportCodeMT(string countrySite)
        {
            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByAirportCode();
            }

            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }


            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest airprotCodesSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.AirportCodesSuccessfullyWrittenPath + "/" +
                              Global.AirportCodesSuccessfullyWrittenFileName
                    };


                    using (
                        GetObjectResponse airportCodesSuccessfullyWrittenObjectResponse =
                            s3Client.GetObject(airprotCodesSuccessfullyWrittenObjectRequest))
                    {
                        if (airportCodesSuccessfullyWrittenObjectResponse != null &&
                            airportCodesSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            airportCodesSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string airportCodesSuccessfullyWritten = String.Empty;

                            using (Stream stream = airportCodesSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);

                                airportCodesSuccessfullyWritten = streamReader.ReadToEnd();
                            }

                            if (!String.IsNullOrEmpty(airportCodesSuccessfullyWritten))
                            {
                                string[] airportCodes = airportCodesSuccessfullyWritten.Split(',');

                                response.Count = airportCodes.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(airportCodes.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForInsertingAirportCodes = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {
                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            airportCodes.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            airportCodes.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForAirportCodesMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForInsertingAirportCodes.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }

                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByAirportCodeMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        /// <summary>
        /// This method downloads the AirportCode specific data (Json) files (which has airportcode, resort ids, dates etc mapping ) and updates/adds data into Redis
        /// </summary>
        /// <param name="airportCodes"></param>
        /// <param name="countrySite"></param>
        private static void DownloadS3FileAndInsertIntoRedisForAirportCodesMT(List<string> airportCodes, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {

                    GetObjectRequest routingDataByAirportCodeObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite)
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForAirportCode");
                            foreach (string fileName in airportCodes)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByAirportCodeObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByAirportCode + "/" + fileName;

                                RoutingDataByAirportCode routingDataByAirportCode = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByAirportCodeObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8,
                                                    false);

                                                routingDataByAirportCode =
                                                    JsonConvert.DeserializeObject<RoutingDataByAirportCode>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByAirportCode != null)
                                                {
                                                    if (
                                                        Global.AirportCodeToDepartureIdMappingMT.ContainsKey(
                                                            routingDataByAirportCode.airportCode))
                                                    {
                                                        routingDataByAirportCode.departureId =
                                                            Global.AirportCodeToDepartureIdMappingMT[
                                                                routingDataByAirportCode.airportCode];
                                                    }

                                                    routingDataByAirportCode.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                string airportCode = fileName.IndexOf('.') > 0 ? fileName.Split('.')[0] : fileName;

                                if (Global.AirportCodeToDepartureIdMappingMT.ContainsKey(airportCode))
                                {
                                    int departureId = Global.AirportCodeToDepartureIdMappingMT[airportCode];

                                    if (routingDataByAirportCode != null)
                                    {
                                        redisClient.Set(departureId.ToString(), routingDataByAirportCode);
                                    }
                                }
                                else
                                {
                                    if (routingDataByAirportCode != null)
                                    {
                                        redisClient.Set(airportCode, routingDataByAirportCode);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForAirportCodesMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }
        }

        #endregion End of Processing S3 Records by Airport Code

        #region Processing S3 Records By Resort Id MT
        /// This method is invoked after processing TTSS file (by writing contents to S3 based on AWS lambda RoutingApi_InsertBy_AirportCode_ResortIdIndividually) 
        ///   which have ResortIds and airport codes, Dates and few other details.. This method will read the S3 contents(all airport specific Jsons) and writes/updates the same data in Redis
        /// If method is successful, the resort id count will be returned else specific error related to the issue in processing will be returned
        /// <param name="countrySite"></param>
        /// <returns></returns>
        public Stream ProcessS3RecordsByResortIdMT(string countrySite)
        {

            if (string.IsNullOrWhiteSpace(countrySite))
            {
                return ProcessS3RecordsByResortId();
            }


            countrySite = countrySite.ToUpper();
            if (!countrySite.Equals(Global.COUNTRY_SITE_IE))
            {
                throw new WebFaultException<string>("Unsupported Countrysite", HttpStatusCode.NotImplemented);
            }





            RoutingResponseToLambda response = new RoutingResponseToLambda();

            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest resortIdsSuccessfullyWrittenObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite),
                        Key = Global.ResortIdsSuccessfullyWrittenPath + "/" +
                              Global.ResortIdsSuccessfullyWrittenFileName
                    };

                    using (
                        GetObjectResponse resortIdsSuccessfullyWrittenObjectResponse =
                            s3Client.GetObject(resortIdsSuccessfullyWrittenObjectRequest))
                    {
                        if (resortIdsSuccessfullyWrittenObjectResponse != null &&
                            resortIdsSuccessfullyWrittenObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                            resortIdsSuccessfullyWrittenObjectResponse.ContentLength > 0)
                        {
                            string resortIdsSuccessfullyWritten = String.Empty;

                            using (Stream stream = resortIdsSuccessfullyWrittenObjectResponse.ResponseStream)
                            {
                                StreamReader streamReader = new StreamReader(stream);

                                resortIdsSuccessfullyWritten = streamReader.ReadToEnd();
                            }

                            if (!String.IsNullOrEmpty(resortIdsSuccessfullyWritten))
                            {
                                string[] resortIds = resortIdsSuccessfullyWritten.Split(',');

                                response.Count = resortIds.Length;

                                int numberOfLoops = Int32.Parse(ConfigurationManager.AppSettings["NumberOfThreadsForResortId"]);
                                int remainder = 0;
                                int numberOfItemsPerLoop = Math.DivRem(resortIds.Length, numberOfLoops, out remainder);
                                int numberOfItemsForTheLastBatch = numberOfItemsPerLoop + remainder;

                                List<Task> tasksCreatedForInsertingResortIds = new List<Task>();

                                for (int loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
                                {

                                    List<string> fileNamesToProcessInThisLoop = new List<string>();
                                    if (loopIndex == numberOfLoops - 1)
                                    {
                                        fileNamesToProcessInThisLoop =
                                            resortIds.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsForTheLastBatch)
                                                .ToList();
                                    }
                                    else
                                    {
                                        fileNamesToProcessInThisLoop =
                                            resortIds.Skip(loopIndex * numberOfItemsPerLoop)
                                                .Take(numberOfItemsPerLoop)
                                                .ToList();
                                    }

                                    Task task = Task.Factory.StartNew(() =>
                                    {
                                        DownloadS3FileAndInsertIntoRedisForResortIdsMT(fileNamesToProcessInThisLoop, countrySite);
                                    });

                                    tasksCreatedForInsertingResortIds.Add(task);
                                }
                            }
                            else
                            {
                                response.ErrorMessage = "The response from the file is null.";
                            }

                        }
                        else
                        {
                            response.ErrorMessage = "Reading of the file keys successfully inserted into S3 failed.";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessage = "Exception occured in ProcessS3RecordsByResortIdsMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace;
                ErrorLogger.Log(response.ErrorMessage, LogLevel.RoutingApiError);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }

        /// <summary>
        /// This method downloads the ResortId specific data (Json) files (which has resort id, airport codes, dates etc mapping ) and updates/adds data into Redis
        /// </summary>
        /// <param name="resortIdsToBeProccessed"></param>
        /// <param name="countrySite"></param>
        private static void DownloadS3FileAndInsertIntoRedisForResortIdsMT(List<string> resortIdsToBeProccessed, string countrySite)
        {
            try
            {
                BasicAWSCredentials basicAwsCredentials =
                    new BasicAWSCredentials(ConfigurationManager.AppSettings["AwsAccessKeyIdForRoutingApi"],
                        ConfigurationManager.AppSettings["AwsSecretAccessKeyForRoutingApi"]);

                using (AmazonS3Client s3Client = new AmazonS3Client(basicAwsCredentials, RegionEndpoint.EUWest1))
                {
                    GetObjectRequest routingDataByResortIdObjectRequest = new GetObjectRequest
                    {
                        BucketName = RoutingConfigValueHelper.GetRoutingApiS3BucketName(countrySite)
                    };

                    using (
                        PooledRedisClientManager pooledRedisClientManager =
                            new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApiIE))
                    {
                        using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                        {
                            redisClient.Db = RoutingConfigValueHelper.GetRedisDbId(countrySite, "RedisDbForResortId");
                            foreach (string fileName in resortIdsToBeProccessed)
                            {
                                if (fileName.Trim().Equals(String.Empty))
                                {
                                    continue;
                                }

                                routingDataByResortIdObjectRequest.Key =
                                    Global.JsonFilesPathForRoutingDataByResortId + "/" + fileName;

                                RoutingDataByResortId routingDataByResortId = null;

                                try
                                {
                                    using (GetObjectResponse routingDataByAirportCodeObjectResponse =
                                        s3Client.GetObject(routingDataByResortIdObjectRequest))
                                    {
                                        if (routingDataByAirportCodeObjectResponse != null &&
                                            routingDataByAirportCodeObjectResponse.HttpStatusCode == HttpStatusCode.OK &&
                                            routingDataByAirportCodeObjectResponse.ContentLength > 0)
                                        {
                                            using (Stream stream = routingDataByAirportCodeObjectResponse.ResponseStream
                                            )
                                            {
                                                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8,
                                                    false);

                                                routingDataByResortId =
                                                    JsonConvert.DeserializeObject<RoutingDataByResortId>(
                                                        streamReader.ReadToEnd());

                                                if (routingDataByResortId != null)
                                                {
                                                    routingDataByResortId.dateTimeLastUpdatedIntoRedis =
                                                        DateTime.Now.ToString(
                                                            ConfigurationManager.AppSettings["LastModifiedDateFormat"]);
                                                }

                                                if (routingDataByResortId != null &&
                                                    routingDataByResortId.airportCodes != null &&
                                                    routingDataByResortId.airportCodes.Count > 0)
                                                {
                                                    foreach (string airportCode in routingDataByResortId.airportCodes)
                                                    {
                                                        List<int> departureIds = new List<int>();

                                                        Global.AirportCodeToMultipleDepartureIdsMappingMT.TryGetValue(
                                                            airportCode,
                                                            out departureIds);

                                                        if (departureIds != null)
                                                        {
                                                            routingDataByResortId.departureIds.AddRange(
                                                                departureIds.Except(routingDataByResortId.departureIds)
                                                                    .ToList());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {

                                }

                                if (routingDataByResortId != null && fileName.IndexOf('.') > -1)
                                {
                                    redisClient.Set(fileName.Split('.')[0], routingDataByResortId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("DownloadS3FileAndInsertIntoRedisForResortIdMT. Message: " + exception.Message + ". StackTrace: " + exception.StackTrace);
            }

        }

        #endregion End of Processing S3 Records By Resort Id


        #endregion
    }
}
