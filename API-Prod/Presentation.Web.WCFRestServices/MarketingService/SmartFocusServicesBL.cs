﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;

namespace Presentation.Web.WCFRestServices
{
    public class SmartFocusServicesBL
    {
        public string insertUpdateXML = "<?xml version='1.0' encoding='utf-8'?><synchroMember><memberUID>EMAIL:{0}</memberUID><dynContent><entry><key>FIRSTNAME</key><value>{1}</value></entry><entry><key>LASTNAME</key><value>{2}</value></entry><entry><key>EMAIL</key><value>{3}</value></entry><entry><key>SOURCE</key><value>Website_webform</value></entry><entry><key>EMAIL_OPT_IN</key><value>Yes</value></entry>{4}{5}</dynContent></synchroMember>";
        public string phoneNumberEntryXML = "<entry><key>EMVCELLPHONE</key><value>{0}</value></entry>";
        public string airportEntryXML = "<entry><key>AIRPORT_REGION_OF_INTEREST_1</key><value>{0}</value></entry>";

        private string Token { get; set; } 

        public void OpenConnection()
        {
            string token = string.Empty;
            string requestURL = string.Format(ConfigurationManager.AppSettings["SmartFocusOpenConnRestURL"].ToString(), 
                ConfigurationManager.AppSettings["SmartFocusDomainURL"].ToString(),
                ConfigurationManager.AppSettings["SmartFocusLogin"].ToString(),
                ConfigurationManager.AppSettings["SmartFocusPassword"].ToString(),
                ConfigurationManager.AppSettings["SmartFocusKey"].ToString());
            string result = Utilities.ExecuteGetWebRequest(requestURL);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            XmlNode resNode = doc.SelectSingleNode("response");
            XmlNode resResult = null;
            if (resNode.Attributes["responseStatus"] != null && resNode.Attributes["responseStatus"].Value == "success")
            {
                resResult = resNode.SelectSingleNode("result");               
            }
            token = (resResult == null) ? string.Empty : resResult.InnerText;
            Token = token;            
        }

        public Int64 InsertUpdateMemberSubscription(string firstName, string lastName, string email, string phoneNumber, string airport)
        {
            Int64 jobId = 0;
            insertUpdateXML=PreparetInsertUpdateMemberXML(firstName, lastName, email, phoneNumber, airport);
            string requestURL = string.Format(ConfigurationManager.AppSettings["SmartFocusInsertUpdateRestURL"].ToString(), ConfigurationManager.AppSettings["SmartFocusDomainURL"].ToString(),Token);
            string result = Utilities.ExecutePostWebRequest(requestURL, insertUpdateXML);
            XmlDocument docResponse = new XmlDocument();
            docResponse.LoadXml(result);
            XmlNode responseNode = docResponse.SelectSingleNode("response");
            XmlNode resID = null;
            if (responseNode.Attributes["responseStatus"] != null && responseNode.Attributes["responseStatus"].Value == "success")
            {
                resID = responseNode.SelectSingleNode("result");                
            }
            jobId = (resID != null) ? Convert.ToInt64(resID.InnerText) : 0;           
            return jobId;
        }

        public bool GetJobStatus(Int64 jobId)
        {
            bool jobStatus=false;           
            string requestURL = string.Format(ConfigurationManager.AppSettings["SmartFocusGetJobStatusRestURL"].ToString(), ConfigurationManager.AppSettings["SmartFocusDomainURL"].ToString(), Token, jobId);
            string result = Utilities.ExecuteGetWebRequest(requestURL);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            XmlNode resNode = doc.SelectSingleNode("response");
            XmlNode resResult = null;
            if (resNode.Attributes["responseStatus"] != null && resNode.Attributes["responseStatus"].Value == "success")
            {
                resResult = resNode.SelectSingleNode("processStatus");             
            }
            string statusResponse = (resResult == null) ? string.Empty : resResult.LastChild.InnerText;
            if (statusResponse.Equals("Insert"))
            {
                jobStatus = true;
            }          
            return jobStatus;
        }

        public string GetMemberByEmail(string email)        {   
            string requestURL = string.Format(ConfigurationManager.AppSettings["SmartFocusGetMemberByEmailRestURL"].ToString(), ConfigurationManager.AppSettings["SmartFocusDomainURL"].ToString(), Token, email);
            string result = Utilities.ExecuteGetWebRequest(requestURL);           
            return result;
        }

        public void CloseConnection()
        {
            string requestURL = string.Format(ConfigurationManager.AppSettings["SmartFocusCloseConnRestURL"].ToString(), ConfigurationManager.AppSettings["SmartFocusDomainURL"].ToString(), Token);
            string result = Utilities.ExecuteGetWebRequest(requestURL);
        }

        private string PreparetInsertUpdateMemberXML(string firstName, string lastName, string email, string phoneNumber, string airport)
        {
            string poneNumberXML="";
            string airportXML = "";

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                poneNumberXML = string.Format(phoneNumberEntryXML, phoneNumber);
            }

            if (!string.IsNullOrEmpty(airport))
            {
                airportXML = string.Format(airportEntryXML, airport);
            }

            return string.Format(insertUpdateXML, email, firstName, lastName, email, poneNumberXML, airportXML);
        }

    }
}