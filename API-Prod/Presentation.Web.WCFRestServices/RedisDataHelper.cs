﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ServiceStack.Redis;
using Presentation.WCFRestService.Model.Enum;
using System.Configuration;
using Presentation.WCFRestService.Model.Misc;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.ElasticSearchV2;

namespace Presentation.Web.WCFRestServices.RedisDataAccessHelper
{
    static class RedisDataHelper
    {
        public static string GetValueByKey(string key, string host, int port, long db, string password = null)
        {
            string result = string.Empty;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = System.Text.Encoding.UTF8.GetString(redisClient.Get(key));
                }
            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }
        public static string GetTAValueByKey(string key, string host, int port, long db, string password = null)
        {
            string result = string.Empty;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = System.Text.Encoding.UTF8.GetString(redisClient.Get(key));
                }
            }
            catch (System.ArgumentNullException ex)
            {
                //throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                //throw ex;
            }

            return result;
        }

        public static bool InsertValue(string key, string value, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Add(key, System.Text.Encoding.UTF8.GetBytes(value));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }

        public static bool UpdateValue(string key, string value, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Set(key, System.Text.Encoding.UTF8.GetBytes(value));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }

        public static bool DeleteValue(string key, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Remove(key);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }
        public static bool InsertTAValue(string key, string value, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Add(key, System.Text.Encoding.UTF8.GetBytes(value));
                    //redisClient.Expire(key,int.Parse(ConfigurationManager.AppSettings["ArtirixKeyExpireValue"]));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }
        public static bool UpdateTAValue(string key, string value, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Replace(key, System.Text.Encoding.UTF8.GetBytes(value));
                    //redisClient.Expire(key,int.Parse(ConfigurationManager.AppSettings["ArtirixKeyExpireValue"]));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }
        public static bool InsertMHIDInfo(string key, string value, string host, int port, long db, string password = null)
        {
            bool result = false;
            try
            {
                using (RedisClient redisClient = new RedisClient(host, port, password))
                //using (RedisClient redisClient = new RedisClient(host, port, password, db))
                {
                    redisClient.Db = (int)db;
                    result = redisClient.Add(key, System.Text.Encoding.UTF8.GetBytes(value));
                    //redisClient.Expire(key,int.Parse(ConfigurationManager.AppSettings["ArtirixKeyExpireValue"]));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }
            return result;
        }
        public static string GetRedisValueByKey(string key, long db)
        {
            string result = string.Empty;
            try
            {                
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.GetValue(key);
                        if (string.IsNullOrWhiteSpace(result))
                        {
                            result = string.Empty;
                        }
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }
        public static bool SetHotelInfoRedisValueByKey(string key, MHIDStaticInfo value, long db)
        {
            bool result = false;
            try
            {
               // ErrorLogger.Log("\nRedisConnectionString in RedisDataHelper.SetHotelInfoRedisValueByKey: " + Global.RedisConnectionStringForRoutingApi, LogLevel.Information);
               // ErrorLogger.Log("\nKey is: " + ". Db is: " + db, LogLevel.Information);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }
        public static bool SetTAInfoRedisValueByKey(string key, StaticResponseParms value, long db)
        {
            bool result = false;
            try
            {
                // ErrorLogger.Log("\nRedisConnectionString in RedisDataHelper.SetHotelInfoRedisValueByKey: " + Global.RedisConnectionStringForRoutingApi, LogLevel.Information);
                // ErrorLogger.Log("\nKey is: " + ". Db is: " + db, LogLevel.Information);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }
        public static bool SetTAReviews(string key , TAResponse value, string db)
        {
            bool result = false;
            try
            {
                //ErrorLogger.Log("\nRedisConnectionString in RedisDataHelper.SetHotelInfoRedisValueByKey: " + Global.RedisConnectionStringForRoutingApi, LogLevel.Information);
               // ErrorLogger.Log("\nKey is: " + ". Db is: " + db, LogLevel.Information);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }
        public static bool SetTAReviewsTime(string key, string value, string db)
        {
            bool result = false;
            try
            {
                //ErrorLogger.Log("\nRedisConnectionString in RedisDataHelper.SetHotelInfoRedisValueByKey: " + Global.RedisConnectionStringForRoutingApi, LogLevel.Information);
               // ErrorLogger.Log("\nKey is: " + ". Db is: " + db, LogLevel.Information);
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }        
        public static bool SetHotelDescriptionRedisValueByKey(string key, MHDescription value, long db)
        {
            bool result = false;
            try
            {
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisPasswordForRoutingApi + "@" + Global.RedisHostForRoutingApi + ":" +
                                                     Global.RedisPortForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }

        public static bool SetStaticHotelInfoByKey(string key, StaticResponseParms value, long db)
        {
            bool result = false;
            try
            {
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisPasswordForRoutingApi + "@" + Global.RedisHostForRoutingApi + ":" +
                                                     Global.RedisPortForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(db);
                        result = redisClient.Set(key, value);
                    }

                }

            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw ex;
            }

            return result;
        }

    }
}
