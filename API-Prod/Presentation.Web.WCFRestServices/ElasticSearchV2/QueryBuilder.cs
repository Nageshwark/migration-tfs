﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class QueryBuilder
    {
        public static string GetSearchURL(APISearchRequest apiSearchRequest)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("");

            //sb.Append("https://dataservice.teletextholidays.co.uk/GetJsonService.svc/ESHolidaySearch?");
            //IE CODE
            if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_IE)
            {
                sb.Append(Global.DataServicesUrlRoot + "ESHolidaySearch" + "/" + Global.COUNTRY_SITE_IE + "?");
            }
            else
            {
                //UK
                sb.Append(Global.DataServicesUrlRoot + "ESHolidaySearch?");
            }

            sb.Append("adults=" + apiSearchRequest.adults);
            sb.Append("&children=" + apiSearchRequest.children);
            sb.Append("&ratings=" + string.Join(",", apiSearchRequest.ratings));
            sb.Append("&departureDate=" + apiSearchRequest.departureDate.ToString("yyy-MM-dd"));
            sb.Append("&dateMin=" + apiSearchRequest.dateMin.ToString("yyy-MM-dd"));
            sb.Append("&dateMax=" + apiSearchRequest.dateMax.ToString("yyy-MM-dd"));
            sb.Append("&durationMin=" + apiSearchRequest.durationMin);
            sb.Append("&durationMax=" + apiSearchRequest.durationMax);
            if (!string.IsNullOrWhiteSpace(apiSearchRequest.destinationType) && apiSearchRequest.destinationType.ToLower() == "region")
            {
                sb.Append("&destinationType=Region");
                sb.Append("&destinationIds=" + apiSearchRequest.destinationIds);
                sb.Append("&labelId=" + apiSearchRequest.labelId);
            }
            else
            {
                //sb.Append("&destinationType=Label");
                sb.Append("&destinationIds=" + apiSearchRequest.destinationIds);
                //sb.Append("&labelId=" + apiSearchRequest.destinationIds);

            }
            sb.Append("&priceMin=" + apiSearchRequest.priceMin);
            if (apiSearchRequest.priceMin != 0)
            {
                sb.Append("&priceMax=" + apiSearchRequest.priceMax);
            }
            sb.Append("&departureIds=" + apiSearchRequest.departureIds);
            sb.Append("&channelId=" + apiSearchRequest.channelId);

            if (apiSearchRequest.sort != null && apiSearchRequest.sort.Count != 0)
            {
                sb.Append("&sort=" + string.Join(",", apiSearchRequest.sort));
            }
            if (apiSearchRequest.tripAdvisorRating != 0)
            {
                sb.Append("&tripadvisorrating=" + apiSearchRequest.tripAdvisorRating);
            }
            if (apiSearchRequest.hotelKeysToExclude.Count != 0)
            {
                sb.Append("&hotelKeysToExclude=" + string.Join(",", apiSearchRequest.hotelKeysToExclude));
            }
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                sb.Append("&hotelKeys=" + string.Join(",", apiSearchRequest.hotelKeys));
            }
            if (apiSearchRequest.usersPreferredHotelKeys.Count != 0)
            {
                sb.Append("&preferredHotelKeys=" + string.Join(",", apiSearchRequest.usersPreferredHotelKeys));
            }
            if (apiSearchRequest.tradingNameIds.Count == 1)
            {
                sb.Append("&tradingNameIds=" + string.Join(",", apiSearchRequest.tradingNameIds));
            }
            return sb.ToString();
        }
        public static string GetDiversityURL(APIDiversityRequest apiDiversityRequest)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            //sb.Append("https://dataservice.teletextholidays.co.uk/GetJsonService.svc/ESDiversity?");

            //IE CODE
            if (apiDiversityRequest.countrySite == Global.COUNTRY_SITE_IE)
            {
                sb.Append(Global.DataServicesUrlRoot + "ESDiversity" + "/" + Global.COUNTRY_SITE_IE + "?");
            }
            else
            {
                //UK
                sb.Append(Global.DataServicesUrlRoot + "ESDiversity?");
            }

            sb.Append("adults=" + apiDiversityRequest.adults);
            sb.Append("&children=" + apiDiversityRequest.children);
            sb.Append("&ratings=" + string.Join(",", apiDiversityRequest.ratings));
            sb.Append("&dateMin=" + apiDiversityRequest.dateMin);
            sb.Append("&dateMax=" + apiDiversityRequest.dateMax);
            sb.Append("&durationMin=" + apiDiversityRequest.durationMin);
            sb.Append("&durationMax=" + apiDiversityRequest.durationMax);
            sb.Append("&destinationId=" + apiDiversityRequest.destinationId);
            sb.Append("&departureIds=" + apiDiversityRequest.departureIds);
            if (apiDiversityRequest.hotelKeys.Count != 0)
            {
                sb.Append("&hotelKeys=" + string.Join(",", apiDiversityRequest.hotelKeys));
            }
            if (apiDiversityRequest.tradingNameIds.Count == 1)
            {
                sb.Append("&tradingNameIds=" + string.Join(",", apiDiversityRequest.tradingNameIds));
            }
            return sb.ToString();
        }
        public static string GetSearchQuery(APISearchRequest apiSearchRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            Dictionary<int, string> childAirportCodes = Global.childAirportCodes;

            //IE CODE
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                childAirportCodes = Global.childAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
            }

            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
            TimeSpan tSpan = new TimeSpan(0, 0, apiSearchRequest.cacheHotTime, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"adults\":{\"query\":\"" + apiSearchRequest.adults + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"children\":{\"query\":\"" + apiSearchRequest.children + "\",\"operator\":\"and\"}}},");
            if (apiSearchRequest.departureIds < 0)
            {
                searchParams.Append("{\"match\":{\"airportIds\":{\"query\":\"" + childAirportCodes[apiSearchRequest.departureIds].Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"match\":{\"airportIds\":{\"query\":\"" + apiSearchRequest.departureIds + "\",\"operator\":\"and\"}}},");
            }
            if (apiSearchRequest.destinationType == ConfigurationManager.AppSettings["DefaultDestinationType"])
            {
                searchParams.Append("{\"match\":{\"regionIds\":{\"query\":\"" + parentRegionIds[apiSearchRequest.destinationIds.ToString()].Replace(",", " ") + "\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"match\":{\"regionIds\":{\"query\":\"" + apiSearchRequest.destinationIds + "\",\"operator\":\"and\"}}},");
            }
            // If date range for query is small (<= 15 days)
            if (!apiSearchRequest.longDateRangeQuery)
            {
                searchParams.Append("{\"match\":{\"longDateRangeQuery\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            }
            // if date range for query is large (> 15 days)
            else
            {
                searchParams.Append("{\"range\":{\"availabilityTimeStamp\":{\"gte\":\"" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}},");
            }

            searchParams.Append("{\"match\":{\"tradingNameIds\":{\"query\":\"" + string.Join(" ", apiSearchRequest.tradingNameIds) + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"boardType\":{\"query\":\"" + string.Join(" ", apiSearchRequest.boardType) + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"dateMax\":{\"gte\":\"" + apiSearchRequest.dateMin.ToString("yyyy-MM-dd") + "T00:00:00\"}}},");
            searchParams.Append("{\"range\":{\"dateMin\":{\"lte\":\"" + apiSearchRequest.dateMax.ToString("yyyy-MM-dd") + "T23:59:59\"}}},");
            //searchParams.Append("{\"range\":{\"resetStage\":{\"lte\":\"3\"}}},");
            //searchParams.Append("{\"match\":{\"labelId\":{\"query\":\"" + apiSearchRequest.labelId + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"durationMax\":{\"query\":\"" + apiSearchRequest.durationMax + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"durationMax\":{\"query\":\"" + apiSearchRequest.durationMin + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"isCacheHit\":{\"query\":\"false\",\"operator\":\"and\"}}},{\"match\":{\"endPoint\":{\"query\":\"search\",\"operator\":\"and\"}}},{\"match\":{\"isRegion\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                //searchParams.Append("{\"match\":{\"isHotelKeys\":{\"query\":\"true\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"match\":{\"ratings\":{\"query\":\"" + string.Join(" ", apiSearchRequest.ratings) + "\",\"operator\":\"and\"}}},");
                searchParams.Append("{\"match\":{\"isHotelKeys\":{\"query\":\"false\",\"operator\":\"and\"}}},");
            }
            searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("\"sort\":[{\"_score\":{\"order\":\"asc\"}}]}");

            return searchParams.ToString();
        }
        public static string GetTTSSSearchURL(APISearchRequest apiSearchRequest)
        {
            StringBuilder searchParams = new StringBuilder();
            //searchParams.Append("http://mediatravel.api.test.balancer.ttss.net/dpSearch.php?searchId=2&userName=TTHOD&");

            //IE CODE
            string TTSSAPIendpointWebConfigKey = "TTSSAPIendpoint";
            if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_IE)
            {
                TTSSAPIendpointWebConfigKey += Global.COUNTRY_SITE_IE;
            }

            searchParams.Append(ConfigurationManager.AppSettings[TTSSAPIendpointWebConfigKey]);
            //searchParams.Append(ConfigurationManager.AppSettings["TTSSAPIendpoint"]);

            searchParams.Append("?searchId=2&userName=TTHOD&");
            if (ConfigurationManager.AppSettings["GetTTSSNewParams"].ToLower() == "true")
            {
                searchParams.Append("profile=MediatravelAPINew&");
            }
            if (apiSearchRequest.timeout != 0)
            {
                searchParams.Append("timeout=" + apiSearchRequest.timeout + "&");
            }
            if (apiSearchRequest.destinationIds != 0)
            {
                searchParams.Append("destinationId=" + apiSearchRequest.destinationIds + "&");
            }

            if (apiSearchRequest.departureIds != 0)
            {
                searchParams.Append("departureId=" + apiSearchRequest.departureIds + "&");
            }
            if (apiSearchRequest.boardType.Count != 0)
            {
                if (apiSearchRequest.boardType.Count == 6)
                {
                    searchParams.Append("boardType=-1&");
                }
                else
                {
                    searchParams.Append("boardType=");
                    searchParams.Append(string.Join(",", apiSearchRequest.boardType.ToArray()));
                    searchParams.Append("&");
                }

            }
            //if (apiSearchRequest.ratings.Count != 0)
            //{
            //    searchParams.Append("rating=");
            //    searchParams.Append(string.Join("", apiSearchRequest.ratings.ToArray()));
            //    searchParams.Append("&");
            //}
            if (apiSearchRequest.dateMin != null)
            {
                searchParams.Append("dateMin=" + apiSearchRequest.dateMin.ToString("yyyy-MM-dd") + "&");
            }
            if (apiSearchRequest.dateMax != null)
            {
                searchParams.Append("dateMax=" + apiSearchRequest.dateMax.ToString("yyyy-MM-dd") + "&");
            }

            if (apiSearchRequest.durationMin != 0)
            {
                searchParams.Append("durationMin=" + apiSearchRequest.durationMin + "&");
            }

            if (apiSearchRequest.durationMax != 0)
            {
                searchParams.Append("durationMax=" + apiSearchRequest.durationMax + "&");
            }

            if (apiSearchRequest.adults != 0)
            {
                searchParams.Append("adults=" + apiSearchRequest.adults + "&");
            }

            if (apiSearchRequest.children != 0)
            {
                searchParams.Append("children=" + apiSearchRequest.children + "&");
            }
            if (apiSearchRequest.priceMin != 0 && apiSearchRequest.priceMin < 80)
            {
                apiSearchRequest.priceMin = 80;
            }
            if (apiSearchRequest.tradingNameIds.Count != 0)
            {
                searchParams.Append("tradingNameId=");
                searchParams.Append(string.Join(",", apiSearchRequest.tradingNameIds.ToArray()));
                searchParams.Append("&");
            }
            else
            {
                //searchParams.Append("tradingNameId=192,576" + "&");
                //IE CODE
                string tradingIdWebConfigKey = "DefaultTradingNameIds";
                if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_IE)
                {
                    tradingIdWebConfigKey += apiSearchRequest.countrySite.ToUpper();
                }

                searchParams.Append("tradingNameId=" + ConfigurationManager.AppSettings[tradingIdWebConfigKey] + "&");

            }

            if (!string.IsNullOrWhiteSpace(apiSearchRequest.destinationType))
            {
                searchParams.Append("destinationType=" + apiSearchRequest.destinationType + "&");
            }
            else
            {
                searchParams.Append("destinationType=Label&");
            }

            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                searchParams.Append("hotelKey=");
                searchParams.Append(string.Join(",", apiSearchRequest.hotelKeys.ToArray()));
                searchParams.Append("&");
            }
            else
            {
                searchParams.Append("rating=");
                searchParams.Append(string.Join("", apiSearchRequest.ratings.ToArray()));
                searchParams.Append("&");
            }
            if (apiSearchRequest.sort != null && apiSearchRequest.sort.Count != 0 && apiSearchRequest.sort.Contains("rank"))
            {
                searchParams.Append("sortBy=rank&");
            }
            searchParams.Append("maxResults=" + ConfigurationManager.AppSettings["TTSSMaxSearchResults"] + "&");
            searchParams.Append("outputFormat=" + ConfigurationManager.AppSettings["TSSOutputFormat"]);
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }
        public static string GetFacetsQuery(APISearchRequest apiSearchRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            Dictionary<int, string> childAirportCodes = Global.childAirportCodes;

            //IE CODE
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                childAirportCodes = Global.childAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
            }

            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            if (apiSearchRequest.departureIds < 0)
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + childAirportCodes[apiSearchRequest.departureIds].Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + apiSearchRequest.departureIds + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + apiSearchRequest.adults + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + apiSearchRequest.children + "\",\"operator\":\"and\"}}},");
            if (!apiSearchRequest.boardType.Contains<string>("-1"))
            {
                searchParams.Append("{\"terms\":{\"accommodation.boardTypeId\":[\"" + string.Join("\",\"", apiSearchRequest.boardType) + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"journey.duration\":{\"query\":\"" + apiSearchRequest.durationMax + "\",\"operator\":\"and\"}}},");
            if (apiSearchRequest.destinationType == ConfigurationManager.AppSettings["DefaultDestinationType"])
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + parentRegionIds[apiSearchRequest.destinationIds.ToString()].Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + apiSearchRequest.destinationIds + "\"]}},");
            }
            searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"from\":\"" + apiSearchRequest.dateMin.ToString("yyyy-MM-dd") + "T00:00:00\",\"to\":\"" + apiSearchRequest.dateMax.ToString("yyyy-MM-dd") + "T23:59:59\"}}},");
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                searchParams.Append("{\"match\":{\"hotel.iff\":{\"query\":\"" + string.Join(" ", apiSearchRequest.hotelKeys) + "\",\"operator\":\"and\"}}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + string.Join("\",\"", apiSearchRequest.ratings) + "\"]}},");
            }
            string tradingNameIdOffers = string.Join(",", apiSearchRequest.tradingNameIds.Select(n => n.ToString()).ToArray());

            /* Replace logic for tradingNameIds like 808_ICE etc. */
            string pattern = "_[a-zA-Z]+";
            string replace = string.Empty;
            Regex rgx = new Regex(pattern);
            tradingNameIdOffers = rgx.Replace(tradingNameIdOffers, replace);

            if (!string.IsNullOrWhiteSpace(tradingNameIdOffers))
            {
                searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + tradingNameIdOffers.Replace(",", "\",\"") + "\"]}},");
            }
            searchParams.Append("{\"range\":{\"hotel.rating.averageRating\":{\"gte\":" + (double)apiSearchRequest.tripAdvisorRating + "}}},");
            //searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("{\"has_child\":{\"type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"query\":{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}}}]}},");
            searchParams.Append("\"size\":0,\"aggs\":{");
            searchParams.Append("\"price_min\":{\"min\":{\"field\":\"price\"}},");
            searchParams.Append("\"price_max\":{\"max\":{\"field\":\"price\"}},");
            searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":5000},\"aggs\":{\"NoOfHotels\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}},");
            searchParams.Append("\"airports\":{\"terms\":{\"field\":\"departureIds\",\"size\":5000},\"aggs\":{\"NoOfAirports\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}},");
            searchParams.Append("\"resorts\":{\"terms\":{\"field\":\"regionId\",\"size\":5000},\"aggs\":{\"NoOfResorts\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}}}}");
            string paramsString = searchParams.ToString();
            return paramsString;
        }
        public static string GetFacetsDiversityQuery(APIDiversityRequest apiDiversityRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            Dictionary<int, string> childAirportCodes = Global.childAirportCodes;

            //IE CODE
            if (apiDiversityRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                childAirportCodes = Global.childAirportCodesMT[apiDiversityRequest.countrySite.ToLower()];
            }

            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            if (apiDiversityRequest.departureIds < 0)
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + childAirportCodes[apiDiversityRequest.departureIds].Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + apiDiversityRequest.departureIds + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + apiDiversityRequest.adults + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + apiDiversityRequest.children + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"journey.duration\":{\"query\":\"" + apiDiversityRequest.durationMax + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"from\":\"" + apiDiversityRequest.dateMin + "T00:00:00\",\"to\":\"" + apiDiversityRequest.dateMax + "T23:59:59\"}}},");
            searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + string.Join("\",\"", apiDiversityRequest.ratings) + "\"]}},");
            //searchParams.Append("{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}},");
            searchParams.Append("{\"has_child\":{\"type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"query\":{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}}}]}},");
            searchParams.Append("\"size\":0,\"aggs\":{");
            searchParams.Append("\"price_min\":{\"min\":{\"field\":\"price\"}},");
            searchParams.Append("\"price_max\":{\"max\":{\"field\":\"price\"}},");
            searchParams.Append("\"hotel\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":5000},\"aggs\":{\"NoOfHotels\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}},");
            searchParams.Append("\"airports\":{\"terms\":{\"field\":\"departureIds\",\"size\":5000},\"aggs\":{\"NoOfAirports\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}},");
            searchParams.Append("\"resorts\":{\"terms\":{\"field\":\"regionId\",\"size\":5000},\"aggs\":{\"NoOfResorts\":{\"cardinality\":{\"field\":\"hotel.iff\"}}}}}}");
            string paramsString = searchParams.ToString();
            return paramsString;
        }
        public static string GetOffersQuery(APISearchRequest apiSearchRequest, int offersCount, string tradingNameIdOffers, List<int> hotelKeysToExclude, bool isUserPrefferential)
        {
            DateTime toDate = DateTime.UtcNow;
            Dictionary<int, string> childAirportCodes = Global.childAirportCodes;

            //IE CODE
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                childAirportCodes = Global.childAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
            }

            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);

            StringBuilder searchParams = new StringBuilder();

            searchParams.Append("");

            string sortParameter = string.Empty;
            string sort = string.Empty;
            if (apiSearchRequest.sort == null || apiSearchRequest.sort.Count == 0)
            {
                sort = "price";
                sortParameter = "asc";
            }
            else
            {
                sortParameter = "desc";
                if (apiSearchRequest.sort.Contains("pagination"))
                {
                    if (apiSearchRequest.sort.Contains("tripadvisorratingdesc"))
                    {
                        sort = "hotel.rating.averageRating";
                        sortParameter = "desc";
                    }
                    else if (apiSearchRequest.sort.Contains("ratingdesc"))
                    {
                        sort = "accommodation.rating";
                        sortParameter = "desc";
                    }
                    else if(apiSearchRequest.sort.Contains("rank"))
                    {
                        sort = "rank";
                        sortParameter = "asc";
                    }
                    else
                    {
                        sort = "price";
                        sortParameter = "asc";
                    }
                }
                else
                {
                    if (apiSearchRequest.sort.Contains("tripadvisorratingdesc"))
                    {
                        sort = "hotel.rating.averageRating";
                        sortParameter = "desc";
                    }
                    else if (apiSearchRequest.sort.Contains("rank"))
                    {
                        sort = "rank";
                        sortParameter = "asc";
                    }
                    else
                    {
                        sort = "accommodation.rating";
                        sortParameter = "desc";

                    }
                }
            }
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            if (apiSearchRequest.departureIds < 0)
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + childAirportCodes[apiSearchRequest.departureIds].Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"departureIds\":[\"" + apiSearchRequest.departureIds + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + apiSearchRequest.adults + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + apiSearchRequest.children + "\",\"operator\":\"and\"}}},");
            if (!apiSearchRequest.boardType.Contains<string>("-1"))
            {
                searchParams.Append("{\"terms\":{\"accommodation.boardTypeId\":[\"" + string.Join("\",\"", apiSearchRequest.boardType) + "\"]}},");
            }
            searchParams.Append("{\"match\":{\"journey.duration\":{\"query\":\"" + apiSearchRequest.durationMax + "\",\"operator\":\"and\"}}},");

            /* Replace logic for tradingNameIds like 808_ICE etc. */
            string pattern = "_[a-zA-Z]+";
            string replace = string.Empty;
            Regex rgx = new Regex(pattern);
            tradingNameIdOffers = rgx.Replace(tradingNameIdOffers, replace);

            if (!string.IsNullOrWhiteSpace(tradingNameIdOffers))
            {
                searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + tradingNameIdOffers.Replace(",", "\",\"") + "\"]}},");
            }
            if (apiSearchRequest.usersPreferredHotelKeys.Count != 0 && isUserPrefferential)
            {
                searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + string.Join("\",\"", apiSearchRequest.usersPreferredHotelKeys.Select(n => n.ToString()).ToArray()) + "\"]}},");
            }
            if (apiSearchRequest.destinationType == ConfigurationManager.AppSettings["DefaultDestinationType"])
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + parentRegionIds[apiSearchRequest.destinationIds.ToString()].Replace(",", "\",\"") + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"regionId\":[\"" + apiSearchRequest.destinationIds + "\"]}},");
            }
            searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"from\":\"" + apiSearchRequest.dateMin.ToString("yyyy-MM-dd") + "T00:00:00\",\"to\":\"" + apiSearchRequest.dateMax.ToString("yyyy-MM-dd") + "T23:59:59\"}}},");
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + string.Join("\",\"", apiSearchRequest.hotelKeys.Select(n => n.ToString()).ToArray()) + "\"]}},");
            }
            else
            {
                searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + string.Join("\",\"", apiSearchRequest.ratings) + "\"]}},");
            }
            if (apiSearchRequest.tripAdvisorRating != 0)
            {
                searchParams.Append("{\"range\":{\"hotel.rating.averageRating\":{\"gte\":" + (double)apiSearchRequest.tripAdvisorRating + "}}},");
            }
            if (apiSearchRequest.priceMax != 0)
            {
                searchParams.Append("{\"range\":{\"price\":{\"from\":" + apiSearchRequest.priceMin + ",\"to\":" + apiSearchRequest.priceMax + "}}},");
            }
            else
            {
                searchParams.Append("{\"range\":{\"price\":{\"from\":" + apiSearchRequest.priceMin + "}}},");
            }
            searchParams.Append("{\"has_child\":{\"type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"query\":{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}}}]");
            if (hotelKeysToExclude.Count != 0)
            {
                searchParams.Append(",\"must_not\":[{\"terms\":{\"hotel.iff\":[\"" + string.Join("\",\"", hotelKeysToExclude.Select(n => n.ToString()).ToArray()) + "\"]}}]}},");
            }
            else
            {
                searchParams.Append("}},");
            }
            searchParams.Append("\"size\": 0,");
            searchParams.Append("\"aggs\":{\"hotel_iff\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":" + offersCount + ",\"order\":{\"min_price\":\"" + sortParameter + "\"}},\"aggs\":{\"min_price\":{\"min\":{\"field\":\"" + sort + "\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}}}}}}}}");

            return searchParams.ToString();
        }
        public static string GetDiversityRequestQuery(APIDiversityRequest apiDiversityRequest)
        {
            DateTime toDate = DateTime.UtcNow;
            TimeSpan tSpan = new TimeSpan(0, 0, 30, 0);
            DateTime fromDate = toDate.Subtract(tSpan);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            searchParams.Append("{\"query\":{\"bool\":{\"must\":[");
            searchParams.Append("{\"match\":{\"destinationIds\":{\"query\":\"" + apiDiversityRequest.destinationId + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"adults\":{\"query\":\"" + apiDiversityRequest.adults + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"children\":{\"query\":\"" + apiDiversityRequest.children + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"departureIds\":{\"query\":\"" + apiDiversityRequest.departureIds + "\",\"operator\":\"and\"}}}");
            //searchParams.Append(",{\"match\":{\"hotelKeys\":{\"query\":\"" + string.Join(" ", apiDiversityRequest.hotelKeys) + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"hotelKeys\":{\"query\":\"" + string.Join(" ", apiDiversityRequest.hotelKeys) + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"terms\":{\"ratings\":[\"" + string.Join("\",\"", apiDiversityRequest.ratings) + "\"]}}");
            searchParams.Append(",{\"range\":{\"timeStamp\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}");
            searchParams.Append(",{\"range\":{\"dateMax\":{\"gte\":\"" + apiDiversityRequest.dateMin + "T00:00:00\"}}}");
            searchParams.Append(",{\"range\":{\"dateMin\":{\"lte\":\"" + apiDiversityRequest.dateMax + "T23:59:59\"}}}");
            searchParams.Append(",{\"match\":{\"isCacheHit\":{\"query\":\"false\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"endPoint\":{\"query\":\"diversity\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"terms\":{\"tradingNameIds\":[\"" + string.Join("\",\"", apiDiversityRequest.tradingNameIds) + "\"]}}");
            searchParams.Append(",{\"match\":{\"durationMin\":{\"query\":\"" + apiDiversityRequest.durationMin + "\",\"operator\":\"and\"}}}");
            searchParams.Append(",{\"match\":{\"durationMax\":{\"query\":\"" + apiDiversityRequest.durationMax + "\",\"operator\":\"and\"}}}");
            searchParams.Append("]}}}");
            return searchParams.ToString();
        }
        public static string GetDiversityQuery(APIDiversityRequest apiDiversityRequest)
        {
            StringBuilder searchParams = new StringBuilder();
            string diversityQuery = string.Empty;
            searchParams.Append("");
            searchParams.Append("{\"query\":{ \"bool\":{ \"must\":[");
            searchParams.Append("{\"match\":{\"accommodation.adults\":{\"query\":\"" + apiDiversityRequest.adults + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"match\":{\"labelId\":{\"query\":\"" + apiDiversityRequest.destinationId + "\",\"operator\":\"and\"}}},");
            if (apiDiversityRequest.departureIds < 0 && apiDiversityRequest.departureIds != 0)
            {
                searchParams.Append("{\"match\":{\"parentDepartureIds\":{\"query\":\"" + apiDiversityRequest.departureIds + "\",\"operator\":\"and\"}}},");
            }
            else if (apiDiversityRequest.departureIds > 0 && apiDiversityRequest.departureIds != 0)
            {
                searchParams.Append("{\"match\":{\"departureIds\":{\"query\":\"" + apiDiversityRequest.departureIds + "\",\"operator\":\"and\"}}},");
            }
            searchParams.Append("{\"range\":{\"journey.duration\":{\"gte\":" + apiDiversityRequest.durationMin + ",\"lte\":" + apiDiversityRequest.durationMax + "}}},");
            searchParams.Append("{\"terms\":{\"tradingNameId\":[\"" + string.Join("\",\"", apiDiversityRequest.tradingNameIds) + "\"]}},");

            searchParams.Append("{\"match\":{\"accommodation.children\":{\"query\":\"" + apiDiversityRequest.children + "\",\"operator\":\"and\"}}},");
            searchParams.Append("{\"terms\":{\"accommodation.rating\":[\"" + string.Join("\",\"", apiDiversityRequest.ratings) + "\"]}},");

            searchParams.Append("{\"terms\":{\"hotel.iff\":[\"" + string.Join("\",\"", apiDiversityRequest.hotelKeys) + "\"]}},");
            if (string.IsNullOrWhiteSpace(apiDiversityRequest.dateMax))
            {
                searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"from\":\"" + apiDiversityRequest.dateMin + "T00:00:00" + "\"}}}]}},");
            }
            else if (string.IsNullOrWhiteSpace(apiDiversityRequest.dateMin))
            {
                searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"to\":\"" + apiDiversityRequest.dateMax + "T23:55:55" + "\"}}}]}},");
            }
            else if (!string.IsNullOrWhiteSpace(apiDiversityRequest.dateMin) && !string.IsNullOrWhiteSpace(apiDiversityRequest.dateMax))
            {
                searchParams.Append("{\"range\":{\"journey.outboundDepartureDate\":{\"from\":\"" + apiDiversityRequest.dateMin + "T00:00:00" + "\",\"to\":\"" + apiDiversityRequest.dateMax + "T23:55:55" + "\"}}}]}},");
            }
            else
            {
                searchParams.Append("]}},");
            }
            searchParams.Append("\"size\": 0,");
            searchParams.Append("\"aggs\":{\"group_by_iff\":{\"terms\":{\"field\":\"hotel.iff\",\"size\":20},\"aggs\":{\"group_by_category\":{\"terms\":{\"field\":\"accommodation.boardTypeId\",\"order\":{\"min_price\":\"asc\"}},\"aggs\":{\"min_price\":{\"min\":{\"field\":\"price\"}},\"top_price\":{\"top_hits\":{\"size\":1,\"sort\":{\"price\":{\"order\":\"asc\"}},\"_source\":{\"includes\":[\"quoteRef\",\"price\",\"journey.outboundDepartureDate\"]}}}}}}}}}");
            return searchParams.ToString();
        }
        public static string GetTTSSDiversityURL(APIDiversityRequest apiDiversityRequest)
        {
            StringBuilder searchParams = new StringBuilder();
            //searchParams.Append("http://mediatravel.api.test.ttss.net/dpSearch.php?searchId=2&userName=TTHOD&");

            //IE CODE
            string TTSSAPIendpointWebConfigKey = "TTSSAPIendpoint";
            if (apiDiversityRequest.countrySite == Global.COUNTRY_SITE_IE)
            {
                TTSSAPIendpointWebConfigKey += Global.COUNTRY_SITE_IE;
            }

            searchParams.Append(ConfigurationManager.AppSettings[TTSSAPIendpointWebConfigKey]);
            //searchParams.Append(ConfigurationManager.AppSettings["TTSSAPIendpoint"]);

            searchParams.Append("?searchId=2&userName=TTHOD&");
            if (ConfigurationManager.AppSettings["GetTTSSNewParams"].ToLower() == "true")
            {
                searchParams.Append("profile=MediatravelAPINew&");
            }
            if (apiDiversityRequest.destinationId != 0)
            {
                searchParams.Append("destinationId=" + apiDiversityRequest.destinationId + "&");
            }

            if (apiDiversityRequest.departureIds != 0)
            {
                searchParams.Append("departureId=" + apiDiversityRequest.departureIds + "&");
            }

            //if (apiDiversityRequest.ratings.Count != 0)
            //{
            //    searchParams.Append("rating=");
            //    searchParams.Append(string.Join("", apiDiversityRequest.ratings.ToArray()));
            //    searchParams.Append("&");
            //}
            if (apiDiversityRequest.dateMin != null)
            {
                searchParams.Append("dateMin=" + apiDiversityRequest.dateMin + "&");
            }
            if (apiDiversityRequest.dateMax != null)
            {
                searchParams.Append("dateMax=" + apiDiversityRequest.dateMax + "&");
            }

            if (apiDiversityRequest.durationMin != 0)
            {
                searchParams.Append("durationMin=" + apiDiversityRequest.durationMin + "&");
            }

            if (apiDiversityRequest.durationMax != 0)
            {
                searchParams.Append("durationMax=" + apiDiversityRequest.durationMax + "&");
            }

            if (apiDiversityRequest.adults != 0)
            {
                searchParams.Append("adults=" + apiDiversityRequest.adults + "&");
            }

            if (apiDiversityRequest.children != 0)
            {
                searchParams.Append("children=" + apiDiversityRequest.children + "&");
            }
            if (apiDiversityRequest.tradingNameIds.Count != 0)
            {
                searchParams.Append("tradingNameId=");
                searchParams.Append(string.Join(",", apiDiversityRequest.tradingNameIds.ToArray()));
                searchParams.Append("&");
            }
            else
            {
                //IE CODE
                if (apiDiversityRequest.countrySite == Global.COUNTRY_SITE_IE)
                {
                    string tradingIdWebConfigKey = "DefaultTradingNameIds";
                    tradingIdWebConfigKey += apiDiversityRequest.countrySite.ToUpper();
                    searchParams.Append("tradingNameId=" + ConfigurationManager.AppSettings[tradingIdWebConfigKey] + "&");
                }
                else
                {
                    searchParams.Append("tradingNameId=192,576" + "&");

                }
            }
            searchParams.Append("destinationType=Label&");
            if (apiDiversityRequest.hotelKeys.Count != 0)
            {
                searchParams.Append("hotelKey=");
                searchParams.Append(string.Join(",", apiDiversityRequest.hotelKeys.ToArray()));
                searchParams.Append("&");
            }
            searchParams.Append("maxResults=" + ConfigurationManager.AppSettings["TTSSMaxSearchResults"] + "&");
            searchParams.Append("outputFormat=json");
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrWhiteSpace(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }
            return paramsString;
        }

        public static string GetUpdateDirectionalSellingChatMessageQuery(ChatMessage chatMessage)
        {
            ESMessageDoc esMessageDoc = new ESMessageDoc();
            esMessageDoc.chatId = chatMessage.sessionId;
            esMessageDoc.messages = new List<MessageObj>();

            MessageObj msg = new MessageObj();
            msg.sender = chatMessage.sender;
            msg.message = chatMessage.message;
            msg.timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");

            esMessageDoc.messages.Add(msg);

            StringBuilder sb = new StringBuilder();
            sb.Append("");

            string query = "{\"script\":{\"inline\":\"ctx._source.messages.add(params.messages[0])\",\"lang\":\"painless\",\"params\":{\"messages\":" + JsonConvert.SerializeObject(esMessageDoc.messages) + "}},\"upsert\":" + JsonConvert.SerializeObject(esMessageDoc) + "}";
            sb.Append(query);

            return sb.ToString();
        }
    }
}