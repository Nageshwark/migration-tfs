﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Web;
using static Presentation.WCFRestService.Model.ElasticSearchV2.SearchResponseParams;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class ExecuteQuery
    {
        public static SearchRequestAvailability CheckRequestInSearchIndice(APISearchRequest apiSearchRequest ,StringBuilder errorLogger)
        {
            SearchRequestAvailability searchRequestAvailabilty = new SearchRequestAvailability();
            string searchRequestQuery = QueryBuilder.GetSearchQuery(apiSearchRequest);
            //string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"] + "/_search", searchRequestQuery);

            //IE CODE
            string searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetRequestIndex(apiSearchRequest) + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"] + "/_search", searchRequestQuery);

            SearchRequestResponse searchRequestResponse = new SearchRequestResponse();
            try                                                                                                                                                                         
            {
                searchRequestResponse = JsonConvert.DeserializeObject<SearchRequestResponse>(searchResponse);
                if (searchRequestResponse.hits.total != 0)
                {
                    searchRequestAvailabilty.ttssCount = searchRequestResponse.hits.hits[0]._source.ttssCount;
                    searchRequestAvailabilty.resetStage = -1;
                    //searchRequestAvailabilty.resetStage = searchRequestResponse.hits.hits[0]._source.resetStage;
                }
            }
            catch (Exception ex)
            {
                errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace));
            }
            return searchRequestAvailabilty;
        }
        public static TTSSResponseStream ExecuteTTSSGetWebRequest(string webReqURL, StringBuilder errorLogger)
        {
            TTSSResponseStream ttssRes = new TTSSResponseStream();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(webReqURL);
            httpWebRequest.Proxy = null;
            httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["ttssTimeOut"]);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            httpWebRequest.Headers.Add("Accept-Encoding", "gzip,deflate");
            httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            try
            {
                System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
                timer.Start();
                using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    timer.Stop();
                    ttssRes.ttssResponseTime = timer.Elapsed.TotalMilliseconds;
                    using (Stream stream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                        ttssRes.ttssResponse = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                if (ConfigurationManager.AppSettings["EnableTTSSSearchLogs"] == "true")
                {
                    ErrorLogger.Log((string.Format("Web exception occured while making HTTP GET to TTSS. TimeStamp - {0}, Url - {1},  Message - {2}, Stack Trace - {3}\n", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss"), webReqURL, ex.Message, ex.StackTrace)), LogLevel.TTSSSearchAPICallException);
                }
                errorLogger.AppendLine((string.Format("Web exception occured while making HTTP to TTSS call. Url - {0},  Message - {1}, Stack Trace - {2}", webReqURL, ex.Message, ex.StackTrace)));
            }
            catch (Exception ex)
            {
                if (ConfigurationManager.AppSettings["EnableTTSSSearchLogs"] == "true")
                {
                    ErrorLogger.Log((string.Format("Web exception occured while making HTTP GET to TTSS. TimeStamp - {0}, Url - {1},  Message - {2}, Stack Trace - {3}\n", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss"), webReqURL, ex.Message, ex.StackTrace)), LogLevel.TTSSSearchAPICallException);
                }
                errorLogger.AppendLine((string.Format("Exception occured while making HTTP to TTSS call. Url - {0},  Message - {1}, Stack Trace - {2}", webReqURL, ex.Message, ex.StackTrace)));
            }
            return ttssRes;
        }

    }
}