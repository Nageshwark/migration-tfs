﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.Teleappliant
{
    public class TeleappliantLogging
    {
        public delegate LogTeleaappliantRequestResponstData LogTeleappliantData(LogTeleaappliantRequestResponstData logTeleaappliantRequestResponstData);

        public LogTeleaappliantRequestResponstData LogData(LogTeleaappliantRequestResponstData logTeleaappliantRequestResponstData)
        {
            List<MySqlParameter> procParams = new List<MySqlParameter>();
            MySqlParameter param;
            MySqlParameter paramResult;


            param = new MySqlParameter("@RequestTime", MySqlDbType.DateTime);
            param.Value = Convert.ToDateTime(logTeleaappliantRequestResponstData.RequestTime);
            procParams.Add(param);

            param = new MySqlParameter("@RequestSessionId", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = logTeleaappliantRequestResponstData.RequestSessionId;
            procParams.Add(param);

            param = new MySqlParameter("@ResponseSessionId", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = logTeleaappliantRequestResponstData.ResponseSessionId;
            procParams.Add(param);

            param = new MySqlParameter("@DeviceType", MySqlDbType.VarChar);
            param.Size = 20;
            param.Value = logTeleaappliantRequestResponstData.DeviceType;
            procParams.Add(param);

            param = new MySqlParameter("@IPAddress", MySqlDbType.VarChar);
            param.Size = 15;
            param.Value = logTeleaappliantRequestResponstData.IPAddress;
            procParams.Add(param);

            param = new MySqlParameter("@CountryCode", MySqlDbType.VarChar);
            param.Size = 2;
            param.Value = logTeleaappliantRequestResponstData.CountryCode;
            procParams.Add(param);

            param = new MySqlParameter("@RequestIngressNumber", MySqlDbType.Int32);
            param.Value = logTeleaappliantRequestResponstData.RequestIngressNumber;
            procParams.Add(param);

            param = new MySqlParameter("@RequestDestinationId", MySqlDbType.Int32);
            param.Value = logTeleaappliantRequestResponstData.RequestDestinationId;
            procParams.Add(param);

            param = new MySqlParameter("@ResponseIngressNumber", MySqlDbType.Int32);
            param.Value = logTeleaappliantRequestResponstData.ResponseIngressNumber;
            procParams.Add(param);

            param = new MySqlParameter("@ResponseDestinationId", MySqlDbType.Int32);
            param.Value = logTeleaappliantRequestResponstData.ResponseDestinationId;
            procParams.Add(param);

            param = new MySqlParameter("@ResponsePhoneNumber", MySqlDbType.VarChar);
            param.Size = 15;
            param.Value = logTeleaappliantRequestResponstData.ResponsePhoneNumber;
            procParams.Add(param);

            // @RESULT

            paramResult = new MySqlParameter("@LoggingIdOut", MySqlDbType.Int32);
            paramResult.Direction = ParameterDirection.Output;

            procParams.Add(paramResult);

            try
            {
                ExecuteCommand("iPro_sp_TeleappliantLogging", ConfigurationManager.ConnectionStrings["ttWeb"].ConnectionString, procParams);

                if (int.Parse(paramResult.Value.ToString()) > 0)
                {
                    logTeleaappliantRequestResponstData.Id = int.Parse(paramResult.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return logTeleaappliantRequestResponstData;
        }

        public static void ExecuteCommand(string procName, string ConnectionString, List<MySqlParameter> procParams = null)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);

            con.Open();

            MySqlCommand com = new MySqlCommand();

            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = procName;

            if (procParams != null && procParams.Count != 0)
            {
                foreach (MySqlParameter param in procParams)
                {
                    com.Parameters.Add(param);
                }
            }

            try
            {
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                con.Close();

                throw ex;
            }

            con.Close();
        }
    }

}