﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model
{
    public class RoutingData
    {
        public int ResortId { get; set; }
        public string AirportCode { get; set; }
        public List<RoutingMonth> Months { get; set; }
    }

    public class RoutingMonth
    {
        public string monthKey { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public List<RoutingDuration> Durations { get; set; }
    }

    public class RoutingDuration
    {
        public int Duration { get; set; }
        public List<int> Dates { get; set; }
    }

    public class RoutingResponse
    {
        public bool valid { get; set; }
        public int took { get; set; }
        public int total { get; set; }
        public List<int> destinationIds { get; set; }
        public List<int> departureIds { get; set; }
        public List<string> dates { get; set; }
        public List<int> durations { get; set; }

        public RoutingResponse()
        {
            this.valid = false;
            this.total = 0;
            this.destinationIds = new List<int>();
            this.departureIds = new List<int>();
            this.dates = new List<string>();
            this.durations = new List<int>();
        }
    }

}

