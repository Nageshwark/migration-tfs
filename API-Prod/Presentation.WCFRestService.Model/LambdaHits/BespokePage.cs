﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.LambdaHits
{
    public class BespokePage
    {
        public string pageName { get; set; }
        public List<string> tags { get; set; }
    }
}
