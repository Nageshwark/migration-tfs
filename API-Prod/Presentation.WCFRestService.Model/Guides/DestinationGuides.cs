﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Guides
{

    public class Destination
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool isNewGuidePage { get; set; }
        public string linkurl { get; set; }
        public string destcodes { get; set; }
        public string resortcode { get; set; }
        public string reglevel { get; set; }
        public string parent_regid { get; set; }
        public string parent_regname { get; set; }
        public string parent_link { get; set; }
        public string grand_regid { get; set; }
        public string grand_regname { get; set; }
        public string grand_reglink { get; set; }
    }

    public class DestinationGuides
    {
        public List<Destination> destinations { get; set; }
    }
}
