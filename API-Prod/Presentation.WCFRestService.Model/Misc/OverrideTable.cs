﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class OverrideTable
    {
        public int mhid { get; set; }
        public string hotelName { get; set; }
        public string destination { get; set; }
        public string destinationID { get; set; }
        public int rank  { get; set; }
        public int toDisplayOnFod { get; set; }
        public string comment { get; set; }
        public string Destinationcode { get; set; }
        public string LoadDate { get; set; }

    }
    public class HotelRankingsInfo
    {
        public int mhid { get; set; }
        public string hotelName { get; set; }
        public string destination { get; set; }
        public string destinationID { get; set; }
        public int rank { get; set; }
        public bool toDisplayOnFod { get; set; }
        public string comment { get; set; }
    }
    public class HotelRankingDataModel
    {
        public string Destinationcode { get; set; }
        public string DestinationName { get; set; }
        public int MasterHotelId { get; set; }
        public string HotelName { get; set; }
        public int StarRating { get; set; }
        public double TripAdvisorRating { get; set; }
        public int Bookings_Past365Days { get; set; }
        public int Bookings_Past30Days { get; set; }
        public double BookingsToRank { get; set; }
        public int RANK { get; set; }
        public string LoadDate { get; set; }
        public int DestinationID { get; set; }
        public int IsActive { get; set; }
        public string Comment { get; set; }
    }
}
