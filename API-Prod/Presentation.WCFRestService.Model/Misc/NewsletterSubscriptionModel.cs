﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class NewsletterSubscriptionModel
    {
        public string firstName { get; set; }
        public string surName { get; set; }
        public string emailAddress { get; set; }
        public bool optedPersonalisedDeals { get; set; }
        public string preferredAirport { get; set; }
        public string contactNumber { get; set; }
        public string boardBasis { get; set; }
        public string addressLineOne { get; set; }
        public string town { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string postCode { get; set; }
        public bool optForEmail { get; set; }
        public bool optForSMS { get; set; }
        public bool optForPost { get; set; }
        public string channel { get; set; }
        public string source { get; set; }
    }
}
