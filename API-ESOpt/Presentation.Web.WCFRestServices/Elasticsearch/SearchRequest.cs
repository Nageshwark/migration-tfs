﻿using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Artirix;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Presentation.Web.WCFRestServices
{
    public static class SearchRequest
    {
        public static SearchResponse getResponseId(string destinationIds, string departureDate, string dateMin, string dateMax, int durationMin, int durationMax,
                                                    int adults, int children, int priceMin, int priceMax, int departureIds, string boardType, string ratings,
                                                    string destinationType, string labelId, string hotelKeys, string tradingNameIds)
        {
            SearchResponse srcResponse = new SearchResponse();
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
            int parentDepartureIds = parentAirportCodes[departureIds];
            string esSearchQuery = string.Empty;
            esSearchQuery = Utilities.GetESSearchQuery(destinationIds, departureIds, departureDate, dateMin, dateMax, durationMin, durationMax, adults,
                                                            children, boardType, ratings, destinationType, labelId, hotelKeys, tradingNameIds);
            var searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"] + "/_search?pretty", esSearchQuery);
            ESSearchResponse esSearchResponse = new ESSearchResponse();
            ESSearchRequest esSearchrequest = new ESSearchRequest();
            try
            {
                esSearchResponse = JsonConvert.DeserializeObject<ESSearchResponse>(searchResponse);
                if (esSearchResponse.hits.total != 0)
                {
                    srcResponse.responseId = esSearchResponse.hits.hits[0]._source.responseId;
                    srcResponse.ttssCount = esSearchResponse.hits.hits[0]._source.ttssCount;
                }
            }
            catch (Exception ex)
            {

            }
            return srcResponse;
        }
        public static Holidays holidaysElasticsearchInclusivity(string destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                        string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin, int priceMax, string ratings,
                                        string tradingNameIds, string destinationType, string hotelKeys, int tripAdvisorRating, string hotelKeysToExclude,
                                        string sort, string channelId, string labelId, string usersPreferredHotelKeys, string skipFacets)
        {
            string imgSourceURLFormat = Global.imgSourceURLFormat;
            SearchResponse srcResponse = new SearchResponse();
            Holidays holidays = new Holidays();
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
            int parentDepartureIds = parentAirportCodes[departureIds];
            string esSearchQuery = string.Empty;
            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
            Dictionary<string, string> airportCodes = Global.airportCodes;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            string regionDestinationIds = destinationIds;
            if (string.IsNullOrWhiteSpace(destinationType))
            {
                regionDestinationIds = parentRegionIds[destinationIds];
            }
            esSearchQuery = Utilities.GetParentESSearchQuery(regionDestinationIds, parentDepartureIds, departureDate, dateMin, dateMax, durationMin, durationMax, adults, children, boardType, ratings, destinationType, labelId, hotelKeys);
            var searchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"] + "/_search?pretty", esSearchQuery);
            ESSearchResponse esSearchResponse = null;
            ESSearchRequest esSearchrequest = new ESSearchRequest();
            try
            {
                esSearchResponse = JsonConvert.DeserializeObject<ESSearchResponse>(searchResponse);
                if (esSearchResponse.hits.total != 0)
                {
                    srcResponse.responseId = esSearchResponse.hits.hits[0]._source.responseId;
                    srcResponse.ttssCount = esSearchResponse.hits.hits[0]._source.ttssCount;
                    Journey journey;
                    Accommodation accommodation;
                    OfferHotel offerHotel;
                    WCFRestService.Model.Artirix.Rating taRating;
                    int offerCount = 0;
                    if (!string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        offerCount = hotelKeys.Split(',').Length;
                    }
                    else
                    {
                        offerCount = int.Parse(ConfigurationManager.AppSettings["OfferCount"]);
                    }
                    bool UserPref = false;
                    bool trading576 = false;
                    string usersPreferredHotelKeysQuery = string.Empty;
                    string ESUPHKResponse = string.Empty;
                    ElasticsearchResponse esUPHKResponse = new ElasticsearchResponse();
                    string hkte = string.Empty;
                    string hkteUPHK = string.Empty;
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        var taNameIds = string.Empty;
                        if (!string.IsNullOrWhiteSpace(tradingNameIds))
                        {
                            taNameIds = tradingNameIds;
                        }
                        else
                        {
                            taNameIds = "576,192";
                        }
                        usersPreferredHotelKeysQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                    departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                    labelId, tripAdvisorRating, usersPreferredHotelKeys, taNameIds, 1);
                        ESUPHKResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", usersPreferredHotelKeysQuery);
                        esUPHKResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESUPHKResponse);
                        if (esUPHKResponse.hits.total != 0)
                        {
                            offerCount = offerCount - 1;
                            UserPref = true;
                            hkteUPHK = esUPHKResponse.aggregations.hotel_iff.buckets[0].top_price.hits.hits[0]._source.hotel.iff;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                    {
                        hkte = hkteUPHK + "," + hotelKeysToExclude;
                    }
                    else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                    {
                        hkte = hotelKeysToExclude;
                    }
                    else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        hkte = hkteUPHK;
                    }
                    string esQuery576 = string.Empty;
                    string ESTNID576Response = string.Empty;
                    ElasticsearchResponse esTNID576Response = new ElasticsearchResponse();
                    int tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);
                    string esQuery = string.Empty;
                    if (string.IsNullOrWhiteSpace(sort) && tradingNameIds != "808" && string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                        offerCount = offerCount - esTNID576Response.aggregations.hotel_iff.buckets.Count;
                        if (string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                     select ttssResult.key).Distinct().ToList());
                        }
                        else if (!string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = hkte + "," + String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                                  select ttssResult.key).Distinct().ToList());
                        }
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, "", "192", offerCount);
                        trading576 = true;
                    }
                    else
                    {
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, "", tradingNameIds, offerCount);
                    }
                    string esFacetsQuery = Utilities.GetESFacetsQuery(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                                        departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys,
                                                                        destinationType, labelId, tripAdvisorRating, usersPreferredHotelKeys);

                    var ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                    FacetsResponse esFacetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                    var ESResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery);
                    ElasticsearchResponse esSerResponse = null;
                    esSerResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESResponse);
                    offerCount = offerCount - esSerResponse.aggregations.hotel_iff.buckets.Count;
                    if (offerCount > 0 && trading576)
                    {
                        hkte = string.Empty;
                        if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        {
                            hkte = hkteUPHK + "," + hotelKeysToExclude;
                        }
                        else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        {
                            hkte = hotelKeysToExclude;
                        }
                        else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                        {
                            hkte = hkteUPHK;
                        }

                        //if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        //{
                        //    hkte = hkteUPHK + "," + hotelKeysToExclude;
                        //}
                        //else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        //{
                        //    hkte = hotelKeysToExclude;
                        //}

                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", offerCount + tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                    }
                    if (srcResponse.ttssCount < int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"]) ||
                        (srcResponse.ttssCount == int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"]) && esFacetsResponse.hits.total > int.Parse(ConfigurationManager.AppSettings["ThresholdOfferLimit"])))
                    {
                        holidays.total = esFacetsResponse.hits.total;
                        holidays.totalHotels = esFacetsResponse.aggregations.hotel.buckets.Length;
                        holidays.offers = new List<Offer>();
                        ESRSource source = new ESRSource();
                        if (UserPref == true)
                        {
                            foreach (ESRBucket bucket in esUPHKResponse.aggregations.hotel_iff.buckets)
                            {
                                Offer offr = new Offer();
                                try
                                {
                                    offr.id = bucket.top_price.hits.hits[0]._source.id;
                                    offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                    offr.price = bucket.top_price.hits.hits[0]._source.price;
                                    offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                    offr.isPreferredByUser = true;
                                    var phoneNumber = string.Empty;
                                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    }
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = "0123456789";
                                    }
                                    offr.phone = phoneNumber;
                                    //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                    journey = new Journey();
                                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                    journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                    journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                    journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                    offr.journey = journey;
                                    accommodation = new Accommodation();
                                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                    offr.accommodation = accommodation;
                                    offerHotel = new OfferHotel();
                                    offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                    offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                    offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                    //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                    offerHotel.features = new List<object>();
                                    offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                    offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                    offerHotel.mobileimages = new List<string>();
                                    offerHotel.thumbnailimages = new List<string>();
                                    offerHotel.images = new List<string>();
                                    int hoteliffCount = 0;
                                    if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                    {
                                        hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                        {
                                            offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        }
                                    }
                                    else
                                    {
                                        offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                    }
                                    taRating = new WCFRestService.Model.Artirix.Rating();
                                    taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                    taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                    taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                    offerHotel.rating = taRating;
                                    offr.hotel = offerHotel;
                                    offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                    offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                    offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                }
                                catch (Exception ex)
                                {

                                }
                                holidays.offers.Add(offr);
                            }
                        }
                        if (string.IsNullOrWhiteSpace(sort) && trading576 == true)
                        {
                            foreach (ESRBucket bucket in esTNID576Response.aggregations.hotel_iff.buckets)
                            {
                                Offer offr = new Offer();
                                try
                                {
                                    offr.id = bucket.top_price.hits.hits[0]._source.id;
                                    offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                    offr.price = bucket.top_price.hits.hits[0]._source.price;
                                    var phoneNumber = string.Empty;
                                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    }
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = "0123456789";
                                    }
                                    offr.phone = phoneNumber;
                                    journey = new Journey();
                                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                    journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                    journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                    journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                    offr.journey = journey;
                                    accommodation = new Accommodation();
                                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                    offr.accommodation = accommodation;
                                    offerHotel = new OfferHotel();
                                    offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                    offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                    offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                    //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                    offerHotel.features = new List<object>();
                                    offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                    offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                    offerHotel.mobileimages = new List<string>();
                                    offerHotel.thumbnailimages = new List<string>();
                                    offerHotel.images = new List<string>();
                                    int hoteliffCount = 0;
                                    if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                    {
                                        hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                        {
                                            offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        }
                                    }
                                    else
                                    {
                                        offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                    }
                                    taRating = new WCFRestService.Model.Artirix.Rating();
                                    taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                    taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                    taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                    offerHotel.rating = taRating;
                                    offr.hotel = offerHotel;
                                    offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                    offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                    offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                    offr.isPreferredByUser = true;
                                    offr.isPreferential = true;
                                }
                                catch (Exception ex)
                                {

                                }
                                holidays.offers.Add(offr);
                            }
                        }
                        foreach (ESRBucket bucket in esSerResponse.aggregations.hotel_iff.buckets)
                        {
                            Offer offr = new Offer();
                            try
                            {
                                offr.id = bucket.top_price.hits.hits[0]._source.id;
                                offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                offr.price = bucket.top_price.hits.hits[0]._source.price;
                                offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                var phoneNumber = string.Empty;
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                }
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = "0123456789";
                                }
                                offr.phone = phoneNumber;
                                //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                journey = new Journey();
                                journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                offr.journey = journey;
                                accommodation = new Accommodation();
                                accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                offr.accommodation = accommodation;
                                offerHotel = new OfferHotel();
                                offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                offerHotel.features = new List<object>();
                                offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                offerHotel.mobileimages = new List<string>();
                                offerHotel.thumbnailimages = new List<string>();
                                offerHotel.images = new List<string>();
                                int hoteliffCount = 0;
                                if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                {
                                    hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                    for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                    {
                                        offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                    }
                                }
                                else
                                {
                                    offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                }
                                taRating = new WCFRestService.Model.Artirix.Rating();
                                taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                offerHotel.rating = taRating;
                                offr.hotel = offerHotel;
                                offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            holidays.offers.Add(offr);
                        }
                        holidays.facets = new Facets();
                        holidays.facets.priceRange = new Pricerange();
                        holidays.facets.priceRange.max = Convert.ToInt32(esFacetsResponse.aggregations.price_max.value);
                        holidays.facets.priceRange.min = Convert.ToInt32(esFacetsResponse.aggregations.price_min.value);
                        holidays.facets.airports = new List<WCFRestService.Model.Artirix.Airport>();
                        foreach (FacetsAirportsBucket airportBucket in esFacetsResponse.aggregations.airports.buckets)
                        {
                            try
                            {
                                WCFRestService.Model.Artirix.Airport airport = new WCFRestService.Model.Artirix.Airport();
                                airport.count = airportBucket.doc_count;
                                airport.departureId = airportBucket.key;
                                airport.name = airportCodes[airportBucket.key.ToString()];
                                holidays.facets.airports.Add(airport);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        holidays.facets.hotels = new List<FacetHotel>();
                        foreach (FacetsHotelBucket hotelBucket in esFacetsResponse.aggregations.hotel.buckets)
                        {
                            FacetHotel facetHotel = new FacetHotel();
                            facetHotel.count = hotelBucket.doc_count;
                            facetHotel.hotelKey = hotelBucket.key.ToString();
                            if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                            {
                                facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                                if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                                {
                                    facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];
                                }

                                holidays.facets.hotels.Add(facetHotel);
                            }

                        }
                        holidays.facets.resorts = new List<Resort>();
                        foreach (FacetsResortsBucket resortBucket in esFacetsResponse.aggregations.resorts.buckets)
                        {
                            Resort resort = new Resort();
                            resort.count = resortBucket.NoOfHotels.value;
                            resort.destinationId = Convert.ToInt32(resortBucket.key);
                            if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                            {
                                resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                                holidays.facets.resorts.Add(resort);
                            }
                        }
                        holidays.facets.totalHotels = esFacetsResponse.hits.total;
                        Task.Factory.StartNew(() =>
                        {
                            ESSearchRequest esSearchrequestInc = new ESSearchRequest();
                            esSearchrequestInc.adults = adults;
                            esSearchrequestInc.children = children;
                            esSearchrequestInc.dateMax = dateMax + "T23:59:59";
                            esSearchrequestInc.dateMin = dateMin + "T00:00:00";
                            esSearchrequestInc.departureIds = departureIds;
                            if (string.IsNullOrWhiteSpace(destinationType))
                            {
                                esSearchrequestInc.destinationIds = destinationIds;
                            }
                            else
                            {
                                esSearchrequestInc.destinationIds = labelId;
                            }
                            esSearchrequestInc.durationMin = durationMin;
                            esSearchrequestInc.durationMax = durationMax;
                            esSearchrequestInc.GUID = skipFacets;
                            esSearchrequestInc.parentDepartureIds = parentDepartureIds;
                            esSearchrequestInc.responseId = srcResponse.responseId;
                            esSearchrequestInc.regionIds = regionDestinationIds.Split(',').ToList();
                            esSearchrequestInc.boardType = boardType.Split(',').ToList();
                            esSearchrequestInc.ratings = ratings.Split(',').ToList();
                            esSearchrequestInc.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                            //esSearchrequest.ttssCount = serResponse.ttssCount;
                            //esSearchrequest.ttssResponseTime = ttssRes.ttssResponseTime;
                            esSearchrequestInc.isCahceHit = true;
                            if (!string.IsNullOrWhiteSpace(hotelKeys))
                            {
                                esSearchrequestInc.hotelKeys = hotelKeys;
                                esSearchrequestInc.isHotelKeys = "true";
                            }
                            else
                            {
                                //esSearchrequest.hotelKeys = null;
                                esSearchrequestInc.isHotelKeys = "false";
                            }
                            if (string.IsNullOrWhiteSpace(tradingNameIds))
                            {
                                var taIds = "576,192";
                                esSearchrequestInc.tradingNameId = taIds.Split(',').ToList();
                            }
                            else
                            {
                                esSearchrequestInc.tradingNameId = tradingNameIds.Split(',').ToList();
                            }
                            var httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"] + "/_search?pretty");
                            httpWebSearchRequest.ContentType = "application/json";
                            httpWebSearchRequest.Method = "POST";
                            httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                            using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                            {
                                var test2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(esSearchrequestInc);
                                streamWriter.Write(test2);
                                streamWriter.Flush();
                                streamWriter.Close();
                            }
                            var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                            string esSearchInsertionResult = string.Empty;
                            ESNewSearchInsertion esSearchInsertionResponse = null;
                            using (var streamReader = new StreamReader(httpSearchResponse.GetResponseStream()))
                            {
                                esSearchInsertionResult = streamReader.ReadToEnd();
                                try
                                {
                                    esSearchInsertionResponse = JsonConvert.DeserializeObject<ESNewSearchInsertion>(esSearchInsertionResult);
                                    if (esSearchInsertionResponse.created.Equals(true))
                                    {
                                        //responseId = esSearchrequest.timeStamp;
                                        //srcResponse.responseId = esSearchInsertionResponse._id;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        });
                    }
                    else
                    {
                        int offersInserted = TTSSandBulkInsertionV_2(destinationIds, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, priceMin,
                                                          priceMax, tradingNameIds, destinationType, hotelKeys, tripAdvisorRating, ratings, boardType, departureDate, labelId, skipFacets);
                        esFacetsQuery = Utilities.GetESFacetsQuery(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                                        departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys,
                                                                        destinationType, labelId, tripAdvisorRating, usersPreferredHotelKeys);

                        ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                        FacetsResponse facResp = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                        while (facetsTrials > 0)
                        {
                            if (facResp.aggregations.price_max.value != 0 && facResp.aggregations.price_min.value != 0 && facResp.hits.total >= offersInserted)
                            {
                                ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                                facResp = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                                break;
                            }
                            else
                            {
                                ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                                facResp = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                                Thread.Sleep(500);
                                facetsTrials--;
                            }
                        }
                        //string esQuery = esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, duration, destinationIds, departureDate,
                        // hotelKeysToExclude, sortParameter, priceMin, priceMax, dateMin, dateMax);
                        offerCount = 0;
                        if (!string.IsNullOrWhiteSpace(hotelKeys))
                        {
                            offerCount = hotelKeys.Split(',').Length;
                        }
                        else
                        {
                            offerCount = int.Parse(ConfigurationManager.AppSettings["OfferCount"]);
                        }
                        facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
                        ESUPHKResponse = string.Empty;
                        //ElasticsearchResponse esUPHKResponse = new ElasticsearchResponse();
                        if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                        {
                            ESUPHKResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", usersPreferredHotelKeysQuery);
                            esUPHKResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESUPHKResponse);
                            if (esUPHKResponse.hits.total != 0)
                            {
                                offerCount = offerCount - esUPHKResponse.aggregations.hotel_iff.buckets.Count;
                                UserPref = true;
                                hkteUPHK = esUPHKResponse.aggregations.hotel_iff.buckets[0].top_price.hits.hits[0]._source.hotel.iff;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        {
                            hkte = hkteUPHK + "," + hotelKeysToExclude;
                        }
                        else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        {
                            hkte = hotelKeysToExclude;
                        }
                        else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                        {
                            hkte = hkteUPHK;
                        }
                        ESTNID576Response = string.Empty;
                        tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);
                        if (string.IsNullOrWhiteSpace(sort) && tradingNameIds != "808" && string.IsNullOrWhiteSpace(hotelKeys))
                        {
                            esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                            labelId, tripAdvisorRating, "", "576", tradingNameId576Count);
                            ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                            esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                            offerCount = offerCount - esTNID576Response.aggregations.hotel_iff.buckets.Count;
                            if (string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                            {
                                hkte = String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                         select ttssResult.key).Distinct().ToList());
                            }
                            else if (!string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                            {
                                hkte = hkte + "," + String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                                      select ttssResult.key).Distinct().ToList());
                            }
                            esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", "192", offerCount);
                            trading576 = true;
                        }
                        else
                        {
                            esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", tradingNameIds, offerCount);
                        }

                        ESResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery);
                        ElasticsearchResponse esIncSerResponse = null;
                        esIncSerResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESResponse);
                        offerCount = offerCount - esIncSerResponse.aggregations.hotel_iff.buckets.Count;
                        if (offerCount > 0 && trading576)
                        {
                            hkte = string.Empty;
                            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                            {
                                hkte = hkteUPHK + "," + hotelKeysToExclude;
                            }
                            else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                            {
                                hkte = hotelKeysToExclude;
                            }
                            else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                            {
                                hkte = hkteUPHK;
                            }

                            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                            {
                                hkte = hkteUPHK + "," + hotelKeysToExclude;
                            }
                            else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                            {
                                hkte = hotelKeysToExclude;
                            }
                            else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                            {
                                hkte = hkteUPHK;
                            }
                            esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                            labelId, tripAdvisorRating, "", "576", offerCount + tradingNameId576Count);
                            ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                            esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                        }
                        try
                        {
                            if (facResp.hits.total != 0)
                            {

                                holidays.total = facResp.hits.total;
                                holidays.totalHotels = facResp.aggregations.hotel.buckets.Length;
                                holidays.offers = new List<Offer>();
                                if (UserPref == true)
                                {
                                    foreach (ESRBucket bucket in esUPHKResponse.aggregations.hotel_iff.buckets)
                                    {
                                        Offer offr = new Offer();
                                        try
                                        {
                                            offr.id = bucket.top_price.hits.hits[0]._source.id;
                                            offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                            offr.price = bucket.top_price.hits.hits[0]._source.price;
                                            offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                            offr.isPreferredByUser = true;
                                            var phoneNumber = string.Empty;
                                            phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                            if (string.IsNullOrWhiteSpace(phoneNumber))
                                            {
                                                phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                            }
                                            if (string.IsNullOrWhiteSpace(phoneNumber))
                                            {
                                                phoneNumber = "0123456789";
                                            }
                                            offr.phone = phoneNumber;
                                            //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                            journey = new Journey();
                                            journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                            journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                            journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                            journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                            journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                            journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                            journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                            journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                            journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                            journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                            journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                            journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                            journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                            journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                            journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                            journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                            offr.journey = journey;
                                            accommodation = new Accommodation();
                                            accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                            accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                            accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                            accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                            accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                            offr.accommodation = accommodation;
                                            offerHotel = new OfferHotel();
                                            offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                            offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                            offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                            //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                            offerHotel.features = new List<object>();
                                            offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                            offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                            offerHotel.mobileimages = new List<string>();
                                            offerHotel.thumbnailimages = new List<string>();
                                            offerHotel.images = new List<string>();
                                            int hoteliffCount = 0;
                                            if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                            {
                                                hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                                {
                                                    offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                    offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                    offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                }
                                            }
                                            else
                                            {
                                                offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                            }
                                            taRating = new WCFRestService.Model.Artirix.Rating();
                                            taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                            taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                            taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                            offerHotel.rating = taRating;
                                            offr.hotel = offerHotel;
                                            offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                            offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                            offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        holidays.offers.Add(offr);
                                    }
                                }
                                if (string.IsNullOrWhiteSpace(sort) && trading576 == true)
                                {
                                    foreach (ESRBucket bucket in esTNID576Response.aggregations.hotel_iff.buckets)
                                    {
                                        Offer offr = new Offer();
                                        try
                                        {
                                            offr.id = bucket.top_price.hits.hits[0]._source.id;
                                            offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                            offr.price = bucket.top_price.hits.hits[0]._source.price;
                                            var phoneNumber = string.Empty;
                                            phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                            if (string.IsNullOrWhiteSpace(phoneNumber))
                                            {
                                                phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                            }
                                            if (string.IsNullOrWhiteSpace(phoneNumber))
                                            {
                                                phoneNumber = "0123456789";
                                            }
                                            offr.phone = phoneNumber;
                                            //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                            journey = new Journey();
                                            journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                            journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                            journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                            journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                            journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                            journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                            journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                            journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                            journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                            journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                            journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                            journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                            journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                            journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                            journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                            journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                            offr.journey = journey;
                                            accommodation = new Accommodation();
                                            accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                            accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                            accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                            accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                            accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                            offr.accommodation = accommodation;
                                            offerHotel = new OfferHotel();
                                            offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                            offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                            offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                            //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                            offerHotel.features = new List<object>();
                                            offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                            offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                            offerHotel.mobileimages = new List<string>();
                                            offerHotel.thumbnailimages = new List<string>();
                                            offerHotel.images = new List<string>();
                                            int hoteliffCount = 0;
                                            if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                            {
                                                hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                                {
                                                    offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                    offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                    offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                }
                                            }
                                            else
                                            {
                                                offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                            }
                                            taRating = new WCFRestService.Model.Artirix.Rating();
                                            taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                            taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                            taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                            offerHotel.rating = taRating;
                                            offr.hotel = offerHotel;
                                            offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                            offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                            offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                            offr.isPreferredByUser = true;
                                            offr.isPreferential = true;
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        holidays.offers.Add(offr);
                                    }
                                }
                                foreach (ESRBucket bucket in esIncSerResponse.aggregations.hotel_iff.buckets)
                                {
                                    Offer offr = new Offer();
                                    try
                                    {
                                        offr.id = bucket.top_price.hits.hits[0]._source.id;
                                        offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                        offr.price = bucket.top_price.hits.hits[0]._source.price;
                                        offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                        //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                        var phoneNumber = string.Empty;
                                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        }
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = "0123456789";
                                        }
                                        offr.phone = phoneNumber;
                                        //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        journey = new Journey();
                                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                        journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                        journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                        journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                        offr.journey = journey;
                                        accommodation = new Accommodation();
                                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                        offr.accommodation = accommodation;
                                        offerHotel = new OfferHotel();
                                        offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                        offerHotel.mobileimages = new List<string>();
                                        offerHotel.thumbnailimages = new List<string>();
                                        offerHotel.images = new List<string>();
                                        int hoteliffCount = 0;
                                        if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                        {
                                            hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                            {
                                                offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            }
                                        }
                                        else
                                        {
                                            offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                        }
                                        taRating = new WCFRestService.Model.Artirix.Rating();
                                        taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                        taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                        taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                        offerHotel.rating = taRating;
                                        //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                        offerHotel.features = new List<object>();
                                        offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                        offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                        offr.hotel = offerHotel;
                                        offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                        offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                        offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    holidays.offers.Add(offr);
                                }
                                holidays.facets = new Facets();
                                holidays.facets.priceRange = new Pricerange();
                                holidays.facets.priceRange.max = Convert.ToInt32(facResp.aggregations.price_max.value);
                                holidays.facets.priceRange.min = Convert.ToInt32(facResp.aggregations.price_min.value);
                                holidays.facets.airports = new List<WCFRestService.Model.Artirix.Airport>();
                                foreach (FacetsAirportsBucket airportBucket in facResp.aggregations.airports.buckets)
                                {
                                    try
                                    {
                                        WCFRestService.Model.Artirix.Airport airport = new WCFRestService.Model.Artirix.Airport();
                                        airport.count = airportBucket.doc_count;
                                        airport.departureId = airportBucket.key;
                                        airport.name = airportCodes[airportBucket.key.ToString()];
                                        holidays.facets.airports.Add(airport);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                holidays.facets.hotels = new List<FacetHotel>();
                                foreach (FacetsHotelBucket hotelBucket in facResp.aggregations.hotel.buckets)
                                {
                                    FacetHotel facetHotel = new FacetHotel();
                                    facetHotel.count = hotelBucket.doc_count;
                                    facetHotel.hotelKey = hotelBucket.key.ToString();
                                    if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                                    {
                                        facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                                        if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                                        {
                                            facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];
                                        }
                                        holidays.facets.hotels.Add(facetHotel);
                                    }

                                }
                                holidays.facets.resorts = new List<Resort>();
                                foreach (FacetsResortsBucket resortBucket in facResp.aggregations.resorts.buckets)
                                {
                                    Resort resort = new Resort();
                                    resort.count = resortBucket.NoOfHotels.value;
                                    resort.destinationId = Convert.ToInt32(resortBucket.key);
                                    if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                                    {
                                        resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                                        holidays.facets.resorts.Add(resort);
                                    }
                                }
                                holidays.facets.totalHotels = esIncSerResponse.hits.total;
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                }
                else
                {
                    int offersInserted = TTSSandBulkInsertionV_2(destinationIds, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, priceMin,
                                                                    priceMax, tradingNameIds, destinationType, hotelKeys, tripAdvisorRating, ratings, boardType,
                                                                    departureDate, labelId, skipFacets);
                    string esfacetsQuery = Utilities.GetESFacetsQuery(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                                        departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys,
                                                                        destinationType, labelId, tripAdvisorRating, usersPreferredHotelKeys);


                    var ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esfacetsQuery);
                    FacetsResponse esFacetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                    while (facetsTrials > 0)
                    {
                        if (esFacetsResponse.aggregations.price_max.value != 0 && esFacetsResponse.aggregations.price_min.value != 0 && esFacetsResponse.hits.total >= offersInserted)
                        {
                            ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esfacetsQuery);
                            esFacetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                            break;
                        }
                        else
                        {
                            ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esfacetsQuery);
                            esFacetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                            Thread.Sleep(500);
                            facetsTrials--;
                        }
                    }
                    //Holidays holidays = new Holidays();
                    WCFRestService.Model.Artirix.Rating taRating;
                    Journey journey;
                    Accommodation accommodation;
                    OfferHotel offerHotel;
                    if (boardType == "9,2,8,3,4,5")
                    {
                        boardType = string.Empty;
                    }
                    bool UserPref = false;
                    bool trading576 = false;
                    string hkte = string.Empty;
                    string hkteUPHK = string.Empty;
                    int offerCount = 0;
                    if (!string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        offerCount = hotelKeys.Split(',').Length;
                    }
                    else
                    {
                        offerCount = int.Parse(ConfigurationManager.AppSettings["OfferCount"]);
                    }
                    facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
                    string usersPreferredHotelKeysQuery = string.Empty;
                    string ESUPHKResponse = string.Empty;
                    ElasticsearchResponse esUPHKResponse = new ElasticsearchResponse();
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        var taNameIds = string.Empty;
                        if (!string.IsNullOrWhiteSpace(tradingNameIds))
                        {
                            taNameIds = tradingNameIds;
                        }
                        else
                        {
                            taNameIds = "576,192";
                        }
                        usersPreferredHotelKeysQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                    departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                    labelId, tripAdvisorRating, usersPreferredHotelKeys, taNameIds, 1);
                        ESUPHKResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", usersPreferredHotelKeysQuery);
                        esUPHKResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESUPHKResponse);
                        if (esUPHKResponse.hits.total != 0)
                        {
                            offerCount = offerCount - esUPHKResponse.aggregations.hotel_iff.buckets.Count;
                            UserPref = true;
                            hkteUPHK = esUPHKResponse.aggregations.hotel_iff.buckets[0].top_price.hits.hits[0]._source.hotel.iff;
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                    {
                        hkte = hkteUPHK + "," + hotelKeysToExclude;
                    }
                    else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                    {
                        hkte = hotelKeysToExclude;
                    }
                    else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        hkte = hkteUPHK;
                    }
                    string esQuery576 = string.Empty;
                    string ESTNID576Response = string.Empty;
                    string esQuery = string.Empty;
                    ElasticsearchResponse esTNID576Response = new ElasticsearchResponse();
                    int tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);
                    if (string.IsNullOrWhiteSpace(sort) && tradingNameIds != "808" && string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                        offerCount = offerCount - esTNID576Response.aggregations.hotel_iff.buckets.Count;
                        if (string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                     select ttssResult.key).Distinct().ToList());
                        }
                        else if (!string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = hkte + "," + String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                                  select ttssResult.key).Distinct().ToList());
                        }
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", "192", offerCount);
                        trading576 = true;
                    }
                    else
                    {
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", tradingNameIds, offerCount);
                    }


                    var ESResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery);
                    ElasticsearchResponse esSerResponse = null;
                    esSerResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESResponse);
                    offerCount = offerCount - esSerResponse.aggregations.hotel_iff.buckets.Count;
                    if (offerCount > 0 && trading576)
                    {
                        hkte = string.Empty;
                        if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        {
                            hkte = hkteUPHK + "," + hotelKeysToExclude;
                        }
                        else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        {
                            hkte = hotelKeysToExclude;
                        }
                        else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                        {
                            hkte = hkteUPHK;
                        }

                        //if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        //{
                        //    hkte = hkteUPHK + "," + hotelKeysToExclude;
                        //}
                        //else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        //{
                        //    hkte = hotelKeysToExclude;
                        //}

                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", offerCount + tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                    }
                    try
                    {
                        if (esSerResponse.hits.total != 0)
                        {

                            holidays.total = esFacetsResponse.hits.total;
                            holidays.totalHotels = esFacetsResponse.aggregations.hotel.buckets.Length;
                            holidays.offers = new List<Offer>();
                            ESRSource source = new ESRSource();
                            if (UserPref == true)
                            {
                                foreach (ESRBucket bucket in esUPHKResponse.aggregations.hotel_iff.buckets)
                                {
                                    Offer offr = new Offer();
                                    try
                                    {
                                        offr.id = bucket.top_price.hits.hits[0]._source.id;
                                        offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                        offr.price = bucket.top_price.hits.hits[0]._source.price;
                                        offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                        offr.isPreferredByUser = true;
                                        var phoneNumber = string.Empty;
                                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        }
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = "0123456789";
                                        }
                                        offr.phone = phoneNumber;
                                        //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                        journey = new Journey();
                                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                        journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                        journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                        journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                        offr.journey = journey;
                                        accommodation = new Accommodation();
                                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                        offr.accommodation = accommodation;
                                        offerHotel = new OfferHotel();
                                        offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                        offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                        offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                        //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                        offerHotel.features = new List<object>();
                                        offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                        offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                        offerHotel.mobileimages = new List<string>();
                                        offerHotel.thumbnailimages = new List<string>();
                                        offerHotel.images = new List<string>();
                                        int hoteliffCount = 0;
                                        if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                        {
                                            hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                            {
                                                offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            }
                                        }
                                        else
                                        {
                                            offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                        }
                                        taRating = new WCFRestService.Model.Artirix.Rating();
                                        taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                        taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                        taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                        offerHotel.rating = taRating;
                                        offr.hotel = offerHotel;
                                        offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                        offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                        offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    holidays.offers.Add(offr);
                                }
                            }
                            if (string.IsNullOrWhiteSpace(sort) && trading576 == true)
                            {
                                foreach (ESRBucket bucket in esTNID576Response.aggregations.hotel_iff.buckets)
                                {
                                    Offer offr = new Offer();
                                    try
                                    {
                                        offr.id = bucket.top_price.hits.hits[0]._source.id;
                                        offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                        offr.price = bucket.top_price.hits.hits[0]._source.price;
                                        var phoneNumber = string.Empty;
                                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        }
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = "0123456789";
                                        }
                                        offr.phone = phoneNumber;
                                        //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                        journey = new Journey();
                                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                        journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                        journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                        journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                        offr.journey = journey;
                                        accommodation = new Accommodation();
                                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                        offr.accommodation = accommodation;
                                        offerHotel = new OfferHotel();
                                        offerHotel.latlon = bucket.top_price.hits.hits[0]._source.hotel.latlon;
                                        offerHotel.starRating = bucket.top_price.hits.hits[0]._source.hotel.starRating;
                                        offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                        //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                        offerHotel.features = new List<object>();
                                        offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                        offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                        offerHotel.mobileimages = new List<string>();
                                        offerHotel.thumbnailimages = new List<string>();
                                        offerHotel.images = new List<string>();
                                        int hoteliffCount = 0;
                                        if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                        {
                                            hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                            {
                                                offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            }
                                        }
                                        else
                                        {
                                            offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                        }
                                        taRating = new WCFRestService.Model.Artirix.Rating();
                                        taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                        taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                        taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                        offerHotel.rating = taRating;
                                        offr.hotel = offerHotel;
                                        offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                        offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                        offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                        offr.isPreferredByUser = true;
                                        offr.isPreferential = true;
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    holidays.offers.Add(offr);
                                }
                            }
                            foreach (ESRBucket bucket in esSerResponse.aggregations.hotel_iff.buckets)
                            {
                                Offer offr = new Offer();
                                try
                                {
                                    offr.id = bucket.top_price.hits.hits[0]._source.id;
                                    offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                    offr.price = bucket.top_price.hits.hits[0]._source.price;
                                    offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                    //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                    var phoneNumber = string.Empty;
                                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    }
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = "0123456789";
                                    }
                                    offr.phone = phoneNumber;//GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    journey = new Journey();
                                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                    journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                    journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                    journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                    offr.journey = journey;
                                    accommodation = new Accommodation();
                                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                    offr.accommodation = accommodation;
                                    offerHotel = new OfferHotel();
                                    offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                    offerHotel.mobileimages = new List<string>();
                                    offerHotel.thumbnailimages = new List<string>();
                                    offerHotel.images = new List<string>();
                                    int hoteliffCount = 0;
                                    if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                    {
                                        hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                        {
                                            offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        }
                                    }
                                    else
                                    {
                                        offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                    }
                                    taRating = new WCFRestService.Model.Artirix.Rating();
                                    taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                    taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                    taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                    offerHotel.rating = taRating;
                                    //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                    offerHotel.features = new List<object>();
                                    offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                    offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                    offr.hotel = offerHotel;
                                    offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                    offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                    offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                }
                                catch (Exception ex)
                                {

                                }
                                holidays.offers.Add(offr);
                            }
                            holidays.facets = new Facets();
                            holidays.facets.priceRange = new Pricerange();
                            holidays.facets.priceRange.max = Convert.ToInt32(esFacetsResponse.aggregations.price_max.value);
                            holidays.facets.priceRange.min = Convert.ToInt32(esFacetsResponse.aggregations.price_min.value);
                            holidays.facets.airports = new List<WCFRestService.Model.Artirix.Airport>();
                            foreach (FacetsAirportsBucket airportBucket in esFacetsResponse.aggregations.airports.buckets)
                            {
                                try
                                {
                                    WCFRestService.Model.Artirix.Airport airport = new WCFRestService.Model.Artirix.Airport();
                                    airport.count = airportBucket.doc_count;
                                    airport.departureId = airportBucket.key;
                                    airport.name = airportCodes[airportBucket.key.ToString()];
                                    holidays.facets.airports.Add(airport);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            holidays.facets.hotels = new List<FacetHotel>();
                            foreach (FacetsHotelBucket hotelBucket in esFacetsResponse.aggregations.hotel.buckets)
                            {
                                FacetHotel facetHotel = new FacetHotel();
                                facetHotel.count = hotelBucket.doc_count;
                                facetHotel.hotelKey = hotelBucket.key.ToString();
                                if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                                {
                                    facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                                    if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                                    {
                                        facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];

                                    }
                                    holidays.facets.hotels.Add(facetHotel);
                                }

                            }
                            holidays.facets.resorts = new List<Resort>();
                            foreach (FacetsResortsBucket resortBucket in esFacetsResponse.aggregations.resorts.buckets)
                            {
                                Resort resort = new Resort();
                                resort.count = resortBucket.NoOfHotels.value;
                                resort.destinationId = Convert.ToInt32(resortBucket.key);
                                if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                                {
                                    resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                                    holidays.facets.resorts.Add(resort);
                                }
                            }
                            holidays.facets.totalHotels = esFacetsResponse.aggregations.hotel.buckets.Length;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                new System.ServiceModel.Web.WebFaultException<string>("Error with the search saving", System.Net.HttpStatusCode.InternalServerError);
            }

            //holidays.offers.OrderBy(t => t.tradingNameId).ThenByDescending(o => o.price);
            return holidays;
        }

        public static int TTSSandBulkInsertion(string destinationIds, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults, int children,
                                                  int priceMin, int priceMax, string tradingNameIds, string destinationType, string hotelKeys, int tripAdvisorRating,
                                                  string ratings, string boardType, string departureDate, string labelId, string skipFacets)
        {
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            SearchResponse serResponse = new SearchResponse();
            Random random = new Random();
            var paramsString = string.Empty;
            string test = string.Empty;
            string bulk = string.Empty;
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            ESOffer offer;
            ESJourney journey;
            ESAccommodation accommodation;
            ESOfferHotel hotel;
            ESRating taRating;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            serResponse.responseId = DateTime.Now.Ticks;
            paramsString = Utilities.GetTTSSUrlV_2(destinationIds, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, priceMin,
                                                    tradingNameIds, destinationType, hotelKeys, tripAdvisorRating, ratings, boardType);
            string TTSSURL = ConfigurationManager.AppSettings["TTSSURL"] + "searchId=2&userName=TTHOD&" + paramsString;
            var result = String.Empty;
            string combo = string.Empty;
            List<string> distinctHotelKeys = null;
            List<string> redisStaticInfo = null;
            Dictionary<int, MHIDStaticInfo> staticInfo = new Dictionary<int, MHIDStaticInfo>();
            int offersDecorated = 0;
            int offersInserted = 0;
            ttssResponseStream ttssRes = new ttssResponseStream();
            ttssRes = Utilities.ExecuteTTSSGetWebRequest(TTSSURL);
            XmlDocument docResponse = new XmlDocument();
            docResponse.LoadXml(ttssRes.ttssResponse);
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Container));
                Container mytest = (Container)serializer.Deserialize(new StringReader(ttssRes.ttssResponse));

                MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
                distinctHotelKeys = (from ttssResult in mytest.Results.Result select ttssResult.HotelKey).Distinct().ToList();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(mhidStaticInfo._source.MasterHotelId))
                            {
                                staticInfo.Add(mhidStaticInfo._source.MasterHotelId, mhidStaticInfo);
                            }
                        }
                    }
                }
                Parallel.ForEach(mytest.Results.Result, parrOptions, results =>
                //foreach (var results in mytest.Results.Result)
                {
                    var staticData = String.Empty;
                    MHIDStaticInfo redisStaticData = null;
                    offer = new ESOffer();
                    try
                    {
                        if (staticInfo.ContainsKey(int.Parse(results.HotelKey)))
                        {
                            redisStaticData = staticInfo[int.Parse(results.HotelKey)];
                        }
                        else
                        {
                            staticData = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["StaticData"] + results.HotelKey);
                            redisStaticData = JsonConvert.DeserializeObject<MHIDStaticInfo>(staticData);
                            if (!staticInfo.ContainsKey(int.Parse(results.HotelKey)) && redisStaticData != null)
                            {
                                staticInfo.Add(int.Parse(results.HotelKey), redisStaticData);
                            }
                        }
                        if (redisStaticData != null)
                        {
                            serResponse.ttssCount = int.Parse(mytest.Results.Count);
                            offer.airportCode = reverseAirportCodes[results.Departure];
                            if (departureIds < 0)
                            {
                                offer.airportCollectionId = departureIds;
                            }
                            offer.id = results.Id;
                            offer.quoteRef = results.QuoteRef;
                            offer.price = int.Parse(results.Price);
                            offer.labelId = int.Parse(destinationIds);
                            offer.regionId = int.Parse(results.DestinationId);
                            offer.regionName = redisStaticData._source.Region.Regname;
                            offer.responseId = serResponse.responseId;
                            offer.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                            offer.resortId = redisStaticData._source.Region.ResortId;
                            if (results.TradingNameId == "576")
                            {
                                offer.isPreferential = true;
                                offer.isPreferredByUser = true;
                            }
                            else
                            {
                                offer.isPreferential = false;
                                offer.isPreferredByUser = false;
                            }
                            offer.phone = results.Phone;
                            journey = new ESJourney();
                            journey.departure = results.Departure;
                            journey.outboundArrivalDate = results.OutboundArrivalDate;
                            journey.inboundDepartureDate = results.InboundDepartureDate;
                            journey.departureDate = results.DepartureDate;
                            journey.returnDate = results.ReturnDate;
                            journey.duration = int.Parse(results.Duration);
                            journey.gatewayName = results.GatewayName;
                            journey.gatewayCode = results.GatewayCode;
                            journey.flightOperator = results.FlightOperator;
                            journey.destination = results.Destination;
                            journey.glat = results.Glat;
                            journey.glong = results.Glong;
                            journey.parentRegion = redisStaticData._source.Region.ParentRegionName;
                            journey.parentRegionId = redisStaticData._source.Region.ParentRegionId;
                            journey.inboundFlightDirect = results.InboundFlightDirect == "true";
                            journey.outboundFlightDirect = results.OutboundFlightDirect == "true";
                            offer.journey = journey;
                            accommodation = new ESAccommodation();
                            accommodation.boardTypeCode = results.BoardTypeCode;
                            accommodation.boardTypeId = int.Parse(results.BoardTypeId);
                            accommodation.adults = int.Parse(results.Adults);
                            accommodation.children = int.Parse(results.Children);
                            accommodation.rating = (int)Convert.ToDouble(results.Rating);
                            accommodation.updated = results.Updated;
                            offer.accommodation = accommodation;
                            hotel = new ESOfferHotel();
                            hotel.iff = int.Parse(results.HotelKey);
                            hotel.starRating = redisStaticData._source.Rating.ToString();
                            hotel.name = redisStaticData._source.BuildingName;
                            hotel.description = null;
                            hotel.features = redisStaticData._source.Features;
                            hotel.images = new List<string>();
                            hotel.images = null;
                            hotel.mobileimages = null;
                            taRating = new ESRating();
                            if (redisStaticData.TripAdvisor.TAID != 0)
                            {
                                taRating.tripAdvisorId = redisStaticData.TripAdvisor.TAID.ToString();
                            }
                            if (redisStaticData.TripAdvisor.reviewsCount != 0)
                            {
                                taRating.reviewCount = redisStaticData.TripAdvisor.reviewsCount;
                            }
                            if (redisStaticData.TripAdvisor.TARating != 0)
                            {
                                taRating.averageRating = (int)((float)redisStaticData.TripAdvisor.TARating * 10);
                            }
                            hotel.rating = taRating;
                            offer.hotel = hotel;
                            offer.hotelOperator = results.HotelOperator;
                            offer.contentId = results.ContentId;
                            offer.tradingNameId = int.Parse(results.TradingNameId);
                            test = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offer);
                            offersInserted++;
                            combo = "{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\",\"_id\":\"" + results.QuoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + test;
                            searchParams.AppendLine(combo);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
            catch (Exception ex)
            {

            }
            try
            {
                bulk = searchParams.ToString();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //var test1 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offerlist);
                    streamWriter.Write(bulk);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

            }
            //Thread.Sleep(2 * serResponse.ttssCount);
            Task.Factory.StartNew(() =>
            {
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                int parentDepartureIds = parentAirportCodes[departureIds];
                string regionDestinationIds = string.Empty;
                if (string.IsNullOrWhiteSpace(destinationType))
                {
                    Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                    regionDestinationIds = parentRegionIds[destinationIds];
                }
                else
                {
                    regionDestinationIds = destinationIds;
                }
                ESSearchRequest esSearchrequest = new ESSearchRequest();
                esSearchrequest.adults = adults;
                esSearchrequest.children = children;
                esSearchrequest.dateMax = dateMax + "T23:59:59";
                esSearchrequest.dateMin = dateMin + "T00:00:00";
                esSearchrequest.GUID = skipFacets;
                esSearchrequest.ttssURL = TTSSURL;
                if (string.IsNullOrWhiteSpace(destinationType))
                {
                    esSearchrequest.destinationIds = destinationIds;
                }
                else
                {
                    esSearchrequest.destinationIds = labelId;
                }
                esSearchrequest.departureIds = departureIds;
                esSearchrequest.durationMin = durationMin;
                esSearchrequest.durationMax = durationMax;
                esSearchrequest.parentDepartureIds = parentDepartureIds;
                esSearchrequest.responseId = serResponse.responseId;
                esSearchrequest.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                esSearchrequest.regionIds = regionDestinationIds.Split(',').ToList();
                if (string.IsNullOrWhiteSpace(boardType))
                {
                    boardType = "2,3,5,4,8,9";
                }
                esSearchrequest.boardType = boardType.Split(',').ToList();
                esSearchrequest.ratings = ratings.Split(',').ToList();
                esSearchrequest.ttssCount = serResponse.ttssCount;
                esSearchrequest.ttssResponseTime = ttssRes.ttssResponseTime;
                esSearchrequest.isCahceHit = false;
                esSearchrequest.offersFound = distinctHotelKeys.Count;
                esSearchrequest.offersDecorated = staticInfo.Count;
                esSearchrequest.offersInserted = offersInserted;
                if (!string.IsNullOrWhiteSpace(hotelKeys))
                {
                    esSearchrequest.hotelKeys = hotelKeys;
                    esSearchrequest.isHotelKeys = "true";
                }
                else
                {
                    //esSearchrequest.hotelKeys = null;
                    esSearchrequest.isHotelKeys = "false";
                }
                if (string.IsNullOrWhiteSpace(tradingNameIds))
                {
                    var taIds = "576,192";
                    esSearchrequest.tradingNameId = taIds.Split(',').ToList();
                }
                else
                {
                    esSearchrequest.tradingNameId = tradingNameIds.Split(',').ToList();
                }
                var httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);
                httpWebSearchRequest.ContentType = "application/json";
                httpWebSearchRequest.Method = "POST";
                httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                {
                    var test2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(esSearchrequest);
                    streamWriter.Write(test2);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                string esSearchInsertionResult = string.Empty;
                ESNewSearchInsertion esSearchInsertionResponse = null;
                using (var streamReader = new StreamReader(httpSearchResponse.GetResponseStream()))
                {
                    esSearchInsertionResult = streamReader.ReadToEnd();
                    try
                    {
                        esSearchInsertionResponse = JsonConvert.DeserializeObject<ESNewSearchInsertion>(esSearchInsertionResult);
                        if (esSearchInsertionResponse.created.Equals(true))
                        {
                            //responseId = esSearchrequest.timeStamp;
                            //srcResponse.responseId = esSearchInsertionResponse._id;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            });
            return offersInserted;
        }

        //3)Using
        public static int TTSSandBulkInsertionV_2(string destinationIds, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax,
                                                            int adults, int children, int priceMin, int priceMax, string tradingNameIds, string destinationType,
                                                            string hotelKeys, int tripAdvisorRating, string ratings, string boardType, string departureDate, string labelId, string skipFacets)
        {
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            SearchResponse serResponse = new SearchResponse();
            Random random = new Random();
            int randomNumber = random.Next(0, 1000);
            var paramsString = string.Empty;
            string test = string.Empty;
            string bulk = string.Empty;
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            ESOffer offer;
            ESJourney journey;
            ESAccommodation accommodation;
            ESOfferHotel hotel;
            ESRating taRating;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            Int32 minCountOfImage = Global.minCountOfImage;
            string imgSourceURLFormat = Global.imgSourceURLFormat;
            serResponse.responseId = DateTime.Now.Ticks;
            DateTime timeStamp = DateTime.UtcNow;
            paramsString = Utilities.GetTTSSUrlV_2(destinationIds, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, priceMin,
                                                    tradingNameIds, destinationType, hotelKeys, tripAdvisorRating, ratings, boardType);
            string TTSSURL = ConfigurationManager.AppSettings["TTSSURL"] + "searchId=2&userName=TTHOD&" + paramsString;
            var result = String.Empty;
            string combo = string.Empty;
            List<string> distinctHotelKeys = null;
            List<string> redisStaticInfo = null;
            Dictionary<int, MHIDStaticInfo> staticInfo = new Dictionary<int, MHIDStaticInfo>();
            int offersDecorated = 0;
            int offersInserted = 0;
            ttssResponseStream ttssRes = new ttssResponseStream();
            ttssRes = Utilities.ExecuteTTSSGetWebRequest(TTSSURL);
            XmlDocument docResponse = new XmlDocument();
            docResponse.LoadXml(ttssRes.ttssResponse);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Container));
                Container mytest = (Container)serializer.Deserialize(new StringReader(ttssRes.ttssResponse));
                MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
                distinctHotelKeys = (from ttssResult in mytest.Results.Result select ttssResult.HotelKey).Distinct().ToList();
                using (ServiceStack.Redis.PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(mhidStaticInfo._source.MasterHotelId))
                            {
                                staticInfo.Add(mhidStaticInfo._source.MasterHotelId, mhidStaticInfo);
                            }
                        }
                    }
                }

                Parallel.ForEach(mytest.Results.Result, parrOptions, results =>
                // foreach (var results in mytest.Results.Result)
                {
                    var staticData = String.Empty;
                    MHIDStaticInfo redisStaticData = null;
                    offer = new ESOffer();
                    try
                    {
                        if (staticInfo.ContainsKey(int.Parse(results.HotelKey)))
                        {
                            redisStaticData = staticInfo[int.Parse(results.HotelKey)];
                        }
                        else
                        {
                            staticData = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["StaticData"] + results.HotelKey);
                            redisStaticData = JsonConvert.DeserializeObject<MHIDStaticInfo>(staticData);
                            if (!staticInfo.ContainsKey(int.Parse(results.HotelKey)) && redisStaticData != null)
                            {
                                staticInfo.Add(int.Parse(results.HotelKey), redisStaticData);
                            }
                        }
                        if (redisStaticData != null)
                        {
                            serResponse.ttssCount = int.Parse(mytest.Results.Count);
                            offer.airportCode = reverseAirportCodes[results.Departure];
                            if (departureIds < 0)
                            {
                                offer.airportCollectionId = departureIds;
                            }
                            offer.id = results.Id;
                            offer.quoteRef = results.QuoteRef;
                            offer.price = int.Parse(results.Price);
                            offer.labelId = int.Parse(destinationIds);
                            offer.regionId = int.Parse(results.DestinationId);
                            offer.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                            offer.regionName = redisStaticData._source.Region.Regname;
                            offer.responseId = serResponse.responseId;
                            offer.resortId = redisStaticData._source.Region.ResortId;
                            if (results.TradingNameId == "576")
                            {
                                offer.isPreferential = true;
                                offer.isPreferredByUser = true;
                            }
                            else
                            {
                                offer.isPreferential = false;
                                offer.isPreferredByUser = false;
                            }
                            offer.phone = results.Phone;
                            journey = new ESJourney();
                            journey.departure = results.Departure;
                            journey.outboundArrivalDate = results.OutboundArrivalDate;
                            journey.inboundDepartureDate = results.InboundDepartureDate;
                            journey.departureDate = results.DepartureDate;
                            journey.returnDate = results.ReturnDate;
                            journey.duration = int.Parse(results.Duration);
                            journey.gatewayName = results.GatewayName;
                            journey.gatewayCode = results.GatewayCode;
                            journey.flightOperator = results.FlightOperator;
                            journey.destination = results.Destination;
                            journey.glat = results.Glat;
                            journey.glong = results.Glong;
                            journey.parentRegion = redisStaticData._source.Region.ParentRegionName;
                            journey.parentRegionId = redisStaticData._source.Region.ParentRegionId;
                            journey.inboundFlightDirect = results.InboundFlightDirect == "true";
                            journey.outboundFlightDirect = results.OutboundFlightDirect == "true";
                            offer.journey = journey;
                            accommodation = new ESAccommodation();
                            accommodation.boardTypeCode = results.BoardTypeCode;
                            accommodation.boardTypeId = int.Parse(results.BoardTypeId);
                            accommodation.adults = int.Parse(results.Adults);
                            accommodation.children = int.Parse(results.Children);
                            accommodation.rating = (int)Convert.ToDouble(results.Rating);
                            accommodation.updated = results.Updated;
                            offer.accommodation = accommodation;
                            hotel = new ESOfferHotel();
                            hotel.iff = int.Parse(results.HotelKey);
                            hotel.name = redisStaticData._source.BuildingName;
                            hotel.description = null;
                            hotel.features = redisStaticData._source.Features;
                            hotel.starRating = redisStaticData._source.Rating.ToString();
                            hotel.images = null;
                            hotel.mobileimages = null;
                            hotel.latlong = results.Glat + "," + results.Glong;
                            taRating = new ESRating();
                            if (redisStaticData.TripAdvisor.TAID != 0)
                            {
                                taRating.tripAdvisorId = redisStaticData.TripAdvisor.TAID.ToString();
                            }
                            if (redisStaticData.TripAdvisor.reviewsCount != 0)
                            {
                                taRating.reviewCount = redisStaticData.TripAdvisor.reviewsCount;
                            }
                            if (redisStaticData.TripAdvisor.TARating != 0)
                            {
                                taRating.averageRating = (int)((float)redisStaticData.TripAdvisor.TARating * 10);
                            }
                            hotel.rating = taRating;
                            offer.hotel = hotel;
                            offer.hotelOperator = results.HotelOperator;
                            offer.contentId = results.ContentId;
                            offer.tradingNameId = int.Parse(results.TradingNameId);
                            test = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offer);
                            offersInserted++;
                            combo = "{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\",\"_id\":\"" + results.QuoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + test;
                            searchParams.AppendLine(combo);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
            catch (Exception ex)
            {

            }

            try
            {
                bulk = searchParams.ToString();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                request.ContentType = "application/json";
                request.Method = "POST";
                request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    //var test1 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offerlist);
                    streamWriter.Write(bulk);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

            }
            //Thread.Sleep(2 * serResponse.ttssCount);
            Task.Factory.StartNew(() =>
            {
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                int parentDepartureIds = parentAirportCodes[departureIds];
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                string regionDestinationIds = destinationIds;
                if (string.IsNullOrWhiteSpace(destinationType))
                {
                    regionDestinationIds = parentRegionIds[destinationIds];
                }
                ESSearchRequest esSearchrequest = new ESSearchRequest();
                esSearchrequest.adults = adults;
                esSearchrequest.children = children;
                esSearchrequest.dateMax = dateMax + "T23:59:59";
                esSearchrequest.dateMin = dateMin + "T00:00:00";
                esSearchrequest.departureIds = departureIds;
                if (string.IsNullOrWhiteSpace(destinationType))
                {
                    esSearchrequest.destinationIds = destinationIds;
                }
                else
                {
                    esSearchrequest.destinationIds = labelId;
                }
                esSearchrequest.durationMin = durationMin;
                esSearchrequest.durationMax = durationMax;
                esSearchrequest.parentDepartureIds = parentDepartureIds;
                esSearchrequest.responseId = serResponse.responseId;
                esSearchrequest.regionIds = regionDestinationIds.Split(',').ToList();
                esSearchrequest.ttssCount = serResponse.ttssCount;
                esSearchrequest.ttssResponseTime = ttssRes.ttssResponseTime;
                esSearchrequest.isCahceHit = false;
                esSearchrequest.ttssURL = TTSSURL;
                esSearchrequest.GUID = skipFacets;
                esSearchrequest.boardType = boardType.Split(',').ToList();
                esSearchrequest.ratings = ratings.Split(',').ToList();
                esSearchrequest.timeStamp = timeStamp.ToString("yyyy-MM-ddTHH:mm:ss");
                esSearchrequest.offersFound = distinctHotelKeys.Count;
                esSearchrequest.offersDecorated = staticInfo.Count;
                esSearchrequest.offersInserted = offersInserted;
                if (!string.IsNullOrWhiteSpace(hotelKeys))
                {
                    esSearchrequest.hotelKeys = hotelKeys;
                    esSearchrequest.isHotelKeys = "true";
                }
                else
                {
                    //esSearchrequest.hotelKeys = null;
                    esSearchrequest.isHotelKeys = "false";
                }
                if (string.IsNullOrWhiteSpace(tradingNameIds))
                {
                    var taIds = "576,192";
                    esSearchrequest.tradingNameId = taIds.Split(',').ToList();
                }
                else
                {
                    esSearchrequest.tradingNameId = tradingNameIds.Split(',').ToList();
                }
                var httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);
                httpWebSearchRequest.ContentType = "application/json";
                httpWebSearchRequest.Method = "POST";
                httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                {
                    var test2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(esSearchrequest);
                    streamWriter.Write(test2);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                string esSearchInsertionResult = string.Empty;
                ESNewSearchInsertion esSearchInsertionResponse = null;
                using (var streamReader = new StreamReader(httpSearchResponse.GetResponseStream()))
                {
                    esSearchInsertionResult = streamReader.ReadToEnd();
                    try
                    {
                        esSearchInsertionResponse = JsonConvert.DeserializeObject<ESNewSearchInsertion>(esSearchInsertionResult);
                        if (esSearchInsertionResponse.created.Equals(true))
                        {
                            //responseId = esSearchrequest.timeStamp;
                            //srcResponse.responseId = esSearchInsertionResponse._id;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            });
            return offersInserted;
        }

        public static Holidays holidaysElasticsearchDirect(long responseId, int ttssCount, string destinationIds, int departureIds, string boardType, string departureDate,
                                        string dateMin, string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin, int priceMax,
                                        string ratings, string tradingNameIds, string destinationType, string hotelKeys, int tripAdvisorRating, string hotelKeysToExclude,
                                        string sort, string channelId, string labelId, string usersPreferredHotelKeys, string skipFacets)
        {
            string imgSourceURLFormat = Global.imgSourceURLFormat;
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            Holidays holidays = new Holidays();
            Journey journey;
            WCFRestService.Model.Artirix.Rating taRating;
            Accommodation accommodation;
            OfferHotel offerHotel;
            Dictionary<string, string> airportCodes = Global.airportCodes;
            int offerCount = 0;
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                offerCount = hotelKeys.Split(',').Length;
            }
            else
            {
                offerCount = int.Parse(ConfigurationManager.AppSettings["OfferCount"]);
            }
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            string usersPreferredHotelKeysQuery = string.Empty;
            string ESUPHKResponse = string.Empty;
            bool UserPref = false;
            bool trading576 = false;
            string hkte = string.Empty;
            string hkteUPHK = string.Empty;
            ElasticsearchResponse esUPHKResponse = new ElasticsearchResponse();
            string esFacetsQuery = string.Empty;
            esFacetsQuery = Utilities.GetESFacetsQuery(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, usersPreferredHotelKeys);
            var ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
            FacetsResponse facetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
            if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                var taNameIds = string.Empty;
                if (!string.IsNullOrWhiteSpace(tradingNameIds))
                {
                    taNameIds = tradingNameIds;
                }
                else
                {
                    taNameIds = "576,192";
                }
                usersPreferredHotelKeysQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, usersPreferredHotelKeys, taNameIds, 1);
                ESUPHKResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", usersPreferredHotelKeysQuery);
                esUPHKResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESUPHKResponse);
                if (esUPHKResponse.hits.total != 0)
                {
                    offerCount = offerCount - esUPHKResponse.aggregations.hotel_iff.buckets.Count;
                    UserPref = true;
                    hkteUPHK = esUPHKResponse.aggregations.hotel_iff.buckets[0].top_price.hits.hits[0]._source.hotel.iff;
                }

            }
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                hkte = hkteUPHK + "," + hotelKeysToExclude;
            }
            else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                hkte = hotelKeysToExclude;
            }
            else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                hkte = hkteUPHK;
            }
            string esQuery576 = string.Empty;
            string ESTNID576Response = string.Empty;
            ElasticsearchResponse esTNID576Response = new ElasticsearchResponse();
            int tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);
            string esQuery = string.Empty;
            if (string.IsNullOrWhiteSpace(sort) && tradingNameIds != "808" && string.IsNullOrWhiteSpace(hotelKeys))
            {
                esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", "576", tradingNameId576Count);
                ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                offerCount = offerCount - esTNID576Response.aggregations.hotel_iff.buckets.Count;
                if (string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                {
                    hkte = String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                             select ttssResult.key).Distinct().ToList());
                }
                else if (!string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                {
                    hkte = hkte + "," + String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                          select ttssResult.key).Distinct().ToList());
                }
                esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                    departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                    labelId, tripAdvisorRating, "", "192", offerCount);
                trading576 = true;
            }
            else
            {
                esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                    departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                    labelId, tripAdvisorRating, "", tradingNameIds, offerCount);
            }
            var ESResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery);
            ElasticsearchResponse esSerResponse;
            esSerResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESResponse);
            offerCount = offerCount - esSerResponse.aggregations.hotel_iff.buckets.Count;
            if (offerCount > 0 && trading576)
            {
                hkte = string.Empty;
                if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    hkte = hkteUPHK + "," + hotelKeysToExclude;
                }
                else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                {
                    hkte = hotelKeysToExclude;
                }
                else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    hkte = hkteUPHK;
                }

                //if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                //{
                //    hkte = hkteUPHK + "," + hotelKeysToExclude;
                //}
                //else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                //{
                //    hkte = hotelKeysToExclude;
                //}
                esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                labelId, tripAdvisorRating, "", "576", offerCount + tradingNameId576Count);
                ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
            }

            try
            {
                if (facetsResponse.aggregations.hotel.buckets.Length != 0 &&
                    ((facetsResponse.aggregations.hotel.buckets.Length > int.Parse(ConfigurationManager.AppSettings["ThresholdOfferLimit"]) && ttssCount == int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"]))
                        || ttssCount < int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"])))
                {
                    holidays.total = facetsResponse.hits.total;
                    holidays.totalHotels = facetsResponse.aggregations.hotel.buckets.Length;
                    holidays.offers = new List<Offer>();
                    ESRSource source = new ESRSource();
                    if (UserPref == true)
                    {
                        foreach (ESRBucket bucket in esUPHKResponse.aggregations.hotel_iff.buckets)
                        {
                            Offer offr = new Offer();
                            try
                            {
                                offr.id = bucket.top_price.hits.hits[0]._source.id;
                                offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                offr.price = bucket.top_price.hits.hits[0]._source.price;
                                offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                offr.isPreferredByUser = true;
                                var phoneNumber = string.Empty;
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                }
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = "0123456789";
                                }
                                offr.phone = phoneNumber;
                                //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                journey = new Journey();
                                journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                offr.journey = journey;
                                accommodation = new Accommodation();
                                accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                offr.accommodation = accommodation;
                                offerHotel = new OfferHotel();
                                offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                offerHotel.mobileimages = new List<string>();
                                offerHotel.thumbnailimages = new List<string>();
                                offerHotel.images = new List<string>();
                                int hoteliffCount = 0;
                                if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                {
                                    hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                    for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                    {
                                        offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                    }
                                }
                                else
                                {
                                    offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                }
                                taRating = new WCFRestService.Model.Artirix.Rating();
                                taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                offerHotel.rating = taRating;
                                //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                offerHotel.features = new List<object>();
                                offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                offr.hotel = offerHotel;
                                offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            holidays.offers.Add(offr);
                        }
                    }

                    if (trading576 == true && string.IsNullOrWhiteSpace(sort))
                    {
                        foreach (ESRBucket bucket in esTNID576Response.aggregations.hotel_iff.buckets)
                        {
                            Offer offr = new Offer();
                            try
                            {
                                offr.id = bucket.top_price.hits.hits[0]._source.id;
                                offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                offr.price = bucket.top_price.hits.hits[0]._source.price;
                                var phoneNumber = string.Empty;
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                }
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = "0123456789";
                                }
                                offr.phone = phoneNumber;
                                //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                journey = new Journey();
                                journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                offr.journey = journey;
                                accommodation = new Accommodation();
                                accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                offr.accommodation = accommodation;
                                offerHotel = new OfferHotel();
                                offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                offerHotel.mobileimages = new List<string>();
                                offerHotel.thumbnailimages = new List<string>();
                                offerHotel.images = new List<string>();
                                int hoteliffCount = 0;
                                if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                {
                                    hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                    for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                    {
                                        offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                    }
                                }

                                else
                                {
                                    offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                }
                                taRating = new WCFRestService.Model.Artirix.Rating();
                                taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                offerHotel.rating = taRating;
                                //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                offerHotel.features = new List<object>();
                                offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                offr.hotel = offerHotel;
                                offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                offr.isPreferredByUser = true;
                                offr.isPreferential = true;
                            }
                            catch (Exception ex)
                            {

                            }
                            holidays.offers.Add(offr);
                        }
                    }
                    foreach (ESRBucket bucket in esSerResponse.aggregations.hotel_iff.buckets)
                    {
                        Offer offr = new Offer();
                        try
                        {
                            offr.id = bucket.top_price.hits.hits[0]._source.id;
                            offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                            offr.price = bucket.top_price.hits.hits[0]._source.price;
                            offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                            var phoneNumber = string.Empty;
                            phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                            }
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = "0123456789";
                            }
                            offr.phone = phoneNumber;
                            journey = new Journey();
                            journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                            journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                            journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                            journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                            journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                            journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                            journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                            journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                            journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                            journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                            journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                            journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                            journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                            journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                            journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                            journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                            offr.journey = journey;
                            accommodation = new Accommodation();
                            accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                            accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                            accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                            accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                            accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                            offr.accommodation = accommodation;
                            offerHotel = new OfferHotel();
                            offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                            offerHotel.mobileimages = new List<string>();
                            offerHotel.thumbnailimages = new List<string>();
                            offerHotel.images = new List<string>();
                            int hoteliffCount = 0;
                            if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                            {
                                hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                    offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                    offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                            else
                            {
                                offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            }
                            taRating = new WCFRestService.Model.Artirix.Rating();
                            taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                            taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                            taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                            offerHotel.rating = taRating;
                            //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                            offerHotel.features = new List<object>();
                            offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                            offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                            offr.hotel = offerHotel;
                            offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                            offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                            offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        holidays.offers.Add(offr);
                    }
                    //);
                    holidays.facets = new Facets();
                    holidays.facets.priceRange = new Pricerange();
                    holidays.facets.priceRange.max = Convert.ToInt32(facetsResponse.aggregations.price_max.value);
                    holidays.facets.priceRange.min = Convert.ToInt32(facetsResponse.aggregations.price_min.value);
                    holidays.facets.airports = new List<Airport>();
                    foreach (FacetsAirportsBucket airportBucket in facetsResponse.aggregations.airports.buckets)
                    {
                        try
                        {
                            Airport airport = new Airport();
                            airport.count = airportBucket.doc_count;
                            airport.departureId = airportBucket.key;
                            airport.name = airportCodes[airportBucket.key.ToString()];
                            holidays.facets.airports.Add(airport);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    holidays.facets.hotels = new List<FacetHotel>();
                    foreach (FacetsHotelBucket hotelBucket in facetsResponse.aggregations.hotel.buckets)
                    {
                        FacetHotel facetHotel = new FacetHotel();
                        facetHotel.count = hotelBucket.doc_count;
                        facetHotel.hotelKey = hotelBucket.key.ToString();
                        if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                        {
                            facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                            if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                            {
                                facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];
                            }
                            holidays.facets.hotels.Add(facetHotel);
                        }
                    }
                    holidays.facets.resorts = new List<Resort>();
                    foreach (FacetsResortsBucket resortBucket in facetsResponse.aggregations.resorts.buckets)
                    {
                        Resort resort = new Resort();
                        resort.count = resortBucket.NoOfHotels.value;
                        resort.destinationId = Convert.ToInt32(resortBucket.key);
                        if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                        {
                            resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                            holidays.facets.resorts.Add(resort);
                        }
                    }
                    holidays.facets.totalHotels = facetsResponse.aggregations.hotel.buckets.Length;
                    Task.Factory.StartNew(() =>
                    {
                        Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                        int parentDepartureIds = parentAirportCodes[departureIds];
                        string regionDestinationIds = string.Empty;
                        ESSearchRequest esSearchrequestInc = new ESSearchRequest();
                        if (string.IsNullOrWhiteSpace(destinationType))
                        {
                            Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                            regionDestinationIds = parentRegionIds[destinationIds];
                            esSearchrequestInc.destinationIds = destinationIds;
                        }
                        else
                        {
                            regionDestinationIds = destinationIds;
                            esSearchrequestInc.destinationIds = labelId;
                        }
                        if (string.IsNullOrWhiteSpace(boardType))
                        {
                            boardType = "9,2,8,3,4,5";

                        }
                        if (string.IsNullOrWhiteSpace(ratings))
                        {
                            ratings = "1,2,3,4,5";

                        }
                        esSearchrequestInc.adults = adults;
                        esSearchrequestInc.children = children;
                        esSearchrequestInc.dateMax = dateMax + "T23:59:59";
                        esSearchrequestInc.dateMin = dateMin + "T00:00:00";
                        esSearchrequestInc.GUID = skipFacets;
                        esSearchrequestInc.departureIds = departureIds;
                        esSearchrequestInc.durationMin = durationMin;
                        esSearchrequestInc.durationMax = durationMax;
                        esSearchrequestInc.parentDepartureIds = parentDepartureIds;
                        esSearchrequestInc.responseId = responseId;
                        esSearchrequestInc.regionIds = regionDestinationIds.Split(',').ToList();
                        esSearchrequestInc.boardType = boardType.Split(',').ToList();
                        esSearchrequestInc.ratings = ratings.Split(',').ToList();
                        esSearchrequestInc.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                        if (!string.IsNullOrWhiteSpace(hotelKeys))
                        {
                            esSearchrequestInc.hotelKeys = hotelKeys;
                            esSearchrequestInc.isHotelKeys = "true";
                        }
                        else
                        {
                            //esSearchrequest.hotelKeys = null;
                            esSearchrequestInc.isHotelKeys = "false";
                        }
                        if (string.IsNullOrWhiteSpace(tradingNameIds))
                        {
                            var taIds = "576,192";
                            esSearchrequestInc.tradingNameId = taIds.Split(',').ToList();
                        }
                        else
                        {
                            esSearchrequestInc.tradingNameId = tradingNameIds.Split(',').ToList();
                        }
                        //esSearchrequest.ttssCount = serResponse.ttssCount;
                        //esSearchrequest.ttssResponseTime = ttssRes.ttssResponseTime;
                        esSearchrequestInc.isCahceHit = true;
                        var httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["SearchIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);
                        httpWebSearchRequest.ContentType = "application/json";
                        httpWebSearchRequest.Method = "POST";
                        httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                        using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                        {
                            var test2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(esSearchrequestInc);
                            streamWriter.Write(test2);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                        string esSearchInsertionResult = string.Empty;
                        ESNewSearchInsertion esSearchInsertionResponse = null;
                        using (var streamReader = new StreamReader(httpSearchResponse.GetResponseStream()))
                        {
                            esSearchInsertionResult = streamReader.ReadToEnd();
                            try
                            {
                                esSearchInsertionResponse = JsonConvert.DeserializeObject<ESNewSearchInsertion>(esSearchInsertionResult);
                                if (esSearchInsertionResponse.created.Equals(true))
                                {

                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    });
                }
                else
                {
                    int offersInserted = TTSSandBulkInsertion(destinationIds, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, priceMin,
                                                          priceMax, tradingNameIds, destinationType, hotelKeys, tripAdvisorRating, ratings, boardType,
                                                          departureDate, labelId, skipFacets);
                    //Holidays holidays = new Holidays();
                    hkte = string.Empty;
                    hkteUPHK = string.Empty;
                    offerCount = 0;
                    if (!string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        offerCount = hotelKeys.Split(',').Length;
                    }
                    else
                    {
                        offerCount = int.Parse(ConfigurationManager.AppSettings["OfferCount"]);
                    }
                    facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
                    esFacetsQuery = Utilities.GetESFacetsQuery(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                                        departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys,
                                                                        destinationType, labelId, tripAdvisorRating, usersPreferredHotelKeys);
                    ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                    facetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                    while (facetsTrials > 0)
                    {
                        if (facetsResponse.aggregations.price_max.value != 0 && facetsResponse.aggregations.price_min.value != 0 && facetsResponse.hits.total >= offersInserted)
                        {
                            ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                            facetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                            break;
                        }
                        else
                        {
                            ESFacetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esFacetsQuery);
                            facetsResponse = JsonConvert.DeserializeObject<FacetsResponse>(ESFacetsResponse);
                            Thread.Sleep(500);
                            facetsTrials--;
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        var taNameIds = string.Empty;
                        if (!string.IsNullOrWhiteSpace(tradingNameIds))
                        {
                            taNameIds = tradingNameIds;
                        }
                        else
                        {
                            taNameIds = "576,192";
                        }
                        usersPreferredHotelKeysQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                    departureDate, hotelKeysToExclude, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                    labelId, tripAdvisorRating, usersPreferredHotelKeys, taNameIds, 1);
                        ESUPHKResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", usersPreferredHotelKeysQuery);
                        esUPHKResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESUPHKResponse);
                        if (esUPHKResponse.hits.total != 0)
                        {
                            offerCount = offerCount - esUPHKResponse.aggregations.hotel_iff.buckets.Count;
                            hkteUPHK = esUPHKResponse.aggregations.hotel_iff.buckets[0].top_price.hits.hits[0]._source.hotel.iff;
                            UserPref = true;
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                    {
                        hkte = hkteUPHK + "," + hotelKeysToExclude;
                    }
                    else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                    {
                        hkte = hotelKeysToExclude;
                    }
                    else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        hkte = hkteUPHK;
                    }
                    esQuery576 = string.Empty;
                    ESTNID576Response = string.Empty;
                    esTNID576Response = new ElasticsearchResponse();
                    if (string.IsNullOrWhiteSpace(sort) && tradingNameIds != "808" && string.IsNullOrWhiteSpace(hotelKeys))
                    {
                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                        offerCount = offerCount - esTNID576Response.aggregations.hotel_iff.buckets.Count;
                        if (string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                     select ttssResult.key).Distinct().ToList());
                        }
                        else if (!string.IsNullOrWhiteSpace(hkte) && esTNID576Response.aggregations.hotel_iff.buckets.Count != 0)
                        {
                            hkte = hkte + "," + String.Join(",", (from ttssResult in esTNID576Response.aggregations.hotel_iff.buckets
                                                                  select ttssResult.key).Distinct().ToList());
                        }
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, "", "192", offerCount);
                        trading576 = true;
                    }
                    else
                    {
                        esQuery = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                            departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                            labelId, tripAdvisorRating, "", tradingNameIds, offerCount);
                    }



                    var ESDirResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery);
                    ElasticsearchResponse esDirSerResponse = null;
                    esDirSerResponse = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESDirResponse);
                    offerCount = offerCount - esDirSerResponse.aggregations.hotel_iff.buckets.Count;
                    if (offerCount > 0 && trading576)
                    {
                        hkte = string.Empty;

                        if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        {
                            hkte = hkteUPHK + "," + hotelKeysToExclude;
                        }
                        else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        {
                            hkte = hotelKeysToExclude;
                        }
                        else if (string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                        {
                            hkte = hkteUPHK;
                        }

                        //if (!string.IsNullOrWhiteSpace(hotelKeysToExclude) && !string.IsNullOrWhiteSpace(usersPreferredHotelKeys) && !string.IsNullOrWhiteSpace(hkte))
                        //{
                        //    hkte = hkteUPHK + "," + hotelKeysToExclude;
                        //}
                        //else if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
                        //{
                        //    hkte = hotelKeysToExclude;
                        //}

                        esQuery576 = Utilities.GetESQueryV_2(departureIds, boardType, ratings, adults, children, durationMin, durationMax, destinationIds,
                                                        departureDate, hkte, sort, priceMin, priceMax, dateMin, dateMax, hotelKeys, destinationType,
                                                        labelId, tripAdvisorRating, "", "576", offerCount + tradingNameId576Count);
                        ESTNID576Response = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search?pretty", esQuery576);
                        esTNID576Response = JsonConvert.DeserializeObject<ElasticsearchResponse>(ESTNID576Response);
                    }
                    try
                    {
                        if (facetsResponse.hits.total != 0)
                        {

                            holidays.total = facetsResponse.hits.total;
                            holidays.totalHotels = facetsResponse.aggregations.hotel.buckets.Length;
                            holidays.offers = new List<Offer>();
                            ESRSource source = new ESRSource();
                            if (UserPref == true)
                            {
                                foreach (ESRBucket bucket in esUPHKResponse.aggregations.hotel_iff.buckets)
                                {
                                    Offer offr = new Offer();
                                    try
                                    {
                                        offr.id = bucket.top_price.hits.hits[0]._source.id;
                                        offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                        offr.price = bucket.top_price.hits.hits[0]._source.price;
                                        offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                        offr.isPreferredByUser = true;
                                        var phoneNumber = string.Empty;
                                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        }
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = "0123456789";
                                        }
                                        offr.phone = phoneNumber;
                                        //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                        //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                        journey = new Journey();
                                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                        journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                        journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                        journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                        offr.journey = journey;
                                        accommodation = new Accommodation();
                                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                        offr.accommodation = accommodation;
                                        offerHotel = new OfferHotel();
                                        offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                        offerHotel.mobileimages = new List<string>();
                                        offerHotel.thumbnailimages = new List<string>();
                                        offerHotel.images = new List<string>();
                                        int hoteliffCount = 0;
                                        if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                        {
                                            hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                            {
                                                offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            }
                                        }
                                        else
                                        {
                                            offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                        }
                                        taRating = new WCFRestService.Model.Artirix.Rating();
                                        taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                        taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                        taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                        offerHotel.rating = taRating;
                                        //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                        offerHotel.features = new List<object>();
                                        offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                        offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                        offr.hotel = offerHotel;
                                        offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                        offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                        offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    holidays.offers.Add(offr);
                                }
                            }
                            if (trading576 == true && string.IsNullOrWhiteSpace(sort))
                            {
                                foreach (ESRBucket bucket in esTNID576Response.aggregations.hotel_iff.buckets)
                                {
                                    Offer offr = new Offer();
                                    try
                                    {
                                        offr.id = bucket.top_price.hits.hits[0]._source.id;
                                        offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                        offr.price = bucket.top_price.hits.hits[0]._source.price;
                                        var phoneNumber = string.Empty;
                                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                        }
                                        if (string.IsNullOrWhiteSpace(phoneNumber))
                                        {
                                            phoneNumber = "0123456789";
                                        }
                                        offr.phone = phoneNumber;
                                        //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);//bucket.top_price.hits.hits[0]._source.phone;
                                        //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                        journey = new Journey();
                                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                        journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                        journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                        journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                        offr.journey = journey;
                                        accommodation = new Accommodation();
                                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                        offr.accommodation = accommodation;
                                        offerHotel = new OfferHotel();
                                        offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                        offerHotel.mobileimages = new List<string>();
                                        offerHotel.thumbnailimages = new List<string>();
                                        offerHotel.images = new List<string>();
                                        int hoteliffCount = 0;
                                        if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                        {
                                            hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                            {
                                                offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                                offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            }
                                        }
                                        else
                                        {
                                            offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                        }
                                        taRating = new WCFRestService.Model.Artirix.Rating();
                                        taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                        taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                        taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                        offerHotel.rating = taRating;
                                        //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                        offerHotel.features = new List<object>();
                                        offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                        offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                        offr.hotel = offerHotel;
                                        offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                        offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                        offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                        offr.isPreferredByUser = true;
                                        offr.isPreferential = true;
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    holidays.offers.Add(offr);
                                }
                            }
                            foreach (ESRBucket bucket in esDirSerResponse.aggregations.hotel_iff.buckets)
                            {
                                Offer offr = new Offer();
                                try
                                {
                                    offr.id = bucket.top_price.hits.hits[0]._source.id;
                                    offr.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                                    offr.price = bucket.top_price.hits.hits[0]._source.price;
                                    offr.isPreferential = bucket.top_price.hits.hits[0]._source.isPreferential;
                                    var phoneNumber = string.Empty;
                                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = GetTelephoneNumberByLabelID(int.Parse(destinationIds), bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    }
                                    if (string.IsNullOrWhiteSpace(phoneNumber))
                                    {
                                        phoneNumber = "0123456789";
                                    }
                                    offr.phone = phoneNumber;
                                    //offr.phone = bucket.top_price.hits.hits[0]._source.phone;
                                    //offr.phone = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId.ToString(), bucket.top_price.hits.hits[0]._source.price);
                                    journey = new Journey();
                                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                                    journey.flightOperator = bucket.top_price.hits.hits[0]._source.journey.flightOperator;
                                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                                    journey.inboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDirect;
                                    journey.outboundFlightDirect = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDirect;
                                    offr.journey = journey;
                                    accommodation = new Accommodation();
                                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                                    offr.accommodation = accommodation;
                                    offerHotel = new OfferHotel();
                                    offerHotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff;
                                    offerHotel.mobileimages = new List<string>();
                                    offerHotel.thumbnailimages = new List<string>();
                                    offerHotel.images = new List<string>();
                                    int hoteliffCount = 0;
                                    if (Global.mhidtoImageCountList.ContainsKey(offerHotel.iff))
                                    {
                                        hoteliffCount = Global.mhidtoImageCountList[offerHotel.iff];
                                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                        {
                                            offerHotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                            offerHotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + offerHotel.iff + "/" + hotelIndex + ".jpg"));
                                        }
                                    }
                                    else
                                    {
                                        offerHotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                    }
                                    taRating = new WCFRestService.Model.Artirix.Rating();
                                    taRating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                                    taRating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                                    taRating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                                    offerHotel.rating = taRating;
                                    //offerHotel.description = bucket.top_price.hits.hits[0]._source.hotel.description;
                                    offerHotel.features = new List<object>();
                                    offerHotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                                    offerHotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                                    offr.hotel = offerHotel;
                                    offr.hotelOperator = bucket.top_price.hits.hits[0]._source.hotelOperator;
                                    offr.contentId = bucket.top_price.hits.hits[0]._source.contentId;
                                    offr.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId.ToString();
                                }
                                catch (Exception ex)
                                {

                                }
                                holidays.offers.Add(offr);
                            }
                            holidays.facets = new Facets();
                            holidays.facets.priceRange = new Pricerange();
                            holidays.facets.priceRange.max = Convert.ToInt32(facetsResponse.aggregations.price_max.value);
                            holidays.facets.priceRange.min = Convert.ToInt32(facetsResponse.aggregations.price_min.value);
                            holidays.facets.airports = new List<WCFRestService.Model.Artirix.Airport>();
                            foreach (FacetsAirportsBucket airportBucket in facetsResponse.aggregations.airports.buckets)
                            {
                                try
                                {
                                    WCFRestService.Model.Artirix.Airport airport = new WCFRestService.Model.Artirix.Airport();
                                    airport.count = airportBucket.doc_count;
                                    airport.departureId = airportBucket.key;
                                    airport.name = airportCodes[airportBucket.key.ToString()];
                                    holidays.facets.airports.Add(airport);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            holidays.facets.hotels = new List<FacetHotel>();
                            foreach (FacetsHotelBucket hotelBucket in facetsResponse.aggregations.hotel.buckets)
                            {
                                FacetHotel facetHotel = new FacetHotel();
                                facetHotel.count = hotelBucket.doc_count;
                                facetHotel.hotelKey = hotelBucket.key.ToString();
                                if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                                {
                                    facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                                    if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                                    {
                                        facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];

                                    }
                                    holidays.facets.hotels.Add(facetHotel);
                                }

                            }
                            holidays.facets.resorts = new List<Resort>();
                            foreach (FacetsResortsBucket resortBucket in facetsResponse.aggregations.resorts.buckets)
                            {
                                Resort resort = new Resort();
                                resort.count = resortBucket.NoOfHotels.value;
                                resort.destinationId = Convert.ToInt32(resortBucket.key);
                                if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                                {
                                    resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                                    holidays.facets.resorts.Add(resort);
                                }
                            }
                            holidays.facets.totalHotels = facetsResponse.aggregations.hotel.buckets.Count();
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            catch (Exception ex)
            {

            }
            //holidays.offers.OrderBy(t => t.tradingNameId).ThenByDescending(o => o.price);
            return holidays;
        }

        public static int DiversitytoTTSS(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                           int children, string ratings, string hotelKeys, string tradingNameId)
        {
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            string diversityTTSSUrl = string.Empty;
            string paramsString = Utilities.GetDiversityTTSSUrl(destinationId, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, ratings, hotelKeys, tradingNameId);
            diversityTTSSUrl = ConfigurationManager.AppSettings["TTSSURL"] + "searchId=2&userName=TTHOD&" + paramsString;
            ttssResponseStream ttssRes = new ttssResponseStream();
            ttssRes = Utilities.ExecuteTTSSGetWebRequest(diversityTTSSUrl);
            List<string> distinctHotelKeys = null;
            SearchResponse serResponse = new SearchResponse();
            serResponse.responseId = DateTime.Now.Ticks;
            Dictionary<int, MHIDStaticInfo> staticInfo = new Dictionary<int, MHIDStaticInfo>();
            List<string> redisStaticInfo = null;
            string test = string.Empty;
            string bulk = string.Empty;
            Dictionary<string, int> iffvsMasterIdMapping = Global.iffvsMasterIdMapping;
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            ESOffer offer;
            ESJourney journey;
            ESAccommodation accommodation;
            ESOfferHotel hotel;
            ESRating taRating;
            DateTime timeStamp = DateTime.UtcNow;
            string result = string.Empty;
            string combo = string.Empty;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            int offersDecorated = 0;
            int offersInserted = 0;
            XmlDocument docResponse = new XmlDocument();
            docResponse.LoadXml(ttssRes.ttssResponse);
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Container));
                Container mytest = (Container)serializer.Deserialize(new StringReader(ttssRes.ttssResponse));
                MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
                distinctHotelKeys = (from ttssResult in mytest.Results.Result select ttssResult.HotelKey).Distinct().ToList();
                using (ServiceStack.Redis.PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(mhidStaticInfo._source.MasterHotelId))
                            {
                                staticInfo.Add(mhidStaticInfo._source.MasterHotelId, mhidStaticInfo);
                            }
                        }
                    }
                }
                Parallel.ForEach(mytest.Results.Result, parrOptions, results =>
                //foreach (var results in mytest.Results.Result)
                {
                    var staticData = String.Empty;
                    MHIDStaticInfo redisStaticData = null;
                    offer = new ESOffer();
                    try
                    {
                        if (staticInfo.ContainsKey(int.Parse(results.HotelKey)))
                        {
                            redisStaticData = staticInfo[int.Parse(results.HotelKey)];
                        }
                        else
                        {
                            staticData = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["StaticData"] + results.HotelKey);
                            redisStaticData = JsonConvert.DeserializeObject<MHIDStaticInfo>(staticData);
                            if (!staticInfo.ContainsKey(int.Parse(results.HotelKey)) && redisStaticData != null)
                            {
                                staticInfo.Add(int.Parse(results.HotelKey), redisStaticData);
                            }
                        }
                        if (redisStaticData != null)
                        {
                            serResponse.ttssCount = int.Parse(mytest.Results.Count);
                            offer.airportCode = reverseAirportCodes[results.Departure];
                            if (departureIds < 0)
                            {
                                offer.airportCollectionId = departureIds;
                            }
                            offer.id = results.Id;
                            offer.quoteRef = results.QuoteRef;
                            offer.price = int.Parse(results.Price);
                            offer.labelId = destinationId;
                            offer.regionId = int.Parse(results.DestinationId);
                            offer.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                            offer.regionName = redisStaticData._source.Region.Regname;
                            offer.responseId = serResponse.responseId;
                            offer.resortId = redisStaticData._source.Region.ResortId;
                            if (results.TradingNameId == "576")
                            {
                                offer.isPreferential = true;
                                offer.isPreferredByUser = true;
                            }
                            else
                            {
                                offer.isPreferential = false;
                                offer.isPreferredByUser = false;
                            }
                            offer.phone = results.Phone;
                            journey = new ESJourney();
                            journey.departure = results.Departure;
                            journey.outboundArrivalDate = results.OutboundArrivalDate;
                            journey.inboundDepartureDate = results.InboundDepartureDate;
                            journey.departureDate = results.DepartureDate;
                            journey.returnDate = results.ReturnDate;
                            journey.duration = int.Parse(results.Duration);
                            journey.gatewayName = results.GatewayName;
                            journey.gatewayCode = results.GatewayCode;
                            journey.flightOperator = results.FlightOperator;
                            journey.destination = results.Destination;
                            journey.glat = results.Glat;
                            journey.glong = results.Glong;
                            journey.parentRegion = redisStaticData._source.Region.ParentRegionName;
                            journey.parentRegionId = redisStaticData._source.Region.ParentRegionId;
                            journey.inboundFlightDirect = results.InboundFlightDirect == "true";
                            journey.outboundFlightDirect = results.OutboundFlightDirect == "true";
                            offer.journey = journey;
                            accommodation = new ESAccommodation();
                            accommodation.boardTypeCode = results.BoardTypeCode;
                            accommodation.boardTypeId = int.Parse(results.BoardTypeId);
                            accommodation.adults = int.Parse(results.Adults);
                            accommodation.children = int.Parse(results.Children);
                            accommodation.rating = (int)Convert.ToDouble(results.Rating);
                            accommodation.updated = results.Updated;
                            offer.accommodation = accommodation;
                            hotel = new ESOfferHotel();
                            hotel.iff = int.Parse(results.HotelKey);
                            hotel.name = redisStaticData._source.BuildingName;
                            hotel.description = null;
                            hotel.features = redisStaticData._source.Features;
                            hotel.starRating = redisStaticData._source.Rating.ToString();
                            hotel.images = null;
                            hotel.mobileimages = null;
                            hotel.latlong = results.Glat + "," + results.Glong;
                            taRating = new ESRating();
                            if (redisStaticData.TripAdvisor.TAID != 0)
                            {
                                taRating.tripAdvisorId = redisStaticData.TripAdvisor.TAID.ToString();
                            }
                            if (redisStaticData.TripAdvisor.reviewsCount != 0)
                            {
                                taRating.reviewCount = redisStaticData.TripAdvisor.reviewsCount;
                            }
                            if (redisStaticData.TripAdvisor.TARating != 0)
                            {
                                taRating.averageRating = (int)((float)redisStaticData.TripAdvisor.TARating * 10);
                            }
                            hotel.rating = taRating;
                            offer.hotel = hotel;
                            offer.hotelOperator = results.HotelOperator;
                            offer.contentId = results.ContentId;
                            offer.tradingNameId = int.Parse(results.TradingNameId);
                            test = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offer);
                            offersInserted++;
                            combo = "{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["ResultsIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\",\"_id\":\"" + results.QuoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + test;
                            searchParams.AppendLine(combo);
                        }
                        else
                        {

                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
               );
            }
            catch (Exception ex)
            {

            }
            try
            {
                bulk = searchParams.ToString();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                //request.ContentType = "application/json";
                request.Method = "POST";
                request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    //var test1 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offerlist);
                    streamWriter.Write(bulk);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                //httpWebRequest.Credentials = new NetworkCredential("nithin", "nithin");
                //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{
                //    //var test1 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offerlist);
                //    streamWriter.Write(bulk);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}
                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    result = streamReader.ReadToEnd();
                //}
            }
            catch (Exception ex)
            {

            }
            Thread.Sleep(2 * serResponse.ttssCount);
            Task.Factory.StartNew(() =>
            {
                ESDiversitySearchRequest esSearchrequest = new ESDiversitySearchRequest();
                esSearchrequest.adults = adults;
                esSearchrequest.children = children;
                esSearchrequest.dateMax = dateMax + "T23:59:59";
                esSearchrequest.dateMin = dateMin + "T00:00:00";
                esSearchrequest.departureIds = departureIds;
                esSearchrequest.destinationIds = destinationId.ToString();
                esSearchrequest.durationMin = durationMin;
                esSearchrequest.durationMax = durationMax;
                esSearchrequest.responseId = serResponse.responseId;
                esSearchrequest.ttssCount = serResponse.ttssCount;
                esSearchrequest.ttssResponseTime = ttssRes.ttssResponseTime;
                if (offersInserted > 0)
                {
                    esSearchrequest.isCahceHit = false;
                }
                else
                {
                    esSearchrequest.isCahceHit = true;
                }
                esSearchrequest.ttssURL = diversityTTSSUrl;
                esSearchrequest.ratings = ratings.Split(',').ToList();
                esSearchrequest.timeStamp = timeStamp.ToString("yyyy-MM-ddTHH:mm:ss");
                esSearchrequest.offersFound = distinctHotelKeys.Count;
                esSearchrequest.offersDecorated = staticInfo.Count;
                esSearchrequest.offersInserted = offersInserted;
                esSearchrequest.hotelKeys = hotelKeys.Split(',').ToList();
                if (string.IsNullOrWhiteSpace(tradingNameId))
                {
                    var trNIds = "576,192";
                    esSearchrequest.tradingNameId = trNIds.Split(',').ToList();
                }
                else
                {
                    esSearchrequest.tradingNameId = tradingNameId.Split(',').ToList();
                }
                var httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["DiversityIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"]);
                httpWebSearchRequest.ContentType = "application/json";
                httpWebSearchRequest.Method = "POST";
                httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                {
                    var test2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(esSearchrequest);
                    streamWriter.Write(test2);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                string esSearchInsertionResult = string.Empty;
                ESNewSearchInsertion esSearchInsertionResponse = null;
                using (var streamReader = new StreamReader(httpSearchResponse.GetResponseStream()))
                {
                    esSearchInsertionResult = streamReader.ReadToEnd();
                    try
                    {
                        esSearchInsertionResponse = JsonConvert.DeserializeObject<ESNewSearchInsertion>(esSearchInsertionResult);
                        if (esSearchInsertionResponse.created.Equals(true))
                        {
                            //responseId = esSearchrequest.timeStamp;
                            //srcResponse.responseId = esSearchInsertionResponse._id;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            });
            return offersInserted;
        }


        private static string GetTelephoneNumber(int regionId, string tradingNameId, int price)
        {
            try
            {
                var phoneNumbers = Global.DestinationTelePhoneNumbers[tradingNameId + '|' + regionId.ToString()];
                var telePhoneSets = Global.TelePhoneSets;
                var random = new Random();
                string telephoneNumber = string.Empty;

                if (phoneNumbers != null && phoneNumbers.Count() > 0)
                {
                    if (price <= phoneNumbers[0].Price)
                    {
                        int index = random.Next(telePhoneSets[phoneNumbers[0].TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers[0].TelephoneSet].Number[index];
                    }

                    for (int index = 0; index < phoneNumbers.Count(); index++)
                    {
                        if (phoneNumbers[index].Price <= price && price < phoneNumbers[index + 1].Price)
                        {
                            int numberCount = random.Next(telePhoneSets[phoneNumbers[index].TelephoneSet].Number.Count);
                            return telePhoneSets[phoneNumbers[index].TelephoneSet].Number[numberCount];
                        }
                    }

                    if (price >= phoneNumbers.Last().Price)
                    {
                        int numberCount = random.Next(telePhoneSets[phoneNumbers.Last().TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers.Last().TelephoneSet].Number[numberCount];
                    }
                }

                return telephoneNumber;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private static string GetTelephoneNumberByLabelID(int labelId, string tradingNameId, int price)
        {
            XmlDocument xml = new XmlDocument();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            xml.Load(fileName);
            XmlNodeList regionList = xml.SelectNodes(string.Format("/Container/HolidaysData/Label[@Id='{0}']/Region", labelId));
            string regionId = string.Empty;
            string telephoneNumber = string.Empty;

            if (regionList != null && regionList.Count > 0)
            {
                foreach (XmlNode xn in regionList)
                {
                    regionId = xn.Attributes["Id"].Value;
                    telephoneNumber = GetTelephoneNumber(int.Parse(regionId), tradingNameId, price);

                    if (!string.IsNullOrEmpty(telephoneNumber))
                    {
                        break;
                    }
                }
            }

            return telephoneNumber;
        }

    }

}