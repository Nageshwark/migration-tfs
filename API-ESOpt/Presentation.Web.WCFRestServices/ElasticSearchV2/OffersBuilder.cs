﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web;
using static Presentation.WCFRestService.Model.ElasticSearchV2.FacetsResponseParams;
using static Presentation.WCFRestService.Model.ElasticSearchV2.OffersResponseParams;
using System.Xml;
using static Presentation.WCFRestService.Model.ElasticSearchV2.SearchResponseParams;
using static Presentation.WCFRestService.Model.ElasticSearchV2.DiversityResponseParams;
using Presentation.WCFRestService.Model.Misc;
using System.Text;
using ServiceStack.Redis;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class OffersBuilder
    {
        public static HolidayOffers GenerateOffers(SearchRequestAvailability searchRequestAvailability, APISearchRequest apiSearchRequest, StringBuilder errorLogger)
        {
            HolidayOffers holidayOffers = new HolidayOffers();
            Dictionary<string, string> airportCodes = Global.airportCodes;
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();

            string imgSourceURLFormat = Global.imgSourceURLFormat;
            string esResultsSearch = ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search";
            bool userPrefferedHotelKeys = false;
            bool tradingNameId576Offers = false;
            bool isTradingNameId576OffersAvailable = false;
            bool isFacetsCallRequired = false;
            bool isPersonalizationSearchRequired = false;
            List<string> personalizedParms = new List<string>();
            bool isRecentSearchCookie = false;
            bool isFavoriteCookie = false;
            int thresholdOfferLimit = int.Parse(ConfigurationManager.AppSettings["ThresholdOfferLimit"]);
            if (apiSearchRequest.boardType.Count == 1 && apiSearchRequest.boardType.Contains("5") )
            {
                thresholdOfferLimit = int.Parse(ConfigurationManager.AppSettings["AIThresholdOfferLimit"]);
            }
            if(apiSearchRequest.hotelKeys.Count !=0)
            {
                thresholdOfferLimit = apiSearchRequest.hotelKeys.Count;
            }

            int offerCountConfigValue = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["OfferCount"], out offerCountConfigValue);

            int tradingNameId576OffersCount = offerCountConfigValue;

            string tradingNameIdOffers = string.Empty;

            List<int> hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude;

            int tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);

            int offerCount = 0;
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                offerCount = apiSearchRequest.hotelKeys.Count;
            }
            else
            {
                offerCount = offerCountConfigValue;
            }
            #region TTSS_Criteria
            string facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
            string facetsResponse = string.Empty;

            if (searchRequestAvailability.ttssCount > 0)
            {
                try
                {
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);

                    if (searchRequestAvailability.ttssCount >= int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"]) && facetsRequestResponse.aggregations.hotel.buckets.Count <= thresholdOfferLimit)
                    {
                        ttssResultsParams = TTSSExecution.HitTTSSToGetOffers(apiSearchRequest, errorLogger);
                        isFacetsCallRequired = true;
                    }
                }
                catch (Exception ex)
                {
                    //ErrorLogger.Log(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }

            }
            else
            {
                ttssResultsParams = TTSSExecution.HitTTSSToGetOffers(apiSearchRequest, errorLogger);
                isFacetsCallRequired = true;
            }
            #endregion
            holidayOffers.offers = new List<HOffer>();
            #region Personalization_Criteria

            if(apiSearchRequest.sort != null && apiSearchRequest.sort.StartsWith("pers"))
            {
                if ((apiSearchRequest.personalizedParms.Count != 0))
                {
                    isPersonalizationSearchRequired = true;
                    personalizedParms = apiSearchRequest.personalizedParms;
                }
                else if (!string.IsNullOrWhiteSpace(apiSearchRequest.recentSearchCookie) && isPersonalizationSearchRequired == false)
                {
                    var recentSearches = apiSearchRequest.recentSearchCookie.Split('~');
                    foreach(var search in recentSearches)
                    {
                        var searchParms = search.Split('_');
                        if(!string.IsNullOrWhiteSpace(searchParms[13]) && searchParms[13] != "null" && searchParms[13].Split(',').Length !=0)
                        {
                            isRecentSearchCookie = true;
                           
                                var sParams = searchParms[13].Split(',');
                                for (int temp = 0; temp < sParams.Length; temp++)
                                {
                                    personalizedParms.Add(sParams[temp]);
                                }
                            
                            isPersonalizationSearchRequired = true;
                            break;
                        }
                    }
                }
                
                else if (!string.IsNullOrWhiteSpace(apiSearchRequest.favoriteCookie) &&  isPersonalizationSearchRequired == false)
                {
                    var favoriteCookieSearch = apiSearchRequest.favoriteCookie.Split('~');
                    foreach (var search in favoriteCookieSearch)
                    {
                        var searchParms = search.Split('_');
                        if (!string.IsNullOrWhiteSpace(searchParms[10]) && searchParms[10] != null)
                        {
                            personalizedParms.Add(searchParms[10]);
                            isFavoriteCookie = true;
                            isPersonalizationSearchRequired = true;
                            break;
                        }
                    }
                }
                else
                {
                    isPersonalizationSearchRequired = false;
                }

            }
            #endregion

            if (isPersonalizationSearchRequired)
            {
                int personalizationOfferCount = 20;
                List<int> personalizationOffersKeys = new List<int>();
                List<int> personalizationOffersKeysToExclude = new List<int>();
                bool isPersonalization = true;
                Dictionary<int, double> cosineValues = new Dictionary<int, double>();
                Dictionary<int, List<int>> imageGallery = new Dictionary<int, List<int>>();
                List<string> redisStaticInfo = null;
                StaticPersonalResponseParms staticResponseParms = new StaticPersonalResponseParms();
                if ((apiSearchRequest.personalizedParms.Count != 0))
                {
                    cosineValues = PersonalizedSearch.PersonalizedResponseParms(apiSearchRequest);
                }
                else if (!string.IsNullOrWhiteSpace(apiSearchRequest.recentSearchCookie) && isRecentSearchCookie)
                {
                    cosineValues = PersonalizedSearch.RecentSearch(apiSearchRequest);
                }
                else if (!string.IsNullOrWhiteSpace(apiSearchRequest.favoriteCookie) && isFavoriteCookie)
                {
                    cosineValues = PersonalizedSearch.FavoriteCookie(apiSearchRequest);
                }
                else
                {
                    isPersonalization = false;
                }

                if(isPersonalization)
                {
                    personalizationOfferCount = cosineValues.Keys.Count;
                    personalizationOffersKeys = cosineValues.Keys.ToList();
                    personalizationOffersKeysToExclude = null;
                    //imageGallery = PersonalizedSearch.GetImages(personalizationOffersKeys);
                }
                else
                {
                    personalizationOffersKeysToExclude = apiSearchRequest.hotelKeysToExclude;
                }
                string personalizedQuery = QueryBuilder.GetPersonalizedQuery(apiSearchRequest, personalizationOfferCount, "192,576", personalizationOffersKeysToExclude, false, personalizationOffersKeys);
                Dictionary<int, StaticPersonalResponseParms> staticInfo = new Dictionary<int, StaticPersonalResponseParms>();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForPMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(cosineValues.Keys.ToList().ConvertAll<string>(x => x.ToString()));
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticPersonalResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }
                string personalizedOfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, personalizedQuery);
                OffersResponse personalizedOffers = JsonConvert.DeserializeObject<OffersResponse>(personalizedOfferQueryResponse);
                foreach (ORPHotelBucket bucket in personalizedOffers.aggregations.hotel_iff.buckets)
                {
                    HOffer offer = new HOffer();
                    offer.id = bucket.top_price.hits.hits[0]._source.id;
                    offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                    offer.price = bucket.top_price.hits.hits[0]._source.price;
                    offer.relavanceScore = cosineValues[bucket.top_price.hits.hits[0]._source.hotel.iff];
                    string phoneNumber = string.Empty;
                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                    }
                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        phoneNumber = "0123456789";
                    }
                    offer.phone = phoneNumber;
                    Journey journey = new Journey();
                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                    offer.journey = journey;
                    Accommodation accommodation = new Accommodation();
                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                    offer.accommodation = accommodation;
                    OfferHotel hotel = new OfferHotel();
                    hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                    hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                    if (staticInfo[bucket.top_price.hits.hits[0]._source.hotel.iff].tatagnames != null)
                    {
                        hotel.tags = staticInfo[bucket.top_price.hits.hits[0]._source.hotel.iff].tatagnames;
                    }
                    hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                    WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                    rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                    rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                    rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                    hotel.rating = rating;

                    hotel.mobileimages = new List<string>();
                    hotel.thumbnailimages = new List<string>();
                    hotel.images = new List<string>();
                    int hoteliffCount = 0;
                    var tempJson = "{\"data\":[\"" + string.Join("\",\"", personalizedParms) + "\"]}";                
                    string tempUrl = ConfigurationManager.AppSettings["ImagePersonalizedUrl"] + hotel.iff;
                    string imageResponse = Utilities.ExecuteImagesPostJsonWebRequest(tempUrl, tempJson);
                    if (string.IsNullOrWhiteSpace(imageResponse))
                    {
                        if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                        {
                            hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                            if (hoteliffCount == 0)
                            {
                                hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                            }
                            else
                            {
                                //hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/4.jpg"));
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                        }
                        else
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }
                    }
                    else
                    {
                        var imageIndex = imageResponse.Split(',');
                        for (int hotelIndex = 0; hotelIndex < imageIndex.Length; hotelIndex++)
                        {
                            hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + imageIndex[hotelIndex] + ".jpg"));
                            hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + imageIndex[hotelIndex] + ".jpg"));
                            hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + hotel.iff + "/" + imageIndex[hotelIndex] + ".jpg"));
                        }
                    }
                    offer.hotel = hotel;
                    offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                    offer.isPreferredByUser = false;
                    if (offer.tradingNameId == "576")
                    {
                        offer.isPreferential = true;
                    }
                    else
                    {
                        offer.isPreferential = false;
                    }
                    holidayOffers.offers.Add(offer);
                }
                if(holidayOffers.offers.Count == 0)
                {
                    isPersonalizationSearchRequired = false;
                }
                //}
            }
            else
            {
                if(apiSearchRequest.sort != null && apiSearchRequest.sort.StartsWith("pers"))
                {
                    apiSearchRequest.sort = string.Empty;
                }
                #region Offers_Logic
                OffersResponse userPrefferedHotelKeyOffer = new OffersResponse();
                OffersResponse tradingNameId576Offer = new OffersResponse();
                OffersResponse genericOffers = new OffersResponse();
                if (apiSearchRequest.usersPreferredHotelKeys.Count != 0)
                {
                    tradingNameIdOffers = string.Join(",", apiSearchRequest.tradingNameIds.Select(n => n.ToString()).ToArray());
                    string userPrefferedHotelKeyQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, 1, tradingNameIdOffers, hotelKeysToExclude, true);
                    string userPrefferedHotelKeyQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, userPrefferedHotelKeyQuery);
                    userPrefferedHotelKeyOffer = JsonConvert.DeserializeObject<OffersResponse>(userPrefferedHotelKeyQueryResponse);
                    if (userPrefferedHotelKeyOffer.aggregations.hotel_iff.buckets.Count != 0)
                    {
                        offerCount = offerCount - 1;
                        tradingNameId576OffersCount = tradingNameId576OffersCount - 1;
                        userPrefferedHotelKeys = true;
                        hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude.Concat(apiSearchRequest.usersPreferredHotelKeys).ToList();
                    }
                }
                if (string.IsNullOrWhiteSpace(apiSearchRequest.sort) && !apiSearchRequest.tradingNameIds.Contains<string>("808") && apiSearchRequest.hotelKeys.Count == 0)
                {
                    string tradingNameId576OfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCountConfigValue, "576", hotelKeysToExclude, false);
                    string tradingNameId576OfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, tradingNameId576OfferQuery);
                    tradingNameId576Offer = JsonConvert.DeserializeObject<OffersResponse>(tradingNameId576OfferQueryResponse);
                    if (tradingNameId576Offer.aggregations.hotel_iff.buckets.Count > 0)
                    {
                        offerCount = offerCount - Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                        tradingNameId576OffersCount = tradingNameId576OffersCount - Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                        tradingNameId576Offers = true;
                        if (tradingNameId576Offer.aggregations.hotel_iff.buckets.Count > tradingNameId576Count)
                        {
                            isTradingNameId576OffersAvailable = true;
                        }
                    }

                    string genericOfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCount, "192", hotelKeysToExclude, false);
                    string genericOfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, genericOfferQuery);
                    genericOffers = JsonConvert.DeserializeObject<OffersResponse>(genericOfferQueryResponse);
                    if (genericOffers.aggregations.hotel_iff.buckets.Count < offerCount)
                    {
                        offerCount = offerCount - genericOffers.aggregations.hotel_iff.buckets.Count;
                        if (isTradingNameId576OffersAvailable)
                        {
                            tradingNameId576OffersCount = Math.Min(offerCount + tradingNameId576Count, tradingNameId576OffersCount + tradingNameId576Count);
                        }
                    }
                    else
                    {
                        tradingNameId576OffersCount = Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                    }
                    //make both 576 and 192 queries
                }
                else
                {
                    tradingNameIdOffers = string.Join(",", apiSearchRequest.tradingNameIds.Select(n => n.ToString()).ToArray());
                    string genericOfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCount, tradingNameIdOffers, hotelKeysToExclude, false);
                    string genericOfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, genericOfferQuery);
                    genericOffers = JsonConvert.DeserializeObject<OffersResponse>(genericOfferQueryResponse);
                    //prepare a generic query
                }
                #endregion

                #region UserPrefferedHotelKeyOffers
                if (userPrefferedHotelKeys)
                {
                    foreach (ORPHotelBucket bucket in userPrefferedHotelKeyOffer.aggregations.hotel_iff.buckets)
                    {
                        HOffer offer = new HOffer();
                        offer.id = bucket.top_price.hits.hits[0]._source.id;
                        offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                        offer.price = bucket.top_price.hits.hits[0]._source.price;
                        string phoneNumber = string.Empty;
                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                        }
                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = "0123456789";
                        }
                        offer.phone = phoneNumber;
                        Journey journey = new Journey();
                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                        offer.journey = journey;
                        Accommodation accommodation = new Accommodation();
                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                        offer.accommodation = accommodation;
                        OfferHotel hotel = new OfferHotel();
                        hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                        hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                        hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                        WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                        rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                        rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                        rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                        hotel.rating = rating;
                        hotel.mobileimages = new List<string>();
                        hotel.thumbnailimages = new List<string>();
                        hotel.images = new List<string>();
                        int hoteliffCount = 0;
                        if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                        {
                            hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                            if (hoteliffCount == 0)
                            {
                                hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                            }
                            else
                            {
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                        }
                        else
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                        }
                        offer.hotel = hotel;
                        offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                        offer.isPreferredByUser = true;
                        if (offer.tradingNameId == "576")
                        {
                            offer.isPreferential = true;
                        }
                        else
                        {
                            offer.isPreferential = false;
                        }
                        holidayOffers.offers.Add(offer);
                    }

                }
                #endregion

                #region TradingNameId576Offers            
                if (tradingNameId576Offers)
                {
                    List<ORPHotelBucket> limitedList = tradingNameId576Offer.aggregations.hotel_iff.buckets.Take(tradingNameId576OffersCount).ToList();

                    foreach (ORPHotelBucket bucket in limitedList)
                    {
                        HOffer offer = new HOffer();
                        offer.id = bucket.top_price.hits.hits[0]._source.id;
                        offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                        offer.price = bucket.top_price.hits.hits[0]._source.price;
                        string phoneNumber = string.Empty;
                        phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                        }
                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = "0123456789";
                        }
                        offer.phone = phoneNumber;
                        Journey journey = new Journey();
                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                        offer.journey = journey;
                        Accommodation accommodation = new Accommodation();
                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                        offer.accommodation = accommodation;
                        OfferHotel hotel = new OfferHotel();
                        hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                        hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                        hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                        WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                        rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                        rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                        rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                        hotel.rating = rating;
                        hotel.mobileimages = new List<string>();
                        hotel.thumbnailimages = new List<string>();
                        hotel.images = new List<string>();
                        int hoteliffCount = 0;
                        if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                        {
                            hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                            if (hoteliffCount == 0)
                            {
                                hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                            }
                            else
                            {
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                        }
                        else
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }
                        offer.hotel = hotel;
                        offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                        offer.isPreferential = true;
                        offer.isPreferredByUser = false;
                        holidayOffers.offers.Add(offer);

                    }
                }

                #endregion

                #region GenericOffers
                foreach (ORPHotelBucket bucket in genericOffers.aggregations.hotel_iff.buckets)
                {
                    HOffer offer = new HOffer();
                    offer.id = bucket.top_price.hits.hits[0]._source.id;
                    offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                    offer.price = bucket.top_price.hits.hits[0]._source.price;
                    string phoneNumber = string.Empty;
                    phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString(), bucket.top_price.hits.hits[0]._source.price);
                    }
                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        phoneNumber = "0123456789";
                    }
                    offer.phone = phoneNumber;
                    Journey journey = new Journey();
                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.departureDate;
                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.returnDate;
                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.departure;
                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.gatewayCode;
                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.gatewayName;
                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;
                    offer.journey = journey;
                    Accommodation accommodation = new Accommodation();
                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                    offer.accommodation = accommodation;
                    OfferHotel hotel = new OfferHotel();
                    hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                    hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                    hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                    WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                    rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                    rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                    rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                    hotel.rating = rating;
                    hotel.mobileimages = new List<string>();
                    hotel.thumbnailimages = new List<string>();
                    hotel.images = new List<string>();
                    int hoteliffCount = 0;
                    if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                    {
                        hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                        if (hoteliffCount == 0)
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }
                        else
                        {
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                            }
                        }
                    }
                    else
                    {
                        hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                        hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                    }
                    offer.hotel = hotel;
                    offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                    offer.isPreferredByUser = false;
                    if (offer.tradingNameId == "576")
                    {
                        offer.isPreferential = true;
                    }
                    else
                    {
                        offer.isPreferential = false;
                    }
                    holidayOffers.offers.Add(offer);
                }
                #endregion
            }

            #region Facets
            if (isFacetsCallRequired)
            {
                facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
                facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", facetsQuery);
                facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
            }
            holidayOffers.totalHotels = facetsRequestResponse.aggregations.hotel.buckets.Count;
            holidayOffers.facets = new Facets();
            holidayOffers.facets.priceRange = new Pricerange();
            holidayOffers.facets.priceRange.min = Convert.ToInt32(facetsRequestResponse.aggregations.price_min.value);
            holidayOffers.facets.priceRange.max = Convert.ToInt32(facetsRequestResponse.aggregations.price_max.value);
            holidayOffers.facets.airports = new List<Airport>();
            foreach (FRRAirportBucket airportBucket in facetsRequestResponse.aggregations.airports.buckets)
            {
                try
                {
                    Airport airport = new Airport();
                    airport.count = airportBucket.NoOfAirports.value;
                    airport.departureId = airportBucket.key;
                    airport.name = airportCodes[airportBucket.key.ToString()];
                    holidayOffers.facets.airports.Add(airport);
                }
                catch (Exception ex)
                {

                }
            }
            holidayOffers.facets.hotels = new List<FacetHotel>();
            foreach (FRRHotelBucket hotelBucket in facetsRequestResponse.aggregations.hotel.buckets)
            {
                FacetHotel facetHotel = new FacetHotel();
                facetHotel.count = hotelBucket.NoOfHotels.value;
                facetHotel.hotelKey = hotelBucket.key.ToString();
                if (Global.MHIDtoMHNMap.ContainsKey(hotelBucket.key.ToString()))
                {
                    facetHotel.name = Global.MHIDtoMHNMap[hotelBucket.key.ToString()];
                    if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                    {
                        facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];
                    }
                    holidayOffers.facets.hotels.Add(facetHotel);
                }
            }
            holidayOffers.facets.resorts = new List<Resort>();
            foreach (FRRResortsBucket resortBucket in facetsRequestResponse.aggregations.resorts.buckets)
            {
                Resort resort = new Resort();
                resort.count = resortBucket.NoOfResorts.value;
                resort.destinationId = Convert.ToInt32(resortBucket.key);
                if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                {
                    resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                    holidayOffers.facets.resorts.Add(resort);
                }
            }
            string searchUrl = QueryBuilder.GetSearchURL(apiSearchRequest);
            #endregion

            #region  Async_call_to_insert_in_request_index
            Task.Factory.StartNew(() =>
            {
                SearchRequestParams searchRequestParams = new SearchRequestParams();
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                Dictionary<int, string> childAirportCodes = Global.childAirportCodes;
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                searchRequestParams.offersReturned = holidayOffers.offers.Count;
                searchRequestParams.searchURL = searchUrl;
                searchRequestParams.adults = apiSearchRequest.adults;
                searchRequestParams.boardType = apiSearchRequest.boardType;
                searchRequestParams.children = apiSearchRequest.children;
                searchRequestParams.departureDate = apiSearchRequest.departureDate.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.dateMin = apiSearchRequest.dateMin.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.dateMax = apiSearchRequest.dateMax.ToString("yyyy-MM-ddT23:59:59");
                searchRequestParams.durationMin = apiSearchRequest.durationMin;
                searchRequestParams.durationMax = apiSearchRequest.durationMax;
                searchRequestParams.departureIds = apiSearchRequest.departureIds;
                searchRequestParams.airportIds = new List<string>();
                if (apiSearchRequest.departureIds < 0)
                {
                    searchRequestParams.airportIds = childAirportCodes[apiSearchRequest.departureIds].Split(',').ToList();
                    searchRequestParams.parentDepartureIds = apiSearchRequest.departureIds;
                }
                else
                {
                    searchRequestParams.parentDepartureIds = parentAirportCodes[apiSearchRequest.departureIds];
                    searchRequestParams.airportIds.Add(apiSearchRequest.departureIds.ToString());
                }
                searchRequestParams.priceMax = apiSearchRequest.priceMax;
                searchRequestParams.priceMin = apiSearchRequest.priceMin;
                searchRequestParams.ttssResponseTime = ttssResultsParams.ttssResponseTime;
                searchRequestParams.regionIds = new List<string>();
                if (!string.IsNullOrWhiteSpace(apiSearchRequest.destinationType))
                {
                    searchRequestParams.destinationIds = apiSearchRequest.labelId;
                    searchRequestParams.labelId = apiSearchRequest.labelId;
                    searchRequestParams.destinationType = "Region";
                    searchRequestParams.isRegion = true;
                    searchRequestParams.regionIds.Add(apiSearchRequest.destinationIds.ToString());
                }
                else
                {
                    searchRequestParams.destinationIds = apiSearchRequest.destinationIds;
                    searchRequestParams.labelId = apiSearchRequest.destinationIds;
                    searchRequestParams.destinationType = "Label";
                    searchRequestParams.isRegion = false;
                    searchRequestParams.regionIds = parentRegionIds[apiSearchRequest.destinationIds.ToString()].Split(',').ToList();
                }
                searchRequestParams.taRating = apiSearchRequest.tripAdvisorRating;
                searchRequestParams.channelId = apiSearchRequest.channelId;
                if (!string.IsNullOrWhiteSpace(ttssResultsParams.ttssURL))
                {
                    searchRequestParams.isCacheHit = false;
                }
                else
                {
                    searchRequestParams.isCacheHit = true;
                }
                searchRequestParams.ratings = apiSearchRequest.ratings;
                searchRequestParams.tradingNameIds = apiSearchRequest.tradingNameIds;
                searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.ttssURL = ttssResultsParams.ttssURL;
                searchRequestParams.indexingTime = ttssResultsParams.indexingTime;
                searchRequestParams.decoratingTime = ttssResultsParams.decoratingTime;
                searchRequestParams.offersInserted = ttssResultsParams.offersInserted;
                searchRequestParams.offersFound = ttssResultsParams.offersFound;
                searchRequestParams.ttssCount = ttssResultsParams.ttssCount;
                searchRequestParams.facetsTrails = ttssResultsParams.facetsTrials;
                searchRequestParams.offersDecorated = ttssResultsParams.offersDecorated;
                searchRequestParams.hotelKeys = apiSearchRequest.hotelKeys;
                if (apiSearchRequest.hotelKeys.Count > 0)
                {
                    searchRequestParams.isHotelKeys = true;
                }
                else
                {
                    searchRequestParams.isHotelKeys = false;
                }
                searchRequestParams.usersPreferredHotelKeys = apiSearchRequest.usersPreferredHotelKeys;
                searchRequestParams.hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude;
                searchRequestParams.skipFacets = apiSearchRequest.skipFacets;
                searchRequestParams.endPoint = "search";
                searchRequestParams.personalizedParms = apiSearchRequest.personalizedParms;
                searchRequestParams.sort = apiSearchRequest.sort;
                try
                {
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);
                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLogger.Log(string.Format("Web exception occured while inserting Search call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting Search call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
            });
            #endregion
            return holidayOffers;
        }

        public static WCFRestService.Model.Misc.Diversity GenerateDiversityOffers(APIDiversityRequest apiDiversityRequest, StringBuilder errorLogger)
        {
            Diversity diversity = new Diversity();
            string diversityUrl = string.Empty;
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            string diversityRequestQuery = string.Empty;
            diversityRequestQuery = QueryBuilder.GetDiversityRequestQuery(apiDiversityRequest);
            //esDiversityQuery = Utilities.DiversitySearchQuery(destinationId, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, ratings, hotelKeys, tradingNameIds);
            var ESDSearchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"] + "/_search", diversityRequestQuery);
            SearchRequestResponse esDSR = new SearchRequestResponse();
            esDSR = JsonConvert.DeserializeObject<SearchRequestResponse>(ESDSearchResponse);
            if (esDSR.hits.total == 0)
            {
                ttssResultsParams = TTSSExecution.HitTTSSToGetDiversityOffers(apiDiversityRequest, errorLogger);
            }
            string diversityQuery = string.Empty;
            diversityQuery = QueryBuilder.GetDiversityQuery(apiDiversityRequest);
            var diversityOffersResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", diversityQuery);
            DiversityResponseParams.DiversityResponse diversityResponse = new DiversityResponseParams.DiversityResponse();
            try
            {
                diversityResponse = JsonConvert.DeserializeObject<DiversityResponseParams.DiversityResponse>(diversityOffersResponse);
                diversity.destinationId = apiDiversityRequest.destinationId;
                diversity.hotels = new List<DiversityHotel>();
                foreach (DiversityResponseParams.IFFBucket divHotels in diversityResponse.aggregations.group_by_iff.buckets)
                {
                    DiversityHotel diversityHotel = new DiversityHotel();
                    diversityHotel.hotelKey = divHotels.key.ToString();
                    DiversityOffer divOffer = new DiversityOffer();
                    diversityHotel.offers = new List<DiversityOffer>();
                    foreach (DiversityResponseParams.BoardTypeBucket boardType in divHotels.group_by_category.buckets)
                    {
                        DiversityOffer diversityOffer = new DiversityOffer();
                        diversityOffer.boardTypeId = boardType.key;
                        diversityOffer.price = boardType.min_price.value;
                        diversityOffer.quoteRef = boardType.top_price.hits.hits[0]._source.quoteRef;
                        diversityOffer.date = boardType.top_price.hits.hits[0]._source.journey.departureDate;
                        //diversityOffer.date = DateTime.ParseExact(boardType.top_price.hits.hits[0]._source.journey.departureDate, "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture);
                        diversityHotel.offers.Add(diversityOffer);
                    }
                    diversity.hotels.Add(diversityHotel);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Diversity Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            diversityUrl = QueryBuilder.GetDiversityURL(apiDiversityRequest);
            Task.Factory.StartNew(() =>
            {
                SearchRequestParams searchRequestParams = new SearchRequestParams();
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                Dictionary<int, string> childAirportCodes = Global.childAirportCodes;
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;
                searchRequestParams.offersReturned = diversity.hotels.Count;
                searchRequestParams.searchURL = diversityUrl;
                searchRequestParams.adults = apiDiversityRequest.adults;
                searchRequestParams.children = apiDiversityRequest.children;
                searchRequestParams.dateMin = apiDiversityRequest.dateMin;
                searchRequestParams.dateMax = apiDiversityRequest.dateMax;
                searchRequestParams.durationMin = apiDiversityRequest.durationMin;
                searchRequestParams.durationMax = apiDiversityRequest.durationMax;
                searchRequestParams.departureIds = apiDiversityRequest.departureIds;
                searchRequestParams.airportIds = new List<string>();
                if (apiDiversityRequest.departureIds < 0)
                {
                    searchRequestParams.airportIds = childAirportCodes[apiDiversityRequest.departureIds].Split(',').ToList();
                    searchRequestParams.parentDepartureIds = apiDiversityRequest.departureIds;
                }
                else
                {
                    searchRequestParams.parentDepartureIds = parentAirportCodes[apiDiversityRequest.departureIds];
                    searchRequestParams.airportIds.Add(apiDiversityRequest.departureIds.ToString());
                }
                searchRequestParams.ttssResponseTime = ttssResultsParams.ttssResponseTime;
                searchRequestParams.destinationIds = apiDiversityRequest.destinationId;
                searchRequestParams.labelId = apiDiversityRequest.destinationId;
                searchRequestParams.destinationType = "Label";
                searchRequestParams.isRegion = false;

                if (!string.IsNullOrWhiteSpace(ttssResultsParams.ttssURL))
                {
                    searchRequestParams.isCacheHit = false;
                }
                else
                {
                    searchRequestParams.isCacheHit = true;
                }
                searchRequestParams.ratings = apiDiversityRequest.ratings;
                searchRequestParams.tradingNameIds = apiDiversityRequest.tradingNameIds;
                searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.ttssURL = ttssResultsParams.ttssURL;
                searchRequestParams.indexingTime = ttssResultsParams.indexingTime;
                searchRequestParams.decoratingTime = ttssResultsParams.decoratingTime;
                searchRequestParams.offersInserted = ttssResultsParams.offersInserted;
                searchRequestParams.offersFound = ttssResultsParams.offersFound;
                searchRequestParams.ttssCount = ttssResultsParams.ttssCount;
                searchRequestParams.offersDecorated = ttssResultsParams.offersDecorated;
                searchRequestParams.hotelKeys = apiDiversityRequest.hotelKeys;
                if (apiDiversityRequest.hotelKeys.Count > 0)
                {
                    searchRequestParams.isHotelKeys = true;
                }
                else
                {
                    searchRequestParams.isHotelKeys = false;
                }
                searchRequestParams.endPoint = "diversity";
                try
                {
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);
                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLogger.Log(string.Format("Web exception occured while inserting Diversity call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting Diversity call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
            });
            return diversity;
        }

        private static string GetTelephoneNumber(int regionId, string tradingNameId, int price)
        {
            try
            {
                string destinationTelephoneNumberKey = tradingNameId + '|' + regionId.ToString();

                if (!Global.DestinationTelePhoneNumbers.ContainsKey(destinationTelephoneNumberKey))
                {
                    return String.Empty;
                }

                List<Presentation.WCFRestService.Model.DestinationTelePhoneNumber> phoneNumbers = Global.DestinationTelePhoneNumbers[destinationTelephoneNumberKey];
                var telePhoneSets = Global.TelePhoneSets;
                var random = new Random();
                string telephoneNumber = string.Empty;

                if (phoneNumbers != null && phoneNumbers.Count > 0)
                {
                    if (price <= phoneNumbers[0].Price)
                    {
                        int index = random.Next(telePhoneSets[phoneNumbers[0].TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers[0].TelephoneSet].Number[index];
                    }

                    for (int index = 0; index < phoneNumbers.Count; index++)
                    {
                        if (phoneNumbers[index].Price <= price && price < phoneNumbers[index + 1].Price)
                        {
                            int numberCount = random.Next(telePhoneSets[phoneNumbers[index].TelephoneSet].Number.Count);
                            return telePhoneSets[phoneNumbers[index].TelephoneSet].Number[numberCount];
                        }
                    }

                    if (price >= phoneNumbers.Last().Price)
                    {
                        int numberCount = random.Next(telePhoneSets[phoneNumbers.Last().TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers.Last().TelephoneSet].Number[numberCount];
                    }
                }

                return telephoneNumber;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private static string GetTelephoneNumberByLabelID(int labelId, string tradingNameId, int price)
        {
            XmlDocument xml = new XmlDocument();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            xml.Load(fileName);
            XmlNodeList regionList = xml.SelectNodes(string.Format("/Container/HolidaysData/Label[@Id='{0}']/Region", labelId));
            string regionId = string.Empty;
            string telephoneNumber = string.Empty;

            if (regionList != null && regionList.Count > 0)
            {
                foreach (XmlNode xn in regionList)
                {
                    regionId = xn.Attributes["Id"].Value;
                    telephoneNumber = GetTelephoneNumber(int.Parse(regionId), tradingNameId, price);

                    if (!string.IsNullOrEmpty(telephoneNumber))
                    {
                        break;
                    }
                }
            }

            return telephoneNumber;
        }
    }
}