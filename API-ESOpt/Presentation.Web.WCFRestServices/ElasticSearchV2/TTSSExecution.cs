﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using System;
using System.Collections.Generic;
using System.Linq;
using Presentation.WCFRestService.Model.Enum;
using System.Web;
using Newtonsoft.Json;
using ServiceStack.Redis;
using System.Configuration;
using System.Threading.Tasks;
using System.Globalization;
using System.Text;
using System.Net;
using System.IO;
using static Presentation.WCFRestService.Model.ElasticSearchV2.FacetsResponseParams;
using System.Threading;
using Presentation.WCFRestService.Model.Misc;
using System.Xml.Serialization;
using System.Xml;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class TTSSExecution
    {
        public static TTSSResultsParams HitTTSSToGetOffers(APISearchRequest apiSearchRequest, StringBuilder errorLogger)
        {
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            TTSSResponseStream ttssRespStream = new TTSSResponseStream();
            OffersDocument offers;
            OffersDocumentJourney journey;
            OffersDocumentAccommodation accommodation;
            OffersDocumentOfferHotel hotel;
            OffersDocumentRating hotelTARating;
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            int offersInserted = 0;
            int ttssCount = 0;
            double offerDecorationTimeElapsed = 0;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            List<string> distinctHotelKeys = new List<string>();
            Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            string ttssURL = QueryBuilder.GetTTSSSearchURL(apiSearchRequest);
            ttssRespStream = ExecuteQuery.ExecuteTTSSGetWebRequest(ttssURL ,errorLogger);
            #region Json
            if (ConfigurationManager.AppSettings["TSSOutputFormat"] == "json")
            {
                var ttssResults = JsonConvert.DeserializeObject<TTSSResonseParms>(ttssRespStream.ttssResponse);
                ttssCount = ttssResults.Count;
                distinctHotelKeys = (from offerResults in ttssResults.Results select offerResults.hotelKey).Distinct().ToList();
                System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
                offersDecorationTimer.Start();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }
                //Parallel.ForEach(ttssResults.Results, parrOptions, results =>
                foreach (var results in ttssResults.Results)
                {
                    if (staticInfo.ContainsKey(int.Parse(results.hotelKey)))
                    {
                        staticResponseParms = staticInfo[int.Parse(results.hotelKey)];
                        offers = new OffersDocument();
                        offers.id = results.id;
                        offers.quoteRef = results.quoteRef;
                        offers.price = results.price;
                        offers.isPreferential = false;
                        journey = new OffersDocumentJourney();
                        journey.outboundArrivalDate = results.outboundArrivalDate;
                        journey.inboundDepartureDate = results.inboundDepartureDate;
                        journey.departureDate = results.departureDate;
                        journey.returnDate = results.returnDate;
                        journey.duration = int.Parse(results.duration);
                        journey.departure = results.departure;
                        journey.gatewayName = results.gatewayName;
                        journey.destination = results.destination;
                        journey.glat = results.glat;
                        journey.glong = results.glong;
                        if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                        {
                            journey.glat = staticResponseParms.lat;
                            journey.glong = staticResponseParms.lng;
                        }
                        journey.parentRegion = staticResponseParms.parentRegion;
                        journey.parentRegionId = staticResponseParms.parentRegionId;
                        journey.gatewayCode = results.gatewayCode;
                        offers.journey = journey;
                        accommodation = new OffersDocumentAccommodation();
                        accommodation.boardTypeCode = results.boardTypeCode;
                        accommodation.boardTypeId = int.Parse(results.boardTypeId);
                        accommodation.children = int.Parse(results.children);
                        accommodation.adults = int.Parse(results.adults);
                        accommodation.rating = (int)Convert.ToDouble(results.rating);
                        accommodation.updated = results.updated;
                        offers.accommodation = accommodation;
                        hotel = new OffersDocumentOfferHotel();
                        hotel.iff = int.Parse(results.hotelKey);
                        hotel.features = staticResponseParms.features;
                        hotel.name = staticResponseParms.name;
                        hotelTARating = new OffersDocumentRating();
                        hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                        hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                        hotelTARating.reviewCount = staticResponseParms.reviewCount;
                        hotel.rating = hotelTARating;
                        offers.hotel = hotel;
                        offers.tradingNameId = new List<int>();
                        offers.tradingNameId.Add(int.Parse(results.tradingNameId));
                        offers.departureIds = int.Parse(results.departureId);
                        if (apiSearchRequest.departureIds < 0)
                        {
                            offers.parentDepartureIds = parentAirportCodes[int.Parse(results.departureId)];
                        }
                        else
                        {
                            offers.parentDepartureIds = int.Parse(results.departureId);
                        }
                        offers.regionId = int.Parse(results.destinationId);
                        offers.labelId = apiSearchRequest.destinationIds;
                        offers.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                        searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.quoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));
                        offersInserted++;
                    }
                }
                //);

                offersDecorationTimer.Stop();
                offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;
            }
            #endregion
            #region XML
            else
            {
                XmlDocument docResponse = new XmlDocument();
                docResponse.LoadXml(ttssRespStream.ttssResponse);
                XmlSerializer serializer = new XmlSerializer(typeof(Container));
                Container mytest = (Container)serializer.Deserialize(new StringReader(ttssRespStream.ttssResponse));

                MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
                distinctHotelKeys = (from ttssResult in mytest.Results.Result select ttssResult.HotelKey).Distinct().ToList();
                ttssCount = int.Parse(mytest.Results.Count);
                System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
                offersDecorationTimer.Start();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }
                //Parallel.ForEach(mytest.Results.Result, parrOptions, results =>
                foreach (var results in mytest.Results.Result)
                {
                    if (staticInfo.ContainsKey(int.Parse(results.HotelKey)))
                    {

                        staticResponseParms = staticInfo[int.Parse(results.HotelKey)];
                        offers = new OffersDocument();
                        offers.id = results.Id;
                        offers.quoteRef = results.QuoteRef;
                        offers.price = (int)Convert.ToDouble(results.Price);
                        offers.isPreferential = false;
                        journey = new OffersDocumentJourney();
                        journey.outboundArrivalDate = results.OutboundArrivalDate;
                        journey.inboundDepartureDate = results.InboundDepartureDate;
                        journey.departureDate = results.DepartureDate;
                        journey.returnDate = results.ReturnDate;
                        journey.duration = int.Parse(results.Duration);
                        journey.departure = results.Departure;
                        journey.gatewayName = results.GatewayName;
                        journey.destination = results.Destination;
                        journey.glat = results.Glat;
                        journey.glong = results.Glong;
                        if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                        {
                            journey.glat = staticResponseParms.lat;
                            journey.glong = staticResponseParms.lng;
                        }
                        journey.parentRegion = staticResponseParms.parentRegion;
                        journey.parentRegionId = staticResponseParms.parentRegionId;
                        journey.gatewayCode = results.GatewayCode;
                        offers.journey = journey;
                        accommodation = new OffersDocumentAccommodation();
                        accommodation.boardTypeCode = results.BoardTypeCode;
                        accommodation.boardTypeId = int.Parse(results.BoardTypeId);
                        accommodation.children = int.Parse(results.Children);
                        accommodation.adults = int.Parse(results.Adults);
                        accommodation.rating = (int)Convert.ToDouble(results.Rating);
                        accommodation.updated = results.Updated;
                        offers.accommodation = accommodation;
                        hotel = new OffersDocumentOfferHotel();
                        hotel.iff = int.Parse(results.HotelKey);
                        hotel.features = staticResponseParms.features;
                        hotel.name = staticResponseParms.name;
                        hotelTARating = new OffersDocumentRating();
                        hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                        hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                        hotelTARating.reviewCount = staticResponseParms.reviewCount;
                        hotel.rating = hotelTARating;
                        offers.hotel = hotel;
                        offers.tradingNameId = new List<int>();
                        offers.tradingNameId.Add(int.Parse(results.TradingNameId));
                        offers.departureIds = int.Parse(results.DepartureId);
                        if (apiSearchRequest.departureIds < 0)
                        {
                            offers.parentDepartureIds = parentAirportCodes[int.Parse(results.DepartureId)];
                        }
                        else
                        {
                            offers.parentDepartureIds = int.Parse(results.DepartureId);
                        }
                        offers.regionId = int.Parse(results.DestinationId);
                        offers.labelId = apiSearchRequest.destinationIds;
                        offers.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                        searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.QuoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));
                        offersInserted++;
                    }
                }
                //);
                offersDecorationTimer.Stop();
                offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;
            }
            #endregion
            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            if (distinctHotelKeys.Count != 0)
            {
                try
                {
                    string bulkOffers = searchParams.ToString();
                    ServicePointManager.DefaultConnectionLimit = 1000;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(bulkOffers);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (httpResponse != null)
                    {
                        httpResponse.Close();
                        httpResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    errorLogger.AppendLine((string.Format("Web exception occured while Bulk call. Url - {0}, Message - {1}, Stack Trace - {2}", ttssURL,  ex.Message, ex.StackTrace)));
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("Exception occured while making HTTP call. Url - {0}, Message - {1}, Stack Trace - {2}", ttssURL,  ex.Message, ex.StackTrace)));
                }
                string facetsResponse = string.Empty;                
                FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();
                string facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
                facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", facetsQuery);
                facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                while (facetsTrials > 0)
                {
                    if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(500);
                        facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", facetsQuery);
                        facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                        facetsTrials--;
                    }
                }
            }
            offersIndexingTimer.Stop();
            ttssResultsParams.ttssCount = ttssCount;
            ttssResultsParams.decoratingTime = offerDecorationTimeElapsed;
            ttssResultsParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.offersDecorated = offersDecorated;
            ttssResultsParams.offersFound = distinctHotelKeys.Count;
            ttssResultsParams.offersInserted = offersInserted;
            ttssResultsParams.ttssResponseTime = ttssRespStream.ttssResponseTime;
            ttssResultsParams.facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]) - facetsTrials;
            ttssResultsParams.ttssURL = ttssURL;
            return ttssResultsParams;
        }
        public static TTSSResultsParams HitTTSSToGetDiversityOffers(APIDiversityRequest apiDiversityRequest, StringBuilder errorLogger )
        {
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            TTSSResponseStream ttssRespStream = new TTSSResponseStream();
            OffersDocument offers;
            OffersDocumentJourney journey;
            OffersDocumentAccommodation accommodation;
            OffersDocumentOfferHotel hotel;
            OffersDocumentRating hotelTARating;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            string ttssURL = QueryBuilder.GetTTSSDiversityURL(apiDiversityRequest);
            ttssRespStream = ExecuteQuery.ExecuteTTSSGetWebRequest(ttssURL,errorLogger);
            var ttssResults = JsonConvert.DeserializeObject<TTSSResonseParms>(ttssRespStream.ttssResponse);
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
            List<string> distinctHotelKeys = (from offerResults in ttssResults.Results select offerResults.hotelKey).Distinct().ToList();
            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            int offersInserted = 0;
            Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
            offersDecorationTimer.Start();
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                    redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                    offersDecorated = redisStaticInfo.Count;
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                        if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                        {
                            staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                        }
                    }
                }
            }
            //Parallel.ForEach(ttssResults.Results, parrOptions, results =>
            foreach (var results in ttssResults.Results)
            {
                if (staticInfo.ContainsKey(int.Parse(results.hotelKey)))
                {
                    staticResponseParms = staticInfo[int.Parse(results.hotelKey)];
                    offers = new OffersDocument();
                    offers.id = results.id;
                    offers.quoteRef = results.quoteRef;
                    offers.price = results.price;
                    offers.isPreferential = false;
                    journey = new OffersDocumentJourney();
                    journey.outboundArrivalDate = results.outboundArrivalDate;
                    journey.inboundDepartureDate = results.inboundDepartureDate;
                    journey.departureDate = results.departureDate;
                    journey.returnDate = results.returnDate;
                    journey.duration = int.Parse(results.duration);
                    journey.departure = results.departure;
                    journey.gatewayName = results.gatewayName;
                    journey.destination = results.destination;
                    journey.glat = results.glat;
                    journey.glong = results.glong;
                    if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                    {
                        journey.glat = staticResponseParms.lat;
                        journey.glong = staticResponseParms.lng;
                    }
                    journey.parentRegion = staticResponseParms.parentRegion;
                    journey.parentRegionId = staticResponseParms.parentRegionId;
                    journey.gatewayCode = results.gatewayCode;
                    offers.journey = journey;
                    accommodation = new OffersDocumentAccommodation();
                    accommodation.boardTypeCode = results.boardTypeCode;
                    accommodation.boardTypeId = int.Parse(results.boardTypeId);
                    accommodation.children = int.Parse(results.children);
                    accommodation.adults = int.Parse(results.adults);
                    accommodation.rating = (int)Convert.ToDouble(results.rating);
                    accommodation.updated = results.updated;
                    offers.accommodation = accommodation;
                    hotel = new OffersDocumentOfferHotel();
                    hotel.iff = int.Parse(results.hotelKey);
                    hotel.features = staticResponseParms.features;
                    hotel.name = staticResponseParms.name;
                    hotelTARating = new OffersDocumentRating();
                    hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                    hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                    hotelTARating.reviewCount = staticResponseParms.reviewCount;
                    hotel.rating = hotelTARating;
                    offers.hotel = hotel;
                    offers.tradingNameId = new List<int>();
                    offers.tradingNameId.Add(int.Parse(results.tradingNameId));
                    offers.departureIds = int.Parse(results.departureId);
                    if (apiDiversityRequest.departureIds < 0)
                    {
                        offers.parentDepartureIds = parentAirportCodes[int.Parse(results.departureId)];
                    }
                    else
                    {
                        offers.parentDepartureIds = int.Parse(results.departureId);
                    }
                    offers.regionId = int.Parse(results.destinationId);
                    offers.labelId = apiDiversityRequest.destinationId;
                    offers.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                    searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.quoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));
                    offersInserted++;
                }
            }
            //);
            offersDecorationTimer.Stop();
            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            try
            {
                string bulkOffers = searchParams.ToString();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                httpWebRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(bulkOffers);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse != null)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                }
            }
            catch (Exception ex)
            {
                errorLogger.AppendLine((string.Format("Exception - {0} & Bulk Request Failed TTSS URL - {1}", ex.Message, ttssURL)));
            }
            string facetsResponse = string.Empty;
            FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();
            string facetsQuery = QueryBuilder.GetFacetsDiversityQuery(apiDiversityRequest);
            facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", facetsQuery);
            facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
            while (facetsTrials > 0)
            {
                if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                {
                    break;
                }
                else
                {
                    Thread.Sleep(500);
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "/_search", facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                    facetsTrials--;
                }
            }
            offersIndexingTimer.Stop();
            ttssResultsParams.ttssCount = ttssResults.Count;
            ttssResultsParams.decoratingTime = offersDecorationTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.offersDecorated = offersDecorated;
            ttssResultsParams.offersFound = distinctHotelKeys.Count;
            ttssResultsParams.offersInserted = offersInserted;
            ttssResultsParams.ttssResponseTime = ttssRespStream.ttssResponseTime;
            ttssResultsParams.ttssURL = ttssURL;
            return ttssResultsParams;
        }
    }
}