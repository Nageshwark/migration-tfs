﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Artirix;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Guides;
using Presentation.WCFRestService.Model.Misc;
using Presentation.Web.WCFRestServices.MyBooking;
using Presentation.Web.WCFRestServices.TopDog;
using Presentation.WCFRestService.Model.Reviews;
using Presentation.Web.WCFRestServices.Reviews;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Presentation.Web.WCFRestServices.Guides;
using System.Globalization;
using Presentation.Web.WCFRestServices.RedisDataAccessHelper;
using System.Threading.Tasks;
using Presentation.WCFRestService.Model.Flight.Artirix;
using System.Threading;
using System.Net.Http;
using System.ServiceModel;
using Presentation.Web.WCFRestServices.Google;
using System.Web.Script.Serialization;
using Presentation.WCFRestService.Model.DeepLinks;
using Presentation.Web.WCFRestServices.LambdaHits;
using Presentation.Web.WCFRestServices.StaticHotelLanding;
using Amazon.CloudFront.Model;
using Authentication = Paysafe.ThreeDSecure.Authentications;
using EnrollmentChecks = Paysafe.ThreeDSecure.EnrollmentChecks;
using Authorization = Paysafe.CardPayments.Authorization;
using Paysafe;
using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.Web.WCFRestServices.ElasticSearchV2;
using Offer = Presentation.WCFRestService.Model.Artirix.Offer;
using static Presentation.WCFRestService.Model.ElasticSearchV2.SearchResponseParams;

namespace Presentation.Web.WCFRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GetJsonService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select GetJsonService.svc or GetJsonService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class GetJsonService : IGetJsonService
    {
        static string[] documentCodes = { "Admin.PaymentsSummary", "Admin.ATOL.Receipt", "Admin.RSI.Transfer", "Admin.RSI.Accom", "Admin.RSI.AdditionalExtras" };

        # region Teletext Web API

        public Stream AuthenticateCustomerOTP(string mobileNo, string otpr)
        {
            AuthStatus otps = new AuthStatus();
            otps = Teletext.AuthCustomerOTP(mobileNo, otpr);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(otps)));
        }

        public Stream GetCustomerAuthenticationOTP(string mobileNo)
        {
            OTP otp = new OTP();
            otp = Teletext.GetCustomerAuthOTP(mobileNo);
            string country = "0";
            string authKey = "99211ABjkMgJiIwQ56683c98";
            string mobileNumber = mobileNo;
            string senderId = "TelTxt";
            string message = HttpUtility.UrlEncode("Hi your OTP is " + otp.OTPValue);
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authKey);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", senderId);
            sbPostData.AppendFormat("&route={0}", "TelTxt");

            try
            {
                string sendSMSUri = "https://control.msg91.com/api/sendhttp.php";
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (SystemException ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(otp)));
        }

        public Stream GetGuidesIndex()
        {
            string fileName = fileName = HttpRuntime.AppDomainAppPath + @"data\guideindex.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetDestinations()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinations-overseas.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelList(string destinationCode, string hotelName, string resortId)
        {
            List<AutoSuggestHotel> selectedHotels = new List<AutoSuggestHotel>();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\hotellist.json";

            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            if (!string.IsNullOrEmpty(json))
            {
                GuideHotel guideHotel = JsonConvert.DeserializeObject<GuideHotel>(json);
                var elements = destinationCode.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
                foreach (string dest in elements)
                {
                    List<AutoSuggestHotel> filterHotels = null;
                    if (resortId == "0")
                    {
                        filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                    }
                    else
                    {
                        if (resortId.IndexOf(",") > -1)
                        {
                            List<string> resIds = resortId.Split(',').ToList<string>();
                            filterHotels = guideHotel.Hotels.FindAll(
                                                                        h => h.DestinationCode.ToLower() == dest.ToLower() &&
                                                                        h.HotelName.ToLower().Contains(hotelName.ToLower()))
                                                                    .Where(x => resIds.Any(y => int.Parse(y) == x.ResortID)).ToList();
                        }
                        else
                        {
                            filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.ResortID == int.Parse(resortId) && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                        }
                    }

                    foreach (AutoSuggestHotel hotel in filterHotels)
                    {
                        if (!selectedHotels.Exists(a => a.FriendlyURL == hotel.FriendlyURL))
                            selectedHotels.Add(hotel);
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(selectedHotels)));
        }

        public Stream GetHotelListForAutoSuggest(string regionId, string hotelName)
        {
            string destinationCode = string.Empty;
            string resortId = string.Empty;
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.id.Equals(regionId))
                    {
                        destinationCode = region.destcodes;
                        resortId = region.resortcode;
                        break;
                    }
                }
            }

            List<AutoSuggestHotel> selectedHotels = new List<AutoSuggestHotel>();
            fileName = HttpRuntime.AppDomainAppPath + @"data\hotellist.json";

            if (!string.IsNullOrEmpty(destinationCode) && !string.IsNullOrEmpty(resortId))
            {
                if (System.IO.File.Exists(fileName))
                {
                    using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        json = sr.ReadToEnd();
                    }
                }
                else
                {
                    json = "{}";
                }

                if (!string.IsNullOrEmpty(json))
                {
                    GuideHotel guideHotel = JsonConvert.DeserializeObject<GuideHotel>(json);
                    var elements = destinationCode.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
                    foreach (string dest in elements)
                    {
                        List<AutoSuggestHotel> filterHotels = null;
                        if (resortId == "0")
                        {
                            filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                        }
                        else
                        {
                            if (resortId.IndexOf(",") > -1)
                            {
                                List<string> resIds = resortId.Split(',').ToList<string>();
                                filterHotels = guideHotel.Hotels.FindAll(
                                                                            h => h.DestinationCode.ToLower() == dest.ToLower() &&
                                                                            h.HotelName.ToLower().Contains(hotelName.ToLower()))
                                                                        .Where(x => resIds.Any(y => int.Parse(y) == x.ResortID)).ToList();
                            }
                            else
                            {
                                filterHotels = guideHotel.Hotels.FindAll(h => h.DestinationCode.ToLower() == dest.ToLower() && h.ResortID == int.Parse(resortId) && h.HotelName.ToLower().Contains(hotelName.ToLower()));
                            }
                        }

                        foreach (AutoSuggestHotel hotel in filterHotels)
                        {
                            if (!selectedHotels.Exists(a => a.FriendlyURL == hotel.FriendlyURL))
                                selectedHotels.Add(hotel);
                        }
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(selectedHotels)));
        }

        public Stream GetGuidesIndexList()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetGuide(string RegionId)
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\" + @"region" + RegionId + ".json";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "{}";
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetRegionById(string RegionId)
        {
            RequestedRegion reqRegion = new RequestedRegion();
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.id.Equals(RegionId))
                    {
                        reqRegion.regid = region.id;
                        reqRegion.regUrl = region.linkurl;
                        reqRegion.regname = region.name;
                        reqRegion.reglevel = region.reglevel;
                        reqRegion.parent_regid = region.parent_regid != null ? region.parent_regid : string.Empty;
                        reqRegion.parent_regname = region.parent_regname != null ? region.parent_regname : string.Empty;
                        reqRegion.parent_link = region.parent_link != null ? region.parent_link : string.Empty;
                        reqRegion.grand_regid = region.grand_regid != null ? region.grand_regid : string.Empty;
                        reqRegion.grand_regname = region.grand_regname != null ? region.grand_regname : string.Empty;
                        reqRegion.grand_reglink = region.grand_reglink != null ? region.grand_reglink : string.Empty;
                        break;
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reqRegion)));
        }

        public Stream GetRegionByURL(string url)
        {
            RequestedRegion reqRegion = new RequestedRegion();
            var json = String.Empty;

            string fileName = HttpRuntime.AppDomainAppPath + @"data\guidesindexlist.json";
            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(json))
            {
                DestinationRegionList destRegionList = JsonConvert.DeserializeObject<DestinationRegionList>(json);
                foreach (DestinationRegion region in destRegionList.destinations)
                {
                    if (region.linkurl.ToLower().Equals(url.ToLower()))
                    {
                        reqRegion.regid = region.id;
                        reqRegion.regUrl = region.linkurl;
                        reqRegion.regname = region.name;
                        reqRegion.reglevel = region.reglevel;
                        reqRegion.parent_regid = region.parent_regid != null ? region.parent_regid : string.Empty;
                        reqRegion.parent_regname = region.parent_regname != null ? region.parent_regname : string.Empty;
                        reqRegion.parent_link = region.parent_link != null ? region.parent_link : string.Empty;
                        reqRegion.grand_regid = region.grand_regid != null ? region.grand_regid : string.Empty;
                        reqRegion.grand_regname = region.grand_regname != null ? region.grand_regname : string.Empty;
                        reqRegion.grand_reglink = region.grand_reglink != null ? region.grand_reglink : string.Empty;
                        break;
                    }
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reqRegion)));
        }

        public Stream GetOverseasDestination(string destinationName)
        {
            List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(destinationName);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(overseasList)));
        }

        public Stream GetOvrseaseDestinationLabelRegions()
        {
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            var json = String.Empty;

            if (System.IO.File.Exists(fileName))
            {
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    json = sr.ReadToEnd();
                }
            }
            else
            {
                json = "";
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetMasterHotelIdByURL(string url)
        {
            string masterHotelId = string.Empty;
            IHotelService hotelService = new HotelService();
            masterHotelId = hotelService.GetMasterHotelIdByURL(url);
            var masterHotel = new { MasterHotelId = masterHotelId };
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterHotel)));
        }

        public Stream GetHotelDataByMasterHotelId(string masterHotelId)
        {
            string json = string.Empty;
            IHotelService hotelService = new HotelService();
            json = hotelService.GetHotelDataByMasterHotelId(masterHotelId);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel)
        {
            IHotelService hotelService = new HotelService();
            bool result = hotelService.SetMasterHotelData(hotel);
            string json = "{" + "\"" + "Status" + "\"" + ":" + "\"" + (result ? "Success" : "Fail") + "\"" + "}";
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream SaveGuideInformation(Guide guide, string userId)
        {
            try
            {
                GuideDestination orgGuideDestination = new GuideDestination();

                if (!string.IsNullOrWhiteSpace(guide.regid))
                {
                    //If Guide Exists, get the JSON file from S3.
                    orgGuideDestination = Guides.Guides.GetGuideDestinationInfo(guide.regid);
                    if (guide == null)
                    {
                        throw new NullReferenceException(string.Format("Guide- {0} does not exist.", guide.regid));
                    }
                    orgGuideDestination.regid = guide.regid;
                    //Remove existing Places TO GO/Things To Do and Overview sections from the JSON, as we are going to dump new data.
                    orgGuideDestination.sections.RemoveAll(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("overview", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("essential info", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                }
                else
                {
                    //Get the new GuideId from the GuideIndexList JSON file.
                    orgGuideDestination.regid = Guides.Guides.getNextRegID();
                    if (string.IsNullOrWhiteSpace(orgGuideDestination.regid))
                    {
                        throw new NullReferenceException("Next RegionId does not exist.");
                    }
                    guide.regid = orgGuideDestination.regid;
                    orgGuideDestination.sections = new List<section>();
                }

                //Dump the new Data.
                orgGuideDestination.bgimage = guide.bgimage;
                orgGuideDestination.ptitle = guide.ptitle;
                orgGuideDestination.bgimage_m = guide.bgimage_m;
                orgGuideDestination.bgimage_t = guide.bgimage_t;
                orgGuideDestination.bgimage_sd = guide.bgimage_sd;
                orgGuideDestination.pmetadesc = guide.pmetadesc;
                orgGuideDestination.sectionid = guide.sectionid;
                orgGuideDestination.pmetakeyword = guide.pmetakeyword;
                orgGuideDestination.showGuideOnMobile = guide.showGuideOnMobile;

                orgGuideDestination.regname = guide.regname;
                orgGuideDestination.reglevel = guide.reglevel;
                orgGuideDestination.recommendedhotels = guide.recommendedhotels;
                orgGuideDestination.parent_regid = guide.parent_regid;
                orgGuideDestination.parent_regname = guide.parent_regname;
                orgGuideDestination.parent_link = guide.parent_link;
                orgGuideDestination.grand_regid = guide.grand_regid;
                orgGuideDestination.grand_regname = guide.grand_regname;
                orgGuideDestination.grand_reglink = guide.grand_reglink;
                orgGuideDestination.highSeasonalAverage = guide.highSeasonalAverage;

                orgGuideDestination.sections.AddRange(guide.sections);
                orgGuideDestination.expertviews = guide.expertviews;
                orgGuideDestination.metatags = guide.metatags;
                orgGuideDestination.dropDownLabelName = guide.dropDownLabelName;

                //Once the data is dumped to the Object, Upload it to S3.
                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["AWSGuideBucketName"], guide.regid + ".json", orgGuideDestination);

                var isGuideIndexListUpdated = Guides.Guides.AddRecordguidesindexlist(orgGuideDestination);
                ErrorLogger.Log(string.Format("Guide - {0} successfully updated By the User - {1}", guide.regid, userId), LogLevel.Information);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("SaveGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideInformation(string regionId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null");
                }

                var guide = Guides.Guides.GetGuideInfo(regionId);
                if (guide == null)
                {
                    throw new NullReferenceException(string.Format("Guide- {0} does not exist.", regionId));
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guide.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guide.showLastMinuteDeals = true;
                }

                if (guide.recommendedhotels == null)
                {
                    guide.recommendedhotels = new List<RecommendedHotel>();
                }

                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideInformationByUrl(string url)
        {
            try
            {
                var regionId = Guides.Guides.GetDestinationIdByUrl(url.ToLower().Replace("/guidesv2", "/guides"));
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or empty");
                }

                var guide = Guides.Guides.GetGuideInfo(regionId);

                if (guide == null)
                {
                    throw new NullReferenceException(string.Format("Guide- {0} does not exist.", regionId));
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guide.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guide.showLastMinuteDeals = true;
                }
                if (guide.recommendedhotels == null)
                {
                    guide.recommendedhotels = new List<RecommendedHotel>();
                }

                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guide)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformationByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideLandingPageInfo(string regionId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or Empty");
                }

                var guideDestination = Guides.Guides.GetGuide(regionId);

                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guideDestination)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideLandingPageInfo returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuideLandingPageInfoByUrl(string url)
        {
            try
            {
                var regionId = Guides.Guides.GetDestinationIdByUrl(url.ToLower().Replace("/guidesv2", "/guides"));
                if (string.IsNullOrWhiteSpace(regionId))
                {
                    throw new ArgumentException("RegionId is null or Empty");
                }
                //Get Region From S3.                
                GuideDestination guideDestination = Guides.Guides.GetGuideDestinationInfo(regionId); //JsonConvert.DeserializeObject<GuideDestination>(json);
                if (guideDestination == null)
                {
                    throw new NullReferenceException("Guide object does not exist");
                }
                List<OverseasDestination> overseasList = Guides.Guides.GetOverseasDestination(guideDestination.regname);
                if (overseasList != null && overseasList.Count > 0)
                {
                    guideDestination.showLastMinuteDeals = true;
                }
                //Get GuideIndexList JSON File.
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                //Get Child Regions based on the Region ID                
                IEnumerable<Destination> guideChilds = destinationGuides.destinations.Where(p => p.parent_regid == regionId);

                //Append ChildRegion Info to the ParentRegion if Exists
                if (guideChilds != null && guideChilds.Count() > 0)
                {
                    guideDestination.link_summary = new List<link_summary>();
                    GuideDestination guideChildInfo = null;
                    link_summary linkSummary = null;
                    for (var guideChildIndex = 0; guideChildIndex < guideChilds.Count(); guideChildIndex++)
                    {
                        var guideChild = guideChilds.ToList()[guideChildIndex];
                        //Get Child Regions from S3.                        
                        guideChildInfo = Guides.Guides.GetGuideDestinationInfo(guideChild.id);
                        if (guideChildInfo == null)
                        {
                            continue;
                        }

                        linkSummary = new link_summary();
                        linkSummary.lname = Guides.Guides.GetDestinationName(guideChild.id);
                        linkSummary.guideId = guideChild.id;
                        linkSummary.bgimage_m = guideChildInfo.bgimage_m;
                        linkSummary.lurl = Guides.Guides.GetDestinationUrl(guideChild.id).ToLower();
                        linkSummary.sectionid = guideChildInfo.sectionid;
                        linkSummary.recommendedhotels = guideChildInfo.recommendedhotels == null ? new List<RecommendedHotel>() : guideChildInfo.recommendedhotels;

                        //Add Child Regions Places To Go/Things To Do to the Parent Region.
                        var childGuidePlacesToGo = guideChildInfo.sections.Where(s => s.sname.Equals("places to go", StringComparison.OrdinalIgnoreCase) || s.sname.Equals("things to do", StringComparison.OrdinalIgnoreCase));
                        if (childGuidePlacesToGo != null && childGuidePlacesToGo.Count() > 0)
                        {
                            linkSummary.PlacesToGo = childGuidePlacesToGo.FirstOrDefault();
                        }

                        guideDestination.link_summary.Add(linkSummary);
                    }
                }

                if (guideDestination.recommendedhotels == null)
                {
                    guideDestination.recommendedhotels = new List<RecommendedHotel>();
                }

                guideDestination.ptitle = guideDestination.ptitle.Replace("Holidays |", "Holidays 2017/2018 |").Replace("Holidays –", "Holidays 2017/2018 –");
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["GuideMaxAge"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(guideDestination)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideLandingPageInfoByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetGuidesIndexListFromCloudFront()
        {
            try
            {
                DestinationGuides destinationGuides = Guides.Guides.GetGuideIndexList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(destinationGuides)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetGuideInformationByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetTeletextRouting(string destinationId, string departureId, string dates)
        {
            int number;
            if (string.IsNullOrEmpty(destinationId) && string.IsNullOrEmpty(departureId) && string.IsNullOrEmpty(dates))
            {
                throw new WebFaultException<string>("all parameters are empty.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(destinationId) && !int.TryParse(destinationId, out number))
            {
                throw new WebFaultException<string>("Invalid destinationId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }
            else if (!string.IsNullOrEmpty(departureId) && !int.TryParse(departureId, out number))
            {
                throw new WebFaultException<string>("Invalid departureId.", System.Net.HttpStatusCode.ServiceUnavailable);
            }

            RoutingResponse routingResult = RoutingApiResponseGenerator.GetRoutingData(destinationId, departureId, dates);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(routingResult)));
        }

        #endregion

        #region Flight Stats API

        public Stream GetFlightByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightStats flightStats = new FlightStats();

            FlightStatusResponse flight = flightStats.GetFlightByFlightNumberAndFlightDate(flightNumber, day, month, year);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(flight)));

        }

        public Stream GetFlightScheduleByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightStats flightStats = new FlightStats();

            FlightScheduleResponse flight = flightStats.GetFlightScheduleByFlightNumberAndFlightDate(flightNumber, day, month, year);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(flight)));

        }
        #endregion

        #region TopDog API

        public Stream GetMobileBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;
            MobileBasket mobileBasket = new MobileBasket();
            mobileBasket.Airlines = new Dictionary<string, string>();
            mobileBasket.Airports = new Dictionary<string, string>();
            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, true);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, true);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetTOPDOGBasket(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                BookingReference = BookingReference.ToUpper();
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetBasket(BookingReference, Surname, DepartureDate, false);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetQuickQuoteDetails(string BookingReference, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                MyBookings myBookingDetails = new MyBookings();
                myBookingDetails = TopDogManager.GetQuickQuoteDetails(BookingReference, DepartureDate);

                if (myBookingDetails != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDetails)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetDocuments(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;

            try
            {
                MyDocuments myBookingDocuments = null;

                try
                {
                    BookingReference = BookingReference.ToUpper();
                    Stream data = Utilities.DownloadS3Object(ConfigurationManager.AppSettings["BookingAWSBucketName"], string.Format("{0}-invoice.pdf", BookingReference.ToLower()));
                    StreamReader reader = new StreamReader(data);
                    Task.Run(() => TopDogManager.GetDocuments(BookingReference, Surname, DepartureDate));
                    myBookingDocuments = new MyDocuments();
                    DocumentType docType = new DocumentType();
                    DocumentList docList = new DocumentList();
                    List<DocumentList> dl = new List<DocumentList>();
                    docType.Description = "Consolidated Document";
                    docList.DocumentType = new DocumentType();
                    docList.DocumentType = docType;
                    docList.Category = (int)Category.Generic;
                    docList.DocumentUrl = ConfigurationManager.AppSettings["S3TeletextBookingUrl"].ToString() + "/" + string.Format("{0}-invoice.pdf", BookingReference.ToLower());
                    docList.DocumentFormat = "PDF";
                    dl.Add(docList);
                    myBookingDocuments.DocumentList = dl;
                }
                catch (Exception ex)
                {
                    var bookingDocuments = TopDogManager.GetDocuments(BookingReference, Surname, DepartureDate);

                    if (bookingDocuments != null)
                    {
                        myBookingDocuments = new MyDocuments();
                        List<DocumentList> dl = new List<DocumentList>();
                        myBookingDocuments.BookingReference = bookingDocuments[0].BookingReference;
                        myBookingDocuments.Comment = bookingDocuments[0].Comment;
                        foreach (var document in bookingDocuments[0].DocumentList)
                        {
                            DocumentType docType = new DocumentType();
                            docType.Code = document.DocumentType.Code;
                            docType.Description = document.DocumentType.Description;

                            DocumentList docList = new DocumentList();

                            if (documentCodes.Contains(document.DocumentType.Code))
                            {
                                docList.Category = (int)Category.Payments;
                            }
                            else if (document.DocumentType.Code == "Admin.ImportantInformation")
                            {
                                docList.Category = (int)Category.Important_Information;
                            }
                            else if (document.DocumentType.Code == "ATOL-CERTIFICATE")
                            {
                                docList.Category = (int)Category.ATOL_Certificate;
                            }
                            else if (document.DocumentType.Code == "Admin.E-TicketsDetails.Combined")
                            {
                                docList.Category = (int)Category.Flights;
                            }
                            else if (document.DocumentType.Code == "Admin.Accommodation.Voucher.Supplier")
                            {
                                docList.Category = (int)Category.Accomodation;
                            }
                            else if (document.DocumentType.Code == "Admin.TransferVoucher")
                            {
                                docList.Category = (int)Category.Transfer;
                            }
                            docList.CreationDateTime = document.CreationDateTime;
                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentFormat = document.DocumentFormat;
                            docList.DocumentUrl = document.DocumentUrl.Replace("10.0.0.190", ConfigurationManager.AppSettings["TopDogDocumentsURL"]);
                            docList.DeliveryType = document.DeliveryType;
                            docList.DocumentType = docType;
                            docList.UserName = document.UserName;
                            dl.Add(docList);
                        }
                        myBookingDocuments.DocumentList = dl;
                    }
                }
                //else
                //{
                //    myBookingDocuments = new MyDocuments();
                //    DocumentType docType = new DocumentType();
                //    DocumentList docList = new DocumentList();
                //    List<DocumentList> dl = new List<DocumentList>();
                //    docType.Description = "Consolidated Document";
                //    docList.Category = (int)Category.Generic;
                //    docList.DocumentUrl = ConfigurationManager.AppSettings["S3TeletextBookingUrl"].ToString() + "/" + string.Format("{0}-invoice.pdf", BookingReference.ToLower());
                //    docList.DocumentFormat = "PDF";
                //    dl.Add(docList);
                //    myBookingDocuments.DocumentList = dl;
                //}


                if (myBookingDocuments != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(myBookingDocuments)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        public Stream GetPaymentPlan(string BookingReference, string Surname, string DepartureDate)
        {
            var json = String.Empty;
            try
            {
                BookingReference = BookingReference.ToUpper();
                var paymentPlan = TopDogManager.GetPayments(BookingReference, Surname, DepartureDate);

                if (paymentPlan != null)
                {
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentPlan)));
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
        }

        #endregion

        # region Mobile App API

        public Stream GetHotelsByDestinationCode(string destCode)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelsByDestCodeURL"], destCode);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelByID(string hotelId)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelByIdURL"], hotelId);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelReviewByID(string hotelId)
        {
            var json = String.Empty;
            string url = string.Format(ConfigurationManager.AppSettings["HotelReviewByIdURL"], hotelId);
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }


        public Stream GetHolidaySearch(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                        string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorRating,
                                        string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType,
                                        string labelId, string preferredHotelKeys, string skipFacets, string destinationType, string platform)
        {
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixImgUrl(
                                adults, children, ratings, departureDate, dateMin, dateMax,
                                durationMin, durationMax, destinationIds, priceMin, priceMax,
                                departureIds, tripadvisorRating, sort, hotelKeysToExclude,
                                channelId, hotelKeys, tradingNameIds, boardType, labelId, preferredHotelKeys, skipFacets, destinationType);

            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            if (string.IsNullOrWhiteSpace(ArtirixServiceUrlFmt))
            {
                throw new WebFaultException<string>("Service url not found in config.", System.Net.HttpStatusCode.BadRequest);
            }

            string artirixSearchUrl = ArtirixServiceUrlFmt + "search?" + paramsString;
            if (string.IsNullOrWhiteSpace(platform))
            {
                platform = "desktop";
            }
            try
            {
                Holidays holidays = new Holidays();
                holidays = ArtirixImageSearch.GetAtxImgSrc(artirixSearchUrl, platform);
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["ArtirixCacheTime"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidays)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>(ex.Message + ". " + ex.StackTrace, System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetHolidaySearchV_2(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin,
                                        string durationMax, string destinationIds, string priceMin, string priceMax, string departureIds, string tripadvisorRating,
                                        string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType,
                                        string labelId, string preferredHotelKeys, string skipFacets, string destinationType)
        {
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixImgUrl(
                                adults, children, ratings, departureDate, dateMin, dateMax,
                                durationMin, durationMax, destinationIds, priceMin, priceMax,
                                departureIds, tripadvisorRating, sort, hotelKeysToExclude,
                                channelId, hotelKeys, tradingNameIds, boardType, labelId, preferredHotelKeys, skipFacets, destinationType);
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            if (string.IsNullOrWhiteSpace(ArtirixServiceUrlFmt))
            {
                throw new WebFaultException<string>("Service url not found in config.", System.Net.HttpStatusCode.BadRequest);
            }
            string artirixSearchUrl = ArtirixServiceUrlFmt + "search?" + paramsString;
            string json = string.Empty;
            Boolean isPresentInRedis = false;
            try
            {
                json = RedisDataHelper.GetValueByKey(
                    artirixSearchUrl,
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["ArtirixRedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));

                isPresentInRedis = true;
            }
            catch (System.ArgumentNullException ex)
            {
                isPresentInRedis = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!isPresentInRedis)
            {
                ThreadPool.SetMaxThreads(int.Parse(ConfigurationManager.AppSettings["ArtirixMaxThreads"]), 0);
                ThreadPool.SetMinThreads(int.Parse(ConfigurationManager.AppSettings["ArtirixMinThreads"]), 0);
                ThreadPool.QueueUserWorkItem(HolidaysAsync, new object[] { artirixSearchUrl });
            }
            if (!string.IsNullOrWhiteSpace(json))
            {
                try
                {
                    Holidays holidays = new Holidays();
                    holidays = ArtirixImageSearch.GetAsyncAtxImgSrc(json);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    HolidaysAsyncResult result = new HolidaysAsyncResult()
                    {
                        isCompleted = true,
                        facets = holidays.facets,
                        offers = holidays.offers,
                        total = holidays.total,
                        totalHotels = holidays.totalHotels
                    };
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                    throw new WebFaultException<string>(ex.Message + ". " + ex.StackTrace, System.Net.HttpStatusCode.BadRequest);
                }
            }
            else
            {
                HolidaysAsyncResult result = new HolidaysAsyncResult()
                {
                    isCompleted = false
                };
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));
            }
        }

        private void HolidaysAsync(object state)
        {
            object[] artirixValue = state as object[];
            string artirixUrl = artirixValue[0].ToString();
            string artirixRespStr = Utilities.ExecuteMobileArtirixGetWebRequest(artirixUrl);
            RedisDataHelper.InsertTAValue(artirixUrl, artirixRespStr, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["ArtirixRedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
            //return artirixRespStr;
        }

        public Stream GetHolidays(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");

            if (!string.IsNullOrEmpty(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrEmpty(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrEmpty(ratings))
            {
                searchParams.Append("ratings=" + ratings + "&");
            }

            if (!string.IsNullOrEmpty(departureDate))
            {
                searchParams.Append("departureDate=" + departureDate + "&");
            }

            if (!string.IsNullOrEmpty(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrEmpty(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrEmpty(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrEmpty(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrEmpty(destinationIds))
            {
                searchParams.Append("destinationIds=" + destinationIds + "&");
            }

            if (!string.IsNullOrEmpty(priceMin))
            {
                searchParams.Append("priceMin=" + priceMin + "&");
            }

            if (!string.IsNullOrEmpty(priceMax))
            {
                searchParams.Append("priceMax=" + priceMax + "&");
            }

            if (!string.IsNullOrEmpty(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrEmpty(tripadvisorrating))
            {
                searchParams.Append("tripadvisorrating=" + tripadvisorrating + "&");
            }

            if (!string.IsNullOrEmpty(sort))
            {
                searchParams.Append("sort=" + sort + "&");
            }

            if (!string.IsNullOrEmpty(hotelKeysToExclude))
            {
                searchParams.Append("hotelKeysToExclude=" + hotelKeysToExclude + "&");
            }

            if (!string.IsNullOrEmpty(channelId))
            {
                searchParams.Append("channelId=" + channelId + "&");
            }

            if (!string.IsNullOrEmpty(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrEmpty(tradingNameIds))
            {
                searchParams.Append("tradingNameIds=" + tradingNameIds + "&");
            }

            if (!string.IsNullOrEmpty(boardType))
            {
                searchParams.Append("boardType=" + boardType + "&");
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            json = Utilities.GetJson<string>(url);
            Holidays holidays = JsonConvert.DeserializeObject<Holidays>(json);
            if (holidays != null && holidays.offers != null)
            {
                foreach (Offer offr in holidays.offers)
                {
                    if (offr != null && offr.hotel != null && offr.hotel.images != null)
                    {
                        int count = offr.hotel.images.Count;
                        for (int i = 0; i < count; i++)
                        {
                            offr.hotel.images[i] = string.Format(ConfigurationManager.AppSettings["ArtirixImgPath"], offr.hotel.images[i]);
                        }
                    }
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidays)));
        }

        public Stream GetMapByLocation(string lat, string lon)
        {
            string url = string.Format(ConfigurationManager.AppSettings["GoogleMapURL"], lat, lon, lat, lon);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.Proxy = null;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return responseStream;
        }

        public Stream GetHomePageSlider()
        {
            List<Slider> sliderList = Teletext.GetHomepageSlider();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sliderList)));
        }

        public Stream GetOffersAndDealsSlider()
        {
            var json = String.Empty;
            string fileName = ConfigurationManager.AppSettings["OffersandDealsS3URL"];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileName);
            request.Proxy = null;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            json = reader.ReadToEnd();
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }


        public Stream GetPubsAndRestruantsByGoogleAPI(string lat, string lon, string type, string hotelId)
        {
            int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
            var s3Response = String.Empty;
            string googleKey = type + hotelId;
            bool googleResponse = false;
            //googleResponse = GoogleAPI.GetBarsAndRestaurants(lat, lon, type, googleKey, key1);                       

            while (!googleResponse && keyCount > 0)
            {
                string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                googleResponse = GoogleAPI.GetBarsAndRestaurants(lat, lon, type, googleKey, googleAPIKey);
                keyCount--;
            }
            try
            {
                string s3URL = "https://teletext-googleapi.s3.amazonaws.com/BarsandRestaurants/" + googleKey + ".json";
                s3Response = Utilities.ExecuteGoogleGetWebRequest(s3URL);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("S3 and Google returned empty response", System.Net.HttpStatusCode.BadRequest);
            }
            if (!string.IsNullOrWhiteSpace(s3Response))
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(s3Response));
        }

        public Stream GetArtirixHotelDescription(string hotelOperator, string contentId)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();
            if (!string.IsNullOrEmpty(hotelOperator) && !string.IsNullOrEmpty(contentId))
            {
                searchParams.Append("hotelOperator=" + hotelOperator + "&");
                searchParams.Append("contentId=" + contentId);
            }
            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "description?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetArtirixRouting(string destinationId, string departureId, string dates)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();

            if (!string.IsNullOrEmpty(destinationId))
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }

            if (!string.IsNullOrEmpty(departureId))
            {
                searchParams.Append("departureId=" + departureId + "&");
            }

            if (!string.IsNullOrEmpty(dates))
            {
                searchParams.Append("dates=" + dates);
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "routing?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetArtirixDiversity(string destinationId, string hotelKeys, string departureIds, string dateMin, string dateMax, string children, string adults, string durationMax, string durationMin, string ratings)
        {
            var json = String.Empty;
            StringBuilder searchParams = new StringBuilder();

            if (!string.IsNullOrEmpty(destinationId))
            {
                searchParams.Append("destinationId=" + destinationId + "&");
            }

            if (!string.IsNullOrEmpty(hotelKeys))
            {
                searchParams.Append("hotelKeys=" + hotelKeys + "&");
            }

            if (!string.IsNullOrEmpty(departureIds))
            {
                searchParams.Append("departureIds=" + departureIds + "&");
            }

            if (!string.IsNullOrEmpty(dateMin))
            {
                searchParams.Append("dateMin=" + dateMin + "&");
            }

            if (!string.IsNullOrEmpty(dateMax))
            {
                searchParams.Append("dateMax=" + dateMax + "&");
            }

            if (!string.IsNullOrEmpty(children))
            {
                searchParams.Append("children=" + children + "&");
            }

            if (!string.IsNullOrEmpty(adults))
            {
                searchParams.Append("adults=" + adults + "&");
            }

            if (!string.IsNullOrEmpty(durationMax))
            {
                searchParams.Append("durationMax=" + durationMax + "&");
            }

            if (!string.IsNullOrEmpty(durationMin))
            {
                searchParams.Append("durationMin=" + durationMin + "&");
            }

            if (!string.IsNullOrEmpty(ratings))
            {
                searchParams.Append("ratings=" + ratings);
            }

            string paramsString = searchParams.ToString();

            if (!string.IsNullOrEmpty(paramsString))
            {
                string lastChar = paramsString.Substring(paramsString.Length - 1);

                if (lastChar.Equals("&"))
                {
                    paramsString = paramsString.Substring(0, paramsString.Length - 1);
                }
            }

            string url = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "diversity?" + paramsString;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetHotelContent(string hotelOperator, string contentId)
        {
            var json = String.Empty;
            string url = ConfigurationManager.AppSettings["ArtirixContentDescription"] + "hotelOperator=" + hotelOperator + "&contentId=" + contentId;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetGoogleAirportInfo(string airport)
        {
            int keyCount = int.Parse(ConfigurationManager.AppSettings["GoogleKeyCount"]);
            string airportkey = airport.ToLower();
            var s3Response = String.Empty;
            bool googleResponse = false;
            while (!googleResponse && keyCount > 0)
            {
                string GoogleAPIKey = "GoogleAPIKey" + keyCount.ToString();
                string googleAPIKey = ConfigurationManager.AppSettings[GoogleAPIKey];
                googleResponse = GoogleAPI.GetAirportInfo(airport, googleAPIKey);
                keyCount--;
            }
            try
            {
                string s3URL = "https://teletext-googleapi.s3.amazonaws.com/Airports/" + airportkey + ".json";
                s3Response = Utilities.ExecuteGoogleGetWebRequest(s3URL);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("S3 and Google returned empty response", System.Net.HttpStatusCode.BadRequest);
            }
            if (!string.IsNullOrWhiteSpace(s3Response))
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(s3Response));
        }

        public Stream GetGoogleDistance(string oLat, string oLng, string dLat, string dLng, string mode)
        {
            var json = string.Empty;
            string url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + oLat + "," + oLng + "&destinations=" + dLat + "," + dLng + "&transit_mode=" + mode;
            json = Utilities.GetJson<string>(url);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneWeek"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        #endregion

        # region PostCode API

        public Stream GetAddressesByPostCode(string postCode)
        {
            IPostCodeAddresses postCodeAddresses = new OpenPostCode();

            PostCodeResponse resp = postCodeAddresses.GetAddressesByPostCode(postCode);

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";

            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(resp)));
        }

        #endregion

        # region SmartFocus API

        public Stream GetNewsLetterSubscription(string name, string email, string phone, string airport)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            status = MarketingService.GetNewsLetterSubscription(name, email, phone, airport);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status)));
        }
        public Stream GetSubscription(emailSubscription emailSubcription)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            status = MarketingService.GetNewsLetterSubscription(emailSubcription.name, emailSubcription.email, emailSubcription.phone, emailSubcription.airport);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status)));
        }

        public Stream GetNewsLetterSubscriberByEmail(string email)
        {
            string status = MarketingService.GetNewsLetterSubscriberByEmail(email);
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(status));
        }
        #endregion

        #region MinPriceTool

        public Stream GetArtirixMinPrice(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates)
        {
            var preUrl = String.Empty;
            preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + channelName + "/" + destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrl(departureName, boardType, durationMax, adults, children, ratings, priceMin, priceMax,
                                                    hotelKeys, tradingNameIds, dateOffSet, flexDates);
            try
            {
                var paramsString = string.Empty;
                paramsString = Utilities.GetArtirixUrl(adults, children, ratings, departureDate, dateMin, dateMax,
                                                        durationMin, durationMax, destinationIds, priceMin, priceMax,
                                                        departureIds, tripadvisorrating, sort, hotelKeysToExclude,
                                                        channelId, hotelKeys, tradingNameIds, boardType);
                var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
                string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
                MinPriceView minPriceView = new MinPriceView();
                minPriceView = MinPriceTool.MinPriceResponseView(artirixSearchUrl, preUrl, postUrl);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));


            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }


        }


        public Stream GetMinPriceHomePage()
        {
            string inputURL = ConfigurationManager.AppSettings["MinPriceInput"];
            string outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
            string inputFileResponse = Utilities.ExecuteGetWebRequest(inputURL);
            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            List<MinPriceToolObject> minPriceToolInput;
            List<MinPriceToolObject> minPriceToolOutput;
            minPriceToolInput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(inputFileResponse);
            minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse);
            int countIn = minPriceToolInput.Count;
            int countOut = minPriceToolOutput.Count;
            try
            {
                if (countIn >= countOut && countOut != 0)
                {
                    for (int count = 0; count < countOut; count++)
                    {
                        minPriceToolInput[count].lastUpdated = minPriceToolOutput[count].lastUpdated;
                        minPriceToolInput[count].minPrice = minPriceToolOutput[count].minPrice;
                        minPriceToolInput[count].quoteRef = minPriceToolOutput[count].quoteRef;
                    }
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else if (countOut == 0)
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutput.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at Image Updation:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
        }

        public Stream DeleteObject(string queryId, string userId)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolOutputObject = new List<MinPriceToolObject>();
            minPriceToolInputObject = MinPriceTool.DeleteInputObject(queryId);
            minPriceToolOutputObject = MinPriceTool.DeleteOutputObject(queryId);
            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully deleted By the User - {1}", queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }
        public Stream GetSectionObjects(string sectionIds, string platform)
        {
            List<MinPriceToolObject> minPriceToolSectionObject = new List<MinPriceToolObject>();
            minPriceToolSectionObject = MinPriceTool.SectionObject(sectionIds);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                if (cacheTime < 1)
                {
                    cacheTime = 900;
                }
            }

            List<MinPriceToolObject> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.id).ToList();
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }
        public Stream GetQueryIdObjects(string queryId, string sectionId)
        {
            MinPriceToolObject minPriceToolSectionObject = new MinPriceToolObject();
            minPriceToolSectionObject = MinPriceTool.queryId(queryId, sectionId);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            DateTime lastCreated = DateTime.UtcNow;
            double cacheTime = 0;
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                lastCreated = objResponse.LastModified.AddHours(1);
                cacheTime = lastCreated.Subtract(DateTime.UtcNow).TotalSeconds;
                // lastCreated = objRequest.ModifiedSinceDate;
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolSectionObject)));

        }

        public Stream SetQueryImageUrl(string queryId, string sectionId, string imageUrl, string userId)
        {
            List<MinPriceToolObject> minPriceQueryUrl = new List<MinPriceToolObject>();
            minPriceQueryUrl = MinPriceTool.SetQueryImageUrl(queryId, sectionId, imageUrl);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0}, section - {1}, imageUrl - {2} successfully set the image url by the User - {3}", queryId, sectionId, imageUrl, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceQueryUrl.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
        }

        public Stream GetDistinctSectionId(string sectionIdStartWith, string platform)
        {
            string outputURL = ConfigurationManager.AppSettings["MinPriceoutput"];
            Dictionary<string, SectionInfo> sectionInfoDictionary = new Dictionary<string, SectionInfo>();

            string outputFileResponse = Utilities.ExecuteGetWebRequest(outputURL);
            List<MinPriceToolObject> minPriceToolOutput;

            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse).Where(x => x.isFromDesktop == true).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse).Where(x => x.isFromMobile == true).ToList();
            }
            else
            {
                minPriceToolOutput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(outputFileResponse);
            }

            SectionInfo sectionInfo;
            int countIn = minPriceToolOutput.Count;
            for (int count = 0; count < countIn; count++)
            {
                if (!string.IsNullOrEmpty(sectionIdStartWith))
                {
                    var secId = minPriceToolOutput[count].sectionId.ToLower();
                    if (!sectionInfoDictionary.ContainsKey(secId) && (secId.StartsWith(sectionIdStartWith.ToLower()) || string.IsNullOrWhiteSpace(secId)))
                    {
                        sectionInfo = new SectionInfo();
                        sectionInfo.displaySectionName = minPriceToolOutput[count].displaySectionName;
                        sectionInfo.image = minPriceToolOutput[count].image;
                        sectionInfo.sectionId = minPriceToolOutput[count].sectionId;
                        sectionInfoDictionary.Add(sectionInfo.sectionId, sectionInfo);
                    }
                }
                else
                {
                    sectionInfo = new SectionInfo();
                    sectionInfo.displaySectionName = minPriceToolOutput[count].displaySectionName;
                    sectionInfo.image = minPriceToolOutput[count].image;
                    sectionInfo.sectionId = minPriceToolOutput[count].sectionId;
                    if (!sectionInfoDictionary.ContainsKey(sectionInfo.sectionId))
                    {
                        sectionInfoDictionary.Add(sectionInfo.sectionId, sectionInfo);
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sectionInfoDictionary.Values)));
        }

        public Stream EditorUpdate(editUpdate editUpdate, string userId)
        {

            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            string outputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            string jsonObjectInput = Utilities.ExecuteGetWebRequest(outputUrl);
            minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);
            int countIn = minPriceToolInputObject.Count;
            int flag = 0;
            for (int test = 0; test < countIn; test++)
            {
                if (minPriceToolInputObject[test].sectionId.ToLower() == editUpdate.sectionId.ToLower() && minPriceToolInputObject[test].query.id.ToLower() == editUpdate.ID.ToLower())
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                        break;
                    }
                    else
                    {
                        throw new WebFaultException<string>("The combination of Section Id and ID already exists ", System.Net.HttpStatusCode.BadRequest);
                    }
                }
            }
            if (flag == 0)
            {
                for (int test = 0; test < countIn; test++)
                {
                    if (minPriceToolInputObject[test].queryId == editUpdate.queryId)
                    {
                        minPriceToolInputObject[test].sectionId = editUpdate.sectionId;
                        minPriceToolInputObject[test].displaySectionName = Utilities.RemoveDiacritics(editUpdate.displaySectionName);
                        minPriceToolInputObject[test].query.id = editUpdate.ID;
                        minPriceToolInputObject[test].query.description = Utilities.RemoveDiacritics(editUpdate.description);
                        flag = 1;
                    }
                }
            }
            IAmazonS3 client;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsInputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = awsBucketName,
                    Key = awsOutputFolderKeyName
                };
                request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                PutObjectResponse response2 = client.PutObject(request);
            }

            ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully EditorUpdate By the User - {1}", editUpdate.queryId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));

        }

        public Stream SaveorUpdateMinPrice(saveUpdateMinPrice saveUpdateMinPrice, string userId)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            IAmazonS3 client;
            var preUrl = String.Empty;
            preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrl(saveUpdateMinPrice.departureName, saveUpdateMinPrice.boardType, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.adults,
                                                    saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings, saveUpdateMinPrice.priceMin, saveUpdateMinPrice.priceMax,
                                                    saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds, saveUpdateMinPrice.dateOffSet, saveUpdateMinPrice.flexDates);
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixUrl(saveUpdateMinPrice.adults, saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings,
                                                    saveUpdateMinPrice.departureDate, saveUpdateMinPrice.dateMin, saveUpdateMinPrice.dateMax,
                                saveUpdateMinPrice.durationMin, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.destinationIds, saveUpdateMinPrice.priceMin,
                                saveUpdateMinPrice.priceMax, saveUpdateMinPrice.departureIds, saveUpdateMinPrice.tripadvisorrating, saveUpdateMinPrice.sort,
                                saveUpdateMinPrice.hotelKeysToExclude, saveUpdateMinPrice.channelId, saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds,
                                saveUpdateMinPrice.boardType);
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            if (saveUpdateMinPrice.isDynamic)
            {
                saveUpdateMinPrice.dynamicUrl = "sample url"; //TODO: Generate url dynamically.
            }
            else
            {
                saveUpdateMinPrice.dynamicUrl = string.Empty;
            }
            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.InsertNewObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl);
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.UpdateOldObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl);


                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Information);
                throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null \n Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetArtirixMinPriceNew(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates)
        {
            var preUrl = String.Empty;
            preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + channelName + "/" + destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(departureName, boardType, durationMax, adults, children, ratings, priceMin, priceMax,
                                                    hotelKeys, tradingNameIds, dateOffSet, flexDates);
            try
            {
                var paramsString = string.Empty;
                paramsString = Utilities.GetArtirixUrl(adults, children, ratings, departureDate, dateMin, dateMax,
                                                        durationMin, durationMax, destinationIds, priceMin, priceMax,
                                                        departureIds, tripadvisorrating, sort, hotelKeysToExclude,
                                                        channelId, hotelKeys, tradingNameIds, boardType);
                var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
                string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
                MinPriceView minPriceView = new MinPriceView();
                minPriceView = MinPriceTool.MinPriceResponseViewNew(artirixSearchUrl, preUrl, postUrl, departureDate, flexDates);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceView)));


            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
                throw new WebFaultException<string>("Artirix returned an error response", System.Net.HttpStatusCode.BadRequest);
            }


        }

        public Stream SaveorUpdateMinPriceNew(saveUpdateMinPrice saveUpdateMinPrice, string userId)
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
            IAmazonS3 client;
            var preUrl = String.Empty;
            preUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + saveUpdateMinPrice.channelName + "/" + saveUpdateMinPrice.destinationName;
            var postUrl = String.Empty;
            postUrl = Utilities.GetWebsitePostUrlNew(saveUpdateMinPrice.departureName, saveUpdateMinPrice.boardType, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.adults,
                                                    saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings, saveUpdateMinPrice.priceMin, saveUpdateMinPrice.priceMax,
                                                    saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds, saveUpdateMinPrice.dateOffSet, saveUpdateMinPrice.flexDates);
            var paramsString = string.Empty;
            paramsString = Utilities.GetArtirixUrl(saveUpdateMinPrice.adults, saveUpdateMinPrice.children, saveUpdateMinPrice.starRatings,
                                                    saveUpdateMinPrice.departureDate, saveUpdateMinPrice.dateMin, saveUpdateMinPrice.dateMax,
                                saveUpdateMinPrice.durationMin, saveUpdateMinPrice.durationMax, saveUpdateMinPrice.destinationIds, saveUpdateMinPrice.priceMin,
                                saveUpdateMinPrice.priceMax, saveUpdateMinPrice.departureIds, saveUpdateMinPrice.tripadvisorrating, saveUpdateMinPrice.sort,
                                saveUpdateMinPrice.hotelKeysToExclude, saveUpdateMinPrice.channelId, saveUpdateMinPrice.iff, saveUpdateMinPrice.tradingNameIds,
                                saveUpdateMinPrice.boardType);
            if (saveUpdateMinPrice.isDynamic)
            {
                saveUpdateMinPrice.dynamicUrl = "sample url"; //TODO: Generate url dynamically.
            }
            else
            {
                saveUpdateMinPrice.dynamicUrl = string.Empty;
            }
            var ArtirixServiceUrlFmt = Global.ArtirixServiceUrlFmt;
            string artirixSearchUrl = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"] + "search?" + paramsString;
            try
            {
                if (string.IsNullOrWhiteSpace(saveUpdateMinPrice.queryId)) // save the new object and push it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.InsertNewObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl);
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully Save By the User - {1}", minPriceToolInputObject.Last().queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }
                else // search in the input file and overide it to S3
                {

                    List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
                    minPriceToolInputObject = MinPriceTool.UpdateOldObjectToS3(artirixSearchUrl, saveUpdateMinPrice, preUrl, postUrl);


                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsInputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = awsOutputFolderKeyName
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }

                    ErrorLogger.Log(string.Format("Min Price Tool -- QueryId - {0} successfully UpdateMinPrice By the User - {1}", saveUpdateMinPrice.queryId, userId), LogLevel.Information);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject.OrderBy(o => o.sectionId).ThenBy(T => T.query.id).ToList())));
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Information);
                throw new WebFaultException<string>("The combination of SectionId and Id may exists \t or Section Id or Id may be null \n Please check the input Parameters again ", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetSectionObjectsV2(string sectionIds, string platform)
        {
            List<MinPriceToolObject> minPriceToolSectionObject = new List<MinPriceToolObject>();
            minPriceToolSectionObject = MinPriceTool.SectionObjectV2(sectionIds);
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
            string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];

            List<MinPriceToolObject> result = null;
            if (platform != null && platform.ToLower().Equals("desktop"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromDesktop == true).OrderBy(o => o.query.id).ToList();
            }
            else if (platform != null && platform.ToLower().Equals("mobile"))
            {
                result = minPriceToolSectionObject.Where(x => x.isFromMobile == true).OrderBy(o => o.query.id).ToList();
            }
            else
            {
                result = minPriceToolSectionObject.OrderBy(o => o.query.isDynamic).ToList();
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age= 1800");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            WebOperationContext.Current.OutgoingResponse.LastModified = DateTime.UtcNow;
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result)));

        }

        #endregion

        #region ElasticSearch

        public Stream ESHolidaySearch(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string personalizedParms ,string recentSearchCookie,string favoriteCookie)
        {
            StringBuilder errorLogger = new StringBuilder();
            #region Mandatory params check 
            //Destination Check
            if (destinationIds == 0)
            {
                throw new WebFaultException<string>("Destination cannot be Null", HttpStatusCode.BadRequest);
            }
            //Departure Check
            if (departureIds == 0)
            {
                throw new WebFaultException<string>("Departure cannot be Null", HttpStatusCode.BadRequest);
            }
            //DateMin Check 
            if (string.IsNullOrWhiteSpace(dateMin))
            {
                throw new WebFaultException<string>("DateMin cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMin, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMin cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            //DateMax Check
            if (string.IsNullOrWhiteSpace(dateMax))
            {
                throw new WebFaultException<string>("DateMax cannot be Null", HttpStatusCode.BadRequest);
            }
            else
            {
                if (DateTime.UtcNow > DateTime.ParseExact(dateMax, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                {
                    throw new WebFaultException<string>("DateMax cannot be in the past. Must be in the future", HttpStatusCode.BadRequest);
                }
            }
            #endregion
            #region Defaults
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                List<string> HotelKeys = hotelKeys.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(HotelKeys).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
            }
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                List<string> hKTE = hotelKeysToExclude.Split(',').ToList();
                if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                {
                    List<string> uPHK = usersPreferredHotelKeys.Split(',').ToList();
                    uPHK = uPHK.Except(hKTE).ToList();
                    usersPreferredHotelKeys = string.Join(",", uPHK);
                }
            }
            #endregion
            #region Building the API Search Request Object
            APISearchRequest searchRequest = new APISearchRequest(destinationIds, departureIds, boardType, departureDate, dateMin,
                                       dateMax, durationMin, durationMax, adults, children, priceMin,
                                       priceMax, ratings, tradingNameIds, destinationType, hotelKeys,
                                       tripAdvisorRating, hotelKeysToExclude, sort, channelId, labelId,
                                       usersPreferredHotelKeys, skipFacets, personalizedParms, recentSearchCookie, favoriteCookie);

            #endregion
            SearchRequestAvailability searchRequestAvailability = ExecuteQuery.CheckRequestInSearchIndice(searchRequest, errorLogger);
            HolidayOffers holidayOffers = new HolidayOffers();
            holidayOffers = OffersBuilder.GenerateOffers(searchRequestAvailability, searchRequest, errorLogger);
            if (!string.IsNullOrWhiteSpace(sort))
            {
                if (sort.StartsWith("trip"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.hotel.rating.averageRating).ThenBy(t => t.price).ToList();
                    }
                }
                else if(sort.StartsWith("rating"))
                {
                    if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(u => u.isPreferredByUser).ThenByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                    else
                    {
                        holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.accommodation.rating).ThenBy(t => t.price).ToList();
                    }
                }
            }
            if(searchRequest.personalizedParms.Count !=0)
            {
                holidayOffers.offers = holidayOffers.offers.OrderByDescending(o => o.relavanceScore).ToList();
            }
            if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            {
                Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holidayOffers)));
        }

        public Stream ESDiversity(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                           int children, string ratings, string hotelKeys, string tradingNameIds)
        {
            StringBuilder errorLogger = new StringBuilder();
            APIDiversityRequest apiDiversityRequest = new APIDiversityRequest();
            #region Defaults
            if (adults == 0)
            {
                apiDiversityRequest.adults = 2;
            }
            else
            {
                apiDiversityRequest.adults = adults;
            }
            apiDiversityRequest.children = children;
            apiDiversityRequest.departureIds = departureIds;
            apiDiversityRequest.dateMax = dateMax;
            apiDiversityRequest.dateMin = dateMin;
            if (durationMax == 0)
            {
                apiDiversityRequest.durationMax = 7;
                apiDiversityRequest.durationMin = 7;
            }
            else
            {
                apiDiversityRequest.durationMax = durationMax;
                apiDiversityRequest.durationMin = durationMin;
            }
            if (string.IsNullOrWhiteSpace(ratings))
            {
                apiDiversityRequest.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                apiDiversityRequest.ratings = ratings.Split(',').ToList();
            }
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                apiDiversityRequest.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();
            }
            else
            {
                apiDiversityRequest.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            if (destinationId == 0)
            {
                DiversityError error = new DiversityError();
                error.error = "A destinationId is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.destinationId = destinationId;
            }
            if (string.IsNullOrWhiteSpace(hotelKeys))
            {
                DiversityError error = new DiversityError();
                error.error = "At least 1 hotelKey is required";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
            else
            {
                apiDiversityRequest.hotelKeys = hotelKeys.Split(',').ToList();
            }
            #endregion
            Diversity diversity = new Diversity();
            diversity = OffersBuilder.GenerateDiversityOffers(apiDiversityRequest, errorLogger);
            if (!string.IsNullOrWhiteSpace(errorLogger.ToString()))
            {
                Task.Factory.StartNew(() => ErrorLogger.Log(errorLogger.ToString(), LogLevel.ElasticSearch));
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(diversity)));
        }

        public Stream TAReviews(string taid)
        {
            string taReviews = string.Empty;
            string tsReviewsInsertionTime = string.Empty;
            string cacheTime = string.Empty;
            TAResponse taResponse = new TAResponse();
            TAReviews reviews = new TAReviews();
            ReviewsData reviewsData;
            tsReviewsInsertionTime = RedisDataHelper.GetRedisValueByKey(taid + "-Time", Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
            if (!string.IsNullOrWhiteSpace(tsReviewsInsertionTime))
            {
                if (DateTime.UtcNow < DateTime.Parse(tsReviewsInsertionTime.Substring(1, tsReviewsInsertionTime.Length - 2)).AddDays(Convert.ToInt32(ConfigurationManager.AppSettings["TAReviewsLiveTime"])))
                {
                    taReviews = RedisDataHelper.GetRedisValueByKey(taid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
                    taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                    reviews.data = new List<ReviewsData>();
                    foreach (Datum data in taResponse.data)
                    {
                        reviewsData = new ReviewsData();
                        reviewsData.published_date = data.published_date;
                        reviewsData.rating = data.rating;
                        reviewsData.title = data.title;
                        reviewsData.text = data.text;
                        reviewsData.subratings = data.subratings;
                        reviewsData.user = data.user;
                        reviewsData.user.username = data.user.username;
                        reviewsData.user.user_location.name = data.user.user_location.name;
                        reviews.data.Add(reviewsData);
                    }
                    cacheTime = (DateTime.Parse(tsReviewsInsertionTime.Substring(1, tsReviewsInsertionTime.Length - 2)).AddDays(1) - DateTime.UtcNow).TotalSeconds.ToString();
                }
                else
                {
                    string taURL = ConfigurationManager.AppSettings["TAURL"] + taid + "/reviews?key=" + ConfigurationManager.AppSettings["TAKey"] + "&isTA=true";
                    var response = Utilities.ExecuteTAGetWebRequest(taURL);
                    if (!string.IsNullOrWhiteSpace(response))
                    {
                        taResponse = JsonConvert.DeserializeObject<TAResponse>(response);
                        reviews.data = new List<ReviewsData>();
                        foreach (Datum data in taResponse.data)
                        {
                            reviewsData = new ReviewsData();
                            reviewsData.published_date = data.published_date;
                            reviewsData.rating = data.rating;
                            reviewsData.title = data.title;
                            reviewsData.text = data.text;
                            reviewsData.subratings = data.subratings;
                            reviewsData.user = data.user;
                            reviewsData.user.username = data.user.username;
                            reviewsData.user.user_location.name = data.user.user_location.name;
                            reviews.data.Add(reviewsData);
                        }
                        RedisDataHelper.InsertTAValue(taid, response, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                        Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]),
                        (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                        RedisDataHelper.SetTAReviewsTime(taid + "-Time", DateTime.UtcNow.ToString("s"), ConfigurationManager.AppSettings["RedisDBForTAReviews"]);
                        cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                    }
                    else
                    {
                        taReviews = RedisDataHelper.GetRedisValueByKey(taid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]));
                        taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                        reviews.data = new List<ReviewsData>();
                        foreach (Datum data in taResponse.data)
                        {
                            reviewsData = new ReviewsData();
                            reviewsData.published_date = data.published_date;
                            reviewsData.rating = data.rating;
                            reviewsData.title = data.title;
                            reviewsData.text = data.text;
                            reviewsData.subratings = data.subratings;
                            reviewsData.user = data.user;
                            reviewsData.user.username = data.user.username;
                            reviewsData.user.user_location.name = data.user.user_location.name;
                            reviews.data.Add(reviewsData);
                        }
                    }
                }
            }
            else
            {
                string taURL = ConfigurationManager.AppSettings["TAURL"] + taid + "/reviews?key=" + ConfigurationManager.AppSettings["TAKey"] + "&isTA=true";
                taReviews = Utilities.ExecuteTAGetWebRequest(taURL);
                if (!string.IsNullOrWhiteSpace(taReviews))
                {
                    taResponse = JsonConvert.DeserializeObject<TAResponse>(taReviews);
                    reviews.data = new List<ReviewsData>();
                    foreach (Datum data in taResponse.data)
                    {
                        reviewsData = new ReviewsData();
                        reviewsData.published_date = data.published_date;
                        reviewsData.rating = data.rating;
                        reviewsData.title = data.title;
                        reviewsData.text = data.text;
                        reviewsData.subratings = data.subratings;
                        reviewsData.user = data.user;
                        reviewsData.user.username = data.user.username;
                        reviewsData.user.user_location.name = data.user.user_location.name;
                        reviews.data.Add(reviewsData);
                    }
                    cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                    RedisDataHelper.InsertTAValue(taid, taReviews, ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForTAReviews"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                    RedisDataHelper.SetTAReviewsTime(taid + "-Time", DateTime.UtcNow.ToString("s"), ConfigurationManager.AppSettings["RedisDBForTAReviews"]);
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
        }

        public Stream UpdateTAInfo()
        {
            string configUrl = ConfigurationManager.AppSettings["TAConfigURL"];
            string configResponse = Utilities.ExecuteGetWebRequest(configUrl);
            TAConfig taConfig = JsonConvert.DeserializeObject<TAConfig>(configResponse);
            string taUpdateUrl = ConfigurationManager.AppSettings["TAUpdateURL"];
            string taUpdateResponse = Utilities.ExecuteGetWebRequest(taUpdateUrl);
            List<TA> taInputModelResponse = JsonConvert.DeserializeObject<List<TA>>(taUpdateResponse);
            TAUpdateList taList = new TAUpdateList();
            taList.updatedMHUIDs = new List<int>();
            taList.updatedMHUIDs = (from taInputModel in taInputModelResponse
                                    where taInputModel.lastModifiedDate.Equals(taConfig.dateTimeLastUpdated)
                                    select taInputModel.masterHotelId).ToList();
            taList.count = taList.updatedMHUIDs.Count;
            MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            TAInfo taInfo = new TAInfo();
            Task.Factory.StartNew(() =>
            {
                for (int Count = 0; Count < taInputModelResponse.Count; Count++)
                {
                    try
                    {
                        if (taConfig.dateTimeLastUpdated == taInputModelResponse[Count].lastModifiedDate)
                        {
                            string hotelInfo = RedisDataHelper.GetRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));
                            if (!string.IsNullOrWhiteSpace(hotelInfo))
                            {
                                mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(hotelInfo);
                                var mhidTAUrl = ConfigurationManager.AppSettings["MHIDTAInfo"] + taInputModelResponse[Count].masterHotelId + ".json";
                                var mhidTAInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUrl);
                                if (!string.IsNullOrWhiteSpace(mhidTAInfoResponse))
                                {
                                    taInfo = JsonConvert.DeserializeObject<TAInfo>(mhidTAInfoResponse);
                                    mhidStaticInfo.TripAdvisor.reviewsCount = !string.IsNullOrEmpty(taInfo.numberOfReviews) ? int.Parse(taInfo.numberOfReviews) : 0;
                                    mhidStaticInfo.TripAdvisor.TARating = !string.IsNullOrEmpty(taInfo.rating) ? double.Parse(taInfo.rating) : 0;
                                    mhidStaticInfo.insertedTime = DateTime.UtcNow.ToString("s");
                                }
                                RedisDataHelper.SetHotelInfoRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), mhidStaticInfo, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));
                            }
                            string staticHotelInfo = RedisDataHelper.GetRedisValueByKey(taInputModelResponse[Count].masterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
                            if (!string.IsNullOrWhiteSpace(staticHotelInfo))
                            {
                                staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(staticHotelInfo);
                                var mhidTAUrl = ConfigurationManager.AppSettings["MHIDTAInfo"] + taInputModelResponse[Count].masterHotelId + ".json";
                                var mhidTAInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidTAUrl);
                                if (!string.IsNullOrWhiteSpace(mhidTAInfoResponse))
                                {
                                    taInfo = JsonConvert.DeserializeObject<TAInfo>(mhidTAInfoResponse);
                                    staticResponseParms.reviewCount = !string.IsNullOrEmpty(taInfo.numberOfReviews) ? int.Parse(taInfo.numberOfReviews) : 0;
                                    staticResponseParms.averageRating = !string.IsNullOrEmpty(taInfo.rating) ? float.Parse(taInfo.rating) : 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log(ex.Message + "---1", LogLevel.ElasticSearch);
                    }
                }
            });
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(taList)));
        }
        public Stream AkamaiPurge(AkamaiPurge akamaipurge)
        {
            List<string> emailIds = new List<string>();
            emailIds.Add("nithin@teletext-holidays.co.uk");
            if (emailIds.Contains(akamaipurge.email))
            {
                Akamai akamai = new Akamai();
                akamai.objects = new List<string>();
                akamai.objects.Add(akamaipurge.url);
                //akamai.action = "remove";
                akamai.type = "arl";
                //akamai.domain = "production";
                var bulk = JsonConvert.SerializeObject(akamai);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.ccu.akamai.com/ccu/v2/queues/default");
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://private-anon-e97b54ddc0-akamaiopen2purgeccuv2production.apiary-mock.com/ccu/v2/queues/default");
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                byte[] bytes = Encoding.UTF8.GetBytes(bulk);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                var result1 = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();
                Utilities.TriggerMail(akamaipurge.url, 1, akamaipurge.email);
            }
            else
            {
                Utilities.TriggerMail(akamaipurge.url, 2, akamaipurge.email);
            }

            return null;
        }

        public Stream GetMasterHotelInfo(string mhid)
        {
            string cacheTime = string.Empty;
            MHIDStaticInfo mhidStaticInfo = Utilities.GetMasterHotelInfo(mhid, out cacheTime);
            if (mhidStaticInfo == null)
            {
                throw new WebFaultException<string>("GetMasterHotelInfo: MasterHotelId Does not exist:" + mhid, System.Net.HttpStatusCode.BadRequest);
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneDay"]);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhidStaticInfo)));
        }

        public Stream PostMasterHotelInfo(MHIDStaticInfo mhidStaticInfo, string userId)
        {
            //try
            //{            
            string cacheTime = string.Empty;
            var existingMasterHotelObject = Utilities.GetMasterHotelInfo(mhidStaticInfo._source.MasterHotelId.ToString(), out cacheTime);
            if (existingMasterHotelObject == null)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotelId Does not exist:" + mhidStaticInfo._source.MasterHotelId, System.Net.HttpStatusCode.BadRequest);
            }

            if (mhidStaticInfo.ETag != existingMasterHotelObject.ETag)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
            }

            var isEtagUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], mhidStaticInfo._source.MasterHotelId + ".json", mhidStaticInfo, string.Format("{0}/", ConfigurationManager.AppSettings["MasterHotelFolderPath"]));
            if (string.IsNullOrEmpty(isEtagUpdated))
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel unable to update.", System.Net.HttpStatusCode.BadRequest);
            }

            mhidStaticInfo.ETag = isEtagUpdated;
            mhidStaticInfo.insertedTime = DateTime.UtcNow.ToString("s");
            var masterHotelDataUpdated = RedisDataHelper.SetHotelInfoRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), mhidStaticInfo, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHIDInfo"]));

            if (!masterHotelDataUpdated)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
            }

            MHDescription mhDescription = new MHDescription();
            mhDescription.insertedTime = DateTime.UtcNow.ToString("s");
            mhDescription.description = mhidStaticInfo._source.Description;
            var masterHotelDescUpdated = RedisDataHelper.SetHotelDescriptionRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), mhDescription, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));

            if (!masterHotelDescUpdated)
            {
                throw new WebFaultException<string>("PostMasterHotelInfo: MasterHotel Desc not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
            }

            string staticHotelInfo = RedisDataHelper.GetRedisValueByKey(mhidStaticInfo._source.MasterHotelId.ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));
            if (!string.IsNullOrWhiteSpace(staticHotelInfo))
            {
                var staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(staticHotelInfo);
                staticResponseParms.features = mhidStaticInfo._source.Features;
                staticResponseParms.name = mhidStaticInfo._source.BuildingName;
                var staticHotelInfoUpdated = RedisDataHelper.SetStaticHotelInfoByKey(mhidStaticInfo._source.MasterHotelId.ToString(), staticResponseParms, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]));

                if (!staticHotelInfoUpdated)
                {
                    throw new WebFaultException<string>("PostMasterHotelInfo: Static Hotel Info not updated to Redis.", System.Net.HttpStatusCode.BadRequest);
                }
            }

            ErrorLogger.Log(string.Format("PostMasterHotelInfo - {0} successfully updated By the User - {1}", mhidStaticInfo._source.MasterHotelId, userId), LogLevel.Information);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhidStaticInfo)));
            //}
            //catch (Exception ex)
            //{
            //    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
            //    throw new WebFaultException<string>("SaveGuideInformation returned an error response", System.Net.HttpStatusCode.BadRequest);
            //}
        }

        public Stream ProcessMasterHotelImages(string MHID, string MasterHotelImages, string userId, string MHETag)
        {
            string cacheTime = string.Empty;
            var existingMasterHotelObject = Utilities.GetMasterHotelInfo(MHID, out cacheTime); //Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
            if (!existingMasterHotelObject.ETag.Contains(MHETag))
            {
                throw new WebFaultException<string>("ProcessMasterHotelImages: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
            }

            List<string> mHotelImages = MasterHotelImages.Split(',').ToList();
            if (mHotelImages == null || mHotelImages.Count() == 0)
            {
                throw new WebFaultException<string>("ProcessMasterHotelImages: Images are empty", System.Net.HttpStatusCode.BadRequest);
            }

            //Delete Object from S3
            foreach (var mHotelImage in mHotelImages)
            {
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID));
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID));
                Utilities.DeleteObjectToS3(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], mHotelImage + ".jpg", string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID));
            }

            //Rename the Images.
            var imageCount = Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID));
            var thumbnailImages = Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID));
            Utilities.RenameMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID));
            //Update the images count in MasterHotel JSON object.            
            MHIDStaticInfo mhidStaticInfo = Utilities.GetMasterHotelInfo(MHID, out cacheTime);
            //            var mhidStaticInfo = GetMasterHotelInfoTest(MHID);// Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
            mhidStaticInfo._source.ImageCount = imageCount;
            //Update the JSON into Redis
            ErrorLogger.Log(string.Format("ProcessMasterHotelImages - {0} successfully updated By the User - {1}", MHID, userId), LogLevel.Information);
            return PostMasterHotelInfo(mhidStaticInfo, userId);
        }

        public Stream GetMasterHotelDescription(string mhid)
        {
            MHDescription mhDescription = new MHDescription();
            MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
            string cacheTime = string.Empty;
            string description = string.Empty;
            description = RedisDataHelper.GetRedisValueByKey(mhid, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));
            //mhDescription = JsonConvert.DeserializeObject<MHDescription>(description);
            //var insertionDate = (DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime));
            if (string.IsNullOrWhiteSpace(description) || ((DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime)).AddDays(1) - DateTime.UtcNow).TotalSeconds < 0)
            {
                var mhidURL = ConfigurationManager.AppSettings["MHIDStaticInfo"] + mhid + ".json";
                var mhidStaticInfoResponse = Utilities.ExecuteTAGetWebRequest(mhidURL);
                if (!string.IsNullOrEmpty(mhidStaticInfoResponse))
                {
                    mhidStaticInfo = JsonConvert.DeserializeObject<MHIDStaticInfo>(mhidStaticInfoResponse);
                    mhDescription.description = mhidStaticInfo._source.Description;
                    mhDescription.insertedTime = DateTime.UtcNow.ToString("s");
                    RedisDataHelper.SetHotelDescriptionRedisValueByKey(mhid, mhDescription, Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]));
                    cacheTime = ConfigurationManager.AppSettings["CacheTimeOutOneDay"];
                }
            }
            else
            {
                mhDescription = JsonConvert.DeserializeObject<MHDescription>(description);
                var insertionDate = (DateTime.Parse(JsonConvert.DeserializeObject<MHDescription>(description).insertedTime));
                if ((insertionDate.AddDays(1) - DateTime.UtcNow).TotalSeconds > 0)
                {
                    cacheTime = ((insertionDate.AddDays(1) - DateTime.UtcNow).TotalSeconds).ToString();
                }
            }
            WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(mhDescription)));
        }

        public string AddMasterHotelTag(string tagName, string userId)
        {
            var masterHotelTagsJSON = string.Empty;
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelTagsMasterJSON"])))
            {
                masterHotelTagsJSON = reader.ReadToEnd();
            }
            List<string> masterHotelTags = JsonConvert.DeserializeObject<List<string>>(masterHotelTagsJSON);
            var isTagExists = masterHotelTags.Where(p => p == tagName);
            if (isTagExists != null && isTagExists.Count() > 0)
            {
                return tagName;
            }

            masterHotelTags.Add(tagName);
            var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelTagsMasterJSON"], masterHotelTags.OrderBy(p => p));
            ErrorLogger.Log(string.Format("AddMasterHotelTag - {0} successfully added By the User - {1}", tagName, userId), LogLevel.Information);
            return tagName;
        }

        public string AddMasterHotelFacilities(string facilityName, string userId)
        {
            var masterHotelFacilityJSON = string.Empty;
            using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelFacililtiesMasterJSON"])))
            {
                masterHotelFacilityJSON = reader.ReadToEnd();
            }
            List<string> masterHotelFacilities = JsonConvert.DeserializeObject<List<string>>(masterHotelFacilityJSON);
            var isTagExists = masterHotelFacilities.Where(p => p == facilityName);
            if (isTagExists != null && isTagExists.Count() > 0)
            {
                return facilityName;
            }

            masterHotelFacilities.Add(facilityName);
            var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], ConfigurationManager.AppSettings["MasterHotelFacililtiesMasterJSON"], masterHotelFacilities.OrderBy(p => p));
            ErrorLogger.Log(string.Format("AddMasterHotelFacilities - {0} successfully added By the User - {1}", facilityName, userId), LogLevel.Information);
            return facilityName;
        }

        public Stream SwapMasterHotelImages(string MHID, string destImage, string MHETag, string userId)
        {
            MHIDStaticInfo existingMasterHotelObject = null;
            try
            {
                string cacheTime = string.Empty;
                existingMasterHotelObject = Utilities.GetMasterHotelInfo(MHID, out cacheTime);
                //existingMasterHotelObject =  Utilities.DownloadS3ObjectForMasterHotel(ConfigurationManager.AppSettings["MasterHotelAWSBucket"], string.Format("{0}/{1}.json", ConfigurationManager.AppSettings["MasterHotelFolderPath"], MHID));
                if (!existingMasterHotelObject.ETag.Contains(MHETag))
                {
                    throw new WebFaultException<string>("ProcessMasterHotelImages: MasterHotel Concurrency Exception", System.Net.HttpStatusCode.BadRequest);
                }
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh460w840FolderPath"], MHID), destImage);
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh175w244FolderPath"], MHID), destImage);
                Utilities.SwapingMasterHotelImages(ConfigurationManager.AppSettings["MasterHotelImagesAWSBucket"], string.Format(ConfigurationManager.AppSettings["MasterHotelImageh205w286FolderPath"], MHID), destImage);
                ErrorLogger.Log(string.Format("ProcessMasterHotelImages - {0} successfully updated By the User - {1}", MHID, userId), LogLevel.Information);
            }
            catch (Exception ex)
            { }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(existingMasterHotelObject)));
        }

        public void CreateTimeBasedIndices()
        {
            string searchIndice = ConfigurationManager.AppSettings["SearchIndice"];
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string diversityIndice = ConfigurationManager.AppSettings["DiversityIndice"];
            string searchDataType = ConfigurationManager.AppSettings["SearchIndiceDataType"];
            string diversityDataType = ConfigurationManager.AppSettings["DiversityIndiceDataType"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            int t = 0;
            if (Utilities.ExecuteGetIndiceWebRequest(searchIndice, searchDataType))
            {
                t++;
            }
            if (Utilities.ExecuteGetIndiceWebRequest(resultsIndice, resultsDataType))
            {
                t++;
            }
            if (Utilities.ExecuteGetIndiceWebRequest(diversityIndice, diversityDataType))
            {
                t++;
            }
            Utilities.TriggerIndiceInfoMail(t);
        }

        public void DeleteTimeBasedIndices()
        {
            string searchIndice = ConfigurationManager.AppSettings["SearchIndice"];
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string diversityIndice = ConfigurationManager.AppSettings["DiversityIndice"];
            string searchDataType = ConfigurationManager.AppSettings["SearchIndiceDataType"];
            string diversityDataType = ConfigurationManager.AppSettings["DiversityIndiceDataType"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            Utilities.DeleteTimeBasedIndice(searchIndice, searchDataType, -7);
            Utilities.DeleteTimeBasedIndice(resultsIndice, resultsDataType, -1);
            Utilities.DeleteTimeBasedIndice(diversityIndice, diversityDataType, -1);
        }
        public void DeleteResultsIndexResults()
        {
            string resultsIndice = ConfigurationManager.AppSettings["ResultsIndice"];
            string resultsDataType = ConfigurationManager.AppSettings["ResultsIndiceDataType"];
            string clusterURL = ConfigurationManager.AppSettings["ES-ClusterUrl"];
            string webReqURL = clusterURL + resultsIndice + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + resultsDataType + "/_delete_by_query?conflicts=proceed";
            string postData = "{\"query\":{\"bool\":{\"must\":[{\"range\":{\"timeStamp\":{\"to\":\"" + DateTime.UtcNow.AddHours(-1).ToString("yyyy-MM-ddTHH:mm:ss") + "\"}}}]}}}";
            var result = Utilities.ExecuteESPostJsonWebRequest(webReqURL, postData);

        }

        #endregion

        #region Dynamic SEO

        public Stream GetCategoryList()
        {
            try
            {
                var categoryList = Utilities.GetCategoryList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(categoryList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetCategoryList returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream ProcessCategoryList(List<DynamicSEO.Category> categoryList, string userId)
        {
            try
            {
                var masterCategoryList = Utilities.GetCategoryList();
                foreach (var category in categoryList)
                {
                    var baseCategory = masterCategoryList.Where(p => p.categoryId == category.categoryId);
                    if (baseCategory != null && baseCategory.Count() > 0)
                    {
                        masterCategoryList.Remove(baseCategory.FirstOrDefault());
                    }

                    masterCategoryList.Add(category);
                }

                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterCategoryJSON"], masterCategoryList);
                //var isFileUpdated = Utilities.PutObjectToS3("teletext-deploy", "MasterCategory.json", masterCategoryList);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterCategoryList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("ProcessCategoryList returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetTemplateList()
        {
            try
            {
                var templateList = Utilities.GetTemplateList();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(templateList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetTemplateList returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream ProcessTemplateList(List<DynamicSEO.Template> templateList, string userId)
        {
            try
            {
                var masterTemplateList = Utilities.GetTemplateList();
                foreach (var template in templateList)
                {
                    var baseTemplate = masterTemplateList.Where(p => p.templateId == template.templateId);
                    if (baseTemplate != null && baseTemplate.Count() > 0)
                    {
                        masterTemplateList.Remove(baseTemplate.FirstOrDefault());
                    }

                    masterTemplateList.Add(template);
                }

                var isFileUpdated = Utilities.PutObjectToS3(ConfigurationManager.AppSettings["DynamicSEOBucket"], ConfigurationManager.AppSettings["MasterTemplateJSON"], masterTemplateList);
                //var isFileUpdated = Utilities.PutObjectToS3("teletext-deploy", "MasterTemplate.json", masterTemplateList);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(masterTemplateList)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("ProcessTemplateList returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public Stream GetHoliday(string fileName)
        {
            try
            {
                var holiday = Utilities.GetHoliday(fileName.Replace("/holidays/", ""));

                if (holiday != null)
                {
                    var guide = Guides.Guides.GetGuide(holiday.regionId);
                    holiday.bgImage = guide.bgimage;
                    List<DynamicSEO.PlacesToGo> guideSubSections = new List<DynamicSEO.PlacesToGo>();
                    if (guide.reglevel == "1")
                    {
                        guideSubSections = guide.link_summary[0].PlacesToGo.subsections.Take(3).Select(mn => new DynamicSEO.PlacesToGo { subheader = mn.subheader, thumbnailImageurl = mn.thumbnailImageurl }).ToList();
                    }
                    else
                    {
                        guideSubSections = guide.sections.Where(p => p.surl == "#places").FirstOrDefault().subsections.Take(3).Select(mn => new DynamicSEO.PlacesToGo { subheader = mn.subheader, thumbnailImageurl = mn.thumbnailImageurl }).ToList();
                    }

                    holiday.placesToGo = guideSubSections;
                }
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneDay"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(holiday)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetHoliday returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region GoogleAPI
        public Stream GetGoogleNearBy(float lati, float logi, string patternType, string type, string radius)
        {
            string lat = lati.ToString("0.00");
            string lon = logi.ToString("0.00");
            GoogleResponse googleResponse = new GoogleResponse();
            Result result;
            GoogleBARObject resultgoogleobject = new GoogleBARObject();
            if (string.IsNullOrWhiteSpace(radius))
            {
                radius = ConfigurationManager.AppSettings["radius"];
            }
            patternType = "Pattern";
            if (!string.IsNullOrWhiteSpace(type))
            {
                if (type.Contains(","))
                {
                    DiversityError error = new DiversityError();
                    error.error = "Please check the fields";
                    return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
                }
            }
            var googleNearBy = string.Empty;
            if (GoogleAPI.GetGoogleNearBy(lat, lon, patternType, type, radius) == true)
            {
                string attracts = type;
                if (String.IsNullOrWhiteSpace(type))
                {
                    attracts = "Attractions";
                }
                var googleS3URl = "https://s3-eu-west-1.amazonaws.com/teletext-googleapi/" + patternType.ToUpper() + "/" + attracts.ToUpper() + "/" + lat + "tt" + lon + ".json";
                var googleS3URLResponse = Utilities.ExecuteGetWebRequest(googleS3URl);
                googleResponse = JsonConvert.DeserializeObject<GoogleResponse>(googleS3URLResponse);
                resultgoogleobject.results = new List<Result>();
                foreach (GogResult rst in googleResponse.results)
                {
                    result = new Result();
                    result.geometry = new Geometry();
                    result.geometry.location = new Location();
                    result.geometry.location.lat = rst.geometry.location.lat.ToString();
                    result.geometry.location.lng = rst.geometry.location.lng.ToString();
                    result.id = rst.id;
                    result.name = rst.name;
                    result.rating = rst.rating.ToString();
                    result.vicinity = rst.vicinity;
                    resultgoogleobject.results.Add(result);
                }
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneMonth"]);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(resultgoogleobject)));
            }
            else
            {
                DiversityError error = new DiversityError();
                error.error = "Google didn't respond. Please try after sometime";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }
        }

        public Stream GetImage(string lat, string lng, string zoom)
        {
            bool isS3Present = true;
            bool isExpiry = false;
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSGoogleBucket"];
            if (string.IsNullOrWhiteSpace(zoom))
            {
                zoom = "6";
            }
            string key = "staticmaps/" + lat + "tt" + lng + "z" + zoom + ".png";
            IAmazonS3 client;
            var result = string.Empty;
            using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
            {
                try
                {
                    GetObjectRequest objRequest = new GetObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = key
                    };
                    GetObjectResponse objResponse = client.GetObject(objRequest);
                    if (objResponse.LastModified.AddMonths(1).Subtract(DateTime.UtcNow).TotalSeconds > 0)
                    {
                        WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + objResponse.LastModified.AddMonths(1).Subtract(DateTime.UtcNow).TotalSeconds);
                        WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                        return objResponse.ResponseStream;
                    }
                    else
                    {
                        isS3Present = true;
                        isExpiry = true;
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    isS3Present = false;
                }

            }
            if (!isS3Present || isExpiry)
            {
                try
                {
                    var webClient = new WebClient();
                    byte[] imageBytes = webClient.DownloadData("https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=" + zoom + "&size=640x304&maptype=roadmap&markers=color:red|label:.|" + lat + "," + lng + "&key=" + ConfigurationManager.AppSettings["GoogleStaticMapKey"]);
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = key
                        };
                        request.InputStream = new MemoryStream(imageBytes);
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneMonth"]);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                    return new MemoryStream(imageBytes);
                }
                catch (WebException ex)
                {
                    if (isExpiry)
                    {
                        Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                        {
                            BucketName = awsBucketName,
                            Key = key
                        };
                        GetObjectResponse objResponse = client.GetObject(objRequest);
                        WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                        return objResponse.ResponseStream;
                    }
                    else
                    {
                        DiversityError error = new DiversityError();
                        error.error = "Google didn't respond. Please try after sometime";
                        return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
                    }
                }
            }
            else
            {
                DiversityError error = new DiversityError();
                error.error = "Google didn't respond. Please try after sometime";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(error)));
            }

        }


        public void InvalidateUATCloudFront(string urlPaths)
        {
            List<string> paths = new List<string>();
            var invalidatePaths = urlPaths.Split(',');
            for (int i = 0; i < invalidatePaths.Length; i++)
            {
                paths.Add(invalidatePaths[i]);
            }
            Amazon.CloudFront.AmazonCloudFrontClient client = new Amazon.CloudFront.AmazonCloudFrontClient("AKIAJURLJ7MPSQG5JINQ", "5E26AjCm94YRaC42HL9Z31+hKakuoCkz7mBtPkv9", Amazon.RegionEndpoint.EUWest1);
            CreateInvalidationRequest request = new CreateInvalidationRequest();
            request.DistributionId = "E2HADNS0RYZ6AD";
            request.InvalidationBatch = new InvalidationBatch
            {
                CallerReference = DateTime.UtcNow.Ticks.ToString(),
                Paths = new Paths
                {
                    Items = paths,
                    Quantity = paths.Count
                }

            };
            CreateInvalidationResponse response = client.CreateInvalidation(request);
            client.Dispose();
        }

        public void InvalidatePRODCloudFront(string urlPaths)
        {
            List<string> paths = new List<string>();
            var invalidatePaths = urlPaths.Split(',');
            for (int i = 0; i < invalidatePaths.Length; i++)
            {
                paths.Add(invalidatePaths[i]);
            }
            Amazon.CloudFront.AmazonCloudFrontClient client = new Amazon.CloudFront.AmazonCloudFrontClient("AKIAIOLBY2H5PCCKK6DA", "fBgn5M/a0Vh4FXrOTIGj2j0ZSPw5ZhGS6UcKtoXE", Amazon.RegionEndpoint.EUWest1);
            CreateInvalidationRequest request = new CreateInvalidationRequest();
            request.DistributionId = "EYDRTIRGYAOZ";
            request.InvalidationBatch = new InvalidationBatch
            {
                CallerReference = DateTime.UtcNow.Ticks.ToString(),
                Paths = new Paths
                {
                    Items = paths,
                    Quantity = paths.Count
                }

            };
            CreateInvalidationResponse response = client.CreateInvalidation(request);
            client.Dispose();
        }


        #endregion

        #region Reviews        
        public Stream GetAllPublishedReviews(int limit, int startIndex, string reviewType)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(reviewType))
                {
                    reviewType = "all";
                }
                var reviews = ReviewController.GetAllPublishedReviews(limit, startIndex, reviewType);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetReviews returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream GetReviews(string limit, string startIndex, string reviewProvider, string showPublished, string showRatings)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(limit))
                {
                    limit = "30";
                }
                if (string.IsNullOrWhiteSpace(startIndex))
                {
                    startIndex = "0";
                }
                if (string.IsNullOrWhiteSpace(reviewProvider))
                {
                    reviewProvider = "all";
                }
                if (string.IsNullOrWhiteSpace(showPublished))
                {
                    showPublished = "all";
                }
                if (string.IsNullOrWhiteSpace(showRatings))
                {
                    showRatings = "1,2,3,4,5";
                }
                var reviews = ReviewController.GetReviews(limit, startIndex, reviewProvider, showPublished, showRatings);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reviews)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetReviews returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream PublishorUnpublishReviews(ReviewInformation review, string userId)
        {
            ResponseSummary response = new ResponseSummary();
            string operationDone = string.Empty;

            if (review.isPublished)
            {
                // review is published previously, and we are unpublishing it
                response = ReviewController.UnpublishReview(review);
                operationDone = "unpublished";
            }
            else
            {
                // review is unpublished previsouly, and we are publishing it

                response = ReviewController.PublishReview(review);
                operationDone = "published";
            }
            ErrorLogger.Log(string.Format("Reviews Modulation Tool -- Review Id - {0} - Review Title - {1} - successfully {2} by the User - {3}", review.reviewId, review.reviewTitle, operationDone, userId), LogLevel.CustomerReviews);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(response)));
        }
        #endregion

        # region HomePage
        public Stream GetNearestAirportGroup(string Latitude, string Longitude)
        {
            try
            {
                var airportGroupName = Utilities.GetNearestAirportGroup(double.Parse(Latitude), double.Parse(Longitude));
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(airportGroupName));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetNearestAirportGroup returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region DeepLinks

        public void GenerateDeepLinks()
        {
            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
            #region QuickQuotes
            string qqURL = "https://teletext-mobileapp-mybookings.s3.amazonaws.com/QuickQuotes/MobileAppFeed_QQ_" + DateTime.UtcNow.ToString("yyyyMMdd") + ".csv";
            string fileNameQQ = "Quotes-" + DateTime.UtcNow.ToString("yyyyMMdd") + ".json";
            Dictionary<string, DLStatus> quoteStatus = new Dictionary<string, DLStatus>();
            quoteStatus = JsonConvert.DeserializeObject<Dictionary<string, DLStatus>>(Utilities.ExecuteDeepLinkGetWebRequest("https://teletext-mobileapp-mybookings.s3.amazonaws.com/DeepLinkStatus/QuickQuotes/" + fileNameQQ));
            try
            {
                List<QuickQuotesDL> qqDl = new List<QuickQuotesDL>();
                List<Nexmo> nexmo = new List<Nexmo>();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(qqURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    while (!reader.EndOfStream)
                    {
                        var result = reader.ReadLine().Split(',');

                        if (result[15].ToLower() == "\"valid\"")
                        {
                            if (!quoteStatus.ContainsKey(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')))
                            {

                                try
                                {
                                    string MobileNumber = "918947052876";  /*result[10].TrimStart('\\').TrimStart('"').TrimEnd('"')*/
                                    if (MobileNumber.Length == 12)
                                    {
                                        QuickQuotesDL qqdl = new QuickQuotesDL();
                                        DLStatus dlStatus = new DLStatus();
                                        dlStatus.status = false;
                                        qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                        qqdl.campaign = "MyBooking_Live";
                                        qqdl.channel = "sms";
                                        DataDL dataDL = new DataDL();
                                        dlStatus.surname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dlStatus.deptDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dlStatus.mobileNumber = MobileNumber;
                                        dlStatus.reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                        dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                        dataDL.Source = "Quote";
                                        //dataDL.Source = "MyBooking";
                                        qqdl.data = dataDL;
                                        string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                        Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                        var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + MobileNumber + "&from=Teletext&text=" + branch.url);
                                        if (nexmoResponse.Contains("\"status\": \"0\""))
                                        {
                                            dlStatus.status = true;
                                            quoteStatus.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                        else
                                        {
                                            dlStatus.status = false;
                                            quoteStatus.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                                }
                            }
                            else
                            {
                                if (quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')].status == false)
                                {
                                    var dlStatus = quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')];
                                    QuickQuotesDL qqdl = new QuickQuotesDL();
                                    qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                    qqdl.campaign = "MyBooking_Live";
                                    qqdl.channel = "sms";
                                    DataDL dataDL = new DataDL();
                                    dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                    dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT-QQ-", "");
                                    dataDL.Source = "Quote";
                                    qqdl.data = dataDL;
                                    string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                    Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                    var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + dlStatus.mobileNumber + "&from=Teletext&text=" + branch.url);
                                    if (nexmoResponse.Contains("\"status\": \"0\""))
                                    {
                                        dlStatus.status = true;
                                        quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                    else
                                    {
                                        dlStatus.status = false;
                                        quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                }
                            }
                        }
                    }
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = "teletext-mobileapp-mybookings",
                            Key = "DeepLinkStatus/QuickQuotes/" + fileNameQQ
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(quoteStatus)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region MyBooking
            string mbURL = "https://teletext-mobileapp-mybookings.s3.amazonaws.com/ConfirmedBookings/MobileAppFeed_TT_" + DateTime.UtcNow.ToString("yyyyMMdd") + ".csv";
            string fileNameMB = "MyBooking-" + DateTime.UtcNow.ToString("yyyyMMdd") + ".json";
            Dictionary<string, DLStatus> quoteStatusMB = new Dictionary<string, DLStatus>();
            quoteStatusMB = JsonConvert.DeserializeObject<Dictionary<string, DLStatus>>(Utilities.ExecuteDeepLinkGetWebRequest("https://teletext-mobileapp-mybookings.s3.amazonaws.com/DeepLinkStatus/MyBookings/" + fileNameMB));
            try
            {
                List<QuickQuotesDL> qqDl = new List<QuickQuotesDL>();
                List<Nexmo> nexmo = new List<Nexmo>();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(mbURL);
                httpWebRequest.Proxy = null;
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    while (!reader.EndOfStream)
                    {
                        var result = reader.ReadLine().Split(',');

                        if (result[15].ToLower() == "\"valid\"")
                        {
                            if (!quoteStatusMB.ContainsKey(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')))
                            {

                                try
                                {
                                    string MobileNumber = "918947052876";  /*result[10].TrimStart('\\').TrimStart('"').TrimEnd('"')*/
                                    if (MobileNumber.Length == 12)
                                    {
                                        QuickQuotesDL qqdl = new QuickQuotesDL();
                                        DLStatus dlStatus = new DLStatus();
                                        dlStatus.status = false;
                                        qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                        qqdl.campaign = "MyBooking_Live";
                                        qqdl.channel = "sms";
                                        DataDL dataDL = new DataDL();
                                        dlStatus.surname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                        dlStatus.deptDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                        dlStatus.mobileNumber = MobileNumber;
                                        dlStatus.reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                        dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                        dataDL.Source = "MyBooking";
                                        qqdl.data = dataDL;
                                        string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                        Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                        var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + MobileNumber + "&from=Teletext&text=" + branch.url);
                                        if (nexmoResponse.Contains("\"status\": \"0\""))
                                        {
                                            dlStatus.status = true;
                                            quoteStatusMB.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                        else
                                        {
                                            dlStatus.status = false;
                                            quoteStatusMB.Add(result[1].TrimStart('\\').TrimStart('"').TrimEnd('"'), dlStatus);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                                }
                            }
                            else
                            {
                                if (quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')].status == false)
                                {
                                    var dlStatus = quoteStatus[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')];
                                    QuickQuotesDL qqdl = new QuickQuotesDL();
                                    qqdl.branch_key = "key_live_bhjZ0KKIHfahLYTVnxicadgoFBhIvZvI";
                                    qqdl.campaign = "MyBooking_Live";
                                    qqdl.channel = "sms";
                                    DataDL dataDL = new DataDL();
                                    dataDL.BookerSurname = result[9].TrimStart('\\').TrimStart('"').TrimEnd('"');
                                    dataDL.DepDate = DateTime.Parse(result[11].TrimStart('\\').TrimStart('"').TrimEnd('"')).ToString("dd MMM yyyy");
                                    dataDL.Reference = result[1].TrimStart('\\').TrimStart('"').TrimEnd('"').Replace("TT", "");
                                    dataDL.Source = "Quote";
                                    qqdl.data = dataDL;
                                    string branchResponse = Utilities.ExecutePostWebRequest("https://api.branch.io/v1/url/", JsonConvert.SerializeObject(qqdl));
                                    Branch branch = JsonConvert.DeserializeObject<Branch>(branchResponse);
                                    var nexmoResponse = Utilities.ExecuteGetWebRequest("https://rest.nexmo.com/sms/json?api_key=54cf8eed&api_secret=8203aed2&to=" + dlStatus.mobileNumber + "&from=Teletext&text=" + branch.url);
                                    if (nexmoResponse.Contains("\"status\": \"0\""))
                                    {
                                        dlStatus.status = true;
                                        quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                    else
                                    {
                                        dlStatus.status = false;
                                        quoteStatusMB[result[1].TrimStart('\\').TrimStart('"').TrimEnd('"')] = dlStatus;
                                    }
                                }
                            }
                        }
                    }
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        PutObjectRequest request = new PutObjectRequest()
                        {
                            BucketName = "teletext-mobileapp-mybookings",
                            Key = "DeepLinkStatus/MyBookings/" + fileNameMB
                        };
                        request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(quoteStatusMB)));
                        PutObjectResponse response2 = client.PutObject(request);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        public Stream SendAPPLink(APPLink applink)
        {
            string nexmoURL = string.Format(ConfigurationManager.AppSettings["NexmoUrl"], applink.mobileNumber, applink.from, applink.subject);
            var nexmoResponse = Utilities.ExecuteGetWebRequest(nexmoURL);
            return new MemoryStream(Encoding.UTF8.GetBytes(nexmoResponse));
        }




        #endregion

        #region LambdaHits
        public void GenerateRecentBlogsFromDB(string procedureName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(procedureName))
                {
                    throw new WebFaultException<string>("procedureName parameter cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                GetRecentBlogsBL.GenerateRecentBlogsFromDBController(procedureName);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GenerateRecentBlogsFromDB returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }

        public void GenerateBlogsByTagsFromDB(string procedureName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(procedureName))
                {
                    throw new WebFaultException<string>("procedureName parameter cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                GetRecentBlogsBL.GenerateTagSpecificRecentBlogsFromDB(procedureName);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace: {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GenerateBlogsByTagsFromDB returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Blogs
        public Stream GetRecentBlogs(string pageName)
        {
            try
            {
                var recentBlogs = GetRecentBlogsBL.GetRecentBlogsWebController(pageName);
                double cacheTime = 0;

                if (recentBlogs.Count > 0)
                {
                    string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                    string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                    string awsBucket = string.Empty;
                    string awsKeyName = string.Empty;

                    DateTime lastCreated = DateTime.UtcNow;
                    IAmazonS3 client;

                    if (string.IsNullOrWhiteSpace(pageName))
                    {
                        awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                        awsKeyName = ConfigurationManager.AppSettings["RecentBlogsFilePath"];

                    }
                    else if (!string.IsNullOrWhiteSpace(pageName))
                    {
                        awsBucket = ConfigurationManager.AppSettings["BlogsBucket"];
                        awsKeyName = ConfigurationManager.AppSettings["PageSpecificBlogsFilePath"] + pageName.Replace(" ", "-").ToLower().Trim() + ".json";
                    }
                    using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                    {
                        Amazon.S3.Model.GetObjectRequest objRequest = new GetObjectRequest()
                        {
                            BucketName = awsBucket,
                            Key = awsKeyName
                        };
                        Amazon.S3.Model.GetObjectResponse objResponse = client.GetObject(objRequest);
                        lastCreated = objResponse.LastModified.AddHours(6);
                        cacheTime = Math.Ceiling(lastCreated.Subtract(DateTime.UtcNow).TotalSeconds);
                        if (cacheTime < 0)
                        {
                            cacheTime = 3600;
                        }
                    }
                }

                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + cacheTime.ToString());
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(recentBlogs)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & Details: {1} & StackTrace; {2}", ex.Message, ex.Data, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }
        #endregion

        #region HotelLandingPage
        public Stream GetMasterHotelInfoByUrl(string url)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(url))
                {
                    throw new WebFaultException<string>("Parameter url cannot be null.", System.Net.HttpStatusCode.BadRequest);
                }
                var hotelInfo = StaticHotelLandingController.GetHotelInfoByUrl(url);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                // WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(hotelInfo)));
            }
            catch (WebFaultException wex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", wex.Message, wex.StackTrace), LogLevel.Error);
                throw wex;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetMasterHotelInfoByUrl returned an error response", System.Net.HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Payments
        public Stream ProcessPayment(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiKey"];
                string apiSecret = System.Configuration.ConfigurationManager.AppSettings["PaysafeApiSecret"];
                string accountNumber = System.Configuration.ConfigurationManager.AppSettings["PaysafeAccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PaysafeCurrencyBaseUnitsMultiplier"]);

                PaysafeApiClient client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);

                EnrollmentChecks enrollmentChecks = EnrollmentChecks.Builder()
                       .merchantRefNum(System.Guid.NewGuid().ToString())
                       .amount(Convert.ToInt32(TopDogManager.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber) * currencyBaseUnitsMultiplier))
                       .currency("GBP")
                       .customerIp(paymentDetails.customerIp)
                       .userAgent(paymentDetails.userAgent)
                       .card()
                           .cardNum(paymentDetails.CardNumber)
                           .cardExpiry()
                               .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                               .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                               .Done()
                           .Done()
                       .acceptHeader(paymentDetails.acceptHeader)
                       .merchantUrl(paymentDetails.merchantUrl)
                       .Build();

                EnrollmentChecks response = client.threeDSecureService().submit(enrollmentChecks);

                if (response.threeDEnrollment() == "N")
                {
                    Authorization auth = client.cardPaymentService().authorize(Authorization.Builder()
                        .merchantRefNum(response.merchantRefNum())
                        .amount(Convert.ToInt32(TopDogManager.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber) * currencyBaseUnitsMultiplier))
                        .settleWithAuth(true)
                       .card()
                           .cardNum(paymentDetails.CardNumber)
                           .cardExpiry()
                               .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                               .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                               .Done()
                           .Done()
                            .billingDetails()
                                .street(paymentDetails.Address1 + paymentDetails.Address2)
                                .city(paymentDetails.City)
                                .state(paymentDetails.State)
                                .country(paymentDetails.CountryCode)
                                .zip(paymentDetails.PostCode)
                                .Done()
                        .Build());

                    paymentDetails.IsThreeDSecure = false;

                    if (!string.IsNullOrEmpty(auth.id()) && auth.error() == null && auth.status() == "COMPLETED")
                    {
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = "";
                    }
                    else
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                }
                else
                {
                    paymentDetails.IsThreeDSecure = true;
                    paymentDetails.AcsUrl = response.acsURL();
                    paymentDetails.PaReq = response.paReq();
                }

                paymentDetails.enrollmentId = response.id();
                paymentDetails.TransactionID = response.merchantRefNum();
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("SaveGuideInformation returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public Stream ProcessThreeDSecurePayment(MakePayment paymentDetails)
        {
            try
            {
                var json = String.Empty;

                string apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
                string apiSecret = System.Configuration.ConfigurationManager.AppSettings["ApiSecret"];
                string accountNumber = System.Configuration.ConfigurationManager.AppSettings["AccountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrencyBaseUnitsMultiplier"]);

                PaysafeApiClient client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.TEST, accountNumber);

                Authentication authentications = Authentication.Builder()
                      .merchantRefNum(paymentDetails.merchantRefNum)
                      .paResp(paymentDetails.paResp)
                      .enrollmentId(paymentDetails.enrollmentId)
                      .Build();

                Authentication response_authentication = client.threeDSecureService().submit(authentications);
                string payment_id = string.Empty;

                if (response_authentication.threeDResult() == "Y" && response_authentication.status() == "COMPLETED")
                {
                    var auth = client.cardPaymentService().authorize(Authorization.Builder()
                           .merchantRefNum(paymentDetails.merchantRefNum)
                           .amount(Convert.ToInt32(TopDogManager.GetFinalAmount(paymentDetails.CardType, paymentDetails.Amount.ToString("0.00"), paymentDetails.CardNumber) * currencyBaseUnitsMultiplier))
                           .settleWithAuth(true)
                            .card()
                               .cardNum(paymentDetails.CardNumber)
                               .cardExpiry()
                                   .month(Convert.ToInt32(paymentDetails.ExpiryMonth))
                                   .year(Convert.ToInt32(paymentDetails.ExpiryYear))
                                   .Done()
                               .Done()
                              .authentication()
                                .eci(response_authentication.eci())
                                .cavv(response_authentication.cavv())
                                .xid(response_authentication.xid())
                                .threeDEnrollment("Y")
                                .threeDResult(response_authentication.threeDResult())
                                .signatureStatus(response_authentication.signatureStatus())
                            .Done()
                             .billingDetails()
                                    .street(paymentDetails.Address1 + paymentDetails.Address2)
                                    .city(paymentDetails.City)
                                    .state("")
                                    .country(paymentDetails.Country)
                                    .zip(paymentDetails.PostCode)
                            .Done()
                           .Build());


                    if (string.IsNullOrEmpty(auth.id()) || !string.IsNullOrEmpty(auth.error().message()) || auth.status() != "COMPLETED")
                    {
                        paymentDetails.paymentError = auth.error().message();
                        paymentDetails.IsSuccessful = false;
                    }
                    else
                    {
                        paymentDetails.paymentError = "0000 : The Authorisation was Successful.";
                        paymentDetails.IsSuccessful = true;
                        paymentDetails.PaysafeAuthorisationCode = auth.id();
                        paymentDetails.PaySafeVPSAuthCode = "";
                    }
                }

                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paymentDetails)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("ProcessThreeDSecurePayment returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public void MakePaymentProcess(MakePayment paymentDetails)
        {
            try
            {
                if (paymentDetails.PaysafeAuthorisationCode != null && paymentDetails.IsSuccessful)
                {
                    decimal cardCharge = 0;
                    bool isPercentType = false;
                    TopDogManager.GetCardPercentage(paymentDetails.CardType, paymentDetails.CardNumber, ref cardCharge, ref isPercentType);
                    paymentDetails.CardCharges = cardCharge;
                    paymentDetails.CardId = TopDogManager.GetCardId(TopDogManager.FindCardType(paymentDetails.CardNumber, paymentDetails.CardType));
                    TopDogManager.MakePaymentProcess(paymentDetails);
                    bool mailSent = false;
                    /******SUCCESSFULL EMAIL**************/ //TODO
                    mailSent = TopDogManager.SendTakePaymentConfirmationEmail(paymentDetails.Email, "Your payment has been received: " + paymentDetails.RefNo, paymentDetails.TransactionID, paymentDetails);
                    /******SUCCESSFULL EMAIL**************/

                    try
                    {
                        //TopDogManager mybookingManager = new TopDogManager();
                        //TopDogManager.TopDogPaymentPost topDogPaymentPost = new TopDogManager.TopDogPaymentPost(mybookingManager.AddPayment);
                        //topDogPaymentPost.BeginInvoke(paymentDetails, new AsyncCallback(TaskCompleted), paymentDetails.merchantRefNum);
                    }
                    catch (Exception ex)
                    {
                        paymentDetails.IsTopDogPostingSuccess = false;
                        string errstr = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
                        paymentDetails.TopDogError = errstr;
                        TopDogManager.MakePaymentProcess(paymentDetails);
                    }
                }
                else
                {
                    TopDogManager.MakePaymentProcess(paymentDetails);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = string.Empty;
                if (ex.InnerException != null)
                {
                    errorMessage = ex.InnerException.Message;
                }
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    errorMessage = errorMessage + "-------" + ex.Message;
                }
                if (!string.IsNullOrEmpty(ex.StackTrace))
                {
                    errorMessage = errorMessage + "-------" + ex.StackTrace;
                }
                paymentDetails.paymentError = errorMessage;
                TopDogManager.MakePaymentProcess(paymentDetails);
            }
        }

        public void TaskCompleted(IAsyncResult asyncResult)
        {
            string transactionId = (string)asyncResult.AsyncState;
            if (asyncResult.IsCompleted)
            {
                MakePayment objPay = TopDogManager.GetPaymentDetails(transactionId);
            }
        }

        public Stream GetCreditCardCharges()
        {
            try
            {
                //MakePayment makepayment = new MakePayment();

                //makepayment.RefNo = "TT1234567";
                //makepayment.FirstName = "Chandrakanth_Modified3";
                //makepayment.LastName = "Eega3";
                //makepayment.Email = "Email3";
                //makepayment.Amount = 1000;
                //makepayment.TotalDueAmount = 12003;
                //makepayment.CardCharges = 123;
                //makepayment.IsSuccessful = true;
                //makepayment.PaymentDate = System.DateTime.Now;
                //makepayment.DepartureDate = System.DateTime.Now.AddDays(45);
                //makepayment.PaymentDueDate = System.DateTime.Now.AddDays(-45);
                //makepayment.TransactionID = System.Guid.NewGuid().ToString();
                //makepayment.PaysafeAuthorisationCode = System.Guid.NewGuid().ToString();
                //makepayment.paymentError = "Payment Error";
                //makepayment.TopDogError = "TopDogError";
                //makepayment.IsTopDogPostingSuccess = true;
                //makepayment.CardId = 1;
                //makepayment.PaymentId = 3;
                //TopDogManager.MakePaymentProcess(makepayment);

                var creditCardCharges = TopDogManager.GetCreditCardCharges();
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "public, max-age=" + ConfigurationManager.AppSettings["CacheTimeOutOneHour"]);
                return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(creditCardCharges)));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw new WebFaultException<string>("GetCreditCardCharges returned an error response", System.Net.HttpStatusCode.InternalServerError);
            }
        }

        #endregion Payments
    }
}
