﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Presentation.WCFRestService.Model;
using System.Web;
using Presentation.WCFRestService.Model.Routing;

namespace Presentation.Web.WCFRestServices
{
    enum Category { Payments, Flights, Accomodation, Transfer, ATOL_Certificate, Important_Information, Generic };
    public class Global : System.Web.HttpApplication
    {
        public static string iffToImageCountResult { get; set; }
        public static string mhidToImageCountResult { get; set; }
        public static string labelRegionMapping { get; set; }
        public static string imgPerMapping { get; set; }
        public static string iffvsMasterId { get; set; }
        public static string airportIds { get; set; }
        public static string reverseAirportIds { get; set; }
        public static string parentAirportIds { get; set; }
        public static string childAirportIds { get; set; }
        public static string ArtirixServiceUrlFmt { get; set; }
        public static Int32 minCountOfImage { get; set; }
        public static Dictionary<string, int> iffToImageCountList { get; set; }
        public static Dictionary<string, int>mhidtoImageCountList { get; set; }
        public static Dictionary<string, string> labelRegionMappingList { get; set; }
        public static Dictionary<string, string> imgPerMappingList { get; set; }
        public static Dictionary<string, string> imhPer { get; set; }
        public static Dictionary<string, int> iffvsMasterIdMapping { get; set; }
        public static Dictionary<string, string> airportCodes { get; set; }
        public static Dictionary<string, int> reverseAirportCodes { get; set; }
        public static Dictionary<int, int> parentAirportCodes { get; set; }
        public static Dictionary<int, string> childAirportCodes { get; set; }
        public static string imgSourceURLFormat { get; set; }
        public static string artxImgSourceURLFormat { get; set; }
        public static Int32 MaxDegreeOfParallelism { get; set; }
        public static List<HotelURL> AllHotelURLMap { get; set; }
        public static Dictionary<string, MasterHotelImagesMap> iffToMHIDMap { get; set; }
        public static Dictionary<string, string> MHIDtoMHNMap { get; set; }
        public static Dictionary<string, int> MHIDtoRegIDMap { get; set; }
        public static Dictionary<int, string> RegIDtoRegNameMap { get; set; }
        public static Dictionary<string, string> GuidetoGuideBoundaries { get; set; }
        public static Dictionary<string, TelePhoneSet> TelePhoneSets { get; set; }
        public static Dictionary<string, List<DestinationTelePhoneNumber>> DestinationTelePhoneNumbers { get; set; }


        #region Declaring Static Variables For Routing Api

        public static Dictionary<string, List<string>> DestinationResortMap = new Dictionary<string, List<string>>();

        public static readonly Dictionary<string, List<int>> AirportCodeToMultipleDepartureIdsMapping = new Dictionary<string, List<int>>();

        public static Dictionary<string, List<string>> AirportGroupIdOrAirportIdToAirportsMapping = new Dictionary<string, List<string>>();

        public static Dictionary<string, int> AirportCodeToDepartureIdMapping = new Dictionary<string, int>();

        public static string RoutingApiS3BucketName;

        public static string JsonFilesPathForRoutingDataByAirportCodeAndResortId;

        public static string FirstSetSuccessfullyWrittenPath;
        public static string FirstSetSuccessfullyWrittenFileName;

        public static string SecondSetSuccessfullyWrittenPath;
        public static string SecondSetSuccessfullyWrittenFileName;

        public static string ThirdSetSuccessfullyWrittenPath;
        public static string ThirdSetSuccessfullyWrittenFileName;

        public static string FourthSetSuccessfullyWrittenPath;
        public static string FourthSetSuccessfullyWrittenFileName;

        public static string JsonFilesPathForRoutingDataByAirportCode;
        public static string AirportCodesSuccessfullyWrittenPath;
        public static string AirportCodesSuccessfullyWrittenFileName;

        public static string JsonFilesPathForRoutingDataByResortId;
        public static string ResortIdsSuccessfullyWrittenPath;
        public static string ResortIdsSuccessfullyWrittenFileName;

        public static string RedisHostForRoutingApi;
        public static string RedisPortForRoutingApi;
        public static string RedisPasswordForRoutingApi;

        public static int RedisDbForResortIdAndAirportCode;
        public static int RedisDbForAirportCode;
        public static int RedisDbForResortId;

        public static string RedisConnectionStringForRoutingApi;
        #endregion

        protected void Application_Start(object sender, EventArgs e)
        {
            string fileName = ConfigurationManager.AppSettings["GetHolidaySearchURL"];
            string mhidFile = ConfigurationManager.AppSettings["MHIDtoIMGCount"];
            iffToImageCountResult = Utilities.ExecuteGetWebRequest(fileName);
            mhidToImageCountResult = Utilities.ExecuteGetWebRequest(mhidFile);
            string regionLabelMapping = ConfigurationManager.AppSettings["RegionLablelMapping"];
            labelRegionMapping = Utilities.ExecuteGetWebRequest(regionLabelMapping);
            imgPerMapping = Utilities.ExecuteGetWebRequest("https://teletext-deploy.s3.amazonaws.com/webapp/js/data/alljson_new.json");
            iffToImageCountList = JsonConvert.DeserializeObject<Dictionary<string, int>>(iffToImageCountResult);
            mhidtoImageCountList = JsonConvert.DeserializeObject<Dictionary<string, int>>(mhidToImageCountResult);
            labelRegionMappingList = JsonConvert.DeserializeObject<Dictionary<string, string>>(labelRegionMapping);
            imgPerMappingList = JsonConvert.DeserializeObject<Dictionary<string, string>>(imgPerMapping);
            ArtirixServiceUrlFmt = ConfigurationManager.AppSettings["ArtirixServiceUrlFmt"];
            minCountOfImage = Int32.Parse(ConfigurationManager.AppSettings["MinCountofImages"]);
            imgSourceURLFormat = ConfigurationManager.AppSettings["TeletextImgPath"];
            artxImgSourceURLFormat = ConfigurationManager.AppSettings["ArtirixImgPath"];
            MaxDegreeOfParallelism = Int32.Parse(ConfigurationManager.AppSettings["MaxDegreeOfParallelism"]);
            AllHotelURLMap = Utilities.FillAllHotelsURL();
            iffvsMasterId = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ifftomasterHotelIdMapping"]);
            iffvsMasterIdMapping = JsonConvert.DeserializeObject<Dictionary<string, int>>(iffvsMasterId);
            airportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportDictionary"]);
            airportCodes = JsonConvert.DeserializeObject<Dictionary<string, string>>(airportIds);
            reverseAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["AirportToAirportRegion"]);
            reverseAirportCodes = JsonConvert.DeserializeObject<Dictionary<string, int>>(reverseAirportIds);
            parentAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ParentAirport"]);
            parentAirportCodes = JsonConvert.DeserializeObject<Dictionary<int, int>>(parentAirportIds);
            childAirportIds = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["ChildAirport"]);
            childAirportCodes = JsonConvert.DeserializeObject<Dictionary<int, string>>(childAirportIds);
            iffToMHIDMap = JsonConvert.DeserializeObject<Dictionary<string, MasterHotelImagesMap>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["IffCodeToMHID"]));
            MHIDtoMHNMap = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["MHIDtoMHN"]));
            MHIDtoRegIDMap = JsonConvert.DeserializeObject<Dictionary<string, int>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["MHIDtoRegID"]));
            RegIDtoRegNameMap = JsonConvert.DeserializeObject<Dictionary<int, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["RegIDtoRegName"]));
            GuidetoGuideBoundaries = JsonConvert.DeserializeObject<Dictionary<string, string>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["GuidetoGuideBoundaries"]));
            TelePhoneSets = JsonConvert.DeserializeObject<Dictionary<string, TelePhoneSet>>(Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["TelephoneSets"]));
            var destinationTelephonesetsJSON = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["DestinationTelephonesets"]);
            DestinationTelePhoneNumbers = Utilities.GetDestinationTelephoneNumberMappings(destinationTelephonesetsJSON, TelePhoneSets);

            #region Initializing the Static variables related to Routing Api

            DestinationResortMap = Utilities.GenerateDestinationResortMap(labelRegionMappingList);

            RoutingApiS3BucketName = ConfigurationManager.AppSettings["RoutingApiS3BucketName"];

            JsonFilesPathForRoutingDataByAirportCodeAndResortId = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByResortIdAndAirportCode"];

            FirstSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["FirstSetSuccessfullyWrittenFileName"];
            FirstSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["FirstSetSuccessfullyWrittenPath"];

            SecondSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["SecondSetSuccessfullyWrittenFileName"];
            SecondSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["SecondSetSuccessfullyWrittenPath"];

            ThirdSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["ThirdSetSuccessfullyWrittenFileName"];
            ThirdSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["ThirdSetSuccessfullyWrittenPath"];

            FourthSetSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["FourthSetSuccessfullyWrittenFileName"];
            FourthSetSuccessfullyWrittenPath = ConfigurationManager.AppSettings["FourthSetSuccessfullyWrittenPath"];

            JsonFilesPathForRoutingDataByAirportCode = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByAirportCode"];
            AirportCodesSuccessfullyWrittenPath = ConfigurationManager.AppSettings["AirportCodesSuccessfullyWrittenPath"];
            AirportCodesSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["AirportCodesSuccessfullyWrittenFileName"];

            JsonFilesPathForRoutingDataByResortId = ConfigurationManager.AppSettings["JsonFilesPathForRoutingDataByResortId"];
            ResortIdsSuccessfullyWrittenPath = ConfigurationManager.AppSettings["ResortIdsSuccessfullyWrittenPath"];
            ResortIdsSuccessfullyWrittenFileName = ConfigurationManager.AppSettings["ResortIdsSuccessfullyWrittenFileName"];

            RedisHostForRoutingApi = ConfigurationManager.AppSettings["RedisHostForRoutingApi"];
            RedisPortForRoutingApi = ConfigurationManager.AppSettings["RedisPortForRoutingApi"];
            RedisPasswordForRoutingApi = ConfigurationManager.AppSettings["RedisPasswordForRoutingApi"];

            RedisConnectionStringForRoutingApi = Utilities.GetRedisConnectionStringForRoutingApi();

            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForResortIdAndAirportCode"], out RedisDbForResortIdAndAirportCode);
            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForAirportCode"], out RedisDbForAirportCode);
            Int32.TryParse(ConfigurationManager.AppSettings["RedisDbForResortId"], out RedisDbForResortId);

            //System.Diagnostics.Debugger.Break();

            string airportsXmlData = Utilities.ExecuteGetWebRequest(ConfigurationManager.AppSettings["RoutingAirports"]);

            List<AirportGroup> airportGroups = Utilities.GetAirportsFromXmlString(airportsXmlData);

            //if (airportGroups != null && airportGroups.Count > 0)
            //{
            //    AirportCodeToMultipleDepartureIdsMapping = Utilities.GenerateAirportCodeToDepartureIdMapping(airportGroups);
            //}

            #endregion

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST, PUT, DELETE");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}