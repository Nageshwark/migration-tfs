﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.WCFRestService.Model.Misc;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using Presentation.WCFRestService.Model.Enum;
using System.ServiceModel.Web;
using Presentation.WCFRestService.Model.StaticHotelLanding;

namespace Presentation.Web.WCFRestServices.StaticHotelLanding
{
    public class StaticHotelLandingController
    {
        public static object GetHotelInfoByUrl(string url)
        {
            try
            {
                string Mhid = GetMHIDByUrl(url);
                string cacheTime = string.Empty;
                MHIDStaticInfo mhidStaticInfo = null;
                StaticMasterHotelInfo staticMHInfo = null;
                // Use MHID to get the static hotel info
                if(!string.IsNullOrWhiteSpace(Mhid))
                {
                    staticMHInfo = new StaticMasterHotelInfo();
                    mhidStaticInfo = Utilities.GetMasterHotelInfo(Mhid.ToString(), out cacheTime);
                    if(mhidStaticInfo._source != null)
                    {
                        MasterHotelSource _source = new MasterHotelSource();
                        _source.BuildingName = mhidStaticInfo._source.BuildingName;
                        _source.Description = mhidStaticInfo._source.Description;
                        _source.DestinationCode = mhidStaticInfo._source.DestinationCode;
                        _source.DestinationName = mhidStaticInfo._source.DestinationName;
                        _source.Features = mhidStaticInfo._source.Features;
                        _source.GIATAID = mhidStaticInfo._source.GIATAID;
                        _source.Location = mhidStaticInfo._source.Location;
                        _source.MasterHotelId = mhidStaticInfo._source.MasterHotelId;
                        _source.MasterResortId = mhidStaticInfo._source.MasterResortId;
                        _source.MasterResortName = mhidStaticInfo._source.MasterResortName;
                        _source.Rating = mhidStaticInfo._source.Rating;
                        _source.RatingLevel = mhidStaticInfo._source.RatingLevel;
                        _source.Region = mhidStaticInfo._source.Region;
                        _source.RegionTTSSLabelD = mhidStaticInfo._source.RegionTTSSLabelD;

                        staticMHInfo._source = _source;
                    }
                    if(mhidStaticInfo.TripAdvisor != null)
                    {
                        TripAdvisor taInfo = new TripAdvisor();
                        taInfo.hotelStyle = mhidStaticInfo.TripAdvisor.hotelStyle;
                        taInfo.noOfRooms = mhidStaticInfo.TripAdvisor.noOfRooms;
                        taInfo.reviewsCount = mhidStaticInfo.TripAdvisor.reviewsCount;
                        taInfo.TAID = mhidStaticInfo.TripAdvisor.TAID;
                        taInfo.TARating = mhidStaticInfo.TripAdvisor.TARating;

                        staticMHInfo.TripAdvisor = taInfo;
                    }
                    // Supply the Search Query Parameters
                    SearchQueryParameters searchQueryParams = new SearchQueryParameters();
                    searchQueryParams.adults = ConfigurationManager.AppSettings["adults"];
                    searchQueryParams.boardType = ConfigurationManager.AppSettings["boardType"];
                    searchQueryParams.channelId = ConfigurationManager.AppSettings["channelId"];
                    searchQueryParams.children = ConfigurationManager.AppSettings["children"];
                    searchQueryParams.departureIds = ConfigurationManager.AppSettings["departureIds"];
                    searchQueryParams.duration = ConfigurationManager.AppSettings["duration"];
                    searchQueryParams.ratings = ConfigurationManager.AppSettings["ratings"];

                    staticMHInfo.searchParams = searchQueryParams;

                    // Meta Tags for the static hotel landing page
                    staticMHInfo.metaDescription = ConfigurationManager.AppSettings["StaticHotelLandingMetaDescription"];
                    staticMHInfo.title = ConfigurationManager.AppSettings["StaticHotelLandingMetaTitle"];

                    staticMHInfo.images = new List<string>();
                    staticMHInfo.mobileimages = new List<string>();
                    staticMHInfo.thumbnailimages = new List<string>();

                    if (Global.mhidtoImageCountList.ContainsKey(mhidStaticInfo._source.MasterHotelId.ToString()))
                    {
                        int hoteliffCount = Global.mhidtoImageCountList[mhidStaticInfo._source.MasterHotelId.ToString()];
                        string imgSourceURLFormat = Global.imgSourceURLFormat;
                        for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                        {
                            staticMHInfo.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["DesktopMobileImageBucket"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                            staticMHInfo.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["ThumbnailImageBucket"] + "/" + mhidStaticInfo._source.MasterHotelId.ToString() + "/" + hotelIndex + ".jpg"));
                        }
                    }

                    if (mhidStaticInfo == null)
                    {
                        throw new WebFaultException<string>("GetMasterHotelInfo: MasterHotelId Does not exist:" + Mhid.ToString(), System.Net.HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("GetHotelInfoByUrl: No mapped MasterHotelId found for the URL:" + url, System.Net.HttpStatusCode.InternalServerError);
                }
                return staticMHInfo;
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        private static string GetMHIDByUrl(string url)
        {
            try
            {
                //List<MHIDBuildingNameMapping> mhidBuildingNameMapping = new List<MHIDBuildingNameMapping>();
                Dictionary<string, string> mhidBuildingNameMapping = new Dictionary<string, string>();
                string mhidBuildingNameMappingResponse = string.Empty;
                string Mhid = string.Empty;

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForMhidData(ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFileBucketName"], ConfigurationManager.AppSettings["MHIDHotelLandingPageURLMappingFilePath"])))
                {
                    mhidBuildingNameMappingResponse = reader.ReadToEnd();
                }
                mhidBuildingNameMapping = JsonConvert.DeserializeObject<Dictionary<string, string>>(mhidBuildingNameMappingResponse);

                // Search for the Url to get the MHID
                url = url.Replace("/hotels","");

                if (mhidBuildingNameMapping.TryGetValue(url.ToLower().Trim(), out Mhid))
                {

                }
                return Mhid;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }
    }
}