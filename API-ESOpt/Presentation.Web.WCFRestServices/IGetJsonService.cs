﻿using Presentation.WCFRestService.Model.DeepLinks;
using Presentation.WCFRestService.Model.Guides;
using Presentation.WCFRestService.Model.Misc;
using Presentation.WCFRestService.Model.Reviews;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Script.Services;


namespace Presentation.Web.WCFRestServices
{
    [ServiceContract]
    [ScriptService]
    public interface IGetJsonService
    {
        # region Teletext Web API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "AuthenticateCustomerOTP/{mobileNo}/{otpr}")]
        Stream AuthenticateCustomerOTP(string mobileNo, string otpr);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCustomerAuthenticationOTP/{mobileNo}")]
        Stream GetCustomerAuthenticationOTP(string mobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuidesIndex")]
        Stream GetGuidesIndex();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuidesIndexList")]
        Stream GetGuidesIndexList();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDestinations")]
        Stream GetDestinations();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuide/{RegionId}")]
        Stream GetGuide(string RegionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetRegionById/{RegionId}")]
        Stream GetRegionById(string RegionId);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetRegionByURL?url={url}")]
        Stream GetRegionByURL(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOverseasDestination/{destinationName}")]
        Stream GetOverseasDestination(string destinationName);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOvrseaseDestinationLabelRegions")]
        Stream GetOvrseaseDestinationLabelRegions();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelList/{destinationCode}/{hotelName}/{resortId}")]
        Stream GetHotelList(string destinationCode, string hotelName, string resortId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelListForAutoSuggest/{regionId}/{hotelName}")]
        Stream GetHotelListForAutoSuggest(string regionId, string hotelName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetMasterHotelIdByURL?url={url}")]
        Stream GetMasterHotelIdByURL(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelDataByMasterHotelId/{masterHotelId}")]
        Stream GetHotelDataByMasterHotelId(string masterHotelId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                   RequestFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "SetMasterHotelData")]
        Stream SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideLandingPageInfo/{RegionId}")]
        Stream GetGuideLandingPageInfo(string RegionId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SaveGuideInformation?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream SaveGuideInformation(Guide guide, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideInformation/{RegionId}")]
        Stream GetGuideInformation(string regionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideInformationByUrl?url={url}")]
        Stream GetGuideInformationByUrl(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetGuideLandingPageInfoByUrl?url={url}")]
        Stream GetGuideLandingPageInfoByUrl(string url);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetGuidesIndexListFromCloudFront")]
        Stream GetGuidesIndexListFromCloudFront();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                           BodyStyle = WebMessageBodyStyle.Bare,
                           UriTemplate = "GetTeletextRouting?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetTeletextRouting(string destinationId, string departureId, string dates);

        #endregion

        #region Flight Stats API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetFlightByFlightNumberAndFlightDate?flightNumber={flightNumber}&day={day}&month={month}&year={year}")]
        Stream GetFlightByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetFlightScheduleByFlightNumberAndFlightDate?flightNumber={flightNumber}&day={day}&month={month}&year={year}")]
        Stream GetFlightScheduleByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year);

        #endregion

        # region TopDog API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetMobileBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetMobileBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetTOPDOGBasket/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetTOPDOGBasket(string BookingReference, string Surname, string DepartureDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetQuickQuoteDetails/{BookingReference}/{DepartureDate}")]
        Stream GetQuickQuoteDetails(string BookingReference, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDocuments/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetDocuments(string BookingReference, string Surname, string DepartureDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetPaymentPlan/{BookingReference}/{Surname}/{DepartureDate}")]
        Stream GetPaymentPlan(string BookingReference, string Surname, string DepartureDate);
        #endregion

        # region Mobile App API


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelsByDestinationCode/{destCode}")]
        Stream GetHotelsByDestinationCode(string destCode);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelByID/{hotelId}")]
        Stream GetHotelByID(string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHotelReviewByID/{hotelId}")]
        Stream GetHotelReviewByID(string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHolidaySearch?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&destinationType={destinationType}&platform={platform}")]
        Stream GetHolidaySearch(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType, string labelId, string preferredHotelKeys, string skipFacets, string destinationType, string platform);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHolidaySearchV_2?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&destinationType={destinationType}")]
        Stream GetHolidaySearchV_2(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType, string labelId, string preferredHotelKeys, string skipFacets, string destinationType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHolidays?adults={adults}&children={children}&ratings={ratings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}")]
        Stream GetHolidays(string adults, string children, string ratings, string departureDate, string dateMin, string dateMax, string durationMin, string durationMax, string destinationIds, string priceMin, string priceMax,
             string departureIds, string tripadvisorrating, string sort, string hotelKeysToExclude, string channelId, string hotelKeys, string tradingNameIds, string boardType);


        [OperationContract]
        [WebGet(UriTemplate = "GetMapByLocation?lat={lat}&lon={lon}",
                                RequestFormat = WebMessageFormat.Xml,
                                ResponseFormat = WebMessageFormat.Xml,
                                BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetMapByLocation(string lat, string lon);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetHomePageSlider")]
        Stream GetHomePageSlider();


        [OperationContract]

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetOffersAndDealsSlider")]
        Stream GetOffersAndDealsSlider();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPubsAndRestruantsByGoogleAPI/{lat}/{lon}/{type}/{hotelId}",
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetPubsAndRestruantsByGoogleAPI(string lat, string lon, string type, string hotelId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixHotelDescription?hotelOperator={hotelOperator}&contentId={contentId}")]
        Stream GetArtirixHotelDescription(string hotelOperator, string contentId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixRouting?destinationId={destinationId}&departureId={departureId}&dates={dates}")]
        Stream GetArtirixRouting(string destinationId, string departureId, string dates);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetArtirixDiversity?destinationId={destinationId}&hotelKeys={hotelKeys}&departureIds={departureIds}&dateMin={dateMin}&dateMax={dateMax}&children={children}&adults={adults}&durationMax={durationMax}&durationMin={durationMin}&ratings={ratings}")]
        Stream GetArtirixDiversity(string destinationId, string hotelKeys, string departureIds, string dateMin, string dateMax, string children, string adults, string durationMax, string durationMin, string ratings);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHotelContent?hotelOperator={hotelOperator}&contentId={contentId}")]
        Stream GetHotelContent(string hotelOperator, string contentId);        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetGoogleAirportInfo/{airport}")]
        Stream GetGoogleAirportInfo(string airport);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetGoogleDistance/{oLat}/{oLng}/{dLat}/{dLng}/{mode}")]
        Stream GetGoogleDistance(string oLat, string oLng, string dLat, string dLng, string mode);



        #endregion

        # region PostCode API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAddressesByPostCode/{postCode}")]
        Stream GetAddressesByPostCode(string postCode);

        #endregion

        # region SmartFocus API

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetNewsLetterSubscription?name={name}&email={email}&phone={phone}&airport={airport}")]
        Stream GetNewsLetterSubscription(string name, string email, string phone, string airport);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "EmailSubscription")]
        Stream GetSubscription(emailSubscription emailSubscription);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetNewsLetterSubscriberByEmail/{email}")]
        Stream GetNewsLetterSubscriberByEmail(string email);


        #endregion

        #region MinPriceTool
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetArtirixMinPrice?adults={adults}&children={children}&starRatings={starRatings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&departureName={departureName}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&channelName={channelName}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}")]
        Stream GetArtirixMinPrice(string adults, string children, string starRatings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetMinPriceHomePage")]
        Stream GetMinPriceHomePage();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "DeleteObject?queryId={queryId}&userId={userId}")]
        Stream DeleteObject(string queryId, string userId);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetSectionObjects?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjects(string sectionIds, string platform);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetQueryIdObjects?queryId={queryId}&sectionId={sectionId}")]
        Stream GetQueryIdObjects(string queryId, string sectionId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SetQueryImageUrl?queryId={queryId}&sectionId={sectionId}&imageUrl={imageUrl}&userId={userId}")]
        Stream SetQueryImageUrl(string queryId, string sectionId, string imageUrl, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetDistinctSectionId?sectionIdStartWith={sectionIdStartWith}&platform={platform}")]
        Stream GetDistinctSectionId(string sectionIdStartWith, string platform);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "EditorUpdate?userId={userId}")]
        Stream EditorUpdate(editUpdate editUpdate, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SaveorUpdateMinPrice?userId={userId}")]
        Stream SaveorUpdateMinPrice(saveUpdateMinPrice saveUpdateMinPrice, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetArtirixMinPriceNew?adults={adults}&children={children}&starRatings={starRatings}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&destinationIds={destinationIds}&destinationName={destinationName}&priceMin={priceMin}&priceMax={priceMax}&departureIds={departureIds}&departureName={departureName}&tripadvisorrating={tripadvisorrating}&sort={sort}&hotelKeysToExclude={hotelKeysToExclude}&channelId={channelId}&channelName={channelName}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}&boardType={boardType}&dateOffSet={dateOffSet}&flexDates={flexDates}")]
        Stream GetArtirixMinPriceNew(string adults, string children, string starRatings, string departureDate, string dateMin, string dateMax,
                                               string durationMin, string durationMax, string destinationIds, string destinationName, string priceMin,
                                               string priceMax, string departureIds, string departureName, string tripadvisorrating, string sort,
                                               string hotelKeysToExclude, string channelId, string channelName, string hotelKeys, string tradingNameIds,
                                               string boardType, string dateOffSet, string flexDates);

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "SaveorUpdateMinPriceNew?userId={userId}")]
        Stream SaveorUpdateMinPriceNew(saveUpdateMinPrice saveUpdateMinPrice, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetSectionObjectsV2?sectionIds={sectionIds}&platform={platform}")]
        Stream GetSectionObjectsV2(string sectionIds, string platform);

        #endregion

        #region ElasticSearch
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "ESHolidaySearch?destinationIds={destinationIds}&departureIds={departureIds}&boardType={boardType}&departureDate={departureDate}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&priceMin={priceMin}&priceMax={priceMax}&ratings={ratings}&tradingNameIds={tradingNameIds}&destinationType={destinationType}&hotelKeys={hotelKeys}&tripAdvisorRating={tripAdvisorRating}&hotelKeysToExclude={hotelKeysToExclude}&sort={sort}&channelId={channelId}&labelId={labelId}&preferredHotelKeys={preferredHotelKeys}&skipFacets={skipFacets}&personalizedParms={personalizedParms}&recentSearchCookie={recentSearchCookie}&favoriteCookie={favoriteCookie}")]
        Stream ESHolidaySearch(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin, string dateMax, int durationMin, int durationMax,
                                 int adults, int children, int priceMin, int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                 int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId, string preferredHotelKeys, string skipFacets,
                                    string personalizedParms, string recentSearchCookie, string favoriteCookie);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "ESDiversity?destinationId={destinationId}&departureIds={departureIds}&dateMin={dateMin}&dateMax={dateMax}&durationMin={durationMin}&durationMax={durationMax}&adults={adults}&children={children}&ratings={ratings}&hotelKeys={hotelKeys}&tradingNameIds={tradingNameIds}")]
        Stream ESDiversity(int destinationId, int departureIds, string dateMin, string dateMax, int durationMin, int durationMax, int adults,
                            int children, string ratings, string hotelKeys, string tradingNameIds);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "TAReviews?TAID={TAID}")]
        Stream TAReviews(string taid);
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "UpdateTAInfo")]
        Stream UpdateTAInfo();

        [OperationContract]
        [WebInvoke(Method = "POST",
                                RequestFormat = WebMessageFormat.Json,
                                ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "AkamaiPurge")]
        Stream AkamaiPurge(AkamaiPurge akamaipurge);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "GetMasterHotelInfo?MHID={MHID}")]
        Stream GetMasterHotelInfo(string MHID);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PostMasterHotelInfo?MHID={MHID}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream PostMasterHotelInfo(MHIDStaticInfo mhidStaticInfo, string MHID);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "GetMasterHotelDescription?MHID={MHID}")]
        Stream GetMasterHotelDescription(string MHID);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "ProcessMasterHotelImages?MHID={MHID}&MasterHotelImages={MasterHotelImages}&userId={userId}&MHETag={MHETag}")]
        Stream ProcessMasterHotelImages(string MHID, string MasterHotelImages, string userId, string MHETag);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddMasterHotelTag?tagName={tagName}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        string AddMasterHotelTag(string tagName, string userId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddMasterHotelFacilities?facilityName={facilityName}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        string AddMasterHotelFacilities(string facilityName, string userId);


        [OperationContract]
        [WebInvoke(UriTemplate = "/SwapMasterHotelImages?MHID={MHID}&destImage={destImage}&MHETag={MHETag}&userId={userId}",
                 Method = "GET",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream SwapMasterHotelImages(string MHID, string destImage, string MHETag, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "CreateTimeBasedIndices")]
        void CreateTimeBasedIndices();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "DeleteTimeBasedIndices")]
        void DeleteTimeBasedIndices();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                              BodyStyle = WebMessageBodyStyle.Bare,
                              UriTemplate = "DeleteResultsIndexResults")]
        void DeleteResultsIndexResults();


        #endregion

        #region Dynamic SEO
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetCategoryList")]
        Stream GetCategoryList();

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessCategoryList?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessCategoryList(List<DynamicSEO.Category> categoryList, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetTemplateList")]
        Stream GetTemplateList();

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessTemplateList?userId={userId}",
                 Method = "POST",
                  RequestFormat = WebMessageFormat.Json,
                  ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessTemplateList(List<DynamicSEO.Template> templateList, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetHoliday?fileName={fileName}")]
        Stream GetHoliday(string fileName);

        #endregion

        #region GoogleAPI
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetGoogleNearBy?lat={lat}&lon={lon}&patternType={patternType}&type={type}&radius={radius}",
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetGoogleNearBy(float lat, float lon, string patternType, string type, string radius);

        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GetImage?lat={lat}&lng={lng}&zoom={zoom}")]
        Stream GetImage(string lat, string lng, string zoom);


        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "InvalidateUATCloudFront?urlPaths={urlPaths}")]
        void InvalidateUATCloudFront(string urlPaths);

        [OperationContract]
        [WebInvoke(Method = "GET",
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "InvalidatePRODCloudFront?urlPaths={urlPaths}")]
        void InvalidatePRODCloudFront(string urlPaths);

        #endregion

        #region Reviews

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetAllPublishedReviews?limit={limit}&startIndex={startIndex}&reviewType={reviewType}")]
        Stream GetAllPublishedReviews(int limit, int startIndex, string reviewType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetReviews?limit={limit}&startIndex={startIndex}&reviewProvider={reviewProvider}&showPublished={showPublished}&showRatings={showRatings}")]
        Stream GetReviews(string limit, string startIndex, string reviewProvider, string showPublished, string showRatings);


        [OperationContract]
        [WebInvoke(Method = "POST",
                               RequestFormat = WebMessageFormat.Json,
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "PublishorUnpublishReviews?userId={userId}")]
        Stream PublishorUnpublishReviews(ReviewInformation review, string userId);


        #endregion

        #region HomePage
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetNearestAirportGroup/{Latitude}/{Longitude}")]
        Stream GetNearestAirportGroup(string Latitude, string Longitude);
        #endregion

        #region DeepLinks

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                  BodyStyle = WebMessageBodyStyle.Bare,
                                  UriTemplate = "GenerateDeepLinks")]
        void GenerateDeepLinks();

        [OperationContract]
        [WebInvoke(Method = "POST",
                               RequestFormat = WebMessageFormat.Json,
                               ResponseFormat = WebMessageFormat.Json,
                               BodyStyle = WebMessageBodyStyle.Bare,
                               UriTemplate = "SendAPPLink")]
        Stream SendAPPLink(APPLink applink);


        #endregion

        #region LambdaHits
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GenerateRecentBlogsFromDB?procedureName={procedureName}")]
        void GenerateRecentBlogsFromDB(string procedureName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GenerateBlogsByTagsFromDB?procedureName={procedureName}")]
        void GenerateBlogsByTagsFromDB(string procedureName);
        #endregion

        #region Blogs
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetRecentBlogs?pageName={pageName}")]
        Stream GetRecentBlogs(string pageName);
        #endregion

        #region HotelLandingPage
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                BodyStyle = WebMessageBodyStyle.Bare,
                                UriTemplate = "GetMasterHotelInfoByUrl?url={url}")]
        Stream GetMasterHotelInfoByUrl(string url);
        #endregion

        #region Payments

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessPayment",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        Stream ProcessPayment(MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessThreeDSecurePayment",
                Method = "POST",
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json)]
        Stream ProcessThreeDSecurePayment(MakePayment paymentDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                 BodyStyle = WebMessageBodyStyle.Bare,
                                 UriTemplate = "GetCreditCardCharges")]
        Stream GetCreditCardCharges();

        [OperationContract]
        [WebInvoke(UriTemplate = "/MakePaymentProcess",
                    Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Bare)]
        void MakePaymentProcess(MakePayment paymentDetails);
        #endregion Payments


    }
}
