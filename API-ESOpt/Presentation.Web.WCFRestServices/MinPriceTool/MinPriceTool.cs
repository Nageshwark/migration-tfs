﻿using Presentation.WCFRestService.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Artirix;
using System.ServiceModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Enum;
using Presentation.Web.WCFRestServices.RedisDataAccessHelper;
using Presentation.WCFRestService.Model.Misc;

namespace Presentation.Web.WCFRestServices
{
    public class MinPriceTool
    {
        public static MinPriceView MinPriceResponseView(string artirixSearchUrl, string preUrl, string postUrl)
        {
            MinPriceView minPriceView = null;
            int flag = 0;
            int minPrice = 0;
            Holidays holidays = new Holidays();
            var artirixRespStr = string.Empty;
            artirixRespStr = Utilities.ExecuteGetWebRequest(artirixSearchUrl);
            holidays = JsonConvert.DeserializeObject<Holidays>(artirixRespStr);
            if (holidays == null || holidays.offers == null)
            {
                throw new WebFaultException<string>("Artirix returned an empty response", System.Net.HttpStatusCode.InternalServerError);
            }
            try
            {
                minPriceView = new MinPriceView();
                foreach (Offer offr in holidays.offers)
                {
                    if (offr.isPreferential == true && flag == 0)
                    {
                        minPriceView.minimumPrice = offr.price;
                        minPrice = minPriceView.minimumPrice;
                        minPriceView.boardBasis = offr.accommodation.boardTypeCode;
                        minPriceView.quoteRef = offr.quoteRef;
                        minPriceView.starRating = offr.accommodation.rating;
                        minPriceView.artirixUrl = artirixSearchUrl;
                        minPriceView.departureDate = offr.journey.departureDate;
                        minPriceView.websiteUrl = preUrl + "/" + offr.journey.departureDate.ToString("yyyy-MM-dd") + "/" + postUrl;
                        minPriceView.iff = offr.hotel.iff;
                        minPriceView.hotelName = Utilities.RemoveDiacritics(offr.hotel.name);
                        minPriceView.description = string.Empty;
                        minPriceView.glat = offr.journey.glat;
                        minPriceView.glong = offr.journey.glong;
                        flag++;
                    }

                    if (offr.isPreferential == false && (minPrice > offr.price || flag == 0))
                    {
                        minPriceView.minimumPrice = offr.price;
                        minPriceView.boardBasis = offr.accommodation.boardTypeCode;
                        minPriceView.quoteRef = offr.quoteRef;
                        minPriceView.starRating = offr.accommodation.rating;
                        minPriceView.artirixUrl = artirixSearchUrl;
                        minPriceView.departureDate = offr.journey.departureDate;
                        minPriceView.websiteUrl = preUrl + "/" + offr.journey.departureDate.ToString("yyyy-MM-dd") + "/" + postUrl;
                        minPriceView.iff = offr.hotel.iff;
                        minPriceView.hotelName = Utilities.RemoveDiacritics(offr.hotel.name);
                        minPriceView.description = string.Empty;
                        minPriceView.glat = offr.journey.glat;
                        minPriceView.glong = offr.journey.glong;
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetAtxSearch:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }

            return minPriceView;
        }
        public static MinPriceView MinPriceResponseViewNew(string artirixSearchUrl, string preUrl, string postUrl, string departureDate, string flexDates)
        {
            MinPriceView minPriceView = null;
            int flag = 0;
            int minPrice = 0;
            Holidays holidays = new Holidays();
            var artirixRespStr = string.Empty;
            artirixRespStr = Utilities.ExecuteGetWebRequest(artirixSearchUrl);
            holidays = JsonConvert.DeserializeObject<Holidays>(artirixRespStr);
            if (holidays == null || holidays.offers == null)
            {
                throw new WebFaultException<string>("Artirix returned an empty response", System.Net.HttpStatusCode.InternalServerError);
            }
            try
            {
                minPriceView = new MinPriceView();
                foreach (Offer offr in holidays.offers)
                {
                    if (offr.isPreferential == true && flag == 0)
                    {
                        minPriceView.minimumPrice = offr.price;
                        minPrice = minPriceView.minimumPrice;
                        minPriceView.boardBasis = offr.accommodation.boardTypeCode;
                        minPriceView.quoteRef = offr.quoteRef;
                        minPriceView.starRating = offr.accommodation.rating;
                        minPriceView.artirixUrl = artirixSearchUrl;
                        minPriceView.departureDate = offr.journey.departureDate;
                        if (flexDates == "yes")
                        {
                            minPriceView.websiteUrl = preUrl + "/" + departureDate + "/" + postUrl;
                        }
                        else
                        {
                            minPriceView.websiteUrl = preUrl + "/" + offr.journey.departureDate.ToString("yyyy-MM-dd") + "/" + postUrl;
                        }
                        minPriceView.iff = offr.hotel.iff;
                        minPriceView.hotelName = Utilities.RemoveDiacritics(offr.hotel.name);
                        minPriceView.description = string.Empty;
                        minPriceView.glat = offr.journey.glat;
                        minPriceView.glong = offr.journey.glong;
                        flag++;
                    }

                    if (offr.isPreferential == false && (minPrice > offr.price || flag == 0))
                    {
                        minPriceView.minimumPrice = offr.price;
                        minPriceView.boardBasis = offr.accommodation.boardTypeCode;
                        minPriceView.quoteRef = offr.quoteRef;
                        minPriceView.starRating = offr.accommodation.rating;
                        minPriceView.artirixUrl = artirixSearchUrl;
                        minPriceView.departureDate = offr.journey.departureDate;
                        if (flexDates == "yes")
                        {
                            minPriceView.websiteUrl = preUrl + "/" + departureDate + "/" + postUrl;
                        }
                        else
                        {
                            minPriceView.websiteUrl = preUrl + "/" + offr.journey.departureDate.ToString("yyyy-MM-dd") + "/" + postUrl;
                        }
                        minPriceView.iff = offr.hotel.iff;
                        minPriceView.hotelName = Utilities.RemoveDiacritics(offr.hotel.name);
                        minPriceView.description = string.Empty;
                        minPriceView.glat = offr.journey.glat;
                        minPriceView.glong = offr.journey.glong;
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at GetAtxSearch:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }

            return minPriceView;
        }

        public static List<MinPriceToolObject> DeleteInputObject(string queryId)
        {
            List<MinPriceToolObject> minPriceToolInputObject = null;
            try
            {
                var jsonObjectInput = String.Empty;
                string inputUrl = ConfigurationManager.AppSettings["MinPriceInput"];
                jsonObjectInput = Utilities.ExecuteGetWebRequest(inputUrl);
                minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);
                minPriceToolInputObject.RemoveAll(r => r.queryId.ToLower() == queryId.ToLower());
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                IAmazonS3 client;
                using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {
                    Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = awsInputFolderKeyName
                    };
                    request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolInputObject)));
                    PutObjectResponse response2 = client.PutObject(request);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Object Not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                return null;
            }
            return minPriceToolInputObject;
        }
        public static List<MinPriceToolObject> DeleteOutputObject(string queryId)
        {
            List<MinPriceToolObject> minPriceToolOutputObject = null;
            try
            {
                var jsonObjectOutput = String.Empty;
                string outputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
                minPriceToolOutputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectOutput);
                minPriceToolOutputObject.RemoveAll(r => r.queryId.ToLower() == queryId.ToLower());
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
                string awsBucketName = ConfigurationManager.AppSettings["AWSBucketName"];
                string awsInputFolderKeyName = ConfigurationManager.AppSettings["AWSInputFolderName"];
                string awsOutputFolderKeyName = ConfigurationManager.AppSettings["AWSOutputFolderName"];
                IAmazonS3 client;
                using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {
                    Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = awsBucketName,
                        Key = awsOutputFolderKeyName
                    };
                    request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(minPriceToolOutputObject)));
                    PutObjectResponse response2 = client.PutObject(request);
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Object Not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                return null;
            }
            return minPriceToolOutputObject;
        }

        public static List<MinPriceToolObject> SectionObject(string sectionIds)
        {
            List<MinPriceToolObject> minPriceToolOutputObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolSectionIdObject = new List<MinPriceToolObject>();
            try
            {
                var jsonObjectOutput = String.Empty;
                string outputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
                minPriceToolOutputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectOutput);
                if (string.IsNullOrEmpty(sectionIds))
                {
                    return minPriceToolOutputObject;
                }
                var sectionSplit = sectionIds.Split(',');
                for (int i = 0; i < sectionSplit.Length; i++)
                {
                    minPriceToolSectionIdObject.AddRange(minPriceToolOutputObject.FindAll(r => r.sectionId.ToLower() == sectionSplit[i].ToLower() & r.query.isDynamic == false));
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("SectionId not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }

            return minPriceToolSectionIdObject;

        }
        public static List<MinPriceToolObject> SectionObjectV2(string sectionIds)
        {
            List<MinPriceToolObject> minPriceToolOutputObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolSectionIdObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolSectionIdDynamicObject = new List<MinPriceToolObject>();
            List<MinPriceToolObject> minPriceToolSectionIdStaticObject = new List<MinPriceToolObject>();
            Holidays holidays = new Holidays();
            bool setStaticCount = false;
            int staticCount = int.Parse(ConfigurationManager.AppSettings["StaticOffersCount"]);
            List<string> distinctHotelKeys = null;
            try
            {
                var jsonObjectOutput = String.Empty;
                string outputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
                minPriceToolOutputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectOutput);
                if (string.IsNullOrEmpty(sectionIds))
                {
                    return minPriceToolOutputObject;
                }
                var sectionSplit = sectionIds.Split(',');

                for (int i = 0; i < sectionSplit.Length; i++)
                {
                    minPriceToolSectionIdDynamicObject.AddRange(minPriceToolOutputObject.FindAll(r => r.sectionId.ToLower() == sectionSplit[i].ToLower() & r.query.isDynamic == true));
                }
                if (minPriceToolSectionIdDynamicObject.Count != 0)
                {
                    setStaticCount = true;
                }
                for (int i = 0; i < sectionSplit.Length; i++)
                {
                    minPriceToolSectionIdStaticObject.AddRange(minPriceToolOutputObject.FindAll(r => r.sectionId.ToLower() == sectionSplit[i].ToLower() & r.query.isDynamic == false));
                }
                distinctHotelKeys = minPriceToolSectionIdStaticObject.Select(key => key.query.iff).Distinct().ToList();
                string hotelKeysToExclude = string.Join(",", distinctHotelKeys);
                if (setStaticCount)
                {
                    foreach (MinPriceToolObject staticQuery in minPriceToolSectionIdStaticObject)
                    {
                        if (staticCount > 0)
                        {
                            minPriceToolSectionIdObject.Add(staticQuery);
                            staticCount--;
                        }
                    }
                }
                else
                {
                    foreach (MinPriceToolObject staticQuery in minPriceToolSectionIdStaticObject)
                    {
                        minPriceToolSectionIdObject.Add(staticQuery);
                    }
                }
                foreach (MinPriceToolObject dynamicQuery in minPriceToolSectionIdDynamicObject)
                {
                    var artirixUrl = dynamicQuery.query.artirixUrl.Replace("http://fed.prd.tt.artirix.com/search", "https://dataservice.teletextholidays.co.uk/GetJsonService.svc/GetHolidaySearch") + "&hotelKeysToExclude=" + hotelKeysToExclude;
                    var artirixResponse = Utilities.ExecuteGetWebRequest(artirixUrl);
                    holidays = JsonConvert.DeserializeObject<Holidays>(artirixResponse);
                    foreach (Offer offr in holidays.offers)
                    {
                        try
                        {
                            MinPriceToolObject minPriceToolObject = new MinPriceToolObject();
                            minPriceToolObject.sectionId = dynamicQuery.sectionId;
                            minPriceToolObject.queryId = dynamicQuery.queryId;
                            minPriceToolObject.minPrice = offr.price;
                            minPriceToolObject.lastUpdated = offr.accommodation.updated.ToString();
                            minPriceToolObject.quoteRef = offr.quoteRef;
                            minPriceToolObject.image = offr.hotel.mobileimages[0];
                            minPriceToolObject.hotelLocation = offr.journey.destination;
                            minPriceToolObject.starRating = offr.accommodation.rating.ToString();
                            minPriceToolObject.tripAdvisorRating = offr.hotel.rating.averageRating.ToString();
                            minPriceToolObject.reviewCount = offr.hotel.rating.reviewCount.ToString();
                            minPriceToolObject.boardBasis = offr.accommodation.boardTypeCode;
                            minPriceToolObject.displayParentRegion = offr.journey.parentRegion;
                            minPriceToolObject.displaySectionName = dynamicQuery.displaySectionName;
                            if (Global.GuidetoGuideBoundaries.ContainsKey(dynamicQuery.sectionId.ToLower().Replace("guide_", "")))
                            {
                                var boundaries = Global.GuidetoGuideBoundaries[dynamicQuery.sectionId.ToLower().Replace("guide_", "")].Split(',');
                                try
                                {
                                    if (double.Parse(offr.journey.glat) >= double.Parse(boundaries[1]) && double.Parse(boundaries[3]) >= double.Parse(offr.journey.glat) && double.Parse(offr.journey.glong) >= double.Parse(boundaries[0]) && double.Parse(boundaries[2]) >= double.Parse(offr.journey.glong))
                                    {

                                        minPriceToolObject.glat = offr.journey.glat;
                                        minPriceToolObject.glong = offr.journey.glong;
                                    }
                                    else
                                    {
                                        minPriceToolObject.glat = string.Empty;//offr.journey.glat;
                                        minPriceToolObject.glong = string.Empty;//offr.journey.glong;
                                    }
                                }
                                catch
                                {
                                    minPriceToolObject.glat = string.Empty;//offr.journey.glat;
                                    minPriceToolObject.glong = string.Empty;//offr.journey.glong;
                                }

                            }
                            else
                            {
                                minPriceToolObject.glat = string.Empty;//offr.journey.glat;
                                minPriceToolObject.glong = string.Empty;//offr.journey.glong;
                            }
                            minPriceToolObject.isImageOverride = dynamicQuery.isImageOverride;
                            minPriceToolObject.isFromMobile = dynamicQuery.isFromMobile;
                            minPriceToolObject.isFromDesktop = dynamicQuery.isFromDesktop;
                            MinPriceToolQuery minPriceToolQuery = new MinPriceToolQuery();
                            minPriceToolQuery.id = dynamicQuery.query.id;
                            minPriceToolQuery.name = offr.hotel.name;
                            minPriceToolQuery.description = dynamicQuery.query.name;
                            minPriceToolQuery.baseDepartureDate = offr.journey.departureDate.ToString();
                            minPriceToolQuery.destinationIds = dynamicQuery.query.destinationIds;
                            minPriceToolQuery.departureIds = dynamicQuery.query.departureIds;
                            minPriceToolQuery.boardType = dynamicQuery.query.boardType;
                            minPriceToolQuery.duration = dynamicQuery.query.duration;
                            minPriceToolQuery.adults = dynamicQuery.query.adults;
                            minPriceToolQuery.children = dynamicQuery.query.children;
                            minPriceToolQuery.priceMax = dynamicQuery.query.priceMax;
                            minPriceToolQuery.priceMin = dynamicQuery.query.priceMin;
                            minPriceToolQuery.ratings = dynamicQuery.query.ratings;
                            minPriceToolQuery.channelId = dynamicQuery.query.channelId;
                            minPriceToolQuery.flexibleDates = dynamicQuery.query.flexibleDates;
                            minPriceToolQuery.dateOffset = dynamicQuery.query.dateOffset;
                            minPriceToolQuery.iff = offr.hotel.iff;
                            minPriceToolQuery.tradingNameId = offr.tradingNameId;
                            minPriceToolQuery.channelName = dynamicQuery.query.channelName;
                            minPriceToolQuery.dateMax = dynamicQuery.query.dateMax;
                            minPriceToolQuery.dateMin = dynamicQuery.query.dateMin;
                            minPriceToolQuery.destinationName = dynamicQuery.query.destinationName;
                            minPriceToolQuery.departureName = dynamicQuery.query.departureName;
                            minPriceToolQuery.websiteUrl = ConfigurationManager.AppSettings["WebsiteSearchURL"] + "/" + dynamicQuery.query.channelName + "/" + dynamicQuery.query.destinationName + "/" + offr.journey.departureDate.ToString("yyyy-MM-dd") + "/" + Utilities.GetWebsitePostUrl(dynamicQuery.query.departureName,
                                                                                        string.Join(",", dynamicQuery.query.boardType), dynamicQuery.query.duration.ToString(), dynamicQuery.query.adults.ToString(),
                                                               dynamicQuery.query.children.ToString(), string.Join(",", dynamicQuery.query.ratings), dynamicQuery.query.priceMin.ToString(), dynamicQuery.query.priceMax.ToString(),
                                                        offr.hotel.iff, offr.tradingNameId, dynamicQuery.query.dateOffset.ToString(), dynamicQuery.query.flexibleDates.ToString());
                            minPriceToolQuery.artirixUrl = artirixUrl + "&hotelKeys=" + offr.hotel.iff;
                            minPriceToolQuery.isDynamic = true;
                            minPriceToolQuery.dynamicUrl = artirixUrl + "&hotelKeys=" + offr.hotel.iff;
                            minPriceToolObject.query = minPriceToolQuery;
                            minPriceToolSectionIdObject.Add(minPriceToolObject);
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log("SectionId not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("SectionId not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }

            return minPriceToolSectionIdObject;

        }
        public static MinPriceToolObject queryId(string queryId, string sectionId)
        {
            MinPriceToolObject minPriceToolOutputObject = null;
            try
            {
                var jsonObjectOutput = String.Empty;
                string searchObject = sectionId + queryId;
                string outputUrl = ConfigurationManager.AppSettings["AWSIndividualURLsFolderURL"] + searchObject + ".json";
                jsonObjectOutput = Utilities.ExecuteGetWebRequest(outputUrl);
                minPriceToolOutputObject = JsonConvert.DeserializeObject<MinPriceToolObject>(jsonObjectOutput);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("No queryId found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return minPriceToolOutputObject;

        }

        public static List<MinPriceToolObject> SetQueryImageUrl(string queryId, string sectionId, string imageUrl)
        {
            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            try
            {
                string inputUrl = ConfigurationManager.AppSettings["MinPriceInput"];

                string jsonObjectInput = Utilities.ExecuteGetWebRequest(inputUrl);
                minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);
                int countIn = minPriceToolInputObject.Count;
                for (int count = 0; count < countIn; count++)
                {
                    if (minPriceToolInputObject[count].sectionId.ToLower() == sectionId.ToLower() && minPriceToolInputObject[count].query.id.ToLower() == queryId.ToLower())
                    {
                        minPriceToolInputObject[count].image = imageUrl;
                        minPriceToolInputObject[count].isImageOverride = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at Image Updation:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return minPriceToolInputObject;
        }
        public static List<MinPriceToolObject> InsertNewObjectToS3(string artirixSearchUrl, saveUpdateMinPrice saveUpdateMinPrice, string preUrl, string postUrl)
        {
            string inputUrl = ConfigurationManager.AppSettings["MinPriceOutput"];
            string inputFileName = Utilities.ExecuteGetWebRequest(inputUrl);
            Holidays holidays = new Holidays();
            string artirixRespStr = Utilities.ExecuteGetWebRequest("http://fed.prd.tt.artirix.com/search?hotelKeys=" + saveUpdateMinPrice.hotelKeys);
            Dictionary<string, int> iffToImageCountList = Global.iffToImageCountList;
            holidays = JsonConvert.DeserializeObject<Holidays>(artirixRespStr);
            MinPriceToolObject ardata = null;
            List<MinPriceToolObject> minPriceToolInput = new List<MinPriceToolObject>();
            minPriceToolInput = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(inputFileName);
            var sectionIdQueryIdChecker = minPriceToolInput.FindAll(r => r.sectionId.ToLower() == saveUpdateMinPrice.sectionId.ToLower() && r.query.id.ToLower() == saveUpdateMinPrice.ID.ToLower());
            if (sectionIdQueryIdChecker.Count != 0)
            {
                throw new WebFaultException<string>("The combination of Section Id and ID exists ", System.Net.HttpStatusCode.InternalServerError);
            }
            try
            {
                if (saveUpdateMinPrice.sectionId == null || saveUpdateMinPrice.ID == null)
                {
                    throw new WebFaultException<string>("Section Id or Id cannot be null \n Please check the fields again", System.Net.HttpStatusCode.InternalServerError);
                }
                ardata = new MinPriceToolObject();
                ardata.query = new MinPriceToolQuery();
                ardata.queryId = System.Guid.NewGuid().ToString();
                ardata.sectionId = saveUpdateMinPrice.sectionId;
                var dt = DateTime.UtcNow;
                ardata.minPrice = int.Parse(saveUpdateMinPrice.minPrice);
                ardata.lastUpdated = String.Format("{0:s}", dt);
                ardata.quoteRef = saveUpdateMinPrice.quoteRef;
                ardata.hotelLocation = Utilities.RemoveDiacritics(holidays.offers[0].journey.destination);
                ardata.starRating = holidays.offers[0].accommodation.rating.ToString();
                ardata.reviewCount = holidays.offers[0].hotel.rating.reviewCount.ToString();
                if (iffToImageCountList.ContainsKey(saveUpdateMinPrice.hotelKeys))
                {
                    ardata.image = string.Format(ConfigurationManager.AppSettings["TeletextImgPath"], saveUpdateMinPrice.hotelKeys + "/1.jpg");
                }
                else
                {
                    ardata.image = "http://images.tt.artirix.com" + holidays.offers[0].hotel.images[0] + "-240mx.jpg";
                }
                ardata.displaySectionName = System.Web.HttpUtility.HtmlEncode(saveUpdateMinPrice.displaySectionName);
                if (saveUpdateMinPrice.boardType == "5")
                {
                    ardata.boardBasis = "All Inclusive";
                }
                else if (saveUpdateMinPrice.boardType == "4")
                {
                    ardata.boardBasis = "Full Board";
                }
                else if (saveUpdateMinPrice.boardType == "2")
                {
                    ardata.boardBasis = "Self Catering";
                }
                else if (saveUpdateMinPrice.boardType == "8")
                {
                    ardata.boardBasis = "Bed & Breakfast";
                }
                else if (saveUpdateMinPrice.boardType == "9")
                {
                    ardata.boardBasis = "Room Only";
                }
                else if (saveUpdateMinPrice.boardType == "3")
                {
                    ardata.boardBasis = "Half Board";
                }
                else
                {
                    ardata.boardBasis = "All Inclusive";
                }
                ardata.displayParentRegion = Utilities.RemoveDiacritics(holidays.offers[0].journey.parentRegion);
                ardata.tripAdvisorRating = holidays.offers[0].hotel.rating.averageRating.ToString();
                ardata.query.adults = int.Parse(saveUpdateMinPrice.adults);
                ardata.query.baseDepartureDate = saveUpdateMinPrice.departureDate;
                ardata.query.boardType = saveUpdateMinPrice.boardType.Split(',').Select(int.Parse).ToList();
                ardata.query.channelId = saveUpdateMinPrice.channelId;
                ardata.query.children = int.Parse(saveUpdateMinPrice.children);
                ardata.query.departureIds = saveUpdateMinPrice.departureIds.Split(',').Select(int.Parse).ToList();
                ardata.query.description = Utilities.RemoveDiacritics(saveUpdateMinPrice.description);
                ardata.query.destinationIds = saveUpdateMinPrice.destinationIds.Split(',').Select(int.Parse).ToList();
                ardata.query.duration = int.Parse(saveUpdateMinPrice.durationMax);
                ardata.query.isDynamic = saveUpdateMinPrice.isDynamic;
                ardata.query.dynamicUrl = saveUpdateMinPrice.dynamicUrl;
                if (!string.IsNullOrEmpty(saveUpdateMinPrice.iff))
                {
                    ardata.query.iff = saveUpdateMinPrice.iff;
                }
                else
                {
                    ardata.query.iff = string.Empty;
                }
                if (saveUpdateMinPrice.flexDates == "yes")
                {
                    ardata.query.flexibleDates = true;
                }
                else
                {
                    ardata.query.flexibleDates = false;
                }
                ardata.query.dateOffset = int.Parse(saveUpdateMinPrice.dateOffSet);
                ardata.query.name = Utilities.RemoveDiacritics(saveUpdateMinPrice.hotelName);
                ardata.query.id = saveUpdateMinPrice.ID;
                ardata.query.channelName = Utilities.RemoveDiacritics(saveUpdateMinPrice.channelName);
                ardata.query.departureName = Utilities.RemoveDiacritics(saveUpdateMinPrice.departureName);
                ardata.query.destinationName = Utilities.RemoveDiacritics(saveUpdateMinPrice.destinationName);
                ardata.query.dateMin = saveUpdateMinPrice.dateMin;
                ardata.query.dateMax = saveUpdateMinPrice.dateMax;
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMax))
                {
                    ardata.query.priceMax = int.Parse(saveUpdateMinPrice.priceMax);
                }
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMin))
                {
                    ardata.query.priceMin = int.Parse(saveUpdateMinPrice.priceMin);
                }
                ardata.query.ratings = saveUpdateMinPrice.starRatings.Split(',').Select(int.Parse).ToList();
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.tradingNameIds))
                {
                    ardata.query.tradingNameId = saveUpdateMinPrice.tradingNameIds;
                }
                ardata.query.artirixUrl = artirixSearchUrl;
                ardata.query.websiteUrl = preUrl + "/" + saveUpdateMinPrice.departureDate + "/" + postUrl;
                ardata.glat = saveUpdateMinPrice.glat;
                ardata.glong = saveUpdateMinPrice.glong;
                ardata.isImageOverride = true;
                ardata.isFromDesktop = saveUpdateMinPrice.isFromDesktop;
                ardata.isFromMobile = saveUpdateMinPrice.isFromMobile;

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at MinPriceinsertion:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            if (!minPriceToolInput.Contains(ardata))
            {
                minPriceToolInput.Add(ardata);
            }
            return minPriceToolInput;
        }
        public static List<MinPriceToolObject> UpdateOldObjectToS3(string artirixSearchUrl, saveUpdateMinPrice saveUpdateMinPrice, string preUrl, string postUrl)
        {
            List<MinPriceToolObject> minPriceToolInputObject = new List<MinPriceToolObject>();
            try
            {
                if (saveUpdateMinPrice.sectionId == null)
                {
                    throw new WebFaultException<string>("Section Id cannot be null \n Please check the fields again", System.Net.HttpStatusCode.InternalServerError);
                }
                string inputUrl = ConfigurationManager.AppSettings["MinPriceInput"];
                Holidays holidays = new Holidays();
                string artirixRespStr = Utilities.ExecuteGetWebRequest("http://fed.prd.tt.artirix.com/search?hotelKeys=" + saveUpdateMinPrice.hotelKeys);
                Dictionary<string, int> iffToImageCountList = Global.iffToImageCountList;
                holidays = JsonConvert.DeserializeObject<Holidays>(artirixRespStr);
                string jsonObjectInput = Utilities.ExecuteGetWebRequest(inputUrl);
                minPriceToolInputObject = JsonConvert.DeserializeObject<List<MinPriceToolObject>>(jsonObjectInput);

                var sectionIdQueryIdChecker = minPriceToolInputObject.Find(r => r.sectionId.ToLower() == saveUpdateMinPrice.sectionId.ToLower() && r.query.id.ToLower() == saveUpdateMinPrice.ID.ToLower());

                if (sectionIdQueryIdChecker == null)
                {
                    throw new WebFaultException<string>("The combination of Section Id and ID not found ", System.Net.HttpStatusCode.InternalServerError);
                }

                var dt = DateTime.Now;
                sectionIdQueryIdChecker.minPrice = int.Parse(saveUpdateMinPrice.minPrice);
                sectionIdQueryIdChecker.lastUpdated = String.Format("{0:s}", dt);
                sectionIdQueryIdChecker.quoteRef = saveUpdateMinPrice.quoteRef;
                sectionIdQueryIdChecker.sectionId = saveUpdateMinPrice.sectionId;
                sectionIdQueryIdChecker.hotelLocation = Utilities.RemoveDiacritics(holidays.offers[0].journey.destination);
                sectionIdQueryIdChecker.starRating = holidays.offers[0].accommodation.rating.ToString();
                sectionIdQueryIdChecker.reviewCount = holidays.offers[0].hotel.rating.reviewCount.ToString();
                if (saveUpdateMinPrice.isImageOverride == true)
                {
                    if (iffToImageCountList.ContainsKey(saveUpdateMinPrice.hotelKeys))
                    {
                        sectionIdQueryIdChecker.image = string.Format(ConfigurationManager.AppSettings["TeletextImgPath"], saveUpdateMinPrice.hotelKeys + "/1.jpg");
                    }
                    else
                    {
                        sectionIdQueryIdChecker.image = "http://images.tt.artirix.com" + holidays.offers[0].hotel.images[0] + "-240mx.jpg";
                    }
                    sectionIdQueryIdChecker.isImageOverride = true;
                }
                else
                {
                    sectionIdQueryIdChecker.isImageOverride = false;

                }

                sectionIdQueryIdChecker.displaySectionName = System.Web.HttpUtility.HtmlEncode(saveUpdateMinPrice.displaySectionName);
                if (saveUpdateMinPrice.boardType == "5")
                {
                    sectionIdQueryIdChecker.boardBasis = "All Inclusive";
                }
                else if (saveUpdateMinPrice.boardType == "4")
                {
                    sectionIdQueryIdChecker.boardBasis = "Full Board";
                }
                else if (saveUpdateMinPrice.boardType == "2")
                {
                    sectionIdQueryIdChecker.boardBasis = "Self Catering";
                }
                else if (saveUpdateMinPrice.boardType == "8")
                {
                    sectionIdQueryIdChecker.boardBasis = "Bed & Breakfast";
                }
                else if (saveUpdateMinPrice.boardType == "9")
                {
                    sectionIdQueryIdChecker.boardBasis = "Room Only";
                }
                else if (saveUpdateMinPrice.boardType == "3")
                {
                    sectionIdQueryIdChecker.boardBasis = "Half Board";
                }
                else
                {
                    sectionIdQueryIdChecker.boardBasis = "All Inclusive";
                }
                sectionIdQueryIdChecker.displayParentRegion = Utilities.RemoveDiacritics(holidays.offers[0].journey.parentRegion);
                sectionIdQueryIdChecker.tripAdvisorRating = holidays.offers[0].hotel.rating.averageRating.ToString();
                sectionIdQueryIdChecker.query.adults = int.Parse(saveUpdateMinPrice.adults);
                sectionIdQueryIdChecker.query.baseDepartureDate = saveUpdateMinPrice.departureDate;
                sectionIdQueryIdChecker.query.boardType = saveUpdateMinPrice.boardType.Split(',').Select(int.Parse).ToList();
                sectionIdQueryIdChecker.query.channelId = saveUpdateMinPrice.channelId;
                sectionIdQueryIdChecker.query.children = int.Parse(saveUpdateMinPrice.children);
                sectionIdQueryIdChecker.query.departureIds = saveUpdateMinPrice.departureIds.Split(',').Select(int.Parse).ToList();
                sectionIdQueryIdChecker.query.isDynamic = saveUpdateMinPrice.isDynamic;
                sectionIdQueryIdChecker.query.dynamicUrl = saveUpdateMinPrice.dynamicUrl;
                if (saveUpdateMinPrice.flexDates == "yes")
                {
                    sectionIdQueryIdChecker.query.flexibleDates = true;
                }
                else
                {
                    sectionIdQueryIdChecker.query.flexibleDates = false;
                }
                sectionIdQueryIdChecker.query.dateOffset = int.Parse(saveUpdateMinPrice.dateOffSet);

                sectionIdQueryIdChecker.query.description = Utilities.RemoveDiacritics(saveUpdateMinPrice.description);
                sectionIdQueryIdChecker.query.destinationIds = saveUpdateMinPrice.destinationIds.Split(',').Select(int.Parse).ToList();
                sectionIdQueryIdChecker.query.duration = int.Parse(saveUpdateMinPrice.durationMax);
                sectionIdQueryIdChecker.query.id = saveUpdateMinPrice.ID;
                if (!string.IsNullOrEmpty(saveUpdateMinPrice.iff))
                {
                    sectionIdQueryIdChecker.query.iff = saveUpdateMinPrice.iff;
                }
                else
                {
                    sectionIdQueryIdChecker.query.iff = string.Empty;
                }
                sectionIdQueryIdChecker.query.name = Utilities.RemoveDiacritics(saveUpdateMinPrice.hotelName);
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMax))
                {
                    sectionIdQueryIdChecker.query.priceMax = int.Parse(saveUpdateMinPrice.priceMax);
                }
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.priceMin))
                {
                    sectionIdQueryIdChecker.query.priceMin = int.Parse(saveUpdateMinPrice.priceMin);
                }
                sectionIdQueryIdChecker.query.ratings = saveUpdateMinPrice.starRatings.Split(',').Select(int.Parse).ToList();
                if (!string.IsNullOrWhiteSpace(saveUpdateMinPrice.tradingNameIds))
                {
                    sectionIdQueryIdChecker.query.tradingNameId = saveUpdateMinPrice.tradingNameIds;
                }
                sectionIdQueryIdChecker.query.artirixUrl = artirixSearchUrl;
                sectionIdQueryIdChecker.query.websiteUrl = preUrl + "/" + saveUpdateMinPrice.departureDate + "/" + postUrl;
                sectionIdQueryIdChecker.query.destinationName = Utilities.RemoveDiacritics(saveUpdateMinPrice.destinationName);
                sectionIdQueryIdChecker.query.channelName = Utilities.RemoveDiacritics(saveUpdateMinPrice.channelName);
                sectionIdQueryIdChecker.query.departureName = Utilities.RemoveDiacritics(saveUpdateMinPrice.departureName);
                sectionIdQueryIdChecker.query.dateMax = saveUpdateMinPrice.dateMax;
                sectionIdQueryIdChecker.query.dateMin = saveUpdateMinPrice.dateMin;
                sectionIdQueryIdChecker.glat = saveUpdateMinPrice.glat;
                sectionIdQueryIdChecker.glong = saveUpdateMinPrice.glong;
                sectionIdQueryIdChecker.isFromMobile = saveUpdateMinPrice.isFromMobile;
                sectionIdQueryIdChecker.isFromDesktop = saveUpdateMinPrice.isFromDesktop;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error at MinPriceupdation:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                throw ex;
            }
            return minPriceToolInputObject;
        }
    }
}