﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Presentation.Web.WCFRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRoutingService" in both code and config file together.
    [ServiceContract]
    public interface IRoutingService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare, 
                                   UriTemplate = "ProcessS3RecordsByAirportCodeAndResortIdFirstSet")]
        Stream ProcessS3RecordsByAirportCodeAndResortIdFirstSet();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ProcessS3RecordsByAirportCodeAndResortIdSecondSet")]
        Stream ProcessS3RecordsByAirportCodeAndResortIdSecondSet();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ProcessS3RecordsByAirportCodeAndResortIdThirdSet")]
        Stream ProcessS3RecordsByAirportCodeAndResortIdThirdSet();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ProcessS3RecordsByAirportCodeAndResortIdFourthSet")]
        Stream ProcessS3RecordsByAirportCodeAndResortIdFourthSet();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ProcessS3RecordsByAirportCode")]
        Stream ProcessS3RecordsByAirportCode();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "ProcessS3RecordsByResortId")]
        Stream ProcessS3RecordsByResortId();
    }
}
