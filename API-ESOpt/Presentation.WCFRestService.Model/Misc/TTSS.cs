﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Presentation.WCFRestService.Model.Misc
{
    [XmlRoot(ElementName = "Result")]
    public class Result
    {
        [XmlAttribute(AttributeName = "destinationId")]
        public string DestinationId { get; set; }
        [XmlAttribute(AttributeName = "tradingNameId")]
        public string TradingNameId { get; set; }
        [XmlAttribute(AttributeName = "departureDate")]
        public string DepartureDate { get; set; }
        [XmlAttribute(AttributeName = "outboundArrivalDate")]
        public string OutboundArrivalDate { get; set; }
        [XmlAttribute(AttributeName = "inboundDepartureDate")]
        public string InboundDepartureDate { get; set; }
        [XmlAttribute(AttributeName = "returnDate")]
        public string ReturnDate { get; set; }
        [XmlAttribute(AttributeName = "duration")]
        public string Duration { get; set; }
        [XmlAttribute(AttributeName = "departureId")]
        public string DepartureId { get; set; }
        [XmlAttribute(AttributeName = "departure")]
        public string Departure { get; set; }
        [XmlAttribute(AttributeName = "boardTypeId")]
        public string BoardTypeId { get; set; }
        [XmlAttribute(AttributeName = "boardTypeCode")]
        public string BoardTypeCode { get; set; }
        [XmlAttribute(AttributeName = "adults")]
        public string Adults { get; set; }
        [XmlAttribute(AttributeName = "children")]
        public string Children { get; set; }
        [XmlAttribute(AttributeName = "rating")]
        public string Rating { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string Price { get; set; }
        [XmlAttribute(AttributeName = "gatewayCode")]
        public string GatewayCode { get; set; }
        [XmlAttribute(AttributeName = "gatewayName")]
        public string GatewayName { get; set; }
        [XmlAttribute(AttributeName = "accommodation")]
        public string Accommodation { get; set; }
        [XmlAttribute(AttributeName = "updated")]
        public string Updated { get; set; }
        [XmlAttribute(AttributeName = "outboundFlightDirect")]
        public string OutboundFlightDirect { get; set; }
        [XmlAttribute(AttributeName = "inboundFlightDirect")]
        public string InboundFlightDirect { get; set; }
        [XmlAttribute(AttributeName = "destination")]
        public string Destination { get; set; }
        [XmlAttribute(AttributeName = "glong")]
        public string Glong { get; set; }
        [XmlAttribute(AttributeName = "glat")]
        public string Glat { get; set; }
        [XmlAttribute(AttributeName = "hotelOperator")]
        public string HotelOperator { get; set; }
        [XmlAttribute(AttributeName = "contentId")]
        public string ContentId { get; set; }
        [XmlAttribute(AttributeName = "hotelKey")]
        public string HotelKey { get; set; }
        [XmlAttribute(AttributeName = "quoteRef")]
        public string QuoteRef { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "flightOperator")]
        public string FlightOperator { get; set; }
        [XmlAttribute(AttributeName = "isPreferential")]
        public string IsPreferential { get; set; }
        [XmlAttribute(AttributeName = "allocation")]
        public string Allocation { get; set; }
        [XmlAttribute(AttributeName = "contentSource")]
        public string ContentSource { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "tradingName")]
        public string TradingName { get; set; }
        [XmlAttribute(AttributeName = "tradingNameImage")]
        public string TradingNameImage { get; set; }
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }
    }

    [XmlRoot(ElementName = "Results")]
    public class Results
    {
        [XmlElement(ElementName = "Result")]
        public List<Result> Result { get; set; }
        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
    }

    [XmlRoot(ElementName = "Search")]
    public class Search
    {
        [XmlAttribute(AttributeName = "tradingNameId")]
        public string TradingNameId { get; set; }
        [XmlAttribute(AttributeName = "dateMin")]
        public string DateMin { get; set; }
        [XmlAttribute(AttributeName = "dateMax")]
        public string DateMax { get; set; }
        [XmlAttribute(AttributeName = "durationMin")]
        public string DurationMin { get; set; }
        [XmlAttribute(AttributeName = "durationMax")]
        public string DurationMax { get; set; }
        [XmlAttribute(AttributeName = "adults")]
        public string Adults { get; set; }
        [XmlAttribute(AttributeName = "children")]
        public string Children { get; set; }
        [XmlAttribute(AttributeName = "destinationIds")]
        public string DestinationIds { get; set; }
        [XmlAttribute(AttributeName = "timeout")]
        public string Timeout { get; set; }
        [XmlAttribute(AttributeName = "boardBasisId")]
        public string BoardBasisId { get; set; }
        [XmlAttribute(AttributeName = "departureId")]
        public string DepartureId { get; set; }
    }

    [XmlRoot(ElementName = "Container")]
    public class Container
    {
        [XmlElement(ElementName = "Results")]
        public Results Results { get; set; }
        [XmlElement(ElementName = "Search")]
        public Search Search { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
    }

}
