﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.HomePage
{
    public class AirportGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }

    }
}
