﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class ResponseSummary
    {
        public string errorMessage { get; set; }
        public int errorCode { get; set; }
    }
}
