﻿using System;
namespace Presentation.WCFRestService.Model.Reviews
{
    public class ReviewInformation
    {
        public string reviewId { get; set; }
        public string reviewTitle { get; set; }
        public string description { get; set; }
        public int reviewRating { get; set; }
        public CustomerInfo customer { get; set; }
        public string postedOn { get; set; }
        public string reviewProviderName { get; set; }
        public bool isPublished { get; set; }
    }
    
    public class CustomerInfo
    {
        public string postedBy { get; set; }
    }
}
