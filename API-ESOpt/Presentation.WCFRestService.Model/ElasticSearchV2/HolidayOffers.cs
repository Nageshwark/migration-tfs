﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class HolidayOffers
    {
        //public int total { get; set; }
        public List<HOffer> offers { get; set; }
        public Facets facets { get; set; }
        public int totalHotels { get; set; }
    }
    public class Facets
    {
        public List<FacetHotel> hotels { get; set; }
        public List<Airport> airports { get; set; }
        public Pricerange priceRange { get; set; }
        public List<Resort> resorts { get; set; }
        //public int totalHotels { get; set; }
    }
    public class Resort
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public int count { get; set; }
    }
    public class Pricerange
    {
        public int min { get; set; }
        public int max { get; set; }
    }
    public class Airport
    {
        public string name { get; set; }
        public int departureId { get; set; }
        public int count { get; set; }
    }
    public class FacetHotel
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public string hotelKey { get; set; }
        public int count { get; set; }
    }
    public class HOffer
    {
        public string id { get; set; } //TTSS
        public string quoteRef { get; set; } //TTSS
        public int price { get; set; } //TTSS
        public bool isPreferential { get; set; }  //if TAId = 576 true else false
        public string phone { get; set; }  //TTSS
        public Journey journey { get; set; }
        public Accommodation accommodation { get; set; }
        public OfferHotel hotel { get; set; } 
        //public string hotelOperator { get; set; }
        //public string contentId { get; set; }
        public string tradingNameId { get; set; } //TTSS
        //public object score { get; set; }
        public bool isPreferredByUser { get; set; }
         public double relavanceScore { get; set; }
    }
    public class Journey
    {
        public DateTime outboundArrivalDate { get; set; } //TTSS
        public DateTime inboundDepartureDate { get; set; } //TTSS
        public DateTime departureDate { get; set; } //TTSS
        public DateTime returnDate { get; set; } //TTSS
        public int duration { get; set; } //TTSS
        public string departure { get; set; } //TTSS
        public string gatewayName { get; set; } //TTSS
        //public string flightOperator { get; set; }
        public string destination { get; set; }  //TTSS
        public string glat { get; set; }  //TTSS
        public string glong { get; set; } //TTSS
        public string parentRegion { get; set; } //Static
        public int parentRegionId { get; set; }  //Static
        public string gatewayCode { get; set; } //TTSS
        //public bool inboundFlightDirect { get; set; }
        //public bool outboundFlightDirect { get; set; }
    }
    public class Accommodation
    {
        public string boardTypeCode { get; set; } //TTSS
        public int adults { get; set; }  //TTSS
        public int children { get; set; }  //TTSS
        public int rating { get; set; }  //TTSS
        public DateTime updated { get; set; }  //TTSS
    } 
    public class OfferHotel
    {
        public string iff { get; set; } //TTSS
        //public string latlon { get; set; }
        //public string starRating { get; set; }
        public List<string> images { get; set; } // Mapping
        public List<string> mobileimages { get; set; } //Mapping
        public List<string> thumbnailimages { get; set; } //Mapping
        //public string description { get; set; }
        public List<object> features { get; set; } //static
        public List<string> tags { get; set; }
        public string name { get; set; } //static
        public Rating rating { get; set; }
    }
    public class Rating
    {
        public string tripAdvisorId { get; set; } //Static
        public float averageRating { get; set; } //Static
        public int reviewCount { get; set; } //Static
    }
}
