﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class TTSSResonseParms
    {
        public string Status { get; set; }
        public int Count { get; set; }
        public List<Result> Results { get; set; }
        //public Search Search { get; set; }
    }
    public class Result
    {
        public string destinationId { get; set; }
        public string tradingNameId { get; set; }
        public string departureDate { get; set; }
        public string outboundArrivalDate { get; set; }
        public string inboundDepartureDate { get; set; }
        public string returnDate { get; set; }
        public string duration { get; set; }
        public string departureId { get; set; }
        public string departure { get; set; }
        public string boardTypeId { get; set; }
        public string boardTypeCode { get; set; }
        public string adults { get; set; }
        public string children { get; set; }
        public string rating { get; set; }
        public int price { get; set; }
        public string gatewayCode { get; set; }
        public string gatewayName { get; set; }
        public string accommodation { get; set; }
        public string updated { get; set; }
        public string outboundFlightDirect { get; set; }
        public string inboundFlightDirect { get; set; }
        public string destination { get; set; }
        public string glong { get; set; }
        public string glat { get; set; }
        public string hotelOperator { get; set; }
        public string contentId { get; set; }
        public string hotelKey { get; set; }
        public string quoteRef { get; set; }
        public string id { get; set; }
        public string flightOperator { get; set; }
        public string isPreferential { get; set; }
        public string allocation { get; set; }
        public string contentSource { get; set; }
        public string source { get; set; }
        public string tradingName { get; set; }
        public string tradingNameImage { get; set; }
        public string phone { get; set; }
    }

    public class Search
    {
        public string tradingNameId { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public string durationMin { get; set; }
        public string durationMax { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public string destinationId { get; set; }
        public double timeout { get; set; }
        public int boardBasisId { get; set; }
        public string departureId { get; set; }
    }
    public class TTSSResponseStream
    {
        public string ttssResponse { get; set; }
        public double ttssResponseTime { get; set; }

    }
}
