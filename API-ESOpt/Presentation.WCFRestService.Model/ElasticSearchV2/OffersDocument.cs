﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class OffersDocument
    {
        public string id { get; set; }
        public string quoteRef { get; set; }
        public int price { get; set; }
        public bool isPreferential { get; set; }
        //public string phone { get; set; }
        public OffersDocumentJourney journey { get; set; }
        public OffersDocumentAccommodation accommodation { get; set; }
        public OffersDocumentOfferHotel hotel { get; set; }
        //public string hotelOperator { get; set; }
        //public string contentId { get; set; }
        public List<int> tradingNameId { get; set; }
        //public bool isPreferredByUser { get; set; }
        public int departureIds { get; set; }
        public int parentDepartureIds { get; set; }
        public int regionId { get; set; }
        public int labelId { get; set; }
        //public long responseId { get; set; }
        //public int resortId { get; set; }
        //public string regionName { get; set; }
        public string timeStamp { get; set; }
    }
    public class OffersDocumentJourney
    {
        public string outboundArrivalDate { get; set; }
        public string inboundDepartureDate { get; set; }
        public string departureDate { get; set; }
        public string returnDate { get; set; }
        public int duration { get; set; }
        public string departure { get; set; }
        public string gatewayName { get; set; }
        //public string flightOperator { get; set; }
        public string destination { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public string gatewayCode { get; set; }
       // public bool inboundFlightDirect { get; set; }
        //public bool outboundFlightDirect { get; set; }
    }
    public class OffersDocumentAccommodation
    {
        public string boardTypeCode { get; set; }
        public int boardTypeId { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int rating { get; set; }
        public string updated { get; set; }
    }

    public class OffersDocumentOfferHotel
    {
        public int iff { get; set; }
        //public List<string> images { get; set; }
        //public List<string> mobileimages { get; set; }
        //public List<string> thumbnailimages { get; set; }
        //public string description { get; set; }
        public List<object> features { get; set; }
        public string name { get; set; }
        //public string latlong { get; set; }
        //public string starRating { get; set; }
        public OffersDocumentRating rating { get; set; }
    }
    public class OffersDocumentRating
    {
        public string tripAdvisorId { get; set; }
        public int averageRating { get; set; }
        public int reviewCount { get; set; }
    }
}
