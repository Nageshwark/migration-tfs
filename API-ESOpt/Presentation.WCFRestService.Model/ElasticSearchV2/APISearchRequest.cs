﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class APISearchRequest
    {
        public int destinationIds { get; set; }
        public int departureIds { get; set; }
        public List<string> boardType { get; set; }
        public DateTime departureDate { get; set; }
        public DateTime dateMin { get; set; }
        public DateTime dateMax { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public List<string> ratings { get; set; }
        public List<string> tradingNameIds { get; set; }
        public string destinationType { get; set; }
        public List<string> hotelKeys { get; set; }
        public int tripAdvisorRating { get; set; }
        public string sort { get; set; }
        public List<int> hotelKeysToExclude { get; set; }
        public int channelId { get; set; }
        public int labelId { get; set; }
        public List<int> usersPreferredHotelKeys { get; set; }
        public bool  skipFacets { get; set; }
        public List<string> personalizedParms { get; set; }
        public string recentSearchCookie { get; set; }
        public string favoriteCookie { get; set; }

        public APISearchRequest()
        {
            //ToDo: Set default values to the properties
        }

        public APISearchRequest(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string personalizedParms, string recentSearchCookie, string favoriteCookie)
        {
            this.usersPreferredHotelKeys = new List<int>();
            if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                this.usersPreferredHotelKeys = usersPreferredHotelKeys.Split(',').Select(int.Parse).ToList();
            }
            if (string.Compare(skipFacets, "true") == 0)
            {
                this.skipFacets = true;
            }
            else
            {
                this.skipFacets = false;
            }
            this.sort = sort;
            this.channelId = channelId;
            this.labelId = labelId;
            this.hotelKeysToExclude = new List<int>();
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                this.hotelKeysToExclude = hotelKeysToExclude.Split(',').Select(int.Parse).ToList();
            }
            this.tripAdvisorRating = tripAdvisorRating;
            this.hotelKeys = new List<string>();
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                this.hotelKeys = hotelKeys.Split(',').ToList();
            }
            if (string.IsNullOrEmpty(destinationType))
            {
                this.destinationType = ConfigurationManager.AppSettings["DefaultDestinationType"];
            }
            else
            {
                this.destinationType = destinationType;
            }
            this.tradingNameIds = new List<string>();
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                this.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();
            }
            else
            {
                this.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            this.ratings = new List<string>();
            if (string.IsNullOrWhiteSpace(ratings))
            {
                this.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                this.ratings = ratings.Split(',').ToList();
            }
            if (durationMax == 0)
            {
                this.durationMin = 7;
                this.durationMax = 7;
            }
            else
            {
                this.durationMin = durationMin;
                this.durationMax = durationMax;
            }
            if (adults == 0)
            {
                this.adults = 2;
            }
            else
            {
                this.adults = adults;
            }
            this.children = children;
            this.priceMax = priceMax;
            this.priceMin = priceMin;
            this.departureDate = DateTime.Parse(departureDate);
            this.dateMax = DateTime.Parse(dateMax);
            this.dateMin = DateTime.Parse(dateMin);
            this.boardType = new List<string>();
            if (string.IsNullOrWhiteSpace(boardType))
            {
                this.boardType = ConfigurationManager.AppSettings["DefaultBoardType"].Split(',').ToList();
            }
            else
            {
                this.boardType = boardType.Split(',').ToList();
            }
            this.personalizedParms = new List<string>();
            if(!string.IsNullOrWhiteSpace(personalizedParms))
            {
                this.personalizedParms = personalizedParms.Split(',').ToList();
            }
            this.favoriteCookie = favoriteCookie;
            this.recentSearchCookie = recentSearchCookie;
            this.departureIds = departureIds;
            this.destinationIds = destinationIds;
        }
    }
}
