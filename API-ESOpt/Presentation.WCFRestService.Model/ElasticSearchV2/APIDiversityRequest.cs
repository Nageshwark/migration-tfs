﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class APIDiversityRequest
    {
        public int destinationId { get; set; }
        public int departureIds { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public List<string> ratings { get; set; }
        public List<string> tradingNameIds { get; set; }
        public List<string> hotelKeys { get; set; }
    }
}
