﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.DeepLinks
{
    public class QuickQuotesDL
    {
        public string branch_key { get; set; }
        public string campaign { get; set; }
        public string channel { get; set; }
        public DataDL data { get; set; }
    }
    public class DataDL
    {
        public string BookerSurname { get; set; }
        public string DepDate { get; set; }
        public string Reference { get; set; }
        public string Source { get; set; }
    }
    public class Nexmo
    {
        public string MobileNumber { get; set; }
        public string url { get; set; }
    }
    public class Branch
    {
        public string url { get; set; }
    }
    public class DLStatus
    {
        public string mobileNumber { get; set; }
        public string surname { get; set; }
        public string deptDate { get; set; }
        public string reference { get; set; }
        public bool status { get; set; }
    }
    public class APPLink
    {
        public string mobileNumber { get; set; }
        public string subject { get; set; }
        public string from { get; set; }
    }
}
