﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Guides
{
    public class Guide
    {
        [JsonProperty("bgimage")]
        public string bgimage { get; set; }        
        public string bgimage_m { get; set; }        
        public string bgimage_t { get; set; }
        public string bgimage_sd { get; set; }
        [JsonProperty("ptitle")]
        public string ptitle { get; set; }
        [JsonProperty("pmetadesc")]
        public string pmetadesc { get; set; }
        public string sectionid { get; set; }
        [JsonProperty("pmetakeyword")]
        public string pmetakeyword { get; set; }
        [JsonProperty("regid")]
        public string regid { get; set; }
        [JsonProperty("regname")]
        public string regname { get; set; }
        [JsonProperty("reglevel")]
        public string reglevel { get; set; }
        [JsonProperty("showGuideOnMobile")]
        public bool showGuideOnMobile { get; set; }
        [JsonProperty("showLastMinuteDeals")]
        public bool showLastMinuteDeals { get; set; }
        [JsonProperty("recommendedhotels")]
        public List<RecommendedHotel> recommendedhotels { get; set; }
        [JsonProperty("parent_regid")]
        public string parent_regid { get; set; }
        [JsonProperty("parent_regname")]
        public string parent_regname { get; set; }
        [JsonProperty("parent_link")]
        public string parent_link { get; set; }
        [JsonProperty("grand_regid")]
        public string grand_regid { get; set; }
        [JsonProperty("grand_regname")]
        public string grand_regname { get; set; }
        [JsonProperty("grand_reglink")]
        public string grand_reglink { get; set; }
        [JsonProperty("sections")]
        public List<section> sections { get; set; }
        public List<ExpertView> expertviews { get; set; }
        public List<MetaTag> metatags { get; set; }
        [JsonProperty("highSeasonalAverage")]
        public string highSeasonalAverage { get; set; }
        public string dropDownLabelName { get; set; }
    }
}
